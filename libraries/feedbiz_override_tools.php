<?php
define('FB_MAX_TIME_OUT',9999);
define('FB_LOG_GET_LOCAL_FILE',true);
define('FB_LOG_CALL_FILE_PATH',dirname(__FILE__).'/../script/log/');
define('FB_LOG_CALL_FILE_FILEPATH',FB_LOG_CALL_FILE_PATH.'all_call_file.csv');
ini_set('display_errors',1);
$_fb_call_command='';

$_fb_cli= php_sapi_name() == "cli"?true:false;
if($_fb_cli){
    if(!empty($argv)){ $_fb_call_command = 'CLI: '.implode(' ',$argv);
    }
}else{
    if(!empty($_SERVER['REQUEST_URI']))$_fb_call_command = 'URL: '. $_SERVER['REQUEST_URI'];
}

if(!function_exists('fbiz_get_contents')){
    function fbiz_put_calling_csv_log($param=array()){ 
        if(empty($param))return; 
        if(!FB_LOG_GET_LOCAL_FILE && isset($param['type']) && $param['type']=='local')return; 
        if(!file_exists(FB_LOG_CALL_FILE_PATH)){
            mkdir(FB_LOG_CALL_FILE_PATH,0777);
        }   
        $fp = fopen(FB_LOG_CALL_FILE_FILEPATH, 'a');
        
        fputcsv($fp, $param); 
        fclose($fp); 
    }
    function fbiz_put_calling_log($filename,$additional=null){//$additional key = start_at,type,status,time,timeout
        global $_fb_call_command; 
        
        $e = new Exception();
        $log_param=array();
        $start_at = date('Y-m-d H:i:s');
        $status=$type=$timeout='';
        $time = -microtime(true);
        if(!empty($additional) && is_array($additional)){
            foreach($additional as $k =>$v){
                if(is_string($k)){
                    ${$k} = $v;
                }
            }
        }
        $time += microtime(true); 
        
        
        $log_param['start_at'] = $start_at;
        $timeout= $timeout<=0?FB_MAX_TIME_OUT:$timeout;
        $log_param['file_or_url']=$filename;
        $log_param['type']=$type;
        $log_param['status']=$status; 
        $log_param['set_timeout']=$timeout; 
        $log_param['duration']=$time;
        $log_param['cmd']=$_fb_call_command;
        $log_param['call_stack']=$e->getTraceAsString();
        fbiz_put_calling_csv_log($log_param);
    }
    function fbiz_get_contents($filename,$additional=null){
        global $_fb_call_command; 
        $timeout=120;
        $curl_method=false;
        $check_header_first=false;
        $stream_context=null;
        $use_include_path=false;
        if(!empty($additional) && is_array($additional)){
            foreach($additional as $k =>$v){
                if(is_string($k)){
                    ${$k} = $v;
                }
            }
        }else if(!empty($additional) && is_numeric($additional)){
            $timeout = $additional;
        }
        $log_param=array();
        $log_param['start_at'] = date('Y-m-d H:i:s');
        
        
        $call_file=$call_line=$call_from_func='';
        $timeout= $timeout<=0?FB_MAX_TIME_OUT:$timeout;
        $log_param['file_or_url']=$filename;
        $log_param['type']='';
        $log_param['status']=''; 
        $log_param['set_timeout']=$timeout; 
        $log_param['duration']=0;
        $log_param['cmd']=$_fb_call_command;
//        $log_param['call_stack']='';
        $e = new Exception();
//        $trace = $e->getTrace(); 
//        if(isset($trace[0])){
//            $call_file = $trace[0]['file'];
//            $call_line = $trace[0]['line'];
//            $call_from_func = empty($trace[1]['function'])?'':$trace[1]['function'].'()';
//            unset($trace);
//            $log_param['call_stack'] = "$call_from_func @$call_file # $call_line";
            $log_param['call_stack'] = $e->getTraceAsString();
//        } 
        $time = -microtime(true); 
        if (filter_var($filename, FILTER_VALIDATE_URL)) {// get remote file case
            $log_param['type']='remote'; 
            if($check_header_first){
                $file_headers = @get_headers($filename); 
                if($file_headers[0] == 'HTTP/1.0 404 Not Found'){ // "The file $filename does not exist";
                    $time += microtime(true);
                    $log_param['duration'] = $time;
                    $log_param['status']="not exist";
                    fbiz_put_calling_csv_log($log_param);
                    return false;  
                } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found'){// "The file $filename does not exist, and I got redirected to a custom 404 page..";
                    $time += microtime(true);
                    $to = empty($file_headers[7])?'':$file_headers[7];
                    $log_param['duration'] = $time;
                    $log_param['status']="redirect to ($to) and not exist";
                    fbiz_put_calling_csv_log($log_param);
                    return false;
                } else {// "The file $filename exists";
                    $log_param['status']="pass";
                }
            }
        }else{// get local file case
            $log_param['type']='local';
            $log_param['set_timeout']='-';
            if(function_exists('stream_resolve_include_path')){
            if(false !== stream_resolve_include_path($filename)){//check file exist
                $log_param['status']="pass";
            }else{// file not exist
                $time += microtime(true);
                $log_param['duration'] = $time;
                $log_param['status']="not exist";
                fbiz_put_calling_csv_log($log_param);
//                return false;
            }
            }else{
                $time += microtime(true);
                $log_param['duration'] = $time;
                $log_param['status']="not exist";
                fbiz_put_calling_csv_log($log_param);
//                return false;
            }
        }
        
        if($curl_method){ 
            $log_param['type']='curl';
            $return = @curl_get_contents($filename,null,$timeout);
        }else{
            $stream_context = stream_context_create(array(
                'http' => array(
                    'method' => 'GET',
                    'timeout' => $timeout,   
                )
            ));
            $return = @file_get_contents($filename,$use_include_path,$stream_context);
        } 
        if(!empty($return)){
            $log_param['status']="pass (".sizeof($return).")";
        }
        $time += microtime(true);
        $log_param['duration'] = $time;
        fbiz_put_calling_csv_log($log_param);
        return $return;
    }
    
}

if(!function_exists('curl_get_contents')){
    function curl_get_contents($url,$post_data=array(),$timeout=60,$verbose=false,$ref_url=false,$cookie_location=false,$return_transfer=true)
    {
	$return_val = false;
 
	$pointer = curl_init();
 
	curl_setopt($pointer, CURLOPT_URL, $url);
	curl_setopt($pointer, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($pointer, CURLOPT_RETURNTRANSFER, $return_transfer);
	curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.28 Safari/534.10");
	curl_setopt($pointer, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($pointer, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($pointer, CURLOPT_HEADER, false);
	curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($pointer, CURLOPT_AUTOREFERER, true);
 
	if($cookie_location !== false)
	{
		curl_setopt($pointer, CURLOPT_COOKIEJAR, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIEFILE, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIE, session_name() . '=' . session_id());
	}
 
	if($verbose !== false)
	{
		$verbose_pointer = fopen($verbose,'w');
		curl_setopt($pointer, CURLOPT_VERBOSE, true);
		curl_setopt($pointer, CURLOPT_STDERR, $verbose_pointer);
	}
 
	if($ref_url !== false)
	{
	    curl_setopt($pointer, CURLOPT_REFERER, $ref_url);
	}
 
	if(count($post_data) > 0)
	{
	    curl_setopt($pointer, CURLOPT_POST, true);
	    curl_setopt($pointer, CURLOPT_POSTFIELDS, $post_data);
	}
 
	$return_val = curl_exec($pointer);
 
	$http_code = curl_getinfo($pointer, CURLINFO_HTTP_CODE);
 
	if($http_code == 404)
	{
		return false;
	}
 
	curl_close($pointer);
 
	unset($pointer);
 
	return $return_val;
    }
}

