<?php
define('CURRENT_PATH',dirname(__FILE__));


    
class StoreInterface{
    const STORE_REGIST_PATH = 'http://localhost/feed-biz-store/index.php?controller=DataAccounts&fn=newCustomerFeedbiz'; 
    const EXPIRE_TIME_MIN = 15;
    const FB_REGIST_PATH = 'https://client.feed.biz/users/store_register';
    const FB_CHK_LOGIN_PATH = 'https://client.feed.biz/admin/tasks/store_login';
    const FB_USER_JSON_PATH = 'https://client.feed.biz/script/log/users_info4fbstore.json';
    const FB_USER_MFA_JSON_PATH = 'https://client.feed.biz/script/log/mfa_activate_users.json';
    const TMP_STORE_DIR = "/tmp/";
    const FB_USER_JSON_LOCAL = 'users_info4fbstore.json';
    const FB_USER_MFA_JSON_LOCAL = 'mfa_activate_users.json';
    const FB_USER_JSON_LOCAL_EXPIRE = 120;
    public $error_msg = '';
    public function __construct() {
        
    }
    
    public function checkMfaFbUsers($username,$otp){
        $json = $this->getFbUsersMfaJson();
        $users = json_decode($json,true);
        
        $otp_pass=false; 
        if(isset($users[$username])){
           include(CURRENT_PATH.'/mfa/config.php');
           $key = base64_decode($key_list[$username]);
           if(TokenAuth6238::verify($key,$otp)){
               $otp_pass=true;
           }
        }else{
            $otp_pass=true;
        }
        return $otp_pass;
    } 
    public function checkFbUsersExists($username=''){
        $json = $this->getFbUsersJson();
        $users = json_decode($json,true);
        return isset($users[$username])?$users[$username]:false;
    }
    public function getFbUsersMfaJson(){
        $dir = CURRENT_PATH.self::TMP_STORE_DIR;
        $file = $dir.self::FB_USER_MFA_JSON_LOCAL;
        if(!file_exists($dir)){
            mkdir($dir);
        }
        if(!file_exists($file) || (file_exists($file) && (filemtime($file) - time() + self::FB_USER_JSON_LOCAL_EXPIRE) < 0)){
            $out = curl_get_contents(self::FB_USER_MFA_JSON_PATH);
            file_put_contents($file, $out); 
        }else{
            $out = file_get_contents($file);
        }
        return $out; 
    }
    public function getFbUsersJson(){
        $dir = CURRENT_PATH.self::TMP_STORE_DIR;
        $file = $dir.self::FB_USER_JSON_LOCAL;
        if(!file_exists($dir)){
            mkdir($dir);
        }
		//echo filemtime($file)-time()+self::FB_USER_JSON_LOCAL_EXPIRE;
        if(!file_exists($file) || (file_exists($file) && (filemtime($file) - time() + self::FB_USER_JSON_LOCAL_EXPIRE) < 0)){
            $out = curl_get_contents(self::FB_USER_JSON_PATH);
            file_put_contents($file, $out); 
        }else{
            $out = file_get_contents($file);
        }
        return $out; 
    }
    public function checkAvailLogin($data=array(), $getURL = false){
        $key = array('username','password','ip');
        if(empty($data)){return false;}
        foreach($key as $k =>$v){
            if(!isset($data[$v])){
                return false;
            }
        }
        
        if($getURL)
            return self::FB_CHK_LOGIN_PATH.'/'.self::encrypt($data);

        $ret = curl_get_contents(self::FB_CHK_LOGIN_PATH, array('fb_encrypted_data'=>self::encrypt($data)));
        $j = json_decode($ret,true);

        if(isset($j['result']) && $j['result']==true){
            //return true;
            return $j;
        }else{
            $this->error_msg = isset($j['error'])?$j['error']:'';
            return false;
        }
    }
    public function sendNewRegister($data=array()){
        $key = array('username','password','ip','name','lastname');
        if(empty($data)){return false;}
        foreach($key as $k =>$v){
            if(!isset($data[$v])){
                return false;
            }
        }
        $ret = curl_get_contents(self::FB_REGIST_PATH,array('fb_encrypted_data'=>self::encrypt($data)));
        $j = json_decode($ret,true);
        if(isset($j['result']) && $j['result']==true){
        return true;
        }else{
            $this->error_msg = isset($j['error'])?$j['error']:'';
            return false;
        }
    }

    public function sendNewRegisterStore($data=array()){
        $key = array('username','ip','uid','uname');
        if(empty($data)){return false;}
        foreach($key as $k =>$v){
            if(!isset($data[$v])){
                return false;
            }
        }
        $ret = curl_get_contents(self::STORE_REGIST_PATH,array('fb_encrypted_data'=>self::encrypt($data)));
        $j = json_decode($ret,true);
        if(isset($j['result']) && $j['result']==true){
            return true;
        }else{
            $this->error_msg = isset($j['error'])?$j['error']:'';
            return false;
        }
    }
    
    static public function encrypt($data=array()){
        
        $time = time();
        $time_en = md5($time);
        $tf = substr($time, -1,1);
        $ta = substr($time, -2,1);
        $tb = substr($time, -3,1);
        $tg = substr($time, -4,1);
        $pf = substr($time_en, $tf+$tg, 1);
        $pb = substr($time_en, $ta+$tb, 1);
        $time_end = $pb.$time_en.$pf;
        $input = array('k'=> bin2hex(gzcompress($time_end)) ,'a'=>base64_encode(time()),'d'=>$data);
        $f = mt_rand(0,99999);
        $b = mt_rand(0,99999);
        $st = (base64_encode(($f.'!'.base64_encode(json_encode($input)).'!'.$b)));
        return $st;
    }
    
    
    static public function decrypt($code=''){
        $out = base64_decode($code);
        if (strpos($out, "!") !== false) {
            $out = explode('!', $out);
            if (isset($out[1])) {
                $cur_time = time();
                $out = base64_decode($out[1]);
                $data = json_decode($out, true);
                if (!empty($data)) {

                    $key = gzuncompress(hex2bin(($data['k'])));
                    $link_time = base64_decode($data['a']);
                    $transfer_data = $data['d'];
                    $time = $link_time;
                    $time_en = md5($time);
                    $tf = substr($time, -1, 1);
                    $ta = substr($time, -2, 1);
                    $tb = substr($time, -3, 1);
                    $tg = substr($time, -4, 1);
                    $pf = substr($time_en, $tf + $tg, 1);
                    $pb = substr($time_en, $ta + $tb, 1);
                    $time_end = $pb . $time_en . $pf;
                    $dif = round(abs($cur_time - $link_time) / 60, 2);
                    if ($dif <= self::EXPIRE_TIME_MIN && $key == $time_end) {
                        return $transfer_data;
                    }
                }
            }
        }
        return false;
    }
    
}

if (!function_exists('hex2bin')) {
    function hex2bin($str) {
        $sbin = "";
        $len = strlen($str);
        for ($i = 0; $i < $len; $i += 2) {
            $sbin .= pack("H*", substr($str, $i, 2));
        }

        return $sbin;
    }
}

if(!function_exists('curl_get_contents')){
    function curl_get_contents($url,$post_data=array(),$timeout=60,$verbose=false,$ref_url=false,$cookie_location=false,$return_transfer=true)
    {
	$return_val = false;
 
	$pointer = curl_init();
 
	curl_setopt($pointer, CURLOPT_URL, $url);
	curl_setopt($pointer, CURLOPT_TIMEOUT, $timeout);
	curl_setopt($pointer, CURLOPT_RETURNTRANSFER, $return_transfer);
	curl_setopt($pointer, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.28 Safari/534.10");
	curl_setopt($pointer, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($pointer, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($pointer, CURLOPT_HEADER, false);
	curl_setopt($pointer, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($pointer, CURLOPT_AUTOREFERER, true);
 
	if($cookie_location !== false)
	{
		curl_setopt($pointer, CURLOPT_COOKIEJAR, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIEFILE, $cookie_location);
		curl_setopt($pointer, CURLOPT_COOKIE, session_name() . '=' . session_id());
	}
 
	if($verbose !== false)
	{
		$verbose_pointer = fopen($verbose,'w');
		curl_setopt($pointer, CURLOPT_VERBOSE, true);
		curl_setopt($pointer, CURLOPT_STDERR, $verbose_pointer);
	}
 
	if($ref_url !== false)
	{
	    curl_setopt($pointer, CURLOPT_REFERER, $ref_url);
	}
 
	if(count($post_data) > 0)
	{
	    curl_setopt($pointer, CURLOPT_POST, true);
	    curl_setopt($pointer, CURLOPT_POSTFIELDS, $post_data);
	}
 
	$return_val = curl_exec($pointer);
 
	$http_code = curl_getinfo($pointer, CURLINFO_HTTP_CODE);
 
	if($http_code == 404)
	{
		return false;
	}
 
	curl_close($pointer);
 
	unset($pointer);
 
	return $return_val;
    }
}