<?php
if(!defined('BASEPATH'))define('BASEPATH',dirname(__FILE__).'/../system/');
if(!defined('APPPATH'))define('APPPATH',dirname(__FILE__).'/../application/');
require_once dirname(__FILE__).'/database.php';
if(defined('_SKIP_NEWRELIC_')){
    include_once dirname(__FILE__).'/newrelic_report.php';
}
include dirname(__FILE__).'/../application/config/database.php';

date_default_timezone_set('UTC');

define('_ci_db_hostname',$db['default']['hostname'])  ; 
define('_ci_db_username',$db['default']['username'])  ; 
define('_ci_db_password',$db['default']['password'])  ; 
define('_ci_db_database',$db['default']['database'])  ;  
if(!class_exists('ci_db_connect')){
    class ci_db_connect extends database{
        public function __construct() {
            $this->connectdb(_ci_db_database,_ci_db_username,_ci_db_password,_ci_db_hostname);
        }
        public function getBackendProcessID($update=true){
            global $_bp_proc_id;
            if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
                $this->getBackendProcess($update);
            }else{
                return array();
            }
            return $_bp_proc_id;
        }
        public function getBackendProcess($update=true){
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                return array();
            }
            global $_bp_lasttime,$_bp_proc,$_bp_proc_id;
            if(empty($_bp_lasttime)){
                $_bp_lasttime = time();
            }else{
                if(time() - $_bp_lasttime <  5){
                    return $_bp_proc;
                }
                $_bp_lasttime = time();
            }
            if($update){
                $this->updateBackendProcess();
            }
            $tb = 'feedbiz_backend_process';
            $sql = "select host,pid from $tb";
            $q = $this->select_query($sql);
            $list = $this->fetch($q);
            $out = array();
            $out2 = array();
            $pid = array();
            if(is_array($list)){
                foreach($list as $l){
                    if(empty($l['pid'])||empty($l['host']))continue;
                    $out[$l['host']][$l['pid']]=true;
                    $out2[$l['pid']][$l['host']]=$l['host'];
                }
            }
            $_bp_proc = $out;
            $_bp_proc_id = $out2;
            return $_bp_proc;
        }
        public function updateBackendProcess(){
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                return array();
            }
            global $_bp_lastupdate ;
            if(empty($_bp_lastupdate)){
                $_bp_lastupdate = time();
            }else{
                if(time() - $_bp_lastupdate < 60){
                    return  ;
                }
                $_bp_lastupdate = time();
            }
            include(dirname(__FILE__) . '/../application/config/config.php');
            $cmd = " ps -eo pid,cmd  | grep '.php' | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
            $host = $config['host_code'];
            $tb = _ci_db_database.'.feedbiz_backend_process';
            ob_start();
            passthru($cmd );
            $outt = ob_get_contents();
            ob_end_clean();

            $list = explode("\n", $outt);
            foreach($list as $l){
                if(empty($l))continue;
                $l = trim($l);
                $pid = trim(substr($l,0,strpos($l,' ',1)));
                $cmd = trim(substr($l,strpos($l,' ',1)));
                $data[]=array('host'=>$host,'pid'=>$pid/*,'cmd'=> $this->escape_str($cmd)*/);
            }

            $sql = "delete from {$tb} where host = '$host';";
    //        $cmd = 'mysql -u '._ci_db_username.' -p'._ci_db_password.' -h '._ci_db_hostname.' -e "'.$sql.'"';
    //        exec($cmd);
            $this->select_query($sql);
            //exec('bash -c "exec nohup setsid '.$cmd.' > /dev/null 2>&1 &  "');
             $ox = array();
                $key_set='';
                if(!empty($data)){
                foreach($data as $d){
                    if(empty($key_set))
                    $key_set = implode(',',array_keys($d)).',date_add';
                    $d_set = "('".implode("','",$d)."',now())";
                    $ox[]=$d_set;
                }
                $sql = "replace into {$tb} ({$key_set}) values ".implode(',',$ox).';';
                $this->select_query($sql);

    //            $cmd = 'mysql -u '._ci_db_username.' -p'._ci_db_password.' -h '._ci_db_hostname.' -e "'.$sql.'"';
    //            exec($cmd);
                }
                $sql = "update feedbiz_backend set server_load = '".sizeof($ox)."' , processing = '',status=1, last_active = now()  where host_code = '{$host}'";
                $this->select_query($sql);
        }
    }
}