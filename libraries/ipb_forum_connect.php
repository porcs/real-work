<?php
    function connect_db(){ 
        include dirname(__FILE__).'/../application/config/database.php';
        
        //-----------------------------------------
	// Connect to database
	//-----------------------------------------
        if(!isset($db['skip_forum']) || (isset($db['skip_forum'])&& $db['skip_forum']=='skip'))return false;
        $link = @mysqli_connect($db['default']['hostname'], 'feed_biz_forum', 'f33d_6is_f0rum');
	//or die('Impossible de se connecter : ' . mysqli_error());
	
	/* check connection */
	if (!$link) {
//		printf("Impossible de se connecter : %s\n", $link->connect_error);
//		exit();
            return false;
	}

	mysqli_select_db($link, 'feed_biz_forum')or die('Impossible de sélectionner la base de données');
	//mysqli_select_db('daetas') or die('Impossible de sélectionner la base de données');
	mysqli_query($link, "SET NAMES UTF8");
	mysqli_query($link, "SET character_set_results=utf8");
        
        return $link;
    }

    function genarate_pass_salt($email){
        $ips_connect_key = '4da3240a0fb7a734d46f75f6c1307009';
        return md5($ips_connect_key . $email);
    }
    
    function register($username,$email,$password){
        $link = connect_db();
        if(!$link)return false;
        //genarate_pass_hash
        $genarate_pass_salt = genarate_pass_salt($email);
        $genarate_pass_hash = crypt( $password, '$2a$13$' . $genarate_pass_salt );
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
	
        //INSERT INTO core_members(member_group_id,name,email,joined,ip_address,members_pass_hash,members_pass_salt,skin)VALUES('4','don12','donotdont@gmail.com','1429521695','180.183.103.2','$2a$13$d76dc795c429f2768805duOS3lIsergz.J/6DGuHMcLXaW1M.2kny','d76dc795c429f2768805d8dc7fd31c21','2');
	$query = "INSERT INTO core_members ( `member_group_id`,`name`,`email`,`joined`,`ip_address`,`members_pass_hash`,`members_pass_salt`,`skin` ) VALUES ('3', '". mysqli_real_escape_string($link,strstr($email, '@', true))."', '". $email ."' , '". time() ."' , '". $ip ."' ,'". $genarate_pass_hash ."', '". $genarate_pass_salt ."', '2' )";
        mysqli_query($link,$query);
	
	mysqli_close($link);
    }
    
    function setUserCookie($name, $value) {
         //$date = date("D, d M Y H:i:s",strtotime('1 January 2015')) . 'GMT';
         $date = gmdate('D, d M Y H:i:s \G\M\T',time()+24*60*60*7);
         header("Set-Cookie: ".$name."=".$value."; EXPIRES=".$date."; path=/; domain=forum.feed.biz");
    }
    
    function unsetUserCookie($name) {
         //$date = date("D, d M Y H:i:s",strtotime('1 January 2015')) . 'GMT';
         $date = gmdate('D, d M Y H:i:s \G\M\T',time()-24*60*60*90);
         header("Set-Cookie: ".$name."=''; EXPIRES=".$date."; path=/; domain=forum.feed.biz", false);
    }
    
    function ipb_session_login($email,$password){
         $link = connect_db();
         if(!$link)return false;
         $query = "SELECT `member_id`,`member_login_key`,`members_pass_hash` FROM `core_members` WHERE `email` = '".$email."'";
         $result = mysqli_query($link,$query);
         $user = mysqli_fetch_array($result);
         
         //genarate_pass_hash
        $genarate_pass_salt = genarate_pass_salt($email);
        $genarate_pass_hash = crypt( $password, '$2a$13$' . $genarate_pass_salt );
        //print_r($user);
        //echo $user['members_pass_hash'].' : '.$genarate_pass_hash." -+ ".$user['member_login_key'];
        if($user['members_pass_hash'] == $genarate_pass_hash){
            $domain = '.feed.biz';
            setcookie( 'ips4_member_id', $user['member_id'], time()+60*60*24*7, '/', $domain);
            setcookie( 'ips4_pass_hash', $user['member_login_key'], time()+60*60*24*7, '/', $domain);
            //setcookie( 'ipsconnect_' . md5( $this->url_to_this_file ), '1', time()+60*60*24*30, '/' );*/
            
            /*setUserCookie('ips4_member_id',$user['member_id']);
            setUserCookie('ips4_pass_hash',$user['member_login_key']);*/
            
            //echo 'ips setcookie';
        }
        mysqli_close($link);
    }
    
    function ipb_session_login_activate($email){
        $link = connect_db();
        if(!$link)return false;
        $query = "SELECT `member_id`,`member_login_key`,`members_pass_hash`,`members_pass_salt` FROM `core_members` WHERE `email` = '".$email."'";
        $result = mysqli_query($link,$query);
        $user = mysqli_fetch_array($result);
         
        $ips_connect_key = '4da3240a0fb7a734d46f75f6c1307009';
        $member_login_key = md5(md5($user['members_pass_salt']).md5($ips_connect_key));
         
        $query = "UPDATE `core_members` SET `member_login_key`='". $member_login_key ."' WHERE `email` = '".$email."'";
        mysqli_query($link,$query);
         
        $domain = '.feed.biz';
        setcookie( 'ips4_member_id', $user['member_id'], time()+60*60*24*7, '/', $domain);
        setcookie( 'ips4_pass_hash', $member_login_key, time()+60*60*24*7, '/', $domain);
          
        mysqli_close($link);
    }
    
    function ipb_clear_session($id){
            $link = connect_db();
            if(!$link)return false;
            //Chack Old = New Pass
            $query = "UPDATE `core_sessions` SET `data`='". NULL ."' WHERE `id` = '".$id."'";
            mysqli_query($link,$query);
            mysqli_close($link);
            $domain = '.feed.biz';
            $domain2 = 'forum.feed.biz';
            setcookie( 'ips4_member_id','', time()-60*60*24*7, '/', $domain);
            setcookie( 'ips4_pass_hash','', time()-60*60*24*7, '/', $domain);
            setcookie( 'ips4_member_id','', time()-60*60*24*7, '/', $domain2);
            setcookie( 'ips4_pass_hash','', time()-60*60*24*7, '/', $domain2);
            unset($_COOKIE['ips4_member_id']);
            unset($_COOKIE['ips4_pass_hash']);
            clearstatcache();
            
            /*unsetUserCookie('ips4_member_id');
            unsetUserCookie('ips4_pass_hash');*/
    }
    
    function ipb_update_image($email,$urlpicture){
        $link = connect_db();
        if(!$link)return false;
         $query = "UPDATE `core_members` SET `pp_main_photo`='". $urlpicture ."',`pp_thumb_photo`='". $urlpicture ."',`pp_photo_type`='custom' WHERE `email` = '".$email."'";
         mysqli_query($link,$query);
         mysqli_close($link);
    }
    
    function ipb_update_name($email,$name){
        $link = connect_db();
        if(!$link)return false; 
         $query = "UPDATE `core_members` SET `name`='". $name ."' WHERE `email` = '".$email."'";
         mysqli_query($link,$query);
         mysqli_close($link);
    }
    
    function changepassword($email,$password,$newpassword){
        $link = connect_db();
        if(!$link)return false;
        //Chack Old = New Pass
        $query = "SELECT `member_id`,`member_login_key`,`members_pass_hash`,`members_pass_salt` FROM `core_members` WHERE `email` = '".$email."'";
        $result = mysqli_query($link,$query);
        $user = mysqli_fetch_array($result);
        
        //genarate_pass_hash
        $genarate_pass_salt = genarate_pass_salt($email);
        $genarate_pass_hash = crypt( $password, '$2a$13$' . $genarate_pass_salt );
        //print_r($user);
        //echo $user['members_pass_hash'].' : '.$genarate_pass_hash." -+ ".$user['member_login_key'];
        if($user['members_pass_hash'] == $genarate_pass_hash){
         
        //NEW genarate_pass_hash
        $genarate_pass_salt = genarate_pass_salt($email);
        $genarate_pass_hash = crypt( $newpassword, '$2a$13$' . $genarate_pass_salt );
         
        $query = "UPDATE `core_members` SET `members_pass_hash`='". $genarate_pass_hash ."',`members_pass_salt`='". $genarate_pass_salt ."' WHERE `email` = '".$email."'";
        mysqli_query($link,$query);
        
        //Login Again
        ipb_session_login($email,$newpassword);
        }
         
        mysqli_close($link);
    }
    
    function forgetpassword($email,$newpassword){
         $link = connect_db();
         if(!$link)return false;
        //NEW genarate_pass_hash
        $genarate_pass_salt = genarate_pass_salt($email);
        $genarate_pass_hash = crypt( $newpassword, '$2a$13$' . $genarate_pass_salt );
         
        $query = "UPDATE `core_members` SET `members_pass_hash`='". $genarate_pass_hash ."',`members_pass_salt`='". $genarate_pass_salt ."' WHERE `email` = '".$email."'";
        mysqli_query($link,$query);
        
        //Login Again
        ipb_session_login($email,$newpassword);
         
        mysqli_close($link);
    }
    
    function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '4da3240a0fb7a734d46f75f6c1307009';
        $secret_iv = '$Forum.Feed.Biz\+*-/>/$Client.Feed.biz';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
    function get_forum_session($session_id){
        $link = connect_db();
         if(!$link)return array();
          
        $query = "SELECT `data` FROM `core_sessions` WHERE `id` = '". $session_id ."' limit 1";
        
        $q = mysqli_query($link,$query);
        $session = $out = array();
        while($session = mysqli_fetch_array($q)){
//            echo $session['data'];
            $array = explode(';',$session['data']);
            foreach($array as $a){
                $v=$a;
                $vx = explode('|',$v);
                if(!isset($vx[1]))continue;
                $temp = $vx[1];
                $t = explode(':',$temp);
                if(!isset($t[2]))continue;
                $out[$vx[0]]  = str_replace('"','',$t[2]);
            }
            
        } 
        //Login Again
         
        mysqli_close($link);
        return $out;
    } 
   /*
   function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '4da3240a0fb7a734d46f75f6c1307009';
        $secret_iv = '$Forum.Feed.Biz\=+*-/>/$Client.Feed.biz';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    $timeout = (time()+60*3);
    $plain_txt = $timeout."501309";
    echo "Time Out in 3 minus = $timeout\n";
    echo "Plain Text = $plain_txt\n";

    $encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);
    echo "Encrypted Text = $encrypted_txt\n";

    $decrypted_txt = encrypt_decrypt('decrypt', $encrypted_txt);
    echo "Decrypted Text = $decrypted_txt\n";

    //Cut time out
    $timecookie = substr($decrypted_txt,0,10);
    echo "Timecookie = $timecookie\n";

    $timeout = round(abs(time() - 1429778717));//1429778319
    if($timeout <= 60*3){
     echo "Can use time ".$timeout."(s)\n";
    }else{
     echo "Time out\n";
    }

    $password = substr($decrypted_txt,10);
    echo "Password = $password\n";

    if( $plain_txt === $decrypted_txt ) echo "SUCCESS";
    else echo "FAILED";

    echo "\n";
    */
    
   function ipb_session_write_db($id,$data){
       $link = connect_db();
        if(!$link)return false;
        
        $query = "SELECT `data` FROM `core_sessions` WHERE `id` = '".$id."'";
        $result = mysqli_query($link,$query);
        $sess = mysqli_num_rows($result);
        
        if($sess > 0){        
            $query = "UPDATE `core_sessions` SET `data`='". $data ."' WHERE `id` = '".$id."'";
            mysqli_query($link,$query);
        }else{
            $query = "INSERT INTO `core_sessions`(`id`,`data`)VALUES('". $id ."','". $data ."')";
            mysqli_query($link,$query);
        }
        
        mysqli_close($link);
   }
   
    function marge_member(){
        
    }
    
    function ips_login_feedbiz(){
        
    }