<?php

class Hooks {	

	/**
	 * Determines wether hooks are enabled
	 *
	 * @var bool
	 */
	var $enabled		= FALSE;
	/**
	 * List of all hooks set in config/hooks.php
	 *
	 * @var array
	 */
	var $hooks		= array(
				    'send_orders_cron' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'FBA',
							'function'		=> 'updateFbaOrder',
							'params'		=> array('user_name', 'id_shop', 'site', 'orders'), 
							'filepath'		=> 'libraries/Amazon/hook/FBA.php'   
						),					    
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'OrderAcknowledge',
							'function'		=> 'updateAcknowledgeOrder',
							'params'		=> array('user_name', 'id_shop', 'site', 'orders'), 
							'filepath'		=> "libraries/Amazon/hook/OrderAcknowledge.php"   
						),
				    ),
				    'save_orders' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'FBA',
							'function'		=> 'createFbaOrder', 
							'params'		=> array('user_name', 'id_shop', 'site', 'id_order', 'force_send'=>false), 
							'filepath'		=> "libraries/Amazon/hook/FBA.php"   
						),					  				    
				    ),
                                    'cancel_orders' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'OrderAcknowledge',
							'function'		=> 'cancelOrder',   
							'params'		=> array('user_name', 'id_order', 'id_marketplace', 'id_shop', 'site'),
							'filepath'		=> "libraries/Amazon/hook/OrderAcknowledge.php"
						),
				    ),
				    'cancel_fba_order' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'FBA',
							'function'		=> 'cancelFbaOrder', 
							'params'		=> array('user_name', 'id_shop', 'id_order'), 
							'filepath'		=> "libraries/Amazon/hook/FBA.php"   
						),					  				    
				    ),
				    'get_info_fba_order' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'FBA',
							'function'		=> 'getFbaOrderInfo', 
							'params'		=> array('user_name', 'id_shop', 'id_order'), 
							'filepath'		=> "libraries/Amazon/hook/FBA.php"   
						),					  				    
				    ),
                                    'import_products' => array(
						array(
							'marketplace_id'	=> 2,
							'marketplace_name'	=> 'amazon',
							'class'			=> 'ProductManagement',
							'function'		=> 'removeDeltedProducts',
							'params'		=> array('user_name', 'id_shop', 'debug'),
							'filepath'		=> "libraries/Amazon/hook/ProductManagement.php"
						),
				    )
	    );
	/**
	 * Determines wether hook is in progress, used to prevent infinte loops
	 *
	 * @var bool
	 */
	var $in_progress	= FALSE;

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{	    
		$this->_initialize();
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize the Hooks Preferences
	 *
	 * @access	private
	 * @return	void
	 */
	function _initialize()
	{		
		$this->enabled = TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Call Hook
	 *
	 * Calls a particular hook
	 *
	 * @access	private
	 * @param	string	the hook name
	 * @return	mixed
	 */
	function _call_hook($which = '', $params = array())
	{	    
		if ( ! $this->enabled || ! isset($this->hooks[$which]))
		{
			return FALSE;
		}

		if (isset($this->hooks[$which]) && is_array($this->hooks[$which]))
		{
			foreach ($this->hooks[$which] as $k => $val)
			{					
				$this->_run_hook($val, $params);
			}
		}
		else
		{
			$this->_run_hook($this->hooks[$which], $params);
		}

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Run Hook
	 *
	 * Runs a particular hook
	 *
	 * @access	private
	 * @param	array	the hook details
	 * @return	bool
	 */
	function _run_hook($data, $params = array())
	{
		if ( ! is_array($data))
		{
			return FALSE;
		}

		// -----------------------------------
		// Safety - Prevents run-away loops
		// -----------------------------------

		// If the script being called happens to have the same
		// hook call within it a loop can happen

		if ($this->in_progress == TRUE)
		{
		    
			return;
		}

		// -----------------------------------
		// Set file path
		// -----------------------------------

		if ( ! isset($data['filepath']) )
		{
			return FALSE;
		}

		$filepath = APPPATH.$data['filepath'];

		if ( ! file_exists($filepath))
		{
			return FALSE;
		}

		// -----------------------------------
		// Set class/function name
		// -----------------------------------

		$class		= FALSE;
		$function	= FALSE;

		if (isset($data['class']) && $data['class'] != '')
		{
			$class = $data['class'];
		}

		if (isset($data['function']))
		{
			$function = $data['function'];
		}

		if ($class === FALSE && $function === FALSE)
		{
			return FALSE;
		}

		// -----------------------------------
		// Set the in_progress flag
		// -----------------------------------

		$this->in_progress = TRUE;

		// -----------------------------------
		// Call the requested class and/or function
		// -----------------------------------

		if ($class !== FALSE)
		{
			if ( ! class_exists($class))
			{
				require($filepath);
			}    
			
			
			$refClass = new ReflectionClass($class);
			$class_instance = $refClass->newInstance();

			call_user_func_array(array($class_instance, $function), $params);
		}
		else
		{
			if ( ! function_exists($function))
			{
				require($filepath);
			}
			
			call_user_func_array(array($function), $params);
		}

		$this->in_progress = FALSE;
		return TRUE;
	}

}
