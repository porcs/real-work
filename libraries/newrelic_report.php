<?php
include_once(dirname(__FILE__).'/include_lib_so.php');
include_once(dirname(__FILE__).'/feedbiz_override_tools.php');
define('NEWRELIC_DEBUG_ENABLE',true);
error_reporting( E_ALL|E_STRICT );
if (extension_loaded('newrelic')) {
  if(!(isset($argv) && !empty($argv) && is_array($argv))){
      $argv = array();
  } 
  newrelic_add_custom_parameter ('Running Source',PHP_SAPI);
  newrelic_add_custom_parameter ('PHP ARGV',implode(' ',$argv));
}
if(!function_exists('newrelic_add_debug')){
function newrelic_add_debug ($key='',$val=''){
    if(!NEWRELIC_DEBUG_ENABLE) return;
    if($key=='' || $val ==''){return;}
    if (extension_loaded('newrelic')) {
        newrelic_add_custom_parameter ($key,$val);
    }
    
}
}
if(!function_exists('monitor_offer')){
    function monitor_offer($user_name,$uid){
        if(!in_array($uid, array('26'))){
            return;
        }
        require_once (dirname(__FILE__).'/../application/libraries/db.php');
        $db = new Db($user_name,'',true,false);
        if(!$db->exist_db){
            return;
        }
        $sql = "select * from (SELECT
products_product.id_product,0 as id_product_attribute,
products_product.reference,
products_product.quantity,
products_product.price,
products_product.date_add,
offers_product.quantity AS offer_qty,
offers_product.price AS offer_price,
offers_product.date_add AS offer_add
FROM
products_product
left JOIN offers_product ON products_product.id_product = offers_product.id_product AND products_product.id_shop = offers_product.id_shop
UNION
SELECT
products_product_attribute.id_product,
products_product_attribute.id_product_attribute,
products_product_attribute.reference,
products_product_attribute.quantity,
products_product_attribute.price,
products_product_attribute.date_add,
offers_product_attribute.quantity AS offer_qty,
offers_product_attribute.price AS offer_price,
offers_product_attribute.date_add AS offer_add
FROM
products_product_attribute
left JOIN offers_product_attribute ON products_product_attribute.id_product_attribute = offers_product_attribute.id_product_attribute AND products_product_attribute.id_product = offers_product_attribute.id_product AND products_product_attribute.id_shop = offers_product_attribute.id_shop

) as x order by x.id_product";
        $out = $db->db_query_string_fetch($sql);
        $dir =  dirname(__FILE__).'/../assets/apps/users/'.$user_name.'/debugs/';
        if(!file_exists($dir)){
            mkdir($dir,0777,true);
        }
        $file_name = 'current_stock_'.date('Y_m_d_H_i_s').'.csv';
        $fp = fopen($dir.$file_name, 'w');
        foreach($out as $k=>$o){
            if($k==0){
                $array_key = array_keys($o);
                fputcsv($fp, $array_key);
            }
            fputcsv($fp, $o);
        }
        fclose($fp);
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
            exec('gzip '.$dir.$file_name);
            $file_name = 'offer_update_flag'.date('Y_m_d_H_i_s').'.sql.gz';
            exec("mysqldump -h rds-qa.feed.biz -u master -pMfb90-25rds_1520 fb_".$user_name." offers_flag_update | gzip > ".$dir.$file_name."");
        }
        
    }

}
if (php_sapi_name() == "cli") {

  $levels = array(
            E_ERROR				=>	'Error',
            E_WARNING			=>	'Warning',
            E_PARSE				=>	'Parsing Error',
            E_NOTICE			=>	'Notice',
            E_CORE_ERROR		=>	'Core Error',
            E_CORE_WARNING		=>	'Core Warning',
            E_COMPILE_ERROR		=>	'Compile Error',
            E_COMPILE_WARNING	=>	'Compile Warning',
            E_USER_ERROR		=>	'User Error',
            E_USER_WARNING		=>	'User Warning',
            E_USER_NOTICE		=>	'User Notice',
            E_STRICT			=>	'Runtime Notice'
    );
if(!function_exists('cli_log_exception')){
function cli_log_exception($severity, $message, $filepath, $line)
	{
    global $config,$argv,$levels,$last_query;
    if(is_object($config) || !isset($config['base_url'])){
        $host = 'client.feed.biz';
    }else{
        $host = str_replace(array("https://","http://",'/'),'',$config['base_url']);
    }
    $call_file = isset($argv[0])?"(".$argv[0].")":'';
    $call = implode(' ',$argv);
    $add_query = !empty($last_query)?$last_query:'';
 		$severity = ( ! isset($levels[$severity])) ? $severity : $levels[$severity];
                
                
                $key = md5($severity.'  --> '.$message. ' '.$filepath.' '.$line ."\n");
                if(!isset($levels['key'])){
                    $levels['key'] = array();
                }
                if(isset($levels['key'][$key])){
                    return;
                }
                $levels['key'][$key] = $key;
//                $his_dir = dirname(__FILE__).'/mail_history';
//                if(!file_exists($his_dir)){
//                    mkdir($his_dir,777,true);
//                }
//                $his_file_path = $his_dir.'/'.$key;
//                if(file_exists($his_file_path)){ 
//                   return true; 
//                }
//                file_put_contents($his_file_path,''); 
                $mail_content ='Error on '.$host.' @ PHP Cli '.$call_file;
                $subject = $mail_content ;
                $mail_content .="\n".'Server Time : '.date('c')."\n";
                $mail_content .=  "\n".$severity.'  --> '.$message. ' '.$filepath.' '.$line ."\n";
                $mail_content .= 'Command : '.$call."\n";
//                $mail_content .= print_r($levels,true);
                if(!empty($add_query)){
                    $mail_content .= 'Last query : '.substr($add_query,400)."\n";
                }
             if(strpos($message,'get_headers(')!==false){
                 //skip
             }
                $send_to=array();
                $send_to[]='palm@common-services.com';
//                if(strpos($call,'ebay') !==false){
                    $send_to[]='por@common-services.com';
//                }
//                if(strpos($call,'amazon') !==false){
//                    $send_to[]='praew@common-services.com';
//                }  
                $e = new Exception; 
                $trace = $e->getTraceAsString();
                $mail_content .= "\n\n".$trace."\n\n";
//                log_message('error',$trace) ;
                require_once dirname(__FILE__) . '/../application/libraries/Swift/swift_required.php';
                try {
                    $transport = Swift_SmtpTransport::newInstance();
                    $mailer = Swift_Mailer::newInstance($transport);
                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject($subject) 
                            ->setFrom(array('support@feed.biz' => ''.$host.''))
                            ->setTo($send_to )
                            ->setBody('<pre>'.$mail_content.'</pre>', 'text/html');
                    $out = $mailer->send($swift_message) ;        
                } catch (Exception $ex) {
                    
                }
                return true;
                 
	}
    set_error_handler('cli_log_exception');
    }
}