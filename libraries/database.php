<?php
$gbl_ci_db_conn = null;
$gbl_ci_db_r = null;
class database {

    var $host = '192.168.1.3';
    var $database;
    var $connect_db;
    var $selectdb;
    var $db;
    var $sql;
    var $table;
    var $where;
    public $prefix_table = '';
    public $query = array(
        'select'    =>  array(),
        'from'      => 	'',
        'join'      => 	array(),
        'where'     => 	array(),
        'group'     =>  array(),
        'having'    =>  array(),
        'order'     => 	array(),
        'limit'     => 	array('offset' => 0, 'limits' => 0),
    );
    
    public function connectdb($db_name = "database", $user = "username", $pwd = "password", $host = '192.168.1.3') {
        global $gbl_ci_db_conn,$gbl_ci_db_r;
        
        include(dirname(__FILE__) . '/../application/config/database.php');  
        $dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
        if($db_name=="database"){
            $db_name=$db['default']['database'] ;
        }
        if($user=="username"){
            $user=$db['default']['username'] ;
        }
        if($pwd=="password"){
            $pwd=$db['default']['password'] ;
        }
        if($host == '192.168.1.3'){
            $host=$db['default']['hostname'] ;
        }
        
        
        $this->database = $db_name;
        $this->username = $user;
        $this->password = $pwd;
        if(!isset($gbl_ci_db_conn)){
        $this->connect_db = mysqli_connect($host, $this->username, $this->password) or $this->_error();
        $this->db = $this->connect_db;
        
        $this->dbr = mysqli_select_db($this->connect_db, $this->database) or $this->_error();
        mysqli_query($this->connect_db, "SET NAMES UTF8");
        mysqli_query($this->connect_db, "SET character_set_results=utf8");
            $gbl_ci_db_conn = $this->connect_db;
            $gbl_ci_db_r = $this->dbr;
        }else{
            $this->db = $this->connect_db = $gbl_ci_db_conn;
            $this->dbr = $gbl_ci_db_r;
        }
        
        if ($this->dbr): return true;
        else : return false;
        endif;
    }

    public function select_query($sql = "sql") {
        if ($result = mysqli_query($this->connect_db, $sql)) : return $result;
        else : $this->_error();
            return false;
        endif;
    }

    public function fetch($sql = "sql") {
        if(is_bool($sql)) :
            $this->_error();
            return false;
        elseif ($result = mysqli_fetch_array($sql)) : return $result;
        else : $this->_error();
            return false;
        endif;
    }

    public function add_db($table = "table", $data = "data", $rSql = false) {
        foreach ($data as $key => $value) :
            if (empty($add)) : $add = "(";
            else : $add = $add . ",";
            endif;

            if (empty($val)) : $val = "(";
            else : $val = $val . ",";
            endif;

            $add = $add . $key;
            $val = $val . "'" . $value . "'";
        endforeach;
        $add = $add . ")";
        $val = $val . ")";
        $sql = "INSERT INTO " . $table . " " . $add . " VALUES " . $val;

        if ($rSql)
            return $sql;
        if (mysqli_query($this->connect_db, $sql)) : return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function replace_db($table = "table", $data = "data", $rSql = false) {
        foreach ($data as $key => $value) :
            if (empty($add)) : $add = "(";
            else : $add = $add . ",";
            endif;

            if (empty($val)) : $val = "(";
            else : $val = $val . ",";
            endif;

            $add = $add . $key;
            $val = $val . "'" . $value . "'";
        endforeach;
        $add = $add . ")";
        $val = $val . ")";
        $sql = "REPLACE INTO " . $table . " " . $add . " VALUES " . $val;

        if ($rSql)
            return $sql;
        if (mysqli_query($this->connect_db, $sql)) : return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function insert_id() {
        if ($result = mysqli_insert_id($this->connect_db)) : return $result;
        else : $this->_error();
            return false;
        endif;
    }

    public function count_rows($sql = "sql") {
        if ($result = mysqli_num_rows($sql)) : return $result;
        else : $this->_error();
            return false;
        endif;
    }

    public function num_rows($table = "table", $field = "field", $where = "where") {
        if ($where == "") : $where = "";
        else : $where = " WHERE " . $where;
        endif;

        $sql = "SELECT " . $field . " FROM " . $table . $where;
        if (mysqli_query($this->connect_db, $sql)) : return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function update($table = "table", $data = "set", $where = "where") {
        $set = "";
        $where_query=array();
        foreach ($data as $key => $value) :
            if (!empty($set)) : $set = $set . ",";
            endif;
            $set = $set . $key . "='" . $value . "'";
        endforeach;
        
        if ( !empty($where) && is_array($where) ) {
            foreach ($where as $key => $value) {
                if($value === '' || is_null($value)){
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                }
                $where_query[] =  $key . " = '" . $value."' ";
            }
            if(empty($where_query)){$where_query=array(1);}
            $sql = "UPDATE " . $table . " SET " . $set . " WHERE " . implode(' and ',$where_query);
        }
        else {
            $sql = "UPDATE " . $table . " SET " . $set . " WHERE " . $where;
        } 
        if (mysqli_query($this->connect_db, $sql)) :
            return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function update_db($table = "table", $data = "data", $where = "where") {
        $set = "";
        foreach ($data as $key => $value) :
            if (!empty($set)) : $set = $set . ",";
            endif;
            $set = $set . $key . "='" . $value . "'";
        endforeach;

        $sql = "UPDATE " . $table . " SET " . $set . " WHERE " . $where;
        if (mysqli_query($this->connect_db, $sql)) : return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function del($table = "table", $where = "where") {
        $sql = "DELETE FROM " . $table . " WHERE " . $where;
        if (mysqli_query($this->connect_db, $sql)) : return true;
        else : $this->_error();
            return false;
        endif;
    }

    public function _error() {
        if($this->connect_db)
        $this->error[] = mysqli_error($this->connect_db);
    }

    public function get_configuration($id_user = null) {
        if (!isset($id_user)) :
            return FALSE;
        endif;
        $sql = "SELECT * FROM configuration Where id_customer = '" . $id_user . "'";
        return $this->select_query($sql);
    }

    public function show_table($table = null) {
        if (!isset($table)) :
            return FALSE;
        endif;
        $sql = "SHOW TABLES LIKE '" . $table . "'";
        return $this->select_query($sql);
    }

    public function truncate_table($table = null) {
        if (!isset($table)) :
            return FALSE;
        endif;
        $sql = "TRUNCATE TABLE " . $table;
        return $this->select_query($sql);
    }
    
    public function escape_str($str){
        return mysqli_escape_string($this->connect_db, $str);
    }

############################################################################    
    
    public function select($fields)
    {
        if (!empty($fields))
        {
            foreach ($fields as $value) 
                $select[] =  $value;

            $query_string = implode(', ', $select) ;

            $this->query['select'][] = $query_string;
        }
        
        return $this;
    }
        
    public function from ($table, $alias = null )
    {
         
        if (!empty($table))
            $this->query['from'] = $table . ($alias ? ' '.$alias : '');
        return $this;
    }
    
    public function where($restriction, $operation = "=", $where_operation = 'AND')
    {
        $where = array();
        foreach ($restriction as $key => $value) 
        {
            if($operation == 'IN'){
                $where[] =  $key . " $operation " . $value;
            } else {
                
                if($value === '' || is_null($value)){
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
		    
		    if($operation != "=")
			$value = "{$value}";
		    else
			$value = "'{$value}'";
                }
                $where[] =  $key . " $operation " . $value;
            }
        }
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = " $where_operation " . implode(" $where_operation ", $where) ;
        else
            $query_string = ' WHERE ' . implode(" $where_operation ", $where) ;
        //$query_string = ' WHERE ' . implode(' AND ', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_not_equal($restriction)
    {
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'{$value}'";
                
            $where[] =  $key . ' != ' . $value;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_like($restriction, $operation = null)
    {
        $where = array();
        
        if(isset($operation)) {
            $operation = ' OR ';            
        } else {
            $operation = ' AND ';
        }
            
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'%{$value}%'";

            $where[] =  $key . ' like ' . $value  ;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode($operation, $where) ;
        else
            $query_string = ' WHERE ' . implode($operation, $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_select($restriction, $operation = 'IN')
    {
        foreach ($restriction as $key => $value) 
            $where[] =  $key . ' ' . $operation . ' ' . $value;
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;
        
        return $this;
    }
    
    public function join($join)
    {
        if (!empty($join))
            $this->query['join'][] = $join;

        return $this;
    }
    
    public function leftJoin($table, $alias = null, $on = null )	    
    {
        return $this->join('LEFT JOIN '  .$table . ' ' . ($alias ? $alias : '') . ($on ? ' ON '. $on : ''));
    }
    
    public function innerJoin($table, $alias = null, $on = null, $no_prefix = false)
    {
        return $this->join('INNER JOIN '  .$table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function outerJoin($table, $alias = null, $on = null, $no_prefix = false)
    {
        return $this->join('OUTER JOIN '  .$table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function orderBy($fields)
    {
        if (!empty($fields))
            $this->query['order'][] = $fields;

        return $this;
    }
    
    public function groupBy($fields)
    {
        if (!empty($fields))
            $this->query['group'][] = $fields;

        return $this;
    }
    
    public function limit($limit, $offset = 0)
    {
        $offset = (int)$offset;
        if ($offset < 0)
            $offset = 0;

        $this->query['limit'] = array(
            'offset' => $offset,
            'limits' => (int)$limit,
        );
        return $this;
    }
    
    public function get($table, $where=array(), $fields=array(), $orderBy=null)
    {
            $this->from($table);

            if (!empty($fields))
                    $this->select($fields);		

            if (!empty($where)) 
                    $this->where($where);

            if (isset($orderBy) && !empty($orderBy))
                    $this->orderBy($orderBy);        

            return $this->db_array_query($this->query);        
    }
        
    public function db_array_query($query,$debug=false) 
    { 
        if(!isset($query) || empty($query) || !$query || empty($this->db) || !$this->db)
            return array();
            
        $sql = is_array($query) ? $this->query_string($query) : $query; 
        if($debug){
            echo $sql.'<br>';
            echo print_r($query,true);  
        } 
        $exec = array();
         
        $q = mysqli_query($this->db, $sql);
        if($q)
        while($o = mysqli_fetch_assoc($q)){ 
            $exec[] = $o;
        }
         
        unset($this->query);
         
        return $exec ? $exec : array() ;
    } 
    
    public function query_string($arr_query) 
    { 
        $query_string = '';
        
        if(empty($arr_query['select']))
        {
            $query_string .= 'SELECT * FROM ';
        }
        else
        {
            foreach ($arr_query['select'] as $select)
            {
                $query_string .= 'SELECT ' . $select . ' FROM ';
            }
        }
        
        if(!isset($arr_query['from']) || empty($arr_query['from']))
            return false;
        
        $query_string .= $arr_query['from'];
        
        if(isset($arr_query['join']) && !empty($arr_query['join']))
            foreach ($arr_query['join'] as $join)
            {
                $query_string .= ' ' . $join;
            }
        
        if(isset($arr_query['where']) && !empty($arr_query['where']))
            foreach ($arr_query['where'] as $where)
            {
                $query_string .= $where;
            }
        
        if(isset($arr_query['group']) && !empty($arr_query['group']))
        {
            $groups = array();
            foreach ($arr_query['group'] as $group)
                $groups[] =  $group;
            
            $query_string .= ' GROUP BY ' . implode(', ', $groups) ;
        }
        
        if(isset($arr_query['order']) && !empty($arr_query['order']))
        {
            $orders = array();
            foreach ($arr_query['order'] as $order)
                $orders[] =  $order;
            
            $query_string .= ' ORDER BY ' . implode(', ', $orders) ;
        }
        
        if(isset($arr_query['limit']) && $arr_query['limit']['limits'] != 0)
            $query_string .= ' LIMIT ' . $arr_query['limit']['limits'];
        
        if(isset($arr_query['limit']) && isset($arr_query['limit']['offset']) && $arr_query['limit']['offset'] != 0)
            $query_string .= ' OFFSET ' . $arr_query['limit']['offset'] ;
        
        $query_string .= '; ';
        //echo '<pre>' . print_r($query_string, true) . '</pre>'; 
        
        return $query_string;
    } 
    
    function affected_rows()
	{
		return @mysqli_affected_rows($this->db);
	}
}
