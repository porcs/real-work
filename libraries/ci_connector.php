<?php

require_once dirname(__FILE__).'/ci_db_connect.php';

class ci_connector extends ci_db_connect 
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    public function db_query_string_fetch($sql, $result = array()) 
    { 
        if( !empty($sql)) {
            $q = mysqli_query($this->connect_db, $sql);

            if ( !empty($q)) {
                while($out = mysqli_fetch_assoc($q)) {
                    $result[] = $out;
                }
            }
        }
        
        return $result;
    } 
}