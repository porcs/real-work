<?php
ini_set('display_errors',1);
error_reporting(E_ALL|E_STRICT);

 

class feedbiz_server_backend{
    
    function __construct($db_con = null,$register = false) { 
        if(empty($db_con)){
            $system_path = 'system';
            if(!defined('BASEPATH'))
            define('BASEPATH', str_replace("\\", "/", $system_path)); 
            include(dirname(__FILE__) . '/../application/config/database.php'); 
             $this->db_con = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
            mysqli_select_db($this->db_con,$db['default']['database'] ) or die('Could not select database.'); 
        }else{
        $this->db_con=$db_con;
        }
        $this->get_current_host_ip();
        $this->clear_history_load();
        if($register){
            $this->register_server();
        }else{
            $this->get_id_server();
        }
    }
    public function get_current_host_ip(){
        if(isset($_SERVER['SERVER_ADDR']) && !empty($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '127.0.0.1'){
            $localIP = $_SERVER['SERVER_ADDR'];
        }elseif (version_compare(phpversion(), '5.3.0', '<')) {
            $localIP = getHostByName(php_uname('n'));
        }else{
            $localIP = getHostByName(getHostName());
        }
        $this->ip = $localIP;
        return $localIP;
    }
    public function getIdByTargetIp($target_ip){
        $sql="select  id from feedbiz_backend where   ip_target = '$target_ip' ";
        $q = mysqli_query($this->db_con, $sql);
        $d = mysqli_fetch_assoc($q);
        return isset($d['id'])?$d['id']:false;
    }
    public function get_id_server(){
         $sql="select  id from feedbiz_backend where   ip = '{$this->ip}' ";
        $q = mysqli_query($this->db_con, $sql);
        $d = mysqli_fetch_assoc($q);
        if(isset($d['id'])){
            $this->id = $d['id'];
            return $this->id;
        }
        return false;
    }
    
    
    public function register_server(){
         
        $sql="select  id,ip from feedbiz_backend where status = 1";
        $q = mysqli_query($this->db_con, $sql);
        $d = mysqli_num_rows($q);
        $cron_active_f = "";
        $cron_active = "";
        if($d==0){
            $cron_active_f = ",cron_active";
            $cron_active = ",'1'";
        }
        include(dirname(__FILE__) . '/../application/config/database.php');
        $host_code = isset($config['host_code'])?$config['host_code']:'';
        $sql = "INSERT INTO feedbiz_backend (ip,ip_target,last_active,host_code $cron_active_f) "
                . " values('{$this->ip}','{$this->ip}',now(),'{$host_code}' $cron_active) "
                . " ON DUPLICATE KEY UPDATE  last_active=VALUES(last_active),status=1 ;";
        
//        $sql = "replace into feedbiz_backend (ip,last_active $cron_active_f) values('{$this->ip}',now() $cron_active)"; 
        mysqli_query($this->db_con, $sql);
        $this->id = mysqli_insert_id($this->db_con);
    }
    public function clear_load(){
        $sql = "update feedbiz_backend set server_load = 0 where id = '{$this->id}'";
        mysqli_query($this->db_con, $sql);
        return $sql;
    }
    public function get_load(){
        $sql="select  server_load from feedbiz_backend where   id = '{$this->id}' ";
        $q = mysqli_query($this->db_con, $sql);
        $d = mysqli_fetch_assoc($q);
        return isset($d['server_load'])?$d['server_load']:0;
    }
    public function get_php_process($line = true){
//        $cmd = " ps aux | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' ";
//        $out = exec($cmd);
//        if(!$line){
//        return array('proc'=>$out,'no'=>(int)substr_count( $out, "\n" ));
//        }else{
//            return (int)substr_count( $out, "\n" );
//        }
//        
        $cmd = " ps aux | grep php | grep -v 'php-fpm'| grep -v 'php sig_update_status.php' | grep -v '/bin/sh'  | grep -v 'sh -c'|grep -v 'grep' "; 
        ob_start();
        passthru($cmd ); 
        $out = ob_get_contents();
        ob_end_clean();  
        $add = 0 ;
        if(trim($out) != ''){
            $add=$score;
        } 
        $no = $add+(int)substr_count( $out, "\n" );
    //    $str = strtok($out, "\n");
        $str = $out;
        if(!$line){
            return array('proc'=>$out,'no'=>(int)substr_count( $out, "\n" ));
        }else{
            return (int)substr_count( $out, "\n" );
        } 
    }
    public function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }
    public function update_server_status(){
        $status = $this->get_php_process(false); 
        $sql = "update feedbiz_backend set server_load = '{$status['no']}' , processing = '{$status['proc']}',status=1, last_active = now()  where id = '{$this->id}'";
        mysqli_query($this->db_con, $sql);
        return $sql;
    }
    public function update_server_load($no=0,$current=''){
         
        $sql = "update feedbiz_backend set server_load = '{$no}' , processing = '{$current}',status=1, last_active = now()  where id = '{$this->id}'";
        mysqli_query($this->db_con, $sql);
        return $sql;
    }
    public function clear_history_load($over=60){
        $sql = "update feedbiz_backend_load set expired = 1 where last_active <= (NOW() - INTERVAL {$over} MINUTE)";
        mysqli_query($this->db_con, $sql);
        return $sql;
    }
    public function insert_history_load($user_id,$target_ip,$url,$option=array()){
        $user_ip = $this->getUserIP();
        $option = array_merge($option,array('user_ip'=>$user_ip));
        $sql = "insert feedbiz_backend_load (user_id,web_server_ip,back_server_ip,last_active,activity,url) "
                . " values ('{$user_id}','{$this->ip}','{$target_ip}',now(),'".json_encode($option)."','{$url}') ";
        mysqli_query($this->db_con, $sql);
        return $sql;
    }
    
    public function get_best_host($user_id=null){
        if(!empty($user_id)){
            $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb ,users u"
                    . "  where u.backend_host_id = fb.id and fb.status = 1  and u.id = '$user_id' order by  fb.server_load  asc limit 1 ";
             $q = mysqli_query($this->db_con, $sql);
     
             if(mysqli_num_rows($q)==0){
                 $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb "
                    . "  where  fb.status = 1  order by  fb.server_load  asc limit 1 ";
             }
        }else{
             $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb "
                    . "  where  fb.status = 1  order by  fb.server_load  asc limit 1 ";
        }
        $q = mysqli_query($this->db_con, $sql);
        $d = mysqli_fetch_assoc($q);
        return $d;
    }
    
    public function setLastHost($user_id,$id){
        $sql = "update users set backend_host_id = '$id' where u.id = '$user_id' ";
        $q = mysqli_query($this->db_con, $sql);
        return true;
    }
    
}
//
//$obj = new feedbiz_server_backend();
//echo print_r($obj->update_server_status( ),true);