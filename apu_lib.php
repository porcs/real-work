<?php
define('HSNLG','has_new_login');


function ajax_update_process(){ 
        global $config,$userdata;
        $isAjax=isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        if(!$isAjax) { 
            die('Access denied - not an AJAX request...');
        } 
        $ref_host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        if($ref_host!=$_SERVER["HTTP_HOST"]){
            die('Access denied - host are not correct');
        }
        
        validateAjaxToken();
        @header_remove();
        $force_terminate = false;
        $win = false;
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
              $force_terminate = false;
              $win=true;
        }
        $win=true;
         
        
        if($force_terminate  && isset($userdata['id'])){// not login
            die('Session timeout');
        }  

        $user = isset($userdata['user_name'])?$userdata['user_name']:null; 
        if(empty($user)){
            die('Session timeout');
        }
        $clear_proc = isset($_REQUEST['clear_all_process'])?$_REQUEST['clear_all_process']:null;
        if($clear_proc  == 'all'){
            if (!session_id()) {  
            session_start(); 
            }
            unset($_SESSION['last_sig']);header_remove();die('clear_all');
        }else if(!empty($clear_proc)){
            if (!session_id()) {  
            session_start(); 
            }
            if(isset($_SESSION['last_sig']['proc'][$clear_proc ]))
                unset($_SESSION['last_sig']['proc'][$clear_proc ]);
            die();
        }
        $dp_proc = isset($_REQUEST['progress_display'])?$_REQUEST['progress_display']:null;
         if(!is_null($dp_proc)&&!empty($dp_proc)){
            if (!session_id()) {  
            session_start(); 
            }
            if($dp_proc=='show'){
                $_SESSION['progress_display']=true;
            }else{
                $_SESSION['progress_display']=false;
            }
            die();
        }
        
                
        try{  
            $data = @file_get_contents (($config['ssl_protocol']?'https://':'http://')."127.0.0.1:3001/get_process?$user");
        }catch(Exception $ex){ 
             die('can not get process');
        }
        if (!session_id()) {  
          session_start();   
          if($win){
                if(!isset($_SESSION['last_sig']) && isset($_SESSION['hide_sig'])){
                  try{  
                      @file_get_contents (($config['ssl_protocol']?'https://':'http://')."127.0.0.1:3001/clear_process?$user");
                      //unset($_SESSION['hide_sig']);
                       
                  }catch(Exception $ex){ 
                      die('can not clear process');
                  }
                }
          }else{
              if(!isset($_SESSION['last_sig'])){
                  unset($_SESSION['hide_sig']);
              }
          }
        } 
        if($win){
            $same = false; 
            if(isset($_SESSION['last_data'])){
                if(strcmp($_SESSION['last_data'],$data)==0 ){
                    $t = json_decode($data, true);
                    if(!empty($t) && isset($t['proc']) ){
                        $same = true;
                        $_SESSION['count_same'] = $_SESSION['count_same']+1;
                    }
                } 
            }else{ 
                $_SESSION['count_same'] = 0;
            }
            if(isset($data))
            $_SESSION['last_data'] = $data;

            if($same&&$_SESSION['count_same']>100){  
                try{  
//                    @file_get_contents (($config['ssl_protocol']?'https://':'http://')."127.0.0.1:3001/clear_process?$user");
//                    if(isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig'] ) > 0){
//                        foreach($_SESSION['hide_sig'] as $k=>$v){
//                            if($v['popup_display']){
//                                unset($_SESSION['hide_sig'][$k]);
//                            }
//                        }
//                     }
//                    unset($_SESSION['last_data']);
                }catch(Exception $ex){ 
                    die('can not clear process');
                }
            }
        }
        
        if(!isset($_SESSION['count_empty'])){
            $_SESSION['count_empty'] = 0;
        }
         if(isset($data) && !empty($data)){
            $ex = json_decode($data, true); 
            if(!isset($ex['proc'])){
                $_SESSION['count_empty']  = $_SESSION['count_empty'] +1;
            }else{
                $_SESSION['count_empty'] =0;
            }
         }
         if($_SESSION['count_empty'] >100){ 
             if(isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig'] ) > 0){
                foreach($_SESSION['hide_sig'] as $k=>$v){
//                    if($v['status']==4 || $v['status']==9){
                        unset($_SESSION['hide_sig'][$k]);
//                    }
                }
             }
         }
         if($_SESSION['count_empty'] >120){
             $_SESSION['count_empty'] =0;
             if(isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig'] ) > 0){
                foreach($_SESSION['hide_sig'] as $k=>$v){ 
                        unset($_SESSION['hide_sig'][$k]); 
                }
             }
         }
             
         
        
        if(isset($data) && !empty($data)){
            $ex = json_decode($data, true); 
            if(isset($ex['proc']))
            foreach($ex['proc'] as $k => $v){
                if($k==''){//if($k==''|| $v['bid']==null){
                    unset($ex['proc'][$k]);
                    continue;
                }
                $v['action'] = str_replace('%20',' ',$v['action']);
                if(!$ex['proc'][$k]['popup_display']){
                    $_SESSION['hide_sig'][$v['action']] = $v;
                    if(isset($_SESSION['display_all_popup']) && $_SESSION['display_all_popup'] == '1'){
                        
                    }else{
                        if(isset($_SESSION['progress_display'])  && $_SESSION['progress_display']== false){
                            unset($ex['proc'][$k]);
                        }
                    }
                }else{
                    $_SESSION['hide_sig'][str_replace(' ','_',$v['process'])] = $v;
                }
            }
            if(isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig'])>0){
                 set_userdata('hide_sig',json_encode($_SESSION['hide_sig']));
            }else{
                 unset_userdata('hide_sig');
            }
        }else{$ex = array('error_reason'=>'Can not connect server node');
             die('Can not connect server node.');
        }
                
        if(isset($_SESSION['last_sig'])){
            $l_sig = $_SESSION['last_sig'];
        }else{
            $l_sig = $ex;
        }
        $out_sig = $ex;
        
        if(isset($l_sig['proc'])){
            foreach($l_sig['proc'] as $k => $v){ 
                
                
                if( !isset($ex['proc'][$k]) ){ //something wrong
                    $out_sig['proc'][$k] = $l_sig['proc'][$k];
                    if(!isset($l_sig['proc'][$k]['status'])){
                        unset($l_sig['proc'][$k]); 
                        unset($out_sig['proc'][$k]);continue;
                    }
                    if($l_sig['proc'][$k]['status'] == 4 || $l_sig['proc'][$k]['status'] == 8){// process not start
                         $out_sig['proc'][$k]['s_status'] = 'error';
                    }else if($l_sig['proc'][$k]['progress'] == 100 || $l_sig['proc'][$k]['status'] == 9){// process done
                        $out_sig['proc'][$k]['s_status'] = 'complete';
                        if(time() - strtotime($out_sig['proc'][$k]['time']) > 240){
                            unset($out_sig['proc'][$k]);
                        }
                        
                    }else if($l_sig['proc'][$k]['progress'] != 100 && $l_sig['proc'][$k]['status'] != 9){// process terminate with out done
                        if($l_sig['proc'][$k]['progress']>50){
                        $out_sig['proc'][$k]['s_status'] = 'complete';
                        }else{
                        $out_sig['proc'][$k]['s_status'] = 'error';
                        }
                    }else if($l_sig['proc'][$k]['progress'] == 0 || $l_sig['proc'][$k]['status'] == 0){// process not start

                    }else{// process terminate with out done
                        $out_sig['proc'][$k]['s_status'] = 'error';
                    }
                    
//                    if(isset($out_sig['proc'][$k]['time']) && (time() - strtotime($out_sig['proc'][$k]['time']) > 600)){
//                        unset($out_sig['proc'][$k]);
//                    }
                }else{// process running
                    if($l_sig['proc'][$k]['status'] == 9 || $l_sig['proc'][$k]['progress'] == 100){
                        $out_sig['proc'][$k]['s_status'] = 'complete';
                    }else if($l_sig['proc'][$k]['status'] == 4 || $l_sig['proc'][$k]['status'] == 8){// process not start
                         $out_sig['proc'][$k]['s_status'] = 'error';
                    }else{
                        $out_sig['proc'][$k]['s_status'] = 'running';
                    }
                }
                 
            }
        }
        if(!isset($_SESSION['sig_last_time']))$_SESSION['sig_last_time']=array();
        if(isset($out_sig['proc'])){
            foreach($out_sig['proc'] as $k=>$v){
                if(isset($out_sig['proc'][$k]['time'])){
                    $last_time = isset($_SESSION['sig_last_time'][$k])?$_SESSION['sig_last_time'][$k]:0;
                    $sig_time = strtotime($out_sig['proc'][$k]['time']);
                     $new_sig = true;
                    if($last_time >= $sig_time){
                        $new_sig = false; 
                    } 
                    $_SESSION['sig_last_time'][$k] = $sig_time;
                    $out_sig['proc'][$k]['nsig'] = $new_sig; 
                    
                    if(isset($out_sig['proc'][$k]['time']) && (time() - strtotime($out_sig['proc'][$k]['time']) > 600)){
                        unset($out_sig['proc'][$k]);
                    }
                }
            }
        }
        
        if(!isset($ex['delay']) || !isset($ex['timeout'])){
            $out_sig['delay']=6001;
            $out_sig['timeout']=10001;
        } 
        if(!isset($_SESSION['progress_display'])){
            $out_sig['dp'] = true;
        }else{
            $out_sig['dp'] = $_SESSION['progress_display'];
        }
         
        if( isset($data) && strpos($data,'throw') === false){
            if(sizeof($out_sig)!=0){
            echo json_encode($out_sig);
            }
        }
        $_SESSION['last_sig']=$out_sig;
        if(isset($out_sig['proc'])){
            foreach($out_sig['proc'] as $k=>$v){
                if(isset($out_sig['proc'][$k]['time'])){ 
                    if(isset($out_sig['proc'][$k]['time']) && (time() - strtotime($out_sig['proc'][$k]['time']) > 600)){
                        unset($out_sig['proc'][$k]);
                    }
                }
            }
        }
        
        @header_remove();
        session_write_close();
        
        exit;
    } 
function validateAjaxToken(){
    global $userdata,$dbx;
    $ajax_request = strpos($_SERVER['HTTP_ACCEPT'], 'json') !== FALSE;
    $token = isset($_SERVER['HTTP_X_AUTH_TOKEN']) ? $_SERVER['HTTP_X_AUTH_TOKEN'] : '';
    $error_message = '';

    if(empty($token)){
            $error_message = 'Ajax authentication fail.'; 
    }
    else{
        if(isset($userdata['id'])){
            $user_id = $userdata['id'] ;
            $sql = "select request_id,token,status,user_code from token where request_id = '$user_id' and status != 0 and source = 'login' ";
            $r = mysqli_query($dbx,$sql);
            if (mysqli_num_rows($r)==0){
                $error_message = 'Ajax authentication fail(tk).';
            }else{
                $out = mysqli_fetch_assoc($r);
                $tokenDB = $out['token']; 

                $mtime = isset($userdata['mtime'])?$userdata['mtime']:'';
                if(empty($mtime)){
                    $error_message = 'Ajax authentication fail(ss).';

                }else{
                $mtime = explode('|',$mtime); 
                $otp = md5($mtime[0].'|'.$tokenDB.'|'.$mtime[1]);
                    if($otp == $token){
                        return true;
                    }else{ 
                        $session_id = $userdata['session_id'];
                        if($out['status'] >1 && $out['status'] !=9  && $out['user_code'] != $session_id){
                            $error_message = "Your account has logged in by another session";
                            $data = array( 'token_error'=>'other_login', 'case'=>'1','msg'=>$error_message);
                            $data['delay']=5001;
                            $data['timeout']=10001;
                            die(json_encode($data));
                        }else if($out['status']==9  && $out['user_code'] != $session_id){
                            $error_message = "Your account has logged in by Feed.biz Admin";
                            $data = array('token_error'=>'other_login','case'=>'2','msg'=>$error_message);
                            $data['delay']=5001;
                            $data['timeout']=10001;
                            die(json_encode($data));
                        }else{
                            $error_message = 'Ajax authentication fail(ot).';
                        }

                    }
                }
            }
        }
    } 
   
            die($error_message); 
}
function unset_userdata($newdata = array())
	{
            global $userdata;
		if (is_string($newdata))
		{
			$newdata = array($newdata => '');
		}

		if (count($newdata) > 0)
		{
			foreach ($newdata as $key => $val)
			{
				unset($userdata[$key]);
			}
		}

		 sess_write();
	}
function set_userdata($newdata = array(), $newval = '')
{
        global $userdata;
        if (is_string($newdata))
        {
                $newdata = array($newdata => $newval);
                
        }

        if (count($newdata) > 0)
        {
                foreach ($newdata as $key => $val)
                {
                        $userdata[$key] = $val;
                }
        }

        sess_write();
}
function _serialize($data)
{
        if (is_array($data))
        {
                array_walk_recursive($data,  '_escape_slashes' );
        }
        elseif (is_string($data))
        {
                $data = str_replace('\\', '{{slash}}', $data);
        }
        return serialize($data);
}
function encode($string, $key = '')
{
         
        $key = encryption_key;

//        if ($this->_mcrypt_exists === TRUE)
//        {
//                $enc = $this->mcrypt_encode($string, $key);
//        }
//        else
//        {
//                $enc = $this->_xor_encode($string, $key);
//        }
        $enc =  mcrypt_encode($string, $key);

        return base64_encode($enc);
}
function _escape_slashes(&$val, $key)
{
        if (is_string($val))
        {
                $val = str_replace('\\', '{{slash}}', $val);
        }
}
function _add_cipher_noise($data, $key)
{
        $keyhash =  _hash($key);
        $keylen = strlen($keyhash);
        $str = '';

        for ($i = 0, $j = 0, $len = strlen($data); $i < $len; ++$i, ++$j)
        {
                if ($j >= $keylen)
                {
                        $j = 0;
                }

                $str .= chr((ord($data[$i]) + ord($keyhash[$j])) % 256);
        }

        return $str;
}
function _get_cipher()
{
//        if ($this->_mcrypt_cipher == '')
//        {
//                $this->_mcrypt_cipher = MCRYPT_RIJNDAEL_256;
//        }
//
//        return $this->_mcrypt_cipher;
    return MCRYPT_RIJNDAEL_256;
}
function _get_mode()
{
//        if ($this->_mcrypt_mode == '')
//        {
//                $this->_mcrypt_mode = MCRYPT_MODE_CBC;
//        }
//
//        return $this->_mcrypt_mode;
    return MCRYPT_MODE_CBC;
}
function mcrypt_encode($data, $key)
{       
        $key = md5($key);
        $init_size = mcrypt_get_iv_size( _get_cipher(),  _get_mode());
        $init_vect = mcrypt_create_iv($init_size, MCRYPT_RAND);
        return  _add_cipher_noise($init_vect.mcrypt_encrypt( _get_cipher(), $key, $data, _get_mode(), $init_vect), $key);
}
function _set_cookie($cookie_data = NULL)
{
    global $config,$userdata;
        if (is_null($cookie_data))
        {
                $cookie_data = $userdata;
        }

        // Serialize the userdata for the cookie
        $cookie_data = _serialize($cookie_data);

        if ($config['sess_encrypt_cookie'] == TRUE)
        {
                $cookie_data =  encode($cookie_data);
        }
        else
        {
                // if encryption is not used, we provide an md5 hash to prevent userside tampering
                $cookie_data = $cookie_data.md5($cookie_data. (encryption_key));
        }

        $expire=  $config['sess_expiration'] + time();

        // Set the cookie
//        setcookie(
//                        $config['sess_cookie_name'],
//                        $cookie_data,
//                        $expire,
//                        $config['cookie_path'],
//                        $config['cookie_domain'],
//                        $config['cookie_secure']
//                );
}
function sess_write()
{       
        global $config,$userdata,$dbx;
//        echo '<pre>';
//        print_r($userdata);
//        echo '</pre>';
        // Are we saving custom data to the DB?  If not, all we do is update the cookie
        if ($config['sess_use_database'] === FALSE)
        {
                 _set_cookie();
                return;
        }

        // set the custom userdata, the session data we will set in a second
        $custom_userdata = $userdata;
        $cookie_userdata = array();

        // Before continuing, we need to determine if there is any custom data to deal with.
        // Let's determine this by removing the default indexes to see if there's anything left in the array
        // and set the session data while we're at it
        foreach (array('session_id','ip_address','user_agent','last_activity') as $val)
        {
                unset($custom_userdata[$val]);
                $cookie_userdata[$val] = $userdata[$val];
        }

        // Did we find any custom data? If not, we turn the empty array into a string
        // since there's no reason to serialize and store an empty array in the DB
        if (count($custom_userdata) === 0)
        {
                $custom_userdata = '';
        }
        else
        {
                // Serialize the custom data array so we can store it
                $custom_userdata =  _serialize($custom_userdata);
        }
         
        $sql = "update ".$config['sess_table_name']." set last_activity = '".sqlite_escape_string($userdata['last_activity'])."', user_data = '".sqlite_escape_string($custom_userdata)."' where  session_id = '".$userdata['session_id']."'";
        
        // Run the update query
        mysqli_query($dbx,$sql);
        // Write the cookie. Notice that we manually pass the cookie data array to the
        // _set_cookie() function. Normally that function will store $this->userdata, but
        // in this case that array contains custom data, which we do not want in the cookie.
         _set_cookie($cookie_userdata);
}

function mb_unserialize($string) {
    $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $string);
    return unserialize($string);
}
function _unserialize($data)
	{
                $datax = @unserialize(stripslashes ($data));
                
                if(empty($datax)){
                    $data = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
                            function($match) {
                                return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                            },
                        $data );
                    $dataA = @unserialize(stripslashes ($data));
//                    $dataA = mb_unserialize(stripslashes($data));
                    if(!empty($dataA)&&is_array($dataA)){ $data= $dataA;}
                }else{
                    $data = $datax;
                }
                  

		if (is_array($data))
		{
			array_walk_recursive($data,  '_unescape_slashes' );
			return $data;
		}

		return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
//                $datax = @unserialize(strip_slashes($data)); 
//                if(empty($datax)){
//                    $dataA = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
//                            function($match) {
//                                return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
//                            },
//                        $data );
////                    $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $data);
//                    $dataA = unserialize(stripslashes ($dataA));
//                    
//                    if(!empty($dataA)&&is_array($data)){ $data= $dataA;}
//                }else{
//                    $data=$datax;
//                }
//		if (is_array($data))
//		{
//			array_walk_recursive($data, array(  '_unescape_slashes'));
//			return $data;
//		}
//
//		return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
	}
function _unescape_slashes(&$val, $key)
{
        if (is_string($val))
        {
                $val= str_replace('{{slash}}', '\\', $val);
        }
}
function mcrypt_decode($data, $key)
	{       $key = md5($key);
		$data =  _remove_cipher_noise($data, $key);
		$init_size = mcrypt_get_iv_size( _get_cipher(),  _get_mode());

		if ($init_size > strlen($data))
		{
			return FALSE;
		}

		$init_vect = substr($data, 0, $init_size);
		$data = substr($data, $init_size);
                 
		return rtrim(mcrypt_decrypt( _get_cipher(), $key, $data,  _get_mode(), $init_vect), "\0");
	}
function _hash($str)
{
        return ( _hash_type == 'sha1') ? _sha1($str) : md5($str);
}

function _sha1($str)
{
        if ( ! function_exists('sha1'))
        {
                if ( ! function_exists('mhash'))
                {
                        require_once(BASEPATH.'libraries/Sha1.php');
                        $SH = new CI_SHA;
                        return $SH->generate($str);
                }
                else
                {
                        return bin2hex(mhash(MHASH_SHA1, $str));
                }
        }
        else
        {
                return sha1($str);
        }
}

function _remove_cipher_noise($data, $key)
	{
		$keyhash =  _hash($key);
		$keylen = strlen($keyhash);
		$str = '';

		for ($i = 0, $j = 0, $len = strlen($data); $i < $len; ++$i, ++$j)
		{
			if ($j >= $keylen)
			{
				$j = 0;
			}

			$temp = ord($data[$i]) - ord($keyhash[$j]);

			if ($temp < 0)
			{
				$temp = $temp + 256;
			}

			$str .= chr($temp);
		}

		return $str;
	}



        
?>