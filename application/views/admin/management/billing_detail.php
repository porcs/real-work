<div class="container">    
    <h2><?php echo $this->lang->line('manager_bill_detail'); ?></h2>
    <?php echo $nav; ?>
</div>

<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="top: 24px;width: 100%;padding-top: 5px;padding-bottom: 5px;z-index: 100;margin: 0;" class="affix-top">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;background: #FBFBFB;background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
            <div style="text-align:right;width:100%;">
                <a href="<?php echo site_url('admin/billing/'); ?>" class="btn btn-info btn-search">
                    <i class="icon-arrow-left icon-white"></i> 
                    <?php echo $this->lang->line('back'); ?>
                </a>
                <a href="<?php echo site_url('admin/billing/printBilling/?id='.$query['id_billing']); ?>" target="_blank" class="btn btn-success btn-search">
                    <i class="icon-print icon-white"></i> 
                    <?php echo $this->lang->line('print'); ?>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="span5">
            <div class="x-table well " style="background:#FBFBFB;">
                <form class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_title_id'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['id_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_title_username'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['user_name']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_title_amount'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $this->mydate->moneySymbol($query['currency_billing']); ?><?php echo number_format($query['amt_billing'],2); ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_company'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['company_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_first_name'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['first_name_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_last_name'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['last_name_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('address1'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['address1_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('address2'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['address2_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('stateregion'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['stateregion_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('city'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['city_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('country'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['country_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('zipcode'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $query['zipcode_billing']; ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_payment_method'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <span class="label label-info" style="text-transform: capitalize;"><?php echo $query['payment_method_billing']; ?></span>
                        </div>
                    </div>
                    

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('status'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <span class="label<?php if($query['status_billing']=='active'){?> label-success<?php }else{ ?> label-important<?php } ?>" style="text-transform: capitalize;"><?php echo $query['status_billing']; ?></span>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_title_created'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo date('j F Y',strtotime($query['created_date_billing'])); ?>
                        </div>
                    </div>

                    <?php
                    if($query['discount_cmd_billing'])
                    {
                        $discountCmd = unserialize($query['discount_cmd_billing']);
                        $discountLog = unserialize($query['discount_log_billing']);
                        
                        $disCode = '';
                        if(isset($discountCmd['gift']) && $discountCmd['gift'])
                        {
                            $disTitle = 'Gift Voucher Discount';
                            $disCode = $discountLog['gift'];
                        }
                        else
                        {
                            $disTitle = 'Referral Discount';
                        }
                    ?>
                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_dc_title'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php echo $disTitle; ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_dc_detail'); ?></b>
                        </label>
                        <div class="controls" style="padding-top:5px;">
                            <?php if($disCode!=''){ ?>
                            <p><b><?php echo $this->lang->line('billing_dc_code'); ?>:&nbsp;</b><span class="label label-info"><?php echo $disCode; ?></span></p>
                            <?php } ?>
                            <p><b><?php echo $this->lang->line('billing_dc_full'); ?>:&nbsp;</b><?php echo $this->mydate->moneySymbol($query['currency_billing']).number_format($discountCmd['total']['full_amt'],2); ?></p>
                            <p><b><?php echo $this->lang->line('billing_dc_amt'); ?>:&nbsp;</b><?php echo $this->mydate->moneySymbol($query['currency_billing']).number_format($discountCmd['total']['dc_amt'],2); ?></p>
                            <p><b><?php echo $this->lang->line('billing_dc_total'); ?>:&nbsp;</b><?php echo $this->mydate->moneySymbol($query['currency_billing']).number_format($discountCmd['total']['total_amt'],2); ?></p>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                    
                    <div class="control-group">
                        <label class="control-label">
                            <b><?php echo $this->lang->line('billing_payment_log'); ?></b>
                        </label>
                    </div>
                    
                    <?php
                    if($query['payment_log_billing'])
                    {
                        $arrPaymentLog = unserialize($this->cipher->decrypt($query['payment_log_billing']));
                        print('<pre>');
                        print_r($arrPaymentLog);
                        print('</pre>');
                    }
                    ?>
                    
                </form>
            </div>
        </div>
        <div class="span7">
            <table class="table table-striped table-bordered table-hover list table-condensed">
                <thead>
                    <tr>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 20px;"><?php echo $this->lang->line('no_'); ?></th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 30px;"><?php echo $this->lang->line('billing_table_id'); ?>.</th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;"><?php echo $this->lang->line('billing_table_package'); ?>.</th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 70px;"><?php echo $this->lang->line('billing_table_type'); ?>.</th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 70px;"><?php echo $this->lang->line('billing_table_amt'); ?>.</th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 90px;"><?php echo $this->lang->line('billing_table_startdate'); ?>.</th>
                        <th style="cursor: default;text-align: center;color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #E6E6E6;width: 90px;"><?php echo $this->lang->line('billing_table_enddate'); ?>.</th>
                    </tr>
                </thead>
                <?php if($queryDetail){ ?>
                <tbody>
                    <?php $count=1; foreach($queryDetail as $val){ ?>
                    <tr>
                        <td style="text-align: center;"><?php echo $count; ?></td>
                        <td style="text-align: right;"><?php echo $val['id_package']; ?></td>
                        <td><?php echo $val['package_detail']['desc']; ?></td>
                        <td><span class="label label-info" style="text-transform: capitalize;"><?php echo $val['option_package']; ?></span></td>
                        <td style="text-align: right;"><?php echo $this->mydate->moneySymbol($query['currency_billing']).number_format($val['package_detail']['amt'],2); ?></td>
                        <td><?php echo date('j F Y',strtotime($val['start_date_bill_detail'])); ?></td>
                        <td><?php echo date('j F Y',strtotime($val['end_date_bill_detail'])); ?></td>
                    </tr>
                    <?php $count+=1; } ?>
                    
                    <?php if($query['discount_cmd_billing']){ ?>
                    <tr>
                        <td style="text-align: center;"><?php echo $count; ?></td>
                        <td style="text-align: right;">&nbsp;</td>
                        <td><?php echo $disTitle; ?></td>
                        <td><span class="label label-info" style="text-transform: capitalize;">discount</span></td>
                        <td style="text-align: right;">-<?php echo $this->mydate->moneySymbol($query['currency_billing']).number_format($discountCmd['total']['dc_amt'],2); ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <?php } ?>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>
</div>

<script language="javascript">
$(function(){   
    $('title').html($('h2').html());
});
</script>