<?php
switch($sort)
{
    case 'ASC':
        $iconSort = '<i class="arrow asc"></i>';
        $sortAction = 'DESC';
        break;
    case 'DESC':
        $iconSort = '<i class="arrow desc"></i>';
        $sortAction = 'ASC';
        break;
}

$paramPageArr = $param;
unset($paramPageArr['page']);
$paramPage = implode('&',$paramPageArr);
?>
<div class="container">    
    <h2><?php echo $this->lang->line('manager_bill');?></h2>
    <?php echo $nav; ?>
</div>

<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="top: 24px;width: 100%;padding-top: 5px;padding-bottom: 5px;z-index: 100;margin: 0;" class="affix-top">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;background: #FBFBFB;background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
            <div style="text-align:right;width:100%;">
                <b><?php echo $this->lang->line('search'); ?>:&nbsp;</b>
                <input type="text" name="keyword" class="input-xlarge" style="margin: 0;">&nbsp;
                <button type="button" class="btn btn-info btn-search"><i class="icon-search icon-white"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="span12">
            <?php if($query['page']<=$endPage){ ?>
            <div class="pagination pagination-right">
                <ul>
                    <li<?php echo $query['page']==1?' class="disabled"':''; ?>><a href="<?php echo $query['page']!=1?site_url('admin/billing/?'.$paramPage.'&page='.($query['page']-1)):'#'; ?>">«</a></li>
                    <?php for($i=$startPage;$i<=$endPage;$i++){ ?>
                    <li<?php echo $i==$query['page']?' class="active"':''; ?>>
                        <a href="<?php echo $i==$query['page']?'#':site_url('admin/billing/?'.$paramPage.'&page='.$i); ?>"><?php echo $i; ?></a>
                    </li>
                    <?php } ?>
                    <li<?php echo $query['page']==$totalRow?' class="disabled"':''; ?>><a href="<?php echo $query['page']==$totalRow?'#':site_url('admin/billing/?'.$paramPage.'&page='.($query['page']+1)); ?>">»</a></li>
                </ul>
            </div>
            <?php } ?>
            <table class="table table-striped table-bordered table-hover list table-condensed">
                <thead>
                    <tr>
                        <th style="cursor: default;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 20px;">
                            <?php echo $this->lang->line('no_'); ?>
                        </th>
                        <th class="topic-sort" data-sort="<?php echo $sortAction; ?>" data-field="b.id_billing" style="cursor: pointer;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 10%;">
                            <?php echo $this->lang->line('billing_title_id'); ?>.
                            <?php
                            if($order == 'b.id_billing')
                            {
                                echo $iconSort;
                            }
                            ?>
                        </th>
                        <th class="topic-sort" data-sort="<?php echo $sortAction; ?>" data-field="u.user_name" style="cursor: pointer;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 15%;">
                            <?php echo $this->lang->line('billing_title_username'); ?>.
                            <?php
                            if($order == 'u.user_name')
                            {
                                echo $iconSort;
                            }
                            ?>
                        </th>
                        <th style="cursor: default;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 15%;">
                            <?php echo $this->lang->line('billing_title_amount'); ?>.
                        </th>
                        <th class="topic-sort" data-sort="<?php echo $sortAction; ?>" data-field="b.payment_method_billing" style="cursor: pointer;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 15%;">
                            <?php echo $this->lang->line('billing_title_method'); ?>.
                            <?php
                            if($order == 'b.payment_method_billing')
                            {
                                echo $iconSort;
                            }
                            ?>
                        </th>
                        <th class="topic-sort" data-sort="<?php echo $sortAction; ?>" data-field="b.created_date_billing" style="cursor: pointer;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 15%;">
                            <?php echo $this->lang->line('billing_title_created'); ?>.
                            <?php
                            if($order == 'b.created_date_billing')
                            {
                                echo $iconSort;
                            }
                            ?>
                        </th>
                        <th class="topic-sort" data-sort="<?php echo $sortAction; ?>" data-field="b.status_billing" style="cursor: pointer;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;width: 10%;">
                            <?php echo $this->lang->line('status'); ?>.
                            <?php
                            if($order == 'b.status_billing')
                            {
                                echo $iconSort;
                            }
                            ?>
                        </th>
                        <th style="cursor: default;text-align: center; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;">
                            <?php echo $this->lang->line('actions'); ?>.
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($query['query']){
                    foreach($query['query'] as $key => $val){
                    ?>
                    <tr>
                        <td style="text-align: right;"><?php echo number_format($key+1); ?></td>
                        <td><?php echo $val['id_billing']; ?></td>
                        <td><?php echo $val['user_name']; ?></td>
                        <td style="text-align: right;">
                            <?php
                            echo $this->mydate->moneySymbol($val['currency_billing']);
                            if($val['discount_log_billing']!='')
                            {
                                $tmpDiscount = unserialize($val['discount_cmd_billing']);
                                echo number_format($tmpDiscount['total']['total_amt'],2);
                            }
                            else
                            {
                                echo number_format($val['amt_billing'],2);
                            }
                            ?>
                        </td>
                        <td><span class="label label-info" style="text-transform: capitalize;"><?php echo $val['payment_method_billing']; ?></span></td>
                        <td><?php echo date('j F Y',strtotime($val['created_date_billing'])); ?></td>
                        <td style="text-align: center;"><span class="label<?php if($val['status_billing']=='active'){ ?> label-success<?php }else{ ?> label-important<?php } ?>" style="text-transform: capitalize;"><?php echo $val['status_billing']; ?></span></td>
                        <td style="text-align: center;">
                            <a type="button" href="<?php echo site_url('admin/billing/detail/?id='.$val['id_billing']); ?>" class="btn btn-mini btn-primary">
                                <?php echo $this->lang->line('view'); ?>
                            </a>
                            <a type="button" href="<?php echo site_url('admin/billing/printBilling/?id='.$val['id_billing']); ?>" target="_blank" class="btn btn-mini btn-success">
                                <?php echo $this->lang->line('print'); ?>
                            </a>                                                                                    
                        </td>
                    </tr>
                    <?php 
                    }}
                    ?>
                </tbody>
            </table>
            <?php if($query['page']<=$endPage){ ?>
            <div class="pagination pagination-right">
                <ul>
                    <li<?php echo $query['page']==1?' class="disabled"':''; ?>><a href="<?php echo $query['page']!=1?site_url('admin/billing/?'.$paramPage.'&page='.($query['page']-1)):'#'; ?>">«</a></li>
                    <?php for($i=$startPage;$i<=$endPage;$i++){ ?>
                    <li<?php echo $i==$query['page']?' class="active"':''; ?>>
                        <a href="<?php echo $i==$query['page']?'#':site_url('admin/billing/?'.$paramPage.'&page='.$i); ?>"><?php echo $i; ?></a>
                    </li>
                    <?php } ?>
                    <li<?php echo $query['page']==$totalRow?' class="disabled"':''; ?>><a href="<?php echo $query['page']==$totalRow?'#':site_url('admin/billing/?'.$paramPage.'&page='.($query['page']+1)); ?>">»</a></li>
                </ul>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<script language="javascript">
$(function(){
    $('.topic-sort').click(function(){
        var sort=$(this).attr('data-sort');
        var field=$(this).attr('data-field');
        
        window.location.href = "<?php site_url('admin/billing/'); ?>?order="+field+"&sort="+sort;
    });
    
    $('.btn-search').click(function(){
        var keyword = $('input[name="keyword"]').val();
        if(keyword != '')
        {
            window.location.href = "<?php site_url('admin/billing/'); ?>?keyword="+keyword;
        }
    });
    
    $('title').html($('h2').html());
});
</script>