<?php $AUTH = $this->session->userdata('AUTH'); ?>

<div class="container">
    
    <h2><?php echo $this->lang->line('packages_manager');?></h2>
    <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
        <?php if ((int) $AUTH['group']['group_manage_flag'] == 1 || 
                (int) $AUTH['group']['group_manage_flag'] == 3 ||
                (int) $AUTH['user_manage_flag'] == 1 || 
                (int) $AUTH['user_manage_flag'] == 3 ) { ?>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/packages/packages"> &nbsp; <?php echo $this->lang->line('packages');?> &nbsp; </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/packages/marketplace"><?php echo $this->lang->line('marketplace');?></a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/packages/country"><?php echo $this->lang->line('country');?></a></li> 
        <li><a href="<?php echo base_url(); ?>index.php/admin/packages/currency"><?php echo $this->lang->line('currency');?></a></li> 
        <?php } ?>
    </ul>
    
    <?php echo $content; ?>
    
</div>
