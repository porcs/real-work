<?php
global $date_format_convert;
$CI = & get_instance();
$lang = $CI->lang;

$group_1 = array(
	'name_offer_pkg' => 'Marketplace',
	'title_offer_sub_pkg' => 'Site country',
	'domain_offer_sub_pkg' => 'Site domain'
);

$group_2 = array(

	'amt_offer_price_pkg' => 'Price',
	'currency_offer_price_pkg' => 'Currency',
	'priod_offer_price_pkg' => 'Priod',
	'element_offer_sub_pkg' => 'Zone',
	'id_region' => 'Region',
	'status_offer_price_pk' => 'Status',
        'offer_pkg_type' => 'Pkg type (for Dev.)',
        'comments' => 'Comments (for Dev.)',
        'id_site_ebay' => 'ID site eBay (for Dev.)',
);

$group_f_1 = array();
$group_f_2 = array();
foreach ( $rs as $ka => $va) {
	foreach ( $va as $kb => $vb ) {
		if ( in_array( $kb, array_keys( $group_1 ) ) ) {
			$group_f_1[$kb] = $vb;
		}
		if ( in_array( $kb, array_keys( $group_2 ) ) ) {
			$group_f_2[$kb] = $vb;
		}
	}
}
?>
</div>
<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="
         top: 24px;
         width: 100%;
         padding-top:5px;
         padding-bottom:5px;
         z-index: 100;">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;
        	background: #FBFBFB;
       		background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
            <div style="text-align:right; width:100%;">
                <a class="btn"  onclick="crudBack();">   <i class="icon-arrow-left"></i>  <?php echo $lang->line('back'); ?>  &nbsp; </a>
                <a class="btn btn-info" onclick="__edit();" > &nbsp;  <i class="icon-edit icon-white"></i>  <?php echo $lang->line('edit');?> &nbsp; </a>
            </div>
        </div>
    </div>
    </div>
<div class="container">

<div class='x-table well <?php echo $this->conf['color']; ?>' style="background:#FBFBFB;">
    <?php $elements = $this->form; ?>
    <form method="post" action="" id="crudForm" <?php if ($this->frmType == '2') { ?>class="form-horizontal"<?php } ?>>

         <div class="col-box-2">
             <h3>Marketplace Information</h3>
			 <?php foreach ( $group_f_1 as $k => $v) { ?>
				<div class="control-group">
					<label for="crudTitle" class="control-label">
					<b><?php echo $group_1[$k] ?></b>
					</label>
					<div class="controls" style="padding-top:5px;"><?php echo empty($v) ? ' - ': $v ?></div>
				</div>

			 <?php }?>
         </div>

         <div class="col-box-2">
             <h3>Package Information</h3>
			 <?php foreach ( $group_f_2 as $k => $v) { ?>
				<div class="control-group">
					<label for="crudTitle" class="control-label">
					<b><?php echo $group_2[$k] ?></b>
					</label>
					<div class="controls" style="padding-top:5px;"><?php echo empty($v) ? ' - ': $v ?></div>
				</div>

			 <?php }?>
         </div>

         <div style="clear: both;"></div>

    </form>
    <script>
    function __edit() {
    	<?php
    	$q = $this->queryString;
    	$q['xtype'] = 'form';
    	?>
    	                        window.location = "?<?php echo http_build_query($q, '', '&'); ?>";
    	                    }
                    function crudBack() {
<?php
$q = $this->queryString;
$q['xtype'] = 'index';
if (isset($q['key']))
    unset($q['key']);
?>
                        window.location = "?<?php echo http_build_query($q, '', '&'); ?>";
                    }
                    $(document).ready(function() {
                    	$('title').html($('h2').html());
                    });
    </script>
</div>