<?php $CI = & get_instance();
$key = $CI->input->post('key');
$lang = $CI->lang;  ?>

</div>
<style>.controls .row {    
    clear: both;
    display: inline-block;
    position: relative;
    width: 80%; 
    margin: 0 0 5px 0px;}</style>
<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="
         top: 24px;
         width: 100%;
         padding-top:5px;
         padding-bottom:5px;
         z-index: 100;">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;
        	background: #FBFBFB;
       		background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
            <div style="text-align:right;width:100%;">
                <a class="btn"  onclick="crudCancel();">  &nbsp; <?php echo $lang->line('cancel'); ?>  &nbsp; </a>
                <a class="btn btn-info" onclick="crudConfirm();" > &nbsp;  <i class="icon-edit icon-white"></i>  <?php echo $lang->line('confirm'); ?> &nbsp; </a>
            </div>
        </div>
    </div>
    </div>
<div class="container">

<div class='x-table well  <?php echo $this->conf['color']; ?>' style="background:#FBFBFB;">
    <?php
    $q = $this->queryString;
    $q['xtype'] = 'confirm';
    if (isset($q['key']))
        unset($q['key']);
    ?>
    <form method="post" action="?<?php echo http_build_query($q, '', '&'); ?>"  enctype="multipart/form-data"
          id="crudForm" style="padding: 0; margin: 0;" <?php if ($this->frmType == '2') { ?>class="form-horizontal"<?php } ?>>
        
              <?php 
              $elements = $this->form;
//              foreach ($this->primaryKey as $f) {
//                  $ary = explode('.', $f);
//                  if (isset($_GET['key'][$f]) || isset($key[$ary[0]][$ary[1]])) {
//                      if (isset($_GET['key'][$f])) {
//                          $_POST['key'][$ary[0]][$ary[1]] = $_GET['key'][$f];
//                      }
//                      echo __hidden('key.' . $f);
//                  }
//              }
              $group_name='';
              if(isset($_POST['group']) && !empty($_POST['group'])){ 
                  foreach($_POST['group'] as $k=>$v){
                  echo __hidden('key.' .$k,array('value'=>$v) );
                  $group_name=$v;
                  }
              }
              ?>
              <?php if (!empty($this->errors)) { ?>
            <div class="alert alert-error">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?php foreach ($this->errors as $error) { ?>
                    <?php if (count($error) > 0) { ?>
                        <strong>Error!</strong>
                        <?php echo implode('<br />', $error); ?>
                        <br />
                    <?php } ?>
                <?php } ?>
            </div>
        <?php }   
        ?>
        <div class="control-group ">
            <label for="crudRowsPerPage" class="control-label"><b>Color name                                 
                </b> </label>
                <div class="controls">
                    <input type="text" name="data[mapping_color][color_name]" id="dataMapping_colorColor_name" style="width:210px;" 
                           value="<?php echo $group_name;
                    ?>">
                    </div>
        </div>
        
        <div class="control-group ">
            <label for="crudRowsPerPage" class="control-label"><b>Region / mapping value                                 
                </b> </label>
                <div class="controls">
                    <?php
                    if(!empty($_POST['data']) && is_array($_POST['data'])){
                        $i=0;
                        foreach($_POST['data'] as $k=>$v){
                        $region = $v[$this->conf['table']]['region'];
                        $value = $v[$this->conf['table']]['value'];
                    ?>
                    <div class="row">
                    <input type="text" name="data[mapping_color][region][]" id="dataMapping_colorColor_name" style="width:210px;"  value="<?php echo $region?>">
                    &nbsp;  &nbsp;  &nbsp;
                    <input type="text" name="data[mapping_color][value][]" id="dataMapping_colorColor_name" style="width:210px;"  value="<?php echo $value?>">
                    <i class="<?php echo $i==0?'icon-plus':'icon-minus';?> msg_field_add" style="margin-left: 5px;"></i>
                    </div>
                    <?php $i++;} 
                        }else{?>
                        <div class="row">
                        <input type="text" name="data[mapping_color][region][]" id="dataMapping_colorColor_name" style="width:210px;"  value="">
                        &nbsp;  &nbsp;  &nbsp;
                        <input type="text" name="data[mapping_color][value][]" id="dataMapping_colorColor_name" style="width:210px;"  value="">
                        <i class="icon-plus msg_field_add" style="margin-left: 5px;"></i>
                        </div>
                    
                    <?php }
                    ?>
                    
                     
                </div>
        </div>
         
    </form>
    <script>
                           function crudCancel() {
<?php
$q = $this->queryString;
$q['xtype'] = 'index';
if (isset($q['key']))
    unset($q['key']);
?>
                               window.location = "?<?php echo http_build_query($q, '', '&'); ?>";
                           }

                           function crudConfirm() {
                               $('#crudForm').submit();
                           }
                           $(document).ready(function() {
                        	   $('title').html($('h2').html());
                                   
                                   function apply(){
                                       console.log($('.icon-minus'));
                                        $('.icon-minus').unbind().on('click',function(){
                                            $(this).parent().remove();
                                        });
                                        $('.icon-plus').unbind().on('click',function(){
                                            var clone = $(this).parent().clone();
                                            clone.find('input').each(function(){$(this).val('');});
                                            clone.find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
                                            $(this).parents('.controls').append(clone);
                                            apply();
                                        });
                                   }
                                   apply();
                                   
                           });
    </script>
</div>
