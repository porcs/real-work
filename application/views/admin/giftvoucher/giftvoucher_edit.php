<script language="javascript" src="<?php echo site_url('assets/js/bootstrap-datepicker.js'); ?>"></script>
<script language="javascript" src="<?php echo site_url('assets/js/jquery.validationEngine.js'); ?>"></script>
<script language="javascript" src="<?php echo site_url('assets/js/jquery.validationEngine-en.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/datepicker2.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/validationEngine.jquery.css'); ?>">

<div class="container">
    <h2><?php echo $this->lang->line('giftvoucher_manager'); ?>: <?php echo $this->lang->line('giftvoucher_opt_edit_title'); ?></h2>
    <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/builder"> <?php echo $this->lang->line('components'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/groups"> <?php echo $this->lang->line('group_component'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/table/index"><?php echo $this->lang->line('table_builder'); ?></a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/language/index"> <?php echo $this->lang->line('language_manager'); ?> </a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/giftvoucher/index"> <?php echo $this->lang->line('giftvoucher_manager'); ?> </a></li>
    </ul>
    
    <div style="height: 52px;">
        <div data-spy="affix" data-offset-top="90" style="top: 24px;width: 100%;padding-top:5px;padding-bottom:5px;z-index: 100;" class="affix-top">
            <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;background: #FBFBFB;background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);margin: 0;">
                <div style="text-align:right;width:100%;">
                    <a href="<?php echo site_url('admin/giftvoucher/'); ?>" type="button" class="btn">
                        <i class="icon-chevron-left"></i> 
                        <?php echo $this->lang->line('cancel'); ?>
                    </a>
                    <button type="button" class="btn btn-info" id="btnSave">
                        <i class="icon-check icon-white"></i> 
                        <?php echo $this->lang->line('save_change'); ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <form method="post" id="frmGift" action="<?php echo site_url('admin/giftvoucher/edit/?id='.$queryGiftOption[0]['id_giftopt']); ?>" class="form-horizontal">
            <div>&nbsp;</div>
            <div class="control-group ">
                <label for="gift_name" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_name'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['name_giftopt']; ?>" type="text" id="gift_name" name="gift[name]" class="input-signup input-large validate[required]">
                </div>
            </div>
            
            <div class="control-group ">
                <label for="gift_cmd" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_cmd'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['cmd_giftopt']; ?>" type="text" id="gift_cmd" name="gift[cmd]" class="input-signup input-small">
                </div>
            </div>
            
            <div class="control-group ">
                <label for="gift_amt" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_amt'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['amt_giftopt']; ?>" type="text" id="gift_amt" name="gift[amt]" class="input-signup input-small validate[required]" value="0">
                </div>
            </div>
            
            <div class="control-group ">
                <label class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_currency'); ?></b>
                </label>
                <div class="controls">
                    <select id="gift_currency" name="gift[currency]" class="input-signup input-small">
                        <option value="EUR"<?php if($queryGiftOption[0]['currency_giftopt'] == 'EUR' ){ ?> selected="selected"<?php } ?>>EUR</option>
                        <option value="USD"<?php if($queryGiftOption[0]['currency_giftopt'] == 'USD' ){ ?> selected="selected"<?php } ?>>USD</option>
                        <option value="%"<?php if($queryGiftOption[0]['currency_giftopt'] == '%' ){ ?> selected="selected"<?php } ?>>%</option>
                    </select>
                </div>
            </div>
            
            <div class="control-group ">
                <label for="gift_code" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_qty'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['code_qty_giftopt']; ?>" type="text" id="gift_code" name="gift[code]" class="input-signup input-mini validate[required]" value="0">
                </div>
            </div>
            
            <div class="control-group ">
                <label class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_opt'); ?></b>
                </label>
                <div class="controls">
                    <select id="gift_priod" name="gift[priod]" class="input-signup input-small">
                        <option value="month"<?php if($queryGiftOption[0]['priod_giftopt'] == 'month' ){ ?> selected="selected"<?php } ?>>Month</option>
                        <option value="year"<?php if($queryGiftOption[0]['priod_giftopt'] == 'year' ){ ?> selected="selected"<?php } ?>>Year</option>
                        <option value="fixed"<?php if($queryGiftOption[0]['priod_giftopt'] == 'fixed' ){ ?> selected="selected"<?php } ?>>Fixed</option>
                    </select>
                </div>
            </div>
            
            <div class="control-group ">
                <label for="gift_startDate" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_start'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['start_date_giftopt']; ?>" type="text" id="gift_startDate" name="gift[startDate]" class="input-signup input-small validate[required]">
                </div>
            </div>
            
            <div class="control-group ">
                <label for="gift_endDate" class="control-label">
                    <b><?php echo $this->lang->line('giftvoucher_opt_add_end'); ?></b>
                </label>
                <div class="controls">
                    <input value="<?php echo $queryGiftOption[0]['end_date_giftopt']; ?>" type="text" id="gift_endDate" name="gift[endDate]" class="input-signup input-small validate[required]">
                </div>
            </div>
            
            <div class="control-group ">
                <label class="control-label">
                    <b><?php echo $this->lang->line('status'); ?></b>
                </label>
                <div class="controls">
                    <select id="gift_status" name="gift[status]" class="input-signup input-small">
                        <option value="active"<?php if($queryGiftOption[0]['status_giftopt'] == 'active' ){ ?> selected="selected"<?php } ?>>
                            <?php echo $this->lang->line('active'); ?>
                        </option>
                        <option value="inactive"<?php if($queryGiftOption[0]['status_giftopt'] == 'inactive' ){ ?> selected="selected"<?php } ?>>
                            <?php echo $this->lang->line('inactive'); ?>
                        </option>
                    </select>
                </div>
            </div>
        </form>
        
        <script language="javascript">
        $(function(){
            $('#gift_startDate').datepicker({
                format: 'yyyy-mm-dd'
            });
            
            $('#gift_endDate').datepicker({
                format: 'yyyy-mm-dd'
            });
            
            $('#btnSave').click(function(){
                $('#frmGift').validationEngine();
                $('#frmGift').submit();
            });
            
            $('title').html($('h2').html());
        });
        </script>
    </div>
</div>