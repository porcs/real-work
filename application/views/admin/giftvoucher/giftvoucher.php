<div class="container">
    <h2><?php echo $this->lang->line('giftvoucher_manager'); ?></h2>
    <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/builder"> <?php echo $this->lang->line('components'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/groups"> <?php echo $this->lang->line('group_component'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/table/index"><?php echo $this->lang->line('table_builder'); ?></a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/language/index"> <?php echo $this->lang->line('language_manager'); ?> </a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/giftvoucher/index"> <?php echo $this->lang->line('giftvoucher_manager'); ?> </a></li>
    </ul>
    
    <div style="height: 52px;">
        <div data-spy="affix" data-offset-top="90" style="top: 24px;width: 100%;padding-top:5px;padding-bottom:5px;z-index: 100;" class="affix-top">
            <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;background: #FBFBFB;background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);margin: 0;">
                <div style="text-align:right;width:100%;">
                    <a href="<?php echo site_url('admin/giftvoucher/add'); ?>" type="button" class="btn btn-info">
                        <i class="icon-plus icon-white"></i> 
                        <?php echo $this->lang->line('add'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <table id="datatb" class="table table-bordered table-hover list table-condensed table-striped">
            <thead>
                <tr>
                    <th width="60"><?php echo $this->lang->line('giftvoucher_table_number'); ?></th>
                    <th><?php echo $this->lang->line('giftvoucher_table_title'); ?></th>
                    <th width="40"><?php echo $this->lang->line('giftvoucher_table_code'); ?></th>
                    <th width="40"><?php echo $this->lang->line('giftvoucher_table_used'); ?></th>
                    <th width="40"><?php echo $this->lang->line('giftvoucher_table_remain'); ?></th>
                    <th width="80"><?php echo $this->lang->line('giftvoucher_table_active'); ?></th>
                    <th width="80"><?php echo $this->lang->line('giftvoucher_table_exp'); ?></th>
                    <th width="110"><?php echo $this->lang->line('actions'); ?></th>
                </tr>
            </thead>
            <?php if($query){ ?>
            <tbody>
                <?php foreach($query as $key => $val){ ?>
                <tr>
                    <td style="text-align: right;"><?php echo ($key+1); ?></td>
                    <td><?php echo $val['name_giftopt']; ?></td>
                    <td style="text-align: right;"><?php echo number_format($val['code_qty_giftopt']); ?></td>
                    <td style="text-align: right;"><?php echo number_format($val['used']); ?></td>
                    <td style="text-align: right;"><?php echo number_format($val['code_qty_giftopt']-$val['used']); ?></td>
                    <td><?php echo date('d-m-Y',strtotime($val['start_date_giftopt'])); ?></td>
                    <td>
                        <?php if(strtotime($val['end_date_giftopt']) >= time()){ ?>
                        <span class="label label-success"><?php echo date('d-m-Y',strtotime($val['end_date_giftopt'])); ?></span>
                        <?php }else{ ?>
                        <span class="label label-important"><?php echo date('d-m-Y',strtotime($val['end_date_giftopt'])); ?></span>
                        <?php } ?>
                    </td>
                    <td style="text-align: center;">
                        <a type="button" href="<?php echo site_url('admin/giftvoucher/detail/?id='.$val['id_giftopt']); ?>" class="btn btn-mini">
                            <?php echo $this->lang->line('view'); ?>
                        </a>
                        <a type="button" href="<?php echo site_url('admin/giftvoucher/edit/?id='.$val['id_giftopt']); ?>" class="btn btn-mini btn-info">
                            <?php echo $this->lang->line('edit'); ?>
                        </a>
                        <a type="button" href="<?php echo site_url('admin/giftvoucher/deleteGiftOpt/?id='.$val['id_giftopt']); ?>" onclick="return confirm('Delete this Gift Option?');" class="btn btn-mini btn-danger">
                            <?php echo $this->lang->line('delete'); ?>
                        </a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            <?php } ?>
        </table>
    </div>
    
    <script language="javascript" type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.10.2.min.js');?>"></script>
    <script language="javascript" type="text/javascript" src="<?php echo site_url('assets/js/jquery.dataTables.js');?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/demo_table_jui.css'); ?>">
    
    <script language="javascript" type="text/javascript">
    $(function(){
        $('#datatb').dataTable({
            'order': [[0,'asc']],
            'aoColumns':[
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                 {
                    'bSortable': false
                }
            ]
        });
        
        $('title').html($('h2').html());
    });
    </script>
</div>