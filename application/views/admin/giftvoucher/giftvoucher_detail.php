<script language="javascript" type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.10.2.min.js');?>"></script>
<script language="javascript" type="text/javascript" src="<?php echo site_url('assets/js/jquery.dataTables.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/demo_table_jui.css'); ?>">

<div class="container">
    <h2><?php echo $this->lang->line('giftvoucher_manager'); ?>: <?php echo $this->lang->line('giftvoucher_detail_title'); ?></h2>
    <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/builder"> <?php echo $this->lang->line('components'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/component/groups"> <?php echo $this->lang->line('group_component'); ?> </a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/table/index"><?php echo $this->lang->line('table_builder'); ?></a></li>
        <li><a href="<?php echo base_url(); ?>index.php/admin/language/index"> <?php echo $this->lang->line('language_manager'); ?> </a></li>
        <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/giftvoucher/index"> <?php echo $this->lang->line('giftvoucher_manager'); ?> </a></li>
    </ul>
    
    <div style="height: 52px;">
        <div data-spy="affix" data-offset-top="90" style="top: 24px;width: 100%;padding-top:5px;padding-bottom:5px;z-index: 100;" class="affix-top">
            <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;background: #FBFBFB;background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);margin: 0;">
                <div style="text-align:right;width:100%;">
                    <a href="#myModal" type="button" class="btn btn-info" data-toggle="modal">
                        <i class="icon-plus icon-white"></i> 
                        <?php echo $this->lang->line('giftvoucher_gen_title'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="span6">
                <div class="x-table well " style="background:#FBFBFB;">
                    <form class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_name'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo $queryGiftOption[0]['name_giftopt']; ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_cmd'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo $queryGiftOption[0]['cmd_giftopt']!=''?$queryGiftOption[0]['cmd_giftopt']:'-'; ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_amt'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo $queryGiftOption[0]['currency_giftopt']!='%'?$queryGiftOption[0]['currency_giftopt']:''; ?><?php echo number_format($queryGiftOption[0]['amt_giftopt']); ?><?php echo $queryGiftOption[0]['currency_giftopt']=='%'?$queryGiftOption[0]['currency_giftopt']:''; ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_qty'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo number_format($queryGiftOption[0]['code_qty_giftopt']); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_table_used'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo number_format($queryGiftOption[0]['used']); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_table_remain'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php
                                $totalQty = $queryGiftOption[0]['code_qty_giftopt'] - $queryGiftOption[0]['used'];
                                ?>
                                <?php echo number_format($totalQty); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_start'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo date('d-m-Y',strtotime($queryGiftOption[0]['start_date_giftopt'])); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_exp'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo date('d-m-Y',strtotime($queryGiftOption[0]['end_date_giftopt'])); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('status'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;text-transform: capitalize;">
                                <?php if($queryGiftOption[0]['status_giftopt'] == 'active'){ ?>
                                <span class="label label-success"><?php echo $queryGiftOption[0]['status_giftopt']; ?></span>
                                <?php }else{ ?>
                                <span class="label label-important"><?php echo $queryGiftOption[0]['status_giftopt']; ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">
                                <b><?php echo $this->lang->line('giftvoucher_opt_add_created'); ?></b>
                            </label>
                            <div class="controls" style="padding-top:5px;">
                                <?php echo date('d-m-Y H:i:s', strtotime($queryGiftOption[0]['created_date_giftopt'])); ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="span6">
                <table id="datatb" class="table table-bordered table-hover list table-condensed table-striped">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('giftvoucher_code_id'); ?></th>
                            <th><?php echo $this->lang->line('giftvoucher_opt_add_start'); ?></th>
                            <th><?php echo $this->lang->line('giftvoucher_opt_add_exp'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('actions'); ?></th>
                        </tr>
                    </thead>
                    <?php if(!empty($codeList)){ ?>
                    <tbody>
                        <?php foreach($codeList as $key => $val){ ?>
                        <tr>
                            <td><?php echo $val['id_giftcode']; ?></td>
                            <td><?php echo date('d-m-Y',strtotime($val['start_date_giftcode'])); ?></td>
                            <td><?php echo date('d-m-Y',strtotime($val['end_date_giftcode'])); ?></td>
                            <td>
                                <div style="text-transform: capitalize;">
                                    <?php if($val['status_giftcode'] == 'active'){ ?>
                                    <span class="label label-success"><?php echo $val['status_giftcode']; ?></span>
                                    <?php }else{ ?>
                                    <span class="label label-important"><?php echo $val['status_giftcode']; ?></span>
                                    <?php } ?>
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <a type="button" href="<?php echo site_url('admin/giftvoucher/deleteCode/?id='.$val['id_giftcode']); ?>" onclick="return confirm('Delete this gift code?');" class="btn btn-mini btn-danger">
                                    <?php echo $this->lang->line('delete'); ?>
                                </a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>


<div id="myModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo $this->lang->line('giftvoucher_gen_title'); ?></h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
        <div class="control-group">
            <label class="control-label">
                <b><?php echo $this->lang->line('giftvoucher_opt_add_qty'); ?></b>
            </label>
            <div class="controls">
                <select name="gift_qty" class="input-mini">
                    <?php
                    if($totalQty > 0){
                    for($i=1;$i<=$totalQty;$i++){
                    ?>
                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php }} ?>
                </select>
            </div>
        </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-ajaxSave" data-id="<?php echo $queryGiftOption[0]['id_giftopt']; ?>">
            <?php echo $this->lang->line('save_change'); ?>
        </button>
    </div>
</div>

<script language="javascript">
    var ajaxVar;
    $(function(){
        $('#datatb').dataTable();
        
        $('.btn-ajaxSave').click(function(){
            var id = $(this).attr('data-id');
            var itemValue = $('select[name="gift_qty"]').val();
            
            $(this).attr('disabled',true);
            ajaxVar = $.ajax({
                type: 'POST',
                data: $.param({
                    cmd: 'giftcode',
                    id: id,
                    value: itemValue
                }),
                url: "<?php echo site_url('admin/giftvoucher/generate/'); ?>",
                beforeSend: function(){
                    try{
                        ajaxVar.abort();
                    }catch(Exception){}
                },
                success: function(xhr){
                    if(xhr == 'true')
                    {
                        window.location.reload();
                    }
                }
            });
        });
        
        $('title').html($('h2').html());
    });
    
    $(window).load(function(){
        $('select[name="datatb_length"]').addClass('input-small');
    });
</script>