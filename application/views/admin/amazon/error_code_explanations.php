<?php $AUTH = $this->session->userdata('AUTH'); ?>

<div class="container">
    
    <h2><?php echo $this->lang->line('error_code_explanations');?></h2>    
    
    <?php echo $content; ?>
    
</div>