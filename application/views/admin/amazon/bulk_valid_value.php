<div class="hero-unit"> 
    <div class="container">
        <div class="row about">
            <div class="main-content">


                <div class="page-content" style="text-align:left">

                    <div class="page-header position-relative">
                        <h2>
                            Amazon bulk valid values
                        </h2>
                    </div>            

                    <?php if( isset($message)){?><pre><?php echo $message; ?></pre><?php } ?>
                    
                    <div class="x-table well  " style="background:#FBFBFB;"> 
                        <ul class="">
                            <li>
                                <div style="margin:5px; margin-bottom: 15px">
                                    <button id="runFlatFile" class="btn btn-default" type="button" onclick="runFlatFile()">
                                        update valid value
                                    </button>
                                    <p>Insert flat.file.csv into amazon_valid_value database</p>
                                </div>
                            </li>
                            <!-- 
                                <li>
                                    <div style="margin:5px; margin-bottom: 15px">
                                        <a id="dumpAmazonValidValue" class="btn btn-default" href="dumpValidValue">
                                            Dump amazon_valid_value table
                                        </a>                           
                                    </div>
                                </li>
                             -->
                            <li>
                                <div style="margin:5px; margin-bottom: 15px">
                                    <button id="updateFieldSettings" class="btn btn-default" type="button" onclick="updateFieldSettings()">
                                        update Field Settings
                                    </button>
                                    <p>Download Fields Settings From https://dl.dropboxusercontent.com/u/53469716/amazon/</p>
                                </div>
                            </li>
                            <li>
                                <div style="margin:5px; margin-bottom: 15px">
                                    <button id="downloadFlatFile" class="btn btn-default" type="button" onclick="downloadFlatFile()">
                                        Download Flat File
                                    </button>
                                    <p>Download Flat File From https://s3.amazonaws.com/seller-templates</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div><!--page-content-->

            </div><!--main-content-->
        </div>
    </div>
</div>

<script>
    var baseurl = '<?php echo $url ; ?>';   
    function runFlatFile() {
        $('#runFlatFile').parent().append('<span>Start running..</span>');
        $.ajax({
            url: baseurl + '/amazon/excutefile/validvalue',
            dataType: "json",  
            success: function(data){
                console.log(data);
                if(data.pass == true){
                    $('#runFlatFile').parent().find('span').remove();
                    $('#runFlatFile').parent().append('<span>Success</span>');
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                }
            }
        });
    }    
    function updateFieldSettings() {
        $('#updateFieldSettings').parent().append('<span>Start running..</span>');
        $.ajax({
            url: baseurl + '/amazon/update/validvalue',
            dataType: "json",          
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                }
            }
        });
    }    
    function dumpAmazonValidValue() {
        $('#updateFieldSettings').parent().append('<span>Start running..</span>');
        $.ajax({
            url: baseurl + '/amazon/update/validvalue',
            dataType: "json",          
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                }
            }
        });
    }    
    function downloadFlatFile() {
        $('#downloadFlatFile').parent().append('<span>Start running..</span>');
        $.ajax({
            url: baseurl + '/amazon/download/flatfile',
            dataType: "json",          
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
                if (xhr.responseText) {
                    console.log('Progress status error :', xhr.responseText);
                }
            }
        });
    }    
    
//    $(document).ready(function(){
//        $('#dumpAmazonValidValue').
//    });
</script>