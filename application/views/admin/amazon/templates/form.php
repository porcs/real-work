<?php
$CI = & get_instance();
$key = $CI->input->post('key');
$lang = $CI->lang;
?>

</div>
<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="
         top: 24px;
         width: 100%;
         padding-top:5px;
         padding-bottom:5px;
         z-index: 100;">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;
             background: #FBFBFB;
             background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
            <div style="text-align:right;width:100%;">
                <a class="btn"  onclick="crudCancel();">  &nbsp; <?php echo $lang->line('cancel'); ?>  &nbsp; </a>
                <a class="btn btn-info" onclick="crudConfirm();" > &nbsp;  <i class="icon-edit icon-white"></i>  <?php echo $lang->line('confirm'); ?> &nbsp; </a>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class='x-table well  <?php echo $this->conf['color']; ?>' style="background:#FBFBFB;">
        <?php
        $q = $this->queryString;
        $q['xtype'] = 'confirm';
        if (isset($q['key']))
            unset($q['key']);
        ?>
        <form method="post" action="?<?php echo http_build_query($q, '', '&'); ?>"  enctype="multipart/form-data"
              id="crudForm" style="padding: 0; margin: 0;" <?php if ($this->frmType == '2') { ?>class="form-horizontal"<?php } ?>>
                  <?php
                  $elements = $this->form;
                  foreach ($this->primaryKey as $f) {
                      $ary = explode('.', $f);
                      if (isset($_GET['key'][$f]) || isset($key[$ary[0]][$ary[1]])) {
                          if (isset($_GET['key'][$f])) {
                              $_POST['key'][$ary[0]][$ary[1]] = $_GET['key'][$f];
                          }
                          echo __hidden('key.' . $f);
                      }
                  }
                  ?>
                  <?php if (!empty($this->errors)) { ?>
                <div class="alert alert-error">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php foreach ($this->errors as $error) { ?>
                        <?php if (count($error) > 0) { ?>
                            <strong>Error!</strong>
                            <?php echo implode('<br />', $error); ?>
                            <br />
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php
            if (!empty($elements)) {
                foreach ($elements as $field => $v) {
                    if (empty($v['element']))
                        continue;
                    ?>
                    <div class="control-group <?php if (array_key_exists($field, $this->errors)) { ?> error <?php } ?>">
                        <label for="crudRowsPerPage" class="control-label"><b><?php echo (!empty($v['alias'])) ? $v['alias'] : $field; ?>
                                <?php if (array_key_exists($field, $this->validate)) { ?><b
                                        style="color: red;">*</b> <?php } ?>
                            </b> </label>
                        <div class="controls">
                            <?php
                            $e = $v['element'];
                            if (!empty($e) && isset($e[0])) {
                                switch (strtolower(trim($e[0]))) {
                                    case 'image':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        $attributes['style'] = 'display:none;';
                                        echo __image('data.' . $field, $attributes);
                                        break;
                                    case 'text':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __text('data.' . $field, $attributes);
                                        break;
                                    case 'date':
                                        $attributes = array();
                                        $attributes['style'] = "width:180px;";
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __date('data.' . $field, $attributes);
                                        break;
                                    case 'datetime':
                                        $attributes = array();
                                        $attributes['style'] = "width:180px;";
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __datetime('data.' . $field, $attributes);
                                        break;
                                    case 'textarea':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __textarea('data.' . $field, $attributes);
                                        break;
                                    case 'editor':
                                        $attributes = array();
                                        $attributes['style'] = 'height:300px;';
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __editor('data.' . $field, $attributes);
                                        break;
                                    case 'hidden':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __hidden('data.' . $field, $attributes);
                                        break;
                                    case 'radio':
                                        $options = array();
                                        $params = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            if (array_key_exists('option_table', $e[1])) {
                                                if (array_key_exists('option_key', $e[1]) &&
                                                        array_key_exists('option_value', $e[1])) {
                                                    $_dao = new ScrudDao($e[1]['option_table'], $this->da);
                                                    $params['fields'] = array($e[1]['option_key'], $e[1]['option_value']);
                                                    $rs = $_dao->find($params);
                                                    if (!empty($rs)) {
                                                        foreach ($rs as $v) {
                                                            $options[$v[$e[1]['option_key']]] = $v[$e[1]['option_value']];
                                                        }
                                                    }
                                                }
                                            } else {
                                                $options = $e[1];
                                            }
                                        }
                                        $attributes = array();
                                        if (isset($e[2]) && !empty($e[2])) {
                                            $attributes = $e[2];
                                        }
                                        echo __radio('data.' . $field, $options, $attributes);
                                        break;
                                    case 'checkbox':
                                        $options = array();
                                        $params = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $options = $e[1];
                                        } else {
                                            $e[1] = array(1 => 'Yes');
                                            $options = $e[1];
                                        }
                                        $attributes = array();
                                        if (isset($e[2]) && !empty($e[2])) {
                                            $attributes = $e[2];
                                        }
                                        echo __checkbox('data.' . $field, $options, $attributes);
                                        break;
                                    case 'password':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __password('data.' . $field, $attributes);
                                        break;
                                    case 'file':
                                        $attributes = array();
                                        $attributes['style'] = 'display:none;';
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __file('data.' . $field, $attributes);
                                        break;
                                    case 'select':
                                        $options = array();
                                        $params = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            if (array_key_exists('option_table', $e[1])) {
                                                if (array_key_exists('option_key', $e[1]) &&
                                                        array_key_exists('option_value', $e[1])) {
                                                    $_dao = new ScrudDao($e[1]['option_table'], $this->da);
                                                    $params['fields'] = array($e[1]['option_key'], $e[1]['option_value']);
                                                    $rs = $_dao->find($params);
                                                    if (!empty($rs)) {
                                                        foreach ($rs as $v) {
                                                            $options[$v[$e[1]['option_key']]] = $v[$e[1]['option_value']];
                                                        }
                                                    }
                                                }
                                            } else {
                                                $options = $e[1];
                                            }
                                        }
                                        $attributes = array();
                                        if (isset($e[2]) && !empty($e[2])) {
                                            $attributes = $e[2];
                                        }
                                        echo __select('data.' . $field, $options, $attributes);
                                        break;
                                    case 'autocomplete':
                                        $options = array();
                                        $params = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            if (array_key_exists('option_table', $e[1])) {
                                                if (array_key_exists('option_key', $e[1]) &&
                                                        array_key_exists('option_value', $e[1])) {
                                                    $_dao = new ScrudDao($e[1]['option_table'], $this->da);
                                                    $params['fields'] = array($e[1]['option_key'], $e[1]['option_value']);
                                                    $rs = $_dao->find($params);
                                                    if (!empty($rs)) {
                                                        foreach ($rs as $v) {
                                                            $options[$v[$e[1]['option_key']]] = $v[$e[1]['option_value']];
                                                        }
                                                    }
                                                }
                                            } else {
                                                $options = $e[1];
                                            }
                                        }
                                        $attributes = array();
                                        if (isset($e[2]) && !empty($e[2])) {
                                            $attributes = $e[2];
                                        }
                                        echo __autocomplete('data.' . $field, $options, $attributes);
                                        break;
                                    case 'button':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __button($attributes);
                                        break;
                                    case 'submit':
                                        $attributes = array();
                                        if (isset($e[1]) && !empty($e[1])) {
                                            $attributes = $e[1];
                                        }
                                        echo __submit($attributes);
                                        break;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </form>
        <script>
            function crudCancel() {
<?php
$q = $this->queryString;
$q['xtype'] = 'index';
if (isset($q['key']))
    unset($q['key']);
?>
                window.location = "?<?php echo http_build_query($q, '', '&'); ?>";
            }

            function crudConfirm() {
                var msg_field = [];
                /*$('input[name="data[amazon_error_resolutions][msg_field]"]').each(function () {
                    msg_field.push($(this).val());
                });*/
                $('input[name="msg_field"]').each(function () {
                    msg_field.push($(this).val());
                });
                /*$('input[name="data[amazon_error_resolutions][msg_field]"]').parents('.control-group').append('<input type="hidden" class="mergn_field" />');
                $('.mergn_field').attr('value', msg_field);*/
                //$('input[name="data[amazon_error_resolutions][msg_field]"]').attr('value', msg_field);
                //$('input[name="data[amazon_error_resolutions][msg_field]"]:last').val(msg_field);
                $('input[name="data[amazon_error_resolutions][msg_field]"]').val(msg_field);
                $('#crudForm').submit();
            }
            $(document).ready(function () {
                $('title').html($('h2').html());
                
                $('#dataAmazon_error_resolutionsMsg_field').after('<i class="icon-plus msg_field_add" style="margin-left: 5px;"></i>').after('<input type="text" name="msg_field" class="msg_field" stype="width:400px" />');

                $('.msg_field_add').on('click', function () {
                    var parent = $(this).parents('.control-group');
                    var lastrow = $('input[name="data[amazon_error_resolutions][msg_field]"]:last').parents('.control-group');
                    var clone = parent.clone().insertAfter(lastrow);
                    clone.find('.control-label').html('');
                    clone.find('input').attr('value', '');
                    clone.find('.icon-plus').addClass('icon-minus').removeClass('icon-plus').on('click', function () {
                        $(this).parents('.control-group').remove();
                    });
                });

                if ($('input[name="data[amazon_error_resolutions][msg_field]"]').val().indexOf(',') > 0 || $('input[name="msg_field"]').val().indexOf(',') > 0) {
                    var check_keyword = $('input[name="data[amazon_error_resolutions][msg_field]"]').val().replace(/, /g ,',');
                    var array_field = check_keyword.split(',');
                    for (a in array_field) {
                        //console.log(array_field[a]);
                        if (a == 0) {
                            //$('#dataAmazon_error_resolutionsMsg_field').attr('value', array_field[a]);
                            $('#dataAmazon_error_resolutionsMsg_field').parents('.control-group').find('input').attr('value', array_field[a]);
                        } else {
                            var parent = $('#dataAmazon_error_resolutionsMsg_field').parents('.control-group');
                            var lastrow = $('input[name="data[amazon_error_resolutions][msg_field]"]:last').parents('.control-group');
                            var clone = parent.clone().insertAfter(lastrow);
                            clone.find('.control-label').html('');
                            clone.find('input').attr('value', array_field[a]);
                            clone.find('.icon-plus').addClass('icon-minus').removeClass('icon-plus').on('click', function () {
                                $(this).parents('.control-group').remove();
                            });
                        }
                    }
                }
            });
        </script>
    </div>
