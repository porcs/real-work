<div class="container">
		<h2><?php echo $this->lang->line('messages'); ?></h2>
        <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
            <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/messages/index"><?php echo $this->lang->line('messages'); ?></a></li>
        </ul>
</div>
<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="
         top: 24px;
         width: 100%;
         padding-top:5px;
         padding-bottom:5px;
         z-index: 100;">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;
        	background: #FBFBFB;
       		background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
       		
            <div style="text-align:right;width:100%;">
                <a class="btn btn-info" href="<?php echo base_url(); ?>index.php/admin/messages/form"><i class="icon-plus icon-white"></i> <?php echo $this->lang->line('add_message'); ?></a>
            </div>
        </div>
    </div>
 </div>
<div class="container">
        <div>
            <table class="table table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th style="text-align:center;width:100px; cursor:default; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;"><?php echo $this->lang->line('no_'); ?></th>
                        <th style=" cursor:default; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;"><?php echo $this->lang->line('messages'); ?></th>
                        <th style=" cursor:default; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;"><?php echo $this->lang->line('language'); ?></th>

                        <th style="text-align:center; width: 120px;  cursor:default; color:#333333;text-shadow: 0 1px 0 #FFFFFF;background-color: #e6e6e6;"><?php echo $this->lang->line('actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($messages) > 0) {
                        foreach ($messages as $k => $message) {
                            $comDao = new ScrudDao('components', $this->db);
                            ?>
                            <tr>
                                <td style="text-align:center;"><?php echo $message['message_code']; ?></td>
                                <td><?php echo $message['message_name']; ?></td>
                                <td><?php echo $message['message_language']; ?></td>
                                <td style="text-align: center;">
                                    <a type="button" class="btn btn-mini btn-info" id="table_btn_fields" onclick="edit_message('<?php echo $message['message_code']; ?>')"><?php echo $this->lang->line('edit'); ?></a>
                                    <a type="button" class="btn btn-mini btn-danger" id="table_btn_delete" onclick="modal_delete_message('<?php echo $message['message_code']; ?>');"><?php echo $this->lang->line('delete'); ?></a>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="4"><?php echo $this->lang->line('no_data_to_display'); ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

</div>
<script>
	function modal_delete_message(message){
		$.sModal({
			image: '<?php echo __MEDIA_PATH__; ?>icons/error.png',
	        animate: 'fadeDown',
	        content:"Are you sure you want to delete <b>"+message+"</b>?",
	        buttons: [
	            {
	                text: ' <i class="icon-remove icon-white"></i>  Delete  ',
	                addClass: 'btn-danger',
	                click: function(id, data) {
	                	delete_message(message);
	                    $.sModal('close', id);
	                }
	            },
	            {
	                text: ' Cancel ',
	                click: function(id, data) {
	                    $.sModal('close', id);
	                }
	            }
	        ]
	    });
	}

	function alert_modal_delete_message(message){
		$.sModal({
			image: '<?php echo __MEDIA_PATH__; ?>icons/error.png',
	        animate: 'fadeDown',
	        content:"you can not delete <b>"+message+"</b> message because there is at least one component  are created from this message.",
	        buttons: [
	            {
	                text: ' Cancel ',
	                click: function(id, data) {
	                    $.sModal('close', id);
	                }
	            }
	        ]
	    });
	}
	
    function edit_message(message){
        window.location = '<?php echo base_url(); ?>index.php/admin/messages/form?message='+message;
    }
    function delete_message(message){
        $.post('<?php echo base_url(); ?>index.php/admin/messages/delete', {message_code:message}, function(data){
            $('#delModal').modal('hide');
            window.location = '<?php echo base_url(); ?>index.php/admin/messages/index';
        },'html');
    }
    $(document).ready(function(){
        $('title').html($('h2').html());
    });
</script>