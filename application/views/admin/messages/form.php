<div id="container" class="container">
	<div class="container">
		<h2><?php echo $this->lang->line('message');?></h2>
        <ul class="nav nav-tabs" id="auth_tab" style="margin-bottom: 0px;">
            <li class="active"><a  style="cursor: pointer;" href="<?php echo base_url(); ?>index.php/admin/messages/index"><?php echo $this->lang->line('messages'); ?></a></li>
        </ul>
</div>
</div>
<div style="height: 52px;">
    <div data-spy="affix" data-offset-top="90" style="
         top: 24px;
         width: 100%;
         padding-top:5px;
         padding-bottom:5px;
         z-index: 100;">
        <div class="container" style="border-bottom: 1px solid #CCC; padding-bottom:5px;padding-top:5px;
        	background: #FBFBFB;
       		background-image: linear-gradient(to bottom, #FFFFFF, #FBFBFB);">
       		
            <div style="text-align:right;width:100%;">
                <button onclick="save_setting();" class="btn btn-primary"
							type="button"><?php echo $this->lang->line('save_change'); ?></button>
                <a type="button" class="btn  btn-danger" id="table_btn_delete" href="<?php echo base_url(); ?>index.php/admin/messages/index" >Back</a>
            </div>
        </div>
    </div>
 </div>
<div  class="container">
<div class="container">		
		
		<div class="row-fluid show-grid">
			<div class="span12">
				<form accept-charset="utf-8" method="post" id="MessageSaveForm" class="form-horizontal">
					<legend><?php echo $this->lang->line('message_info'); ?></legend>
					<div class="control-group">
						<label for="MessageCode" class="control-label"><?php echo $this->lang->line('message_code'); ?>
							<span style="color: red;">*</span>
						</label>
						<div class="controls">
							<?php 
							if (isset($message[$default]['message_code'])){?>
								<input type="text" class="input-xlarge"	placeholder="<?php echo $this->lang->line('message_code'); ?>" name="message_code" value="<?php echo htmlspecialchars($message[$default]['message_code']); ?>" disabled="disabled"/>
								<input type="hidden" name="message_code" value="<?php echo htmlspecialchars($message[$default]['message_code']); ?>"/>
								<input type="hidden" name="action" value="update"/>
							<?php }
							else{?>
								<input type="text" class="input-xlarge"	placeholder="<?php echo $this->lang->line('message_code'); ?>" name="message_code"/>
								<input type="hidden" name="action" value="insert"/>
							<?php }?>							
						</div>
					</div>
					<div class="control-group">
						<label for="SettingDefaultGroup" class="control-label"><?php echo $this->lang->line('message_type'); ?> </label>
						<div class="controls">
							<select name="message_type">
								<option value=""></option>
								<?php if (!empty($types)){?>
								<?php foreach ($types as $k=>$v){?>
									<option value="<?php echo $k; ?>"
									<?php if (isset($message[$default]) && $message[$default]['message_type'] == $k){?>
										selected="selected"
									<?php }?>
									><?php echo htmlspecialchars($v); ?></option>	
								<?php }?>
								<?php }?>
							</select>
						</div>
					</div>
					
<?php  
foreach($languages as $k=>$language)
{        
	$language_id = $k;//$language['id'];
	$language_iso = isset($language['language_code'])?$language['language_code']:$k;
	$language_iso_slit = explode('_', $language_iso);
	$language_code = $k;//$language_iso_slit[0];
?>					<input type="hidden" name="message[<?php echo $language_id;?>]"/>
					<h2><?php echo $language; ?></h2>
					<div class="control-group">
						<label for="message_name" class="control-label"><?php echo $this->lang->line('message_name'); ?>
						</label>
						<div class="controls">
							<input type="text" class="input-xxlarge"	placeholder="<?php echo $this->lang->line('message_name'); ?>" name="message[<?php echo $language_id;?>][message_name]"
							value="<?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_name']) : ''; ?>"/>
						</div>
					</div>
					
					<div class="control-group">
						<label for="message_problem" class="control-label"><?php echo $this->lang->line('message_problem'); ?>
						</label>
						<div class="controls">
							<textarea rows="" cols="" class="input-xxlarge" name="message[<?php echo $language_id;?>][message_problem]"><?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_problem']) : ''; ?></textarea>
							
							<!-- input type="text" class="input-xxlarge"	placeholder="<?php echo $this->lang->line('message_problem'); ?>" name="message[<?php echo $language_id;?>][message_problem]"
							value="<?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_problem']) : ''; ?>"/-->
						</div>
					</div>
					<div class="control-group">
						<label for="message_cause" class="control-label"><?php echo $this->lang->line('message_cause'); ?>
						</label>
						<div class="controls">
							<textarea rows="" cols="" class="input-xxlarge" name="message[<?php echo $language_id;?>][message_cause]"><?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_cause']) : ''; ?></textarea>
							<!-- input type="text" class="input-xxlarge"	placeholder="<?php echo $this->lang->line('message_cause'); ?>" name="message[<?php echo $language_id;?>][message_cause]"
							value="<?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_cause']) : ''; ?>"/-->
						</div>
					</div>
					<div class="control-group">
						<label for="message_solution" class="control-label" ><?php echo $this->lang->line('message_solution'); ?>
						</label>
						<div class="controls">
							<textarea rows="" cols="" class="input-xxlarge" name="message[<?php echo $language_id;?>][message_solution]"><?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_solution']) : ''; ?></textarea>
							<!-- input type="text" class="input-xxlarge"	placeholder="<?php echo $this->lang->line('message_solution'); ?>" name="message[<?php echo $language_id;?>][message_solution]"
							value="<?php echo isset($message[$language_code]) ? htmlspecialchars($message[$language_code]['message_solution']) : ''; ?>"/-->
						</div>
					</div>
<?php 
}
?>
				</form>
			</div>
		</div>
	</div>
<script type="text/javascript">

function save_setting(){
	console.log($('#MessageSaveForm').serialize());
	$.post('<?php echo base_url(); ?>index.php/admin/messages/save',$('#MessageSaveForm').serialize(),function(o){
		if (o.error == 0){
			var strAlertSuccess = '<div class="alert alert-success" style="position: fixed; right:3px; bottom:20px; -webkit-box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; -moz-box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; display: none;">'
				+ '<button data-dismiss="alert" class="close" type="button">×</button>'
				+ '<?php echo $this->lang->line('successfully_changed_setting');?>'
				+ '</div>';
			var alertSuccess = $(strAlertSuccess).appendTo('#container.container');
			alertSuccess.show();
			setTimeout(function() {
				alertSuccess.remove();
				//if($('[name=action]').val() == 'insert'){
					window.location.href='<?php echo base_url(); ?>index.php/admin/messages/form?message='+$('[name=message_code]').val();
				//}
			}, 2000);
		}else if (o.error == 1){
			var strAlertSuccess = '<div class="alert alert-error" style="position: fixed; right:3px; bottom:20px; -webkit-box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; -moz-box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; box-shadow: 0px 1px 0px rgba(255, 255, 255, 0.8) inset; display: none;">'
				+ '<button data-dismiss="alert" class="close" type="button">×</button>'
				+ '<strong><?php echo $this->lang->line('error'); ?>!</strong> '+o.error_message
				+ '</div>';
			var alertSuccess = $(strAlertSuccess).appendTo('body');
			alertSuccess.show();
			setTimeout(function() {
				alertSuccess.remove();
			}, 2000);
		}
	},'json');
}
$(document).ready(function(){
	$('title').html($('h2').html());
});
</script>

</div>
