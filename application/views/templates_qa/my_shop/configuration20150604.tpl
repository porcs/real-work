{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="{if !isset($popup)}main {/if} p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="data_source"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"}
    {if isset($popup)}
        <div class="row">
            <div class="col-xs-12">
                <div class="headSettings clearfix m-b10">
                    <h4 class="headSettings_head">{ci_language line="Module"}/{ci_language line="Extension"}</h4>						
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="engine clearfix">
                    <div class="engine_item">
                        <img class="engine_item-picture" src="{$base_url}assets/images/engine/prestashop.png" alt="">
                        <p class="engine_item-text">{ci_language line="Prestashop"}</p>
                        <a class="btn btn-save" href="/shop_modules/prestashop.zip">{ci_language line="Download"}</a>
                    </div>
                    <div class="engine_item">
                        <img class="engine_item-picture" src="{$base_url}assets/images/engine/shopify.png" alt="">
                        <p class="engine_item-text">{ci_language line="Shopify"}</p>
                        <a class="btn btn-save" target="_blank" href="/shopify/?load=install">{ci_language line="Install"}</a>
                    </div>
                    <div class="engine_item">
                        <img class="engine_item-picture" src="{$base_url}assets/images/engine/opencart.png" alt="">
                        <p class="engine_item-text">{ci_language line="Opencart"}</p>
                        <a class="btn btn-save" href="/shop_modules/opencart.rar">{ci_language line="Download"}</a>
                    </div>
                    <div class="engine_item">
                        <img class="engine_item-picture" src="{$base_url}assets/images/engine/magento.png" alt="">
                        <p class="engine_item-text">{ci_language line="Magento"}</p>
                        <a class="btn btn-save" href="/shop_modules/magento.tgz">{ci_language line="Download"}</a>
                    </div>
                    <div class="engine_item">
                        <img class="engine_item-picture" src="{$base_url}assets/images/engine/woo.png" alt="">
                        <p class="engine_item-text">{ci_language line="Woo Commerce"}</p>
                        <a class="btn btn-save" href="/shop_modules/woocommerce.zip">{ci_language line="Download"}</a>
                    </div>
                </div>
            </div>
        </div>
    {/if}
    <form action="{$base_url}my_shop/connect_save" method="post" id="mainconfigform" >
        <div class="row">
            <div class="col-xs-12">
                <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
                    <h4 class="headSettings_head">{ci_language line="Feed Data"}</h4>						
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 m-b10">
                <p class="regRoboto poor-gray">{ci_language line="URL provided by your Feed.biz module"}</p>
                <div class="form-group">
                    <input type="text" class="form-control" id="base_url" name="base_url" value="{if isset($feed_biz.base_url)}{$feed_biz.base_url}{/if}">
                    <input type="hidden" name="verified" value="{if isset($feed_biz.verified)}{$feed_biz.verified}{/if}">
                </div>
                <button type="button" class="btn btn-verify btn_ver {if isset($feed_biz.verified) && $feed_biz.verified!=""} btn-success verified{/if}">
                    <i class="icon_ver {if isset($feed_biz.verified)&&$feed_biz.verified!=""}fa fa-check{else}fa fa-globe{/if}"></i>
                    <i class="icon_chk fa fa-spinner fa-spin" class="icon_chk" style="display:none"></i>
                    <span class="txt_ver">
                        {if isset($feed_biz.verified)&&$feed_biz.verified!=""}
                            {ci_language line="Verified"}
                        {else}
                            {ci_language line="Verify"}
                        {/if}
                    </span>
                </button>
                {if isset($feed_biz.verified)&&$feed_biz.verified=="verified"}
                    <button type="button" class="btn btn-save btn_import_product btn_import pull-right" rel="feed">{ci_language line="Import my catalog"}</button>
                {/if}

            </div>
            {if isset($feed_mode) && $feed_mode == 1 && !isset($popup)}
                <div class="col-xs-12">
                    <div class="form-group b-Top p-t10">
                        <button class="btn btn-save pull-right" type="submit" id="save-button">{ci_language line="save_feed_data"}</button>
                    </div>
                </div>
            {/if}
        </div>

        {if !isset($popup) || !$popup}
            {if isset($feed_mode) && $feed_mode != 1}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Import mode"}</h4>						
                        </div>
                    </div>
                </div>
                <div class="row custom-form">
                    <div class="col-sm-4">
                        <label class="cb-radio m-t10 col-xs-12">
                            <input name="mode" type="radio" value="1" {if isset($status) && $status == 1}checked="checked" rel="checked"{/if} />
                            <p class="text-uc dark-gray montserrat m-b0 bold">{ci_language line="Auto Importing"}
                                <span class="recommended">{ci_language line="Recommend"}</span>
                            </p>  
                            <p class="light-gray m-l25">{ci_language line="Feed.biz will periodically update your products"}</p>
                        </label>
                    </div>
                    <div class="col-sm-5">
                        <label class="cb-radio m-t10 col-xs-12">
                            <input name="mode" type="radio" value="0" {if isset($status) && $status == 0}checked="checked" rel="checked"{/if} />
                            <p class="text-uc dark-gray montserrat m-b0 bold">{ci_language line="Manual Importing"}</p>  
                            <p class="light-gray m-l25">{ci_language line="Your products will be updated after clicking the save button."}</p>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="m-t10 btn btn-save" type="submit" id="save-button">{ci_language line="save_feed_data"}</button>
                        </div>
                    </div>

                </div>
            {/if}
        {/if}
    </form>
    <div class="col-sm-3">
        <div class="pull-right">
            <input type="hidden" id="verifying" value="{ci_language line="please_wait_for_verifying"}..." >
            <input type="hidden" id="verify" value="{ci_language line="verify"}" >
            <input type="hidden" id="check" err_code="11-00003" value="{ci_language line="please_check_url_and_verify_again"}." >
            <input type="hidden" id="error_connection" err_code="11-00004" value="{ci_language line="can_not_connect_server"}." >
            <input type="hidden" id="empty_url" value="{ci_language line="please_paste_your_uri_connector"}.">
            <input type="hidden" class="cus_msg" err_code="11-00004"  rel="Access Denied." value="{ci_language line="Access Denied."}">
            <input type="hidden" id="can_not_load_setting_data" value="{ci_language line="System can not download data. Please try again"}.">
            <input type="hidden" id="can_not_load_data" value="{ci_language line="System can not download data. Please try again"}.">
            <input type="hidden" id="duplicate_load_data" value="{ci_language line="System can not download some part of your data."}.">

            <input type="hidden" id="empty_data" value="{ci_language line="Empty Data"}.">
            <input type="hidden" id="dom_error" value="{ci_language line="XML DOM Parse Error"}.">
            <input type="hidden" id="setting_dom_error" value="{ci_language line="Setting Feed Error"}.">

            <input id="status-confirm" type="hidden" value="{ci_language line="Are you sure to change mode"}?" />
            <input id="status-message" type="hidden" value="{ci_language line='Please choose mode'}" />
            <input id="all-process-complete" type="hidden" value="{ci_language line='All importing are completed.'}" />
            <input id="some-process-running" type="hidden" value="{ci_language line='Your process are running. Please wait.'}" />

        </div>
    </div>
</div>

<!--inline scripts related to this page-->
<script src="{$base_url}assets/js/FeedBiz/my_feeds/configuration/data_source.js"></script>
<script src="{$base_url}assets/js/FeedBiz/my_feeds/configuration/data_feed.js"></script>

{include file="footer.tpl"}