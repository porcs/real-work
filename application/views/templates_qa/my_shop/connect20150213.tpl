{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css2/admin/module.admin.page.tables.min.css" />
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/my_shop/connect.css" />
<div {if !isset($popup)}id="content"{else} class="row"{/if}>
    <div class="heading-buttons bg-white border-bottom innerAll">
        <h1 class="content-heading padding-none pull-left {if isset($popup)}margin-none{/if}">{ci_language line="data_source"}</h1>
        <div class="clearfix"></div>
    </div>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"}
    <!-- Tabs -->
    <div class="innerAll spacing-x2" >
        
        <!-- Tab Download -->
        <div class="widget widget-inverse">
            <div class="widget-head">
                <h4 class="heading"><i class="fa fa-download"></i> {ci_language line="Module"} / {ci_language line="Extension"}</h4>
            </div>
            <div class="widget-body text-center">
                <div class="text-center separator bottom ui">	
                    <div class="row">
                        <div class="col-md-4 form-group">			
                            <div class="innerAll">
                                <div class="col-xs-6 text-right">
                                    <div class="simg" id="presta"  ></div> 
                                </div>
                                <div class="col-xs-6 text-left">
                                    <h5 class="strong">{ci_language line="Prestashop"}</h5>
                                    <a class="btn btn-primary btn-stroke" href="/shop_modules/prestashop.zip">{ci_language line="Download"}</a>
                                </div>				
                            </div>
                        </div>
                        <div class="col-md-4 form-group">			
                            <div class="innerAll">
                                <div class="col-xs-6 text-right">
                                    <div class="simg" id="opencart"  ></div>  
                                </div>
                                <div class="col-xs-6 text-left">
                                    <h5 class="strong">{ci_language line="Opencart"}</h5>
                                    <a class="btn btn-primary btn-stroke" href="/shop_modules/opencart.rar">{ci_language line="Download"}</a>
                                </div>				
                            </div>
                        </div>
                        <div class="col-md-4 form-group">			
                            <div class="innerAll">
                                <div class="col-xs-6 text-right">
                                    <div class="simg" id="magento" ></div>  
                                </div>
                                <div class="col-xs-6 text-left">
                                    <h5 class="strong">{ci_language line="Magento"}</h5>
                                    <a class="btn btn-primary btn-stroke" href="/shop_modules/magento.tgz">{ci_language line="Download"}</a>
                                </div>				
                            </div>
                        </div>
                        <div class="col-md-4 form-group">			
                            <div class="innerAll">
                                <div class="col-xs-6 text-right"> 
                                    <div class="simg" id="shopify"  ></div>  
                                </div>
                                <div class="col-xs-6 text-left">
                                    <h5 class="strong">{ci_language line="Shopify"}</h5>
                                    <a class="btn btn-primary btn-stroke" target="_blank" href="/shopify/?load=install">{ci_language line="Install"}</a>
                                </div>				
                            </div>
                        </div>
                        <div class="col-md-4 form-group">			
                            <div class="innerAll">
                                <div class="col-xs-6 text-right">
                                    <div class="simg" id="woo"   ></div>   
                                </div>
                                <div class="col-xs-6 text-left">
                                    <h5 class="strong">{ci_language line="Woo Commerce"}</h5>
                                    <a class="btn btn-primary btn-stroke" href="/shop_modules/woocommerce.zip">{ci_language line="Download"}</a>
                                </div>				
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        
        
        
        <form action="{$base_url}my_shop/connect_save" method="post" id="mainconfigform" >
            {if !isset($popup) || !$popup}<div class="widget widget-tabs widget-tabs-responsive">{/if}

                {if !isset($popup) || !$popup}
                    <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-rss"></i> {ci_language line="Feed Data"}</a></li>
                                    {if $feed_mode != 1}
                                    <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-cloud-download"></i> {ci_language line="Import mode"}</a></li>
                                    {/if}
                                 
                            </ul>
                    </div><!-- Tabs Heading -->
                {/if}
                
                <div class="widget-body">
                    {if !isset($popup) || !$popup} 
                        {if !isset($feed_biz) || empty($feed_biz) }
                            <div class="alert alert-block alert-warning">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <i class="fa fa-warning-sign"></i>
                                <b>{ci_language line="Warning"} : </b> {ci_language line="No data source."}
                            </div>
                        {/if} 
                    {/if}

                    <div class="tab-content">                        
                        <div class="tab-pane active" id="tab-1">
                            <div class="tab-content  profile-edit-tab-content overflow-hidden">
                                <div id="tab_feed" class="tab-pane in active">                                    
                                    <div class="widget-box  " id="standard">
                                         

                                        <div class="widget-body " >
                                            <div class="widget-body-inner">
                                            
                                                <div class="widget-main form-horizontal" id="standard_source">
                                                    
                                                    <div id="feed_url" class="">
                                                        <h5 class="bolder">{ci_language line="URL provided by your Feed.biz module"}</h5>
                                                        <div class="row-fluid">
                                                            <div class="form-group margin-right-none margin-left-none">
                                                                <input type="text" class="form-control valid" id="base_url" name="base_url" value="{if    isset($feed_biz.base_url)}{$feed_biz.base_url}{/if}">
                                                                <input type="hidden" name="verified" value="{if isset($feed_biz.verified)}{$feed_biz.verified}{/if}">
                                                            </div>
                                                            <div class="form-group margin-right-none margin-left-none">
                                                                <div class="pull-left" >
                                                                    <button type="button" class="btn_ver btn btn-stroke{if isset($feed_biz.verified) && $feed_biz.verified!=""} btn-success verified{else}btn-warning{/if}">
                                                                        <i class="icon_ver {if isset($feed_biz.verified)&&$feed_biz.verified!=""}fa fa-check{else}fa fa-globe{/if}"></i>
                                                                        <i class="icon_chk fa fa-spinner fa-spin orange" class="icon_chk" style="display:none"></i>
                                                                        <span class="txt_ver">
                                                                            {if isset($feed_biz.verified)&&$feed_biz.verified!=""}
                                                                                {ci_language line="Verified"}
                                                                            {else}
                                                                                {ci_language line="Verify"}
                                                                            {/if}
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                                {if isset($feed_biz.verified) && $feed_biz.verified!=""}
                                                                    {if !isset($popup) || !$popup} 
                                                                        <div class="pull-right">
                                                                            <button type="button" class="btn_import_product btn_import btn btn-success btn-stroke" rel="feed">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import my catalog"}!
                                                                                </span>
                                                                            </button>
                                                                        
                                                                           {* <button type="button" class="btn_import_offer btn_import btn btn-success btn-stroke"  rel="offer">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import offers NOW"}! 
                                                                                </span>
                                                                            </button>*}
                                                                        </div> 
                                                                    {else}
                                                                        <div class="pull-right col-sd-4">
                                                                            <button type="button" class="btn_import_product btn_import btn btn-success btn-stroke" rel="product">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import my catalog"}! 
                                                                                </span>
                                                                            </button>
                                                                        </div> 
                                                                    {/if}                                                                

                                                                {/if}
                                                            </div>

                                                        </div>
                                                    </div>   {*feed_url*}
                                                    <div id="import_feed_msg" class="alert alert-warning" style="display: none"> 
                                                        <strong><i class="fa fa-comments"></i></strong>
                                                        <div class="msg"></div>
                                                    </div>
                                                </div>{*widget-main*}
                                                
                                            </div>{*widget-body-inner*}
                                        </div>{*widget-body*}

                                    </div>{*widget-box*}                                    
                                    
                                     

                                </div>{*tab_feed*}

                            </div>{*tab-content*}

                        </div>

                        {if $feed_mode != 1}
                            <div class="tab-pane" id="tab-2">

                                <div id="feed_mode" class="tab-pane in ">
                                    <div id="mode">
                                        <h4 class="innerAll half heading-buttons border-bottom">
                                            {ci_language line="Import mode"}
                                        </h4>

                                        <!-- Beginner -->
                                        <div class="itemdiv modebox">
                                            <div class="user text-right ">
                                                <label class="radio-custom">
                                                    <i class="fa fa-fw fa-circle-o"></i>
                                                    <input name="mode" class="ace-checkbox-3" type="radio" value="1" {if isset($status) && $status == 1}checked rel="checked"{/if}>
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                            <div class="body">
                                                <div class="name bigger-140 green">
                                                    <span rel="mode">{ci_language line="Auto Importing"}</span>
                                                    <span class="label label-primary">{ci_language line="Recommend"}</span>

                                                </div>
                                                <div class="text">
                                                    <i class="fa fa-quote-left"></i>
                                                    {ci_language line="Our system will offen update your products."}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Expert -->
                                        <div class="itemdiv modebox">
                                            <div class="user text-right ">
                                                <label class="radio-custom">
                                                    <i class="fa fa-fw fa-circle-o"></i>
                                                    <input name="mode" class="ace-checkbox-3" type="radio" value="0" {if isset($status) && $status == 0}checked rel="checked"{/if}>
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                            <div class="body">
                                                <div class="name bigger-140 green">
                                                    <span rel="mode">{ci_language line="Manual Importing"}</span>

                                                </div>
                                                <div class="text">
                                                    <i class="fa fa-quote-left"></i>
                                                    {ci_language line="Your products will be updated after clicking the save button."}
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>{*feed_mode*}
                            </div><!-- Tab content -->
                        {/if}  
 
                    </div><!-- Tab content -->
                    
                    <div class=" row-fluid">
                        <div class="span12 text-right">
                            <input type="hidden" id="verifying" value="{ci_language line="please_wait_for_verifying"}..." >
                            <input type="hidden" id="verify" value="{ci_language line="verify"}" >
                            <input type="hidden" id="check" err_code="11-00003" value="{ci_language line="please_check_url_and_verify_again"}." >
                            <input type="hidden" id="error_connection" err_code="11-00004" value="{ci_language line="can_not_connect_server"}." >
                            <input type="hidden" id="empty_url" value="{ci_language line="please_paste_your_uri_connector"}.">
                            
                            <input type="hidden" id="can_not_load_setting_data" value="{ci_language line="System can not download data. Please try again"}.">
                            <input type="hidden" id="can_not_load_data" value="{ci_language line="System can not download data. Please try again"}.">
                            <input type="hidden" id="duplicate_load_data" value="{ci_language line="System can not download some part of your data."}.">
                            
                            <input id="status-confirm" type="hidden" value="{ci_language line="Are you sure to change mode"}?" />
                            <input id="status-message" type="hidden" value="{ci_language line='Please choose mode'}" />
                            <input id="all-process-complete" type="hidden" value="{ci_language line='All importing are completed.'}" />
                            <input id="some-process-running" type="hidden" value="{ci_language line='Your process are running. Please wait.'}" />
                            {if !isset($popup) || !$popup}
                                <button class="btn btn-info" type="submit" id="save-button" style="margin-top: 15px" >
                                    <i class="fa fa-save"></i>
                                    {ci_language line="save_feed_data"}

                                </button>
                            {/if}
                        </div>
                    </div>
                </div><!-- // Tabs Heading END -->

            {if !isset($popup) || !$popup}</div>{/if}
        </form>
    </div>
</div>                   

<input type="hidden" id="userdata" value="{if isset($profile)}{$profile}{/if}" />

{if isset($profile)}
    <script src="{$base_url}assets/js/FeedBiz/users/authenticate.js"></script>
{/if}

<!--page specific plugin scripts-->
<script src="{$base_url}assets/js/jquery.validate.min.js"></script>
<script src="{$base_url}assets/js/bootbox.min.js"></script>
<script src="{$base_url}assets/js/chosen.jquery.min.js"></script> 
<script src="{$base_url}assets/js/jquery.dataTables.min.js"></script>
<script src="{$base_url}assets/js/jquery.dataTables.bootstrap.js"></script>
<script src="{$base_url}assets/js/FeedBiz/my_feeds/configuration/data_feed_status.js"></script>
<!--inline scripts related to this page-->
<script src="{$base_url}assets/js/FeedBiz/my_feeds/configuration/data_source.js"></script>
{*if isset($start_feed) && $start_feed == "true"*}
<script src="{$base_url}assets/js/FeedBiz/my_feeds/configuration/data_feed.js"></script>
{*/if*} 
{if !isset($popup) || !$popup}
    <script src="{$base_url}assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.min.js"></script>
    <script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
    <script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-radio/fuelux-radio.js?v=v1.2.3"></script>
{/if}
<script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-radio/fuelux-radio.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/custom/js/datatables.init.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/classic/assets/js/tables-classic.init.js?v=v1.2.3"></script>

{include file="footer.tpl"}