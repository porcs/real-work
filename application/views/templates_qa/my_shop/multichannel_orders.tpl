{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_shop/multichannel_orders.css" />

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="MultiChannel Orders"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="row">

        <div class="col-xs-12">
            <div id="fba-loader">
                <div class="status row" style="display:none;">
                    <div class="col-xs-12 validate blue">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i> 
                            </div>
                            <div class="validateCell">
                                <p class="pull-left"></p>
                            </div>
                        </div>
                    </div>              
                </div>
            </div>
        </div>

        <div class="col-xs-12">

            <div class="headSettings clearfix">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Orders"}</h4>                      
                </div>
                <div class="pull-sm-right clearfix">
                   <div class="showColumns clearfix">                                
                        <div class="b-Right p-r10">                          
                        </div>
                   </div>
                </div>          
            </div><!--headSettings-->
           
            <div class="row">
                <div class="col-xs-12">
                    <table class="responsive-table" id="my_orders">
                        <thead class="table-head">
                            <tr>                                
                                <th>{ci_language line="ID"}</th>
                                <th>{ci_language line="Sale Order ID"}</th>
                                <th>{ci_language line="Sale Order Ref."}</th>
                                <th>{ci_language line="Sale Channel"}</th>
                                <th>{ci_language line="Sale Language"}</th>
                                <th>{ci_language line="Multi Channel"}</th>
                                <th>{ci_language line="Multi Channel Status"}</th>
                                <th>{ci_language line="Total Quantity"}</th>
                                <th>{ci_language line="Total Paid"}</th>
                                <th>{ci_language line="Order Date"}</th>   
                                <!-- <th>{ci_language line="Automatic Multi-Channel"}</th>  -->                               
                                <th class="text-center" width="10%" >{ci_language line="Ship through FBA Multi-Channel"}</th>                                
                                <th class="text-center" width="10%"></th>                                
                            </tr>
                        </thead>
                    </table>
                    <table class="table tableEmpty">
                        <tr>
                            <td>{ci_language line="No data available in table"}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>			
    </div>

    <!--Domain-->
    {*if isset($domain) && !empty($domain)}
        {foreach $domain as $marketplace_name =>$marketplace}
            {foreach $marketplace as $id => $data}
                <input type="hidden" id="{$marketplace_name}_{$id}" value="{$data}">
            {/foreach}           
        {/foreach}
    {/if*}

</div>

<div class="modal fade" id="infoFbaOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Fulfillment Order Result"}</h1>
                    </div>
                </div>

                <div class="row">                       
                    <div class="col-xs-12 p-0">
                        <div id="viewResult"></div>
                    </div>  
                </div>
             
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>


{include file="amazon/IncludeDataTableLanguage.tpl"}
<input type="hidden" id="processing" value="Processing...">
<input type="hidden" id="cancel_fba" value="{ci_language line="Cancel"}">
<input type="hidden" id="info_fba" value="{ci_language line="View"}">
<input type="hidden" id="create_fba" value="{ci_language line="Ship"}">
<input type="hidden" id="created_fba" value="{ci_language line="Shipped"}">
<input type="hidden" id="create_fba_fail" value="{ci_language line="Ship Automatically is fail"}">
<input type="hidden" id="re_create_fba" value="{ci_language line="Click to ship"}">
<input type="hidden" id="automatically" value="{ci_language line="Automatically"}">

<input type="hidden" id="shipping_fba" value="{ci_language line="Shipping"}">

<!--status-->
<input type="hidden" id="fba_status_submited" value="{$fba_status_submited}">
<input type="hidden" id="fba_status_received" value="{$fba_status_received}">
<input type="hidden" id="fba_status_invalid" value="{$fba_status_invalid}">
<input type="hidden" id="fba_status_planning" value="{$fba_status_planning}">
<input type="hidden" id="fba_status_processing" value="{$fba_status_processing}">
<input type="hidden" id="fba_status_cancelled" value="{$fba_status_cancelled}">
<input type="hidden" id="fba_status_complete" value="{$fba_status_complete}">
<input type="hidden" id="fba_status_completepartialled" value="{$fba_status_completepartialled}">
<input type="hidden" id="fba_status_unfulfillable" value="{$fba_status_unfulfillable}">
<input type="hidden" id="fba_status_fail" value="{ci_language line="Fail"}">

<!--confirm message-->
<input type="hidden" id="confirm_cancel_fba" value="{ci_language line="Submitted"}">
<input type="hidden" id="confirm_cancel_fba_1" value="{ci_language line="Are you sure to want to cancel this Amazon shipping, Order ID : "}">
<input type="hidden" id="confirm_cancel_fba_2" value="{ci_language line="This operation is not reversible.."}">
<input type="hidden" id="confirm-create-message" value="{ci_language line="Are you sure to Ship through FBA Multi-Channel, Order ID : "}">

<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_shop/multichannel_orders.js"></script>
{include file="footer.tpl"}