{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/my_feed/my_products.css" />

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="My products"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings statistics clearfix">	
                <div class="row m-t10">

                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-sm-12 col-md-12" id="my_prod_list_filter_container">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-sm-4 col-md-5">
                                <p class="m-t10 light-gray">{ci_language line="Filter by conditions"}</p>
                            </div>
                            <div class="col-sm-8 col-md-7">
                                <select id="my_prod_list_filter_condition">
                                    <option value="">{ci_language line="All conditions"}</option>
                                    {foreach $shop_condition as $id => $s_cond}
                                        <option value="{$id}" >{ucfirst(strtolower($s_cond['txt']))}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-sm-4 col-md-5">
                                <p class="m-t10 light-gray">{ci_language line="Filter by category"}</p>
                            </div>
                            <div class="col-sm-8 col-md-7">
                                <select id="my_prod_list_filter_category">
                                    <option value="">{ci_language line="All categories"}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="col-sm-4 col-md-5">
                                    <p class="m-t10 light-gray">{ci_language line="Per Page"}</p>
                                </div>
                                <div class="col-sm-8 col-md-7" id="my_prod_list_length_container">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 statistics">
                    <table class=" custom-form" id="my_prod_list">
                        <thead class="table-head">
                            <tr>
                                {*<th class="tableCheckbo" data-th="#: " data-name=""><label class="cb-checkbox"><input id="sel_all" type="checkbox"></label></th>*}
                                <th>{ci_language line="Product ID"}</th>
                                <th>{ci_language line="Image"}</th>
                                <th>{ci_language line="Product name > Product attribute"}</th>

                                <th><table><tr>
                                        
                                <th class="p_ref">{ci_language line="Reference"}</th>
                                <th>{ci_language line="SKU"}</th>
                                <th>{ci_language line="EAN"}</th>
                                <th>{ci_language line="UPC"}</th>
                            </tr></table></th>

                        <th>{ci_language line="Price"}</th>
                        <th>{ci_language line="Quantity"}</th>
                        </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>			
    </div>
</div>

<input type="hidden" id="processing" value="Processing...">

{*<script src="{$base_url}assets/js/jquery.colorbox-m.js"></script>*}
<script language="javascript" src="{$base_url}assets/js/jquery.blockUI.js"></script>
<!--inline scripts related to this page-->
<script src="{$base_url}assets/js/FeedBiz/my_shop/my_products.js"></script>
{include file="footer.tpl"}