<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/component/{str_replace(' ', '_', strtolower($function))}.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/component/includescript.css" />
<div class="row">	
    <div class="col-xs-12">
        <div class="headSettings clearfix b-Top-None">
            <div class="col-xs-8 clearfix">
                <div class="row">
                    <h4 class="col-xs-1 p-0 headSettings_head">{ci_language line="Offers"}</h4>	
                    <div class="col-xs-10 clearfix">
                        <div class="col-xs-5 p-0 "><input id="search_sku_offers" placeholder="{ci_language line="Search: SKU"}" class="form-control" /> </div>
                        <div class="col-xs-5 p-0 m-l5"><input id="search_messgae_code_offers" placeholder="{ci_language line="Search: Message"}" class="form-control" /></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 clearfix">
                <div class="showColumns clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="error_offers" class="amazon-responsive-table">
                    <thead class="table-head">
                        <tr>
                            <th></th>
                            <th>{ci_language line="Code"}</th>
                            <th><span class="p-l20">{ci_language line="Message"}</span></th>
                            <th>{ci_language line="Totals SKU"}</th>		                                       
                        </tr>
                    </thead>							
                </table>
                <table class="table tableEmpty">
                    <tr>
                        <td>{ci_language line="No data available in table"}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="method" value="error_resolutions" />