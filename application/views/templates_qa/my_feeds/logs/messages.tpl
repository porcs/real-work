{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Messages log"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Messages product"}</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							{*<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="Batch ID"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="Product"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Shop"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="Type"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											{ci_language line="Message"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											<i class="fa fa-clock-o"></i> &nbsp; {ci_language line="Date"}
										</label>
									</li>
								</ul>
							</div>*}
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table id="message-products" class="responsive-table">
							<thead class="table-head">
								<tr>
									{*<th class="tableId" rel="id_product">{ci_language line="Batch ID"}</th>*}
									<th class="tableUserName" rel="id_product">{ci_language line="ID"}</th>
									<th class="tableUserName" rel="id_product"><p class="p-l10 m-0">{ci_language line="Product"}</p></th>
									<th class="tableEmail" rel="shop"><p class="p-l10 m-0">{ci_language line="Shop"}</p></th>
									<th class="tableStatus" rel="type"><p class="p-l10 m-0">{ci_language line="Type"}</p></th>
									<th class="tableJoined" rel="message">{ci_language line="Message"}</th>
									<th class="tableSummary" rel="date_add"><i class="fa fa-clock-o"></i> &nbsp; {ci_language line="Date"}</th>
								</tr>
							</thead>
							<tbody>
                                                            {*if isset($product)}
                                                                {foreach from=$product key=key item=value}
                                                                    <tr>
                                                                        <td class="tableId" data-th="{$value.batch_id}:">{$value.batch_id}</td>
                                                                        <td class="tableUserName" data-th="{$value.id_product}:">{$value.id_product}</td>
                                                                        <td class="tableEmail" data-th="{$value.shop}:">{$value.shop}</td>
                                                                        <td class="tableStatus" data-th="{ci_language line="Type"}:">
                                                                            {if $value.severity ==1}
                                                                                <span class="label label-warning">
                                                                                    {ci_language line="Warning"}
                                                                                </span>
                                                                            {else}
                                                                                <span class="label label-important">
                                                                                    {ci_language line="Error"}
                                                                                </span>
                                                                            {/if}
                                                                        </td>
                                                                        <td class="tableJoined" data-th="{ci_language line="Message"}:">{$value.message}</td>
                                                                        <td class="tableSummary" data-th="{ci_language line="Date"}:">{$value.date_add}</td>
                                                                    </tr>
                                                                {/foreach}
                                                           {/if*}			
							</tbody>
						</table>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Messages offer"}</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							{*<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="Batch ID"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="Product"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Shop"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="Type"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											{ci_language line="Message"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											<i class="fa fa-clock-o"></i> &nbsp; {ci_language line="Date"}
										</label>
									</li>
								</ul>
							</div>*}
						</div>
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table id="message-offers" class="responsive-table">
							<thead class="table-head">
								<tr>
									{*<th class="tableId" rel="id_product"><p class="p-l10 m-0">{ci_language line="Batch ID"}</p></th>*}
									<th class="tableUserName" rel="id_product">{ci_language line="ID"}</th>
									<th class="tableUserName" rel="id_product"><p class="p-l10 m-0">{ci_language line="Product"}</p></th>
									<th class="tableEmail" rel="shop"><p class="p-l10 m-0">{ci_language line="Shop"}</p></th>
									<th class="tableStatus" rel="type"><p class="p-l10 m-0">{ci_language line="Type"}</p></th>
									<th class="tableJoined" rel="message">{ci_language line="Message"}</th>
									<th class="tableSummary" rel="date_add"><i class="fa fa-clock-o"></i> &nbsp; {ci_language line="Date"}</th>
								</tr>
							</thead>
							<tbody>
                                                            {*if isset($offer)}
                                                                {foreach from=$offer key=key item=value}
                                                                    <tr>
                                                                        <td class="tableId" data-th="{ci_language line="Batch ID"}:">{$value.batch_id}</td>
                                                                        <td class="tableUserName" data-th="{ci_language line="Product"}:">{$value.id_product}</td>
                                                                        <td class="tableEmail" data-th="{ci_language line="Shop"}:">{$value.shop}</td>
                                                                        <td class="tableStatus" data-th="{ci_language line="Type"}:">
                                                                            {if $value.severity ==1}
                                                                                <span class="label label-warning">
                                                                                    {ci_language line="Warning"}
                                                                                </span>
                                                                            {else}
                                                                                <span class="label label-important">
                                                                                    {ci_language line="Error"}
                                                                                </span>
                                                                            {/if}
                                                                        </td>
                                                                        <td class="tableJoined" data-th="{ci_language line="Message"}">{$value.message}</td>
                                                                        <td class="tableSummary" data-th="{ci_language line="Date"}">{$value.date_add}</td>
                                                                    </tr>
                                                                {/foreach}
                                                           {/if*}
							</tbody>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<input type="hidden" id="type_products" value="{$type_products}" /> 
	<input type="hidden" id="type_offers" value="{$type_offers}" /> 
	<input type="hidden" id="message_Warning" value="{ci_language line="Warning"}" /> 
	<input type="hidden" id="message_Error" value="{ci_language line="Error"}" /> 

	
	
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/logs/messages.js"></script>
{*<script src="{$cdn_url}assets/js/calldatatable.js"></script>*}

{include file="IncludeDataTableLanguage.tpl"}
{include file="footer.tpl"}