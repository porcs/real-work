{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Batch Log"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Last import log"}</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="batch_id" checked/>
											{ci_language line="Batch ID"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="source" checked/>
											{ci_language line="Source"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="shop" checked/>
											{ci_language line="Shop"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="type" checked/>
											{ci_language line="Type"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="no_total" checked/>
											{ci_language line="Total"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="no_success" checked/>
											{ci_language line="No. Successful"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="no_warn" checked/>
											{ci_language line="No. Warning"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="no_error" checked/>
											{ci_language line="No. Error"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="datetime" checked/>
											{ci_language line="Import Date"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="download" checked/>
											{ci_language line="Download"}
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId" rel="batch_id" >{ci_language line="Batch ID"}</th>
									<th class="tableUserName" rel="source" >{ci_language line="Source"}</th>
									<th class="tableEmail" rel="shop" >{ci_language line="Shop"}</th>
									<th class="tableStatus" rel="type" >{ci_language line="Type"}</th>
									<th class="tableJoined" rel="no_total">{ci_language line="Total"}</th>
									<th class="tableSummary" rel="no_success">{ci_language line="No. Successful"}</th>
									<th class="tableError" rel="no_warning">{ci_language line="No. Warning"}</th>
									<th class="tableDate" rel="no_error">{ci_language line="No. Error"}</th>
									<th class="tableDownload" rel="datetime">{ci_language line="Import Date"}</th>
									<th class="tableExported" rel="download">{ci_language line="Download"}</th>
								</tr>
							</thead>
                                                        {*{if isset($logs)}
                                                                <tbody>
                                                                    {foreach from=$logs key=key item=value}
                                                                            <tr>
                                                                                    <td class="tableId" rel="batch_id" data-th="{ci_language line="Batch ID"}:">{$value.batch_id}</td>
                                                                                    <td class="tableUserName" rel="source" data-th="{ci_language line="Source"}:">{$value.source}</td>
                                                                                    <td class="tableEmail" rel="shop" data-th="{ci_language line="Shop"}:">{$value.shop}</td>
                                                                                    <td class="tableStatus" rel="type" data-th="{ci_language line="Type"}:">{$value.type}</td>
                                                                                    <td class="tableJoined" rel="no_total" data-th="{ci_language line="Total"}:">{$value.no_total}</td>
                                                                                    <td class="tableSummary" rel="no_success" data-th="{ci_language line="No. Successful"}:">{$value.no_success}</td>
                                                                                    <td class="tableError" rel="no_warning" data-th="{ci_language line="No. Warning"}:">{$value.no_warning}</td>
                                                                                    <td class="tableDate" rel="no_error" data-th="{ci_language line="No. Error"}:">{$value.no_error}</td>
                                                                                    <td class="tableDownload" rel="datetime" data-th="{ci_language line="Import Date"}:">{$value.datetime}</td>
                                                                                    <td class="tableExported" data-th="{ci_language line="Download"}:">
                                                                                        {if $value.type == "Product"}
                                                                                            <a class="btn btn-primary" href="{$base_url}my_feeds/log_download/products/{$value.batch_id}">
                                                                                        {else if $value.type == "Offer"}
                                                                                            <a class="btn btn-primary" href="{$base_url}my_feeds/log_download/offers/{$value.batch_id}">
                                                                                        {/if}
                                                                                            <i class="fa fa-download"></i>
                                                                                        </a>
                                                                                    </td>
                                                                            </tr>
                                                                    {/foreach}
                                                                </tbody>
                                                        {/if}   *}                                          
                                                </table>
					</div>
				</div>
			</div>			
		</div>
	</div>

{*<script src="{$cdn_url}assets/js/calldatatable.js"></script>*}
{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/logs/batches.js"></script>