{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Import history logs"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Import history"}</h4>						
					</div>
					<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							{*<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="shop name"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="task"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Run by"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="time"}
										</label>
									</li>
								</ul>
							</div>*}
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
                                            <table id="import-history" class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId" rel="history_table_name">{ci_language line="shop_name"}</th>
									<th class="tableUserName" rel="history_action">{ci_language line="task"}</th>
									<th class="tableEmail" rel="user_name">{ci_language line="Run by"}</th>
									<th class="tableStatus" rel="history_date_time">{ci_language line="time"}</th>
								</tr>
							</thead>
                                                                <tbody>
                                                        {*{if $history}
                                                                    {foreach $history as $val}
                                                                        <tr>
                                                                                <td class="tableId" data-th="{ci_language line="shop_name"}:" data-name="">{$val['history_table_name']}</td>
                                                                                <td class="tableUserName" data-th="{ci_language line="task"}:">{str_replace('_',' ',$val['history_action'])}</td>
                                                                                <td class="tableEmail" data-th="{ci_language line="running_by"}:">{($val['user_name'] )}</td>
                                                                                <td class="tableStatus" data-th="{ci_language line="time"}:">{($val['history_date_time'] )}</td>									
                                                                        </tr>
                                                                    {/foreach}	
                                                        {/if}*}
                                                                </tbody>
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>
    <input type="hidden" id="userdata" value="{if isset($profile)}{$profile}{/if}" />
{if isset($profile)}
    <script src="{$cdn_url}assets/js/FeedBiz/users/authenticate.js"></script>
{/if}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
{*<script src="{$cdn_url}assets/js/calldatatable.js"></script>*}
{*<script src="{$cdn_url}assets/js/bootbox.min.js"></script>*}
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/data_feed_status.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/data_source.js"></script>
{include file="IncludeDataTableLanguage.tpl"}
{include file="footer.tpl"}