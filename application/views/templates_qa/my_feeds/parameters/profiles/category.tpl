{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{if isset($popup) || (isset($mode_default) && ($mode_default == 1))}<style> form .treeRight{ display:none;} </style>{/if}
<div class="{if !isset($popup)}main {/if} p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Categories"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                {include file="popup_step.tpl"}
                <form action="{$base_url}profiles/category_save/{if isset($popup)}{$popup}{/if}" method="post" id="profiles_category" autocomplete="off" class="form-horizontal" novalidate>
                {*<div class="row">
                    <div class="col-xs-12">
                            <div class="validate blue">
                                    <div class="validateRow">
                                            <div class="validateCell">
                                                    <i class="note"></i>
                                            </div>
                                            <div class="validateCell">
                                                    <p class="pull-left">{ci_language line='You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.'}</p>
                                                    <i class="fa fa-remove pull-right"></i>
                                            </div>
                                    </div>
                            </div>
                    </div>
                </div>*}
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom m-t10 p-0">
					<h4 class="headSettings_head pull-sm-left p-t10">{ci_language line="Choose categories"}</h4>										
					<ul class="pull-sm-right clearfix treeSettings">
						<li class="expandAll"><i class="cb-plus"></i>{ci_language line="Expand all"}</li>
						<li class="collapseAll"><i class="cb-minus"></i>{ci_language line="Collapse all"}</li>
						<li class="checkBaAll"><i class="cb-checked"></i>{ci_language line="Check all"}</li>
                                                <li class="uncheckBaAll"><i class="cb-unchecked"></i>{ci_language line="Uncheck all"}</li>
					</ul>
				</div>
			</div>
		</div>

        <div id="none_category" class="row m-t10 " style="display:none">
            <div class="col-xs-12">{$no_category}</div>             
        </div>
		
                <div class="tree clearfix custom-form" >
                        <i class="icon-folder"></i>
                        <p class="treeHead treeRight">
                            {if isset($mode_default) && ($mode_default == 1)}
                                {ci_language line="Prices Mapping"}
                            {else}
                                {ci_language line="Profiles Mapping"}
                            {/if}
                        </p>
                                                                
                        <div id="main" class="display-none">
                            <div class="treeRight selectTree">
                                                    <select id="category{if isset($id_category)}{$id_category}{/if}_id_profile" class="search-select">
                                                            <option value=""/> -
                                                            {if isset($mode_default) && ($mode_default == 1)}
                                                                {ci_language line="Prices Mapping"}                        
                                                            {else}
                                                                {ci_language line="Default profiles mapping"}
                                                            {/if}   
                                                             - </option>
                                                            {if (isset($profile) && !empty($profile) )}
                                                               {foreach from=$profile key=pkey item=pvalue}
                                                                   <option value="{$pvalue.id_profile}">
                                                                       {$pvalue.name}
                                                                   </option>
                                                               {/foreach}  
                                                            {/if}
                                                    </select>
                                                    <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this));"></i>
                                    </div>
                            </div>
                        
                        <ul id="tree1" class="tree_point">

                        </ul>
                </div>
                        
                        <div class="row btnShow">
                            <div class="col-sm-12">
                            <div class="pull-right m-t20">
                                    <a href="" type="reset" class="pull-left link p-size m-tr10">
                                        Reset
                                    </a>
                                    {*<button class="btn btn-save pull-right" type="submit">{ci_language line="Save"}</button>*}
                                    {if isset($popup)}
                                    <button class="btn btn-save pull-right" id="save_continue_data" type="submit" name="save" value="continue">{ci_language line="Save & continue"}</button>
                                    {else}
                                    <button class="btn btn-save pull-right" id="save_continue_data" type="submit" name="save" value="continue">{ci_language line="Save"}</button>
                                    {/if}
                            </div>
                            </div>
                    </div>
                    {if isset($popup)}
                        <div class="row btnPrevious" style='bottom: 0; display: none; position: fixed;'>
                            <div class="col-sm-12">
                                <div class="pull-left m-t20">
                                    <button class="btn btn-save" id="back" onclick="history.back();">
                                        {ci_language line="Previous"}
                                    </button>
                                </div>
                            </div>
                        </div>
                    {/if}
                </form>
	</div>
 
{include file="footer.tpl"}

<script src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/profiles/profiles_category.js"></script>  