{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Rules"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                <form action="{$base_url}offers/rules_save" method="post" id="" autocomplete="off" novalidate>
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top b-Bottom p-t10 p-b10">
					<a href="" class="link showProfile p-size" id="add">+ {ci_language line="Add a rule to the list"}</a>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">0</span> {ci_language line="Rule(s)"}</p>
				</div>
			</div>
		</div>
		<div class="row showSelectBlock custom-form ruleSet" id="main">
			<div class="col-sm-8 m-t10">
				<a href="" class="removeProfile">{ci_language line="Remove a rule from list"}</a>
				<div class="profileBlock clearfix">
					<p class="regRoboto poor-gray pull-left">{ci_language line="Rule name"}</p>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countRule">0</span> {ci_language line="Item(s)"}</p>
					<div class="form-group  ">
                                                <input type="text" rel="name" required class="form-control" />
                                                <div class="error absolute text-left display-none">
							<p>
								<i class="fa fa-exclamation-circle"></i>
								<span rel="err_rule_name_blank">{ci_language line="Please enter rule name first"}</span><span rel="err_rule_name_duplicate" class="hide">{ci_language line="Please enter unique rule name"}</span>
							</p>
						</div>
					</div>
					<div class="ruleSettings"></div>
                                        <div class="items">
                                            <div class="setBlockMain clearfix hidden">
						<div class="m-b10 clearfix">
							<a href="" class="pull-right removeSet">{ci_language line="Remove set"} <i class="cb-remove"></i></a>
						</div>
						<div class="setBlock_content">
							<div class="setBlock_text">	
								<div class="row addSettings item" rel="0">
									<div class="col-sm-6 addCheckBo">
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                  <input type="checkbox" rel="manufacturer" />
                                                                                                  {ci_language line="Manufacturer"}
											</label>
										</div>
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                    <input type="checkbox" rel="supplier" />
                                                                                                  {ci_language line="Supplier"}
											</label>
										</div>
										
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                  <input type="checkbox" rel="price_ranges" />
                                                                                                  {ci_language line="Price range"}
											</label>
										</div>
                                                                                <div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                    <input type="checkbox" rel="actions" />
												   {ci_language line="Action"}
											</label>
										</div>
									</div>
									<div class="col-sm-6 addSelect">
										<div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="id_manufacturer" sum-rel="manufacturer" id="id_manufacturer">
                                                                                                <option value="" ></option>
                                                                                                {if isset($manufacturer)}
                                                                                                    {foreach from=$manufacturer key=mkey item=mvalue}
                                                                                                        <option value="{$mkey}">{$mvalue}</option>
                                                                                                    {/foreach}  
                                                                                                {/if}
											 
											</select>
										</div>
										<div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="id_supplier" sum-rel="supplier" id="id_supplier">
                                                                                                <option value="" ></option>
                                                                                                {if isset($supplier)}
                                                                                                    {foreach from=$supplier key=skey item=svalue}
                                                                                                        <option value="{$skey}">{$svalue.name}</option>
                                                                                                    {/foreach}  
                                                                                                {/if}
												 
											</select>
										</div>
										
										<div class="choZn price_range_item">
											<div class="row price_range" rel="1">
												<div class="col-xs-4">
													<div class="form-group">
														<input rel="price_range_from" type="text" sum-rel="price_ranges" sum-srel="from" class="form-control"/>
													</div>
												</div>
												<div class="col-xs-1">
													<p class="m-t10">{ci_language line="to"}</p>
												</div>
												<div class="col-xs-4">
													<div class="form-group">
                                                                                                            <input rel="price_range_to" type="text" sum-rel="price_ranges" sum-srel="to" class="form-control" readonly=""/>
													</div>
												</div>
												<div class="col-xs-1">
													<i class="cb-plus  good price-range-add"></i>
												</div>
											</div>
										</div>
                                                                                <div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="action" sum-rel="actions" id="action">
                                                                                                <option value=""></option>
                                                                                                <option value="0">{ci_language line="Drop"}</option>
												 
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>                                                  
                                        </div>
					<div class="pull-right m-b10">
						<a href="" class="showSet">+ {ci_language line="Add set"} <i class="cb-add"></i></a>
					</div>
				</div>
			</div>
			<div class="col-sm-4 m-t65">
                                <div class="headSettings clearfix p-b10 b-Bottom ">
					<h4 class="headSettings_head">{ci_language line="Summary"}</h4>						
				</div>
				<div class="main-summary setSettings ">
					<p class="head_text rule_name b-Top p-t10 m-0">					
						{ci_language line="Rule name"}:</b> &nbsp; <span class="rule-name pull-sm-right p-text"></span>
					</p>
                                        <div class="sum_item display-none" rel="0">
                                            <p class="head_text b-Top p-t10 m-0">					
                                                {ci_language line="Item"} <span></span>
                                            </p>	
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="manufacturer">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Manufacturer"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="supplier">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Supplier"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            
                                            <div class="clearfix p-t10 summaryBlock display-none"  rel="actions">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Action"}</p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                                    <div class="clearfix p-t10 summaryBlock display-none"  rel="price_ranges">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Price range"} </p>
                                                    <div class="sum_price_range_item pull-sm-right p-text clearfix">
                                                        <p  rel="1"><span rel="from"></span> {ci_language line="to"} <span rel="to"></span></p>
                                                    </div>
                                            </div>
                                        </div>
					
					 
				</div>
                             
                            </div>
			</div>
                            
                            
                        <!-- START IN DATABASE -->
                        {if isset($rules)}
                                {foreach from=$rules key=key item=value}
                                
                    <div class="row showSelectBlock custom-form ruleSet display-block" >
			<div class="col-sm-8 m-t10">
				<a href="" class="removeProfile" data-id="{$value.id_rule}"  rel="{$value.name}">{ci_language line="Remove a rule from list"}</a>
				<div class="profileBlock clearfix">
					<p class="regRoboto poor-gray pull-left">{ci_language line="Rule name"}</p>
					<p class="pull-right montserrat poor-gray"><span class="dark-gray countRule">{if isset($value.rule)}{sizeof($value.rule)}{else}0{/if}</span> {ci_language line="Item(s)"}</p>
					<div class="form-group  ">
                                                <input type="text" rel="name" required class="form-control"  name="rule[{$key}][name]" value="{$value.name}" data-content="{$key}" />
                                                <div class="error text-left display-none">
							<p>
								<i class="fa fa-exclamation-circle"></i>
								
								<span rel="err_rule_name_blank">{ci_language line="Please enter rule name first"}</span><span rel="err_rule_name_duplicate" class="hide">{ci_language line="Please enter unique rule name"}</span>
							</p>
						</div>
                                                        <input type="hidden" rel="id_rule" name="rule[{$key}][id_rule]" value="{$value.id_rule}"> 
					</div>
					<div class="ruleSettings"></div>
                                        <div class="items">
                                            <div class="setBlockMain clearfix hidden">
						<div class="m-b10 clearfix">
							<a href="" class="pull-right removeSet">{ci_language line="Remove set"} <i class="cb-remove"></i></a>
						</div>
						<div class="setBlock_content">
							<div class="setBlock_text">	
                                                            
								<div class="row addSettings item" rel="0">
									<div class="col-sm-6 addCheckBo"> 
									</div>
									<div class="col-sm-6 addSelect"> 
									</div>
								</div>
							</div>
						</div>
                                            </div>  
                                        {if isset($value.rule)}
                                            {foreach from=$value.rule key=ikey item=ivalue}    
                                            <div class="setBlock  clearfix  " rel="s{$ikey+1}"  no-rel="{$ikey+1}">
						<div class="m-b10 clearfix">
							<a href="" class="pull-right removeSet" data-id="{$ivalue.id_rule_item}">{ci_language line="Remove set"} <i class="cb-remove"></i></a>
                                                        <input type="hidden" rel="id_rule_item" name="rule[{$key}][rule][{$ikey+1}][id_rule_item]" value="{$ivalue.id_rule_item}"> 
						</div>
						<div class="setBlock_content">
							<div class="setBlock_text">	
                                                            
                                                                <div class="row addSettings item" rel="{$ikey+1}">
									<div class="col-sm-6 addCheckBo">
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                  <input type="checkbox" rel="manufacturer" {if isset($ivalue.id_manufacturer) && $ivalue.id_manufacturer != 0} checked {/if} />
                                                                                                  {ci_language line="Manufacturer"}
											</label>
										</div>
                                                                               
                                                                        </div>
                                                                        <div class="col-sm-6 addSelect">
                                                                                <div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="id_manufacturer" sum-rel="manufacturer" id="id_manufacturer"  name="rule[{$key}][rule][{$ikey+1}][id_manufacturer]">
                                                                                                <option value="" ></option>
                                                                                                {if isset($manufacturer)}
                                                                                                    {foreach from=$manufacturer key=mkey item=mvalue}
                                                                                                        <option value="{$mkey}" {if isset($ivalue.id_manufacturer) && $mkey == $ivalue.id_manufacturer} selected {/if}>{$mvalue}</option>
                                                                                                    {/foreach}  
                                                                                                {/if}
											 
											</select>
										</div>
                                                                        </div>
                                                                </div>
                                                                <div class="row addSettings item" rel="{$ikey+1}">
									<div class="col-sm-6 addCheckBo">
										<div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                  <input type="checkbox" rel="supplier" {if isset($ivalue.id_supplier) && $ivalue.id_supplier != 0} checked {/if} />
                                                                                                  {ci_language line="Supplier"}
											</label>
										</div>
                                                                        </div>
                                                                        <div class="col-sm-6 addSelect">
                                                                                <div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="id_supplier" sum-rel="supplier" id="id_supplier"  name="rule[{$key}][rule][{$ikey+1}][id_supplier]">
                                                                                                <option value="" ></option>
                                                                                                {if isset($supplier)}
                                                                                                    {foreach from=$supplier key=skey item=svalue}
                                                                                                        <option value="{$skey}" {if isset($ivalue.id_supplier) && $skey == $ivalue.id_supplier} selected {/if}>{$svalue.name}</option>
                                                                                                    {/foreach}  
                                                                                                {/if}
												 
											</select>
										</div>
                                                                        </div>
                                                                </div>
                                                                
                                                                                                
                                                                <div class="row addSettings item" rel="{$ikey+1}">
									<div class="col-sm-6 addCheckBo">
										  <div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                  <input type="checkbox" rel="price_ranges" {if isset($ivalue.price_range) && $ivalue.price_range != 0}checked {/if}/>
                                                                                                  {ci_language line="Price range"}
											</label>
										</div>
                                                                        </div>
                                                                        <div class="col-sm-6 addSelect">
                                                                                <div class="choZn price_range_item">
                                                                                         <!-- Price range -->
                                                                                    {$prkey = 0}
                                                                                    
                                                                                    {if isset($ivalue.price_range)}
                                                                                    {assign var=len value=$ivalue.price_range|@count}
                                                                                        {foreach from=$ivalue.price_range key=prkey item=prvalue}
											<div class="row price_range" rel="{$prkey+1}">
												<div class="col-xs-4">
													<div class="form-group">
                                                                                                                <input type="hidden" rel="id_rule_item_price_range" name="rule[{$key}][rule][{$ikey+1}][price_range][{$prkey+1}][id_rule_item_price_range]" value="{$prvalue.id_rule_item_price_range}"> 
                                                                                                                <input rel="price_range_from" type="text" sum-rel="price_ranges" sum-srel="from" class="input-mini price form-control" name="rule[{$key}][rule][{$ikey+1}][price_range][{$prkey+1}][price_range_from]" {if !isset($prvalue.price_range_from)} {*disabled*} {else} value="{$prvalue.price_range_from}" {/if} data-content="rule[{$key}][rule][{$ikey+1}][price_range]"/>
													</div>
												</div>
												<div class="col-xs-1">
													<p class="m-t10">{ci_language line="to"}</p>
												</div>
												<div class="col-xs-4">
													<div class="form-group">
                                                                                                            
                                                                                                            <input type="hidden" rel="id_rule_item_price_range" name="rule[{$key}][rule][{$ikey+1}][price_range][{$prkey+1}][id_rule_item_price_range]" value="{$prvalue.id_rule_item_price_range}"> 
                                                                                                                <input rel="price_range_to" type="text" sum-rel="price_ranges" sum-srel="to" class="input-mini price form-control" name="rule[{$key}][rule][{$ikey+1}][price_range][{$prkey+1}][price_range_to]" {if !isset($prvalue.price_range_to)} {*disabled*} {else} value="{$prvalue.price_range_to}" {/if} data-content="rule[{$key}][rule][{$ikey+1}][price_range]"/>
                                                                                                             
													</div>
												</div>
												<div class="col-xs-1">
													<i class="cb-plus   {if $len==$prkey+1} good {else} bad {/if} price-range-add"></i>
												</div>
											</div>
                                                                                        {/foreach}
                                                                                    {/if}
										</div>
                                                                        </div>
                                                                </div>
                                                                                
                                                                <div class="row addSettings item" rel="{$ikey+1}">
									<div class="col-sm-6 addCheckBo">
										 <div class="checkBo">
											<label class="cb-checkbox text-uc montserrat dark-gray bold m-t10">
                                                                                                    <input type="checkbox" rel="actions" {if isset($ivalue.action) && $ivalue.action != 0}checked {/if}/>
												   {ci_language line="Action"}
											</label>
										</div>
                                                                        </div>
                                                                        <div class="col-sm-6 addSelect">
                                                                                <div class="choZn">
											<select class="search-select" data-placeholder="Choose" rel="action" sum-rel="actions" id="action"  name="rule[{$key}][rule][{$ikey+1}][action]">
                                                                                                <option value=""></option>
                                                                                                <option value="0" {if isset($ivalue.action) && $ivalue.action == 0} selected {/if}>{ci_language line="Drop"}</option>
												 
											</select>
										</div>
                                                                        </div>
                                                                </div>  
								 
							</div>
						</div>
					</div>   
                                             {/foreach}
                                        {/if}                                                  
                                                                                                
                                        </div>
					<div class="pull-right m-b10">
						<a href="" class="showSet">+ {ci_language line="Add set"} <i class="cb-add"></i></a>
					</div>
				</div>
			</div>
			<div class="col-sm-4 m-t65">
                                <div class="headSettings clearfix p-b10 b-Bottom ">
					<h4 class="headSettings_head">{ci_language line="Summary"}</h4>						
				</div>
				<div class="main-summary setSettings display-block">
					<p class="head_text rule_name b-Top p-t10 m-0">					
						{ci_language line="Rule name"}:</b> &nbsp; <span class="rule-name pull-sm-right p-text">{$value.name}</span>
					</p>
                                        <div class="sum_item display-none" rel="0">
                                            <p class="head_text b-Top p-t10 m-0">					
                                                {ci_language line="Item"} <span></span>
                                            </p>	
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="manufacturer">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Manufacturer"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="supplier">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Supplier"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            <div class="clearfix p-t10 summaryBlock display-none"  rel="price_ranges">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Price range"} </p>
                                                    <div class="sum_price_range_item pull-sm-right p-text clearfix">
                                                        <p  rel="1"><span rel="from"></span> {ci_language line="to"} <span rel="to"></span></p>
                                                    </div>
                                            </div>
                                            <div class="clearfix p-t10 summaryBlock display-none"  rel="actions">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Action"}</p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            
                                        </div>
                                        {if isset($value.rule)}
                                            {foreach from=$value.rule key=ikey item=ivalue}     
                                        <div class="sum_item  " rel="s{$ikey+1}">
                                            <p class="head_text b-Top p-t10 m-0">					
                                                {ci_language line="Item"} <span></span>
                                            </p>	
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="manufacturer">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Manufacturer"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            <div class="clearfix p-t10 summaryBlock display-none" rel="supplier">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Supplier"} </p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                            
                                           
                                            <div class="clearfix p-t10 summaryBlock display-none"  rel="price_ranges">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Price range"} </p>
                                                    <div class="sum_price_range_item pull-sm-right p-text clearfix">
                                                        {$prkey = 0}
                                                            {if isset($ivalue.price_range)}
                                                                {foreach from=$ivalue.price_range key=prkey item=prvalue} 
                                                        <p  rel="{$prkey+1}"><span rel="from"></span> {ci_language line="to"} <span rel="to"></span></p>
                                                                {/foreach}
                                                            {/if}
                                                    </div>
                                            </div>
                                             <div class="clearfix p-t10 summaryBlock display-none"  rel="actions">
                                                    <p class="pull-sm-left poor-gray">{ci_language line="Action"}</p>						
                                                    <p><span class="pull-sm-right p-text"></span></p>
                                            </div>
                                        </div>
                                        {/foreach}
                                        {/if}
					
					 
				</div>
                             
                            </div>
			</div>
                                    
                                    
                                {/foreach}
                        {/if}
                <!-- END IN DATABASE -->

                {include file="helpers/action_save_continue.tpl" data='profiles'}
		
                </form>
	</div>
                <input type="hidden" value="{ci_language line="Are you sure to delete this price range"}." id="confirm-delete-price-range-message" />
                <input type="hidden" value="{ci_language line="Price range error"}." id="price-range-error-message" />
                <input type="hidden" value="{ci_language line="Successfully"}." id="success-message" />
                <input type="hidden" value="{ci_language line="Delete was successful"}." id="delete-success-message" />
                <input type="hidden" value="{ci_language line="Error"}!" id="error-message" />
                <input type="hidden" value="{ci_language line="Delete was unsuccessful"}!" id="delete-unsuccess-message" />
                <input type="hidden" value="{ci_language line="Are you sure to delete rule"}" id="confirm-delete-rule-message" />
                <input type="hidden" value="{ci_language line="Are you sure to delete rule item"}" id="confirm-delete-rule-item-message" />
                <input type="hidden" value="{ci_language line="Do you want to delete"}" id="confirm-message" />

{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/bootbox.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/offers/rules.js"></script>