{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css2/admin/module.admin.page.form_elements.min.css" />
<script src="{$cdn_url}assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.min.js"></script>

<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    <div class="page-content">            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="products"} 
                <small><i class="icon-double-angle-right"></i> {ci_language line="carriers"} </small>
            </h1>
        </div>
              
        <div class="row-fluid">
            
            <div class="offset1 span10">
                
                <div class="alert alert-warning" style="display: none">{ci_language line="please_config"}</div>
                
                <div id="category">
                    
                    <form action="{$base_url}my_feeds/parameters/products/category_save" method="post" >
                        <div class="widget-box">
                        <div class="widget-header header-color-blue2">
                            <h4 class="lighter smaller">{ci_language line="choose_categories"}</h4>
                            <div class="widget-toolbar">
                                <a href="#"></a>
                                <a href="#" data-action="collapse"><i class="icon-only bigger-110 icon-chevron-up"></i></a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-toolbox padding-8 clearfix">
                                <button id="expandAll" class="btn btn-mini btn-success" type="button">
                                    <i class=" icon-folder-open"></i>
                                    {ci_language line="expand_all"}
                                </button>
                                <button id="collapseAll" class="btn btn-mini btn-warning" type="button">
                                    <i class=" icon-folder-close"></i>
                                    {ci_language line="collapse_all"}
                                </button> 
                                <button id="checkAll" class="btn btn-mini btn-pink" type="button">
                                    <i class=" icon-check "></i>
                                    {ci_language line="check_all"}
                                </button> 
                                <button id="unCheckAll" class="btn btn-mini btn-purple" type="button">
                                    <i class="  icon-check-empty"></i>
                                    {ci_language line="uncheck_all"}
                                </button>
                            </div>
                            <div class="widget-main padding-30">

                                <div class="tree tree-unselectable">
                                    <div class="tree-folder" style="display: block;">	
                                        <div class="tree-folder-header">					
                                            <i class="icon-folder-close orange"></i>					
                                            <div class="tree-folder-name">{if isset($root)}{$root.name}{/if}</div>				
                                        </div>
                                    </div> 

                                </div>
                                <div id="tree1" class="tree tree-selectable">
                                    <div id="tree1_loading" class="tree-loading"><i class="icon-spinner icon-spin orange bigger-225"></i></div>
                                </div>
                                        
                            </div>{*widget-main*}
                        </div>{*widget-body*}

                    </div>{*widget-box*}
                    
                    <div class="form-actions center">
                         <button class="btn btn-primary" type="submit">
                            <i class="icon-ok bigger-110"></i>
                            {ci_language line="save"}
                        </button>
                    </div><!--form-actions-->
                        
                    </form>
                </div>{*category*}
                
            </div>{*span12*}
            
        </div>{*row-fluid*}
                            
    </div>
</div>

{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/fuelux/fuelux.tree.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/category.js"></script>  
