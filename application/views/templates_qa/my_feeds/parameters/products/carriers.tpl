{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

    <div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="carriers"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
        <form action="{$base_url}my_feeds/parameters/products/carriers_save" method="post" >

        		<div class="row">
        			<div class="col-xs-12">
        				<div class="headSettings clearfix pull-left">
        					<h4 class="headSettings_head">{ci_language line="choose_carriers"}</h4>		
                            <button id="checkAll" class="btn btn-success" type="button">
                                <i class="fa fa-check-square-o "></i>
                                {ci_language line="check_all"}
                            </button> 
                            <button id="unCheckAll" class="btn btn-warning" type="button">
                                <i class="fa fa-square-o"></i>
                                {ci_language line="uncheck_all"}
                            </button>				
        				</div>
        			</div>
        		</div>
                                
                {if isset($carriers)}
                    <div class="row custom-form">
                    {foreach from=$carriers key=carrier_key item=carrier_value}
                        {$selected = ''}
                        {$class = ''}
                        {if $carrier_value.selected == true }
                            {$selected = 'checked=""'}
                            {$class = 'class="active"'}
                        {/if}

                        {if isset($carrier_value.name)}
                            <div class="col-xs-12 {$class}">
                                <label class="cb-checkbox">
                                <i class="fa fa-fw fa-square-o"></i>
                                    <input name="carrier[]" type="checkbox" value="{$carrier_value.id_carrier}" {if $selected}checked="checked"{/if} />
                                    {$carrier_value.name}
                                </label>
                            </div>
                        {/if}
                    {/foreach}
                    </div>
                {/if}
		          
                {include file="helpers/action_save_continue.tpl" data='filters'}
                  
                </form>
	</div>
                                        
{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/carriers.js"></script>
