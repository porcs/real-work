{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main-content">
    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    
    <div class="page-content">
            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="mapping"} {ci_language line="products"}
                <small><i class="icon-double-angle-right"></i> {ci_language line="characteristics"} </small>
            </h1>
        </div>
        
        <div class="row-fluid">
            <div class="offset1 span10">
                <form action="{$base_url}/my_feeds/characteristics_save" id="mapping-attribute" class="form-horizontal" method="post">
                    <h4 class="row-fluid smaller lighter blue">
                        <label class="pull-right inline">
                            <input id="count" value="{$characteristics_row}" readonly="">
                            Records
                            <i class="icon-th-large"></i>
                        </label>
                    </h4>

                    <div class="control-group">

                        <div id="character-1" rel="{if $characteristics_row != 0}{$characteristics_row-1}{else}0{/if}" class="row-odd"> 
                            
                            <input class="input-text" type="text" disabled placeholder="{ci_language line="enter_a_characteristics"}" />
                            &nbsp; &nbsp; <i class="icon-exchange icon-large grey"></i>&nbsp; &nbsp; 
                            <input type="text" disabled class="input-value" />
                            &nbsp; &nbsp; 
                            <a class="addnewmapping" id="attr" rel="{if $characteristics_row != 0}{$characteristics_row-1}{else}0{/if}">
                                <i class="icon-plus-sign icon-large green"></i>
                            </a>
                            <a class="removemapping" id="attr" style="display: none;">
                                <i class="icon-minus-sign icon-large red"></i>
                            </a>

                        </div>

                        <div id="new-mapping">
                            {if isset($characteristics)}
                                {foreach from=$characteristics key=charkey item=charvalue}
                                    {if is_array($charvalue)}
                                        <div {if $charkey%2 == 0}class="row-even"{else}class="row-odd"{/if}> 
                                            <input class="input-text" type="text" value="{$charvalue.character}"  name="character[{$charkey}]"/>
                                            &nbsp; &nbsp; <i class="icon-exchange icon-large grey"></i>&nbsp; &nbsp; 
                                            <input type="text" class="input-value" value="{$charvalue.value}"  name="mapping[{$charkey}]"/>
                                            &nbsp; &nbsp;
                                            <a class="removemapping" id="attr"><i class="icon-minus-sign icon-large red"></i></a>
                                        </div>  
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>

                    </div>

                    <div class="form-actions">
                        <button class="btn btn-info" type="submit">
                            <i class="icon-ok bigger-110"></i>
                            {ci_language line="save"}
                        </button>
                    </div>
                </form>

            </div>{*span10*}
            
        </div>{*row-fluid*}
                            
    </div>{*page-content*}
    
</div>{*main-content*}

{include file="footer.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/characteristics.js"></script>  