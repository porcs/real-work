{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/js/datatable/media/css/jquery.dataTables.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_feed/my_products.css" />
    
<div id="content">
    <div class="heading-buttons bg-white border-bottom innerAll">
	<h1 class="content-heading padding-none pull-left">{ci_language line="My products"}</h1>
	<div class="clearfix"></div>
    </div>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        <div class="innerAll spacing-x2">
            <div class="row">
            <div class="col-md-12">
            <div class="widget widget-inverse">

                <!-- Widget Heading -->
                <div class="widget-head">
                    <h3 class="heading show_thumbnails"><i class="fa fa-shopping-cart"></i> {ci_language line="My products"}</h3>
                </div>
                <!-- // Widget Heading END -->

                <div class="widget-body">   
                    <div class="filter_add">
                        <label id="sel_cat">
                            <span>{ci_language line="Filter by category"}:</span>
                        <select id="sel_category">
                            <option value="">{ci_language line="All categories"}</option> 
                        </select>
                        </label>
                        <label id="sel_cond">
                            <span>{ci_language line="Filter by conditions"}:</span>
                            <select id="sel_condition" >
                                <option value="">{ci_language line="All conditions"}</option>
                                {foreach $shop_condition as $id => $s_cond}
                                    <option value="{$id}" >{ucfirst(strtolower($s_cond['txt']))}</option>
                                {/foreach}
                            </select>
                        </label>
                    </div>
                    <table class=" colVis table" id="my_prod_list"  >
                        <!-- Table heading -->
                        <thead class="bg-gray">
                            <tr>
                                <th><input id="sel_all" type="checkbox"></th>
                                <th>#</th>
                                <th>{ci_language line="Product name > SKU"} <span id="listing_th">{ci_language line="> Listing"}</span></th>
                                <th>{ci_language line="Variants"}</th>
                                <th>{ci_language line="Price"}</th>
                                <th>{ci_language line="Quantity"}</th>
                                
                            </tr>
                        </thead>
                        <!-- // Table heading END -->
                       
                </table>
                <!-- // Table END -->
                    
            </div>
        </div>
    </div>
</div>
</div>
</div>
{include file="footer.tpl"}

<script src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/my_products.js"></script>