{include file="header.tpl"}
{ci_config name="base_url"}
{include file="sidebar.tpl"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="general"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                <form action="{$base_url}my_feeds/general_save" method="post" id="mode" >
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="Mode"}</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="montserrat dark-gray text-uc m-t10">{ci_language line="General Mode"}</p>
			</div>
		</div>
		<div class="row custom-form">
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="mode" type="radio" value="1" {if isset($feed_mode.mode) && $feed_mode.mode == 1}checked="checked" rel="checked"{/if} />
					<p class="text-uc dark-gray montserrat m-b0 bold ">
                                            {if isset($feed_mode.mode) && $feed_mode.mode == 1}
                                                <span class="recommended m-l0 m-r10">{ci_language line="Default Mode"}</span>
                                            {/if}
						{ci_language line="Beginner"}
					</p>
					<p class="light-gray m-l25">{ci_language line="Choose Beginner mode"}</p>
				</label>
			</div>
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="mode" value="2" {if isset($feed_mode.mode) && $feed_mode.mode == 2}checked="checked" rel="checked"{/if} />					
					<p class="text-uc dark-gray montserrat m-b0 bold">
                                            {if isset($feed_mode.mode) && $feed_mode.mode == 2}
						<span class="recommended m-l0 m-r10">{ci_language line="Default Mode"}</span>
                                            {/if}
						{ci_language line="Expert"}
					</p>
					<p class="light-gray m-l25">{ci_language line="Choose Expert mode"}</p>
				</label>
			</div>
			<div class="col-sm-4">
				<label class="cb-radio w-100">
					<input type="radio" name="mode" value="3" {if isset($feed_mode.mode) && $feed_mode.mode == 3}checked="checked" rel="checked"{/if} />
					<p class="text-uc dark-gray montserrat m-b0 bold">
                                            {if isset($feed_mode.mode) && $feed_mode.mode == 3}
                                                <span class="recommended m-l0 m-r10">{ci_language line="Default Mode"}</span>
                                            {/if}
						{ci_language line="Advanced"}
					</p>
					<p class="light-gray m-l25">{ci_language line="Choose Advanced mode"}</p>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="Authentification"}</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="validate blue">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<p class="pull-left">{ci_language line="Security token used between your Shop and Feed.biz"}</p>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="Username"}</p>
				<div class="form-group">
					<input type="text" class="form-control" value="{$username}" readonly>
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="Token"}</p>
				<div class="form-group">
					<input type="password" class="form-control" value="{$token}" name="token" readonly>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="m-t25">
                                    <!-- Alret Popup-->
                                    <input id="general-confirm" type="hidden" value="{ci_language line="Are you sure to change mode"}?" />
                                    <input id="general-message" type="hidden" value="{ci_language line='Please choose mode'}" />
					                <input type="hidden" name="continue" value="my_shop/connect" />
						            <button type="submit" class="btn btn-save pull-right" name="save" value="continue">
						                {ci_language line="Save & continue"} 
						            </button>
						            <button type="submit" class="btn btn-link link p-size pull-right p-0 m-0 m-tr10" name="save">
						                {ci_language line="save"}
						            </button>
				</div>
			</div>
		</div>
            </form>
	</div>

{include file="footer.tpl"}
<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/bootbox.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/general.js"></script> 