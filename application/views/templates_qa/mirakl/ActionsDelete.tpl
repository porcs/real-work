{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="mirakl/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#" data-toggle="tab" class="">
                    <i class="icon-syn"></i> <span>{ci_language line="Delete"}</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row">
                    <div class="col-md-12">
                        <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Parameter"}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-save pull-right" id="delete_products" type="button">
            <i class="fa fa-upload"></i><span class="send-status"> {ci_language line="Delete Products"}</span>              
        </button>
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to delete products?"}" id="delete-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />

{include file="mirakl/IncludeParameters.tpl"}

<script src="{$cdn_url}assets/js/FeedBiz/mirakl/delete.js"></script> 

{include file="mirakl/IncludeScript.tpl"}
{include file="mirakl/PopupStepFooter.tpl"} {* alert msg if inactive set in parameter *}
{include file="footer.tpl"}