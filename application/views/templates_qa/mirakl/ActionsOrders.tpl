{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="mirakl/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#orders" data-toggle="tab" class="">
                    <i class="icon-creation-down"></i> <span>{ci_language line="Orders"}</span>
                </a>
            </li>                        
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="orders">
              
                        
                    <div class="row">
                    <div class="col-md-12">
                        <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Import Orders"}</h4>
                        </div>
                    </div>
                </div>
                        
                <p class="regRoboto">{ci_language line="Parameters"}</p>
                        

                <div class="row">        
                    <div class="col-sm-6">
                        <p class="poor-gray">{ci_language line='Order Date Range'}</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group calendar firstDate">
                                    <input class="form-control order-date" type="text" id="order-date-from" value="{if isset($yesterday)}{$yesterday}{/if}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group calendar lastDate">
                                    <input class="form-control order-date" type="text" id="order-date-to" value="{if isset($today)}{$today}{/if}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <p class="poor-gray">{ci_language line='Order Status'}</p>
                        <select name="orders-statuses" class="search-select" id="order-status">
                            <option value="Importable">{ci_language line='Ready to be Imported'}</option>
                            <option value="ALL">{ci_language line='All Pending Orders'}</option>
                        </select>
                    </div>
                    <div class="col-sm-3 sm-left" id="orders-import">
                        <button class="btn btn-save m-t25" id="import_orders_list" type="button">
                            <i class="fa fa-download"></i> {ci_language line="Import Orders"} 
                        </button>
                    </div>
                </div>
                <div class="row" id="import-loader" style="display:none; margin-top: 10px;">
                    <div class="col-md-12 validate blue">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="fa fa-spinner fa-spin"></i> 
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">{ci_language line="Conecting to Mirakl ..."}</p>
                            </div>
                        </div>
                    </div>              
                </div>
            </div>
        </div>
    </div>
</div>
                            
{include file="mirakl/IncludeParameters.tpl"}


<script src="{$cdn_url}assets/js/FeedBiz/mirakl/orders_import.js"></script>

{include file="mirakl/IncludeScript.tpl"}
{include file="mirakl/PopupStepFooter.tpl"} {* alert msg if inactive set in parameter *}
{include file="footer.tpl"}