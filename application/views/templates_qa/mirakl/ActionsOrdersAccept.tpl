{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="mirakl/PopupStepHeader.tpl"}

<div class="row-fluid">

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#orders" data-toggle="tab" class="">
                        <i class="icon-creation-down"></i> <span>{ci_language line="Orders"}</span>
                    </a>
                </li>                        
            </ul>
        </div>
    </div>

    <!--  Message Here -->
    <div id="send-products-message">
        <div class="status row" style="display:none;">
            <div class="col-md-12 validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i> 
                    </div>
                    <div class="validateCell">
                        <p class="pull-left"></p>
                    </div>
                </div>
            </div>              
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active" id="orders">
                    
                  <div class="row">
                        <div class="col-md-12">
                            <div class="headSettings clearfix ">
                                <h4 class="headSettings_head col-xs-6 row">{ci_language line="Accept Orders"}</h4>
                                <button type="button" id="accept_order_list" class="btn btn-save m-0 pull-right">{ci_language line="Lookup"} <i class="fa fa-search"></i></button>
                            </div>
                        </div>
                  </div>

                    <!-- Import Order Message Here Loading... -->
                    <div class="row" id="accept-loader" style="display:none; margin-top: 10px;">
                        <div class="col-md-12 validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="fa fa-spinner fa-spin"></i> 
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left">{ci_language line="Conecting to Mirakl ..."}</p>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="row" id="accept-orders-result" style="display:none; margin-top: 10px;">
                        <div class="col-md-12 validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="fa fa-check"></i> 
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left" id="accept-orders-result-msg"></p>
                                </div>
                            </div>
                        </div>              
                    </div>
                   <div class="row" id="accept-orders-result-err" style="display:none; margin-top: 10px;">
                        <div class="col-md-12 validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="fa fa-check"></i> 
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left" id="accept-orders-result-msg-err"></p>
                                </div>
                            </div>
                        </div>              
                    </div>
                    <div class="row" style="display:none" id="wapper_table_accept_list">
                        <div class="col-md-12">
                            <div class="headSettings clearfix">
                                <div class="pull-md-left clearfix">
                                        <h4 class="headSettings_head">{ci_language line="Accept Orders"}</h4>						
                                </div>                                			
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="accept-table" class="mirakl-responsive-table" style="">
                                        <thead class="table-head">
                                            <tr>
                                                <th>{ci_language line="ID"}</th>
                                                <th>{ci_language line="Order ID Ref"}.</th>
                                                <th>{ci_language line="Order Date"}</th>
                                                <th>{ci_language line="Name"}</th>
                                                <th>{ci_language line="Ship"}</th>
                                                <th>{ci_language line="Total"}</th>
                                                <th>{ci_language line="Status"}</th>
                                                <th>{ci_language line="Tracking No."}</th>
                                                <th>{ci_language line="Invoice No."}</th>
                                                <th>{ci_language line="Exported"}</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_accept_order_list">
                                        </tbody>
                                    </table><!--table-->
                                    <table id="table_no_data" class="mirakl-responsive-table">
                                        
                                    </table>
                                </div><!--col-md-12-->
                            </div><!--row--> 

                        </div><!--col-md-12-->
                    </div><!--row-->
                    <div class="row"  id="accept_list">
                        <div class="col-md-12">
                            <div class="headSettings clearfix">
                                <div class="pull-md-left clearfix">
                                    <h4 class="headSettings_head">{ci_language line="Accept List"}</h4>						
                                </div>                                			
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="accept-list" class="mirakl-responsive-table">
                                        <thead class="table-head">
                                            <tr>
                                                <th>{ci_language line="ID"}</th>
                                                <th>{ci_language line="Order ID Ref"}.</th>
                                                <th>{ci_language line="Order Date"}</th>
                                                <th>{ci_language line="Name"}</th>
                                                <th>{ci_language line="Ship"}</th>
                                                <th>{ci_language line="Total"}</th>
                                                <th>{ci_language line="Status"}</th>
                                                <th>{ci_language line="Tracking No."}</th>
                                                <th>{ci_language line="Invoice No."}</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table><!--table-->
                                </div><!--col-md-12-->
                            </div><!--row--> 

                        </div><!--col-md-12-->
                    </div><!--row-->
                </div>
            </div>
        </div> <!-- End Col -->
    </div> <!-- End Row -->
    
    <div style="display:none"> 
	<table id="table_order_details">
	    <thead class="table-head">
	        <tr>
                    <th></th>
	            <th>{ci_language line="Row"}</th>
                    <th>{ci_language line="Produt ID"}</th>
                    <th>{ci_language line="Name"}</th>
                    <th>{ci_language line="Status"}</th>
                    <th>{ci_language line="SKU"}</th>
                    <th>{ci_language line="Ship."}</th>
                    <th>{ci_language line="Qty."}</th>
                    <th>{ci_language line="Price"}</th>
	        </tr>
	    </thead>
	    <tbody></tbody>
	</table>
    </div>


    {*include file="mirakl/IncludeDataTableLanguage.tpl"*}
</div> 

{include file="mirakl/IncludeParameters.tpl"}


<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/orders_accept.js"></script> 

{include file="mirakl/IncludeScript.tpl"}
{include file="mirakl/PopupStepFooter.tpl"} {* alert msg if inactive set in parameter *}
{include file="footer.tpl"}