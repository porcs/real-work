{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="mirakl/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#" data-toggle="tab" class="">
                    <i class="icon-syn"></i><span>{ci_language line="product"}</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="row">
                    <div class="col-md-12">
                        <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Parameter"}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-b10 custom-form">  
    <label class="col-md-4 poor-gray m-t3 m-r10 text-right">{ci_language line="Only In Stock Products"} :</label>
    <div class="col-md-4">
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="create-in-stock" type="checkbox" value="1" id="create-in-stock" data-state-on="{ci_language line="YES"}" data-state-off="{ci_language line="NO"}">
            </label>
            <span class="cb-state">{ci_language line="YES"}</span>
        </div>
    </div>
</div>

<div class="row m-b10 custom-form">  
    <label class="col-md-4 poor-gray m-t3 m-r10 text-right">{ci_language line="Send to Mirakl"} :</label>
    <div class="col-md-4">
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="send-products" type="checkbox" value="1" id="send" data-state-on="{ci_language line="YES"}" data-state-off="{ci_language line="NO"}" checked>
            </label>
            <span class="cb-state">{ci_language line="YES"}</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-save pull-right m-r10" id="download_products" type="button">
            <i class="fa fa-download"></i><span class="send-status"> {ci_language line="Downloads"}</span>              
        </button>
        
        {*<button class="btn btn-save pull-right" id="send_products" type="button">
            <i class="fa fa-upload"></i><span class="send-status"> {ci_language line="Export Products"}</span>              
        </button>*}
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to export products?"}" id="send-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />

{include file="mirakl/IncludeParameters.tpl"}

<script src="{$cdn_url}assets/js/FeedBiz/mirakl/products.js"></script> 

{include file="mirakl/IncludeScript.tpl"}
{include file="mirakl/PopupStepFooter.tpl"} {* alert msg if inactive set in parameter *}
{include file="footer.tpl"}