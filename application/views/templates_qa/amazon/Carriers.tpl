{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
       
<form id="form-mapping" method="post" action="{$base_url}amazon/save_carriers" enctype="multipart/form-data" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 
    
    <!--Carrier--> 
    {if isset($carriers)}
        <div id="carrier-mapping">

            <div class="row">
                <div class="col-xs-12">
                    <p class="head_text p-t20 p-b20">
                       {ci_language line="Carriers Mapping"}
                    </p>
                </div>
            </div>
                    
            {*<div class="row">
                <div class="col-xs-12">
                    <div class="validate blue b-Bottom p-b10">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">
                                    {ci_language line="Associated carrier, useful to display your preferate carrier on the order page and on the invoice "}
                                </p>
                                <i class="fa fa-remove pull-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>*}
                                
            <div class="row">
                <div class="col-xs-12">
                    <p class="poor-gray m-t10 b-Bottom p-b10">
                        <i class="fa fa-arrow-right true-green m-r10"></i>
                        {ci_language line="For incoming orders"}
                    </p>				
                </div>
            </div> 

            <div class="row">
                <div class="col-xs-12">
                    <div class="col-sm-6 p-l0">
                        <p class="montserrat text-info text-uc">
                            {ci_language line="Amazon carrier side"}
                        </p>   
                    </div>  
                    <div class="col-sm-6">
                        <p class="montserrat text-info text-uc">
                            {ci_language line="Feed.biz carrier side"}
                        </p>   
                    </div>            
                </div>
            </div> 

            
            <!--new-mapping-incoming-carrier-->
            <div id="new-mapping-incoming-carrier">
                {if isset($mapping_carrier['incoming'])}    
                    {$i = 0}                                               
                    {foreach from=$mapping_carrier['incoming'] key=vk item=attr}                    
                        {if $attr.selected == true}                                        
                            <div class="new-mapping-incoming-carrier row" {if $i == 0}id="main-incoming-carrier"{/if}>                             
                                <div class="col-sm-6 m-b20">
                                    <select rel="incoming-carrier-select" class="carrier-select-value search-select" data-content="{$vk}" id="incoming-carrier-select-{$vk}">
                                         <option value="">{ci_language line="Choose Amazon carrier side"}</option>
                                        {if isset($amazon_carrier_incoming)}
                                            {foreach from=$amazon_carrier_incoming key=ckey item=cvalue}
                                                <option value="{$ckey}" {if $ckey == $attr.mapping && $attr.selected == true}selected{/if}>
                                                    {$cvalue}
                                                </option>
                                            {/foreach} 
                                        {/if}
                                    </select> 
                                    <i class="blockRight noBorder"></i>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group withIcon clearfix">
                                        <select rel="incoming-carrier-value" class="carrier-value search-select" id="incoming-carrier-value-{$vk}" name="carrier[incoming][{$vk}][id_carrier]">
                                            <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                           {if isset($carriers)}
                                                {foreach from=$carriers key=akey item=value}
                                                    <option value="{$value.id_carrier}" {if isset($value.id_carrier_ref) && $value.id_carrier_ref == $attr.id_carrier_ref && $attr.selected == true}selected{/if}>
                                                        {$value.name}
                                                    </option>
                                                {/foreach} 
                                            {/if}
                                        </select>
                                        {if $i == 0}
                                            <i class="addnewmappingcarrier cb-plus good" rel="incoming"></i>
                                            <i class="removemappingcarrier cb-plus bad" rel="incoming" style="display:none"></i>
                                        {else}
                                            <i class="removemappingcarrier cb-plus bad" rel="incoming" style="cursor: pointer"></i>                                
                                        {/if}
                                    </div>
                                </div>
                            </div><!--row-->

                            {$i = $i+1}  
                        {/if}

                    {/foreach}
                {else}
                    <!--main-incoming-carrier-->
                    <div id="main-incoming-carrier" class="new-mapping-incoming-carrier row">
                        <div class="col-sm-6">
                            <select rel="incoming-carrier-select" class="carrier-select-value search-select disabled">
                                <option value=""><span style="color: #f5f5f5">{ci_language line="Choose Amazon carrier side"}</span></option>
                                {if isset($amazon_carrier_incoming)}
                                    {foreach from=$amazon_carrier_incoming key=ckey item=cvalue}
                                        <option value="{$ckey}">{$cvalue}</option>
                                    {/foreach} 
                                {/if}
                            </select> 
                            <i class="blockRight noBorder"></i>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group withIcon clearfix">
                                <select rel="incoming-carrier-value" class="carrier-value search-select disabled">
                                    <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                    {if isset($carriers)}
                                        {foreach $carriers key=akey item=value}
                                            <option value="{$akey}">{$value.name}</option>
                                        {/foreach} 
                                    {/if}
                                </select>
                                <i class="addnewmappingcarrier cb-plus good" rel="incoming"></i> 
                                <i class="removemappingcarrier cb-plus bad" rel="incoming" style="display:none"></i>
                            </div>
                        </div>
                    </div>
                {/if}
            </div> {*new-mapping-carrier*}

            <div class="p-t20">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="poor-gray b-Bottom p-b10">
                            <i class="fa fa-arrow-left true-pink m-r10 "></i>
                            {ci_language line="For Outgoing Orders"}
                        </p>                
                    </div>
                </div>                   
            </div>

             <div class="row">
                <div class="col-xs-12">
                    <div class="col-sm-6 p-l0">
                        <p class="montserrat text-info text-uc">
                            {ci_language line="Feed.biz carrier side"}
                        </p>   
                    </div>  
                    <div class="col-sm-6">
                        <p class="montserrat text-info text-uc">
                            {ci_language line="Amazon carrier side"}
                        </p>   
                    </div>            
                </div>
            </div> 

            <!--new-mapping-outgoing-carrier-->
            <div id="new-mapping-outgoing-carrier">
                {if isset($mapping_carrier['outgoing'])}      
                    {$j = 0}                                             
                    {foreach from=$mapping_carrier['outgoing'] key=vk item=attr}                        
                        {if $attr.selected == true}
                            <div class="row new-mapping-outgoing-carrier" {if $j == 0}id="main-outgoing-carrier"{/if}>
                                <div class="col-sm-6">
                                    <select rel="outgoing-carrier-select" class="carrier-select-value search-select" id="outgoing-carrier-select-{$vk}" data-content="{$vk}">
                                        <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                        {if isset($carriers)}
                                            {foreach from=$carriers key=akey item=value}
                                            <option value="{$value.id_carrier}" {if $value.id_carrier == $attr.id_carrier && $attr.selected == true}selected{/if}>
                                                {$value.name}
                                            </option>
                                            {/foreach} 
                                        {/if}
                                    </select>                   
                                    <i class="blockRight noBorder"></i>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group withIcon clearfix">
                                        <select rel="outgoing-carrier-value" class="carrier-value search-select" id="outgoing-carrier-value-{$vk}" name="carrier[outgoing][{$vk}][id_carrier]" data-content="{$vk}" >
                                            <option value="">{ci_language line="Choose Amazon carrier side"}</option>
                                            {if isset($amazon_carrier_outgoing)}
                                                {foreach from=$amazon_carrier_outgoing key=ckey item=cvalue}
                                                    <option value="{$ckey}" {if $cvalue == $attr.mapping && $attr.selected == true}selected{/if}>
                                                        {$cvalue}
                                                    </option>
                                                {/foreach} 
                                                <option value="Other" class="blue" {if $attr.mapping == "Other"}selected{/if}>
                                                    - {ci_language line="Other"} - 
                                                </option>
                                            {/if}
                                        </select>   
                                        {if $attr.mapping == "Other"}
                                           <input type="text" id="outgoing-carrier-other-{$vk}" name="carrier[outgoing][{$vk}][other]" rel="outgoing-carrier-other" value="{if isset($attr.other)}{$attr.other}{/if}" class="form-control m--t10">
                                        {/if}

                                        {if $j == 0}
                                            <i class="addnewmappingcarrier cb-plus good" rel="outgoing"></i>
                                            <i class="removemappingcarrier cb-plus bad" rel="outgoing" style="display:none"></i>
                                        {else}
                                            <i class="removemappingcarrier cb-plus bad" rel="outgoing" style="cursor: pointer"></i>                                
                                        {/if}
                                        <!-- <i class="removemappingcarrier cb-plus bad" rel="incoming"></i> -->
                                    </div>
                                </div>               
                            </div> 
                            {$j = $j+1}                             
                        {/if}
                    {/foreach}
                {else}
                    <!--main-outgoing-carrier-->
                    <div id="main-outgoing-carrier" class="new-mapping-outgoing-carrier row">
                        <div class="col-sm-6">
                            <select rel="outgoing-carrier-select" class="carrier-select-value search-select">
                                <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                {if isset($carriers)}
                                    {foreach $carriers key=akey item=value}
                                        <option value="{$akey}">{$value.name}</option>
                                    {/foreach} 
                                {/if}
                            </select>                   
                            <i class="blockRight noBorder"></i>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group withIcon clearfix">
                                <select rel="outgoing-carrier-value" class="carrier-value search-select">
                                    <option value=""><span style="color: #f5f5f5">{ci_language line="Choose Amazon carrier side"}</span></option>
                                    {if isset($amazon_carrier_outgoing)}
                                        {foreach from=$amazon_carrier_outgoing key=ckey item=cvalue}
                                            <option value="{$ckey}">{$cvalue}</option>
                                        {/foreach} 
                                    {/if}
                                    <option value="Other" class="blue"> - {ci_language line="Other"} - </option>
                                </select> 
                                <i class="addnewmappingcarrier cb-plus good" rel="outgoing"></i> 
                                <i class="removemappingcarrier cb-plus bad" rel="outgoing" style="display:none"></i>
                            </div>
                        </div>               
                    </div>
                {/if}
            </div> {*new-mapping-carrier*}

            {if isset($amazon_display[$ext]['shipping_override']) && $amazon_display[$ext]['shipping_override'] == 1}
                <div class="row">
                    <div class="col-xs-12">
                        <p class="head_text p-t20 p-b20 b-Top">
                           {ci_language line="Shipping Override"}
                        </p>
                    </div>
                </div>                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="validate blue b-Bottom p-b10">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="note"></i>
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left">
                                        {ci_language line="Amazon Shipping Method to override the default shipping charges when specified in product sheet"}.
                                    </p><br/>
                                    <p class="pull-left">
                                        {ci_language line="Please refer to the documentation to choose the appropriate value"}.
                                    </p>
                                    <i class="fa fa-remove pull-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="main-shipping-override">
                    <p class="montserrat text-info text-uc">{ci_language line="Standard"}</p>
                    <div class="col-sm-6 p-l0">
                        <div class="form-group">
                            <select id="shipping-override-standard" name="carrier[shipping_override][{$shipping_standard}]" class="search-select">
                                <option value="">{ci_language line="Choose Amazon carrier"}</option>
                                {if isset($amazon_shipping_override[{$shipping_standard}])}
                                    {foreach $amazon_shipping_override[{$shipping_standard}] key=akey item=value}
                                        <option value="{$value}"{if isset($shipping_override[$shipping_standard]['mapping']) && $shipping_override[$shipping_standard]['mapping'] == $value} selected{/if}>{$value}</option>
                                    {/foreach} 
                                {/if}
                            </select>                            
                        </div>
                    </div>
                </div>
            {/if} 
        </div>
    {/if} 
    
    {include file="amazon/PopupStepMoreFooter.tpl"} 
    
</form>
            
{include file="amazon/PopupStepFooter.tpl"}    
                        
<!--Message-->
<input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">         
<script src="{$cdn_url}assets/js/FeedBiz/amazon/carriers.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
