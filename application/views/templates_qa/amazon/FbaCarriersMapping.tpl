{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
       
<form id="form-mapping" method="post" action="{$base_url}amazon/fba/save_fba_carriers/{$id_country}" enctype="multipart/form-data" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 
    
    <!--Carrier--> 
        <div id="carrier-mapping">

            <div class="row">
                <div class="col-xs-12">
                    <p class="head_text p-t20 p-b20">
                       {ci_language line="Carriers Mapping"}
                    </p>
                </div>
            </div>   

            <div class="row">
                <div class="col-xs-12">
                    <div class="validate blue b-Bottom p-b10">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">
                                    {ci_language line="Associate as relevant as possible your Store's carrier with the Amazon carrier for FBA Multi-Channel."}
                                </p>                             
                                <i class="fa fa-remove pull-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <p class="montserrat text-info text-uc">
                        {ci_language line="Feed.biz carrier side"}
                    </p>   
                </div>            
                <div class="col-sm-6">
                    <p class="montserrat text-info text-uc">
                        {ci_language line="Amazon carrier side"}
                    </p>   
                </div>  
            </div> 

            
            <!--new-mapping-fba-carrier-->
            <div id="new-mapping-fba-carrier">
                {if isset($mapping_carrier['fba'])}    
                    {$i = 0}                                               
                    {foreach from=$mapping_carrier['fba'] key=vk item=attr}                    
                        {if $attr.selected == true}                                        
                            <div class="new-mapping-fba-carrier row" {if $i == 0}id="main-fba-carrier"{/if}>                             
                                <div class="col-sm-6 m-b20">
                                    <select rel="fba-carrier-select" class="carrier-select-value search-select" data-content="{$vk}" id="fba-carrier-select-{$vk}">
                                        <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                           {if isset($carriers)}
                                                {foreach from=$carriers key=akey item=value}
                                                    <option value="{$value.id_carrier}" {if isset($value.id_carrier_ref) && $value.id_carrier_ref == $attr.id_carrier_ref && $attr.selected == true}selected{/if}>
                                                        {$value.name}
                                                    </option>
                                                {/foreach} 
                                            {/if}
                                    </select> 
                                    <i class="blockRight noBorder"></i>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group withIcon clearfix">
                                        <select rel="fba-carrier-value" class="carrier-value search-select" id="fba-carrier-value-{$vk}" name="carrier[fba][{$vk}][id_carrier]">
                                            <option value="">{ci_language line="Choose Amazon carrier side"}</option>
                                        {if isset($amazon_carriers)}
                                            {foreach from=$amazon_carriers key=ckey item=cvalue}
                                                <option value="{$cvalue}" {if $cvalue == $attr.mapping && $attr.selected == true}selected{/if}>
                                                    {$cvalue}
                                                </option>
                                            {/foreach} 
                                        {/if}
                                        </select>
                                        {if $i == 0}
                                            <i class="addnewmappingcarrier cb-plus good" rel="fba"></i>
                                            <i class="removemappingcarrier cb-plus bad" rel="fba" style="display:none"></i>
                                        {else}
                                            <i class="removemappingcarrier cb-plus bad" rel="fba" style="cursor: pointer"></i>                                
                                        {/if}
                                    </div>
                                </div>
                            </div><!--row-->

                            {$i = $i+1}  
                        {/if}

                    {/foreach}
                {else}
                    <!--main-fba-carrier-->
                    <div id="main-fba-carrier" class="new-mapping-fba-carrier row">
                        <div class="col-sm-6"> 
                            <select rel="fba-carrier-select" class="carrier-select-value search-select disabled">
                                <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                                {if isset($carriers)}
                                    {foreach $carriers key=akey item=value}
                                        <option value="{$akey}">{$value.name}</option>
                                    {/foreach} 
                                {/if}
                            </select>                        
                            <i class="blockRight noBorder"></i>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group withIcon clearfix">
                                <select rel="fba-carrier-value" class="carrier-value search-select disabled">
                                    <option value=""><span style="color: #f5f5f5">{ci_language line="Choose Amazon carrier side"}</span></option>
                                    {if isset($amazon_carriers)}
                                        {foreach from=$amazon_carriers key=ckey item=cvalue}
                                            <option value="{$cvalue}">{$cvalue}</option>
                                        {/foreach} 
                                    {/if}
                                </select> 
                                <i class="addnewmappingcarrier cb-plus good" rel="fba"></i> 
                                <i class="removemappingcarrier cb-plus bad" rel="fba" style="display:none"></i>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>

        </div>        

    {include file="amazon/PopupStepMoreFooter.tpl"} 
    
</form>
            
{include file="amazon/PopupStepFooter.tpl"}    
                        
<!--Message-->   
<script src="{$cdn_url}assets/js/FeedBiz/amazon/carriers.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
