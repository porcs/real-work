{include file="header.tpl"}
{ci_config name="base_url"} 

<div class="p-rl40 p-xs-rl20">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="lightRoboto text-uc head-size light-gray">
                {ci_language line="Add Custom Value"}
            </h1>
        </div>
    </div>

    <form id="form-mapping-custom-value" method="post" enctype="multipart/form-data">
                      
        <div id="error-color" class="row" style="display: none;">
            <div class="col-xs-12 m-t10">
                <div class="b-Bottom">
                    <div class="validate pink">
                        <div class="validateRow">
                            <div class="validateCell"><i class="note"></i></div>
                            <div class="validateCell">
                                <p><span class="bold"></p>
                                <i class="fa fa-remove pull-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="m-b40 p-b40">
            {foreach $mappings as $universe => $mapping}   

                {if isset($mapping['product_type']) && !empty($mapping['product_type'])}
                    
                    {foreach $mapping['product_type'] as $product_type => $mapp}

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="head_text clearfix p-t15 p-b5 b-Top">
                                    <div class="row">
                                        {if isset($mappings[$universe]['translate'])}
                                            <p class="col-xs-6 p-b0">
                                                {$mappings[$universe]['translate']} > 
                                                {if isset($mappings[$universe]['product_type'][$product_type]['translate'])}
                                                    {$mappings[$universe]['product_type'][$product_type]['translate']}
                                                {/if}
                                            </p>
                                        {/if}
                                        {if isset($mappings[$universe]['name'])}
                                            <p class="col-xs-6 p-b0 {if isset($mappings[$universe]['translate'])}text-right{/if}">
                                                {$mappings[$universe]['name']} > 
                                                {if isset($mappings[$universe]['product_type'][$product_type]['name'])}
                                                    {$mappings[$universe]['product_type'][$product_type]['name']}
                                                {/if}
                                            </p>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        {if isset($mapp['attribute']) && !empty($mapp['attribute'])}
                            <div class="row m-t10">

                                {foreach $mapp['attribute'] as $map => $attribute}          
                                   
                                    {if isset($attribute['name'])}
                                        <div class="col-xs-12">
                                            <div class="col-xs-2">
                                                <label class="m-t10">
                                                    <span class="p-size tooltips" title="{$attribute['name']}">
                                                        {if isset($mappings[$universe]['product_type'][$product_type]['attribute'][$map]['translate'])}
                                                            {$mappings[$universe]['product_type'][$product_type]['attribute'][$map]['translate']}
                                                        {else}
                                                            {$attribute['name']}
                                                        {/if}
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col-xs-10">
                                                <div class="form-group">
                                                    <input class="form-control tags-input" type="text" data-role="tagsinput" name="custom_value[{$universe}][{$product_type}][{$map}]"{if isset($mappings[$universe]['product_type'][$product_type]['attribute'][$map]['custom_value'])}value="{$mappings[$universe]['product_type'][$product_type]['attribute'][$map]['custom_value']}"{/if} placeholder="{ci_language line="Add Value"}" />
                                                </div>
                                            </div>
                                        </div>
                                    {/if}

                                {/foreach}

                            </div>
                        {/if}

                    {/foreach}

                {/if}
                   
            {/foreach}
        </div>

        <div class="row popup_action">
            <div class="col-xs-12">
                <div class="p-t20">
                    <div class="inline pull-right">   
                        <input type="hidden" value="{$ext}" name="ext" />  
                        <input type="hidden" id="l_Custom_Value" value="{ci_language line="Custom Value"}" />  
                        <button class="btn btn-save" id="save_data" type="button">
                            {ci_language line="Save"}
                        </button>                                   
                    </div>
                </div>      
            </div> 
        </div>

    </form>
</div>
{include file="amazon/IncludeScript.tpl"}
<script type="text/javascript" src="{$cdn_url}assets/js/tagsinput/bootstrap-tagsinput.min.js"></script> 
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/mappings_custom_value.js"></script> 
{include file="footer.tpl"}