{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}


{include file="amazon/PopupStepHeader.tpl"}	             

<div class="row">	
    <div class="col-xs-12">
    	<div class="headSettings clearfix b-Top-None">
            <div class="col-xs-8{*pull-sm-left*} clearfix">
                <div class="row">
                    <h4 class="col-xs-1 p-0 headSettings_head">{ci_language line="Offers"}</h4>	
                    <div class="col-xs-10 clearfix">
                    	<div class="col-xs-5 p-0 "><input id="search_sku_offers" placeholder="{ci_language line="Search: SKU"}" class="form-control" /> </div>
                    	<div class="col-xs-5 p-0 m-l5"><input id="search_messgae_code_offers" placeholder="{ci_language line="Search: Message"}" class="form-control" /></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4{*pull-sm-right*} clearfix">
               <div class="showColumns clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
		        <table id="error_offers" class="amazon-responsive-table">
		            <thead class="table-head">
		                <tr>
		                	<th></th>
		                    <th>{ci_language line="Code"}</th>
		                    <th><span class="p-l20">{ci_language line="Message"}</span></th>
		                    <th>{ci_language line="Totals SKU"}</th>		                                       
		                </tr>
		            </thead>							
		        </table>
		        <table class="table tableEmpty">
		            <tr>
		                <td>{ci_language line="No data available in table"}</td>
		            </tr>
		        </table>
		    </div>
	    </div>
    </div>
</div>

<div class="row m-t40">   
    <div class="col-xs-12">
        <div class="headSettings clearfix b-Top">
            <div class="col-xs-8 clearfix">
                <div class="row">
                    <h4 class="col-xs-1 p-0 headSettings_head">{ci_language line="Products"}</h4> 
                    <div class="col-xs-10 clearfix">
                        <div class="col-xs-5 p-0 "><input id="search_sku_products" placeholder="{ci_language line="Search: SKU"}" class="form-control" /> </div>
                        <div class="col-xs-5 p-0 m-l5"><input id="search_messgae_code_products" placeholder="{ci_language line="Search: Message"}" class="form-control" /></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 clearfix">
               <div class="showColumns clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="error_products" class="amazon-responsive-table">
                    <thead class="table-head">
                        <tr>
                            <th></th>
                            <th>{ci_language line="Code"}</th>
                            <th><span class="p-l20">{ci_language line="Message"}</span></th>
                            <th>{ci_language line="Totals SKU"}</th>                                               
                        </tr>
                    </thead>                            
                </table>
                <table class="table tableEmpty">
                    <tr>
                        <td>{ci_language line="No data available in table"}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div style="display:none"> 

	<div id="error_header">
		<div class="" style="padding: 5px 15px;border: 1px solid #f4f4f4;border-radius: 6px;background-color: rgb(255, 240, 240); display: none;">
			<p class="solutions poor-gray {*b-Bottom*} p-t10 p-b10 m-0"></p>        			
		</div>
                
		<div class="pull-right p-0" >
            <div class="header-control m-t10">
                <div class="pull-right" style="display:none">
                    {*<a href="{$base_url}amazon/error_data_download_template/{$id_country}" type="button" class="btn btn-default m-b0 m-l5 download_template" target="_blank">{ci_language line="Download Template"}</a>*}
                    <button type="button" class="btn btn-default m-b0 m-l5 download_template" {*onclick=""*}>{ci_language line="Download Template"}</button>
                </div>
                <div class="pull-right upload-template-div m-l5"  style="display:none">
                    <form class="upload-template" method="POST" enctype="multipart/form-data" >
                        <label class="btn btn-save m-t0 m-b0 file_upload ">
                            <i class="fa fa-cloud-upload"></i> {ci_language line="Upload file"} 
                        </label>
                        <input type="file" class="file-upload" name="file-upload" multiple="multiple" />
                    <form>
                <div class="status" style="display: none">
                    <p class="p-t10 m-b10 m-l5">
                        <i class="fa fa-spinner fa-spin"></i> 
                        <span>{ci_language line="Uploading file"} ..</span>
                    </p>
                </div> 
                <div class="error" style="display: none">
                    <p class="p-t10 m-t0 m-b10 m-l5">
                        <i class="fa fa-exclamation-circle"></i>
                        <span></span>
                    </p>
                </div>
                </div>                        
                
                <div class="pull-right"  style="display:none">
                    <button type="button" class="btn btn-save m-b0 m-l5 edit_error" {*onclick=""*}>{ci_language line="Edit Attribute"}</button>
                </div>
                <div class="pull-right"  style="display:none">
                    <button type="button" class="btn btn-save m-b0 m-l5 override_error" {*onclick=""*}>{ci_language line="Override"}</button>
                </div>
                <div class="pull-right"  style="display:none">
                    <button type="button" class="btn btn-save m-b0 m-l5 accept_error" >{ci_language line="Solved"}</button>
                    <div class="status" style="display: none">
                        <p class="p-t10 m-b10 m-l5">
                            <i class="fa fa-spinner fa-spin"></i> 
                            <span>{ci_language line="Submitting"} ..</span>
                        </p>
                    </div> 
                    <div class="error" style="display: none">
                        <p class="p-t10 m-t0 m-b10 m-l5">
                            <i class="fa fa-exclamation-circle"></i>
                            <span></span>
                        </p>
                    </div>
                </div>
                                        
                <div class="pull-right" style="display:none">
                    <button type="button" class="btn btn-save m-b0 m-l5 recreate_error" rel="{$recreate}">{ci_language line="Delete & Recreate"}</button>
                    <div class="status" style="display: none">
                        <p class="p-t10 m-b10 m-l5">
                            <i class="fa fa-spinner fa-spin"></i> 
                            <span>{ci_language line="Submitting"} ..</span>
                        </p>
                    </div> 
                    <div class="error" style="display: none">
                        <p class="p-t10 m-t0 m-b10 m-l5">
                            <i class="fa fa-exclamation-circle"></i>
                            <span></span>
                        </p>
                    </div>
                </div>

                <div class="pull-right"  style="display:none">
                    <button type="button" class="btn btn-save m-b0 m-l5 creation_error" rel="{$creation}">{ci_language line="Creation"}</button>
                    <div class="status" style="display: none">
                        <p class="p-t10 m-b10">
                            <i class="fa fa-spinner fa-spin"></i> 
                            <span>{ci_language line="Submitting"} ..</span>
                        </p>
                    </div> 
                    <div class="error" style="display: none">
                        <p class="p-t10 m-t0 m-b10">
                            <i class="fa fa-exclamation-circle"></i>
                            <span></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<table id="error_details">
	    <thead class="table-head">
	        <tr>
                <th class="p-l10 tableId tableNoSort">
                    <label class="amazon-checkbox">
                        <span class="amazon-inner">
                            <i>
                                <input type="checkbox" class="check-all"/>
                            </i>
                        </span>
                    </label>                        
                </th>
	            <th>{ci_language line="SKU"}</th>
	            <th><div class="msg">{ci_language line="EAN/UPC"}</div></th>
	            <th>{ci_language line="Price"}</th>
	            <th>{ci_language line="Quantity"}</th>
	        </tr>
	    </thead>
	    <tbody></tbody>
	</table>
</div>

{include file="amazon/PopupStepFooter.tpl"}

<input type="hidden" id="processing" value="{ci_language line="Processing"}" />
<input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="offers" value="{if isset($offers)}{$offers}{/if}" />
<input type="hidden" id="products" value="{if isset($products)}{$products}{/if}" />
<input type="hidden" id="l_Success" value="{ci_language line="Success"}" />
<input type="hidden" id="l_Submit" value="{ci_language line="Submitting"}" />
<input type="hidden" id="l_Nothing_found" value="{ci_language line="Nothing found!"}" />
<input type="hidden" id="l_Error" value="{ci_language line="Error!"}" />
<input type="hidden" id="l_Message" value="{ci_language line="Message"}" />

<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/error_resolations.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/highchart/highstock.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
