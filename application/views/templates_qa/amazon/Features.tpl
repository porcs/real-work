{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
		                
<form id="amazon-configuration-features" method="post" action="{$base_url}amazon/save_configuration_features" enctype="multipart/form-data" autocomplete="off" >
    {include file="amazon/PopupStepMoreHeader.tpl"} 
	
	{if isset($amazon_display[$ext]['field_id']) && !empty($amazon_display[$ext]['field_id'])}
		<input type="hidden" name="field_id" value="{$amazon_display[$ext]['field_id']}" />
	{/if}

	<div class="row">
		<div class="col-xs-12">
			<div class="clearfix custom-form p-t20">
				<p class="montserrat text-info text-uc m-b20">
					{ci_language line="Catalog Features"}
				</p>				
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['creation']) && $amazon_display[$ext]['creation'] == 1}checked{/if}">
						<input type="checkbox" name="creation" value="1" {if isset($amazon_display[$ext]['creation']) && $amazon_display[$ext]['creation'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Products Creation"}.</p>
						<p class="m-l23">{ci_language line="Create new products on Amazon"}</p>
					</label>

					<label class="cb-checkbox {if isset($amazon_display[$ext]['create_out_of_stock']) && $amazon_display[$ext]['create_out_of_stock'] == 1}checked{/if}" > 
                        <input name="create_out_of_stock" type="checkbox" value="1" {if isset($amazon_display[$ext]['create_out_of_stock']) && $amazon_display[$ext]['create_out_of_stock'] == 1}checked{/if}/>
                        <p class="text-uc dark-gray montserrat m-b0">
                        	{ci_language line="Create Out Of Stock."}                        	
                        </p>
                    	{*if !isset($amazon_display[$ext]['creation']) || $amazon_display[$ext]['creation'] == 0}
                        	<span class="m-b0 error poor-grey help-text m-0" style="cursor: not-allowed;" >
                    			* {ci_language line="To active field, please activated Products Creation first."}
                        	</span>
                    	{/if*}

                        <p class="m-l23">{ci_language line="Allow to create the products even they are out of stock, including the combinations"}</p>

                    </label>
					
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['import_orders']) && $amazon_display[$ext]['import_orders']}checked{/if}">
						<input type="checkbox" name="import_orders" value="1" {if isset($amazon_display[$ext]['import_orders']) && $amazon_display[$ext]['import_orders']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Import Orders"}.</p>
						<p class="m-l23">{ci_language line="Activate import of orders"}</p>
					</label>

                    <label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['allow_send_order_cacelled']) && $amazon_display[$ext]['allow_send_order_cacelled'] == 1}checked{/if}">
                        <input name="allow_send_order_cacelled" type="checkbox" value="1" {if isset($amazon_display[$ext]['allow_send_order_cacelled']) && $amazon_display[$ext]['allow_send_order_cacelled'] == 1}checked{/if} />
                        <p class="text-uc dark-gray montserrat m-b0">
                        	{ci_language line="Order Cancelled."}
                        </p>                       
                        <p class="m-l23">{ci_language line="Allow to send \"Order Cancelled\" automatically to Shop"}</p>
                    </label>

					{*<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['allow_automatic_offer_creation']) && $amazon_display[$ext]['allow_automatic_offer_creation'] == 1}checked{/if}">
						<input type="checkbox" name="allow_automatic_offer_creation" value="1" {if isset($amazon_display[$ext]['allow_automatic_offer_creation']) && $amazon_display[$ext]['allow_automatic_offer_creation'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Create Offers"}.</p>
						<p class="m-l23">{ci_language line="Allows create offers automatically"}</p>
					</label>*}
					
				</div>
				<div class="col-sm-6">

					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['delete_products']) && $amazon_display[$ext]['delete_products']}checked{/if}">
						<input type="checkbox" name="delete_products" value="1" {if isset($amazon_display[$ext]['delete_products']) && $amazon_display[$ext]['delete_products']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Delete Products"}.</p>
						<p class="m-l23">{ci_language line="Activate delete products on Amazon"}</p>
					</label>

                    <label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['price_rules']) && $amazon_display[$ext]['price_rules'] == 1}checked{/if}">
						<input type="checkbox" name="price_rules" value="1" {if isset($amazon_display[$ext]['price_rules']) && $amazon_display[$ext]['price_rules'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Price Rules"}.</p>
						<p class="m-l23">{ci_language line="Adjust and format your prices for Amazon"}</p>
					</label>    
					
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['second_hand']) && $amazon_display[$ext]['second_hand'] == 1}checked{/if}">
						<input type="checkbox" name="second_hand" value="1" {if isset($amazon_display[$ext]['second_hand']) && $amazon_display[$ext]['second_hand'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Second Hand"}.</p>
						<p class="m-l23">{ci_language line="Sell second hand, collectible or refurbished products"}</p>
					</label>

				</div>							
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="clearfix custom-form b-Top p-t20">
				<p class="montserrat text-info text-uc m-b20">
					{ci_language line="Smart Features"}
				</p>	
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['repricing']) && $amazon_display[$ext]['repricing'] == 1}checked{/if}">
						<input type="checkbox" name="repricing" value="1" {if isset($amazon_display[$ext]['repricing']) && $amazon_display[$ext]['repricing'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Repricing"}.</p>
						<p class="m-l23">{ci_language line="Activate Repricing Features."}</p>
					</label>
				</div>
				{*<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($cron_send_orders) && $cron_send_orders}checked{/if}">
						<input type="checkbox" name="cron_send_orders" value="1" {if isset($cron_send_orders) && $cron_send_orders}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Smart Shipping"}.</p>
						<p class="m-l23">{ci_language line="Allows to calculate shipping by weight. For advanced users."}</p>
					</label>
				</div>*}
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['fba']) && $amazon_display[$ext]['fba'] == 1}checked{/if}">
						<input type="checkbox" id="fba" name="fba" value="1" {if isset($amazon_display[$ext]['fba']) && $amazon_display[$ext]['fba'] == 1}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Amazon FBA"}.</p>
						<p class="m-l23">{ci_language line="Activate Fulfillment by Amazon (FBA)."}</p>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="clearfix custom-form b-Top p-t20">				
				<p class="montserrat text-info text-uc m-b20">
					{ci_language line="Expert Mode"}
				</p>
				{*<div class="col-sm-12">	
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['activate']) && $amazon_display[$ext]['activate']}checked{/if}">
						<input type="checkbox" name="expert_mode_activate" value="1" {if isset($amazon_display[$ext]['activate']) && $amazon_display[$ext]['activate']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Activate Expert Mode"}.</p>
						<p class="m-l23">{ci_language line="Activate Expert Mode. Support is void whether activated without the agreement of the support"}</p>
					</label>
				</div>*}
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['code_examption']) && $amazon_display[$ext]['code_examption']}checked{/if}">
						<input type="checkbox" name="code_examption" value="1" {if isset($amazon_display[$ext]['code_examption']) && $amazon_display[$ext]['code_examption']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Code Exemption"}.</p>
						<p class="m-l23">{ci_language line="Activate a GCID Code Exemption"}</p>
					</label>	
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['synchronization_field']) && $amazon_display[$ext]['synchronization_field']}checked{/if}">
						<input type="checkbox" name="synchronization_field" value="1" {if isset($amazon_display[$ext]['synchronization_field']) && $amazon_display[$ext]['synchronization_field']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Synchronization field"}.</p>
						<p class="m-l23">{ci_language line="Synchronization field"}</p>
					</label>		
				</div>
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['sku_as_supplier_reference']) && $amazon_display[$ext]['sku_as_supplier_reference']}checked{/if}">
						<input type="checkbox" name="sku_as_supplier_reference" value="1" {if isset($amazon_display[$ext]['sku_as_supplier_reference']) && $amazon_display[$ext]['sku_as_supplier_reference']}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="SKU as supplier reference"}.</p>
						<p class="m-l23">{ci_language line="SKU as supplier reference"}</p>
					</label>		
					{if (isset($mode_default) && $mode_default >= 2)}
						<label class="cb-checkbox w-100 {if isset($amazon_display[$ext]['shipping_override']) && $amazon_display[$ext]['shipping_override'] == 1}checked{/if}">
							<input type="checkbox" name="shipping_override" value="1" {if isset($amazon_display[$ext]['shipping_override']) && $amazon_display[$ext]['shipping_override'] == 1}checked{/if} style="opacity: 0;" />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Shipping Override"}.</p>
							<p class="m-l23">{ci_language line="Allows shipping override."}</p>
						</label>
					{/if}
				</div>
			</div>
		</div>
	</div>
	
    {include file="amazon/PopupStepMoreFooter.tpl"}
</form>

{include file="amazon/PopupStepFooter.tpl"}
{include file="amazon/IncludeScript.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/amazon/features.js"></script>
{include file="footer.tpl"}