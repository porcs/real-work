{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}    
    
    <form action="{$base_url}amazon/save_category{if isset($popup)}/{$popup}{/if}" method="post" id="category" autocomplete="off" novalidate>
        {include file="amazon/PopupStepMoreHeader.tpl"} 
        
        <div id="category">

            <div class="row">
                <div class="col-xs-12 m-b10">
                    <div class="headSettings clearfix b-Bottom b-Top-None p-0">
                        <h4 class="headSettings_head pull-sm-left p-t10">{ci_language line="Choose categories"}</h4>                                     
                        <ul class="pull-sm-right clearfix treeSettings">
                            <li class="expandAll"><i class="cb-plus"></i>{ci_language line="Expand all"}</li>
                            <li class="collapseAll"><i class="cb-minus"></i>{ci_language line="Collapse all"}</li>
                            <li class="checkBaAll"><i class="cb-checked"></i>{ci_language line="Check all"}</li>
                            <li class="uncheckBaAll"><i class="cb-unchecked"></i>{ci_language line="Uncheck all"}</li>
                        </ul>
                    </div>
                </div>
            </div><!-- header -->

             <div id="has_category" class="row">
                <div class="col-xs-12">
                    <div class="validate blue">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">
                                    {ci_language line="The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized."}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--sub - header -->

            <div id="none_category" class="row" style="display:none">
                <div class="col-xs-12">{$no_category}</div> 
                {* 
                    <div class="validate yellow">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">{ci_language line="Please choose category in My feeds > Parameters > Category"}</p>
                            </div>
                        </div>
                    </div>
                *}
            </div>

            <div class="tree clearfix custom-form m-0"> 

                <i class="icon-folder"></i>
                <p class="treeHead treeRight">{ci_language line="Profiles Mapping"}</p>
                
                {if $prifile_mapping}
                    <div id="main" style="display: none;">
                        <div class="treeRight selectTree">
                            <div class="tree_show" style="display: block;">
                                <select class="form-control profile">
                                    <option value=""> - - </option>
                                    {if (isset($profile) && !empty($profile) )}
                                        {foreach from=$profile key=pkey item=pvalue}
                                            <option value="{$pvalue.id_profile}" {if isset($selected) && $selected == $pvalue.id_profile} selected {/if}>{$pvalue.name}</option>
                                        {/foreach}  
                                    {/if}
                                </select>                        
                                <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this));"></i>
                            </div>
                        </div>
                    </div> 
                {/if}

                <ul id="tree1" class="tree_point m-t10"></ul>
                
            </div>

            <div class="m-t10 p-0"></div>

        </div> <!-- category -->
                
    {include file="amazon/PopupStepMoreFooter.tpl"}
    
</form>    

{include file="amazon/PopupStepFooter.tpl"}    


<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/FeedBiz/amazon/category.js"></script> 
<script src="{$cdn_url}assets/js/jquery.blockUI.js"></script>

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}