{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#repricing" data-toggle="tab" class="">
                    <i class="icon-dollar"></i> <span>{ci_language line="FBA"}</span>
                </a>
            </li>                                     
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <div class="tab-pane active" id="fba">                
                <div class="status" style="display: none">
                    <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                </div>
                
                <!--fba estimated-->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="fba_estimated"> 
                            <p class="head_text montserrat black p-t20 b-None">{ci_language line="FBA Estimated"}</p>
                                    
                            <div class="p-0 col-md-8">
                                <ul class="p-l20">
                                    <li class="col-xs-12 p-0">
                                        <p class="poor-gray">{ci_language line='This will get fba estimated fba fees data.'}</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="p-0 col-md-4">              
                                <button class="btn btn-rule m-b5{if $fba_master_platform == 0} disabled{/if}" id="fba_estimated" type="button" value="fba_estimated">
                                    {ci_language line="FBA Estimated"}
                                </button>                                        
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting FBA Estimated"} ..</p>
                                </div>
                                <div class="error" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>                   
                        </div>                   
                    </div>                   
                </div>

                <hr>

                <!--fba manager-->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="fba_manager"> 
                            <p class="head_text montserrat black b-None">{ci_language line="FBA Manager"}</p>
                                    
                            <div class="p-0 col-md-8">
                                <ul class="p-l20">
                                    <li class="col-xs-12 p-0">
                                        <p class="poor-gray">{ci_language line='This will listting and manage your FBA.'}</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="p-0 col-md-4">
                                <button class="btn btn-rule m-b5{if $fba_master_platform == 0} disabled{/if}" id="fba_manager" type="button" value="fba_manager">
                                    {ci_language line="FBA manager"}
                                </button>                                        
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting FBA manager"} ..</p>
                                </div>
                                <div class="error" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>

                <hr>

                <!--fba Orders Status-->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="fba_orders_status"> 
                            <p class="head_text montserrat black b-None">{ci_language line="FBA Orders Status"}</p>
                                    
                            <div class="p-0 col-md-8">
                                <ul class="p-l20">
                                    <li class="col-xs-12 p-0">
                                        <p class="poor-gray">{ci_language line='Update FBA orders status.'}</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="p-0 col-md-4">
                                <button class="btn btn-rule m-b5" id="fba_orders_status" type="button" value="fba_orders_status">
                                    {ci_language line="FBA Orders Status"}
                                </button>                                        
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting FBA Orders Status"} ..</p>
                                </div>
                                <div class="error" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>                   
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
    
<input type="hidden" value="{ci_language line="Are you sure to get fba estimated fba fees data?"}" id="fba-estimated-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to listting your FBA?"}" id="fba-manager-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to send FBA Orders Status?"}" id="fba-orders-status-confirm" />

<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}