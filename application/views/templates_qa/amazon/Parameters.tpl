{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
<form id="form-api-setting" method="post" action="{$base_url}amazon/parameters" enctype="multipart/form-data" autocomplete="off" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 

    {*if (isset($mode_default) && $mode_default == 2)*}
        <div class="row">
            <div class="col-xs-12">
                <div class="m-t20 custom-form">
                    {*<div class="col-sm-6 col-md-6 col-lg-6">*}
                        <p class="montserrat dark-gray text-uc">{ci_language line="Preference"}</p>
                        {*<label class="amazon-checkbox w-100 {if isset($api_settings['allow_automatic_offer_creation']) && $api_settings['allow_automatic_offer_creation'] == 1}checked{/if}">
                            <span class="amazon-inner">
                                <i>
                                    <input name="allow_automatic_offer_creation" type="checkbox" value="1" {if isset($api_settings['allow_automatic_offer_creation']) && $api_settings['allow_automatic_offer_creation'] == 1}checked{/if} />
                                </i>
                            </span>
                            <p class="text-uc dark-gray montserrat m-b0">{ci_language line="Allow automatic offer creation."}</p>
                        </label>
                        <label class="amazon-checkbox w-100">
                            <input name="allow_automatic_offer_creation" type="checkbox" value="1" {if isset($api_settings['allow_automatic_offer_creation']) && $api_settings['allow_automatic_offer_creation'] == 1}checked {/if} />
                            {ci_language line="Allow automatic offer creation."}
                        </label> *} 
                        
                        <div class="">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="amazon-checkbox w-100 {if isset($api_settings['synchronize_quantity']) && $api_settings['synchronize_quantity'] == 1}checked{elseif empty($api_settings)}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input name="synchronize_quantity" type="checkbox" value="1" {if isset($api_settings['synchronize_quantity']) && $api_settings['synchronize_quantity'] == 1}checked{elseif empty($api_settings)}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="Quantity."}</p>
                                    <p class="m-l23">{ci_language line="Allow to send (synchronize) stock to amazon"}{$ext}</p>
                                </label>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <label class="amazon-checkbox w-100 {if isset($api_settings['synchronize_price']) && $api_settings['synchronize_price'] == 1}checked{elseif empty($api_settings)}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input name="synchronize_price" type="checkbox" value="1" {if isset($api_settings['synchronize_price']) && $api_settings['synchronize_price'] == 1}checked{elseif empty($api_settings)}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="Price."}</p>
                                    <p class="m-l23">{ci_language line="Allow to send (synchronize) price to amazon"}{$ext}</p>
                                </label>
                            </div>
                        </div>
                        {*<div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="">
                                <label class="amazon-checkbox w-100 {if isset($api_settings['allow_send_order_cacelled']) && $api_settings['allow_send_order_cacelled'] == 1}checked{elseif empty($api_settings)}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input name="allow_send_order_cacelled" type="checkbox" value="1" {if isset($api_settings['allow_send_order_cacelled']) && $api_settings['allow_send_order_cacelled'] == 1}checked{elseif empty($api_settings)}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="Order Cancelled."}</p>
                                    <p class="m-l23">{ci_language line="Allow to send \"Order Cancelled\" automatically to Shop"}</p>
                                </label>
                            </div>
                            <div class="">
                                <label class="amazon-checkbox w-100 {if isset($api_settings['create_out_of_stock']) && $api_settings['create_out_of_stock'] == 1}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input name="create_out_of_stock" type="checkbox" value="1" {if isset($api_settings['create_out_of_stock']) && $api_settings['create_out_of_stock'] == 1}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="Create Out Of Stock."}</p>
                                    <p class="m-l23">{ci_language line="Allow to create the products even they are out of stock, including the combinations"}</p>
                                </label>
                            </div>   
                            
                        </div>*}
                    {*</div>*}
                    {*<div class="col-sm-6 col-md-4 col-lg-3">
                        <p class="montserrat dark-gray text-uc">{ci_language line="Mode"}</p>
                        <label class="amazon-checkbox w-100 {if isset($api_settings['creation']) && $api_settings['creation'] == 1}checked{/if} ">
                            <span class="amazon-inner">
                                <i>
                                    <input name="creation" type="checkbox" value="1" {if isset($api_settings['creation']) && $api_settings['creation'] == 1}checked="checked"{/if} />
                                </i>
                            </span>
                            {ci_language line="Creation Mode"}
                        </label>
                    </div> *} 
                </div>
            </div>
        </div>
    {*/if*}

    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-tb10 m-b10">
                 {ci_language line="API settings"}
            </p>
        </div>
    </div>
    
    {*<div class="row">
        <div class="col-xs-12">
            <div class="validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="help"></i>
                    </div>
                    <div class="validateCell">
                        <p class="pull-left">
                            <span class="bold">Please read our online tutorial :</span>                         
                                <a href="http://documentation.common-services.com/feedbiz/amazon-get-access-data" class="link" target="_blank" title="learn more">http://documentation.common-services.com/feedbiz/amazon-get-access-data</a>
                        </p>                      
                    </div>
                </div>
            </div>
        </div>
    </div>*}

    <div class="row m-t10">
        <div class="col-sm-4 form-group">
            <p class="text-uc dark-gray montserrat m-b5 p-0">{ci_language line="Application Name"} : </p>
            <p class="poor-gray">
                Feed.biz &nbsp; 
                <a class="link copy_to_clipboard" title="{ci_language line="copy to cliboard"}">
                    <i class="fa fa-copy"></i>
                    <input type="hidden" value="Feed.biz" />
                </a>
            </p>
        </div>
        <div class="col-sm-4 form-group">
            <p class="text-uc dark-gray montserrat m-b5 p-0">{ci_language line="Developer Account Number"} : </p>
            {if isset($developer_account_number)}
                <p class="poor-gray">
                    {$developer_account_number} &nbsp; 
                    <a class="link copy_to_clipboard"  title="{ci_language line="copy to cliboard"}">
                        <i class="fa fa-copy"></i>
                        <input type="hidden" value="{$developer_account_number}" />
                    </a>
                </p>
            {/if}
        </div>
        <div class="col-sm-4 form-group">
            <button type="button" class="{*link p-size*}btn btn-check m-0 m-t10" id="get_merchant_id">
                    <i class="icon-cloudaccess"></i> {ci_language line="Get Access Data"} 
            </button>
        </div>
    </div>

    <div class="row m-t10">        
        <div class="col-sm-4 form-group">
                <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="Seller ID"} *</label>            
                <input class="form-control col-md-12" type="text" id="merchant_id" name="merchant_id" {if isset($api_settings['merchant_id'])}value='{$api_settings['merchant_id']}'{/if}/>
                <input type="hidden" id="aws_id" name="aws_id" {if isset($aws_id)}value='{$aws_id}'{/if}/>
                <input type="hidden" id="secret_key" name="secret_key" {if isset($secret_key)}value='{$secret_key}'{/if}/>
        </div>
        <div class="col-sm-4 form-group m-b10">
                <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="MWS Auth Token"}</label>            
                <input class="form-control col-md-12" type="text" id="auth_token" name="auth_token" {if isset($api_settings['auth_token'])}value='{$api_settings['auth_token']}'{/if}/>
            <!--  -->
        </div>
        <div class="col-sm-4 form-group">
                <button type="button" id="api_check" class="btn btn-check">
                    <i class="icon-check"></i>
                    <span id="check">{ci_language line="Check Connectivity"}</span>
                    <span id="checking" style="display:none">{ci_language line="Checking .."}</span>
                </button>
                <!-- help -->
                <div id="status-message" style="display:none">
                    <p></p>
                </div>
        </div>
    </div>

    <div class="row m-b10 custom-form"> 
        <div class="col-sm-4">
            <p class="pull-left poor-gray m-t3 m-r10">{ci_language line="Active"} :</p>
            <div class="cb-switcher pull-left">
                <label class="inner-switcher">
                    <input name="active" id="debug" type="checkbox" value="1" {if (isset($api_settings['active']) && $api_settings['active'] == 1) || !isset($api_settings['active'])}checked="checked"{/if} data-state-on="ON" data-state-off="OFF">
                </label>
                <span class="cb-state">{ci_language line="ON"}</span>
            </div>
        </div>
     </div>

    <div class="row alert-code" style="display:none"></div>

    <div class="row alert-message m-t10 m-b10" style="display:none">
        <div class="col-xs-12">
            <div class="validate pink m-t10">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i>
                    </div>
                    <div class="validateCell">
                        <div class="pull-left code"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {include file="amazon/PopupStepMoreFooter.tpl"} 
                
    <!--Message--> 
    <input type="hidden" id="connect-fail" value="{ci_language line="Connection to Amazon Failed"}" />
    <input type="hidden" id="connect-error" value="{ci_language line="Data connection error! Please contact your support, error code:"}" />
    <input type="hidden" value="{ci_language line="Loading"}.." id="loading">  
    <input type="hidden" value="{ci_language line="Copied"}.." id="l_Copied">  
    <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">    
</form>    
            
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.zclip.min.js"></script>            
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/parameters.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}    
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}