{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}         
    
    <input type="hidden" value="{if isset($country)}{str_replace(array(' '), '_', $country)}{/if}" id="country">
    <input type="hidden" value="{$id_country}" id="id_country">
    
    <div id="main_result" style="display: none;">
        <h5 class="header montserrat dark-gray text-uc m-t15">{ci_language line="ID submission"} - <span></span></h5>
        <ul class="msg_value list-unstyled">                
            <li class="processed clearfix">                
                <div class="pull-left"><p class="light-gray m-b5">{ci_language line="Processed"}</p></div>
                <div class="pull-right"><p class="light-gray m-b5"><span></span> {ci_language line="item(s)"}</p></div>
            </li>
            <li class="success clearfix">
                <div class="pull-left"><p class="light-gray m-b5">{ci_language line="Success"}</p></div>
                <div class="pull-right"><p class="light-gray m-b5"><span></span> {ci_language line="item(s)"}</p></div>
            </li>
            <li class="warning clearfix">
                <div class="pull-left"><p class="light-gray m-b5">{ci_language line="Warning"}</p></div>
                <div class="pull-right"><p class="light-gray m-b5"><span></span> {ci_language line="item(s)"}</p></div>
            </li>   
            <li class="error clearfix">
                <div class="pull-left"><p class="light-gray m-b5">{ci_language line="Error"}</p></div>
                <div class="pull-right"><p class="light-gray m-b5"><span></span> {ci_language line="item(s)"}</p></div>
            </li>               
            <li class="error_report showSelectBlock">
                
                <div class="m-b10">
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="head_text p-t20 b-None m-0">
                                <span class="p-t10">{ci_language line="Error report"}</span> 
                                <i id="error_report_block" class="fa fa-caret-down" style="cursor: pointer;"></i>
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="error_logs amazon-responsive-table showSelectBlock">
                                <thead class="table-head">
                                    <tr>
                                        <th class="center" width="15%">{ci_language line="SKU"}</th>
                                        <th class="center" width="10%">{ci_language line="Code"}</th>
                                        <th class="center">{ci_language line="Message"}</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </li>
        </ul>
    </div>
    
    <div class="row-fluid">
        
        <div class="clearfix text-center" id="page-load">
            <div class="feedbizloader m-t20"><div class="feedbizcolor"></div></div>        
            <h4 class="m-t20"></h4>
        </div>
        
        <div class="clearfix" id="debugs"></div>            
        
        <!--Loading-->
        <div id="loading" style="display: none" class="m-b20">

            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix text-center m-t20" id="loading-content">
                        <div class="feedbizloader m-t20"><div class="feedbizcolor"></div></div>        
                        <h4 class="m-t20"></h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div id="msg_title" class="headSettings clearfix b-Bottom b-Top-None text-center">
                        <h4 class="headSettings_head"></h4>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">   
                    <div id="skip-content" class="amazon-content p-10 m-t5 m-b5 clearfix" style="display:none">
                        <div class="col-xs-2">
                            <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Statistics"} : </h5>
                        </div>
                        <div class="col-xs-10">
                            <p id="no_send" class="m-t10 m-b10 light-gray"></p>
                            <p id="no_skipped" class="m-t10 m-b10 light-gray" style="display:none">
                                <span></span> 
                                <button class="btn btn-link show-skip-details">
                                    <i class="fa fa-caret-down"></i> {ci_language line="view detail"}
                                </button>
                            </p>
                            <p id="skip-details" class="m-t10 m-b10 light-gray" style="display:none"></p>                            
                        </div>
                    </div>
                </div>
            </div>

            <div id="amazon-content" class="b-Top" style="display: none;"> 

                <!--Product-->
                <div id="msg_product" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Product feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Inventory-->
                <div id="msg_inventory" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Inventory feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Price-->
                <div id="msg_price" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Price feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Image-->
                <div id="msg_image" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Image Feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Relationship-->
                <div id="msg_relationship" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Relationship Feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Shipping override-->
                <div id="msg_shipping_override" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Shipping override Feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

                <!--Reprice-->
                <div id="msg_reprice" class="b-Bottom p-10 m-t5 m-b5 clearfix disabled" style="display: none;">
                    <div class="col-xs-2">
                        <h5 class="montserrat dark-gray text-uc m-t10">{ci_language line="Repricing Feed"} : </h5>
                    </div>
                    <div class="col-xs-10">
                        <div class="msg_status">
                            <div class="pull-right m--t5">
                                <img src="{$cdn_url}assets/images/loader-connection.gif"/> 
                            </div>
                            <p class="m-t10 m-b10 light-gray"> - </p>
                        </div>   
                        <div class="msg_result"></div>
                    </div>
                </div>

            </div>

        </div>
                
        <!--message-->           
        <input type="hidden" id="Waiting" value="{ci_language line="Wait to process"}" />
        <input type="hidden" id="NothingToSend" value="{ci_language line="Nothing to send"}" />
        <input type="hidden" id="Preparing-product" value="{ci_language line="Preparing product to send"}" />
        <input type="hidden" id="Preparing-inventory" value="{ci_language line="Preparing inventory to send"}" />
        <input type="hidden" id="Preparing-price" value="{ci_language line="Preparing price to send"}" />
        <input type="hidden" id="Preparing-image" value="{ci_language line="Preparing image to send"}" />
        <input type="hidden" id="Preparing-relationship" value="{ci_language line="Preparing relationship to send"}" />
        <input type="hidden" id="Connecting-to-Amazon" value="{ci_language line="Connecting to Amazon"}.." />

    </div><!--row-fluid-->  
            
{include file="amazon/PopupStepFooter.tpl"}
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">          

<!--inline scripts related to this page-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/send.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"} 