{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#synchronize" data-toggle="tab" class="">
                    <i class="icon-creation-up"></i> <span>{ci_language line="Images"}</span>
                </a>
            </li>                
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <!--Synchronization-->
            <div id="synchronize" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-10">
                                                 
                        <div id="sync_options">
                            <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
                    
                            <p class="head_text montserrat black p-t20 b-None">{ci_language line="Send Images Only"}</p>
                            
                            <div class="p-0 col-md-10">
                                <ul class="p-l20">
                                    <li class="col-xs-12 p-0">
                                        <p class="poor-gray">{ci_language line='This will update Amazon Images, within the selected categories in your configuration.'}</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="p-0 col-md-2 text-right">
                                    <div class="{*pull-sm-right*}" id="sync_send_images_only">
                                        <button class="btn btn-amazon" id="send_images_only" type="button" value="sync_send_images_only">
                                            <i class="icon-amazon"></i> {ci_language line="Send Images Only"}
                                        </button>                                        
                                        <div class="status" style="display: none">
                                            <p>{ci_language line="Starting send images"} ..</p>
                                        </div>
                                        <div class="error" style="display: none">
                                            <p class="m-t0">
                                                <i class="fa fa-exclamation-circle"></i>
                                                <span></span>
                                            </p>
                                        </div>
                                    </div>
                            </div>
                                
                        </div>      
                    </div>
                </div>
            </div>
             
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
    
<input type="hidden" value="{ci_language line="Are you sure to send relations?"}" id="send_relations_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to send images?"}" id="send_images_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to get orders?"}" id="get-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update order shipments?"}" id="update-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to delete product?"}" id="delete-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 

<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
