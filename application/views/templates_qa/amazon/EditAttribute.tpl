{*include file="header.tpl"*}
{ci_config name="base_url"}

        
<div class="p-rl40 p-xs-rl20 m-t30 p-t10">
	
	<!-- Head -->
	<h1 class="lightRoboto text-uc head-size light-gray b-Bottom p-b10 m-b20">
	    {ci_language line="Attribute Override"}
	</h1>

	<form id="amazon-error-resolation" method="post" enctype="multipart/form-data" autocomplete="off" >
		{*<pre>{print_r($sku, true)}</pre>*}
		{if isset($sku) && !empty($sku)}
			{foreach $sku as $k}
				<div class="attribute row">
					<div class="col-xs-12">
						<div class="head_text clearfix p-t15 p-b10 m-b20">
							<input type="hidden" rel="key" value="{$k['key']}">
		                    {if isset($k['name'])}
			                    <p class="m-0">{$k['name']}</p>
			                    <input type="hidden" name="attribute_overide[{$k['key']}][sku]" rel="sku" value="{$k['name']}">
		                    {/if}
		                    {if isset($k['id_product'])}
			                    <input type="hidden" name="attribute_overide[{$k['key']}][id_product]" rel="id_product" value="{$k['id_product']}">
		                    {/if}
		                    {if isset($k['id_product_attribute'])}
			                    <input type="hidden" name="attribute_overide[{$k['key']}][id_product_attribute]" rel="id_product_attribute" value="{$k['id_product_attribute']}">
		                    {/if}
		                    {if isset($k['id_model'])}
			                    <input type="hidden" name="attribute_overide[{$k['key']}][id_model]" rel="id_model" value="{$k['id_model']}">
		                    {/if}
		                </div>
		            </div>

		            {if isset($k['missing_model']) && !empty($k['missing_model'])}
		            	<div class="attribute-overide col-xs-12">
		            		<p class="p-size poor-gray">{$k['missing_model']}</p>
		            	</div>
		            {else}

		            	{if isset($k['attribute']) && !empty($k['attribute'])}
		            		{$i = 0}
		            		{foreach $k['attribute'] as $attribute_key => $a}
		            			{if !isset($a['attribute_field'])}
		            				{$a['attribute_field'] = ''}
		            			{/if}
								<div class="attribute-overide col-xs-12">
								    <div class="col-sm-6">			
								        <select class="search-select" rel="attribute_field" name="attribute_overide[{$k['key']}][attribute][{$a['attribute_field']}][attribute_field]"> 
								        	{if isset($k['amazon_attribute']) && !empty($k['amazon_attribute'])}
												{foreach $k['amazon_attribute'] as $amazon_attribute => $val}
													<option value="{$amazon_attribute}" {if isset($a['attribute_field']) && $amazon_attribute == $a['attribute_field']}selected{/if}>
														{preg_replace( '/([a-z0-9])([A-Z])/', "$1 $2", $amazon_attribute)}
													</option>
												{/foreach}
											{/if}
										</select>
								        <i class="blockRight noBorder"></i>
								    </div>
								    <div class="col-sm-6">
								        <div class="form-group withIcon clearfix">
											<input type="text" class="form-control" rel="override_value" placeholder="{ci_language line="Your Override"}" value="{if isset($a['Amazon_value'])}{$a['Amazon_value']}{/if}" name="attribute_overide[{$k['key']}][attribute][{$a['attribute_field']}][override_value]" />
								            {if $i == 0}
								            	<i class="cb-plus good add_more_attribute"></i> 
								            {/if}
								            <i class="cb-plus bad remove_attribute" {if $i == 0}style="display:none"{/if}></i>
								        </div>
								    </div>
								</div>
								{$i = $i+1}
							{/foreach}
						{/if}

					{/if}
				</div>
			{/foreach}
		{/if}
		<input type="hidden" name="error_code" id="error_code" value="{if isset($error_code)}{$error_code}{/if}" />
		<input type="hidden" name="action_type" id="action_type" value="{if isset($action_type)}{$action_type}{/if}" />
		<input type="hidden" name="id_shop" id="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />
		<input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
		
		{*include file="amazon/PopupStepMoreFooter.tpl"*}

		<div class="row action">
	        <div class="col-xs-12">
	            <div class="b-Top p-t20">
	                <div class="inline pull-right">  
	                    <!--Message--> 
	                    <input type="hidden" id="country" value="{if isset($country)}{$country}{/if}" />	                       
	                    <input type="hidden" id="ext" name="ext" value="{$ext}" />
                        <button class="btn btn-save" id="save_data" type="submit" >
                            {ci_language line="Save"}
                        </button>
	                </div>
	            </div>		
	        </div>
	    </div>

	</form>

</div>	

<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/edit_attribute.js"></script>

{include file="amazon/IncludeScript.tpl"}
{*include file="footer.tpl"*}	