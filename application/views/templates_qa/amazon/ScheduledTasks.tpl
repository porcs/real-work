{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}

{*<form id="amazon-status" method="post" action="{$base_url}amazon/save_status" enctype="multipart/form-data" autocomplete="off" >*}

    {include file="amazon/PopupStepMoreHeader.tpl"} 

	{*<div class="row">
		<div class="col-xs-12">
			<div class="headSettings p-t10 b-Bottom">
				<h4 class="headSettings_head">{ci_language line="Scheduled Tasks"}</h4>
			</div>
		</div>			
	</div>*}

        <div class="row">
        <div class="col-xs-12">
            
             
            <div class="row">
                <div class="col-xs-12 statistics">
                    <table class="responsive-table custom-form" id="my_task_list" ext="{$ext}" market="{$market}">
                        <thead class="table-head">
                            <tr>
                                <th><label class="cb-checkbox"><input class="sel_all" type="checkbox" ><span class="th_txt">{ci_language line="Allow automatic update"}</span></label></th>
                                <th>{ci_language line="Tasks"}</th>
                                <th>{ci_language line="Checked time"}</th>
                                 <th class='center-align'>{ci_language line="Status"}</th>
                                <th class='center-align'>{ci_language line="Next time"}</th>
                            </tr>
                        </thead>
                        {if isset($tasks)}
                        <tbody>
                            {foreach from=$tasks key=key item=value}
                                <tr class="task_row" rel="{$value['task_name']}">
                                    <td><label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="{$value['task_key']}" crel='{$value['task_id']}' {if $value['task_running'] == true}checked{/if}>{$value['task_display_name']}</label></td>
                                    <td>{ci_language line=$value['task_name']}</td>
                                    <td class="t_time">{$value['task_lastupdate']}</td>
                                     <td class='center-align'><div class="t_status {$value['task_status']}"></div></td>
                                    <td class='center-align'><div class="countdown_next" rel="{$countdown[$value['task_key']]}"></div></td>
                                    
                                </tr>
                            {/foreach}
                        </tbody>
                        {/if}
                    </table>

                </div>
            </div>
        </div>			
    </div>
	{*<!--Offers-->
	<div class="row">
		<div class="col-xs-12">
			<div class="custom-form b-Top">
				<p class="montserrat text-info text-uc m-t20">
					{ci_language line="Offers"}
				</p>				
				<div class="col-sm-6">
					<label class="cb-checkbox w-100 {if isset($cron_update_offers) && $cron_update_offers}checked{/if}">
						<input type="checkbox" name="cron_update_offers" value="1" {if isset($cron_update_offers) && $cron_update_offers}checked{/if} style="opacity: 0;" />
						<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Allow automatic update offer"}.</p>
						<p class="m-l23">{ci_language line="Automatic update your stock levels on Amazon every hour"}</p>
					</label>
				</div>	
			</div>
		</div>	
	</div>

	<!--Orders-->
	{if isset($amazon_display[$ext]['import_orders']) && $amazon_display[$ext]['import_orders'] == 1}
		<div class="row">
			<div class="col-xs-12">
				<div class="custom-form m-t20">
					<p class="montserrat text-info text-uc">
						{ci_language line="Orders"}
					</p>	
					<div class="col-sm-6">
						<label class="cb-checkbox w-100 {if isset($cron_send_orders) && $cron_send_orders}checked{/if}">
							<input type="checkbox" name="cron_send_orders" value="1" {if isset($cron_send_orders) && $cron_send_orders}checked{/if} style="opacity: 0;" />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Allow automatic export orders to shop"}.</p>
							<p class="m-l23">{ci_language line="Automatic import of your orders every hour"}</p>
						</label>				
					</div>
					<div class="col-sm-6">
						<label class="cb-checkbox w-100 {if isset($cron_update_order_status) && $cron_update_order_status}checked{/if}">
							<input type="checkbox" name="cron_update_order_status" value="1" {if isset($cron_update_order_status) && $cron_update_order_status}checked{/if} style="opacity: 0;" />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Allow send status to Amazon"}.</p>
							<p class="m-l23">{ci_language line="Automatic update your order status on Amazon every hour"}</p>
						</label>
					</div>
				</div>
			</div>
		</div>
	{/if}


	{if isset($amazon_display[$ext]['creation']) && $amazon_display[$ext]['creation']}
		<div class="row">
			<div class="col-xs-12">
				<div class="custom-form m-t20">
					<p class="montserrat text-info text-uc">
						{ci_language line="Product Creation"}
					</p>				
					<div class="col-sm-6">
						<label class="cb-checkbox w-100 {if isset($cron_create_products) && $cron_create_products}checked{/if}">
							<input type="checkbox" name="cron_create_products" value="1" {if isset($cron_create_products) && $cron_create_products}checked{/if} style="opacity: 0;" />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Alllow automatic creation of New products"}.</p>
							<p class="m-l23">{ci_language line="Automatically create new products on Amazon every day"}</p>
						</label>
					</div>					
				</div>
			</div>
		</div>
	{/if}

	{if isset($amazon_display[$ext]['delete_products']) && $amazon_display[$ext]['delete_products'] == 1}
		<div class="row">
			<div class="col-xs-12">
				<div class="custom-form m-t20">
					<p class="montserrat text-info text-uc">
						{ci_language line="Delete"}
					</p>				
					<div class="col-sm-6">
						<label class="cb-checkbox w-100 {if isset($cron_delete_products) && $cron_delete_products}checked{/if}">
							<input type="checkbox" name="cron_delete_products" value="1" {if isset($cron_delete_products) && $cron_delete_products}checked{/if} style="opacity: 0;" />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="Allow Automatic deletion of products"}.</p>
							<p class="m-l23">{ci_language line="Automatically delete the selected products on Amazon every days"}</p>
						</label>			
					</div>
				</div>
			</div>
		</div>
	{/if}*}

    {include file="amazon/PopupStepMoreFooter.tpl"}

{*</form>*}

{include file="amazon/PopupStepFooter.tpl"}
{include file="amazon/IncludeScript.tpl"}


<input type="hidden" id="processing" value="Processing...">
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/amazon/schedule_task.css" />
<script language="javascript" src="{$base_url}assets/js/jquery.blockUI.js"></script> 
<script src="{$base_url}assets/js/countdown.js"></script> 
 
 <script language="javascript" src="{$base_url}assets/js/FeedBiz/amazon/schedule_task.js"></script> 
{include file="footer.tpl"}

