{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#creation" data-toggle="tab" class="">
                    <i class="icon-creation-up"></i> <span>{ci_language line="Products"}</span>
                </a>
            </li>                        
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
                                      
            <!--creation-->
            <div class="tab-pane active" id="creation">
                <div class="row">
                    <div class="col-sm-10">                        
                        <ul class="m-t15 p-l20">
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Products Wizard"}</b></p>
                                <p class="poor-gray">                                        
                                    {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.'}
                                </p>
                                <p class="poor-gray">                                        
                                    {ci_language line='This will send all the products having a profile and within the selected categories in your configuration.'}
                                </p>
                            </li>
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Update products"}</b></p>
                                <p class="poor-gray">
                                    {ci_language line='This will update Amazon depending your products, within the selected categories in your configuration.'}
                                </p>
                            </li>
                        </ul>
                        <div id="creat_options" class="clearfix"> 
                            <div class="pull-sm-left m-t20">
                                <button class="btn btn-wizard wizard_btn" href="{$base_url}/amazon/parameters/{$id_country}/2/{$create_mode}" id="amazon_creation_wizard">
                                    <i class="icon-wizard"></i> {ci_language line="Products Wizard"}
                                </button>									
                            </div>
                                <div class="pull-sm-right m-t20">
                                    <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
                                    <button class="btn btn-amazon m-r5" id="send-products" type="button" value="synchronize">
                                        <i class="icon-amazon"></i> {ci_language line="Update products"}
                                    </button>                                        
                                    <div class="status" style="display: none">
                                        <p>{ci_language line="Starting update"} ..</p>
                                    </div>
                                    <div class="error" style="display: none">
                                        <p class="m-t0">
                                            <i class="fa fa-exclamation-circle"></i>
                                            <span></span>
                                        </p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
    
<input type="hidden" value="{ci_language line="Are you sure to update products?"}" id="send-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 

<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
