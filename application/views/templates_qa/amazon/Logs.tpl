{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}        

<div class="clearfix">
	<div class="amazonLogs">
		<div class="logs m-t20">
			<h4 class="logs_head">{ci_language line="Latest Updates"}</h4>
		</div>
		<div class="logs">
			<h5 class="logs_text">{ci_language line="Normal Update"}</h5>
			<div class="row">
				<div class="col-xs-1">
					<i class="icon-log_date"></i>
				</div>
				<div class="col-xs-5 logsContent">
					<p class="logs_text-content"><span>{ci_language line="Date"}</span></p>
					{if $last_normal_update.date_upd == "Never"}
						<p class="logs_text-content">0000 - 00 - 00</p>
					{else}
						<p class="logs_text-content">{date('Y - m - d', strtotime($last_normal_update.date_upd))}</p>							
					{/if}
				</div>
				<div class="col-xs-5">
					<p class="logs_text-content"><span>{ci_language line="Time"}</span></p>
					<p class="logs_text-content">{date('H : i : s', strtotime($last_normal_update.date_upd))}</p>
				</div>
			</div>
		</div>
		<div class="logs drown">
			<h5 class="logs_text">{ci_language line="Cron update"}</h5>
			<div class="row">
				<div class="col-xs-1">
					<i class="icon-log_clock"></i>
				</div>
				<div class="col-xs-5 logsContent">
					<p class="logs_text-content"><span>{ci_language line="Date"}</span></p>
					{if $last_cron_update.date_upd == "Never"}
						<p class="logs_text-content">0000 - 00 - 00</p>		
					{else}
						<p class="logs_text-content">{date('Y - m - d', strtotime($last_cron_update.date_upd))}</p>							
					{/if}					
				</div>
				<div class="col-xs-5">
					<p class="logs_text-content"><span>{ci_language line="Time"}</span></p>
					<p class="logs_text-content">{date('H : i : s', strtotime($last_cron_update.date_upd))}</p>
				</div>
			</div>
		</div>
	</div>	

	<div class="amazonTableLogs">
		<p class="head_text p-t25 b-None m-0 col-xs-3">
			<span>{ci_language line="History"}</span>
		</p>
		<form id="logs-filter" method="post" enctype="multipart/form-data" autocomplete="off" >
			<p class="head_text p-t25 b-None m-0  col-xs-3">
				<span>{ci_language line="From"}</span>
				<input type="text" class="inputDate form-control logs-filter" name="date_from" id="date_from">  
			</p>
			<p class="head_text p-t25 b-None m-0  col-xs-3">
				<span>{ci_language line="To"}</span>
				<input type="text" class="inputDate form-control logs-filter" name="date_to" id="date_to"> 
			</p>
			<p class="head_text p-t25 b-None m-0 col-xs-3 p-r0">
				<span>{ci_language line="Action"}</span>
				<select  class="search-select logs-filter" data-placeholder="Choose" name="action_type" id="action_type">

					<option value="">--</option>
					{if isset($filter)}
						{foreach $filter as $fil}
	                		<option value="{$fil['field']}">{ci_language line="{$fil['value']}"}</option>	                   
						{/foreach}
					{/if}
	            </select>  
			</p>
		</form>

		<table id="main-logs" style="display: none;">
			<tr>
				<td class="tableId" data-th="Date / Time:" data-name="" width="15%">
					<p class="logRow date"></p>
					<p class="logRow time"></p>
				</td>
				<td class="tableUserName" data-th="Action:" width="25%">
					<p class="logRow action_type"><span></span></p>
					<p class="logRow batch_id_log"><button type="button" value="{if isset($data) && isset($data.batch_id)}{$data.batch_id}{/if}" class="link batch_id"></button></p>
				</td>							
				<td class="tableStatus status_log" data-th="Status:" width="15%"></td>
				<td class="tableJoined result_log" data-th="Joined:" width="45%">
					<p class="logRow"></p>
				</td>
			</tr>
		</table>

		<table class="table tableLog m-b0">
			<thead class="table-head">
				<tr>
					<th class="tableId" width="15%">{ci_language line="Date / Time"}</th>
					<th class="tableUserName" width="30%">{ci_language line="Action"}</th>
					<th class="tableStatus" width="15%">{ci_language line="Status"}</th>
					<th class="tableJoined" width="40%">{ci_language line="Result"}</th>
				</tr>
			</thead>				
		</table>
		<div id="history_log" style="height: 280px; overflow: hiddden">
			<table id="history_log_inner" class="table tableLog">				
				<tbody>					
					{if isset($history)}
	                    {foreach $history as $data}
							<tr>
								<td class="tableId" data-th="Date / Time:" data-name="" width="15%">
									<p class="logRow date">{$data.date_upd_date}</p>
									<p class="logRow time">{$data.date_upd_time}</p>
								</td>
								<td class="tableUserName" data-th="Action:" width="25%">
									<p class="logRow action_type"><span>{$data.action_type}{if $data.is_cron == 1} ({ci_language line="cron"}){/if}.</span></p>
									<p class="logRow batch_id_log"><button type="button" value="{$data.batch_id}" class="link batch_id">{$data.batch_id}</button></p>
								</td>							
								<td class="tableStatus status_log" data-th="Status:" width="15%">
		                            {if $data.action_type != "Import Orders"}  
	                                    {if isset($data.detail.error) && !empty($data.detail.error)}
	                                        <p class="logRow logError">
	                                        	<i class="fa fa-warning red"></i>  {ci_language line="Error"}
	                                    	</p>
	                                    {else}
											<p class="logRow logSuccess">
		                                        <i class="fa fa-check green"></i> {ci_language line="Success"}
											</p>
	                                    {/if}
	                                {else}
	                                	<p class="logRow logSuccess">
	                                        <i class="fa fa-check green"></i> {ci_language line="Success"}
										</p>
		                            {/if}
								</td>
								<td class="tableJoined result_log" data-th="Result:" width="45%">
									<p class="logRow">
		                                <!--Order Status-->
		                                {if $data.action_type == "FBA Order"}

		                                	{if isset($data.detail.error)}
	                                        	{$data.detail.error}
                                        	{else}
			                                	{if isset($data.detail.type) && !empty($data.detail.type)} 
			                                		{ci_language line="FBA Order"} {$data.detail.type} <br/> 
			                                	{/if}
			                                	{ci_language line="Result"} :  			                                  
			                                    {if isset($data.no_process)}{($data.no_process-$data.no_skipped)} {ci_language line="success orders"} {/if}  
			                                    {if isset($data.no_skipped)}, {$data.no_skipped} {ci_language line="skipped orders"} {/if}
                                        	{/if}

		                                {elseif $data.action_type == "FBA Manager"}

		                                	{if isset($data.detail.error)}
	                                        	{$data.detail.error}
                                        	{else}
			                                	{if isset($data.detail.type) && !empty($data.detail.type)} {ci_language line="Behaviour"} : {$data.detail.type} <br/> {/if}
			                                	{ci_language line="Process"} {$data.no_process} {ci_language line="events"}, 
			                                    <!--Number of switching to AFN-->
			                                    {*if isset($data.no_success)}{$data.no_success}{/if} {ci_language line="items switching to AFN"}, 
		                                    	<!--Number of switching to MFN-->
			                                    {if isset($data.no_error)}{$data.no_error}{/if} {ci_language line="items switching to MFN"*}  
			                                    <!--Number of Skipped-->
			                                    {if isset($data.no_skipped)}{$data.no_skipped}{/if} {ci_language line="skipped items"} 
                                        	{/if}

										{else if $data.action_type == "Import Orders"}       

		                                    {if isset($data.detail.order_status)} 
		                                        {if $data.detail.order_status == "All"}{ci_language line='Retrieve all pending orders'}{/if}
		                                        {if $data.detail.order_status == "Pending"}
		                                            {ci_language line='Pending - This order is pending in the market place'}
		                                        {/if}
		                                        {if $data.detail.order_status == "Unshipped"}{ci_language line='Unshipped - This order is waiting to be shipped'}{/if}
		                                        {if $data.detail.order_status == "PartiallyShipped"}
		                                            {ci_language line='Partially shipped - This order was partially shipped'}
		                                        {/if}
		                                        {if $data.detail.order_status == "Shipped"}{ci_language line='Shipped - This order was shipped'}{/if}
		                                        {if $data.detail.order_status == "Canceled"}{ci_language line='Cancelled - This order was cancelled'}{/if}
		                                    {/if}
		                                    <!--Order Date Range-->
		                                    {if isset($data.detail.order_range)}{ci_language line="from"} : {$data.detail.order_range} {/if}
	                                       	<br/>
	                                       	{ci_language line="Result"} : 
		                                    {$data.no_process} {ci_language line="import orders"}, 
		                                    <!--Number of Success Order-->
		                                    {if isset($data.no_success) && $data.no_success > 0}{$data.no_success}{else}0{/if} {ci_language line="success orders"}, 
		                                    <!--Number of Skipped Order-->
		                                    {if isset($data.no_process) && isset($data.no_success) && (($data.no_process - $data.no_success) > 0)} 
		                                        {$data.no_process - $data.no_success} 
		                                    {else} 0 {/if} {ci_language line="skipped orders"}                                                 
			                             <!--Feeds-->
			                            {else}

			                            	{if isset($data.detail.error)}
	                                        	{$data.detail.error}
                                        	{else}
			                                    {if $data.action_type == "Export Orders"}  
			                                        {$data.no_process} 
			                                        {ci_language line="orders to send"}, 
			                                        {$data.no_success} 
			                                        {ci_language line="success"}, 
			                                        {$data.no_error} 
			                                        {ci_language line="skipped"}
			                                    {else}
			                                    	{*if isset($data.no_process)}
				                                    	{$data.no_process} 
				                                        {ci_language line="process"}, 
			                                        {/if*}
			                                    	{if isset($data.no_send)}
			                                        	{$data.no_send} 
			                                        	{ci_language line="items to send"}, 
		                                        	{/if}
			                                        {if isset($data.no_skipped)} 
			                                        	{$data.no_skipped} {ci_language line="skipped items"}
			                                        {/if} 
			                                    {/if}
		                                    {/if}

			                            {/if}
									</p>
								</td>
							</tr>
						{/foreach}
	                {/if}  											
				</tbody>
			</table>
		</div>
	</div>
</div>
 
 <p class="m-t30 b-None p-0"></p>           

<div class="row">
	<div class="col-xs-12">
	    
	    <div class="headSettings clearfix">
	        <div class="pull-sm-left clearfix">
	            <h4 class="headSettings_head">{ci_language line="Skipped Details"}</h4>                      
	        </div>
	        <div class="pull-sm-right clearfix">
	           <div class="showColumns clearfix"></div>
	        </div>          
	    </div><!--headSettings-->

	    <div class="row">
	        <div class="col-xs-12">	            
	            <table id="product_logs" class="amazon-responsive-table">
	                <thead class="table-head">
	                    <tr>
	                        <th class="center">{ci_language line="Batch ID"}</th>
	                        <th class="center">{ci_language line="Action Process"}</th>
	                        <th class="center">{ci_language line="Action type"}</th>
	                        <th class="center">{ci_language line="Messages"}</th>
	                        <th class="center">{ci_language line="Date"}</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    {if isset($logs)}
	                        {foreach from=$logs key=key item=value}
	                            <tr>
	                                <td>{*<a href="{$base_url}amazon/downloadLog/{$id_country}/{$id_shop}/{$value.batch_id}">*}{$value.batch_id}{*</a>*}</td>
	                                <td>{$value.action_process}</td>
	                                <td>{$value.action_type}</td>
	                                <td>{$value.messages} {if $value.action_type == "warning"}[{ci_language line="Skipped"}]{/if}</td>
	                                <td>{$value.datetime}</td>
	                            </tr>
	                        {/foreach}
	                    {else}
	                         <tr>
	                             <td></td>
	                             <td></td>
	                             <td></td>
	                             <td></td>
	                             <td></td>
	                        </tr>
	                    {/if}
	                </tbody>                        
	            </table><!--table-->
	            
	        </div><!--col-xs-12-->
	    </div><!--row-->
	    
	</div><!--col-xs-12-->
</div><!--row--> 
                                                    
<input type="hidden" id="order_status_All" value="{ci_language line='Retrieve all pending orders'}" />
<input type="hidden" id="order_status_Pending" value="{ci_language line='Pending - This order is pending in the market place'}" />
<input type="hidden" id="order_status_Unshipped" value="{ci_language line='Unshipped - This order is waiting to be shipped'}" />
<input type="hidden" id="order_status_PartiallyShipped" value="{ci_language line='Partially shipped - This order was partially shipped'}" />
<input type="hidden" id="order_status_Shipped" value="{ci_language line='Shipped - This order was shipped'}" />
<input type="hidden" id="order_status_Canceled" value="{ci_language line='Cancelled - This order was cancelled'}" />
<input type="hidden" id="success_log" value="{ci_language line='Success'}" />
<input type="hidden" id="success_order" value="{ci_language line='success orders'}" />
<input type="hidden" id="form_log" value="{ci_language line='form'} : " />
<input type="hidden" id="result_log" value="{ci_language line="Result"} : " />
<input type="hidden" id="skipped_order" value="{ci_language line="skipped orders"}" />
<input type="hidden" id="items_to_send" value="{ci_language line="items to send"}" />
<input type="hidden" id="skipped_items" value="{ci_language line="skipped items"}" />
<input type="hidden" id="import_orders" value="{ci_language line="import orders"}" />
<input type="hidden" id="orders_to_send" value="{ci_language line="orders to send"}" />
<input type="hidden" id="success" value="{ci_language line="success"}" />
<input type="hidden" id="skipped" value="{ci_language line="skipped"}" />
<input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="batch_id" value="{if isset($batch_id)}{$batch_id}{/if}" />
<input type="hidden" id="l_Error" value="{ci_language line="Error"}" />

<input type="hidden" id="fba_process" value="{ci_language line="Process"}" />
<input type="hidden" id="behaviour" value="{ci_language line="Behaviour"}" />
<input type="hidden" id="events" value="{ci_language line="events"}" />
<input type="hidden" id="items_switching_to_AFN" value="{ci_language line="items switching to AFN"}" />
<input type="hidden" id="items_switching_to_MFN" value="{ci_language line="items switching to MFN"}" />
<input type="hidden" id="FBA_Order" value="{ci_language line="FBA Order"}" />

{include file="amazon/IncludeDataTableLanguage.tpl"}
{include file="amazon/PopupStepFooter.tpl"}  
        
<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/logs.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"} 