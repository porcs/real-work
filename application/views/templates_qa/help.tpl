{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/help.css" />

<div id="content" class="main" style="margin-left: 300px;">
    <div class="p-rl40 p-xs-rl20">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Help Center"}</h1>
        {*include file="breadcrumbs.tpl"*} <!--breadcrumb-->

        <div class="row">

            {if isset($search_empty) && $search_empty}

            {/if}

            {if isset($search) || isset($search_empty) }

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Online Tutorials"}</h4>                                                     
                        </div>
                    </div>

                    <div class="col-xs-12 m-l25">
                        <h4 class="light-gray">{ci_language line="Search Result"} : </h4>
                        {if isset($search_empty) && $search_empty} 
                            <div class="col-sm-12">
                                <p class="poor-gray">{ci_language line="Your search returned no matches."}</p>   
                            </div>
                        {/if}

                        {if !empty($search)}
                            <div class="col-sm-4">
                                <ul class="list-circle-green m-t10">
                                    {$mod = (sizeof($search) <= 1) ? 3 : round(sizeof($search)/3) }
                                    {foreach $search as $msg name=populartutorial}
                                        <li>
                                            <a class="link" href="{$msg['message_cause']}" target="_blank">{ucwords(strtolower(str_replace('_', ' ' ,$msg['message_name'])))}</a>
                                        </li>
                                        {if $smarty.foreach.populartutorial.iteration%($mod) == 0}
                                        </ul>
                                    </div>

                                    <div class="col-sm-4">
                                        <ul class="list-circle-green m-t10">
                                        {/if}
                                    {/foreach}
                                </ul>                            
                            </div>
                        {/if}
                        <div class="col-xs-12 m-b30">
                            <a href="http://documentation.common-services.com/feedbiz/sitemap" class="more m-b30" target="_blank">{ci_language line="All Tutorials"}</a>
                        </div>

                    </div>
                </div>

            {/if}

            {if !isset($search)}
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Presentation"}</h4>						
                        </div>
                    </div>
                    <div class="col-xs-12 m-l25">
                        <div class="col-sm-10">
                            <div class="panel-group" id="presents">
                                {if isset($presents)}
                                    {foreach $presents as $present name=presentshow}
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                {*<h4 class="panel-title">
                                                <ul class="list-circle-green m-t10">*}
                                                {*<li><a class="link" href="{$base_url}help/presentation/how-does-it-works">{ci_language line="How does it works"}</a></li>*}
                                                {*                                                    <li></li>*}
                                                <p><a class="link" {*href="{$base_url}help/presentation/{$present['message_name']|replace:' ':'-'|lower}">*} data-toggle="collapse" data-parent="#accordion" href="#{$present['message_type']}_{$present['message_name']|replace:' ':'-'|lower}" {*rel="{$present['message_cause']}"*}>{$present['message_name']}</a></p>
                                                    {*</ul>
                                                    </h4>*}
                                            </div>
                                            <div id="{$present['message_type']}_{$present['message_name']|replace:' ':'-'|lower}" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <video poster="/assets/images/cover.jpg" controls="controls" width="100%" height="100%">
                                                        <source src="{$present['message_cause']}" type="video/mp4" />
                                                        {if $present['message_problem']}<track src="{$base_url}assets/videos/cc/{$present['message_problem']}" kind="subtitles" srclang="{$present['message_language']}" label="English">{/if}
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Configuration"}</h4>						
                        </div>
                    </div>
                    <div class="col-xs-12 m-l25">
                        <div class="col-sm-10">
                            {*<ul class="list-circle-yellow m-t10">
                            <li><a class="link" href="{$base_url}help/configuration/{$config['message_name']|replace:' ':'-'|lower}">{$config['message_name']}</a></li>
                            </ul>*}
                            <div class="panel-group" id="config">
                                {if isset($configs)}
                                    {foreach $configs as $config name=presentshow}
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <p><a class="link collapsed" data-toggle="collapse" data-parent="#config" href="#{$config['message_type']}_{$config['message_name']|replace:' ':'-'|lower}" rel="{$config['message_cause']}">{$config['message_name']}</a></p>
                                            </div>
                                            <div id="{$config['message_type']}_{$config['message_name']|replace:' ':'-'|lower}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <video poster="/assets/images/cover.jpg" controls="controls" width="100%" height="100%">
                                                        {*<source src="{$config['message_cause']}" type="video/mp4" />*}
                                                        {if $config['message_problem']}<track src="{$base_url}assets/videos/cc/{$config['message_problem']}" kind="subtitles" srclang="{$config['message_language']}" label="English">{/if}
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{ci_language line="Marketplaces"}</h4>						
                        </div>
                    </div>
                    <div class="col-xs-12 m-l25">
                        <div class="col-sm-10">
                            <h4 class="dark-gray bold">{ci_language line="Amazon"}</h4>
                            <h4 class="light-gray m-l20">{ci_language line="Tutorials"}</h4>
                            {*<ul class="list-circle-blue list-2 m-t10 m-l35">
                            <li><a class="link" href="{$base_url}help/amazonmarketplace/{$amazonmarket['message_name']|replace:' ':'-'|lower}">{$amazonmarket['message_name']}</a></li>
                            </ul>*}
                            <div class="panel-group" id="amazonmarket">
                                {if isset($amazonmarkets)}
                                    {foreach $amazonmarkets as $amazonmarket name=presentshow}
                                        <div class="panel panel-default m-l25">
                                            <div class="panel-heading">
                                                <p><a class="link collapsed" data-toggle="collapse" data-parent="#amazonmarket" href="#{$amazonmarket['message_type']}_{$amazonmarket['message_name']|replace:' ':'-'|lower}" rel="{$amazonmarket['message_cause']}">{$amazonmarket['message_name']}</a></p>
                                            </div>
                                            <div id="{$amazonmarket['message_type']}_{$amazonmarket['message_name']|replace:' ':'-'|lower}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <video poster="/assets/images/cover.jpg" controls="controls" width="100%" height="100%">
                                                        {*<source src="{$amazonmarket['message_cause']}" type="video/mp4" />*}
                                                        {if $amazonmarket['message_problem']}<track src="{$base_url}assets/videos/cc/{$amazonmarket['message_problem']}" kind="subtitles" srclang="{$amazonmarket['message_language']}" label="English">{/if}
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                            <h4 class="dark-gray bold">{ci_language line="Ebay"}</h4>
                            <h4 class="light-gray m-l20">{ci_language line="Tutorials"}</h4>
                            {*<ul class="list-circle-blue list-2 m-t10 m-l35">
                            <li><a class="link" href="{$base_url}help/ebaymarketplace/{$ebaymarket['message_name']|replace:' ':'-'|lower}">{$ebaymarket['message_name']}</a></li>
                            </ul>*}

                            <div class="panel-group" id="ebaymarket">
                                {if isset($ebaymarkets)}
                                    {foreach $ebaymarkets as $ebaymarket name=presentshow}
                                        <div class="panel panel-default m-l25">
                                            <div class="panel-heading">
                                                <p><a class="link collapsed" data-toggle="collapse" data-parent="#ebaymarket" href="#{$ebaymarket['message_type']}_{$ebaymarket['message_name']|replace:' ':'-'|lower}" rel="{$ebaymarket['message_cause']}">{$ebaymarket['message_name']}</a></p>
                                            </div>
                                            <div id="{$ebaymarket['message_type']}_{$ebaymarket['message_name']|replace:' ':'-'|lower}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <video poster="/assets/images/cover.jpg" controls="controls" width="100%" height="100%">
                                                        {*<source src="{$ebaymarket['message_cause']}" type="video/mp4" />*}
                                                        {if $ebaymarket['message_problem']}<track src="{$base_url}assets/videos/cc/{$ebaymarket['message_problem']}" kind="subtitles" srclang="{$ebaymarket['message_language']}" label="English">{/if}                                                    
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
            {/if}

        </div>
    </div>
</div>
<!-- // Content END -->

<!-- // DIV END in HEADER -->
</div>
</div>
<!-- // DIV END in HEADER -->
<script src="{$cdn_url}assets/js/FeedBiz/help.js"></script>
{include file="footer.tpl"}