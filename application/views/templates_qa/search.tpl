{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
    
    <input type="hidden" value="{if isset($search_string)}{$search_string}{/if}" id="search-string"/> 
    <input type="hidden" value="{if isset($menus)}{$menus}{/if}" id="menu-string"/> 
    <input type="hidden" value="{ci_language line="less"}" id="see_less"/> 
    <input type="hidden" value="{ci_language line="more"}" id="see_more"/>
    
    <h1 class="lightRoboto text-uc head-size light-gray">
        {$class} {$function}
    </h1> 
    
    {include file="breadcrumbs.tpl"} 
            
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 m-b20 b-Bottom">
                <h4 class="headSettings_head text-norm">
                    {ci_language line="Your query : "} <span class="poor-gray">"{if isset($search_string)}{$search_string}{/if}"</span>
                </h4>						
            </div>
        </div>
    </div>
            
    <div class="row">
        
        <div class="col-sm-6 col-lg-3" style="display: none;">
            <h4 class="text-uc dark-gray bold">{ci_language line="Menus"}</h4>
            <div id="search-menu">
                <ul class="list-circle-blue search-more">
                    <li id="search-menu-main" class="link" style="display: none;"></li>
                </ul>
                <a class="m-b10 see_more more">{ci_language line="more"}</a>
            </div>
        </div>
        
        {if isset($logs) && !empty($logs)}
        <div class="col-sm-6 col-lg-3">
            <h4 class="text-uc dark-gray bold">Logs</h4>
                <ul class="list-circle-yellow search-more">
                    {foreach $logs as $log}
                        {foreach $log as $log_message}
                            <li>{$log_message['name']} : {$log_message['message']} ({$log_message['batch_id']})..</li>
                        {/foreach}
                    {/foreach}
                </ul>
                <a href="{$base_url}my_feeds/messages" class="m-b10 more">{ci_language line="more"}</a>
        </div>
        {/if}
        
        <div class="col-sm-6 col-lg-3">
            <h4 class="text-uc dark-gray bold">Histories</h4>
            <ul class="list-circle-green">
                <li>... Invalid Carriers Name ...</li>
                <li>... Invalid Carriers Name ...</li>
                <li>... Invalid Carriers Name ...</li>
                <li>... Invalid Carriers Name ...</li>
                <li>... Invalid Carriers Name ...</li>					
            </ul>
            <a href="" class="m-b10 more">more</a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <h4 class="text-uc dark-gray bold">Products</h4>
            <ul class="list-circle-red">
                <li><a href="" class="link">Products Name Carrier ...</a></li>
                <li><a href="" class="link">Products Name Carrier ...</a></li>
                <li><a href="" class="link">Products Name Carrier ...</a></li>				
            </ul>
            <a href="" class="m-b10 more">more</a>
        </div>
        
    </div>
                
    <div class="row">
        <div class="col-sm-4">
            <div class="pullBorder p-20 m-t20">
                <p class="text-uc dark-gray bold">{ci_language line="Search Products"}</p>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-group form-group m-0">								
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-save blue-Border m-0" type="submit">{ci_language line="Search"}</button>
                            </span>					
                        </div>
                    </div>
                </div>
            </div>							
        </div>
        <div class="col-sm-4">
            <div class="pullBorder p-20 m-t20">
                <p class="text-uc dark-gray bold">{ci_language line="Search Orders"}</p>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-group form-group m-0">								
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-save blue-Border m-0" type="submit">{ci_language line="Search"}</button>
                            </span>					
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="pullBorder p-20 m-t20">
                <p class="text-uc dark-gray bold">{ci_language line="Search Help"}</p>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="input-group form-group m-0">								
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-save blue-Border m-0" type="submit">{ci_language line="Search"}</button>
                            </span>					
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
                            
</div>

<script src="{$cdn_url}assets/js/FeedBiz/search.js"></script>     
{include file="footer.tpl"}