{include file="header.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/users/forgot_password.css" />

<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <form action="{$base_url}users/forgotten_password" method="post" id="forgot_pass_form" autocomplete="off">
                        <div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">{ci_language line="retrieve_password"}</h4>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">{ci_language line="enter_your_email_to_receive_instructions"}</p>
						<div class="form-group">
							<input type="email" name="email" id="email" class="form-control valid" placeholder="{ci_language line="email"}" />
						</div>
					</div>
				</div>							
				<div class="row">					
                                    <div class="col-xs-6 m-t5">
                                        <a class="sign_link" href="login"><i class="fa fa-long-arrow-left"></i> {ci_language line="back_to_login"}</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="submit" name="action" value="{ci_language line="send_me"}" class="btn btn-save pull-right" />
                                    </div>
				</div>
			</div>
                    </form>
		</div>
	</div>
</div>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/users/forgot_password.js" type="text/javascript"></script>

{include file="footer.tpl"}