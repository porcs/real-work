{*not use / old file*}
{include file="header.tpl"}
{ci_config name="base_url"}

<div class="page-content">
    <div class="row-fluid center">
        <div class="login-container">
            <div class="position-relative">
                <div id="login-box" class="login-box visible widget-box no-border">
                    
                    <div id="loading" class="alert alert-block" style="display: none">
                        <h3 class="smaller lighter green">
                            <i class="icon-spinner icon-spin orange bigger-125"></i> {ci_language line="waiting_for_create_user"} ..
                        </h3>
                    </div>

                    <h3 id="error" class="alert alert-error" style="display: none">
                        <strong>{ci_language line="sorry"} ! </strong>
                        {ci_language line="something_may_have_gone_wrong"}, <br>
                        {ci_language line="try_again"}
                        
                        <a href="{$base_url}users/login">
                            <span>{ci_language line="back_to_login"}</span>
                        </a>
                    </h3>
                </div>
             </div>
        </div>
                      
        <input type="hidden" id="userdata" value="{if isset($profile)}{$profile}{/if}" />               
        
    </div>
</div>

{include file="footer.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/users/authenticate.js"></script>