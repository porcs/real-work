{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/users/profile.css" />
<div id="content" class="{if !isset($popup)}main {/if}p-rl40 p-xs-rl20" class="row">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="edit_profile"}</h1>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        
        {include file="popup_step.tpl"}
        
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                <h4 class="headSettings_head">{ci_language line="Image"}</h4>						
            </div>
        </div>
    </div>

    
<form id="imageform" method="post" enctype="multipart/form-data" action='{$base_url}/users/profile_image{if isset($popup) && $popup}/{$popup}{/if}'>
    <div class="row">
        <div class="col-xs-12 dSTable">
            <div class="profile dRow">
                <div class="profile_picture dCell"> 
                    <img class="icon-big-circle" src="{$profile.image}" alt="">
                    <button class="btn btn-add fileSelect">+ {ci_language line="Add image"}</button>
                    <input class="uploadbtn" type="file" name="userfile">
                </div>
                <div class="dCell p-l20">
                    <p class="profile_text ">{if isset($user_first_name)}{$user_first_name}{/if} {if isset($user_las_name)}{$user_las_name}{/if}</p>
                </div>				
            </div>
        </div>
    </div>
     <input type="hidden" name="id" value="{$profile.id}" />
</form>
<form action="{$base_url}index.php/users/profile_edit{if isset($popup) && $popup}/{$popup}{/if}" enctype="multipart/form-data" method="post" autocomplete="off" {*class="form-horizontal margin-none"*} id="validateSubmitForm">
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 m-tb20 b-Bottom">
                <h4 class="headSettings_head">{ci_language line="basic_info"}</h4>						
            </div>
        </div>
    </div>
                
    <div class="row">
        <div class="col-sm-4">
            <p class="regRoboto poor-gray">{ci_language line="email"}</p>
            <div class="form-group">
                {if isset($profile.user_email) && !empty($profile.user_email)}
                    <input type="text" class="form-control" name="email" id="email" value="{$profile.user_email}" readonly="readonly" />
                {else}
                    <input type="text" class="form-control valid" name="email" id="email" />
                {/if}
            </div>
        </div>
        <div class="col-sm-4">
            <p class="regRoboto poor-gray">{ci_language line="firstname"}</p>
            <div class="form-group">
                <input type="text" name="firstname" id="firstname" class="form-control valid" value="{if isset($profile.user_first_name)}{$profile.user_first_name}{/if}" />
            </div>
        </div>
        <div class="col-sm-4">
            <p class="regRoboto poor-gray">{ci_language line="lastname"}</p>
            <div class="form-group">
                <input type="text" name="lastname" id="lastname" class="form-control valid" value="{if isset($profile.user_las_name)}{$profile.user_las_name}{/if}" />
            </div>
        </div>
    </div>
    <div class="row custom-form">
            <div class="col-sm-6">
                <p>
                    <label class="cb-checkbox">
                        <input type="checkbox" name="newsletter" id="newsletter" {if $profile.user_newsletter==1 }checked="checked" {/if} />
                        {ci_language line="newsletter_detail"}
                    </label>
                </p>
                <p>
                    <label class="cb-checkbox">
                    {if isset($popup) && $popup} 
                            <input id="agree" name="agree" type="checkbox" />
                            {ci_language line="i_accept"} 
                    {else}
                        {ci_language line="Download"}
                    {/if}
                        <a href="{$cdn_url}assets/docs/Terms and Conditions for Feed.biz.pdf" class="link" target="_blank">{ci_language line="user_agreement"}</a>
                        
                   </label>
               </p>
               <p for="agree" id="agree-error" class="text-left help-inline error display-none"><i class="fa fa-exclamation-circle"></i> {ci_language line="please_agree"}.</p>
            </div>
        
            
        <div class="col-sm-6">
            <!--Message-->
            <input type="hidden" id="password-message" value="{ci_language line="your_password_must_be_long"}" />
            <input type="hidden" id="password-not-match-message" value="{ci_language line="please_enter_the_same_value_again"}"/>
            <input type="hidden" id="email-message" value="{ci_language line="please_enter_your_email"}"/>
            <input type="hidden" id="email-available-message" value="{ci_language line="email_available"}"/>
            <input type="hidden" id="email-note-message" value="{ci_language line="note_can_not_change_email"}"/>
            <input type="hidden" id="email-duplicate-message" value="{ci_language line="email_already_taken"}"/>
            <input type="hidden" id="firstname-message" value="{ci_language line='please_enter_your_first_name'}" />
            <input type="hidden" id="lastname-message" value="{ci_language line='please_enter_your_last_name'}" />

            <input type="hidden" name="{$csrf.key}" value="{$csrf.value}" />
            <input type="hidden" name="id" value="{$profile.id}" />
            {if isset($popup) && $popup}<button class="btn btn-save pull-right" type="submit">Save</button>{/if}
        </div>
    </div>
                
    {if !isset($popup)}
        <div class="row">
            <div class="col-xs-12">
                <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                    <h4 class="headSettings_head">{ci_language line="password"}</h4>						
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <p class="regRoboto poor-gray">{ci_language line="current_password"}</p>
                <div class="form-group">
                    <input type="password" name="current" id="current" class="form-control">					
                </div>
            </div>
            <div class="col-sm-4">
                <p class="regRoboto poor-gray">{ci_language line="new_password"}</p>
                <div class="form-group">
                    <input type="password" name="new" id="new" class="form-control valid">
                </div>
            </div>
            <div class="col-sm-4">
                <p class="regRoboto poor-gray">{ci_language line="new_passwordconf"}</p>
                <div class="form-group">
                    <input type="password" name="new_confirm" id="new_confirm" class="form-control valid">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-save pull-right m-t20" type="submit">{if isset($popup) && $popup} {ci_language line="save_continue"} {else} {ci_language line="save"} {/if}</button>
            </div>
        </div>
    {/if}
    </form>
{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/jquery.maskedinput.min.js"></script>
<script src="{$cdn_url}assets/js/jquery.ajax_image_form.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/users/profile.js"></script>
