{include file="header.tpl"}
{ci_config name="base_url"}

<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <form action="{$base_url}users/login" method="post" id="loginform" autocomplete="off">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">{ci_language line="Login to your Account"}</h4>
				<div class="row">
					<div class="col-sm-6">
                                            <a href="{if isset($facebook_path)}{$facebook_path}{/if}" class="btn btn-facebook pull-width"><i class="fa fa-facebook"></i>{ci_language line="Facebook Account"}</a>
					</div>
					<div class="col-sm-6">
                                            <a href="{if isset($googleplus_path)}{$googleplus_path}{/if}" class="btn btn-google pull-width"><i class="fa fa-google-plus"></i>{ci_language line="Google Account"}</a>
					</div>
				</div>
				<div class="row">
                                    <div class="col-xs-12">
                                        <p class="regRoboto poor-gray">{ci_language line="email"}</p>
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="exampleInputEmail1" name="email" id="email" placeholder="{ci_language line="email"}">
                                        </div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-xs-12">
                                        <p class="regRoboto poor-gray">{ci_language line="password"}</p>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" id="password" placeholder="{ci_language line="password"}">
                                        </div>
                                    </div>
				</div>
				<div class="row custom-form">
					<div class="col-xs-6">
						<label class="cb-checkbox">
                                                    <input type="checkbox" name="remember_me" value="1"/>
							{ci_language line="remember_me"}
						</label>
					</div>
					<div class="col-xs-6">
						<button type="submit" class="btn btn-save pull-right">Login</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 xs-center">
						<a class="sign_link" href="{$base_url}users/register">{ci_language line="Create a new account?"}</a>
					</div>
					<div class="col-sm-6 xs-center sm-right">
						<a class="sign_link" href="forgotten_password" >{ci_language line="forgot_password"}</a>
					</div>
				</div>
			</div>
                    </form>
		</div>
	</div>
</div>
 
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/users/login.js" type="text/javascript"></script>

{include file="footer.tpl"}
