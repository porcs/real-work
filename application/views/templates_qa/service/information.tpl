{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="package_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                <form id="frmInfo" method="post">
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">{ci_language line="package_wizard_package"}</span>
						</li>
						<li class="headSettings_point-item active">
							<span class="step">2</span>
							<span class="title">{ci_language line="package_wizard_info"}</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">3</span>
							<span class="title">{ci_language line="package_wizard_complete"}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix b-Bottom p-b10">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">{ci_language line="package_info"}</h4>						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h5 class="montserrat text-uc dark-gray">{ci_language line="package_info_title"}</h5>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="firstname"} *</p>
				<div class="form-group">
                                    <input value="{$info['firstname']}" type="text" name="info[firstname]" id="firstname" placeholder="{ci_language line="firstname"}" class="{*validate[required]*}valid form-control" />
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="lastname"} *</p>
				<div class="form-group">
                                    <input value="{$info['lastname']}" type="text" name="info[lastname]" id="lastname" placeholder="{ci_language line="lastname"}" class="{*validate[required]*}valid form-control" />
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="address1"} *</p>
				<div class="form-group">
                                    <input value="{$info['address1']}" type="text" name="info[address1]" id="address1" placeholder="{ci_language line="address1"}" class="{*validate[required]*}valid form-control">
				</div>
			</div>
                </div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="address2"}</p>
				<div class="form-group">
                                    <input value="{$info['address2']}" type="text" name="info[address2]" id="address2" placeholder="{ci_language line="address2"}" class="form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="zipcode"} *</p>
				<div class="form-group">
                                    <input value="{$info['zipcode']}" type="text" name="info[zipcode]" id="zipcode" placeholder="{ci_language line="zipcode"}" class="{*validate[required]*}valid form-control">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="city"} *</p>
				<div class="form-group">
                                    <input value="{$info['city']}" type="text" name="info[city]" id="city" placeholder="{ci_language line="city"}" class="{*validate[required]*}valid form-control">
				</div>
			</div>
                </div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="complement"}</p>
				<div class="form-group">
                                    <input value="{$info['complement']}" type="text" name="info[complement]" id="complement" placeholder="{ci_language line="complement"}" class="{*validate[required]*}valid form-control" />
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="state_region"} *</p>
				<div class="form-group">
                                    <input value="{$info['stateregion']}" type="text" name="info[stateregion]" id="state_region" placeholder="{ci_language line="state_region"}" class="{*validate[required]*}valid form-control" />
				</div>
			</div>
			<div class="col-sm-4 country form-group">
				<p class="regRoboto poor-gray">{ci_language line="country"} *</p>
				<select value="{$info['country']}" name="info[country]" id="country" placeholder="{ci_language line="country"}" class="validate[required] search-select">
					{$country_option}
				</select>
{*                                <input value="{$info['country']}" type="text" name="info[country]" id="country" placeholder="{ci_language line="country"}" class="validate[required] valid form-control">*}
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* {ci_language line="fields are required"}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="pull-right">
                                        <a href="" class="link p-size pull-left m-tr10 btnPrev">{ci_language line="checkout_btn_prev"}</a>
					<button type="submit" class="btn btn-save {*btnNext*}">{ci_language line="checkout_btn_next"} <i class="m-l5 fa fa-angle-right"></i></button>
				</div>
				
			</div>
		</div>
        </form>
</div>
                                
<script language="javascript">
    var siteurl = "{$siteurl}";
</script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.information.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.validate.min.js"></script>

{include file="footer.tpl"}