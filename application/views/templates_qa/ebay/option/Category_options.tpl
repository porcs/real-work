{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}  

    <div class="row">
        <div class="col-xs-12">
            <form method="post" id="category_options_form" autocomplete="off" >
                <div class="headSettings clearfix b-Top-None">  
                    <div class="col-xs-6 clearfix">
                        <div class="row">
                            <h4 class="p-0 headSettings_head">{ci_language line="by"} {ci_language line="Category"}</h4>
                        </div>
                    </div>                                                                 
                    <div class="col-sx-6 pull-right" >
                        <button type="button" class="btn btn-save m-b0 m-l5">
                            {ci_language line="Submit"}
                        </button>
                        <div class="status" style="display: none">                                                  
                            <p class="p-t10 m-l5 p-b0 m-b0">
                                <i class="fa fa-spinner fa-spin"></i> 
                                <span>{ci_language line="Submiting"}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div id="category_options"></div>                       
            </form>					 
        </div>
    </div>		

    <div id="model" style="display:none">
        <div class="row-edit p-10 p-l40 custom-form">
           <table cellpadding="5" cellspacing="5" border="0" class="table_detail" >
                <!--Hidden Input-->
                <tr style="display:none">
                    <td width="35%">
                        <input type="hidden" class="id_category form-control" rel="id_category" />
                    </td>
                    <td width="65%"></td>                    
                </tr>                 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Disabled"} : </p></td>
                    <td>
                            <label class="cb-checkbox"></i><input type="checkbox" class="" value="1" rel="disable" />
                        {ci_language line="Check this box to never export this product to the marketplace"}</label>                     
                    </td>
                </tr> 
                 <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Force in Stock"} : </p></td>
                    <td>
                                <label class="cb-checkbox" onclick="show_force(this)" ></i><input type="checkbox" rel="force_chk"/>
                        {ci_language line="This quantity will appear on the marketplace, even it's out of Stock"}</label>
                        <div class="row clearfix" style="display:none">
                            <div class="col-xs-4">
                                <input type="text" rel="force" class="form-control" disabled onkeyup="this.value=this.value.replace(/[^\d]/,'')"/> 
                            </div>
                        </div>                      
                    </td>
                </tr> 
            </table>
        </div>
    </div>

{include file="amazon/PopupStepFooter.tpl"}

<input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="id_marketplace" value="{if isset($id_marketplace)}{$id_marketplace}{/if}" />

<input type="hidden" id="shipping_type_1" value="{ci_language line="Standard"}">
<input type="hidden" id="shipping_type_2" value="{ci_language line="Express"}">
<input type="hidden" id="shipping_type_0" value="">
<input type="hidden" id="l_Error" value="{ci_language line="Error"}">
<input type="hidden" id="l_Success" value="{ci_language line="Success"}">
<input type="hidden" id="l_Submit" value="{ci_language line="Submiting"}">
<input type="hidden" id="l_Edit" value="{ci_language line="Edit"}">
<input type="hidden" id="l_Reset" value="{ci_language line="Reset"}">
<input type="hidden" id="l_Mismatch" value="{ci_language line="Item unactivated: Multiple edition is possible only for matching values"}">
<input type="hidden" id="reset-confirm" value="{ci_language line="Do you want to reset all product options in "}">

{include file="IncludeDataTableLanguage.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/treeTable.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/category_options.js"></script>

{include file="footer.tpl"}