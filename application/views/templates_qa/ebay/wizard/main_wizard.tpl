{include file="header.tpl"}
{ci_config name="base_url"}
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/select2/select2.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/select2/select2.min.js"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/form/form_elements.min.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/fuelux-radio/fuelux-radio.js?v=v1.2.3"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/checkBo/checkBo.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay_bootbox.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/bootbox/bootbox.min.js?v=v1.2.3"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay.min.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay_import.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/{$current_page}.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/advance_import.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/wizard.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/datatable_import.css" />
{if empty($root_page)}{ci_config name="root_page"}{/if}
{if !empty($validation_data)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/validate/jquery.validate.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/colorbox/jquery.colorbox-m.js"></script>
<form class="form-horizontal" id="form-submit" method="POST" {if !empty($next_page)}action="{$next_page}"{/if}>
    <div class="row wizard-page">
        <div class="innerAll spacing-x2" >
            <div id="success" class="alert alert-success"></div>
            <div id="warning" class="alert alert-warning"></div>
            <div id="error" class="alert alert-danger"></div>
            <div id="loading-result" class="alert alert-block">
                <h4 class="smaller lighter grey">
                    <i class="fa fa-spinner fa-spin"></i> {ci_language line="importing"}
                </h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-inverse">
                        <div class="innerAll half bg-inverse margin-none center wizard-header">
                            <h5 class="margin-none wizard-wrap">{ci_language line="ebay_wizard_configuration"}</h5>
                        </div>
                        <div class="row p-t10">
                        {include file="message_document.tpl"} 
                        </div>
                        {include file="ebay/wizard/wizard_step.tpl"}
                        {if $popup > 2}
                        <div class="row">
                            <div class="col-xs-12"> 
                                <h3 class="head_text p-t10 b-Top m-b0 clearfix heading margin-none">
                                    <img src="{$cdn_url}assets/images/flags/{$flag_country}.png" class="flag m-r10"> {$name_country}
                                </h3> 
                            </div>
                        </div>
                        {/if}
                        {if $popup == 1}
                                {include file="ebay/configuration_authentication.tpl"} 
                        {elseif $popup == 2}
                                {include file="ebay/wizard/wizard_choose.tpl"} 
                        {elseif $popup == 3}
                                {include file="ebay/wizard/wizard_shipping.tpl"} 
                        {elseif $popup == 4}
                                {include file="ebay/mapping/configuration_univers.tpl"} 
                        {elseif $popup == 5}
                                {include file="ebay/mapping/configuration_mapping_categories.tpl"}
                        {elseif $popup == 6}
                                {include file="ebay/configuration_actions.tpl"} 
                        {elseif $popup == 7}
                                {include file="ebay/wizard/wizard_finish.tpl"} 
                        {/if}
                    </div>                        
                </div>                        
            </div>
        </div>
        {if empty($btn_flow) || (isset($btn_flow) && $btn_flow == 'open') }
        <div class="widget-body controller-page">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    {if !empty($is_reset)}
                    <button class="btn btn-inverse" type="reset">
                        <i class="fa fa-undo"></i>
                        {ci_language line="reset"}
                    </button>
                    {/if}
                    {if !empty($prev_page) && !isset($hide_previous)}
                    <button class="btn" type="back" data-page="{$prev_page}" data-last="finish">
                        {ci_language line="prev"}
                    </button>
                    {/if}
                </div>
                <div class="col-xs-6 text-right">
                    {if !empty($next_page) && !isset($hide_next)}
                    <button class="btn btn-primary" type="submit" data-last="finish">
                        {ci_language line="next"}
                        <i class="fa fa-angle-right"></i>
                    </button>
                    {/if}
                </div>
            </div>
            <div class="clearfix"></div>      
        </div>
        {/if}
    </div>
    <div class="close">
        <button type='button' class='close' data-dismiss='alert'><i class='fa fa-close'></i></button><i class='fa fa-exclamation-triangle'></i>
    </div>
    <input type="hidden" name="popup-step" id="popup-step" value="{$popup}">
    <input type="hidden" name="id-site" id="id-site" value="{$id_site}" />
    <input type="hidden" name="id-packages" id="id-packages" value="{$packages}" />
    <input type="hidden" name="current-page" id="current-page" value="{$current_page}" />
    <input type="hidden" name="ajax-url" id="ajax-url" value="{$ajax_url}" />
    <input type="hidden" name="update-{$current_page}-success" class="update-{$current_page}-success" value="{ci_language line="update_{$current_page}_success"}">
    <input type="hidden" name="update-{$current_page}-fail" class="update-{$current_page}-fail" value="{ci_language line="update_{$current_page}_fail"}">
    {if isset($hidden_data)}
    {foreach from = $hidden_data key = key_hiden_data item = hidden_data_value}
    <input type="hidden" name="{$key_hiden_data}" class="{$key_hiden_data}" value="{$hidden_data_value}">
    {/foreach}
    {/if}
    {if isset($validation_data)}
    {foreach from = $validation_data key = field_validation_data item = data_validate_value}
    {foreach from = $data_validate_value key = validation_key_valid item = message_value}
    <input type="hidden" name="{$field_validation_data}-{$validation_key_valid}" class="{$field_validation_data}-{$validation_key_valid}" value="{$message_value}" data-rules="{$validation_key_valid}" data-type="validation">
    {/foreach}
    {/foreach}
    {/if}
</form>
{if !empty($jquery_form)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/form/jquery.form.js"></script>
{/if}
{if !empty($currency_format)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/formatCurrency/jquery.formatCurrency.js"></script>
{/if}
{if !empty($data_tree)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tree/fuelux.tree.offers.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/blockUI/jquery.blockUI.js"></script>
{/if}
{if !empty($data_table)}
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/jquery.dataTables.bootstrap.js"></script>*}
<script type="text/javascript" src="{$cdn_url}assets/js/datatables.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/dataTables.colVis.min.js"></script>
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/DT_bootstrap.js?v=v1.2.3"></script>*}
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ebay.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_template.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_wizard_start.js"></script>
{if !empty($tinymce)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tinymce/tinymce.min.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/checkBo/checkBo.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/{$current_page}.js"></script>
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/authorization_main.js"></script>*}
{include file="footer.tpl"}
