{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="downl_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->	
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">{ci_language line="downl_list_title"}</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="downl_table_id"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="downl_table_title"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="downl_table_date"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="downl_table_method"}
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th>{ci_language line="downl_table_id"}</th>
									<th>{ci_language line="downl_table_title"}</th>
									<th>{ci_language line="downl_table_date"}</th>
									<th>{ci_language line="downl_table_method"}</th>
									<th></th>
								</tr>
							</thead>
                                                <tbody>
                                                {if is_array($query)}
                                                {foreach $query as $field}
                                                <tr>
                                                    <td data-th="{ci_language line="downl_table_id"}:">{$field['id_billing']}</td>
                                                    <td data-th="">
                                                        {if sizeof($field['detail']) > 0}
                                                        <ul class="media-list pricing-table-header grey">
                                                            {foreach $field['detail'] as $val}
                                                            <li>
                                                                <i class="icon-double-angle-right"></i> 
                                                                {$val['package_detail']['desc']}
                                                            </li>
                                                            {/foreach}
                                                        </ul>
                                                        {else}
                                                        &nbsp;
                                                        {/if}
                                                    </td>
                                                    <td data-th="{ci_language line="downl_table_title"}:">{date('j F Y H:i:s',strtotime($field['created_date_billing']))}</td>
                                                    <td data-th="{ci_language line="downl_table_date"}:" style="text-transform: capitalize;">
                                                        {if $field['payment_method_billing'] == 'credit'}
                                                            <span class="label label-large label-yellow padding_set"  ><i class="fa fa-credit-card"></i> {ci_language line="history_paypal_credit"}</span>
                                                        {elseif $field['payment_method_billing'] == 'express'}
                                                            <span class="label label-large label-grey padding_set"  ><i class="fa fa-external-link"></i> {ci_language line="history_paypal_express"}</span>
                                                        {elseif $field['payment_method_billing'] == 'preapproval'}
                                                            <span class="label label-large label-primary padding_set" ><i class="fa fa-check"></i> {ci_language line="history_paypal_preapproval"}</span>
                                                        {/if}
                                                    </td>
                                                    <td data-th="{ci_language line="downl_table_method"}:" style="text-align: center;">
                                                        <button class="btn btn-app btn-primary" data-id="{$field['id_billing']}" title="{ci_language line="Click here to print"}">
                                                            <i class="fa fa-print fa-2x"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                {/foreach}
                                                {/if}
                                            </tbody>
					</div>
				</div>
			</div>			
		</div>
	</div>

<script src="{$cdn_url}assets/js/calldatatable.js"></script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.billing.download.js"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/billing/information.css" />
{include file="footer.tpl"}