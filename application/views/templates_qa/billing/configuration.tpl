{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/validationEngine.jquery.css" />
<script language="javascript" src="{$cdn_url}assets/js/jquery.validationEngine.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.validationEngine-en.js"></script>


<div class="main p-rl40 p-xs-rl20 m-t20">
    <form method="post" id="frmConfig">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="cfg_page_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
	<div class="row m-t10">
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">{ci_language line="cfg_username"}</p>
			<div class="form-group">
				<input type="text" class="form-control" value="{$profile['user_name']}" readonly="readonly" />
			</div>
		</div>
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">{ci_language line="cfg_email"}</p>
			<div class="form-group">
				<input type="text" class="form-control validate[required,custom[email]]" id="cfg_email" name="cfg[email]" placeholder="{ci_language line="cfg_email"}" value="{if isset($payerEmail)}{$payerEmail}{/if}" />
			</div>
		</div>
		<div class="col-sm-4">
			<p class="regRoboto poor-gray">{ci_language line="cfg_regdate"}</p>
			<div class="form-group">
				<input type="text" class="form-control" value="{date('d/m/Y', strtotime($profile['user_created_on']))}" readonly="readonly" />
			</div>
		</div>
	</div>
	<div class="row custom-form">
		<div class="col-xs-12">
			<p class="pull-left text-uc montserrat dark-gray m-t2">									
				{ci_language line="cfg_preapproval"}:
			</p>
			<div class="cb-switcher m-l10">
				<label class="inner-switcher">
					<input type="checkbox" data-state-on="ON" data-state-off="OFF" name="cfg[preapproval]" id="cfg_preapproval" {if isset($isPreapproval) && $isPreapproval} checked="checked"{/if}/>
				</label>
				<span class="cb-state">ON</span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<p class="m-t10 regRoboto light-gray">{ci_language line="cfg_preapproval_detail"}
				<a href="{$siteurl}assets/docs/What_is_paypal_pre-approval_for_Feed.biz.pdf" onclick="javascript:window.open('{$siteurl}assets/docs/What_is_paypal_pre-approval_for_Feed.biz.pdf','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;" title="{ci_language line="cfg_preapproval_link_text"}" class="link">{ci_language line="cfg_preapproval_link_text"}</a>
			</p>
		</div>
		<div class="col-sm-6">
			<div class="pull-sm-right clearfix">
				<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/pay-pal.png" alt=""></a>
				<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/mastercard.png" alt=""></a>
				<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/visa.png" alt=""></a>
				<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/express.png" alt=""></a>
				<a href="https://www.paypal.com/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/discover.png" alt=""></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 b-Top m-t20 p-t20">
			<div class="pull-right">
				<button type="button" class="btn btn-save btn-savechanges">{ci_language line="cfg_btn_save"} <i class="m-l5 fa fa-angle-right"></i></button>
			</div>
		</div>
	</div>
    </form>
</div>

    <script language="javascript">
        var preapprovalClick = "{$preClick}";
    </script>
    <script language="javascript" type="text/javascript" src="{$siteurl}assets/js/jquery.blockUI.js"></script>
    <script language="javascript" type="text/javascript" src="{$siteurl}assets/js/jquery.billing.configuration.js"></script>

</div>
{include file="footer.tpl"}