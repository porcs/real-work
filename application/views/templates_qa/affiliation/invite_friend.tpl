{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/bootstrap-tagsinput.css">  

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="aff_inv_title"}</h1>
     {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="row">
            <div class="col-xs-12">
                    <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                            <h4 class="headSettings_head">{ci_language line="aff_inv_topic_share"}</h4>						
                    </div>
            </div>
    </div>
    <div class="row">
            <div class="col-xs-12">
                    <p class="regRoboto poor-gray">{ci_language line="aff_inv_descript_share"}</p>
                    <button class="btn btn-facebook" onclick="return goclicky(this,611,300)" url="http://www.facebook.com/sharer.php?s=100&p[title]={ci_language line=" Join with us "}&p[url]={$linkUrl}"><i class="fa fa-facebook"></i>{ci_language line="Share on your"} {ci_language line="Facebook Account"}</button>
                    <button class="btn btn-google" onclick="return goclicky(this,611,300)" url="https://plusone.google.com/_/+1/confirm?hl=en&url={$linkUrl}"><i class="fa fa-google-plus"></i>{ci_language line="Share on your"} {ci_language line="Google Account"}</button>
                    <button class="btn btn-twitter" onclick="return goclicky(this)" url="https://twitter.com/intent/tweet?text={$linkUrl}"><i class="fa fa-twitter"></i>{ci_language line="Share on your"} {ci_language line="Twitter Account"}</button>
            </div>
    </div>
    <div class="row">
            <div class="col-xs-12">
                    <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                            <h4 class="headSettings_head">{ci_language line="aff_inv_topic_link"}</h4>						
                    </div>
            </div>
    </div>
    <div class="row">
            <div class="col-xs-12">
                    <p class="regRoboto poor-gray">{ci_language line="aff_inv_descript_link"}</p>
                    <div class="form-group">
                            <input type="text" id="form-field-1" value="{$linkUrl}" class="form-control valid" readonly="readonly" style="cursor: text" />					
                    </div>
            </div>
    </div>
    <div class="row">
            <div class="col-xs-12">
                    <div class="headSettings clearfix p-b10 m-tb20 b-Bottom">
                            <h4 class="headSettings_head">{ci_language line="aff_inv_topic_email"}</h4>						
                    </div>
            </div>
    </div>
    <form method="post" id="frmEmail" action="{$base_url}users/affiliation/email_friend">
        <div class="row">
                <div class="col-xs-12">
                        <p class="regRoboto poor-gray">{ci_language line="aff_inv_descript_email"}</p>
                        <div class="form-group">
                                <input class="form-control tags-input" id="tag_email" name="ref[remail]" type="text" data-role="tagsinput" required />
                                <input type="hidden" id="aff_inv_email_placeholder" name="aff_inv_email_placeholder" value="{ci_language line='aff_inv_email_placeholder'}" />
                                <input type="hidden" id="mess" name="mess" value="{if isset($mess) && $mess}{$mess}{/if}" />
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-save pull-right" type="button">{ci_language line="aff_inv_btn_send"}</button>
                </div>
        </div>
    </form>
</div>
<script src="{$cdn_url}assets/js/FeedBiz/users/invite_friend.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.affiliation.invite.js"></script>
{include file="footer.tpl"}