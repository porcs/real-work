    <div class="form-group">
        <textarea 
        class="form-control {if isset($data['class'])}{$data['class']}{/if}"
        {if isset($data['id'])}id="{$data['id']}"{/if} 
        {if isset($data['name'])}name="{$data['name']}"{/if}  />
            {if isset($data['value'])}value="{$data['value']}"{/if}
        </textarea>
    </div>