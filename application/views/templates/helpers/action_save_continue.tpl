<div class="row">
	<div class="col-xs-12">
		<div class="b-Top m-t10 p-t10">
            {if isset($data)}
                <input type="hidden" name="continue" value="{$data}" />
            {/if}
            <button type="submit" class="btn btn-save pull-right" id="save_continue_data" name="save" value="continue">
                {ci_language line="Save & continue"}
            </button>
            <button type="submit" class="btn btn-link link p-size pull-right p-0 m-0 m-tr10" id="save_data" name="save">
                {ci_language line="save"}
            </button>
		</div>
	</div>
</div>