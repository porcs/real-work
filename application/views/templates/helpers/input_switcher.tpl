<div class="cb-switcher pull-left">
    <label class="inner-switcher">
        <input type="checkbox" 
        {if isset($data['id'])}id="{$data['id']}"{/if} 
        {if isset($data['name'])}name="{$data['name']}"{/if} 
        {if isset($data['value'])}value="{$data['value']}"{/if} 
        {if isset($data['checked']) && $data['checked'] == 1}checked{/if} 
        data-state-on="{ci_language line="ON"}" data-state-off="{ci_language line="ON"}" />
    </label>
    <span class="cb-state">{ci_language line="ON"}</span>
</div>