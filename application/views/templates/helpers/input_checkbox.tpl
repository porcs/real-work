<label class="checkBa w-100">
	<input type="checkbox"  
	{if isset($data['id'])}id="{$$data['id']}"{/if}  
	{if isset($data['name'])}name="{$data['name']}"{/if}
	{if isset($data['value'])}value="{$data['value']}"{/if} 
	{if isset($data['checked']) && $data['checked'] == 1}checked{/if} /> 
        <label {if isset($data['id'])}for="{$data['id']}"{/if}>
            {if isset($data['label'])}{ci_language line="$data['label']"}{/if}
        </label>
</label>