<select 
    {if isset($data['id'])}id="{$data['id']}"{/if} 
    {if isset($data['name'])}name="{$data['name']}"{/if} 
    class="chosen-select" 
    multiple="multiple" 
    {if isset($data['placeholder'])}data-placeholder="{ci_language line="$data['placeholder']"}"{/if} 
    data-no_results_text="{if isset($data['no_results_text'])}{ci_language line="Oops, nothing found!"}{else}{ci_language line="Oops, nothing found!"}{/if}">
    {foreach $data['value'] as $key => $option}
        <option {if isset($key)}value="{$key}"{/if} {if isset($option['selected']) && $option['selected']}selected="selected"{/if}>{if isset($option['name'])}{$optionp['name']}{/if}</option>
    {/foreach}
</select>