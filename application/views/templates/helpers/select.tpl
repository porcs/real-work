{if isset($data['size'])}
	<div class="row">
		{if $data['size'] == "sm"}
	    	<div class="col-sm-4">
		{elseif $data['size'] == "md"}
			<div class="col-sm-4">
		{elseif $data['size'] == "lg"}
			<div class="col-sm-12">
		{/if}
{/if}
	<select 
	    {if isset($data['id'])}id="{$data['id']}"{/if}  
	    {if isset($data['name'])}name="{$data['name']}"{/if} 
	    class="search-select">
	    {foreach $data['value'] as $key => $option}
	        <option {if isset($option['value'])}value="{$option['value']}"{/if} {if isset($option['selected']) && $option['selected']}selected="selected"{/if}>{if isset($option['name'])}{$option['name']}{/if}</option>
	    {/foreach}    
	</select>
{if isset($data['size'])}
	    </div>
	</div>
{/if}