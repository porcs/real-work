{if isset($data['size'])}
	<div class="row">
		{if $data['size'] == "sm"}
	    	<div class="col-sm-4">
		{elseif $data['size'] == "md"}
			<div class="col-sm-4">
		{elseif $data['size'] == "lg"}
			<div class="col-sm-12">
		{/if}
{/if}
    <input type="text" 
    class="form-control" 
    {if isset($data['id'])}id="{$data['id']}"{/if} 
    {if isset($data['name'])}name="{$data['name']}"{/if} 
    {if isset($data['value'])}value="{$data['value']}"{/if} />
	        
{if isset($data['size'])}
	    </div>
	</div>
{/if}