{if isset($data['fields'])}
    {foreach $data['fields'] as $key => $fields}        
        <div class="form-group col-md-12 m-t10">
            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10" id="email_provider"><span class="b-blue">{$key}</span></label>
            <div class="col-md-7">                    
                {include file="helpers/{$fields['type']}.tpl" data=$fields }
                {if isset($fields['help_text'])}<p class="poor-gray m-t5">{$fields['help_text']}</p>{/if}
            </div>
        </div>
    {/foreach}
{/if}