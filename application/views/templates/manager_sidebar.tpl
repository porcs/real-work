{ci_language file="sidebar" lang="$lang"}
{ci_config name="base_url"}
{if !isset($popup) || !$popup}
    <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
    </a>
    <div id="menu" class="hidden-print">

    <div class="sidebar active">

        <ul class="sidebar_content">
            <li class="sidebar-item{if (isset($url[0]) && $url[0] == 'manager' && (empty($url[1]) || $url[1]=='index' ))} active{/if}">
                <a class="sidebar-item_link link" href="{$base_url}manager">
                    <i class="icon-clock"></i>
                    <span class="menu-text"> {ci_language line="Manager dashboard"} </span>
                </a>
            </li><!--dashboard-->
            <li class="sidebar-item    {if isset($url[0]) && $url[0] == 'manager' && (isset($url[1]) && $url[1] == 'security')} active{/if}" >
                <a class="sidebar-item_link link" href="{$base_url}manager/security">
                    <i class="fa-clock"></i><span class="menu-text">{ci_language line="Security"}</span>
                </a>
            </li>
 
        </ul> 

    </div>
</div>
{/if} 