{if !empty($smarty.cookies.storeURL)}
    <iframe src="{$smarty.cookies.storeURL}" width="0" height="0" class="hidden"></iframe>
    {$smarty.cookies.storeURL = ''}
    {$smarty.cookies['storeURL'] = ''}
    {$_COOKIE['storeURL'] = ''}
    {*php}unset($_COOKIE['storeURL']);setcookie('storeURL', null, -1, '/');{/php*}
{/if}

{if !isset($cdn_url)} 
        {assign var="cdn_url" value=$base_url} 
{/if}
            {if isset($id_shop)}
                <input id="id_shop" value="{$id_shop}" type="hidden" />
                <script type="text/javascript">
                    //check offer
                    $(function() {
                        var id_shop = $('#id_shop').val();
                        $.ajax({
                            url: '{$base_url}offers/check_shop/' + id_shop,
                            type: 'GET',
                            cache: false,
                            success: function(data) {
                                if(data === "false")
                                {
                                    $('#menu_profiles').hide();
                                    $('#menu_offers').hide();
                                }else{
                                    $('#menu_profiles').show();
                                    $('#menu_offers').show();
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) 
                            {
                                console.log('Return Error : ' + errorThrown);
                            }
                        });
                    });
                </script>
            {/if}

            <div style="display: none" id="nav-search-input"></div>
            </div>

            <script type="text/javascript">
                var $txt_loading = '{ci_language line="Loading"}';
                var $txt_error = '{ci_language line="Error"}';
                var $txt_cannot_change_shop_offer = '{ci_language line="Cannot change shop offer"}';
                var $txt_cannot_change_shop_product = '{ci_language line="Cannot change shop product"}'; 
                var $txt_complete = '{ci_language line="Completed"}';
                var $txt_other_login = '{ci_language line="txt_other_login"}';
                var $txt_admin_login = '{ci_language line="txt_admin_login"}';
            </script>    
            
            <div class="progress_cv" id="progressbar">
                <div class="progress_small" >
                    <div class="progress_max"><i class="icon cb-maximize"></i></div>
                    <div class="close_pfull active"></div>
                    <div class="running_task"  > 
                        <div class="running_text">
                            <ul>
                                <li class="ptitle">{ci_language line="Tasks "} |<span></span></li>
                                <li class="prun">{ci_language line="Running"}: <span></span></li>
                                <li class="psuc">{ci_language line="Success"}: <span></span></li> 
                                <li class="perr">{ci_language line="Error"}: <span></span></li>
                                <li class="pclose">{ci_language line="No tasks running."}<span></span></li>
                                <li class="phide"><span id="cfb" data-k="{if isset($csrf_key)}{$csrf_key}{/if}" class="display-none" >{if isset($csrf_val)}{$csrf_val}{/if}</span></li>
                            </ul>
                        </div> 
                    </div>
                     
                </div>
                <div class="progressbar progress_full">
                    <div class="progress_min"><i class="icon cb-minimize"></i></div>
                    <div class="close_pfull active"></div>
                    <ul></ul>
                </div>
            </div>

        </div> 
    </div> 
     
    <input type="hidden" id="error_txt" value="{ci_language line="Error"}" /> 
    
    {if isset($mode_default)}
    <input type="hidden" id="mode_default" value="{$mode_default}" />
    {/if}
 
    {if isset($mode_default) && $mode_default ==1}
    <div class="modal fade" id="upgrade_mode">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <iframe src="" style="" width="100%" height="500" frameborder="0" data-url="{$base_url}my_feeds/configuration/general/popup"></iframe>     
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>   
    {/if}

    {if isset($missing_id_shop) && $missing_id_shop}
    <input type="hidden" id="fb_wizard_title" value="{ci_language line="Feedbiz starter wizard"}" />
    <input type='hidden' id='missing_id_shop' value='1'>
    <script src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
    {/if}

    {if isset($missing_id_shop_flag) && $missing_id_shop_flag}
    <input type='hidden' id='missing_id_shop_flag' value='1'>
    {/if}
    <input type="hidden" id="no_avail_datatable" value="{ci_language line="No data available in table"}" />
    <input type="hidden" id="confirm_update_cron_task_txt" value="{ci_language line="Do you want to update the running status of automatic task(s)?"}" />
    {include file="message_code.tpl"} 
    {if isset($manager_mode) && $manager_mode  == true}
        <input type="hidden" id="restrict_txt" value="{ci_language line="This page is not allow for Feed manager"}"/> 
        <input type="hidden" id="upgrade_mode_txt" value=""/> 
        <script src="{$cdn_url}assets/js/FeedBiz/restrict_area.js"></script>
    {else}
        {if isset($advance_mode) && $advance_mode && isset($mode_default) && $mode_default != 2}
            <input type="hidden" id="restrict_txt" value="{ci_language line="This option is in advance mode, please activate advance mode."}"/> 
            <input type="hidden" id="upgrade_mode_txt" value="{ci_language line="upgrade mode"}"/> 
            <script src="{$cdn_url}assets/js/FeedBiz/restrict_area.js"></script>
        {else}
            {if isset($expert_mode) && $expert_mode && isset($mode_default) && $mode_default == 1}
            <input type="hidden" id="restrict_txt" value="{ci_language line="This option is in expert mode, please activate expert mode."}"/> 
            <input type="hidden" id="upgrade_mode_txt" value="{ci_language line="upgrade mode"}"/>  
            <script src="{$cdn_url}assets/js/FeedBiz/restrict_area.js"></script>
            {/if}
        {/if}
    {/if}
    <!-- == New Theme == -->    
    <script src="{$cdn_url}assets/js/FeedBiz/footer.js"></script>
    <script src="{$cdn_url}assets/js/bootstrap.min.js"></script>
    <script src="{$cdn_url}assets/js/bootbox.min.js"></script> 
    <script src="{$cdn_url}assets/js/chosen.jquery.js"></script> 
    <script src="{$cdn_url}assets/js/checkBo.min.js"></script> 
    <script src="{$cdn_url}assets/js/bootstrap-tagsinput.min.js"></script> 
    <script src='{$cdn_url}assets/js/jquery.nicescroll.js'></script>
    <script src="{$cdn_url}assets/js/datatables.js"></script>
    <script src="{$cdn_url}assets/js/search.js"></script>
    <script src="{$cdn_url}assets/js/jquery.fancybox.pack.js"></script>
    <script src="{$cdn_url}assets/js/limit.js"></script>
    <script src="{$cdn_url}assets/js/slick.js"></script>
    <script src="{$cdn_url}assets/js/function.js"></script>  
    <script src="{$cdn_url}assets/js/script.js"></script>
    <script src="{$cdn_url}assets/js/jquery.spritely-0.4.js"></script>
    <script src="{$cdn_url}libraries/ga.js"></script>
    
</body>
</html>