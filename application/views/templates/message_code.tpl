{if isset($message_code) && !empty($message_code)}
    {ci_language file="message_code" lang="$lang"}
    {ci_config name="base_url"}

    {if isset($message_code)}

        {if isset($message_code.error)}
           
            {foreach $message_code.error as $error}
                <div class="notification bad message_code" style="display:none;">
                    <div class="container">
                        <p>
                            {if isset($error.message_name) && !empty($error.message_name)}
                                <span class="bold">
                                    {$error.message_name} 
                                    {if isset($error.require_country) && $error.require_country && isset($country)}[ {$country} ]{/if} : 
                                </span>
                            {/if}
                        </p> 
                        <p>
                            {if isset($error.message_problem) && !empty($error.message_problem)}{$error.message_problem}{/if}
                            {if isset($error.message_cause) && !empty($error.message_cause)}{$error.message_cause}{/if}
                            {if isset($error.message_solution) && !empty($error.message_solution)}{$error.message_solution}{/if}
                        </p>

                        <!--Link-->
                        {if isset($error.message_link) && !empty($error.message_link)}
                            <p class="text-right">
                                <a href="{$base_url}{$error.message_link}{if isset($require_country) && $require_country && isset($id_country)}/{$id_country}{/if}">
                                    {if isset($error.message_link) && !empty($error.message_link_label)}
                                        {$error.message_link_label}
                                    {else}
                                        {ci_language line="Learn More"}
                                    {/if}
                                </a>
                            </p>
                        {/if}  

                    </div>
                    <i class="fa fa-remove"></i>
                </div>                        
            {/foreach}
           
        {/if}

        {if isset($message_code.warning)}
           
            {foreach $message_code.warning as $warning}
                <div class="notification warning message_code" style="display:none;">
                    <div class="container">
                        <p>
                            {if isset($warning.message_name) && !empty($warning.message_name)}
                                <span class="bold">
                                    {$warning.message_name} 
                                    {if isset($warning.require_country) && $warning.require_country && isset($country)}[ {$country} ]{/if} : 
                                </span>
                            {/if}
                        </p> 
                        <p>    
                            {if isset($warning.message_problem) && !empty($warning.message_problem)}{$warning.message_problem}{/if}
                            {if isset($warning.message_cause) && !empty($warning.message_cause)}{$warning.message_cause}{/if}
                            {if isset($warning.message_solution) && !empty($warning.message_solution)}{$warning.message_solution}{/if}                            
                        </p>

                        <!--Link-->
                        {if isset($warning.message_link) && !empty($warning.message_link)}
                            <p class="text-right">
                                <a class="link" href="{$base_url}{$warning.message_link}{if isset($warning.require_country) && $warning.require_country && isset($id_country)}/{$id_country}{/if}">
                                    {if isset($warning.message_link) && !empty($warning.message_link_label)}
                                        {$warning.message_link_label}
                                    {else}
                                        {ci_language line="Learn More"}
                                    {/if}
                                </a>
                            </p>
                        {/if}  

                        <i class="fa fa-remove"></i>
                    </div>
                </div>
            {/foreach}
                       
        {/if}

        {if isset($message_code.info)}
         
            {foreach $message_code.info as $info}
                <div class="notification wait message_code" style="display:none;">
                    <div class="container">
                        <p>
                            {if isset($info.message_name) && !empty($info.message_name)}
                                <span class="bold"> 
                                    {$info.message_name}
                                    {if isset($info.require_country) && $info.require_country && isset($country)}[ {$country} ]{/if} : 
                                </span>
                            {/if}
                        </p>
                        <p>
                            {if isset($info.message_problem) && !empty($info.message_problem)}{$info.message_problem}{/if}
                            {if isset($info.message_cause) && !empty($info.message_cause)}{$info.message_cause}{/if}
                            {if isset($info.message_solution) && !empty($info.message_solution)}{$info.message_solution}{/if}                           
                        </p>

                        <!--Link-->
                        {if isset($info.message_link) && !empty($info.message_link)}
                            <p class="text-right">
                                <a class="link" href="{$base_url}{$info.message_link}{if isset($info.require_country) && $info.require_country && isset($id_country)}/{$id_country}{/if}">
                                    {if isset($info.message_link) && !empty($info.message_link_label)}
                                        {$info.message_link_label}
                                    {else}
                                        {ci_language line="Learn More"}
                                    {/if}
                                </a>
                            </p>
                        {/if}  

                        <i class="fa fa-remove"></i>
                    </div>
                </div>
            {/foreach}                                   
           
        {/if}

    {/if} 

    <script src="{$cdn_url}assets/js/feedbiz.message_code.js"></script>   
{/if} 