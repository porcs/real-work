{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/feedbiz/service/packages.css">
<form id="frmPackages" method="post" class="pkg_ov form-horizontal">
<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="package_package"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">{ci_language line="package_wizard_package"}</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">2</span>
							<span class="title">{ci_language line="package_wizard_info"}</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">3</span>
							<span class="title">{ci_language line="package_wizard_complete"}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row"><form id="frmPackages" method="post" class="pkg_ov form-horizontal">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
                                                    
                                                                <div class="col-sm-6">
                                                                        <h4 class="headSettings_head">{ci_language line="package_title"}</h4>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                        <div class="row">
                                                                                <div class="col-sm-5"><p class="m-t10 light-gray">{ci_language line="Currency"}</p></div>
                                                                                <div class="col-sm-7">
                                                                                        <select name="currency" id="currency" class="search-select">
                                                                                                {foreach $arrCurrency as $key => $val}
                                                                                                    <option value="{$key}"{if isset($currency) && $currency==$key} selected="selected"{/if}>{$val}</option>
                                                                                                {/foreach}
                                                                                        </select>
                                                                                </div>
                                                                        </div>
                                                                </div>
							
						</div>
						<div class="row">
							<div class="col-xs-12">
								<ul class="nav nav-tabs">
                                                                    {if $offer} 
                                                                        {foreach $offer as $field key=k}
                                                                            <li {if $k==0}class="active"{/if}><a href="#pkg{$k}" data-toggle="tab">{$field['name_offer_pkg']}</a></li>
                                                                        {/foreach}
                                                                    {/if}						
								</ul>
							</div>
						</div>
						<div class="tab-content">
                                                    {if $offer}
                                                        {assign var="last_region" value=''}
                                                        {foreach $offer as $field key=k}
                                                            <div class="tab-pane {if $k==0}active{/if}" id="pkg{$k}">
								<div class="row">
									<div class="checkboSettings col-sm-12">
                                                                                <p class="m-t10 pull-left">
                                                                                   {ci_language line=$basepackage['domain_offer_sub_pkg']}
                                                                                </p>
										<p class="m-t10 pull-right">
                                                                                    
                                                                                    <label class="cb-checkbox pull-left">
                                                                                        {if isset($field['chk']) && $field['chk']}
                                                                                            <input type="checkbox" name="offer_pkgs[]" pkt="{if isset($field['default_id'])}{$field['default_id']}{/if}" value="{$field['id_offer_pkg']}" checked="checked" class="hidden">
                                                                                            <input type="checkbox" checked="checked" disabled="disabled">
                                                                                        {else}
                                                                                            <input{if isset($field['checked_item']) && $field['checked_item']} checked="checked"{/if}   pkt="{if isset($field['default_id'])}{$field['default_id']}{/if}" type="checkbox" name="offer_pkgs[]" id="offer_pkgs_{$field['id_offer_pkg']}" class="main_pk  hidden"  value="{$field['id_offer_pkg']}">
                                                                                        {/if} 
                                                                                    </label>
                                                                                    
                                                                                     <span class="true-pink">{$currencySign}{$basepackage['packageAmount']}</span> {if $basepackage['priod_offer_price_pkg'] == 'month'}{ci_language line="per/month"}{/if}{if $basepackage['priod_offer_price_pkg'] == 'time'}{ci_language line="for livetime"}{/if}

                                                                                    {*<span class="true-pink">{$currencySign}{$field['packageAmount']}</span> {if $field['priod_offer_price_pkg'] == 'month'}{ci_language line="per/month"}{/if}{if $field['priod_offer_price_pkg'] == 'time'}{ci_language line="for livetime"}{/if}*}
                                                                                    {if $basepackage['pk_status'] == 1}
                                                                                        <span class="label label-success">{ci_language line="Purchased"}</span>
                                                                                    {/if}
                                                                                    {if $basepackage['pk_status'] == 2}
                                                                                        <span class="label label-important">{ci_language line="Expired"}!</span>
                                                                                    {/if}
                                                                                </p>
									</div>
								</div>
                                                                {if isset($field['sub']) && $field['sub']}
                                                                    {foreach $field['sub'] as $ks=>$val name=foo_pkg}
                                                                    {if $val['name_region'] != $last_region}
                                                                        {if $ks > 0 }</div>{/if}
								<div class="allCheckBo">
									<div class="mainCheckbo">
											{$val['name_region']}
									</div>
                                                                        {$last_region = $val['name_region'] }
                                                                    {/if}
                                                                    {if $val['element_offer_sub_pkg'] == 'play_us' }<div {*class="allCheckBo"*}>{/if}
									<div class="checkboSettings clearfix">
										<label class="cb-checkbox pull-left">
											<input type="checkbox" name="{$val['element_offer_sub_pkg']}[]" pkt="{$field['id_offer_pkg']}"  value="{$val['id_offer_price_pkg']}"{if isset($val['checked'])} checked="checked"{/if}
                                                                                   {if $val['offer_pkg_type'] == 1} class="default sub"{else} class="sub " {/if} />
											{$val['domain_offer_sub_pkg']} {if !empty($val['title_offer_sub_pkg'])}({$val['title_offer_sub_pkg']}){/if}
                                                                                        {if $val['pk_status'] == 1}
                                                                                            <span class="label label-success">{ci_language line="Purchased"}</span>
                                                                                        {/if}
                                                                                        {if $val['pk_status'] == 2}
                                                                                            <span class="label label-important"  >{ci_language line="Expired"}!</span>
                                                                                        {/if}
										</label>
										<p class="m-t10 pull-right"><span class="true-pink">+ {$currencySign} {$val['subPackageAmount']}</span> {if $val['priod_offer_price_pkg'] == 'month'} {ci_language line="per/month"}{/if}{if $val['priod_offer_price_pkg'] == 'time'}{ci_language line="for livetime"}{/if}</p>
									</div>
                                                                    {if $smarty.foreach.foo_pkg.last & $ks >= 0}</div>{/if}
                                                                    {/foreach}
                                                                {/if}
							</div>
                                                        {/foreach}
                                                        {/if}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">{ci_language line="package_summary"}</h4>						
							</div>
						</div>
                                                <div class="overview-display allCheckBo">
                                                {if $arrOverviewOffer}
                                                    {foreach $arrOverviewOffer as $pktype}
                                                            {foreach $pktype as $field}
                                                                <div class="checkboSettings head clearfix">
                                                                        <label class="pull-left text-uc strong-text dark-gray">
                                                                              <b>{$field['name_offer_pkg']}</b>
                                                                        </label>
                                                                        {if isset($field['thisAmount']) && !empty($field['thisAmount'])}
                                                                        <p class="pull-right strong-text"><span class="true-pink">{$currencySign}{$field['thisAmount']}</span></p>
                                                                        {/if}
                                                                </div>
                                                            
                                                                {if isset($field['arrAmazonEU'])}
                                                                    {foreach $field['arrAmazonEU'] as $field2}
                                                                <div class="checkboSettings clearfix">
                                                                        <label class="pull-left">
                                                                                {$field2['domain_offer_sub_pkg']}
                                                                        </label>
                                                                        <p class="pull-right"><span class="true-pink">+ {$currencySign}{$field2['thisAmountAmazon']}</span></p>
                                                                </div>
                                                                    {/foreach}
                                                                {/if}
                                                            {/foreach}
                                                    {/foreach}
                                                    <div class="checkboSettings head clearfix b-Bottom">
                                                            <label class="pull-left text-uc strong-text dark-gray">
                                                                  <b>{ci_language line="package_select_subtotal"}</b>
                                                            </label>
                                                            <p class="pull-right strong-text"><span class="true-green package_select_subtotal_amt">{$currencySign}{$allTotalAmount}</span></p>
                                                    </div>
                                                {else}
                                                    <div class="b-Bottom m-t10 m-b10">
                                                            <p class="text-center italic poor-gray">{ci_language line="No package selected"}</p>
                                                    </div>
                                                {/if}
					</div>
						<div class="pull-right m-t20">
							{*<p href="" class="poor-gray p-size pull-left m-tr10">
								You can add packages from other tabs such as eBay and Play.com
							</p>*}
							<button type="button" class="btn btn-save btnNext" {if isset($initBtn)}{$initBtn}{/if}>{ci_language line="checkout_btn_next"} <i class="m-l5 fa fa-angle-right"></i></button>
                                                        <input type="hidden" id="thisurl" value="{base_url('service/packages')}">
						</div>
                                    </div>
				</div>
			</div>
		</div>
        </div>
    </form>

<script language="javascript">
    var siteurl = "{$base_url}";
</script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/billing/packages.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.package.js"></script>                     
{include file="footer.tpl"}