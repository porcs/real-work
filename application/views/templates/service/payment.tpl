{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="package_payment"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings extend clearfix m-b10">
					<ul class="headSettings_point clearfix">
						<li class="headSettings_point-item active">
							<span class="step">1</span>
							<span class="title">{ci_language line="package_wizard_payment"}</span>
						</li>
						<li class="headSettings_point-item">
							<span class="step">2</span>
							<span class="title">{ci_language line="package_wizard_complete"}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
                {if $arrOverviewOffer}
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-sm-6">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">{ci_language line="package_overview"}</h4>						
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<p class="m-t10 m-b0 dark-gray montserrat">{ci_language line="checkout_order_pkg_period"} :</p>
								<p class="light-gray">{$startDate} - {$endDate}</small>
							</div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-5"><p class="m-t20 light-gray">{ci_language line="Currency"}</p></div>
									<div class="col-sm-7 m-t10 m-b10">
										<select name="currency" id="currency" class="search-select">
                                                                                    {foreach $arrCurrency as $key => $val}
                                                                                        <option value="{$key}"{if isset($currency) && $currency==$key} selected="selected"{/if}>{$val}</option>
                                                                                    {/foreach}
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="allCheckBo b-Top">
                                                    {if $arrOverviewOffer }
                                                        {foreach $arrOverviewOffer  as $field}
							<div class="checkboSettings clearfix">
                                                                <p class="pull-left">
                                                                    {$field['name_offer_pkg']}                                                                    
                                                                </p>
								<p class="pull-right dCell"><span class="true-pink"> {$currencySign}&nbsp;{$field['thisAmount']}</span> {if $field['period'] == 'month'}{ci_language line="per/month"}{/if}{if $field['period'] == 'livetime'}{ci_language line="for livetime"}{/if}</p>
							</div>
                                                        {/foreach}
                                                    {/if}
                                                    {if $discount_date > 0 || $discount > 0 || $giftDC > 0}
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">
									{ci_language line="package_select_subtotal"}
								</p>
								<p class="pull-right strong-text"><span class="true-green"> {$currencySign}&nbsp;{$allsubTotalAmount}</span> {*{if $field['period'] == 'month'}{ci_language line="per/month"}{/if}{if $field['period'] == 'livetime'}{ci_language line="for livetime"}{/if}*}</p>
							</div>
                                                    {/if}
                                                    {if $giftDC > 0}
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">
									{$giftQuery['name_giftopt']}
								</p>
								<p class="pull-right strong-text"><span class="true-green"> {$currencySign}&nbsp;{$giftDC}</span> </p>
							</div>
                                                    {/if}
                                                    {if $discount > 0}
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">
									{ci_language line="package_select_referral"}
								</p>
								<p class="pull-right strong-text"><span class="true-green"> {$currencySign}&nbsp;{$discount}</span> </p>
							</div>
                                                    {/if}
                                                    {if $discount_date > 0}
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">
									{ci_language line="Package discount"}
								</p>
								<p class="pull-right strong-text"><span class="true-green"> -{$currencySign}&nbsp;{$discount_date}</span> </p>
							</div>
                                                    {/if}
                                               
							<div class="checkboSettings head clearfix b-Bottom">
								<p class="pull-left text-uc strong-text dark-gray">
									{ci_language line="package_select_total"}
								</p>
								<p class="pull-right strong-text"><span class="true-green"> {$currencySign}&nbsp;{$allTotalAmount}</span> </p>
							</div>
						</div>					
					</div>
                                                        
                                <form id="frmPayment" method="post">
					<div class="col-sm-6 custom-form">
						<div class="headSettings clearfix b-Bottom p-b10">
							<div class="pull-md-left clearfix">
								<h4 class="headSettings_head">{ci_language line="checkout_pay_method"}</h4>						
							</div>
						</div>
						<label class="cb-radio m-t10 pull-left">
							<input type="radio" name="pay[method]" id="pay_express" value="express" class="validate[required]" {if $isPreapproval != 'active'} checked="checked" {/if} />
							<p class="text-uc dark-gray montserrat m-b0 ">{ci_language line="checkout_pay_express"}</p>  
							<p class="light-gray m-l25">{ci_language line="express_date"}</p>
						</label>
						<label class="cb-radio m-t10">
							<input type="radio" name="pay[method]" id="pay_preapproval" value="preapproval" class="validate[required]"   {if $isPreapproval == 'active'} checked="checked" {/if} />
							<p class="text-uc dark-gray montserrat m-b0">{ci_language line="checkout_pay_preapproval"}</p>
                                                        {if $isPreapproval == 'none'}
                                                            <p class="light-gray m-b0  m-l25">{ci_language line="preapproval_no"}</p>
                                                        {elseif $isPreapproval == 'active'}
                                                            <p class="light-gray m-b0 m-l25"> {ci_language line="preapproval_active"}</p>
                                                        {elseif $isPreapproval == 'inactive'}
                                                            <p class="light-gray m-b0 m-l25"> {ci_language line="preapproval_inactive"}, 
                                                                <a href="{$siteurl}billing/configuration">{ci_language line="preapproval_inactive_btn"}</a>
                                                            </p>
                                                        {/if}
                                                        <p class="light-gray m-l25">{$chDate}</p>
							{*<p class="light-gray m-b0 m-l25">Your pre-approval key is  inactive. <a href="" class="link">Change it now.</a></p>
							<p class="light-gray m-l25">We will automatically charge you every <span class="dark-gray">21st</span> of each month</p>*}
						</label>
						<div class="clearfix">
							<a href="https://www.paypal.com/webapps/mpp/paypal-popup" class="pp_link" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/pay-pal.png" alt=""></a>
							<a href="https://www.paypal.com/webapps/mpp/paypal-popup" class="pp_link" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/mastercard.png" alt=""></a>
							<a href="https://www.paypal.com/webapps/mpp/paypal-popup" class="pp_link" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/visa.png" alt=""></a>
							<a href="https://www.paypal.com/webapps/mpp/paypal-popup" class="pp_link" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/express.png" alt=""></a>
							<a href="https://www.paypal.com/webapps/mpp/paypal-popup" class="pp_link" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;"><img class="pull-left m-r10 m-t10" src="{$cdn_url}assets/images/pay/discover.png" alt=""></a>
						</div>                                               
                                

                                    <div class="row-fluid layout-credit hide">
                                        <div class="span12">
                                            <div class="profile-user-info margin-top10" >
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            &nbsp;
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">

                                                        <ul class="cards">
                                                            <li class="mastercard">{ci_language line="MasterCard"}</li>
                                                            <li class="visa">{ci_language line="Visa"}</li>
                                                            <li class="discover">{ci_language line="Discover"}</li>                                                                        
                                                            <li class="amex">{ci_language line="Amex"}</li>
                                                        </ul>

                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_ccnum"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[card_number]" id="card_number" class="validate[required,funcCall[checkCCValid]] form-control" placeholder="{ci_language line="checkout_pay_cc_ccnum"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_cvv"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[cvv]" id="cre_cvv" class="validate[required] form-control input-mini" placeholder="{ci_language line="checkout_pay_cc_cvv"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_firstname"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[first_name]" id="cre_first_name" class="validate[required] form-control" placeholder="{ci_language line="checkout_pay_cc_firstname"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_lastname"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[last_name]" id="cre_last_name" class="validate[required] form-control" placeholder="{ci_language line="checkout_pay_cc_lastname"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_exp"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <div class="form-inline">
                                                            <select name="cre[exp_mm]" id="pay_exp_month" class="input-medium">
                                                                <option value="" selected="selected">-- {ci_language line="checkout_pay_cc_month"} --</option>
                                                                <option value="01">{ci_language line="January"}</option>
                                                                <option value="02">{ci_language line="February"}</option>
                                                                <option value="03">{ci_language line="March"}</option>
                                                                <option value="04">{ci_language line="April"}</option>
                                                                <option value="05">{ci_language line="May"}</option>
                                                                <option value="06">{ci_language line="June"}</option>
                                                                <option value="07">{ci_language line="July"}</option>
                                                                <option value="08">{ci_language line="August"}</option>
                                                                <option value="09">{ci_language line="September"}</option>
                                                                <option value="10">{ci_language line="October"}</option>
                                                                <option value="11">{ci_language line="November"}</option>
                                                                <option value="12">{ci_language line="December"}</option>
                                                            </select>
                                                            <select name="cre[exp_yy]" id="pay_exp_year" class="input-medium">
                                                                <option value="" selected="selected">-- {ci_language line="checkout_pay_cc_year"} --</option>
                                                                {for $i=(date('Y')-1) to (date('Y')+15)}
                                                                <option value="{$i}">{$i}</option>
                                                                {/for}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_street"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[street]" id="cre_street" class="validate[required] form-control" placeholder="{ci_language line="checkout_pay_cc_street"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_city"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[city]" id="cre_city" class="validate[required] form-control input-small" placeholder="{ci_language line="checkout_pay_cc_city"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_state"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[state]" id="cre_state" class="validate[required] form-control input-small" placeholder="{ci_language line="checkout_pay_cc_state"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_country"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[country]" id="cre_country" class="validate[required] form-control input-small" placeholder="{ci_language line="checkout_pay_cc_country"}">
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name">
                                                        <span>
                                                            {ci_language line="checkout_pay_cc_zip"}
                                                        </span>
                                                    </div>

                                                    <div class="profile-info-value">
                                                        <span>
                                                            <input type="text" name="cre[zip]" id="cre_zip" class="validate[required] form-control input-small" placeholder="{ci_language line="checkout_pay_cc_zip"}">
                                                        </span>
                                                    </div>
                                                </div>
                                        <input type="hidden" name="cre[cctype]" id="cctype" value="">
                                            </div>
                                        </div>
                                    </div>
                                
                                                
						<div class="row">
							<div class="col-xs-12 b-Top m-t20 p-t20">
								<div class="pull-right">
                                                                        
									<button type="submit" class="btn btn-save btnNext">{ci_language line="checkout_btn_next"} <i class="m-l5 fa fa-angle-right"></i></button>
								</div>
							</div>
						</div>
                                         
					</div></form>      
				</div>
			</div>
		</div>
              {/if}
        </div>
		

<script language="javascript" src="{$cdn_url}assets/js/jquery.validationEngine.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.validationEngine-en.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.payment.js"></script>

<script language="javascript">
    
    {*$("head").append('<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/fonts.css">');
    $("head").append('<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/style.css">');
    $("head").append('<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/validationEngine.jquery.css">');
    $("head").append('<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/creditcard.css" />');*}
    
    var siteurl = "{$siteurl}";
    var err = "{$err}";
    
      var thisurl = '{$base_url}service/payment/extend'; 
     
    
</script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/service/payment.js"></script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.creditCardValidator.js"></script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.creditCardValidator.action.js"></script>

 
{include file="footer.tpl"}