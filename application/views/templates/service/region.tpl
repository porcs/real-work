{*not use / old file*}
{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/fonts.css">
<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/style.css">

<div class="main-content">
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">
        <div class="page-header position-relative">
            <h1>
                {ci_language line="package_title"}
                <small>
                    <i class="icon-double-angle-right"></i> 
                    {ci_language line="package_region"}
                </small>
            </h1>
        </div>
        
        <div class="row-fluid">
            <div class="span12">                
                <div style="position: relative;">
                    <ul class="wizard-steps">
                        <li class="active" style="min-width: 25%; max-width: 25%;">
                            <span class="step">1</span>
                            <span class="title">{ci_language line="package_wizard_region"}</span>
                        </li>

                        <li style="min-width: 25%; max-width: 25%;">
                            <span class="step">2</span>
                            <span class="title">{ci_language line="package_wizard_package"}</span>
                        </li>

                        <li style="min-width: 25%; max-width: 25%;">
                            <span class="step">3</span>
                            <span class="title">{ci_language line="package_wizard_info"}</span>
                        </li>
                        
                        <li style="min-width: 25%; max-width: 25%;">
                            <span class="step">4</span>
                            <span class="title">{ci_language line="package_wizard_complete"}</span>
                        </li>
                    </ul>
                    <br class="package-lbl-clear">
                </div>
                
                <hr>
                
                <form id="frmRegion" method="post" class="form-horizontal">
                    
                    <div style="display: block;text-align: center">
                        <h3 class="blue">{ci_language line="package_region_detail"}</h3>
                    </div>
                    
                    <div class="region-wrapper">
                        <div class="region-map"></div>
                        
                        {if $region}
                        {foreach $region as $field}
                        <div style="position: absolute;top: {$field['map_y_region']}px;left: {$field['map_x_region']}px;">
                            <label>
                                <input type="checkbox" id="chk_{$field['id_region']}" name="region[]" value="{$field['id_region']}_{$field['name_region']}"{if !empty($sessionRegion[$field['id_region']])} checked="checked"{/if}>
                                <span class="lbl" style="font-weight: bold;"> {$field['name_region']}</span>
                            </label>
                        </div>
                        {/foreach}
                        {/if}
                        
                        <div class="control-group" style="margin-top: 20px;">
                            <label class="control-label">
                                {ci_language line="package_region_currency"}
                            </label>

                            <div class="controls">
                                <select name="currency" id="currency" class="currency-font input-medium">
                                {foreach $arrCurrency as $key => $val}
                                    <option value="{$key}"{if $currency==$key} selected="selected"{/if}>{$val}</option>
                                {/foreach}
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="package-control">
                        <div class="button-right">
                            <button type="button" class="btn btn-primary btnNext"{if $initBtn}{$initBtn}{/if}>
                                {ci_language line="checkout_btn_next"} 
                                <i class="icon-double-angle-right"></i>
                            </button>
                        </div>
                        <br class="package-lbl-clear">
                    </div>
                    
                </form>
                    
            </div>
        </div>
        
    </div>
    
</div>

<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.region.js"></script>
                        
{include file="footer.tpl"}