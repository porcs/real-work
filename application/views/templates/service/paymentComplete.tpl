{include file="header.tpl"} 
{ci_config name="base_url"} 
{include file="sidebar.tpl"} 
<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/fonts.css">
<link rel="stylesheet" type="text/css" href="{$cdn_url}assets/css/style.css">

<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/service/service_step.css" />
<div class="main p-rl40 p-xs-rl20">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="package_complete"}</h1>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
 
        <div class="row">
                <div class="col-xs-12">
                        <div class="headSettings clearfix m-b10">
                                <ul class="headSettings_point clearfix">
                                    <li class="headSettings_point-item active">
                                                <span class="step">1</span>
                                                <span class="title">{ci_language line="package_wizard_package"}</span>
                                        </li> 
                                        <li class="headSettings_point-item active">
                                                <span class="step">2</span>
                                                <span class="title">{ci_language line="package_wizard_complete"}</span>
                                        </li> 
                                </ul>
                        </div>
                </div>
        </div>
        
        <div class="row">
                <div class="col-xs-12">
           
                
                <hr>
                
                <form id="frmComplete" method="post" class="form-horizontal">
                    
                    <div class="center">
                        <h3 class="green">{ci_language line="complete_pay_title"}</h3>
                        <p>
                            {ci_language line="complete_pay_detail"} 
                            <strong>{if isset($billID) && $billID != ''}{$billID}{/if}</strong>
                        </p>
                        <p>
                            <button type="button" class="btn btn-app btn-yellow btn-mini btn-print" data-id="{if isset($billID) && $billID != ''}{$billID}{/if}">
                                <i class="icon-print bigger-160"></i>
                                {ci_language line="checkout_btn_print"}
                            </button>
                        </p>
                    </div>
                    
                    <div class="package-control">
                        <div class="button-left">
                            &nbsp;
                        </div>
                        <div class="button-right">
                            <button type="button" class="btn btn-primary btnNext">
                                {ci_language line="finish"}  
                                <i class="icon-double-angle-right"></i>
                            </button>
                        </div>
                        <br class="package-lbl-clear">
                    </div>
                </form>
                    
            </div>
        </div>
        
    </div>
    
</div>

<input type="hidden" id="return2package" value="{$return2package}">

<script language="javascript">
    var siteurl = "{$siteurl}";
</script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.paymentComplete.js"></script>
     
{include file="footer.tpl"}
