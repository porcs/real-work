{if $arrOverviewOffer}
    {foreach $arrOverviewOffer as $pktype}
            {foreach $pktype as $field}
                <div class="checkboSettings head clearfix">
                        <label class="pull-left text-uc strong-text dark-gray">
                              <b>{$field['name_offer_pkg']}</b>
                        </label>
                        {if isset($field['thisAmount']) && !empty($field['thisAmount'])}
                            <p class="pull-right strong-text"><span class="true-pink">{$currencySign}{$field['thisAmount']}</span></p>
                        {/if}
                </div>

                {if isset($field['arrAmazonEU'])}
                    {foreach $field['arrAmazonEU'] as $field2}
                <div class="checkboSettings clearfix">
                        <label class="pull-left">
                                {$field2['domain_offer_sub_pkg']}
                        </label>
                        <p class="pull-right"><span class="true-pink">+ {$currencySign}{$field2['thisAmountAmazon']}</span></p>
                </div>
                    {/foreach}
                {/if}
            {/foreach}
    {/foreach}
    <div class="checkboSettings head clearfix b-Bottom">
            <label class="pull-left text-uc strong-text dark-gray">
                  <b>{ci_language line="package_select_subtotal"}</b>
            </label>
            <p class="pull-right strong-text"><span class="true-green package_select_subtotal_amt">{$currencySign}{$allTotalAmount}</span></p>
    </div>
{else}
    <div class="b-Bottom m-t10 m-b10">
            <p class="text-center italic poor-gray">{ci_language line="No data available in table"}</p>
    </div>
{/if}