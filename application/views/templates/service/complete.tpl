{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="package_complete"}</h1>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        
        <div class="row">
                <div class="col-xs-12">
                        <div class="headSettings clearfix m-b10">
                                <ul class="headSettings_point clearfix">
                                        <li class="headSettings_point-item active">
                                                <span class="step">1</span>
                                                <span class="title">{ci_language line="package_wizard_package"}</span>
                                        </li>
                                        <li class="headSettings_point-item active">
                                                <span class="step">2</span>
                                                <span class="title">{ci_language line="package_wizard_info"}</span>
                                        </li>
                                        <li class="headSettings_point-item active">
                                                <span class="step">3</span>
                                                <span class="title">{ci_language line="package_wizard_complete"}</span>
                                        </li>
                                </ul>
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10">
                                <h4 class="headSettings_head">{ci_language line="Billing Information"}</h4>	
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-xs-12">
                        <div class="headSettings clearfix b-Bottom p-b10 text-center">
                                <h4 class="headSettings_head pull-none col-xs-12">{ci_language line="complete_title"}</h4>	
                                <p class="light-gray regRoboto">{ci_language line="complete_detail"}</p>
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-xs-6 m-t20">
                        <a href="" class="link p-size pull-right m-t8 btnPrev">{ci_language line="checkout_btn_prev"}</a>				
                </div>
                <div class="col-xs-6 m-t20">
                        <button class="btn btn-save btnNext">{ci_language line="finish"}</button>
                </div>
        </div>
</div>

<script language="javascript">
    var siteurl = "{$siteurl}";
</script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/jquery.page.complete.js"></script>
                        
{include file="footer.tpl"}