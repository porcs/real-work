{ci_language file="$label" lang="$lang"}
{ci_language file="header" lang="$lang"}
{ci_config name="base_url"}
    {if !isset($cdn_url)} 
        {assign var="cdn_url" value=$base_url} 
    {/if}
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <html> <![endif]-->
<!--[if !IE]><!-->
<html><!-- <![endif]-->
<head>
    <title>{if isset($page_title_specify)}{$page_title_specify}{else}{ci_language line="page_title"} - {if isset($page_name)}{ucfirst(str_replace("_", " ", $page_name))}{/if}{/if}</title>
    <!--<meta name="description" content="{*ci_language line="page_description"*}" />-->
    <!--<meta name="keywords" content="{*ci_language line="page_keyword"*}" />-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="SHORTCUT ICON" href="{$cdn_url}assets/images/favicon.ico"/>
    <link rel="ICON" href="{$cdn_url}assets/images/favicon.ico"/>
    
    <!-- = = New Theme = = -->
    <link rel="shortcut icon" type="image/x-icon" href="{$cdn_url}assets/images/favicon.ico" />
    <meta name="viewport" content="width=device-width, user-scalable=no">   
    
    <!-- css -->
    <link rel="stylesheet" href="{$cdn_url}assets/css/bootstrap/bootstrap.css"> 
    <link rel="stylesheet" href="{$cdn_url}assets/css/font-awesome-4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{$cdn_url}assets/css/fancybox.css">     
    <link rel="stylesheet" href="{$cdn_url}assets/css/checkBo.css">
    <link rel="stylesheet" href="{$cdn_url}assets/css/style.css">
    <link rel="stylesheet" href="{$cdn_url}assets/css/colorbox3.css" />  
    <link rel="stylesheet" href="{$cdn_url}assets/css/custom.css" />
    <!-- css tooltipster -->
    <link rel="stylesheet" href="{$cdn_url}assets/css/tooltipster.css" />
    <link rel="stylesheet" href="{$cdn_url}assets/css/themes/tooltipster-shadow.css" />
    
    <!-- Library -->
    <script src="{$cdn_url}assets/js/jquery/jquery-1.11.2.min.js"></script>
    <script src="{$cdn_url}assets/js/jquery/jquery-ui.min.js"></script>
    <!-- Library toottipster -->
    <script src="{$cdn_url}assets/js/jquery.tooltipster.min.js"></script>   

    <!-- Draggable touch-enabled mobile devices. -->
    <script src="{$cdn_url}assets/js/jquery.ui.touch-punch.min.js"></script>
    
    {if isset($logged)}
        <script src="{$cdn_url}assets/js/feedbiz.custom.js"></script>       
        {if !isset($popup) || !$popup}
            <script src="{$cdn_url}assets/js/feedbiz_process.js"></script> 
        {/if} 
    {/if}
    <script>
        var base_url = '{$base_url}';
        {if isset($cdn_url)}
        var cdn_url = '{$cdn_url}';
        {else}
            {assign var="cdn_url" value=$base_url}
        var cdn_url = base_url;
        {/if}
    </script>
    
       
</head>

<body class="loginWrapper breakpoint-992 breakpoint-768" {if isset($popup) && $popup != ''} style="padding:0px;"{/if}>
    
    <header class="navbar-fixed-top" role="navigation" {if isset($popup) && $popup != ''} style=" display:none"{/if}>
        
        <div class="logo">
    		<a href=""><img src="{$cdn_url}assets/images/main-logo.png" alt="" class="logo-picture"></a>
    		{if isset($logged)}
                    <button class="btn btn-menu">
    			<span class="icon-bar"></span>
    			<span class="icon-bar"></span>
    			<span class="icon-bar"></span>
    			<span class="icon-bar"></span>
    		</button>
            {/if}
    	</div>

        {if isset($logged)}
            
            <nav class="nav">
                    <ul class="loginBlock profile">
                            <li class="search">
                                <form id="search" method="post" action="{$base_url}help/search" enctype="multipart/form-data" autocomplete="off" >
                                    <input class="form-control" name="search_string" />
                                    <i class="btn-MainSearch"></i>
                                </form>
                            </li>
                            {*$notifications|@print_r*}
                            {if isset($notifications)}
                            <li class="logNotification">                             
                                <a tabindex="0" role="button" class="notificationicon on" >
                                    {if isset($notifications['count']) && $notifications['count'] > 0}
                                    <span class="badge badge-warning pull-right">{$notifications['count']}</span>
                                    {/if}
                                    <i class="fa fa-bell fa-2x"></i>
                                </a>
                                <ul id="notificationMenu" class="notifications-popup hidden">
                                    <li class="titlebar">
                                      <span class="title">{ci_language line="Notifications"}</span>                                      
                                      </span>
                                    </li>                                    
                                    <div class="notifbox">
                                        
                                    {foreach $notifications as $nkey => $notification}
                                        {if isset($notification['marketplace'])}
                                        <li class="notif{if isset($notification['flag_unread']) && $notification['flag_unread']} unread{/if}">
                                            <a href="#" onclick="flagUnreadNotification('{$notification['marketplace']}', '{$notification['id_shop']}', '{$notification['id_country']}', '{$notification['notification_identify']}')">
                                                  <div class="imageblock"> 
                                                    <img src="{$base_url}assets/images/marketplace/{$notification['marketplace']}-icon.png" class="notifimage" width="45" height="45" />
                                                  </div> 
                                                <div class="messageblock">
                                                    {if isset($notification['notification_message'])}
                                                        <div class="message">{$notification['notification_message']}</div>
                                                    {/if}
                                                    {if isset($notification['notification_date'])}
                                                    <div class="messageinfo m-t2">
                                                        {*<i class="fa fa-clock-o"></i>*}
                                                        {if isset($notification['notification_date']['time'])}
                                                            {if $notification['notification_date']['time'] <= 0}
                                                                {ci_language line="now"}
                                                            {else}
                                                                {$notification['notification_date']['time']}
                                                                
                                                                {if isset($notification['notification_date']['ago'])}
                                                                    {ci_language line="{$notification['notification_date']['ago']}"}
                                                                {/if} 
                                                                {ci_language line="ago"}
                                                            {/if} 
                                                        {/if} 
                                                    </div>
                                                    {/if}
                                                </div>
                                            </a>
                                        </li>
                                        {/if}
                                    {/foreach}
                                    </div>

                                    {if count($notifications) > 10}
                                    <li class="seeall">
                                      <a>{ci_language line="See All"}</a>
                                    </li>
                                    {/if}
                                  </ul>
                            </li>
                            {/if}
                            <li>
                                {assign var="path" value=getcwd()}
                                {assign var="image" value="$path/assets/images/profile/35/$user_id.jpg"} 
                                {if file_exists($image)}   
                                    <a href="{$base_url}users/profile"><img class="icon-avatar icon-circle" src="{$base_url}assets/images/profile/35/{$user_id}.jpg" alt=""></a>
                                {else} 
                                    <a href="{$base_url}users/profile"><img class="icon-avatar icon-circle" src="{$base_url}assets/images/profile/empty-profile.png" alt=""></a>
                                {/if}
                            </li>
                            {if isset($user_first_name) or isset($user_las_name)}
                                <li class="username hidden-xs">
                                    <a href="{$base_url}users/profile">{if isset($user_first_name)}{$user_first_name}{/if} {if isset($user_las_name)}{$user_las_name}{/if}</a>
                                </li>
                            {/if}
                            <li class="language">
                                <a href>
                                    {if isset($lang)}{strtoupper(mb_substr($lang, 0, 2))}{else}EN{/if}
                                </a>
                                <ul class="language_block">
                                    <li>                                        
                                        <button type="button" class="link language_translate" value="english">
                                            {ci_language line="English"}
                                        </button>
                                    </li>
                                    <li>
                                        <button type="button" class="link language_translate" value="french">
                                            {ci_language line="French"}
                                        </button>
                                    </li>
                                </ul>
                            </li>
                            <li class="logOut">
				<a href="{$base_url}users/logout">{*ci_language line="Log out"*} <i class="icon-logOut"></i></a>
                            </li>
                    </ul>
                    <i class="btn btn-more"></i>
            </nav>            
            <input type="hidden" id="language_translate_success" value="{ci_language line="Success"}">
            <input type="hidden" id="language_translate_error" value="{ci_language line="An error has occurred on language translation"}">
        {else}            
            <nav class="nav">
		<ul class="loginBlock">
			<li><a href="{$base_url}users/login">{ci_language line="Login"}</a></li>
			<li><a href="{$base_url}users/register">{ci_language line="sign_up"}</a></li>
		</ul>
		<i class="btn btn-more"></i>
            </nav>
        {/if}
    </header>

    {*{if isset($expert_mode) && $expert_mode && isset($mode_default) && $mode_default == 1}
    <div id="notification_mode" class="notification_mode">
        <div class="container">
            <p><i class="fa fa-lock"></i> 
                {ci_language line="This option is in expert mode, please activate expert mode."} 
                <a class="link" data-toggle="modal" data-target="#upgrade_mode">
                    <i class="fa fa-star"></i> {ci_language line="upgrade mode"}
                </a> 
            </p>
        </div>
    </div>
    {/if}*}

    <div class="notifications">
        {if isset($error) }
            <div class="notification{if isset($popup) && $popup != ''} popup{/if} bad">
                <div class="container">
                    <p><i class="fa fa-exclamation-triangle"></i> {$error}</p>
                    <i class="fa fa-remove"></i>
                </div>
            </div>
        {/if}
        {if isset($message) }
            <div class="notification{if isset($popup) && $popup != ''} popup{/if} good">
                <div class="container"><p><i class="fa fa-check"></i> {$message}</p>
                    <i class="fa fa-remove"></i>
                </div>
            </div>
        {/if}
        <div class="notification bad message_code {if isset($popup) && $popup != ''} popup{/if} " style="display:none;" >
            <div class="container">
                <p></p>
                <i class="fa fa-remove"></i>
            </div>
            <a href="header.tpl"></a>
        </div>

        <div class="notification{if isset($popup) && $popup != ''} popup{/if} good ajax_notify">
            <div class="container">
                <p>
                    {ci_language line="Please wait"}...
                </p>
                <i class="fa fa-remove"></i>
            </div>
        </div>
    </div>
    
    <div class="display-none" >
        <div id="user_inbox" class="col-xs-12 clearfix p-0">
                <ul>
                    <li>
                        <script> 
                            $.ajaxSetup({
                                headers: { 'X-Auth-Token': "{$ajax_token}" },
                            });
                        </script>
                    </li>
                </ul>
        </div>
    </div>
   
    <div class="{if isset($popup) && $popup != ''}popup-wrapper{else}page-wrapper{/if}">
	<div class="wrapper">    