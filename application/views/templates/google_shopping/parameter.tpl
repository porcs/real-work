<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10">
            {ci_language line="Setting"}
        </p>
    </div>
</div>

<div class="row m-t10">
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t5 text-right">{ci_language line="Active"} :</label>
            <div class="col-md-8">
                <div class="cb-switcher pull-left">
                    <label class="inner-switcher">
                        <input name="active" id="debug" type="checkbox" value="1" {if isset($active) && $active == 1}checked="checked"{/if} data-state-on="ON" data-state-off="OFF">
                    </label>
                    <span class="cb-state">{ci_language line="ON"}</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-t10">
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t5 text-right">{ci_language line="Export Shipping(carrier) ?"} :</label>
            <div class="col-md-8">
                <div class="cb-switcher pull-left">
                    <label class="inner-switcher">
                        <input name="export_shipping" id="debug" type="checkbox" value="1" {if isset($export_shipping) && $export_shipping == 1}checked="checked"{/if} data-state-on="ON" data-state-off="OFF">
                    </label>
                    <span class="cb-state">{ci_language line="ON"}</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10 b-Top">
            {ci_language line="Copy URL"}
        </p>
    </div>
</div>

<div class="row m-t10">
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t10 text-right">{ci_language line="URL"} : </label>
            <div class="col-md-8">
                <div class="col-xs-10 row">
                    <input class="form-control col-md-12 m-b10" type="text" id="merchant_id" name="url" {if isset($url_google_link)}value='{$url_google_link}'{/if} readonly="readonly" />
                    <p class="poor-gray text-right">{ci_language line="This URL use Google Feed"}.</p>
                </div>
                <div class="col-xs-2">
                    <button type="button" class="btn btn-rule copy_to_clipboard">{ci_language line="Copy"}</button>
                    <span class="copyAnswer"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="id_method" value="set_parameter" />
<input type="hidden" id="method" value="parameter" />
<input type="hidden" name="update-parameter-success" class="update-parameter-success" value="{ci_language line="update_parameter_success"}">
<input type="hidden" name="update-parameter-fail" class="update-parameter-fail" value="{ci_language line="update_parameter_fail"}">