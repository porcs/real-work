<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10">
            {ci_language line="Export feed file"}
        </p>
    </div>
</div>
 

<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10 b-Top">
            {ci_language line="Feed URL for scheduled fetches"}
        </p>
    </div>
</div>

<div class="row m-t10">
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t10 text-right">{ci_language line="Feed type - Products"} : </label>
            <div class="col-md-8">
                <div class="col-xs-10 row">
                    <input class="form-control col-md-12 m-b10" type="text" id="merchant_id" name="url" {if isset($url_google_link)}value='{$url_google_link}'{/if} readonly="readonly" />
                    <p class="poor-gray text-right">{ci_language line="This URL use Google Feed"}.</p>
                </div>
                <div class="col-xs-2">
                    <button type="button" class="btn btn-rule copy_to_clipboard">{ci_language line="Copy"}</button>
                    <span class="copyAnswer"></span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-md-12 form-group">
            <div class="col-sm-9">
                <label class="text-uc dark-gray montserrat col-md-4 p-t10 text-right">{ci_language line="Feed type - Online Products Inventory Update"} : </label>
                <div class="col-md-8">
                    <div class="col-xs-10 row">
                        <input class="form-control col-md-12 m-b10" type="text" id="merchant_id" name="url" {if isset($url_google_link)}value='{$url_google_link}'{/if} readonly="readonly" />
                        <p class="poor-gray text-right">{ci_language line="This URL use Google Feed"}.</p>
                    </div>
                    <div class="col-xs-2">
                        <button type="button" class="btn btn-rule copy_to_clipboard">{ci_language line="Copy"}</button>
                        <span class="copyAnswer"></span>
                    </div>

                </div>
            </div>
        </div>
</div>
                
<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10 b-Top">
            {ci_language line="Download feed file for regular uploads"}
        </p>
    </div>
</div>
        
<div class="row m-t10">
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t10 text-right">{ci_language line="Feed type - Products"} : </label>
            <div class="col-md-8"> 
                <div class="col-xs-12 row">
                    <a href="{if isset($url_google_link_download)}{$url_google_link_download}{/if}" class="btn btn-rule  ">Download feed file</a> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <div class="col-sm-9">
            <label class="text-uc dark-gray montserrat col-md-4 p-t10 text-right">{ci_language line="Feed type - Online Products Inventory Update"} : </label>
            <div class="col-md-8"> 
                <div class="col-xs-12 row">
                    <a href="{if isset($url_google_link_download)}{$url_google_link_download}{/if}" class="btn btn-rule  ">{ci_language line="Download feed file"}</a> 
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="id_method" value="set_parameter" />
<input type="hidden" id="method" value="parameter" /> 
<input type="hidden" id="method" value="parameter" /> 