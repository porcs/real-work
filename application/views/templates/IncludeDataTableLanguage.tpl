<input type="hidden" id="emptyTable" value="{ci_language line="No data available"}" />
<input type="hidden" id="info" value="{ci_language line="Showing _START_ to _END_ of _TOTAL_ entries"}" />
<input type="hidden" id="infoEmpty" value="{ci_language line="Showing 0 to 0 of 0 entries"}" />
<input type="hidden" id="infoFiltered" value="{ci_language line="(filtered from _MAX_ total entries)"}" />
<input type="hidden" id="infoPostFix" value />
<input type="hidden" id="thousands" value="," />
<input type="hidden" id="lengthMenu" value="{ci_language line="Per page: _MENU_"}" />
<input type="hidden" id="loadingRecords" value="{ci_language line="Loading"}..." />
<input type="hidden" id="processing" value="{ci_language line="Processing..."}" />
<input type="hidden" id="search" value="{ci_language line="Search:"}" />
<input type="hidden" id="zeroRecords" value="{ci_language line="No matching records found"}" />
<input type="hidden" id="paginate-first" value="{ci_language line="First"}" />
<input type="hidden" id="paginate-last" value="{ci_language line="Last"}" />
<input type="hidden" id="paginate-next" value="{ci_language line="Next"}" />
<input type="hidden" id="paginate-previous" value="{ci_language line="Previous"}" />
<input type="hidden" id="aria-sortAscending" value=": {ci_language line="activate to sort column ascending"}" />
<input type="hidden" id="aria-sortDescending" value=": {ci_language line="activate to sort column descending"}" />