{include file="header.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
	<div class="notFound">
		<div class="headSettings clearfix b-Bottom p-b10">
			<h4 class="headSettings_head"><span>{ci_language line="Oops..."}</span> {ci_language line="the page you are looking for cannot be found"}</h4>	
		</div>	
		<div class="notFound_reason">
			<ul class="main_reason">
				<li>{ci_language line="Errors can be caused due to various reasons, such as:"}</li>
			</ul>
			<div class="notFound_reason-point">
				<ul class="pull-sm-left m-r30">
					<li>{ci_language line="Page requested does not exist."}</li>
					<li>{ci_language line="Server is down."}</li>
					<li>{ci_language line="Internet connection is down."}</li>
				</ul>
				<ul>
					<li>{ci_language line="Broken links"}</li>
					<li>{ci_language line="Incorrect URL"}</li>
					<li>{ci_language line="Page has been moved to a different address"}</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="b-Top m-t20 p-t20">
				<a href="{$base_url}dashboard" class="link p-size"><i class="fa fa-long-arrow-left m-r10"></i>{ci_language line="Back"}</a>
			</div>
		</div>
	</div>
</div>

{include file="footer.tpl"}