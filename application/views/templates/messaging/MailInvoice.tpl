{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
{include file="components/popup_step_header.tpl"}

<!--messaging.css-->
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/messaging/messaging.css" />

<form id="messaging-mail-invoice" class="custom-form" method="post" action="{$base_url}messaging/set_messaging/{if isset($action)}{$action}{/if}/{if isset($marketplace_name)}{$marketplace_name}{/if}/{if isset($id_country)}{$id_country}{/if}" enctype="multipart/form-data" autocomplete="off" >
    <div class="row {if isset($popup)}popup_action{/if}">

        <div class="col-xs-12">
            <div class="clearfix p-t20">
                <p class="head_text p-b10 m-b10">
                    {ci_language line="Send invoice by email"}
                </p>
                <div class="form-group col-md-12">
                    <label class="text-uc dark-gray montserrat col-md-2 text-right p-t5">{ci_language line="Active"}</label>
                    <div class="col-md-8 clearfix">
                        <div class="cb-switcher pull-left">
                            <input name="mail_invoice_active" type="checkbox" value="1" data-state-on="ON" data-state-off="OFF" {if isset($mail_invoice.mail_invoice_active) && $mail_invoice.mail_invoice_active}checked="checked"{/if}>
                            <span class="cb-state">{ci_language line="ON"}</span>
                        </div>
                        <div class="row">
                            <p class="poor-gray m-t5 m-b0 col-md-12">
                                {ci_language line="Activate invoice per email automation."}
                            </p>
                            <p class="poor-gray m-t5 col-md-12">
                                {ci_language line="Feed.biz will send the email to the customers with the invoice as an attached document (PDF)."}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="configure-content {if !isset($mail_invoice.mail_invoice_active) || empty($mail_invoice.mail_invoice_active)}hidden{/if}">
                    <div class="form-group col-md-12">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Orders Statuses"}</label>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <select class="search-select" name="mail_invoice_order_state">
                                        <option value="">{ci_language line="Please Choose a Orders Status"}</option>
                                        {foreach $order_status as $os}
                                            <option value="{$os}" {if isset($mail_invoice.mail_invoice_order_state)}{if $mail_invoice.mail_invoice_order_state == $os}selected="selected"{/if}{/if}>{$os}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <p class="poor-gray m-t5 m-b0">{ci_language line="Choose the order status which will trigger the invoice sending."}</p>
                            <!-- <p class="poor-gray m-t5 m-b0">{ci_language line="Only the eligible states are shown (email set to off, invoice to on)."}</p> -->
                        </div>
                    </div>
                    <div class="form-group col-md-12 m-t10">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Mail Template"}</label>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group m-b0">
                                        <select class="search-select" name="mail_invoice_template">
                                            <option value="">{ci_language line="Please Choose a Mail Template"}</option>
                                            {foreach $template_list as $tl}
                                                <option value="{$tl}" {if isset($mail_invoice.mail_invoice_template) && $mail_invoice.mail_invoice_template == $tl}selected="selected"{/if} {if isset($customize_template) && in_array($tl, $customize_template)} class="customize_template"{/if}>{$tl}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                    <p class="poor-gray m-t5">
                                        {ci_language line="File template which will be used to announce the invoice."}
                                    </p>
                                </div>
                                <div class="col-md-4 m-b10">
                                    <a id="preview_template" class="link" href="#">
                                        <i class="fa fa-search-plus p-t10"></i> 
                                        <span class="p-t10">{ci_language line="Preview"}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-12 profileBlock m-t0 p-t5">
                                <p class="poor-gray m-t5 m-b0">
                                    {ci_language line="You can add or remove files by using Manage Mail Template."}
                                </p>
                                <div class="form-group m-b0">
                                    <a class="btn btn-small btn-default mail-template m-t5" data-name="mail-template" data-toggle="modal" data-target="#uploadFile">
                                        <i class="fa fa-upload"></i> {ci_language line="Manage Mail Template"}
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group col-md-12 m-t10">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Additionnal File"}</label>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group m-b0">
                                        <select class="search-select additionnal" name="mail_invoice_additionnal">
                                            <option value="">{ci_language line="Please Choose a Additoinal File"}</option>
                                            {if isset($additionnal_file)}
                                                {foreach $additionnal_file as $af}
                                                    <option value="{$af}" {if isset($mail_invoice.mail_invoice_additionnal) && $mail_invoice.mail_invoice_additionnal == $af}selected="selected"{/if}>{$af}</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <p class="poor-gray m-t5">
                                {ci_language line="Additionnal PDF file to send as attachment, which could be for example: Terms & Conditions."}
                            </p>

                            <div class="col-md-12 profileBlock m-t0 p-t5">
                                <p class="poor-gray m-t5 m-b0">
                                    {ci_language line="You can add or remove files by useing Manage Additionnal File."}
                                </p>
                                <div class="form-group m-b0">
                                    <a class="btn btn-small btn-default additionnal-file m-t5" data-name="additionnal-file" data-toggle="modal" data-target="#uploadFile">
                                        <i class="fa fa-upload"></i> {ci_language line="Manage Additionnal File"}
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div id="smtp_parameters">
                        <div class="form-group row">  
                            <div class="m-t10 col-md-12">  
                                <p class="head_text p-t10">
                                    {ci_language line="SMTP parameters"}
                                </p>
                            </div>
                        </div>  
                        <div class="form-group col-md-12">            
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10"></label>
                            <div class="col-md-8">
                                <div class="validate blue m-b0">
                                    <div class="validateRow">
                                        <div class="validateCell">
                                            <i class="fa fa-info-circle"></i>                
                                        </div>
                                        <div class="validateCell">
                                            <p>{ci_language line="The invoice email will send  by the SMTP you provide."}</p>                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                        <div class="form-group col-md-12">            
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10"></label>
                            <div class="col-md-8" id="duplicate_smtp_contain">
                                    <div class="pull-left p-t5"> 
                                        <label class="cb-checkbox" id="duplicate_smtp" >
                                            </i><input type="checkbox" class="" value="1" />
                                            {ci_language line="Duplicate SMTP"}
                                        </label>
                                    </div>
                                    <div class="pull-left col-sm-7" style="display:none"> 
                                        <select class="search-select" id="smtp_select">
                                            <option value=""> - - </option>
                                            {if isset($smtp_duplicate)}
                                                {foreach $smtp_duplicate as $smtp_duplicate_mk => $smtp_duplicate_sites}
                                                    {foreach $smtp_duplicate_sites as $smtp_duplicate_site}
                                                        <option value="{$smtp_duplicate_mk}/{$smtp_duplicate_site['id']}">
                                                            {ucfirst($smtp_duplicate_mk)} - {ucfirst($smtp_duplicate_site['name'])}
                                                        </option>
                                                     {/foreach}
                                                {/foreach}
                                           {/if}
                                        </select>
                                    </div>
                            </div>
                        </div> 
                        <div class="form-group col-md-12 m-t5">            
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="SMTP server"}</label>
                            <div class="col-md-8">
                                <input type="text" name="mail_invoice_smtp_server" class="form-control"{if isset($mail_invoice.mail_invoice_smtp_server)} value="{$mail_invoice.mail_invoice_smtp_server}"{/if} autocomplete="off" id="smtp_server"/>
                                <p class="poor-gray m-t5 m-b0">{ci_language line="IP address or server name (e.g. smtp.mydomain.com)."}</p>
                            </div>
                        </div> 
                        <div class="form-group col-md-12">            
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Login"}</label>
                            <div class="col-md-8">
                                <input type="text" name="mail_invoice_mail_from" class="form-control"{if isset($mail_invoice.mail_invoice_mail_from)} value="{$mail_invoice.mail_invoice_mail_from}"{/if} autocomplete="off" id="mail_from"/> 
                                <p class="poor-gray m-t5 m-b0">{ci_language line="Login email (e.g. contact@common-services.com)."}</p>
                            </div>
                        </div> 
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Password"}</label>
                            <div class="col-md-8">
                                <input type="password" name="mail_invoice_password" class="form-control"{if isset($mail_invoice.mail_invoice_password)} value="{$mail_invoice.mail_invoice_password}"{/if} autocomplete="off" id="password"/>
                                <p class="poor-gray m-t5 m-b0"></p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Encryption"}</label>
                            <div class="col-md-8">
                                <div style="width:100px;">
                                     <div class="form-group m-b0">
                                        <select class="search-select additionnal" name="mail_invoice_encryption" id="encryption">
                                            <option value="">{ci_language line="None"}</option>
                                            <option value="tls"{if isset($mail_invoice.mail_invoice_encryption) && $mail_invoice.mail_invoice_encryption == "tls"} selected="selected"{/if}>TLS</option>
                                            <option value="ssl"{if isset($mail_invoice.mail_invoice_encryption) && $mail_invoice.mail_invoice_encryption == "ssl"} selected="selected"{/if}>SSL</option>
                                        </select>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5 m-b0">{ci_language line="GMail uses SSL, for other environments please ask your hosting provider"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Port"}</label>
                            <div class="col-md-8">
                                <div style="width:100px;">
                                    <input type="text" name="mail_invoice_port" class="form-control"{if isset($mail_invoice.mail_invoice_port)} value="{$mail_invoice.mail_invoice_port}"{/if} autocomplete="off" id="port"/>
                                </div>
                                <p class="poor-gray m-t5 m-b0">{ci_language line="Port number to use. (gmail use 465)"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12 m-b20 p-b20">
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Maximum email"}</label>
                            <div class="col-md-8">
                                <div style="width:100px;display:inline-block;">
                                    <input type="text" name="mail_maximum" class="form-control" value="{if isset($mail_maximum)}{$mail_maximum}{/if}" autocomplete="off" id="port"/>
                                </div>
                                <span class="m-t5 m-b0 light-gray">
                                    {ci_language line="Per hour."}
                                </span>
                                <p class="poor-gray m-t5 m-b0">
                                    {ci_language line="Your mail server limits residential to send messages per hour."} 
                                    <em>{ci_language line="Effect to All Marketplace site."}</em>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <div class="b-Top p-t20">
                <div class="inline pull-left">
                    {if !isset($not_previous)}
                        <button class="pull-left link p-size m-tr10" id="back" type="button">
                            {ci_language line="Previous"}
                        </button>
                    {/if}
                </div>
                <div class="inline pull-right">  
                    {if isset($not_continue) && $not_continue == true && !isset($popup)}
                        <button class="btn btn-save" id="save_data" type="submit" >
                            {ci_language line="Save"}
                        </button>
                    {else}
                        {if !isset($popup)}
                            <button class="pull-left link p-size m-tr10" id="save_data" type="submit" >
                                {ci_language line="Save"}
                            </button>
                        {/if}                    
                        <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                    {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                </button>                        
            {/if}     
        </div>

    </div>
</form>

<div class="modal fade" id="uploadFile" aria-hidden="true" style="display: none;" ng-app="app">
    <div class="modal-dialog" id="fileuploadctrl" ng-controller="FileUploadCtrl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>					
            </div>
            <div class="modal-body">
                {literal}
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                            {{name_title}}
                        </h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="fa fa-info-circle"></i>
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left">
                                        <span class="bold">{/literal}{ci_language line="Manage Mail Template"}{literal} </span>  
                                        {/literal}{ci_language line="is use to add or delete the template for email."}{literal} 
                                        <!--The template will use to all email action (Mail invoice, Mail review and Customer thread), please be careful when delete it.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-t10">

                    <form class="upload-template" method="POST" enctype="multipart/form-data">	
                        
                        <p class="head_text p-tb10 m-b10 m-t15">
                             {/literal}{ci_language line="Upload new file"}{literal} 
                        </p>

                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-file-o"></i> {/literal}{ci_language line="Browse…"}{literal} <input type="file" class="form-control" name="file-upload" id="fileToUpload" ng-model-instant multiple onchange="angular.element(this).scope().setFiles(this)" />
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" readonly="" value="{{namefile_all}}">
                                </div>

                                <div class="progressbar col-md-12 p-t35" ng-show="progress > 0">
                                    <div class="relative">
                                        <div class="progressCondition">
                                            <div class="progressLine" data-value="{{progress}}"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <button type="button" class="btn btn-save m-b0 update" id="upload_product_options" ng-disabled="files.length == 0 || !files" ng-click="uploadFile()" value="Upload"><i class="fa fa-upload"></i> {/literal}{ci_language line="Upload File"}{literal}</button>                               
                                <div class="status update" style="" ng-show="progress > 0 && progress < 100">                                                   
                                    <p class="p-t10 m-l5 p-b0 m-b0">
                                        <i class="fa fa-spinner fa-spin"></i> 
                                        <span>{/literal}{ci_language line="Starting upload"}{literal}</span>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div ng-show="files.length">
                            <div class="m-l30" ng-repeat="file in files.slice(0) | orderBy : orderPO : reverse track by $index">
                                <a class="link true-pink" href="#" ng-click="removeFileItem(file)"><i class="fa fa-minus-circle"></i></a>
                                <span style="font-size: 12px;">{{file.webkitRelativePath || file.name}}</span>
                                (<span ng-switch="file.size > 1024*1024" style="font-size: 12px;"><span ng-switch-when="true">{{file.size / 1024 / 1024 | number:2}} MB</span><span ng-switch-default>{{file.size / 1024 | number:2}} kB</span></span>)
                            </div>
                        </div>

                        <div ng-show="files_server.length">
                            <p class="head_text p-tb10 m-b10 m-t15">
                                {{currentUploadFile == "mail-template" ? "{/literal}{ci_language line="Uploaded Mail template"}{literal}" : "{/literal}{ci_language line="Uploaded Additional file"}{literal}" }}
                            </p>
                            <div ng-repeat="file_s in files_server | orderBy : orderPO : reverse track by $index">
                                <a class="link true-pink" href="#" ng-click="deleteFileOnServer(file_s)"><i class="fa fa-minus-circle"></i></a>
                                <span style="font-size: 12px;">{{file_s}}</span>
                            </div>
                        </div>                                

                    </form>
                </div>
                {/literal}
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
<input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
<input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
<input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />
<input type="hidden" id="action" name="action" value="{if isset($action)}{$action}{/if}" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/messaging/messaging.js"></script>

{include file="footer.tpl"}