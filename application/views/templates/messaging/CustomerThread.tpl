{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
{include file="components/popup_step_header.tpl"}

<!--messaging.css-->
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/messaging/messaging.css" />

<form id="messaging-mail-invoice" class="custom-form" method="post" action="{$base_url}messaging/set_messaging/{if isset($action)}{$action}{/if}/{if isset($marketplace_name)}{$marketplace_name}{/if}/{if isset($id_country)}{$id_country}{/if}" enctype="multipart/form-data" autocomplete="off" >
    <div class="row {if isset($popup)}popup_action{/if}">
        
        <div class="col-xs-12">
            <div class="clearfix p-t20">
                <p class="head_text p-tb10 m-b10">
                    {ci_language line="Customer Service"}
                </p>
                <div class="form-group col-md-12">
                    <label class="text-uc dark-gray montserrat col-md-2 text-right p-t5">{ci_language line="Active"}</label>
                    <div class="col-md-7 clearfix">
                        <div class="cb-switcher pull-left">
                            <input name="customer_thread_active" type="checkbox" value="1" data-state-off="OFF" {if isset($customer_thread.customer_thread_active) && $customer_thread.customer_thread_active}checked="checked"{/if} />
                            <span class="cb-state">{ci_language line="ON"}</span>
                        </div>
                        <div class="row">
                            <p class="poor-gray m-t5 m-b0 col-md-12">
                                {ci_language line="This feature allows to receive customers messages from Email and answer directly from Customer Service."}
                            </p>
                        </div>                       
                    </div>
                </div>
                <div class="configure-content{if !isset($customer_thread.customer_thread_active) || empty($customer_thread.customer_thread_active)} hidden{/if}">
                    <div class="form-group col-md-12">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t5"></label>
                        <div class="col-md-7 clearfix">                        
                            <div class="validate blue m-b0">
                                <div class="validateRow">
                                    <div class="validateCell">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="validateCell">
                                        <p class="">
                                            <span class="bold">{ci_language line="Reply Email Template"}</span>  
                                            {ci_language line="are on"} <a class="link p-size" href="{$base_url}my_shop/messaging/reply_template/{if isset($id_lang)}{$id_lang}{/if}">{ci_language line="My shop > Messaging > Reply Template"}</a> {if isset($id_lang)}({$id_lang}){/if} {ci_language line="tab."}       
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    {if !isset($mk_general) || !$mk_general}             
                    <div class="form-group col-md-12 m-t10">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10" data-toggle="modal" data-target="#popUp" data-text="email_provider_text"><span class="b-blue">{ci_language line="Email Provider"}</span></label>
                        <div class="col-md-7">
                                <select class="search-select" name="customer_thread_mail_provider">
                                    <option value="">{ci_language line="Please Choose a Email Provider"}</option>
                                    {if isset($email_providers)}
                                        {foreach $email_providers as $kep => $ep}
                                            <option value="{$kep}" {if isset($customer_thread.customer_thread_mail_provider) && $customer_thread.customer_thread_mail_provider == $kep}selected="selected"{/if}>{$ep}</option>
                                        {/foreach}
                                    {/if}
                                </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12 m-t10">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10" data-toggle="modal" data-target="#popUp" data-text="login_text"><span class="b-blue">{ci_language line="Login"}</span></label>
                        <div class="col-md-7">
                            <input type="text" name="customer_thread_login" class="form-control"{if isset($customer_thread.customer_thread_login)} value="{$customer_thread.customer_thread_login}"{/if} />
                        </div>
                    </div>
                    <div class="form-group col-md-12 m-t10">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Password"}</label>
                        <div class="col-md-7">
                            <input type="password" name="customer_thread_password" class="form-control"{if isset($customer_thread.customer_thread_password)} value="{$customer_thread.customer_thread_password}"{/if} />
                        </div>
                    </div>
                    <div class="form-group col-md-12 m-t10 m-b20 p-b30">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10" data-toggle="modal" data-target="#popUp" data-text="labels_text"><span class="b-blue">{ci_language line="Labels"}</span></label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" style="width:300px;overflow:hidden" disabled="disabled" value="{if isset($labels)}{foreach $labels as $dl}{$dl} {/foreach}{/if}" />
                            <p class="poor-gray m-t5 m-b0">{ci_language line="Do not forget to setup the"} <span id="filters" data-toggle="modal" data-target="#popUp" data-text="filters_text">{ci_language line="Filters"}</span> {ci_language line="and to configure the email on"} <span id="seller_central" data-toggle="modal" data-target="#popUp" data-text="seller_central_text">{ci_language line="Seller Central"}</span></p>
                        </div>
                    </div>
                    {/if} 
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <div class="b-Top p-t20">
                <div class="inline pull-left">
                    {if !isset($not_previous)}
                        <button class="pull-left link p-size m-tr10" id="back" type="button">
                            {ci_language line="Previous"}
                        </button>
                    {/if}
                </div>
                <div class="inline pull-right">  
                    {if isset($not_continue) && $not_continue == true && !isset($popup)}
                        <button class="btn btn-save" id="save_data" type="submit" >
                            {ci_language line="Save"}
                        </button>
                    {else}
                        {if !isset($popup)}
                            <button class="pull-left link p-size m-tr10" id="save_data" type="submit" >
                                {ci_language line="Save"}
                            </button>
                        {/if}                    
                        <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                            {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                        </button>                        
                    {/if}               
                </div>
            </div>		
        </div>
         
    </div>
</form>


<div class="modal fade" id="popUp" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" id="fileuploadctrl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                  
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
                
<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
<input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
<input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
<input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />
<input type="hidden" id="action" name="action" value="{if isset($action)}{$action}{/if}" />

<input type="hidden" id="filters_text" value='<p class="head_text p-tb10 m-b10">{ci_language line="Filters"}</p> {ci_language line="You will have to create a filter to apply automatically the suitable label:<br><br>For instance, all messages coming from"} {if isset($marketplace_name)}{$marketplace_name}{/if}{if isset($ext)}{$ext}{/if}, {ci_language line="apply the label"} {$marketplace_name|capitalize}{if isset($ext)}-{$ext|replace:'.co.':''|capitalize}{/if} {ci_language line="as shown on the picture below"} ;<br><br> <img class="img-responsive" src="{$base_url}assets/images/manual/messaging/filter.jpg" /><br><br>{ci_language line="Please note the presence of the asterisk (*@"}{if isset($marketplace_name)}{$marketplace_name}{/if}{if isset($ext)}{$ext}{/if})<br><br>{ci_language line="You will repeat the operation for each marketplace (.fr, .de, etc.)"}' />
<input type="hidden" id="seller_central_text" value='<p class="head_text p-tb10 m-b10">{ci_language line="Seller Central"}</p> {ci_language line='In Seller Central, you will redirect the &#8220;Customer Service Email&#8221; to your Gmail mailbox'} ;<br><br><img class="img-responsive" src="{$base_url}assets/images/manual/messaging/email_{$marketplace_name}.jpg" /><br><br>{ci_language line="Note: don&#39;t use the &#8220;notification&#8221; menu, it won&#39;t work. Only the configuration, as shown on the picture above works !"}' />
<input type="hidden" id="email_provider_text" value='<p class="head_text p-tb10 m-b10">{ci_language line="Email Provider"}</p> {ci_language line="Email provider. For a better compatibility, <br />we recommend Gmail. If you have any other email provider, <br />please contact our support. &#8220;IMAP&#8221; <br />support has to be configured in email settings."} <br /><br /><img class="img-responsive" src="{$base_url}assets/images/manual/messaging/imap.jpg" />' />
<input type="hidden" id="login_text" value='<p class="head_text p-tb10 m-b10">{ci_language line="Login"}</p> {ci_language line="Enter the email account login, for instance ;"} <br /><br /><img class="img-responsive" src="{$base_url}assets/images/manual/messaging/email_login.jpg" />' />
<input type="hidden" id="labels_text" value='<p class="head_text p-tb10 m-b10">{ci_language line="Labels"}</p> {ci_language line="Labels you will have to create on Gmail (Imap Labels)"} <br /><br /><img class="img-responsive" src="{$base_url}assets/images/manual/messaging/labels.jpg" />' />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.tooltipster.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/messaging/messaging.js"></script>

{include file="footer.tpl"}