{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<!--messaging.css-->
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/messaging/messaging.css" />

<div class="main p-rl40 p-xs-rl20">
   
    <h1 class="lightRoboto text-uc head-size light-gray">{$function}</h1>      
    {include file="breadcrumbs.tpl"} 
    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-t10 b-Top clearfix m-0">     
                {if isset($reply_template_lang)}<img src="{$base_url}assets/images/lang/{str_replace(array(".",".."), "_", ltrim($reply_template_lang, "."))}.png" class="flag m-r10" alt="">{/if}          
                {if isset($reply_template_language)}
                    <span class="p-t10">{$reply_template_language}</span>
                {/if}
            </p>
        </div>
    </div>

    <form id="messaging-mail-invoice" class="custom-form" method="post" action="{$base_url}messaging/set_messaging/{if isset($action)}{$action}{/if}/{if isset($marketplace_name)}{$marketplace_name}{/if}/{if isset($id_lang)}{$id_lang}{/if}" enctype="multipart/form-data" autocomplete="off" >
        <div class="row {if isset($popup)}popup_action{/if}">
            
            <div class="col-xs-12">
                <div class="clearfix p-t20">
                    <p class="head_text p-tb10 m-b10">
                        {ci_language line="Customer Service"}
                    </p>
                    <div class="form-group col-md-12">
                        <label class="text-uc dark-gray montserrat col-md-2 text-right p-t5">{ci_language line="Active"}</label>
                        <div class="col-md-7 clearfix">
                            <div class="cb-switcher pull-left">
                                <input name="reply_template_active" type="checkbox" value="1" data-state-off="OFF" {if isset($reply_template.reply_template_active) && $reply_template.reply_template_active}checked="checked"{/if} />
                                <span class="cb-state">{ci_language line="ON"}</span>
                            </div>
                        </div>
                    </div>
                    <div class="configure-content{if !isset($reply_template.reply_template_active) || empty($reply_template.reply_template_active)} hidden{/if}">
                        <div class="form-group col-md-12 m-t10">
                            <label class="text-uc dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Mail Template"}</label>
                            <div class="col-md-7">
                                <div class="form-group m-b0">
                                    <select class="search-select" name="reply_template_template">
                                        <option value="">{ci_language line="Please Choose a Mail Template"}</option>
                                        {foreach $template_list as $tl}
                                            <option value="{$tl}" {if isset($reply_template.reply_template_template) && $reply_template.reply_template_template == $tl}selected="selected"{/if} {if isset($customize_template) && in_array($tl, $customize_template)} class="customize_template"{/if}>{$tl}</option>
                                        {/foreach}
                                    </select>
                                </div>
                               
                                <p class="poor-gray m-t5 m-b0">{ci_language line="Please choose here the email template for Customer Service."}</p>
                                <p class="poor-gray m-t5 m-b0">{ci_language line="Usually, the template is \"reply_msg\", please don't change this value if you are unsure."}</p>

                                <div class="col-md-12 profileBlock m-t5 p-t5 m-b20">
                                    <p class="poor-gray m-t5 m-b0">
                                        {ci_language line="You can add or remove files by using Manage Mail Template."}
                                    </p>
                                    <div class="form-group m-b0">
                                        <a class="btn btn-small btn-default mail-template m-t5" data-name="mail-template" data-toggle="modal" data-target="#uploadFile">
                                            <i class="fa fa-upload"></i> {ci_language line="Manage Mail Template"}
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <a id="preview_template" class="link" href="#"><i class="fa fa-search-plus p-t10"></i> <span class="p-t10">{ci_language line="Preview"}</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-xs-12">
                <div class="b-Top p-t20">
                    <div class="inline pull-left">
                        {if !isset($not_previous)}
                            <button class="pull-left link p-size m-tr10" id="back" type="button">
                                {ci_language line="Previous"}
                            </button>
                        {/if}
                    </div>
                    <div class="inline pull-right">  
                        {if isset($not_continue) && $not_continue == true && !isset($popup)}
                            <button class="btn btn-save" id="save_data" type="submit" >
                                {ci_language line="Save"}
                            </button>
                        {else}
                            {if !isset($popup)}
                                <button class="pull-left link p-size m-tr10" id="save_data" type="submit" >
                                    {ci_language line="Save"}
                                </button>
                            {/if}                    
                            <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                                {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                            </button>                        
                        {/if}               
                    </div>
                </div>		
            </div>
             
        </div>
        <input type="hidden" id="lang" name="lang" value="{if isset($reply_template_lang)}{$reply_template_lang}{/if}" />
    </form>

    <div class="modal fade" id="uploadFile" aria-hidden="true" style="display: none;" ng-app="app">
        <div class="modal-dialog" id="fileuploadctrl" ng-controller="FileUploadCtrl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                  
                </div>
                <div class="modal-body">
                    {literal}
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                                {{name_title}}
                            </h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="validate blue">
                                <div class="validateRow">
                                    <div class="validateCell">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="validateCell">
                                        <p class="pull-left">
                                            <span class="bold">{/literal}{ci_language line="Manage Mail Template"}{literal} </span>  
                                            {/literal}{ci_language line="is use to add or delete the template for reply customer services email."}{literal}  
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="m-t10">

                        <form class="upload-template" method="POST" enctype="multipart/form-data">  
                            
                            <p class="head_text p-tb10 m-b10 m-t15">
                                 {/literal}{ci_language line="Upload new file"}{literal} 
                            </p>

                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-file-o"></i> {/literal}{ci_language line="Browse…"}{literal} <input type="file" class="form-control" name="file-upload" id="fileToUpload" ng-model-instant multiple onchange="angular.element(this).scope().setFiles(this)" />
                                            </span>
                                        </span>
                                        <input type="text" class="form-control" readonly="" value="{{namefile_all}}">
                                    </div>

                                    <div class="progressbar col-md-12 p-t35" ng-show="progress > 0">
                                        <div class="relative">
                                            <div class="progressCondition">
                                                <div class="progressLine" data-value="{{progress}}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <button type="button" class="btn btn-save m-b0 update" id="upload_product_options" ng-disabled="files.length == 0 || !files" ng-click="uploadFile()" value="Upload"><i class="fa fa-upload"></i> {/literal}{ci_language line="Upload File"}{literal}</button>                               
                                    <div class="status update" style="" ng-show="progress > 0 && progress < 100">                                                   
                                        <p class="p-t10 m-l5 p-b0 m-b0">
                                            <i class="fa fa-spinner fa-spin"></i> 
                                            <span>{/literal}{ci_language line="Starting upload"}{literal}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div ng-show="files.length">
                                <div class="m-l30" ng-repeat="file in files.slice(0) | orderBy : orderPO : reverse track by $index">
                                    <a class="link true-pink" href="#" ng-click="removeFileItem(file)"><i class="fa fa-minus-circle"></i></a>
                                    <span style="font-size: 12px;">{{file.webkitRelativePath || file.name}}</span>
                                    (<span ng-switch="file.size > 1024*1024" style="font-size: 12px;"><span ng-switch-when="true">{{file.size / 1024 / 1024 | number:2}} MB</span><span ng-switch-default>{{file.size / 1024 | number:2}} kB</span></span>)
                                </div>
                            </div>

                            <div ng-show="files_server.length">
                                <p class="head_text p-tb10 m-b10 m-t15">
                                    {{currentUploadFile == "mail-template" ? "{/literal}{ci_language line="Uploaded Mail template"}{literal}" : "{/literal}{ci_language line="Uploaded Additional file"}{literal}" }}
                                </p>
                                <div ng-repeat="file_s in files_server | orderBy : orderPO : reverse track by $index">
                                    <a class="link true-pink" href="#" ng-click="deleteFileOnServer(file_s)"><i class="fa fa-minus-circle"></i></a>
                                    <span style="font-size: 12px;">{{file_s}}</span>
                                </div>
                            </div>                                

                        </form>
                    </div>
                    {/literal}
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
                    
    <input type="hidden" id="id_country" name="id_country" value="{if isset($reply_template_lang)}{$reply_template_lang}{/if}" />
    <input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
    <input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
    <input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
    <input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />
    <input type="hidden" id="action" name="action" value="{if isset($action)}{$action}{/if}" />

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.5/angular.min.js"></script>
    <script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
    <script type="text/javascript" src="{$cdn_url}assets/js/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/messaging/messaging.js"></script>

{include file="footer.tpl"}