{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative">
            <h1>
                <a href="{$base_url}index.php/statistics/products">{ci_language line="products"}</a>
                <small><i class="icon-double-angle-right"></i> {if isset($product.name)}{$product.name}{/if}</small>
            </h1>
        </div>

        <div class="row-fluid">            
            <div class="span12">								
                {if isset($error) }<div class="alert alert-danger">{$error}</div>{/if}
                {if isset($message) }<div class="alert alert-info">{$message}</div>{/if}
                
                <div class="row-fluid">
                    <div class="tabbable">
                        <ul class="nav nav-tabs" id="product">
                            <li class="active">
                                <a data-toggle="tab" href="#information">
                                    <i class="pink2 icon-info-sign bigger-110"></i>
                                    {ci_language line="information"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#price">
                                    <i class="blue icon-dollar bigger-110"></i>
                                    {ci_language line="price"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#category">
                                    <i class="orange2 icon-book bigger-110"></i>
                                    {ci_language line="category"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#topology">
                                    <i class="pink icon-compass bigger-110"></i>
                                    {ci_language line="topology"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#combination">
                                    <i class="green icon-list bigger-110"></i>
                                    {ci_language line="combination"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#image">
                                    <i class="orange icon-picture bigger-110"></i>
                                    {ci_language line="image"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#feature">
                                    <i class="purple icon-puzzle-piece bigger-110"></i>
                                    {ci_language line="feature"}
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#supplier">
                                    <i class="red icon-building bigger-110"></i>
                                    {ci_language line="supplier"}
                                </a>
                            </li>
                        </ul>{*nav*}
                        
                        <div class="tab-content">

                            <div class="space-30"></div> 

                            <div id="information" class="tab-pane active">
                                
                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <label class="control-label" for="product_name">{ci_language line="name"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_name" id="product_name" value="{if isset($product.name)}{$product.name}{/if}">
                                        </div>
                                    </div>
                                    {*<div class="control-group">
                                        <label class="control-label" for="product_reference">{ci_language line="reference"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_reference" id="product_reference" value="{if isset($product.upc)}{$product.upc}{/if}">
                                        </div>
                                    </div>*}
                                    <div class="control-group">
                                        <label class="control-label" for="product_ean13">{ci_language line="ean13"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_ean13" id="product_ean13" value="{if isset($product.ean13)}{$product.ean13}{/if}">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="product_upc">{ci_language line="upc"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_upc" id="product_upc" value="{if isset($product.upc)}{$product.upc}{/if}">
                                        </div>
                                    </div>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="product_condition">{ci_language line="condition"}</label>
                                        <div class="controls">
                                            <select id="product_condition" id="product_condition">
                                                {foreach from=$conditions key=ckey item=cvalue}
                                                    <option value="{$ckey}" {if $product.condition == $ckey} selected ="selected" {/if}>{$cvalue}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="product_description">{ci_language line="description"}</label>
                                        <div class="controls">
                                            <textarea id="product_description" name="product_description" class="autosize-transition span8">{if isset($product.description)}{$product.description}{/if}</textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="product_tags">{ci_language line="tags"}</label>
                                        <div class="controls">
                                            <input type="text" name="product_tags" id="product_tags" value="{if isset($product.tags)}{$product.tags}{/if}" placeholder="{ci_language line="enter_tags"} .." />
                                        </div>
                                    </div>
                                        
                                    <div class="control-group">
                                        <label class="control-label" for="product_status">{ci_language line="status"}</label>
                                        <div class="controls">
                                            <div class="space-2"></div>
                                            <label class="">
                                                <input name="product_status" type="radio" value="enabled" {if $product.active == 1}checked{/if}>
                                                <span class="lbl"> {ci_language line="enabled"}</span>
                                            </label>
                                            <label class="">
                                                <input name="product_status" type="radio" value="disabled" {if $product.active != 1}checked{/if}>
                                                <span class="lbl"> {ci_language line="disabled"}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                        
                            </div>{*information*}

                            <div id="price" class="tab-pane">
                                
                                <div class="form-horizontal">
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="product_price">{ci_language line="price"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_price" id="product_price" value="{if isset($product.price)}{$product.price}{/if}">
                                            
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="product_tax">{ci_language line="tax"}</label>
                                        <div class="controls">
                                            <select id="product_tax" id="product_tax">
                                                {foreach from=$taxes key=tkey item=tvalue}
                                                    <option value="{$tvalue.id}" {if $product.tax == $tvalue.id} selected ="selected" {/if}>
                                                        {$tvalue.name}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="product_currency">{ci_language line="currency"}</label>
                                        <div class="controls">
                                             <select id="product_currency" id="product_currency">
                                                {foreach from=$currency key=cukey item=cuvalue}
                                                    <option value="{$cukey}" {if $product.currency == $cukey} selected ="selected" {/if}>
                                                    {$cuvalue.name} ({$cuvalue.iso_code})</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    
                                    {if isset($product.sale)}
                                        
                                        <div class="space-30"></div>
                                        <div class="row-fluid">
                                            <h4 class=" offset1 span10 header blue bolder smaller">
                                                <i class="icon-money green"></i>
                                                {ci_language line="specific_prices"}
                                            </h4>
                                        </div>
                                        <div class="space"></div>
                                        
                                        {foreach from=$product.sale key=skey item=svalue}
                                            <div class="control-group">
                                                <label class="control-label" for="id-date-range-picker-1">{ci_language line="available"}</label>
                                                <div class="controls">
                                                    <div class="row-fluid input-prepend">
                                                        <input type="text" name="sale_date_{$skey}" class="datetime" value="{$svalue.date_from} - {$svalue.date_to}">
                                                        <span class="add-on">
                                                            <i class="icon-calendar"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="product_sale_price">{ci_language line="apply_a_discount_of"}</label>
                                                <div class="controls">
                                                    <input type="text" name="sale_price_{$skey}" id="sale_price_{$skey}" value="{$svalue.price}">
                                                     <select name="sale_type_{$skey}" id="sale_type_{$skey}">
                                                            <option value="amount" {if $svalue.type == "Amount"} selected {/if}>
                                                            {ci_language line="Amount"}</option>
                                                            <option value="percentage" {if $svalue.type == "Percentage"} selected {/if}>
                                                            {ci_language line="Percentage"}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row-fluid">
                                                <div class=" offset1 span10"><hr /></div>
                                            </div>
                                        {/foreach}
                                            
                                    {/if}
                                    
                                </div>{*form-horizontal*}
                                        
                            </div>{*price*}
                            
                            <div id="category" class="tab-pane">
                                
                                <div class="offset1 span10">
                                    <div class="tree tree-unselectable">
                                        <div class="tree-folder display-block" >	
                                            <div class="tree-folder-header">					
                                                <i class="icon-folder-close orange"></i>					
                                                <div class="tree-folder-name">{$root.name}</div>				
                                            </div>
                                        </div> 
                                        
                                    </div>
                                    <div id="tree1" class="tree tree-selectable">
                                        <div id="tree1_loading" class="tree-loading"><i class="icon-spinner icon-spin orange bigger-225"></i></div>
                                    </div>
                                    
                                </div>{*offset1 span10*}
                                        
                            </div>{*category*}
                            
                            <div id="topology" class="tab-pane">
                                
                                <div class="form-horizontal">
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="product_width">{ci_language line="width"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_width" id="product_width" value="{if isset($product.width)}{$product.width}{/if}">
                                            <span class="help-inline">{if isset($dimension)}{$dimension}{/if} </span>
                                        </div>
                                    </div>
                                        
                                    <div class="control-group">
                                        <label class="control-label" for="product_height">{ci_language line="height"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_height" id="product_height" value="{if isset($product.height)}{$product.height}{/if}">
                                            <span class="help-inline">{if isset($dimension)}{$dimension}{/if} </span>
                                        </div>
                                    </div>
                                        
                                    <div class="control-group">
                                        <label class="control-label" for="product_depth">{ci_language line="depth"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_depth" id="product_depth" value="{if isset($product.depth)}{$product.depth}{/if}">
                                            <span class="help-inline">{if isset($dimension)}{$dimension}{/if} </span>
                                        </div>
                                    </div>
                                        
                                    <div class="control-group">
                                        <label class="control-label" for="product_weight">{ci_language line="weight"}</label>
                                        <div class="controls">
                                            <input type="text" id="product_weight" id="product_weight" value="{if isset($product.weight)}{$product.weight}{/if}">
                                            <span class="help-inline">{if isset($weight)}{$weight}{/if} </span>
                                        </div>
                                    </div>
                                    
                                    <!--Carrier-->
                                    <hr>
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="product_weight"> 
                                            <i class="icon-truck pink2"></i> 
                                            {ci_language line="carriers"} : 
                                        </label>
                                        <div class="controls">
                                            <select multiple="" class="chzn-select" id="product_carrier" name="product_carrier" data-placeholder="{ci_language line="Choose a Carrier"} ...">
                                                <option value=""></option>
                                                {foreach from=$carrier key=cakey item=cavalue}
                                                    <option value="{$cakey}">{$cavalue.name}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                </div>{*form-horizontal*}
                                        
                            </div>{*Topology*}
                            

                        </div>{*tab-content*}
                        
                    </div>{*tabbable*}
                    
                </div>{*row-fluid*}
                
             </div>{*span12*}
             
        </div>{*row-fluid*}

    </div>{*page-content*}
    
</div>{*main-content*}

<!--page specific plugin scripts-->
{include file="jqueryDataTablesScript.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/fuelux/fuelux.tree.min.js"></script>
<script src="{$cdn_url}assets/js/bootstrap-tag.min.js"></script>
<script src="{$cdn_url}assets/js/jquery.autosize-min.js"></script>
<script src="{$cdn_url}assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="{$cdn_url}assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="{$cdn_url}assets/js/date-time/moment.min.js"></script>
<script src="{$cdn_url}assets/js/date-time/daterangepicker.min.js"></script>
<script src="{$cdn_url}assets/js/chosen.jquery.min.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/statistics.js" type="text/javascript"></script>

{include file="footer.tpl"}