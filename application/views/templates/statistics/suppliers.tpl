{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative"><h1>{ci_language line="suppliers"}</h1></div>

        <div class="row-fluid">
            
            <div class="span12">
								
                {if isset($error) }<div class="alert alert-danger">{$error}</div>{/if}
                {if isset($message) }<div class="alert alert-info">{$message}</div>{/if}
                
                  
                    
             </div>{*span12*}
             
        </div>{*row-fluid*}

    </div>{*page-content*}
    
</div>{*main-content*}

<!--inline scripts related to this page-->
<script type="text/javascript"></script>

{include file="footer.tpl"}