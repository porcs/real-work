{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/dashboard.css" />
<script src="{$cdn_url}assets/js/angular/angular.min.js"></script>
<script src="{$cdn_url}assets/js/angular/Chart.js"></script>
<script src="{$cdn_url}assets/js/angular/d3.v3.min.js"></script>
<script src="{$cdn_url}assets/js/angular/angular-chart.js"></script>

<div id="content" class="main" style="margin-left: 300px;">
    <div class="p-rl40 p-xs-rl20">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Dashboard"}</h1>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    </div>
    <div class="{*innerAll spacing-x2*} content">
        <div class="p-rl40 p-xs-rl20">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <h4 class="dark-gray bold">{ci_language line="Activities"}</p></h4>     
                    {if !isset($activity) || sizeof($activity)==0}
                        <ul class="list-circle-blue"><li>{ci_language line="No data."}</li></ul>
                    {else}
                        <ul class="list-circle-blue">  
                                {foreach $activity as $act name=activitylimit}
                                    {if $smarty.foreach.activitylimit.index < 4 && $smarty.foreach.activitylimit.index != 0}
                                    {if trim($act.type)!=''}<li>{$act.type}{if isset($act.url)}{/if}</li> {/if}
                                    {/if}
                                {/foreach} 
                        </ul>
                        <a href="{$base_url}my_feeds/import_history" class="more m-b30">{ci_language line="See more"}</a>
                    {/if}
                    
                               
                </div>

                <div class="col-lg-3 col-sm-6">
                    <h4 class="dark-gray bold">{ci_language line="Best categories"}</h4>
                    <ul class="list-circle-unstyled m-b30">
                        {if !isset($best_cat) || sizeof($best_cat)==0}
                            <li>{ci_language line="No data."}<li>
                            {else}
                                {foreach $best_cat as $cat name=catlimit}
                                    {if $smarty.foreach.catlimit.index < 3}
                                    <li>
                                        <div class="minichart{*$smarty.foreach.catlimit.index*}"></div>
                                        <p>{$cat.name} ({$cat.shop_name})</p>
                                        <p><span>{$cat.all_num|number_format:0:".":","}</span></p>
                                    </li>
                                {/if}
                            {/foreach}
                        {/if}
                    </ul>
                </div>

                <div class="col-lg-3 col-sm-6">
                    <h4 class="dark-gray bold">{ci_language line="Configuration checklist"}</h4>     
                    {if  (isset($user_need_update_module) && $user_need_update_module !== true) }
                    <ul class="list-circle-orange m-b20  ">
                        <li> 
                                {$user_need_update_module}    
                                <a href="{$base_url}my_shop/connect" class="link go_title" >{ci_language line="My Shop > Connect"}</a> 
                        </li>				
                    </ul>
                    {/if}                       
                    <ul class="list-circle-green m-b20">
                        <li>
                            {if !isset($id_shop)}
                                {ci_language line="You must feed data source."}<br />
                                <a href="{$base_url}/my_shop/configuration" class="link go_title"  >
                                    {ci_language line="Go to data source"}
                                </a>
                            {else}
                                {ci_language line="Data source completed."} 
                            {/if}
                        </li>
                        <li>
                            {if !isset($id_shop)}
                                {ci_language line="You must config marketplaces."}<br />
                                <a href="{$base_url}/marketplace/configuration" class="link go_title"  >
                                    {ci_language line="Go to data source"}
                                </a>
                            {else}
                                {ci_language line="Market place configuration completed."}
                            {/if}
                        </li>
                    </ul>
                    <ul class="{if !isset($category) || empty($category)}list-circle-orange{else}list-circle-green{/if} m-b20">
                        <li>
                            {if !isset($category) || empty($category)}
                                {ci_language line='You must choose category in profile.'}<br />
                                <a href="{$base_url}/my_feeds/parameters/profiles/category" class="link go_title" > 
                                    {ci_language line='Go to profiles category'}
                                </a>
                            {else}
                                {ci_language line="Category configuration completed."}
                            {/if} 
                        </li>				
                    </ul> 
                    {if !isset($user_secure) || (isset($user_secure) && !$user_secure) }
                    <ul class="list-circle-orange m-b20  ">
                        <li> 
                                {ci_language line="Please secure your account by activating Multi-Factor Authentication in"}   
                                <a href="{$base_url}users/security" class="link go_title" >{ci_language line="My Account > Security"}</a> 
                        </li>				
                    </ul>
                    {/if}
                    
                </div>

                <div class="col-lg-3 col-sm-6">
                    <h4 class="dark-gray bold">{ci_language line="Major Errors"}</h4>
                    <ul class="list-circle-red">
                        {if (!isset($error_prod)||sizeof($error_prod)==0) && (!isset($error_offer)||sizeof($error_offer)==0)}
                            <li>{ci_language line="No data."}</li>
                            {else}
                                {if isset($error_prod)}

                                {foreach $error_prod as $k => $er name=error_prod}
                                    {if $smarty.foreach.error_prod.index < 3}
                                        <li>
                                            {if $er.type=='error'}
                                                <p>{ci_language line=$k}</p>
                                            {else}
                                                <p>{ci_language line=$k}</p>
                                            {/if}
                                            {if isset($er.percent)}
                                                <p><span>{$er.percent|number_format:2:".":","} %</span></p>
                                            {/if}
                                        </li>
                                    {/if}
                                {/foreach}

                            {/if}

                            {if isset($error_offer)}
                                {foreach $error_offer as $k => $er name=error_offer}
                                    {if $smarty.foreach.error_offer.index < 2}
                                        <li>
                                            {if $er.type=='error'}
                                                <p>{ci_language line="Error"}: {ci_language line=$k}</p>
                                            {else}
                                                <p>{ci_language line=$k}</p>
                                            {/if}
                                            {if isset($er.percent)}
                                                <p><span>{$er.percent|number_format:2:".":","} %</span></p>
                                            {/if}

                                        </li>
                                    {/if}
                                {/foreach}
                            {/if}
                        {/if}	
                    </ul>
                    <a href="{$base_url}my_feeds/messages" class="more m-b30">{ci_language line="See more"}</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="mainChart p-xs-rl20 m-t15">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart">
                            {if !isset($all_prod)}{$all_prod = 0}{/if}
                        {if !isset($active_prod)}{$active_prod = 0}{/if}
                    {if !isset($use_percent)}{$use_percent = 0}{/if}
                    {math equation="100*(x-y)/50000" x=$all_prod y=$active_prod assign="inuse_precent"}
                    {math equation="100-x-y" x=$use_percent y=$inuse_precent assign="freeuse_precent"}

                    {math equation="50000-x" x=$all_prod assign='numfreespace'}
                    {math equation="x-y" x=$all_prod y=$active_prod assign='numinactive'}
                    <h4 class="dark-gray bold m-t20">{ci_language line="Inventory"}</h4>
                    {if !isset($use_percent)||!isset($all_prod)||$all_prod==0  }
                        <p class=" widget-main padding-16 center light-gray lightRoboto">{ci_language line="No data."}</p>
                    {else}
                        <div class="widget-main  position-relative">
                            <div id="chartdonut"></div>
                        </div>
                    {/if}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="chart">
                    <h4 class="dark-gray bold m-t20">{ci_language line="Ordered items"}</h4>
                    <ul class="tabs">
                        <li class="day30_order ">{ci_language line="Last 30 days"}</li>
                        <li class="day7_order">{ci_language line="Last 7 days"}</li>
                        <li class="day_order active">{ci_language line="Today"}</li>
                    </ul>
                    {if !isset($use_percent)||!isset($all_prod)||$all_prod==0  }
                        <p class=" widget-main padding-16 center light-gray lightRoboto">{ci_language line="No data."}</p>
                    {else}
                        <div class="widget-main  position-relative">
                            <div id="chartdonut_order"></div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
</div>                                           

<div class="row">
    <div class="col-xs-12 p-b20">	
        <div class="p-rl40 p-xs-rl20">
            <div class="chart lineChart">
                <h4 class="dark-gray bold m-t20">{ci_language line="Sales"}</h4>
                <ul class="tabs">
                    <li id="sale_day30">{ci_language line="Months"}</li>
                    <li id="sale_day7"id="sale_day7">{ci_language line="Weeks"}</li>
                    <li class="active"  id="sale_day1">{ci_language line="Days"}</li>
                </ul>
                <canvas id="chart_sale" class="col-md-12" height="50"></canvas>
            </div>
        </div>
    </div>
</div>

<div class="row p-rl40 p-xs-rl20 m-t20">
    <div class="col-md-8">
         
        <div class="col-sm-6 col-md-6">
            <div class="pay">
                <img class="pay_picture" src="{$cdn_url}assets/images/pay/amazon.png" alt="">
                {if  !isset($f_step) || (isset($f_step) && ($f_step> 2||$f_step=='fin')) }
                    {if isset($amazon_btn)}
                        <a class="pay_link wizard_btn" href="{$base_url}/amazon/platform/1" id="amazon_synchronize_wizard">{ci_language line="Configure Amazon"}</a>
                    {else}
                        <a class="pay_link wizard_btn" href="{$base_url}/marketplace/configuration_popup/4/amazon" id="amazon_synchronize_wizard">{ci_language line="Configure Amazon"}</a>
                    {/if}
                {else}
                    <a class="pay_link wizard_btn" href="{$base_url}/dashboard/iframe_popup_conf/{$f_step}" id="amazon_synchronize_wizard">{ci_language line="Configure Amazon"}</a>
                {/if}
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="pay">
                <img class="pay_picture" src="{$cdn_url}assets/images/pay/ebay.png" alt="">
                {if  !isset($f_step) || (isset($f_step) && ($f_step> 2||$f_step=='fin')) }
                    {if isset($ebay_btn)}
                        <a class="pay_link wizard_btn" href="{$base_url}ebay/ebay_wizard" id="ebay_wizard">{ci_language line="Configure eBay"}</a>
                    {else}
                        <a class="pay_link wizard_btn" href="{$base_url}/marketplace/configuration_popup/4/ebay" id="ebay_synchronize_wizard">{ci_language line="Configure eBay"}</a>
                    {/if}
                {else}
                    <a class="pay_link wizard_btn" href="{$base_url}/dashboard/iframe_popup_conf/{$f_step}" id="ebay_synchronize_wizard">{ci_language line="Configure eBay"}</a>
                {/if}
            </div>
        </div>     
        {if $countdown['amazon'] != -1}
            <div class="col-sm-6 col-md-{$countdown['count']}">
                <div class="pay">
                    <img class="pay_picture" src="{$cdn_url}assets/images/logo-amazon.png" alt="">
                    <p class="center true-blue lightRoboto m-b15">{ci_language line="Automatic update offer to Amazon in"} : </p>
                    <ul class="pay_list">
                        <li class="pay_item">{ci_language line="Hours"}</li>
                        <li class="pay_item">{ci_language line="Minutes"}</li>
                        <li class="pay_item">{ci_language line="Secondes"}</li>
                    </ul>
                    <div class="countdown" rel="{$countdown['amazon']}"></div>
                    <div id="note"></div>
                </div>
            </div>
        {/if}
        {if $countdown['ebay'] != -1}
            <div class="col-sm-6 col-md-{$countdown['count']}">
                <div class="pay">
                    <img class="pay_picture" src="{$cdn_url}assets/images/logo-ebay.png" alt="">
                    <p class="center true-blue lightRoboto m-b15">{ci_language line="Automatic update offer to ebay in"} : </p>
                    <ul class="pay_list">
                        <li class="pay_item">{ci_language line="Hours"}</li>
                        <li class="pay_item">{ci_language line="Minutes"}</li>
                        <li class="pay_item">{ci_language line="Secondes"}</li>
                    </ul>
                    <div class="countdown" rel="{$countdown['ebay']}"></div>
                    <div id="note"></div>
                </div>
            </div>
        {/if}
        {if $countdown['fb'] != -1}
            <div class="col-sm-6 col-md-{$countdown['count']}">
                <div class="pay">
                    <img class="pay_picture" src="{$cdn_url}assets/images/logo_count.png" alt="">
                    <p class="center true-blue lightRoboto m-b15">{ci_language line="Automatic import offer from"}{if isset($shop_default)} {$shop_default}{/if} {ci_language line="in"} : </p>
                    <ul class="pay_list">
                        <li class="pay_item">{ci_language line="Hours"}</li>
                        <li class="pay_item">{ci_language line="Minutes"}</li>
                        <li class="pay_item">{ci_language line="Secondes"}</li>
                    </ul>
                    <div class="countdown" rel="{$countdown['fb']}"></div>
                    <div id="note"></div>
                </div>
            </div>
        {/if}
    </div>
    <div class="col-md-4">
        <div class="col-sm-6 col-md-3">
            <div class="support dSTable">
                <div class="dRow">
                    <img src="{$cdn_url}{$support_employee_info1.image}" alt="" class="dCell icon-circle icon-user support_picture">							
                    <div class="dCell">
                        <p class="tooLight-gray">{ci_language line="Your support"}</p>
                        <p class="dark-gray h1 m-t0">{$support_employee_info1.name}</p>
                        <a href="mailto:{$support_employee_info1.email}" class="link regRoboto h4">{$support_employee_info1.email}</a>
                    </div>
                </div>
            </div>

            {*{foreach from=$support_employee_info3 item=teamsupport name=supportteams }
            <div class="support dSTable">
            <div class="dRow">
            <img src="{$cdn_url}{$teamsupport.image}" alt="" class="dCell icon-circle icon-user support_picture">							
            <div class="dCell">
            {if $smarty.foreach.supportteams.first}<p class="tooLight-gray">{ci_language line="Support teams"}</p>{/if}
            <p class="dark-gray h1 m-t0">{$teamsupport.name}</p>
            <a href="mailto:{$teamsupport.email}" class="link regRoboto h4">{$teamsupport.email}</a>
            </div>
            </div>
            </div>
            {/foreach}*}

        </div>
    </div>
</div>

</div>
</div>
<!-- // Content END -->

<!-- // DIV END in HEADER -->
</div>
</div>
<!-- // DIV END in HEADER -->


<!-- // Data Chart // -->
<input type="hidden" name="count_to" value="{if isset($active_prod)}{$active_prod}{else}0{/if}" />
<input type="hidden" name="degree" value="{if isset($degree)}{$degree}{else}-180{/if}" />
<input type="hidden" name="degree_all" value="{if isset($degree)}{$degree}{else}-180{/if}" />
<input type="hidden" name="f_step" value="{if isset($f_step)}true{else}false{/if}" />
<input type="hidden" name="process_running" value="{ci_language line="Process will be running soon."}" />
<input type="hidden" name="process_not_found" value="{ci_language line="You have not set up this process."}" />
<input type="hidden" name="service_comming_soon" value="{ci_language line="This service is coming soon."}" />
<input type="hidden" name="fb_wizard_title" value="{ci_language line="Feedbiz starter wizard"}" />

<input type="hidden" name="spacefree" value="{ci_language line="Free space"} ({$numfreespace|number_format})" />
<input type="hidden" name="active" value="{ci_language line="Active"} ({$active_prod|number_format})" />
<input type="hidden" name="inactive" value="{ci_language line="Inactive"} ({$numinactive|number_format})" />

<input type="hidden" name="use_percent" value="{math equation="a/100" a=$use_percent}" />
<input type="hidden" name="inuse_precent" value="{math equation="a/100" a=$inuse_precent}" />
<input type="hidden" name="freeuse_precent" value="{math equation="a/100" a=$freeuse_precent}" />
<input type="hidden" name="orders_items" value='{if isset($orders_items)}{$orders_items}{else}{ }{/if}' />
<input type="hidden" name="orders_graph" value='{if isset($orders_graph)}{$orders_graph}{else}{ }{/if}' />

<!-- // End Data Chart // -->

<input type="hidden" id="userdata" value="{if isset($profile)}{$profile}{/if}" />               
<input type="hidden" id="f_step" value="{if isset($f_step)}{$f_step}{/if}" />               
<input type="hidden" id="command" value="{if isset($command)}{$command}{/if}" />
{if isset($profile_product)}<input type="hidden" id="userdata_product" value="{$profile_product}" />{/if}
{if isset($profile_offer)}<input type="hidden" id="userdata_offer" value="{$profile_offer}" />{/if}
{if isset($profile_order)}<input type="hidden" id="userdata_order" value="{$profile_order}" />{/if} 
{include file="footer.tpl"}
{if isset($profile)}
    <script src="{$cdn_url}assets/js/FeedBiz/users/authenticate.js"></script>
{/if}
<script src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>

<script src="{$cdn_url}assets/js/highchart/highstock.js"></script>

<script src="{$cdn_url}assets/js/jquery.spritely-0.4.js"></script>
<script src="{$cdn_url}assets/js/countdown.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/dashboard.js" type="text/javascript"></script>