                <!-- // Widget Heading END -->
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom">{ci_language line="synchronization_wizard"}</h3>
                </div>
                <div class="synchronization-style">
                {include file="ebay/configuration_step.tpl"}
                </div>
                <div class="widget-body">
                    <h5 class="header-main header-full">{ci_language line="information_inventory"}</h5>
                    <div class="synchronization-warp">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-11">
                                    <h5 class="title-main title-bottom title-half no-margin">{ci_language line="your_store"}</h5>
                                    <div class="widget-box">
                                        <div class="form-group inventory-main">
                                            <label class="control-label"><i class="fa fa-database"></i> {ci_language line="database_store"}</label>
                                            <div class="col-md-12">
                                                <div class="product-list"><i class="fa fa-angle-double-right"></i> {ci_language line="synchronize_title"} <b>{$count_product}</b> {ci_language line="product_s"}</div>
                                            </div>
                                            <div class="help-block text-small">{ci_language line="your_store_help"}</div>
                                        </div>
                                        <div class="form-group inventory-main">
                                            <label class="control-label"><i class="fa fa-shopping-cart"></i> {ci_language line="ebay_store"}</label>
                                            <div class="col-md-12">
                                                <div class="product-list"><i class="fa fa-angle-double-right"></i> {ci_language line="synchronize_title"} <b>{$count_synchronize}</b> {ci_language line="product_s"}</div>
                                            </div>
                                            <div class="help-block text-small">{ci_language line="ebay_store_help"}</div>
                                        </div>
                                    </div> 
                                </div> 
                            </div> 
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <h5 class="title-main title-bottom title-half no-margin">{ci_language line="synchronize_product"}</h5>
                                    <div class="widget-box">
                                        <div class="form-group synchronize-main">
                                            <label class="control-label"><i class="fa fa-check-square"></i> {ci_language line="synchronize_product"}</label>
                                            <div class="col-md-12">
                                                <div class="product-list"><i class="fa fa-angle-double-right"></i> {ci_language line="synchronize_title"} <b>{$export_count}</b> {ci_language line="product_s"}</div>
                                            </div>
                                            <div class="help-block text-small">{ci_language line="synchronize_product_help"}</div>
                                        </div>
                                        <div class="form-group synchronize-main">
                                            <label class="control-label"><i class="fa fa-times-circle"></i> {ci_language line="unknown_product"}</label>
                                            <div class="col-md-12">
                                                <div class="product-list"><i class="fa fa-angle-double-right"></i> {ci_language line="synchronize_title"} <b>{$unknown}</b> {ci_language line="product_s"}</div>
                                            </div>
                                            <div class="help-block text-small">{ci_language line="unknown_product_help"}</div>
                                        </div>
                                    </div>                    
                                </div>                    
                            </div>                    
                        </div>     
                        <div class="col-md-12">
                            <h5 class="title-main title-bottom title-half">{ci_language line="synchronize_matching"}</h5>
                            <button class="btn btn-primary btn-synchronization" type="submit" data-last="finish">
                                {ci_language line="get_start"}
                            </button>
                        </div>                    
                    </div>                    
                </div>
