                <!-- // Widget Heading END -->
                <div class="widget-head">
                    <h3 class="header-main header-half header-bottom">{ci_language line="synchronization_wizard"}</h3>
                </div>
                <div class="synchronization-style">
                {include file="ebay/configuration_step.tpl"}
                </div>
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom text-center">{ci_language line="get_synchronization_wizard"}</h3>
                </div>
                <div class="widget-body">
                        <div class="widget-main text-center">
                            <div class="help-block text-small">{ci_language line="synchronization_start_help"}</div>
                            <button class="btn btn-primary btn-synchronization" type="button" data-last="finish">
                                {ci_language line="get_start"}
                            </button>
                            <div class="clearfix synchronization-load center" id="synchronization-page-load">
                                    <i class="fa fa-spinner fa-spin fa-3x"></i>
                            </div>
                        </div>                    
                </div>
