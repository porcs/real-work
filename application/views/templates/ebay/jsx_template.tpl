{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay.min.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay_import.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/{$current_page}.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/advance_import.css" />
{if empty($root_page)}{ci_config name="root_page"}{/if}
<div id="content" class="{if !isset($popup)}main {/if}p-rl40 p-xs-rl20" class="row" ng-app="ebayApp" >
<form class="form-horizontal" id="form-submit" method="POST" {if !empty($next_page)}action="{$next_page}"{/if}  enctype='multipart/form-data'>
        <h1 class="lightRoboto text-uc head-size light-gray"><img class="ebay-logo" src="{$cdn_url}assets/images/EBay_logo.svg-Custom.png"> {ci_language line="header_$current_page"}</h1>
            
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        <div class="row">
            <div class="col-xs-12"> 
                <h3 class="head_text p-t10 b-Top m-b0 clearfix heading margin-none">
                    <img src="{$cdn_url}assets/images/flags/{$flag_country}.png" class="flag m-r10"> {$name_country}
                </h3> 
            </div>
        </div>
        <div class="innerAll spacing-x2">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-inverse">
                        <div id="main"></div>
                    </div>
                </div>
            </div>
        </div>
    <div class="close">
        <button type='button' class='close' data-dismiss='alert'><i class='fa fa-close'></i></button><i class='fa fa-exclamation-triangle'></i>
    </div>
    <input type="hidden" name="id-site" id="id-site" value="{$id_site}" />
    <input type="hidden" name="id-packages" id="id-packages" value="{$packages}" />
    <input type="hidden" name="current-page" id="current-page" value="{$current_page}" />
    {if isset($ajax_url)}
    <input type="hidden" name="ajax-url" id="ajax-url" value="{$ajax_url}" />
    {/if}
    <input type="hidden" name="update-{$current_page}-success" class="update-{$current_page}-success" value="{ci_language line="update_{$current_page}_success"}">
    <input type="hidden" name="update-{$current_page}-fail" class="update-{$current_page}-fail" value="{ci_language line="update_{$current_page}_fail"}">
    {if isset($hidden_data)}
    {foreach from = $hidden_data key = key item = hidden}
    <input type="hidden" name="{$key}" class="{$key}" value="{$hidden}">
    {/foreach}
    {/if}
    {if isset($validation_data)}
    {foreach from = $validation_data key = field item = data}
    {foreach from = $data key = validation_key item = validation_message}
    <input type="hidden" name="{$field}-{$validation_key}" class="{$field}-{$validation_key}" value="{$validation_message}" data-rules="{$validation_key}" data-type="validation">
    {/foreach}
    {/foreach}
    {/if}
</form>
</div>
{if !empty($jquery_form)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/form/jquery.form.js"></script>
{/if}
{if !empty($currency_format)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/formatCurrency/jquery.formatCurrency.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ebay.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_template.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_wizard_start.js"></script>
{if !empty($ng)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/node_modules/angular/angular.min.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/react.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/react-redux.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/react-dom.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/browser.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.10.3/babel.min.js"></script> -->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/redux.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/react/expect.min.js"></script>
<script type="text/babel" src="{$cdn_url}assets/js/FeedBiz/ebay/jsx/{$current_page}.jsx"></script>

{include file="footer.tpl"}