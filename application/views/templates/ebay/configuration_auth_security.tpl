                    
                    <div class="widget-head">
                        <h3 class="heading show_thumbnails"><i class="fa fa-shield"></i> {ci_language line="header_$current_page"}</h3><div class="pull-right">{ci_language line="product_key_mode"} <div class="make-switch" data-on="success" {if !isset($appMode)}data-off="default"{else}data-off="ture"{/if} data-size="mini"><input id="form-app-mode" type="checkbox" {if !isset($appMode)}checked='checked'{/if}></div></div>
                    </div>
                    <!-- // Widget Heading END -->
                    <div class="widget-body">
                        <h5 class="innerAll half heading-buttons border-bottom">{ci_language line="title_$current_page"}</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="form-div-id">{ci_language line="user_id:"}</label>
                            <div class="col-md-10">
                                <span class="input-icon input-icon-right span4">
                                    <input class="form-control" type="text" name="form-user-id" id="form-user-id" value="{if isset($userID) }{$userID}{/if}" placeholder="{ci_language line="user_id"}">
                                </span>
                            </div>
                        </div>
                    </div>