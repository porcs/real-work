                            {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                            <!-- // Widget Heading END -->
                            <div class="widget-body">
                                <div class="widget-body padding-none">
                                    <div class="widget-main synchronize-wrap padding-none">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="export_product"}</h5>
                                        <div class="help-block text-small">{ci_language line="export_product_help"}</div>
                                        <div class="col-md-12 synchronize-main text-right">
                                            <div class="col-xs-12 col-md-12 export-product-button">
                                                <div class="col-md-12">
                                                    <button class="btn btn-primary btn-wizard send-products" id="send-products"><i class="fa fa-long-arrow-up"></i> {ci_language line="export_product_update"}</button>
                                                    {include file="ebay/configuration_actions_alert.tpl"}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>