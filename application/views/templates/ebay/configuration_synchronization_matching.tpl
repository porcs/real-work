                <!-- // Widget Heading END -->
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom">{ci_language line="synchronization_wizard"}</h3>
                </div>
                <div class="synchronization-style">
                {include file="ebay/configuration_step.tpl"}
                </div>
                <div class="widget-body">
                    <h5 class="header-main header-full header-bottom">{ci_language line="synchronize_and_matching"}</h5>
{*                    <div class="title-main title-bottom title-half no-margin">{ci_language line="reference_type"}</div>*}
                    <div class="synchronization-warp">
                        <div class="widget-main synchronization-main">
                            <div class="col-md-12">
                                <div class="col-md-4 form-group">
                                    <label class="control-label" for="dispatch_time">{ci_language line="choose_reference"}</label>
                                    <div class="col-md-11">
                                        <select class="reference" id='reference' name='choose_reference'>
                                            <option value="sku">{ci_language line="product_sku"}</option>
                                            <option value="ean13">{ci_language line="product_ean13"}</option>
                                            <option value="upc">{ci_language line="product_upc"}</option>
                                            <option value="reference">{ci_language line="product_reference"}</option>
                                            <option value="custom">{ci_language line="custom_synchronize"}</option>
                                        </select>
                                        <div class="help-block text-small">{ci_language line="synchronization_detail_select_help"}</div>
                                    </div>
                                </div>                
                                <div class="col-md-4 form-group insert-key-group insert-key-group-hide">
                                    <label class="control-label" for="insert-key-name">{ci_language line="insert_key"}</label>
                                    <div class="col-md-11">
                                        <input class="form-control insert-key-name" type="text" id="insert-key-name" name="insert-key-name">
                                        <div class="help-block text-small">{ci_language line="synchronization_detail_key_help"}</div>
                                    </div>
                                </div>                
                                <div class="col-md-4 form-group insert-key-group insert-key-group-hide">
                                    <label class="control-label" for="reference-key-name">{ci_language line="reference_key"}</label>
                                    <div class="col-md-11">
                                        <input class="form-control reference-key-name" type="text" id="reference-key-name" name="reference-key-name">
                                        <div class="help-block text-small">{ci_language line="synchronization_detail_reference_help"}</div>
                                    </div>
                                </div>                
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-synchronization" type="button" data-last="finish">
                                    {ci_language line="auto_matching"}
                                </button>
                            </div> 
                        </div>                    
                    </div>   

                    <div class="clearfix"></div>
{*                    <div class="title-main title-bottom title-half no-margin">{ci_language line="matching_product"}*}
{*                        <ul class="confirm-ul green">
                            <li><i class="fa fa-check-square"></i> {ci_language line="confirm_synchronize"}</li>
                        </ul>*}
                    </div>
                    <div class="synchronization-warp">
                        <div class="clearfix">
                        <div class="col-xs-6">
                            <div class="col-xs-6 form-group cover-group">
                                <label class="control-label-fluid col-xs-6">{ci_language line="choose_mapping_against"}</label>
                                <div class="col-xs-6">
                                    <select class="synchronize-select">
                                        <option value="unknown">{ci_language line="unknown_product"}</option> 
                                    </select>
                                </div>
                            </div>
                        </div>
                        </div>
                            <table class="dynamicTable colVis table" id="statistics" >
                                <thead>
                                    <tr>
                                        <th class='center check-all' rel='check_all' data-rel="0"><label class="cb-checkbox cb-sm" id="checkAllButton"><input id="check-all" type="checkbox"></label></th>
                                        <th class='center product-image' rel='product_image' data-rel="1">{ci_language line="product_image"}</th>
                                        <th class='center id-product' rel='id_product' data-rel="2">{ci_language line="product_item"}</th>
                                        <th class='product-content' rel='product_content' data-rel="3">{ci_language line="product_content"}</th>
{*                                        <th class='profile-name' rel='profile_name' data-rel="7">{ci_language line="profile_name"}</th>*}
                                        <th class='product-information' rel='product_information' data-rel="4">{ci_language line="product_information"}</th>
                                        <th class='product-detail' rel='product_detail' data-rel="5">{ci_language line="product_detail"}</th>
                                        <th class='center product-active' rel='active' data-rel="6">{ci_language line="active"}</th>
                                    </tr>
                                </thead>
                                <!-- // Table heading END -->
                                <tbody>
                                    {if !empty($statistics)} {foreach from=$statistics key=id item=statistic}
                                    <tr>
                                        <td class='center' rel='check_all'><input id="check-all" type="checkbox"></td>
                                        <td class='center' rel='product_image'>{ci_language line="product_image"}</td>
                                        <td class='center' rel='id_product'>{$statistic['id_product']}</td>
                                        <td class='center' rel='product_content'>{ci_language line="product_content"}</td>
{*                                        <td class='center' rel='profile_name'>{ci_language line="profile_name"}</td>*}
                                        <td class='center' rel='product_information'>{ci_language line="product_information"}</td>
                                        <td class='center' rel='product_detail'>{ci_language line="product_detail"}</td>
                                        <td class='center' rel='active'>{ci_language line="active"}</td>
                                    </tr>
                                    {/foreach} {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class='center'></th>
                                        <th class='center'></th>
                                        <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_item"}" rel='id_product'></th>
                                        <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_content"}" rel='product_content'></th>
{*                                        <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="profile_name"}" rel='profile_name'></th>*}
                                        <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_information"}" rel='product_information'></th>
                                        <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_detail"}" rel='product_detail'></th>
                                        <th class='center'>
                                            <select class="product-active">
                                                <option value="-1">{ci_language line="all"}</option> 
                                                <option value="1">{ci_language line="active"}</option> 
                                                <option value="0">{ci_language line="unactive"}</option> 
                                            </select>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- // Table END -->           
                        </div>
                    </div>
                <script>
                    {literal}
                        var lg_current   = [{/literal}{$currency_code}{literal}];
                    {/literal}  
                </script>
