
                {if isset($popup)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                <div class="widget-body">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        <div class="widget-body padding-none">
                        {include file="ebay/main_info.tpl"}
                            <div class="profile-shipping">
                                <h5 class="title-main title-bottom title-half">{ci_language line="payment_location"}</h5>
                                <div class="widget-main sp-postal-code">
                                    <div class="form-group postal-main">
                                        <div class="col-md-12">
                                            <div class="col-md-5 form-group">
                                                <label class="control-label" for="form-postal-code">{ci_language line="postcode"}</label>
                                                <div class="col-md-10 position-relative">
                                                    <input class="form-postal-code form-control" type="text" id="form-postal-code"  data-example="{if isset($code)}{$code['exam_code']}{/if}" data-code="{if isset($code)}{$code['code']}{/if}" name="form-postal-code" value="{if isset($postal_code) }{$postal_code}{/if}" placeholder="{ci_language line="postcode"}" required {if isset($postal_code_pattern) }{$postal_code_pattern}{/if}><div class="asterisk"><i class="icon-asterisk"></i></div>
                                                    <div class="clear clearfix"></div>
                                                    <div class="clear clearfix help-block text-small">{ci_language line="postcode_help"}</div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 form-group">
                                                <label class="control-label" for="dispatch_time">{ci_language line="dispatch_time"}</label>
                                                <div class="col-md-10">
                                                    <select class="dispatch_time" id='dispatch_time' name='dispatch_time'>
                                                        {foreach from = $dispatch_time item = time}
                                                            <option value="{$time['dispatch_time']}" {if isset($time['selected'])}selected='selected'{/if}>{ci_language line="{$time['dispatch_time_name']}"}</option>
                                                        {/foreach}
                                                    </select>
                                                    <div class="help-block text-small">{ci_language line="dispatch_time_help"}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>