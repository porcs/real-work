
                <div class="widget-head">
                    <h3 class="header-main header-full">{ci_language line="title_$current_page"}</h3>
                </div>
                <!-- // Widget Heading END -->
                <div class="widget-body">
<!--                     <h5 class="title-main title-bottom title-half">{ci_language line="default_price_rule"}</h5> -->
                    {include file="ebay/main_info.tpl"}                  
                    {if !empty($statistics)}
                        <div class="row m-t20">
                            <div class="col-sm-2 b-Right">
                                    <p class="montserrat dark-gray text-uc">{ci_language line="processed"} </p>
                                    <p class="poor-gray"><span class="true-blue">{if isset($statistics)}{$statistics[0]['total']}{else}0{/if}</span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-sm-2 b-Right">
                                    <p class="montserrat dark-gray text-uc">{ci_language line="success"} </p>
                                    <p class="poor-gray"><span class="true-green">{if isset($statistics)}{$statistics[0]['success']}{else}0{/if}</span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-sm-2 b-Right">
                                    <p class="montserrat dark-gray text-uc">{ci_language line="warning"} </p>
                                    <p class="poor-gray"><span class="warning-text">{if isset($statistics)}{$statistics[0]['warning']}{else}0{/if}</span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-sm-2 b-Right">
                                    <p class="montserrat dark-gray text-uc">{ci_language line="error"} </p>
                                    <p class="poor-gray"><span class="true-pink">{if isset($statistics)}{$statistics[0]['error']}{else}0{/if}</span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-sm-2 p-t20">
                                    <a href="{if isset($packages)}{$base_url}{"/ebay/statistics_products/"}{$packages}{/if}" target="_blank" class="link p-size">{ci_language line="see_more_detail"}</a>							
                            </div>
                        </div>
                    {/if}
                </div>
