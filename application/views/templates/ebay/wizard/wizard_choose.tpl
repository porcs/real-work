
                <div class="widget-head">
                    <h3 class="header-main header-full">{ci_language line="title_$current_page"}</h3>
                </div>
                <!-- // Widget Heading END -->
                <div class="widget-body">
                    <h5 class="title-main title-bottom title-half">{ci_language line="country_list"}</h5>
                    {include file="ebay/main_info.tpl"}
                    <div class="widget-main">
                        <ul id="form-field-select" class="selectWizard clearfix">
                            {if !empty($country_site) && count($country_site)}{foreach from = $country_site item = site}
                                <li class="selectWizard_point" value="{$site['id_site_ebay']}" data-key="{$site['id']}"><img src="{$base_url}assets/images/flags/{$site['ext_edit']}.png"><p class="poor-gray">{$site['country']}</p></li>
                            {/foreach}{/if}
                        </ul>
                    </div>
                </div>
