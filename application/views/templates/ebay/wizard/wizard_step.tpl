        {if !empty($popup) && isset($popup) } 
                <ul class="wizard-steps wizard-style"> 
                        {counter start = 1 skip = 1 assign = popup_step}
                        {if !empty($next) }{foreach from = $next key = page item = value }
                        <li {if $popup >= $popup_step} class="active"{/if}><span class="step">{$popup_step}</span><span class="title">{ci_language line="$page"}</span></li>
                        {counter}
                        {/foreach}{/if}
                        <li {if $popup >= $popup_step} class="active"{/if}><span class="step">{$popup_step}</span><span class="title">{ci_language line="wizard_finish"}</span></li> 
                </ul>
                        <div style="clear: both"></div>
        {/if}