
                <div class="widget-head">
                    <h3 class="heading show_thumbnails"><i class="fa fa-flag"></i> {ci_language line="title_$current_page"}</h3>
                </div>
               <!-- // Widget Heading END -->
                <div class="widget-body">                    
                    <div class="form-group">
                        <button class="btn btn-success send-products" id="send-products" type="button">
                            <span class="send"><i class="fa fa-upload"></i> {ci_language line="send_products_to_ebay"}</span>
                            <span class="sending">{ci_language line="sending.."}</span>
                            <span class="error-message"></span>
                        </button>

                        <div id="send-products-message" class="alert alert-success">
                            {ci_language line="Starting Send"}
                        </div>
                    </div>
                </div>

