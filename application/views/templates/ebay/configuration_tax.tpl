                    
                    {*<div class="widget-head">
                        <h3 class="header-main header-full">{ci_language line="title_$current_page"}</h3>
                    </div>*}
                    <!-- // Widget Heading END -->
                    <div class="widget-body">
{*                    <h5 class="title-main title-bottom title-half">{ci_language line="default_tax_term"}</h5>*}
                        {include file="ebay/main_info.tpl"}
                        <div class="widget-main">
                            <div class="price_rules">
                                    <div class="form-group tax-main">
                                    <label class="control-label col-xs-2" for="tax-type">{ci_language line="selected_tax"}</label>
                                    <div class="help-block text-small">{ci_language line="tax_term_help"}</div>
                                    <div class="col-xs-12 col-md-5">
                                          <div class="col-xs-12 col-md-11">
                                            <select class="tax-type" id='tax-type' name='tax-type'>
                                                <option value="0">{ci_language line="no_tax"}</option>
                                                {if !empty($tax_lang)}
                                                    {foreach from = $tax_lang item = tax }
                                                        <option value="{$tax['id_tax']}" {if !empty($tax_selected) && $tax_selected[0]['id_tax'] == $tax['id_tax']}selected='selected'{/if}>{$tax['name']}</option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>
                                </div>   
                                <div class="form-group tax-main">
                                    <label class="control-label col-md-2" for="form-price-rounding">{ci_language line="rounding"}</label>
                                    <div class="col-md-10">
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="1" {if !empty($tax_selected) && $tax_selected[0]['decimal'] == 1}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="one_digit"}</span>
                                            </label>
                                        </div>
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="2" {if !empty($tax_selected) && $tax_selected[0]['decimal'] == 2}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="two_digit"}</span>
                                            </label>
                                        </div>
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="0" {if !empty($tax_selected) && $tax_selected[0]['decimal'] == 0 || empty($tax_selected)}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="none"}</span>
                                            </label>
                                        </div>
                                        <div class="help-block text-small">{ci_language line="price_modifier_rounding_help"}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>