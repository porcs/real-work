
                    <div class="status update">
                        <p class="m-t0">
                            <span>{ci_language line="starting_send"}..</span>
                        </p>
                    </div>
                    <div name="message_success">
                        <p class="m-t0 green">
                            <i class="fa fa-check-square"></i>
                            <span></span>
                        </p>
                    </div>
                    <div name="message_error">
                        <p class="m-t0 red">
                            <i class="fa fa-exclamation-circle"></i>
                            <span></span>
                        </p>
                    </div>