                <!-- // Widget Heading END -->
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom">{ci_language line="synchronization_wizard"}</h3>
                </div>
                <div class="synchronization-style">
                {include file="ebay/configuration_step.tpl"}
                </div>
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom text-center">{ci_language line="synchronize_finish"}</h3>
                </div>
                <div class="widget-body">
                        <div class="widget-main text-center">
                            <div class="help-block text-small">{ci_language line="synchronization_finish_help"}</div>
                            <button class="btn btn-primary btn-primary-categories" type="submit" data-last="finish">
                                {ci_language line="auth_setting"}
                            </button>
                        </div>                    
                </div>
