
                    <!-- // Widget Heading END -->
                    {*<div class="widget-head">
                        <h3 class="header-main header-full">{ci_language line="create_your_return"}</h3>
                    </div>*}
                    <div class="widget-body">
                        {*<h5 class="title-main title-bottom title-half">{ci_language line="returns_setting"}</h5>*}
                        {include file="ebay/main_info.tpl"}
                        <div class="widget-main">
                            <div class="form-group returns-main">
                                <label class="control-label" for="returns-policy-type">{ci_language line="returns_policy"}</label>
                                <div class="col-md-12">
                                    <div class="col-xs-11 col-md-4">
                                        <div class="col-md-10">
                                            <select class="returns-policy-type" id='returns-policy-type' name='returns-policy-type'>
                                                <option value="ReturnsAccepted" {if isset($profile_data['returns_policy']) && $profile_data['returns_policy'] == 'ReturnsAccepted'}selected='selected'{/if}>{ci_language line="returns_accepted"}</option>
                                                <option value="ReturnsNotAccepted" {if isset($profile_data['returns_policy']) && $profile_data['returns_policy'] == 'ReturnsNotAccepted'}selected='selected'{/if}>{ci_language line="returns_not_accepted"}</option>
                                            </select>
                                            {*<div class="col-md-12">
                                                <div class="help-block text-small">{ci_language line="return_specify_help"}</div>
                                            </div>*}
                                        </div>
                                        <div class="p-t10 p-l10 col-sm-2">{_get_tooltip_icon({ci_language line="return_specify_help"})}</div>
                                    </div>
                                    <div class="col-xs-11 col-md-4 policy-days">
                                        <div class="col-md-10">
                                            <select class="returns-policy-days" id='returns-policy-days' name='returns-policy-days'>
                                                {foreach from = $returns_within item = re_within}
                                                    <option value="{$re_within['within']}" {if isset($re_within['selected'])}selected='selected'{/if}>{ci_language line="{strtolower($re_within['within'])}"}</option>
                                                {/foreach}
                                            </select>
                                            {*<div class="col-md-12">
                                                <div class="help-block text-small">{ci_language line="return_within_help"}</div>
                                            </div>*}
                                        </div>
                                        <div class="p-t10 p-l10 col-sm-2">{_get_tooltip_icon({ci_language line="return_within_help"})}</div>
                                    </div>
                                    <div class="col-xs-11 col-md-4 policy-pays">
                                        <div class="col-md-10">
                                            <select class="returns-policy-pays" id='returns-policy-pays' name='returns-policy-pays'>
                                                <option value="Buyer" {if isset($profile_data['returns_pays']) && $profile_data['returns_pays'] == 'Buyer'}selected='selected'{/if}>{ci_language line="buyer"}</option>
                                                <option value="Seller" {if isset($profile_data['returns_pays']) && $profile_data['returns_pays'] == 'Seller'}selected='selected'{/if}>{ci_language line="seller"}</option>
                                            </select>
                                            {*<div class="col-md-12">
                                                <div class="help-block text-small">{ci_language line="return_pays_help"}</div>
                                            </div>*}
                                        </div>
                                        <div class="p-t10 p-l10 col-sm-2">{_get_tooltip_icon({ci_language line="return_pays_help"})}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group returns-main">
                                <label class="control-label">{ci_language line="holiday_return"} {_get_tooltip_icon({ci_language line="return_pays_help"})} <div class="help-block text-small"><a href="http://pages.ebay.com/sellerinformation/news/springupdate2014/holidayreturns.html" target="blank">{ci_language line="learn_more"}</a></div></label>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input id="holiday-return" name="form-holiday-return-checkbox" type="checkbox" value=""  {if isset($profile_data['holiday_return']) && $profile_data['holiday_return'] == 1}checked='checked'{/if}><span class="lbl">{ci_language line="holiday_return_label"}</span></label>
                                    </div>
                                    {*<div class="col-md-11">
                                        <div class="help-block text-small"><a href="http://pages.ebay.com/sellerinformation/news/springupdate2014/holidayreturns.html" target="blank">{ci_language line="learn_more"}</a></div>
                                    </div>*}
                                </div>
                            </div>
                            <div class="form-group returns-main">
                                <label class="control-label" for="returns-policy-type">{ci_language line="return_details"} {_get_tooltip_icon({ci_language line="return_information_help"})}</label>
                                <div class="col-md-12">
                                    {*<div class="help-block text-small">{ci_language line="return_information_help"}</div>*}
                                    <span class="col-md-12">
                                        <textarea id='returns-information' name='returns-information' class="returns-information" maxlength="1000">{if isset($profile_data['returns_information'])}{$profile_data['returns_information']}{/if}</textarea>
                                    </span>
                                    <div class="help-block red help-clear countdown-textarea">{ci_language line="remaining"} : 1000</div>
                                    <div class="help-block text-small">{ci_language line="return_instructions_max_help"}</div>
                                </div>
                            </div>
                        </div>
                    </div>
