                    {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
{*                    <div class="widget-head {$field_hide}">
                        <h3 class="header-main header-half">{ci_language line="title_$current_page"}</h3>
                    </div>*}
                    <!-- // Widget Heading END -->
                    <div class="widget-body">
{*                        <h5 class="title-main title-bottom title-half">{ci_language line="adjustment_price"}</h5>*}
                        {include file="ebay/main_info.tpl"}
                        <div class="widget-main">
                            <div class="price_rules">
                                <div class="form-group">
                                    <div class="col-md-12 form-group">
                                        <label class="control-label col-md-2"  >{ci_language line="title_price_modifier"} {_get_tooltip_icon({ci_language line="price_modifier_rules_help"})}</label>
                                        {*<div class="help-block text-small">{ci_language line="price_modifier_rules_help"}</div>*}
                                        <input type="hidden" rel="price_rule_count" value="{if !empty($rel_round)}{$rel_round}{else}0{/if}" />
                                        <ul class="list-regular list-unstyled padding-left-none">
                                            {counter name='m1' start = 0 skip = 1 assign = counter_count}
                                            {if !empty($price_modifier)}
                                                {foreach from=$price_modifier item = price}
                                                    <li class="price-rule-main clearfix price-rule-li" rel="{if isset($price['rel'])}{$price['rel']}{else}{$counter_count}{/if}">
                                                        <div class="col-xs-11 col-md-3 form-group">
                                                            <div class="col-xs-12 col-md-11">
                                                                <select rel="type" class="price_rule" name="price_rules[value][{$counter_count}][type]" id="price_rules_value_{$counter_count}_type" data-row="{$counter_count}" data-content="">
                                                                    <option value="percent" {if isset($price['price_type']) && $price['price_type'] == "percent"}selected{/if}>{ci_language line="percentage"}</option>
                                                                    <option value="value" {if isset($price['price_type']) && $price['price_type'] == "value"}selected{/if}>{ci_language line="value"}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon"><strong>{if isset({$currency_symbol})}{$currency_symbol}{/if}</strong></span>
                                                                </div>
                                                                <input rel="from" class="form-control price_rule" type="text" {if isset($price['price_from'])}value="{$price['price_from']}"{/if} name="price_rules[value][{$counter_count}][from]" id="price_rules_value_{$counter_count}_from" data-row="{$counter_count}" data-content=""> 
                                                            </div>
                                                            <div class="col-xs-0 col-md-2 text-center angle-right"><i class="fa fa-chevron-right exchange-item"></i></div>
                                                        </div>

                                                        <div class="col-xs-12 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon"><strong>{if isset({$currency_symbol})}{$currency_symbol}{/if}</strong></span>
                                                                </div>
                                                                <input rel="to" class="form-control price_rule" type="text" {if isset($price['price_to'])}value="{$price['price_to']}"{/if} name="price_rules[value][{$counter_count}][to]" id="price_rules_value_{$counter_count}_to" data-row="{$counter_count}" data-content=""> 
                                                            </div>
                                                            <div class="col-xs-0 col-md-2 text-center angle-right"><i class="fa fa-long-arrow-right exchange-item"></i></div>
                                                        </div>  

                                                        <div class="col-xs-12 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon"><strong>{ci_language line="percent"}</strong></span>
                                                                </div>
                                                                <input rel="value" class="form-control price_rule" type="text" {if isset($price['price_value'])}value="{$price['price_value']}"{/if} name="price_rules[value][{$counter_count}][value]" id="price_rules_value_{$counter_count}_value" data-row="{$counter_count}" data-content=""> 
                                                            </div>
                                                            {counter name='m1'}
                                                            <div class="col-xs-1 col-md-2 text-center">
                                                                {if count($price_modifier) == $counter_count}
                                                                <a class="icons-plus addPriceRule"><i class="fa fa-plus-square-o"></i></a>
                                                                <a class="icons-plus removePriceRule"><i class="fa fa-minus-square-o"></i></a>
                                                                {else}
                                                                <a class="icons-plus removePriceRule"><i class="fa fa-minus-square-o"></i></a>
                                                                <script>$("li.price-rule-main[rel='{$price['rel']}']").find('.removePriceRule').show()</script>
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    </li>
                                                {/foreach}
                                            {else}
                                                <li class="price-rule-main clearfix price-rule-li" rel="{$counter_count}">
                                                    <div class="clearfix">
                                                        <div class="col-xs-3 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-11">
                                                                <select rel="type" class="price_rule" name="price_rules[value][{$counter_count}][type]" id="price_rules_value_{$counter_count}_type" data-row="{$counter_count}" data-content="">
                                                                    <option value="percent" selected="">{ci_language line="percentage"}</option>
                                                                    <option value="value">{ci_language line="value"}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon">{if isset({$currency_symbol})}{$currency_symbol}{/if}</span>
                                                                </div>
                                                                <input rel="from" class="form-control price_rule" type="text" name="price_rules[value][{$counter_count}][from]" id="price_rules_value_{$counter_count}_from" data-row="{$counter_count}" data-content=""> 
                                                            </div>
                                                            <div class="col-xs-0 col-md-2 text-center angle-right"><i class="fa fa-chevron-right exchange-item"></i></div>
                                                        </div>
                                                        <div class="col-xs-3 col-md-3 form-group">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon">{if isset({$currency_symbol})}{$currency_symbol}{/if}</span>
                                                                </div>
                                                                <input rel="to" class="form-control price_rule" type="text" name="price_rules[value][{$counter_count}][to]" id="price_rules_value_{$counter_count}_to" data-row="{$counter_count}" data-content=""> 
                                                            </div>
                                                            <div class="col-xs-0 col-md-2 text-center angle-right"><i class="fa fa-long-arrow-right exchange-item"></i></div>
                                                        </div>  

                                                        <div class="col-xs-2 col-md-2 form-group">
                                                            <div class="col-xs-11 col-md-11">
                                                                <div class="input-group hide">
                                                                    <span class="input-group-addon">{ci_language line="percent"}</span>
                                                                </div>
                                                                <input rel="value" class="form-control price_rule" type="text" name="price_rules[value][{$counter_count}][value]" id="price_rules_value_{$counter_count}_value" data-row="{$counter_count}" data-content="" > 
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-1 col-md-1 text-center">
                                                            <a class="icons-plus addPriceRule"><i class="fa fa-plus-square-o"></i></a>
                                                            <a class="icons-plus removePriceRule"><i class="fa fa-minus-square-o"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            {/if}
                                            <li class="clearfix" rel="{$counter_count}">
                                                <div class="clearfix">
                                                    <div class="col-xs-3 col-md-3 form-group">
                                                    </div>
                                                    <div class="col-xs-3 col-md-3 form-group">
                                                        <div class="help-block text-small">{ci_language line="price_modifier_min_help"}</div>
                                                    </div>
                                                    <div class="col-xs-3 col-md-3 form-group">
                                                        <div class="help-block text-small">{ci_language line="price_modifier_max_help"}</div>
                                                    </div>  
                                                    <div class="col-xs-2 col-md-2 form-group">
                                                        <div class="help-block text-small">{ci_language line="price_modifier_value_help"}</div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>                           
                                    </div>
                                </div>
                                <div class="form-group price-rounding">
                                    <label class="control-label col-md-2" for="form-price-rounding">{ci_language line="rounding"} {_get_tooltip_icon({ci_language line="price_modifier_rounding_help"})}</label>
                                    <div class="col-md-10">
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="1" {if !empty($round_selected) && $round_selected['decimal'] == 1}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="one_digit"}</span>
                                            </label>
                                        </div>
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="2" {if !empty($round_selected) && $round_selected['decimal'] == 2}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="two_digit"}</span>
                                            </label>
                                        </div>
                                        <div class="checkbox col-md-12">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input name="form-price-rounding" type="radio" value="0" {if !empty($round_selected) && $round_selected['decimal'] == 0 || empty($round_selected)}checked="checked"{/if}>
                                                <span class="lbl"> {ci_language line="none"}</span>
                                            </label>
                                        </div>
                                        {*<div class="help-block text-small">{ci_language line="price_modifier_rounding_help"}</div>*}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>