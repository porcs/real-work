                    {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                    {if isset($postal_hide)}{$postal_hide = 'field-hide'}{else}{$postal_hide = ''}{/if}
                    <div class="widget-head {$field_hide}">
                        <h3 class="heading"><i class="fa fa-cog"></i> {ci_language line="title_$current_page"}</h3>
                    </div>
                    <!-- // Widget Heading END -->
                    <div class="widget-body {$postal_hide}">
                        <h5 class="innerAll half heading-buttons border-bottom">{ci_language line="payment_location"}</h5>
                        <div class="row-fluid">
                            <div class="form-group">
                                <label class="control-label col-md-2" for="form-postal-code">{ci_language line="postcode:"}</label>
                                <div class="col-md-10">
                                    <span class="span6 position-relative">
                                        <input class="span12 form-control" type="text" id="form-postal-code"  data-example="{if isset($code)}{$code['exam_code']}{/if}" data-code="{if isset($code)}{$code['code']}{/if}" name="form-postal-code" value="{if isset($postal_code) }{$postal_code}{/if}" placeholder="{ci_language line="postcode"}" required {if isset($postal_code_pattern) }{$postal_code_pattern}{/if}><div class="asterisk"><i class="icon-asterisk"></i></div></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group {$field_hide}">
                                <label class="control-label col-md-2" for="form-payment-checkbox">{ci_language line="merchant_credit_cards:"}</label>
                                <div class="col-md-10">
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="VisaMC" {if isset($VisaMC)}checked="checked"{/if}><span class="lbl"> {ci_language line="visa_mastercard"}</span></label>
                                    </div>
                                    {if $id_site == 0}
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="Discover" {if isset($Discover)}checked="checked"{/if}><span class="lbl"> {ci_language line="discover"}</span></label>
                                    </div>
                                    {/if}
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="AmEx" {if isset($AmEx)}checked="checked"{/if}><span class="lbl"> {ci_language line="american_express"}</span></label>
                                    </div>
                                </div>
                            </div>
                            {if $id_site == 0}
                            <div class="form-group {$field_hide}">
                                <label class="control-label col-md-2" for="form-payment-checkbox">{ci_language line="other_payment:"}</label>
                                <div class="col-md-10">
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="MOCC" {if isset($MOCC)}checked="checked"{/if}><span class="lbl"> {ci_language line="money_order"}</span></label>
                                    </div>
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="PersonalCheck" {if isset($PersonalCheck)}checked="checked"{/if}><span class="lbl"> {ci_language line="personal_check"}</span></label>
                                    </div>
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="CashOnPickup" {if isset($CashOnPickup)}checked="checked"{/if}><span class="lbl"> {ci_language line="pay_on_pickup"}</span></label>
                                    </div>
                                </div>
                            </div>
                            {/if}
                        </div>
                                
                        <h5 class="innerAll half heading-buttons border-bottom {$field_hide}">{ci_language line="returns_policy:"}</h5>
                        <div class="row-fluid {$field_hide}">
                            <div class="form-group">
                                <label class="control-label col-md-2" for="returns-policy-type">{ci_language line="returns_policy:"}</label>
                                <div class="col-md-10">
                                    <span class="input-icon input-icon-right">
                                        <select class="returns-policy-type" id='returns-policy-type' name='returns-policy-type'>
                                            <option value="ReturnsAccepted" {if isset($returns_policy) && $returns_policy == 'ReturnsAccepted'}selected='selected'{/if}>{ci_language line="returns_accepted"}</option>
                                            <option value="ReturnsNotAccepted" {if isset($returns_policy) && $returns_policy == 'ReturnsNotAccepted'}selected='selected'{/if}>{ci_language line="returns_not_accepted"}</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="returns-policy-type">{ci_language line="returns_within:"}</label>
                                <div class="col-md-10">
                                    <span class="input-icon input-icon-right">
                                        <select class="returns-policy-days" id='returns-policy-days' name='returns-policy-days'>
                                            {foreach from = $return item = return_within}
                                                <option value="{$return_within['within']}" {if isset($return_within['selected'])}selected='selected'{/if}>{ci_language line="{strtolower($return_within['within'])}"}</option>
                                            {/foreach}
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="returns-policy-type">{ci_language line="who_pays:"}</label>
                                <div class="col-md-10">
                                    <span class="input-icon input-icon-right">
                                        <select class="returns-policy-pays" id='returns-policy-pays' name='returns-policy-pays'>
                                            <option value="Buyer" {if isset($returns_pays) && $returns_pays == 'Buyer'}selected='selected'{/if}>{ci_language line="buyer"}</option>
                                            <option value="Seller" {if isset($returns_pays) && $returns_pays == 'Seller'}selected='selected'{/if}>{ci_language line="seller"}</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="returns-policy-type">{ci_language line="information:"}</label>
                                <div class="col-md-10">
                                    <span class="input-icon input-icon-right span6">
                                        <textarea id='returns-information' name='returns-information' class="autosize-transition returns-information span12">{if isset($returns_information)}{$returns_information}{/if}</textarea>
                                    </span>
                                </div>
                            </div>
                        </div>
                                            
                        <h5 class="innerAll half heading-buttons border-bottom {$field_hide}">{ci_language line="listing_duration"}</h5>
                        <div class="row-fluid {$field_hide}">
                                <div class="form-group">
                                    <label class="control-label col-md-2" for="listing-duration_days">{ci_language line="listing_duration:"}</label>
                                    <div class="col-md-10">
                                        <span class="input-icon input-icon-right">
                                            <select class="listing-duration_days" id='listing-duration_days' name='listing-duration_days'>
                                                {foreach from = $listing item = listing_duration}
                                                    <option value="{$listing_duration['duration']}" {if isset($listing_duration['selected'])}selected='selected'{/if}>{ci_language line="{strtolower($listing_duration['duration'])}"}</option>
                                                {/foreach}
                                            </select>
                                        </span>
                                    </div>
                                </div>
                        </div>
                                                
                        {if isset($feed_mode) && ($feed_mode == 2 || $feed_mode == 3)}
                        <h5 class="innerAll half heading-buttons border-bottom {$field_hide}">{ci_language line="other_option"}</h5>
                        <div class="row-fluid {$field_hide}">
                            <div class="form-group">
                                <label class="control-label col-md-2" for="listing-duration_days">{ci_language line="option_add_product:"}</label>
                                <div class="col-md-10">
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-image" type="checkbox" value="1" {if isset($no_image) && $no_image == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="noaction_noimage"}</span></label>
                                    </div>
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-discount" type="checkbox" value="1" {if isset($discount) && $discount == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="calculate_discount_price"}</span></label>
                                    </div>
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-sync" type="checkbox" value="1" {if isset($sync) && $sync == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="synchronize_with_products"}</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid {$field_hide}">
                            <div class="form-group">
                                <label class="control-label col-md-2" for="listing-duration_days">{ci_language line="option_orders_product:"}</label>
                                <div class="col-md-10">
                                    <div class="checkbox col-md-4">
                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-orders" type="checkbox" value="1" {if isset($just_orders) && $just_orders == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="import_orders"}</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/if}
                    </div>