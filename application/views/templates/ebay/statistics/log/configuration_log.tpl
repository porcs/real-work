                    
{*                        <div class="widget-head">
                            <h3 class="header-main header-half">{ci_language line="title_$current_page"}</h3>
                        </div>*}
                        <!-- // Widget Heading END -->
                        <div class="widget-body">                            
                                <!-- Table -->
                                <table class="dynamicTable colVis table" id="statistics" >
                                    <!-- Table heading -->
                                    <thead>
                                        <tr>
                                            <th rel='batch_id'>{ci_language line="batch_id"}</th>
                                            <th rel='source'>{ci_language line="source"}</th>
                                            <th rel='type' class='center'>{ci_language line="type"}</th>
                                            <th rel='total' class='center'>{ci_language line="total"}</th>
                                            <th rel='success' class='center'>{ci_language line="success"}</th>
                                            <th rel='warning' class='center'>{ci_language line="warning"}</th>
                                            <th rel='error' class='center'>{ci_language line="error"}</th>
                                            <th rel='date_add' class="center sorting_desc"> {ci_language line="export_date"}</th>
                                            <th rel='download' class="center"></th>
                                        </tr>
                                    </thead>
                                    <!-- // Table heading END -->
                                    <tbody>
                                        {if !empty($statistics)} {foreach from=$statistics key=id item=statistic}
                                        <tr>
                                            <td rel='batch_id'>{$statistic['batch_id']}</td>
                                            <td rel='source'>{if isset($shop_default)}{$shop_default}{/if}</td>
                                            <td rel='type' class="center">{$statistic['type']}</td>
                                            <td rel='total' class="center">{$statistic['total']}</td>
                                            <td rel='success' class="center">{$statistic['success']}</td>
                                            <td rel='warning' class="center">{$statistic['warning']}</td>
                                            <td rel='error' class="center">{$statistic['error']}</td>
                                            <td rel='date_add' class="center">{$statistic['date_add']|date_format:"%Y/%m/%d %T"}</td>
                                            <td rel='download' class="center">{if isset($statistic['batch_id'])}<a href='#' class='batch_id' data-batch-id='{$statistic['batch_id']}'></a>{/if}</td>
                                        </tr>

                                        {/foreach} {/if}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="batch_id"}" rel='batch_id'></th>
                                            <th><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="source"}" rel='source'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="type"}" rel='type'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="total"}" rel='total'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="success"}" rel='success'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="warning"}" rel='warning'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="error"}" rel='error'></th>
                                            <th class="center sorting_desc"><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="export_date"}" rel='date_add'></th>
                                            <th class="center"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <!-- // Table END -->
                        </div>