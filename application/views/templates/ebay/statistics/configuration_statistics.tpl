                    
{*                        <div class="widget-head">
                            <h3 class="header-main header-half">{ci_language line="title_$current_page"}</h3>
                        </div>*}
                        <!-- // Widget Heading END -->
                        <div class="widget-body">  
                            <div class="showColumns   custom-form display-none check_filter_cv">
                                <label class="cb-checkbox check_filter">
                                    <input type="checkbox" id="chk_ignore_inactive" checked="checked"/>
                                    {ci_language line="Ignore inactive & out of stock products"}
                                </label>
                            </div>
                                <!-- Table -->
                                <table class="dynamicTable colVis table" id="statistics" >
                                    <thead>
                                        <tr>
                                            <th class='center' rel='batch_id'>{ci_language line="batch_id"}</th>
                                            <th class='center' rel='id_product'>{ci_language line="product_id"}</th>
                                            <th class='center' rel='id_product'>{ci_language line="product_sku"}</th>
                                            <th class='center' rel='id_product'>{ci_language line="product_ean13"}</th>
                                            <th class='center' rel='response'>{ci_language line="response"}</th>
                                            <th class='center sorting_desc' rel='date_add'>{ci_language line="export_date"}</th>
                                            <th class='center' rel='type'>{ci_language line="type"}</th>
                                            <th rel='name'>{ci_language line="name"}</th>
                                            <th rel='message'>{ci_language line="message"}</th>
                                        </tr>
                                    </thead>
                                    <!-- // Table heading END -->
                                    <tbody>
                                        {if !empty($statistics)} {foreach from=$statistics key=id item=statistic}
                                        <tr>
                                            <td class='center' rel='batch_id'>{$statistic['batch_id']}</td>
                                            <td class='center' rel='id_product'>{$statistic['id_product']}</td>
                                            <td class='center' rel='id_product'>{$statistic['product_sku']}</td>
                                            <td class='center' rel='id_product'>{$statistic['product_ean13']}</td>
                                            <td class='center' rel='response'><span class="label {if $statistic['response'] == 'Warning'}label-warning{elseif $statistic['response'] == 'Success'}label-success{elseif $statistic['response'] == 'Error'}label-important{else}label-inverse{/if}">{$statistic['response']}</span></td>
                                            <td class='center' rel='date_add'>{$statistic['date_add']|date_format:"%Y/%m/%d %T"}</td>
                                            <td class='center' rel='type'>{$statistic['type']}</td>
                                            <td rel='name'>{$statistic['name_product']}</td>
                                            <td class='center' rel='message'>{$statistic['message']}</td>
                                        </tr>
                                        {/foreach} {/if}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="batch_id"}" rel='batch_id'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_id"}" rel='id_product'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_id"}" rel='product_sku'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="product_id"}" rel='product_ean13'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="response"}" rel='response'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="export_date"}" rel='date_add'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="type"}" rel='type'></th>
                                            <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="name"}" rel='name'></th>
                                            <th class="center sorting_desc"><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="message"}" rel='message'></th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <!-- // Table END -->
                        </div>