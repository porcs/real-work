        {if !empty($popup_config) && isset($popup_config) } 
                <ul class="wizard-steps wizard-style"> 
                        {counter start = 1 skip = 1 assign = popup_step}
                        {if !empty($popup_next) }{foreach from = $popup_next key = page item = value }
                        <li {if $popup_config >= $popup_step} class="active"{/if}><span class="step">{$popup_step}</span><span class="title">{ci_language line="$page"}</span></li>
                        {counter}
                        {/foreach}{/if}
                        <li {if $popup_config >= $popup_step} class="active"{/if}><span class="step">{$popup_step}</span><span class="title">{ci_language line="wizard_finish"}</span></li> 
                </ul>
                <div style="clear: both"></div>
        {/if}