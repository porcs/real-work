
                    <!-- // Widget Heading END -->
{*                    <div class="widget-head">
                        <h3 class="header-main header-full">{ci_language line="create_your_listing"}</h3>
                    </div>*}
                    <div class="widget-body">
                        <h5 class="title-main title-bottom title-half">{ci_language line="profile_listing_config"}</h5>
                        <div class="widget-main">
                            <div class="form-group config-main {if $hidden_data['id_profile'] == 0}field-hide{/if}">
                                <label class="control-label" for="profile-name">{ci_language line="profile_name"}</label>
                                <div class="col-md-12">
                                    <div class="col-md-6 position-relative">
                                        <div class="col-md-11">
                                            <input class="form-control" type="text" id="profile-name" name="profile-name" value="{if isset($profile_data['profile_name'])}{$profile_data['profile_name']}{/if}" placeholder="{ci_language line="profile_name"}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 class="title-main title-bottom title-half {if $hidden_data['id_profile'] == 0}field-hide{/if}">{ci_language line="profile_category_config"}</h5>
                            <div class="form-group {if $hidden_data['id_profile'] == 0}field-hide{/if}">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="primary_categories"}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-12">
                                            <input readonly class="form-control" type="text" id="primary-categories" name="primary-categories" value="{if !empty($profile_data['ebay_category_name'])}{$profile_data['ebay_category_name']}{/if}" placeholder="{ci_language line="primary_categories"}">
                                                <div class="col-md-12 categories-margin">
                                                    <input readonly class="form-control" type="text" id="id-ebay-categories" name="id-ebay-categories" value="{if !empty($profile_data['id_ebay_category'])}{$profile_data['id_ebay_category']}{/if}" placeholder="{ci_language line="id_ebay_categories_only"}">
                                                    <button class="btn btn-primary btn-primary-categories" type="button" data-last="finish">
                                                        {ci_language line="suggestions_only"}
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="help-block text-small">{ci_language line="primary_categories_help"}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="primary_store_categories"}</label>
                                        <div class="col-md-11">
                                            <input readonly class="form-control" rel="category_setup" type="text" id="primary-store-categories" name="primary-store-categories" value="{if !empty($profile_data['store_category_name'])}{$profile_data['store_category_name']}{/if}" placeholder="{ci_language line="primary_store_categories"}">
                                            <div class="col-md-12 categories-margin">
                                                <div class="col-md-12">
                                                    <input readonly class="form-control" type="text" id="id-ebay-store-categories" name="id-ebay-store-categories" value="{if !empty($profile_data['id_store_category'])}{$profile_data['id_store_category']}{/if}" placeholder="{ci_language line="id_ebay_categories_only"}">
                                                    <span>
                                                        <button class="btn btn-primary btn-primary-store-categories" type="button" data-last="finish">
                                                            {ci_language line="suggestions_only"}
                                                        </button>
                                                    </span>
                                                    <span class="get-store">
                                                        <button class="btn btn-success btn-get-store" type="button" data-last="finish">
                                                            {ci_language line="get_store"}
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="help-block text-small">{ci_language line="store_category_help"}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main {if $hidden_data['id_profile'] == 0}field-hide{/if}">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="secondary_categories"}</label>
                                        <div class="col-md-11">
                                            <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-secondary-categories-checkbox" type="checkbox" value="" {if !empty($profile_data['id_secondary_category'])}checked="checked"{/if}><span class="lbl"> {ci_language line="second_category_warning_help"}</span></label>
                                            <div class="clear clearfix style-hide">
                                                <span class="position-relative clearspan">
                                                    <input readonly class="form-control" rel="category_setup" type="text" id="secondary-categories" name="secondary-categories" value="{if !empty($profile_data['secondary_category_name'])}{$profile_data['secondary_category_name']}{/if}" placeholder="{ci_language line="secondary_categories"}">
                                                    <div class="col-md-12 categories-margin">
                                                        <input readonly class="form-control" type="text" id="id-ebay-secondary-categories" name="id-ebay-secondary-categories" value="{if !empty($profile_data['id_secondary_category'])}{$profile_data['id_secondary_category']}{/if}" placeholder="{ci_language line="id_ebay_categories_only"}">
                                                        <button class="btn btn-primary btn-secondary-categories" type="button" data-last="finish">
                                                            {ci_language line="suggestions_only"}
                                                        </button>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="help-block text-small">{ci_language line="second_category_help"}</div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="secondary_store_categories"}</label>
                                        <div class="col-md-11">
                                            <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-secondary-store-categories-checkbox" type="checkbox" value="" {if !empty($profile_data['id_secondary_store_category'])}checked="checked"{/if}><span class="lbl"> {ci_language line="store_category_warning_help"}</span></label>
                                            <div class="clear clearfix style-hide">
                                                <span class="position-relative clearspan">
                                                    <input readonly class="form-control" rel="category_setup" type="text" id="secondary-store-categories" name="secondary-store-categories" value="{if !empty($profile_data['secondary_store_category_name'])}{$profile_data['secondary_store_category_name']}{/if}" placeholder="{ci_language line="secondary_store_categories"}">
                                                    <div class="col-md-12 categories-margin">
                                                        <div class="col-md-12">
                                                            <input readonly class="form-control" type="text" id="id-ebay-secondary-store-categories" name="id-ebay-secondary-store-categories" value="{if !empty($profile_data['id_secondary_store_category'])}{$profile_data['id_secondary_store_category']}{/if}" placeholder="{ci_language line="id_ebay_categories_only"}">
                                                            <button class="btn btn-primary btn-secondary-store-categories" type="button" data-last="finish">
                                                                {ci_language line="suggestions_only"}
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="help-block text-small">{ci_language line="store_secondary_category_help"}</div>
                                                </div>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="out-of-stock">{ci_language line="out_of_stock"} {_get_tooltip_icon({ci_language line="out_of_stock_help"})}</label>
                                        <div class="col-md-11">
                                            <input class="form-control" type="text" id="out-of-stock" name="out-of-stock" value="{if isset($profile_data['out_of_stock_min'])}{$profile_data['out_of_stock_min']}{else}0{/if}" placeholder="{ci_language line="out_of_stock"}">
                                            {*<div class="help-block text-small">{ci_language line="out_of_stock_help"}</div>*}
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="maximum-quantity">{ci_language line="maximum_quantity"} {_get_tooltip_icon({ci_language line="maximum_quantity_help"})}</label>
                                        <div class="col-md-11">
                                            <input class="form-control" type="text" id="maximum-quantity" name="maximum-quantity" value="{if isset($profile_data['maximum_quantity'])}{$profile_data['maximum_quantity']}{else}1000{/if}" placeholder="{ci_language line="maximum_quantity"}">
                                            {*<div class="help-block text-small">{ci_language line="maximum_quantity_help"}</div>*}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main hide">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="title_format"}</label>
                                        <div class="col-md-10">
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input rel="title-format" class="title-format" name="title-format" type="radio" value="0" {if isset($profile_data['title_format']) && $profile_data['title_format'] == 0}checked='checked'{elseif !isset($profile_data['title_format'])}checked='checked'{/if}>
                                                <span class="lbl"> {ci_language line="standard_title"}</span>
                                            </label>
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input rel="title-format" class="title-format" name="title-format" type="radio" value="1" {if isset($profile_data['title_format']) && $profile_data['title_format'] == 1}checked='checked'{/if}>
                                                <span class="lbl"> {ci_language line="manufacturer_title"}</span>
                                            </label>
                                            <label class="col-md-12 col-xs-12 radio radio-custom">
                                                <i class="fa fa-fw fa-square-o"></i>
                                                <input rel="title-format" class="title-format" name="title-format" type="radio" value="2" {if isset($profile_data['title_format']) && $profile_data['title_format'] == 2}checked='checked'{/if}>
                                                <span class="lbl"> {ci_language line="manufacturer_title_reference"}</span>
                                            </label>
                                            <div class="help-block text-small">
                                                {ci_language line="title_format_help"}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="identifier-with-items">{ci_language line="identifier_with_items"}</label>
                                        <div class="col-md-11">
                                            <select class="identifier-with-items" name="identifier-with-items" id="identifier-with-items">
                                                <option value="ItemID" {if !empty($profile_data['track_inventory']) && $profile_data['track_inventory'] == 'ItemID'}checked='selected'{/if}>{ci_language line="item_id_only"}</option>
                                                <option value="SKU" {if !empty($profile_data['track_inventory']) && $profile_data['track_inventory'] == 'SKU'}selected='selected'{/if}>{ci_language line="sku"}</option>
                                            </select>
                                            <div class="help-block text-small">{ci_language line="sku_help"}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="auto_pay"} {_get_tooltip_icon({ci_language line="auto_pay_help"})}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-12">
                                                <select class="auto-pay" name="auto-pay" id="auto-pay">
                                                    <option value="0" {if isset($profile_data['auto_pay']) && $profile_data['auto_pay'] == 0}selected='selected'{/if}>{ci_language line="close_only"}</option>
                                                    <option value="1" {if isset($profile_data['auto_pay']) && $profile_data['auto_pay'] == 1}selected='selected'{/if}>{ci_language line="open_only"}</option>
                                                </select>
                                                {*<div class="help-block text-small">{ci_language line="auto_pay_help"}<br></div>*}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="visitor_counter"} {_get_tooltip_icon({ci_language line="visitor_counter_help"})}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-12">
                                                <select class="visitor-counter" name="visitor-counter" id="visitor-counter">
                                                    {foreach from = $visitor_counter item = visitor_data}
                                                        <option value="{$visitor_data['visitor_value']}" {if isset($visitor_data['selected'])}selected='selected'{/if}>{$visitor_data['visitor_name']}</option>
                                                    {/foreach}
                                                </select>
                                                {*<div class="help-block text-small">{ci_language line="visitor_counter_help"}</div>*}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="country-name">{ci_language line="country_name"} {_get_tooltip_icon({ci_language line="country_name_help"})}</label>
                                        <div class="col-md-11">
                                            <input class="form-control" type="text" id="country-name" name="country-name" value="{if isset($country)}{$country}{/if}" readonly placeholder="{ci_language line="country_name"}">
                                            {*<div class="help-block text-small">{ci_language line="country_name_help"}<br></div>*}
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="currency-only">{ci_language line="currency_only"} {_get_tooltip_icon({ci_language line="currency_help"})}</label>
                                        <div class="col-md-11">
                                            <input class="form-control" type="text" id="currency-only" data-value='{$currency_iso}' name="currency-only" value="{if isset($currency_name)}{$currency_name} ({$currency_iso}){/if}" readonly placeholder="{ci_language line="currency_only"}">
                                            {*<div class="help-block text-small">{ci_language line="currency_help"}<br></div>*}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group config-main">
                                <div class="col-md-12">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="listing-duration-days">{ci_language line="listing_duration"} {_get_tooltip_icon({ci_language line="listing_duration_help"})}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-12">
                                                <select class="listing-duration-days" id='listing-duration-days' name='listing-duration-days'>
                                                    {foreach from = $listing item = listing_duration}
                                                        <option value="{$listing_duration['duration']}" {if isset($listing_duration['selected'])}selected='selected'{/if}>{ci_language line="{strtolower($listing_duration['duration'])}"}</option>
                                                    {/foreach}
                                                </select>
                                                {*<div class="help-block text-small">{ci_language line="listing_duration_help"}</div>*}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label">{ci_language line="gallery_plus"} {_get_tooltip_icon({ci_language line="gallery_plus_help"})}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-12">
                                                <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input id="gallery-plus" name="form-gallery-plus-checkbox" type="checkbox" value=""  {if isset($profile_data['gallery_plus']) && $profile_data['gallery_plus'] == 1}checked='checked'{/if}><span class="lbl"> {ci_language line="gallery_plus_warning_help"}</span></label>
                                                {*<div class="help-block text-small">{ci_language line="gallery_plus_help"}</div>*}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {if isset($feed_mode) && ($feed_mode == 2 || $feed_mode == 3)}
                            <h5 class="title-main title-bottom title-half {if !empty($wizard_open)}field-hide{/if}">{ci_language line="other_option"}</h5>
                            <div class="widget-main {if !empty($wizard_open)}field-hide{/if}">
                                <div class="form-group config-main">
                                    <div class="col-md-12 form-group">
                                        <label class="control-label" for="other-option">{ci_language line="option_add_product"}</label>
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-image" type="checkbox" value="1" {if isset($no_image) && $no_image == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="noaction_noimage"}</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-out-of-stock-created" type="checkbox" value="1" {if isset($profile_data['out_of_stock_created']) && $profile_data['out_of_stock_created'] == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="out_of_stock_created"}</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group config-main">
                                    <div class="col-md-12 form-group">
                                        <label class="control-label" for="other-option">{ci_language line="option_orders_product"}</label>
                                        <div class="col-md-11">
                                            <div class="col-md-10">
                                                <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-product-configuration-orders" type="checkbox" value="1" {if isset($just_orders) && $just_orders == 1}checked="checked"{/if}><span class="lbl"> {ci_language line="import_orders"}</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/if}         
                        </div>
                    </div>