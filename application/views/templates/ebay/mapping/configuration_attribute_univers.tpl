                    
                        <div class="row">
                                <div class="col-xs-12">
                                        <div class="headSettings clearfix b-Bottom b-Top-None p-0 p-b1">
                                                <h4 class="headSettings_head pull-sm-left p-t10">{ci_language line="title_$current_page"}</h4>										
                                                <ul class="pull-sm-right clearfix treeSettings">						
                                                        <li class="checkedAll active"><i class="cb-checked"></i>{ci_language line="check_all"}</li>
                                                        <li class="uncheckedAll"><i class="cb-unchecked"></i>{ci_language line="uncheck_all"}</li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <!-- // Widget Heading END -->
                        <div class="widget-body">    
                            {include file="ebay/main_info.tpl"}
                            <div class="row custom-form">
                                <div class="col-xs-12">
                                    <div class="tree">
                                        <div class="form-group" id="univers_list">
{*                                            {foreach from = $root key = 'key' item = 'categories'}
                                            {if !empty($categories)}*}
                                            <div class="univers_list_group widget-main">
                                                    {counter start=0 skip=1 assign=counter_l}
                                                    <ul class="left col-lg-12 col-md-12 col-xs-8 col-sm-6">
                                                        {if isset($root) && empty($select_root)}
                                                        {foreach from = $root item = 'category'}
                                                        {counter}
                                                        <li class='col-md-4'>
                                                            <label class="cb-checkbox w-100" for="c{$counter_l}">
                                                            <input name="form-checkbox" type="checkbox" id="c{$counter_l}" class='status' value="{$category['id_attribute_group']}" title="{$category['name']}" ><span class="lbl"></span>
                                                            <div class='univer-string'>{$category['name']}</div></label>
                                                        </li>
                                                        {/foreach}
                                                        {elseif isset($root) && !empty($select_root)}
                                                        {foreach from = $root item = 'category'}
                                                        {counter}
                                                        <li class='col-md-4'>
                                                            <label class="cb-checkbox w-100" for="c{$counter_l}">
                                                            <input name="form-checkbox" type="checkbox" id="c{$counter_l}" class='status' value="{$category['id_attribute_group']}" title="{$category['name']}" {foreach from = $select_root item = 'selected'}{if $category['id_attribute_group'] == $selected['id_group']}checked='checked'{/if}{/foreach}><span class="lbl"></span>
                                                            <div class='univer-string'>{$category['name']}</div></label>
                                                        </li>
                                                        {/foreach}   
                                                        {/if}
                                                    </ul>
                                                    <div class='clearfix'></div>
                                            </div>
{*                                            {/if}
                                            {/foreach}*}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>