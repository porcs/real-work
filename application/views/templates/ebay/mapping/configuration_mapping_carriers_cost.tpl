                <!-- // Widget Heading END -->
                <div class="widget-body padding-none">
                    {include file="ebay/main_info.tpl"}
                    <div class="profile-shipping">
                        <h5 class="title-main title-bottom title-half">{ci_language line="shipping_rules"} {_get_tooltip_icon({ci_language line="shipping_rules_help"})}</h5>
                        <div class="widget-main">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-5 form-group dispatch-main">
                                        <div class="col-md-10">
                                            <label class="col-md-12 col-xs-12 radio radio-custom"><i class="fa fa-fw fa-square-o"></i><input class="carrier-configuration" name="carrier-configuration" type="radio" value="2" {if isset($carrier_conf) && ($carrier_conf == '2')}{'checked'}{/if}> <span class="lbl"> {ci_language line="weight"}</span></label>
                                            <label class="col-md-12 col-xs-12 radio radio-custom"><i class="fa fa-fw fa-square-o"></i><input class="carrier-configuration" name="carrier-configuration" type="radio" value="4" {if isset($carrier_conf) && ($carrier_conf == '4')}{'checked'}{/if}> <span class="lbl"> {ci_language line="price"}</span></label>
                                            <label class="col-md-12 col-xs-12 radio radio-custom"><i class="fa fa-fw fa-square-o"></i><input class="carrier-configuration" name="carrier-configuration" type="radio" value="3" {if isset($carrier_conf) && ($carrier_conf == '3')}{'checked'}{/if}> <span class="lbl"> {ci_language line="item"}</span></label>
                                        </div>
                                       {* <div class="col-md-10">
                                            <div class="help-block text-small">{ci_language line="shipping_rules_help"}</div>
                                        </div>*}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="collapse-weight" class="collapse-swap {if !empty($carrier_conf) && $carrier_conf != 2 }field-hide{/if}" rel="weight">
                        <div class="widget-main padding-none rules-main">
                            <div class="transparent padding-none">
                            <h5 class="title-main title-bottom title-half">{ci_language line="rules_weight"}</h5>
                                {if !empty($getDomestic) }{foreach from = $getDomestic key = domestic_key item = domestic }
                                <div class="rules-body row-fluid" rel='{$domestic_key}'>
                                    <div class="rules-carriers-main">
                                        <div class="form-group rules-carriers-body">
                                            <label class="control-label rules-label">{ci_language line="all"} </label>
                                            <div class="col-md-12">
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="from" type="text" min="0" name="min-weight" class="rules-min-weight form-control" placeholder="{ci_language line="weight_kg"}"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="to" type="text" min="0" name="max-weight" class="rules-max-weight form-control" placeholder="{ci_language line="weight_kg"}"></div></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost" class="rules-main-cost form-control" placeholder="{ci_language line="cost"} ({$current})"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals" class="rules-additionals form-control" placeholder="{ci_language line="additional"} ({$current})"></div></div>
                                                    </div>   
                                                </div>   
                                                <div class="col-md-2"><a class="icons-plus {if !empty($domestic_key) && $domestic_key == 1}addrulecost{else}removerulecost{/if}"><i class="fa {if !empty($domestic_key) && $domestic_key == 1}fa-plus-square-o{else}fa-minus-square-o{/if}"></i></a></div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    {if !empty($domestic) && !array_key_exists('id_ebay_shipping', $domestic)}{foreach from = $domestic item = domestics }
                                    <div class="form-group rules-carriers-body">
                                        <label class="control-label rules-label">{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}{if !empty($domestics['country_service'])}{' ( '|cat:$domestics['country_service']|cat:' ) '}{/if}</label>
                                        <div class="col-md-12 rules-carriers-real" data-service="{$domestics['id_shipping']}" data-ebay-service="{$domestics['id_ebay_shipping']}" data-internation="{$domestics['is_international']}" data-country="{$domestics['country_service']}">
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="from" type="text" min="0" name="min-weight-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-min-weight form-control" placeholder="{if !empty($domestics['min_weight'])}{$domestics['min_weight']}{else}{ci_language line="weight_kg"}{/if}" value="{if isset($domestics['min_weight']) && is_numeric($domestics['min_weight'])}{$domestics['min_weight']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="to" type="text" min="0" name="max-weight-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-max-weight form-control" placeholder="{if !empty($domestics['max_weight'])}{$domestics['max_weight']}{else}{ci_language line="weight_kg"}{/if}" value="{if isset($domestics['max_weight']) && is_numeric($domestics['max_weight'])}{$domestics['max_weight']}{/if}"></div></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-main-cost form-control" placeholder="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-additionals form-control" placeholder="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{/if}"></div></div>
                                                </div>   
                                            </div>   
                                        </div>
                                        <hr>
                                    </div>
                                    {/foreach}
                                    {else}
                                    <div class="form-group rules-carriers-body">
                                        <label class="control-label rules-label">{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}{if !empty($domestic['country_service'])}{' ( '|cat:$domestic['country_service']|cat:' ) '}{/if}</label>
                                        <div class="col-md-12 rules-carriers-real" data-service="{$domestic['id_shipping']}" data-ebay-service="{$domestic['id_ebay_shipping']}" data-internation="{$domestic['is_international']}" data-country="{$domestic['country_service']}">
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="from" type="text" min="0" name="min-weight-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-min-weight form-control" placeholder="{if !empty($domestic['min_weight'])}{$domestic['min_weight']}{else}{ci_language line="weight_kg"}{/if}" value="{if !empty($domestic['min_weight'])}{$domestic['min_weight']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><i class="fa fa-calculator"></i></span><input rel="to" type="text" min="0" name="max-weight-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-max-weight form-control" placeholder="{if !empty($domestic['max_weight'])}{$domestic['max_weight']}{else}{ci_language line="weight_kg"}{/if}" value="{if !empty($domestic['max_weight'])}{$domestic['max_weight']}{/if}"></div></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-main-cost form-control" placeholder="{if !empty($domestic['shipping_cost'])}{$domestic['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestic['shipping_cost'])}{$domestic['shipping_cost']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-weight-{$domestic_key}" class="rules-additionals form-control" placeholder="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{/if}"></div></div>
                                                </div>   
                                            </div>   
                                        </div>
                                        <hr>
                                    </div>
                                    {/if}
                                </div>
                                {/foreach}{/if}
                            </div>
                            <div class="carriers-help" rel='99'>
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="col-xs-11 col-md-5">
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="weight_min_help"}</div></div>
                                            </div>
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="weight_max_help"}</div></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-11 col-md-5">
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="cost_help"}</div></div>
                                            </div>
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="additional_help"}</div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="collapse-price" class="collapse-swap {if !empty($carrier_conf) && $carrier_conf != 4 }field-hide{/if}" rel="prices">
                        <div class="widget-main rules-main padding-none">
                            <div class="transparent">
                                <h5 class="title-main title-bottom title-half">{ci_language line="rules_price"}</h5>
                                {if !empty($getDomestic) }{foreach from = $getDomestic key = domestic_key item = domestic }
                                <div class="rules-body row-fluid" rel='{$domestic_key}'>
                                    <div class="rules-carriers-main">
                                        <div class="form-group rules-carriers-body">
                                            <label class="control-label rules-label">{ci_language line="all"} </label>
                                            <div class="col-md-12">
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="from" type="text" min="0" name="min-price" class="rules-min-price form-control" placeholder="{ci_language line="price_min"}"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="to" type="text" min="0" name="max-price" class="rules-max-price form-control" placeholder="{ci_language line="price_max"}"></div></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost" class="rules-main-cost form-control" placeholder="{ci_language line="cost"} ({$current})"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals" class="rules-additionals form-control" placeholder="{ci_language line="additional"} ({$current})"></div></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2"><a class="icons-plus {if !empty($domestic_key) && $domestic_key == 1}addrulecost{else}removerulecost{/if}"><i class="fa {if !empty($domestic_key) && $domestic_key == 1}fa-plus-square-o{else}fa-minus-square-o{/if}"></i></a></div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    {if !empty($domestic) && !array_key_exists('id_ebay_shipping', $domestic)}{foreach from = $domestic item = domestics }
                                    <div class="form-group rules-carriers-body">
                                        <label class="control-label rules-label">{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}{if !empty($domestics['country_service'])}{' ( '|cat:$domestics['country_service']|cat:' ) '}{/if}</label>
                                        <div class="col-md-12 rules-carriers-real" data-service="{$domestics['id_shipping']}" data-ebay-service="{$domestics['id_ebay_shipping']}" data-internation="{$domestics['is_international']}" data-country="{$domestics['country_service']}">
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="from" type="text" min="0" name="min-price-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-min-price form-control" placeholder="{ci_language line="price_min"}" value="{if isset($domestics['min_price']) && is_numeric($domestics['min_price'])}{$domestics['min_price']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="to" type="text" min="0" name="max-price-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-max-price form-control" placeholder="{ci_language line="price_max"}" value="{if isset($domestics['max_price']) && is_numeric($domestics['max_price'])}{$domestics['max_price']}{/if}"></div></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-main-cost form-control" placeholder="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-additionals form-control" placeholder="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{/if}"></div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    {/foreach}
                                    {else}
                                    <div class="form-group rules-carriers-body">
                                        <label class="control-label rules-label">{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}{if !empty($domestic['country_service'])}{' ( '|cat:$domestic['country_service']|cat:' ) '}{/if}</label>
                                        <div class="col-md-12 rules-carriers-real" data-service="{$domestic['id_shipping']}" data-ebay-service="{$domestic['id_ebay_shipping']}" data-internation="{$domestic['is_international']}" data-country="{$domestic['country_service']}">
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="from" type="text" min="0" name="min-price-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-min-price form-control" placeholder="{ci_language line="price_min"}" value="{if isset($domestic['min_price']) && is_numeric($domestic['min_price'])}{$domestic['min_price']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input rel="to" type="text" min="0" name="max-price-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-max-price form-control" placeholder="{ci_language line="price_max"}" value="{if isset($domestic['max_price']) && is_numeric($domestic['max_price'])}{$domestic['max_price']}{/if}"></div></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-main-cost form-control" placeholder="{if !empty($domestic['shipping_cost'])}{$domestic['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestic['shipping_cost'])}{$domestic['shipping_cost']}{/if}"></div></div>
                                                </div>
                                                <div class="col-xs-11 col-md-6">
                                                    <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-prices-{$domestic_key}" class="rules-additionals form-control" placeholder="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{/if}"></div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    {/if}
                                </div>
                                {/foreach}{/if}
                            </div>
                            <div class="carriers-help" rel='99'>
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="col-xs-11 col-md-5">
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="price_min_help"}</div></div>
                                            </div>
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="price_max_help"}</div></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-11 col-md-5">
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="cost_help"}</div></div>
                                            </div>
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="additional_help"}</div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="collapse-item" class="collapse-swap {if !empty($carrier_conf) && $carrier_conf != 3 }field-hide{/if}" rel="items">
                        <div class="widget-main rules-main padding-none">
                            <div class="transparent">
                                <h5 class="title-main title-bottom title-half">{ci_language line="rules_item"}</h5>
                                {if !empty($getDomestic) }{foreach from = $getDomestic key = domestic_key item = domestic }
                                <div class="rules-body" rel='{$domestic_key}'>
                                    <div class="rules-carriers-main">
                                        <div class="form-group rules-carriers-body">
                                            <label class="control-label rules-label">{ci_language line="all"} </label>
                                            <div class="col-md-12">
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost" class="rules-main-cost form-control" placeholder="{ci_language line="cost"} ({$current})"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals" class="rules-additionals form-control" placeholder="{ci_language line="additional"} ({$current})"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    {if !empty($domestic) && !array_key_exists('id_ebay_shipping', $domestic)}{foreach from = $domestic item = domestics }
                                        <div class="form-group rules-carriers-body">
                                            <label class="control-label rules-label">{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}{if !empty($domestics['country_service'])}{' ( '|cat:$domestics['country_service']|cat:' ) '}{/if}</label>
                                            <div class="col-md-12 rules-carriers-real" data-service="{$domestics['id_shipping']}" data-ebay-service="{$domestics['id_ebay_shipping']}" data-internation="{$domestics['is_international']}" data-country="{$domestics['country_service']}">
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-items-1" class="rules-main-cost form-control" placeholder="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{/if}"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestics['name_shipping']|cat:' - '|cat:$domestics['name_ebay_shipping']}-items-1" class="rules-additionals form-control" placeholder="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{/if}"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    {/foreach}
                                    {else}
                                        <div class="form-group rules-carriers-body">
                                            <label class="control-label rules-label">{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}{if !empty($domestic['country_service'])}{' ( '|cat:$domestic['country_service']|cat:' ) '}{/if}</label>
                                            <div class="col-md-12 rules-carriers-real" data-service="{$domestic['id_shipping']}" data-ebay-service="{$domestic['id_ebay_shipping']}" data-internation="{$domestic['is_international']}" data-country="{$domestic['country_service']}">
                                                <div class="col-xs-11 col-md-5">
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="cost-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-items-1" class="rules-main-cost form-control" placeholder="{if !empty($domestic['shipping_cost'])}{$domestic['shipping_cost']}{else}{ci_language line="cost"} ({$current}){/if}" value="{if !empty($domestics['shipping_cost'])}{$domestics['shipping_cost']}{/if}"></div></div>
                                                    </div>
                                                    <div class="col-xs-11 col-md-6">
                                                        <div class="col-md-11"><div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span><input type="text" min="0" name="additionals-{$domestic['name_shipping']|cat:' - '|cat:$domestic['name_ebay_shipping']}-items-1" class="rules-additionals form-control" placeholder="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{else}{ci_language line="additional"} ({$current}){/if}" value="{if isset($domestics['shipping_additionals'])}{$domestics['shipping_additionals']}{/if}"></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    {/if}
                                </div>
                                {/foreach}{/if}
                            </div>
                            <div class="carriers-help" rel='99'>
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="col-xs-11 col-md-5">
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="cost_help"}</div></div>
                                            </div>
                                            <div class="col-xs-11 col-md-6">
                                                <div class="col-md-11"><div class="help-block text-small">{ci_language line="additional_help"}</div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    {literal}
                        var lg_current   = [{/literal}{$currency_code}{literal}];
                        //var choose_value = {/literal}{if isset($data_output)}{$data_output}{else}[]{/if}{literal};
                    {/literal}  
                </script>