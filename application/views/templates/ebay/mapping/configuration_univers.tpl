                    
                        <div class="row">
                                <div class="col-xs-12">
                                        <div class="headSettings clearfix b-Bottom b-Top-None p-0 p-b1">
                                                <h4 class="headSettings_head pull-sm-left p-t10">{ci_language line="title_$current_page"}</h4>										
                                                <ul class="pull-sm-right clearfix treeSettings">						
                                                        <li class="checkedAll active"><i class="cb-checked"></i>{ci_language line="check_all"}</li>
                                                        <li class="uncheckedAll"><i class="cb-unchecked"></i>{ci_language line="uncheck_all"}</li>
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        <!-- // Widget Heading END -->
                        <div class="widget-body">    
{*                            <h5 class="title-main title-bottom title-half">{ci_language line="ebay_root_category"}</h5>*}
                            {include file="ebay/main_info.tpl"}
                            <div class="row custom-form">
                                <div class="">
                                    <div class="universe">
                                        <div class="form-group" id="univers_list">
                                        {counter start=0 skip=1 assign=counter_l} 
                                            <ul class="left col-lg-4 col-md-5 col-xs-8 col-sm-6">
                                                {if isset($root) && empty($select_root)}
                                                {foreach from = $root item = 'category'}
                                                {counter}
                                                {if $counter_l % 2 ==0} {continue} {/if}
                                                <li>
                                                    <label class="cb-checkbox w-100" for="c{$counter_l}">
                                                    <input name="form-checkbox" type="checkbox" id="c{$counter_l}" class='status' value="{$category->id_category}" title="{$category->name}" ><span class="lbl"></span>
                                                    <div>{$category->name}</div></label>
                                                </li>
                                                {/foreach}
                                                {elseif isset($root) && !empty($select_root)}
                                                {foreach from = $root item = 'category'}
                                                {counter}
                                                {if $counter_l % 2 ==0} {continue} {/if}
                                                <li>
                                                    <label class="cb-checkbox w-100" for="c{$counter_l}">
                                                    <input name="form-checkbox" type="checkbox" id="c{$counter_l}" class='status' value="{$category->id_category}"  title="{$category->name}" {foreach from = $select_root item = 'selected'}{if $category->id_category == $selected['id_ebay_category']}checked='checked'{/if}{/foreach}><span class="lbl"></span>
                                                    <div>{$category->name}</div></label>
                                                </li>
                                                {/foreach}   
                                                {/if}
                                            </ul>
                                            {counter start=0 skip=1 assign=counter_r}
                                            <ul class="right col-lg-4 col-md-5 col-xs-8 col-sm-6">
                                                {if isset($root) && empty($select_root)}
                                                {foreach from = $root item = 'category'}
                                                {counter}
                                                {if $counter_r % 2 ==1} {continue} {/if}
                                                <li>
                                                    <label class="cb-checkbox w-100" for="c{$counter_r}">
                                                    <input name="form-checkbox" type="checkbox" id="c{$counter_r}" class='status' value="{$category->id_category}" title="{$category->name}" ><span class="lbl"></span>
                                                    <div>{$category->name}</div></label>
                                                </li>
                                                {/foreach}
                                                {elseif isset($root) && !empty($select_root)}
                                                {foreach from = $root item = 'category'}
                                                {counter}
                                                {if $counter_r % 2 ==1} {continue} {/if}
                                                <li>
                                                    <label class="cb-checkbox w-100" for="c{$counter_r}">
                                                    <input name="form-checkbox" type="checkbox" id="c{$counter_r}" class='status' value="{$category->id_category}"  title="{$category->name}" {foreach from = $select_root item = 'selected'}{if $category->id_category == $selected['id_ebay_category']}checked='checked'{/if}{/foreach}><span class="lbl"></span>
                                                    <div>{$category->name}</div></label>
                                                </li>
                                                {/foreach}   
                                                {/if}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>