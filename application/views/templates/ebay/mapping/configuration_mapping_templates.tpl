                        <div class="row">
                                <div class="col-xs-12">
                                        <div class="headSettings b-Bottom clearfix p-0">
                                                <h4 class="headSettings_head pull-sm-left p-t10"> {ci_language line="title_$current_page"}</h4>										
                                                <ul class="pull-sm-right treeSettings">
                                                        <li class="expandAll"><i class="cb-plus"></i>{ci_language line="expand_all"}</li>
                                                        <li class="collapseAll"><i class="cb-minus"></i>{ci_language line="collapse_all"}</li>
                                                        <li class="checkedAll"><i class="cb-checked"></i>{ci_language line="check_all"}</li>
                                                        <li class="uncheckedAll"><i class="cb-unchecked"></i>{ci_language line="uncheck_all"}</li> 				
                                                </ul>
                                        </div>
                                </div>
                        </div>                    
                       {* <div class="widget-head">
                            <h3 class="heading"><i class="fa fa-archive"></i> {ci_language line="title_$current_page"}</h3>
                        </div>*}
                        <!-- // Widget Heading END -->
{*                        <h5 class="title-main title-bottom title-half">{ci_language line="default_template"}</h5>*}
                        <div class="widget-body tree">  
                            {include file="ebay/main_info.tpl"}
                            <div class="">
                                <div class="tree tree-unselectable">
                                    <div class="tree-folder">	
                                        <div class="tree-folder-header">
                                            <i class="icon-folder"></i>	
                                            <p class="treeHead treeRight">{ci_language line="mapping_templates"}</p>

                                        </div>
                                    </div> 
                                </div>

                                <div id="main" class="tree">
                                    <div class="treeRight selectTree">
                                        <div class="tree_show">
                                        <select class="select-main col-xs-10 search-select" name="category_id_profile" id="category_id_profile">
                                            <option value="0">{ci_language line="default_template"}</option>
                                            {if (isset($files) && !empty($files) )}
                                               {foreach from=$files item=fvalue}
                                                   <option value="{$fvalue}">{$fvalue}</option>
                                               {/foreach}  
                                            {/if}
                                        </select>
                                         <i class="icon-more cp_down_btn"></i>
                                        </div>
                                    </div>
                                </div>
                                        
                                {if !empty($ebay_templates_selected) }
                                <div id="main-selected" class="tree">
                                    <div class="treeRight selectTree">
                                        <div class="tree_show">
                                        <select class="col-xs-10 search-select" name="category_select_id_profile" id="category_select_id_profile">
                                            {foreach from=$ebay_templates_selected key=ekey item=evalue}
                                            <option value="{$evalue['template_selected']}" rel="{$evalue['id_category']}">{$evalue['template_selected']}</option>
                                            {/foreach}  
                                        </select>

                                        <i class="icon-more cp_down_btn"></i>
                                        </div>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            
                            <ul id="tree1" class=" tree-selectable"></ul>
                            

                            <div class="clearfix center" id="page-load">
                                    <i class="fa fa-spinner fa-spin fa-3x orange"></i>
                            </div>

                        </div>