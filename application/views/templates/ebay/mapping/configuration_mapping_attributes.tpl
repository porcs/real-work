                <!-- // Widget Heading END -->
                {if isset($attribute_group_value)}{foreach from = $attribute_group_value key = attribute_value_key item = attribute_value_name}
                <select class="variations-values-ebay-main style-hide" id='variations-values-ebay-select-{$attribute_value_key}' name='variations-values-ebay-main'>
                    <option value="0">{ci_language line="choose_variation_value"}</option>
                    {if isset($attribute_value_name)}{foreach from = $attribute_value_name key = value_key item = value_name}
                    <option value="{$value_name}">{$value_name}</option>
                    {/foreach}{/if}
                </select>
                {/foreach}{/if}
{*                <div class="widget-head">
                    <h3 class="header-main header-full">{ci_language line="create_your_variation"}</h3>
                </div>*}
                <div class="widget-body">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        {if !empty($attribute_main) && !empty($attribute_group)}
                        {include file="ebay/configuration_advance_tab.tpl"}
                        <div class="widget-body padding-none">
                            <div class="tab-content">
                                {include file="ebay/main_info.tpl"}
                                <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 1}active{/if}" id="tab-1">
                                    <div class="">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="variation_setting"}</h5>
                                        <div class="help-block text-small">{ci_language line="variation_help"}</div>
                                        <div class='space space2'></div>
                                        <div class="widget-main no-padding">
                                            {if isset($attribute_main)}{foreach from = $attribute_main key = main_key item = main_name}
                                            <div class="form-group variation-main" rel="{if isset($main_name['id_attribute_group'])}{$main_name['id_attribute_group']}{else}0{/if}">
                                                <div class="col-md-12 form-group type-name">
                                                    <label class="control-label" for="variations">{ci_language line="condition_id"}</label>
                                                    <div class="col-md-12">
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <input readonly class="form-control" type="text" id="variations-name-{$main_key}" name="variations-name-mapping" data-value="{if !empty($main_name['name'])}{$main_name['name']}{/if}" placeholder="{if !empty($main_name['name'])}{ucfirst(strtolower($main_name['name']))} ( {ci_language line="host_url"} ){/if}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-md-1">
                                                            <div class="col-md-12 text-center angle-right">
                                                                <i class="fa fa-angle-right"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <select class="variations-name-ebay" id='variations-name-ebay-select-{$main_key}' name='variations-name-ebay-mapping-{$main_key}'>
                                                                    <option value="0">{ci_language line="choose_variation_type"}</option>
                                                                    {if isset($attribute_group)}{foreach from = $attribute_group key = attribute_key item = attribute_name}
                                                                            <option mode="{$attribute_name['id_selection_mode']}" value="{$attribute_key}" {if empty($main_name['attribute_name']) && !empty($main_name['name']) && strtolower($main_name['name']) == strtolower($attribute_name['name'])}selected='selected'{elseif !empty($main_name['attribute_name']) && !empty($attribute_name['name']) && strtolower($main_name['attribute_name']) == strtolower($attribute_name['name'])}selected='selected'{/if}>{$attribute_name['name']}</option>
                                                                    {/foreach}{/if}
                                                                    {if isset($main_name['attribute_name'])}{foreach from = $attribute_main key = new_key item = new_name}{if isset($new_name['mode']) && $new_name['mode'] == -1}
                                                                            <option mode="-1" value="{$new_name['attribute_name']}" {if $new_name['attribute_name'] == $main_name['attribute_name']}selected="selected"{/if}>{$new_name['attribute_name']}</option>
                                                                    {/if}{/foreach}{/if}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class='clear clearfix'></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 form-group type-value">
                                                    <label class="control-label" for="variations">{ci_language line="variation_value"}</label>
                                                    <div class="col-md-12">
                                                    {if isset($attribute_value_main)}{foreach from = $attribute_value_main[$main_name['id_attribute_group']]['name'] key = value_main_key item = value_main}
                                                        <div class="col-md-12 value-type-group" rel="{$attribute_value_main[$main_name['id_attribute_group']]['id_attribute'][$value_main_key]}">
                                                            {if !empty($attribute_value_main[$main_name['id_attribute_group']]['attribute_value'])}
                                                            {assign var="attribute_value" value=$attribute_value_main[$main_name['id_attribute_group']]['attribute_value'][$value_main_key]}
                                                            {else}
                                                            {assign var="attribute_value" value=0}
                                                            {/if}
                                                            {if !empty($attribute_value_main[$main_name['id_attribute_group']]['mode'])}
                                                            {assign var="attribute_mode" value=$attribute_value_main[$main_name['id_attribute_group']]['mode'][$value_main_key]}
                                                            {else}
                                                            {assign var="attribute_mode" value=0}
                                                            {/if}
                                                            <div class="col-xs-5 col-md-5 root-value">
                                                                <div class="col-md-12">
                                                                    <input readonly class="form-control attribute-main" type="text" id="variations-value-{$value_main_key}" name="variations-value-mapping" data-value="{if !empty($value_main)}{$value_main}{/if}" placeholder="{if !empty($value_main)}{ucfirst(strtolower($value_main))}{/if}">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-1 col-md-1">
                                                                <div class="col-md-12 text-center angle-right">
                                                                    <i class="fa fa-angle-right"></i>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-5 col-md-5 input-main-type {if empty($attribute_mode)}style-hide{else if isset($attribute_mode) && $attribute_mode == 0}style-hide{/if}">
                                                                <div class="col-md-12">
                                                                    <input type="text" class="attribute-main-ebay form-control" name="attribute-main-ebay-{$value_main}-{$main_name['id_attribute_group']}" value="{if !empty($attribute_value) && isset($attribute_mode) && $attribute_mode == '1'}{$attribute_value}{else}0{/if}" rel="0" data-value="{if !empty($attribute_value) && isset($attribute_mode) && $attribute_mode == '1'}{$attribute_value}{else}0{/if}" data-key="0">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-5 col-md-5 select-main-type {if !empty($attribute_value) && isset($attribute_mode) && $attribute_mode == '1'}style-hide{/if}">
                                                                <div class="col-md-12">
                                                                    <select class="variations-values-ebay" id='variations-values-ebay-{$value_main}-{$value_main_key}' name='variations-values-ebay-mapping-{$value_main}-{$main_name['id_attribute_group']}'>
                                                                        <option value="0">{ci_language line="choose_variation_value"}</option>
                                                                        {if !empty($attribute_value) && isset($attribute_mode) && $attribute_mode == '0'}
                                                                            <option value="{$attribute_value}" selected="selected">{$attribute_value}</option>
                                                                        {/if}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 {if !empty($attribute_value) && isset($attribute_mode) && $attribute_mode == '1'}style-hide{/if}"><a class="edit-new btn btn-stroke btn-primary btn-circle pull-right"><i class="fa fa-pencil"></i></a></div>
                                                            <div class="col-md-1 {if empty($attribute_mode)}style-hide{else if isset($attribute_mode) && $attribute_mode == 0}style-hide{/if}"><a class="edit-new btn btn-stroke btn-danger btn-circle pull-right"><i class="fa fa-eraser"></i></a></div>
                                                            <div class='clear clearfix'></div>
                                                        </div>
                                                        {/foreach}{/if}
                                                        <div class="col-md-12">
                                                            <div class="help-block text-small">{ci_language line="variation_value_help"}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            {/foreach}{/if}
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 2}active{/if}" id="tab-2">
                                    <div class="">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="variation_setting"}</h5>
                                        <div class="help-block text-small">{ci_language line="variation_help"}</div>
                                        <div class='space space2'></div>
                                        <div class="widget-main no-padding">
                                            <div class="form-group include-varitation-main">
                                                <div class="col-md-12">
                                                    <div class="col-md-6 form-group exclude-variation-type">
                                                        <label class="control-label" for="variations"><i class="fa fa-minus-square"></i> {ci_language line="exclude_variation"}</label>
                                                        <div class="col-xs-11 col-md-10">
                                                            <div class="ul-exclude-type">
                                                                {if isset($attribute_group)}{foreach from = $attribute_group key = attribute_key item = attribute_name}
                                                                {if ( !empty($attribute_name) && array_key_exists('name', $attribute_name) ) }
                                                                        {if empty($profile_group) || !array_key_exists($attribute_name['name'], $profile_group)}
                                                                        <input readonly class="form-control li-exclude-type" name="li-variation-type" data-group="{$attribute_name['id_group']}" value="{$attribute_name['name']}">
                                                                        {/if}
                                                                {elseif !empty($attribute_name) && is_array($attribute_name) }
                                                                        {foreach from = $attribute_name key = attribute_keys item = attribute_names}
                                                                            {if empty($profile_group) || !array_key_exists($attribute_names['name'], $profile_group)}
                                                                            <input readonly class="form-control li-exclude-type" name="li-variation-type" data-group="{$attribute_names['id_group']}" value="{$attribute_names['name']}">
                                                                            {/if}
                                                                        {/foreach}
                                                                {/if}
                                                                {/foreach}{/if}
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-md-2 form-group sp-exchange">
                                                            <a class="sp-exchange-type-left btn btn-primary btn-circle btn-stroke"><i class="fa fa-angle-left"></i></a>
                                                            <div class="clearfix"></div>
                                                            <a class="sp-exchange-type-right btn btn-primary btn-circle btn-stroke"><i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 form-group include-variation-type">
                                                        <div class="col-xs-10 col-md-10 form-group">
                                                            <label class="control-label" for="variations"><i class="fa fa-check-square"></i> {ci_language line="include_variation"}</label>
                                                            <div class="ul-include-type" name="ul-include-type">
                                                                {if !empty($profile_group)}{foreach from = $profile_group key = profile_key item = profile_type}
                                                                        {if is_array($profile_type) && !array_key_exists('id_group', $profile_type) }
                                                                            <input readonly class="form-control li-include-type" name="li-variation-type" data-group="{$profile_type[0]['id_group']}" value="{$profile_key}">
                                                                        {else}
                                                                            <input readonly class="form-control li-include-type" name="li-variation-type" data-group="{$profile_type['id_group']}" value="{$profile_key}">
                                                                        {/if}
                                                                {/foreach}{/if}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5 class="title-main title-bottom title-half {if empty($profile_group)}style-hide{/if} variation-value-title">{ci_language line="variation_setting_value"}</h5>
                                            {if isset($attribute_group)}{foreach from = $attribute_group key = attribute_key item = attribute_name}
                                            {if ( !empty($attribute_name) && array_key_exists('name', $attribute_name) ) }
                                                <div class="form-group sp-variation-main {if empty($profile_group[$attribute_name['name']])}style-hide{/if}" rel="{if isset($attribute_name['id_group'])}{$attribute_name['id_group']}{else}0{/if}">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6 form-group sp-type-name">
                                                            <div class="col-xs-11 col-md-10">
                                                                <div class="col-md-12">
                                                                    <input readonly class="form-control" type="text" id="sp-variations-name-{$attribute_key}" name="sp-variations-name-mapping" data-value="{if !empty($attribute_name['name'])}{$attribute_name['name']}{/if}" placeholder="{if !empty($attribute_name['name'])}{ucfirst(strtolower($attribute_name['name']))}{/if}">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-1 col-md-2">
                                                                <div class="col-md-12 text-center angle-right">
                                                                    <i class="fa fa-angle-right"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 form-group sp-type-value">
                                                            {if empty($attribute_name['selected'])}
                                                            <div class="col-md-12 sp-type-value-sub" rel="1">
                                                                <div class="col-md-12 sp-value-type-group">
                                                                    <div class="col-xs-11 col-md-10 sp-input-main-type style-hide">
                                                                        <div class="col-md-12">
                                                                            <input type="text" class="sp-attribute-main-ebay form-control" name="sp-attribute-main-ebay-{$attribute_name['name']}-1" readonly value="0" rel="0" data-value="0" data-key="0">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-11 col-md-10 sp-select-main-type">
                                                                        <div class="col-md-12">
                                                                            <select class="sp-variations-values-ebay" id='sp-variations-values-ebay-{$attribute_name['name']}-1' name='sp-variations-values-ebay-mapping-{$attribute_name['name']}-1'>
                                                                                <option value="0">{ci_language line="choose_variation_value"}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2"><a class="sp-edit-new btn btn-stroke btn-primary btn-circle pull-right" style="text-decoration: none;"><i class="fa fa-pencil"></i></a></div>
                                                                    <div class='clear clearfix'></div>
                                                                </div>
                                                            </div>
                                                            {else}
                                                            {counter name='Rel1' start = 1 skip = 1 assign = mapping_rel}
                                                            {foreach from = $attribute_name['selected'] key = value_name item = mode }
                                                            <div class="col-md-12 sp-type-value-sub" rel="{$mapping_rel}">
                                                                <div class="col-md-12 sp-value-type-group">
                                                                    <div class="col-xs-11 col-md-10 sp-input-main-type {if $mode == 0}style-hide{/if}">
                                                                        <div class="col-md-12">
                                                                            <input type="text" class="sp-attribute-main-ebay form-control" name="sp-attribute-main-ebay-{$attribute_name['name']}-{$mapping_rel}" value="{if isset($value_name) && isset($mode) && $mode == '1'}{$value_name}{else}0{/if}" rel="0" data-value="0" data-key="0">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-11 col-md-10 sp-select-main-type {if $mode == 1}style-hide{/if}">
                                                                        <div class="col-md-12">
                                                                            <select class="sp-variations-values-ebay" id='sp-variations-values-ebay-{$attribute_name['name']}-{$mapping_rel}' name='sp-variations-values-ebay-mapping-{$attribute_name['name']}-{$mapping_rel}'>
                                                                                <option value="0">{ci_language line="choose_variation_value"}</option>
                                                                                {if isset($value_name) && isset($mode) && $mode == '0'}
                                                                                <option value="{$value_name}" selected="selected">{$value_name}</option>
                                                                                {/if}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 {if isset($mode) && $mode == 1}style-hide{/if}"><a class="sp-edit-new btn btn-stroke btn-primary btn-circle pull-right"><i class="fa fa-pencil"></i></a></div>
                                                                    <div class="col-md-2 {if isset($mode) && $mode == 0}style-hide{/if}"><a class="sp-edit-new btn btn-stroke btn-danger btn-circle pull-right"><i class="fa fa-eraser"></i></a></div>
                                                                    <div class='clear clearfix'></div>
                                                                </div>
                                                            </div>
                                                            {counter name='Rel1'}
                                                            {/foreach}
                                                            {/if}
                                                            <div class="col-xs-10 col-md-10">
                                                                <label class="label-insert-plus" for="variations"><a class="insert-plus"><i class="fa fa-plus-square-o"></i></a> {ci_language line="add_more"}</label>
                                                                <label class="label-remove-plus pull-right {if empty($attribute_name['selected'])}style-hide{else if !empty($attribute_name['selected']) && count($attribute_name['selected']) == 1}style-hide{/if}" for="variations"><a class="remove-plus"><i class="fa fa-minus-square"></i></a> {ci_language line="remove_more"}</label>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12 form-group">
{*                                                                <div class="sp-help help-block text-small">{ci_language line="variation_prepare_value_help"}</div>*}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            {elseif !empty($attribute_name) && is_array($attribute_name) }
                                                {foreach from = $attribute_name key = attribute_keys item = attribute_names}
                                                    <div class="form-group sp-variation-main {if empty($profile_group[$attribute_names['name']])}style-hide{/if}" rel="{if isset($attribute_names['id_group'])}{$attribute_names['id_group']}{else}0{/if}">
                                                        <div class="col-md-12">
                                                            <div class="col-md-6 form-group sp-type-name">
                                                                <div class="col-xs-11 col-md-10">
                                                                    <div class="col-md-12">
                                                                        <input readonly class="form-control" type="text" id="sp-variations-name-{$attribute_keys}" name="sp-variations-name-mapping" data-value="{if !empty($attribute_names['name'])}{$attribute_names['name']}{/if}" placeholder="{if !empty($attribute_names['name'])}{ucfirst(strtolower($attribute_names['name']))}{/if}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-1 col-md-2">
                                                                    <div class="col-md-12 text-center angle-right">
                                                                        <i class="fa fa-angle-right"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 form-group sp-type-value">
                                                                {if empty($attribute_names['selected'])}
                                                                <div class="col-md-12 sp-type-value-sub" rel="1">
                                                                    <div class="col-md-12 sp-value-type-group">
                                                                        <div class="col-xs-11 col-md-10 sp-input-main-type style-hide">
                                                                            <div class="col-md-12">
                                                                                <input type="text" class="sp-attribute-main-ebay form-control" name="sp-attribute-main-ebay-{$attribute_names['name']}-1" readonly value="0" rel="0" data-value="0" data-key="0">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-11 col-md-10 sp-select-main-type">
                                                                            <div class="col-md-12">
                                                                                <select class="sp-variations-values-ebay" id='sp-variations-values-ebay-{$attribute_names['name']}-1' name='sp-variations-values-ebay-mapping-{$attribute_names['name']}-1'>
                                                                                    <option value="0">{ci_language line="choose_variation_value"}</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2"><a class="sp-edit-new btn btn-stroke btn-primary btn-circle pull-right" style="text-decoration: none;"><i class="fa fa-pencil"></i></a></div>
                                                                        <div class='clear clearfix'></div>
                                                                    </div>
                                                                </div>
                                                                {else}
                                                                {counter name='Rel1' start = 1 skip = 1 assign = mapping_rel}
                                                                {foreach from = $attribute_names['selected'] key = value_name item = mode }
                                                                <div class="col-md-12 sp-type-value-sub" rel="{$mapping_rel}">
                                                                    <div class="col-md-12 sp-value-type-group">
                                                                        <div class="col-xs-11 col-md-10 sp-input-main-type {if $mode == 0}style-hide{/if}">
                                                                            <div class="col-md-12">
                                                                                <input type="text" class="sp-attribute-main-ebay form-control" name="sp-attribute-main-ebay-{$attribute_names['name']}-{$mapping_rel}" value="{if isset($value_name) && isset($mode) && $mode == '1'}{$value_name}{else}0{/if}" rel="0" data-value="0" data-key="0">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-11 col-md-10 sp-select-main-type {if $mode == 1}style-hide{/if}">
                                                                            <div class="col-md-12">
                                                                                <select class="sp-variations-values-ebay" id='sp-variations-values-ebay-{$attribute_names['name']}-{$mapping_rel}' name='sp-variations-values-ebay-mapping-{$attribute_names['name']}-{$mapping_rel}'>
                                                                                    <option value="0">{ci_language line="choose_variation_value"}</option>
                                                                                    {if isset($value_name) && isset($mode) && $mode == '0'}
                                                                                    <option value="{$value_name}" selected="selected">{$value_name}</option>
                                                                                    {/if}
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 {if isset($mode) && $mode == 1}style-hide{/if}"><a class="sp-edit-new btn btn-stroke btn-primary btn-circle pull-right"><i class="fa fa-pencil"></i></a></div>
                                                                        <div class="col-md-2 {if isset($mode) && $mode == 0}style-hide{/if}"><a class="sp-edit-new btn btn-stroke btn-danger btn-circle pull-right"><i class="fa fa-eraser"></i></a></div>
                                                                        <div class='clear clearfix'></div>
                                                                    </div>
                                                                </div>
                                                                {counter name='Rel1'}
                                                                {/foreach}
                                                                {/if}
                                                                <div class="col-xs-10 col-md-10">
                                                                    <label class="label-insert-plus" for="variations"><a class="insert-plus"><i class="fa fa-plus-square-o"></i></a> {ci_language line="add_more"}</label>
                                                                    <label class="label-remove-plus pull-right {if empty($attribute_names['selected'])}style-hide{else if !empty($attribute_names['selected']) && count($attribute_names['selected']) == 1}style-hide{/if}" for="variations"><a class="remove-plus"><i class="fa fa-minus-square"></i></a> {ci_language line="remove_more"}</label>
                                                                </div>
                                                                <div class="col-xs-12 col-md-12 form-group">
{*                                                                    <div class="sp-help help-block text-small">{ci_language line="variation_prepare_value_help"}</div>*}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            {/if}
                                            {/foreach}{/if}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {else}
                            <div class="headSettings clearfix b-Bottom p-b10">
                                <h4 class="headSettings_head">{ci_language line="create_your_variation"}</h4>
                            </div>
                            <div class="notFound">
                                <div class="headSettings clearfix b-Bottom p-b10">
                                        <h4 class="headSettings_head"><span>Oops...</span> {ci_language line="variation_warning_title"}</h4>	
                                </div>	
                                <div class="notFound_reason">
                                        <ul class="main_reason">
                                        	{if $error_code == "mapping_not_found"}
                                        		<li>{ci_language line="mapping_not_found"}</li>
                                        	{/if}
                                        	{if $error_code == "no_attributes"}
                                        		<li>{ci_language line="no_attributes"}</li>
                                        	{/if}
                                        </ul>
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
