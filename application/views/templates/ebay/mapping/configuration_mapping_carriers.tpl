                <!-- // Widget Heading END -->
{*                <div class="widget-head">
                    <h3 class="header-main header-full">{ci_language line="create_your_shipping"}</h3>
                </div>*}
                {if isset($popup)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                <div class="widget-body" ng-controller="CarrierMain as carrier">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        <div class="widget-head header-tab-switch">
                            <ul>
                                <li {if isset($tab_data['tab']) && $tab_data['tab'] == 1}class="active"{/if}><a href="#tab-1" data-toggle="tab" ng-click="carrier.init(1)"><i class="fa fa-exchange"></i> {ci_language line="use_matchings"}</a></li>
                                <li {if isset($tab_data['tab']) && $tab_data['tab'] == 2}class="active"{/if}><a href="#tab-2" data-toggle="tab" ng-click="carrier.init(2)"><i class="fa fa-wrench"></i> {ci_language line="use_specifics"}</a></li>
                            </ul>
                        </div>
                        <div class="widget-body padding-none">
                            <div class="tab-content">
                                {include file="ebay/main_info.tpl"}
                                <div class="form-group option_modes">
                                    <label class="pull-left">{ci_language line="Active"}: </label>
                                    <div class="option_mode pull-left">
                                        <div class="cb-switcher" ng-click="carrier.switchChange()" ng-init="carrier.init(carrier.tab_datas.current_active)">
                                            <label class="inner-switcher">
                                                <input name="active" id="form-app-mode" type="checkbox" data-state-on="ON" data-state-off="OFF">
                                            </label>
                                            <span class="cb-state">{ci_language line="ON"}</span>
                                        </div>
                                    </div>
                                    <div class="help-block text-small">[[carrier.tab_help]]</div>
                                </div>
                                <div ng-show="carrier.tab_datas.tab_data[carrier.current_tab].active" class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 1}active{/if}" id="tab-1">
                                    <div class="profile-shipping">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="payment_location"}</h5>
                                        <div class="widget-main sp-postal-code">
                                            <div class="form-group postal-main">
                                                <div class="col-md-12">
                                                    <div class="col-md-3 form-group">
                                                        <label class="control-label" for="form-postal-code">{ci_language line="postcode"} {_get_tooltip_icon({ci_language line="postcode_help"})}</label>
                                                        <div class="col-md-12 position-relative">
                                                            <input class="form-postal-code form-control" type="text" id="form-postal-code"  data-example="{if isset($code)}{$code['exam_code']}{/if}" data-code="{if isset($code)}{$code['code']}{/if}" name="form-postal-code" value="{if isset($postal_code) }{$postal_code}{/if}" placeholder="{ci_language line="postcode"}" required {if isset($postal_code_pattern) }{$postal_code_pattern}{/if}><div class="asterisk"><i class="icon-asterisk"></i></div>
                                                            <div class="clear clearfix"></div>
                                                            {*<div class="clear clearfix help-block text-small">{ci_language line="postcode_help"}</div>*}
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 col-md-1"></i></div>
                                                    <div class="col-md-3 form-group">
                                                        <label class="control-label" for="dispatch_time">{ci_language line="dispatch_time"} {_get_tooltip_icon({ci_language line="dispatch_time_help"})}</label>
                                                        <div class="col-md-12">
                                                            <select class="dispatch_time" id='dispatch_time' name='dispatch_time'>
                                                                {foreach from = $dispatch_time item = time}
                                                                    <option value="{$time['dispatch_time']}" {if isset($time['selected'])}selected='selected'{/if}>{ci_language line="{$time['dispatch_time_name']}"}</option>
                                                                {/foreach}
                                                            </select>
                                                            {*<div class="help-block text-small">{ci_language line="dispatch_time"}</div>*}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-main padding-none carrier-main {$field_hide}">
                                            <div class="transparent">
                                                <h5 class="title-main title-bottom title-half">{ci_language line="mapping_carrier_default"}</h5>
                                                <div class="carrier-body" rel='1'>
                                                    <div class="widget-main">
                                                        <div class="shipping-carriers-main">
                                                            <div class="form-group shipping-carriers-body">
                                                                <div class="col-md-12">
                                                                    <div class="col-xs-11 col-md-3">
                                                                        <div class="col-md-12">
                                                                            <select class="domestic-ebay-carriers" name="domestic-ebay-carriers-1">
                                                                                <option value="0">{ci_language line="ebay_carrier"}</option>
                                                                                {foreach from=$ebay_carriers item=carriers}{if $carriers->is_international == "0" && $carriers->is_valid == "1" && $carriers->is_flat == "1"}
                                                                                <option data-service="{$carriers->service}" value="{$carriers->id_shipping}" {if !empty($default_selected) && isset($default_selected[0]['id_ebay_shipping']) && $default_selected[0]['id_ebay_shipping'] == $carriers->id_shipping}selected='selected'{/if}>{$carriers->name}</option>
                                                                                {/if}{/foreach}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-1 col-md-1 text-center">
                                                                        <div class="checkbox">
                                                                            <label class="checkbox-custom"><i class="fa fa-fw fa-square-o green"></i><input name="form-carrier-default" type="checkbox" value="1" {if !empty($default_selected) && !empty($default_selected[0]['is_enabled'])}checked="checked"{/if}></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </div>
                                        </div>
                                        <div class="widget-main padding-none domestic-main {$field_hide}">
                                            <div class="transparent">
                                                <h5 class="title-main title-bottom title-half">{ci_language line="mapping_domestic_shipping"}</h5>
                                                {counter name='d1' start=2 skip=1 assign=counter_domestic_key}
                                                {foreach from=$domestic_selected key=key item=domestic}
                                                <div class="domestic-body" rel='{$counter_domestic_key}'>
                                                    <div class="widget-main">
                                                        <div class="domestic-carriers-main">
                                                            <div class="form-group domestic-carriers-body">
                                                                <label class="control-label" for="domestic-carriers-{$counter_domestic_key}">{ci_language line='choose_a_carrier'}</label>
                                                                <div class="col-md-12">
                                                                    <div class="col-xs-11 col-md-3">
                                                                        <div class="col-md-12">
                                                                            <select class="domestic-carriers" name="domestic-carriers-{$counter_domestic_key}" id='domestic-carriers-{$counter_domestic_key}'>
                                                                                <option value="0">{ci_language line="associate_carrier"}</option>
                                                                                {foreach from=$selected item=select}{if is_array($select) && isset($select) }
                                                                                <option data-value="{$select['name']}" value="{$select['id_carrier']}" {if isset($domestic['id_shipping']) && $domestic['id_shipping'] == $select['id_carrier']}selected='selected'{/if}>{$select['name']}</option>
                                                                                {/if}{/foreach}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-1 col-md-1 text-center angle-right"><i class="fa fa-chevron-right exchange-item"></i></div>
                                                                    <div class="col-xs-11 col-md-3">
                                                                        <div class="col-md-12">
                                                                            <select class="domestic-ebay-carriers" name="domestic-ebay-carriers-{$counter_domestic_key}">
                                                                                <option value="0">{ci_language line="ebay_carrier"}</option>
                                                                                {foreach from=$ebay_carriers item=carriers}{if $carriers->is_international == "0" && $carriers->is_valid == "1" && $carriers->is_flat == "1"}
                                                                                <option data-service="{$carriers->service}" value="{$carriers->id_shipping}" {if isset($domestic['id_ebay_shipping']) && $domestic['id_ebay_shipping'] == $carriers->id_shipping}selected='selected'{/if}>{$carriers->name}</option>
                                                                                {/if}{/foreach}
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-11 col-md-1 text-center">
                                                                    {if {$counter_domestic_key} == 2}
                                                                    <a class="icons-plus addlocalmapping"><i class="fa fa-plus-square-o"></i></a>
                                                                    {else}
                                                                    <a class="icons-plus removemapping"><i class="fa fa-minus-square-o"></i></a>
                                                                    {/if}
                                                                    {counter name='d1'}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/foreach}              
                                            </div>
                                        </div>
                                        <div class="widget-main padding-none international-main {$field_hide}">
                                            <div class="padding-none transparent">
                                                <h5 class="title-main title-bottom title-half">{ci_language line="mapping_international_shipping"}</h5>
                                                {counter name='i1' start=2 skip=1 assign=counter_international_key}
                                                {counter name='i2' start=2 skip=1 assign=counter_international_key}
                                                {foreach from=$international_genarate key=key item=international}
                                                {foreach from=$international key=key_ebay item=shipping_international}
                                                <div class="international-body" rel='{$counter_international_key}'>
                                                    <div class="widget-main">                                             
                                                        <div class="international-carriers-main">
                                                            <div class="international-carriers-body">
                                                                <div class="form-group">
                                                                    <label class="control-label choose-carriers" for="international-carriers-{$counter_international_key}">{ci_language line='choose_a_carrier'}</label>
                                                                    <div class="col-md-12">
                                                                        <div class="col-xs-11 col-md-3">
                                                                            <div class="col-md-12">
                                                                                <select class="international-carriers" name="international-carriers-{$counter_international_key}" id='international-carriers-{$counter_international_key}'>
                                                                                    <option value="0">{ci_language line="associate_carrier"}</option>
                                                                                    {foreach from=$selected item=select}
                                                                                    <option value="{$select['id_carrier']}" {if isset($shipping_international['id_shipping']) && $shipping_international['id_shipping'] == $select['id_carrier']}selected='selected'{/if}>{$select['name']}</option>
                                                                                    {/foreach}
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1 col-md-1 text-center angle-right"><i class="fa fa-chevron-right exchange-item"></i></div>
                                                                        <div class="col-xs-11 col-md-3">
                                                                            <div class="col-md-12">
                                                                                <select class="international-ebay-carriers" name="international-ebay-carriers-{$counter_international_key}">
                                                                                    <option value="0">{ci_language line="ebay_carrier"}</option>
                                                                                    {foreach from=$ebay_carriers item=carriers}{if $carriers->is_international == "1" && $carriers->is_valid == "1" && $carriers->is_flat == "1"}
                                                                                    <option data-service="{$carriers->service}" value="{$carriers->id_shipping}" {if isset($shipping_international['id_ebay_shipping']) && $shipping_international['id_ebay_shipping'] == $carriers->id_shipping}selected='selected'{/if}>{$carriers->name}</option>
                                                                                    {/if}{/foreach}
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1 col-md-1"></div>
                                                                        <div class="col-xs-11 col-md-3 form-group text-center">
                                                                            <div class="col-md-12">
                                                                                <select class="country-name" name="country-name-{$counter_international_key}" id='country-name-{$counter_international_key}'>
                                                                                    <option value="0">{ci_language line="choose_a_country"}</option>
                                                                                    {foreach from=$ebay_country item=country}{if $country->service != 'None' }
                                                                                    <option value="{$country->service}" {if !empty($country_selected) && isset($key) && $key == $country->service}selected='selected'{/if}>{$country->name}</option>
                                                                                    {/if}{/foreach}
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class='col-xs-2 col-md-1 text-center'>
                                                                            {if {$counter_international_key} == 2 && $counter_international_key == 2}
                                                                            <a class="icons-plus addintermapping"><i class="fa fa-plus-square-o"></i></a>
                                                                            {else}
                                                                            <a class="icons-plus removemapping"><i class="fa fa-minus-square-o"></i></a>
                                                                            {/if}
                                                                        </div>
                                                                    </div>
                                                                </div>
{*                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                    </div>
                                                                </div>*}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {counter name='i1'}
                                                {/foreach}
                                                {counter name='i2'}
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                <div ng-show="carrier.tab_datas.tab_data[carrier.current_tab].active"  class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 2}active{/if}" id="tab-2">
                                    <div class="profile-shipping">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="payment_location"}</h5>
                                        <div class="widget-main sp-postal-code">
                                            <div class="form-group postal-main">
                                                <label class="control-label" for="sp-form-postal-code">{ci_language line="postcode"}</label>
                                                <div class="col-md-12">
                                                    <span class="col-md-5 position-relative">
                                                        <div class="col-md-11">
                                                            <input class="form-postal-code form-control" type="text" id="sp-form-postal-code"  data-example="{if isset($code)}{$code['exam_code']}{/if}" data-code="{if isset($code)}{$code['code']}{/if}" name="sp-form-postal-code" value="{if isset($sp_postal_code) }{$sp_postal_code}{/if}" placeholder="{ci_language line="postcode"}" required {if isset($postal_code_pattern) }{$postal_code_pattern}{/if}><div class="asterisk"><i class="icon-asterisk"></i></div>
                                                            <div class="clear clearfix"></div>
                                                            <div class="clear clearfix help-block text-small">{ci_language line="postcode_help"}</div>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <h5 class="title-main title-bottom title-half">{ci_language line="shipping_time"}</h5>
                                        <div class="widget-main sp-dispatch-main">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <div class="col-md-5 form-group">
                                                        <label class="control-label" for="sp-dispatch_time">{ci_language line="dispatch_time"}</label>
                                                        <div class="col-md-10">
                                                            <select class="sp-dispatch-time" id='sp-dispatch-time' name='sp-dispatch-time'>
                                                                {foreach from = $sp_dispatch_time item = time}
                                                                    <option value="{$time['dispatch_time']}" {if isset($time['selected'])}selected='selected'{/if}>{ci_language line="{$time['dispatch_time_name']}"}</option>
                                                                {/foreach}
                                                            </select>
                                                            <div class="help-block text-small">{ci_language line="dispatch_time_help"}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-main sp-domestic-main padding-none">
                                            <div class="transparent padding-none">
                                                <h5 class="title-main title-bottom title-half">{ci_language line="mapping_domestic_shipping"}</h5>
                                                {counter name='d1' start=2 skip=1 assign=counter_domestic_key}
                                                {foreach from=$profile_shipping key=key item=domestic}{if empty($domestic['country_service']) && empty($domestic['name_ebay_country']) || empty($profile_shipping)}
                                                <div class="sp-domestic-body" rel='{$counter_domestic_key}'>
                                                    <div class="widget-main">
                                                        <div class="form-group sp-domestic-carriers-main">
                                                            <div class="form-group sp-domestic-carriers-body">
                                                                <label class="control-label" for="sp-domestic-carriers-{$counter_domestic_key}">{ci_language line='choose_a_carrier'}</label>
                                                                <div class="col-md-12">
                                                                    <div class="col-xs-11 col-md-5 form-group">
                                                                        <div class="col-md-11">
                                                                            <select class="sp-domestic-ebay-carriers" name="sp-domestic-ebay-carriers-{$counter_domestic_key}">
                                                                                <option value="0">{ci_language line="ebay_carrier"}</option>
                                                                                {foreach from=$ebay_carriers item=carriers}{if $carriers->is_international == "0" && $carriers->is_valid == "1" && $carriers->is_flat == "1"}
                                                                                <option data-service="{$carriers->service}" value="{$carriers->id_shipping}" {if isset($domestic['id_ebay_shipping']) && $domestic['id_ebay_shipping'] == $carriers->id_shipping}selected='selected'{/if}>{$carriers->name}</option>
                                                                                {/if}{/foreach}
                                                                            </select>
{*                                                                            <div class="help-block text-small">{ci_language line="ebay_shipping_help"}</div>*}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xs-2 col-md-1">
                                                                    {if {$counter_domestic_key} == 2}
                                                                    <a class="icons-plus sp-addlocalmapping"><i class="fa fa-plus-square-o"></i></a>
                                                                    {else}
                                                                    <a class="icons-plus sp-removemapping"><i class="fa fa-minus-square-o"></i></a>
                                                                    {/if}
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                         <div class="col-xs-11 col-md-5 form-group carrier-margin-top">
                                                                            <div class="col-md-11">
                                                                                <div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span>
                                                                                <input class="cost-rate form-control" name="cost-rate-{$counter_domestic_key}" type="text" placeholder="{ci_language line="cost"} ({$current})" type="text" min="0" value="{if isset($domestic['shipping_cost'])}{$domestic['shipping_cost']}{/if}"></div>
{*                                                                                <div class="help-block text-small">{ci_language line="cost_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-11 col-md-5 form-group carrier-margin-top">
                                                                            <div class="col-md-11">
                                                                                <div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span>
                                                                                <input class="additional-rate form-control" name="additional-rate-{$counter_domestic_key}" type="text" placeholder="{ci_language line="additions"} ({$current})" type="text" min="0" value="{if isset($domestic['shipping_additionals'])}{$domestic['shipping_additionals']}{/if}"></div>
{*                                                                                <div class="help-block text-small">{ci_language line="additional_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {counter name='d1'}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {else}{assign var="my_array" value=1} {/if}{/foreach}        
                                            </div>
                                        </div>
                                        <div class="widget-main sp-international-main padding-none">
                                            <div class="transparent padding-none">
                                                <h5 class="title-main title-bottom title-half">{ci_language line="mapping_international_shipping"}</h5>
                                                {counter name='i1' start=2 skip=1 assign=counter_international_key}
                                                {foreach from=$profile_shipping key=key item=international}{if (!empty($profile_shipping) && !empty($international['country_service']) && !empty($international['name_ebay_country'])) || empty($profile_shipping) || (empty($my_array) && $counter_international_key == 2) }
                                                <div class="sp-international-body" rel='{$counter_international_key}'>
                                                    <div class="widget-main form-group">                                             
                                                        <div class="sp-international-carriers-main">
                                                            <div class="sp-international-carriers-body">
                                                                <div class="form-group"><label class="control-label sp-choose-country" for="sp-country-name-{$counter_international_key}">{ci_language line='choose_a_carrier'}</label>
                                                                    <div class="col-md-12">
                                                                        <div class="col-xs-11 col-md-5">
                                                                            <div class="col-md-11">
                                                                                <select class="sp-international-ebay-carriers" name="sp-international-ebay-carriers-{$counter_international_key}">
                                                                                    <option value="0">{ci_language line="ebay_carrier"}</option>
                                                                                    {foreach from=$ebay_carriers item=carriers}{if $carriers->is_international == "1" && $carriers->is_valid == "1" && $carriers->is_flat == "1"}
                                                                                    <option data-service="{$carriers->service}" value="{$carriers->id_shipping}" {if isset($international['id_ebay_shipping']) && $international['id_ebay_shipping'] == $carriers->id_shipping}selected='selected'{/if}>{$carriers->name}</option>
                                                                                    {/if}{/foreach}
                                                                                </select>
{*                                                                                <div class="help-block text-small">{ci_language line="ebay_international_shipping_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-11 col-md-5">
                                                                            <div class="col-md-11">
                                                                                <select class="sp-country-name" name="sp-country-name-{$counter_international_key}" id='sp-country-name1-{$counter_international_key}'>
                                                                                    <option value="0">{ci_language line="choose_a_country"}</option>
                                                                                    {foreach from=$ebay_country item=country}{if $country->service != 'None' }
                                                                                    <option value="{$country->service}" {if !empty($international['country_service']) && $international['country_service'] == $country->service}selected='selected'{/if}>{$country->name}</option>
                                                                                    {/if}{/foreach}
                                                                                </select>
{*                                                                                <div class="help-block text-small">{ci_language line="country_shipping_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                        <div class='col-xs-2 col-md-1'>
                                                                            {if {$counter_international_key} == 2}
                                                                            <a class="icons-plus sp-addintermapping"><i class="fa fa-plus-square-o"></i></a>
                                                                            {else}
                                                                            <a class="icons-plus sp-interRemovemapping"><i class="fa fa-minus-square-o"></i></a>
                                                                            {/if}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <div class="col-xs-11 col-md-5 carrier-margin-top">
                                                                            <div class="col-md-11">
                                                                                <div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span>
                                                                                <input class="cost-rate form-control" name="cost-rate-inter-{$counter_international_key}" type="text" placeholder="{ci_language line="cost"} ({$current})" type="text" min="0" value="{if isset($international['shipping_cost']) && !empty($international['country_service']) && !empty($international['name_ebay_country'])}{$international['shipping_cost']}{/if}"></div>
{*                                                                                <div class="help-block text-small">{ci_language line="cost_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-11 col-md-5 carrier-margin-top">
                                                                            <div class="col-md-11">
                                                                                <div class="input-group"><span class="input-group-addon"><strong>{ci_language line="dolla"}</strong></span>
                                                                                <input class="additional-rate form-control" name="additional-rate-inter-{$counter_international_key}" type="text" placeholder="{ci_language line="additions"} ({$current})" type="text" min="0" value="{if isset($international['shipping_additionals']) && !empty($international['country_service']) && !empty($international['name_ebay_country'])}{$international['shipping_additionals']}{/if}"></div>
{*                                                                                <div class="help-block text-small">{ci_language line="additional_help"}</div>*}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {counter name='i1'}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/if}{/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    {literal}
                        var lg_current   = [{/literal}{$currency_code}{literal}];
                        var choose_value = {/literal}{if isset($data_output)}{$data_output}{else}[]{/if}{literal};
                    {/literal}  
                    var app_data = {$app_data};
                </script>