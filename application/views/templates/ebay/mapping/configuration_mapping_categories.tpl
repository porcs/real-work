                    
                        <!-- // Widget Heading END -->
                        <div id="main">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <input type="text" class="select-main col-xs-10" name="category_id_profile" id="category_id_profile" readonly value="{ci_language line="choose_ebay_categories"}" rel="" data-value="0" data-key="">
                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        <div id="main_selectbox" class="tree">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <select class="col-xs-10 search-select select-main" name="category_select_id_profile" id="category_select_id_profile">
{*                                    <option></option>*}
                                    <option value="0">{ci_language line="choose_ebay_categories"}</option>
                                    {if (isset($ebay_categories) && !empty($ebay_categories) )}
                                        {foreach from=$ebay_categories key=ekey item=evalue}
                                        <option value="{$evalue.id}">{$evalue.name}</option>
                                        {/foreach}  
                                    {/if}
                                </select>

                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        <div id="main-store" class="tree">
                            {$ebay_store_categories}
                        </div>
                        {if !empty($ebay_categories_selected) }
                        <div id="main-selected" class="tree">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <select class="col-xs-10 search-select select-main" name="category_select_id_profile">
                                    {foreach from=$ebay_categories_selected key=ekey item=evalue}
                                    <option value="{$evalue['id_ebay_category']}" rel="{$evalue['id_category']}" data-name="{$evalue['name_category']}">{$evalue['name_ebay_category']}</option>
                                    {/foreach}  
                                </select>

                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($ebay_secondary_categories_selected) }
                        <div id="main-secondary-selected" class="tree">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <select class="col-xs-10 search-select select-main" name="category_select_id_profile">
                                    {foreach from=$ebay_secondary_categories_selected key=ekey item=evalue}
                                    <option value="{$evalue['id_ebay_category']}" rel="{$evalue['id_category']}" data-name="{$evalue['name_category']}">{$evalue['name_ebay_category']}</option>
                                    {/foreach}  
                                </select>

                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($ebay_store_selected) }
                        <div id="main-store-selected" class="tree">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <select class="col-xs-10 search-select select-main" name="category_select_id_profile">
                                    {foreach from=$ebay_store_selected key=ekey item=evalue}
                                    <option value="{$evalue['id_ebay_category']}" rel="{$evalue['id_category']}" data-name="{$evalue['name_category']}">{$evalue['name_ebay_category']}</option>
                                    {/foreach}  
                                </select>

                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($ebay_secondary_store_selected) }
                        <div id="main-secondary-store-selected" class="tree">
                            <div class="treeRight selectTree">
                                <div class="tree_show">
                                <select class="col-xs-10 search-select select-main" name="category_select_categories_store_id_profile">
                                    {foreach from=$ebay_secondary_store_selected key=ekey item=evalue}
                                    <option value="{$evalue['id_ebay_category']}" rel="{$evalue['id_category']}" data-name="{$evalue['name_category']}">{$evalue['name_ebay_category']}</option>
                                    {/foreach}  
                                </select>

                                <i class="icon-more cp_down_btn"></i>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {*start content*}
                        <div class="row">
                                <div class="col-xs-12">
                                        <div class="headSettings b-Bottom clearfix p-0">
                                                <h4 class="headSettings_head pull-sm-left p-t10"> {ci_language line="title_$current_page"}</h4>										
                                                <ul class="pull-sm-right treeSettings">
                                                        <li class="expandAll"><i class="cb-plus"></i>{ci_language line="expand_all"}</li>
                                                        <li class="collapseAll"><i class="cb-minus"></i>{ci_language line="collapse_all"}</li>
                                                        <li class="checkedAll"><i class="cb-checked"></i>{ci_language line="check_all"}</li>
                                                        <li class="uncheckedAll"><i class="cb-unchecked"></i>{ci_language line="uncheck_all"}</li>
                                                        <li class="getStore link"><i class="cb-store"></i>{ci_language line="get_store"}</li>						
                                                </ul>
                                        </div>
                                </div>
                        </div>
                        {if isset($popup)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
{*                        <h5 class="title-main title-bottom title-half">{ci_language line="ebay_root_category"}</h5>*}
                        <div class="widget widget-tabs widget-tabs-responsive">
                                <div class="widget-head header-tab-switch {$field_hide}">
                                    <ul>
                                        <li class="active" rel="main-selected"><a href="#tab-1" data-toggle="tab"> {ci_language line="primary_categories"}</a></li>
                                        <li class="{$field_hide}" rel="main-secondary-selected"><a href="#tab-2" data-toggle="tab"> {ci_language line="secondary_categories"}</a></li>
                                        <li class="{$field_hide}" rel="main-store-selected"><a href="#tab-3" data-toggle="tab" class="getStore"> {ci_language line="primary_store_categories"}</a></li>	
                                        <li class="{$field_hide}" rel="main-secondary-store-selected"><a href="#tab-4" data-toggle="tab" class="getStore"> {ci_language line="secondary_store_categories"}</a></li>	
                                    </ul>
                                </div>
                           
                            <div class="widget-body padding-none">
                                <div class="tab-content">
                                    {include file="ebay/main_info.tpl"}
                                    <div class="tab-pane active" id="tab-1"> 
                                        <div class="tree clearfix custom-form">   
                                            <div class="">
                                                <div class="tree-unselectable">
                                                    <div class="tree-folder">	
                                                        <div class="tree-folder-header">
                                                            <i class="icon-folder"></i>	
                                                            <p class="treeHead treeRight">{ci_language line="choose_ebay_categories"}</p>
                                                               
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>  
                                            <ul id="tree1" class=" tree_point tree-selectable"></ul> 
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-2"> 
                                        <div class="tree clearfix custom-form">   
                                            <div class="">
                                                <div class="tree-unselectable">
                                                    <div class="tree-folder">	
                                                        <div class="tree-folder-header">
                                                            <i class="icon-folder"></i>	
                                                            <p class="treeHead treeRight">{ci_language line="choose_ebay_categories"}</p>
                                                               
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <ul id="tree2" class=" tree_point tree-selectable"></ul> 
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-3"> 
                                        <div class="tree clearfix custom-form">   
                                            <div class="">
                                                <div class="tree-unselectable">
                                                    <div class="tree-folder">	
                                                        <div class="tree-folder-header">
                                                            <i class="icon-folder"></i>	
                                                            <p class="treeHead treeRight">{ci_language line="choose_ebay_store_categories"}</p>
                                                                
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <ul id="tree3" class=" tree_point  tree-selectable"></ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-4"> 
                                        <div class="tree clearfix custom-form">  
                                            <div class="">
                                                <div class="tree-unselectable">
                                                    <div class="tree-folder">	
                                                        <div class="tree-folder-header">
                                                           <i class="icon-folder"></i>	
                                                            <p class="treeHead treeRight">{ci_language line="choose_ebay_store_categories"}</p>
                                                                
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <ul id="tree4" class="tree_point tree-selectable"></ul>
                                        </div>
                                    </div>
                                    <div class="clearfix center" id="page-load">
                                            <i class="fa fa-spinner fa-spin fa-3x orange"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        