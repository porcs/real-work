                        {if !isset($file)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
{*                        <div class="widget-head">
                            <h3 class="header-main header-half">{ci_language line="title_$current_page"}</h3>
                        </div>*}
                        <!-- // Widget Heading END -->
                        <div class="widget-body">                            
                            <h5 class="title-main title-bottom title-half">{ci_language line="default_templates"} {_get_tooltip_icon({ci_language line="templates_default_help"})}</h5>
                            <div class="form-group templates-main">
                                {*<div class="help-block text-small">{ci_language line="templates_default_help"}</div>*}
                                <div class="default-download align-left"><a class="btn btn-small btn-success"><i class="fa fa-download"></i> {ci_language line="download"}</a></div>
                            </div>
                            <div class="form-group templates-main {$field_hide}">
                                <div class="headSettings clearfix b-Bottom p-0">
                                    <h5 class="title-main col-md-3 col-sm-3">{ci_language line="custom_templates"} {_get_tooltip_icon({ci_language line="templates_custom_help"})}</h5>
                                    <ul class="pull-sm-right treeSettings">
                                        <li class="file-download blue"><i class="fa fa-download"></i> {ci_language line="download"}</li>
                                        <li class="file-deleted bootbox-confirm red"><i class="fa fa-trash"></i> {ci_language line="delete_only"}</li>
                                        <li class="file-preview green"><i class="fa fa-search"></i> {ci_language line="preview_only"}</li>
                                    </ul>
                                </div>
                                {*<div class="help-block text-small">{ci_language line="templates_custom_help"}</div>*}
                                <div id='custom-templates' class="{$field_hide}">
                                    <div class="list-templates">
                                        <div class="form-group">
                                            <div class="checkbox col-md-12">
                                                <label class="col-md-12 col-xs-12 radio radio-custom">
                                                    <i class="fa fa-fw fa-square-o"></i>
                                                    <input class='custom-name' name="custom-name" type="radio" value="">
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                        {if isset($file) }
                                        {foreach from = $file item = file_list }
                                        <div class="form-group">
                                            <div class="checkbox col-md-12">
                                                <label class="col-md-12 col-xs-12 radio radio-custom">
                                                    <i class="fa fa-fw fa-square-o"></i>
                                                    <input class='custom-name' name="custom-name" type="radio" value="{if !empty($file_list)}{$file_list}{/if}">
                                                    <span class="lbl"> {$file_list}</span>
                                                </label>
                                            </div>
                                        </div>
                                        {*<div class="form-group">
                                            <div class="col-md-12 form-group templates-main" for="form-file">
                                                <label class="control-label">{$file_list}</label>
                                                <div class="col-md-12">
                                                <a class="btn btn-primary file-download"><i class="fa fa-download"></i> {ci_language line="download"}</a>
                                                <a class="btn btn-danger file-deleted bootbox-confirm"><i class="fa fa-trash"></i> {ci_language line="delete_only"}</a>
                                                <a class="btn btn-inverse file-preview"><i class="fa fa-search"></i> {ci_language line="preview_only"}</a>
                                                </div>
                                            </div>
                                        </div>*}
                                        {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group templates-main">
                                <h5 class="title-main title-bottom title-half">{ci_language line="add_update_template"} {_get_tooltip_icon({ci_language line="synchronization_finish_help"})}</h5>
                                {*<div class="help-block text-small">{ci_language line="synchronization_finish_help"}</div>*}
                                <div class="add-templates align-left">
                                    <div class="form-group">
                                        <label class="control-label" for="form-file">{ci_language line="file_only"}</label>
                                        <div class="col-md-10">
                                            <div class="col-md-9 col-xs-8">
                                                <select id="form-file" name='form-file' class="form-file">
                                                    <option value="0" disabled selected>{ci_language line="choose_file"}</option>
                                                    <option value="New">{ci_language line="new_only"}</option>
                                                    {if isset($file) }
                                                    {foreach from = $file item = file_list }
                                                    <option value="{$file_list}">Edit > {$file_list}</option>
                                                    {/foreach}
                                                    {/if}
                                                </select>
                                                <div>
                                                    <input id="FileInput" name="FileInput" type="file" multiple="true">
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <button class="file-click btn btn-primary" type="submit" ><i class="fa fa-save"></i> {ci_language line="upload_template"}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>