                <!-- // Widget Heading END -->
                <div class="widget-head">
                    <h3 class="header-main header-full header-bottom">{ci_language line="create_your_condition"}</h3>
                </div>
                <div class="widget-body">
                    <div class="widget widget-tabs widget-tabs-responsive">
                        {if !empty($condition_values)}
                        {include file="ebay/configuration_advance_tab.tpl"}
                        <div class="widget-body padding-none">
                            <div class="tab-content">
                                {include file="ebay/main_info.tpl"}
                                <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 1}active{/if}" id="tab-1">
                                    <div class="widget-body padding-none">
                                        <div class="condition-pane">
                                            <h5 class="title-main title-bottom title-half">{ci_language line="condition_setting"}</h5>
                                            <div class="widget-main">
                                                <div class="form-group condition-main">
                                                    <label class="control-label" for="conditions">{ci_language line="condition_id"} {_get_tooltip_icon({ci_language line="condition_help"})}</label>
                                                    {*{if isset($shop_condition)}{foreach from = $shop_condition key = shop_key item = condition}
                                                    <div class="col-md-12 condition-main-values">
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <input readonly class="form-control" type="text" id="condition-values-{$shop_key}" name="condition-values-mapping" data-value="{if !empty($condition['txt'])}{$condition['txt']}{/if}" placeholder="{if !empty($condition['txt'])}{ucfirst(strtolower($condition['txt']))} ( {ci_language line="host_url"} ){/if}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-md-1">
                                                            <div class="col-md-12 text-center angle-right">
                                                                <i class="fa fa-angle-right"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <select class="condition-values" id='condition-values-select-{$shop_key}' name='condition-values-mapping'>
                                                                    {if isset($condition_values)}{foreach from = $condition_values item = condition_value}
                                                                            <option value="{$condition_value['condition_value']}" {if empty($condition['condition_value']) && !empty($condition_value['condition_name']) && $condition['txt'] == strtolower($condition_value['condition_name'])}selected='selected'{elseif !empty($condition_value['condition_value']) && !empty($condition['condition_value']) && $condition_value['condition_value'] == $condition['condition_value']}selected='selected'{/if}>{$condition_value['condition_name']}</option>
                                                                    {/foreach}{/if}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/foreach}{/if}*}
                                                    {if isset($condition_values)}{foreach from = $condition_values key = shop_key item = condition_value}
                                                    <div class="col-md-12 condition-main-values">
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <input readonly class="form-control" type="text" id="condition-values-{$shop_key}" data-key="{$condition_value['condition_value']}" name="condition-values-mapping" data-value="{if !empty($condition_value['condition_name'])}{$condition_value['condition_name']}{/if}" placeholder="{if !empty($condition_value['condition_name'])}{ucfirst(strtolower($condition_value['condition_name']))}{/if}">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-1 col-md-1">
                                                            <div class="col-md-12 text-center angle-right">
                                                                <i class="fa fa-angle-right"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-5 col-md-5">
                                                            <div class="col-md-12">
                                                                <select class="condition-values" id='condition-values-select-{$shop_key}' name='condition-values-mapping'>
                                                                	<option value="0">{ci_language line="choose_a_condition"}</option>
                                                                    {if isset($shop_condition)}{foreach from = $shop_condition item = condition}
                                                                            <option value="{$condition['txt']}" {if empty($condition['condition_value']) && !empty($condition['txt']) && strtolower($condition['txt']) == strtolower($condition_value['condition_name'])}selected='selected'{elseif !empty($condition_value['condition_value']) && !empty($condition['condition_value']) && in_array($condition_value['condition_value'], $condition['condition_value'])}selected='selected'{/if}>{ucfirst($condition['txt'])} ( {ci_language line="host_url"} )</option>
                                                                    {/foreach}{/if}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {/foreach}{/if}
                                                    <div class='clear clearfix'></div>
                                                    {*<div class="help-block text-small">{ci_language line="condition_help"}</div>*}
                                                </div>
                                            </div>
                                            {if isset($condition_description)}<h5 class="title-main title-bottom title-half">{ci_language line="condition_details"}</h5>{/if}
                                            <div class="widget-main">
                                                {if isset($condition_description)}{foreach from = $condition_description key = condition_description_key item = condition_description_value}
                                                <div class="form-group condition-main">
                                                    <label class="control-label" for="condition-description">{ci_language line="condition_description"} ( {ci_language line="{strtolower($condition_description_value['condition_name'])}"} ) {_get_tooltip_icon({ci_language line="condition_{$condition_description_value['condition_name']}_help"})}</label>
                                                    <div class="col-md-12">
                                                        {*<div class="help-block text-small">{ci_language line="condition_{$condition_description_value['condition_name']}_help"}</div>*}
                                                        <div class="col-md-12">
                                                            <textarea id='condition-description-{$condition_description_value['condition_name']}' name='condition-description' class="condition-description">{if isset($condition_description_value['condition_description'])}{$condition_description_value['condition_description']}{/if}</textarea>
                                                        </div>
                                                        <div class="help-block red help-clear countdown-textarea">{ci_language line="remaining"} : 1000</div>
                                                        <div class="help-block text-small">{ci_language line="condition_description_max_help"}</div>
                                                    </div>
                                                </div>
                                                {/foreach}{/if}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 2}active{/if}" id="tab-2">
                                    <div class="widget-body padding-none">
                                        <div class="condition-pane">
                                            <h5 class="title-main title-bottom title-half">{ci_language line="condition_setting"}</h5>
                                            <div class="widget-main">
                                                <div class="form-group condition-main">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6 form-group">
                                                            <label class="control-label" for="conditions">{ci_language line="condition_id"} {_get_tooltip_icon({ci_language line="condition_help"})}</label>
                                                            <div class="col-md-10">
                                                                <select class="condition-values" id='condition-values-select' name='condition-values'>
                                                                    {if isset($condition_values)}{foreach from = $condition_values item = condition_value}
                                                                            <option value="{$condition_value['condition_value']}" {if !empty($profile_data['condition_value']) && !empty($profile_data['condition_value']) && $condition_value['condition_value'] == $profile_data['condition_value']}selected='selected'{/if}>{$condition_value['condition_name']}</option>
                                                                    {/foreach}{/if}
                                                                </select>
                                                                {*<div class="help-block text-small">{ci_language line="condition_help"}</div>*}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5 class="title-main title-bottom title-half">{ci_language line="condition_details"}</h5>
                                            <div class="widget-main">
                                                <div class="form-group condition-description-main">
                                                    <label class="control-label" for="condition-description">{ci_language line="condition_description"} {_get_tooltip_icon({ci_language line="condition_description_help"})}</label>
                                                    <div class="col-md-12">
                                                        {*<div class="help-block text-small">{ci_language line="condition_description_help"}</div>*}
                                                        <span class="col-md-12 condition-description-span">
                                                            <textarea id='condition-description' name='condition-description' class="condition-description">{if isset($profile_data['condition_description'])}{$profile_data['condition_description']}{/if}</textarea>
                                                        </span>
                                                        <div class="help-block red help-clear countdown-textarea">{ci_language line="remaining"} : 1000</div>
                                                        <div class="help-block text-small">{ci_language line="condition_description_max_help"}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {else}
                            <div class="notFound">
                                <div class="notFound_reason">
                                        <ul class="main_reason">
                                        	{if $error_code == "mapping_not_found"}
                                                <li>{ci_language line="mapping_not_found"}</li>
                                            {elseif $error_code == "condition_not_found"}
                                                <li>{ci_language line="condition_not_found"}</li>
                                            {/if}
                                        </ul>
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
