    <!--Attribute--> 
    <div id="attribute-mapping" class="hide" ng-controller="VariationValue" >     
        <div ng-repeat = "data in appData">
            <div class="row" id="row_varian_[[data.id_attribute_group]]" >
                <div class="col-xs-12">
                    <div class="head_text clearfix p-t15 p-b15">
                        <p class="pull-left m-0">[[data.name]]</p>
                        <p class="pull-right m-0">[[mappingVariationString[data.id_attribute_group].name]]</p>
                    </div>
                </div>
            </div>
            <div class="row text-center p-t15 b-Bottom">
                <div class="col-xs-12">
                    <p class="poor-gray">{ci_language line="This attribute provide the free text. You can use this value for"}
                    <a class="link attr-link" id="opt_[[mappingData[data.id_attribute_group].label]]_[[data.id_attribute_group]]"
                       ng-init="checkMapping(data.id_attribute_group, data)" 
                       ng-click="switchVarianOption(data.id_attribute_group, mappingData[data.id_attribute_group].inverse);">
                        {ci_language line="[[mappingData[data.id_attribute_group].decription]]"}
                    </a>
                    for [[data.name]]/[[data.attribute_name === null ? data.name : data.attribute_name]].</p>
                </div>
            </div>
                    
            <div class="row attribute-rows" id="row_mapping_[[data.id_attribute_group]]" ng-show=!showDefault[data.id_attribute_group] >
                <div class="col-xs-12">
                    <div class="p-t15 b-Bottom">
                        <div class="row attribute-row" data-group="[[data.id_attribute_group]]" >
                            <div class="col-sm-6 m-b20">
                                <input class="form-control fixs-row" type="text" readonly data-attrid="[[data.id_attribute_group]]" value="[[data.name]]">
                                <i class="blockRight noBorder"></i>
                            </div>
                            <div class="col-sm-6">     
                                <input class="form-control ebay-row" type="text" ng-init="addVariationValue(data.id_attribute_group, data)" ng-model="mappingVariationString[data.id_attribute_group].name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type='hidden' id='push_mapping' value='{ci_language line="mapping a new value"}'>
    <input type='hidden' id='push_default' value='{ci_language line="use this value"}'>

    <script type="text/javascript">
        var app_data = {$app_data};
    </script>