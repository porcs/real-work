<div class="tree-folder">				
    <div class="tree-folder-header"  >
        {if isset($type) && $type == "folder"}
            <i class="icon-minus add_category"></i>
        {/if}
        <div class="tree-folder-name">
            <label class="checkbox-custom">
                <i class="fa fa-fw fa-square-o"></i>
                <input type="checkbox" rel="id_category" name="category[{$id_category}][id_category]" id="category_{$id_category}" value="{$id_category}" class="checbox-tree-folder" />
                <script>
                    {if isset($checked) && ($checked == true)} 
                        $('#category_{$id_category}').checkbox('check');
                    {else} 
                        $('#category_{$id_category}').checkbox('uncheck');
                    {/if}
                </script>
                <span class="lbl">{$name}</span>
            </label>
                {if (isset($selected) && !empty($selected))}
                    <div class="pull-right">
                        <select class="select-main col-xs-10" name="category[{$id_category}][id_profile]" id="category{$id_category}_id_profile">
                            <option value="0">{ci_language line="default_template"}</option>
                            {if (isset($templates) && !empty($templates) )}
                                {foreach from=$templates item=fvalue}
                                    <option value="{$fvalue}" {if isset($selected) && $selected == $fvalue} selected {/if}>{$fvalue}</option>
                                {/foreach}  
                            {/if}
                        </select>
                        <i class="fa fa-angle-double-down cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                    </div>
                {else}
                    {if isset($checked)&& ($checked == true)  }
                        <div class="pull-right">
                            <select class="select-main col-xs-10" name="category[{$id_category}][id_profile]" id="category{$id_category}_id_profile">
                            <option value="0">{ci_language line="default_template"}</option>
                            {if (isset($templates) && !empty($templates) )}
                                {foreach from=$templates item=fvalue}
                                    <option value="{$fvalue}">{$fvalue}</option>
                                {/foreach}  
                            {/if}
                        </select>
                            <i class="fa fa-angle-double-down cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                        </div>
                    {/if}
                {/if}
        </div>
    </div>				
    <div class="tree-folder-content">{if (isset($sub_html) && !empty($sub_html) )}{$sub_html}{/if}</div>
</div>    