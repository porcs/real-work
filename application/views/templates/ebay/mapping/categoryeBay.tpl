{if isset($root) && $root == 'true'}
<div class="widget-body categories_searches">
    <div class="widget-main form-group">
    <label class="control-label">{ci_language line="find_categories"}</label>
    <div class="col-md-12 form-group">
            <div class="col-md-12">
                <label class="categories_search">
                    <div class="help-block text-small">
                        {ci_language line="categories_search_help"}
                    </div>
                    <h5 class="input-group">
                        <input class="col-md-12 form-control" type="text" id="form-finder" name="finder">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-finder"><i class="fa fa-search"></i>{ci_language line="search"}</button>
                        </div>
                    </h5>
                </label>
            </div>
    </div>
    <ul id="finder-result"></ul>
    </div>
</div>
<div class="widget-main form-group">
<label class="control-label">{ci_language line="list_categories"}</label>
<div id="back-bar" class="col-md-12" rel='{$parent_id}' data-name='{$parent_name}'><a>Back</a></div>
<div id="breadcrumbs">
    <ol>
        {if !empty($breadcrumbs)}{foreach from = $breadcrumbs key = breadcrumbs_key item = breadcrumbs_item}
        <li><a id="category-{$breadcrumbs_key}" >{$breadcrumbs_item}</a></li>
        {/foreach}{/if}
    </ol>
</div>
<div id="category-list" class="col-md-12" data-level='{if isset($type)}{$type}{/if}'>
{/if}
{if !isset($error) || $error != 'true'}
{if isset($root) && $root == 'true'}
    <ol>
{/if}
        {if !empty($sub_html)}
                {$sub_html}
        {elseif isset($profile) && $profile == 'true'}
        <li><a id="category-{$id_category}" class="category">{$name}<span class="category-id"> ({$id_category}) </span>{if !empty($child)}<span class="leaf"> »</span>{else}<span class="leaf"> <i class="fa fa-check-circle green"></i></span>{/if}</a></li>
        {/if}
{if isset($root) && $root == 'true'}
    </ol>
</div>
{/if}
{else}
    {if !empty($sub_html)}
        <div class="categories-error">{$sub_html}</div>
    </div>
    {/if}
{/if}