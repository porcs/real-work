<li class="tree-folder-name">
    
	<span class="tree_item">
		<p>
			{if isset($type) && $type == "folder"}
				<i class="cb-plus good active add_category"></i>	
			{/if}								
			<label class="cb-checkbox cb-sm">											
			 	<input type="checkbox" rel="id_category" name="category[{$id_category}][id_category]" class="category_{$id_category} checbox-tree-folder" value="{$id_category}"   />
                                <span class="lbl">{$name}</span>
			</label>
		</p>
	</span>

	{if (isset($selected) && !empty($selected))}
		<div class="treeRight selectTree">
			<div class="tree_show" style="display: block;">
				<input type="text" class="select-main col-xs-10" name="category[{$id_category}][id_profile]" class="category{$id_category}_id_profile_check" readonly  value="{$selected_name}" rel="{$id_category}" data-value="{$selected_name}" data-key="{$selected}">
                                <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
			</div>
		</div>
        {elseif (isset($switch) && !empty($switch))}
		<div class="treeRight selectTree">
                        <div class="tree_show" style="display: block;">
                                <input type="text" class="select-main col-xs-10" name="category[{$id_category}][id_profile]" class="category{$id_category}_id_profile_check" readonly value="{ci_language line="choose_ebay_categories"}" rel="{$id_category}" data-value="0" data-key="">
                                <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                        </div>
                </div>
        {else}
                    {if isset($checked)&& ($checked == true)  }
                        <div class="treeRight selectTree">
                                <div class="tree_show" style="display: block;">
                                        <input type="text" class="select-main col-xs-10" name="category[{$id_category}][id_profile]" class="category{$id_category}_id_profile_check" readonly value="{ci_language line="choose_ebay_categories"}" rel="{$id_category}" data-value="0" data-key="">
                                        <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                                </div>
                        </div>
                         
                    {/if}
	{/if}

	{if (isset($sub_html) && !empty($sub_html) )}
		<ul class="tree_child" style="display: block;">
			{$sub_html}
		</ul>
	{/if}
</li>