<div class="p-rl40 p-xs-rl20 m-t30 p-t10">

	<!-- Head -->
	<h1
		class="lightRoboto text-uc head-size light-gray b-Bottom p-b10 m-b20">
		Attribute Override</h1>
		
	<form id="amazon-error-resolation" method="post"
		enctype="multipart/form-data" autocomplete="off"
		novalidate="novalidate">
		<div class="attribute row">
			{$currentProduct = 0}
			{foreach from=$products key=productID item=product}
				{foreach from=$product key=combinationID item=combination}
				<div class="col-xs-12">
					<div class="head_text clearfix p-t15 p-b10 m-b20">
						<input type="hidden" rel="key" value="J801_C7_Rose_T36">
						<p class="m-0">
							{if $currentProduct != $productID}
								<span class="title-main">{$combination['product']} - {$combination['sku_product']}</span>
								{$currentProduct = $productID}
								<span class="label label-warning">{$productID}</span>
							{/if}
							
							{if $combinationID > 0}
								- {$combination['sku']}<span class="label label-warning"> {$combinationID}</span>
							{/if}
						</p>
					</div>
				</div>
				<div class="row attribute-rows">
					<div class="col-xs-12">
						<div class="p-t15 b-Bottom body-override" product="{$productID}" combination="{$combinationID}">
						
						</div>
					</div>
				</div>
				{/foreach}
			{/foreach}
			



			<div class="row action">
				<div class="col-xs-12">
					<div class="b-Top p-t20">
						<div class="inline pull-right">
							<!--Message-->
							<input type="hidden" id="country" value="France"> <input
								type="hidden" id="ext" name="ext" value=".fr">
							<button class="btn btn-save" id="save_data" type="submit" onclick="save_override()">Save</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	<script>
	var override_datasource = {json_encode($products)};
	</script>
	
							<div class="row attribute-row hide" id="row_template">
								<div class="col-sm-6 m-b20">
									{if (isset($attribute_group))}
									<select class="dd-ebay-attribute">
										{foreach $attribute_group as $attribute_group_list}
											{foreach $attribute_group_list as $attribute_group_value}
											<option value="{$attribute_group_value['id_group']}">{$attribute_group_value['name']}</option>
											{/foreach}
										{/foreach}
									</select> <i class="blockRight noBorder"></i>
									{/if}
								</div>
								<div class="col-sm-5">
									<input type="text" class="form-control" name="" />
								</div>
								<div class="col-sm-1">
                                    <i class="cb-plus addSpecificField"></i>
                        		</div>
							</div>
</div>