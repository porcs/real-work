<div id="sync_match" class="row m-b40 p-b20" ng-controller="productCtl">
    <div id="ebay-automaton-matching-header" class="row">
        <div id="tree1" class="col-xs-12">
            <div class="headSettings clearfix b-Bottom b-Top-None p-0">
                <h4 class="headSettings_head pull-md-left p-t10 m-b0">{ci_language line="Separate combinations"}</h4>
                <div class="wizardSettings pull-md-right">
                    <div class="pull-md-left">
                        {*<label>
                        <i class="fa fa-eye fa-2x"></i> Hidden Notification
                        </label>*}
                        {*<label class="cb-checkbox" ng-click="checkAll()">
                        <input type="checkbox" id="matching-display-selectall" ng-change="selectAll()" ng-model="statusSelectAll" />
                        {ci_language line="Select all"}
                        </label>*}
                        <label class="checkBa" ng-click="separateItemAll()">
                            <input type="checkbox" id="separate-item-selectall" />
                            <label for="separate-item-selectall">{ci_language line="Separate items all"}</label>
                        </label>
                    </div>                                  
                    <ul class="wizardSettings_list pull-md-right">
                        <li class="wizardSettings_confirm" ng-click="onConfirm()">
                            {ci_language line="Confirm"}
                        </li>
                        {*<li class="wizardSettings_reject" ng-click="onReject()">
                        {ci_language line="Reject"}
                        </li>
                        <li class="wizardSettings_send" ng-click="onSend()">
                        {ci_language line="Send"}
                        </li>*}
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--MODEL-->
    <div id="ebay-automaton-matching-product-model" class="hide m-t15">
        {literal}<div ng-repeat="item in items" class="row p-0 col-md-12 productitem" ng-class="{disable:item.notification.type == 'error'}" ng-hide="item.isConfirm">{/literal}
            <div class="row p-0 col-xs-4 col-sm-2 col-md-1 m-t2 m-b2"><a ng-click="openLightboxModal($index,item)"><img {*ng-src="[[imageItem(item)]]"*} class="picture" width="65" height="65"></a></div>
            <div class="col-xs-8 col-sm-10 col-md-11">
                <div class="col-sm-5">
                    <h4 class="true-blue m-t20">[[item.name]]</h4>
                    <p class="poor-gray">[[item.title]]</p>
                    <p class="poor-gray" ng-if="!item.isGroup">[[item.price]]</p>
                    {*<p class="poor-gray" ng-if="item.isGroup">[[item.balance]]</p>*}
                </div>
                <div class="col-sm-2">
                    <div class="col-md-12 text-right">
                        <label class="poor-gray m-t20">[[counterCombination(item)]] {ci_language line="items"}</label>                        
                    </div>
                </div>
                <div class="col-sm-5" ng-if="item.notification.type == 'error'">
                    <h2 class="warning-text"><i class="fa fa-exclamation-triangle"></i> {ci_language line="Notification"}</h2>
                    <p class="true-pink">[[item.notification.msg]]</p>
                </div>
                <div class="col-sm-5 text-right separate" ng-if="item.notification.type != 'error'">
                    {*<h2 class="poor-gray"><i class="fa fa-bars"></i> Menu</h2>*}
                    <div class="col-md-12 form-group">
                        <label class="checkBa m-t20" ng-if="item.notification.type != 'error'">
                            <input type="checkbox" name="separate_item_[[item._id]]" id="separate_item_[[item._id]]" ng-model="item.separateItem" /> 
                            <label for="separate_item_[[item._id]]">{ci_language line="Separate Item"}</label>
                        </label>
                        {*<a class="btn btn-default" ng-if="item.notification.type != 'error'" ng-click="addGroup($event,true)">[[item.notification.title]]([[item.comanation.length]])</a>
                        <a class="btn btn-default" ng-if="item.notification.type != 'error'" ng-click="autoGenerate()">Auto Generator : [[item.notification.title]]([[item.comanation.length]])</a>*}
                    </div>
                    <div class="col-md-12 withIcon" ng-show="item.separate" ng-if="item.notification.type != 'error'" ng-repeat="itemcomanation in item.comanation_select track by $index">
                        <div class="col-md-12">
                            <label class="col-xs-2">Group [[$index + 1]]</label>
                            <div class="col-xs-10 form-group withIcon">
                                {*<div>
                                Min Price : [[item.pricemin[$index]|number]] | Max Price : [[item.pricemax[$index]|number]] == % : [[item.pricecentage[$index]|number]] 
                                <span class="true-green" ng-if="item.pricecentage[$index]<=4">TRUE</span>
                                <span class="true-pink" ng-if="item.pricecentage[$index]>4">FALSE</span>
                                </div>*}
                                <script type="text/ng-template" id="browser_[[item._id]]_[[$index]]">
                                    [[option.label]] <span>$[[option.price|number]]</span>
                                </script>
                                <select class="row col-md-12"
                                        selector
                                        multi="true"
                                        model="item.comanation_select[$index]"
                                        options="item.comanation_clone[$index]"
                                        ng-click="comanationOption(item.comanation)"
                                        ng-init="comanationOption(item.comanation)"
                                        value-attr="id"
                                        view-item-template="'browser_[[item._id]]_[[$index]]'"
                                        dropdown-item-template="'browser_[[item._id]]_[[$index]]'"></select>
                                <i class="" ng-class="$last?'cb-plus good':'cb-minus bad'" ng-click="$last?addGroup($event,false):removeGroup($event, itemcomanation)"></i>
                            </div>
                            {*<p>Current value: <code ng-bind="item.comanation_select[$index] | json"></code></p>*}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {literal}
        <style type="text/css">
            .lightbox-nav{position:relative;margin-bottom:12px;height:22px;text-align:center;font-size:0}.lightbox-nav .btn-group{vertical-align:top}.lightbox-nav .close{position:absolute;top:0;right:0}.lightbox-image-container{position:relative;text-align:center}.lightbox-image-caption{position:absolute;top:0;left:0;margin:.5em .9em;color:#000;font-size:1.5em;font-weight:700;text-align:left;text-shadow:.1em .1em .2em rgba(255,255,255,.5)}.lightbox-image-caption span{padding-top:.1em;padding-bottom:.1em;background-color:rgba(255,255,255,.75);box-shadow:.4em 0 0 rgba(255,255,255,.75),-.4em 0 0 rgba(255,255,255,.75)}
            
            .picture{
                width: 65px;
                height: 65px;
                cursor: pointer;
                background: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+DQo8c3ZnDQogICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iDQogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIg0KICAgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIg0KICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyINCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCINCiAgIHhtbG5zOmlua3NjYXBlPSJodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy9uYW1lc3BhY2VzL2lua3NjYXBlIg0KICAgd2lkdGg9IjY1Ig0KICAgaGVpZ2h0PSI2NSINCiAgIHZpZXdCb3g9IjAgMCA2NSA2NSINCiAgIGlkPSJzdmc0MTM2Ig0KICAgdmVyc2lvbj0iMS4xIg0KICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45MSByMTM3MjUiDQogICBzb2RpcG9kaTpkb2NuYW1lPSJjYW1hcmEuc3ZnIj4NCiAgPG1ldGFkYXRhDQogICAgIGlkPSJtZXRhZGF0YTQxNDQiPg0KICAgIDxyZGY6UkRGPg0KICAgICAgPGNjOldvcmsNCiAgICAgICAgIHJkZjphYm91dD0iIj4NCiAgICAgICAgPGRjOmZvcm1hdD5pbWFnZS9zdmcreG1sPC9kYzpmb3JtYXQ+DQogICAgICAgIDxkYzp0eXBlDQogICAgICAgICAgIHJkZjpyZXNvdXJjZT0iaHR0cDovL3B1cmwub3JnL2RjL2RjbWl0eXBlL1N0aWxsSW1hZ2UiIC8+DQogICAgICAgIDxkYzp0aXRsZSAvPg0KICAgICAgPC9jYzpXb3JrPg0KICAgIDwvcmRmOlJERj4NCiAgPC9tZXRhZGF0YT4NCiAgPGRlZnMNCiAgICAgaWQ9ImRlZnM0MTQyIiAvPg0KICA8c29kaXBvZGk6bmFtZWR2aWV3DQogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiINCiAgICAgYm9yZGVyY29sb3I9IiM2NjY2NjYiDQogICAgIGJvcmRlcm9wYWNpdHk9IjEiDQogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiDQogICAgIGdyaWR0b2xlcmFuY2U9IjEwIg0KICAgICBndWlkZXRvbGVyYW5jZT0iMTAiDQogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIg0KICAgICBpbmtzY2FwZTpwYWdlc2hhZG93PSIyIg0KICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE4NTgiDQogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwNTgiDQogICAgIGlkPSJuYW1lZHZpZXc0MTQwIg0KICAgICBzaG93Z3JpZD0iZmFsc2UiDQogICAgIGlua3NjYXBlOnpvb209IjYuODUzODQ2MiINCiAgICAgaW5rc2NhcGU6Y3g9IjY5LjUxMjM3MyINCiAgICAgaW5rc2NhcGU6Y3k9IjIuMzcxNzUyNSINCiAgICAgaW5rc2NhcGU6d2luZG93LXg9IjE5NzQiDQogICAgIGlua3NjYXBlOndpbmRvdy15PSItOCINCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSINCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnNDEzNiIgLz4NCiAgPHBhdGgNCiAgICAgZD0ibSAzMi41MjI5NjksMjAuMzcyODUzIGMgLTAuOTQwMzc5LDAgLTEuOTI0NDU0LDAuNzQzNjE3IC0yLjMyMjE2NiwxLjc2NjE1NSBsIC0wLjMyNzA2NiwwLjg1MDM3MSAtMi43ODAwNTgsMCAwLC0wLjUyMzMwNSBjIDAsLTAuMjg5Mzg4IC0wLjIzMzkxNywtMC41MjMzMDUgLTAuNTIzMzA1LC0wLjUyMzMwNSBsIC0zLjY2MzEzNiwwIGMgLTAuMjg5Mzg3LDAgLTAuNTIzMzA1LDAuMjMzOTE3IC0wLjUyMzMwNSwwLjUyMzMwNSBsIDAsMC42NzA0ODQgYyAtMS41Mzk1NjMsMC40NTQ3NTIgLTIuNjE2NTI1LDEuODczODI1IC0yLjYxNjUyNSwzLjUxNTk1NiBsIDAsMTAuOTg5NDA3IGMgMCwyLjAxOTQzNCAxLjY0MzcwMSwzLjY2MzEzNiAzLjY2MzEzNSwzLjY2MzEzNiBsIDE4LjgzODk4NCwwIGMgMi4wMTk0MzQsMCAzLjY2MzEzNSwtMS42NDM3MDIgMy42NjMxMzUsLTMuNjYzMTM2IGwgMCwtMTAuOTg5NDA3IGMgMCwtMi4wMTk0MzQgLTEuNjQzNzAxLC0zLjY2MzEzNSAtMy42NjMxMzUsLTMuNjYzMTM1IGwgLTEuMjEwMTQzLDAgLTAuMzI3MDY2LC0wLjg1MDM3MSBjIC0wLjM5NzcxMiwtMS4wMjMwNjEgLTEuMzgxNzg3LC0xLjc2NjE1NSAtMi4zMjIxNjYsLTEuNzY2MTU1IGwgLTUuODg3MTgzLDAgeiBtIDIuOTQzNTkxLDQuNzA5NzQ2IGMgMy43NTEwNTEsMCA2LjgwMjk2NywzLjA1MTkxNSA2LjgwMjk2Nyw2LjgwMjk2NiAwLDMuNzUxMDUxIC0zLjA1MTkxNiw2LjgwMjk2NiAtNi44MDI5NjcsNi44MDI5NjYgLTMuNzUxMDUsMCAtNi44MDI5NjYsLTMuMDUxOTE1IC02LjgwMjk2NiwtNi44MDI5NjYgMCwtMy43NTEwNTEgMy4wNTE5MTYsLTYuODAyOTY2IDYuODAyOTY2LC02LjgwMjk2NiB6IG0gLTEyLjAzNjAxNywwLjUyMzMwNSBjIDAuNTc3NzI5LDAgMS4wNDY2MTEsMC40Njg4ODIgMS4wNDY2MTEsMS4wNDY2MSAwLDAuNTc3NzI5IC0wLjQ2ODg4MiwxLjA0NjYxIC0xLjA0NjYxMSwxLjA0NjYxIC0wLjU3ODI1MiwwIC0xLjA0NjYxLC0wLjQ2ODg4MSAtMS4wNDY2MSwtMS4wNDY2MSAwLC0wLjU3NzcyOCAwLjQ2ODM1OCwtMS4wNDY2MSAxLjA0NjYxLC0xLjA0NjYxIHogbSAxMi4wMzYwMTcsMC41MjMzMDUgYyAtMy4xNzM4NDUsMCAtNS43NTYzNTYsMi41ODI1MTEgLTUuNzU2MzU2LDUuNzU2MzU2IDAsMy4xNzM4NDYgMi41ODI1MTEsNS43NTYzNTYgNS43NTYzNTYsNS43NTYzNTYgMy4xNzM4NDYsMCA1Ljc1NjM1NiwtMi41ODI1MSA1Ljc1NjM1NiwtNS43NTYzNTYgMCwtMy4xNzM4NDUgLTIuNTgyNTEsLTUuNzU2MzU2IC01Ljc1NjM1NiwtNS43NTYzNTYgeiINCiAgICAgaWQ9InBhdGg0MTM4Ig0KICAgICBzdHlsZT0iZmlsbDojZTJlMmUyO2ZpbGwtb3BhY2l0eTowLjk0MTE3NjQ3Ig0KICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIiAvPg0KPC9zdmc+DQo=") no-repeat center center;
            }
            
            @media screen and (min-width: 977px) {
                .separate.col-sm-5.text-right {
                    padding-right: 115px;
                }
            }
            
            @-webkit-keyframes selector-rotate{0%{-webkit-transform:rotateZ(-359deg)}100%{-webkit-transform:rotateZ(0)}}@-moz-keyframes selector-rotate{0%{-moz-transform:rotateZ(-359deg)}100%{-moz-transform:rotateZ(0)}}@-o-keyframes selector-rotate{0%{-o-transform:rotateZ(-359deg)}100%{-o-transform:rotateZ(0)}}@keyframes selector-rotate{0%{transform:rotateZ(-359deg)}100%{transform:rotateZ(0)}}[selector]{display:none;position:relative}[selector].selector{display:block}.selector{font-size:1em;line-height:normal;color:#495c68;/* text-shadow:0 1px 0 rgba(255,255,255,.5) */}.selector-input{display:block;margin:0;position:relative;width:100%;padding:.8em 2.6em .7em .7em;overflow:hidden;cursor:pointer;border:1px solid #bbb;-webkit-box-shadow:0 1px 0 rgba(0,0,0,.05),inset 0 1px 0 rgba(255,255,255,.8);box-shadow:0 1px 0 rgba(0,0,0,.05),inset 0 1px 0 rgba(255,255,255,.8);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;/* -webkit-border-radius:4px; *//* border-radius:4px; *//* font-weight:inherit; *//* background-color:#f9f9f9; *//* background-image:-webkit-linear-gradient(#fafafa,#eee); */background-image:-moz-linear-gradient(#fafafa,#eee);background-image:-ms-linear-gradient(#fafafa,#eee);background-image:-o-linear-gradient(#fafafa,#eee);/* background-image:linear-gradient(#fafafa,#eee) */}.selector.rtl .selector-input{padding-right:.7em;padding-left:2.6em}.selector .selector-values{list-style:none}.selector input{outline:0;background:0 0!important;border-color:transparent!important;width:2px;cursor:pointer}.selector.has-value input,.selector.open input{cursor:text}.selector.disabled{opacity:.6}.selector.remove-button.has-value .selector-input{padding-right:3.5em}.selector.remove-button.has-value.rtl .selector-input{padding-right:.7em;padding-left:3.5em}.selector.open .selector-input{background:0 0;border-bottom-color:#f0f0f0;padding-right:2.6em;-webkit-border-radius:4px 4px 0 0;-moz-border-radius:4px 4px 0 0;border-radius:4px 4px 0 0}.selector.open.rtl .selector-input{padding-right:.7em;padding-left:2.6em}.selector.open.empty .selector-input{-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;border-bottom-color:#bbb}.selector.multiple.rtl .selector-values,.selector.multiple.rtl input{float:right}.selector .selector-values,.selector .selector-values>li,.selector .selector-values>li>div,.selector input{padding:0;margin:0;border:0;display:inline}.selector.multiple .selector-input{padding:.5em .55em .2em!important;cursor:text;background:#fff;-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.1);box-shadow:inset 0 1px 1px rgba(0,0,0,.1)}.selector.multiple.has-value .selector-input{padding-left:.35em!important;padding-right:.35em!important}.selector.multiple .selector-values{float:none}.selector.multiple .selector-values>li,.selector.multiple input{padding:.2em .6em;margin:0 .15em .25em;border-width:1px;border-style:solid;float:left;line-height:normal}.selector.multiple .selector-values>li{display:inline-block;position:relative;border-color:#0987d6;color:#fff;/* text-shadow:0 1px 1px rgba(0,0,0,.2); */font-weight:300;/* -webkit-border-radius:3px; *//* border-radius:3px; *//* box-shadow:inset 0 1px 1px rgba(255,255,255,.5),0 1px 1px rgba(0,0,0,.2); */background-color: #4693fe;}.selector.multiple.remove-button .selector-values>li{padding-right:1.9em}.selector.multiple.rtl .selector-values>li,.selector.multiple.rtl input{float:right}.selector.multiple.rtl.remove-button .selector-values>li{padding-right:.6em;padding-left:1.9em}.selector-helper{position:absolute;display:block;width:2.6em;top:0;right:0;bottom:0}.selector.rtl .selector-helper{right:inherit;left:0}.selector-helper .selector-icon{display:block;position:relative;height:100%}.selector-helper .selector-icon:after{content:'';display:block;position:absolute;top:50%;left:50%;margin-top:-.1em;margin-left:-.4em;width:0;height:0;border:.4em solid #888;border-left-color:transparent;border-right-color:transparent;border-bottom:none}.selector.has-value.remove-button .selector-helper{border-left:1px solid #bbb}.selector.has-value.remove-button.rtl .selector-helper{border-left:none;border-right:1px solid #bbb}.selector.has-value.remove-button .selector-icon{display:table;width:100%}.selector.has-value.remove-button .selector-icon:after{content:'\00d7';display:table-cell;position:relative;top:0;left:0;margin:0;border:none;height:100%;text-align:center;vertical-align:middle}.selector.loading .selector-helper.selector-global-helper{width:1.3em;height:1.3em;margin:.65em .7em}.selector.loading .selector-global-helper .selector-icon{display:table;width:100%}.selector.loading .selector-global-helper .selector-icon:after{content:'';width:100%;height:100%;margin:0;top:0;left:0;opacity:.5;border-top:1px solid #545a6a;border-bottom:1px solid #d4d4db;border-left:1px solid #545a6a;border-right:1px solid #d4d4db;-webkit-animation:selector-rotate .5s linear infinite;-moz-animation:selector-rotate .5s linear infinite;-o-animation:selector-rotate .5s linear infinite;animation:selector-rotate .5s linear infinite;-webkit-border-radius:100%;-moz-border-radius:100%;border-radius:100%}.selector.loading .selector-global-helper,.selector.open .selector-global-helper{border-left:none!important;border-right:none!important}.selector.multiple .selector-helper{display:none}.selector.multiple.remove-button .selector-helper{display:block;width:1.3em;border-left:1px solid #0987d6;cursor:pointer}.selector.multiple.remove-button .selector-helper:hover{background:rgba(0,0,0,.1)}.selector.multiple.remove-button.rtl .selector-helper{border-left:none;border-right:1px solid #0987d6}.selector.multiple.loading .selector-input{padding-right:3em!important}.selector.multiple.loading .selector-global-helper{margin:.75em .8em;border-left:none;border-right:none}.selector.multiple.loading.rtl .selector-input{padding-right:.55em!important;padding-left:3em!important}.selector.multiple.loading.rtl.has-value .selector-input{padding-right:.25em!important}.selector-input input,.selector-shadow{padding-left:0!important;padding-right:0!important;border-left:0!important;border-right:0!important;max-width:100%!important}.selector-shadow{position:absolute;top:0;left:0;opacity:0;visibility:hidden;white-space:pre;margin:0}.selector-dropdown{display:none;list-style:none;padding:0!important;margin:0!important;position:absolute;background:#fff;border:1px solid #ccc;border-top:0;max-height:15.5em;overflow-x:hidden;overflow-y:auto;z-index:1000;-webkit-border-radius:0 0 3px 3px;-moz-border-radius:0 0 3px 3px;border-radius:0 0 3px 3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.1);box-shadow:0 1px 3px rgba(0,0,0,.1);-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.selector-dropdown>li{padding:.65em .8em;overflow:hidden;cursor:pointer}.selector-dropdown>.selector-optgroup{background:#fefefe;border-top:1px solid #f0f0f0;border-bottom:1px solid #f0f0f0}.selector-dropdown>.selector-option.grouped{padding-left:1.6em}.selector-dropdown>.selector-option.active{background:#f5fafd}.selector.open .selector-dropdown{display:block}

            .productitem{
                border-bottom: 1px solid #e2e2e2;
                list-style: none;
            }

            .productitem.active{
                background-color: #E6F0FF;
                border: 1px solid #4693fe;
            }

            .productitem.disable{
                -webkit-background-size: 50px 50px;
                -moz-background-size: 50px 50px;
                background-size: 50px 50px;
                cursor: not-allowed;
                background-image: -webkit-gradient(linear, left top, right bottom, color-stop(.25, rgba(255, 255, 255, .45)), color-stop(.25, transparent), color-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .45)), color-stop(.75, rgba(255, 255, 255, .45)), color-stop(.75, transparent), to(transparent));
                background-image: -webkit-linear-gradient(45deg, rgba(255, 255, 255, .45) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .45) 50%, rgba(255, 255, 255, .45) 75%, transparent 75%, transparent);
                background-image: -moz-linear-gradient(45deg, rgba(255, 255, 255, .45) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .45) 50%, rgba(255, 255, 255, .45) 75%, transparent 75%, transparent);
                background-image: -ms-linear-gradient(45deg, rgba(255, 255, 255, .45) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .45) 50%, rgba(255, 255, 255, .45) 75%, transparent 75%, transparent);
                background-image: -o-linear-gradient(45deg, rgba(255, 255, 255, .45) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .45) 50%, rgba(255, 255, 255, .45) 75%, transparent 75%, transparent);
                background-image: linear-gradient(45deg, rgba(255, 255, 255, .45) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .45) 50%, rgba(255, 255, 255, .45) 75%, transparent 75%, transparent);
                background-color: #eee;
                opacity: 0.7;
            }
        </style>
    {/literal}
    <script type="text/javascript">
        {literal}
            var json_data = {/literal}{if isset($json_data)}{$json_data}{else}[]{/if}{literal};
        {/literal}  
    </script>

</div>