{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/advance_import.css" />
<div id="content" class="main p-rl40 p-xs-rl20" ng-app="ebayApp" >
    <form class="form-horizontal" id="form-submit" method="POST" {if !empty($next_page)}action="{$next_page}"{/if} enctype='multipart/form-data'>
        <h1 class="lightRoboto text-uc head-size light-gray">
            <img class="ebay-logo" src="{$cdn_url}assets/images/EBay_logo.svg-Custom.png"> {ci_language line="header_$current_page"}
        </h1>
        {include file="breadcrumbs.tpl"}
        <div class="row">
            <div class="col-xs-12"> 
                <h3 class="head_text p-t10 b-Top m-b0 clearfix heading margin-none">
                    <img src="{$cdn_url}assets/images/flags/{$flag_country}.png" class="flag m-r10"> {$name_country}
                </h3> 
            </div>
        </div>
        <div class="innerAll spacing-x2">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget widget-inverse">
                        <div id="main"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    var jsonData = {$json_data};
</script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/jsx/{$current_page}/output/bundle.js"></script>
{include file="footer.tpl"}