{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/select2/select2.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/select2/select2.min.js"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/form/form_elements.min.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/fuelux-radio/fuelux-radio.js?v=v1.2.3"></script>
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/specific/checkBo/checkBo.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay_bootbox.css" />
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/bootbox/bootbox.min.js?v=v1.2.3"></script>
{*<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay.min.css" />*}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/ebay_import.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/{$current_page}.css" />
{*<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/advance_import.css" />
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/eBay/datatable_import.css" />*}
{if empty($root_page)}{ci_config name="root_page"}{/if}
{if !empty($validation_data)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/validate/jquery.validate.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/colorbox/jquery.colorbox-m.js"></script>
<div id="content" class="{if !isset($popup)}main {/if}p-rl40 p-xs-rl20" class="row">
    <form class="form-horizontal" id="form-submit" method="POST" {if !empty($next_page)}action="{$next_page}"{/if}  enctype='multipart/form-data'>
            <h1 class="lightRoboto text-uc head-size light-gray"><img class="ebay-logo" src="{$cdn_url}assets/images/EBay_logo.svg-Custom.png"> {ci_language line="header_$current_page"}</h1>

            {include file="breadcrumbs.tpl"} <!--breadcrumb-->
            <div class="row">
                <div class="col-xs-12"> 
                    <h3 class="head_text p-t10 b-Top m-b0 clearfix heading margin-none">
                        <img src="{$cdn_url}assets/images/flags/{$flag_country}.png" class="flag m-r10"> {$name_country}
                    </h3> 
                </div>
            </div>
            <div class="innerAll spacing-x2">
                <div id="success" class="alert alert-success"></div>
                <div id="warning" class="alert alert-warning"></div>
                <div id="error" class="alert alert-danger"></div>
                <div id="loading-result" class="alert alert-block">
                    <h4 class="smaller lighter grey">
                        <i class="fa fa-spinner fa-spin"></i> {ci_language line="importing"}
                    </h4>
                </div>
                {if isset($info)}
                <div class="row">
			<div class="col-xs-12">
				<div class="validate blue m-t20">
					<div class="validateRow">
						<div class="validateCell">
							<i class="note"></i>
						</div>
						<div class="validateCell">
							<div class="pull-left">
                                                            <p>
                                                        {ci_language line="infomation"}</h5> {if isset($info_{$current_page})}{$info_{$current_page}}{else}{ci_language line="info_{$current_page}"}{/if}
                                                            </p>
                                                        </div>
							<i class="fa fa-remove pull-right"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
                {/if}
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget widget-inverse">
                            {include file="ebay/{$root_page}configuration_{$current_page}.tpl"}
                            {if empty($btn_flow) || (isset($btn_flow) && $btn_flow == 'open') }
                            <div class="widget-body">
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        {if !empty($prev_page)}
                                        <button class="btn" type="back" data-page="{$prev_page}" data-last="finish">
                                            <i class="fa fa-arrow-left"></i>
                                            {ci_language line="prev"}
                                        </button>
                                        {/if}
                                        {if !empty($is_reset)}
                                        <button class="btn btn-inverse" type="reset">
                                            <i class="fa fa-undo"></i>
                                            {ci_language line="reset"}
                                        </button>
                                        {/if}
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        {if !empty($next_page)}
                                        <button class="btn btn-primary" type="submit" data-last="finish">
                                            {ci_language line="next"}
                                            <i class="fa fa-arrow-right"></i>
                                        </button>
                                        {/if}
                                    </div>
                                </div>
                                <div class="clearfix"></div>      
                            </div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        
        <div class="close">
            <button type='button' class='close' data-dismiss='alert'><i class='fa fa-close'></i></button><i class='fa fa-exclamation-triangle'></i>
        </div>
        <input type="hidden" name="id-site" id="id-site" value="{$id_site}" />
        <input type="hidden" name="id-packages" id="id-packages" value="{$packages}" />
        <input type="hidden" name="current-page" id="current-page" value="{$current_page}" />
        <input type="hidden" name="ajax-url" id="ajax-url" value="{$ajax_url}" />
        <input type="hidden" name="update-{$current_page}-success" class="update-{$current_page}-success" value="{ci_language line="update_{$current_page}_success"}">
        <input type="hidden" name="update-{$current_page}-fail" class="update-{$current_page}-fail" value="{ci_language line="update_{$current_page}_fail"}">
        {if isset($hidden_data)}
        {foreach from = $hidden_data key = key item = hidden}
        <input type="hidden" name="{$key}" class="{$key}" value="{$hidden}">
        {/foreach}
        {/if}
        {if isset($validation_data)}
        {foreach from = $validation_data key = field item = data}
        {foreach from = $data key = validation_key item = validation_message}
        <input type="hidden" name="{$field}-{$validation_key}" class="{$field}-{$validation_key}" value="{$validation_message}" data-rules="{$validation_key}" data-type="validation">
        {/foreach}
        {/foreach}
        {/if}
    </form>
</div>
{if !empty($jquery_form)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/form/jquery.form.js"></script>
{/if}
{if !empty($currency_format)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/formatCurrency/jquery.formatCurrency.js"></script>
{/if}
{if !empty($data_tree)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tree/fuelux.tree.offers.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/blockUI/jquery.blockUI.js"></script>
{/if}
{if !empty($data_table)}
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/jquery.dataTables.bootstrap.js"></script>*}
<script type="text/javascript" src="{$cdn_url}assets/js/datatables.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/dataTables.colVis.min.js"></script>
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/DT_bootstrap.js?v=v1.2.3"></script>*}
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ebay.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_template.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/function_ajax_wizard_start.js"></script>
{if !empty($tinymce)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tinymce/tinymce.min.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/checkBo/checkBo.min.js"></script>
{*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/{$current_page}.js"></script>*}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/categories_main.js"></script>
{include file="footer.tpl"}