
                    <!-- // Widget Heading END -->
                    {*<div class="widget-head">
                        <h3 class="header-main header-full">{ci_language line="create_your_payment"}</h3>
                    </div>*}
                    <div class="widget-body">
                        {*<h5 class="title-main title-bottom title-half">{ci_language line="payment_setting"}</h5>*}
                        {include file="ebay/main_info.tpl"}
                        <div class="widget-main">
                        <div class="row-fluid">
                            <div class="form-group payment-main">
                                <div class="col-md-12">
                                    <div class="col-xs-11 col-md-6">
                                        <div class="col-md-12 form-group">
                                            <label class="control-label" for="form-payment-checkbox">{ci_language line="merchant_credit_cards"}{_get_tooltip_icon({ci_language line="payment_help"})}</label>
                                            <div class="col-xs-11 col-md-5">
                                                <div class="col-md-11">
                                                    <div class="checkbox">
                                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-default" type="checkbox" value="" disabled checked="checked"><span class="lbl"> {ci_language line="paypal"} <span class="recommended">{ci_language line="default"}</span></span></label>
                                                    </div>
                                                </div>
                                            {if !empty($payment_method)}{foreach $payment_method as $payment}{if $payment['priority'] == 'main'}
                                                <div class="col-md-11">
                                                    <div class="checkbox">
                                                        <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="{$payment['payment_method']}" {if isset($payment['selected']) && $payment['selected']}checked="checked"{/if}><span class="lbl"> {$payment['payment_name']}</span></label>
                                                    </div>
                                                </div>
                                            {/if}{/foreach}{/if}
                                            </div>
                                        </div>
                                        {*<div class="col-md-11">
                                            <div class="help-block text-small">{ci_language line="payment_help"}</div>
                                        </div>*}
                                    </div>
                                    <div class="col-xs-11 col-md-6">
                                        <div class="col-md-12  form-group">
                                            <label class="control-label" for="form-payment-checkbox">{ci_language line="other_payment"}</label>
                                            {if !empty($payment_method)}{foreach $payment_method as $payment}{if $payment['priority'] == 'other'}
                                            <div class="checkbox">
                                                <label class="checkbox-custom"><i class="fa fa-fw fa-square-o"></i><input name="form-payment-checkbox" type="checkbox" value="{$payment['payment_method']}" {if isset($payment['selected']) && $payment['selected']}checked="checked"{/if}><span class="lbl"> {$payment['payment_name']}</span></label>
                                            </div>
                                            {/if}{/foreach}{/if}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group payment-main">
                                <label class="control-label" for="payment-instructions">{ci_language line="payment_instructions"}{_get_tooltip_icon({ci_language line="payment_instructions_help"})}</label>
                                {*<div class="help-block text-small">{ci_language line="payment_instructions_help"}</div>*}
                                <div class="col-md-12">
                                    <span class="col-md-12 col-xs-12 payment-instructions-span">
                                        <textarea id='payment-instructions' name='payment-instructions' class="payment-instructions" maxlength="500">{if isset($profile_data['payment_instructions'])}{$profile_data['payment_instructions']}{/if}</textarea>
                                    </span>
                                    <div class="help-block red help-clear countdown-textarea">{ci_language line="remaining"} : 500</div>
                                    <div class="help-block text-small">{ci_language line="payment_instructions_max_help"}</div>
                                </div>
                            </div>
                        </div>
                    </div>