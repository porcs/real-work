            
                        {if !empty($error_messages)}
                            <div class="row">
				<div class="col-xs-12">
                                    <div class="validate pink">
                                        <div class="validateRow">
                                            <div class="validateCell">
                                                <i class="note"></i>
                                            </div>
                                            <div class="validateCell">
                                                <div class="pull-left">
                                                    <ul>
                                                        {foreach from=$error_messages key=index item=error_message}
                                                            <li><p>{$error_message}</p></li>
                                                        {/foreach}
                                                    </ul>
                                                </div>
                                                <i class="fa fa-remove pull-right"></i>
                                            </div>
                                        </div>
                                    </div>
				</div>
                            </div>
                            {/if}     
                            {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                            <!-- // Widget Heading END -->
                            <div class="widget-body">
                                <div class="synchronize-wrap">
                                    {*<h5 class="title-main title-bottom title-half">{ci_language line="synchronization"}</h5>
                                    <div class="help-block text-small">{ci_language line="synchronization_start_help"}</div>
                                    <div class="col-md-12 synchronize-main">
                                        <div class="col-xs-6 col-md-6">
                                            <div class="col-md-12">
                                                <a href="{$base_url}ebay/synchronization/{$packages}"><button class="btn btn-success btn-wizard"><i class="fa fa-magic"></i> {ci_language line="synchronize_wizard"}</button></a>
                                            </div>
                                        </div>

                                    </div>*}
                                    
                                    <div class="clearfix"></div>
                                    <h5 class="title-main title-bottom title-half">{ci_language line="update_product_offer"} {_get_tooltip_icon({ci_language line="update_offer_product_help"})}</h5>
                                    {*<div class="help-block text-small">{ci_language line="update_offer_product_help"}</div>*}
                                    <div class="col-md-12 synchronize-main text-right">
                                        <div class="col-xs-12 col-md-12 export-offer-button">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary btn-wizard" id="send-offers"><i class="fa fa-external-link-square"></i> {ci_language line="offer_product_update"}</button>
                                                {include file="ebay/configuration_actions_alert.tpl"}
                                            </div>
                                        </div>
                                    </div>
                                            
                                    <!--  ADMIN TOOL -->
                                    {if $remote == true}
                                    <div class="widget-main synchronize-wrap">
                                        <h5 class="title-main title-bottom title-half">{ci_language line="Admin Tools"} {_get_tooltip_icon({ci_language line="admin_compress_product_help"})}</h5>
                                        {*<div class="help-block text-small">{ci_language line="admin_compress_product_help"}</div>*}
                                        <div class="col-md-12 synchronize-main text-right">
                                            <div class="col-xs-12 col-md-12 compress-product-button">
                                                <div class="col-md-12">
                                                    <button class="btn btn-primary btn-wizard" id="compress-file"><i class="fa fa-long-arrow-up"></i> {ci_language line="compress_a_zip_file"}</button>
                                                    {include file="ebay/configuration_actions_alert.tpl"}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/if}
                                </div>
                            </div>