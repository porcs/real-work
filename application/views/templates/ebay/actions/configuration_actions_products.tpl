            
                            {if !empty($error_messages)}
                            <div class="row">
				<div class="col-xs-12">
                                    <div class="validate pink">
                                        <div class="validateRow">
                                            <div class="validateCell">
                                                <i class="note"></i>
                                            </div>
                                            <div class="validateCell">
                                                <div class="pull-left">
                                                    <ul>
                                                        {foreach from=$error_messages key=index item=error_message}
                                                            <li><p>{$error_message}</p></li>
                                                        {/foreach}
                                                    </ul>
                                                </div>
                                                <i class="fa fa-remove pull-right"></i>
                                            </div>
                                        </div>
                                    </div>
				</div>
                            </div>
                            {/if}                            
                            
                            {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                            <div class="widget-head">
                                <h3 class="header-main header-full">{ci_language line="title_$current_page"}</h3>
                            </div>
                            <!-- // Widget Heading END -->
                            <div class="widget-body">
                                <div class="widget widget-tabs widget-tabs-responsive">
                                    <div class="widget-head">
                                        <ul>
                                            <li {if isset($tab_data['tab']) && $tab_data['tab'] == 1}class="active"{/if}><a href="#tab-1" data-toggle="tab"><i class="fa fa-long-arrow-up"></i> {ci_language line="export_product"}</a></li>
                                            <li {if isset($tab_data['tab']) && $tab_data['tab'] == 2}class="active"{/if}><a href="#tab-2" data-toggle="tab"><i class="fa fa-trash-o"></i> {ci_language line="delete_product"}</a></li>
                                        </ul>
                                    </div>
                                    <div class="widget-body padding-none">
                                        <div class="tab-content">
                                            <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 1}active{/if}" id="tab-1">
                                                <div class="widget-main synchronize-wrap">
                                                    <h5 class="title-main title-bottom title-half">{ci_language line="export_product"} {_get_tooltip_icon({ci_language line="export_product_help"})}</h5>
                                                    {*<div class="help-block text-small">{ci_language line="export_product_help"}</div>*}
                                                    <div class="col-md-12 synchronize-main text-right">
                                                        <div class="col-xs-12 col-md-12 export-product-button">
                                                            <div class="col-md-12">
                                                                <button class="btn btn-primary btn-wizard send-products" id="send-products"><i class="fa fa-long-arrow-up"></i> {ci_language line="export_product_update"}</button>
                                                                <button class="btn btn-primary btn-wizard send-products" id="revise-product"><i class="fa fa-long-arrow-up"></i> {ci_language line="revise_product"}</button>
                                                                {include file="ebay/configuration_actions_alert.tpl"}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!--  ADMIN TOOL -->
                                                {if $remote == true}
                                                <div class="widget-main synchronize-wrap">
                                                    <h5 class="title-main title-bottom title-half">{ci_language line="Admin Tools"} {_get_tooltip_icon({ci_language line="admin_compress_product_help"})}</h5>
                                                    {*<div class="help-block text-small">{ci_language line="admin_compress_product_help"}</div>*}
                                                    <div class="col-md-12 synchronize-main text-right">
                                                        <div class="col-xs-12 col-md-12 compress-product-button">
                                                            <div class="col-md-12">
                                                                <button class="btn btn-primary btn-wizard" id="compress-file"><i class="fa fa-long-arrow-up"></i> {ci_language line="compress_a_zip_file"}</button>
                                                                <button class="btn btn-primary btn-wizard send-products" id="verify-product"><i class="fa fa-long-arrow-up"></i> {ci_language line="verify_product"}</button>
                                                                {include file="ebay/configuration_actions_alert.tpl"}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/if}
                                            </div>
                                            <div class="tab-pane {if isset($tab_data['tab']) && $tab_data['tab'] == 2}active{/if}" id="tab-2">
                                                <div class="widget-main synchronize-wrap form-group">
                                                    <h5 class="title-main title-bottom title-half">{ci_language line="delete_product"} {_get_tooltip_icon({ci_language line="delete_product_help"})}</h5>
                                                    {*<div class="help-block text-small">{ci_language line="delete_product_help"}</div>*}
                                                    <div class="col-md-12 synchronize-main">
                                                        <div class="col-xs-12 col-md-12 form-group">
                                                            <label class="control-label label-mb10" for="order-status">{ci_language line="delete_type"}</label>
                                                            <div class="col-md-3">
                                                                <div class="col-md-11">
                                                                    <label class="col-md-12 col-xs-12 radio radio-custom">
                                                                        <i class="fa fa-fw fa-square-o"></i>
                                                                        <input type="radio" name="group" id="deactivate_stock" class="delete-stock" name="delete-stock" value="0" checked="checked"></i>
                                                                        <span class="lbl"> {ci_language line="delete_disable_product"}</span> {_get_tooltip_icon({ci_language line="delete_disabled_help"})}
                                                                    </label>
                                                                    {*<div class="col-md-10">
                                                                        <div class="help-block text-small">{ci_language line="delete_disabled_help"}</div>
                                                                    </div>*}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="col-md-11">
                                                                    <label class="col-md-12 col-xs-12 radio radio-custom">
                                                                        <i class="fa fa-fw fa-square-o"></i>
                                                                        <input type="radio" name="group" id="out_of_stock" class="delete-stock" name="delete-stock" value="1" checked="checked"></i>
                                                                        <span class="lbl"> {ci_language line="delete_out_of_stock"}</span> {_get_tooltip_icon({ci_language line="delete_out_of_stock_help"})}
                                                                    </label>
                                                                    {*<div class="col-md-10">
                                                                        <div class="help-block text-small">{ci_language line="delete_out_of_stock_help"}</div>
                                                                    </div>*}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="col-md-11">
                                                                    <label class="col-md-12 col-xs-12 radio radio-custom">
                                                                        <i class="fa fa-fw fa-square-o"></i>
                                                                        <input type="radio" name="group" id="all_in_stock" class="delete-stock" name="delete-stock" value="2" checked="checked"></i>
                                                                        <span class="lbl"> {ci_language line="delete_all_sent_product"}</span> {_get_tooltip_icon({ci_language line="delete_all_in_stock_help"})}
                                                                    </label>
                                                                    {*<div class="col-md-10">
                                                                        <div class="help-block text-small">{ci_language line="delete_all_in_stock_help"}</div>
                                                                    </div>*}
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="col-md-11">
                                                                    <label class="col-md-12 col-xs-12 radio radio-custom">
                                                                        <i class="fa fa-fw fa-square-o"></i>
                                                                        <input type="radio" name="group" id="all_in_stock" class="delete-stock" name="delete-stock" value="3" checked="checked"></i>
                                                                        <span class="lbl"> {ci_language line="delete_full_sent_product"}</span> {_get_tooltip_icon({ci_language line="delete_full_in_stock_help"})}
                                                                    </label>
                                                                    {*<div class="col-md-10">
                                                                        <div class="help-block text-small">{ci_language line="delete_full_in_stock_help"}</div>
                                                                    </div>*}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6 col-md-6 export-delete-button">
                                                            <div class="col-md-12 synchronize-main">
                                                                <button class="btn btn-danger btn-wizard" id="delete-products"><i class="fa fa-trash"></i> {ci_language line="delete_product_update"}</button>
                                                                {include file="ebay/configuration_actions_alert.tpl"}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>