                            {if isset($popup_hide)}{$field_hide = 'field-hide'}{else}{$field_hide = ''}{/if}
                            <!-- // Widget Heading END -->
                            <div class="widget-body">
                                <div class="synchronize-wrap form-group">
                                    <h5 class="title-main title-bottom title-half">{ci_language line="import_order"} {_get_tooltip_icon({ci_language line="import_order_help"})}</h5>
                                    {*<div class="help-block text-small">{ci_language line="import_order_help"}</div>*}
                                    <div class="col-md-12 synchronize-main">
                                        <div class="col-xs-12 col-md-12 form-group">
                                            <label class="control-label label-mb10" for="order-status">{ci_language line="order_status"} {_get_tooltip_icon({ci_language line="order_status_help"})}</label>
                                            <div class="col-md-4">
                                                <select class="order-status" id='order-status' name='order-status'>
                                                    <option value="Completed">{ci_language line="order_completed"}</option>
                                                    <option value="Active">{ci_language line="order_active"}</option>
                                                </select>
                                                <div class="col-md-12">
                                                    <div class="help-block text-small">{*{ci_language line="order_status_help"}*}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-md-6 import-order-button">
                                            <div class="col-md-12">
                                                <button class="btn btn-primary btn-wizard" id="download-orders"><i class="fa fa-long-arrow-down"></i> {ci_language line="import_order_update"}</button>
                                                <button class="btn btn-primary btn-wizard" id="modifier-orders"><i class="fa fa fa-cog"></i> {ci_language line="modifier_order_update"}</button>
                                                <!--  ADMIN TOOL -->
                                                {if $remote == true}
                                                <button class="btn btn-primary btn-wizard" id="debug-orders"><i class="fa fa-bug"></i> {ci_language line="debug_import_order"}</button>
                                                {/if}
                                                {include file="ebay/configuration_actions_alert.tpl"}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <h5 class="title-main title-bottom title-half">{ci_language line="export_shipment"} {_get_tooltip_icon({ci_language line="confirm_order_help"})}</h5>
                                    {*<div class="help-block text-small">{ci_language line="confirm_order_help"}</div>*}
                                    <div class="col-md-12 synchronize-main">
                                        <div class="col-xs-6 col-md-6 export-order-button">
                                            <div class="col-md-12">
                                                <button class="btn btn-success btn-wizard" id="export-orders"><i class="fa fa-long-arrow-up"></i> {ci_language line="export_shipment_update"}</button>
                                                {include file="ebay/configuration_actions_alert.tpl"}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>