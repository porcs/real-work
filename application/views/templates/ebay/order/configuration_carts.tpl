
                        <!-- // Widget Heading END -->
                        <table class="dynamicTable table" id="statistics" >
                            <thead class="table-head">
                                <tr>
                                    <th></th>
                                    <th class="center">{ci_language line="SKU"}</th>
                                    <th class="center">{ci_language line="Status"}</th> 
                                    <th class="center">{ci_language line="Quantity"}</th>
                                    <th class="center">{ci_language line="Date"}</th>
                                    <!-- <th></th> -->
                                </tr>
                            </thead>
                            <!-- // Table heading END -->
                            <tbody>
                                {if !empty($order_result)} {foreach from=$order_result key=id item=order}
                                <tr>
                                    <td></td>
                                    <td class='center'>{$order['SKU']}</td>
                                    <td class='center'>{$order['quantity']}</td>
                                    <td class='center'>{$order['id_buyer']}</td>
                                    <td class='center'>{$order['order_date']}</td>
                                </tr>
                                {/foreach}{/if}
                            </tbody>
                        </table>
                        <input type="hidden" id="OrderIDRef" value="{ci_language line="Order ID"}" />
                        <input type="hidden" id="Pending" value="{ci_language line="Pending"}" />     
                        {include file="amazon/IncludeDataTableLanguage.tpl"}