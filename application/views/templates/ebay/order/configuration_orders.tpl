                    
{*                        <div class="widget-head">
<h3 class="header-main header-half">{ci_language line="title_$current_page"}</h3>
</div>*}
<!-- // Widget Heading END -->
<div class="widget-body">                            
    <!-- Table -->
    <table class="dynamicTable colVis table" id="statistics" >
        <thead>
            <tr>
                <th rel='id_order' data-role='id_order' class='center'>{ci_language line="id_order"}</th>
                <th rel='id_order_ref' data-role='id_order_ref'  class='center'>{ci_language line="id_marketplace_order_ref"}</th>
                <th rel='id_buyer' data-role='id_buyer'  class='center'>{ci_language line="id_buyer"}</th>
                <th rel='order_status' data-role='order_status'  class='center'>{ci_language line="order_status"}</th>
                <th rel='country_name' data-role='country_name'  class='center'>{ci_language line="country_name"}</th>
                <th rel='mp_channel' data-role='mp_channel'  class='center'>{ci_language line="mp_channel"}</th>
                <th rel='payment_method' data-role='payment_method'  class='center'>{ci_language line="payment_method"}</th>
                <th rel='total_paid' data-role='total_paid'  class='center'>{ci_language line="total_paid"}</th>
                <th rel='order_date' data-role='order_date'  class='center'>{ci_language line="order_date"}</th>
                <th rel='invoice_no' data-role='invoice_no'  class='centersorting_desc'>{ci_language line="invoice_no"}</th>
                <th rel='status' data-role='status'  class='center'>{ci_language line="exported"}</th>
            </tr>
        </thead>
        <!-- // Table heading END -->
        <tbody>
            {if !empty($order_result)} {foreach from=$order_result key=id item=order}
                    <tr>
                        <td rel='id_order' class='center'>{$order['id_order']}</td>
                        <td rel='id_order_ref' class='center'>{$order['id_marketplace_order_ref']}</td>
                        <td rel='id_buyer' class='center'>{$order['id_buyer']}</td>
                        <td rel='order_status' class='center'>{$order['order_status']}</td>
                        <td rel='country_name' class='center'>{$order['country_name']}</td>
                        <td rel='mp_channel' class='center'>{$order['mp_channel']}</td>
                        <td rel='payment_method' class='center'>{$order['payment_method']}</td>
                        <td rel='total_paid' class='center'>{number_format($order['total_paid'], 2)} {if !empty($ebay_site)}{$ebay_site[$order['site']]->currency}{/if}</td>
                        <td rel='order_date' class='center'>{$order['order_date']}</td>
                        <td rel='invoice_no' class='center'>{$order['invoice_no']}</td>
                        <td rel='status' class='center'>
                            <button type="button" class="send-orders btn btn-success btn-mini" value="{$order['id_order']}" {if $order['status'] == 1} disabled  style="opacity: 0.3;"{/if}>
                                {if $order['status'] == 1}
                                    <i class=" icon-check bigger-120"></i>&nbsp;
                                    <span class="send-status">{ci_language line="Sent"}</span>
                                {else}
                                    <i class=" icon-upload bigger-120"></i>&nbsp;
                                    <span class="send-status">{ci_language line="Send"}</span>
                                {/if}
                            </button>
                        </td>
                    </tr>
            {/foreach}{/if}
        </tbody>
        <tfoot>
            <tr>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="id_order"}" rel='id_order'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="id_marketplace_order_ref"}" rel='id_order_ref'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="id_buyer"}" rel='id_buyer'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="order_status"}" rel='order_status'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="country_name"}" rel='country_name'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="mp_channel"}" rel='mp_channel'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="payment_method"}" rel='payment_method'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="total_paid"}" rel='total_paid'></th>
                <th class='center'><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="order_date"}" rel='order_date'></th>
                <th class="center"><input class="col-xs-5 form-control" type="text" value="" placeholder="{ci_language line="invoice_no"}" rel='invoice_no'></th>
                <th class="center"></th>
            </tr>
        </tfoot>
    </table>
    <!-- // Table END -->
</div>

<div class="modal fade" id="ebayOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Order Result"}</h1>
                    </div>
                </div>

                <div class="row">                       
                    <div class="col-xs-12 p-0">
                        <div id="viewResult"></div>
                    </div>  
                </div>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>