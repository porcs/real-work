
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="Mode"}</h4>						
				</div>
			</div>
		</div>
		<div class="widget-body">
                    <div class="widget-main">
                        <div class="general-mode">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <div class="col-md-11 custom-form">
                                        <label class="cb-radio w-100">
                                                <input type="radio" name="mode" type="radio" value="1" {if isset($feed_mode.mode) && $feed_mode.mode == 1}checked="checked" rel="checked"{/if} disabled />
                                                <p class="text-uc dark-gray montserrat m-b0 bold ">
                                                    {if isset($feed_mode.mode) && $feed_mode.mode == 1}
                                                        <span class="recommended m-l0 m-r10">{ci_language line="Default Mode"}e</span>
                                                    {/if}
                                                    {ci_language line="sandbox_key"}
                                                </p>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-11 custom-form">
                                        <label class="cb-radio w-100">
                                            <input type="radio" name="mode" value="2" {if isset($feed_mode.mode) && $feed_mode.mode == 2}checked="checked" rel="checked"{/if} disabled  />					
                                            <p class="text-uc dark-gray montserrat m-b0 bold">
                                                {if isset($feed_mode.mode) && $feed_mode.mode == 2}
                                                    <span class="recommended m-l0 m-r10">{ci_language line="Default Mode"}</span>
                                                {/if}
                                                {ci_language line="production_key"}
                                            </p>
                                    </label>
                                    </div>
                                </div>
                            </div>
			</div>
                    </div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="Authentification"}</h4>						
				</div>
			</div>
		</div>
		<div class="row">
                    <div class="col-xs-12">
                        <div class="validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="note"></i>
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left">{ci_language line="Security token used between your Shop and Feed.biz"}</p>
                                    <i class="fa fa-remove pull-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
		</div>
		<div class="row">
                    <div class="col-md-4">
                        <div class="col-md-11">
                            <p class="regRoboto poor-gray">{ci_language line="Username"}</p>
                            <div class="form-group">
                                <input type="text" class="form-control" value="{if isset($username)}{$username}{/if}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-11">
                            <p class="regRoboto poor-gray">{ci_language line="Token"}</p>
                            <div class="form-group">
                                <input type="password" class="form-control" value="{if isset($token)}{$token}{/if}" name="token" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                            <div class="m-t25">
                                <!-- Alret Popup-->
                                <input id="general-confirm" type="hidden" value="{ci_language line="Are you sure to change mode"}?" />
                                <input id="general-message" type="hidden" value="{ci_language line='Please choose mode'}" />
                                <a href="" class="link p-size pull-left m-tr10" id="save_data" type="submit">{ci_language line="Save"}</a>
                                <button class="btn btn-save" id="save_continue_data" type="submit" name="save" value="continue">{ci_language line="Save & continue"}</button>
                            </div>
                    </div>
		</div>