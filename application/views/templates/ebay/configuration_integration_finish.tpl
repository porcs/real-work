                    
                    <div class="widget-head">
                        <h3 class="header-main header-half header-bottom">{ci_language line="title_$current_page"}</h3>
                    </div>
                    <div class="integration-style">
                        {include file="ebay/configuration_step.tpl"}
                    </div>
                    <div class="widget-body">
                        <div class="form-group image-integration">
                            <div class="col-md-12 col-xs-12 ebay-integration-main">
                                <h5 class="title-main title-bottom title-half align-center">{ci_language line="integration_finish_success"}</h5>
                                <div class="col-md-12 align-center">
                                    <div class="help-block text-small">{ci_language line="integration_ebay_finish_help"}</div>
                                    <button class="btn btn-primary btn-synchronization" type="submit" data-last="finish">
                                        {ci_language line="synchronize_wizard"}
                                    </button>
                                    <button class="btn btn-primary btn-setting" type="submit" data-last="finish">
                                        {ci_language line="header_auth_setting"}
                                    </button>
                                </div>
                            </div>
                        </div>                           
                    </div>
                    <!-- // Widget Heading END -->