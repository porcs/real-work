                        <div class="widget-head header-tab-switch">
                            <ul>
                                <li {if isset($tab_data['tab']) && $tab_data['tab'] == 1}class="active"{/if}><a href="#tab-1" data-toggle="tab"><i class="fa fa-exchange"></i> {ci_language line="use_condition_matchings"}</a></li>
                                <li {if isset($tab_data['tab']) && $tab_data['tab'] == 2}class="active"{/if}><a href="#tab-2" data-toggle="tab"><i class="fa fa-wrench"></i> {ci_language line="use_condition_specifics"}</a></li>
                            </ul>
                        </div>