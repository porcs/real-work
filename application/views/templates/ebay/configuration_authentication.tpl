                    {if !empty($popup_config) && isset($popup_config) } 
                    <div class="widget-head">
                        <h3 class="header-main header-half header-bottom">{ci_language line="title_$current_page"}</h3>
                    </div>
                    {/if}

                    <div class="form-group image-integration">
                        <div class="col-md-12 col-xs-12 ebay-integration-main">
                            <div class="title-main title-bottom title-half">{ci_language line="generate_token"} {_get_tooltip_icon({ci_language line="integration_ebay_authorization_help_01"})}
                                {if isset($feed_mode) && ($feed_mode == 2 || $feed_mode == 3)}
                                <div class="debug_mode pull-right">
                                    <div class="cb-switcher">
                                        <label class="inner-switcher">
                                            <input name="active" id="form-app-mode" type="checkbox" value="1" data-state-on="ON" data-state-off="OFF">
                                        </label>
                                        <span class="cb-state">{ci_language line="ON"}</span>
                                    </div>
                                </div>
                                {/if}
                            </div>
                            <div class="col-md-12">
                                
                                {*<div class="help-block text-small">{ci_language line="integration_ebay_authorization_help_01"}</div>*}
                                
                                <button class="btn btn-primary btn-synchronization" type="submit" data-last="finish">
                                    {ci_language line="integration_authorization_wizard"}
                                </button>
                                
                                <div class="help-block text-small">{ci_language line="integration_ebay_authorization_help_02"}</div>
                            </div>
                        </div>
                    </div>                            

                    <!-- // Widget Heading END -->