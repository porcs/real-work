{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div id="content" class="main" style="margin-left: 300px;">
    <div class="p-rl40 p-xs-rl20">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Help Center"}</h1>
        {*include file="breadcrumbs.tpl"*} <!--breadcrumb-->
    
        <div class="row">

            <div class="form-group">
                <div class="col-xs-12">
                        <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                            <h4 class="headSettings_head">{if isset($helpdetial['message_name'])}{$helpdetial['message_name']}{/if}</h4>						
                        </div>
                </div>
                <div class="col-sm-10 m-l25 text-center">
                    {*$present|print_r*}
                    {if isset($helpdetial['message_cause'])}
                        <iframe width="853" height="480" src="{$helpdetial['message_cause']}" frameborder="0" allowfullscreen></iframe>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // Content END -->

<!-- // DIV END in HEADER -->
</div>
</div>
<!-- // DIV END in HEADER -->

{include file="footer.tpl"}