{if !isset($popup) || !$popup}
   	<ul class="breadcrumbs menubar clearfix">
        {if isset($mode_default) }
            {if $mode_default == 1}<li class="bread_item active"><a class="bread_link" href="{$base_url}my_feeds/configuration/general" title="{ci_language line="Beginner"} {ci_language line="Mode"}">{ci_language line="Beginner"}</a></li>{/if}
            {if $mode_default == 2}<li class="bread_item active"><a class="bread_link" href="{$base_url}my_feeds/configuration/general" title="{ci_language line="Expert"} {ci_language line="Mode"}">{ci_language line="Expert"}</a></li>{/if}
            {if $mode_default == 3}<li class="bread_item active"><a class="bread_link" href="{$base_url}my_feeds/configuration/general" title="{ci_language line="Advanced"} {ci_language line="Mode"}">{ci_language line="Advanced"}</a></li>{/if}
        {/if}
        {if isset($breadcrumb_specify) } 
                {$breadcrumb_specify}
        {else}
            {if isset($breadcrumb) } {$breadcrumb}{/if}
        {/if}

         {if (isset($url.1) && $url.1 != 'configuration') && isset($url.0) && ($url.0 != 'users' && $url.0 != 'service' && $url.0 != 'billing')} 
            <li class="dropdown pull-right" style="display: none">
                <a class="dropdown-toggle btn" data-toggle="dropdown">{if isset($shop_default) && !empty($shop_default)}{$shop_default}{else}{ci_language line="Choose shop default"}{/if}&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu text-left">
                    {if isset($shop)}
                        {foreach from=$shop key=shop_key item=shop_value}
                            <li>
                                <a href="#">
                                    <input type="radio" name="id_shop" id= "{$shop_value.id_shop}" value="{$shop_value.id_shop}" {if isset($shop_value.is_default) && $shop_value.is_default == 1}checked{/if}>
                                    <label class="lbl font-size13" for="{$shop_value.id_shop}"  > {$shop_value.name}</label>
                                </a>
                            </li>
                        {/foreach}
                    {/if}
                </ul>
            </li>
        {/if}
   </ul>
{/if}
{include file="message_document.tpl"} 