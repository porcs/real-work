{ci_config name="base_url"}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
		<table width="100%" cellspacing=0>
			<tr style="background:#4693fe">
				<td style="width:20%"></td>
				<td style="padding:10px">
                                    <a href="{$base_url}" style="border: none">{$logo}</a>	
				</td>
				<td style="width:20%"></td>
			</tr>
			<tr>
				<td style="width:20%"></td>
				<td style="padding: 25px 0 0 0;">
					<table style="width:100%; border:1px solid #eeeeee; border-spacing:0px;">
						<tr>
							<td style="padding:10px; color: #919191; font-weight: 300; font-size:20px;">
								<p style="margin-bottom:0; margin-top:10px">Dear Customer,</p>
							</td>
						</tr>
						<tr>
							<td style="padding:10px;">
								<p style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:10px 10px;font-weight:normal;font-size:14px;line-height:1.6;padding:15px 15px 15px 0px; margin-bottom:15px;">
                                                                    You are receiving this email because you just registered on Feed.biz.<br><br>
                                                                    Your account has been created, please click here to activate it now :<br><br>
                                                                    <a href="{$link}" style="color:#4693fe">{ci_language line="Click here to activate account"}</a>
                                                                    {*<?php echo anchor(site_url($activate_link . $id . '/' . $activation), ''); ?>*}<br><br>
                                                                    If you are having difficulties activating your account, then please contact the support service at: support@feed.biz<br><br>
                                                                    This message comes from an unmonitored mailbox. Please do not reply to this message.<br><br>
                                                                    Thanks
                                                                </p>
							</td>
						</tr>
						<tr>
							<td style="background:#eeeeee; padding:10px;">
								<p style="margin-top:0px; margin-bottom:10px">
									This e-mail was sent by 
									<a href="" style="color:#4693fe">support@feed.biz</a>
								</p>		
								<p style="margin-top:0px; margin-bottom:10px">
								<p style="margin-top:0px; margin-bottom:10px">&copy; {date('Y')} <a href="{$base_url}" style="color:#4693fe">Feed.biz</a> All right reserved.</p>
								</p>
							</td>
						</tr>
					</table>
				</td>
				<td style="width:20%"></td>
			</tr>
		</table>
        
        
    </body>
</html>
