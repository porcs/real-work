<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>        
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px; font-family: sans-serif;">
        <table width="100%" cellspacing=0>
            <tr style="background:#4693fe;">
                <td style="padding:10px; ">
                    <a href="">{$logo}</a>  
                </td>
            </tr>
            <tr>
                <td style="border:1px solid #eeeeee;">
                    <table style="width:100%; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px">
                                <p style="margin-bottom:0; margin-top:10px; font-family: sans-serif; font-size: 16px;">Hello,</p>
                                <p style="margin-bottom:10px; margin-top:20px; font-family: sans-serif; font-size: 13px;">Here's the users reports in {if isset($site_name)}{$site_name}{else}Feed.biz{/if}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">

                                <!-- Summary -->
                                <table width="100%" border="0" cellspacing="0" cellpadding="2" style="font-family: sans-serif; font-size: 12px;">
                                    <tbody>
                                        <tr>
                                            <td width="10%" style="text-align: left"><b>Total User</b></td>
                                            <td width="15%" style="text-align: left; border-bottom: dotted 1px #CCC">{$total_user}</td>
                                        <!-- </tr>  
                                        <tr> -->
                                            <td width="5%" style="text-align: left"></td>
                                            <td width="10%" style="text-align: left"><b>Active</b></td>
                                            <td width="15%" style="text-align: left; border-bottom: dotted 1px #CCC">{$active_user}</td>
                                        <!-- </tr>        
                                        <tr> -->
                                            <td width="5%" style="text-align: left"></td>
                                            <td width="10%" style="text-align: left"><b>Inactive</b></td>
                                            <td width="15%" style="text-align: left; border-bottom: dotted 1px #CCC">{$inactive_user}</td>
                                        </tr>  
                                    </tbody>                                   
                                </table>
                                <br/>

                                <!-- Report -->                    
                                <table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family: sans-serif; font-size: 12px;">
                                    <thead>
                                        <tr style="background: #4693FE; color: #FFF; font-weight: normal;">
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Name</th>
                                            <!-- <th>Last name</th> -->
                                            <th>Email</th>
                                            <th>Status </th>
                                            <th>Shop</th>
                                            <th>Software</th>
                                            <th>Module Ver.</th>
                                            <th>Language</th>
                                            <!-- <th>Version</th> -->
                                            <th>Created</th>
                                            <th>Offers</th>
                                            <th>Marketplaces (Items/Errors)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $users as $user}
                                            {$style = '  style="border-bottom: 1px solid #c2c2c2;"'}
                                            {if isset($user['is_new']) && $user['is_new'] == 1} 
                                                {$style = ' style="border-bottom: 1px solid #c2c2c2;background: #F7F7F7"'}
                                            {/if}
                                            <tr{if isset($user['is_new']) && $user['is_new'] == 1} style="color:#FF5722"{/if}>
                                                <td{$style}>
                                                    {if isset($user.id_customer)}{$user.id_customer}{else} &nbsp; {/if}  
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.user_name)}{$user.user_name}{else} &nbsp;{/if} 
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.user_first_name)}{$user.user_first_name}{/if} 
                                                    {if isset($user.user_las_name)}{$user.user_las_name}{else} &nbsp;{/if} 
                                                </td>
                                                <!-- <td{$style}>{if isset($user.user_las_name)}{$user.user_las_name}{/if}</td> -->
                                                <td{$style}>
                                                    {if isset($user.user_email)}{$user.user_email}{else} &nbsp;{/if} 
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.user_status)}
                                                        {if $user.user_status == 1}
                                                            <span style="display: block; background: #8BC34A; border-radius: 5px; padding: 2px 0px; text-align: center; color: #FFF;">Active</span>
                                                        {else}
                                                            <span style="display: block; background: #c3c3c3; border-radius: 5px; padding: 2px; text-align: center; color: #FFF;">Inactive</span>
                                                        {/if}
                                                    {/if}
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.shop_info.name)}{$user.shop_info.name}{else} &nbsp;{/if} 
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.shop_info.software)}{$user.shop_info.software}{else} &nbsp;{/if} 
                                                    {if isset($user.shop_info.version)}{$user.shop_info.version}{else} &nbsp;{/if} 
                                                </td>
                                                <td{$style}>
                                                    {if !empty($user.user_module_ver)}{$user.user_module_ver}{else} &nbsp;{/if}  
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.language)}{$user.language}{else} &nbsp;{/if}  
                                                </td>
                                               <!--  <td{$style}>
                                                    {if isset($user.shop_info.version)}{$user.shop_info.version}{else} &nbsp;{/if} 
                                                </td> -->
                                                <td{$style}>
                                                    {if isset($user.user_created_on)}{$user.user_created_on}{else} &nbsp;{/if} 
                                                </td>
                                                <td{$style}>
                                                    <div style="text-align: right;">
                                                        {if isset($user.no_offer_shop)}
                                                            {$user.no_offer_shop}  
                                                        {else}
                                                            -                                
                                                        {/if}
                                                    </div>
                                                </td>
                                                <td{$style}>
                                                    {if isset($user.marketplace)}
                                                        {$i = 0}
                                                         {foreach $user.marketplace as $marketplace => $detail}
                                                              
                                                            {if ($i != 0)}, {/if}
                                                            {$marketplace} ({if isset($detail.no_offer_marketplace)}<span style="color:#8bc34a">{$detail.no_offer_marketplace}</span>{else}0{/if}/{if isset($detail.no_create_error_marketplace)}<span style="color:#d14836">{$detail.no_create_error_marketplace}</span>{else}0{/if})
                                                            {$i = $i+1}
                                                            
                                                        {/foreach}
                                                        {*foreach $user.marketplace as $marketplace => $details}
                                                            {foreach $details as $domain => $detail}     
                                                                {if ($i != 0)}, {/if}
                                                                {$domain} ({if isset($detail.no_offer_marketplace)}{$detail.no_offer_marketplace}{else}0{/if})
                                                                {$i = $i+1}
                                                            {/foreach}
                                                        {/foreach*}
                                                    {else} 
                                                        &nbsp;
                                                    {/if} 
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>

                                <br/>

                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eeeeee; padding:10px;border-top:1px solid #EEE">                                 
                                <p style="margin-top:10px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">
                                    {l('Please note that you must use an email to access to Feed.biz management system.')}</p>
                                <p style="margin-top:0px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">&copy; {date('Y')} <a href="mailto:support@feed.biz" style="color:#4693fe">Feed.biz.</a> All right reserved.</p>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>   