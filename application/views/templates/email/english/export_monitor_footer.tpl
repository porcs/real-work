                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eeeeee; padding:10px;border-top:1px solid #EEE">                                	
                                <p style="margin-top:10px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">
                                    {l('Please note that you must use an email to access to Feed.biz management system.')}</p>
                                <p style="margin-top:0px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">&copy; {date('Y')} <a href="mailto:support@feed.biz" style="color:#4693fe">Feed.biz</a> {l('All right reserved.')}</p>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:15%"></td>
            </tr>
        </table>
    </body>
</html>   