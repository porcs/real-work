                                <!-- Feed -->                    
                                {if !empty($feed) && !empty($results) }
                                    <h1 style="font-family: sans-serif; font-size: 15px;color: #222; font-weight: 700; background: #EEE; padding: 10px 10px;">
                                        {$shopname}
                                    </h1>
                                    
                                    <table width="100%" height="50px" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="45px">{$ebaylogo}</td>
                                            <td><h4 style="font-size:15px; margin:0 0 0 5px; padding:0; font-family: sans-serif;"> {l('Summary on')} : {$date}</h4></td>
                                        </tr>
                                    </table>
                                    
                                    <table width="100%" border="0" cellspacing="0" cellpadding="5" bordercolor="#ddd" style="font-size:13px; margin:10px 0 0 0; padding:0 0 10px; font-family: sans-serif; border-width: 0.5px;">
                                        <thead style="background: #ededed">
                                            <tr>
                                                <th width="20%" rowspan="2" style="border: 1px solid #ddd; border-right: none;">{l('Sources')}</th>
                                                <th width="10%" rowspan="2" style="border: 1px solid #ddd; border-right: none;">{l('Orders')}</th>
                                                <th width="30%" colspan="3" style="border: 1px solid #ddd; border-right: none; border-bottom: none;">{l('Product Creation Feed')}</th>
                                                <th width="30%" colspan="3" style="border: 1px solid #ddd; border-right: none; border-bottom:  none;">{l('Update Offers Feed')}</th>
                                                <th width="10%" rowspan="2" style="border: 1px solid #ddd; ">{l('Repricing')}</th>
                                            </tr>
                                            <tr>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Sent')}</th>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Success')}</th>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Error')}</th>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Sent')}</th>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Success')}</th>
                                                <th width="10%" style="border: 1px solid #ddd; border-right: none;">{l('Error')}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {foreach $results key = FeedKey item = FeedData}                                      
                                            <tr style="background:#FFF">
                                                <td style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">
                                                    {if isset($FeedData['domain_detail']['domain'])}{$feed['market_place']|cat:$FeedData['domain_detail']['ext']}{else}{$feed['market_place']|cat:" "|cat:$feed['country']}{/if}
                                                </td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Order'])}{$FeedData['Order']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Product']['Total'])}{$FeedData['Product']['Total']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Product']['Success'])}{$FeedData['Product']['Success']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Product']['Error'])}{$FeedData['Product']['Error']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Offer']['Total'])}{$FeedData['Offer']['Total']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Offer']['Success'])}{$FeedData['Offer']['Success']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-right: none; border-bottom: none; border-top: none;">{if isset($FeedData['Offer']['Error'])}{$FeedData['Offer']['Error']}{else}-{/if}</td>
                                                <td align="center" style="border: 1px solid #ddd; border-bottom: none; border-top: none;">{if isset($FeedData['repricing']['no_success'])}{$FeedData['repricing']['no_success']}{else}-{/if}</td>
                                            </tr>
                                        {/foreach}
                                        <tr style="font-weight: bold;background: #ededed;">
                                            <td style="border: 1px solid #ddd; ">{l('Total')}: </td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['order_total'])}{$sum['order_total']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['create_total'])}{$sum['create_total']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['create_success'])}{$sum['create_success']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['create_error'])}{$sum['create_error']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['offer_total'])}{$sum['offer_total']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['offer_success'])}{$sum['offer_success']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; border-right: none;">{if isset($sum['offer_error'])}{$sum['offer_error']}{else}-{/if}</td>
                                            <td align="center" style="border: 1px solid #ddd; ">{if isset($sum['repricing_total'])}{$sum['repricing_total']}{else}-{/if}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                        
                                    <table width="100%" height="50px" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="45px">{$ebaylogo}</td>
                                            <td><h4 style="font-size:15px; margin:0 0 0 5px; padding:0; font-family: sans-serif;"> {l('Statistics')}</h4></td>
                                        </tr>
                                    </table>
                                            
                                    <table width="100%" border="0" cellspacing="0" cellpadding="5" bordercolor="#ddd" style="font-size:13px; margin:10px 0 0 0; padding:0; font-family: sans-serif; border-width: 0.5px; background: #ededed">
                                        <thead style="background: #EDEDED;">
                                            <tr>
                                                <th width="60%" style="border: 1px solid #ddd; border-right: none;">{l('Message')}</th>
                                                <th width="20%" style="border: 1px solid #ddd; border-right: none;">{l('No. of Products')}</th>
                                                <th width="20%" style="border: 1px solid #ddd; ">{l('Percentage(%)')}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {counter start = 0 skip = 1 assign=count}
                                            {foreach $message key = FeedKey item = FeedData}     
                                                <tr style="background:#FFF">
                                                    <td style="border: 1px solid #ddd; border-right: none; border-top: none; border-bottom: none;">{l($FeedKey)}</td>
                                                    <td align="right" style="border: 1px solid #ddd; border-right: none; border-top: none; border-bottom: none; padding-right: 10px;">{$FeedData}</td>
                                                    <td align="right" style="border: 1px solid #ddd; border-top: none; border-bottom: none; padding-right: 10px;">{if !empty($sum['total_error'])}{round(($FeedData / $sum['total_error']) * 100, 2)} %{else}-{/if}</td>
                                                </tr>
                                                {counter}
                                            {/foreach}
                                        <tr style="font-weight: bold;background: #ededed;">
                                            <td style="border: 1px solid #ddd; border-right: none;">{l('Total')}: </td>
                                            <td align="right" style="border: 1px solid #ddd; padding-right: 10px; border-right: none;">{if !empty($sum['total_error'])}{$sum['total_error']}{else}-{/if}</td>
                                            <td align="right" style="border: 1px solid #ddd; padding-right: 10px;">100%</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                            <BR><BR>
                                {/if} 