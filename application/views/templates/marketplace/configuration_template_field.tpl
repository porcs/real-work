{foreach $domain as $region}
    <div class="allCheckBo">
        <div class="mainCheckbo">
             <label class="cb-checkbox checkAll"> 
                <input type="checkbox"/>
                {$region.name}
            </label>
        </div>
        {foreach $region.country as $country}
            <div class="checkboSettings clearfix">
                <label class="cb-checkbox pull-left">
                    <input type="checkbox" name="marketplace[{$country['id_offer_pkg']}][{$country['id_offer_price_pkg']}]" value="{$country['id_offer_price_pkg']}" rel="{$country['offer_pkg_type']}" />
                    {$country['title_offer_sub_pkg']}  ({$country['domain_offer_sub_pkg']})
                 </label>
            </div>
        {/foreach}
    </div>
{/foreach}