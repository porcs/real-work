{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/marketplace/display_conf_link.css" />


<div class="{if !isset($popup)} main {/if} p-rl40 p-xs-rl20"> 
    <h1  class="lightRoboto text-uc head-size light-gray">{ci_language line="Marketplace"}</h1>
    <div class="clearfix"></div>
  
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"} 
    
    <div class="row">
        <div class="col-xs-12">
            <div class="widget widget-inverse">
                <div class="widget-body text-center">
                    <div class="form-group">
                        {if isset($amazon_btn) || isset($ebay_btn)}
                            <div class="col-xs-12">
				<div class="headSettings clearfix m-b10 b-Bottom">
					<h4 class="headSettings_head">{ci_language line="Congatulations, you have successfully configured your first login."}</h4>						
				</div>
                            </div>
                            <p class="regRoboto poor-gray">{ci_language line="And, we recommend to configuration your marketplace account and send your items to marketplaces."}</p>
                        {/if}
                        {if isset($amazon_btn)}
                        <a href="javascript:parent.window.location.href='{$base_url}dashboard/index/amazon_synchronize_wizard'" class="btn btn-save wizard" type="button" id="amazon_synchronize_wizard"  >
                        <i class="icon-beaker bigger-125"></i> 
                        {ci_language line="Amazon Wizard"}
                        </a>
                        {/if}
                        {if isset($ebay_btn)}
                        <a  href="javascript:parent.window.location.href='{$base_url}dashboard/index/ebay_wizard'"  class="btn btn-save wizard" type="button" id="ebay_wizard"  >
                        <i class="icon-beaker bigger-125"></i>
                        {ci_language line="eBay Wizard"}
                        </a>
                        {/if}
                    </div>

                    <div class="form-actions text-right" id="form_action">
                        <a  href="javascript:  /*parent.jQuery.colorbox.close();*/parent.window.location.reload(); "  class="btn btn-success" type="button" id="close_wizard"  >
                            <i class="fa fa-close"></i> {ci_language line="Close"}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        
{include file="footer.tpl"}
 