{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="configuration"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {if $marketplace}
        <form id="frmPackages" method="post" action="{$base_url}marketplace/configuration_data" enctype="multipart/form-data">
            <input type="hidden" name="submit" value="">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        {foreach $marketplace as $field key=pkgid}
                            {if $field['name_offer_pkg'] == 'General'}
                                {$field['name_offer_pkg'] = 'Custom'}
                            {/if}
                            <li {if $pkgid==2}class="active"{/if}><a href="#pkg{$pkgid}" data-toggle="tab">{$field['name_offer_pkg']}</a></li>
                            {/foreach}
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 configuration">
                    <div class="tab-content">
                        {foreach $marketplace as $field key=cpkgid}                                            
                            <div class="tab-pane{if $cpkgid==2} active{/if}" id="pkg{$cpkgid}">		
                                {if $field['id_offer_pkg'] != $id_general}
                                    {if $field['submenu_offer_pkg'] == 1 && isset($field['sub_marketplace'])}
                                        {foreach $field['sub_marketplace'] as $field2}
                                            <div class="allCheckBo">
                                                <div class="mainCheckbo">
                                                    <label class="cb-checkbox checkAll">
                                                        <input type="checkbox"/>
                                                        {ci_language line=$field2.name}
                                                    </label>
                                                </div>
                                                {foreach $field2.country as $field3}
                                                    <div class="checkboSettings clearfix">
                                                        <label class="cb-checkbox pull-left">
                                                            <input type="checkbox" name="marketplace[{$field['id_offer_pkg']}][{$field3['id_offer_sub_pkg']}]" value="{$field3['id_offer_price_pkg']}" {if isset($field3['chk'])} checked="checked" {/if} rel="{$field3['offer_pkg_type']}" />
                                                            {$field3['title_offer_sub_pkg']}  ({$field3['domain_offer_sub_pkg']})
                                                        </label>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        {/foreach}
                                    {/if}
                                {else}    

                                    <div class="allCheckBo">                                    
                                        <div class="mainCheckbo">
                                            <label class="cb-checkbox checkAll">
                                                {ci_language line="Setting"}
                                            </label>
                                        </div>
                                        <div class="checkboSettings clearfix">
                                            <p class="col-md-6 p-t10 text-left">
                                                {ci_language line="Connector"}
                                            </p>
                                            <div class="col-md-6 top--4">
                                                <select id="id_marketplace" class="form-control">
                                                    <option value="{$id_general}">Mirakl</option>
                                                </select>  
                                            </div>
                                        </div>
                                        <div class="checkboSettings clearfix">
                                            <p class="col-md-6 p-t10 text-left">
                                                {ci_language line="Name"}
                                            </p>
                                            <div class="col-md-6 top--4">
                                                <select id="sub_marketplace" class="form-control">
                                                    <option value="">{ci_language line="please select"}</option>
                                                    {if isset($general_sub_domain)}
                                                        {foreach $general_sub_domain as $sub_domain}
                                                            {*if isset($sub_domain['selected']) && $sub_domain['selected'] == 1}
                                                            {$sub_domain_selected = $sub_domain['name']}
                                                            {/if*}
                                                            <option value="{$sub_domain['sub_marketplace']}" {*if isset($sub_domain['selected']) && $sub_domain['selected'] == 1}selected{/if*}>
                                                                {$sub_domain['name']}
                                                            </option>
                                                        {/foreach}
                                                    {/if}
                                                </select>
                                            </div>
                                        </div>        
                                    </div>


                                    {*<div class="row">
                                    <div class="col-xs-12">
                                    <ul class="nav nav-tabs">
                                    {foreach $general_sub_domain as $sub_domain}
                                    <li class="">
                                    <a href="#tab{$sub_domain['name']}" class="tab_general" data-toggle="tab">{$sub_domain['name']}</a>
                                    </li>
                                    {/foreach}
                                    </ul>
                                    </div>
                                    </div>*}

                                    <div id="tabGeneral">

                                        <div id="tab_selected">
                                            <ul class="nav nav-tabs">
                                                {foreach $general_sub_domain as $sub_domain}
                                                    <li class="">
                                                        {if isset($sub_domain['selected']) && $sub_domain['selected'] == 1}
                                                            <a href="#tab{$sub_domain['name']}" class="tab_general" data-toggle="tab">{$sub_domain['name']}</a>
                                                        {/if}
                                                    </li>
                                                {/foreach}
                                            </ul>

                                            <div class="tab-content">
                                                {foreach $general_sub_domain as $domain => $sub_domain}
                                                    {if isset($sub_domain['selected']) && $sub_domain['selected'] == 1}
                                                        <div class="tab-pane" id="tab{$sub_domain['name']}">
                                                            {foreach $sub_domain as $key => $region}
                                                                {if $key != 'selected' && $key != 'name' && $key != 'sub_marketplace'}
                                                                    <div class="allCheckBo">
                                                                        <div class="mainCheckbo">
                                                                            <label class="cb-checkbox checkAll">
                                                                                <input type="checkbox"/>
                                                                                {$region.name}
                                                                            </label>
                                                                        </div>
                                                                        {foreach $region.country as $country}
                                                                            <div class="checkboSettings clearfix">
                                                                                <label class="cb-checkbox pull-left">
                                                                                    <input type="checkbox" name="marketplace[{$field['id_offer_pkg']}][{$country['id_offer_price_pkg']}]" value="{$country['id_offer_price_pkg']}" {if isset($country['chk'])}checked="checked" {/if} rel="{$country['offer_pkg_type']}" />
                                                                                    {$country['title_offer_sub_pkg']}  ({$country['domain_offer_sub_pkg']})
                                                                                </label>
                                                                            </div>
                                                                        {/foreach}
                                                                    </div>
                                                                {/if}
                                                            {/foreach}
                                                        </div>
                                                    {/if}
                                                {/foreach}
                                            </div>
                                        </div>

                                        {if isset($general_sub_domain)}
                                            {foreach $general_sub_domain as $domain => $sub_domain}
                                                <div id="sub_marketplace_{$sub_domain['sub_marketplace']}" style="display: none;">
                                                    {foreach $sub_domain as $key => $region}
                                                        {if $key != 'selected' && $key != 'name' && $key != 'sub_marketplace'}
                                                            <div class="allCheckBo">
                                                                <div class="mainCheckbo">
                                                                    <label class="cb-checkbox checkAll">
                                                                        <input type="checkbox"/>
                                                                        {$region.name}
                                                                    </label>
                                                                </div>
                                                                {foreach $region.country as $country}
                                                                    <div class="checkboSettings clearfix">
                                                                        <label class="cb-checkbox pull-left">
                                                                            <input disabled type="checkbox" name="marketplace[{$field['id_offer_pkg']}][{$country['id_offer_price_pkg']}]" value="{$country['id_offer_price_pkg']}" {if isset($country['chk'])}checked="checked" {/if} rel="{$country['offer_pkg_type']}" />
                                                                            {$country['title_offer_sub_pkg']}  ({$country['domain_offer_sub_pkg']})
                                                                        </label>
                                                                    </div>
                                                                {/foreach}
                                                            </div>
                                                        {/if}
                                                    {/foreach}
                                                </div>
                                            {/foreach}
                                        {/if}
                                    </div>
                                {/if}
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="b-Top m-t20 p-t20">
                        <div class="pull-right ">
                            <a href="javascript:;"   class="pull-left link p-size m-tr10" onclick="bootbox.confirm('{ci_language line="Do you really want to reset?"}', function (result) {
                                        if (result)
                                            $('input:checkbox').prop('checked', false).change();
                                    });">
                                {ci_language line="Reset"}
                            </a>
                            <button type="submit" class="btn btn-save btnNext">{ci_language line="Save"}</button>                   
                        </div>
                    </div>		
                </div>
            </div>
        </form>


    {/if}
</div>

{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/FeedBiz/marketplace/configuration_popup.js"></script>