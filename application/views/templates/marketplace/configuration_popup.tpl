{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/marketplace/configuration_popup.css" />
 
<div class="{if !isset($popup)} main {/if} p-rl40 p-xs-rl20"> 
    <h1  class="lightRoboto text-uc head-size light-gray">{ci_language line="page_title"}</h1>
 
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"} 
    
    <div class="row">
        <div class="col-xs-12">
        <div class="widget widget-inverse">
            <div class="headSettings clearfix p-t10">
                <h4 class="headSettings_head">
                    {ci_language line="Please select your market places."}
                </h4>
            </div><!-- Widget Heading -->

             
                <form id="frmPackages" class="row" method="post" action="{$base_url}marketplace/configuration_data/{$popup}" enctype="multipart/form-data">
                    {if isset($default_market)}
                        <input name="force_redirect_wizrd" value ="{$default_market}" type="hidden">
                        {/if}
                    <div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
                                    {foreach $marketplace as $field key=pkgid}
					<li {if isset($default_market) && $default_market == strtolower($field['name_offer_pkg'])}class="active"
                                            {else if !isset($default_market) && $pkgid==2}class="active"{/if} {if isset($default_market) && $pkgid != $default_market}style="display:none;"{/if}><a href="#pkg{$pkgid}" data-toggle="tab">{$field['name_offer_pkg']}</a></li>
                                    {/foreach}				
				</ul>
			</div>
                    </div>
                    
                    <div class="row">
			<div class="col-xs-12 configuration">
				<div class="tab-content">
                                    {foreach $marketplace as $field key=cpkgid}
                                        
                                        <div class="tab-pane{if isset($default_market) && $default_market == strtolower($field['name_offer_pkg'])}class="active"
                                            {else if !isset($default_market) && $cpkgid==2}class="active"{/if}" id="pkg{$cpkgid}" {if isset($default_market) && strtolower($field['name_offer_pkg']) != $default_market}style="display:none;"{/if}  >
                                                    {if $field['submenu_offer_pkg'] == 1 && isset($field['sub_marketplace'])}
                                                    {foreach $field['sub_marketplace'] as $field2}
						<div class="allCheckBo">
							<div class="mainCheckbo">
								<label class="cb-checkbox checkAll">
									<input type="checkbox"/>
									{ci_language line=$field2.name}
								</label>
							</div>
                                                        {foreach $field2.country as $field3}
							<div class="checkboSettings clearfix">
								<label class="cb-checkbox pull-left">
									<input type="checkbox" name="marketplace[{$field['id_offer_pkg']}][{$field3['id_offer_sub_pkg']}]" value="{$field3['id_offer_price_pkg']}" {if isset($field3['chk'])} checked="checked" {/if} rel="{$field3['offer_pkg_type']}" />
									{$field3['title_offer_sub_pkg']}  ({$field3['domain_offer_sub_pkg']})
								</label>
							</div>
                                                        {/foreach}
							 
						</div>
                                                    {/foreach}
                                                {/if}
					</div>
                                    {/foreach}
				</div>
			</div>
		</div>
                    
                    <div class="form-actions row mgt20 mgbt40">
                        <div class="col-xs-6 text-left">
                           {* <div class="btn btn-default btn_back">
                                <i class="fa fa-arrow-left"></i> 
                                {ci_language line="Back"}
                            </div>   *}
                        </div>
                        <div class="col-xs-6 text-right"> 
                             
                            <div  class="btn btn-save btn_submit">
                            <i class="fa fa-save"></i> 
                            {ci_language line="Save and continue"}
                            </div>
                        </div>
                    </div>

                    <input type="hidden" id="select_atl_once" value="{ci_language line="Please select your market place site at least once."}">
                </form>
            </div>
        </div>
    </div>    
</div>
                        
{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/FeedBiz/marketplace/configuration_popup.js"></script>