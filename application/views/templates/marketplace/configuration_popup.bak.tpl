{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/marketplace/configuration_popup.css" />

<div {if !isset($popup) || !$popup}class="main-content"{/if}>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"} 
    <div class="page-content">
        <div class="page-header position-relative">
            <h1>
                {ci_language line="page_title"}
                <small>
                    <i class="icon-double-angle-right"></i> 
                    {ci_language line="configuration"}
                </small>
            </h1>
        </div>
        
        <div class="row-fluid">
            
            <div class=" offset1 span10">  
                
                <form id="frmPackages" method="post" action="{$base_url}marketplace/configuration_data/{$popup}" enctype="multipart/form-data">

                <div id="step1">
                    <div class="page-header position-relative"><h1>{ci_language line="Please select your market places."}</h1></div>
                    <ul id="market_sel_base" class="span12">
                        <li class="span6 center">
                            <div class="market_place_sel_btn btn   btn-light btn-large" id="amazon">
                                <i class="icon-ok bigger-150"></i>
                                <div class="market_logo"></div>
                                
                            </div>
                        </li>
                        <li class="span6 center">
                            <div class="market_place_sel_btn btn   btn-light btn-large" id="ebay">
                                <i class="icon-ok bigger-150"></i>
                                <div class="market_logo"></div>
                                
                            </div>
                        </li>
                    </ul> 
                    <div class="span12 text-right" style=" margin-top:20px">
                        <div class="form-actions right  " style=" display: none">
                            <div class="btn btn-primary">
                                <i class="icon-check"></i> 
                                {ci_language line="Continue"}
                            </div> &nbsp; &nbsp; 

                        </div>
                    </div>
                </div>   
                <div id="step2">
                    <div class="page-header position-relative"><h1>{ci_language line="Please select your market place sites."}</h1></div>

                    <ul class="unstyled clearfix" >
                        {if $marketplace}
                            
                            {foreach $marketplace as $field}
                                <li  id="{strtolower($field['name_offer_pkg'])}_tab" rel="{strtolower($field['name_offer_pkg'])}" class="market_conf_list">
                                    <div class="widget-box     ">
                                        <div class="widget-header">
                                            <h5>
                                               <div class="market_logo"></div>
                                            
                                            </h5>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">

                                                {if $field['submenu_offer_pkg'] == 1 && isset($field['sub_marketplace'])}
                                                    {foreach $field['sub_marketplace'] as $field2}
                                                        <h4 class="green smaller">{$field2.name}</h4>
                                                        <ul class="unstyled list-striped pricing-table-header" style="border: 1px solid #eee;">
                                                            {foreach $field2.country as $field3}
                                                                <li style="padding: 5px 20px;">
                                                                    <div class="row-fluid">
                                                                        <div class="span10" style="min-height: 0">
                                                                            <i class="icon-angle-right"></i> &nbsp; 
                                                                            {$field3['title_offer_sub_pkg']}  - <span class="grey">{$field3['domain_offer_sub_pkg']}</span>
                                                                        </div>
                                                                        <div class="span2" style="min-height: 0">
                                                                            <label>
                                                                                <input type="checkbox" name="marketplace[{$field['id_offer_pkg']}][{$field3['id_offer_sub_pkg']}]" value="{$field3['id_offer_price_pkg']}" {if isset($field3['chk'])} checked {/if} rel="{$field3['offer_pkg_type']}" />
                                                                                <span class="lbl"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            {/foreach}
                                                        </ul>
                                                    {/foreach}
                                                {/if}
                                            </div>
                                        </div>

                                </li>
                            {/foreach}
                        {/if}
                    </ul>
                    
                    <div class="form-actions row" style="margin-top:20px">
                        <div class="col-xs-6 text-left">
                            <div class="btn btn btn-light btn_back">
                                <i class="icon-share-alt"></i> 
                                {ci_language line="Back"}
                            </div>   
                        </div>
                        <div class="col-xs-6 text-right"> 
                            <div class="btn btn-primary btn_next">
                                <i class="icon-check"></i> 
                                {ci_language line="Continue"}
                            </div>
                            <div  class="btn btn-primary btn_submit" style=" display: none;">
                            <i class="icon-check"></i> 
                            {ci_language line="Save & Continue"}
                            </div>
                        </div>
                    </div>
                </div>                    
                </form>
            </div>
                    
        </div>
        
    </div>
    
</div>
                        
{include file="footer.tpl"}
<script src="{$base_url}assets/js/FeedBiz/marketplace/configuration_popup.js"></script>