{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/marketplace/display_conf_link.css" />

<div {if !isset($popup) || !$popup}class="main-content"{/if}>
    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    {include file="popup_step.tpl"} 
    <div class="page-content">
         
        
        <div class="row-fluid">            
            <div class=" offset1 span10">                 
                <div id="step1">
                    <div class="page-header position-relative"><h1>{ci_language line="Congatulation you have success for configuration of first login."}</h1></div>
                </div>     
            </div>                    
        </div>
                    
        <div class="row-fluid" style="text-align: center;margin-top: 50px;">
            {if isset($amazon_btn) || isset($ebay_btn)}
                <div class="page-header position-relative row-fluid "><h5>{ci_language line="And, we recommend to configuration your marketplace account and send your items to marketplaces."}</h5></div>
            {/if}
                {if isset($amazon_btn)}
                <a href="{$base_url}dashboard/index/amazon_synchronize_wizard" class="btn btn-primary wizard" type="button" id="amazon_synchronize_wizard" style="margin:10px 30px">
                <i class="icon-beaker bigger-125"></i> 
                {ci_language line="Amazon Wizard"}
                </a>
                {/if}
                {if isset($ebay_btn)}
                <a  href="{$base_url}dashboard/index/ebay_wizard"  class="btn btn-primary wizard" type="button" id="ebay_wizard"  style="margin:10px 30px">
                <i class="icon-beaker bigger-125"></i>
                {ci_language line="eBay Wizard"}
                </a>
                {/if}
        </div>                
        <div class="row-fluid" style="text-align:right; margin:100px 0 0 0 ">
            <a  href="javascript: parent.jQuery.colorbox.close();"  class="btn btn-success" type="button" id="ebay_wizard"  style="margin:0 80px">
            <i class="fa fa-reply"></i> {ci_language line="Close"}
            </a>
        </div>        
    </div>    
</div>
                        
{include file="footer.tpl"}
<script src="{$base_url}asset/js/FeedBiz/marketplace/display_conf_link.js"></script>