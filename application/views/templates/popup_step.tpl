{if isset($popup) && $popup != ''}  
    <input type="hidden" id="popup_status" value="{$popup}">

            <div class="headSettings clearfix m-b10">
                    <ul class="headSettings_point clearfix">
                            <li class="headSettings_point-item {if $popup >= '1'} active{/if}" style="width: 25%;">
                                    <span class="step">1</span>
                                    <span class="title">{ci_language line="popup_step1"}</span>
                            </li>
                            <li class="headSettings_point-item {if $popup >= '2'} active{/if}" style="width: 25%;">
                                    <span class="step">2</span>
                                    <span class="title">{ci_language line="popup_step2"}</span>
                            </li>
                            <li class="headSettings_point-item {if $popup >= '3'} active{/if}" style="width: 25%;">
                                    <span class="step">3</span>
                                    <span class="title">{ci_language line="popup_step4"}</span>
                            </li>
                            <li class="headSettings_point-item {if $popup >= '4'} active{/if}" style="width: 25%;">
                                    <span class="step">4</span>
                                    <span class="title">{ci_language line="popup_step5"}</span>
                            </li>
                            {*<li class="headSettings_point-item{if $popup >= '5'} active{/if}" style="width: 20%;">
                                    <span class="step">5</span>
                                    <span class="title">{ci_language line="popup_step5"}</span>
                            </li>*}
                    </ul>
            </div>

{/if}