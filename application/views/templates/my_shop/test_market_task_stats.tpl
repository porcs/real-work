{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_feed/my_products.css" />

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="My schedule tasks"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="row">
        <div class="col-xs-12">
            
             
            <div class="row">
                <div class="col-xs-12 statistics">
                    <table class=" custom-form" id="my_task_list" ext="{$ext}" market="{$market}">
                        <thead class="table-head">
                            <tr>
                                <th><label class="cb-checkbox"><input class="sel_all" type="checkbox" >{ci_language line="Allow automatic update"}</label></th>
                                <th>{ci_language line="Tasks"}</th>
                                <th>{ci_language line="Checked time"}</th>
                                 <th class='center-align'>{ci_language line="Status"}</th>
                                <th class='center-align'>{ci_language line="Next time"}</th>
                            </tr>
                        </thead>
                        {if isset($tasks)}
                        <tbody>
                            {foreach from=$tasks key=key item=value}
                                <tr class="task_row" rel="{$value['task_name']}">
                                    <td><label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="{$value['task_key']}" crel='{$value['task_id']}' {if $value['task_running'] == true}checked{/if}>{$value['task_display_name']}</label></td>
                                    <td>{ci_language line=$value['task_name']}</td>
                                    <td class="t_time">{$value['task_lastupdate']}</td>
                                     <td class='center-align'><div class="t_status {$value['task_status']}"></div></td>
                                    <td class='center-align'><div class="countdown_next" rel="{$countdown[$value['task_key']]}"></div></td>
                                    
                                </tr>
                            {/foreach}
                        </tbody>
                        {/if}
                    </table>

                </div>
            </div>
        </div>			
    </div>
</div>

<input type="hidden" id="processing" value="Processing...">

{*<script src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>*}
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/countdown.js"></script> 
<style>
    .countdown_next.countdownHolder .break_sign  {  position: absolute;  right: -12px;  top: -8px;  font-weight: bold;  font-size: 16px;   display: inline-block;   width: 7px;}
    .countdown_next.countdownHolder { margin:0px auto;text-align:left; width:82px; }
    .countdown_next.countdownHolder > span { border:none;padding:0px; margin: 2px 12px 0 0; position: relative }
    .countdown_next.countdownHolder .position span{ font-size:16px;}
    .countdown_next.countdownHolder .position { width:6px;}
    .t_status { width:20px; height: 20px; border-radius: 40px; background-color:#f74f63; margin: 0 auto }
    .t_status.green{ background-color:#00ce88 }
    .center-align{ text-align: center}
    .right-align{ text-align: right} 
    #my_task_list{ table-layout: auto;width:100% !important;font-size: 12px !important;font-family: 'Roboto', sans-serif !important;font-weight: 400 !important;}
    #my_task_list tr.ch_row td table td{   padding-top:5px; padding-bottom: 5px }
    #my_task_list tr td{ padding:0px 0px; overflow: hidden; box-sizing: border-box;vertical-align: middle; color:#777777;} 
    #my_task_list tr.odd td{ padding:5px 0;} 
    #my_task_list tr.even td{ padding:5px 0;}
    #my_task_list tr td>span{ display:none !important}
     
    
</style>
<script>
$(document).ready(function () {
    $('#my_task_list').DataTable({ searching:false, paging: false});
    var ext = $('#my_task_list').attr('ext');
    var market = $('#my_task_list').attr('market');
    
    $('.countdown_next').each(function(){      
        console.log($(this).attr('rel'));
        $(this).countdown({
            timestamp	: (new Date()).getTime()+ $(this).attr('rel')*1000,
            callback	: function(days, hours, minutes, seconds){},
            break_sign  : ':'
        });        
    });
    
    $('#my_task_list input').checkBo();
    setTimeout(function(){
    $('#my_task_list input[type=checkbox]').trigger('change');
{*        console.log($('#my_task_list input'));*}
        $('#my_task_list input[type=checkbox]').change(function(){
            if($(this).hasClass('sel_all')){
                $('#my_task_list input[type=checkbox].sel_row').prop('checked',$(this).is(':checked')).trigger('change');
            }else{
{*                console.log($(this),$(this).is(':checked'));*}
                
                var cid = $(this).attr('crel');
                var status = $(this).is(':checked');
                $.ajax({ method:'post',url: base_url+'users/ajax_update_cron_status',data:{ ext:ext,cid:cid,status:status},
                    success:function(data){
                        console.log(data);
                    }
                });
            }
            
        });
        var run = [];
        $(".task_row").each(function(){
            var rel = $(this).attr('rel');
            if(rel!='order'){
                rel = 'product';
                $(this).attr('rel',rel);
            }
            if(!run[rel]){
                run[rel]=true;
                $.ajax({ method:'post',url: base_url+'users/ajax_get_serv_status',data:{ ext:ext,market:market,task:rel},dataType:'json',
                    success:function(data){
{*                        console.log(data);*}
                        var status = data.status;
                        var timestamp = data.timestamp;
                        $('#my_task_list').find('tr.task_row[rel='+rel+']').each(function(){
                           $(this).find('.t_time').html(timestamp);
                           $(this).find('.t_status').addClass(status);
                        })
                    }
                });
            }
        })
    },100);
    
    
});
</script>
{include file="footer.tpl"}
