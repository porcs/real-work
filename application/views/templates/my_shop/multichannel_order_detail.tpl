{if !isset($error) || empty($error)}
    <div class="col-xs-12">
        <div class="headSettings clearfix p-0 b-Bottom m-b20">
            <h4 class="headSettings_head">{ci_language line="ORDER"}</h4>                      
        </div>
    </div>
    <div class="col-md-12 form-group">
            <label class="col-sm-3">{ci_language line="Order ID"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderId">{if isset($DisplayableOrderId)}{$DisplayableOrderId}{/if}</p></div>
            <label class="col-sm-3">{ci_language line="Order Status"}</label>
            <div class="col-sm-3">{if isset($FulfillmentOrderStatus)}<p class="{if $FulfillmentOrderStatus=="CANCELLED"}true-pink{else if "COMPLETE"}true-green{else}warning-text{/if}" rel="FulfillmentOrderStatus">{$FulfillmentOrderStatus}</p>{/if}</div>
    </div>
    <div class="col-md-12 form-group">
            <label class="col-sm-3">{ci_language line="Received On"}</label>
            <div class="col-sm-3"><p rel="ReceivedDateTime">{if isset($ReceivedDateTime)}{$ReceivedDateTime}{/if}</p></div>
            <label class="col-sm-3">{ci_language line="Last Update"}</label>
            <div class="col-sm-3"><p rel="StatusUpdatedDateTime">{if isset($StatusUpdatedDateTime)}{$StatusUpdatedDateTime}{/if}</p></div>
    </div>
    <div class="col-md-12 form-group">
            <label class="col-sm-3">{ci_language line="Fulfillment Method"}</label>
            <div class="col-sm-3"><p rel="FulfillmentMethod">{if isset($ReceivedDateTime)}{$ReceivedDateTime}{/if}</p></div>
            <label class="col-sm-3">{ci_language line="Shipping Time Category"}</label>
            <div class="col-sm-3"><p rel="ShippingSpeedCategory">{if isset($ShippingSpeedCategory)}{$ShippingSpeedCategory}{/if}</p></div>
    </div>
    <div class="col-md-12 form-group">
            <label class="col-sm-3">{ci_language line="Estimated Shipping Date"}</label>
            <div class="col-sm-3"><p class="true-green" rel="EstimatedShipDateTime">{if isset($EstimatedShipDateTime)}{$EstimatedShipDateTime}{/if}</p></div>
            <label class="col-sm-3">{ci_language line="Estimated Arrival Date"}</label>
            <div class="col-sm-3"><p class="warning-text" rel="EstimatedArrivalDateTime">{if isset($EstimatedArrivalDateTime)}{$EstimatedArrivalDateTime}{/if}</p></div>
    </div>
{/if}

{if isset($Address)}
    <div class="col-xs-12">
        <div class="headSettings clearfix p-0 b-Bottom m-b20">
            <h4 class="headSettings_head">{ci_language line="ADDRESS"}</h4>                      
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-sm-3">{ci_language line="Name"}</label>
        <div class="col-sm-9"><p rel="AddressName">{if isset($Address['Name'])}{$Address['Name']}{/if}</p></div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-sm-3">{ci_language line="Address"}</label>
        <div class="col-sm-9" rel="Address">{if isset($Address['Line'])}<p>{$Address['Line']}</p>{/if} 
            {if isset($Address['City'])}<p>{$Address['City']}</p>{/if} 
            {if isset($Address['PostalCode'])}<p>{$Address['PostalCode']}</p>{/if}</div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-sm-3">{ci_language line="Phone Number"}</label>
        <div class="col-sm-9"><p rel="AddressPhoneNumber">{if isset($Address['PhoneNumber'])}{$Address['PhoneNumber']}{/if}</p></div>
    </div>
{/if}

{if isset($Shipment)}
    <div class="col-xs-12">
        <div class="headSettings clearfix p-0 b-Bottom m-b20">
            <h4 class="headSettings_head">{ci_language line="SHIPMENT"}</h4>                      
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-sm-3">{ci_language line="Amazon Shipment Id"}</label>
        <div class="col-sm-9"><p rel="AmazonShipmentId">{if isset($Shipment['AmazonShipmentId'])}{$Shipment['AmazonShipmentId']}{/if}</p></div>
    </div>
    <div class="col-md-12 form-group">
            <label class="col-sm-3">{ci_language line="Tracking Number"}</label>
            <div class="col-sm-3"><p rel="TrackingNumber">{if isset($Shipment['TrackingNumber'])}{$Shipment['TrackingNumber']}{/if}</p></div>
            <label class="col-sm-3">{ci_language line="Carrier Code"}</label>
            <div class="col-sm-3"><p rel="CarrierCode">{if isset($Shipment['CarrierCode'])}{$Shipment['CarrierCode']}{/if}</p></div>
    </div>
{/if}

{if isset($Items)}
    <div class="col-xs-12">
        <div class="headSettings clearfix p-0">
            <h4 class="headSettings_head">{ci_language line="ITEMS"}</h4>                      
        </div>
    </div>
    <div class="col-xs-12 form-group">
        <table id="order-fba-detail" style="border-spacing: 2px;" class="table">
            <thead>
                <tr>
                    <th style="text-align:left;width:180px;border-bottom:none;">{ci_language line="SKU"}</th>
                    <th style="text-align:right;width:180px;border-bottom:none;">{ci_language line="Quantity"}</th>
                    <th style="text-align:right;width:180px;border-bottom:none;">{ci_language line="Value"}</th>
                </tr>
            </thead>
            <tbody>
                {foreach $Items as $key => $Item}
                    {if isset($Item['SKU'])}
                        <tr>
                            <td style="font-weight:bold;border-bottom:none;">
                                {if isset($Item['SKU'])}{$Item['SKU']}{/if}
                            </td>
                            <td style="text-align:right;font-weight:bold;border-bottom:none;">
                                {if isset($Item['Quantity'])}{$Item['Quantity']}{/if}
                            </td>
                            <td style="text-align:right;font-weight:bold;border-bottom:none;">
                                {if isset($Item['PerUnitDeclaredValueCurrencyCode'])}{$Item['PerUnitDeclaredValueCurrencyCode']}{/if} 
                                {if isset($Item['PerUnitDeclaredValueValue'])}{$Item['PerUnitDeclaredValueValue']}{/if} 
                            </td>
                        </tr>
                    {/if}
                {/foreach}
            </tbody>
            </table>
            </div>
{else}
        {if isset($error)}
                <table id="order-fba-detail" style="border-spacing: 5px;" class="table">
                        {if is_array($error)}
                                <tr>
                                        <td style="font-weight:bold;color:red;border-bottom:none;">
                                                <ul class="list-circle-red">
                                                        {foreach $error as $err}
                                                                {if !empty($err)}
                                                                        <li>{$err}</li>
                                                                {/if}
                                                        {/foreach}
                                                <ul>
                                        </td>
                                </tr>
                        {else}
                                <tr>
                                        <td style="font-weight:bold;color:red;border-bottom:none;">{$error}</td>
                                </tr>               
                        {/if}
                </table>
        {/if}
{/if}