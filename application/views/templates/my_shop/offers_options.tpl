{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<div class="main p-rl40 p-xs-rl20">

    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Offers Options"}</h1>

    {include file="breadcrumbs.tpl"}

    <div class="row custom-form">

        <div class="col-xs-12">
            <div class="headSettings clearfix b-Top">
                <div class="col-xs-8 clearfix">
                    <div class="row">
                        <h4 class="p-0 headSettings_head">{ci_language line="by Products"}</h4>
                    </div>
                </div>
                {* <div class="col-xs-6 clearfix">
                    <div class="showColumns clearfix">                        
                        <div class="col-xs-6" id="search-category">
                                <select id="category" class="search-select">
                                    <option value=""> - {ci_language line="Filter by category"} - </option>
                                {if isset($category)}{$category}{/if}
                            </select>
                        </div>
                    </div> 
                </div>
                *}
            </div>
        </div>
    
        <div class="col-xs-12">
            <form method="post" id="product_options_form" autocomplete="off" >
                <table class="responsive-table" id="product_options">
                    <thead class="table-head">	
                        <tr>
                            <th colspan="11">
                                <div class="header-control row">
                                    <div class="col-sm-7 clearfix">
                                        <div class="row">
                                            <div class="pull-left" >
                                                <h4 class="p-l5 text-uc dark-gray montserrat">{ci_language line="Filter by Create date"} : </h4>
                                            </div>
                                            <div class="pull-left" >    
                                                <div class="p-l5 row">                
                                                    <div class="col-sm-6">
                                                        <div class="calendar firstDate">
                                                            <input class="form-control order-date" type="text" id="order-date-from" placeholder="{ci_language line="From"}">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="calendar lastDate">
                                                            <input class="form-control order-date" type="text" id="order-date-to" placeholder="{ci_language line="To"}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 clearfix">
                                        <div class="row">
                                            <div class="pull-left" >
                                                <h4 class="p-l5 text-uc dark-gray montserrat p-r18">{ci_language line="Filter by Category"} : </h4>
                                            </div>
                                            <div class="pull-left">
                                                <div class="p-l5 row"> 
                                                    <div class="col-sm-12">
                                                        <div id="search-category">
                                                            <select id="category" class="search-select">
                                                                <option value=""> - {ci_language line="Filter by category"} - </option>
                                                                {if isset($category)}{$category}{/if}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </th>
                        </tr>  
                           
                        <tr>
                            <th colspan="11">
                                <div class="header-control row">
                                    <div class="col-xs-8 pull-left m-t7" >
                                        <a class="link" data-toggle="modal" data-target="#importProductOptions" >
                                            <i class="fa fa-upload"></i>  {ci_language line="Import Offers Options"}
                                        </a>                                
                                        <a class="link m-l20" data-toggle="modal" data-target="#DownloadProductOptions" >
                                            <i class="fa fa-download"></i>  {ci_language line="Download Offers Options"}
                                        </a>		                        
                                    </div>  
                                    <div class="col-sx-4 pull-right" >
                                        <div class="p-b0 m-b0 m-r5">			                               
                                            <button type="button" class="btn btn-save m-b0" id="submit_product_options" disabled="disabled">
                                                {ci_language line="Submit"}
                                            </button>
                                            <div class="status" style="display: none">		                                        	
                                                <p class="p-t10 m-l5 p-b0 m-b0">
                                                    <i class="fa fa-spinner fa-spin"></i> 
                                                    <span>{ci_language line="Submiting"}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </th>								
                        </tr>
                        <tr>
                            <th rowspan="2"></th>
                            <th rowspan="2"><label class="cb-checkbox"> <input type="checkbox" id="select_all_opt" checked="checked"/> </label></th>
                            <th rowspan="2">{ci_language line="SKU"}</th>
                            <th rowspan="2" class="text-center">{ci_language line="Price Override"}</th>
                            <th rowspan="2" class="text-center">{ci_language line="Disabled"}</th>
                            <th rowspan="2" class="text-center">{ci_language line="Force"}</th>
                            <th colspan="2" class="text-center">{ci_language line="Do not export"}</th>
                            <th rowspan="2" class="text-center">{ci_language line="Latency"}</th>
                            <th rowspan="2" class="text-center">{ci_language line="Shipping"}</th>								
                            <th rowspan="2"></th>
                        </tr>					
                        <tr class="text-center">
                            <th class="text-center">{ci_language line="Price"}</th>
                            <th class="text-center">{ci_language line="Quantity"}</th>
                        </tr>
                    </thead>						 
                </table>
            </form>
            <table class="table tableEmpty">
                <tr>
                    <td>{ci_language line="No data available in table"}</td>
                </tr>
            </table>					 
        </div>
        	
    </div>	
    
    <!-- Main child table -->
    <div id="model" style="display:none">
        <div class="row-edit p-10 p-l40 custom-form">
            <table cellpadding="5" cellspacing="5" border="0" class="table_detail" >
                <!--Hidden Input-->
                <tr style="display:none">
                    <td width="30%">
                        <input type="hidden" class="id_product form-control" rel="id_product" />
                        <input type="hidden" class="id_product_attribute form-control" rel="id_product_attribute" />
                        <input type="hidden" class="sku form-control" rel="sku" />                          
                        <input type="hidden" class="parent_sku form-control" rel="parent_sku" />
                    </td>
                    <td width="70%"></td>                    
                </tr>           
                <!-- Price Override -->
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Price Override"} : </p></td>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">                              
                                <input type="text" class="form-control" rel="price" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" />
                            </div>
                            <div class="col-xs-8">
                                <p class="m-t5 poor-grey help-text m-0">
                                    {ci_language line="Net Price for Marketplace. This value will replace your regular price"}
                                </p>
                            </div>
                        </div>
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Disabled"} : </p></td>
                    <td>
                        <label class="cb-checkbox"></i><input type="checkbox" class="" value="1" rel="disable" />
                            {ci_language line="Check this box to never export this product to the marketplace"}</label>                     
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Force in Stock"} : </p></td>
                    <td>
                        <label class="cb-checkbox" onclick="show_force(this)" ></i><input type="checkbox" rel="force_chk"/>
                            {ci_language line="This quantity will appear on the marketplace, even it's out of Stock"}</label>
                        <div class="row clearfix" style="display:none">
                            <div class="col-xs-4">
                                <input type="text" rel="force" class="form-control" disabled onkeyup="this.value = this.value.replace(/[^\d]/, '')"/> 
                            </div>
                        </div>                      
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Price"} : </p></td>
                    <td>
                        <label class="cb-checkbox"></i><input type="checkbox" class="" value="1" rel="no_price_export"/>
                            {ci_language line="Do not synchronize the price"}</label>
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Quantity"} : </p></td>
                    <td>
                        <label class="cb-checkbox"></i><input type="checkbox" class="" value="1" rel="no_quantity_export"/>
                            {ci_language line="Do not synchronize the quantity"}</label>
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Latency"} : </p></td>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">
                                <input type="text" class="form-control" rel="latency" onkeyup="this.value = this.value.replace(/[^\d]/, '')"/>
                            </div>
                            <div class="col-xs-8">
                                <p class="m-t5 poor-grey help-text m-0">
                                    {ci_language line="Latency delay in days before this product will be shipped."}                         
                                </p>
                            </div>
                        </div> 
                    </td>
                </tr> 
                <tr>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Shipping Override"} : </p></td>
                    <td>
                        <div class="row">
                            <div class="col-xs-4">
                                <input type="text" class="form-control" rel="shipping" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" />
                            </div>
                            <div class="col-xs-8">
                                <p class="poor-grey help-text m-0 m-t5">
                                    {ci_language line="Shipping fee override, the shipping fee will be replaced by this value."}                            
                                </p>
                            </div>
                        </div> 
                    </td>
                </tr> 
            </table>        
        </div>
    </div>

    <div class="modal fade" id="importProductOptions">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                                {ci_language line="Import Offers Options"}
                            </h1>
                        </div>
                    </div>
                    <div class="row m-t10"> 
                        <form class="upload-template" method="POST" enctype="multipart/form-data" > 
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <i class="fa fa-file-o"></i> {ci_language line="Browse"}&hellip; <input type="file" class="form-control">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" readonly>
                                </div>
                                <div class="progressbar col-md-12 p-t35 hidden">
                                    <div class="relative">
                                        <div class="progressCondition">
                                            <div class="progressLine" data-value="5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-save m-b0" id="upload_product_options" ><i class="fa fa-upload"></i> {ci_language line="Upload File"}</button>
                                <a href="{$base_url}/my_shop/product_options_download_template" type="button" class="btn btn-default  m-l5 download_template" target="_blank">  
                                    <i class="fa fa-download"></i> {ci_language line="Download Template"}
                                </a>
                                <div class="status" style="display: none">                                                  
                                    <p class="p-t10 m-l5 p-b0 m-b0">
                                        <i class="fa fa-spinner fa-spin"></i> 
                                        <span>{ci_language line="Starting upload"}</span>
                                    </p>
                                </div>
                            </div>
                        </form>                 
                    </div>  
                    <div id="result_product" style="display:none">
                        <div id="product_summary" class="row">
                            <div class="col-xs-12">
                                <p class="head_text">{ci_language line="Summary"}</p>
                                <div class="col-xs-4">
                                    <p>{ci_language line="Upload"} : <span id="product_summary_upload"></span> {ci_language line="product(s)"}</p>
                                </div>
                                <div class="col-xs-4">
                                    <p>{ci_language line="Success"} : <span id="product_summary_success"></span> {ci_language line="product(s)"}</p>
                                </div>
                                <div class="col-xs-4">
                                    <p>{ci_language line="Error"} : <span id="product_summary_error"></span> {ci_language line="product(s)"}</p>
                                </div>
                            </div>
                        </div>

                        <div id="product_with_error" class="row m-t10" style="display:none">
                            <div class="col-xs-12">
                                <p class="head_text">{ci_language line="Product with error"}</p>
                                <div id="error_product" class="col-xs-12"></div>
                            </div>
                        </div>  
                    </div>      
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="DownloadProductOptions">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                                {ci_language line="Download Offers Options"}
                            </h1>
                        </div>
                    </div>
                    <div class="m-t10 custom-form"> 
                        <form action="{$base_url}/my_shop/product_options_download" id="download-product-options" method="POST" enctype="multipart/form-data" > 

                            <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Option"} :</p>

                            <div class="row">
                                <div class="col-xs-12">     
                                    <div class="form-group"> 
                                        <div class="col-md-2">
                                            <label class="cb-checkbox">
                                                <input type="checkbox" id="only_active" name="only_active" value='1'> {ci_language line="Only Active"}
                                            </label>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="cb-checkbox">
                                                <input type="checkbox" id="only_in_stock" name="only_in_stock" value='1'> {ci_language line="Only in Stock"}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">  
                                <div class="col-xs-8">  
                                    <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Filter by category"} : </p> 
                                    <div id="download_product_options_categories">
                                        <div class="form-group withIcon clearfix">  
                                            <div class="col-xs-12">
                                                <select class="search-select" name="category[]">
                                                    <option value=''> - - </option>
                                                    {if isset($category)}{$category}{/if}
                                                </select>
                                                <i class="cb-plus good" id="download_product_options_category"></i>
                                                <i class="cb-plus bad" style="display: none"></i>
                                            </div>
                                        </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="text-right m-t15 b-Top p-t10">  
                                        <button type="submit" class="btn btn-save m-b0" id="download_product_options" >{ci_language line="Submit"}</button>
                                        <div class="status" style="display: none">                                                  
                                            <p class="p-t10 m-l5 p-b0 m-b0">
                                                <i class="fa fa-spinner fa-spin"></i> 
                                                <span>{ci_language line="Starting upload"}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>                 
                    </div>                      
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
		
</div>

<p class=" text-small poor-gray">
    <span style="color:red">*</span> 
    {ci_language line="The product are missing Reference/SKU."}
</p>

</div>

<input type="hidden" id="shop_name" value="{if isset($shop_name)}{$shop_name}{/if}" />
<input type="hidden" id="shipping_type_1" value="{ci_language line="Standard"}">
<input type="hidden" id="shipping_type_2" value="{ci_language line="Express"}">
<input type="hidden" id="shipping_type_0" value="">
<input type="hidden" id="l_Error" value="{ci_language line="Error"}">
<input type="hidden" id="l_Success" value="{ci_language line="Success"}">
<input type="hidden" id="l_Submit" value="{ci_language line="Submiting"}">
<input type="hidden" id="l_Edit" value="{ci_language line="Edit"}">
<input type="hidden" id="l_Reset" value="{ci_language line="reset"}">
<input type="hidden" id="l_Mismatch" value="{ci_language line="Item unactivated: Multiple edition is possible only for matching values"}">
<input type="hidden" id="reset-confirm" value="{ci_language line="Do you want to reset "}">
<input type="hidden" id="l_upload_fail" value="{ci_language line="upload fail, please try again"}">
<input type="hidden" id="l_please_check" value="{ci_language line="Your file have many error please check"}">
<input type="hidden" id="l_missing_order-date-from" value="{ci_language line="Please select date form."}">
<input type="hidden" id="l_missing_order-date-to" value="{ci_language line="Please select date to."}">

{include file="IncludeDataTableLanguage.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/jquery.ajax-progress.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/my_shop/offers_options.js"></script>

{include file="footer.tpl"}
