{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_shop/my_products.css" />

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="My products"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings statistics clearfix">	
                <div class="  m-t10">

                    <div class="col-lg-3 col-md-12">
                        <div class=" ">
                            <div class="col-sm-12 col-md-12" id="my_prod_list_filter_container">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class=" ">
                            <div class="col-sm-4 col-md-5">
                                <p class="m-t10 light-gray">{ci_language line="Filter by conditions"}</p>
                            </div>
                            <div class="col-sm-8 col-md-7">
                                <select id="my_prod_list_filter_condition">
                                    <option value="">{ci_language line="All conditions"}</option>
                                    {foreach $shop_condition as $id => $s_cond}
                                        <option value="{$id}" >{ucfirst(strtolower($s_cond['txt']))}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class=" ">
                            <div class="col-sm-4 col-md-5">
                                <p class="m-t10 light-gray">{ci_language line="Filter by category"}</p>
                            </div>
                            <div class="col-sm-8 col-md-7">
                                <select id="my_prod_list_filter_category">
                                    <option value="">{ci_language line="All categories"}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                            <div class="col-sm-4 col-md-5">
                                <p class="m-t10 light-gray">{ci_language line="Per Page"}</p>
                            </div>
                            <div class="col-sm-4 col-md-7" id="my_prod_list_length_container">
                            </div>
                            <div class="  col-sm-4 col-md-12">
                                 <div class="showColumns clearfix">
                                        <a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
                                        <div class="dropCheckbo">
                                                <ul class="custom-form"> 
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_id'])&&$col_fil['p_id']}checked{/if}">
                                                                        <input type="checkbox" name="p_id" {if isset($col_fil['p_id'])&&$col_fil['p_id']}checked{/if}/>
                                                                        {ci_language line="Product ID"}
                                                                </label>
                                                        </li>  
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_img'])&&$col_fil['p_img']}checked{/if}">
                                                                        <input type="checkbox" name="p_img" {if isset($col_fil['p_img'])&&$col_fil['p_img']}checked{/if}/>
                                                                        {ci_language line="Image"}
                                                                </label>
                                                        </li>
                                                         
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['cat_name'])&&$col_fil['cat_name']}checked{/if}">
                                                                        <input type="checkbox" name="cat_name" {if isset($col_fil['cat_name'])&&$col_fil['cat_name']}checked{/if}/>
                                                                        {ci_language line="Default Category"}
                                                                </label>
                                                        </li> 
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_status'])&&$col_fil['p_status']}checked{/if}">
                                                                        <input type="checkbox" name="p_status" {if isset($col_fil['p_status'])&&$col_fil['p_status']}checked{/if}/>
                                                                        {ci_language line="Status"}
                                                                </label>
                                                        </li> 
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_condition'])&&$col_fil['p_condition']}checked{/if}">
                                                                        <input type="checkbox" name="p_condition" {if isset($col_fil['p_condition'])&&$col_fil['p_condition']}checked{/if}/>
                                                                        {ci_language line="Condition"}
                                                                </label>
                                                        </li> 
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_price'])&&$col_fil['p_price']}checked{/if}">
                                                                        <input type="checkbox" name="p_price" {if isset($col_fil['p_price'])&&$col_fil['p_price']}checked{/if}/>
                                                                        {ci_language line="Price"}
                                                                </label>
                                                        </li> 
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_qty'])&&$col_fil['p_qty']}checked{/if}">
                                                                        <input type="checkbox" name="p_qty" {if isset($col_fil['p_qty'])&&$col_fil['p_qty']}checked{/if}/>
                                                                        {ci_language line="Quantity"}
                                                                </label>
                                                        </li>
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_sale_price'])&&$col_fil['p_sale_price']}checked{/if}">
                                                                        <input type="checkbox" name="p_sale_price" {if isset($col_fil['p_sale_price'])&&$col_fil['p_sale_price']}checked{/if}/>
                                                                        {ci_language line="Sale price"}
                                                                </label>
                                                        </li>
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_sale_from'])&&$col_fil['p_sale_from']}checked{/if}">
                                                                        <input type="checkbox" name="p_sale_from"  {if isset($col_fil['p_sale_from'])&&$col_fil['p_sale_from']}checked{/if}/>
                                                                        {ci_language line="Sale from"}
                                                                </label>
                                                        </li>
                                                        <li>
                                                                <label class="cb-checkbox {if isset($col_fil['p_sale_to'])&&$col_fil['p_sale_to']}checked{/if}">
                                                                        <input type="checkbox" name="p_sale_to" {if isset($col_fil['p_sale_to'])&&$col_fil['p_sale_to']}checked{/if} />
                                                                        {ci_language line="Sale to"}
                                                                </label>
                                                        </li>
                                                        
                                                </ul>
                                        </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 statistics">
                    <table class="responsive-table custom-form" id="my_prod_list">
                        <thead class="table-head">
                            <tr>
                                {*<th class="tableCheckbo" data-th="#: " data-name=""><label class="cb-checkbox"><input id="sel_all" type="checkbox"></label></th>*}
                                <th class="p_id">{ci_language line="Product ID"}</th>
                                <th  class="p_img">{ci_language line="Image"}</th>
                                <th >{ci_language line="Product name > Product attribute"}</th>
                                <th class="cat_name">{ci_language line="Default Category"}</th>
                                
                                <th><table><tr>
                                        
                                    <th class="p_ref">{ci_language line="Reference/SKU"}</th>
                                    {*<th>{ci_language line="SKU"}</th>*}
                                    <th>{ci_language line="EAN"}</th>
                                    <th>{ci_language line="UPC"}</th>
                                </tr></table></th>
                                <th class="p_status">{ci_language line="Status"}</th>
                                <th class="p_condition">{ci_language line="Condition"}</th>
                                <th class="p_price">{ci_language line="Price"}</th>
                                <th class="p_qty">{ci_language line="Quantity"}</th>
                                <th class="p_sale_price">{ci_language line="Sale price"}</th>
                                <th class="p_sale_from">{ci_language line="Sale from"}</th>
                                <th class="p_sale_to">{ci_language line="Sale to"}</th>
                                
                        </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>			
    </div>
</div>
{include file="IncludeDataTableLanguage.tpl"}
<input type="hidden" id="processing" value="Processing...">
<input type="hidden" id="status_active" value="{ci_language line="Active"}">
<input type="hidden" id="status_inactive" value="{ci_language line="Inactive"}">
{*<script src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>*}
<script language="javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_shop/my_products.js"></script>
{include file="footer.tpl"}