{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="{if !isset($popup)}main {/if} p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="connect"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                {include file="popup_step.tpl"}
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix m-b10">
					<h4 class="headSettings_head">{ci_language line="Module"}/{ci_language line="Extension"}</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="engine clearfix">
					<div class="engine_item">
						<img class="engine_item-picture" src="{$cdn_url}assets/images/engine/prestashop.png" alt="">
						<p class="engine_item-text">{ci_language line="Prestashop"}</p>
						<a class="btn btn-save" href="/shop_modules/prestashop.zip">{ci_language line="Download"}</a>
						<p class="light-gray">{ci_language line="New version"} v{if isset($presta_version)}{$presta_version}{else}1.2.30{/if}</p>
					</div>
                                        <div class="engine_item">
						<img class="engine_item-picture" src="{$cdn_url}assets/images/engine/woo.png" alt="">
						<p class="engine_item-text">{ci_language line="Woo Commerce"}</p>
						<a class="btn btn-save" href="/shop_modules/woocommerce.zip">{ci_language line="Download"}</a>
					</div>
					<div class="engine_item" id="shopify">
						<img class="engine_item-picture" src="{$cdn_url}assets/images/engine/shopify.png" alt="">
						<p class="engine_item-text">{ci_language line="Shopify"}</p>
						<a class="btn btn-save disabled" {* target="_blank" href="/shopify/?load=install"*}>{ci_language line="Install"}</a>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="{$cdn_url}assets/images/engine/opencart.png" alt="">
						<p class="engine_item-text">{ci_language line="Opencart"}</p>
						<a class="btn btn-save disabled" {*href="/shop_modules/opencart.rar"*}>{ci_language line="Download"}</a>
					</div>
					<div class="engine_item">
						<img class="engine_item-picture" src="{$cdn_url}assets/images/engine/magento.png" alt="">
						<p class="engine_item-text">{ci_language line="Magento"}</p>
						<a class="btn btn-save disabled" {*href="/shop_modules/magento.tgz"*}>{ci_language line="Download"}</a>
					</div>
					
				</div>
			</div>
		</div>
                 
	</div>
 <script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script language="javascript" src="{$cdn_url}assets/js/FeedBiz/my_shop/connect.js"></script>
{include file="footer.tpl"}