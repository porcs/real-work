{include file="header.tpl"}
{ci_config name="base_url"}

{include file="sidebar.tpl"} 

<div class="main-content">
    
    {*include file="breadcrumbs.tpl"*} <!--breadcrumb-->
    
    <div class="page-content">
            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="Error Message"}:
            </h1>
        </div>
        
        <div class="row-fluid">
            {ci_language line="We found an error, on"} 
            "{$page}",  
            {ci_language line="this error has send to admin"} .
        </div>
    </div>
</div>{*main-content*}  

{include file="footer.tpl"}
 