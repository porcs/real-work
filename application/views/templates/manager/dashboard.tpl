{include file="header.tpl"}
{include file="manager_sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Manager dashboard"}</h1> 
                
		<div class="row">
			<div class="col-xs-12">
                            
                            
                             
                            
				<div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Your customers"}</h4>						
					</div>
					{*<div class="pull-sm-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="user_id" checked/>
											{ci_language line="User ID"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="name" checked/>
											{ci_language line="First name"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="last_name" checked/>
											{ci_language line="Last name"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Email"}
										</label>
									</li> 
								</ul>
							</div>
						</div>	
					</div>		*}	
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th rel="user_id" >{ci_language line="User ID"}</th>
									<th rel="name" >{ci_language line="First name"}</th>
									<th rel="last_name" >{ci_language line="Last name"}</th>
									<th rel="email" >{ci_language line="Email"}</th> 
									<th rel="control">{ci_language line="Login"}</th>
								</tr>
                                                        </thead>   
                                                        <tbody>
                                                                {if !empty($user_list)}
                                                                    {foreach $user_list as $user}
                                                                       <tr>
                                                                            <td rel="user_id" >{$user['id']}</td>
                                                                            <td rel="name" >{$user['user_first_name']}</td>
                                                                            <td rel="last_name" >{$user['user_las_name']}</td>
                                                                            <td rel="email" >{$user['user_email']}</td>
                                                                            <td rel="control" >
                                                                                <a href="{$base_url}manager/remote/{$user['id']}" target="_blank" class="send-orders btn btn-mini"><i class=" fa fa-gears"></i>&nbsp;<span>Remote</span></a>
                                                                            </td>
                                                                            
                                                                       </tr>
                                                                    {/foreach}
                                                                {/if}
                                                        </tbody>                              
                                                </table>
					</div>
				</div>
                                <br><br>
                                <div class="headSettings clearfix">
					<div class="pull-sm-left clearfix">
						<h4 class="headSettings_head">{ci_language line="Your token"}</h4>
                                               
					</div>
                                        <div class="col-xs-3  ">
                                               <input id='token' class="form-control" value='{$token_assist}' source='{$token_assist}'> 
                                        </div>
                                </div>
			</div>			
		</div>
	</div>
 
{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/FeedBiz/manager/dashboard.js"></script>