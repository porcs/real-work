{if isset($message_document) && !empty($message_document)}
    {ci_language file="message_code" lang="$lang"}
    {ci_config name="base_url"}    
    <div class="row">
        <div class="col-xs-12">
            <div class="validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="{if isset($message_document.message_solution)}{if $message_document.message_solution|lower=="video"}help-video{else}help{/if}{/if}"></i>
                    </div>
                    <div class="validateCell">
                        <p class="pull-left">
                            {if isset($message_document.message_text)}<span class="bold">{$message_document.message_text}</span>{/if}                         
                            {if isset($message_document.message_link)}
                                <a href="{$message_document.message_link}" class="link" target="_blank" title="{ci_language line="learn more"}">
                                    {$message_document.message_link}
                                </a>
                            {/if} 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- message document -->
{/if} 