{ci_language file="sidebar" lang="$lang"}
{ci_config name="base_url"}
{if !isset($popup) || !$popup}
    <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
    </a>
    <div id="menu" class="hidden-print">

        <div class="sidebar active">

            <ul class="sidebar_content">
                <li class="sidebar-item{if (isset($url[0]) && $url[0] == 'dashboard' || empty($url[0]))} active{/if}">
                    <a class="sidebar-item_link link" href="{$base_url}dashboard">
                        <i class="icon-clock"></i>
                        <span class="menu-text"> {ci_language line="dashboard"} </span>
                    </a>
                </li><!--dashboard-->

                <li class="sidebar-item{if (isset($url[0]) && $url[0] == 'users')} active{/if}" >
                    <a class="sidebar-item_link link" href="#" data-target="#my_account" data-toggle="collapse">
                        <i class="icon-men"></i>
                        <span class="menu-text"> {ci_language line="my_account"} </span>
                    </a>
                    <ul class="sidebar_down{if (isset($url[0]) && $url[0] == 'users')} active" style="display: block;{/if}" id="my_account">
                        <li class="sidebar-item_Once-down {if (isset($url[1]) && $url[1] == 'profile')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" href="{$base_url}users/profile">
                                {ci_language line="edit_profile"}
                            </a>
                        </li>
                        <li  class="sidebar-item_Once-down {if (isset($url[1]) && $url[1] == 'billing_information')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" href="{$base_url}users/billing_information">
                                {ci_language line="edit_billing"}
                            </a>
                        </li>
                        
                        <li class="sidebar-item_Once-down {if (isset($url[1]) && $url[1] == 'security')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" href="{$base_url}users/security">
                                {ci_language line="Security"}
                            </a>
                        </li>

                        <li class="sidebar-item_Once-down slideItem{if (isset($url[1]) && $url[1] == 'affiliation')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" data-target="#user_affiliation" data-toggle="collapse">
                                {ci_language line="affiliation"}
                            </a>

                            <ul class="{if (isset($url[1]) && $url[1] == 'affiliation')}active" style="display: block;{/if}" id="user_affiliation">

                                <li class="{if (isset($url[2]) && $url[2] == 'affiliateLists')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}users/affiliation/affiliateLists">
                                        {ci_language line="afflist"}
                                    </a>
                                </li>

                                <li class="{if (isset($url[2]) && $url[2] == 'statistic')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}users/affiliation/statistic">
                                        {ci_language line="affstat"}
                                    </a>
                                </li>

                                <li class="{if (isset($url[2]) && $url[2] == 'invite_friend')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}users/affiliation/invite_friend">
                                        {ci_language line="affinvite"}
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li><!--my_account-->

                <li class="sidebar-item{if isset($url[0]) && ($url[0] == 'billing' || $url[0] == 'service')} active{/if}">
                    <a class="sidebar-item_link link" href="#" data-target="#billing" data-toggle="collapse">
                        <i class="icon-money"></i>
                        <span class="menu-text"> {ci_language line="billing"} </span>
                        {if isset($expired_txt) && $expired_txt != '' }
                            <span id="sidebar-tooltip" title="{$expired_txt}{ci_language line="packages expired!"}">
                                <i class="icon-warning"></i>
                            </span>
                        {/if}
                    </a>

                    <ul class="{if isset($url[0]) && ($url[0] == 'billing' || $url[0] == 'service')}active" style="display: block;{/if}" id="billing">
                        {if (isset($pay_conf_show))}
                            <li class="sidebar-item_Once-down{if (isset($url[1]) && ($url[1] == 'configuration' || $url[1] == 'preapproval'))} active{/if}">
                                <a class="sidebar-item_Once-down-link" href="{$base_url}billing/configuration">
                                    {ci_language line="payment_configuration"}
                                </a>
                            </li>
                        {/if}
                        <li class="sidebar-item_Once-down{if (isset($url[1]) && ($url[1] == 'region' || $url[1] == 'packages' 
|| $url[1] == 'information' || $url[1] == 'complete' || ($url[1] == 'payment' && !isset($expired_txt)) ))} active{/if}">
                            <a class="sidebar-item_Once-down-link" href="{$base_url}service/packages">
                                {ci_language line="packages"}
                            </a>
                        </li>
                        <li class="sidebar-item_Once-down{if (isset($url[1]) && ($url[1] == 'download'))} active{/if}">
                            <a class="sidebar-item_Once-down-link" href="{$base_url}billing/download">
                                {ci_language line="download_bill"}
                            </a>
                        </li>
                        {if isset($expired_txt) && $expired_txt != '' }
                            <li class="sidebar-item_Once-down{if (isset($url[1]) && ($url[1] == 'payment'))} active{/if}">
                                <a class="sidebar-item_Once-down-link" href="{$base_url}service/paybillall">
                                    {ci_language line="pay_expire_bill"}
                                </a>
                            </li>
                        {/if}
                    </ul>
                </li><!--billing-->
                <li  class="sidebar-item{if (isset($url[0] ) && $url[0] == 'my_shop')} active{/if}" >
                    <a class="sidebar-item_link link" href="#" data-target="#my_shop" data-toggle="collapse">
                        <i class="icon-cart"></i>
                        <span class="menu-text"> {ci_language line="my_shop"} </span>
                    </a>
                    <ul class="{if isset($url[0]) && $url[0] == 'my_shop'}active" style="display: block;{/if}" id="my_shop">
                        <li class="sidebar-item_Once-down{if (isset($url[0]) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'connect')} active{/if}" >
                            <a class="sidebar-item_Once-down-link" href="{$base_url}my_shop/connect">
                                {ci_language line="connect"}                               
                            </a>
                        </li>
                        <li class="sidebar-item_Once-down{if (isset($url[0]) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'configuration')} active{/if}" >
                            <a class="sidebar-item_Once-down-link" href="{$base_url}my_shop/configuration">
                                {ci_language line="configuration"}                               
                            </a>
                        </li>
                        <li class="sidebar-item_Once-down{if (isset($url[0]) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'my_products')} active{/if}" >
                            <a class="sidebar-item_Once-down-link" href="{$base_url}my_shop/my_products">
                                {ci_language line="my_products"}                               
                            </a>
                        </li>
                        <li class="sidebar-item_Once-down{if (isset($url[0]) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'my_orders')} active{/if}" >
                            <a class="sidebar-item_Once-down-link" href="{$base_url}my_shop/multichannel_orders">
                                {ci_language line="Multichannel Orders"}                               
                            </a>
                        </li>
                        <li class="sidebar-item_Once-down{if (isset($url[0]) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'offers_options')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" data-target="#my_shop_offers_options" data-toggle="collapse" >
                                {ci_language line="Offers Options"}                               
                            </a>
                            <ul class="slideItem{if (isset($url[0] ) && $url[0] == 'my_shop') && (isset($url[1]) && $url[1] == 'offers_options')} active" style="display: block;{/if}" id="my_shop_offers_options">
                                <li class="{if (isset($url[2]) && $url[2] == 'product')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_shop/offers_options/product">
                                        {ci_language line="by Product"}                               
                                    </a>
                                </li>
                                <li class="{if (isset($url[2]) && $url[2] == 'category')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_shop/offers_options/category">
                                        {ci_language line="by Category"}                               
                                    </a>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </li>
                <li  class="sidebar-item{if (isset($url[0] ) && $url[0] == 'my_feeds')} active{/if}" >
                    <a class="sidebar-item_link link" href="#" data-target="#my_feed" data-toggle="collapse">
                        <i class="icon-feed"></i>
                        <span class="menu-text"> {ci_language line="my_feed"} </span>
                    </a>

                    <ul class="{if isset($url[0]) && $url[0] == 'my_feeds'}active" style="display: block;{/if}" id="my_feed">
                        <li class="sidebar-item_Once-down{if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && $url[1] == 'configuration')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" data-target="#configuration" data-toggle="collapse">
                                {ci_language line="configuration"}
                            </a>

                            <ul class="slideItem{if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && $url[1] == 'configuration')}active" style="display: block;{/if}" id="configuration">
                                <li class="{if (isset($url[2]) && $url[2] == 'general')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/configuration/general">
                                        {ci_language line="general"}                               
                                    </a>
                                </li>
                            </ul>
                        </li><!--main_configuration-->

                        <li class="sidebar-item_Once-down{if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && $url[1] == 'parameters')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" href="#" data-target="#parameter_sidebar" data-toggle="collapse">
                                {ci_language line="parameters"}
                            </a>

                            <ul id="parameter_sidebar" class="{if isset($id_shop)} imported{/if} {if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && $url[1] == 'parameters')} active" style="display: block;{/if}"  >

                                {if isset($mode_default) && $mode_default == 1}
                                    <li class="{if (isset($url[3]) && $url[3] == 'filters')} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/filters">
                                            {ci_language line="filters"}                               
                                        </a>
                                    </li><!--filters-->
                                {elseif isset($mode_default) && ($mode_default == 2 || $mode_default == 3)}
                                    <li class="{if (isset($url[2]) && $url[2] == 'carriers')} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/carriers">
                                            {ci_language line="carriers"}                               
                                        </a>
                                    </li><!--carriers-->
                                    <li  class="{if (isset($url[2]) && $url[2] == 'filters')} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/filters">
                                            {ci_language line="filters"}                               
                                        </a>
                                    </li><!--filters-->
                                    <li  class="slideItem{if (isset($url[2]) && $url[2] == 'mapping')} active{/if}" >
                                        <a class="sidebar-item_Third-down-link link" href="#" data-target="#support" data-toggle="collapse">
                                            {ci_language line="mapping"}
                                        </a>
                                        <ul class="{if (isset($url[2]) && $url[2] == 'mapping')}active" style="display: block;{/if}" id="support">
                                            <li class=sidebar-item_Fouth-down"{if (isset($url[3]) && $url[3] == 'manufacturers')} active{/if}" >
                                                <a class="sidebar-item_Fouth-down-link link" href="{$base_url}my_feeds/parameters/mapping/manufacturers">
                                                    {ci_language line="manufacturers"}
                                                </a>
                                            </li>
                                        </ul>
                                    </li><!--mapping-->
                                {/if} 

                                {if isset($mode_default) && $mode_default == 1}   
                                    <li class="{if (isset($url[2]) && $url[2] == 'category') && (!isset($url[3]))} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/category">
                                            {ci_language line="Category"}                               
                                        </a>
                                    </li><!--category-->
                                {elseif isset($mode_default) && ($mode_default == 2 || $mode_default == 3)}
                                    <li class="{if (isset($url[2]) && $url[2] == 'rules') && (!isset($url[3]))} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/rules">
                                            {ci_language line="rules"}                               
                                        </a>
                                    </li><!--profile-->
                                {/if} 

                                {if isset($mode_default) && ($mode_default == 2 || $mode_default == 3)}
                                    <li class="{if (isset($url[2]) && $url[2] == 'profiles') && (!isset($url[3]))} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/profiles">
                                            {ci_language line="Profiles"}                               
                                        </a>
                                    </li><!--configuration-->
                                    <li class="{if (isset($url[2]) && $url[2] == 'category') && (!isset($url[3]))} active{/if}" >
                                        <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/parameters/category">
                                            {ci_language line="Category"}                               
                                        </a>
                                    </li><!--category-->
                                {/if} 

                                <li id="parameter_before_import">
                                    <a class="sidebar-item_Twice-down-link link" href="#" class="red" >    
                                        <div>
                                            {ci_language line="You must feed data source before use this menu."}
                                        </div>
                                    </a>
                                </li>

                            </ul>
                        </li><!--parameters-->


                        <li class="sidebar-item_Once-down{if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && ($url[1] == 'batches' || $url[1] == 'messages'|| $url[1] == 'import_history'))} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" data-target="#log" data-toggle="collapse">
                                {ci_language line="log"}  
                            </a>

                            <ul class="{if (isset($url[0] ) && $url[0] == 'my_feeds') && (isset($url[1]) && ($url[1] == 'batches' || $url[1] == 'messages'|| $url[1] == 'import_history'))}active" style="display: block;{/if}" id="log">
                                <li class="{if (isset($url[1]) && $url[1] == 'batches')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/batches">
                                        {ci_language line="Batches"}                           
                                    </a>
                                </li>
                                <li class="{if (isset($url[1]) && $url[1] == 'messages')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/messages">
                                        {ci_language line="Messages"}                               
                                    </a>
                                </li>
                                <li   class="{if (isset($url[1]) && $url[1] == 'import_history')} active{/if}" >
                                    <a class="sidebar-item_Twice-down-link link" href="{$base_url}my_feeds/import_history">
                                        {ci_language line="Import history"}                               
                                    </a>
                                </li>

                            </ul>
                        </li><!--my feed log-->

                    </ul>

                </li><!--my_feed-->

                <li class="sidebar-item{if isset($url[0]) && ($url[0] == 'marketplace' || $url[0] == 'ebay' || $url[0] == 'amazon' || $url[0] == 'general' || $url[0] == 'google_shopping')} active{/if}" >
                    <a class="sidebar-item_link link" href="#" data-target="#marketplace" data-toggle="collapse">
                        <i class="icon-globe"></i>
                        <span class="menu-text"> {ci_language line="marketplace"} </span>
                    </a>

                    <ul class="{if (isset($url[0]) && ($url[0] == 'marketplace' || $url[0] == 'ebay' || $url[0] == 'amazon' || $url[0] == 'general' || $url[0] == 'google_shopping') && $url[0] != 'my_feeds')}active" style="display: block;{/if}" id="marketplace_menu">

                        <li class="sidebar-item_Once-down{if (isset($url[0]) && ($url[0] == 'ebay' || $url[0] == 'amazon' || $url[0] == 'marketplace') && $url[0] != 'my_feeds') && (isset($url[1]) && $url[1] == 'configuration')} active{/if}" >
                            <a class="sidebar-item_Once-down-link link" href="{$base_url}marketplace/configuration">
                                {ci_language line="Select Marketplace"}
                            </a>
                        </li>                        
                        {if isset($menu['eBay'])}
                            <li class="sidebar-item_Once-down slideItem{if (isset($url[0]) && $url[0] == 'ebay')} active{/if}" >
                                <a class="sidebar-item_Once-down-link link" href="#" data-target="#ebay" data-toggle="collapse">
                                    <span class="menu-text"> {ci_language line="ebay"} </span>
                                </a>

                                {if isset($menu['eBay']['submenu'] )}
                                    <ul class="{if (isset($url[0]) && $url[0] == 'ebay')}active" style="display: block;{/if}" id="ebay">
                                        {foreach $menu['eBay']['submenu'] as $ebay_key => $ebay_menu}
                                            <li class="slideItem{if isset($url[0]) && $url[0] == 'ebay' && (($url[1] =='mapping' && $url[3] == $ebay_menu.id) || ($url[1] !='mapping' && $url[2] == $ebay_menu.id)) && ($url[1] == 'auth_security' || $url[1] == 'authentication' || $url[1] == 'auth_setting' || $url[1] == 'synchronization' || $url[1] == 'synchronization_detail' || $url[1] == 'synchronization_matching' || $url[1] == 'synchronization_finish' || $url[1] == 'tax' || $url[1] == 'log' || $url[1] == 'statistics_products' || $url[1] =='mapping' || $url[1] == 'actions' || $url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders' || $url[1] == 'price_modifier' || $url[1] == 'advanced' || $url[1] == 'offers_options' || $url[1] == 'orders' || $url[1] == 'orders_cancelled' || $url[1] == 'orders_shipped' || $url[1] == 'orders_awaiting_payment') && ((isset($url[3]) && $url[3] == $ebay_menu.id) || (isset($url[2]) && $url[2] == $ebay_menu.id))} active{/if}" >
                                                <a class="sidebar-item_Twice-down-link link" href="#" data-target="#ebay_menu{$ebay_key}" data-toggle="collapse">
                                                    <span class="menu-text">{ucwords(strtolower($ebay_menu.domain))}</span>
                                                </a>

                                                {if isset($id_shop)}
                                                    <ul class="{if isset($url[0]) && $url[0] == 'ebay' && (($url[1] == 'offers_options' && $url[3] == $ebay_menu.id ) || ($url[1] == 'mapping' && $url[3] == $ebay_menu.id) || ($url[2] == $ebay_menu.id && $url[1] == 'auth_security' || $url[1] == 'authentication' || $url[1] == 'integration_authorization' || $url[1] == 'integration_finish' || $url[1] == 'auth_setting' || $url[1] == 'synchronization' || $url[1] == 'synchronization_detail' || $url[1] == 'synchronization_matching' || $url[1] == 'synchronization_finish' || $url[1] == 'payment_method' || $url[1] == 'return_method' || $url[1] == 'tax' || $url[1] == 'templates' || $url[1] == 'log' || $url[1] == 'statistics_products' || $url[1] == 'variation' || $url[1] == 'variation_values' || $url[1] == 'price_modifier' || $url[1] == 'advanced' || $url[1] =='mapping' || $url[1] =='actions' || $url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders' || $url[1] == 'orders' || $url[1] == 'orders_cancelled' || $url[1] == 'orders_shipped' || $url[1] == 'orders_awaiting_payment'))}active" style="display: block;{/if}" id="ebay_menu{$ebay_key}">
                                                        <li class="slideItem{if isset($url[0]) && $url[0] == 'ebay' && (($url[1] =='mapping' && $url[3] == $ebay_menu.id) || ($url[1] !='mapping' && $url[2] == $ebay_menu.id)) && ($url[1] == 'auth_security' || $url[1] == 'payment_method' || $url[1] == 'return_method' || $url[1] == 'authentication' || $url[1] == 'integration_authorization' || $url[1] == 'integration_finish' || $url[1] == 'auth_setting' || $url[1] == 'synchronization' || $url[1] == 'synchronization_detail' || $url[1] == 'synchronization_matching' || $url[1] == 'synchronization_finish' || $url[1] == 'tax'  || $url[1] == 'statistics_products' || $url[1] == 'variation' || $url[1] == 'variation_values' || $url[1] =='mapping')} active{/if}" >
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#configurations{$ebay_key}" data-toggle="collapse">
                                                                <span class="menu-text"> {ci_language line="Configuration"} </span>
                                                            </a>
                                                            <ul class="{if isset($url[0]) && $url[0] == 'ebay' && (($url[1] =='mapping' && $url[3] == $ebay_menu.id) || ($url[1] !='mapping' && $url[2] == $ebay_menu.id)) && ($url[1] == 'auth_security' || $url[1] == 'payment_method' || $url[1] == 'return_method' || $url[1] == 'authentication'  || $url[1] == 'integration_authorization' || $url[1] == 'integration_finish' || $url[1] == 'auth_setting' || $url[1] == 'synchronization' || $url[1] == 'synchronization_detail' || $url[1] == 'synchronization_matching' || $url[1] == 'synchronization_finish' || $url[1] == 'tax' || $url[1] == 'templates'   || $url[1] == 'statistics_products' || $url[1] == 'price_modifier' || $url[1] == 'variation' || $url[1] == 'variation_values' || $url[1] == 'advanced' || $url[1] =='mapping')}active" style="display: block;{/if}" id="configurations{$ebay_key}">
                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'auth_security' || $url[1] == 'authentication' || $url[1] == 'integration_authorization' || $url[1] == 'integration_finish') && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/authentication/{$ebay_menu.id}">
                                                                        {ci_language line="nb_auth_security"}
                                                                    </a>
                                                                </li>

                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'auth_setting') && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/auth_setting/{$ebay_menu.id}">
                                                                        {ci_language line="nb_auth_setting"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[1] == 'payment_method') && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/payment_method/{$ebay_menu.id}">
                                                                        {ci_language line="nb_payment_method"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[1] == 'return_method') && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/return_method/{$ebay_menu.id}">
                                                                        {ci_language line="nb_return_method"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[1] == 'templates') && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/templates/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_templates"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'price_modifier') && $url[2] == $ebay_menu.id)}active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/price_modifier/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_price_modifier"}
                                                                    </a>
                                                                </li>
                                                                <li class="sidebar-item_Fouth-down slideItem{if isset($url[0]) && $url[0] == 'ebay' && $url[1] == 'mapping' && $url[3] == $ebay_menu.id} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="#" data-target="#nb_ebay_mapping{$ebay_key}" data-toggle="collapse">
                                                                        {ci_language line="nb_ebay_mapping"}
                                                                    </a>
                                                                    <ul class="{if isset($url[0]) && $url[0] == 'ebay' && $url[1] == 'mapping' && $url[3] == $ebay_menu.id}active" style="display: block;{/if}" id="nb_ebay_mapping{$ebay_key}">
                                                                        <li  class="{if (isset($url[2]) && ($url[2] == 'mapping_carriers') && $url[3] == $ebay_menu.id)} active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/mapping_carriers/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_mapping_carriers"}
                                                                            </a>
                                                                        </li>
                                                                        <li  class="{if (isset($url[2]) && ($url[2] == 'mapping_carriers_cost') && $url[3] == $ebay_menu.id)} active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/mapping_carriers_cost/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_mapping_carriers_cost"}
                                                                            </a>
                                                                        </li>
                                                                        <li  class="{if (isset($url[2]) && $url[1] == 'mapping' && ($url[2] == 'mapping_templates') && $url[3] == $ebay_menu.id)} active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/mapping_templates/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_mapping_templates"}
                                                                            </a>
                                                                        </li>
                                                                        <li  class="{if (isset($url[2]) && ($url[2] == 'univers') && $url[3] == $ebay_menu.id)}active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/univers/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_univers"}
                                                                            </a>
                                                                        </li>
                                                                        <li class="{if (isset($url[2]) && ($url[0] == 'ebay' && $url[2] == 'mapping_categories') && $url[3] == $ebay_menu.id)} active{/if}">
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/mapping_categories/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_mapping_categories"}
                                                                            </a>
                                                                        </li>
                                                                        {if isset($mode_default) && ($mode_default == 2 || $mode_default == 3)}
                                                                            <li {if (isset($url[2]) && $url[0] == 'ebay') && (isset($url[2]) && $url[2] == 'mapping_conditions')} class="active" {/if}>
                                                                                <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/mapping_conditions/{$ebay_menu.id}">
                                                                                    {ci_language line="nb_ebay_mapping_condition"}                               
                                                                                </a>
                                                                            </li><!--condition-->
                                                                        {/if}
                                                                        <li class="{if (isset($url[2]) && ($url[0] == 'ebay' && $url[2] == 'variation') && $url[3] == $ebay_menu.id)} active{/if}">
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/variation/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_variation"}
                                                                            </a>
                                                                        </li>
                                                                        <li class="{if (isset($url[2]) && ($url[0] == 'ebay' && $url[2] == 'variation_values') && $url[3] == $ebay_menu.id)} active{/if}">
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}ebay/mapping/variation_values/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_variation_values"}
                                                                            </a>
                                                                        </li>   
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="{if (isset($url[0]) && $url[0] == 'ebay') && (isset($url[1]) && $url[1] == 'offers_options') 
                                                        && (isset($url[3]) && $url[3] == $ebay_menu.id)} active{/if}" >                                                            
                                                            <a class="sidebar-item_Third-down-link link" data-target="#offers_options{$ebay_key}" data-toggle="collapse" >
                                                                {ci_language line="Offers Options"}                               
                                                            </a>
                                                            <ul {if (isset($url[0] ) && $url[0] == 'ebay') && (isset($url[1]) && $url[1] == 'offers_options')  && (isset($url[3]) && $url[3] == $ebay_menu.id)}class="active" style="display: block;"{/if} id="ebay_offers_options">
                                                                <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'product')} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/offers_options/product/{$ebay_menu.id}/{$menu['eBay']['id']}">
                                                                        {ci_language line="by Product"}                               
                                                                    </a>
                                                                </li>
                                                                <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'category')} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/offers_options/category/{$ebay_menu.id}/{$menu['eBay']['id']}">
                                                                        {ci_language line="by Category"}                               
                                                                    </a>
                                                                </li>
                                                            </ul>

                                                        </li> <!--Product Option-->
                                                        <li class="slideItem {if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'task' || $url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders') && $url[2] == $ebay_menu.id} active" style="display: block;{/if}">
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#task{$ebay_key}" data-toggle="collapse">
                                                                {ci_language line="Task"}
                                                            </a>
                                                            <ul class="{if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'actions' || $url[1] == 'status' || $url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders') && $url[2] == $ebay_menu.id} active" style="display: block;{/if}">
                                                                <li class="sidebar-item_Fouth-down slideItem active {if (isset($url[0] ) && $url[0] == 'ebay') && (isset($url[1]) && $url[1] == 'actions' || $url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders') && $url[2] == $ebay_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="#" data-target="#action{$ebay_key}" data-toggle="collapse">
                                                                        {ci_language line="Actions"}                               
                                                                    </a>
                                                                    <ul class="{if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'actions_products' || $url[1] == 'actions_offers' || $url[1] == 'actions_orders') && $url[2] == $ebay_menu.id}active" style="display: block;{/if}">
                                                                        <li  class="{if (isset($url[2]) && $url[1] == 'actions_products' && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link" href="{$base_url}ebay/actions_products/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_actions_products"}
                                                                            </a>
                                                                        </li>
                                                                        <li  class="{if (isset($url[2]) && $url[1] == 'actions_offers' && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link" href="{$base_url}ebay/actions_offers/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_actions_offers"}
                                                                            </a>
                                                                        </li>
                                                                        <li  class="{if (isset($url[2]) && $url[1] == 'actions_orders' && $url[2] == $ebay_menu.id)}active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link" href="{$base_url}ebay/actions_orders/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_actions_orders"}
                                                                            </a>
                                                                        </li><!--Action-->
                                                                        <li  class="{if (isset($url[2]) && $url[1] == 'explode_products' && $url[2] == $ebay_menu.id)}active{/if}" >
                                                                            <a class="sidebar-item_Fifth-down-link" href="{$base_url}ebay/explode_products/{$ebay_menu.id}">
                                                                                {ci_language line="nb_ebay_explode_products"}
                                                                            </a>
                                                                        </li><!--Action-->
                                                                    </ul>
                                                                </li><!--Action-->
                                                            </ul>                                                                       
                                                        </li><!--task-->

                                                        <li class="slideItem {if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'orders' || $url[1] == 'orders_cancelled' || $url[1] == 'orders_shipped' || $url[1] == 'orders_awaiting_payment') && $url[2] == $ebay_menu.id} active" style="display: block;{/if}">
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#task{$ebay_key}" data-toggle="collapse">
                                                                {ci_language line="nb_orders"}                               
                                                            </a>
                                                            <ul class="{if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'orders' || $url[1] == 'orders_cancelled' || $url[1] == 'orders_shipped' || $url[1] == 'orders_awaiting_payment') && $url[2] == $ebay_menu.id}active" style="display: block;{/if}">
                                                                <li  class="{if (isset($url[2]) && $url[1] == 'orders' && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link" href="{$base_url}ebay/orders/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_orders_awaiting_shipped"}
                                                                    </a>
                                                                </li>
                                                                <li  class="{if (isset($url[2]) && $url[1] == 'orders_awaiting_payment' && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link" href="{$base_url}ebay/orders_awaiting_payment/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_orders_awaiting_payment"}
                                                                    </a>
                                                                </li>
                                                                <li  class="{if (isset($url[2]) && $url[1] == 'orders_shipped' && $url[2] == $ebay_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link" href="{$base_url}ebay/orders_shipped/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_orders_shipped"}
                                                                    </a>
                                                                </li>
                                                                <li  class="{if (isset($url[2]) && $url[1] == 'orders_cancelled' && $url[2] == $ebay_menu.id)}active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link" href="{$base_url}ebay/orders_cancelled/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_orders_cancelled"}
                                                                    </a>
                                                                </li><!--Action-->
                                                            </ul>       
                                                        </li><!--Status-->

                                                        <li class="slideItem{if (isset($url[0]) && $url[0] == 'ebay') && (isset($url[2]) && ($url[2] == 'report' || (isset($url[1]) && $url[1] == 'log'))) && $url[2] == $ebay_menu.id} active{/if}">
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#report{$ebay_key}" data-toggle="collapse">
                                                                {ci_language line="Report"}
                                                            </a>
                                                            <ul class="{if (isset($url[0]) && $url[0] == 'ebay') && isset($url[1]) && ($url[1] == 'log' || $url[1] == 'statistics_products') && $url[2] == $ebay_menu.id}active" style="display: block;{/if}" id="report{$ebay_key}">
                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'log') && $url[2] == $ebay_menu.id)} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/log/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_log"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'statistics_products') && $url[2] == $ebay_menu.id)} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/statistics_products/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_statistics_product"}
                                                                    </a>
                                                                </li>
                                                                <li class="{if (isset($url[1]) && ($url[0] == 'ebay' && $url[1] == 'error_resolutions') && $url[2] == $ebay_menu.id)} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}ebay/error_resolutions/{$ebay_menu.id}">
                                                                        {ci_language line="nb_ebay_error_resolutions"}
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li><!--Orders-->
                                                    </ul>
                                                {else}
                                                    <ul class="{if isset($url[0]) && $url[0] == 'ebay' && (($url[1] =='mapping' && $url[3] == $ebay_menu.id) || ($url[1] !='mapping' && $url[2] == $ebay_menu.id)) && ($url[1] == 'auth_security' || $url[1] == 'authentication' || $url[1] == 'log' || $url[1] == 'statistics_products' || $url[1] =='mapping' || $url[1] == 'actions' || $url[1] == 'status' || $url[1] == 'orders') && ((isset($url[3]) && $url[3] == $ebay_menu.id) || (isset($url[2]) && $url[2] == $ebay_menu.id))}active" style="display: block;{/if}" id="ebay_menu">
                                                        <li>
                                                            <a class="sidebar-item_Third-down-link link" href="#" class="red" >    
                                                                <div>
                                                                    <i class="fa fa-warning"></i>
                                                                    {ci_language line="You must feed data source before use this menu."}
                                                                </div>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li><!--/.ebay-->
                        {/if}

                        {if isset($menu['Amazon'])}
                            <li class="sidebar-item_Once-down slideItem{if (isset($url[0]) && $url[0] == 'amazon')} active{/if}">
                                <a class="sidebar-item_Once-down-link link" href="#" data-target="#amazon" data-toggle="collapse">
                                    <span class="menu-text"> {ci_language line="amazon"} </span>
                                </a>

                                {if isset($menu['Amazon']['submenu'])}
                                    <ul class="{if (isset($url[0]) && $url[0] == 'amazon')}active" style="display: block;{/if}" id="amazon">
                                        {foreach $menu['Amazon']['submenu'] as $amazon_key => $amazon_menu}
                                            <li class="slideItem{if isset($url[0]) && $url[0] == 'amazon' && ($url[2] == $amazon_menu.id || (isset($url[3]) && $url[3] == $amazon_menu.id) )} active{/if}">
                                                <a  class="sidebar-item_Twice-down-link link" href="#" data-target="#amazon_menu{$amazon_key}" data-toggle="collapse">
                                                    <span class="menu-text"> {$amazon_menu.domain} </span>
                                                </a>
                                                {if isset($id_shop)}
                                                    <ul {if isset($url[0]) && $url[0] == 'amazon' && ($url[2] == $amazon_menu.id || (isset($url[3]) && $url[3] == $amazon_menu.id))}class="active" style="display: block;"{/if} id="amazon_menu{$amazon_key}">
                                                        <li class="slideItem{if isset($url[0]) && $url[0] == 'amazon' && isset($url[1]) && ($url[1] =="features" || $url[1] =="parameters" || $url[1] =="mappings" || $url[1] =="models" || $url[1] =="profiles" || $url[1] =="category") && $url[2] == $amazon_menu.id } active{/if}">
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#amazon_config{$amazon_key}" data-toggle="collapse">
                                                                <span class="menu-text"> {ci_language line="Configuration"} </span>
                                                            </a>

                                                            <ul class="{if isset($url[0]) && $url[0] == 'amazon' && isset($url[1]) && ($url[1] =="features" || $url[1] =="parameters" || $url[1] =="mappings" || $url[1] =="models" || $url[1] =="profiles" || $url[1] =="category" || $url[1] =="carriers" || $url[1] =="conditions") && $url[2] == $amazon_menu.id }active" style="display: block;{/if}" id="amazon_config{$amazon_key}">
                                                                <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'features' && $url[2] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/features/{$amazon_menu.id}">
                                                                        {ci_language line="Features"}
                                                                    </a>
                                                                </li>
                                                                <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'parameters' && $url[2] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/parameters/{$amazon_menu.id}">
                                                                        {ci_language line="Parameters"}
                                                                    </a>
                                                                </li>
                                                                {if isset($amazon_creation_model[$amazon_menu.ext]) && $amazon_creation_model[$amazon_menu.ext] == 1}      
                                                                    <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'models' && $url[2] == $amazon_menu.id} active{/if}">
                                                                        <a class="sidebar-item_Fouth-down-link link"class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/models/{$amazon_menu.id}">
                                                                            {ci_language line="models"}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                                <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'profiles' && $url[2] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/profiles/{$amazon_menu.id}">
                                                                        {ci_language line="profiles"}
                                                                    </a>
                                                                </li>
                                                                <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'category' && $url[2] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/category/{$amazon_menu.id}">
                                                                        {ci_language line="category"}
                                                                    </a>
                                                                </li>
                                                                {if isset($amazon_creation_model[$amazon_menu.ext]) && $amazon_creation_model[$amazon_menu.ext] == 1}
                                                                    <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'mappings' && $url[2] == $amazon_menu.id} active{/if}">
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/mappings/{$amazon_menu.id}">
                                                                            {ci_language line="mappings"}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                                {if isset($amazon_display[$amazon_menu.ext]['import_orders']) && $amazon_display[$amazon_menu.ext]['import_orders'] == 1}
                                                                    <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'carriers' && $url[2] == $amazon_menu.id} active{/if}">
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/carriers/{$amazon_menu.id}">
                                                                            {ci_language line="carriers"}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                                {if isset($amazon_display[$amazon_menu.ext]['second_hand']) && $amazon_display[$amazon_menu.ext]['second_hand'] == 1}
                                                                    <li class="sidebar-item_Fouth-down{if isset($url[0]) && $url[0] == 'amazon' && $url[1] == 'conditions' && $url[2] == $amazon_menu.id} active{/if}">
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/conditions/{$amazon_menu.id}">
                                                                            {ci_language line="conditions"}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                            </ul>
                                                        </li><!--configuration-->

                                                        {if isset($amazon_display[$amazon_menu.ext]['repricing']) && $amazon_display[$amazon_menu.ext]['repricing'] == 1}
                                                            <li class="{if (isset($url[0]) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'repricing') 
                                                        && (isset($url[3]) && $url[3] == $amazon_menu.id)} active{/if}" >
                                                                <a class="sidebar-item_Third-down-link link" data-target="#amazon_repricing" data-toggle="collapse" >
                                                                    {ci_language line="Repricing"}                               
                                                                </a>
                                                                <ul {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'repricing')  && (isset($url[3]) && $url[3] == $amazon_menu.id)}class="active" style="display: block;"{/if} id="amazon_repricing">
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'parameters')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/repricing/parameters/{$amazon_menu.id}">
                                                                            {ci_language line="Parameters"}                               
                                                                        </a>
                                                                    </li>
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'service_settings')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/repricing/service_settings/{$amazon_menu.id}">
                                                                            {ci_language line="Service Settings"}                               
                                                                        </a>
                                                                    </li>
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'strategies')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/repricing/strategies/{$amazon_menu.id}">
                                                                            {ci_language line="Strategies"}                               
                                                                        </a>
                                                                    </li>
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'product_strategies')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/repricing/product_strategies/{$amazon_menu.id}/products">
                                                                            {ci_language line="Product Strategies"}                               
                                                                        </a>
                                                                    </li>                                                                   
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'logs')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/repricing/logs/{$amazon_menu.id}">
                                                                            {ci_language line="Logs"}                               
                                                                        </a>
                                                                    </li>
                                                                </ul>

                                                            </li> <!--Repricing-->
                                                        {/if}

                                                        {if isset($amazon_display[$amazon_menu.ext]['fba']) && $amazon_display[$amazon_menu.ext]['fba'] == 1}
                                                            <li class="{if (isset($url[0]) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'fba') 
                                                        && (isset($url[3]) && $url[3] == $amazon_menu.id)} active{/if}" >
                                                                <a class="sidebar-item_Third-down-link link"  data-target="#amazon_fba" data-toggle="collapse">
                                                                    {ci_language line="FBA"}                               
                                                                </a>
                                                                <ul {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'fba')  && (isset($url[3]) && $url[3] == $amazon_menu.id)}class="active" style="display: block;"{/if} id="amazon_fba">
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'parameters')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/fba/parameters/{$amazon_menu.id}">
                                                                            {ci_language line="Parameters"}                               
                                                                        </a>
                                                                    </li>
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'carriers')} active{/if}" >
                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/fba/carriers/{$amazon_menu.id}">
                                                                            {ci_language line="Carriers"}                               
                                                                        </a>
                                                                    </li>
                                                                    {*<li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'parameters')} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/fba/parameters/{$amazon_menu.id}">
                                                                    {ci_language line="Multichannel"}                               
                                                                    </a>
                                                                    </li>
                                                                    <li class="{if (isset($url[0]) && $url[0] == 'amazon') && (isset($url[2]) && $url[2] == 'orders') 
                                                                    && (isset($url[3]) && $url[3] == $amazon_menu.id)} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link"  data-target="#amazon_fba_orders" data-toggle="collapse">
                                                                    {ci_language line="Orders"}                               
                                                                    </a>
                                                                    <ul {if (isset($url[1]) && $url[1] == 'fba') && (isset($url[2]) && $url[2] == 'orders') && (isset($url[4]) && $url[4] == $amazon_menu.id)}class="active" style="display: block;"{/if} id="amazon_fba_orders">
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[3]) && $url[3] == 'fba_orders')} active{/if}" >
                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/fba/orders/fba_orders/{$amazon_menu.id}">
                                                                    {ci_language line="FBA Orders"}                               
                                                                    </a>
                                                                    </li>
                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[3]) && $url[3] == 'marketplace_orders')} active{/if}" >
                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/fba/orders/marketplace_orders/{$amazon_menu.id}">
                                                                    {ci_language line="Marketplace Orders"}                               
                                                                    </a>
                                                                    </li>
                                                                    </ul>
                                                                    </li>*}
                                                                </ul>
                                                            </li><!--Amazon FBA-->
                                                        {/if}

                                                        <li class="{if (isset($url[0]) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'offers_options') 
                                                        && (isset($url[3]) && $url[3] == $amazon_menu.id)} active{/if}" >                                                            
                                                            <a class="sidebar-item_Third-down-link link" data-target="#amazon_offers_options" data-toggle="collapse" >
                                                                {ci_language line="Offers Options"}                               
                                                            </a>
                                                            <ul {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'offers_options')  && (isset($url[3]) && $url[3] == $amazon_menu.id)}class="active" style="display: block;"{/if} id="amazon_offers_options">
                                                                <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'product')} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/offers_options/product/{$amazon_menu.id}/{$menu['Amazon']['id']}">
                                                                        {ci_language line="by Product"}                               
                                                                    </a>
                                                                </li>
                                                                <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'category')} active{/if}" >
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/offers_options/category/{$amazon_menu.id}/{$menu['Amazon']['id']}">
                                                                        {ci_language line="by Category"}                               
                                                                    </a>
                                                                </li>
                                                            </ul>

                                                        </li> <!--Product Option-->

                                                        <li class="slideItem{if (isset($url[0] ) && $url[0] == 'amazon') && ( (isset($url[1]) && $url[1] == 'scheduled_tasks') || (isset($url[1]) && $url[1] == 'actions')) && ($url[2] == $amazon_menu.id || (isset($url[3]) && $url[3] == $amazon_menu.id))} active{/if}">
                                                            <a class="sidebar-item_Third-down-link link" href="#" data-target="#amazon_task{$amazon_key}" data-toggle="collapse">
                                                                {ci_language line="Task"}
                                                            </a>
                                                            <ul {if (isset($url[0] ) && $url[0] == 'amazon') && ((isset($url[1]) && $url[1] == 'scheduled_tasks') || (isset($url[1]) && $url[1] == 'actions')) && ($url[2] == $amazon_menu.id || (isset($url[3]) && $url[3] == $amazon_menu.id)) }class="active" style="display: block;"{/if} id="amazon_task{$amazon_key}">

                                                                <li class="{if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'scheduled') && $url[2] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="{$base_url}amazon/scheduled_tasks/{$amazon_menu.id}">
                                                                        {ci_language line="Scheduled Tasks"}                               
                                                                    </a>
                                                                </li><!--Status-->

                                                                <li class="slideItem{if (isset($url[0] ) && $url[0] == 'amazon') && ( (isset($url[1]) && $url[1] == 'actions') ) && ((isset($url[2]) && ($url[2] == 'offers' || $url[2] == 'products' || $url[2] == 'orders') || $url[2] == 'relations' || $url[2] == 'images' || $url[2] == 'deletion' || $url[2] == 'repricing' || $url[2] == 'fba' || $url[2] == 'shipping')) && $url[3] == $amazon_menu.id} active{/if}">
                                                                    <a class="sidebar-item_Fouth-down-link link" href="#" data-target="#amazon_actions_{$amazon_key}" data-toggle="collapse">
                                                                        {ci_language line="Actions"}  
                                                                    </a>
                                                                    <ul {if (isset($url[0] ) && $url[0] == 'amazon') && ( (isset($url[1]) && $url[1] == 'actions'))  && ((isset($url[2]) && ($url[2] == 'offers' || $url[2] == 'products' || $url[2] == 'orders') || $url[2] == 'relations' || $url[2] == 'images' || $url[2] == 'delete' || $url[2] == 'repricing' || $url[2] == 'fba' || $url[2] == 'shipping')) && $url[3] == $amazon_menu.id}class="active" style="display: block;"{/if} id="amazon_actions_{$amazon_key}">
                                                                        <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'offers') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                            <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/offers/{$amazon_menu.id}">
                                                                                {ci_language line="Offers"}                               
                                                                            </a>
                                                                        </li><!--Offers-->
                                                                        {if isset($amazon_creation_model) && !empty($amazon_creation_model)} {*isset($creation)*}
                                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'products') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/products/{$amazon_menu.id}">
                                                                                        {ci_language line="Products"}                               
                                                                                    </a>
                                                                                </li><!--Products-->
                                                                            {/if}
                                                                            {if isset($amazon_display[$amazon_menu.ext]['import_orders']) && !empty($amazon_display[$amazon_menu.ext]['import_orders'])}  
                                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'orders') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/orders/{$amazon_menu.id}">
                                                                                        {ci_language line="Orders"}                               
                                                                                    </a>
                                                                                </li><!--Orders-->
                                                                            {/if}
                                                                            <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'relations') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/relations/{$amazon_menu.id}">
                                                                                    {ci_language line="Relations"}                               
                                                                                </a>
                                                                            </li><!--Relations-->
                                                                            <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'images') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/images/{$amazon_menu.id}">
                                                                                    {ci_language line="Images"}                               
                                                                                </a>
                                                                            </li><!--Images-->
                                                                            {if isset($amazon_display[$amazon_menu.ext]['delete_products']) && !empty($amazon_display[$amazon_menu.ext]['delete_products'])}
                                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'delete') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/delete/{$amazon_menu.id}">
                                                                                        {ci_language line="Delete"}                               
                                                                                    </a>
                                                                                </li><!--Delete-->
                                                                            {/if}
                                                                            {if isset($amazon_display[$amazon_menu.ext]['repricing']) && $amazon_display[$amazon_menu.ext]['repricing'] == 1}
                                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'repricing') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/repricing/{$amazon_menu.id}">
                                                                                        {ci_language line="Repricing"}                               
                                                                                    </a>
                                                                                </li><!--Repricing-->
                                                                            {/if}
                                                                            {if isset($amazon_display[$amazon_menu.ext]['fba']) && $amazon_display[$amazon_menu.ext]['fba'] == 1}
                                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'fba') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/fba/{$amazon_menu.id}">
                                                                                        {ci_language line="FBA"}                               
                                                                                    </a>
                                                                                </li><!--FBA-->
                                                                            {/if}
                                                                            <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'shipping') && $url[3] == $amazon_menu.id}class="active"{/if}>
                                                                                <a class="sidebar-item_Fifth-down-link link" href="{$base_url}amazon/actions/shipping/{$amazon_menu.id}">
                                                                                    {ci_language line="Shipping"}                               
                                                                                </a>
                                                                            </li><!--Shipping-->
                                                                        </ul>

                                                                    </li><!--Action-->

                                                                </ul>

                                                            </li><!--task-->

                                                            <li class="{if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'error_resolutions') && $url[2] == $amazon_menu.id} active{/if}">
                                                                <a class="sidebar-item_Third-down-link link" href="{$base_url}amazon/error_resolutions/{$amazon_menu.id}">
                                                                    {ci_language line="Error Resolutions"} 
                                                                    <span class="badge badge-warning pull-right m-r10">
                                                                        {if isset($amazon_error[$amazon_menu.ext])}
                                                                            {$amazon_error[$amazon_menu.ext]}
                                                                        {else}
                                                                            0
                                                                        {/if}
                                                                    </span>
                                                                </a>
                                                            </li><!--Error Resolutions-->

                                                            <li class="{if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'report') && $url[2] == $amazon_menu.id} active{/if}">
                                                                <a class="sidebar-item_Third-down-link link" href="{$base_url}amazon/report/{$amazon_menu.id}">
                                                                    {ci_language line="Report"}                               
                                                                </a>
                                                            </li><!--Report-->

                                                            <li class="{if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'logs') && $url[2] == $amazon_menu.id} active{/if}">
                                                                <a class="sidebar-item_Third-down-link link" href="{$base_url}amazon/logs/{$amazon_menu.id}">
                                                                    {ci_language line="Logs"}                               
                                                                </a>
                                                            </li><!--Logs-->
                                                            {if isset($amazon_display[$amazon_menu.ext]['import_orders']) && $amazon_display[$amazon_menu.ext]['import_orders'] == 1}
                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'orders') && $url[2] == $amazon_menu.id} class="active" {/if}>
                                                                    <a class="sidebar-item_Third-down-link link" href="{$base_url}amazon/orders/{$amazon_menu.id}">
                                                                        {ci_language line="Orders"}                               
                                                                    </a>
                                                                </li><!--Orders-->
                                                                <li {if (isset($url[0] ) && $url[0] == 'amazon') && (isset($url[1]) && $url[1] == 'carts') && $url[2] == $amazon_menu.id} class="active" {/if}>
                                                                    <a class="sidebar-item_Third-down-link link" href="{$base_url}amazon/carts/{$amazon_menu.id}">
                                                                        {ci_language line="Carts"}                               
                                                                    </a>
                                                                </li><!--Carts-->
                                                            {/if}
                                                        </ul>
                                                    {else}
                                                        <ul class="collapse" id="amazon_menu{$amazon_key}">
                                                            <li id="parameter_before_import"  >
                                                                <a class="sidebar-item_Third-down-link link" href="#" class="red" >    
                                                                    <div>
                                                                        <i class="fa fa-warning"></i>
                                                                        {ci_language line="You must feed data source before use this menu."}
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    {/if}
                                                </li>
                                                {/foreach}
                                                </ul>
                                                {/if}
                                                </li><!--/.amazon-->
                                                {/if}                            

                                                <!-- General -->
                                                {if isset($menu['General'])}       
                                                    {foreach $menu['General']['submenu'] as $key => $submenu}
                                                        {if isset($url[0]) && $url[0] == 'general'}
                                                            {if isset($url[1]) && $url[1] == 'actions'} <!-- action have url length = 4, not action have url length = 3 -->
                                                                {$url_sub_marketplace = $url[3]}
                                                                {$url_id_country = $url[4]}
                                                            {else}
                                                                {$url_sub_marketplace = $url[2]}
                                                                {$url_id_country = $url[3]}
                                                            {/if}
                                                        {else}
                                                            {$url_sub_marketplace = ''}
                                                            {$url_id_country = ''}
                                                        {/if}
                                                        <!-- li general-->
                                                        <li class="sidebar-item_Once-down slideItem{if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $submenu.sub_marketplace)} active{/if}">
                                                            <a class="sidebar-item_Once-down-link link" href="#" data-target="#general" data-toggle="collapse">
                                                                {if isset($submenu['name_marketplace'])}
                                                                    {$marketplace_name = $submenu['name_marketplace']}
                                                                {else}
                                                                    {$marketplace_name = ''}    
                                                                {/if}
                                                                <span class="menu-text">{ci_language line=$marketplace_name}</span>
                                                            </a>
                                                            <!-- ul general-->
                                                            <ul class="{if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $submenu.sub_marketplace)}active" style="display: block;{/if}">
                                                                <!-- foreach $submenu -->
                                                                {foreach $submenu as $keysub => $sub_menu}
                                                                    {if $keysub != 'name_marketplace' && $keysub != 'sub_marketplace'}
                                                                        <!-- li sub_menu.domain -->
                                                                        <li class="slideItem{if $url_id_country == $sub_menu.id} active{/if}"> 
                                                                        <a class="sidebar-item_Twice-down-link link" href="#" data-target="#general_menu{$sub_menu.id}" data-toggle="collapse">
                                                                             <span class="menu-text">{$sub_menu.domain}</span>
                                                                        </a>
                                                                        <!-- if isset id shop -->
                                                                        {if isset($id_shop)}
                                                                            <ul {if isset($url[0]) && ($url[0] == 'general') && ($url_sub_marketplace == $sub_menu.sub_marketplace) && ($url_id_country == $sub_menu.id)} class="active" style="display: block;"{/if} id="mirakl_menu{$sub_menu.id}">
                                                                                <!-- li Configuration -->
                                                                                <li class="slideItem{if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $sub_menu.sub_marketplace) &&  isset($url[1]) && ($url[1] =="parameters" ||  $url[1] =="profiles" || $url[1] =="category" || $url[1] =="conditions" || $url[1] =="carriers") && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                    <a class="sidebar-item_Third-down-link link" href="#" data-target="#mirakl_config{$key}" data-toggle="collapse">
                                                                                        <span class="menu-text"> {ci_language line="Configuration"} </span>
                                                                                    </a>
                                                                                    <ul {if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $submenu.sub_marketplace) && ($url_id_country == $sub_menu.id) &&  isset($url[1]) && ($url[1] =="parameters" ||  $url[1] =="profiles" || $url[1] =="category" || $url[1] =="conditions" || $url[1] =="carriers")}class="active" style="display: block;"{/if} id="config_menu{$sub_menu.id}">
                                                                                        <li class="sidebar-item_Fouth-down {if isset($url[2]) && ($url[2] == $key) && $url[1] == 'parameters' && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="{$base_url}general/parameters/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="Parameters"}
                                                                                            </a>
                                                                                        </li>
                                                                                        <li class="sidebar-item_Fouth-down{if isset($url[2]) && ($url[2] == $key) && ($url[1] == 'profiles') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="{$base_url}general/profiles/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="profiles"}
                                                                                            </a>
                                                                                        </li>
                                                                                        <li class="sidebar-item_Fouth-down{if isset($url[2]) && ($url[2] == $key) && ($url[1] == 'category') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="{$base_url}general/category/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="category"}
                                                                                            </a>
                                                                                        </li>
                                                                                        <li class="sidebar-item_Fouth-down{if isset($url[2]) && ($url[2] == $key) && ($url[1] == 'conditions') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="{$base_url}general/conditions/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="conditions"}
                                                                                            </a>
                                                                                        </li>
                                                                                        <li class="sidebar-item_Fouth-down{if isset($url[2]) && ($url[2] == $key) && ($url[1] == 'carrier') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="{$base_url}general/carriers/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="Carriers"}
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <!-- End li Configuration -->
                                                                                <!--Task-->
                                                                                <li class="slideItem{if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions' || $url[1] == 'scheduled_tasks')} active{/if}">
                                                                                    <a class="sidebar-item_Third-down-link link" href="#" data-target="#mirakl_task{$key}" data-toggle="collapse">
                                                                                        {ci_language line="Task"}
                                                                                    </a>

                                                                                    <ul {if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $submenu.sub_marketplace) && ($url_id_country == $sub_menu.id) && ($url[1] == 'actions' || $url[1] == 'scheduled_tasks')} class="active" style="display: block;"{/if}> <!-- scheduled_tasks have url length = 3, not scheduled_tasks have url length = 4 -->
                                                                                        <li class="{if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'scheduled')}active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link" href="{$base_url}general/scheduled_tasks/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                {ci_language line="Scheduled Tasks"}                               
                                                                                            </a>
                                                                                        </li>

                                                                                        <li class="slideItem{if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && ($url[2] == 'offers' || $url[2] == 'products'  || $url[2] == 'delete' || $url[2] == 'actions_orders' || $url[2] == 'orders'))} active{/if}">
                                                                                            <a class="sidebar-item_Fouth-down-link link" href="#" data-target="#mirakl_actions_{$key}" data-toggle="collapse">
                                                                                                {ci_language line="Actions"}  
                                                                                            </a>

                                                                                            <ul {if (isset($url[0]) && $url[0] == 'general') && ($url_sub_marketplace == $sub_menu.sub_marketplace) && ($url_id_country == $sub_menu.id) && (isset($url[1]) && $url[1] == 'actions')  && (isset($url[2]) && ($url[2] == 'offers' || $url[2] == 'products' || $url[2] == 'delete' || $url[2] == 'actions_orders' || $url[2] == 'orders'))}class="active" style="display: block;"{/if} id="mirakl_actions_{$key}">
                                                                                                <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'offers')}class="active"{/if}>
                                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}general/actions/offers/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                        {ci_language line="Offers"}                               
                                                                                                    </a>
                                                                                                </li>
                                                                                                 <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'products')}class="active"{/if}>
                                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}general/actions/products/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                        {ci_language line="Products"}                               
                                                                                                    </a>
                                                                                                </li>
                                                                                                <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'delete')}class="active"{/if}>
                                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}general/actions/delete/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                        {ci_language line="Delete"}                               
                                                                                                    </a>
                                                                                                </li>
                                                                                                <!-- ORDER -->
                                                                                                <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && ($url[2] == 'actions_orders' || $url[2] == 'orders'))}class="active"{/if}>
                                                                                                    <a class="sidebar-item_Fifth-down-link link" href="{$base_url}general/actions/offers/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                        {ci_language line="Order"}                               
                                                                                                    </a>

                                                                                                    <ul {if (isset($url[0]) && $url[0] == 'general')  && ($url_sub_marketplace == $sub_menu.sub_marketplace) && ($url_id_country == $sub_menu.id) && (isset($url[1]) && $url[1] == 'actions')  && (isset($url[2]) && ($url[2] == 'orders' || $url[2] == 'actions_orders'))}class="active" style="display: block;"{/if} id="mirakl_actions_order_{$sub_menu.id}">
                                                                                                        <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'orders')}class="active"{/if}>
                                                                                                            <a class="sidebar-item_Sixth-down-link link" href="{$base_url}general/actions/orders/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                                {ci_language line="Accept Orders"}                               
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li {if (isset($url[0]) && $url[0] == 'general') && (isset($url[1]) && $url[1] == 'actions') && (isset($url[2]) && $url[2] == 'actions_orders')}class="active"{/if}>
                                                                                                            <a class="sidebar-item_Sixth-down-link link" href="{$base_url}general/actions/actions_orders/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                                                {ci_language line="Import Orders"}                               
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </li>
                                                                                                <!-- END ORDER --> 
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul>
                                                                                </li>
                                                                                <!--End Task-->
                                                                                <li class="sidebar-item_Third-down{if isset($url[0]) && ($url[0] == 'general') && ($url[1] == 'Orders') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                    <a class="sidebar-item_Third-down-link link" href="{$base_url}general/orders/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                        {ci_language line="Orders"}
                                                                                    </a>
                                                                                </li>
                                                                                <li class="sidebar-item_Third-down{if isset($url[0]) && ($url[0] == 'general') && ($url[1] == 'report') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                    <a class="sidebar-item_Third-down-link link" href="{$base_url}general/report/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                        {ci_language line="Report"}
                                                                                    </a>
                                                                                </li>
                                                                                <li class="sidebar-item_Third-down{if isset($url[0]) && ($url[0] == 'general') && ($url[1] == 'logs') && ($url_id_country == $sub_menu.id)} active{/if}">
                                                                                    <a class="sidebar-item_Third-down-link link" href="{$base_url}general/logs/{$sub_menu.sub_marketplace}/{$sub_menu.id}">
                                                                                        {ci_language line="Logs"}
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        {else}
                                                                            <ul class="collapse">
                                                                                <li id="parameter_before_import"  >
                                                                                    <a class="sidebar-item_Third-down-link link" href="#" class="red" >    
                                                                                        <div>
                                                                                            <i class="fa fa-warning"></i>
                                                                                            {ci_language line="You must feed data source before use this menu."}
                                                                                        </div>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                        {/if}
                                                                        <!-- End if isset id shop -->
                                                                         </li>
                                                                         <!-- End li sub_menu.domain -->
                                                                    {/if}
                                                                {/foreach}
                                                                <!-- End foreach $submenu -->
                                                            </ul>
                                                            <!-- End ul general-->
                                                        </li>
                                                        <!-- End li general-->
                                                    {/foreach}
                                                {/if}   
                                                <!-- End General -->
                                                    
                                                <!-- Google Shopping -->
                                                {if isset($menu['google_shopping'])}
                                                    <li class="sidebar-item_Once-down slideItem{if (isset($url[0]) && $url[0] == 'google_shopping')} active{/if}">
                                                        <a class="sidebar-item_Once-down-link link" href="#" data-target="#google_shopping" data-toggle="collapse">
                                                            <span class="menu-text"> {ci_language line="Google Shopping"} </span>
                                                        </a>
                                                        {if isset($menu['google_shopping']['submenu'])}
                                                            <ul class="{if ( isset($url[0]) && $url[0] == 'google_shopping')}active" style="display: block;{/if}" id="google_shopping">
                                                                {foreach $menu['google_shopping']['submenu'] as $google_shopping_key => $google_shopping_menu}
                                                                    <li class="slideItem{if isset($url[2]) && $url[2] == $google_shopping_menu.id} active{/if}">
                                                                        <a class="sidebar-item_Twice-down-link link" href="#" data-target="#google_shopping{$google_shopping_key}" data-toggle="collapse">
                                                                            <span class="menu-text">{$google_shopping_menu['domain']}</span>
                                                                        </a>
                                                                        <ul {if isset($url[0]) && $url[0] == 'google_shopping' && ($url[2] == $google_shopping_menu.id || (isset($url[3]) && $url[3] == $google_shopping_menu.id))}class="active" style="display: block;"{/if} id="google_shopping{$google_shopping_key}">
                                                                            <li class="slideItem{if (isset($url[0]) && $url[0] == 'google_shopping') &&  isset($url[1]) && ($url[1] =="configuration" ||  $url[1] =="parameter" ||  $url[1] =="profiles" || $url[1] =="categories" || $url[1] =="conditions" || $url[1] =="mappings" || $url[1] =="attributes") && (isset($url[2]) && $url[2] == $google_shopping_menu.id)} active{/if}">
                                                                                <a class="sidebar-item_Third-down-link link" href="#" data-target="#configuration{$google_shopping_key}" data-toggle="collapse">
                                                                                    <span class="menu-text"> {ci_language line="Configuration"} </span>
                                                                                </a>
                                                                                <ul class="{if (isset($url[0]) && $url[0] == 'google_shopping') &&  isset($url[1]) && ($url[1] =="configuration" ||  $url[1] =="parameter" || $url[1] =="models" || $url[1] =="profiles" || $url[1] =="categories" || $url[1] =="conditions" || $url[1] =="mappings" || $url[1] =="attributes") && (isset($url[2]) && $url[2] == $google_shopping_menu.id)}active" style="display: block;{/if}" id="configuration{$google_shopping_key}">
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'parameter')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/parameter/{$google_shopping_menu.id}">
                                                                                            {ci_language line="Parameter"}
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'models')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/models/{$google_shopping_menu.id}">
                                                                                            {ci_language line="Models"}
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'profiles')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/profiles/{$google_shopping_menu.id}">
                                                                                            {ci_language line="Profiles"}
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'categories')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/categories/{$google_shopping_menu.id}">
                                                                                            {ci_language line="Categories"}
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'conditions')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/conditions/{$google_shopping_menu.id}">
                                                                                            {ci_language line="Conditions"}
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                            <li class="slideItem{if (isset($url[0]) && $url[0] == 'google_shopping') &&  isset($url[1]) && ($url[2] =="product" ||  $url[2] =="category") && (isset($url[3]) && $url[3] == $google_shopping_menu.id)} active{/if}">
                                                                                <a class="sidebar-item_Third-down-link link" href="#" data-target="#google_offeroption{$google_shopping_key}" data-toggle="collapse">
                                                                                    <span class="menu-text"> {ci_language line="Offers Options"} </span>
                                                                                </a>
                                                                                <ul class="{if (isset($url[1]) && $url[1] == 'offers_options')}active" style="display: block;{/if}" id="google_offeroption{$google_shopping_key}">
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'products')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/offers_options/product/{$google_shopping_menu.id}/{$menu['google_shopping']['id']}">
                                                                                            {ci_language line="by Products"}
                                                                                        </a>
                                                                                    </li>
                                                                                    <li class="sidebar-item_Fouth-down{if (isset($url[2]) && $url[2] == 'category')} active{/if}" >
                                                                                        <a class="sidebar-item_Fouth-down-link link" href="{$base_url}google_shopping/offers_options/category/{$google_shopping_menu.id}/{$menu['google_shopping']['id']}">
                                                                                            {ci_language line="by Category"}
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                            <li class="{if (isset($url[0]) && $url[0] == 'google_shopping') &&  isset($url[1]) && ($url[1] =="logs") && (isset($url[3]) && $url[3] == $google_shopping_menu.id)} active{/if}">
                                                                                <a class="sidebar-item_Third-down-link link" href="{$base_url}google_shopping/logs/{$google_shopping_menu.id}">
                                                                                    <span class="menu-text"> {ci_language line="Logs"} </span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                {/foreach}
                                                            </ul>
                                                        {/if}    
                                                    </li>
                                                {/if}
                                                <!-- End Google Shopping -->

                                                </ul>
                                            </li>

                                            <li class="sidebar-item{if (isset($url[0]) && $url[0] == 'stock')} active{/if}">
                                                <a class="sidebar-item_link link" href="">
                                                    <i class="icon-list"></i>
                                                    {ci_language line="Stock"}
                                                </a>

                                                <ul class="sidebar_down{if (isset($url[0]) && ($url[0] == 'stock'))}active" style="display: block;{/if}" id="stock">
                                                    <li class="sidebar-item_Once-down {if (isset($url[0]) && ($url[0] == 'stock')) && (isset($url[1]) && $url[1] == 'report')} active{/if}" >
                                                        <a class="sidebar-item_Once-down-link link" href="{$base_url}stock/report">
                                                            {ci_language line="Report"}
                                                        </a>
                                                    </li>
                                                    <li class="sidebar-item_Once-down {if (isset($url[0]) && ($url[0] == 'stock')) && (isset($url[1]) && $url[1] == 'carts')} active{/if}" >
                                                        <a class="sidebar-item_Once-down-link link" href="{$base_url}stock/carts">
                                                            {ci_language line="Cart Report"}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li> <!--Stock-->

                                            <li class="sidebar-item{if (isset($url[0]) && ($url[0] == 'help'))} active" style="display: block;{/if}">
                                                <a class="sidebar-item_link link" href="{$base_url}help{*mailto:{$support_email}*}">
                                                    <i class="icon-help"></i> Help
                                                </a>
                                            </li>

                                        </ul><!--/.list-unstyled-->
                                    </div>
                                </div>
                                {/if}
