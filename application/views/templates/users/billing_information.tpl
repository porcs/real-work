{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/users/billing_information.css" />

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="edit_billing_information"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="information"}</h4>						
				</div>
			</div>
		</div>
                <form action="{$base_url}index.php/users/billing_information_edit" method="post" id="billinginfoform" autocomplete="off"  >
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="company"}  </p>

				<div class="form-group">
					<input type="text" name="company" id="company" value="{if isset($account.company)}{$account.company}{/if}" class="form-control{* validate[required]*}">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="firstname"} *</p>
				<div class="form-group">
					<input type="text" name="firstname" id="firstname" value="{if isset($account.first_name)}{$account.first_name}{/if}" class="form-control validate[required]">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="lastname"} *</p>
				<div class="form-group">
					<input type="text" name="lastname" id="lastname" value="{if isset($account.last_name)}{$account.last_name}{/if}" class="form-control validate[required]">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* {ci_language line="fields are required"}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix p-b10 b-Bottom m-b10">
					<h4 class="headSettings_head">{ci_language line="address"}</h4>						
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="address1"} *</p>
				<div class="form-group">
					<input type="text" name="address1" id="address1" value="{if isset($account.address1)}{$account.address1}{/if}" class="form-control validate[required]">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="address2"}</p>
				<div class="form-group">
					<input type="text" name="address2" id="address2" value="{if isset($account.address2)}{$account.address2}{/if}" class="form-control validate[required]">					
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="state_region"} *</p>
				<div class="form-group">
					<input type="text" name="stateregion" id="stateregion" value="{if isset($account.stateregion)}{$account.stateregion}{/if}" class="form-control validate[required]">
				</div>
			</div>
                </div>
		<div class="row">
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="zipcode"} *</p>
				<div class="form-group">
					<input type="text" name="zipcode" id="zipcode" value="{if isset($account.zipcode)}{$account.zipcode}{/if}" class="form-control validate[required]">
				</div>
			</div>
			<div class="col-sm-4">
				<p class="regRoboto poor-gray">{ci_language line="city"} *</p>
				<div class="form-group">
					<input type="text" name="city" id="city" value="{if isset($account.city)}{$account.city}{/if}" class="form-control validate[required]">
				</div>
			</div>
			<div class="col-sm-4 country form-group">
				<p class="regRoboto poor-gray">{ci_language line="country"} *</p>
				<select class="search-select validate[required]" id="country" name="country" data-placeholder="Choose">
					{$country_option}
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="pull-right tooSmall regRoboto poor-gray">* {ci_language line="fields are required"}</p>
			</div>
		</div>
		<div class="row">
                            <!--Message-->
                            <input type="hidden" name="{$csrf.key}" value="{$csrf.value}" />
                            <input type="hidden" name="id" value="{$account.id}" />
			<div class="col-xs-12">
				<button type="submit" class="btn btn-save pull-right m-t20">  {ci_language line="save"} </button>
			</div>
		</div>
                </form>
	</div>
<!-- // END content -->
{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/users/billing_information.js" language="javascript"></script>