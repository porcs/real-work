{include file="header.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/users/register.css" />
<script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<input type="hidden" id="captcha_text_invalid" value="{ci_language line="invalide_secure_code"}." />
<input type="hidden" id="captcha_url" value="{$base_url}users/captcha" />
<input type="hidden" id="captcha_key" value="6LcYi90SAAAAAF31qAXcmL-YrXM6PQTqyB1pAZhV" />
<input type="hidden" id="captcha_lang" value="en" />

<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <form action="{$base_url}users/register_save" method="post" id="registerform" autocomplete="off">
                        <input type="hidden" id="referral" name="referral" value="{$user_referral}">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">Create a new Account</h4>
				<div class="row">
					<div class="col-sm-6">
                                            <a href="{if isset($facebook_path)}{$facebook_path}{/if}" class="btn btn-facebook pull-width"><i class="fa fa-facebook"></i>{ci_language line="Facebook Account"}</a>
					</div>
					<div class="col-sm-6">
                                            <a href="{if isset($googleplus_path)}{$googleplus_path}{/if}" class="btn btn-google pull-width"><i class="fa fa-google-plus"></i>{ci_language line="Google Account"}</a>
					</div>
				</div>
				<div class="row">
                                    <div class="col-xs-12">
                                        <p class="regRoboto poor-gray">{ci_language line="email"}</p>
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" placeholder="{ci_language line="email"}" />
                                            <p id="email_return_message" class="display-none help-inline good"></p>
                                        </div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-xs-12">
                                        <p class="regRoboto poor-gray">{ci_language line="password"}</p>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" class="form-control" placeholder="{ci_language line="password"}" />
                                        </div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-xs-12">
                                        <p class="regRoboto poor-gray">Confirm password</p>
                                        <div class="form-group">
                                            <input type="password" name="con_password" id="con_password" class="form-control" placeholder="{ci_language line="passwordconf"}">
                                        </div>
                                    </div>
				</div>
				<div class="row">
					<div class="col-xs-12">
                                            <p class="regRoboto poor-gray">{ci_language line="security_challlenge"}</p>
                                            <div id="captchadiv"></div>
                                            <p id="captchaStatus" class="alert alert-error display-none help-inline error"></p>
					</div>
				</div>
				<div class="row">					
					<div class="col-xs-12">
                                            <!--message-->
                                            <input type="hidden" id="username-available-message" value="{ci_language line="username_available"}"/>
                                            <input type="hidden" id="username-duplicate-message" value="{ci_language line="username_already_taken"}"/>
                                            <input type="hidden" id="email-available-message" value="{ci_language line="email_available"}"/>
                                            <input type="hidden" id="email-duplicate-message" value="{ci_language line="email_already_taken"}"/>
                                            <input type="hidden" id="checking" value="{ci_language line='Checking'} ... "/>
                                            <input type="hidden" id="firstname-message" value="{ci_language line='please_enter_your_first_name'}"/>
                                            <input type="hidden" id="lastname-message" value="{ci_language line='please_enter_your_last_name'}"/>
                                            <input type="hidden" id="username-message" value="{ci_language line='please_enter_your_username'}"/>
                                            <input type="hidden" id="password-message" value="{ci_language line='please_provide_a_password'}"/>
                                            <input type="hidden" id="password-long-message" value="{ci_language line='your_password_must_be_long'}"/>
                                            <input type="hidden" id="con_password-message" value="{ci_language line='please_enter_the_same_value_again'}"/>
                                            <input type="hidden" id="email-message" value="{ci_language line='please_enter_your_email'}"/>
                                            <button type="submit" class="btn btn-save pull-right">{ci_language line='Create Account'}</button>
					</div>
				</div>
			</div>
			<div class="row">
                            <div class="col-xs-12">
                                <h4 class="montserrat text-uc p-b10">{ci_language line="support_informations"}</h4>					
                            </div>
			</div>
			<div class="row dTable">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <img class="icon-small-circle m-r20" src="{$base_url}{$support_image}" alt="">
                                </div>
                                <p class="poor-gray regRoboto p-b40">
                                    {nl2br($support_txt)}
                                </p>												
                            </div>
			</div>
                    </form>
		</div>
	</div>
</div>

{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$base_url}assets/js/jquery.validate.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$base_url}assets/js/FeedBiz/users/register.js"></script>