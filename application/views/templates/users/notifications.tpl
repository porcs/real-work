{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/users/notifications.css" />

<div id="content" class="main p-rl40 p-xs-rl20" class="row">
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Notifications"}</h1>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        
        {include file="popup_step.tpl"}
        <div class="row">
            <div class="col-xs-12">
                <div class="headSettings clearfix p-b10 b-Bottom">
                    <h4 class="headSettings_head">{ci_language line="Your Notifications"}</h4>						
                </div>
            </div>
        </div>
                
        <div class="row">
            
            <div class="notifications col-xs-12">
                <div class="form-group">
                        <ul>
                            <li class="read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li class="none_read">
                                <div class="ui_title"><a href="#">Test inbox na ja</a></div>
                                    <div class="ui_time" title="30/4/2556 10:10">10 mins ago</div>
                                    <div class="ui_mark_read" title="Mark as Read"></div>
                                    <div class="ui_close" title="Turn Off"></div>
                            </li>
                            
                            <li>
                                <div class="ui_title text-center"><a href="#">{ci_language line="View more"}...</a></div>
                            </li>
                        </ul>
                    </div>
                </div>
        </div>
                
                
</div>
                
<script src="{$cdn_url}assets/js/FeedBiz/users/notifications.js"></script>
{include file="footer.tpl"}
