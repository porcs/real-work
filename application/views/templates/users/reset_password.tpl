{*not use / old file*}
{include file="header.tpl"}
{ci_config name="base_url"}

<div class="container">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    {if isset($error) }<div class="alert alert-error">{$error}</div>{/if}
                    {if isset($message) }<div class="alert alert-warning">{$message}</div>{/if}
                    <form action="{$base_url}users/reset_password/{$code}" method="post" id="reset_pass_form" autocomplete="off">
			<div class="signIn">
				<h4 class="montserrat text-center text-uc p-b10">{ci_language line="reset_password"}</h4>				
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">{ci_language line="enter_your_new_password"}</p>
						<div class="form-group">
							<input type="password" name="new" id="new" class="form-control" placeholder="{ci_language line="password"}" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p class="regRoboto poor-gray">Confirm password</p>
						<div class="form-group">
							<input type="password" name="new_confirm" id="new_confirm" class="form-control" placeholder="{ci_language line="passwordconf"}" />
						</div>
					</div>
				</div>
				<div class="row">					
					<div class="col-xs-6 m-t5">
						<a class="sign_link" href="{$base_url}"><i class="fa fa-long-arrow-left"></i> {ci_language line="Back to login"}</a>
					</div>
					<div class="col-xs-6">
                                                <input type="hidden" name="email" value="{if isset($email)}{$email}{/if}" />
                                                <input type="hidden" name="id" value="{if isset($id)}{$id}{/if}" />
                                                <input type="hidden" name="forgotten_password_code" value="{if isset($forgotten_password_code)}{$forgotten_password_code}{/if}" />
                                                <input type="submit" name="action" value="{ci_language line="save"}" class="btn btn-save pull-right"/>

                                                <!-- pop up message -->
                                                <input type="hidden" name="provide_passsword_message" value="{ci_language line="please_provide_a_password"}" />
                                                <input type="hidden" name="passsword_message" value="{ci_language line="your_password_must_be_long"}" />
                                                <input type="hidden" name="comfirm_passsword_message" value="{ci_language line="please_enter_the_same_value_again"}" />
					</div>
				</div>
			</div>
                    </form>
		</div>
	</div>
</div>
{include file="footer.tpl"}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/users/reset_password.js"></script>