{include file="header.tpl"}
{if isset($token_assist)}
    {include file="manager_sidebar.tpl"}
{else}
    {include file="sidebar.tpl"}
{/if}
{ci_config name="base_url"}

<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/users/security.css" />
<div id="content" class="{if !isset($popup)}main {/if}p-rl40 p-xs-rl20 custom-form"  >
        <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Security"}</h1>
        {if !isset($token_assist)}
            {include file="breadcrumbs.tpl"} <!--breadcrumb-->
         
        {/if}
        
         
    {if isset($mfa_enable) && $mfa_enable == 1}
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                <h4 class="headSettings_head">{ci_language line="More security to your account"}</h4>						
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p class="regRoboto  light-gray">{ci_language line="Security activated; next login, Feed.biz will ask you, in addition to your password, the code shown by Google Authenticaor on your phone, you will can login safe."}</p> 
        </div> 
    </div>
        
    <div class="row">   
        <div class="col-sm-5 form-group">

            <div class="form-group  padding_left_100" id="otp_form_status_cv">         
            <p class=" regRoboto dark-gray montserrat  ">{ci_language line="Login security"} </p>
            <div class="cb-switcher  checked ">
                <label class="inner-switcher checked">
                    <input name="mfa_status" id="mfa_status" type="checkbox" value="1"  checked="checked"   data-state-on="{ci_language line="On"}" data-state-off="{ci_language line="Off"}">
                </label>
                <span class="cb-state">{ci_language line="No"}</span>
            </div> 
            </div>
            
            <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Note: Changing status will require OTP."}</p>                         
            <form method="post" class="padding_left_100 display-none" id="change_status_form"  >
                <div class="form-group" id="otp_form_cv">
                    <p class="regRoboto"><span>{ci_language line="Enter OTP"}</span> <input type="text" name="otp_reverify" id="otp_confirm" class="form-control valid" style="width:100px"></p>
                    
                    <button class="btn btn-save pull-right m-t20" id="otp_submit" type="submit">  {ci_language line="Ok"}   </button>
                </div>
                
            </form>  
        </div> 
    </div>
    {else}
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 b-Bottom">
                <h4 class="headSettings_head">{ci_language line="More security to your account"}</h4>						
            </div>
        </div>
    </div>
            
    <div class="row">     
        <div class="col-sm-12">
            <div class="headSettings clearfix p-b10 b-Bottom m-b20 b-Top-None">
                <h4 class="headSettings_head">{ci_language line="Install"}</h4>
            </div>
        </div>
    </div>
            
    <div class="row">
        <div class="col-sm-12">
            <p class="regRoboto  light-gray">{ci_language line="Step 1: Install Google Authenticator"}</p>
            <p class="regRoboto   padding_left_100 "><a class="link" href="https://support.google.com/accounts/answer/1066447" target="blank">https://support.google.com/accounts/answer/1066447</a></p>
        </div> 
    </div> 
    <div class="row">
        <div class="col-sm-12">
            <p class="regRoboto  light-gray">{ci_language line="Step 2: Scan your QR code"}</p>
            <p class="regRoboto  padding_left_150 ">{if isset($qr_code)}{$qr_code}{/if}</p>
        </div> 
    </div> 
        
        
    <div class="row">
        <div class="col-sm-12  ">
            <p class="regRoboto light-gray ">{ci_language line="Step 3: Confirm by the code displayed on Google Authenticator"}</p>
             
            <form method="post" class="padding_left_100">
                <div class="form-group" id="otp_form_cv">
                    <p class="regRoboto"><span>{ci_language line="Enter OTP"}</span> <input type="text" name="otp_verify" id="otp_confirm" class="form-control valid" style="width:100px"></p>
                    
                    <button class="btn btn-save pull-right m-t20" id="otp_submit" type="submit">  {ci_language line="Ok"}   </button>
                </div>
                
            </form> 
        </div> 
    </div>
                
    <div class="row">
        <div class="col-sm-12">
            <p class="regRoboto  light-gray">{ci_language line="Step 4: It's done; next login, Feed.biz will ask you, in addition to your password, the code shown by Google Authenticaor on your phone, you will can login safe."}</p> 
        </div> 
    </div>
        
    <div class="row m-t20">     
        <div class="col-sm-12 m-t20">
            <div class="headSettings clearfix p-b10 b-Bottom m-b20">
                <p class="headSettings_head m-t10">{ci_language line="Reset"}</p>						
            </div>
        </div>
    </div> 
        
    <div class="row ">
        <div class="col-sm-12"> 
            <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Reset your QR code?"} <a class="link" 
{if isset($token_assist)}
    href="{$base_url}/manager/security/reset_otp"
{else}
    href="{$base_url}/users/security/reset_otp"
{/if}
    href="/users/security/reset_otp"
    >{ci_language line="Click here"}</a></p> 
        </div> 
    </div> 
</div>
    {/if} 
<script src="{$cdn_url}assets/js/FeedBiz/users/security.js"></script>
{include file="footer.tpl"}
 