<div class="feedbizloader m-t20 hidden"><div class="feedbizcolor"></div><p class="m-t10 text-info">Processing...</p></div>        

<div class="row">
    <div class="col-xs-12">
        <div class="b-Bottom p-b10 p-t10">
            <button type="button" class="link showProfile p-size" id="add">{ci_language line="+ Add a Model"}</button>
            <p class="pull-right montserrat poor-gray"><span class="dark-gray countModel">{if isset($models_selected) && !empty($models_selected)}{$models_selected|@count}{else}0{/if}</span> {ci_language line="Item(s)"}</p>
        </div>
    </div>
</div>

<div id="main" class="row showSelectBlock market-models">
    <div class="col-md-12 col-lg-12 custom-form m-t10">
        <button type="button" class="link removeProfile remove">{ci_language line="Remove a model from list"}</button>

        <div class="profileBlock clearfix market-model">

            {if !isset($popup)}
                <div class="validate blue">
                    <div class="validateRow">
                        <div class="validateCell">
                            <i class="note"></i>
                        </div>
                        <div class="validateCell">
                            <p class="pull-left">
                                {ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
                            </p>
                        </div>
                    </div>
                </div>
            {/if}

            <!-- Model Name -->
            <p class="poor-gray">{ci_language line="Model name"}</p>
            <div class="form-group has-error">
                <input class="form-control" type="text" rel="name" value="{if isset($model_name)}{$model_name}{/if}" />
                <div class="error">
                    <p>
                        <i class="fa fa-exclamation-circle"></i>
                        {ci_language line="Set Model Name First"}
                    </p>
                </div>
            </div>
            <p class="poor-gray text-right">
                {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
            </p>

            <!-- Product Universe -->
            <p class="poor-gray">{ci_language line="Product Universe"}</p>
            <div class="form-group">
                <select rel="product_universe" disabled data-placeholder="Choose" required="required">
                    <option value="">{ci_language line="Choose a Product Universe option"}</option>
                    {if isset($product_universe)}
                        {foreach $product_universe as $key_puv => $puv}
                            <option value="{$puv['id']}" {*if isset($puv['universe'])}data-universe="{$puv['universe']}"{/if*}>{$puv['text']}</option>
                        {/foreach}
                    {/if}
                </select>                   
            </div>
            <p class="poor-gray text-right">{ci_language line="Choose the main (Root Node) universe for this model."}</p>
            <!--Product Type-->
            <p class="poor-gray">{ci_language line="Product Type"}</p>
            <div rel="product_type_container">
                <div class="form-group withIcon m-t20">
                    <select rel="product_type" disabled data-placeholder="Choose">
                        <option value="">{ci_language line="Choose a Product Type option"}</option>
                        {if isset($product_universe)}
                            {foreach $product_universe as $key_puv => $puv}
                                <option value="{$puv['id']}">{$puv['text']}</option>
                            {/foreach}
                        {/if}
                    </select>
                    <i class="cb-plus good addProducttype"></i>
                </div>
            </div>
            <p class="poor-gray text-right">{ci_language line="Choose the product type for this model"}.</p>

            <!-- Group? -->
            <div class="variation">

                <!-- Variation -->
                <div class="variation_fields_options" rel='variation'>
                    <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                        {ci_language line="Variation"}
                    </p>
                    <div class="row m-b10 m-t20">
                        {if isset($variation_theme)}
                            {foreach $variation_theme as $key_vat => $vat}
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label class="col-md-2 text-capitalize text-right p-t10">{$key_vat}{if $vat['required']==1}<b class="black strong-text">*</b>{/if}</label>
                                            <div class="col-md-3">
                                                <select rel="{$key_vat}" disabled data-placeholder="Choose" {if $vat['required']==1}required="required"{/if}>
                                                    <option value="">{ci_language line="Choose a Variation option"}</option>
                                                    <option value="default_value" class="color:red">{ci_language line="Default value"}</option>

                                                    {if isset($attributes) && !empty($attributes)}
                                                        <optgroup label="{ci_language line='Attribute'}">
                                                            {foreach $attributes as $key_atb => $atb}
                                                                <option value="Attribute_{$key_atb}">{$atb}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    {/if}
                                                    {if isset($features) && !empty($features)}
                                                        <optgroup label="{ci_language line='Feature'}">
                                                            {foreach $features as $key_ft => $ft}
                                                                <option value="Feature_{$key_ft}">{$ft}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    {/if}
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <input rel="{$key_vat}" type="text" class="form-control hidden" disabled="disabled" />
                                                <input rel="{$key_vat}" type="hidden" />
                                            </div>
                                            <div class="col-md-3">
                                                <label class="cb-checkbox pull-left p-t10">
                                                    <input rel="{$key_vat}" type="checkbox" disabled="disabled" value="{$vat['tag']}" />
                                                    {ci_language line="Group by"} {$key_vat|ucfirst} ?
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                </div>
            </div>

            <!-- Specific -->
            <div class="specific_fields_options" rel="specific">
                <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                    {ci_language line="Specific Fields"}
                </p>
                <div class="row m-b10 m-t20">
                    {if isset($specific_fields)}
                        {foreach $specific_fields as $key_scf => $scf}
                            <div class="col-xs-12{if $key_scf=="shipping_weight" or $key_scf=="shipping_label"} hidden{/if}">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="col-md-2 text-capitalize text-right p-t10">{$key_scf}{if $scf['required']==1}<b class="black strong-text">*</b>{/if}</label>
                                        <div class="col-md-3">
                                            {assign var=yesno_option value=FALSE}
                                            {if isset($scf['values'])}
                                                {if COUNT($scf['values'])==2 and $scf['values'][0] == 'TRUE' and $scf['values'][1] == 'FALSE'}
                                                    {assign var=yesno_option value=TRUE}
                                                {/if}
                                            {/if}

                                            {if $yesno_option}
                                                <label class="cb-radio">
                                                    <input type="radio" rel="{$key_scf}" disabled="disabled" value="1" {if $scf['required']==1}required="required"{/if} />
                                                    {ci_language line="Yes"}
                                                </label>
                                                <label class="cb-radio">
                                                    <input type="radio" rel="{$key_scf}" disabled="disabled" value="0" {if $scf['required']==1}required="required"{/if} />
                                                    {ci_language line="No"}
                                                </label>
                                            {else}
                                                <select rel="{$key_scf}" disabled data-placeholder="Choose" {if $scf['required']==1}required="required"{/if}>
                                                    <option value="">{ci_language line="Choose a Specific Field option"}</option>
                                                    <option value="default_value">{ci_language line="Default value"}</option>
                                                    {if isset($custom_label) && !empty($custom_label)}
                                                        {foreach $custom_label as $key_cl => $cl}
                                                            <option value="{$cl}">{$cl}</option>
                                                        {/foreach}
                                                    {/if}
                                                    {if isset($attributes) && !empty($attributes)}
                                                        <optgroup label="{ci_language line='Attribute'}">
                                                            {foreach $attributes as $key_atb => $atb}
                                                                <option value="Attribute_{$key_atb}">{$atb}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    {/if}
                                                    {if isset($features) && !empty($features)}
                                                        <optgroup label="{ci_language line='Feature'}">
                                                            {foreach $features as $key_ft => $ft}
                                                                <option value="Feature_{$key_ft}">{$ft}</option>
                                                            {/foreach}
                                                        </optgroup>
                                                    {/if}
                                                </select>
                                            {/if}
                                        </div>
                                        <div class="col-md-3">
                                            {if !$yesno_option}
                                                {if isset($scf['values'])}
                                                    <select rel="{$key_scf}" data-placeholder="Choose" {if $scf['required']==1}required="required"{/if} class="hidden">
                                                        {foreach $scf['values'] as $key_scf_val => $scf_val}
                                                            <option value="{$scf_val}">{$scf_val}</option>
                                                        {/foreach}
                                                    </select>
                                                {else}
                                                    <input rel="{$key_scf}" type="text" class="form-control hidden" disabled="disabled" />
                                                {/if}
                                            {/if}
                                            <input rel="{$key_scf}" type="hidden" value="select" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>

            <!-- Custom label -->
            <div class="custom_label_options" rel="custom">
                <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                    {ci_language line="Custom label"}
                </p>
                <div class="row m-b10 m-t20">
                    {if isset($custom_attributes)}
                        {foreach $custom_attributes as $key_cab => $cab}
                            <div class="col-xs-12">
                                <div class="form-group m-t10">
                                    <div class="col-md-12">
                                        <label class="col-md-2 text-capitalize text-right p-t10">
                                            {$key_cab}{if $cab['required']==1}
                                                <b class="black strong-text">*</b>
                                            {/if}
                                        </label>
                                        <div class="col-md-3">
                                            <select rel="{$key_cab}" class="search-select"  data-placeholder="Choose" {if $scf['required']==1}required="required"{/if}>
                                                <option value="">{ci_language line="Choose a Specific Field option"}</option>
                                                <option value="default_value">{ci_language line="Default value"}</option>
                                                {if isset($custom_label)}
                                                    {foreach $custom_label as $key_cl => $cl}
                                                        <option value="{$cl}">{$cl}</option>
                                                    {/foreach}
                                                {/if}
                                                {if isset($attributes) && !empty($attributes)}
                                                    <optgroup label="{ci_language line='Attribute'}">
                                                        {foreach $attributes as $key_atb => $atb}
                                                            <option value="Attribute_{$key_atb}">{$atb}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}
                                                {if isset($features) && !empty($features)}
                                                    <optgroup label="{ci_language line='Features'}">
                                                        {foreach $features as $key_atb => $atb}
                                                            <option value="Features[{$key_atb}]">{$atb}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <input rel="{$key_cab}" type="text" class="form-control hidden" disabled="disabled" {if $cab['required']==1}required="required"{/if} />
                                            <input rel="{$key_cab}" type="hidden" value="input" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>


        </div>
    </div>
</div>

<div id="tasks">

    {if isset($models_selected) && !empty($models_selected) }
        {foreach from=$models_selected key=k item=data}
            <div class="row market-models">

                <div class="col-xs-12">
                    <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                        <p class="text-uc dark-gray montserrat pull-sm-left p-t10 m-b0">
                        {if isset($data.name)}{$data.name}{/if} : 
                        <span>
                        {if isset($data.selected_universe)}{$data.selected_universe}{/if}
                        {if isset($data.selected_product_type) && !empty($data.selected_product_type)} 
                            &nbsp;<i class="fa fa-angle-right"></i> 
                            {$data.selected_product_type}
                        {/if}
                        {if isset($data.selected_product_subtype) && !empty($data.selected_product_subtype)}
                            &nbsp;<i class="fa fa-angle-right"></i> 
                            {$data.selected_product_subtype}
                        {/if}
                    </span>
                </p>
                <ul class="pull-sm-right amazonEdit">
                    <li class="editProfile">
                        <button type="button" class="link editProfile_link edit_model">
                            <i class="fa fa-pencil"></i> {ci_language line="edit"}
                        </button>
                        <!-- a class="editProfile_link" href="">edit</a> -->
                    </li>
                    <li class="remProfile">
                        <button type="button" class="link removeProfile_link remove_model" rel="{$data.name}" value="{$data.id_model}">
                            <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                        </button>  
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-12 col-lg-12 custom-form">
            <div  rel="{$k}" class="profileBlock clearfix market-model showSelectBlock m-t10 m-b30">

                <input type="hidden" rel="id_model" name="model[{$k}][id_model]" id="model_{$k}_id_model"  value="{$data.id_model}" />
                <input type="hidden" rel="memory_usage" name="model[{$k}][memory_usage]" id="model_{$k}_memory_usage" data-content="{$k}" />

                <!-- Model Name -->
                <p class="poor-gray">{ci_language line="Model name"}</p>
                <div class="form-group">
                    <input type="text" rel="name" value="{if isset($data.name)}{$data.name}{/if}" name="model[{$k}][name]" id="model_{$k}_name" data-content="{$k}" class="form-control"/>
                    <div class="error" style="display: none;">
                        <p>
                            <i class="fa fa-exclamation-circle"></i>
                            {ci_language line="Set Model Name First"}
                        </p>
                    </div>
                </div>
                <p class="poor-gray text-right">
                    {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
                </p>

                <!-- Product Universe -->
                <p class="poor-gray">{ci_language line="Product Universe"}</p>
                <div class="form-group">
                    {if isset($data.product_universe)}
                        <input type="text" value="{if isset($data.product_universe)}{$data.product_universe}{/if}" rel="product_universe" name="model[{$k}][product_universe]" id="model_{$k}_universe" data-content="{$k}" required="required" />
                    {/if}
                </div>
                <p class="poor-gray text-right">{ci_language line="Choose the main (Root Node) universe for this model."}</p>

                <!--Product Type-->
                <p class="poor-gray">{ci_language line="Product Type"}</p>
                <div rel="product_type_container">
                    {if isset($data.product_type)}
                        {foreach $data.product_type as $key_pdt => $pdt name=product_type}
                            <div class="form-group">
                                <input type="text" value="{if isset($pdt)}{$pdt}{/if}" rel="product_type" name="model[{$k}][product_type]" id="model_{$k}_product_type" data-content="{$k}">
                            </div>
                        {/foreach}
                    {/if}
                </div>
                <p class="poor-gray text-right">{ci_language line="Please select the main category for this model"}.</p>
        <div rel="specific_fields" id="model_{$k}_specific_fields" data-content="{$k}"></div>                             
    </div>
</div>
</div>
{/foreach}                         
{/if}

</div>

<!--Message-->
<input type="hidden" id="recommened_fields" value="{ci_language line="Recommended Fields"}" />
<input type="hidden" id="variation" value="{ci_language line="Variation"}" />
<input type="hidden" id="submit-confirm" value="{ci_language line="Do you want to save 'Models'?"}" />           
<input type="hidden" id="error-message-title" value="{ci_language line="Some value are require."}" />
<input type="hidden" id="error-message-text" value="{ci_language line="Please set a require value in model."}" />
<input type="hidden" id="confirm-delete-model-message" value="{ci_language line="Do you want to delete"}" />
<input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
<input type="hidden" id="delete-success-message" value="{ci_language line="Model was deleted"}" />
<input type="hidden" id="error-message" value="{ci_language line="Error"}" />
<input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />
<input type="hidden" id="selected_variation_data" value="{if isset($mapping_variation_data)}{$mapping_variation_data}{/if}" />
<input type="hidden" id="selected_specific_fields" value="{if isset($mapping_specific_fields)}{$mapping_specific_fields}{/if}" />
<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
{if isset($popup_more)}<input type="hidden" id="popup_more" name="popup_more" value="{$popup_more}" />{/if}
<input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />

<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">
