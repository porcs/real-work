{*if isset($carriers)*}
<form id="form-mapping" method="post" action="{$base_url}amazon/save_carriers" enctype="multipart/form-data" >
    <div id="carrier-mapping">

        <div class="row">
            <div class="col-xs-12">
                <p class="head_text p-t20 p-b20">
                    {ci_language line="Carriers Mapping"}
                </p>
            </div>
        </div>

        {*<div class="row">
        <div class="col-xs-12">
        <div class="validate blue b-Bottom p-b10">
        <div class="validateRow">
        <div class="validateCell">
        <i class="note"></i>
        </div>
        <div class="validateCell">
        <p class="pull-left">
        {ci_language line="Associated carrier, useful to display your preferate carrier on the order page and on the invoice "}
        </p>
        <i class="fa fa-remove pull-right"></i>
        </div>
        </div>
        </div>
        </div>
        </div>*}

        <div class="row">
            <div class="col-xs-12">
                <p class="poor-gray b-Bottom p-b10">
                    <i class="fa fa-arrow-right true-green m-r10"></i>
                    {ci_language line="For incoming orders"}
                </p>				
            </div>
        </div> 

        <div class="row">
            <div class="col-xs-12">
                <div class="col-sm-6 p-l0">
                    <p class="montserrat text-info text-uc">
                        {ci_language line="Amazon carrier side"}
                    </p>   
                </div>  
                <div class="col-sm-6">
                    <p class="montserrat text-info text-uc">
                        {ci_language line="Feed.biz carrier side"}
                    </p>   
                </div>            
            </div>
        </div> 


        <!--new-mapping-incoming-carrier-->
        <div id="new-mapping-incoming-carrier">
            {*if isset($mapping_carrier['incoming'])}    
            {$i = 0}                                               
            {foreach from=$mapping_carrier['incoming'] key=vk item=attr}                    
            {if $attr.selected == true*}                                        
            <div class="new-mapping-incoming-carrier row" {*if $i == 0}id="main-incoming-carrier"{/if*}>                             
                <div class="col-sm-6 m-b20">
                    <select rel="incoming-carrier-select" class="carrier-select-value search-select" data-content="{*$vk*}" id="incoming-carrier-select-{*$vk*}">
                        <option value="">{ci_language line="Choose Amazon carrier side"}</option>
                        {*if isset($amazon_carrier_incoming)}
                        {foreach from=$amazon_carrier_incoming key=ckey item=cvalue}
                        <option value="{$ckey}" {if $ckey == $attr.mapping && $attr.selected == true}selected{/if}>
                        {$cvalue}
                        </option>
                        {/foreach} 
                        {/if*}
                    </select> 
                    <i class="blockRight noBorder"></i>
                </div>
                <div class="col-sm-6">
                    <div class="form-group withIcon clearfix">
                        <select rel="incoming-carrier-value" class="carrier-value search-select" id="incoming-carrier-value-{*$vk*}" name="carrier[incoming][{*$vk*}][id_carrier]">
                            <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                            {*if isset($carriers)}
                            {foreach from=$carriers key=akey item=value}
                            <option value="{$value.id_carrier}" {if isset($value.id_carrier_ref) && $value.id_carrier_ref == $attr.id_carrier_ref && $attr.selected == true}selected{/if}>
                            {$value.name}
                            </option>
                            {/foreach} 
                            {/if*}</form>
                        </select>
                        {*if $i == 0}
                        <i class="addnewmappingcarrier cb-plus good" rel="incoming"></i>
                        <i class="removemappingcarrier cb-plus bad" rel="incoming" style="display:none"></i>
                        {else}
                        <i class="removemappingcarrier cb-plus bad" rel="incoming" style="cursor: pointer"></i>                                
                        {/if*}
                    </div>
                </div>
            </div><!--row-->

            {*$i = $i+1}  
            {/if}

            {/foreach}
            {else*}
            <!--main-incoming-carrier-->
            <div id="main-incoming-carrier" class="new-mapping-incoming-carrier row">
                <div class="col-sm-6">
                    <select rel="incoming-carrier-select" class="carrier-select-value search-select disabled">
                        <option value=""><span style="color: #f5f5f5">{ci_language line="Choose Amazon carrier side"}</span></option>
                        {*if isset($amazon_carrier_incoming)}
                            {foreach from=$amazon_carrier_incoming key=ckey item=cvalue}
                                <option value="{$ckey}">{$cvalue}</option>
                            {/foreach} 
                        {/if*}
                    </select> 
                    <i class="blockRight noBorder"></i>
                </div>
                <div class="col-sm-6">
                    <div class="form-group withIcon clearfix">
                        <select rel="incoming-carrier-value" class="carrier-value search-select disabled">
                            <option value="">{ci_language line="Choose Feed.biz carrier side"}</option>
                            {*if isset($carriers)}
                            {foreach $carriers key=akey item=value}
                            <option value="{$akey}">{$value.name}</option>
                            {/foreach} 
                            {/if*}
                        </select>
                        <i class="addnewmappingcarrier cb-plus good" rel="incoming"></i> 
                        <i class="removemappingcarrier cb-plus bad" rel="incoming" style="display:none"></i>
                    </div>
                </div>
            </div>
            {*/if*}
        </div> {*new-mapping-carrier*}     
    </div>

    <!--Message-->
    <input type="hidden" value="{*if isset($action)}{$action}{/if*}" id="actions">
    <input type="hidden" id="method" value="carriers" />

</form>
{*/if*}