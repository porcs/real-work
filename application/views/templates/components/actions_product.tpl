<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">

            <!--creation-->
            <div class="tab-pane active" id="creation">
                <div class="row">
                    <div class="col-sm-10">                        
                        <ul class="m-t15 p-l20">
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Products Wizard"}</b></p>
                                <p class="poor-gray">                                        
                                    {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.'}
                                </p>
                                <p class="poor-gray">                                        
                                    {ci_language line='This will send all the products having a profile and within the selected categories in your configuration.'}
                                </p>
                            </li>
                        </ul>
                        <div id="creat_options" class="clearfix"> 
                            <div class="clearfix m-t20">
                                <button class="btn btn-wizard wizard_btn" href="{$base_url}amazon/parameters/10/2/2" id="amazon_creation_wizard" >
                                    <i class="icon-wizard"></i> {ci_language line="Products Wizard"}
                                </button>									
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
<input type="hidden" id="method" value="action_product" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">