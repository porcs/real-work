<div class="row">
    <div class="col-xs-12">
        <p class="head_text p-tb10 m-b10">
            {ci_language line="Setting"}
        </p>
    </div>
</div>

<div class="row m-t10">
    <div class="col-sm-4 form-group">
        <p class="text-uc dark-gray montserrat m-b5 col-xs-6 col-sm-12 p-0">{ci_language line="API Key"} : </p>
        <input class="form-control col-md-12" type="text" id="merchant_id" name="api_key" {if isset($api_settings['api_key'])}value='{$api_settings['api_key']}'{/if}/>
    </div>
    <div class="col-sm-4 form-group">
        <p class="text-uc dark-gray montserrat m-b5 col-xs-6 col-sm-12 p-0">{ci_language line="Token Key"} : </p>
        <input class="form-control col-md-12" type="text" id="merchant_id" name="token_key" {if isset($api_settings['token_key'])}value='{$api_settings['token_key']}'{/if}/>
    </div>
    <div class="col-sm-4 form-group">
        <p class="text-uc dark-gray montserrat m-b5 col-xs-6 col-sm-12 p-0">{ci_language line="Secret Key"} : </p>
        <input class="form-control col-md-12" type="text" id="merchant_id" name="secret_key" {if isset($api_settings['secret_key'])}value='{$api_settings['secret_key']}'{/if}/>
    </div>
</div>

<div class="row m-t10">
    <div class="col-sm-4 form-group">
        <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="Merchant Key"} *</label>            
        <input class="form-control col-md-12" type="text" id="merchant_id" name="merchant_key" {if isset($api_settings['merchant_key'])}value='{$api_settings['merchant_key']}'{/if}/>
    </div>
    <div class="col-sm-4 form-group m-b10">
        <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="Merchant Token"}</label>            
        <input class="form-control col-md-12" type="text" id="auth_token" name="merchant_token" {if isset($api_settings['merchant_token'])}value='{$api_settings['merchant_token']}'{/if}/>
    </div>
    <div class="col-sm-4 form-group m-t25 m-b25">
        <p class="pull-left poor-gray m-t3 m-r10">{ci_language line="Active"} :</p>
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="active" id="debug" type="checkbox" value="1" {if (isset($api_settings['active']) && $api_settings['active'] == 1) || !isset($api_settings['active'])}checked="checked"{/if} data-state-on="ON" data-state-off="OFF">
            </label>
            <span class="cb-state">{ci_language line="ON"}</span>
        </div>
    </div>
</div>
<input type="hidden" id="id_method" value="set_parameter" />
<input type="hidden" id="method" value="parameter" />