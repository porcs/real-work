
{*<div {if !isset($popup)}id="content"{/if}>*}

<div class="row-fluid custom-form">  

    <div id="send-products-message">
        <div class="status row" style="display:none;">
            <div class="col-xs-12 validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i> 
                    </div>
                    <div class="validateCell">
                        <p class="pull-left"></p>
                    </div>
                </div>
            </div>              
        </div>
    </div>

    <!--Import Order Message Here-->
    <div id="actions">
        <div class="status row" style="display:none;">
            <div class="col-xs-12 validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="fa fa-spinner fa-spin"></i> 
                    </div>
                    <div class="validateCell">
                        <p class="pull-left">{ci_language line="Conecting to Amazon .."}</p>
                    </div>
                </div>
            </div>              
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Orders"}</h4>                      
                </div>
                <div class="pull-sm-right clearfix">
                    <div class="showColumns clearfix">                                
                        <div class="b-Right p-r10">
                            <p class="small light-gray pull-left m-t5">Show:&nbsp;</p> 
                            <label class="cb-checkbox">
                                <input type="checkbox" name="export-sent" id="export-sent" value="1" checked class="amazon-input" />
                                        {ci_language line="Sent"}
                            </label>
                            <label class="cb-checkbox">
                                <input type="checkbox" name="export-send" id="export-send" value="1" checked class="amazon-input" />
                                        {ci_language line="Send"}
                            </label>
                        </div>
                    </div>
                </div>          
            </div><!--headSettings-->

            <div class="row">
                <div class="col-xs-12">
                    <table id="statistics" class="responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th>{ci_language line="ID"}</th>
                                <th>{ci_language line="Order ID Ref"}.</th>
                                <th>{ci_language line="Buyer Name"}</th>
                                <th>{ci_language line="Address Name"}</th>
                                    {*<th>{ci_language line="Payment"}</th>*}
                                <th>{ci_language line="Amount"}</th>
                                <th>{ci_language line="Status"}</th>
                                <th>{ci_language line="Order Date"}</th>
                                <th>{ci_language line="Shipping Date"}</th>
                                <th>{ci_language line="Tracking No."}</th>
                                <th>{ci_language line="Invoice No."}</th>
                                <th>{ci_language line="Exported"}</th>
                            </tr>
                        </thead>
                    </table><!--table-->
                    <table class="table tableEmpty">
                        <tr>
                            <td>{ci_language line="No data available in table"}</td>
                        </tr>
                    </table>
                </div><!--col-xs-12-->
            </div><!--row--> 

        </div><!--col-xs-12-->
    </div><!--row-->   

    <!--Hidden-->
    <input type="hidden" id="id_country" value="{*if isset($id_country)}{$id_country}{/if*}" />
    <input type="hidden" id="ext" value="{*if isset($ext)}{$ext}{/if*}" />
    <input type="hidden" id="id_user" value="{*if isset($id_user)}{$id_user}{/if*}" />
    <input type="hidden" id="tag" value="{*if isset($tag)}{$tag}{/if*}" />
    <input type="hidden" id="id_marketplace" value="{*if isset($id_marketplace)}{$id_marketplace}{/if*}" />
    <input type="hidden" id="cron_send_orders" value="{*if isset($cron_send_orders)}{$cron_send_orders}{/if*}" />
    <input type="hidden" id="shop_name" value="{*if isset($shop[0]['name'])}{$shop[0]['name']}{/if*}" />
    <input type="hidden" id="pay_now" value="{ci_language line="Pay now"}" />
    <input type="hidden" id="Send" value="{ci_language line="Send"}" />
    <input type="hidden" id="Sent" value="{ci_language line="Sent"}" />
    <input type="hidden" id="Sending" value="{ci_language line="Sending"}" />
    <input type="hidden" id="Connecting" value="{ci_language line="Connecting to Amazon"}.." />
    <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
    <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data."}" />
    <input type="hidden" id="please-try" value="{ci_language line="Please try again."}" />
    <input type="hidden" id="confirm-message" value="{ci_language line="Are you sure to change Address name :"}" />

    {*include file="amazon/IncludeDataTableLanguage.tpl"*}
</div> 

{*include file="amazon/PopupStepFooter.tpl"*} 

<div class="modal fade" id="importOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Parameters"}</h1>
                    </div>
                </div>

                <div class="row">						
                    <div class="col-sm-3">
                        <p class="poor-gray">{ci_language line='Order Date Range'}</p>
                        <div class="form-group calendar firstDate">
                            <input class="form-control order-date" type="text" id="order-date-from" value="{*if isset($yesterday)}{$yesterday}{/if*}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group calendar lastDate m-t25">
                            <input class="form-control order-date" type="text" id="order-date-to" value="{*if isset($today)}{$today}{/if*}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="poor-gray">{ci_language line='Order Status'}</p>
                        <div class="form-group ">
                            <select id="order-status" class="search-select" data-placeholder="Choose">
                                <option value="All">{ci_language line='Retrieve all pending orders'}</option>
                                <option value="Pending">{ci_language line='Pending - This order is pending in the market place'}</option>
                                <option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order is waiting to be shipped'}</option>
                                <option value="PartiallyShipped">{ci_language line='Partially shipped - This order was partially shipped'}</option>
                                <option value="Shipped">{ci_language line='Shipped - This order was shipped'}</option>
                                <option value="Canceled">{ci_language line='Cancelled - This order was cancelled'}</option>
                            </select>								
                        </div>
                    </div>
                </div>
                <!-- <div class="row">
                        <div class="col-xs-12">
                                <ul class="selectRange">
                                        <li class="active today">Today</li>
                                        <li class="yesterday">Yesterday</li>
                                        <li class="last_7Days">last 7 days</li>
                                        <li class="last_30Days">last 30 days</li>
                                        <li class="thisMonth">this month</li>
                                        <li class="lastMonth">last month</li>
                                </ul>
                        </div>
                </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-save" data-dismiss="modal" id="import-orders">{ci_language line="Start importing orders"}</button>
            </div>
        </div>
    </div>
</div>

<div id="order_error_template" style="display:none">
    <div class="headSettings clearfix m-t0 p-t0 b-Top-None">
        <h4 class="headSettings_head pull-left">{ci_language line="Error Detail:"}</h4>
    </div>
    <table width="100%">
        <thead>
            <tr>
                <th width="15%">{ci_language line="Batch id"}</th>
                <td width="15%">{ci_language line="Message date"}</th>
                <td width="70%">{ci_language line="Message"}</th>
            </tr>
        </thead>
        <tbody>
            <tr class="order_error_template_body" style="display:none">
                <td class="batch_id"></td>
                <td class="date_add"></td>
                <td class="message"></td>
            </tr>
        </tbody>
    </table>
</div>
{*</div> *}

<!--page specific plugin scripts-->
{*<script type="text/javascript" src="{$base_url}assets/js/FeedBiz/amazon/orders.js"></script> *}