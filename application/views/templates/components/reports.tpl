<!--Product-->
<div class="row">
    <div class="col-xs-12">
        <div class="headSettings clearfix b-Top-None">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Feed report"}</h4>                      
            </div>
            <div class="pull-sm-right clearfix">
                <div class="showColumns clearfix"></div>
            </div>          
        </div><!--headSettings-->

        <div class="row">
            <div class="col-xs-12">
                <table id="product_reports" class="responsive-table hover">
                    <thead class="table-head">
                        <tr>
                            <th class="center">{ci_language line="Batch ID"}</th>
                            <th class="center">{ci_language line="SKU"}</th>
                            <th class="center">{ci_language line="Code"}</th>
                            <th class="center">{ci_language line="Message"}</th>
                            <th class="center">{ci_language line="Description"}</th>
                            <th class="center">{ci_language line="Date"}</th>
                        </tr>
                    </thead>              
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                                {*<th></th>*}
                        </tr>
                    </tfoot>  
                </table>
            </div> <!--col-xs-12-->
        </div><!--row-->

    </div><!--col-xs-12-->
</div><!--row--> 

<!--Hidden-->
<input type="hidden" id="emptyError" value="{ci_language line="No Error Message"}" />
<input type="hidden" id="success_log" value="{ci_language line='Success'}" />
<input type="hidden" id="success_order" value="{ci_language line='success orders'}" />
<input type="hidden" id="form_log" value="{ci_language line='form'} : " />
<input type="hidden" id="result_log" value="{ci_language line="Result"} : " />
<input type="hidden" id="skipped_order" value="{ci_language line="skipped orders"}" />
<input type="hidden" id="items_to_send" value="{ci_language line="items to send"}" />
<input type="hidden" id="skipped_items" value="{ci_language line="skipped items"}" />
<input type="hidden" id="import_orders" value="{ci_language line="import orders"}" />
<input type="hidden" id="orders_to_send" value="{ci_language line="orders to send"}" />
<input type="hidden" id="success" value="{ci_language line="success"}" />
<input type="hidden" id="skipped" value="{ci_language line="skipped"}" />
<input type="hidden" id="method" value="log" />
<script type="text/javascript" src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/includescript.js"></script>