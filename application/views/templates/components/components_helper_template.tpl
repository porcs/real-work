{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
{include file="components/popup_step_header.tpl"}
{if isset($css_data)}
    {foreach from = $css_data item = css}
    <link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/{str_replace(' ', '_', strtolower($marketplace_name))}/{str_replace(' ', '_', strtolower($js))}.css" />
    {/foreach}
{/if}
<form id="form-api-setting" method="post" {if !empty($action)}action="{$action}"{/if} enctype="multipart/form-data" autocomplete="off" >
<div class="row">
    <div class="container-fluid">{include file="components/helperHTML.tpl"}</div>       
</div>
<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
<input type="hidden" id="field_required" name="field_required" value="{ci_language line="This field is required."}" />                
<input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
<input type="hidden" id="class_call" name="class_call" value="{if isset($class_call)}{$class_call}{/if}" />
<input type="hidden" id="function_call" name="function_call" value="{if isset($function_call)}{$function_call}{/if}" />
<input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
<input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />  
<input type="hidden" id="method_value" name="method_value" value="{if isset($method)}{$method}{/if}" />  
<input type="hidden" id="processing" value="Processing...">
<input type="hidden" id="next_page" value="{if isset($next)}{$next}{/if}">
{if isset($hidden_data)}
    {foreach from = $hidden_data key = key item = hidden}
    <input type="hidden" id="{$key}" name="{$key}" value="{$hidden}">
    {/foreach}
{/if}
</form>
{if isset($js_data)}
    {foreach from = $js_data item = js}
    <script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/{str_replace(' ', '_', strtolower($marketplace_name))}/{str_replace(' ', '_', strtolower($js))}.js"></script>
    {/foreach}
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/{str_replace(' ', '_', strtolower($marketplace_name))}/{str_replace(' ', '_', strtolower($function_call))}.js"></script>
{include file="footer.tpl"}

