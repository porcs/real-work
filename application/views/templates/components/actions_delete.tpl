<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">

            <!--delete-->
            <div class="tab-pane active" id="delete">
                <div class="row">
                    <div class="col-sm-12">                        
                        <ul class="p-l20">
                            <li>
                                <p class="poor-gray">                                        
                                    {ci_language line='This will delete all the products having a profile and within the selected categories in your module configuration and selected for deletion.'}
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="status" style="display: none">
                    <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                </div>
                <div id="delete_options">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="head_text montserrat black p-t20 b-None">
                                {ci_language line="Parameters"}
                            </p>
                        </div>
                    </div>
                    <div class="row custom-form">
                        {*<div class="col-sm-3"> 
                        <label class="cb-radio">
                        <input name="delete-parameters" type="radio" id="disabled" value="{$disabled}" checked="checked">
                        <p class="poor-gray">{ci_language line="Delete disabled products"}</p>
                        <p class="m-l25 poor-gray">
                        {ci_language line="This will delete only disabled offers on Amazon."}
                        </p>
                        </label>
                        </div>*}
                        <div class="col-sm-3">
                            <label class="cb-radio">
                                <input name="delete-parameters" type="radio" id="out_of_stock" value="{*$out_of_stock*}">
                                <p class="poor-gray">{ci_language line="Delete out of stock products"}</p>
                                <p class="m-l25 poor-gray">
                                    {ci_language line="This will delete only out of stock products on Amazon."}
                                </p>
                            </label>
                        </div>
                        <div class="col-sm-3">
                            <label class="cb-radio">
                                <input name="delete-parameters" type="radio" id="all_sent" value="{*$all_sent*}">
                                <p class="poor-gray">{ci_language line="Delete all sent products"}</p>
                                <p class="m-l25 poor-gray">
                                    {ci_language line="This will delete all sent products on Amazon."}
                                </p>
                            </label>
                        </div>
                        <div class="col-sm-3 col-lg-3 sm-right">
                            <button class="btn btn-save m-t25" id="delete-products" type="button" value="delete">
                                {ci_language line="Start Delete"}
                            </button>
                            <div class="status" style="display: none">
                                <p>{ci_language line="Starting delete"} ..</p>
                            </div>
                            <div class="error" style="display: none">
                                <p class="m-t0">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to send relations?"}" id="send_relations_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to send images?"}" id="send_images_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to get orders?"}" id="get-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update order shipments?"}" id="update-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to delete product?"}" id="delete-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
<input type="hidden" id="method" value="action_delete" />
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 