<form id="form-mapping" method="post" action="{$base_url}amazon/save_mappings" enctype="multipart/form-data" >

    {*if !isset($popup) && isset($no_mapping_info)}
    <div class="m-t10 b-Bottom">
    {$no_mapping_info}
    </div>
    {/if*}

    <div id="error-color" class="row" style="display: none;">
        <div class="col-xs-12 m-t10">
            <div class="b-Bottom">
                <div class="validate pink">
                    <div class="validateRow">
                        <div class="validateCell"><i class="note"></i></div>
                        <div class="validateCell">
                            <p><span class="bold">{ci_language line="Warning"}!</span> {ci_language line="Please map all attribute color"}.</p>
                            <i class="fa fa-remove pull-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Attribute--> 

    {*if isset($mappings) && !empty($mappings)*}
    <div id="attribute-mapping"  class="m-b30"> 

        {*foreach $mappings as $key_mapping => $mapping}  

        <!--Product Type-->
        {foreach $mapping as $key_product_type => $product_type*}

        <!--Universe-->
        <div class="row">
            <div class="col-xs-12">
                <div class="head_text clearfix p-t15">
                    <p class="pull-left">
                        {*if isset($mappings[$key_mapping][$key_product_type]['universer_trans'])} 
                        {$mappings[$key_mapping][$key_product_type]['universer_trans']} 
                        {/if*}
                        universer_trans >  product_type_trans
                        {*if isset($mappings[$key_mapping][$key_product_type]['product_type_trans'])} 
                        {$mappings[$key_mapping][$key_product_type]['product_type_trans']} 
                        {/if*}
                    </p>
                    {*if !isset($no_translate) || $no_translate == false}
                    <p class="pull-right">
                    {$key_mapping} > {$key_product_type}
                    </p>
                    {/if*}
                </div>
            </div>
        </div>

        {*if isset($product_type['fields']) && !empty($product_type['fields'])}   

        {foreach $product_type['fields'] as $key_attribute => $attribute}

        {if is_array($attribute)}                                
            
        {if isset($attribute['valid_value'])}
        {$valid = $attribute['valid_value']}
        {else}
        {$valid = ''}
        {/if}

        {if $key_attribute !== "valid_value"}
        {if isset($attribute['none_valid_value']) && !empty($attribute['none_valid_value'])*}
        <!--Product Type-->
        <div class="row">
            <div class="col-xs-12">
                <div class="head_text clearfix p-t10 p-b10">
                    <p class="pull-left m-0">{*$attribute['none_valid_value']['name']*}name</p>
                    <p class="pull-right m-0">{*$attribute['none_valid_value']['field']*}field</p>
                </div>
            </div>
        </div>
        <!--Attribute-->
        <div class="row none_valid_value">
            <div class="col-xs-12">
                <div class="p-t15 p-b15 b-Bottom">
                    <!-- Head -->
                    <div class="text-center">
                        <p class="poor-gray">
                            {ci_language line="This attribute provide the free text."} 
                            {ci_language line="You can"} 
                            <input type="hidden" id="attr-link-1" value="{ci_language line="mapping a new value"}" />
                            <input type="hidden" id="attr-link-2" value="{ci_language line="use this value"}" />
                            <a class='text-small link attr-link' style="cursor: pointer" rel='1'>{ci_language line="mapping a new value"}</a>
                            {ci_language line="for"} {*$attribute['none_valid_value']['name']}/{$attribute['none_valid_value']['field']*}.
                        </p>
                    </div>
                    <!-- Body -->
                    {*$none_valid_value = array()}
                    {$attribute = $attribute['none_valid_value']}
                    {if isset($attribute['attribute'])}
                    {foreach $attribute['attribute'] as $ak => $value}
                    {if isset($attribute['is_color']) && $attribute['is_color']}
                    {$none_valid_value[$ak] = $value['value']['color']}
                    {else}
                    {if isset($value['value'])}
                    {$none_valid_value[$ak] = $value['value']}
                    {/if}
                    {/if}
                    <div class="mapping-valid-value" style="display:none;">
                    {include file="amazon/Mappings-field.tpl"}
                    </div>
                    {/foreach}
                    {/if}                                                       
                    {if isset($none_valid_value) && !empty($none_valid_value)}
                    <div class="mapping-valid-value text-center">
                    <p class="poor-gray">{implode(', ', $none_valid_value)}</p>
                    </div>
                    {/if*}
                </div>
            </div>
        </div>
        {*else*}
        <!--Product Type-->
        <div class="row">
            <div class="col-xs-12">
                <div class="head_text clearfix p-t10 p-b10">
                    <p class="pull-left m-0">{*$attribute['name']*}name</p>
                    <p class="pull-right m-0">{*$attribute['field']*}field</p>
                </div>
            </div>
        </div>
        <!--Attribute-->
        <div class="row">
            <div class="col-xs-12">
                <div class="p-t15 b-Bottom">
                    {*if isset($attribute['attribute'])}
                    {$mapping_valid = array()}
                    {foreach $attribute['attribute'] as $ak => $value}
                    {if isset($value['valid_value']) && $value['valid_value'] == true}
                    {if isset($value['value']) && !empty($value['value'])}
                    {$mapping_valid[$key_attribute][$ak] = $value['value']}      
                    {/if}                                                                                                          
                    {else}
                    {include file="amazon/Mappings-field.tpl"}   
                    {/if}
                    {/foreach}
                    {if isset($mapping_valid[$key_attribute]) && !empty($mapping_valid[$key_attribute])}
                    <div class="text-center p-b10">
                    <div class="text-center">
                    <p class="poor-gray">
                    {ci_language line="The"}  
                    {$attribute['name']}/{$attribute['field']}  
                    {ci_language line="value has mapping automatically"} :
                    </p>
                    </div>
                    <div class="text-center">
                    <p class="poor-gray">{implode(', ',$mapping_valid[$key_attribute])}</p>
                    </div>
                    </div>
                    {/if}
                    {/if*}
                    <div class="row">
                        <div class="col-sm-6 m-b20">
                            <select class="search-select">
                                {*if isset($attribute['is_color']) && $attribute['is_color'] == true}
                                    <option value="{$value.id_attribute}">{$value.value.color}</option>
                                {else}
                                    {if isset($value.value)}
                                        <option value="{$value.id_attribute}">{$value.value}</option>
                                    {/if}
                                {/if*}
                            </select> 
                            <i class="blockRight noBorder"></i>
                        </div>
                        <div class="col-sm-6">
                            {*if isset($attribute['is_color']) && $attribute['is_color'] == true*}
                                <div class="attribute-color">
                                    {*if isset($valid) && !empty($valid)*}

                                        <select rel="attribute-color" class="colorpicker" name="{*$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']*}][{*$key_mapping*}][{*$key_product_type*}][{*$key_attribute*}]">
                                            <option value="None" title="text">--</option>
                                            {*foreach $valid as $key_valid => $valid_value}
                                                {$selected = false}
                                                {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])}
                                                    {$mapping = $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}
                                                    {if $mapping == $valid_value['value']}
                                                        {$selected = true}
                                                    {/if}                                                                            
                                                {else}
                                                    {if isset($value.value.colorMap) && isset($valid_value['desc']) && $value.value.colorMap == $valid_value['desc'] }
                                                        {$selected = true}
                                                    {/if}
                                                {/if}
                                                <option value="{if isset($valid_value['value'])}{$valid_value['value']}{/if}" title="text" {if $selected == true} selected {/if}>
                                                    {if isset($valid_value['desc'])}
                                                        {$valid_value['desc']}
                                                    {/if}
                                                </option>
                                            {/foreach}
                                            {if isset($mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                                                {$custom_value = explode(', ', $mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                                                {foreach $custom_value as $cv}
                                                    <option value="{$cv}" title="text" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $cv == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if}>{$cv}</option>
                                                {/foreach}
                                            {/if*}  
                                        </select>                    

                                    {*else*}

                                        <input type="text" class="form-control" name="{*$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]*}" {*if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])} value="{$value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}" {/if*}/>

                                    {*/if*}
                                </div>
                            {*else*}     

                                {*if isset($valid) && !empty($valid)*}
                                    <select class="search-select" name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]">
                                        <option value="">--</option>
                                        {*foreach $valid as $key_valid => $valid_value*}
                                            <option value="{*$valid_value['value']*}" {*if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $valid_value['value'] == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if*}>{*$valid_value['desc']*}</option>
                                        {*/foreach*}
                                        {*if isset($mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])*}
                                            <optgroup label="{ci_language line="Custom Value"}">
                                                {*$custom_value = explode(', ', $mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                                                {foreach $custom_value as $cv}
                                                    <option value="{$cv}" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $cv == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if}>{$cv}</option>
                                                {/foreach*}
                                            </optgroup>
                                        {*/if*}  

                                    </select>
                                {*else*}
                                    <input type="text" class="form-control" {*name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])} value="{$value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}" {/if*}/>
                                {*/if*}
                            {*/if*}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {*/if}
        {/if}  

        {/if} 

        {/foreach}

        {else*}

        <!--no field to map-->
        <div class="row">
            <div class="col-xs-12">
                <div class="validate">
                    <div class="validateRow">                                   
                        <div class="validateCell">
                            <p class="poor-gray">{ci_language line="No mappings to display"}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>                       

        {*/if}              

        {/foreach}

        {/foreach*} 

    </div>    
    {*/if*}      

    <!--Message-->
    <input type="hidden" id="submit-confirm" value="{ci_language line="Do you want to save 'Mappings'?"}" />
    <input type="hidden" id="error-message" value="{ci_language line="Mapping colors are require"}!" />
    <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
    <input type="hidden" id="save-unsuccess-message" value="{ci_language line="Please map all attribute color."}" />
    <input type="hidden" id="Please-select-color" value="{ci_language line="Please select color."}" />

</form>
<input type="hidden" value="{*if isset($action)}{$action}{/if*}" id="actions"> 
<input type="hidden" id="method" value="mappings" />