{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
{include file="components/popup_step_header.tpl"}
<form {if isset($form_id)}id="{$form_id}"{/if} {if isset($form_name)}name="{$form_name}"{/if} {if isset($save_path)}action="{$save_path}"{/if} method="post" enctype="multipart/form-data" autocomplete="off" >
<div class="row {if isset($popup)}popup_action{/if}">
    {if isset($containers)}
        {foreach $containers as $container}
        <div class="row m-b15">
            <div class="col-xs-12">
        	    {if isset($container['title'])}
        		<div class="col-xs-12">
        		    <p class="head_text p-tb10 m-b10 b-Top">
        			{$container['title']}
        		    </p>
        		</div>
        	    {/if}
                {if isset($container['info'])}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="validate blue">
                            <div class="validateRow">
                                <div class="validateCell">
                                    <i class="help"></i>
                                </div>
                                <div class="validateCell">
                                    <p class="pull-left">
                                       {$container['info']}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/if}
        	    <div class="container-fluid">
            		<div class="custom-form">
            		    {include file="helpers/form_group.tpl" data=$container}
            		</div>
                </div>
            </div>
	    </div>
        {/foreach}
    {/if}

    
    {if empty($not_menu) || $not_menu == 'TRUE' }
    <div class="col-md-12 col-xs-12">
        <div class="b-Top p-t20">
            <div class="inline pull-left">
                {if isset($popup) && !isset($not_previous)}
                    <button class="pull-left link p-size m-tr10" id="back" type="button">
                        {ci_language line="Previous"}
                    </button>
                {/if}
            </div>
            <div class="inline pull-right">  
                {if isset($not_continue) && $not_continue == true && !isset($popup)}
                    <button class="btn btn-save" id="save_data" type="submit" >
                        {ci_language line="Save"}
                    </button>
                {else}
                    {if !isset($popup)}
                        <button class="pull-left link p-size m-tr10" id="save_data" type="submit" >
                            {ci_language line="Save"}
                        </button>
                    {/if}                    
                    <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                        {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                    </button>                        
                {/if} 
            </div>
        </div>		
    </div>
    {/if}
</div>

<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
<input type="hidden" id="field_required" name="field_required" value="{ci_language line="This field is required."}" />                
<input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
<input type="hidden" id="class_call" name="class_call" value="{if isset($class_call)}{$class_call}{/if}" />
<input type="hidden" id="function_call" name="function_call" value="{if isset($function_call)}{$function_call}{/if}" />
<input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
<input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />  
<input type="hidden" id="method_value" name="method_value" value="{if isset($method)}{$method}{/if}" />  
<input type="hidden" id="processing" value="Processing...">
<input type="hidden" id="next_page" value="{if isset($next)}{$next}{/if}">
{if isset($hidden_data)}
    {foreach from = $hidden_data key = key item = hidden}
    <input type="hidden" name="{$key}" class="{$key}" value="{$hidden}">
    {/foreach}
{/if}
</form>

{include file="footer.tpl"}

