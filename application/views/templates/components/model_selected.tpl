    <!-- Group? -->
    <div class="variation">
        <!-- Variation -->
        <div class="variation_fields_options" rel='variation'>
            <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                {ci_language line="Variation"}
            </p>
            <div class="row m-b10 m-t20">
                {if isset($variation_theme)}
                    {foreach $variation_theme as $key_vat => $vat}
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="col-md-2 text-capitalize text-right p-t10">{$key_vat}{if $vat['required']==1}<b class="black strong-text">*</b>{/if}</label>
                                    <div class="col-md-3">
                                        {assign var=variation_check_selected value=FALSE}
                                        {if isset($data.variation_theme[$key_vat]['hidden'])}
                                            {if $data.variation_theme[$key_vat]['hidden']|@count > 1}
                                                {assign var=variation_check_selected value=TRUE}
                                            {else}
                                                {assign var=variation_check_selected value=FALSE}
                                            {/if}
                                        {/if}
                                        <select rel="{$key_vat}" name="{if !$variation_check_selected}model[{$k}][{$key_vat}]{/if}" data-name="model[{$k}][{$key_vat}]" id="model_{$k}_{$key_vat}" data-content="{$k}" class="search-select" data-placeholder="Choose" {if $vat['required']==1}required="required"{/if}>
                                            <option value="">{ci_language line="Choose a Variation option"}</option>
                                            <option value="default_value" {if $variation_check_selected}selected="selected"{/if}>{ci_language line="Default value"}</option>

                                            {if isset($attributes) && !empty($attributes)}
                                                <optgroup label="{ci_language line='Attribute'}">
                                                    {foreach $attributes as $key_atb => $atb}
                                                        <option value="Attribute_{$key_atb}" {if isset($data.variation_theme[$key_vat]['value']) and !$variation_check_selected}{if $data.variation_theme[$key_vat]['value'] == $key_atb && $data.variation_theme[$key_vat]['attr_type'] == 'attribute'}selected="selected"{/if}{/if}>{$atb}</option>
                                                    {/foreach}
                                                </optgroup>
                                            {/if}
                                            {if isset($features) && !empty($features)}
                                                <optgroup label="{ci_language line='Feature'}">
                                                    {foreach $features as $key_ft => $ft}
                                                        <option value="Feature_{$key_ft}" {if isset($data.variation_theme[$key_vat]['value']) and !$variation_check_selected}{if $data.variation_theme[$key_vat]['value'] == $key_atb && $data.variation_theme[$key_vat]['attr_type'] == 'feature'}selected="selected"{/if}{/if}>{$ft}</option>
                                                    {/foreach}
                                                </optgroup>
                                            {/if}
                                        </select>
                                        <input rel="{$key_vat}" type="hidden" {if isset($data.variation_theme[$key_vat]['hidden'])}value="{implode("|",$data.variation_theme[$key_vat]['hidden'])}"{/if} />
                                    </div>
                                    <div class="col-md-3">
                                        <input rel="{$key_vat}" name="{if !$variation_check_selected}model[{$k}][{$key_vat}]{/if}" data-name="model[{$k}][{$key_vat}]" id="model_{$k}_{$key_vat}" name="{if !$variation_check_selected}model[{$k}][{$key_vat}]{/if}" type="text" class="form-control {if !$variation_check_selected}hidden{/if}" {if $variation_check_selected}{if isset($data.variation_theme[$key_vat]['value'])}value="{$data.variation_theme[$key_vat]['value']}"{/if}{/if} />
                                    </div>
                                    <div class="col-md-3">
                                        <label class="pull-left p-t10 cb-checkbox">
                                            <input rel="{$key_vat}" type="checkbox" {if isset($data.variation_theme[$key_vat]['grouping'])}checked="checked"{/if} value="{$vat['tag']}" />
                                            {ci_language line="Group by"} {$key_vat|ucfirst} ?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>
    </div>
            
    <!-- Specific -->
    <div class="specific_fields_options" rel="specific">
        <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
            {ci_language line="Specific Fields"}
        </p>
        <div class="row m-b10 m-t20">
            {if isset($specific_fields)}
                {foreach $specific_fields as $key_scf => $scf}
                    <div class="col-xs-12{if $key_scf=="shipping_weight" or $key_scf=="shipping_label"} hidden{/if}">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="col-md-2 text-capitalize text-right p-t10">{$key_scf}{if $scf['required']==1}<b class="black strong-text">*</b>{/if}</label>
                                <div class="col-md-3">
                                    {assign var=specific_check_selected_hidden value=FALSE}
                                    {if isset($data.specific_fields[$key_scf]['hidden'])}
                                        {if $data.specific_fields[$key_scf]['hidden']|@count > 1}
                                            {assign var=specific_check_selected_hidden value=TRUE}
                                        {else}
                                            {assign var=specific_check_selected_hidden value=FALSE}
                                        {/if}
                                    {/if}

                                    {assign var=yesno_option value=FALSE}
                                    {if isset($scf['values'])}
                                        {if COUNT($scf['values'])==2 and $scf['values'][0] == 'TRUE' and $scf['values'][1] == 'FALSE'}
                                            {assign var=yesno_option value=TRUE}
                                        {/if}
                                    {/if}

                                    {if $yesno_option}
                                        <label class="cb-radio">
                                            <input type="radio" rel="{$key_scf}" {if !$specific_check_selected_hidden}name="model[{$k}][{$key_scf}]"{/if} data-name="model[{$k}][{$key_scf}]" disabled="disabled" value="1" {if $scf['required']==1}required="required"{/if} {if isset($data.specific_fields[$key_scf]['value'])}{if $data.specific_fields[$key_scf]['value']=="1"}checked="checked"{/if}{/if} />
                                            {ci_language line="Yes"}
                                        </label>
                                        <label class="cb-radio">
                                            <input type="radio" rel="{$key_scf}" {if !$specific_check_selected_hidden}name="model[{$k}][{$key_scf}]"{/if} data-name="model[{$k}][{$key_scf}]" disabled="disabled" value="0" {if $scf['required']==1}required="required"{/if} {if isset($data.specific_fields[$key_scf]['value'])}{if $data.specific_fields[$key_scf]['value']=="0"}checked="checked"{/if}{/if} />
                                            {ci_language line="No"}
                                        </label>
                                    {else}
                                        <select rel="{$key_scf}" name="{if !$specific_check_selected_hidden}model[{$k}][{$key_scf}]{/if}" data-name="model[{$k}][{$key_scf}]" id="model_{$k}_{$key_scf}" data-content="{$k}" class="search-select"  data-placeholder="Choose" {if $scf['required']==1}required="required"{/if}>
                                            <option value="">{ci_language line="Choose a Specific Field option"}</option>
                                            <option value="default_value" {if $specific_check_selected_hidden}selected="selected"{/if}>{ci_language line="Default value"}</option>
                                            {if isset($custom_label) && !empty($custom_label)}
                                                {foreach $custom_label as $key_cl => $cl}
                                                    <option value="{$cl}" {if isset($data.specific_fields[$key_scf]['value']) and !$specific_check_selected_hidden}{if $data.specific_fields[$key_scf]['value'] == $cl}selected="selected"{/if}{/if}>{$cl}</option>
                                                {/foreach}
                                            {/if}
                                            {if isset($attributes) && !empty($attributes)}
                                                <optgroup label="{ci_language line='Attribute'}">
                                                    {foreach $attributes as $key_atb => $atb}
                                                        <option value="Attribute_{$key_atb}" {if isset($data.specific_fields[$key_scf]['value']) and !$specific_check_selected_hidden}{if $data.specific_fields[$key_scf]['value'] == $key_atb && isset($data.specific_fields[$key_scf]['attr_type']) && $data.specific_fields[$key_scf]['attr_type'] == 'attribute'}selected="selected"{/if}{/if}>{$atb}</option>
                                                    {/foreach}
                                                </optgroup>
                                            {/if}
                                            {if isset($features) && !empty($features)}
                                                <optgroup label="{ci_language line='Feature'}">
                                                    {foreach $features as $key_ft => $ft}
                                                        <option value="Feature_{$key_ft}" {if isset($data.specific_fields[$key_scf]['value']) and !$specific_check_selected_hidden}{if $data.specific_fields[$key_scf]['value'] == $key_ft && isset($data.specific_fields[$key_scf]['attr_type']) && $data.specific_fields[$key_scf]['attr_type'] == 'feature'}selected="selected"{/if}{/if}>{$ft}</option>
                                                    {/foreach}
                                                </optgroup>
                                            {/if}
                                        </select>
                                    {/if}
                                </div>
                                <div class="col-md-3">
                                    {if !$yesno_option}
                                        {if isset($scf['values'])}
                                            <select rel="{$key_scf}" {if $specific_check_selected_hidden}name="model[{$k}][{$key_scf}]"{/if} data-name="model[{$k}][{$key_scf}]" id="model_{$k}_{$key_scf}" data-placeholder="Choose" {if $scf['required']==1}required="required"{/if} class="search-select{if !$specific_check_selected_hidden} hidden{/if}">
                                                {foreach $scf['values'] as $key_scf_val => $scf_val}
                                                    <option value="{$scf_val}" {if isset($data.specific_fields[$key_scf]['value'])}{if $data.specific_fields[$key_scf]['value'] == $scf_val}selected="selected"{/if}{/if}>{$scf_val}</option>
                                                {/foreach}
                                            </select>
                                        {else}
                                            <input rel="{$key_scf}" {if $specific_check_selected_hidden}name="model[{$k}][{$key_scf}]"{/if} data-name="model[{$k}][{$key_scf}]" id="model_{$k}_{$key_scf}" type="text" {if isset($data.specific_fields[$key_scf]['value'])}{if $specific_check_selected_hidden}value="{$data.specific_fields[$key_scf]['value']}"{/if}{/if} class="form-control {if !$specific_check_selected_hidden}hidden{/if}" />
                                        {/if}
                                    {/if}
                                    <input rel="{$key_scf}" type="hidden" {if isset($data.specific_fields[$key_scf]['hidden'])}value="{implode("|",$data.specific_fields[$key_scf]['hidden'])}"{/if} />
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
    </div>
        
    <!-- Custom label -->
    <div class="custom_label_options" rel="custom">
        <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
            {ci_language line="Custom label"}
        </p>
        <div class="row m-b10 m-t20">

            {if isset($custom_attributes)}
                {foreach $custom_attributes as $key_cab => $cab}
                    <div class="col-xs-12">
                        <div class="form-group m-t10">
                            <div class="col-md-12">
                                <label class="col-md-2 text-capitalize text-right p-t10">{$key_cab}{if $cab['required']==1}<b class="black strong-text">*</b>{/if}</label>
                                <div class="col-md-3">
                                    {assign var=custom_check_selected_hidden value=FALSE}
                                    {if isset($data.custom_attributes[$key_cab]['hidden'])}
                                        {if $data.custom_attributes[$key_cab]['hidden']|@count > 1}
                                            {assign var=custom_check_selected_hidden value=TRUE}
                                        {else}
                                            {assign var=custom_check_selected_hidden value=FALSE}
                                        {/if}
                                    {/if}                                        
                                    <select rel="{$key_cab}" name="{if !$custom_check_selected_hidden}model[{$k}][{$key_cab}]{/if}" data-name="model[{$k}][{$key_cab}]" id="model_{$k}_{$key_cab}" data-content="{$k}" class="search-select"  data-placeholder="Choose" {if $cab['required']==1}required="required"{/if}>
                                        <option value="">{ci_language line="Choose a Specific Field option"}</option>
                                        <option value="default_value" {if $custom_check_selected_hidden}selected="selected"{/if}>{ci_language line="Default value"}</option>
                                        {if isset($custom_label)}
                                            {foreach $custom_label as $key_cl => $cl}
                                                <option value="{$cl}" {if isset($data.custom_attributes[$key_cab]['value']) and !$custom_check_selected_hidden}{if $data.custom_attributes[$key_cab]['value'] == $cl}selected="selected"{/if}{/if}>{$cl}</option>
                                            {/foreach}
                                        {/if}
                                        {if isset($attributes)}
                                            <optgroup label="{ci_language line='Attribute'}">
                                                {foreach $attributes as $key_atb => $atb}
                                                    <option value="Attribute_{$key_atb}" {if isset($data.custom_attributes[$key_cab]['value']) and !$custom_check_selected_hidden}{if $data.custom_attributes[$key_cab]['value'] == $key_atb && $data.custom_attributes[$key_cab]['attr_type'] == 'attribute'}selected="selected"{/if}{/if}>{$atb}</option>
                                                {/foreach}
                                            </optgroup>
                                        {/if}
                                        {if isset($features) && !empty($features)}
                                            <optgroup label="{ci_language line='Feature'}">
                                                {foreach $features as $key_ft => $ft}
                                                    <option value="Feature_{$key_ft}" {if isset($data.custom_attributes[$key_cab]['value']) and !$custom_check_selected_hidden}{if $data.custom_attributes[$key_cab]['value'] == $key_ft  && $data.custom_attributes[$key_cab]['attr_type'] == 'feature'}selected="selected"{/if}{/if}>{$ft}</option>
                                                {/foreach}
                                            </optgroup>
                                        {/if}
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input rel="{$key_cab}" {if $custom_check_selected_hidden}name="model[{$k}][{$key_cab}]"{/if} data-name="model[{$k}][{$key_cab}]" id="model_{$k}_{$key_cab}" type="text" {if isset($data.custom_attributes[$key_cab]['value'])}{if $custom_check_selected_hidden}value="{$data.custom_attributes[$key_cab]['value']}"{/if}{/if} class="form-control {if !$custom_check_selected_hidden}hidden{/if}" />
                                    <input rel="{$key_cab}" type="hidden" {if isset($data.custom_attributes[$key_cab]['hidden'])}value="{implode("|",$data.custom_attributes[$key_cab]['hidden'])}"{/if} />
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
    </div> 