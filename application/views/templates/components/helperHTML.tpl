{function add_attributes key=0}
    {if is_array($attributes_data)}
        {foreach from = $attributes_data key = attribute_key item = attribute_data}
            {$attribute_key|cat:'="'|cat:$attribute_data|cat:'"'}
        {/foreach}
    {else}
        {$attributes_data}
    {/if}
{/function}

{function add_fields}
    <{$fields[0]} {if !empty($fields[1])}{add_attributes attributes_data=$fields[1]}{/if}>
        {for $i = 2; $i < count($fields); $i++}
            {if is_array($fields[$i])}
                {add_fields fields=$fields[$i]}
            {else}
                {$fields[$i]}
            {/if}
        {/for}
    </{$fields[0]}>
{/function}

{if !empty($fields_templates)}
    {foreach from = $fields_templates item = fields}
        {add_fields fields=$fields}
    {/foreach}
{/if}