<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">

            <!--Synchronization-->
            <div id="synchronize" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-10">

                        <ul class="m-t15 p-l20">
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Offers Wizard"}</b></p>
                                <p class="poor-gray">
                                    {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.'}
                                </p>
                            </li>
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Update offers"}</b></p>
                                <p class="poor-gray">
                                    {ci_language line='This will update Amazon depending your offers, within the selected categories in your configuration.'}
                                </p>
                            </li>
                        </ul>                            
                        <div id="sync_options">
                            {if isset($message_code_inner.sync) && (isset($mode_default) && $mode_default > 1)}
                                <div class="m-t20">
                                    {$message_code_inner.sync}
                                </div>
                            {/if}
                            <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />

                            <div class="clearfix m-t20 xs-center">
                                <div class="pull-sm-left">
                                    <button class="btn btn-wizard wizard_btn m-r5" type="button" href="{$base_url}amazon/parameters/10/2/2" id="amazon_synchronize_wizard">
                                        <i class="icon-wizard"></i> {ci_language line="Offers Wizard"}
                                    </button>
                                </div>

                                <div class="pull-sm-right">
                                    <button class="btn btn-amazon m-r5" href="{$base_url}amazon/parameters/10/2/2" id="send-offers" type="button" value="synchronize">
                                        <i class="icon-amazon"></i> {ci_language line="Update offers"}
                                    </button>                                        
                                    <div class="status" style="display: none">
                                        <p>{ci_language line="Starting update"} ..</p>
                                    </div>
                                    <div class="error" style="display: none">
                                        <p class="m-t0">
                                            <i class="fa fa-exclamation-circle"></i>
                                            <span></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
<input type="hidden" id="method" value="actions_offer" />
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">