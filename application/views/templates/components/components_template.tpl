{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
{include file="components/popup_step_header.tpl"}
<form id="form-api-setting" method="post" enctype="multipart/form-data" autocomplete="off" >
<div class="row {if isset($popup)}popup_action{/if}">
    <div class="container-fluid">{include file="components/{str_replace(' ', '_', strtolower($function_call))}.tpl"}</div>
    {if empty($not_menu) || $not_menu == 'TRUE' }
    <div class="col-md-12 col-xs-12">
        <div class="b-Top p-t20">
            <div class="inline pull-left">
                {if isset($popup) && !isset($not_previous)}
                    <button class="pull-left link p-size m-tr10" id="back" type="button">
                        {ci_language line="Previous"}
                    </button>
                {/if}
            </div>
            {if  !isset($no_save_btn) || (isset($no_save_btn) && !$no_save_btn)}
            <div class="inline pull-right">  
                {if empty($next) || (isset($not_continue) && $not_continue == true && !isset($popup))}
                    <button class="btn btn-save" id="save_data" type="submit" >
                        {ci_language line="Save"}
                    </button>
                {else}
                    {if !isset($popup)}
                        <button class="pull-left link p-size m-tr10" id="save_data" type="submit" >
                            {ci_language line="Save"}
                        </button>
                    {/if}                    
                    <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                        {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                    </button>                        
                {/if}               
            </div>
            {/if}
        </div>		
    </div>
    {/if}       
</div>

<input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="country" name="country" value="{if isset($country)}{$country}{/if}" />
<input type="hidden" id="field_required" name="field_required" value="{ci_language line="This field is required."}" />                
<input type="hidden" id="marketplace_name" name="marketplace_name" value="{if isset($marketplace_name)}{$marketplace_name}{/if}" />
<input type="hidden" id="class_call" name="class_call" value="{if isset($class_call)}{$class_call}{/if}" />
<input type="hidden" id="function_call" name="function_call" value="{if isset($function_call)}{$function_call}{/if}" />
<input type="hidden" id="id_mode" name="id_mode" value="{if isset($id_mode)}{$id_mode}{/if}" />
<input type="hidden" id="id_shop" name="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />  
<input type="hidden" id="method_value" name="method_value" value="{if isset($method)}{$method}{/if}" />  
<input type="hidden" id="processing" value="Processing...">
<input type="hidden" id="next_page" value="{if isset($next)}{$next}{/if}">
<input type="hidden" name="update-{$function_call}-success" class="update-{$function_call}-success" value="{ci_language line="update_{$function_call}_success"}">
<input type="hidden" name="update-{$function_call}-fail" class="update-{$function_call}-fail" value="{ci_language line="update_{$function_call}_fail"}">
{if isset($hidden_data)}
    {foreach from = $hidden_data key = key item = hidden}
    <input type="hidden" name="{$key}" class="{$key}" value="{$hidden}">
    {/foreach}
{/if}
</form>
{include file="components/include_script.tpl"}
{include file="footer.tpl"}

