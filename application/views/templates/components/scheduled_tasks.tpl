<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/component/{str_replace(' ', '_', strtolower($function))}.css" />
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 statistics">
                <table class="custom-form" id="my_task_list" ext="{$ext}" market="{$marketplace_name}">
                    <thead class="table-head">
                        <tr>
                            <th><label class="cb-checkbox"><input class="sel_all" type="checkbox" ><span class="th_txt">{ci_language line="Allow automatic update"}</span></label></th>
                            <th>{ci_language line="Tasks"}</th>
                            <th>{ci_language line="Checked time"}</th>
                            <th class='center-align'>{ci_language line="Status"}</th>
                            <th class='center-align'>{ci_language line="Next time"}</th>
                        </tr>
                    </thead>
                    {if isset($tasks)}
                    <tbody>
                        {foreach from=$tasks key=key item=value}
                            <tr class="task_row" rel="{$value['task_name']}">
                                <td><label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="{$value['task_key']}" crel='{$value['task_id']}' {if $value['task_running'] == true}checked{/if}>{$value['task_display_name']}</label></td>
                                <td>{ci_language line=$value['task_name']}</td>
                                <td class="t_time">{$value['task_lastupdate']}</td>
                                 <td class='center-align'><div class="t_status {$value['task_status']}"></div></td>
                                <td class='center-align'><div class="countdown_next" rel="{$countdown[$value['task_key']]}"></div></td>
                            </tr>
                        {/foreach}
                    </tbody>
                    {/if}
                </table>

            </div>
        </div>
    </div>			
</div>