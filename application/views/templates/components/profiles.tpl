<div class="row">
    <div class="col-xs-12">
        <div class="b-Bottom p-b10 p-t10">
            <button type="button" class="link showProfile p-size" id="add">{ci_language line="+ Add a profile to the list"}</button>
            <p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile">{if isset($profiles) && !empty($profiles)}{$profiles|@count}{else}0{/if}</span> {ci_language line="Item(s)"}</p>
        </div>
    </div>
</div>
<div id="main" class="showSelectBlock">
    <div class="col-md-10 col-lg-9 custom-form m-t10 m-b30">
        <a class="link removeProfile remove" type="button">{ci_language line="Remove a profile from list"}</a>
        <div class="profileBlock clearfix market-profile" rel="0">
            <!--Information-->
            <div class="validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i>
                    </div>
                    <div class="validateCell">
                        <p class="pull-left">
                            {ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
                        </p>
                    </div>
                </div>
            </div>

            <!--Profile Name-->
            <p class="regRoboto poor-gray">{ci_language line="Name"}</p>
            <div class="form-group has-error">
                <input type="text" name="profile_name" rel="name" class="form-control" />
                <div class="error">
                    <p>
                        <i class="fa fa-exclamation-circle"></i>
                        {ci_language line="Set Profile Name First"}
                    </p>
                </div>
            </div>
            <p class="poor-gray text-right">
                {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
            </p>

            <!--Model Name-->
            <div class="row">
                <div class="col-xs-12 p-t10">
                    <div class="b-Top p-t20 clearfix">
                        <p class="head_text p-b20">
                            {ci_language line="Model"}
                        </p>
                    </div>
                </div>
            </div>		
            <div class="row">
                <div class="col-xs-12">
                    {if isset($models)}
                        <div class="form-group">
                            <select rel="id_model" class="search-select" disabled>
                                <option value="">{ci_language line="Select a model"}</option>
                                {foreach from=$models item=models_item}
                                    <option value="{$models_item.id_model}">{$models_item.name}</option>
                                {/foreach} 
                            </select>  
                        </div>
                        <p class="regRoboto poor-gray text-right">{ci_language line="Choose a model"}.</p>                       
                    {else}
                    {if isset($no_models)}{$no_models}{/if}
                {/if}							
            </div>
        </div>

        <!--Out of Stock-->
        <p class="regRoboto poor-gray b-Top p-t20">{ci_language line="Out of Stock"}</p>
        <div class="form-group m-b20">
            <div class="row">
                <div class="col-xs-4">
                    <input type="text" rel="out_of_stock" disabled class="form-control" />		
                </div>
                <p class="regRoboto poor-gray text-left p-t10"> {ci_language line="Minimum of quantity required to be in stock to export the product."}</p>
            </div>
            <div class="error text-right" style="display:none">
                <p class="m--t10 p-0">
                    <i class="fa fa-exclamation-circle"></i>
                    <span>{ci_language line="Allow only digit"}.</span>
                </p>
            </div>
        </div>

        <!-- GTIN Field -->
        {*if isset($amazon_display[$ext]['synchronization_field']) && $amazon_display[$ext]['synchronization_field']*}
        <p class="regRoboto poor-gray">{ci_language line="GTIN Field"}</p>
        <div class="m-b10 synch">
            <div class="choZn">
                <select data-placeholder="Choose" rel="synchronization_field" disabled>
                    <option value="" disabled="disabled">{ci_language line="Choose one of the following"}</option>
                    <option value="ean13">{ci_language line="EAN13 (Europe)"}</option>
                    <option value="upc">{ci_language line="UPC (United States)"}</option>
                    <option value="both" selected="">{ci_language line="Both (EAN13 then UPC)"}</option>
                    <option value="reference">{ci_language line="SKU"}</option>
                </select>  							
            </div>
        </div>	
        <p class="regRoboto poor-gray text-right">{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping."}</p>
        {*/if*}

        <div class="row m-t20">

            <!-- Title Format -->
            <div class="col-sm-5 col-md-6 col-lg-5">
                <p class="montserrat dark-gray text-uc">{ci_language line="Title Format"}</p>
                <label class="w-100">
                    <input rel="title_format" type="radio" value="1" disabled="disabled">
                    {ci_language line="Standard Title, Attributes"}	
                </label>
                <label class="w-100">
                    <input rel="title_format" type="radio" value="2" disabled="disabled">
                    {ci_language line="Manufacturer, Title, Attributes"}
                </label>
                <label class="w-100">
                    <input rel="title_format" type="radio" value="3" disabled="disabled">
                    {ci_language line="Manufacturer, Title, Reference, Attributes"}
                </label>						
            </div>

            <!-- HTML Descriptions -->
            <div class="col-sm-5 col-md-6 col-lg-5">
                <p class="montserrat dark-gray text-uc">{ci_language line="HTML Descriptions"}</p>
                <div class="clearfix">
                    <label class="pull-left">
                        <input rel="html_description" type="checkbox" disabled="disabled" value="1">
                        {ci_language line="Yes"}
                    </label>							
                </div>
                <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
            </div>

            <!-- Title Format Descriptions -->
            <div class="col-xs-12">
                <p class="regRoboto poor-gray m-t10">
                    {ci_language line="Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes."}
                    {ci_language line="Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping."}
                </p>
            </div>

        </div>

        <div class="row m-t20">

            <!-- Description Field -->
            <div class="col-sm-5 col-md-6 col-lg-5">
                <p class="montserrat dark-gray text-uc">{ci_language line="Description Field"}</p>
                <label class="w-100">
                    <input rel="description_field" type="radio" name="description_field" value="1" disabled="disabled" />
                    {ci_language line="Description"}
                </label>
                <label class="w-100">
                    <input rel="description_field" type="radio" name="description_field" value="2" disabled="disabled" />
                    {ci_language line="Short Description"}
                </label>
                <label class="w-100">
                    <input rel="description_field" type="radio" name="description_field" value="3" disabled="disabled" />
                    {ci_language line="Both"}
                </label>
                <label class="w-100">
                    <input rel="description_field" type="radio" name="description_field" value="4" disabled="disabled" />
                    {ci_language line="None"}
                </label>
            </div>

            <!-- Description field -->
            <div class="col-xs-12">
                <p class="poor-gray m-t10">
                    {ci_language line="Whether to send short or long description of the product to Google Shopping."}
                </p>
            </div>
        </div>
    </div>
</div>

</div><!-- id main -->

{*<pre>{$profiles|print_r}</pre>*}

<div id="tasks">
    {if !empty($profiles) }
        {foreach from=$profiles key=k item=data}
            <div class="row market-profiles">
                <div class="col-xs-12">
                    <div class="ruleSettings"></div>

                    <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                        <p class="text-uc dark-gray montserrat pull-left p-t10 m-b0">
                            &nbsp;<i class="fa fa-angle-right"></i> 
                            <span>
                            {if isset($data.name)}{$data.name}{/if}
                        </span>
                    </p>
                    <ul class="pull-right amazonEdit">
                        <li class="editProfile">
                            <button type="button" class="link editProfile_link edit_profile">
                                <i class="fa fa-pencil"></i> {ci_language line="edit"}
                            </button>
                            <!-- a class="editProfile_link" href="">edit</a> -->
                        </li>
                        <li class="remProfile">
                            <button type="button" class="link removeProfile_link remove_profile" rel="{$data.name}" value="{$data.id_profile}" >
                                <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                            </button>  
                        </li>
                    </ul>
                </div>
            </div>


            {*out_of_stock
            synchronization_field
            title_format
            html_description
            description_field
            price_rules*}

            <div class="col-md-10 col-lg-9 custom-form m-t10 market-profile showSelectBlock" rel="{$k}">
                <a class="link removeProfile remove" type="button">{ci_language line="Remove a profile from list"}</a>
                <div rel="{$k}" class="profileBlock clearfix {*if !isset($popup_more['no_model'])}showSelectBlock{/if*} m-b30">
                    <input type="hidden" rel="id_profile" name="profile[{$k}][id_profile]" value="{$data.id_profile}"/>

                    <!--Profile Name-->
                    <p class="regRoboto poor-gray">{ci_language line="Name"}</p>
                    <div class="form-group">
                        <input type="text" rel="name" name="profile[{$k}][name]" value="{if isset($data.name)}{$data.name}{/if}" class="form-control"/>
                        <div class="error" style="display: none;">
                            <p>
                                <i class="fa fa-exclamation-circle"></i>
                                {ci_language line="Set Profile Name First"}
                            </p>
                        </div>
                    </div>
                    <p class="poor-gray text-right">
                        {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
                    </p>

                    <!--Model Name-->
                    <div class="row">
                        <div class="col-xs-12 p-t10">
                            <div class="b-Top p-t20 clearfix">
                                <p class="head_text p-b20">
                                    {ci_language line="Model"}			
                                </p>
                            </div>
                        </div>
                    </div>		
                    <div class="row">
                        <div class="col-xs-12">
                            {if isset($models)}
                                <div class="form-group {if isset($popup_more['no_model']) && $popup_more['no_model']}has-error{/if}">
                                    <select class="search-select" rel="id_model" name="profile[{$k}][id_model]" >
                                        <option value="">{ci_language line="Select a model"}</option>
                                        {if isset($models)}
                                            {foreach from=$models item=models_item}
                                                <option value="{$models_item.id_model}" {if isset($data.id_model) && $data.id_model == $models_item.id_model} selected{/if}>
                                                    {$models_item.name}
                                                </option>
                                            {/foreach} 
                                        {/if}  
                                    </select>  
                                    {if isset($popup_more['no_model']) && $popup_more['no_model']}
                                        <div class="error">
                                            <p>
                                                <i class="fa fa-exclamation-circle"></i>
                                                {ci_language line="Please choose a model for this profile."}
                                            </p>
                                        </div>	
                                    {/if}
                                </div>
                                <p class="regRoboto poor-gray text-right">{ci_language line="Choose a model"}.</p>                       
                            {else}
                            {if isset($no_models)}{$no_models}{/if}
                        {/if}							
                    </div>
                </div>

                <!--Out of Stock-->
                <p class="regRoboto poor-gray b-Top p-t20">{ci_language line="Out of Stock"}</p>	
                <div class="form-group m-b20">
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="text" rel="out_of_stock" name="profile[{$k}][out_of_stock]" value="{if isset($data.out_of_stock)}{$data.out_of_stock}{/if}" class="form-control"/> 		
                        </div>
                        <p class="regRoboto poor-gray text-left p-t10"> {ci_language line="Minimum of quantity required to be in stock to export the product."}</p>
                    </div>
                    <div class="error text-right" style="display:none">
                        <p class="m--t10 p-0">
                            <i class="fa fa-exclamation-circle"></i>
                            <span>{ci_language line="Allow only digit"}.</span>
                        </p>
                    </div>
                </div>	

                <!-- GTIN Field -->
                <p class="regRoboto poor-gray">{ci_language line="GTIN Field"}</p>
                <div class="m-b10 synch">
                    <div class="choZn">
                        <select class="search-select" rel="synchronization_field" name="profile[{$k}][synchronization_field]">
                            <option value="" disabled="disabled">{ci_language line="Choose one of the following"}</option>
                            <option value="ean13" {if isset($data.synchronization_field) && $data.synchronization_field == "ean13"}selected{/if}>
                                {ci_language line="EAN13 (Europe)"}</option>
                            <option value="upc"{if isset($data.synchronization_field) && $data.synchronization_field == "upc"}selected{/if}>
                                {ci_language line="UPC (United States)"}</option>
                            <option value="both"{if isset($data.synchronization_field) && $data.synchronization_field == "both"}selected{/if}>
                                {ci_language line="Both (EAN13 then UPC)"}</option>
                            <option value="reference"{if isset($data.synchronization_field) && $data.synchronization_field == "reference"}selected{/if}>
                                {ci_language line="SKU"}</option>
                        </select>
                    </div>
                </div>	
                <p class="regRoboto poor-gray text-right">{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping."}</p>

                <div class="row m-t20">

                    <!-- Title Format -->
                    <div class="col-sm-5 col-md-6 col-lg-5">
                        <p class="montserrat dark-gray text-uc">{ci_language line="Title Format"}</p>
                        <label class="cb-radio w-100 {if isset($data.title_format) && $data.title_format == 1}checked{/if}">
                            <input rel="title_format" name="profile[{$k}][title_format]"  type="radio" value="1" {if isset($data.title_format) && $data.title_format == 1}checked{/if}>
                            {ci_language line="Standard Title, Attributes"}	
                        </label>
                        <label class="cb-radio w-100 {if isset($data.title_format) && $data.title_format == 2}checked{/if}">
                            <input rel="title_format" name="profile[{$k}][title_format]" type="radio" value="2" {if isset($data.title_format) && $data.title_format == 2}checked{/if}>
                            {ci_language line="Manufacturer, Title, Attributes"}
                        </label>
                        <label class="cb-radio w-100 {if isset($data.title_format) && $data.title_format == 3}checked{/if}">
                            <input rel="title_format" name="profile[{$k}][title_format]" type="radio" value="3" {if isset($data.title_format) && $data.title_format == 3}checked{/if}>
                            {ci_language line="Manufacturer, Title, Reference, Attributes"}
                        </label>						
                    </div>

                    <!-- HTML Descriptions -->
                    <div class="col-sm-5 col-md-6 col-lg-5">
                        <p class="montserrat dark-gray text-uc">{ci_language line="HTML Descriptions"}</p>
                        <div class="clearfix">
                            <label class="pull-left cb-checkbox {if isset($data.html_description) && $data.html_description == 1}checked{/if}">
                                <input rel="html_description" type="checkbox" {if isset($data.html_description) && $data.html_description == 1}checked{/if} value="1"  name="profile[{$k}][html_description]">
                                {ci_language line="Yes"}
                            </label>																
                        </div>
                        <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
                    </div>

                    <!-- Title Format Descriptions -->
                    <div class="col-xs-12">
                        <p class="poor-gray m-t10">
                            {ci_language line="Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes."}
                            <!-- </p>
                            <p class="poor-gray"> -->
                            {ci_language line="Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping."}
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </div>
{/foreach} 
{/if}

</div>
<input type="hidden" id="method" value="profiles" />
<input type="hidden" id="currency_sign" value="{if !empty($currency_sign)}{$currency_sign}{/if}" />