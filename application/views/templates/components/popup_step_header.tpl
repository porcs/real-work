<div {if isset($popup)}class="p-rl40 p-xs-rl20"{else}class="main p-rl40 p-xs-rl20"{/if}>
    {if isset($popup) && $popup != ''}          
        
    {else}
        <h1 class="lightRoboto text-uc head-size light-gray">{$function}</h1>      
        {include file="breadcrumbs.tpl"} 
        <div class="row">
            <div class="col-xs-12">
                <p class="head_text p-t10 b-Top clearfix m-0">
                    {if isset($ext)}<img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" class="flag m-r10" alt="">{/if}
                    {if isset($country)}<span class="p-t10">{$country}</span>{/if}
                    {if isset($popup_more) && $popup_more != ''} <i class="fa fa-angle-right"></i> {ucwords($function)} {/if} 
                </p>
            </div>
        </div>

        {if isset($add_data) && !empty($add_data)}
            <div class="row">
                <div class="col-xs-12 p-t10">
                    <div class="b-Bottom p-b10">
                        <button type="button" class="link showProfile p-size" id="add"><i class="fa fa-plus"></i> {if isset($add_data)}{$add_data}{else}{ci_language line="Add a otpion"}{/if}</button>
                        <p class="pull-right montserrat poor-gray">
                            <span class="dark-gray countProfile" id="count">{if isset($size_data)}{$size_data}{else}0{/if}</span> 
                            {ci_language line="Items(s)"}
                        </p>
                    </div>
                </div>
            </div>   
        {/if}       

    {/if}  

    {if isset($no_data)}
        <div class="m-t10" id="no-data">
            {$no_data}
        </div>
    {/if}   