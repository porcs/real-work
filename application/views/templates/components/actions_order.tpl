<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            <!--orders-->
            <div class="tab-pane active" id="orders">                    
                <div class="row">
                    <div class="col-sm-12">
                        <p class="head_text montserrat black p-t20 b-None">
                            {ci_language line="Import Orders"}
                        </p>
                        <ul class="p-l20">
                            <li>
                                <p class="poor-gray">
                                    {ci_language line='This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'}
                                </p>
                            </li>
                        </ul>
                        {if isset($message_code_inner.carrier)}
                            {$message_code_inner.carrier}
                        {else}
                            <div id="parameter_options">
                                <p class="head_text montserrat black p-t20 b-None">
                                    {ci_language line="Parameters"}
                                </p>
                                <div class="row">
                                    <div class="col-sm-5 col-lg-6">
                                        <p class="poor-gray">{ci_language line='Order Date Range'}</p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group calendar firstDate">
                                                    <input class="form-control order-date" type="text" id="order-date-from" value="{if isset($yesterday)}{$yesterday}{/if}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group calendar lastDate">
                                                    <input class="form-control order-date" type="text" id="order-date-to" value="{if isset($today)}{$today}{/if}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <p class="poor-gray">{ci_language line='Order Status'}</p>
                                        <select id="order-status" class="search-select" >
                                            {*<option value=""></option>*}
                                            <option value="All">{ci_language line='Retrieve all pending orders'}</option>
                                            <option value="Pending">{ci_language line='Pending - This order is pending in the market place'}</option>
                                            <option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order is waiting to be shipped'}</option>
                                            <option value="PartiallyShipped">{ci_language line='Partially shipped - This order was partially shipped'}</option>
                                            <option value="Shipped">{ci_language line='Shipped - This order was shipped'}</option>
                                            <option value="Canceled">{ci_language line='Cancelled - This order was cancelled'}</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 col-lg-3 sm-right" id="orders-import">
                                        <button class="btn btn-save m-t25" id="import-orders" type="button" value="orders-import">
                                            {ci_language line="Import orders"} 
                                        </button>
                                        <div class="status sm-right" style="display: none">
                                            <p>{ci_language line="Starting import"} ..</p>
                                        </div>
                                        <div class="error sm-right" style="display: none">
                                            <p class="m-t0">
                                                <i class="fa fa-exclamation-circle"></i>
                                                <span></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <p class="head_text montserrat black p-t20 b-None">
                                    {ci_language line="Update Order Shipments"}
                                </p>
                                <div class="row">
                                    <div class="col-sm-8 col-lg-9">
                                        <ul class="p-l20">
                                            <li>
                                                <p class="poor-gray">
                                                    {ci_language line='This will confirm orders shipment to Amazon. The order was Shipped will update to Amazon by clicking the button below.'}
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="parameter_options"> 
                                        <div class="col-sm-4 col-lg-3 sm-right" id="orders-update">
                                            <button class="btn btn-rule" id="update-orders" type="button" value="orders-update">
                                                {ci_language line="Update Order Shipments"}
                                            </button>
                                            <p></p>
                                            <div class="status sm-right" style="display: none;">
                                                <p>{ci_language line="Starting update"} ..</p>
                                            </div>
                                            <div class="error sm-right" style="display: none">
                                                <p class="m-t0">
                                                    <i class="fa fa-exclamation-circle"></i>
                                                    <span></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    </div>						
                </div>
            </div>
        </div>
    </div>
</div>

{include file="amazon/PopupStepFooter.tpl"}  

<input type="hidden" value="{ci_language line="Are you sure to send relations?"}" id="send_relations_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to send images?"}" id="send_images_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to get orders?"}" id="get-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update order shipments?"}" id="update-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to delete product?"}" id="delete-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
<input type="hidden" id="method" value="action_order" />
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 

<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
