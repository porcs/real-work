<form id="form-mapping" method="post" action="{$base_url}amazon/save_conditions" enctype="multipart/form-data" >

    {if isset($mk_condition) && !empty($mk_condition) && isset($shop_condition) && !empty($shop_condition)}

    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-t20 p-b20 m-b0">
                {ci_language line="Condition Mappings"}
            </p>
        </div>
    </div>

    <div id="condition-group" class="clearfix">

        <div class="row">
            <div class="col-xs-12">
                <p class="head_text p-0 m-0 p-t20 p-b20 b-0 col-sm-6">
                    {$is_marketplace} - {ci_language line="Conditions"}
                </p>
                <p class="head_text p-t20 p-b20 b-0 col-sm-6">
                    {ci_language line="Feed.biz - Conditions"}
                </p>
            </div>
        </div>

        {foreach from=$mk_condition key=mk item= market}

        <div rel="{$mk}">

            {foreach $market as $key => $cond}

            <div class="row conditions condition_row">
                <div class="col-xs-12">
                    <div class="{if $key != 0}b-Top p-t15{else}p-t5{/if}">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="poor-gray p-t10">{$cond['condition_text']}</p>
                                <i class="blockRight noBorder"></i>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="select-main search-select" name="cond[{$cond['id']}]">
                                        <option value="">{ci_language line="Select an option"}</option>
                                        {foreach $shop_condition[$mk] as $s_cond}
                                        <option value="{$s_cond['txt']}" {if isset($s_cond['condition_value']) && !empty($s_cond['condition_value']) && ($s_cond['condition_value'] ==  $cond['condition_value'])}selected="selected"{/if}>{ucfirst(strtolower($s_cond['txt']))}</option>
                                        {/foreach}
                                    </select>                                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                   
            {/foreach}
        </div>   

        {/foreach}

    </div><!--condition-group-->

    {/if}

    <!--Message-->
    <input type="hidden" id="error-message" value="{ci_language line="Mapping colors are require"}!" />
    <input type="hidden" id="save-unsuccess-message" value="{ci_language line="Please map all attribute color."}" />
</form>