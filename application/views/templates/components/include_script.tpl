{if !empty($bootbox)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/bootbox/bootbox.min.js?v=v1.2.3"></script>
{/if}
{if !empty($colorbox)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/colorbox/jquery.colorbox-m.js"></script>
{/if}
{if !empty($currency_format)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/formatCurrency/jquery.formatCurrency.js"></script>
{/if}
{if !empty($data_cooldown)}
<script type="text/javascript" src="{$cdn_url}assets/js/countdown.js"></script> 
{/if}
{if !empty($data_tree)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tree/fuelux.tree.offers.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/blockUI/jquery.blockUI.js"></script>
{/if}
{if !empty($data_table)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tables/dataTables.colVis.min.js"></script>
{/if}
{if !empty($jquery_form)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/form/jquery.form.js"></script>
{/if}
{if !empty($jquery_validate)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/validate/jquery.validate.js"></script>
{/if}
{if !empty($tinymce)}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/ebay/specific/tinymce/tinymce.min.js"></script>
{/if}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/component/{str_replace(' ', '_', strtolower($function_call))}.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/component/components.js"></script>