{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Stock"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="csv">
        <a href="{$base_url}stock/products_download" id="export-stock" class="btn btn-success">{ci_language line="Download CSV"}</a>
    </div>		
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Stock Movement"}</h4>						
                </div>
                {*<div class="pull-sm-right clearfix">
                    <div class="showColumns clearfix">
                        <a href="" class="showColumns_link">Show / Hide Columns</a>
                        <div class="dropCheckbo">
                            <ul class="custom-form">
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="id" checked/>
                                        {ci_language line="Reference"}
                                    </label>
                                </li>
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="username" checked/>
                                        {ci_language line="Date"}
                                    </label>
                                </li>
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="email" checked/>
                                        {ci_language line="Detail"}
                                    </label>
                                </li>
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="status" checked/>
                                        {ci_language line="Quantity"}
                                    </label>
                                </li>
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="joined" checked/>
                                        {ci_language line="Movement Quantity"}
                                    </label>
                                </li>
                                <li>
                                    <label class="cb-checkbox checked">
                                        <input type="checkbox" name="summary" checked/>
                                        {ci_language line="Balance Quantity"}
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>	
                </div>	*}		
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="stock_report" class="responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th class="tableId">{ci_language line="Reference"}</th>
                                <th class="tableUserName">{ci_language line="Date"}</th>
                                <th class="tableEmail">{ci_language line="Detail"}</th>
                                <th class="tableStatus">{ci_language line="Quantity"}</th>
                                <th class="tableJoined">{ci_language line="Movement Quantity"}</th>
                                <th class="tableSummary">{ci_language line="Balance Quantity"}</th>
                            </tr>
                        </thead>							
                    </table>
                    <table class="table tableEmpty">
                        <tr>
                            <td>{ci_language line="No data available in table"}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>			
    </div>
</div>

<!--Data Table-->
{include file="IncludeDataTableLanguage.tpl"}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/stock/report.js"></script> 

<!--page specific plugin scripts-->
{include file="footer.tpl"}