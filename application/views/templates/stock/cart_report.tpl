{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Carts Report"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="csv">
        <a href="{$base_url}stock/carts_download" id="export-stock" class="btn btn-success">{ci_language line="Download CSV"}</a>
    </div>		
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Carts"}</h4>						
                </div>
                	
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table id="stock_report" class="responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th class="">{ci_language line="Reference"}</th>
                                <th class="">{ci_language line="Order Date"}</th>
                                <th class="">{ci_language line="Marketplace"}</th>
                                <th class="">{ci_language line="Order Ref."}</th>
                                <th class="">{ci_language line="Quantity Reserved"}</th>
                                {*<th class="">{ci_language line="Movement Quantity"}</th>
                                <th class="">{ci_language line="Balance Quantity"}</th>*}
                                {*<th class="">{ci_language line="Name"}</th>*}
                            </tr>
                        </thead>							
                    </table>
                    <table class="table tableEmpty">
                        <tr>
                            <td>{ci_language line="No data available in table"}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>			
    </div>
</div>

<!--Data Table-->
{include file="IncludeDataTableLanguage.tpl"}
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/stock/cart_report.js"></script> 

<!--page specific plugin scripts-->
{include file="footer.tpl"}