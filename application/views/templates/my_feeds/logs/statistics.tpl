{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_feed/statistics.css" />
<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="my_feeds"}
                <small><i class="icon-double-angle-right"></i>  {ci_language line="statistics"} </small>
            </h1>
        </div>
        
        <div class="row-fluid">                            
            <div class="offset1 span10">                
                {if isset($feed_biz)}
                    <div class="alert alert-block alert-info" id="list_src_cv"  >
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="icon-remove"></i>
                        </button>
                        <i class="icon-time bigger-100 orange"></i>
                        <b>{ci_language line="Last feed"} : </b> <br/>
                        <ul class="unstyled spaced" id="list_src"  >
                            
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Source"}</b>
                                {ucfirst($feed_biz.source)}
                            </li>
                            
                            {if isset($feed_biz.feed_source)}
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Feed source"}</b>
                                {ucfirst($feed_biz.feed_source)}
                            </li>
                            {/if}
                            
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Shopname"}</b>
                                {ucfirst($feed_biz.shop.name)}
                                <input type="hidden" value="{$feed_biz.shop.name}" id="shopname" />
                            </li>
                            
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Last time"}</b>
                                {ucfirst($feed_biz.time)}
                            </li>
                            
                            {if isset($feed_biz.product.fileurl)}
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Product"}</b> <i class="icon-double-angle-right"></i>  
                                {$feed_biz.product.fileurl}
                            </li>
                            {/if}
                            
                            {if isset($feed_biz.offer.fileurl)}
                            <li >
                                <i class="icon-angle-right"></i>
                                <b>{ci_language line="Offer"}</b> <i class="icon-double-angle-right"></i> 
                                {$feed_biz.offer.fileurl}
                            </li>
                            {/if}
                            
                        </ul>
                    </div>
                {/if}
                    
                <!--Logs-->
                <div id="log">
                    
                    {if isset($feed_biz.product)}
                    <div class="span6" >
                        <input type="hidden" value="true" id="feed_product" />
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                    <i class="icon-signal"></i>
                                    {ci_language line="Products"}
                                </h5>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="product"></div>
                                    
                                    <div class="hr hr8 hr-double"></div>
                                    <div class="clearfix">
                                        <div class="grid4">
                                            <div class="grey">
                                                <i class="icon-circle blue"></i> &nbsp; 
                                                {ci_language line="Total"}
                                            </div>
                                            <h4 class="bigger pull-right" id="product_no_total"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                 <i class="icon-circle green"></i> &nbsp; 
                                               {ci_language line="Success"}
                                            </div>
                                            <h4 class="bigger pull-right" id="product_no_success"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                 <i class="icon-circle"></i> &nbsp; 
                                               {ci_language line="Warning"}
                                            </div>
                                            <h4 class="bigger pull-right" id="product_no_warning"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                <i class="icon-circle red"></i> &nbsp; 
                                                {ci_language line="Error"}
                                            </div>
                                            <h4 class="bigger pull-right" id="product_no_error"></h4>
                                        </div>
                                    </div>
                                </div><!--/widget-main-->
                            </div><!--/widget-body-->
                        </div><!--/widget-box-->
                    </div><!--/span6-->
                    {/if}
                    
                    {if isset($feed_biz.offer)}
                    <div class="span6" >
                        <input type="hidden" value="true" id="feed_offer" />
                        <div class="widget-box">
                            <div class="widget-header widget-header-flat widget-header-small">
                                <h5>
                                    <i class="icon-signal"></i>
                                    {ci_language line="Offers"}
                                </h5>
                            </div>

                            <div class="widget-body">
                                <div class="widget-main">
                                    <div id="offer"></div>
                                    
                                   <div class="hr hr8 hr-double"></div>
                                    <div class="clearfix">
                                        <div class="grid4">
                                            <div class="grey">
                                                <i class="icon-circle blue"></i> &nbsp; 
                                                {ci_language line="Total"}
                                            </div>
                                            <h4 class="bigger pull-right" id="offer_no_total"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                 <i class="icon-circle green"></i> &nbsp; 
                                               {ci_language line="Success"}
                                            </div>
                                            <h4 class="bigger pull-right" id="offer_no_success"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                 <i class="icon-circle"></i> &nbsp; 
                                               {ci_language line="Warning"}
                                            </div>
                                            <h4 class="bigger pull-right" id="offer_no_warning"></h4>
                                        </div>
                                        <div class="grid4">
                                            <div class="grey">
                                                <i class="icon-circle red"></i> &nbsp; 
                                                {ci_language line="Error"}
                                            </div>
                                            <h4 class="bigger pull-right" id="offer_no_error"></h4>
                                        </div>
                                    </div>                                    
                                </div><!--/widget-main-->
                            </div><!--/widget-body-->
                        </div><!--/widget-box-->
                    </div><!--/span6-->
                    {/if}
                    
                </div>

            </div>{*offset1*}
             
        </div>{*row-fluid*}

    </div>{*page-content*}
    
</div>{*main-content*}

<input id="product_error" type="hidden" />
<input id="product_success" type="hidden" />

{include file="footer.tpl"}

<!--page specific plugin scripts-->
{*<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>*}
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/logs/statistics.js"></script>