{include file="header.tpl"}
{ci_config name="base_url"}
{include file="sidebar.tpl"}

<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="mapping"} {ci_language line="products"}
                <small><i class="icon-double-angle-right"></i>  {ci_language line="attributes"} </small>
            </h1>
        </div>
        
        <div class="row-fluid">            
            <div class="offset1 span10">
                <form action="{$base_url}my_feeds/attributes_save" id="mapping-attribute" class="form-horizontal" method="post">
                    <h4 class="row-fluid smaller lighter blue">
                        <label class="pull-right inline">
                            <input id="count" value="{$m_row}" readonly="">
                            {ci_language line="Records"}
                            <i class="icon-th-large"></i>
                        </label>
                    </h4>

                    <div class="control-group">
                        <div id="attribute-1" rel="{if $m_row != 0}{$m_row-1}{else}1{/if}" class="row-odd">
                            <select class="select" disabled id="attribute_1">
                                <option value="" selected="">{ci_language line="choose_a_attribute"}</option>
                                {foreach from=$attribute key=akey item=value}
                                    <option value="{$value.name}">{$value.name}</option>
                                {/foreach} 
                            </select>
                            &nbsp; &nbsp; <i class="icon-exchange icon-large grey"></i>&nbsp; &nbsp; 
                            <input type="text" disabled />
                            &nbsp; &nbsp; 
                            <a class="addnewmapping" id="attr" rel="{if $m_row != 0}{$m_row-1}{else}1{/if}"><i class="icon-plus-sign icon-large green"></i></a>
                            <a class="removemapping" id="attr" style="display: none;"><i class="icon-minus-sign icon-large red"></i></a>
                        </div>

                        <div id="new-mapping">
                            {if isset($m_attribute)}
                                {foreach from=$m_attribute key=mkey item=mvalue}
                                    {if $mvalue.selected == 'true'}
                                        <div {if $mkey%2 == 0}class="row-even"{else}class="row-odd"{/if}> 
                                            <select class="select" name="attribute[{*$mkey*}]">
                                                {foreach from=$m_attribute key=key item=val}
                                                    <option value="{$val.attribute}" {if $val.attribute === $mvalue.attribute} selected="" {/if}>{$val.attribute}</option>
                                                {/foreach}
                                            </select>
                                            &nbsp; &nbsp; <i class="icon-exchange icon-large grey"></i>&nbsp; &nbsp; 
                                            <input type="text" value="{$mvalue.value}"  name="mapping[{*$mkey*}]"/>
                                            &nbsp; &nbsp;
                                            <a class="removemapping" id="attr"><i class="icon-minus-sign icon-large red"></i></a>
                                        </div>  
                                    {/if}
                                {/foreach}
                            {/if}
                        </div>

                    </div>

                    <div class="form-actions">
                        <button class="btn btn-info" type="submit">
                            <i class="icon-ok bigger-110"></i>
                            {ci_language line="save"}
                        </button> &nbsp; &nbsp; &nbsp;
                        {*<button class="btn" type="reset">
                            <i class="icon-undo bigger-110"></i>
                            {ci_language line="reset"}
                        </button>*}
                    </div>
                </form>

            </div>{*span10*}
            
        </div>{*row-fluid*}
                            
    </div>{*page-content*}
    
</div>{*main-content*}

{include file="footer.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/attributes.js"></script>  
    