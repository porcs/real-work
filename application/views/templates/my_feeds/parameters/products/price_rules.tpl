{include file="header.tpl"}
{ci_config name="base_url"}
{include file="sidebar.tpl"}

<div class="main-content">
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="products"} 
                <small><i class="icon-double-angle-right"></i> {ci_language line="price_rules"} </small>
            </h1>
        </div>
        
        <div class="row-fluid">            
            <div class="offset1 span10">
                {*{if isset($error) }<div class="alert alert-danger">{$error}</div>{/if}
                {if isset($message) }<div class="alert alert-info">{$message}</div>{/if}*}

                {*<table id="products" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="center">
                                <label>
                                    <input type="checkbox" />
                                    <span class="lbl"></span>
                                </label>
                            </th>
                            <th>
                            </th>
                            <th>Name</th>
                            <th class="hidden-480">Reference/th>
                            <th class="hidden-480">Category</th>
                            <th class="hidden-phone">Quantity</th>
                            <th class="hidden-480">Price</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody>
                        {if isset($product.product)}
                            
                        {foreach from=$product.product key=id item=item}
                            
                            <tr>
                                <td class="center">
                                    <label>
                                        <input type="checkbox" />
                                        <span class="lbl"></span>
                                    </label>
                                </td>
                                <td>{$id}</td>
                                <td>{$item.name}</td>
                                <td class="hidden-480">{$item.refference}</td>
                                <td class="hidden-480">{$item.category}</td>
                                <td class="hidden-phone right">{$item.quantity}</td>
                                <td class="hidden-480 right">{$item.price} {if isset($item.currency)}<i class="icon-{$item.currency}"></i>{/if}</td>
                                <td class="td-actions">
                                    <div class="hidden-phone visible-desktop action-buttons">
                                        <a class="green" href="products/{$id}">
                                            <i class="icon-pencil bigger-130"></i>
                                        </a>
                                        <a class="red" href="#">
                                            <i class="icon-trash bigger-130"></i>
                                        </a>
                                    </div>

                                    <div class="hidden-desktop visible-phone">
                                        <div class="inline position-relative">
                                            <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-caret-down icon-only bigger-120"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="products/{$id}" class="tooltip-success" data-rel="tooltip" title="Edit">
                                                        <span class="green">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                        <span class="red">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        
                        {/foreach}
                        {/if}
                    </tbody>
                </table>*}

            </div>{*span10*}
            
        </div>{*row-fluid*}
                            
    </div>{*page-content*}
    
</div>{*main-content*}

{include file="footer.tpl"}