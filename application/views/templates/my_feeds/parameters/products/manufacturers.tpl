{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="manufacturers"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                <form action="{$base_url}my_feeds/manufacturers_save" id="mapping-attribute" method="post">
		<div class="row">
			<div class="col-xs-12">
				<div class="b-Top p-t10">
					<p class="pull-right montserrat poor-gray"><span class="dark-gray count"><input id="count" value="{$m_row}" readonly=""></span> {ci_language line="Records"}</p>
				</div>
			</div>
		</div>
		<div id="manufacturer-1" rel="{if $m_row != 0}{$m_row-1}{else}1{/if}" class="row">
			<div class="col-sm-6 m-b20">
				<select class="search-select" id="manufacturer_1" disabled="disabled">
                                    <option value="" selected="">{ci_language line="choose_a_manufacturer"}</option>
                                    {if isset($manufacturer)}
                                        {foreach from=$manufacturer key=akey item=value}
                                            <option value="{$value.name}">{$value.name}</option>
                                        {/foreach} 
                                    {/if}
				</select>
				<i class="blockRight noBorder"></i>
			</div>
			<div class="col-sm-6">
				<div class="form-group withIcon clearfix">
                                        <input type="text" class="form-control" readonly="readonly" rel='mapping'>
					<i class="cb-plus good addnewmapping" rel="{if $m_row != 0}{$m_row-1}{else}1{/if}"></i>
                                        <i class="cb-minus bad removemapping" style="display:none"></i>
				</div>
			</div>
		</div>
                 
                <div id="new-mapping" class="clearfix">
                {if isset($m_manufacturer)}
                {foreach from=$m_manufacturer key=mkey item=mvalue}
                    {if $mvalue.selected == 'true'}
                        <div class="row">
                                <div class="col-sm-6 m-b20">
                                        <select class="search-select" name="manufacturer[]">
                                            {foreach from=$m_manufacturer key=key item=val}
                                                    <option value="{$val.name}" {if $val.name == {$mvalue.name}} selected="" {/if}>{$val.name}</option>
                                            {/foreach}
                                        </select>
                                        <i class="blockRight noBorder"></i>
                                </div>
                                <div class="col-sm-6">
                                        <div class="form-group withIcon clearfix">
                                                <input type="text" class="form-control" value="{$mvalue.value}" name="mapping[]" rel='mapping' />
                                                <i class="cb-plus good addnewmapping" style="display:none"></i>
                                                <i class="cb-minus bad removemapping"></i>
                                        </div>
                                </div>
                        </div>
                        {/if}
                    {/foreach}
                {/if}
                </div>

                    {include file="helpers/action_save_continue.tpl" data='rules'}
                    
                </form>
	</div>

{include file="footer.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/manufacturers.js"></script>