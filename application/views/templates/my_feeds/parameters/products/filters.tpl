{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_feed/filters.css" />

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="filters"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->
    <form action="{$base_url}my_feeds/filters_save" id="filter" method="post">
        <div class="row">
            <div class="col-md-6 p-0">
            <div class="col-xs-12">
                <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                    <h4 class="headSettings_head">{ci_language line="Manufacturers"}</h4>						
                </div>
            </div>
        {*</div>

        <div class="row">*}
            <div class="col-sm-12 relative m-b10">
                <p class="head_text">
                    <i class="fa fa-remove true-pink"></i>
                    {ci_language line="excluded_manufacturers"}
                </p>
                <select id="manufacturers-1" name="manufacturers[]" multiple="multiple" class="col-xs-12 {*includeBlock*} chosen-select" data-placeholder="{ci_language line="excluded_manufacturers"}" data-no_results_text="{ci_language line="Oops, nothing found!"}">
                    {foreach from=$exclude_manufacturers key=ex_key item=ex_value}
                        <option value="{$ex_value.id_manufacturer}" selected="selected">{$ex_value.name}</option>
                    {/foreach}
                    {if isset($include_manufacturers)} {foreach from=$include_manufacturers key=in_key item=in_value}
                            <option value="{$in_value.id_manufacturer}">{$in_value.name}</option>
                        {/foreach} {/if}
                    </select>
                    {*<i class="blockRight include_manufacturers"></i>*}
                </div>
                {*<div class="col-sm-6 relative">
                <p class="head_text">
                <i class="fa fa-check true-green"></i>
                {ci_language line="included_manufacturers"}
                </p>
                <select id="manufacturers-2" multiple="multiple" class="col-xs-12 includeBlock">
                {if isset($include_manufacturers)} {foreach from=$include_manufacturers key=in_key item=in_value}
                <option value="{$in_value.id_manufacturer}">{$in_value.name}</option>
                {/foreach} {/if}
                </select>
                <i class="blockLeft exclude_manufacturers"></i>
                </div>*}
            </div>
            {*</div>

            <div class="row">*}
                <div class="col-md-6 p-0">
                <div class="col-xs-12">
                    <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                        <h4 class="headSettings_head">{ci_language line="suppliers"}</h4>						
                    </div>
                </div>
            {*</div>
            <div class="row">*}
                <div class="col-sm-12 relative">
                    <p class="head_text">
                        <i class="fa fa-remove true-pink"></i>
                        {ci_language line="excluded_suppliers"}
                    </p>
                    <select id="suppliers-1" name="suppliers[]" multiple="multiple" class="col-xs-12 {*includeBlock*} chosen-select" data-placeholder="{ci_language line="excluded_suppliers"}" data-no_results_text="{ci_language line="Oops, nothing found!"}">
                        {foreach from=$exclude_suppliers key=ex_key_suppliers item=ex_value_suppliers}
                            <option value="{$ex_value_suppliers.id_supplier}" selected="selected">{$ex_value_suppliers.name}</option>
                        {/foreach}
                        {if isset($include_suppliers)} {foreach from=$include_suppliers key=in_key_suppliers item=in_value_suppliers}
                                <option value="{$in_value_suppliers.id_supplier}">{$in_value_suppliers.name}</option>
                            {/foreach} {/if}
                        </select>
                        {*<i class="blockRight include_suppliers"></i>*}
                    </div>
                    {*<div class="col-sm-6 relative">
                    <p class="head_text">
                    <i class="fa fa-check true-green"></i>
                    {ci_language line="included_suppliers"}
                    </p>
                    <select id="suppliers-2" multiple="multiple" class="col-xs-12 includeBlock">
                    {if isset($include_suppliers)} {foreach from=$include_suppliers key=in_key_suppliers item=in_value_suppliers}
                    <option value="{$in_value_suppliers.id_supplier}">{$in_value_suppliers.name}</option>
                    {/foreach} {/if}
                    </select>
                    <i class="blockLeft exclude_suppliers"></i>
                    </div>*}
                {*</div>
                <div class="row">*}
                </div>                

                {if isset($mode_default) && $mode_default == 1 }
                    {include file="helpers/action_save_continue.tpl" data='category'}
                {else}
                    {include file="helpers/action_save_continue.tpl" data='mapping/manufacturers'}
                {/if}

                </div>
            </form>
        </div>
        {include file="footer.tpl"}

        <!--inline scripts related to this page-->
        <script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/products/filters.js"></script>    