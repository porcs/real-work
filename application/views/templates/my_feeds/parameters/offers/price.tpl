{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/my_feed/price.css" />
<div {if !isset($popup)}id="content"{else} class="row"{/if}>
    <div class="heading-buttons bg-white border-bottom innerAll">
        <h1 class="content-heading padding-none pull-left {if isset($popup)}margin-none{/if}">{ci_language line="Price modifier"}</h1>
        <div class="clearfix"></div>
    </div>
    {include file="popup_step.tpl"} 
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <div class="innerAll spacing-x2">
        <div class="row-fluid">                
            <div class="col-md-12">
                <div class="alert alert-info">
                    <h5 class="strong text-uppercase"><i class="fa fa-info-circle"></i> {ci_language line="Information"}</h5> 
                    <ul class="list-unstyled text-small">
                        {*<li class="list-icon"><i class="fa fa-caret-right"></i> {ci_language line="Use a familiar name to remember it."}</li>*}
                        <li class="list-icon"><i class="fa fa-caret-right"></i> {ci_language line="Formula to be applied on all exported products prices (multiplication, division, addition, subtraction, percentages)."}</li>
                        <li class="list-icon"><i class="fa fa-caret-right"></i> {ci_language line="Apply a specific price formula to selected categories which will override the main setting."}</li>
                        <li class="list-icon"><i class="fa fa-caret-right"></i> {ci_language line="Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required"}</li>
                    </ul>
                </div>

                <div class="widget widget-inverse">
                                                      
                    {if !isset($popup)}
                    <div class="widget-head">
                        <h3 class="heading show_thumbnails"><i class="fa fa-money"></i> {ci_language line="price"}</h3>
                    </div><!-- widget-head -->
                    {/if}

                    <div class="widget-body">

                        {if !isset($shop_default)}
                            <div class="alert alert-warning">
                                <h5><i class=" fa fa-warning"></i> <b>{ci_language line="Warning!"}</b></h5> 
                                {if isset($popup)} 
                                    {ci_language line="Please wait until importing is successfully completed"}
                                    <input id="no_shop_def" type="hidden" value="{ci_language line='Please wait until importing is successfully completed'}"> {else} {ci_language line="Please choose default shop."} 
                                {/if}
                            </div>
                        {else}
                            <div class="row-fluid">
                                <div class="col-xs-6 text-left margin-bottom15"  >
                                    <button type="button" class="btn btn-success btn-stroke" id="add">
                                        <i class="fa fa-plus"></i> {ci_language line="Add a price to the list"}
                                    </button>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <h4 class="row-fluid smaller lighter blue">
                                        <label class="pull-right inline">
                                            <input id="count" value="{if isset($price)}{sizeof($price)}{else}0{/if}" readonly="">
                                            {ci_language line="Price(s)"}
                                            <i class="fa fa-th-large"></i>
                                        </label>
                                    </h4>
                                </div>
                            </div>
                            <form action="{$base_url}offers/price_save{if isset($popup)}/{$popup}{/if}" method="post" id="" autocomplete="off" class="form-horizontal" novalidate>
                                <ul id="tasks" class="menubar item-list">
                                    <li class="item-orange clearfix display-none" id="main"  >
                                        <div class="text-right {if !isset($popup)} pop_sytle1{/if}" >
                                            <button type="button" class="btn btn-danger remove">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{ci_language line="Price name"}</label>
                                            <div class="col-md-10">
                                                <input type="text" rel="name" required class="form-control {if isset($popup)}span8{/if}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{ci_language line="Price modifier"}</label>
                                            <div class="col-md-10">
                                                <div class="col-xs-4 padding-none">
                                                    <input type="text" class="form-control" value="{ci_language line=" Percentage "}" disabled>
                                                </div>
                                                <div class="col-xs-8">
                                                    <div class="input-group">
                                                        <input type="text" rel="price_percentage" class="input-mini form-control"><span class="input-group-addon"> % </span>
                                                    </div>
                                                </div>
                                                <span class="help-inline display-none" >{ci_language line="Allow only digits."}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <div class="col-xs-4 padding-none">
                                                    <input type="text" class="input-small form-control" value="Value" disabled>
                                                </div>
                                                <div class="col-xs-8">
                                                    <input type="text" rel="price_value" class="input-mini form-control form-control">
                                                </div>
                                                <span class="help-inline display-none"  >{ci_language line="Allow only digits."}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 text-right">{ci_language line="Rounding"}</label>
                                            <div class="col-md-10">
                                                <label class="col-sm-2">
                                                    <input rel="rounding" type="radio" value="1" class="va_sub" />
                                                    <span class="lbl"> {ci_language line="One digit"}  </span>
                                                </label>

                                                <label class="col-sm-2">
                                                    <input rel="rounding" type="radio" value="2" class="va_sub" />
                                                    <span class="lbl"> {ci_language line="Two digit"}  </span>
                                                </label>

                                                <label class="col-sm-2">
                                                    <input rel="rounding" type="radio" checked value="0" class="va_sub" />
                                                    <span class="lbl"> {ci_language line="None"}  </span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                    {if isset($price)} {foreach from=$price key=key item=value}

                                            <li class="item-orange clearfix">
                                                <div class="text-right {if !isset($popup)} pop_style1{/if}" >
                                                    &nbsp;
                                                    <button type="button" class="btn btn-danger remove" value="{$value.id_profile_price}" rel="{$value.name}">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">{ci_language line="Price name"}</label>
                                                    <div class="col-md-10">
                                                        <input type="text" rel="name" name="price[{$key}][name]" value="{$value.name}" class="form-control">
                                                        <input type="hidden" rel="id_profile_price" name="price[{$key}][id_profile_price]" value="{$value.id_profile_price}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">{ci_language line="Price modifier"}</label>
                                                    <div class="col-md-10">
                                                        <div class="col-xs-4">
                                                            <input type="text" class="form-control" value="Percentage" disabled>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <div class="input-group">
                                                                <input type="text" rel="price_percentage" class="form-control" name="price[{$key}][price_percentage]" value="{$value.price_percentage}"><span class="input-group-addon"><strong> % </strong></span></span>
                                                            </div>
                                                        </div>
                                                        <span class="help-inline display-none">{ci_language line="Allow only digits."}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"></label>
                                                    <div class="col-md-10">
                                                        <div class="col-xs-4">
                                                            <input type="text" class="input-small form-control" value="Value" disabled>
                                                        </div>
                                                        <div class="col-xs-8">
                                                            <input type="text" rel="price_value" class="input-mini form-control" name="price[{$key}][price_value]" value="{$value.price_value}">
                                                        </div>
                                                        <span class="help-inline display-none" >{ci_language line="Allow only digits."}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 text-right">{ci_language line="Rounding"}</label>
                                                    <div class="col-md-10">
                                                        <label class="col-sm-2">
                                                            <input rel="rounding" name="price[{$key}][rounding]" type="radio" value="1" {if isset($value.rounding) && $value.rounding==1 } checked {/if} style="vertical-align: sub" />
                                                            <span class="lbl"> {ci_language line="One digit"}  </span>
                                                        </label>

                                                        <label class="col-sm-2">
                                                            <input rel="rounding" name="price[{$key}][rounding]" type="radio" value="2" {if isset($value.rounding) && $value.rounding==2 } checked {/if} style="vertical-align: sub" />
                                                            <span class="lbl"> {ci_language line="Two digit"}  </span>
                                                        </label>

                                                        <label class="col-sm-2">
                                                            <input rel="rounding" name="price[{$key}][rounding]" type="radio" value="0" {if isset($value.rounding) && $value.rounding==0 } checked {/if} style="vertical-align: sub" />
                                                            <span class="lbl"> {ci_language line="None"}  </span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </li>
                                        {/foreach} {/if}

                                    </ul>{*tasks*}

                                    <div class="space-6"></div>

                                    <!-- Alret Popup-->
                                    <input type="hidden" value="{ci_language line=" Do you want to delete "}" id="confirm-message" />
                                    <input type="hidden" value="{ci_language line=" Delete successfully "}" id="delete-success-message" />
                                    <input type="hidden" value="{ci_language line=" Deleted "}" id="deleted-message" />
                                    <input type="hidden" value="{ci_language line=" Error "}!" id="error-message" />
                                    <input type="hidden" value="{ci_language line=" Can not delete "}" id="unsuccess-message" />
                    
                    
                                    <div class="form-group">

                                        <div class="col-md-12 text-right">

                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-save"></i> {if !isset($popup)}{ci_language line="save"}{else}{ci_language line="Save and Continue"}{/if}
                                            </button>
                                        </div>
                                    </div>

                                </form>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{include file="footer.tpl"}
<script src="{$cdn_url}assets/js/bootbox.min.js?v=v1.2.3"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/offers/price.js"></script>  