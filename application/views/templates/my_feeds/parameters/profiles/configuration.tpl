{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
    <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Profiles"}</h1>
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->

    <form action="{$base_url}profiles/configuration_save" method="post" id="configuration" autocomplete="off" novalidate>
        <div class="row">
            {*<div class="col-xs-12">
            <div class="validate blue">
            <div class="validateRow">
            <div class="validateCell">
            <i class="note"></i>
            </div>
            <div class="validateCell">
            <p class="pull-left">{ci_language line="Use a familiar name to remember it."}</p>
            <i class="fa fa-remove pull-right"></i>
            </div>
            </div>
            </div>
            </div>*}
        </div>
        {if empty($rules)}
            <div class="row">
                <div class="col-xs-12">
                    <div class="validate yellow">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left"><span class="bold">{ci_language line="Rules doesn't exits:"}</span> {ci_language line="Missing Rules to map with Category. To create a new rule please go to My Feeds &gt; Parameters &gt; Rules."} </p>
                                <i class="fa fa-remove pull-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}

        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top b-Bottom p-t10 p-b10">
                    <a href="" class="link showProfile p-size">+ {ci_language line="Add a profile to the list"}</a>
                    <p class="pull-right montserrat poor-gray"><span class="dark-gray countProfile"></span> {ci_language line="Profile(s)"}</p>
                </div>
            </div>
        </div>

        <div class="row showSelectBlock profiles" rel="0">
            <div class="col-xs-12 custom-form m-t10">
                <a href="" class="removeProfile">{ci_language line="Remove a profile from list"}</a>
                <div class="profileBlock clearfix">
                    <p class="regRoboto poor-gray">{ci_language line="Profile name"}</p>
                    <div class="form-group has-error">
                        <input type="text" class="form-control" rel="name" required>
                        <div class="error text-left">
                            <p>
                                <i class="fa fa-exclamation-circle"></i>
                                <span rel="err_rule_name_blank">{ci_language line="Please enter rule name first"}</span><span rel="err_rule_name_duplicate" class="hide">{ci_language line="Please enter unique rule name"}</span>
                            </p>
                        </div>
                    </div>
                    <div class="pull-right">
                        <label class="cb-checkbox p-t10 m-r10">
                            <input type="checkbox" value="1" rel="is_default">
                            {ci_language line="Default Profile"}
                        </label>
                        <button type="button" class="btn btn-rule m-b10 m--t5 add-rule">+ {ci_language line="Add Rule Items(s)"}</button>
                    </div>
                    <div class="main-item form-group display-none">
                        <div class="col-xs-11 p-l0 m-l0">
                            <select rel="id_rule" id="id_rule{if isset($key)}{$key}{/if}_additem" required>
                                <option value=""></option>
                                {if isset($rules)}
                                    {foreach from=$rules key=rkey item=rvalue}
                                        <option value="{$rvalue.id_rule}">{$rvalue.name}</option>
                                    {/foreach}  
                                {/if}
                            </select>
                        </div>
                        <div class="col-xs-1">
                            <i class="cb-plus bad remove-rule"></i>
                        </div>
                    </div>
                    <div class="rule-item"></div>
                </div>
            </div>
        </div>


        {if isset($profiles)}
            {foreach from=$profiles key=key item=value}              
                <div class="row showSelectBlock profiles" style="display: block;" rel="{$key}">
                    <div class="col-xs-12 custom-form m-t10">
                        <a href="" class="removeProfile" value="{$value.id_profile}" rel="{$value.name}">{ci_language line="Remove a profile from list"}</a>
                        <div class="profileBlock clearfix">
                            <p class="regRoboto poor-gray">{ci_language line="Profile name"}</p>
                            <div class="form-group">
                                <input type="text" class="form-control" rel="name" name="profile[{$key}][name]" value="{$value.name}" required>
                                <div class="error text-left hide">
                                    <p>
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span rel="err_rule_name_blank">{ci_language line="Please enter rule name first"}</span><span rel="err_rule_name_duplicate" class="hide">{ci_language line="Please enter unique rule name"}</span>
                                    </p>
                                </div>

                                <input type="hidden" rel="id_profile" name="profile[{$key}][id_profile]" value="{$value.id_profile}">
                            </div>
                            <div class="pull-right">
                                <label class="cb-checkbox p-t10 m-r10 {if isset($value.is_default) && $value.is_default == 1} checked {/if} >">
                                    <input type="checkbox" value="1" rel="is_default" name="profile[{$key}][is_default]" {if isset($value.is_default) && $value.is_default == 1} checked {/if} >
                                    {ci_language line="Default Profile"}
                                </label>
                                <button type="button" class="btn btn-rule m-b10 m--t5 add-rule">{ci_language line="+ Add Rule Items(s)"}</button>
                            </div>

                            <div class="main-item form-group display-none">
                                <div class="col-xs-11 p-l0 m-l0">
                                    <select rel="id_rule" id="id_rule{$key}_additem" required>
                                        <option value=""></option>
                                        {if isset($rules)}
                                            {foreach from=$rules key=rkey item=rvalue}
                                                <option value="{$rvalue.id_rule}">{$rvalue.name}</option>
                                            {/foreach}  
                                        {/if}
                                    </select>
                                </div>
                                <div class="col-xs-1">
                                    <i class="cb-plus bad remove-rule"></i>
                                </div>
                            </div>

                            {if isset($value.id_rule)}
                                {foreach from=$value.id_rule key=id_rule_key item=id_rule_value}
                                    <div class="form-group">
                                        <div class="col-xs-11 p-l0 m-l0">
                                            <select rel="id_rule" id="id_rule{$key}_{$id_rule_key}_{$id_rule_value.id_profile_item}" required class="search-select" name="profile[{$key}][rule][{$id_rule_value.id_profile_item}][id_rule]" data-content="{$key}">
                                                <option value=""></option>
                                                {if isset($rules)}
                                                    {foreach from=$rules key=rkey item=rvalue}
                                                        <option value="{$rvalue.id_rule}" {if isset($id_rule_value.id_rule) && $id_rule_value.id_rule == $rvalue.id_rule}{$profile_item = $rvalue.name} selected {/if}>{$rvalue.name}</option>
                                                    {/foreach}  
                                                {/if}
                                            </select>
                                        </div>
                                        <div class="col-xs-1">
                                            <i class="cb-plus bad remove-rule" value="{$id_rule_value.id_profile_item}" rel="{if isset($profile_item)}{$profile_item}{/if}"></i>
                                        </div>
                                    </div>
                                {/foreach}
                            {/if}
                            <div class="rule-item"></div>
                        </div>
                    </div>
                </div>
            {/foreach}
        {/if}

        <!-- Alret Popup-->
        <input type="hidden" value="{ci_language line="Do you want to delete"}" id="confirm-message" />
        <input type="hidden" value="{ci_language line="Delete successful"}" id="delete-success-message" />
        <input type="hidden" value="{ci_language line="Deleted"}" id="deleted-message" />
        <input type="hidden" value="{ci_language line="Error"}!" id="error-message" />
        <input type="hidden" value="{ci_language line="Can not delete"}" id="unsuccess-message" />

        {include file="helpers/action_save_continue.tpl" data='category'}

    </form>
</div>

{include file="footer.tpl"}
<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/bootbox.min.js"></script>

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/parameters/profiles/configuration.js"></script>