{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
 
<div {if !isset($popup)}id="content"{else} class="row"{/if}>
    <div class="heading-buttons bg-white border-bottom innerAll">
        <h1 class="content-heading padding-none pull-left {if isset($popup)}margin-none{/if}">{ci_language line="data_source"}</h1>
        <div class="clearfix"></div>
    </div>
        {include file="breadcrumbs.tpl"} <!--breadcrumb-->
        
    {include file="popup_step.tpl"}
    <!-- Tabs -->
    <div class="innerAll spacing-x2" >
        <form action="{$base_url}my_feeds/data_source_save" method="post" id="mainconfigform" >
            <div class="widget widget-tabs widget-tabs-responsive">

                {if !isset($popup) || !$popup}
                    <div class="widget-head">
                            <ul>
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-rss"></i> {ci_language line="Feed data"}</a></li>
                                    {if $feed_mode != 1}
                                    <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-cloud-download"></i> {ci_language line="Import mode"}</a></li>
                                    {/if}
                                <li><a href="#tab-3" data-toggle="tab"><i class="fa fa-exchange"></i> {ci_language line="Import history"}</a></li>
                            </ul>
                    </div><!-- Tabs Heading -->
                {/if}
                
                <div class="widget-body">
                    {if !isset($popup) || !$popup} 
                        {if !isset($feed_biz) || empty($feed_biz) }
                            <div class="alert alert-block alert-warning">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <i class="fa fa-warning-sign"></i>
                                <b>{ci_language line="Warning"} : </b> {ci_language line="No data source."}
                            </div>
                        {/if} 
                    {/if}

                    <div class="tab-content">                        
                        <div class="tab-pane active" id="tab-1">
                            <div class="tab-content  profile-edit-tab-content overflow-hidden">
                                <div id="tab_feed" class="tab-pane in active">                                    
                                    <div class="widget-box transparent collapsed" id="standard">
                                        <div class="widget-header" {if isset($popup) && $popup} style="display:none" {/if}>
                                            <label>
                                                <input name="source" type="radio" value='standard' id="input_standard" checked style="vertical-align: sub">
                                                <span class="lbl bigger-120"> {ci_language line="standard_feed_source"}</span>
                                            </label>
                                        </div>

                                        <div class="widget-body " >
                                            <div class="widget-body-inner">
                                            
                                                <div class="widget-main form-horizontal" id="standard_source">
                                                    
                                                    <div id="feed_url" class="">
                                                        <h5 class="bolder">{ci_language line="URL provided by your Feed.biz module"}</h5>
                                                        <div class="row-fluid">
                                                            <div class="form-group margin-right-none margin-left-none">
                                                                <input type="text" class="form-control valid" id="base_url" name="base_url" value="{if isset($feed_biz.source) && $feed_biz.source == "standard" &&  isset($feed_biz.base_url)}{$feed_biz.base_url}{/if}">
                                                                <input type="hidden" name="verified" value="{if isset($feed_biz.verified)}{$feed_biz.verified}{/if}">
                                                            </div>
                                                            <div class="form-group margin-right-none margin-left-none">
                                                                <div class="pull-left" >
                                                                    <button type="button" class="btn_ver btn btn-stroke{if isset($feed_biz.verified) && $feed_biz.verified!=""} btn-success verified{else}btn-warning{/if}">
                                                                        <i class="icon_ver {if isset($feed_biz.verified)&&$feed_biz.verified!=""}fa fa-check{else}fa fa-globe{/if}"></i>
                                                                        <i class="icon_chk fa fa-spinner fa-spin orange" class="icon_chk" style="display:none"></i>
                                                                        <span class="txt_ver">
                                                                            {if isset($feed_biz.verified)&&$feed_biz.verified!=""}
                                                                                {ci_language line="Verified"}
                                                                            {else}
                                                                                {ci_language line="Verify"}
                                                                            {/if}
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                                {if isset($feed_biz.verified) && $feed_biz.verified!=""}
                                                                    {if !isset($popup) || !$popup} 
                                                                        <div class="pull-right">
                                                                            <button type="button" class="btn_import_product btn_import btn btn-success btn-stroke" rel="feed">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import products now"}!
                                                                                </span>
                                                                            </button>
                                                                        
                                                                            <button type="button" class="btn_import_offer btn_import btn btn-success btn-stroke"  rel="offer">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import offers now"}! 
                                                                                </span>
                                                                            </button>
                                                                        </div> 
                                                                    {else}
                                                                        <div class="pull-right col-sd-4">
                                                                            <button type="button" class="btn_import_product btn_import btn btn-success btn-stroke" rel="feed">
                                                                                <i class="fa fa-cloud-download"></i> 
                                                                                <span class="txt_import"> 
                                                                                    {ci_language line="Import NOW"}! 
                                                                                </span>
                                                                            </button>
                                                                        </div> 
                                                                    {/if}                                                                

                                                                {/if}
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div id="import_feed_msg" class="alert alert-warning" style="display: none"> 
                                                        <strong><i class="fa fa-comments"></i></strong>
                                                        <div class="msg"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {if !isset($popup) || !$popup}<div class="widget-box transparent collapsed  {if (isset($feed_biz.source) && $feed_biz.source !="custom") || !isset($feed_biz.source)} collapsed {/if}" id="custom" {if isset($popup) && $popup} style="display:none" {/if}>

                                        <div class="widget-header">
                                            <label>
                                                <input name="source" type="radio" value='custom' id="input_custom" {if isset($feed_biz.source) && $feed_biz.source =="custom"}checked{/if} style="vertical-align: sub" />
                                                <span class="lbl bigger-120"> {ci_language line="custom_feed_source"}</span>
                                            </label>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-body-inner">
                                                <div class="widget-main">
                                                    <div class="row-fluid">
                                                        <div>
                                                            <label class="radio-custom">
                                                                <i class="fa fa-fw fa-circle-o"></i>
                                                                <input name="feed_source" type="radio" value="XML" {if isset($feed_biz.feed_source) && $feed_biz.feed_source == "XML" }checked="checked" {/if}>
                                                                <span class="lbl"> {ci_language line="xml"}</span>
                                                            </label>
                                                            <div class="content well no-border" style="display:none">
                                                                &nbsp;
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <label class="radio-custom">
                                                                <i class="fa fa-fw fa-circle-o"></i>
                                                                <input name="feed_source" type="radio" value="CSV" {if isset($feed_biz.feed_source) && $feed_biz.feed_source == "CSV" }checked="checked" {/if}>
                                                                <span class="lbl"> {ci_language line="csv"}</span>
                                                            </label>
                                                            <div class="content well no-border" style="display:none">
                                                                &nbsp;
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <label class="radio-custom">
                                                                <i class="fa fa-fw fa-circle-o"></i>
                                                                <input name="feed_source" type="radio" value="URL" {if isset($feed_biz.feed_source) && $feed_biz.feed_source == "URL" }checked="checked" {/if}>
                                                                <span class="lbl"> {ci_language line="URL"}</span>
                                                            </label>
                                                            <div class="content well no-border" style="display:none">
                                                                &nbsp;
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <label class="radio-custom">
                                                                <i class="fa fa-fw fa-circle-o"></i>
                                                                <input id="feed_source_gmerchant" name="feed_source" type="radio" value="GMerchant" {if isset($feed_biz.feed_source) && $feed_biz.feed_source == "GMerchant" }checked="checked" {/if}>
                                                                <span class="lbl"> {ci_language line="gmerchant"}</span>
                                                            </label>

                                                            <div class="content well form-horizontal no-border" id="gmerchant" {if isset($feed_biz.feed_source) && $feed_biz.feed_source != "GMerchant" }style="display:none"{/if} >

                                                                <h5 class="smaller green">{ci_language line="Google merchant - Basic settings"}</h5>
                                                                <hr/>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label" for=""> {ci_language line="language"}</label>
                                                                    <div class="col-md-10">
                                                                        <select name="language" id="language" class="col-xs-10" data-placeholder="{ci_language line="language"}">

                                                                            {if isset($feed_biz.product.language)}
                                                                                <option value="{$feed_biz.product.language}" selected/>{$feed_biz.product.language}
                                                                            {else}
                                                                                <option value="" />
                                                                            {/if}
                                                                            <option value="United States" data-iso='us' data-curr-name="US Dollar" data-curr-iso="USD" />United States
                                                                            <option value="United Kingdom" data-iso='en' data-curr-name="British Pound" data-curr-iso="GBP" />United Kingdom
                                                                            <option value="Australia" data-iso='au' data-curr-name="Australian Dollar" data-curr-iso="AUD" />Australia 
                                                                            <option value="France" data-iso='fr' data-curr-name="Euro" data-curr-iso="EUR" />France 
                                                                            <option value="Germany" data-iso='de' data-curr-name="Euro" data-curr-iso="EUR" />Germany 
                                                                            <option value="Italy" data-iso='it' data-curr-name="Euro" data-curr-iso="EUR"  />Italy 
                                                                            <option value="Netherlands" data-iso='nl' data-curr-name="Euro" data-curr-iso="EUR" />Netherlands 
                                                                            <option value="Spain" data-iso='es' data-curr-name="Euro" data-curr-iso="EUR" />Spain
                                                                            <option value="Switzerland" data-iso='ch' data-curr-name="Swiss Franc" data-curr-iso="CHF" />Switzerland 
                                                                            <option value="Czech Republic" data-iso='cs' data-curr-name="Czech Crown" data-curr-iso="CZK" />Czech Republic
                                                                            <option value="Japan" data-iso='ja' data-curr-name="Yen" data-curr-iso="JPY" />Japan 
                                                                            <option value="Brazil" data-iso='br' data-curr-name="Brazilian Reals" data-curr-iso="BRL"  />Brazil 
                                                                            <option value="Canada" data-iso='ca' data-curr-name="Canadian Dollar" data-curr-iso="CAD"  />Canada 
                                                                            <option value="India" data-iso='in' data-curr-name="Indian Rupees" data-curr-iso="INR" />India 
                                                                            <option value="Russia" data-iso='ru' data-curr-name="Russian rubles" data-curr-iso="RUB" />Russia 
                                                                            <option value="Sweden" data-iso='se' data-curr-name="Swedish Crown" data-curr-iso="SEK" />Sweden 
                                                                            <option value="Austria" data-iso='at' data-curr-name="Euro" data-curr-iso="EUR" />Austria 
                                                                            <option value="Belgium" data-iso='be' data-curr-name="Euro" data-curr-iso="EUR" />Belgium 
                                                                            <option value="Denmark" data-iso='dk' data-curr-name="Danish Crown" data-curr-iso="DKK" />Denmark 
                                                                            <option value="Mexico" data-iso='mx' data-curr-name="Mexican Pesos" data-curr-iso="MXN"  />Mexico  
                                                                            <option value="Norway" data-iso='no' data-curr-name="Norvegian Crown" data-curr-iso="NOK" />Norway  
                                                                            <option value="Poland" data-iso='pl' data-curr-name="Zloty" data-curr-iso="PLN" />Poland  
                                                                            <option value="Turkey" data-iso='tr' data-curr-name="Turkish Lira" data-curr-iso="TRY"  />Turkey  
                                                                        </select> 
                                                                        &nbsp; &nbsp;      
                                                                        <input type="checkbox" name="is_default" id="is_default" onclick="return false;" checked="checked" value="1" style="vertical-align: sub">
                                                                        <label class="lbl" for="is_default"> {ci_language line="Default"}</label>    
                                                                    </div>
                                                                </div>

                                                                <input type="hidden" name="iso_code" id="iso" {if isset($feed_biz.product.iso_code)} value="{$feed_biz.product.iso_code}"{/if}/>
                                                                <input type="hidden" name="curr_name" id="curr_name" {if isset($feed_biz.product.curr_name)} value="{$feed_biz.product.curr_name}" {/if}/>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label" for=""> {ci_language line="Currency code"}</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" class="input-mini form-control" name="curr_iso" id="curr_iso" readonly {if isset($feed_biz.product.curr_iso)} value="{$feed_biz.product.curr_iso}" {/if}/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label" for=""> {ci_language line="product_url"}</label>
                                                                    <div class="col-md-10">
                                                                        <textarea class="span10" id="gmerchant_file_url" name="gmerchant_file_url">{if  isset($feed_biz.feed_source) && $feed_biz.feed_source == "GMerchant" && isset($feed_biz.product.fileurl)}{$feed_biz.product.fileurl}{/if}</textarea>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>{/if}
                                </div>
                            </div>
                        </div>

                        {if $feed_mode != 1}
                            <div class="tab-pane" id="tab-2">

                                <div id="feed_mode" class="tab-pane in ">
                                    <div id="mode">
                                        <h4 class="innerAll half heading-buttons border-bottom">
                                            {ci_language line="Import mode"}
                                        </h4>

                                        <!-- Beginner -->
                                        <div class="itemdiv modebox">
                                            <div class="user text-right ">
                                                <label class="radio-custom">
                                                    <i class="fa fa-fw fa-circle-o"></i>
                                                    <input name="mode" class="ace-checkbox-3" type="radio" value="1" {if isset($status) && $status == 1}checked rel="checked"{/if}>
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                            <div class="body">
                                                <div class="name bigger-140 green">
                                                    <span rel="mode">{ci_language line="Auto importing"}</span>
                                                    <span class="label label-primary">{ci_language line="Recommend"}</span>
                                                </div>
                                                <div class="text">
                                                    <i class="fa fa-quote-left"></i>
                                                    {ci_language line="Our system will offen update your products."}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Expert -->
                                        <div class="itemdiv modebox">
                                            <div class="user text-right ">
                                                <label class="radio-custom">
                                                    <i class="fa fa-fw fa-circle-o"></i>
                                                    <input name="mode" class="ace-checkbox-3" type="radio" value="0" {if isset($status) && $status == 0}checked rel="checked"{/if}>
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                            <div class="body">
                                                <div class="name bigger-140 green">
                                                    <span rel="mode">{ci_language line="Manual import"}</span>
                                                </div>
                                                <div class="text">
                                                    <i class="fa fa-quote-left"></i>
                                                    {ci_language line="Your products will be updated after clicking the save button."}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Tab content -->
                        {/if}  

                        <div class="tab-pane" id="tab-3">
                            <table class="dynamicTable colVis table">
                                <thead class="bg-gray">
                                    <tr>
                                        <th style="width: 20%;">
                                            <i class="fa fa-caret-right blue blue"></i>
                                            {ci_language line="shop_name"}
                                        </th>
                                        <th style="width: 20%;">
                                            <i class="fa fa-caret-right blue blue"></i>
                                            {ci_language line="task"}
                                        </th>
                                        <th style="width: 20%;">
                                            <i class="fa fa-caret-right blue blue"></i>
                                            {ci_language line="running_by"}
                                        </th>
                                        <th style="width: 20%;">
                                            <i class="fa fa-caret-right blue blue"></i>
                                            {ci_language line="time"}
                                        </th> 
                                    </tr>
                                </thead>
                                {if $history} 
                                    <tbody>
                                        {foreach $history as $val} 
                                            <tr>
                                                <td class="left">{$val['history_table_name']}</td>
                                                <td class="left">{str_replace('_',' ',$val['history_action'])}</td>
                                                <td class="left">{($val['user_name'] )}</td>
                                                <td class="left">{($val['history_date_time'] )}</td> 
                                            </tr>
                                        {/foreach}

                                    </tbody>
                                {/if}
                            </table>
                        </div>
                    </div><!-- Tab content -->
                    
                    <div class=" row-fluid">
                        <div class="span12 text-right">
                            <input type="hidden" id="verifying" value="{ci_language line="please_wait_for_verifying"}" >
                            <input type="hidden" id="verify" value="{ci_language line="verify"}" >
                            <input type="hidden" id="check" value="{ci_language line="please_check_url_and_verify_again"}" >
                            <input type="hidden" id="error_connection" value="{ci_language line="can_not_connect_server"}" >
                            <input type="hidden" id="empty_url" value="{ci_language line="please_paste_your_uri_connector"}">
                            <input id="status-confirm" type="hidden" value="{ci_language line="Are you sure to change mode"}" />
                            <input id="status-message" type="hidden" value="{ci_language line='Please choose mode'}" />
                            <input id="all-process-complete" type="hidden" value="{ci_language line='All importing are completed.'}" />
                            <input id="some-process-running" type="hidden" value="{ci_language line='Your process are running. Please wait.'}" />
                            {if !isset($popup) || !$popup}
                                <button class="btn btn-info" type="submit" id="save-button" style="margin-top: 15px" >
                                    <i class="fa fa-save"></i>
                                    {ci_language line="save_feed_data"}
                                </button>
                            {/if}
                        </div>
                    </div>
                </div><!-- // Tabs Heading END -->

            </div>
        </form>
    </div>
</div>                   

<input type="hidden" id="userdata" value="{if isset($profile)}{$profile}{/if}" />

{if isset($profile)}
    <script src="{$cdn_url}assets/js/FeedBiz/users/authenticate.js"></script>
{/if}

<!--page specific plugin scripts-->
<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/bootbox.min.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/data_feed_status.js"></script>
<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/data_source.js"></script>
    {*if isset($start_feed) && $start_feed == "true"*}
        <script src="{$cdn_url}assets/js/FeedBiz/my_feeds/configuration/data_feed.js"></script>
    {*/if*} 

{include file="footer.tpl"}