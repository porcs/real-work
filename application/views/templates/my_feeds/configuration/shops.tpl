{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main-content">    
    {include file="breadcrumbs.tpl"} <!--breadcrumb-->    
    <div class="page-content">            
        <div class="page-header position-relative"><h1>{ci_language line="shops"}</h1></div>
        <div class="row-fluid">            
            <div class="offset1 span10">                
                <!--general-->
                <div id="general">
                    <!-- Shop List -->
                    {if isset($shop) && !empty($shop)}
                        <h4 class="header smaller lighter blue">
                               <i class="fa fa-check"></i> {ci_language line="Choose shop default"}
                        </h4>

                        <form action="{$base_url}my_feeds/shops_save" method="post" id="shopform" >
                            {foreach from=$shop key=shop_key item=shop_value}
                                <div class="itemdiv shopbox">
                                    <div class="user text-right ">
                                        <label>
                                            <input name="id_shop" class="ace-checkbox-4" type="checkbox" value="{$shop_value.id_shop}" {if isset($shop_value.is_default) && $shop_value.is_default == 1}checked rel="checked"{/if}>
                                            <span class="lbl"></span>
                                        </label>
                                    </div>
                                    <div class="body">
                                        <div class="name blue bigger-120">
                                             <span class="shop">{$shop_value.name}</span>
                                            {if isset($shop_value.is_default) && $shop_value.is_default == 1}
                                                 <span class="label label-important arrowed arrowed-in-right">{ci_language line="Default Shop"}</span>
                                            {/if}
                                        </div>
                                       
                                        <div class="row-fluid">
                                            <div class="space-2"></div>
                                            <ul class="unstyled">
                                                <li>
                                                    <i class="icon-double-angle-right"></i> 
                                                    {ci_language line="product"}<br/>
                                                    &nbsp; &nbsp; &nbsp; &nbsp;<i class="icon-link blue"></i>  {$shop_value.description}
                                                </li>
                                                {if isset($shop_value.offer)}
                                                <li class="offer">
                                                    <i class="icon-double-angle-right"></i>
                                                    {ci_language line="offer"}<br/>
                                                    &nbsp; &nbsp; &nbsp; &nbsp;<i class="icon-link blue"></i>  {$shop_value.offer_link}
                                                </li>
                                                {/if}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr hr-6 dotted"></div>
                            {/foreach} 
                           
                            <div class="form-actions center">
                                <input type="hidden" name="username" id="username" value="{$username}" /> 
                                <input type="hidden" name="id_user" id="id_user" value="{$id_user}" />
                                <input type="hidden" name="please_choose_feed_source" id="please_choose_feed_source" value="{ci_language line='please_choose_feed_source'}" />
                                <input type="hidden" name="q1" id="q1" value="{ci_language line="Do you want to change default shop"}?" />
                                <input type="hidden" name="Error" id="Error" value="{ci_language line='Error'}!" />
                                <input type="hidden" name="Cannot_change_shop_offer" id="Cannot_change_shop_offer" value="{ci_language line="Cannot change shop offer"}" />
                                <button class="btn btn-info" type="submit" id="save-button">
                                   <i class="icon-ok bigger-110"></i>
                                   {ci_language line="save"}
                                </button>
                            </div><!--form-actions-->
                            
                            </form>
                        <div class="space-10"></div>  
                        
                    {else}
                        
                        <div class="alert alert-block alert-warning">
                           <button type="button" class="close" data-dismiss="alert">
                               <i class="icon-remove"></i>
                           </button>
                           <i class="icon-warning-sign bigger-120"></i>
                           <b>{ci_language line="Warning"} : </b> {ci_language line="No shop in database!"}
                       </div>
                       
                    {/if}
                </div>
            </div>
             
        </div>{*row-fluid*}

    </div>{*page-content*}
    
</div>{*main-content*}

<script src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
<script src="{$cdn_url}assets/js/bootbox.min.js"></script>

{include file="footer.tpl"}