{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="pagree_page_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->
                
                <form method="post" id="frmAgree" action="{$base_url}billing/preapproval">
		<div class="row">
			<div class="col-sm-8">
				<div class="payments">
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_title_name"}</p>
						</div>
						<div class="payments_content">
							<p>{ci_language line="pagree_title_detail"}</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_start_date"}</p>
						</div>
						<div class="payments_content">
							<p>{$startDate}</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_end_date"}</p>
						</div>
						<div class="payments_content">
							<p>{$endDate}</p>
						</div>
					</div>	
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_max_per"}</p>
						</div>
						<div class="payments_content">
							<p>{$amount}</p>
						</div>
					</div>	
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_total"}</p>
						</div>
						<div class="payments_content">
							<p>{$maxAmount}</p>
						</div>
					</div>
					<div class="payment_row clearfix">
						<div class="payments_title">
							<p>{ci_language line="pagree_pay_round"}</p>
						</div>
						<div class="payments_content">
							<p>{$maxNumber}</p>
						</div>
					</div>					
				</div>
			</div>
			<div class="col-sm-4">
				<div class="clearfix pull-sm-right checkIn">
					<a href=""><img class="pull-left m-rb10" src="{$cdn_url}assets/images/pay/pay-pal.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="{$cdn_url}assets/images/pay/mastercard.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="{$cdn_url}assets/images/pay/visa.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="{$cdn_url}assets/images/pay/express.png" alt=""></a>
					<a href=""><img class="pull-left m-rb10" src="{$cdn_url}assets/images/pay/discover.png" alt=""></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
                             <input type="hidden" name="hash" id="hash" value="{$hash}">
				<div class="p-t20">
					<div class="pull-right ">
						<a href="" class="pull-left link p-size m-tr10 btn-cancel">
							{ci_language line="pagree_btn_disagree"}
						</a>
						<button type="button" class="btn btn-save btn-savechanges">{ci_language line="pagree_btn_agree"}</button>                   
					</div>
				</div>		
			</div>
		</div>
        </form>
</div>
                        
<script language="javascript">
    var siteurl = "{$base_url}";
</script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.blockUI.js"></script>
<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.billing.preapproval.js"></script>
{include file="footer.tpl"}