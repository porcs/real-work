{ci_config name="base_url"}

{if isset($message)}    
    {if isset($message['type'])}
        <p class="progress_text">{$message['type']}</p>  
        {if isset($message['date']) && isset($message['time'])}
            <div class="row">
                <div class="col-xs-12">
                    <div class="b-Bottom m-b10">
                        <div class="row">
                            <div class="col-sm-6 ">
                                <div class="progress-date b-BlRight">
                                    <p class="progress-date_text"><span>Date:</span> {$message['date']}</p>
                                </div>                          
                            </div>
                            <div class="col-sm-6">
                                <div class="progress-date">
                                    <p class="progress-date_text"><span>Time:</span> {$message['time']}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if} 
    {/if} 
       
    {if isset($message['no_product'])}
        <p class="progress_text m-0 error">{$message['no_product']}</p>
    {else}

        {if isset($message['error']) && !empty($message['error'])}
            <div class="row">
                <div class="col-xs-12">
                    <p class="progress_text error">{ci_language line="Errors"} : </p>
                    <ol class="progress-list_error">
                        {foreach $message['error'] as $k_error => $msg_error}
                            <li><p>{$msg_error}</p></li>
                        {/foreach}
                    </ol>     
                </div>
            </div>           
        {/if} 

        {if isset($message['message']) && !empty($message['message'])}
            <div class="row">
                <div class="col-xs-12">
                    {*<p class="progress_text warning">{ci_language line="Messages"} : </p>*}
                    <ol class="progress-list">
                        {foreach $message['message'] as $k_message => $msg_message}
                            <li><p>{$msg_message}</p></li>
                        {/foreach} 
                    </ol>
                </div>
            </div>                            
        {/if} 

        {if isset($message['warning'])&& !empty($message['warning'])}
            <div class="row">
                <div class="col-xs-12">
                    <p class="progress_text warning">{ci_language line="Warnings"}</p>
                    <ol class="progress-list_warning">
                        {foreach $message['warning'] as $k_warning => $msg_warning}
                            <li><p>{$msg_warning}</p></li>
                        {/foreach}
                    </ol>
                </div>
            </div>
        {/if}  
                    
        {if isset($message['error']) || isset($message['message']) || isset($message['warning'])}
            {if isset($message['country']) && isset($message['all_message'])}
                <div class="b-Bottom p-t10 p-b10 text-right">
                    <a href="{$base_url}general/logs/{$message['sub_marketplace']}/{$message['country']}{if isset($message['path_to_message'])}/0/Orders/warning/{$message['path_to_message']}{/if}" class="link text-uc p-size">{$message['all_message']} <i class="fa fa-long-arrow-right m-r10"></i></a>
                </div>
            {/if}
        {else}            

            <div class="">
                {foreach $message as $k => $msg}
                    {if $k != "result"}                        
                        {if $k == "product" || $k == "inventory" || $k == "price" || $k == "image" || $k == "relationship" || $k == "orders" || $k == "report" || $k == "OrderFulfillment" || $k == "reprice" || $k == "repricing" || $k == "shipping_override" || $k == "offers"}
                            {if isset($msg.no_product)}
                                <p class="progress_small-text">
                                    <i class="fa fa-remove"></i>  {ucwords(str_replace('_', ' ', $k))}, {$msg.no_product}
                                </p>
                            {else if isset($msg.error)}
                                <p class="progress_text m-0 error"> <i class="fa fa-warning"></i> {$msg.error}</p>
                            {else if isset($msg.message)}
                                {if isset($msg.status) && $msg.status == "Success"}
                                    <p class="progress_text true-pink"> 
                                        <i class="fa fa-check"></i>  {ucwords(str_replace('_', ' ', $k))}, {$msg.message} 
                                        {if isset($msg.FeedSubmissionResult.FeedSunmissionId)}- ID : {$msg.FeedSubmissionResult.FeedSunmissionId}{/if}  
                                    </p>
                                    {if isset($msg.Report)}
                                        <div class="b-Bottom p-t10 p-b10 text-right">
                                            <a href="{$base_url}general/report/{$msg.sub_marketplace}/{$msg.country}" class="link text-uc p-size">
                                                {$msg.Report} 
                                                <i class="fa fa-long-arrow-right m-r10"></i>
                                            </a>
                                        </div>
                                    {/if}
                                {else}
                                    <p class="progress_text true-pink">                             
                                        <i class="fa fa-spinner fa-spin"></i> {$msg.message}
                                    </p>
                                {/if}
                            {else}
                                <p class="progress_small-text">                             
                                    <i class="fa fa-check-empty"></i>  {ucwords(str_replace('_', ' ', $k))}, {ci_language line="waiting"}.
                                </p>
                            {/if}
                        {/if}
                    {/if}
                {/foreach}
            </div>
            
        {/if}
    {/if}     
{/if} 