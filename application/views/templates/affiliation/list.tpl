{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="aff_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->	
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">{ci_language line="aff_list_title"}</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="ID"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="User name"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Email"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="Status"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											{ci_language line="Joined"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="summary" checked/>
											{ci_language line="Summary"} {$month}
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">{ci_language line="ID"}</th>
									<th class="tableUserName">{ci_language line="User name"}</th>
									<th class="tableEmail">{ci_language line="Email"}</th>
									<th class="tableStatus">{ci_language line="Status"}</th>
									<th class="tableJoined">{ci_language line="Joined"}</th>
									<th class="tableSummary">{ci_language line="Summary"} {$month}</th>
								</tr>
							</thead>
                                                        {if $queryReferral}
                                                        <!-- Table body -->
                                                        <tbody>
                                                            {foreach $queryReferral as $val}
                                                                            <tr>
                                                                                <td class="tableId" data-th="{ci_language line="ID"}:">{$val['id']}</td>
                                                                                <td class="tableUserName" data-th="{ci_language line="User name"}:">
                                                                                    {if $val['user_provider'] !=''}
                                                                                        {$val['user_first_name']} {$val['user_las_name']} ({$val['user_provider']})
                                                                                    {else}
                                                                                        {$val['user_name']}
                                                                                    {/if}
                                                                                    </td>
                                                                                <td class="tableEmail" data-th="{ci_language line="Email"}:">{$val['user_email']}</td>
                                                                                <td class="tableStatus" data-th="{ci_language line="Status"}:">
                                                                                    {if $val['user_status']==1}
                                                                                    Active
                                                                                    {else}
                                                                                    Inactive
                                                                                    {/if}
                                                                                </td>
                                                                                <td class="tableJoined" data-th="{ci_language line="Joined"}:">{date('j F Y',strtotime($val['user_created_on']))}</td>
                                                                                <td class="tableSummary" data-th="{ci_language line="Summary"} {$month}:"> {$val['count']} bills ({$val['sum']}) </td>
                                                                            </tr>
                                                            {/foreach}
                                                        </tbody>
                                                        <!-- // Table body END -->
                                                        {/if}
                                                        </table>
					</div>
				</div>
			</div>			
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings clearfix">
					<div class="pull-md-left clearfix">
						<h4 class="headSettings_head">{ci_language line="aff_acc_title"}</h4>						
					</div>
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="IP address"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="User name"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Clicks"} {$month}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="Joined"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											{ci_language line="Last activity"}
										</label>
									</li>
								</ul>
							</div>
						</div>
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">{ci_language line="IP address"}</th>
									<th class="tableUserName">{ci_language line="User name"}</th>
									<th class="tableEmail">{ci_language line="Clicks"} {$month}</th>
									<th class="tableStatus">{ci_language line="Joined"}</th>
									<th class="tableJoined">{ci_language line="Last activity"}</th>
								</tr>
							</thead>
							{if $queryAccReferral}
                                                        <!-- Table body -->
                                                        <tbody>
                                                        {foreach $queryAccReferral as $v}
                                                                        {if $v['num'] !=0 }
                                                                        <tr>
                                                                            <td class="tableId" data-th="ID:">{$v['ip']}</td>
                                                                            <td class="tableUserName" data-th="User Name:">{if $v['user_provider'] !=''}
                                                                                    {$v['user_first_name']} {$v['user_las_name']} ({$v['user_provider']})
                                                                                {else}
                                                                                    {$v['user_name']}
                                                                                {/if}</td>
                                                                            <td class="tableEmail" data-th="Email:">{$v['num']}</td>
                                                                            <td class="tableStatus" data-th="Status:">{if $v['id'] !=''}
                                                                                {date('j F Y',strtotime($v['user_created_on']))}
                                                                                {else}
                                                                                {date('j F Y',strtotime($v['create_date']))}
                                                                                {/if}</td> 
                                                                            <td class="tableJoined" data-th="Joined:">{date('j F Y',strtotime($v['last_date']))}</td>
                                                                        </tr>
                                                                        {/if}
                                                        {/foreach}
                                                        </tbody>
                                                        <!-- // Table body END -->
                                                        {/if}
						</table>
					</div>
				</div>
			</div>			
		</div>
	</div>
                                                
<script src="{$cdn_url}assets/js/calldatatable.js"></script>
{*<script language="javascript" type="text/javascript" src="{$cdn_url}assets/js/jquery.affiliation.list.js"></script>*}
{include file="footer.tpl"}