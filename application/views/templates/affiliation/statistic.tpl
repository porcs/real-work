{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div class="main p-rl40 p-xs-rl20">
		<h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="aff_stat_title"}</h1>
		{include file="breadcrumbs.tpl"} <!--breadcrumb-->

		<div id="column"></div>
		<div class="row">
			<div class="col-xs-12">
				<div class="headSettings statistics clearfix">					
					<div class="pull-md-right clearfix">
						<div class="showColumns clearfix">
							<a href="" class="showColumns_link">{ci_language line="Show / Hide Columns"}</a>
							<div class="dropCheckbo">
								<ul class="custom-form">
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="id" checked/>
											{ci_language line="Date"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="username" checked/>
											{ci_language line="Number of commission bills"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="email" checked/>
											{ci_language line="Sales commission"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="status" checked/>
											{ci_language line="Tier commission"}
										</label>
									</li>
									<li>
										<label class="cb-checkbox checked">
											<input type="checkbox" name="joined" checked/>
											{ci_language line="Payout"}
										</label>
									</li>
								</ul>
							</div>
						</div>	
					</div>			
				</div>
				<div class="row">
					<div class="col-xs-12 statistics">
						<table class="responsive-table">
							<thead class="table-head">
								<tr>
									<th class="tableId">{ci_language line="Date"}</th>
									<th class="tableUserName">{ci_language line="Number of commission bills"}</th>
									<th class="tableEmail">{ci_language line="Sales commission"}</th>
									<th class="tableStatus">{ci_language line="Tier commission"}</th>
									<th class="tableJoined">{ci_language line="Payout"}</th>
								</tr>
							</thead>
						
                                                            {if $arrStat} {assign var="totalNum" value="0"} {assign var="totalSale" value="0"} {assign var="totalTier" value="0"} {assign var="totalPay" value="0"}
                                                           <tbody>
                                                               {foreach $arrStat as $val} {assign var="totalNum" value=$totalNum+$val['referral']} {assign var="totalSale" value=$totalSale+$val['sale']} {assign var="totalTier" value=$totalTier+$val['tier']} {assign var="totalPay" value=$totalPay+$val['pay']}
                                                               <tr>
                                                                   <td class="tableId text-left" data-th="{ci_language line="Date"}:" data-name=""><a href="{$val['link']}">{$val['month']}</a></td>
                                                                   <td class="tableUserName text-right" data-th="{ci_language line="Number of commission bills"}:">{number_format($val['referral'])}</td>
                                                                   <td class="tableEmail text-right" data-th="{ci_language line="Sales commission"}:">&euro;{number_format($val['sale'],2)}</td>
                                                                   <td class="tableStatus text-right" data-th="{ci_language line="Tier commission"}:">&euro;{number_format($val['tier'],2)}</td>
                                                                   <td class="tableJoined text-right" data-th="{ci_language line="Payout"}:">&euro;{number_format($val['pay'],2)}</td>
                                                               </tr>
                                                               {/foreach}
                                                               <tr>
                                                                   <td class="tableId text-left bolder" data-th="{ci_language line="Date"}:" data-name="">Total</td>
                                                                   <td class="tableUserName text-right" data-th="{ci_language line="Number of commission bills"}:">{number_format($totalNum)}</td>
                                                                   <td class="tableEmail text-right" data-th="{ci_language line="Sales commission"}:">&euro;{number_format($totalSale,2)}</td>
                                                                   <td class="tableStatus text-right" data-th="{ci_language line="Tier commission"}:">&euro;{number_format($totalTier,2)}</td>
                                                                   <td class="tableJoined text-right" data-th="{ci_language line="Payout"}:">&euro;{number_format($totalPay,2)}</td>
                                                               </tr>
                                                           </tbody>
                                                           {/if}						
						</table>
					</div>
				</div>
			</div>			
		</div>

    <script src="{$cdn_url}assets/js/calldatatable.js"></script>
    <script src="{$cdn_url}assets/js/highchart/highstock.js"></script>
    <script src="{$cdn_url}assets/js/chart.js"></script>
{include file="footer.tpl"}