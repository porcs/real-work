{ci_language file="email" lang="$lang"}
{ci_config name="base_url"}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
        <table width="100%" cellspacing='0'>
            <tr style="background:#4693fe">
                <td style="width:20%"></td>
                <td style="padding:10px">
                    <a href="{$base_url}" style="border: none">{$logo}</a>	
                </td>
                <td style="width:20%"></td>
            </tr>
            <tr>
                <td style="width:20%"></td>
                <td style="padding: 25px 0 0 0;">
                    <table style="width:100%; border:1px solid #eeeeee; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px;">
                                <p style="margin-bottom:0; margin-top:10px">{ci_language line="Dear Customer"},</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {ci_language line="A warm welcome to"} Feed.biz<br><hr><br>
                                    {ci_language line="It is my great pleasure to be appointed as your dedicated Feed.biz supporter."}<br>
                                    {ci_language line="Please do not hesitate to contact me or your Feed.biz supporter at the following contact details whenever your need assistance."} <br><br>
                                    {ci_language line="Click on the link to access your back office"} :<br>
                                    <a href="{$backend_link}"  style="color:#4693fe">{ci_language line="Click here to your back office"}</a><br><br>
                                    {ci_language line="Consider adding this link to your bookmarks."}<br><br><br>  <br> 
                                </p>
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {ci_language line="Yours sincerely"}<br>
                                    {$support_image}<br>{$support_name}<br>Feeb.biz supporter</p>
                                <p style="margin-top:0px; margin-bottom:10px">{$support_signature}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eeeeee; padding:10px;">
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {ci_language line="This e-mail was sent to"} 
                                    <a href="" style="color:#4693fe">support@feed.biz</a>
                                </p>		
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {ci_language line="Please note that you must use an email to access to Feed.biz management system."} 
                                </p>
                                <p style="margin-top:0px; margin-bottom:10px">
                                    &copy; {date('Y')}  <a href="mailto:support@feed.biz" style="color:#4693fe;text-transform: capitalize">Feed.biz.</a> {ci_language line="All right reserved."} </p>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:20%"></td>
            </tr>
        </table>
    </body>
</html>