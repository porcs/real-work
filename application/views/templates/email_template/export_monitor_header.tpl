
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title style=" text-transform: capitalize">Feed.biz</title>        
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px; font-family: sans-serif;">
        <table width="100%" cellspacing='0'>
            <tr style="background:#4693fe;">
                <td style="width:15%; background: #FFF"></td>
                <td style="padding:10px; ">
                    <a href="">{$logo}</a>	
                </td>
                <td style="width:15%; background: #FFF"></td>
            </tr>
            <tr>
                <td style="width:15%"></td>
                <td style="border:1px solid #eeeeee;">
                    <table style="width:100%; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px">
                                <p style="margin-bottom:0; margin-top:10px; font-family: sans-serif; font-size: 14px;">{ci_language line="Dear"} {$fname|cat:' '|cat:$lname},</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">
                                <!-- Feed -->                    