<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>        
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
        <table width="100%" cellspacing=0>
            <tr>
                <td style="width:20%"></td>
                <td style="padding:10px; background:#4693fe;">
                    <a href="">{$logo}</a>	
                </td>
                <td style="width:20%"></td>
            </tr>
            <tr>
                <td style="width:20%"></td>
                <td style="border:1px solid #eeeeee;">
                    <table style="width:100%; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px">
                                <p style="margin-bottom:0; margin-top:10px">{$title},</p>
                                <p style="margin-top:10px; padding:0; font-size:12px; color:#999999;">{$head1}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">
                                <!-- Feed -->                    
                                {if isset($feed) && !empty($feed)}
                                    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                           
                                            <!-- Result -->
                                            {if isset($feed['error'])}
                                                <tr><td height="30" colspan="3"><hr style="border-top:1px solid #EBE3E3; border-bottom: none;"/></td></tr> 
                                                <tr align="left" valign="top">
                                                    <td width="690">
                                                        <p style="margin:0; padding:0; color: red; font-size:14px;line-height:22px;">
                                                            {$feed['error']}
                                                        </p>      
                                                        {if isset($feed['log_data']) && !empty($feed['log_data'])}
                                                            <ul style="font-size:12px;color:#999999;float:none; margin:10px 0;padding:0">
                                                                {foreach $feed['log_data'] as $k => $data}
                                                                    <li style="padding-bottom: 5px;">{if isset($data['message'])}{$data['message']}{/if}</li>
                                                                {/foreach}
                                                            </ul>
                                                        {/if}
                                                    </td>
                                                </tr>    
                                               
                                            {/if}                                           

                                        </tbody>
                                    </table>
                                {/if} 
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eeeeee; padding:10px; font-size: 12px;">                                 
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {$footer2}
                                </p>
                                <p style="margin-top:0px; margin-bottom:10px">
                                    &copy; {date('Y')}  <a href="" style="color:#4693fe; text-transform: capitalize">Feed.biz</a> 
                                    {$footer3}
                                </p>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:20%"></td>
            </tr>
        </table>
    </body>
</html>   