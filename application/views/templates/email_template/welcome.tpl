{ci_language file="email" lang="$lang"} 
{ci_config name="base_url"}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz - {ci_language line="Email Welcome"}</title>
    </head>
<body style="background:#ffffff; margin:0px; border-spacing:0px;">
	<table width="100%" cellspacing='0'>
		<tr style="background:#4693fe">
			<td style="width:20%"></td>
			<td style="padding:10px">
				<a href="{$base_url}" style="border: none">{$logo}</a>	
			</td>
			<td style="width:20%"></td>
		</tr>
		<tr>
			<td style="width:20%"></td>
			<td style="padding: 25px 0 0 0;">
				<table style="width:100%; border:1px solid #eeeeee; border-spacing:0px;">
					<tr>
						<td style="padding:10px; color: #919191; font-weight: 300; font-size:20px;">
							<p style="margin-bottom:0; margin-top:10px">{ci_language line="Welcome To"} {$sitename}</p>
						</td>
					</tr>
					<tr>
						<td style="padding:10px;">
							<p style="margin-top:0px; margin-bottom:10px">
								{ci_language line="Your"} {$sitename} {ci_language line="account has now been created and your login details are below."}<br/>
                                {ci_language line="Before you log into your account we strongly urge you to have a look at our Start Up Guide to avoid issues with your setup."}
                                {ci_language line="It will only take you a few minutes and will save you a lot of time in the long run."}<br/>
                            </p>
							<p style="margin-top:0px; margin-bottom:10px">{ci_language line="Your login details are"}: </p>
							<p style="margin-top:0px; margin-bottom:10px">{ci_language line="Email"}: {$email}</p>
							<p style="margin-top:0px; margin-bottom:10px">{ci_language line="Password"}: {$password}</p>
							<p style="margin-top:0px; margin-bottom:10px">
								{ci_language line="Login address"}: 
								<a href="{$login_link}">{ci_language line="Login"}</a>
							</p>
							{ci_language line="The Support Team at"} {$sitename}, {ci_language line="Happy To Help!"}
						</td>
					</tr>
					<tr>
						<td style="background:#eeeeee; padding:10px;">
							<p style="margin-top:0px; margin-bottom:10px">
								{ci_language line="This e-mail was sent to"} 
								<a href="" style="color:#4693fe">support@feed.biz</a>
							</p>		
							<p style="margin-top:0px; margin-bottom:10px">
								{ci_language line="Please note that you must use an email to access to Feed.biz management system."} 
							</p>
							<p style="margin-top:0px; margin-bottom:10px">
								&copy; {date('Y')} <a href="mailto:support@feed.biz" style="color:#4693fe;text-transform: capitalize">Feed.biz.</a> {ci_language line="All right reserved."} 
							</p>
						</td>
					</tr>
				</table>
			</td>
			<td style="width:20%"></td>
		</tr>
	</table>
</body>
</html>