<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
		<table width="100%" cellspacing='0' style="font-family: arial,sans-serif;">
			<tr style="background:#4693fe">
				<td style="width:15%"></td>
				<td style="padding:10px">
					<a href="{$base_url}" style="border: none">{$logo}</a>
				</td>
				<td style="width:15%"></td>
			</tr>
			<tr>
				<td style="width:15%"></td>
				<td style="padding: 25px 0 0 0;">
					<table style="width:100%; border:1px solid #eeeeee; border-spacing:0px;">
						<tr>
							<td style="padding:10px; color: #919191; font-weight: 300; font-size:20px; font-family: sans-serif;">
								<p style="margin-bottom:0; margin-top:10px">{$title},</p>
							</td>
						</tr>
						<tr>
							<td style="padding:10px;">

								<p style="margin-top:0px; margin-bottom:10px; font-family: sans-serif; font-size: 13px;">
			                        {$message}
			                    </p>
			                    {if isset($message_detail)}
			                    	<table width="100%" border="0" cellspacing="1" cellpadding="3" style="font-family: sans-serif; font-size: 12px;">
										<thead>
											<tr style="background: #eee; font-weight: normal;">
												<th width="10%">{$marketplace}</th>
												<th width="70%">{$Error_Detail}</th>
												<th width="20%">{$Error_Date}</th>
											</tr>
										</thead>
										{foreach $message_detail as $order}
											<tr>
												<td>{$order['marketplace']}</td>
												<td>{$order['message']}</td>
												<td>{$order['date_add']}</td>
											</tr>
										{/foreach}
									</table>
								{/if}
			                    <br><br>
							</td>
						</tr>
						<tr>
							<td style="background:#eeeeee; padding:10px; font-size: 12px;">									
								<p style="margin-top:0px; margin-bottom:10px">
									{$footer2}
								</p>
								<p style="margin-top:0px; margin-bottom:10px">
									&copy; {date('Y')}  <a href="" style="color:#4693fe; text-transform: capitalize">Feed.biz</a> 
									{$footer3}
								</p>
								</p>
							</td>
						</tr>
					</table>
				</td>
				<td style="width:15%"></td>
			</tr>
		</table>
    </body>
</html>