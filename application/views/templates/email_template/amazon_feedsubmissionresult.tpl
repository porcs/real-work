<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Feed.biz</title>        
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
        <table width="100%" cellspacing=0>
            <tr>
                <td style="width:20%"></td>
                <td style="padding:10px; background:#4693fe;">
                    <a href="">{$logo}</a>	
                </td>
                <td style="width:20%"></td>
            </tr>
            <tr>
                <td style="width:20%"></td>
                <td style="border:1px solid #eeeeee;">
                    <table style="width:100%; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px">
                                <p style="margin-bottom:0; margin-top:10px">{$title},</p>
                                <p style="margin-top:10px; padding:0; font-size:12px; color:#999999;">{$head1}</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">
                                <!-- Feed -->                    
                                {if isset($feed) && !empty($feed)}
                                    <table style="width: 640px;" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            
                                            <!--Statistics -->
                                            {if isset($feed['productIndex']) || isset($feed['skipped'])}
                                                <tr align="left" valign="top">
                                                    <td width="150">
                                                        <h4 style="font-size:16px; margin:0; padding:0;">{$l_Statistics} :</h4>
                                                    </td>
                                                    <td width="26"></td>
                                                    <td width="514">
                                                        {if isset($feed['productIndex'])}
                                                            <p style="margin:0; padding:0;color:#254d97;font-size:14px;line-height:22px;">
                                                                {$feed['productIndex']} {$l_Items_to} {$feed['action']} {$l_with_Amazon}.
                                                            </p>
                                                        {/if}
                                                        {if isset($feed['skipped'])}
                                                            <p style="margin:0; padding:0;color:#254d97;font-size:14px;line-height:22px;">
                                                                {$feed['skipped']} {$l_Skipped_items}.
                                                            </p>
                                                        {/if}
                                                    </td>
                                                </tr>
                                            {/if}

                                            <!--Skipped -->
                                            {if isset($feed['log_skipped'])}
                                                <tr><td height="10" colspan="3">&nbsp;</td></tr>   
                                                <tr align="left" valign="top">
                                                    <td width="150">&nbsp;</td>
                                                    <td width="26"></td>
                                                    <td width="514">
                                                        <ul style="font-size:12px; color:#999999; margin: 0; padding: 0; float: none;">
                                                            {foreach $feed['log_skipped'] as $k => $data}
                                                                {if $k != "view_more"}
                                                                    <li style="padding-bottom: 5px;">{$data['message']}</li>
                                                                {/if}
                                                            {/foreach}
                                                        </ul>
                                                    </td>
                                                </tr>
                                                {if isset($feed['log_skipped.view_more']) && !empty($feed['log_skipped.view_more'])}
                                                    <tr><td height="10" colspan="3">&nbsp;</td></tr>   
                                                    <tr align="left" valign="top">
                                                        <td width="150">&nbsp;</td>
                                                        <td width="26"></td>
                                                        <td width="544" height="40">
                                                            <a href="{$feed['log_skipped']['view_more']}" style="background:#254d97; color:#ffffff; font-size:15px; padding:8px 12px; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px; text-decoration:none;">{$l_View_more_on_web_site}</a>
                                                        </td>
                                                    </tr>    
                                                {/if}
                                            {/if}

                                            <!-- Result -->
                                            {if isset($feed['error'])}
                                                <tr><td height="30" colspan="3"><hr style="border-top:1px solid #EBE3E3; border-bottom: none;"/></td></tr> 
                                                <tr align="left" valign="top">
                                                    <td width="150">
                                                        <h4 style="font-size:16px; margin:0; padding:0;">{$l_Result} :</h4>
                                                    </td>
                                                    <td width="26"></td>
                                                    <td width="514">
                                                        <p style="margin:0; padding:0; color: red; font-size:14px;line-height:22px;">
                                                            {$feed['error']}
                                                        </p>      
                                                        {if (isset($feed['log_data']))}
                                                            <ul style="font-size:12px;color:#999999;float:none; margin:10px 0;padding:0">
                                                                {foreach $feed['log_data'] as $k => $data}
                                                                    <li style="padding-bottom: 5px;">{$data['message']}</li>
                                                                {/foreach}
                                                            </ul>
                                                        {/if}
                                                    </td>
                                                </tr>     
                                                {if isset($feed['log_data.view_more']) && !empty($feed['log_data.view_more'])}
                                                    <tr align="left" valign="top">
                                                        <td width="150">&nbsp;</td>
                                                        <td width="26"></td>
                                                        <td width="514">
                                                            <a href="{$feed['log_data']['view_more']}" style="background:#254d97; color:#ffffff; font-size:15px; padding:8px 12px; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px; text-decoration:none;">{$l_View_more_on_web_site}</a>
                                                        </td>
                                                    </tr>
                                                {/if}
                                            {/if}                                           

                                            <!-- Feed Data -->
                                            {if isset($feed['data'])}
                                                {foreach $feed['data'] as $k => $data}
                                                    <tr><td height="30" colspan="3"><hr style="border-top:1px solid #EBE3E3; border-bottom: none;"/></td></tr> 
                                                    <tr align="left" valign="top">
                                                        <td width="150">
                                                            <h4 style="font-size: 14px; color: #555555; margin:0; padding:0;">{$k} {$l_Feed} :</h4>
                                                        </td>
                                                        <td width="26"></td>
                                                        <td width="514">

                                                            <!--feed submission-result-->
                                                            <table style="width:100%;font-size:13px;color:#999999;" border="0" cellspacing="0" cellpadding="0" valign="top">
                                                                <tbody> 
                                                                    {if isset($data['feed_sunmission_id'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Feed_Submission_Id} : {$data['feed_sunmission_id']}
                                                                            </td>
                                                                        </tr>   
                                                                    {/if}     
                                                                    {if isset($data['date_add'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Feed_Submission_date} : {$data['date_add']}
                                                                            </td>
                                                                        </tr>  
                                                                    {/if}    
                                                                    {if isset($data['messages_processed'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Messages_Processed} : {$data['messages_processed']}
                                                                            </td>
                                                                        </tr> 
                                                                    {/if}   
                                                                    {if isset($data['messages_successful'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Messages_Successful} : {$data['messages_successful']}
                                                                            </td>
                                                                        </tr> 
                                                                    {/if}
                                                                    {if isset($data['messages_with_warning'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Messages_With_Warning} : {$data['messages_with_warning']}
                                                                            </td>
                                                                        </tr> 
                                                                    {/if}
                                                                    {if isset($data['messages_with_error'])}
                                                                        <tr>
                                                                            <td style="padding-bottom: 5px;">
                                                                                {$l_Messages_With_Error} : {$data['messages_with_error']}
                                                                            </td>
                                                                        </tr> 
                                                                    {/if}                                                                    
                                                                </tbody>
                                                            </table>                                                                    
                                                        </td>
                                                    </tr>  
                                                    {if isset($data['link']) && !empty($data['link']) && $data['messages_with_error'] > 0}
                                                        <tr align="left" height="50">
                                                            <td width="150">&nbsp;</td>
                                                            <td width="26"></td>
                                                            <td width="514">    
                                                                <a href="{$data['link']}" style="background: #600; color:#ffffff; font-size:15px; padding:8px 12px; border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px; text-decoration:none;">{$l_Download_Error_Message}</a>
                                                            </td>
                                                        </tr>
                                                    {/if} 
                                                {/foreach} 
                                            {/if} 
                                        </tbody>
                                    </table>
                                {/if} 
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#eeeeee; padding:10px; font-size: 12px;">                                 
                                <p style="margin-top:0px; margin-bottom:10px">
                                    {$footer2}
                                </p>
                                <p style="margin-top:0px; margin-bottom:10px">
                                    &copy; {date('Y')}  <a href="" style="color:#4693fe; text-transform: capitalize">Feed.biz</a> 
                                    {$footer3}
                                </p>
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:20%"></td>
            </tr>
        </table>
    </body>
</html>   