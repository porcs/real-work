<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title style="text-transform: capitalize">Feed.biz</title>
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px;">
		<table width="100%" cellspacing='0' style="font-family: arial,sans-serif;">
			<tr style="background:#4693fe">
				<td style="width:15%"></td>
				<td style="padding:10px">
					<a href="{$base_url}" style="border: none">{$logo}</a>
				</td>
				<td style="width:15%"></td>
			</tr>
			<tr>
				<td style="width:15%"></td>
				<td style="padding: 25px 0 0 0;">
					<table style="width:100%; border:1px solid #eeeeee; border-spacing:0px;">
						<tr>
							<td style="padding:10px; color: #919191; font-weight: 300; font-size:20px; font-family: sans-serif;">
								<p style="margin-bottom:0; margin-top:10px">{$title},</p>
							</td>
						</tr>
						<tr>
							<td style="padding:10px;">

								<p style="margin-top:0px; margin-bottom:10px; font-family: sans-serif; font-size: 13px;">
			                        {$message}
			                    </p>
			                     
							</td>
						</tr>
						<tr>
							<td style="background:#eeeeee; padding:10px; font-size: 12px;">									
								<p style="margin-top:0px; margin-bottom:10px">
									{$footer2}
								</p>
								<p style="margin-top:0px; margin-bottom:10px">
									&copy; {date('Y')}  <a href="mailto:support@feed.biz" style="color:#4693fe;">Feed.biz</a>  
									{$footer3}
								</p>
								</p>
							</td>
						</tr>
					</table>
				</td>
				<td style="width:15%"></td>
			</tr>
		</table>
    </body>
</html>