<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title style="text-transform: capitalize">Feed.biz</title>        
    </head>
    <body style="background:#ffffff; margin:0px; border-spacing:0px; font-family: sans-serif;">
        <table width="100%" cellspacing='0'>
            <tr style="">
                <td style="width:20%"></td>
                <td style="padding:10px; background:#4693fe;">
                    <a href="">{$logo}</a>	
                </td>
                <td style="width:20%"></td>
            </tr>
            <tr>
                <td style="width:20%"></td>
                <td style="border:1px solid #eeeeee;">
                    <table style="width:100%; border-spacing:0px;">
                        <tr>
                            <td style="padding:10px; color: #919191; font-weight: 300; font-size:20px">
                                <p style="margin-bottom:0; margin-top:10px; font-family: sans-serif; font-size: 14px;">{ci_language line="Dear"} {$fname|cat:' '|cat:$lname},</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px;">
                                <!-- Feed -->                    
                                {if !empty($feed) && !empty($results) }
                                    {foreach $results key = k item = data}
                                    {assign var="summary" value=0}
                                    <h4 style="font-size:16px; margin:0; padding:0; font-family: sans-serif;">{$feed['market_place']|cat:" "|cat:$feed['country']|cat:" - "|cat:$results[$k]['type']|cat:" ("}{$results[$k]['ebay_user']|cat:")"}</h4>
                                    <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <!-- Feed Data -->
                                            <tr><td height="30" colspan="3"><hr style="border-top:1px solid #EBE3E3; border-bottom: none;"/></td></tr> 
                                            <tr align="left" valign="top">
                                                <td width="100%" style="padding:10px">
                                                    <!--feed submission-result-->
                                                    <table style="width:100%;font-size:13px;color:#999999;" border="0" cellspacing="0" cellpadding="0" valign="top">
                                                        <tbody> 
                                                            <tr>
                                                                <td style="padding-bottom: 5px; font-family: sans-serif; padding-bottom: 20px;">
                                                                    {$ebaylogo}
                                                                </td>
                                                            </tr>   
                                                            {if isset($data['batch_id']) && isset($data['type']) && ($data['type'] == "Product") }
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif;">
                                                                        {ci_language line="Feed Submission ID"} : <span style="color: #666">{$data['batch_id'][0]}</span>
                                                                    </td>
                                                                </tr>   
                                                            {/if}
                                                            {if (isset($data['date_add']))}
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif;">
                                                                        {ci_language line="Feed Submission date"} : <span style="color: #666">{$data['date_add']}</span>
                                                                    </td>
                                                                </tr>  
                                                            {/if}                                                    
                                                            {if (isset($data['ShopTotal']))}
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif; /*color: #666; font-weight: bold;*/">
                                                                        {ci_language line="Total Product on shop"} : <span style="color: #666">{$data['ShopTotal']}</span>
                                                                    </td>
                                                                </tr> 
                                                            {/if}                                                            
                                                            {if (isset($data['EbayTotal']))}
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif; color: #666; font-weight: bold;">
                                                                        {ci_language line="Total Product on eBay"} : <span style="color: #15BE00">{$data['EbayTotal']}</span>
                                                                    </td>
                                                                </tr> 
                                                            {/if}                                                            
                                                            {if (isset($data['ShopOutOfStockTotal']))}
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif; color: #FF0C0C; font-weight: bold;">
                                                                        {ci_language line="Out of stock"} : <span style="color: #FF0C0C">{$data['ShopOutOfStockTotal']} ({number_format($data['ShopOutOfStockTotal'] / $data['ShopTotal'] * 100, 2)|cat:' '|cat:'%'})</span>
                                                                    </td>
                                                                </tr> 
                                                            {/if}                                                            
                                                            {if !empty($data['EbayTotal']) && !empty($data['ShopTotal']) }
                                                                <tr>
                                                                    <td style="padding-bottom: 5px; font-family: sans-serif; color: #FFBC00; font-weight: bold;">
                                                                        {ci_language line="Throughput"} : <span style="color: #FFBC00">{number_format($data['EbayTotal'] / $data['ShopTotal'] * 100, 2)|cat:' '|cat:'%'}</span>
                                                                    </td>
                                                                </tr> 
                                                            {/if}                                                            
                                                        </tbody>
                                                    </table>                                                                    
                                                </td>
                                            </tr>  
                                            <tr><td height="30" colspan="3"><hr style="border-top:1px solid #EBE3E3; border-bottom: none;font-weight: bold;"/></td></tr> 
                                            {if (isset($data['link']) && !empty($data['link']))}
                                                <tr align="left" height="50">
                                                    <td width="150">&nbsp;</td>
                                                    <td width="26"></td>
                                                    <td width="514">    
                                                        <a href="<?php echo $data['link']; ?>" style="background: #600; font-family: sans-serif; color:#ffffff; font-size:15px; padding:8px 12px; border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px; text-decoration:none;">
                                                            {ci_language line="Download Error Message"}
                                                        </a>
                                                    </td>
                                                </tr>
                                            {/if} 
                                        </tbody>
                                    </table>
                                    <table style="width: 100%" border="0" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <!--Statistics -->
                                            <tr align="left" valign="top">
                                                <td width="100%" style="padding:10px">
                                                    <h4 style="font-size:16px; margin:0; padding:0; font-family: sans-serif;">{ci_language line="Statistics"}</h4>
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top">
                                                <td width="100%" style="padding:10px">
                                                    {if isset($data['messages']) }<br>
                                                            <table style="width: 100%; border-color: #FBFBFB;" border="1" cellspacing="0" cellpadding="0" >
                                                                <tr style="background: #FFE344;">
                                                                    <td width=10%><h4 style="font-size:14px; color: #000; margin:0; padding:0; font-family: sans-serif; line-height: 24px; padding-top: 1px; padding-left: 25px; padding-right: 25px; text-align: left">{ci_language line="From"}</h4></td>
                                                                    <td width=72%><h4 style="font-size:14px; color: #000; margin:0; padding:0; font-family: sans-serif; line-height: 24px; padding-top: 1px; padding-left: 10px; ">{ci_language line="Message"}</h4></td>
                                                                    <td width=8%><h4 style="font-size:14px; color: #000; margin:0; padding:0; font-family: sans-serif; line-height: 24px; padding-top: 1px; text-align: center; ">{ci_language line="Level"}</h4></td>
                                                                    <td width=8%><h4 style="font-size:14px; color: #000; margin:0; padding:0; font-family: sans-serif; line-height: 24px; padding-top: 1px; text-align: right; padding-right: 10px; font-weight: bold; ">{ci_language line="Count"}</h4></td>
                                                                </tr>
                                                            {foreach $data['messages'] key = mess_id item = message }{$summary = $summary + $message['Count']}
                                                                <tr {if ($mess_id % 2) == 1}style="background: #F8F8F8;"{/if}>
                                                                    <td style="font-family: sans-serif; line-height: 24px; font-size:14px;color:#666; padding-left: 25px; padding-right: 25px; text-align: left">{$message['MessageFrom']}</td>
                                                                    <td style="font-family: sans-serif; line-height: 24px; font-size:14px;color:#666; padding-left: 10px; ">{$message['Message']}</td>
                                                                    <td style="font-family: sans-serif; line-height: 24px; text-transform: uppercase; font-size:12px;{if $message['Level'] == 'Warning'}color:#FF0C0C;{elseif $message['Level'] == 'Success'}color:#15BE00;{else}color:#666;{/if} text-align: center; ">{$message['Level']}</td>
                                                                    <td style="font-family: sans-serif; line-height: 24px; font-size:12px;color:#666; text-align:right; padding-right: 10px; font-weight: bold; ">{number_format($message['Count'], 0)}</td>
                                                                </tr>
                                                            {/foreach}
                                                                <tr style="background: #FFF;">
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td style="font-family: sans-serif; line-height: 24px; font-size:12px; color:#666; text-align: center; font-weight: bold;">{ci_language line="Totals"}</td>
                                                                    <td style="font-family: sans-serif; line-height: 24px; font-size:12px; color:#666; text-align:right; padding-right: 10px; font-weight: bold;">{number_format($summary, 0)}</td>
                                                                </tr>
                                                     </table>
                                                    {/if}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table><br><br><br><br>
                                {/foreach} 
                                {/if} 
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#FAFAFA; padding:10px;border-top:1px solid #EEE">                                	
                                <p style="margin-top:10px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">
                                    {ci_language line="Please note that you must use an email to access to Feed.biz management system."}
                                </p>
                                <p style="margin-top:0px; margin-bottom:10px; font-family: sans-serif;font-size: 12px;">
                                    &copy; {date('Y')} <a href="mailto:support@feed.biz" style="color:#4693fe;text-transform: capitalize">Feed.biz.</a> {ci_language line="All right reserved."}
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width:20%"></td>
            </tr>
        </table>
    </body>
</html>   