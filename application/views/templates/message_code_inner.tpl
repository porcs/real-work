{if isset($message_code_inner)}

<div class="row">
    <div class="col-xs-12">
        <div class="validate {if $message_code_inner.message_type == "warning"}yellow{elseif $message_code_inner.message_type == "no_data"}gray{else}blue{/if}">
            <div class="validateRow">
                <div class="validateCell">
                    {if $message_code_inner.message_type == "warning"}<i class="note"></i>{else}<i class="fa fa-info-circle"></i>{/if}
                </div>
                <div class="validateCell">
                    <p>
                        {if isset($message_code_inner.message_name) && !empty($message_code_inner.message_name)}<span class="bold">{$message_code_inner.message_name}</span>,{/if}
                        {if isset($message_code_inner.message_problem) && !empty($message_code_inner.message_problem)}{$message_code_inner.message_problem}{/if}
                        {if isset($message_code_inner.message_cause) && !empty($message_code_inner.message_cause)}{$message_code_inner.message_cause}{/if}
                        {if isset($message_code_inner.message_solution) && !empty($message_code_inner.message_solution)}{$message_code_inner.message_solution}{/if}

                    </p>
                    {if isset($message_code_inner.message_link) && !empty($message_code_inner.message_link)}
                        <p>
                            <a href="/{$message_code_inner.message_link}{if isset($message_code_inner.require_country) && $message_code_inner.require_country && isset($id_country)}/{$id_country}{/if}">
                                {if isset($message_code_inner.message_link) && !empty($message_code_inner.message_link_label)}
                                    {$message_code_inner.message_link_label}
                                {else}
                                    {ci_language line="Learn More"}
                                {/if}
                            </a>
                        </p>
                    {/if}  
                    <i class="fa fa-remove pull-right"></i>
                </div>
            </div>
        </div>
    </div>
</div>
{/if}

{if isset($message_code_id) && !empty($message_code_id)}
<span class="dark-gray">
<a href="#" data-id="{$message_code_id}" class="message_code_id popoverThis points dark-gray" rel='popover' data-placement='bottom'><span class="fa fa-question-circle"></span></a>
</span>
{/if}

{if isset($message_code_help) && !empty($message_code_help)}

    <div class="p-10">{if isset($message_code_help.message_name) && !empty($message_code_help.message_name)}<span class="bold">{$message_code_help.message_name}</span>{/if}</div>
    <div class="p-10">
        {if isset($message_code_help.message_problem) && !empty($message_code_help.message_problem)}{$message_code_help.message_problem}{/if}
        {if isset($message_code_help.message_cause) && !empty($message_code_help.message_cause)}{$message_code_help.message_cause}{/if}
        {if isset($message_code_help.message_solution) && !empty($message_code_help.message_solution)}{$message_code_help.message_solution}{/if}
    </div>

{/if}