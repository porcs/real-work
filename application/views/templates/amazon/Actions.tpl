{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

{*if !isset($creation)}
    <div class="row alert-message">
        <div class="col-xs-12">
            <div class="validate blue m-t10">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i>
                    </div>
                    <div class="validateCell">
                        <div class="pull-left">
                            <ul>
                                <li>
                                    <p>
                                        <strong>{ci_language line="Amazon product creation."}</strong> 
                                        {ci_language line="This option allows to create unknown and unreferenced products on Amazon (EAN/UPC unknown, product which not have ASIN). It is recommended first to have first a good knowledge of this module prior using this feature. It is also recommended to try first to match your products with the Amazon offers wizard."}
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        {ci_language line="To enable Amazon product creation go to Amazon > Features and checked PRODUCTS CREATION."}
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if*} 

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#synchronize" data-toggle="tab" class="">
                    <i class="icon-syn"></i> <span>{ci_language line="Offers"}</span>
                </a>
            </li>
            {if isset($creation)}
                <li>
                    <a data-toggle="tab" href="#creation">
                        <i class="icon-creation-up"></i> <span>{ci_language line="Products"}</span>
                    </a>
                </li>
            {/if}
            {if isset($amazon_display[$ext]['import_orders']) && !empty($amazon_display[$ext]['import_orders'])}                
                <li>
                    <a data-toggle="tab" href="#orders">
                        <i class="icon-creation-down"></i> <span>{ci_language line="Orders"}</span>
                    </a>
                </li>  
            {/if}               
            {if isset($amazon_display[$ext]['delete_products']) && !empty($amazon_display[$ext]['delete_products'])}
                <li>
                    <a data-toggle="tab" href="#delete">
                        <i class="icon-garbage"></i> <span>{ci_language line="Delete"}</span>
                    </a>
                </li>
            {/if}                
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <!--Synchronization-->
            <div id="synchronize" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-10">
                        <p class="head_text montserrat black p-t20 b-None">
                            {ci_language line='Offers'}
                        </p>
                        <ul class="p-l20">
                            <li>
                                <p class="dark-gray">{ci_language line="Offers Wizard"}</p>
                                <p class="poor-gray">
                                    {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.'}
                                </p>
                            </li>
                            <li>
                                <p class="dark-gray">{ci_language line="Update offers"}</p>
                                <p class="poor-gray">
                                    {ci_language line='This will update Amazon depending your stocks moves, within the selected categories in your configuration.'}
                                </p>
                            </li>
                        </ul>                            
                        <div id="sync_options">
                            {if isset($message_code_inner.sync) && (isset($mode_default) && $mode_default > 1)}{$message_code_inner.sync}{/if}
                            <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
                            
                            <div class="clearfix m-t20 xs-center">
                                <div class="pull-sm-left">
                                    <button class="btn btn-wizard wizard_btn m-r5" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$sync_mode}" id="amazon_synchronize_wizard">
                                        <i class="icon-wizard"></i> {ci_language line="Offers Wizard"}
                                    </button>
                                </div>
                                                               
                                <div class="pull-sm-right">
                                    <button class="btn btn-amazon m-r5" id="send-offers" type="button" value="synchronize">
                                        <i class="icon-amazon"></i> {ci_language line="Update offers"}
                                    </button>                                        
                                    <div class="status" style="display: none">
                                        <p>{ci_language line="Starting update"} ..</p>
                                    </div>
                                    <div class="error" style="display: none">
                                        <p class="m-t0">
                                            <i class="fa fa-exclamation-circle"></i>
                                            <span></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        
                            <p class="head_text montserrat black p-t20 b-None">{ci_language line="Send Relations Only"}</p>
                            
                                    <div class="p-0 col-md-10">
                                       {* <p class="dark-gray">{ci_language line="Send Relations Only"}</p>*}
                                        <ul class="p-l20">
                                            <li class="col-xs-12 p-0">
                                                <p class="poor-gray">{ci_language line='This will update Amazon Relations, within the selected categories in your configuration.'}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="p-0 col-md-2 text-right">
                                             <div class="{*pull-sm-right m-r10*}" id="sync_send_relations_only">
                                                <button class="btn btn-amazon-invert" id="send_relations_only" type="button" value="sync_send_relations_only">
                                                    <i class="icon-amazon"></i> {ci_language line="Send Relations Only"}
                                                </button>
                                                <div class="status" style="display: none">
                                                    <p>{ci_language line="Starting send relations"} ..</p>
                                                </div>
                                                <div class="error" style="display: none">
                                                    <p class="m-t0">
                                                        <i class="fa fa-exclamation-circle"></i>
                                                        <span></span>
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                
                                
                                <p class="head_text montserrat black p-t20 b-None">{ci_language line="Send Relations Only"}</p>
                                
                                    <div class="p-0 col-md-10">
                                        {*<p class="dark-gray">{ci_language line="Send Images Only"}</p>*}
                                        <ul class="p-l20">
                                            <li class="col-xs-12 p-0">
                                                <p class="poor-gray">{ci_language line='This will update Amazon Images, within the selected categories in your configuration.'}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="p-0 col-md-2 text-right">
                                            <div class="{*pull-sm-right*}" id="sync_send_images_only">
                                                <button class="btn btn-amazon-invert" id="send_images_only" type="button" value="sync_send_images_only">
                                                    <i class="icon-amazon"></i> {ci_language line="Send Images Only"}
                                                </button>                                        
                                                <div class="status" style="display: none">
                                                    <p>{ci_language line="Starting send images"} ..</p>
                                                </div>
                                                <div class="error" style="display: none">
                                                    <p class="m-t0">
                                                        <i class="fa fa-exclamation-circle"></i>
                                                        <span></span>
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                
                        </div>      
                    </div>
                </div>
            </div>
                           
            <!--creation-->
            {if isset($creation)}
                <div class="tab-pane" id="creation">
                    <div class="row">
                        <div class="col-sm-10">
                            <p class="head_text montserrat black p-t20 b-None">
                                {ci_language line='Products'}
                            </p>
                            <ul class="p-l20">
                                <li>
                                    <p class="dark-gray">{ci_language line="Products Wizard"}</p>
                                    <p class="poor-gray">                                        
                                        {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.'}
                                    </p>
                                    <p class="poor-gray">                                        
                                        {ci_language line='This will send all the products having a profile and within the selected categories in your configuration.'}
                                    </p>
                                </li>
                            </ul>
                            <div id="creat_options" class="clearfix"> 
                                <div class="clearfix m-t20">
                                    <button class="btn btn-wizard wizard_btn" href="{$base_url}/amazon/parameters/{$id_country}/2/{$create_mode}" id="amazon_creation_wizard">
                                        <i class="icon-wizard"></i> {ci_language line="Products Wizard"}
                                    </button>									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {/if}
            
            <!--orders-->
            {if isset($amazon_display[$ext]['import_orders']) && !empty($amazon_display[$ext]['import_orders'])}
                <div class="tab-pane" id="orders">                    
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="head_text montserrat black p-t20 b-None">
                                {ci_language line="Import Orders"}
                            </p>
                            <ul class="p-l20">
                                <li>
                                    <p class="poor-gray">
                                        {ci_language line='This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'}
                                    </p>
                                </li>
                            </ul>
                            {if isset($message_code_inner.carrier)}
                                {$message_code_inner.carrier}
                            {else}
                                <div id="parameter_options">
                                    <p class="head_text montserrat black p-t20 b-None">
                                        {ci_language line="Parameters"}
                                    </p>
                                    <div class="row">
                                        <div class="col-sm-5 col-lg-6">
                                            <p class="poor-gray">{ci_language line='Order Date Range'}</p>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group calendar firstDate">
                                                        <input class="form-control order-date" type="text" id="order-date-from" value="{if isset($yesterday)}{$yesterday}{/if}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group calendar lastDate">
                                                        <input class="form-control order-date" type="text" id="order-date-to" value="{if isset($today)}{$today}{/if}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <p class="poor-gray">{ci_language line='Order Status'}</p>
                                            <select id="order-status" class="search-select" >
                                                {*<option value=""></option>*}
                                                <option value="All">{ci_language line='Retrieve all pending orders'}</option>
                                                <option value="Pending">{ci_language line='Pending - This order is pending in the market place'}</option>
                                                <option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order is waiting to be shipped'}</option>
                                                <option value="PartiallyShipped">{ci_language line='Partially shipped - This order was partially shipped'}</option>
                                                <option value="Shipped">{ci_language line='Shipped - This order was shipped'}</option>
                                                <option value="Canceled">{ci_language line='Cancelled - This order was cancelled'}</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4 col-lg-3 sm-right" id="orders-import">
                                            <button class="btn btn-save m-t25" id="import-orders" type="button" value="orders-import">
                                                {ci_language line="Import orders"} 
                                            </button>
                                            <div class="status sm-left" style="display: none">
                                                <p>{ci_language line="Starting import"} ..</p>
                                            </div>
                                            <div class="error sm-left" style="display: none">
                                                <p class="m-t0">
                                                    <i class="fa fa-exclamation-circle"></i>
                                                    <span></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="head_text montserrat black p-t20 b-None">
                                        {ci_language line="Update Order Shipments"}
                                    </p>
                                    <div class="row">
                                        <div class="col-sm-8 col-lg-9">
                                            <ul class="p-l20">
                                                <li>
                                                    <p class="poor-gray">
                                                        {ci_language line='This will confirm orders shipment to Amazon. The order was Shipped will update to Amazon by clicking the button below.'}
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="parameter_options"> 
                                            <div class="col-sm-4 col-lg-3 sm-right" id="orders-update">
                                                <button class="btn btn-rule" id="update-orders" type="button" value="orders-update">
                                                    {ci_language line="Update Order Shipments"}
                                                </button>
                                                <p></p>
                                                <div class="status sm-left" style="display: none;">
                                                    <p>{ci_language line="Starting update"} ..</p>
                                                </div>
                                                <div class="error sm-left" style="display: none">
                                                    <p class="m-t0">
                                                        <i class="fa fa-exclamation-circle"></i>
                                                        <span></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        </div>						
                    </div>
                </div>
            {/if}

            <!--delete-->
            {if isset($amazon_display[$ext]['delete_products']) && !empty($amazon_display[$ext]['delete_products'])}                       
                <div class="tab-pane" id="delete">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="head_text montserrat black p-t20 b-None">
                                {ci_language line="Delete"}
                            </p>
                            <ul class="p-l20">
                                <li>
                                    <p class="poor-gray">                                        
                                        {ci_language line='This will send all the products having a profile and within the selected categories in your module configuration and selected for deletion.'}
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="status" style="display: none">
                        <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                    </div>
                    <div id="delete_options">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="head_text montserrat black p-t20 b-None">
                                    {ci_language line="Parameters"}
                                </p>
                            </div>
                        </div>
                        <div class="row custom-form">
                            <div class="col-sm-3">
                                <label class="cb-radio">
                                    <input name="delete-parameters" type="radio" id="out_of_stock" value="{$out_of_stock}" checked="checked">
                                    <p class="poor-gray">{ci_language line="Delete out of stock products"}</p>
                                    <p class="m-l25 poor-gray">
                                        {ci_language line="This will delete only out of stock products on Amazon."}
                                    </p>
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label class="cb-radio">
                                    <input name="delete-parameters" type="radio" id="all_sent" value="{$all_sent}">
                                    <p class="poor-gray">{ci_language line="Delete all sent products"}</p>
                                    <p class="m-l25 poor-gray">
                                        {ci_language line="This will delete all sent products on Amazon."}
                                    </p>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-save" id="delete-products" type="button" value="delete">
                                {ci_language line="Start to deletion"}
                            </button>
                            <div class="status" style="display: none">
                                <p>{ci_language line="Starting delete"} ..</p>
                            </div>
                            <div class="error" style="display: none">
                                <p class="m-t0">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
    
<input type="hidden" value="{ci_language line="Are you sure to send relations?"}" id="send_relations_only-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to send images?"}" id="send_images_only-confirm" />
<input type="hidden" value="<b>{ci_language line="Are you sure to update offers?"}</b><br/><br/><p>{ci_language line="This will synchronize Amazon depending your stocks moves, within the selected categories in your configuration."}</p>" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to get orders?"}" id="get-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to update order shipments?"}" id="update-orders-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to delete product?"}" id="delete-products-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 

<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
