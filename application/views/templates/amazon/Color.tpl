{if isset($includeLabel) && $includeLabel}
    <p class="poor-gray" {if isset($label)}for="{$label}"{/if}>{ci_language line="Color"}</p>
{/if}  
<div class="form-group">

    <select rel="Color" class="StringNotNull search-select">
        <option value=""> -- </option>
        {if isset($color)}
            {foreach from=$color.value key=akey item=value}
                <option value="{if isset($value.id_attribute_group)}{$value.id_attribute_group}{/if}" {if isset($value.selected) && $value.selected == 1}selected{/if}>
                   {if isset($value.name)}{$value.name}{/if}
                </option>
            {/foreach} 
        {/if}
    </select>

</div>