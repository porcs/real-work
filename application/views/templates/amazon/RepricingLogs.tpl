{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div id="content"> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  
                        
            <div class="row">
                <div class="col-xs-12">

                    <div class="headSettings clearfix b-Top-None">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Repricing Logs"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix"></div>
                        </div>          
                    </div><!--headSettings-->
                    <!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="fba-logs" class="amazon-responsive-table">
                                <thead class="table-head">
                                    <tr>
                                        <th>{ci_language line="SKU"}</th>
                                        <th>{ci_language line="Date"}</th>
                                        <th>{ci_language line="Repricing from"}</th>
                                        <th>{ci_language line="Process by"}</th>
                                        <th>{ci_language line="Mode"}</th>
                                        <th>{ci_language line="Reprice"}</th>
                                    </tr>
                                </thead>
                            </table><!--table-->
                            <table class="table tableEmpty">
                                <tr>
                                    <td>{ci_language line="No data available in table"}</td>
                                </tr>
                            </table>
                        </div><!--col-xs-12-->
                    </div><!--row--> 

                </div><!--col-xs-12-->
            </div><!--row-->   

            <!--Hidden-->
            <input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
            <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
            <input type="hidden" id="ext_key" value="{if isset($ext_key)}{$ext_key}{/if}" />
            <input type="hidden" id="id_user" value="{if isset($id_user)}{$id_user}{/if}" />
            <input type="hidden" id="tag" value="{if isset($tag)}{$tag}{/if}" />
            <input type="hidden" id="id_marketplace" value="{if isset($id_marketplace)}{$id_marketplace}{/if}" />
            <input type="hidden" id="shop_name" value="{if isset($shop[0]['name'])}{$shop[0]['name']}{/if}" />

            {*<input type="hidden" id="l_export" value="Export" />
            <input type="hidden" id="l_reprice" value="Analyst" />*}
            <input type="hidden" id="l_update_option" value="Updated Options" />
            <input type="hidden" id="l_repricing_automaton" value="Repricing Automaton" />
            <input type="hidden" id="l_sync" value="Synchronization" />
            <input type="hidden" id="l_create" value="Products Creation" />

            {include file="amazon/IncludeDataTableLanguage.tpl"}
        </div> 
    
    {include file="amazon/PopupStepFooter.tpl"} 

</div> 
        
<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/repricing_logs.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}