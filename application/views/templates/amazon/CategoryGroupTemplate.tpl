<li>

    <span class="tree_item">
        <p>
            {if isset($type) && $type == "folder"}
                <i class="cb-plus good active add_category"></i>	
            {/if}	

            <label class="checkBa">											
                <input type="checkbox" rel="id_category" name="category[{$id_category}][id_category]" id="category_{$id_category}" value="{$id_category}" {if isset($checked) && ($checked == true)}checked="checked"{/if}> 
                <label for="category_{$id_category}">
                    {$name}
                </label>
            </label>

        </p>
    </span>

    
        <div class="treeRight selectTree">
            <div class="tree_show" style="{if isset($checked) && ($checked == true)}display: block;{else}display: none;{/if}">
                <select class="form-control" {if isset($checked) && ($checked == true)}name="category[{$id_category}][id_profile]"{else}data-name="category[{$id_category}][id_profile]"{/if} id="category_{$id_category}_id_profile">
                    <option value=""> - - </option>
                    {if (isset($profile) && !empty($profile) )}
                        {foreach from=$profile key=pkey item=pvalue}
                            <option value="{$pvalue.id_profile}" {if isset($checked) && ($checked == true)}{if isset($selected) && $selected == $pvalue.id_profile} selected {/if}{/if}>{$pvalue.name}</option>
                        {/foreach}
                    {/if}
                </select>                        
                <i class="icon-more cp_down_btn" onclick="click_copy_val_sel($(this));"></i>
            </div>
        </div>

    {if (isset($sub_html) && !empty($sub_html) )}
        <ul class="tree_child" style="display: block;">
            {$sub_html}
        </ul>
    {/if}

</li>