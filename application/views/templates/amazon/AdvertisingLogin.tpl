{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row">
            <div class="col-xs-12">
                <div class="headSettings clearfix m-b10 b-Top-None">
                    <ul class="headSettings_point clearfix">
                        <li class="headSettings_point-item {if !isset($sub_action) || empty($sub_action)}active{/if}">
                            <span class="step">1</span>
                            <span class="title">{ci_language line="login with Amazon"}</span>
                        </li>
                        <li class="headSettings_point-item {if isset($sub_action) && $sub_action == 2}active{/if}">
                            <span class="step">2</span>
                            <span class="title">{ci_language line="Confirm"}</span>
                        </li>
                        <li class="headSettings_point-item {if isset($sub_action) && $sub_action == 3}active{/if}">
                            <span class="step">3</span>
                            <span class="title">{ci_language line="Complete"}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row-fluid">  
            <!-- <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" class="">
                        <i class="icon-dollar"></i> 
                        <span> -->
                        <!-- <div class="headSettings clearfix">
                            <div class="pull-md-left clearfix">
                                <h4 class="headSettings_head">
                                    {if !isset($sub_action) || empty($sub_action)}{ci_language line="Login with Amazon Account"}{/if}
                                    {if isset($sub_action) && $sub_action == 2}{ci_language line="Confirm Registeration"}{/if}
                                    {if isset($sub_action) && $sub_action == 3}{ci_language line="Profile Deatil"}{/if}
                                </h4>                        
                            </div>
                        </div>  -->                           
                        <!-- </span>
                    </a>
                </li>                                     
            </ul> -->
            <div class="b-Top">
                <!-- 1 Login with Amazon-->
                <div id="adLogin" class="m-t30" {if !isset($sub_action) || empty($sub_action)}{else}style="display:none"{/if}>
                    <div class="row">
                        <div class="col-md-8 text-center">
                            <p class="text-uc montserrat m-tb5 p-0" style="color: #4693fe;">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-sign-in fa-lg"></i> 
                                </span>
                                {ci_language line="Login with Amazon Account"}
                            </p>
                            <p class="poor-gray m-t10">
                                
                                </p>
                            <p class="poor-gray m-t10">
                                {ci_language line="Sign in with your Selling on Amazon account and click through the consent screens."}
                            </p>
                            <hr/> 
                        </div>                       
                        <div class="col-md-4 text-center p-t10">
                			<a href="#" id="LoginWithAmazon">
                			  <img border="0" alt="{ci_language line="Login with Amazon account"}"
                			    src="https://images-na.ssl-images-amazon.com/images/G/01/lwa/btnLWA_gold_156x32.png"
                			    width="156" height="32" />
                			</a>
                        </div>
                    </div>
                </div>
                <!-- 2 Confirm  Register Code-->
                <div id="adRegisInfo" class="m-t30" {if isset($sub_action) && $sub_action == 2}{else}style="display:none"{/if}>  
                    <div class="row">             
                        <div class="col-md-8 text-center">
                            <p class="text-uc montserrat m-b5 p-0 true-green">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-check-circle fa-lg"></i> 
                                </span>
                                {ci_language line="Login success"}
                            </p>
                            <p class="poor-gray m-t10">
                                {ci_language line="Please click \"Confirm Register Profile\" to register your Amazon Advertising profile."}
                            </p>
                            <hr/> 
                        </div>
                        <div class="col-md-4 text-center p-t10">
                            <button type="button" class="btn btn-save m-0" id="Register">
                                {ci_language line="Confirm Register Profile"}
                            </button>
                        </div>
                    </div>
                </div>
                <!-- 3 -->
                <div id="adProfileInfo" class="m-t30" {if isset($sub_action) && $sub_action == 3}{else}style="display:none"{/if}>  
                    <div class="row">   

                        <div class="col-md-8 text-center">
                            <p class="text-uc montserrat m-b5 p-0 true-green">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-check-circle fa-lg"></i> 
                                </span>
                                {ci_language line="Registeration success"}
                            </p> 
                            <p class="poor-gray m-t10">
                                {ci_language line="Now that you have a profile, you can create your first campaign."}
                            </p>  
                            <hr/>                             
                        </div>
                        <div class="col-md-4">
                            <div class="text-center">
                                {if isset($next_page)}
                                    <button type="button" class="btn btn-save m-0" id="next" type="button" value="{$next_page}">
                                        {ci_language line="Manage Profiles"}
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                    </button>                        
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-8">
                           <p class="text-uc montserrat m-b5 p-0">{ci_language line="Registeration Result"}</p>
                            <div class="row text-left">
                                <div class="col-md-4 m-t15">
                                    <p class="text-uc dark-gray montserrat m-b5 p-0">{ci_language line="Id"} : </p>
                                    <p class="poor-gray m-t10">1245454545454
                                        {if isset($profile_profileId)}
                                            {$profile_profileId}
                                        {/if}
                                    </p>
                                </div>                       
                                <div class="col-md-6 m-t15">
                                    <p class="text-uc dark-gray montserrat m-b5 p-0">{ci_language line="Status"} : </p>
                                    <p class="poor-gray m-t10 text-success">85752524525
                                        {if isset($profile_status)}
                                            {$profile_status}
                                        {/if}
                                    </p>                               
                                </div>
                                <div class="col-md-12 m-t15">
                                    <p class="text-uc dark-gray montserrat m-b5 p-0">{ci_language line="Description"} : </p>
                                    <p class="poor-gray m-t10 text-success">85752524525 1245454545454 12454545454541245454545454 1245454545454 1245454545454 1245454545454 1245454545454 1245454545454
                                        {if isset($profile_statusDetails)}
                                            {$profile_statusDetails}
                                        {/if}
                                    </p>                               
                                </div>
                            </div>           
                            <hr/>
                        </div>                         
                    </div>
                </div>

                <p class="poor-gray m-t10">
                    <button type="button" class="link" id="Logout">{ci_language line="Logout"}</button>
                </p> 
                <!--  -->     
                <!-- <div class="row custom-form m-t15"> 
    				<div class="form-group col-md-12">
    				 	<label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                            <span class="b-blue">{ci_language line="Authentication code"}</span>
                        </label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="code" value="{if isset($code)}{$code}{/if}" />
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn" id="RequestAccessToken">{ci_language line="Request access token"}</button> 
                                </div>
                            </div>
                            <p class="poor-gray m-t5">{ci_language line="The name of the ad group"}</p>
                        </div>
    				</div>
    				<div class="form-group col-md-12">
    				 	<label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                            <span class="b-blue">{ci_language line="Token Access"}</span>
                        </label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="token_access" />
                                </div>
                                <div class="col-md-4">
                                	<button type="button" class="btn" id="Register">{ci_language line="Register"}</button> 
                                </div>
                            </div>
                            <p class="poor-gray m-t5">{ci_language line="The name of the ad group"}</p>
                        </div>
    				</div>
    				<div class="form-group col-md-12">
    				 	<label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                            <span class="b-blue">{ci_language line="Register Profile Id"}</span>
                        </label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="register_profile_id" />
                                </div>
                                <div class="col-md-4">
                                	<button type="button" class="btn" id="GetProfileStatus">{ci_language line="Get Profile status"}</button>
                                </div>
                            </div>
                            <p class="poor-gray m-t5">{ci_language line="The name of the ad group"}</p>
                        </div>
    				</div>
    			</div> -->	
            </div>
            <div class="row m-t0 footer-step" {if isset($sub_action) && $sub_action == 4}{else}style="display:none"{/if}>
                <div class="col-xs-12">
                    <div class="b-Top p-t10">			             
                        {if isset($previous_page)}
                        <div class="inline pull-left">
                            <button class="btn btn-small p-size" id="back" type="button" value="{$previous_page}">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> 
                                {ci_language line="Manage Ad Groups"}
                            </button>
                        </div>
                        {/if}
                        {if isset($next_page)}
                        <div class="inline pull-right">  
                            <button class="btn btn-small p-size" id="next" type="button" value="{$next_page}">
                                {ci_language line="Manage Profiles"}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>                        
                        </div>
                        {/if}
                    </div>      
                </div>
            </div>

            <input type="hidden" class="form-control" id="token_access" value="{if isset($token_access)}{$token_access}{/if}" />
            <input type="hidden" class="form-control" id="register_profile_id" value="{if isset($register_profile_id)}{$register_profile_id}{/if}" />
            <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
            <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data"}" />
            {if isset($ad_client_id)}<input type="hidden" value="{$ad_client_id}" id="ad_client_id">{/if}
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}

        </div> 		

    {include file="amazon/PopupStepFooter.tpl"} 

</div> 

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->      
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/advertising.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}