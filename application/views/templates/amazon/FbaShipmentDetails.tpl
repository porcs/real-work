{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}

<form id="form-fba-setting" method="post" action="{$base_url}amazon/fba/save_fba/{$id_country}" enctype="multipart/form-data" autocomplete="off" >

    {include file="amazon/PopupStepMoreHeader.tpl"} 

    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                <h4 class="headSettings_head">{ci_language line="Ship From Address"}</h4>						
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="custom-form p-t10">
                <div class="row">
                    <div class="col-sm-8">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Name"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][Name]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.Name)}value="{$fba.shipment_details.ShipFrom.Name}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The name or business name."} {ci_language line="Maximum: 30 characters."}                     
                            </p>                           
                        </div>     
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Address 1"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][AddressLine1]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.AddressLine1)}value="{$fba.shipment_details.ShipFrom.AddressLine1}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The street address information."} {ci_language line="Maximum: 180 characters."}                     
                            </p>
                        </div>                                
                    </div> 
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Address 2"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][AddressLine2]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.AddressLine2)}value="{$fba.shipment_details.ShipFrom.AddressLine2}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="Additional street address information."} {ci_language line="Maximum: 60 characters."}                     
                            </p>
                        </div>                                
                    </div> 
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Address 3"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][AddressLine3]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.AddressLine3)}value="{$fba.shipment_details.ShipFrom.AddressLine3}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="Additional street address information."} {ci_language line="Maximum: 60 characters."}                     
                            </p>
                        </div>                                
                    </div>    
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="District Or County"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][DistrictOrCounty]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.DistrictOrCounty)}value="{$fba.shipment_details.ShipFrom.DistrictOrCounty}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The district or county."} {ci_language line="Maximum: 30 characters."}                     
                            </p>
                        </div>                                   
                    </div>
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="City"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][City]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.City)}value="{$fba.shipment_details.ShipFrom.City}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The city."} {ci_language line="Maximum: 30 characters."}                     
                            </p>
                        </div>                                   
                    </div>    
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="State Or Province Code"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][StateOrProvinceCode]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.StateOrProvinceCode)}value="{$fba.shipment_details.ShipFrom.StateOrProvinceCode}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The state or province code."} {ci_language line="Maximum: 30 characters."}                     
                            </p>
                        </div>                                  
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Postal Code"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][PostalCode]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.PostalCode)}value="{$fba.shipment_details.ShipFrom.PostalCode}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The zip code or postal code."} {ci_language line="Maximum: 30 characters."}                     
                            </p>
                        </div>                                   
                    </div> 
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Country Code"} :</label>
                        <div class="form-group">
                            <div class="m-l0 p-l0">
                                <input name="shipment_details[ShipFrom][CountryCode]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.CountryCode)}value="{$fba.shipment_details.ShipFrom.CountryCode}"{/if}/>
                            </div>
                            <p class="col-xs-12 m-l0 p-l0 regRoboto poor-gray text-left m-t5">
                                {ci_language line="The country code."} {ci_language line="A two-digit ISO 3166-1 alpha-2 format country code."}                     
                            </p>
                        </div>                                   
                    </div>                       
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Email"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][Email]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.Email)}value="{$fba.shipment_details.ShipFrom.Email}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The email address."} {ci_language line="Maximum: 256 characters."}                     
                            </p>
                        </div>                               
                    </div>
                    <div class="col-sm-4">
                        <label class="text-uc dark-gray montserrat m-b5">{ci_language line="Phone"} :</label>
                        <div class="form-group">
                            <input name="shipment_details[ShipFrom][Phone]" type="text" class="form-control" {if isset($fba.shipment_details.ShipFrom.Phone)}value="{$fba.shipment_details.ShipFrom.Phone}"{/if}/>
                            <p class="regRoboto poor-gray text-left m-t5">
                                {ci_language line="The phone number."} {ci_language line="Maximum: 30 characters."}                     
                            </p>
                        </div>                               
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <p class="text-uc dark-gray montserrat m-t20 m-b15">{ci_language line="Shipping Service Options"}</p>
            <div class="form-group">
                <div class="m-l0 p-l0">
                    <select class="search-select" name="shipment_details[ShippingServiceOptions]">
                        <option value="">--</option>                                         
                        <option value="DeliveryConfirmationWithAdultSignature" {if isset($fba.shipment_details.ShippingServiceOptions) && $fba.shipment_details.ShippingServiceOptions == "DeliveryConfirmationWithAdultSignature"}selected{/if}>
                            {ci_language line="Delivery confirmation with adult signature."}
                        </option>
                        <option value="DeliveryConfirmationWithSignature" {if isset($fba.shipment_details.ShippingServiceOptions) && $fba.shipment_details.ShippingServiceOptions == "DeliveryConfirmationWithSignature"}selected{/if}>
                            {ci_language line="Delivery confirmation with signature."}
                        </option>
                        <option value="DeliveryConfirmationWithoutSignature" {if isset($fba.shipment_details.ShippingServiceOptions) && $fba.shipment_details.ShippingServiceOptions == "DeliveryConfirmationWithoutSignature"}selected{/if}>
                            {ci_language line="Delivery confirmation without signature."}
                        </option>
                        <option value="NoTracking" {if isset($fba.shipment_details.ShippingServiceOptions) && $fba.shipment_details.ShippingServiceOptions == "NoTracking"}selected{/if}>
                            {ci_language line="No delivery confirmation."}
                        </option>
                    </select> 
                </div>
                <p class="regRoboto poor-gray text-left m-t10 m-l0 p-l0  col-sm-6">
                    {ci_language line="The delivery confirmation level."}                     
                </p>
            </div>   
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="text-uc dark-gray montserrat m-t25 m-b15">{ci_language line="Carrier Will PickUp"}</p>
            <div class="custom-form">
                <div class="form-group m-l0 p-l0" {if isset($fba.shipment_details.CarrierWillPickUp) && $fba.shipment_details.CarrierWillPickUp}checked{/if}>
                    <label class="cb-checkbox w-100 text-uc dark-gray montserrat">
                        <input type="checkbox" name="shipment_details[CarrierWillPickUp]" value="1" {if isset($fba.shipment_details.CarrierWillPickUp) && $fba.shipment_details.CarrierWillPickUp}checked{/if}/> 
                        {ci_language line="True"}
                    </label>
                    <p class="regRoboto poor-gray text-left m-l0 p-l0">
                        {ci_language line="Indicates whether the carrier will pick up the package."}
                        {ci_language line="true if the carrier will pick up the package, otherwise false."}
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix p-b10 b-Bottom m-b10">
                <h4 class="headSettings_head">{ci_language line="Declared Value"}</h4>						
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <div class="col-sm-4">
                <label class="text-uc dark-gray montserrat">{ci_language line="Amount"} </label> 
                <input name="shipment_details[DeclaredValue][Amount]" type="text" class="form-control" {if isset($fba.shipment_details.DeclaredValue.Amount)}value="{$fba.shipment_details.DeclaredValue.Amount}"{/if}/>
            </div>
            <div class="col-sm-4">
                <label class="text-uc dark-gray montserrat">{ci_language line="Currency Code"} </label>
                <input name="shipment_details[DeclaredValue][CurrencyCode]" type="text" class="form-control" {if isset($fba.shipment_details.DeclaredValue.CurrencyCode)}value="{$fba.shipment_details.DeclaredValue.CurrencyCode}"{/if}/>
                <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Three-digit currency code."}</p>
            </div>
            <div class="col-sm-4">
                <p class="regRoboto poor-gray text-left m-l0 p-l0 m-t25">
                    {ci_language line="The declared value of the shipment. The carrier uses this value to determine how much to insure the shipment for."}
                </p>
            </div>
        </div>
    </div>

    {include file="amazon/PopupStepMoreFooter.tpl"}

</form>

<input type="hidden" id="set-fba-confirm" value="{ci_language line="Are you sure to set"} <span style='color:red'>{if isset($country)}{$country}{/if} </span> {ci_language line="to FBA Master platform for"} <span style='color:green'> {ci_language line="Amazon"} {if isset($region)}{$region}{/if} </span> ?" />
<input type="hidden" id="unset-fba-confirm" value="{ci_language line="Are you sure to"} <span style='color:red'> <b>{ci_language line="remove"}</b> {if isset($country)}{$country}{/if} </span> {ci_language line="from FBA Master platform for"} <span style='color:green'> {ci_language line="Amazon"} {if isset($region)}{$region}{/if} </span> ?" />
<input type="hidden" id="error-message-title" value="{ci_language line="Error encountered while saving data!"} <br/> {ci_language line="Please try again."}" />
<input type="hidden" id="success-message-title" value="{ci_language line="Save successful."}" />
<input type="hidden" id="l_Master-Platform" value="{ci_language line="Master Platform"}" />
<input type="hidden" id="l_No-master-platform" value="{if isset($country)}{$country}{/if} {ci_language line="are removed from"} {ci_language line="FBA Master Platform"}" />

<script src="{$cdn_url}assets/js/FeedBiz/amazon/fba.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}    
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}