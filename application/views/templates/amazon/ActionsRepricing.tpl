{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#repricing" data-toggle="tab" class="">
                    <i class="icon-money"></i> <span>{ci_language line="Repricing"}</span>
                </a>
            </li>                                     
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <div class="tab-pane active" id="repricing">
              
                <div class="status" style="display: none">
                    <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                </div>

                <div id="repricing_options">  

                    <!--Analysis-->
                    <p class="head_text montserrat black p-t20 b-None">{ci_language line="Repricing - Analysis"}</p>
                    <div class="row">
                        <div class="col-sm-8 col-lg-9">
                            <ul class="p-l20">
                                <li>
                                    <p class="poor-gray">
                                        {ci_language line='Upon clicking the "Repricing - Analysis" Button the repricing will be starting analysis'}
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div> 
                            <div class="col-sm-4 col-lg-3 sm-right">
                                 <button class="btn btn-save" id="repricing-analysis" type="button" value="{$repricing_analysis}">
                                    <i class="fa fa-line-chart"></i> {ci_language line="Repricing - Analysis"}
                                </button>
                                <p></p>
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting Reprice"} ..</p>
                                </div>
                                <div class="error sm-right" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Export-->
                    <p class="head_text montserrat black b-None">{ci_language line="Repricing - Export"}</p>
                    <div class="row">
                        <div class="col-sm-8 col-lg-9">
                            <ul class="p-l20">
                                <li>
                                    <p class="poor-gray">
                                        {ci_language line='Upon clicking the "Repricing - Export" Button the repricing will be updated on Amazon'}
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div> 
                            <div class="col-sm-4 col-lg-3 sm-right">
                                <button class="btn btn-danger m-b15" id="repricing-export" type="button" value="{$repricing_export}">
                                    <i class="fa fa-share-square-o"></i> {ci_language line="Repricing - Export"}
                                </button>
                                <p></p>
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting Export"} ..</p>
                                </div>
                                <div class="error" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row"><div class="col-sm-12"><hr/></div></div>

                    <p class="head_text montserrat black p-t20 b-None">{ci_language line="Trigger a reprice for all offers"}</p>
                    <div class="row">
                        <div class="col-sm-8 col-lg-9">
                            <ul class="p-l20">
                                <li>
                                    <p class="poor-gray">
                                        {ci_language line='Upon clicking the "Trigger a reprice for all offers" Button the repricing will be updated on Amazon'}                                       
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div id="parameter_options"> 
                            <div class="col-sm-4 col-lg-3 sm-right">
                                <button class="btn btn-rule m-b15" id="repricing-trigger" type="button" value="{$repricing_trigger}">
                                    <i class="fa fa-checked"></i> {ci_language line="Trigger a reprice for all offers"}
                                </button>
                                <p></p>
                                <div class="status sm-right" style="display: none;">
                                    <p>Starting trigger ..</p>
                                </div>
                                <div class="error sm-right" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
    
<input type="hidden" value="{ci_language line="Are you sure to repricing product?"}" id="repricing-analysis-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to export price?"}" id="repricing-export-confirm" />
<input type="hidden" value="{ci_language line="Are you sure to trigger a reprice?"}" id="repricing-trigger-confirm" />

<input type="hidden" value="{ci_language line="Trigger reprice successful, your repring will start next syncronize tasks."}" id="success-trigger-reprice" />
<input type="hidden" value="{ci_language line="Trigger reprice unsuccessful, please try again."}" id="success-trigger-reprice" />

<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}