{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}   
     
<div class="row-fluid" id="matching-content">
    
    <div class="clearfix text-center" id="page-load">
        <div class="feedbizloader m-t20"><div class="feedbizcolor"></div></div>        
        <h4 class="m-t20"></h4>
    </div>
    
    <!--Loading-->
    <div class="row" id="loading" style="display: none">
        <div class="col-xs-12 m-t20">
            <div class="pull-left">
                <div id="message">
                    <h4 class="m-t15"></h4>
                    <h5></h5>
                </div>
            </div>
            <div class="pull-right m-t20" id="loading-content">
                <img src="{$cdn_url}assets/images/loader-connection.gif" />
            </div>
        </div>
    </div>
    
    <!--Loaded-->
    <div id="loaded" style="display: none">
                        
        <!--Sync-->
        <div id="sync" style="display:none;" class="m-b40 p-b20">

            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Bottom b-Top-None">
                        <h4 class="headSettings_head">{ci_language line="You have existing inventory on Amazon."}</h4>
                    </div>                      
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 m-t20">
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Local"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span id="feed_no_product" class="dark-gray bold"></span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Amazon"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span id="amazon_no_product" class="dark-gray bold">0</span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Marked as to be synchronize"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span  id="product_to_sync" class="dark-gray bold">0</span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="b-Top m-t5 p-t10">
                        <p id="question" class="montserrat dark-gray text-uc m-t10">{ci_language line="What would you like to do?"}</p>

                        <div class="form-group has-error" id="sync-send-message" style="display: none;">
                            <div class="error">
                                <p>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>  
                        </div>

                        <div class="text-right">
                            <button class="btn btn-wizard p-l12" type="button" id="synchronization_only" title="{ci_language line="Offers only"}">
                                {ci_language line="Synchronize only"}
                            </button>
                            <button class="btn btn-amazon p-l12" type="button" id="synchronization_match" title="{ci_language line="Offers and match"}">
                                {ci_language line="Synchronize and match"}
                            </button>
                        </div>
                    </div>      
                </div>
            </div>
        </div>
        
        <!--Sync & Match-->
        <div id="sync_match" style="display:none;" class="m-b40 p-b20">

            <div id="amazon-automaton-matching-header" class="row">
                <div id="tree1" class="col-xs-12">
                    <div class="headSettings clearfix b-Bottom b-Top-None p-0">
                        <h4 class="headSettings_head pull-md-left p-t10 m-b0">{ci_language line="synchronize and match"}</h4>                                        
                        <div class="wizardSettings pull-md-right custom-form">
                            <div class="pull-md-left">
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" checked="checked" id="matching-load-image"></i></span>
                                    {ci_language line="Load images"}
                                </label>
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" id="matching-display-unmatched" checked="checked"></i></span>
                                    {ci_language line="Display unmatched"}
                                </label>
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" id="matching-display-selectall"></i></span>
                                    {ci_language line="Select all"}
                                </label>
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" id="autoload"></i></span>
                                    {ci_language line="Auto load"}
                                </label>
                            </div>                                  
                            <ul class="wizardSettings_list pull-md-right">
                                <li class="wizardSettings_confirm" id="amazon-automaton-matching-action-confirm">
                                    {ci_language line="Confirm"}
                                </li>
                                <li class="wizardSettings_reject" id="amazon-automaton-matching-action-reject">
                                    {ci_language line="Reject"}
                                </li>
                                <li class="wizardSettings_send" id="finish" disabled>
                                    {ci_language line="Send"}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--MODEL-->
            <div id="amazon-automaton-matching-product-model" style="display:none">
                <div class="selection selectable clearfix">
                    <table class="matching-product-left">
                        <tr>
                            <td rel="image" class="image">
                                <img src="{$cdn_url}assets/images/loader.gif" alt="{ci_language line="Loading"}" rel="loader" style="height:16px" />
                                <img src="{$cdn_url}assets/images/no-image.png" alt="{ci_language line="Loading"}" rel="nope" style="height:64px; display:none" />
                            </td>
                            <td class="content">
                                <span rel="name" class="name"></span><br />
                                <span rel="manufacturer" class="manufacturer"></span> | 
                                <span rel="reference" class="reference"></span> | 
                                <span rel="code" class="code"></span>
                            </td>
                            <td>
                            <img src="{$cdn_url}assets/images/warning.png" alt="{ci_language line="Mismatch"}" class="mismatch" title="{ci_language line="Brand Mismatch !"}" />
                            </td>
                        </tr>
                    </table>    

                    <table class="matching-product-right">
                    <tr>
                        <td class="content">
                            <span rel="amazon_name" class="name"></span><br />
                            <span rel="amazon_brand" class="brand"></span> | 
                            <span rel="amazon_asin" class="asin"></span> | 
                            <span rel="code" class="code"></span>
                        </td>
                        <td rel="amazon_image" class="image">
                            <img src="{$cdn_url}assets/images/loader.gif" alt="{ci_language line="Loading"}" rel="loader" style="height:16px" />
                            <img src="{$cdn_url}assets/images/no-image.png" alt="{ci_language line="Loading"}" rel="nope" style="height:64px;display:none" />
                        </td>
                    </tr>
                    </table>
                </div>
            </div> 

            <div id="amazon-automaton-matching-body">
                <table class="amazon-automaton-matching-body-header">
                    <tr>
                        <td class="">
                            <p class="montserrat dark-gray text-uc">
                                <img src="{$cdn_url}assets/images/house.png" /> 
                                {ci_language line="Offers on the shop side, but not in your Amazon inventory"}:
                            </p>
                        </td>
                        <td class="clearfix">
                            <p class="montserrat dark-gray text-uc">
                                <img src="{$cdn_url}assets/images/a32.gif" />
                                {ci_language line="Matching products on Amazon which allows you to create an offer"}:
                            </p>
                        </td>
                    </tr>
                </table>
            
                <!-- Product Matching Container -->
                <div id="auto_loading" class="m-b15 text-center" style="display: none">
                    <p class="dark-gray text-uc">
                        {ci_language line="Auto loading offer"} &nbsp; 
                        <img src="{$cdn_url}assets/images/loader-connection.gif" alt="{ci_language line="Loading"}" /> 
                    </p>
                </div>  
                <div id="amazon-automaton-matching-products">
                    {*include file="amazon/test.tpl"*} 
                </div>
                
            </div>    
                    
            <div id="amazon-automaton-matching-products-loader" class="amazon-automaton-matching-products-loader" style="display: none">
                <img src="{$cdn_url}assets/images/loading.gif" alt="{ci_language line="Loading"}" />
            </div>  
            
            <div style="display: none">
                <div id="loadmore" class="row" style="cursor:pointer">
                    <div class="col-xs-12">                         
                        <div class="wizardItem empty">{ci_language line="Load more"}</div>
                    </div>
                </div>                
            </div>  
        </div>
             
        <!--Match-->
        <div id="match" style="display:none;" class="m-b40 p-b20">
            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Bottom b-Top-None">
                        <h4 class="headSettings_head">{ci_language line="You don't have existing inventory on Amazon."}</h4>
                    </div>                      
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12">
                    <div class="m-t5 p-t10">
                        <p id="question" class="montserrat dark-gray text-uc m-t10">{ci_language line="What would you like to do?"}</p>
                        <div class="form-group has-error" id="match-send-message" style="display: none;">
                            <div class="error">
                                <p>
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <button class="btn btn-amazon p-l40 p-r40" type="button" id="match_only" title="{ci_language line="Matching Product"}">
                                    {ci_language line="Matching Product"}
                                </button>
                                <p class="poor-gray">{ci_language line='The goal is to create your offers on Amazon.'}</p>
                            </div>
                            {if isset($amazon_display[$ext]['creation']) && $amazon_display[$ext]['creation']}
                                <div class="col-xs-4">   
                                    <button class="btn btn-amazon p-l40 p-r40" type="button" id="create_product" title="{ci_language line="Amazon product creation"}">
                                        {ci_language line="Amazon product creation"}
                                    </button> 
                                    <p class="poor-gray">{ci_language line='The goal is to create the unknown items on Amazon.'}</p>
                                </div>
                            {/if}
                        </div>                                                                                
                    </div>      
                </div>
            </div>
        </div>
               
        <div id="popup_prev" class="col-xs-12 popup_prev b-Top" style="display:none;">
            <div class="">
                <div class="inline pull-left p-b20"> 
                    <button class="pull-left link p-size m-tr10" id="prev" type="button">
                        {ci_language line="Previous"}
                    </button>
                </div>                                             
            </div>   
        </div> 

    </div>   
    
    <div id="popup_action" class="row popup_prev b-Top">
        <div class="col-xs-12">
            <div class="inline pull-left p-b20"> 
                <button class="pull-left link p-size m-tr10" id="back" type="button">
                    {ci_language line="Previous"}
                </button>
            </div>
            <input type="hidden" id="please_wait" value="{ci_language line="Please wait"}" />                        
            <input type="hidden" id="no_product" value="{ci_language line="No products"}" />
            <input type="hidden" id="processing" value="{ci_language line="Processing"}.." />
            <input type="hidden" id="Connecting-to-Amazon" value="{ci_language line="Connecting to Amazon"}.." />
            <input type="hidden" id="No-items-found" value="{ci_language line="No items found."}" />
            <input type="hidden" id="Send" value="{ci_language line="Send"}" />
            <input type="hidden" id="product_s" value="{ci_language line="item(s)"}" />                
            <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
            <input type="hidden" id="mode_match" value="{if isset($mode_match)}{$mode_match}{/if}" />
            <input type="hidden" id="mode_sync" value="{if isset($mode_sync)}{$mode_sync}{/if}" />
            <input type="hidden" id="mode_sync_match" value="{if isset($mode_sync_match)}{$mode_sync_match}{/if}" />
            <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
            <input type="hidden" id="id_shop" value="{if isset($id_shop)}{$id_shop}{/if}" />
            {if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}          
        </div>
    </div>

</div><!--row-fluid-->
            
{include file="amazon/PopupStepFooter.tpl"}
                        
<input type="hidden" value="{if isset($country)}{str_replace(array(' '), '_', $country)}{/if}" id="country">
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">          

<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/matching.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}