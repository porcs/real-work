{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}

<form id="amazon-form" method="POST" action="{$base_url}amazon/save_profiles" name="amazon-form" >

    {include file="amazon/PopupStepMoreHeader.tpl"} 

    {if isset($popup_more['no_model']) && $popup_more['no_model'] }
    	<input type="hidden" id="popup_more_no_model" value="$popup_more['no_model']" />
    {/if}

    <div id="main" class="amazon-profiles showSelectBlock">

        <div class="col-md-11 col-lg-11 custom-form m-t10 m-b30">
        	<button class="link removeProfile remove" type="button">{ci_language line="Remove a profile from list"}</button>

			<div class="profileBlock clearfix amazon-profile">

				{if !isset($popup)}
					<!--Information-->
					<div class="validate blue">
	                    <div class="validateRow">
	                        <div class="validateCell">
	                            <i class="note"></i>
	                        </div>
	                        <div class="validateCell">
	                            <p class="pull-left">
	                        		{ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
	                            </p>
	                        </div>
	                    </div>
	                </div>
                {/if}

                <!--Profile Name-->
				<p class="regRoboto poor-gray">{ci_language line="Profile Name"}</p>
				<div class="form-group has-error">
					<input type="text" rel="name" class="form-control" />  
					<div class="error">
						<p>
							<i class="fa fa-exclamation-circle"></i>
							{ci_language line="Set Profile Name First"}
						</p>
					</div>										
				</div>
				<p class="poor-gray text-right">
                    {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
                </p>

				<!--Model Name-->
	            {if isset($creation)}
					<div class="row">
						<div class="col-xs-12 p-t10">
							<div class="b-Top p-t20 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Model"}			
								</p>
							</div>
						</div>
					</div>		
					<div class="row">
						<div class="col-xs-12">
						 	{if isset($models)}
								<div class="form-group">
			                        <select rel="id_model" disabled>
			                            <option value="">{ci_language line="Select a model"}</option>
		                                {foreach from=$models item=models_item}
		                                    <option value="{$models_item.id_model}">{$models_item.name}</option>
		                                {/foreach} 
			                        </select>  
			                    </div>
		                        <p class="regRoboto poor-gray text-right">{ci_language line="Choose a model"}.</p>                       
		                    {else}
		                        {if isset($no_models)}{$no_models}{/if}
		                    {/if}							
						</div>
					</div>
				{/if}

				<!--Out of Stock-->				
				<p class="regRoboto poor-gray b-Top p-t20">
					{ci_language line="Stock Break-even"}
					{if (isset($mode_default) && $mode_default == 1)}
						<span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
					{/if}
				</p>
				<div class="form-group m-b20">
					<div class="row">
						<div class="col-xs-4">
							<input type="text" {if (isset($mode_default) && $mode_default <> 1)}rel="out_of_stock"{/if} disabled class="form-control" />		
						</div>
						<p class="regRoboto poor-gray text-left p-t10"> {ci_language line="Quantity to deduct to the actual stock to export the product."}</p>
					</div>
					<div class="error text-right" style="display:none">
						<p class="m--t10 p-0">
							<i class="fa fa-exclamation-circle"></i>
							<span>{ci_language line="Allow only digit"}.</span>
						</p>
					</div>
				</div>	

				<!-- Synchronization Field -->
			 	{if isset($amazon_display[$ext]['synchronization_field']) && $amazon_display[$ext]['synchronization_field']}
					<p class="regRoboto poor-gray">{ci_language line="Synchronization Field"}</p>
					<div class="row">
				        <div class="col-xs-4">
							<div class="m-b10 synch">
								<div class="choZn">
								 	<select data-placeholder="Choose" rel="synchronization_field" disabled>
				                        <option value="" disabled="disabled">{ci_language line="Choose one of the following"}</option>
				                        <option value="ean13">{ci_language line="EAN13 (Europe)"}</option>
				                        <option value="upc">{ci_language line="UPC (United States)"}</option>
				                        <option value="both" selected="">{ci_language line="Both (EAN13 then UPC)"}</option>
				                        <option value="reference">{ci_language line="SKU"}</option>
				                    </select>  							
								</div>
							</div>	
						</div>
						<p class="regRoboto poor-gray text-left">
							{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Amazon."}
						</p>
					</div>
	            {/if}

	            <!-- ASIN has the Priority -->
				<p class="regRoboto poor-gray m-t20">
					{ci_language line="ASIN has the Priority"}
					{if (isset($mode_default) && $mode_default == 1)}
						<span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
					{/if}
				</p>
	            <div class="row">
			        <div class="col-xs-4">
						<div class="form-group m-b10{if (isset($mode_default) && $mode_default == 1)} disabled{/if}">
							<label class="amazon-checkbox pull-left">
							    <span class="amazon-inner">
							        <i>
							        	<input {if (isset($mode_default) && $mode_default <> 1)}rel="[association][use_asin]"{/if} type="checkbox" disabled value="1" />
							        </i>
							    </span>
							    {ci_language line="Yes"}
							</label>
						</div>	
					</div>
					<p class="regRoboto poor-gray text-left">
						{ci_language line="Use the ASIN field as synchronization field in priority if filled. This parameter could override the Synchronization Field."}
					</p>
			    </div>
	            
				<div class="row m-t20">
					
	            	<!-- Title Format -->
					<div class="col-sm-5 col-md-6 col-lg-5">
						<p class="montserrat dark-gray text-uc">{ci_language line="Title Format"}</p>
						<label class="amazon-radio w-100 checked">
							<span class="amazon-inner">
								<i><input rel="title_format" type="radio" value="1" checked="checked" disabled></i>
							</span>
							{ci_language line="Standard Title, Attributes"}	
						</label>
						<label class="amazon-radio w-100">
							<span class="amazon-inner">
								<i><input rel="title_format" type="radio" value="2" disabled></i>
							</span>
							{ci_language line="Manufacturer, Title, Attributes"}
						</label>
						<label class="amazon-radio w-100">
							<span class="amazon-inner">
								<i><input rel="title_format" type="radio" value="3" disabled></i>
							</span>
							{ci_language line="Manufacturer, Title, Reference, Attributes"}
						</label>						
					</div>

					<!-- HTML Descriptions -->
					<div class="col-sm-5 col-md-6 col-lg-5">
						<p class="montserrat dark-gray text-uc">{ci_language line="HTML Descriptions"}</p>
						<div class="clearfix">
							<label class="amazon-checkbox pull-left">
							    <span class="amazon-inner">
							        <i>
							        	<input rel="html_description" type="checkbox" disabled value="1">
							        </i>
							    </span>
							    {ci_language line="Yes"}
							</label>
							{*<label class="amazon-checkbox pull-right">
							    <span class="amazon-inner">
							        <i><input rel="unconditionnaly" type="checkbox" disabled value="1"></i>
							    </span>
							    {ci_language line="Unconditionnaly"}
							</label>*}							
						</div>
						<p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
					</div>

		            <!-- Title Format Descriptions -->
					<div class="col-xs-12">
						<p class="regRoboto poor-gray m-t10">
							{ci_language line="Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes."}
						<!-- </p>
						<p class="poor-gray"> -->
							{ci_language line="Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon."}
						</p>
					</div>

				</div>

				<div class="row m-t20">

					<!-- Description Field -->
					<div class="col-sm-5 col-md-6 col-lg-5">
						<p class="montserrat dark-gray text-uc">{ci_language line="Description Field"}</p>
						<label class="amazon-radio w-100 checked">
							<span class="amazon-inner">
								<i><input rel="description_field" type="radio" checked="checked" value="1" disabled></i>
							</span>
							{ci_language line="Description"}
						</label>
						<label class="amazon-radio w-100">
							<span class="amazon-inner">
								<i><input rel="description_field" type="radio" value="2" disabled></i>
							</span>
							{ci_language line="Short Description"}
						</label>
						<label class="amazon-radio w-100">
							<span class="amazon-inner">
								<i><input rel="description_field" type="radio" value="3" disabled></i>
							</span>
							{ci_language line="Both"}
						</label>
						<label class="amazon-radio w-100">
							<span class="amazon-inner">
								<i><input rel="description_field" type="radio" value="4" disabled></i>
							</span>
							{ci_language line="None"}
						</label>
					</div>

					<!-- SKU as Supplier Reference -->
				 	{if isset($amazon_display[$ext]['sku_as_supplier_reference']) && $amazon_display[$ext]['sku_as_supplier_reference']} 
					 	<div class="col-sm-5 col-md-6 col-lg-5">
							<p class="montserrat dark-gray text-uc">{ci_language line="SKU as Supplier Reference"}</p>
							<div class="clearfix">
								<label class="amazon-checkbox pull-left">
								    <span class="amazon-inner">
								        <i>
								        	<input rel="sku_as_supplier_reference" type="checkbox" disabled value="1">
								        </i>
								    </span>
								    {ci_language line="Yes"}
								</label>
								<label class="amazon-checkbox pull-right">
								    <span class="amazon-inner">
								        <i>
								        	<input rel="unconditionnaly" type="checkbox" disabled value="1">
								        </i>
								    </span>
								    {ci_language line="Unconditionnaly"}
								</label>								
							</div>
							<p class="regRoboto poor-gray text-left m-t5">
								{ci_language line="Send the Reference/SKU as the Supplier Reference"}
							</p>
						</div>
		            {/if}
					
					<!-- Description field -->
					<div class="col-xs-12">
						<p class="poor-gray m-t10">
							{ci_language line="Whether to send short or long description of the product to Amazon."}
						</p>
					</div>

				</div>

				<!-- Default condition note -->
				<div class="row">
					<div class="col-xs-12">
						<p class="poor-gray m-t10">{ci_language line="Default Condition Note"}</p>
						<div class="form-group">
							<input type="text" rel="[association][condition_note]" disabled class="form-control" />						
						</div>
						<p class="poor-gray text-right">{ci_language line="Short text about product condition / state which will appear on the product details sheet on Amazon."}</p>
					</div>
				</div>

	            <!-- Latency -->
				<div class="m-t20">
					<p class="poor-gray m-t10">{ci_language line="Latency"}</p>
					<div class="row">
						<div class="form-group">
							<div class="col-xs-4">
								<input type="text" rel="[association][latency]" disabled class="form-control" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>		
							</div>
							<p class="regRoboto poor-gray text-left p-t4">{ci_language line="Latency delay in days before this product will be shipped (ie: preparation time, your own delay before shipment)"}.</p>
						</div>
					</div>
				</div>

				<!-- Default Shipping Template -->
				<div class="row">
					<div class="col-xs-12 p-t10">
						<div class="b-Top p-t20 clearfix">
							<p class="head_text p-b20">
								{ci_language line="Default Shipping Template"}	
								{if (isset($mode_default) && $mode_default == 1)}
								  	<span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
								{/if}			
							</p>
						</div>
					</div>
				</div>					
				
				{if (isset($mode_default) && $mode_default == 1)}
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
							 	<select{if (isset($mode_default) && $mode_default == 1)} disabled{else} class="search-select"{/if}>
			                        <option value="0"></option>						                       
			                    </select>						                    		
							</div>							
						</div>
					</div>
				{else}
					{if isset($shipping_groups) && !empty($shipping_groups)}	
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
								 	<select rel="[association][shipping_group]" disabled>
				                        <option value="0" selected></option>
				                        {if isset($shipping_groups)}
					                        {foreach $shipping_groups as $group_key => $group_name}
				                        		<option value="{$group_key}">{$group_name}</option>
					                        {/foreach}	
				                        {/if}
				                    </select>
				                    		
								</div>							
							</div>
						</div>
					{else}
					 	{if isset($no_shipping_groups)}{$no_shipping_groups}{/if}	
	                {/if}	
                {/if}

                <p class="regRoboto poor-gray text-left p-t4">
                	{*ci_language line="As you configured shipping templates in Seller Central you can specify here the default shipping template belonging to this profile."*}
                	{ci_language line="All the products related to this profile will have this shipping template applied, except if you specify another value directly on the offers options, then, this one will replace the default shipping template."}
                	{*ci_language line="If specified nowhere, then, your default shipping settings configured in Seller Central will be used."*}
                </p>
			                
				{if isset($amazon_display[$ext]['repricing']) && $amazon_display[$ext]['repricing']} 
					<!-- Repricing -->
					<div class="row">
						<div class="col-xs-12 p-t10">
							<div class="b-Top p-t20 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Repricing Strategy"}			
								</p>
							</div>
						</div>
					</div>					
					
					{if isset($repricing_strategies) && !empty($repricing_strategies)}	
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
								 	<select rel="[category][repricing]" disabled>
				                        <option value="0" selected></option>
				                        {if isset($repricing_strategies)}
					                        {foreach $repricing_strategies as $strategies}
				                        		<option value="{$strategies['id_strategy']}">{$strategies['name']}</option>
					                        {/foreach}	
				                        {/if}
				                    </select>
				                    		
								</div>							
							</div>
						</div>
					{else}
					 	{if isset($no_repricing_strategies)}{$no_repricing_strategies}{/if}	
                    {/if}		
					
				{/if}

				{if isset($amazon_display[$ext]['advertising']) && $amazon_display[$ext]['advertising']} 
					<!-- Advertising -->
					<div class="row">
						<div class="col-xs-12 p-t10">
							<div class="b-Top p-t20 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Ad Group"}			
								</p>
							</div>
						</div>
					</div>					
					{if isset($advertising_groups) && !empty($advertising_groups)}	
						<div class="row">
							<div class="col-xs-12">
								<div class="row">
									<div class="form-group advertising_groups_main withIcon clearfix">
	                                	<div class="col-xs-4">
											<select rel="[advertising][adGroupId][]" disabled>
						                        <option value="">{ci_language line="Select an Option"}</option>
						                        {if isset($advertising_groups)}
							                        {foreach $advertising_groups as $adGroups}
							                        	<option value="{$adGroups['adGroupId']}">{$adGroups['name']}</option>
							                        {/foreach}	
						                        {/if}
						                    </select>	
	                                    </div>
                                        <i class="cb-plus good addAdGroup"></i>
										<i class="cb-plus bad removeAdGroup" style="display: none"></i> 
						            </div>
								</div>
							</div>
						</div>
					{else}
					 	{if isset($no_advertising_groups)}{$no_advertising_groups}{/if}	
                    {/if}		
					<p class="regRoboto poor-gray text-left p-t4">
	                	{ci_language line="All the products related to this profile will added to this ad group applied, except if you specify another value directly on the offers options, then, this one will replace the default Ad Groups."}
	                </p>
				{/if}

				{if isset($amazon_display[$ext]['code_examption']) && $amazon_display[$ext]['code_examption']} 
					<!-- Code Exemption -->
					<div class="row">
                    	<div class="col-xs-12">
							<div class="b-Top p-t20 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Code Exemption"}			
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<label class="amazon-checkbox">
							    <span class="amazon-inner">
							        <i>
							        	<input rel="[code_exemption][chk]" type="checkbox" value="1" disabled>
							        </i>
							    </span>
							    {ci_language line="Use EAN/UPC exemption for this profile"}
							</label>							
						</div>
					</div>

					<!-- Field -->
					<div class="form-group code_exemption_field m-t20" style="display: none;">
						<p class="regRoboto poor-gray">{ci_language line="Fields"}</p>
						<div class="m-b10">
							<div class="choZn">
							 	<select rel="[code_exemption][feild]">
			                        <option value="0" selected></option>
			                        <option value="1" style="display:none"></option>
			                        <option value="2">{ci_language line="Model Number"}</option>
			                        <option value="3">{ci_language line="Model Name"}</option>
			                        <option value="4">{ci_language line="Manufacturer Part Number"}</option>
			                        <option value="5">{ci_language line="Catalog Number"}</option>
			                        <option value="10">{ci_language line="Generic"}</option>
			                    </select>					
							</div>
						</div>	
						<div class="row">
							<div class="col-xs-12">
								<div class="pull-left">
									<label class="amazon-checkbox">
									    <span class="amazon-inner">
									        <i>
									        	<input rel="[code_exemption][private]" type="checkbox" value="1">
									        </i>
									    </span>
									    {ci_language line="Private Label"}
									</label>								
								</div>
								<div class="pull-right ">
									<p class="regRoboto poor-gray">{ci_language line="Use EAN/UPC exemption for this profile"}.</p>
								</div>
							</div>
						</div>
		            </div>

		            <!-- Concerns -->
		            <div class="form-group code_exemption_concerns m-t20" style="display: none;">
		            	<p class="regRoboto poor-gray">{ci_language line="Concerns"}</p>
						<div class="m-b10">
							<div class="choZn">
							 	<select rel="[code_exemption][concerns]">
			                        <option value="">{ci_language line="Select an Option"}</option>
			                        <option value="Manufacturer">{ci_language line="Manufacturer"}</option>
			                        <option value="All">{ci_language line="All"}</option>
			                    </select>				
							</div>
						</div>	
		            </div>

		            <div class="code_exemption_concerns_type"></div>

				{/if}
				
				<!-- Recommended Browse Node -->
				<div class="recommended_browse_node">					

					<div class="row">
                    	<div class="col-xs-12 m-t10">
							<div class="b-Top p-t20 m-t10 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Recommended Browse Node"}	
								</p>
							</div>
						</div>
					</div>

					{if isset($ext) && $ext == ".com" || $ext == ".in"}
		                <div class="row">
							<div class="col-xs-12">
								<p class="poor-gray">{ci_language line="Item Type"}</p>
								<div class="form-group">
									<input type="text" rel="[recommended_browse_node][item_type]" class="form-control" disabled/>                            				
								</div>	
								<p class="poor-gray m-b5 text-right">
									<img src="{$cdn_url}assets/images/geo_flags/com.gif" /> &nbsp; 
	                                <img src="{$cdn_url}assets/images/geo_flags/in.gif" /> &nbsp; 
	                                {ci_language line="U.S.A. / India Only"}
	                            </p>
	                            <p class="poor-gray m-b5 text-right">
	                                {ci_language line="Item Type  -   You can use the product classifier to get the item type:"} 	                            
	                                <a class="link p-size" target="_blank" href="https://sellercentral.amazon.com/hz/inventory/classify?ref=ag_pclasshm_cont_invfile">
	                                   ProductClassifier
	                                </a> 
                                </p>
	                            <p class="poor-gray text-right">
	                                {ci_language line="This option is mandatory only for U.S.A. and could be mandatory for India"}.
								</p>
							</div>
						</div>

					{else}
						
						<div class="row">
							<div class="col-xs-12">
								<p class="poor-gray">{ci_language line="Browse Node ID"}</p>
								<input type="hidden" rel="recommended_browse_node_count" value="0" />
								<div class="recommended_browse_node_main form-group withIcon row">
									<div class="col-xs-4">
										<input type="text" rel="recommended_browse_node" disabled class="form-control"/>
										<i class="cb-plus good addRecommendedBrowseNode"></i>	
										<i class="cb-plus bad removeRecommendedBrowseNode" style="display: none"></i>   
									</div>                        				
									<div class="error text-left col-xs-4 m-t10" style="display: none">
										<p class="m-t0 p-0">
											<i class="fa fa-exclamation-circle"></i>{ci_language line="Allow only digit"}.
										</p>
									</div>   
								</div>
							</div>
						</div>
						<p class="poor-gray m-b5 text-right">
							{ci_language line="Amazon Browse Node ID: You can use the product classifier to get Browse Node IDs: "}					
							<a class="link p-size" target="_blank" href="https://sellercentral.amazon.com/hz/inventory/classify?ref=ag_pclasshm_cont_invfile">
								{ci_language line="Amazon.com"} 
							</a>,
							<a class="link p-size" target="_blank" href="https://sellercentral.amazon.fr/hz/inventory/classify?ref=pt_pclasshm_cont_invfile">
								{ci_language line="Amazon.co.uk"} 
							</a>
						</p>
						<p class="poor-gray m-b10 text-right">
							{ci_language line="This option is mandatory for Canada, Europe, Japan"}. ({ci_language line="Max"}: 2)
						</p>
					{/if}

				</div>				

				<!-- Key Product Features -->
				<div class="key_product_features">
					<div class="row">
						<div class="col-xs-12">
							<div class="b-Top p-t20 m-t10 clearfix">
								<p class="head_text p-b20">
									{ci_language line="Key Product Features"}
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">

							<!-- Use Product Features -->
							<label class="amazon-radio w-100">
								<span class="amazon-inner">
									<i><input rel="key_product_feature" type="radio" value="1" disabled></i>
								</span>
								{ci_language line="Use Product Features (Recommended)"}
							</label>							
						 	<div rel="key_product_feature_1" style="display:none">
                                <input type="hidden" rel="key_product_feature_count" value="0" />
                                <div class="key_product_feature_1_inner m-t10">
	                                <div class="form-group key_product_features_main withIcon clearfix">
	                                	<div class="col-xs-4">
											<select class="key_product_feature" rel="[key_product_features][features][value][]" disabled>
		                                        {if isset($features) }
		                                            {foreach from=$features key=feature_key item=feature_item}
		                                                <option value="{$feature_key}">{$feature_item.name}</option>
		                                            {/foreach} 
		                                        {/if}
		                                        <option value="CustomValue" style="color: #6fb3e0; font-weight: bold;">
	                                            	{ci_language line="Custom Value"}
	                                            </option>
		                                    </select>
	                                    </div>
                                        <div class="col-xs-7" style="display: none;" >
	                                    	<input type="text" class="form-control CustomValue" rel="[key_product_features][features][CustomValue][]" readonly />
                                	 	</div>
	                                    <i class="cb-plus good addKeyProductFeatures"></i>	
										<i class="cb-plus bad removeKeyProductFeatures" style="display: none"></i>  
						            </div>
						        </div>
					            <div class="row">
									<div class="col-xs-12  m--t10 m-b20">
										<label class="amazon-checkbox">
										    <span class="amazon-inner">
										        <i>
										        	<input rel="[key_product_features][features][set_name]" type="checkbox" value="1" disabled>
										        </i>
										    </span>
										    {ci_language line="Add Feature Name before Feature Value (ie: Material: Tissue)"}
										</label>								
									</div>
								</div>
                            </div>

                            <!-- Explode Description into Bullets Points -->
                            <label class="amazon-radio w-100">
								<span class="amazon-inner">
									<i><input rel="key_product_feature" type="radio" value="2" disabled></i>
								</span>
								{ci_language line="Explode Description into Bullets Points"}
							</label>							
							<div rel="key_product_feature_2" style="display:none">
                                <div class="form-group key_product_features_main m-t20">
									<select class="key_product_feature" rel="[key_product_features][descriptions]" disabled>
	                                    <option value="description">{ci_language line="Description"}</option>
	                                    <option value="description_short">{ci_language line="Short Description"}</option>
	                                </select> 
					            </div>
                                <p class="poor-gray text-right">
                                    {ci_language line="Not recommended, result could be unpredictable.. Use this option only if you don't have product features and no other choice"}
                                </p>
                            </div>

                            {if isset($eu) || isset($japan) || isset($ca)}
	                            <label class="amazon-radio w-100">
									<span class="amazon-inner">
										<i><input rel="key_product_feature" type="radio" value="3" {if isset($japan) && $japan == true}checked{/if} disabled></i>
									</span>
									{ci_language line="Do not
									 use key product features (Europe, Japan, Canada only)"}
								</label>	                           	                           
	                        {/if}
							
						</div>
					</div>
				</div>

				<!-- Price Rules -->
				{if isset($amazon_display[$ext]['price_rules']) && $amazon_display[$ext]['price_rules']} 
					<div class="price_rules">
						<div class="row">
							<div class="col-xs-12">
								<div class="b-Top p-t20 m-t10 clearfix">
									<p class="head_text p-b20">
										{ci_language line="Price Rules"}
									</p>
								</div>
							</div>
						</div>
						<div class="row price_rule_main">
							<input type="hidden" rel="price_rule_count" value="1" />
							<div class="col-xs-12">
								<p class="poor-gray">{ci_language line="Default Price Rule"}</p>
							</div>
							<div class="price_rule_li" rel="1">
								<div class="col-sm-3">
									<div class="form-group clearfix m-b0">
										<select rel="type" class="price_rule" disabled>
		                                    <option value="percent" selected="">{ci_language line="Percentage"}</option>
		                                    <option value="value">{ci_language line="Value"}</option>
		                                </select>	                               
									</div>
								</div>
								<div class="col-sm-3">
									<div class="input-group">
										<span class="input-group-addon">
											<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
										</span>
										<input rel="from" class="form-control price_rule" type="text" disabled placeholder="{ci_language line="From"}"> 
									</div>
									<i class="blockRight noBorder"></i>
								</div>
								<div class="col-sm-3">
									<div class="input-group">
										<span class="input-group-addon">
											<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
										</span>
										<input rel="to" class="form-control price_rule" type="text" disabled placeholder="{ci_language line="To"}"> 
									</div>
									<i class="blockRight noBorder"></i>
									<i class="blockRightDouble noBorder"></i>
								</div>
								<div class="col-sm-3 pr-0">
									<div class="form-group withIcon clearfix disabled-none">
										<div class="input-group">
											<span class="input-group-addon">
												<strong>%</strong>
											</span>
											<input rel="value" class="form-control price_rule m-b0" type="text" disabled >
										</div>
										<i class="cb-plus good addPriceRule m-0"></i>
										<i class="cb-plus bad removePriceRule m-0" style="display: none"></i>
									</div>							
								</div>
							 	<div class="form-group has-error" style="display:none">
									<div class="error text-right">
										<p class="m--t10 p-0">
											<i class="fa fa-exclamation-circle"></i>
											<span></span>
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p class="poor-gray">
									{ci_language line="You should configure a price rule in value or in percentage for one or several prices ranges."}
								</p>
							</div>
						</div>
						{*if (isset($mode_default) && $mode_default >= 2)}
							<!--Use Price Rule With Sale-->
							<div class="row">
								<div class="col-xs-12">
									<label class="amazon-checkbox pull-left">
									    <span class="amazon-inner">
									        <i> <input rel="[price_rules][specials_apply_rules]" type="checkbox" value="1" /> </i>
									    </span>
									    {ci_language line="Apply price rules to promotions/sales if checked."}
									</label>	
								</div>
							</div>
						{/if*}
						<!--Rounding-->
						<div class="row">
							<div class="col-xs-12">
								<p class="montserrat dark-gray text-uc m-t20">{ci_language line="Rounding"}</p>
								<label class="amazon-radio w-100">
									<span class="amazon-inner">
										<i><input rel="[price_rules][rounding]" type="radio" value="1" checked="" disabled></i>
									</span>
									{ci_language line="One Digit"}
								</label>	
								<label class="amazon-radio w-100">
									<span class="amazon-inner">
										<i><input rel="[price_rules][rounding]" type="radio" value="2" disabled></i>
									</span>
									{ci_language line="Two Digits"}
								</label>	
								<label class="amazon-radio w-100">
									<span class="amazon-inner">
										<i><input rel="[price_rules][rounding]" type="radio" value="0" checked="" disabled></i>
									</span>
									{ci_language line="None"}
								</label>						
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p class="poor-gray m-t10">{ci_language line="Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required."}</p>
							</div>
						</div>
						<p class="montserrat dark-gray text-uc m-t20">{ci_language line="Sales"}</p>
						<div class="row">
							<div class="col-xs-12">
								<label class="amazon-checkbox ">
								    <span class="amazon-inner">
								        <i><input rel="[price_rules][no_sale]" type="checkbox" value="1" /></i>
								    </span>
								    {ci_language line="Do not use sales"}
								</label>							
							</div>
						</div>
		            </div>
	            {/if}

			</div>
		</div>

	</div><!-- id main -->

    <div id="tasks">

       {if isset($profile) }
            {foreach from=$profile key=k item=data}
    		 	<div class="row amazon-profiles">

                    <div class="col-xs-12">
                        <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                        <p class="text-uc dark-gray montserrat pull-left p-t10 m-b0">
                            	{if isset($data.name)}{$data.name}{/if}
	                            {if isset($creation)}
	                                &nbsp;<i class="fa fa-angle-right"></i> 
	                                <span>
	                                    {if isset($data.model_name)}{$data.model_name}{/if}
	                                </span>
	                            {/if}	                            
                            </p>
                        <ul class="pull-right amazonEdit">
                                <li class="editProfile">
                                    <button type="button" class="link editProfile_link edit_profile">
                                    <i class="fa fa-pencil"></i> {ci_language line="edit"}
                                    </button>
                                    <!-- a class="editProfile_link" href="">edit</a> -->
                                </li>
                                <li class="remProfile">
                                    <button type="button" class="link removeProfile_link remove_profile" rel="{$data.name}" value="{$data.id_profile}" >
                                    <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                                    </button>  
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-11 col-lg-11 custom-form">

	                	<div rel="{$k}" class="profileBlock clearfix {if !isset($popup_more['no_model'])}showSelectBlock{/if} m-t35 m-b30 amazon-profile">

	                		<input type="hidden" rel="id_profile" name="profile[{$k}][id_profile]" value="{$data.id_profile}"/>

			                <!--Profile Name-->
							<p class="regRoboto poor-gray">{ci_language line="Profile Name"}</p>
							<div class="form-group">
								<input type="text" rel="name" name="profile[{$k}][name]" value="{if isset($data.name)}{$data.name}{/if}" class="form-control"/>	
							</div>
							<p class="poor-gray text-right">
	                            {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
	                        </p>

							<!--Model Name-->
				            {if isset($creation)}
				            	<div class="row">
									<div class="col-xs-12 p-t10">
										<div class="b-Top p-t20 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Model"}			
											</p>
										</div>
									</div>
								</div>		
								<div class="row">
									<div class="col-xs-12">
									 	{if isset($models)}
											<div class="form-group {if isset($popup_more['no_model']) && $popup_more['no_model']}has-error{/if}">
						                        <select class="search-select" rel="id_model" name="profile[{$k}][id_model]" >
				                                    <option value="">{ci_language line="Select a model"}</option>
				                                    {if isset($models)}
				                                        {foreach from=$models item=models_item}
				                                            <option value="{$models_item.id_model}" {if isset($data.id_model) && $data.id_model == $models_item.id_model} selected{/if}>
				                                                {$models_item.name}
				                                            </option>
				                                        {/foreach} 
				                                    {/if}  
				                                </select>  
				                                {if isset($popup_more['no_model']) && $popup_more['no_model']}
				                                	<div class="error">
														<p>
															<i class="fa fa-exclamation-circle"></i>
															{ci_language line="Please choose a model for this profile."}
														</p>
													</div>	
				                                {/if}
						                    </div>
					                        <p class="regRoboto poor-gray text-right">{ci_language line="Choose a model"}.</p>                       
					                    {else}
					                        {if isset($no_models)}{$no_models}{/if}
					                    {/if}							
									</div>
								</div>
							{/if}

							<!--Out of Stock-->
							<p class="regRoboto poor-gray b-Top p-t20">
								{ci_language line="Stock Break-even"}
								{if (isset($mode_default) && $mode_default == 1)}
									<span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
								{/if}
							</p>	
							<div class="form-group m-b20">
								<div class="row">
									<div class="col-xs-4">
										<input type="text" rel="out_of_stock" name="profile[{$k}][out_of_stock]" value="{if isset($data.out_of_stock)}{$data.out_of_stock}{/if}" class="form-control"{if (isset($mode_default) && $mode_default == 1)} disabled{/if}/> 		
									</div>
									<p class="regRoboto poor-gray text-left p-t10"> {ci_language line="Quantity to deduct to the actual stock to export the product."}</p>
								</div>
								<div class="error text-right" style="display:none">
									<p class="m--t10 p-0">
										<i class="fa fa-exclamation-circle"></i>
										<span>{ci_language line="Allow only digit"}.</span>
									</p>
								</div>
							</div>	
				            
							<!-- Synchronization Field -->
						 	{if isset($amazon_display[$ext]['synchronization_field']) && $amazon_display[$ext]['synchronization_field']} 
								<p class="regRoboto poor-gray">{ci_language line="Synchronization Field"}</p>
								<div class="row">
									<div class="col-xs-4">
										<div class="m-b10 synch">
											<div class="choZn">
											 	<select class="search-select" rel="synchronization_field" name="profile[{$k}][synchronization_field]">
					                                <option value="" disabled="disabled">{ci_language line="Choose one of the following"}</option>
					                                <option value="ean13" {if isset($data.synchronization_field) && $data.synchronization_field == "ean13"}selected{/if}>
					                                    {ci_language line="EAN13 (Europe)"}</option>
					                                <option value="upc"{if isset($data.synchronization_field) && $data.synchronization_field == "upc"}selected{/if}>
					                                    {ci_language line="UPC (United States)"}</option>
					                                <option value="both"{if isset($data.synchronization_field) && $data.synchronization_field == "both"}selected{/if}>
					                                    {ci_language line="Both (EAN13 then UPC)"}</option>
					                                <option value="reference"{if isset($data.synchronization_field) && $data.synchronization_field == "reference"}selected{/if}>
					                                    {ci_language line="SKU"}</option>
					                            </select>   							
											</div>
										</div>
									</div>	
									<p class="regRoboto poor-gray text-left">
										{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Amazon."}
									</p>
				        	    </div>
				            {/if}

				            <!-- ASIN has the Priority -->
							<p class="regRoboto poor-gray m-t20">
								{ci_language line="ASIN has the Priority"}
								{if (isset($mode_default) && $mode_default == 1)}
									<span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
								{/if}
							</p>
				            <div class="row">
						        <div class="col-xs-4">
									<div class="form-group m-b10{if (isset($mode_default) && $mode_default == 1)} disabled{/if}">
										<label class="amazon-checkbox pull-left {if isset($data.association.use_asin) && $data.association.use_asin == 1}checked{/if}{if (isset($mode_default) && $mode_default == 1)} expert_mode_display{/if}">
											    <span class="amazon-inner">
											        <i>
											        	<input rel="[association][use_asin]" type="checkbox" {if isset($data.association.use_asin) && $data.association.use_asin == 1}checked{/if} value="1"  name="profile[{$k}][association][use_asin]"{if (isset($mode_default) && $mode_default == 1)} disabled{/if}>
											        </i>
											    </span>
											    {ci_language line="Yes"}
											</label>
									</div>	
								</div>
								<p class="regRoboto poor-gray text-left">
									{ci_language line="Use the ASIN field as synchronization field in priority if filled. This parameter could override the Synchronization Field."}
								</p>
						    </div>
			            
							<div class="row m-t20">
								
				            	<!-- Title Format -->
								<div class="col-sm-5 col-md-6 col-lg-5">
									<p class="montserrat dark-gray text-uc">{ci_language line="Title Format"}</p>
									<label class="amazon-radio w-100 {if isset($data.title_format) && $data.title_format == 1}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="title_format" name="profile[{$k}][title_format]"  type="radio" value="1" {if isset($data.title_format) && $data.title_format == 1}checked{/if}>
											</i>
										</span>
										{ci_language line="Standard Title, Attributes"}	
									</label>
									<label class="amazon-radio w-100 {if isset($data.title_format) && $data.title_format == 2}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="title_format" name="profile[{$k}][title_format]" type="radio" value="2" {if isset($data.title_format) && $data.title_format == 2}checked{/if}>
											</i>
										</span>
										{ci_language line="Manufacturer, Title, Attributes"}
									</label>
									<label class="amazon-radio w-100 {if isset($data.title_format) && $data.title_format == 3}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="title_format" name="profile[{$k}][title_format]" type="radio" value="3" {if isset($data.title_format) && $data.title_format == 3}checked{/if}>
											</i>
										</span>
										{ci_language line="Manufacturer, Title, Reference, Attributes"}
									</label>						
								</div>

								<!-- HTML Descriptions -->
								<div class="col-sm-5 col-md-6 col-lg-5">
									<p class="montserrat dark-gray text-uc">{ci_language line="HTML Descriptions"}</p>
									<div class="clearfix">
										<label class="amazon-checkbox pull-left {if isset($data.html_description) && $data.html_description == 1}checked{/if}">
										    <span class="amazon-inner">
										        <i>
										        	<input rel="html_description" type="checkbox" {if isset($data.html_description) && $data.html_description == 1}checked{/if} value="1"  name="profile[{$k}][html_description]">
										        </i>
										    </span>
										    {ci_language line="Yes"}
										</label>																
									</div>
									<p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
								</div>

					            <!-- Title Format Descriptions -->
								<div class="col-xs-12">
									<p class="poor-gray m-t10">
										{ci_language line="Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes."}
									<!-- </p>
									<p class="poor-gray"> -->
										{ci_language line="Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon."}
									</p>
								</div>
							</div>

							<div class="row m-t20">

								<!-- Description Field -->
								<div class="col-sm-5 col-md-6 col-lg-5">
									<p class="montserrat dark-gray text-uc">{ci_language line="Description Field"}</p>
									<label class="amazon-radio w-100 {if isset($data.description_field) && $data.description_field == 1}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 1}checked{/if} value="1"  name="profile[{$k}][description_field]">
											</i>
										</span>
										{ci_language line="Description"}
									</label>
									<label class="amazon-radio w-100 {if isset($data.description_field) && $data.description_field == 2}checked{/if} ">
										<span class="amazon-inner">
											<i>
												<input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 2}checked{/if} value="2"  name="profile[{$k}][description_field]">
											</i>
										</span>
										{ci_language line="Short Description"}
									</label>
									<label class="amazon-radio w-100 {if isset($data.description_field) && $data.description_field == 3}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 3}checked{/if} value="3"  name="profile[{$k}][description_field]">
											</i>
										</span>
										{ci_language line="Both"}
									</label>
									<label class="amazon-radio w-100 {if isset($data.description_field) && $data.description_field == 4}checked{/if}">
										<span class="amazon-inner">
											<i>
												<input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 4}checked{/if} value="4"  name="profile[{$k}][description_field]">
											</i>
										</span>
										{ci_language line="None"}
									</label>
								</div>

								<!-- SKU as Supplier Reference -->
							 	{if isset($amazon_display[$ext]['sku_as_supplier_reference']) && $amazon_display[$ext]['sku_as_supplier_reference']}  
								 	<div class="col-sm-5 col-md-6 col-lg-5">
										<p class="montserrat dark-gray text-uc">{ci_language line="SKU as Supplier Reference"}</p>
										<div class="clearfix">
											<label class="amazon-checkbox pull-left {if isset($data.sku_as_supplier_reference.value) && $data.sku_as_supplier_reference.value == 1}checked{/if}">
											    <span class="amazon-inner">
											        <i>
											        	<input rel="sku_as_supplier_reference" type="checkbox" name="profile[{$k}][sku_as_supplier_reference]" {if isset($data.sku_as_supplier_reference.value) && $data.sku_as_supplier_reference.value == 1}checked{/if} value="1">
											        </i>
											    </span>
											    {ci_language line="Yes"}
											</label>
											<label class="amazon-checkbox pull-right {if isset($data.sku_as_supplier_reference.unconditionnaly) && $data.sku_as_supplier_reference.unconditionnaly == 1}checked{/if}">
											    <span class="amazon-inner">
											        <i>
											        	<input rel="unconditionnaly" type="checkbox"  name="profile[{$k}][unconditionnaly]" {if isset($data.sku_as_supplier_reference.unconditionnaly) && $data.sku_as_supplier_reference.unconditionnaly == 1}checked{/if} value="1">
											        </i>
											    </span>
											    {ci_language line="Unconditionnaly"}
											</label>								
										</div>
										<p class="regRoboto poor-gray text-left m-t5">
											{ci_language line="Send the Reference/SKU as the Supplier Reference"}
										</p>
									</div>									
					            {/if}
								
								<!-- Description field -->
								<div class="col-xs-12">
									<p class="poor-gray m-t10">
										{ci_language line="Whether to send short or long description of the product to Amazon."}
									</p>
								</div>
							</div>

							<!-- Default condition note -->
							<div class="row">
								<div class="col-xs-12">
									<p class="poor-gray m-t10">{ci_language line="Default Condition Note"}</p>
									<div class="form-group">
										<input type="text" rel="[association][condition_note]" class="form-control" {if isset($data.association.condition_note)}value="{$data.association.condition_note}"{/if} name="profile[{$k}][association][condition_note]"/>						
									</div>
									<p class="poor-gray text-right">{ci_language line="Short text about product condition / state which will appear on the product details sheet on Amazon."}</p>
								</div>
							</div>						

							<!-- Latency -->
							<div class="m-t20">
								<p class="poor-gray m-t10">{ci_language line="Latency"}</p>
								<div class="row">
									<div class="form-group">
										<div class="col-xs-4">
											<input type="text" rel="[association][latency]" class="form-control" {if isset($data.association.latency)}value="{$data.association.latency}"{/if} name="profile[{$k}][association][latency]" onkeyup="this.value=this.value.replace(/[^\d]/,'')"/>						
										</div>
										<p class="regRoboto poor-gray text-left p-t4">{ci_language line="Latency delay in days before this product will be shipped (ie: preparation time, your own delay before shipment)"}.</p>
									</div>
								</div>
							</div>
							
							<!-- Default Shipping Template -->
							<div class="row">
								<div class="col-xs-12 p-t10">
									<div class="b-Top p-t20 clearfix">
										<p class="head_text p-b20">
											{ci_language line="Default Shipping Template"}		
											{if (isset($mode_default) && $mode_default == 1)}
											  <span class="expert_mode"><i class="fa fa-lock"></i> {ci_language line="expert mode"}</span>
											{/if}	
										</p>
									</div>
								</div>
							</div>	

							{if (isset($mode_default) && $mode_default == 1)}
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
										 	<select class="search-select"{if (isset($mode_default) && $mode_default == 1)} disabled{/if}>
						                        <option value="0"></option>						                       
						                    </select>						                    		
										</div>							
									</div>
								</div>
							{else}
								{if isset($shipping_groups) && !empty($shipping_groups)}	
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
											 	<select rel="[association][shipping_group]" name="profile[{$k}][association][shipping_group]" class="search-select">
							                        <option value="0"></option>
							                        {if isset($shipping_groups)}
								                        {foreach $shipping_groups as $group_key => $group_name}
							                        		<option value="{$group_key}" {if isset($data.association.shipping_group) && $data.association.shipping_group == $group_key}selected{/if}>{$group_name}</option>
								                        {/foreach}	
							                        {/if}
							                    </select>
							                    		
											</div>							
										</div>
									</div>
								{else}
								 	{if isset($no_shipping_groups)}{$no_shipping_groups}{/if}	
				                {/if}	
			                {/if}	

			                <p class="regRoboto poor-gray text-left p-t4">
			                	{ci_language line="All the products related to this profile will have this shipping template applied, except if you specify another value directly on the offers options, then, this one will replace the default shipping template."}
			                </p>

							{if isset($amazon_display[$ext]['repricing']) && $amazon_display[$ext]['repricing']} 
								<!-- Repricing -->
								<div class="row">
									<div class="col-xs-12 p-t10">
										<div class="b-Top p-t20 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Repricing Strategy"}			
											</p>
										</div>
									</div>
								</div>
								{if isset($repricing_strategies) && !empty($repricing_strategies)}	
									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
											 	<select rel="[category][repricing]" name="profile[{$k}][category][repricing]" class="search-select">
							                        <option value="0"></option>
							                        {if isset($repricing_strategies)}
								                        {foreach $repricing_strategies as $strategies}
							                        		<option value="{$strategies['id_strategy']}" {if isset($data.category.repricing) && $data.category.repricing == $strategies['id_strategy']}selected{/if}>
							                        			{$strategies['name']}
							                        		</option>
								                        {/foreach}	
							                        {/if}
							                    </select>					
											</div>							
										</div>
									</div>
								{else}
								 	{if isset($no_repricing_strategies)}{$no_repricing_strategies}{/if}	
			                    {/if}
							{/if}

							{if isset($amazon_display[$ext]['advertising']) && $amazon_display[$ext]['advertising']} 
								<!-- Advertising -->
								<div class="row">
									<div class="col-xs-12 p-t10">
										<div class="b-Top p-t20 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Ad Group"}			
											</p>
										</div>
									</div>
								</div>					
								{if isset($advertising_groups) && !empty($advertising_groups)}	
								<div class="row">
									<div class="col-xs-12">
										<div class="row">
											{if isset($data.advertising) && !empty($data.advertising)}
												{foreach $data.advertising as $advertising_key => $advertising}
													<div class="form-group advertising_groups_main withIcon clearfix">
					                                	<div class="col-xs-4">
															<select rel="[advertising][adGroupId][]" name="profile[{$k}][advertising][adGroupId][]" class="search-select">
										                        <option value="">{ci_language line="Select an Option"}</option>
										                        {if isset($advertising_groups)}
											                        {foreach $advertising_groups as $adGroups}
											                        	<option value="{$adGroups['adGroupId']}" {if $advertising['adGroupId'] == $adGroups['adGroupId']}selected{/if}>{$adGroups['name']}</option>
											                        {/foreach}	
										                        {/if}
										                    </select>	
					                                    </div>
				                                     	{if $advertising_key == 0}
		                                                    <i class="cb-plus good addAdGroup"></i>
		                                                {/if}
														<i class="cb-plus bad removeAdGroup" {if $advertising_key == 0}style="display: none"{/if}></i> 
										            </div>
									            {/foreach}
								            {else}
								            	<div class="form-group advertising_groups_main withIcon clearfix">
				                                	<div class="col-xs-4">
														<select rel="[advertising][adGroupId][]" name="profile[{$k}][advertising][adGroupId][]" class="search-select">
									                        <option value="">{ci_language line="Select an Option"}</option>
									                        {if isset($advertising_groups)}
										                        {foreach $advertising_groups as $adGroups}
										                        	<option value="{$adGroups['adGroupId']}">{$adGroups['name']}</option>
										                        {/foreach}	
									                        {/if}
									                    </select>	
				                                    </div>
                                                    <i class="cb-plus good addAdGroup"></i>
													<i class="cb-plus bad removeAdGroup" style="display: none"></i> 
									            </div>
								            {/if}
										</div>
									</div>
								</div>
								{else}
								 	{if isset($no_advertising_groups)}{$no_advertising_groups}{/if}	
			                    {/if}		
								<p class="regRoboto poor-gray text-left p-t4">
				                	{ci_language line="All the products related to this profile will added to this ad group applied, except if you specify another value directly on the offers options, then, this one will replace the default Ad Groups."}
				                </p>
							{/if}
							<!-- Code Exemption -->
							{if isset($amazon_display[$ext]['code_examption']) && $amazon_display[$ext]['code_examption']}   
								<!-- Code Exemption -->
								<div class="row">
									<div class="col-xs-12">
										<div class="b-Top p-t20 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Code Exemption"}			
											</p>
										</div>
									</div>
								</div>

								<!-- Code Exemption chk-->
								<div class="row">
									<div class="col-xs-12">
										<label class="amazon-checkbox {if isset($data.code_exemption.chk) && $data.code_exemption.chk == 1}checked{/if}">
										    <span class="amazon-inner">
										        <i>
										        	<input rel="[code_exemption][chk]" name="profile[{$k}][code_exemption][chk]" type="checkbox" {if isset($data.code_exemption.chk) && $data.code_exemption.chk == 1}checked{/if} value="1">
										        </i>
										    </span>
										    {ci_language line="Use EAN/UPC exemption for this profile"}
										</label>							
									</div>
								</div>

								<!-- Field -->
								<div class="form-group code_exemption_field m-t20" {if !isset($data.code_exemption.chk)}style="display: none;"{/if}>
									<p class="regRoboto poor-gray">{ci_language line="Fields"}</p>
									<div class="m-b10">
										<div class="choZn">
										 	<select class="search-select" rel="[code_exemption][feild]" name="profile[{$k}][code_exemption][feild]">
			                                    <option value="0" {if !isset($data.code_exemption.feild)}selected{/if}></option>
			                                    <option value="1" style="display:none"></option>
			                                    <option value="2" {if isset($data.code_exemption.feild) && $data.code_exemption.feild == 2}selected{/if}>
			                                        {ci_language line="Model Number"}</option>
			                                    <option value="3" {if isset($data.code_exemption.feild) && $data.code_exemption.feild == 3}selected{/if}>
			                                        {ci_language line="Model Name"}</option>
			                                    <option value="4" {if isset($data.code_exemption.feild) && $data.code_exemption.feild == 4}selected{/if}>
			                                        {ci_language line="Manufacturer Part Number"}</option>
			                                    <option value="5" {if isset($data.code_exemption.feild) && $data.code_exemption.feild == 5}selected{/if}>
			                                        {ci_language line="Catalog Number"}</option>
			                                    <option value="10" {if isset($data.code_exemption.feild) && $data.code_exemption.feild == 10}selected{/if}>
			                                        {ci_language line="Generic"}</option>
			                                </select>				
										</div>
									</div>	
									<!-- Private Label -->
									<div class="row">
										<div class="col-xs-12">
											<div class="pull-left">
												<label class="amazon-checkbox {if isset($data.code_exemption.private) && $data.code_exemption.private == 1}checked{/if}">
												    <span class="amazon-inner">
												        <i>
												        	<input rel="private_label" name="profile[{$k}][code_exemption][private]" type="checkbox" {if isset($data.code_exemption.private) && $data.code_exemption.private == 1}checked{/if} value="1">
												        </i>
												    </span>
												    {ci_language line="Private Label"}
												</label>								
											</div>
											<div class="pull-right ">
												<p class="regRoboto poor-gray">{ci_language line="Use EAN/UPC exemption for this profile"}.</p>
											</div>
										</div>
									</div>
					            </div>

					            <!-- Concerns -->
					            <div class="form-group code_exemption_concerns m-t20" {if !isset($data.code_exemption.chk)}style="display: none;"{/if}>
					            	<p class="regRoboto poor-gray">{ci_language line="Concerns"}</p>
									<div class="m-b10">
										<div class="choZn">
										 	<select class="search-select" rel="[code_exemption][concerns]" {if isset($data.code_exemption.concerns)}name="profile[{$k}][code_exemption][concerns]"{/if}>
			                                    <option value="">{ci_language line="Select an Option"}</option>
			                                    <option value="Manufacturer" {if isset($data.code_exemption.concerns) && $data.code_exemption.concerns == "Manufacturer"}selected{/if}>
			                                        {ci_language line="Manufacturer"}</option>
			                                    <option value="All" {if isset($data.code_exemption.concerns) && $data.code_exemption.concerns == "All"}selected{/if}>
			                                        {ci_language line="All"}</option>
			                                </select>			
										</div>
									</div>	
					            </div>

					            <!-- Concerns type-->
					            <div class="code_exemption_concerns_type">
					            	{if isset($data.code_exemption.manufacturer)}
		                                {foreach from=$data.code_exemption.manufacturer key=manufacturer_key item=manufacturer}
		                                    {$manufacturer}
		                                {/foreach} 
		                            {/if}
					            </div>
							{/if}
							
							<!-- Recommended Browse Node -->
							<div class="recommended_browse_node">

								<!--head-->
								<div class="row">
			                        <div class="col-xs-12 m-t10">
										<div class="b-Top p-t20 m-t10 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Recommended Browse Node"}	
											</p>
										</div>
									</div>
								</div>

								{if isset($ext) && $ext == ".com" || $ext == ".in"}
					                <div class="row">
										<div class="col-xs-12">
											<p class="poor-gray">{ci_language line="Item Type"}</p>
											<div class="form-group">
												<input type="text" rel="[recommended_browse_node][item_type]" name="profile[{$k}][recommended_browse_node][item_type]" id="profile_{$k}_item_type" data-content="{$k}" value="{if isset($data.recommended_browse_node.item_type)}{$data.recommended_browse_node.item_type}{/if}"  class="form-control"/>                          				
											</div>	
											<p class="poor-gray m-b5 text-right">
												<img src="{$cdn_url}assets/images/geo_flags/com.gif" /> &nbsp; 
				                                <img src="{$cdn_url}assets/images/geo_flags/in.gif" /> &nbsp; 
				                                {ci_language line="U.S.A. / India Only"}
				                            </p>
				                            <p class="poor-gray m-b5 text-right">
				                                {ci_language line="Item Type  -   You can use the product classifier to get the item type:"} 	                            
				                                <a class="link p-size" target="_blank" href="https://sellercentral.amazon.com/hz/inventory/classify?ref=ag_pclasshm_cont_invfile">
				                                   ProductClassifier
				                                </a> 
			                                </p>
				                            <p class="poor-gray text-right">
				                                {ci_language line="This option is mandatory only for U.S.A. and could be mandatory for India"}.
											</p>
										</div>
									</div>
								{else}

									<!--field-->
									<div class="row">
										<div class="col-xs-12">
											<p class="poor-gray">{ci_language line="Browse Node ID"}</p>

											<input type="hidden" rel="recommended_browse_node_count" value="{sizeof($data.recommended_browse_node) - 1}" />
											{if isset($data.recommended_browse_node)}
		                                        {foreach from=$data.recommended_browse_node key=recommended_browse_node_key item=recommended_browse_node}
		                                            {if $recommended_browse_node_key !== "item_type"}
														<div class="recommended_browse_node_main form-group withIcon row">
															<div class="col-xs-4">
																<input type="text" rel="recommended_browse_node" name="profile[{$k}][recommended_browse_node][]" id="profile_{$k}_recommended_browse_node_{$recommended_browse_node_key}" data-content="{$k}" value="{$recommended_browse_node}"  class="form-control"/>
																{if $recommended_browse_node_key == 0}
																	<i class="cb-plus good addRecommendedBrowseNode"></i>	
																{/if}
																<i class="cb-plus bad removeRecommendedBrowseNode" {if $recommended_browse_node_key == 0}style="display:none;"{/if}></i>   
															</div>                        				
															<div class="error text-left col-xs-4 m-t10" style="display: none">
																<p class="m-t0 p-0">
																	<i class="fa fa-exclamation-circle"></i>{ci_language line="Allow only digit"}.
																</p>
															</div>   
														</div>
													{/if}
		                                        {/foreach} 
		                                    {else}	
		                                    	<div class="recommended_browse_node_main form-group withIcon row">
		                                    		<div class="col-xs-4">
														<input type="text" rel="recommended_browse_node" name="profile[{$k}][recommended_browse_node][]" id="profile_{$k}_recommended_browse_node_0" data-content="{$k}"  class="form-control"/>
														<i class="cb-plus good addRecommendedBrowseNode"></i>	
														<i class="cb-plus bad removeRecommendedBrowseNode" style="display: none;"></i>   
													</div>                          				
													<div class="error text-left col-xs-4 m-t10" style="display: none">
														<p class="m-t0 p-0">
															<i class="fa fa-exclamation-circle"></i>{ci_language line="Allow only digit"}.
														</p>
													</div> 
												</div>
											{/if}
										</div>
									</div>

									<p class="poor-gray m-b5 text-left">
										{ci_language line="Amazon Browse Node ID: You can use the product classifier to get Browse Node IDs: "}					
										<a class="link p-size" href="https://sellercentral.amazon.com/hz/inventory/classify?ref=ag_pclasshm_cont_invfile" target="_blank">
											{ci_language line="Amazon.com"} 
										</a>, 					
										<a class="link p-size" href="https://sellercentral.amazon.fr/hz/inventory/classify?ref=pt_pclasshm_cont_invfile">
											{ci_language line="Amazon.co.uk"} 
										</a>
									</p>

									<p class="poor-gray m-b10 text-left">
										{ci_language line="This option is mandatory for Canada, Europe, Japan"}. ({ci_language line="Max"}: 2)
									</p>
								{/if}
							</div>

							<!-- Key Product Features -->
							<div class="key_product_features">
								<!-- head -->
								<div class="row">
									<div class="col-xs-12">
										<div class="b-Top p-t20 m-t10 clearfix">
											<p class="head_text p-b20">
												{ci_language line="Key Product Features"}
											</p>
										</div>
									</div>
								</div>
								<!-- body -->
								<div class="row">
									<div class="col-xs-12">

										<!-- Use Product Features -->
										<label class="amazon-radio w-100 {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 1} checked {/if}">
											<span class="amazon-inner">
												<i>
													<input rel="key_product_feature" name="profile[{$k}][key_product_feature]" id="profile_{$k}_key_product_feature" data-content="{$k}" type="radio" value="1" {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 1} checked {/if} >
												</i>
											</span>
											{ci_language line="Use Product Features (Recommended)"}
										</label>
									 	<div rel="key_product_feature_1" {if !isset($data.key_product_features.key_product_feature) || $data.key_product_features.key_product_feature != 1}style="display:none"{/if}>

			                                <input type="hidden" rel="key_product_feature_count" value="{if isset($data.key_product_features.features.value)}{count($data.key_product_features.features.value) - 1}{else}0{/if}" />

			                                <div class="key_product_feature_1_inner row m-t10">
			                                	{if isset($data.key_product_features.features.value) && !empty($data.key_product_features.features.value)}
			                                		
	                                                {foreach from=$data.key_product_features.features.value key=features_key item=features_item}
						                                <div class="form-group {if $features_key == 0}key_product_features_main{/if} withIcon clearfix">

						                                		{if $features_item != "CustomValue"}

						                                			<div class="col-xs-4">
																		<select class="key_product_feature search-select" rel="[key_product_features][features][value][]" name="profile[{$k}][key_product_features][features][value][]" id="profile_{$k}_key_product_features_features_value" data-content="{$k}">
				                                                            {if isset($features) }
				                                                                {foreach from=$features key=feature_key item=feature_item}
				                                                                    <option value="{$feature_key}" {if $features_item == $feature_key}selected{/if}>{$feature_item.name}</option>
				                                                                {/foreach} 
				                                                            {/if}
				                                                            <option value="CustomValue" style="color: #6fb3e0; font-weight: bold;">
				                                                            	{ci_language line="Custom Value"}
				                                                            </option>
				                                                        </select>
			                                                        </div>
			                                                        <div class="col-xs-7" style="display: none;" >
			                                                        	<input type="text" class="form-control CustomValue" rel="[key_product_features][features][CustomValue][]" name="profile[{$k}][key_product_features][features][CustomValue][]" id="profile_{$k}_key_product_features_features_CustomValue" data-content="{$k}" readonly /> 
		                                                        	</div>
		                                                        {elseif $features_item == "CustomValue"}

		                                                        	<div class="col-xs-4">
			                                                        	<select class="key_product_feature search-select" rel="[key_product_features][features][value][]" name="profile[{$k}][key_product_features][features][value][]" id="profile_{$k}_key_product_features_features_value" data-content="{$k}">
				                                                            {if isset($features) }
				                                                                {foreach from=$features key=feature_key item=feature_item}
				                                                                    <option value="{$feature_key}">{$feature_item.name}</option>
				                                                                {/foreach} 
				                                                            {/if}
				                                                            <option value="CustomValue" style="color: #6fb3e0; font-weight: bold;" selected="true">
				                                                            	{ci_language line="Custom Value"}
				                                                            </option>
				                                                        </select>
			                                                        </div>
			                                                        <div class="col-xs-7">
		                                                        		<input type="text" class="form-control CustomValue" rel="[key_product_features][features][CustomValue][]" name="profile[{$k}][key_product_features][features][CustomValue][]" id="profile_{$k}_key_product_features_features_CustomValue" data-content="{$k}" {if isset($data.key_product_features.features['CustomValue'][$features_key])}value="{$data.key_product_features.features['CustomValue'][$features_key]}"{/if} /> 
	                                                        		</div>

	                                                        	{/if}

		                                                        {if $features_key == 0}
		                                                            <i class="cb-plus good addKeyProductFeatures"></i>
		                                                        {/if}
																<i class="cb-plus bad removeKeyProductFeatures" {if $features_key == 0}style="display: none"{/if}></i> 
											            </div>
										          	{/foreach}
	                                            {else}
	                                             	<div class="form-group key_product_features_main withIcon clearfix">
	                                             		<div class="col-xs-4">
															<select class="key_product_feature search-select" rel="[key_product_features][features][value][]" name="profile[{$k}][key_product_features][features][value][]" id="profile_{$k}_key_product_features_features_value" data-content="{$k}">
		                                                        {if isset($features) }
		                                                            {foreach from=$features key=feature_key item=feature_item}
		                                                                <option value="{$feature_key}">{$feature_item.name}</option>
		                                                            {/foreach} 
		                                                        {/if}
		                                                        <option value="CustomValue" style="color: #6fb3e0; font-weight: bold;">
	                                                            	{ci_language line="Custom Value"}
	                                                            </option>
		                                                    </select>
	                                                    </div>
                                                        <div class="col-xs-7" style="display: none;" >
	                                                    	<input type="text" class="form-control CustomValue" rel="[key_product_features][features][CustomValue][]" name="profile[{$k}][key_product_features][features][CustomValue][]" id="profile_{$k}_key_product_features_features_CustomValue" data-content="{$k}" readonly />
                                                    	</div>
                                                        <i class="cb-plus good addKeyProductFeatures"></i>
														<i class="cb-plus bad removeKeyProductFeatures" style="display: none"></i>  
										            </div>										           
	                                            {/if}    

									        </div>
								            <div class="row">
												<div class="col-xs-12  m--t10 m-b20">
													<label class="amazon-checkbox {if isset($data.key_product_features.features.set_name) && $data.key_product_features.features.set_name == 1} checked{/if}">
													    <span class="amazon-inner">
													        <i>
													        	<input rel="[key_product_features][features][set_name]" name="profile[{$k}][key_product_features][features][set_name]" id="profile_{$k}_key_product_features_features_set_name" data-content="{$k}" type="checkbox" value="1" {if isset($data.key_product_features.features.set_name) && $data.key_product_features.features.set_name == 1} checked{/if}>
													        </i>
													    </span>
													    {ci_language line="Add Feature Name before Feature Value (ie: Material: Tissue)"}
													</label>								
												</div>
											</div>
			                            </div>

			                            <!-- Explode Description into Bullets Points -->
			                            <label class="amazon-radio w-100 {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 2}checked{/if}">
											<span class="amazon-inner">
												<i>
													<input rel="key_product_feature" name="profile[{$k}][key_product_feature]" id="profile_{$k}_key_product_feature" data-content="{$k}" type="radio" value="2" {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 2} checked {/if}>
												</i>
											</span>
											{ci_language line="Explode Description into Bullets Points"}
										</label>							
										<div rel="key_product_feature_2" {if !isset($data.key_product_features.key_product_feature) || $data.key_product_features.key_product_feature != 2}style="display:none"{/if}>
			                                <div class="form-group key_product_features_main m-t20">
												<select class="key_product_feature search-select" rel="[key_product_features][descriptions]" name="profile[{$k}][key_product_features][descriptions]" id="profile_{$k}_key_product_features_descriptions" data-content="{$k}" >
		                                            <option value="description" {if isset($data.key_product_features.descriptions) && $data.key_product_features.descriptions == 'description'}selected{/if}>{ci_language line="Description"}</option>
		                                            <option value="description_short" {if isset($data.key_product_features.descriptions) && $data.key_product_features.descriptions == 'description_short'}selected{/if}>{ci_language line="Short Description"}</option>
		                                        </select> 
								            </div>
			                                <p class="poor-gray text-right">
			                                    {ci_language line="Not recommended, result could be unpredictable.. Use this option only if you don't have product features and no other choice"}
			                                </p>
			                            </div>

			                            {if isset($eu) || isset($japan) || isset($ca)}
				                            <label class="amazon-radio w-100 {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 3}checked{/if}">
												<span class="amazon-inner">
													<i>
														<input rel="key_product_feature" name="profile[{$k}][key_product_feature]" id="profile_{$k}_key_product_feature" data-content="{$k}" type="radio" value="3" {if isset($data.key_product_features.key_product_feature) && $data.key_product_features.key_product_feature == 3}checked{/if}>
													</i>
												</span>
												{ci_language line="Do not use key product features (Europe, Japan, Canada only)"}
											</label>	                           	                           
				                        {/if}
										
									</div>
								</div>
							</div>							

							<!-- Price Rules -->
							{if isset($amazon_display[$ext]['price_rules']) && $amazon_display[$ext]['price_rules']}  
								<div class="price_rules">
									<!--head-->
									<div class="row">
										<div class="col-xs-12">
											<div class="b-Top p-t20 m-t10 clearfix">
												<p class="head_text p-b20">
													{ci_language line="Price Rules"}
												</p>
											</div>
										</div>
									</div>
									<!-- Default Price Rule -->
									<div class="row price_rule_main">
										<input type="hidden" rel="price_rule_count" value="{if isset($data.price_rules.value)}{count($data.price_rules.value)-1}{else}0{/if}" />
										<div class="col-xs-12">
											<p class="poor-gray">{ci_language line="Default Price Rule"}</p>
										</div>

									 	{if isset($data.price_rules.value) && !empty($data.price_rules.value)}
	                                        {$i = 0}
	                                        {foreach from=$data.price_rules.value key=price_rules_key item=price_rules_item}
												<div class="price_rule_li" rel="{$i++}">
													<!--select-->
													<div class="col-sm-3">
														<div class="form-group clearfix m-b0">
															<select rel="type" class="form-control price_rule" name="profile[{$k}][price_rules][value][{$i}][type]" id="profile_{$k}_price_rules_value_{$i}_type" data-row="{$i}" data-content="{$k}">
	                                                            <option value="percent" {if isset($price_rules_item['type']) && $price_rules_item['type'] == "percent"}selected{/if}>{ci_language line="Percentage"}</option>
	                                                            <option value="value" {if isset($price_rules_item['type']) && $price_rules_item['type'] == "value"}selected{/if}>{ci_language line="Value"}</option>
	                                                        </select>	                               
														</div>
													</div>
													<!--from-->
													<div class="col-sm-3">
														<div class="input-group">
															<span class="input-group-addon">
																<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
															</span>
															<input rel="from" class="form-control price_rule" type="text" {if isset($price_rules_item['from'])}value="{$price_rules_item['from']}"{/if} name="profile[{$k}][price_rules][value][{$i}][from]" id="profile_{$k}_price_rules_value_{$i}_from" data-row="{$i}" data-content="{$k}"> 
														</div>
														<i class="blockRight noBorder"></i>
													</div>
													<!--to-->
													<div class="col-sm-3">
														<div class="input-group">
															<span class="input-group-addon">
																<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
															</span>
															<input rel="to" class="form-control price_rule" type="text" {if isset($price_rules_item['to'])}value="{$price_rules_item['to']}"{/if} name="profile[{$k}][price_rules][value][{$i}][to]" id="profile_{$k}_price_rules_value_{$i}_to" data-row="{$i}" data-content="{$k}"> 
														</div>
														<i class="blockRight noBorder"></i>
														<i class="blockRightDouble noBorder"></i>
													</div>
													<!--value-->
													<div class="col-sm-3 pr-0">
														<div class="form-group withIcon clearfix disabled-none">
															<div class="input-group">
																<span class="input-group-addon">
																	<strong>
																		{if isset($price_rules_item['type']) && $price_rules_item['type'] == "percent"}
			                                                                %
			                                                            {else}
			                                                                {if isset({$currency_sign})}{$currency_sign}{/if}
			                                                            {/if}
																	</strong>
																</span>
																<input rel="value" class="form-control price_rule m-b0" type="text" {if isset($price_rules_item['value'])}value="{$price_rules_item['value']}"{/if} name="profile[{$k}][price_rules][value][{$i}][value]" id="profile_{$k}_price_rules_value_{$i}_value" data-row="{$i}" data-content="{$k}">
															</div>
															<!--add/remove-->														
	                                                        {if $i == count($data.price_rules.value)}
	                                                            <i class="cb-plus good addPriceRule m-0"></i>
	                                                        {/if}
	                                                        <i class="cb-plus bad removePriceRule m-0" {if $i == count($data.price_rules.value)}style="display: none"{/if}></i>
														</div>							
													</div>
													<!--error-->
												 	<div class="form-group has-error" style="display:none">
														<div class="error text-right">
															<p class="m--t10 p-0">
																<i class="fa fa-exclamation-circle"></i>
																<span></span>
															</p>
														</div>
													</div>
												</div>
											 {/foreach}
	                                    {else}
	                                    	<div class="price_rule_li" rel="0">
												<!--select-->
												<div class="col-sm-3">
													<div class="form-group clearfix m-b0">
														<select rel="type" class="form-control price_rule" name="profile[{$k}][price_rules][value][1][type]" id="profile_{$k}_price_rules_value_1_type" data-row="1" data-content="{$k}">
	                                                        <option value="percent" selected="">{ci_language line="Percentage"}</option>
	                                                        <option value="value">{ci_language line="Value"}</option>
	                                                    </select>                               
													</div>
												</div>
												<!--from-->
												<div class="col-sm-3">
													<div class="input-group">
														<span class="input-group-addon">
															<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
														</span>
														<input rel="from" class="form-control price_rule" type="text" name="profile[{$k}][price_rules][value][1][from]" id="profile_{$k}_price_rules_value_1_from" data-row="1" data-content="{$k}"> 
													</div>
													<i class="blockRight noBorder"></i>
												</div>
												<!--to-->
												<div class="col-sm-3">
													<div class="input-group">
														<span class="input-group-addon">
															<strong>{if isset({$currency_sign})}{$currency_sign}{/if}</strong>
														</span>
														<input rel="to" class="form-control price_rule" type="text" name="profile[{$k}][price_rules][value][1][to]" id="profile_{$k}_price_rules_value_1_to" data-row="1" data-content="{$k}">  
													</div>
													<i class="blockRight noBorder"></i>
													<i class="blockRightDouble noBorder"></i>
												</div>
												<!--value-->
												<div class="col-sm-3 pr-0">
													<div class="form-group withIcon clearfix disabled-none">
														<div class="input-group">
															<span class="input-group-addon">
																<strong>%</strong>
															</span>
															<input rel="value" class="form-control price_rule m-b0" type="text" name="profile[{$k}][price_rules][value][1][value]" id="profile_{$k}_price_rules_value_1_value" data-row="1" data-content="{$k}" > 
														</div>
														<!--add/remove-->														
	                                                    <i class="cb-plus good addPriceRule m-0"></i>
	                                                    <i class="cb-plus bad removePriceRule m-0" style="display: none"></i>
													</div>							
												</div>
												<!--error-->
											 	<div class="form-group has-error" style="display:none">
													<div class="error text-right">
														<p class="m--t10 p-0">
															<i class="fa fa-exclamation-circle"></i>
															<span></span>
														</p>
													</div>
												</div>
											</div>
	                                    {/if}
	                                    	
									</div>
									<div class="row">
										<div class="col-xs-12">
											<p class="poor-gray">
												{ci_language line="You should configure a price rule in value or in percentage for one or several prices ranges."}
											</p>
										</div>
									</div>

									{*if (isset($mode_default) && $mode_default >= 2)}
										<!--Use Price Rule With Sale-->
										<div class="row">
											<div class="col-xs-12">
												<label class="amazon-checkbox pull-left {if isset($data.price_rules.specials_apply_rules) && $data.price_rules.specials_apply_rules == 1}checked{/if}">
												    <span class="amazon-inner">
												        <i>
												        	<input rel="specials_apply_rules" type="checkbox" {if isset($data.price_rules.specials_apply_rules) && $data.price_rules.specials_apply_rules == 1}checked{/if} value="1" name="profile[{$k}][price_rules][specials_apply_rules]" id="profile_{$k}_price_rules_specials_apply_rules" data-content="{$k}" />
												        </i>
												    </span>
												    {ci_language line="Apply price rules to promotions/sales if checked."}
												</label>	
											</div>
										</div>
									{/if*}

									<!--rounding-->
									<div class="row">
										<div class="col-xs-12">
											<p class="montserrat dark-gray text-uc m-t20">{ci_language line="Rounding"}</p>
											<label class="amazon-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 1 }checked{/if}">
												<span class="amazon-inner">
													<i>
														<input rel="[price_rules][rounding]" type="radio" value="1" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 1 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
													</i>
												</span>
												{ci_language line="One Digit"}
											</label>	
											<label class="amazon-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 2 }checked{/if}">
												<span class="amazon-inner">
													<i>
													 	<input rel="[price_rules][rounding]" type="radio" value="2" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 2 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
													</i>
												</span>
												{ci_language line="Two Digits"}
											</label>
											<label class="amazon-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 0 }checked{/if}">
												<span class="amazon-inner">
													<i>
													 	<input rel="[price_rules][rounding]" type="radio" value="0" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 0 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
													</i>
												</span>
												{ci_language line="None"}
											</label>							
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<p class="poor-gray m-t10">{ci_language line="Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required."}</p>
										</div>
									</div>
									<p class="montserrat dark-gray text-uc m-t20">{ci_language line="Sales"}</p>
									<div class="row">
										<div class="col-xs-12">
											<label class="amazon-checkbox {if isset($data.price_rules.no_sale) && $data.price_rules.no_sale == 1 }checked{/if}">
											    <span class="amazon-inner">
											        <i>
											        	<input rel="[price_rules][no_sale]" type="checkbox" {if isset($data.price_rules.no_sale) && $data.price_rules.no_sale == 1}checked{/if} value="1" name="profile[{$k}][price_rules][no_sale]" id="profile_{$k}_price_rules_no_sale" data-content="{$k}" />	
											        </i>
											    </span>
											    {ci_language line="Do not use sales"}
											</label>							
										</div>
									</div>
									
					            </div>
				            {/if}
				            
						</div>

                    </div>

                </div>                
            {/foreach} 
        {/if}

    </div>

    {include file="amazon/PopupStepMoreFooter.tpl"} 
    
    <!--Message-->
    <input type="hidden" id="submit-confirm" value="{ci_language line="Do you want to save 'Profiles'?"}" />
    <input type="hidden" id="confirm-delete-profile-message" value="{ci_language line="Do you want to delete "}" />
    <input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
    <input type="hidden" id="delete-success-message" value="{ci_language line="Profile was deleted"}" />
    <input type="hidden" id="error-message" value="{ci_language line="Error"}" />
    <input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />
    <input type="hidden" id="none-value" value="{ci_language line="A value must be set for this element"}" />
    <input type="hidden" id="invalid-price-range" value="{ci_language line="Invalid price range, price to must be more than price from."}" />
    <input type="hidden" id="key-product-feature-max-message" value="{ci_language line="You can add up to 5 bullets points."}" />

    <input type="hidden" id="selected_variation_data" value="{if isset($mapping_variation_data)}{$mapping_variation_data}{/if}" />
    <input type="hidden" id="selected_specific_fields" value="{if isset($mapping_specific_fields)}{$mapping_specific_fields}{/if}" />
    <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
    <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
    <input type="hidden" id="currency_sign" value="{if isset({$currency_sign})}{$currency_sign}{/if}" />
	<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">    

    {if isset($creation)}<input type="hidden" id="creation" value="1" />{/if}
            
</form>
            
{include file="amazon/PopupStepFooter.tpl"}

<script src="{$cdn_url}assets/js/FeedBiz/amazon/profiles.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}