{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
<form id="form-repricing-strategy" method="post" action="{$base_url}amazon/save_repricing_strategies/{$id_country}" enctype="multipart/form-data" autocomplete="off" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"}     

    <div id="main" class="amazon-strategy showSelectBlock">

        <div class="col-md-10 col-lg-9 custom-form m-t10 m-b30">
            <button class="link removeProfile remove" type="button">{ci_language line="Remove a strategy from list"}</button>

            <div class="profileBlock clearfix amazon-profile">

                <div class="col-xs-12">
                    <p class="regRoboto poor-gray pull-left">{ci_language line="Strategy Name"}</p>
                    <div class="form-group has-error">
                        <input type="text" rel="rs_name" class="form-control" />  
                        <div class="error">
                            <p>
                                <i class="fa fa-exclamation-circle"></i>
                                {ci_language line="Set Strategy Name First"}
                            </p>
                        </div>
                    </div>
                </div><!--Strategy Name-->
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label class="text-uc montserrat dark-gray bold m-t10">{ci_language line="Behavior"}</label>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <select rel="rs_active" disabled>
                                    <option value="{$behavior['Inactive']}">{ci_language line="Inactive"}</option>
                                    <option value="{$behavior['Automatic']}" selected="selected">{ci_language line="Automatic"}</option>
                                    <option value="{$behavior['Manual']}">{ci_language line="Manual"}</option>
                                </select> 
                            </div>                                
                        </div>
                    </div>
                </div> <!--Active-->
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label class="text-uc montserrat dark-gray bold m-t10">
                                {ci_language line="Aggressivity"}
                            </label>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <select rel="rs_aggressivity" disabled>
                                    <option value=""> -- </option>
                                    {for $num=1 to 10}
                                        <option value="{$num}">{$num}</option>
                                    {/for}
                                </select> 
                            </div>
                        </div>
                    </div>
                </div><!--Aggressivity-->
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label class="text-uc montserrat dark-gray bold m-t10">
                                {ci_language line="Base"}
                            </label>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <select rel="rs_base" disabled>
                                    <option value=""> -- </option>
                                    <option value="1" selected="">{ci_language line="Wholesale Price"}</option>
                                    <option value="2">{ci_language line="Regular Price"}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Base-->

                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label class="text-uc montserrat dark-gray bold m-t10">
                                {ci_language line="Limit"}
                            </label>
                            <span class="p-size poor-gray">({ci_language line="on base"})</span>
                        </div>
                        <div class="col-xs-8">
                            <div class="form-group">
                                <select rel="rs_limit" disabled>
                                    <option value=""> -- </option>
                                    {for $num=-100 to 100}
                                        <option value="{$num}">{$num}%</option>
                                    {/for}
                                </select> 
                            </div>
                        </div>
                        
                    </div>
                </div><!--Limit-->
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label class="text-uc montserrat dark-gray bold m-t10">
                                {ci_language line="Delta"} 
                            </label>
                            <span class="p-size poor-gray">({ci_language line="on regular price"})</span>
                        </div>
                        <div class="col-xs-8">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <select rel="rs_delta_min" disabled>
                                            <option value=""> -- </option>
                                            {for $num=-100 to 100}
                                                <option value="{$num}">{$num}%</option>
                                            {/for}
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-xs-2 text-center">
                                    <p class="m-t10">{ci_language line="to"}</p>
                                </div>
                                <div class="col-xs-5">
                                    <div class="form-group">
                                        <select rel="rs_delta_max" disabled>
                                            <option value=""> -- </option>
                                            {for $num=-100 to 100}
                                                <option value="{$num}">{$num}%</option>
                                            {/for}
                                        </select> 
                                    </div>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div><!--Delta-->

            </div>
        </div>
    </div><!-- id main -->

    <div id="tasks">
        {if isset($strategies)}
            {foreach from=$strategies key=k item=strategy}
                <div class="amazon-strategies">

                    <div class="col-xs-12 b-Bottom">

                        <div class="row clearfix m--t1">                           
                            <p class="text-uc dark-gray montserrat pull-sm-left p-t10 m-b0">
                                {if isset($strategy.name)}{$strategy.name}{/if}                                                             
                            </p>
                            <ul class="pull-sm-right amazonEdit">
                                <li class="editProfile">
                                    <button type="button" class="link editProfile_link edit_strategy">
                                        {ci_language line="edit"}
                                    </button>
                                </li>
                                <li class="remProfile">
                                    <button type="button" class="link removeProfile_link remove_strategy" rel="{$strategy.name}" {if isset($strategy.id_strategy)}value="{$strategy.id_strategy}"{/if} />
                                        {ci_language line="delete"}
                                    </button>  
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div rel="{$k}" class="col-md-10 col-lg-9 custom-form">
                        
                        <div class="profileBlock clearfix amazon-strategy showSelectBlock m-b30">

                            <div class="col-xs-12">
                                <p class="regRoboto poor-gray pull-left">{ci_language line="Strategy Name"}</p>
                                <div class="form-group">
                                    <input type="text" rel="rs_name" class="form-control" name="strategy[{$k}][rs_name]" id="strategy_{$k}_rs_name" {if isset($strategy.name)}value="{$strategy.name}"{/if} />
                                    <input type="hidden" rel="id_strategy" name="strategy[{$k}][id_strategy]" id="strategy_{$k}_id_strategy" {if isset($strategy.id_strategy)}value="{$strategy.id_strategy}"{/if} />
                                </div>
                            </div><!--Strategy Name-->
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <label class="text-uc montserrat dark-gray bold m-t10">{ci_language line="Behavior"}</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <select rel="rs_active" name="strategy[{$k}][rs_active]" id="strategy_{$k}_rs_active" class="search-select">
                                                <option value="{$behavior['Inactive']}" {if isset($strategy.active) && $strategy.active == 1}selected="selected"{/if}>
                                                    {ci_language line="Inactive"}
                                                </option>
                                                <option value="{$behavior['Automatic']}" {if isset($strategy.active) && $strategy.active == 2}selected="selected"{/if}>
                                                    {ci_language line="Automatic"}
                                                </option>
                                                <option value="{$behavior['Manual']}" {if isset($strategy.active) && $strategy.active == 3}selected="selected"{/if}>
                                                    {ci_language line="Manual"}
                                                </option>
                                            </select> 
                                        </div>                                
                                    </div>
                                </div>
                            </div> <!--Active-->
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <label class="text-uc montserrat dark-gray bold m-t10">
                                            {ci_language line="Aggressivity"}
                                        </label>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <select rel="rs_aggressivity" name="strategy[{$k}][rs_aggressivity]" id="strategy_{$k}_rs_aggressivity" class="search-select">
                                                <option value=""> -- </option>
                                                {for $num=1 to 10}
                                                    <option value="{$num}" {if isset($strategy.aggressivity) && $strategy.aggressivity == $num}selected{/if}>
                                                        {$num}
                                                    </option>
                                                {/for}
                                            </select> 
                                        </div>
                                    </div>
                                </div>
                            </div><!--Aggressivity-->
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <label class="text-uc montserrat dark-gray bold m-t10">
                                            {ci_language line="Base"}
                                        </label>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <select rel="rs_base" name="strategy[{$k}][rs_base]" id="strategy_{$k}_rs_base" class="search-select">
                                                <option value=""> -- </option>
                                                <option value="1" {if isset($strategy.base) && $strategy.base == 1}selected{/if}>
                                                    {ci_language line="Wholesale Price"}
                                                </option>
                                                <option value="2" {if isset($strategy.base) && $strategy.base == 2}selected{/if}>
                                                    {ci_language line="Regular Price"}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Base-->

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <label class="text-uc montserrat dark-gray bold m-t10">
                                            {ci_language line="Limit"}
                                        </label>
                                        <span class="p-size poor-gray">({ci_language line="on base"})</span>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <select rel="rs_limit" name="strategy[{$k}][rs_limit]" id="strategy_{$k}_rs_limit" class="search-select">
                                                <option value=""> -- </option>
                                                {for $num=-100 to 100}
                                                    <option value="{$num}" {if isset($strategy.limit) && $strategy.limit == $num}selected{/if}>{$num}%</option>
                                                {/for}
                                            </select> 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div><!--Limit-->
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="col-xs-4">
                                        <label class="text-uc montserrat dark-gray bold m-t10">
                                            {ci_language line="Delta"} 
                                        </label>
                                        <span class="p-size poor-gray">({ci_language line="on regular price"})</span>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="row">
                                            <div class="col-xs-5">
                                                <div class="form-group">
                                                    <select rel="rs_delta_min" name="strategy[{$k}][rs_delta_min]" id="strategy_{$k}_rs_delta_min" class="search-select">
                                                        <option value=""> -- </option>
                                                        {for $num=-100 to 100}
                                                            <option value="{$num}" {if isset($strategy.delta_min) && $strategy.delta_min == $num}selected{/if}>
                                                                {$num}%
                                                            </option>
                                                        {/for}
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="col-xs-2 text-center">
                                                <p class="m-t10">{ci_language line="to"}</p>
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="form-group">
                                                    <select rel="rs_delta_max" name="strategy[{$k}][rs_delta_max]" id="strategy_{$k}_rs_delta_max" class="search-select">
                                                        <option value=""> -- </option>
                                                        {for $num=-100 to 100}
                                                            <option value="{$num}" {if isset($strategy.delta_max) && $strategy.delta_max == $num}selected{/if}>
                                                                {$num}%
                                                            </option>
                                                        {/for}
                                                    </select> 
                                                </div>
                                            </div>                               
                                        </div>
                                    </div>
                                </div>
                            </div><!--Delta-->

                        </div>

                    </div>

                </div>
            {/foreach}
        {/if}

    </div><!-- id main -->
    
    {include file="amazon/PopupStepMoreFooter.tpl"} 

    <!--Message-->
    <input type="hidden" id="submit-confirm" value="{ci_language line="Do you want to save 'Repricing Strategy'?"}" />
    <input type="hidden" id="confirm-delete-message" value="{ci_language line="Do you want to delete "}" />
    <input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
    <input type="hidden" id="delete-success-message" value="{ci_language line="Repricing Strategy was deleted"}" />
    <input type="hidden" id="error-message" value="{ci_language line="Error"}" />
    <input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />

</form>    
          
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/repricing_strategies.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}    
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}