{if isset($includeLabel) && $includeLabel}
    <p class="poor-gray" {if isset($label)}for="{$label}"{/if}>{ci_language line="Size"}</p>
{/if}  
<div class="form-group">
    <select rel="Size" class="StringNotNull chzn-select">
        <option value=""> -- </option>
        {if isset($size)}
            {foreach from=$size.value key=akey item=value}
                 <option value="{if isset($value.id_attribute_group)}{$value.id_attribute_group}{/if}" {if isset($value.selected) && $value.selected == 1}selected{/if}>
                   {if isset($value.name)}{$value.name}{/if}
                </option>
            {/foreach} 
        {/if}
    </select>
</div>