{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#shipping" data-toggle="tab" class="">
                    <i class="icon-dollar"></i> <span>{ci_language line="Shipping"}</span>
                </a>
            </li>                                     
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <div class="tab-pane active" id="shipping">                
                <div class="status" style="display: none">
                    <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                </div>
                
                <!--Shipping Override-->
                <p class="head_text montserrat black p-t20 b-None">{ci_language line="Send Shipping Override Only"}</p>
                <div class="row">
                    <div id="shipping_override">

                        <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
                        
                        <div class="col-xs-8">
                            <ul class="p-l20">
                                <li class="col-xs-12 p-0">
                                    <p class="poor-gray">{ci_language line='This will update Amazon Shipping Override, within the selected categories in your configuration.'}</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-4">
                             <div class="" id="sync_send_shipping_override_only">
                                <button class="btn btn-amazon" id="send_shipping_override_only" type="button" value="sync_send_shipping_override_only">
                                    <i class="icon-amazon"></i> {ci_language line="Send Shipping Override Only"}
                                </button>
                                <div class="status" style="display: none">
                                    <p>{ci_language line="Starting send shipping override"} ..</p>
                                </div>
                                <div class="error" style="display: none">
                                    <p class="m-t0">
                                        <i class="fa fa-exclamation-circle"></i>
                                        <span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                            
                    </div>      
                </div>

                <hr/>

                <!--Shipping Templates-->
                <p class="head_text montserrat black p-t20 b-None">{ci_language line="Shipping Templates"}</p> 
                <div class="shipping"> 

                    <div class="row">
                        <div class="col-xs-8">
                            <ul class="p-l20">
                                <li class="col-xs-12 p-0">
                                    <p class="poor-gray">
                                        {ci_language line='Whether you configured shipping template in Seller Central you can use them in Feed.biz '}
                                    <br/>
                                        {ci_language line='You can apply a template, whether to a profile, a product or a combination on the product sheet extension'}
                                    <br/>
                                        {ci_language line='If not specified, nowhere, then, your default shipping settings configured in Seller Central will be used.'}
                                    </p>
                                </li>
                            </ul>
                        </div>

                        <div class="col-xs-4">             
                            <button class="btn btn-success m-b5" id="shipping_groupname" type="button" value="Update Groups Names from Amazon">
                                {ci_language line="Update Groups Names from Amazon"}
                            </button>                                        
                            <div class="status" style="display: none">
                                <p>{ci_language line="Starting Update Groups Names from Amazon"} ..</p>
                            </div>
                            <div class="error" style="display: none">
                                <p class="m-t0">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>
                        </div>                   
                    </div>  

                    <!-- -->
                    <div class="row main-summary setSettings display-block">

                        <div class="col-xs-8 m-t30">
                            <div class="headSettings clearfix p-b10 b-Bottom ">
                                <h4 class="headSettings_head">{ci_language line="Group Names"} </h4>                      
                                <ul class="p-l20">
                                    <li class="col-xs-12 p-0">
                                        <p class="poor-gray">
                                            {ci_language line=' This is the name you gave to your shipping templates.'}                                        
                                            {ci_language line='You will can use shipping template in the profile configuration, and also in the product sheet extension.'}
                                        </p>
                                        <p class="poor-gray">
                                            {ci_language line='Here, there is nothing else to do than to fetch the list.'}
                                        </p>
                                    </li>
                                </ul>
                            </div>

                            <p class="head_text b-Top b-Bottom p-t10 m-0">                   
                                {ci_language line="Available Shipping Groups"}
                            </p>
                                        
                            <div class="sum_item">                               
                                {if isset($shipping_group_names) && !empty($shipping_group_names) }
                                    {foreach $shipping_group_names as $group_names}
                                        <div class="clearfix p-t10 summaryBlock">
                                            <p class="p-l10 pull-sm-left poor-gray">{$group_names}</p>                     
                                        </div>
                                    {/foreach}
                                {else}
                                    <div class="clearfix p-t10 summaryBlock">
                                        <p class="p-l10 pull-sm-left poor-gray">{ci_language line="No data"}</p>                     
                                    </div> 
                                {/if}  
                            </div>

                            <div id="main-template">
                                <div class="clearfix p-t10 summaryBlock" style="display:none">
                                    <p class="p-l10 pull-sm-left poor-gray"></p>                     
                                </div> 
                            </div>
                            
                            <p class="regRoboto text-right b-Top p-size text-small poor-gray p-t5">    
                                * {ci_language line="This list is displayed for information purpose"}
                            </p>  

                        </div>

                    </div>

                </div>                   

            </div>
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Are you sure to send shipping override?"}" id="send_shipping_override_only-confirm" />


<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
{if isset($country)}<input type="hidden" id="country" value="{$country}" />{/if}

<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}