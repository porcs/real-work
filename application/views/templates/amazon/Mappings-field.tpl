<div class="row">
    <div class="col-sm-6 m-b20">
        <select class="search-select">
            {if isset($attribute['is_color']) && $attribute['is_color'] == true}
                <option value="{$value.id_attribute}">{$value.value.color}</option>
            {else}
                {if isset($value.value)}
                    <option value="{$value.id_attribute}">{$value.value}</option>
                {/if}
            {/if}
        </select> 
        <i class="blockRight noBorder"></i>
    </div>
    <div class="col-sm-6">
        {if isset($attribute['is_color']) && $attribute['is_color'] == true}
            <div class="attribute-color">
                {if isset($valid) && !empty($valid)}

                    <select rel="attribute-color" class="colorpicker" name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]">
                        <option value="None" title="text">--</option>
                        {foreach $valid as $key_valid => $valid_value}
                            {$selected = false}
                            {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])}
                                {$mapping = $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}
                                {if $mapping == $valid_value['value']}
                                    {$selected = true}
                                {/if}                                                                            
                             {else}
                                 {if isset($value.value.colorMap) && isset($valid_value['desc']) && $value.value.colorMap == $valid_value['desc'] }
                                     {$selected = true}
                                 {/if}
                             {/if}
                            <option value="{if isset($valid_value['value'])}{$valid_value['value']}{/if}" title="text" {if $selected == true} selected {/if}>
                                {if isset($valid_value['desc'])}
                                    {$valid_value['desc']}
                                {/if}
                            </option>
                        {/foreach}
                        {if isset($mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                            {$custom_value = explode(', ', $mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                            {foreach $custom_value as $cv}
                                <option value="{$cv}" title="text" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $cv == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if}>{$cv}</option>
                            {/foreach}
                        {/if}  
                    </select>                    

                {else}

                    <input type="text" class="form-control" name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])} value="{$value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}" {/if}/>
                    
                {/if}
            </div>
        {else}     

            {if isset($valid) && !empty($valid)}
                <select class="search-select" name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]">
                    <option value="">--</option>
                    {foreach $valid as $key_valid => $valid_value}
                        <option value="{$valid_value['value']}" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $valid_value['value'] == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if}>{$valid_value['desc']}</option>
                    {/foreach}
                    {if isset($mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                        <optgroup label="{ci_language line="Custom Value"}">
                        {$custom_value = explode(', ', $mappings[$key_mapping][$key_product_type]['custom_value'][$key_attribute])}
                        {foreach $custom_value as $cv}
                            <option value="{$cv}" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]) && $cv == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}selected{/if}>{$cv}</option>
                        {/foreach}
                    </optgroup>
                    {/if}  

                </select>
            {else}
                <input type="text" class="form-control" name="{$attribute['type']}[{$attribute['id_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_attribute}]" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}])} value="{$value.mapping[{$key_mapping}][{$key_product_type}][{$key_attribute}]}" {/if}/>
            {/if}
        {/if}
    </div>
</div>