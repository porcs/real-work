<div {if isset($popup)}class="p-rl40 p-xs-rl20"{else}class="main p-rl40 p-xs-rl20"{/if}>

    {if isset($popup) && $popup != ''}          

        <div class="row">
            <div class="col-xs-12">
                <h1 class="lightRoboto text-uc head-size light-gray">
                {$class} {if isset($mode) && $mode == 1}{ci_language line="create offers"}{else}{ci_language line="product creation"}{/if} {ci_language line="Wizard"}
            </h1>
        </div>
    </div>
    <input type="hidden" id="popup_status" value="{$popup}">
    <div class="row">
        <div class="col-xs-12 hidden-xs">
            <div class="headSettings clearfix m-b10">
                <ul class="headSettings_point clearfix" {if isset($popup_more) && $popup_more != ''}style="opacity: 0.5;"{/if}>
                    <li class="headSettings_point-item {if $popup >= '1'} active{/if}" style="width: 20%;">
                        <span class="step">1</span>
                        <span class="title">{ci_language line="Choose Amazon site"}</span>
                    </li>
                    <li class="headSettings_point-item {if $popup >= '2'} active{/if}" style="width: 20%;">
                        <span class="step">2</span>
                        <span class="title">{ci_language line="Config API setting"}</span>
                    </li>
                    <li class="headSettings_point-item {if $popup >= '3'} active{/if}" style="width: 20%;">
                        <span class="step">3</span>
                        <span class="title">{ci_language line="Choose category"}</span>
                    </li>
                    <li class="headSettings_point-item {if $popup >= '4'} active{/if}" style="width: 20%;">
                        <span class="step">4</span>
                        <span class="title">
                            {if isset($mode) && $mode == 1}
                                {ci_language line="Sync & match"}
                            {else}
                                {ci_language line="Creation"}
                            {/if}
                        </span>
                    </li>
                    <li class="headSettings_point-item {if $popup >= '5'} active{/if}" style="width: 20%;">
                        <span class="step">5</span>
                        <span class="title">{ci_language line="Send to Amazon"}</span>
                    </li>
                    
                    
                </ul>
            </div>
        </div>
        <div class="col-xs-12 visible-xs">
            <div class="headSettings clearfix">
                <p class="text-uc dark-gray bold text-center m-0">
                    {if $popup == '1'}
                        step 1 of 5 : {ci_language line="Choose Amazon site"}
                    {/if}
                    {if $popup == '2'}
                        step 2 of 5 : {ci_language line="Config API setting"}
                    {/if}
                    {if $popup == '3'}
                        step 3 of 5 : {ci_language line="Choose category"}
                    {/if}
                    {if $popup == '4'}
                        step 4 of 5 : {if isset($mode) && $mode == 1}{ci_language line="Sync & match"}{else}{ci_language line="Creation"}{/if}
                    {/if}
                    {if $popup == '5'}
                        step 5 of 5 : {ci_language line="Send to Amazon"}
                    {/if}
                </p>
            </div>
        </div>
    </div>

    {if isset($country)} 
        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t10 b-Bottom clearfix">
                    <p class="head_text b-None clearfix m-0">
                        <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" class="flag m-r10" alt="">
                        <span class="p-t10">{$country}</span>                   
                    </p>
                </div>
            </div>
        </div>
    {/if}

    {if isset($add_data) && !empty($add_data)}
        <div class="row">
            <div class="col-xs-12 p-t10">
                <div class="b-Bottom p-b10">
                    <button type="button" class="link showProfile p-size" id="add"><i class="fa fa-plus"></i> {$add_data}</button>
                    <p class="pull-right montserrat poor-gray">
                        <span id="count">{if isset($size_data)}{$size_data}{else}0{/if}</span>
                        {ci_language line="Items(s)"}
                    </p>
                </div>
            </div>
        </div>           
    {/if}        

{else}

    <!-- Head -->
    <h1 class="lightRoboto text-uc head-size light-gray">
        {$function} 
        </h1>      

        {include file="breadcrumbs.tpl"} 

        <!-- Add -->
        <div class="row">
            <div class="col-xs-12">

                {if strtolower($function_tag) == "orders" || strtolower($function_tag) == "mappings"} <div class="b-Top p-t10 b-Bottom clearfix">{/if}
                    <p {if strtolower($function_tag) == "Category"}class="head_text p-t10 b-Top clearfix m-0"{else if strtolower($function_tag) == "orders" || strtolower($function_tag) == "mappings"} class="head_text pull-sm-left b-None clearfix m-0" {else}class="head_text p-t10 b-Top clearfix m-0"{/if}>
                        <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" class="flag m-r10" alt="">
                        {if isset($country)}<span class="p-t10">{$country}</span>{/if}
                        {if isset($popup_more) && $popup_more != ''} <i class="fa fa-angle-right"></i> {ucwords($function_tag)} {/if} 
                    </p>

                    {if strtolower($function_tag) == "orders"} 
                        <div class="pull-sm-right">
                            <button class="btn btn-save m-b10" data-toggle="modal" data-target="#importOrder" type="button">{ci_language line="Import Orders"}</button>  
                            <button class="btn btn-save m-b10" id="send-orders" type="button">{ci_language line="Send orders"}</button>
                        </div>
                    {/if}

                    {if strtolower($function_tag) == "mappings"}
                        <div class="pull-sm-right">
                            <button class="link p-size wizard_btn m-t10" href="{$base_url}/amazon/mappings_custom_value/{$id_country}" id="mappings_custom_value">
                                <i class="fa fa-plus"></i> {ci_language line="Add Custom Value"}
                            </button> 
                        </div>
                    {/if}

                    {if strtolower($function_tag) == "orders" || strtolower($function_tag) == "mappings"}</div>{/if}
            </div>
        </div>

        <!-- <div class="ruleSettings"></div> -->

        {if isset($add_data) && !empty($add_data)}
            <div class="row">
                <div class="col-xs-12 p-t10">
                    <div class="b-Bottom p-b10">
                        <button type="button" class="link showProfile p-size" id="add"><i class="fa fa-plus"></i> {$add_data}</button>
                        {if isset($size_data)}
                            <p class="pull-right montserrat poor-gray">
                                <span class="dark-gray countProfile" id="count">{$size_data}</span> 
                                {ci_language line="Items(s)"}
                            </p>
                        {/if}
                    </div>
                </div>
            </div>   
        {/if}       

        {/if}  

            {if isset($no_data)}
                <div class="m-t10" id="no-data">
                    {$no_data}
                </div>
            {/if}   