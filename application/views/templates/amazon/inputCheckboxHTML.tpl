<label {if isset($checkbox_checked) && !empty($checkbox_checked)}class="col-sm-3 p-r2 checked"{else}class="col-sm-3 p-r2 amazon-checkbox"{/if}>
    <span class="amazon-inner">
        <i>
        	<input type="checkbox" id="{$checkbox_id}" name="{$checkbox_id}" rel="{$checkbox_id}" value="true" {$checkbox_cssClass} {if isset($checkbox_checked) && !empty($checkbox_checked)}checked="checked"{/if} {$checkbox_disabled} />
        </i>
    </span>
    {ci_language line="Yes"}
</label>