{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
<form id="form-repricing" method="post" action="{$base_url}amazon/save_repricing_parameters" enctype="multipart/form-data" autocomplete="off" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 

    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-tb10 m-b10">
                 {ci_language line="API settings"}
            </p>
        </div>
    </div> 

    <div class="row m-t10"> 
                <div class="col-md-12 form-group">
                    <label class="col-md-2 m-t5 cb-checkbox">
                        </i><input type="checkbox" class="" value="1" id="key_pair_checkbox" />
                        {ci_language line="Duplicate key"}
                    </label>
                    <div class="col-md-4 p-0" id="key_pair_contain">
                        <select class="search-select" id="key_pair">
                            <option value=""> - - </option>
                            {if isset($api_key)}
                                {foreach $api_key as $id => $key}
                                    <option value="{$id}">amazon{$key['ext']}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div> 
                <div class="col-sm-4 form-group"><i id="api_key_loading" class="fa fa-spinner fa-spin m-t5" style="display:none"></i></div>   
            
    </div> 

    <div class="row m-t15">        
        <div class="col-sm-4 form-group">
                <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="AWS Key Id"} *</label>            
                <input class="form-control col-md-12" type="text" id="aws_key_id" name="aws_key_id" {if isset($aws_key_id)}value='{$aws_key_id}'{/if}/>
        </div>
        <div class="col-sm-4 form-group m-b10">
                <label class="text-uc dark-gray montserrat m-b5 col-md-12 p-0">{ci_language line="AWS Secret Key"}</label>            
                <input class="form-control col-md-12" type="password" id="aws_secret_key" name="aws_secret_key" {if isset($aws_secret_key)}value='{$aws_secret_key}'{/if}/>
            <!--  -->
        </div>
        <div class="col-sm-4 form-group">
                <button type="button" id="api_check" class="btn btn-check">
                    <i class="icon-check"></i>
                    <span id="check">{ci_language line="Check Connectivity"}</span>
                    <span id="checking" style="display:none">{ci_language line="Checking .."}</span>
                </button>
                <!-- help -->
                <div id="status-message" style="display:none">
                    <p></p>
                </div>
        </div>
    </div>    
    <hr/>
    <div class="row m-t15 m-b20">        
        <div class="form-group">
            <label class="text-uc dark-gray montserrat m-b5 col-md-12">{ci_language line="Default Override Shipping Charges"}</label>            
            <div class="col-md-4">
                <input class="form-control" type="text" name="defaut_override_shipping" {if isset($defaut_override_shipping)}value='{$defaut_override_shipping}'{/if}/>
            </div>
            <p class="regRoboto poor-gray text-left col-md-8 m-t10" >
                {ci_language line="The Default Override Shipping Charges will be used for repricing process."}                                   
            </p>
        </div>
    </div>

    <div class="row alert-code" style="display:none"></div>

    <div class="row alert-message m-t10 m-b10" style="display:none">
        <div class="col-xs-12">
            <div class="validate pink m-t10">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i>
                    </div>
                    <div class="validateCell">
                        <div class="pull-left code"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {include file="amazon/PopupStepMoreFooter.tpl"} 
                
    <!--Message-->
    <input type="hidden" id="connect-success" value="{ci_language line="Connection to Amazon successful"}" /> 
    <input type="hidden" id="connect-fail" value="{ci_language line="Connection to Amazon Failed"}" />
    <input type="hidden" id="connect-error" value="{ci_language line="Data connection error! Please contact your support, error code:"}" />
    <input type="hidden" value="{ci_language line="Loading"}.." id="loading">  
    <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">    
</form>    
          
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/repricing_parameters.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}    
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}