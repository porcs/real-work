<form id="purge-queue-form">
    <input type="hidden" value="{$Repricing_PURGE}" name="action" / >
    <table class="responsive-table">
        <thead class="table-head">
            <tr>
                <th>&nbsp;</th>
                <th>{ci_language line="Name"}</th>
                <th>{ci_language line="URL"}</th>
                <th>{ci_language line="Messages"}</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$data item=queue}
                <tr>
                    <td><label class="cb-checkbox"></i><input type="checkbox" name="purge_queue[{$queue.name}]" value="{$queue.url}" /></td>
                    <td>{$queue.name}</td>
                    <td>{$queue.url}</td>
                    <td>{$queue.count}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</form>