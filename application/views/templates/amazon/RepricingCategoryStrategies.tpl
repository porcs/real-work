{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}  

    <div class="row">
        <div class="col-xs-12">
            <form method="post" id="category_options_form" autocomplete="off" >
                <div class="headSettings clearfix b-Top-None">  
                    <div class="col-xs-6 clearfix">
                        <div class="row">
                            <h4 class="p-0 headSettings_head">{ci_language line="by"} {ci_language line="Category"}</h4>
                        </div>
                    </div>                                                                 
                    <div class="col-sx-6 pull-right" >
                        <button type="button" class="btn btn-save m-b0 m-l5">
                            {ci_language line="Submit"}
                        </button>
                        <div class="status" style="display: none">                                                  
                            <p class="p-t10 m-l5 p-b0 m-b0">
                                <i class="fa fa-spinner fa-spin"></i> 
                                <span>{ci_language line="Submiting"}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div id="category_options"></div>                       
            </form>                  
        </div>
    </div>      

    <div id="model" style="display:none">
        <div class="row-edit p-10 p-l40 custom-form">
           <table cellpadding="5" cellspacing="5" border="0" class="table_detail" >
                <!--Hidden Input-->
                <tr style="display:none">
                    <td width="35%" colspan="3">
                        <input type="hidden" class="id_category form-control" rel="id_category" />
                    </td>                                     
                </tr>                 
                <tr>
                    <td width="5%"></td>
                    <td width="20%"><p class="regRoboto poor-gray p-l5">{ci_language line="Minimum Price"} : </p></td>
                    <td width="50%">
                        <div class="row clearfix m-b10">
                            <div class="col-xs-4">       
                                <div class="clearfix">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <strong>{$currency_sign}</strong>
                                        </span>
                                        <input type="text" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" class="form-control" rel="minimum_price" />
                                    </div> 
                                </div> 
                            </div>
                            <div class="col-xs-8">
                                <p class="m-t5 poor-grey help-text m-0">
                                    ({ci_language line="on regular price"})
                                </p>
                                <p class="price-error p-0 m-0" style="display:none">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span>{ci_language line="A value must be a Number"}</span>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><p class="regRoboto poor-gray p-l5">{ci_language line="Maximun Price"} : </p></td>
                    <td>
                        <div class="row clearfix m-b10">
                            <div class="col-xs-4">       
                                <div class="clearfix">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <strong>{$currency_sign}</strong>
                                        </span>
                                        <input type="text" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" class="form-control" rel="target_price" />
                                    </div> 
                                </div> 
                            </div>
                            <div class="col-xs-8">
                                <p class="m-t5 poor-grey help-text m-0">
                                    ({ci_language line="on regular price"})
                                </p>
                                <p class="price-error p-0 m-0" style="display:none">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span>{ci_language line="A value must be a Number"}</span>
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>    
            </table>
        </div>
    </div>

{include file="amazon/PopupStepFooter.tpl"}

<input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="id_marketplace" value="{if isset($id_marketplace)}{$id_marketplace}{/if}" />
<input type="hidden" id="l_Error" value="{ci_language line="Error"}">
<input type="hidden" id="l_Success" value="{ci_language line="Success"}">
<input type="hidden" id="l_Submit" value="{ci_language line="Submiting"}">
<input type="hidden" id="l_Edit" value="{ci_language line="Edit"}">
<input type="hidden" id="l_Reset" value="{ci_language line="Reset"}">
<input type="hidden" id="l_Mismatch" value="{ci_language line="Item unactivated: Multiple edition is possible only for matching values"}">
<input type="hidden" id="reset-confirm" value="{ci_language line="Do you want to reset all product options in "}">

{include file="IncludeDataTableLanguage.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/FeedBiz/treeTable.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/category_strategies.js"></script>

{include file="footer.tpl"}