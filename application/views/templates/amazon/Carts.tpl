{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div id="content"> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  
             
            <div class="row">
                <div class="col-xs-12">                  

                    <div class="headSettings clearfix b-Top-None">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Amazon Orders"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix">                                
                                <div class="b-Right p-r10"></div>
                           </div>
                        </div>          
                    </div><!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="carts" class="responsive-table">
                                <thead class="table-head">
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{ci_language line="SKU"}</th>
                                        <th class="text-center">{ci_language line="Status"}</th> 
                                        <th class="text-center">{ci_language line="Quantity"}</th>
                                        <th class="text-center">{ci_language line="Date"}</th>
                                        <!-- <th></th> -->
                                    </tr>
                                </thead>
                            </table><!--table-->
                            <table class="table tableEmpty">
                                <tr>
                                    <td>{ci_language line="No data available in table"}</td>
                                </tr>
                            </table>
                        </div><!--col-xs-12-->
                    </div><!--row--> 

                </div><!--col-xs-12-->
            </div><!--row-->   

            <!--Hidden-->
            <input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
            <input type="hidden" id="OrderIDRef" value="{ci_language line="Order ID"}" />
            <input type="hidden" id="Pending" value="{ci_language line="Pending"}" />         

            {include file="amazon/IncludeDataTableLanguage.tpl"}
        </div> 
    
    {include file="amazon/PopupStepFooter.tpl"} 
    
</div> 
        
<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/carts.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}