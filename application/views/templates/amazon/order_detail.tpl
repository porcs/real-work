{if isset($order) && !empty($order)}
    <div class="row col-md-12">
        {if isset($order['Order:SalesChannel'])}
            <label class="col-sm-3">{ci_language line="Sales Channel"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderSalesChannel">{$order['Order:SalesChannel']}</p></div>
        {/if}
        {if isset($order['Order:Multichannel'])}
            <label class="col-sm-3">{ci_language line="Multichannel"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderMultichannel">{$order['Order:Multichannel']}</p></div>
        {/if}
    </div>

    <div class="row col-md-12">
        {if isset($order['Order:Language'])}
            <label class="col-sm-3">{ci_language line="Language"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderLanguage">{$order['Order:Language']}</p></div>
        {/if}
        {if isset($order['Order:Currency'])}
            <label class="col-sm-3">{ci_language line="Currency"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderCurrency">{$order['Order:Currency']}</p></div>
        {/if}
    </div>

    <div class="row col-md-12">
        {if isset($order['Order']['Status'])}
            <label class="col-sm-3">{ci_language line="Status"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderStatus">{$order['Order']['Status']}</p></div>
        {/if}
        {if isset($order['Order']['Type'])}
            <label class="col-sm-3">{ci_language line="Type"}</label>
            <div class="col-sm-3"><p rel="DisplayableOrderType">{$order['Order']['Type']}</p></div>
        {/if}
    </div>

    {if isset($order['Order']['References'])}
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="References"}</h4>                      
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>{ci_language line="ID"}</th>
                    <th>{ci_language line="MarketplaceID"}</th>
                </tr>
            </thead>
            <tr>
                <td>{if isset($order['Order']['References']['Id'])}{$order['Order']['References']['Id']}{/if}</td>
                <td>{if isset($order['Order']['References']['MarketplaceID'])}{$order['Order']['References']['MarketplaceID']}{/if}</td>
            </tr>
        </table>
    </div>
    {/if}
    
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Payment"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Payment']['PaymentMethod'])}
                <label class="col-sm-3">{ci_language line="Payment Method"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderPaymentPaymentMethod">{$order['Order']['Payment']['PaymentMethod']}</p></div>
            {/if}
            {if isset($order['Order']['Payment']['PaymentStatus'])}
                <label class="col-sm-3">{ci_language line="Payment Status"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderPaymentPaymentStatus">{$order['Order']['Payment']['PaymentStatus']}</p></div>
            {/if}
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Payment']['CurrencyID'])}
                <label class="col-sm-3">{ci_language line="Currency ID"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderPaymentCurrencyID">{$order['Order']['Payment']['CurrencyID']}</p></div>
            {/if}
            {if isset($order['Order']['Payment']['Amount'])}
                <label class="col-sm-3">{ci_language line="Amount"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderPaymentAmount">{$order['Order']['Payment']['Amount']}</p></div>
            {/if}
        </div>

        {if isset($order['Order']['Payment']['ExternalTransactionID'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="External Transaction ID"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderPaymentExternalTransactionID">{$order['Order']['Payment']['ExternalTransactionID']}</p></div>
            </div>
        {/if}
    </div>
    
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Invoices"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Invoices:InvoiceNo'])}
                <label class="col-sm-3">{ci_language line="Invoice No"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInvoicesInvoiceNo">{$order['Order']['Invoices:InvoiceNo']}</p></div>
            {/if}
            {if isset($order['Order']['Invoices:SellerOrderId'])}
                <label class="col-sm-3">{ci_language line="Seller Order Id"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInvoicesSellerOrderId">{$order['Order']['Invoices:SellerOrderId']}</p></div>
            {/if}

            {if isset($order['Order']['Invoices'])}
                <table class="table dSTable">
                    <thead>
                        <tr>
                            <th>{ci_language line="Total"}</th>
                            <th class="text-right">{ci_language line="Discount"}</th>
                            <th class="text-right">{ci_language line="Paid"}</th>
                            <th class="text-right">{ci_language line="Shipping"}</th>
                            <th class="text-center">{ci_language line="Products"}</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>{ci_language line="Tax Incl"}</td>
                        <td class="text-right">{$order['Order']['Invoices']['Total']['Discount']['TaxIncl']}</td>
                        <td class="text-right">{$order['Order']['Invoices']['Total']['Paid']['TaxIncl']}</td>
                        <td class="text-right">{$order['Order']['Invoices']['Total']['Shipping']['TaxIncl']}</td>
                        <td class="text-center">{$order['Order']['Invoices']['Total']['Products']}</td>
                    </tr>
                </table>
            {/if}
            {if isset($order['Order']['Invoices']['note']) && !empty($order['Order']['Invoices']['note'])}
                <label class="col-sm-3">{ci_language line="Note"} : </label>
                <div class="col-sm-3"><p rel="DisplayableOrderInvoicesNote">{$order['Order']['Invoices']['note']}</p></div>
            {/if}
        </div>
    </div>
    
    {if isset($order['Order']['Total']['Amount']) && isset($order['Order']['Total']['Shipping']) && isset($order['Order']['Total']['Discount']) && isset($order['Order']['Total']['Paid'])}
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Total"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Total']['Amount'])}
                <label class="col-sm-3">{ci_language line="Amount"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderTotalAmount">{$order['Order']['Total']['Amount']}</p></div>
            {/if}
            {if isset($order['Order']['Total']['Shipping'])}
                <label class="col-sm-3">{ci_language line="Shipping"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderTotalShipping">{$order['Order']['Total']['Shipping']}</p></div>
            {/if}
        </div>
        <div class="row col-md-12">
            {if isset($order['Order']['Total']['Discount'])}
                <label class="col-sm-3">{ci_language line="Discount"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderTotalDiscount">{$order['Order']['Total']['Discount']}</p></div>
            {/if}
            {if isset($order['Order']['Total']['Paid'])}
                <label class="col-sm-3">{ci_language line="Paid"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderTotalPaid">{$order['Order']['Total']['Paid']}</p></div>
            {/if}
        </div>
    </div>
    {/if}
        
    {if isset($order['Order']['Items'])}
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Items"}</h4>                      
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th class="text-center">{ci_language line="ID"}</th>
                    <th class="text-center">{ci_language line="Marketplace Item ID"}</th>
                    <th class="text-center">{ci_language line="Marketplace Product ID"}</th>
                    <th class="text-center">{ci_language line="Reference"}</th>
                    <th class="text-right">{ci_language line="Unit Price"}</th>
                    <th class="text-right">{ci_language line="Ordered"}</th>
                    <th class="text-right">{ci_language line="Paid"}</th>
                    <th class="text-right">{ci_language line="Shipping"}</th>
                </tr>
            </thead>
            {foreach $order['Order']['Items'] as $itemid => $item}
                <tr>
                    <td class="text-center">{if isset($itemid)}{$itemid}{/if}</td>
                    <td class="text-center">{if isset($order['Order']['Items'][$itemid]['MarketplaceItemID'])}{$order['Order']['Items'][$itemid]['MarketplaceItemID']}{/if}</td>
                    <td class="text-center">{if isset($order['Order']['Items'][$itemid]['MarketplaceProductID'])}{$order['Order']['Items'][$itemid]['MarketplaceProductID']}{/if}</td>
                    <td class="text-center">{if isset($order['Order']['Items'][$itemid]['Product']['Reference'])}{$order['Order']['Items'][$itemid]['Product']['Reference']}{/if}</td>
                    <td class="text-right">{if isset($order['Order']['Items'][$itemid]['Price']['Product'])}{$order['Order']['Items'][$itemid]['Price']['Product']}{/if}</td>
                    <td class="text-right">{if isset($order['Order']['Items'][$itemid]['Quantity']['Ordered'])}{$order['Order']['Items'][$itemid]['Quantity']['Ordered']}{/if}</td>
                    <td class="text-right">{if isset($order['Order']['Items'][$itemid]['Total']['Price']['TaxIncl'])}{$order['Order']['Items'][$itemid]['Total']['Price']['TaxIncl']}{/if}</td>
                    <td class="text-right">{if isset($order['Order']['Items'][$itemid]['Total']['Shipping']['TaxIncl'])}{$order['Order']['Items'][$itemid]['Total']['Shipping']['TaxIncl']}{/if}</td>
                </tr>
            {/foreach}
        </table>
    </div>
    {/if}
                
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Buyer"}</h4>                      
            </div>
        </div>

        {if isset($order['Order']['Buyer:ReferenceID'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Buyer Reference ID"}</label>
                <div class="col-sm-9"><p rel="DisplayableOrderBuyerReferenceID">{$order['Order']['Buyer:ReferenceID']}</p></div>
            </div>
        {/if}

        <div class="row col-md-12">
            {if isset($order['Order']['Buyer']['FirstName'])}
                <label class="col-sm-3">{ci_language line="FirstName"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderBuyerFirstName">{$order['Order']['Buyer']['FirstName']}</p></div>
            {/if}
            {if isset($order['Order']['Buyer']['Lastname'])}
                <label class="col-sm-3">{ci_language line="Lastname"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderBuyerLastname">{$order['Order']['Buyer']['Lastname']}</p></div>
            {/if}
        </div>
    </div>
        
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Shipping"}</h4>                      
            </div>
        </div>

        {if isset($order['Order']['Shipping:ReferenceID'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Shipping Reference ID"}</label>
                <div class="col-sm-9"><p rel="DisplayableOrderShippingReferenceID">{$order['Order']['Shipping:ReferenceID']}</p></div>
            </div>
        {/if}

        <div class="row col-md-12">
            {if isset($order['Order']['Shipping']['Name']['Firstname'])}
                <label class="col-sm-3">{ci_language line="Firstname"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingNameFirstname">{$order['Order']['Shipping']['Name']['Firstname']}</p></div>
            {/if}
            {if isset($order['Order']['Shipping']['Name']['Lastname'])}
                <label class="col-sm-3">{ci_language line="Lastname"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingNameLastname">{$order['Order']['Shipping']['Name']['Lastname']}</p></div>
            {/if}
        </div>

        {if isset($order['Order']['Shipping']['Name']['Company'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Company"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingNameCompany">{$order['Order']['Shipping']['Name']['Company']}</p></div>
            </div>
        {/if}

        {if isset($order['Order']['Shipping']['Address']['Address1'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Address 1"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressAddress1">{$order['Order']['Shipping']['Address']['Address1']}</p></div>
            </div>
        {/if}
        {if isset($order['Order']['Shipping']['Address']['Address2'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Address 2"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressAddress2">{$order['Order']['Shipping']['Address']['Address2']}</p></div>
            </div>
        {/if}
        <div class="row col-md-12">
            {if isset($order['Order']['Shipping']['Address']['StateRegion'])}
                <label class="col-sm-3">{ci_language line="State Region"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressStateRegion">{$order['Order']['Shipping']['Address']['StateRegion']}</p></div>
            {/if}
            {if isset($order['Order']['Shipping']['Address']['City'])}
                <label class="col-sm-3">{ci_language line="City"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressCity">{$order['Order']['Shipping']['Address']['City']}</p></div>
            {/if}
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Shipping']['Address']['District'])}
                <label class="col-sm-3">{ci_language line="District"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressDistrict">{$order['Order']['Shipping']['Address']['District']}</p></div>
            {/if}
            {if isset($order['Order']['Shipping']['Address']['CountryCode'])}
                <label class="col-sm-3">{ci_language line="Country Code"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressCountryCode">{$order['Order']['Shipping']['Address']['CountryCode']}</p></div>
            {/if}
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Shipping']['Address']['CountryName'])}
                <label class="col-sm-3">{ci_language line="Country Name"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressCountryName">{$order['Order']['Shipping']['Address']['CountryName']}</p></div>
            {/if}
            {if isset($order['Order']['Shipping']['Address']['PostalCode'])}
                <label class="col-sm-3">{ci_language line="Postal Code"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressPostalCode">{$order['Order']['Shipping']['Address']['PostalCode']}</p></div>
            {/if}
        </div>

        {if isset($order['Order']['Shipping']['Address']['Phone'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Phone"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderShippingAddressPhone">{$order['Order']['Shipping']['Address']['Phone']}</p></div>
            </div>
        {/if}
    </div>
        
    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Carrier"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            <label class="col-sm-3">{ci_language line="Carrier ID"}</label>
            <div class="col-sm-9"><p rel="DisplayableOrderCarrierID">{$order['Order']['Carrier:ID']}</p></div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Carrier']['ShipmentService'])}
                <label class="col-sm-3">{ci_language line="Shipment Service"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderCarrierShipmentService">{$order['Order']['Carrier']['ShipmentService']}</p></div>
            {/if}
            {if isset($order['Order']['Carrier']['Weight'])}
                <label class="col-sm-3">{ci_language line="Weight"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderCarrierWeight">{$order['Order']['Carrier']['Weight']}</p></div>
            {/if}
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Carrier']['TrackingNumber'])}
                <label class="col-sm-3">{ci_language line="Tracking Number"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderCarrierTrackingNumber">{$order['Order']['Carrier']['TrackingNumber']}</p></div>
            {/if}
            {if isset($order['Order']['Carrier']['ShippingServicesCost'])}
                <label class="col-sm-3">{ci_language line="Shipping Services Cost"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderCarrierShippingServicesCost">{$order['Order']['Carrier']['ShippingServicesCost']}</p></div>
            {/if}
        </div>

        {if isset($order['Order']['Carrier']['ShippingServicesLevel'])}
            <div class="row col-md-12">
                <label class="col-sm-3">{ci_language line="Shipping Services Level"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderCarrierShippingServicesLevel">{$order['Order']['Carrier']['ShippingServicesLevel']}</p></div>
            </div>
        {/if}
    </div>

    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Date"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Date']['OrderDate'])}
                <label class="col-sm-3">{ci_language line="Order Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDateOrderDate">{$order['Order']['Date']['OrderDate']}</p></div>
            {/if}
            {if isset($order['Order']['Date']['PurchaseDate'])}
                <label class="col-sm-3">{ci_language line="Purchase Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDatePurchaseDate">{$order['Order']['Date']['PurchaseDate']}</p></div>
            {/if}
        </div>
        <div class="row col-md-12">
            {if isset($order['Order']['Date']['ShippingDate'])}
                <label class="col-sm-3">{ci_language line="Shipping Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDateShippingDate">{$order['Order']['Date']['ShippingDate']}</p></div>
            {/if}
            {if isset($order['Order']['Date']['LatestShipDate'])}
                <label class="col-sm-3">{ci_language line="LatestShip Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDateLatestShipDate">{$order['Order']['Date']['LatestShipDate']}</p></div>
            {/if}
        </div>
        <div class="row col-md-12">
            {if isset($order['Order']['Date']['DeliveryDate'])}
                <label class="col-sm-3">{ci_language line="Delivery Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDateDeliveryDate">{$order['Order']['Date']['DeliveryDate']}</p></div>
            {/if}
            {if isset($order['Order']['Date']['LatestDeliveryDate'])}
                <label class="col-sm-3">{ci_language line="Latest Delivery Date"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderDateLatestDeliveryDate">{$order['Order']['Date']['LatestDeliveryDate']}</p></div>
            {/if}            
        </div>
    </div>

    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Gift"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Gift']['GiftMessage'])}
                <label class="col-sm-3">{ci_language line="Gift Message"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderGiftMessage">{$order['Order']['Gift']['GiftMessage']}</p></div>
            {/if}
            {if isset($order['Order']['Gift']['GiftAmount'])}
                <label class="col-sm-3">{ci_language line="Gift Amount"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderGiftAmount">{$order['Order']['Gift']['GiftAmount']}</p></div>
            {/if}
        </div>
    </div>

    <div class="col-md-12 form-group">
        <div class="headSettings clearfix b-Bottom form-group">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Info"}</h4>                      
            </div>
        </div>

        <div class="row col-md-12">
            {if isset($order['Order']['Info']['AffiliateID'])}
                <label class="col-sm-3">{ci_language line="Affiliate ID"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoAffiliateID">{$order['Order']['Info']['AffiliateID']}</p></div>
            {/if}
            {if isset($order['Order']['Info']['Commission'])}
                <label class="col-sm-3">{ci_language line="Commission"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoCommission">{$order['Order']['Info']['Commission']}</p></div>
            {/if}
        </div>
        <div class="row col-md-12">
            {if isset($order['Order']['Info']['Status'])}
                <label class="col-sm-3">{ci_language line="Status"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoStatus">{$order['Order']['Info']['Status']}</p></div>
            {/if}
            {if isset($order['Order']['Info']['UpdateDate'])}
                <label class="col-sm-3">{ci_language line="UpdateDate"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoUpdateDate">{$order['Order']['Info']['UpdateDate']}</p></div>
            {/if}
        </div>
        <div class="row col-md-12">
            {if isset($order['Order']['Info']['StockMovement'])}
                <label class="col-sm-3">{ci_language line="Stock Movement"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoStockMovement">{$order['Order']['Info']['StockMovement']}</p></div>
            {/if}
            {if isset($order['Order']['Info']['StockMovementOnDate'])}
                <label class="col-sm-3">{ci_language line="StockMovementOnDate"}</label>
                <div class="col-sm-3"><p rel="DisplayableOrderInfoStockMovementOnDate">{$order['Order']['Info']['StockMovementOnDate']}</p></div>
            {/if}
        </div>
    </div>

{/if}