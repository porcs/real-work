{ci_config name="base_url"}
<div {if isset($isAttribute) && $isAttribute == true}class="col-sm-3 p-r2"{else}class="col-sm-3 p-r2"{/if}>
    <select id="{$select_id}" name="{$select_id}" rel="{$select_id}" {$select_cssClass} {$select_disabled}>
        <option value="" {$select_selected_non}> -- </option>
        
        {if isset($specific_fields.attribute)}
            <optgroup label="{ci_language line="Attribute"}" >
                {foreach from=$specific_fields.attribute item=attribute}
                    <option value="attribute[{$attribute.value}]" {$attribute.selected} >{$attribute.desc}</option>
                {/foreach}
            </optgroup>
        {/if}
        
        {if isset($specific_fields.feature)}
            <optgroup label="{ci_language line="Feature"}">
                {foreach from=$specific_fields.feature item=feature}
                    <option value="feature[{$feature.value}]" {$feature.selected} >{$feature.desc}</option>
                {/foreach}
            </optgroup>
        {/if}
        
        {if isset($select_custom_value) && $select_custom_value == true}
            <option value="CustomValue" style="color: #6fb3e0; font-weight: bold;" {if isset($specific_fields.value)}selected{/if}>{ci_language line="Custom Value"}</option>
        {/if}
    </select>

</div>

{if isset($select_options) && !empty($select_options)}
    <div class="col-sm-3 p-r2">
        <select {if !isset($select_options.value)}style="display: none;"{/if} rel="{$select_id}" class="custom_value search-select" >
            <option {if isset($select_selected_non)}value="{$select_selected_non}"{/if}> -- </option>
            {foreach from=$select_options item=option}
                <option value="{$option.value}" {if isset($option.selected)}{$option.selected}{/if} >{$option.desc}</option>
            {/foreach}
        </select>
    </div>
{else}

    <div class="valid_value inline col-sm-3 p-r2" {if !isset($specific_fields.value) || empty($specific_fields.value)}style="display:none"{/if}>
        {if isset($specific_fields.value) && !empty($specific_fields.value)}

            {if is_array($specific_fields.value) && !empty($specific_fields.value)}
                <select {if !isset($specific_fields.value)}style="display: none;"{/if} rel="{$select_id}" class="custom_value search-select" >
                    {foreach from=$specific_fields.value item=option}
                        <option value="{$option.value}" {if isset($option.selected)}{$option.selected}{/if} >{$option.desc}</option>
                    {/foreach}
                </select>
            {else}
                <input type="text" {if !isset($specific_fields.value)}style="display: none;"{/if} rel="{$text_rel}" class="custom_value form-control" {if isset($specific_fields.value)}value="{if $specific_fields.value == 'CustomValue'}{if isset($specific_fields.CustomValue)}{$specific_fields.CustomValue}{/if}{else}{$specific_fields.value}{/if}"{/if}/>
            {/if}

        {/if}
    </div>
{/if} 