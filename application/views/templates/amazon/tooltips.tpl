<h3 class="dark-gray bold b-Bottom p-b5 m-t5">{if isset($translation) && $translation}{$translation}{/if} {if isset($original_name)}({$original_name}){/if}</h3>
{if isset($description) && $description}<p>{$description}</p>{/if}
{if isset($tip) && $tip}
<div class="clearfix">
	<div class="icon_tooltips">
		<i class="icon-idea"></i>
	</div>
	<div class="content-tooltips">
		<p class="dark-gray bold m-0"><u>{ci_language line="Tips"}</u></p> 
		<p class="light-gray">{$tip}</p>
	</div>
</div>
{/if}
{if isset($sample) && $sample}
<div class="clearfix">
	<div class="icon_tooltips clearfix">
		<i class="icon-sample"></i>
	</div>
	<div class="content-tooltips">
		<p class="dark-gray bold m-0"><u>{ci_language line="Sample"}</u></p> 
		<p class="light-gray">{$sample}</p>
	</div>
</div>
{/if}