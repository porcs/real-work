{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  

            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Top-None">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Manage Profiles"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix"></div>
                        </div>          
                    </div><!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="ad_profile" class="amazon-responsive-table hover">
                                <thead class="table-head">
                                    <tr>
                                        <th class="center">{ci_language line="Profile Id"}</th>
                                        <th class="center">{ci_language line="Marketplace String Id"}</th>
                                        <th class="center">{ci_language line="Seller String Id"}</th>
                                        <th class="center">{ci_language line="Timezone"}</th>
                                        <th class="center">{ci_language line="Country Code"}</th>
                                        <th class="center">{ci_language line="Currency Code"}</th>
                                        <th class="center">{ci_language line="Daily Budget"}</th>
                                        <th class="center"></th>
                                    </tr>
                                </thead> 
                            </table>
                        </div> <!--col-xs-12-->
                    </div><!--row-->

                </div><!--col-xs-12-->
            </div><!--row--> 

            <div class="row m-t15 footer-step">
                <div class="col-xs-12">
                    <div class="b-Top p-t10">
                        {if isset($next_page)}
                        <div class="inline pull-right">  
                            <button class="btn btn-small p-size" id="next" type="button" value="{$next_page}">
                                {ci_language line="Manage Campaigns"}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>                        
                        </div>
                        {/if}
                    </div>      
                </div>
            </div>

            <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
            <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data"}" />
            <input type="hidden" id="please-try" value="{ci_language line="Please try again"}" />
            <input type="hidden" value="{ci_language line="Budget"}" id="Edit" />
            <input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
            <input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
            <input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
            <input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
            <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">   
            <input type="hidden" value="{ci_language line="Profile ID"}" id="id_title"> 
            <input type="hidden" value="{ci_language line="item(s)"}" id="items"> 
            <input type="hidden" value="{ci_language line="Total"}" id="Total"> 
            

            {include file="amazon/IncludeDataTableLanguage.tpl"}   

        </div> 

    {include file="amazon/PopupStepFooter.tpl"} 

    <div class="modal fade" id="profileDetails">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Change Daily Budget"}</h1>
                        </div>
                    </div>
                    <div id="profileDetail" class="m-t10"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="template" style="display:none">
        <div class="row custom-form">
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Profile Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="profileId" name="profileId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Daily Budget"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="dailyBudget" name="dailyBudget" />
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t20">
                    <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="save_profile();" >
                        {ci_language line="Save"}
                    </button>
                </div>
            </div>
        </div>
    </div>

</div> 
      
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/advertising.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}