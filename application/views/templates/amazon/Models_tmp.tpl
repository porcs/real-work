<div class="col-md-12 col-lg-12 custom-form">
    <div  rel="{$k}" class="profileBlock clearfix amazon-model m-t10 m-b30">

        <input type="hidden" rel="id_model" name="model[{$k}][id_model]" id="model_{$k}_id_model"  value="{$data.id_model}" />
        <input type="hidden" rel="memory_usage" name="model[{$k}][memory_usage]" id="model_{$k}_memory_usage" data-content="{$k}" />     

        <!-- Model Name -->
        <p class="poor-gray">{ci_language line="Model name"}</p>
        <div class="form-group">
            <input type="text" rel="name" value="{if isset($data.name)}{$data.name}{/if}" name="model[{$k}][name]" id="model_{$k}_name" data-content="{$k}" class="form-control"/>  
        </div>
        <p class="poor-gray text-right">
            {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
        </p>

        <!-- Product Universe -->
        <p class="poor-gray">{ci_language line="Product Universe"}</p>
        <div class="form-group">
            {if isset($data.universe)}
                <select rel="universe" name="model[{$k}][universe]" id="model_{$k}_universe" data-content="{$k}" class="search-select" data-placeholder="Choose">
                    {foreach from=$data.universe item=universe}
                       {$universe} 
                    {/foreach} 
                </select>
            {/if}                   
        </div>
        <p class="poor-gray text-right">{ci_language line="Choose the main (Root Node) universe for this model."}</p>

        <!--Product Type-->
        <div rel="product_type_container">
            <p class="poor-gray">{ci_language line="Product Type"}</p>
            <div class="form-group">
                {if isset($data.product_type_options)}
                    <select rel="product_type" name="model[{$k}][product_type]" id="model_{$k}_product_type" data-content="{$k}" class="search-select" data-placeholder="Choose">
                        {foreach from=$data.product_type_options item=option}
                            {$option}
                        {/foreach} 
                    </select>
                {/if}                                                            
            </div>
            <p class="poor-gray text-right">{ci_language line="Please select the main category for this model"}.</p>
        </div>

        <!--Product Sub Type-->
        <div rel="product_subtype_container" id="model_{$k}_product_subtype_container" data-content="{$k}">
            {if isset($data.product_subtype)}{$data.product_subtype}{/if}
        </div>

        <!--mfr_part_number-->
        <div rel="mfr_part_number_container" id="model_{$k}_mfr_part_number_container" data-content="{$k}">
            {if isset($data.mfr_part_number)}                                    
                {$data.mfr_part_number}
            {/if}
        </div>

        <!--Variation-->
        <div rel="variation_theme_container" id="model_{$k}_variation_theme_container" data-content="{$k}">
            {if isset($data.variation_theme)}
                <p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">
                    {ci_language line="Variation"}
                </p>
                {$data.variation_theme}
            {/if}
        </div>

        <!--Variation Data-->
        <div  rel="variation_data" id="model_{$k}_variation_data" data-content="{$k}">
            {if isset($data.variation_data)}{$data.variation_data}{/if}
        </div>

        <!--Recommended Data-->
        <div rel="recommended_data" id="model_{$k}_recommended_data" data-content="{$k}">
            {if isset($data.recommended_data)}
                <p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">
                    {ci_language line="Recommended Fields"}
                </p>
                {$data.recommended_data}
            {/if}
        </div>

        <!-- Specific -->
        <div class="specific_fields_options" id="model_{$k}_specific_fields_options" data-content="{$k}">
            <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                {ci_language line="Specific Fields"}
            </p>
            <div class="row">
                <div class="col-xs-12 m-b10">
                    <div class="form-group withIcon m-t20">
                        <select rel="specific_options" name="model[{$k}][specific_options]" id="model_{$k}_specific_options" data-content="{$k}" class="search-select" data-placeholder="Choose">
                            {if isset($data.specific_options)}{$data.specific_options}{/if} 
                        </select> &nbsp; 
                        <i class="cb-plus good addSpecificField"></i>
                        <p class="poor-gray text-right">
                            {ci_language line="Choose the value in specific fields and then click ADD button (+) to add more fields for this model."}
                        </p>                                                                      
                        <p class="poor-gray">
                            <span class="addSpecificField_Loading" style="display:none">
                                <i class="fa fa-spinner fa-spin"></i>
                                {ci_language line="Loading"}..
                            </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>  

        <div rel="specific_fields" id="model_{$k}_specific_fields" data-content="{$k}">
            {if isset($data.specific_fields)}{$data.specific_fields}{/if}
        </div>                             
    </div>
</div>