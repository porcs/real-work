<div class="amazon-model-item m-b10">
    {if isset($isMain) && $isMain}
        <p class="poor-gray">{$isMain}</p>
    {/if} 
    <div {if isset($removable_option) && $removable_option && !isset($withIcon)}class="form-group withIcon row"{else}class="form-group {if isset($isMain)}clearfix{else}row{/if}"{/if} {if isset($additionalStyle)}style="{$additionalStyle}"{/if}>

        {if isset($includeLabel) && $includeLabel}
            <div class="col-sm-3 p-0">
                <p class="poor-gray text-right m-t5" {if isset($label)}for="{$label}"{/if}>
                    <span {if isset($displayTitle) && $displayTitle}class="tooltips"{/if} {if isset($displayTitle) && $displayTitle}title="{$displayTitle}"{/if}>
                        {if isset($displayName)}{$displayName}{/if}
                    </span>
                </p>
            </div>
        {/if}  

        {if isset($input_html)}{$input_html}{/if}
  
        {if isset($required) && $required == 1}
            <span class="required">{ci_language line="required"}</span>
        {else if isset($recommended) && $recommended == 1}
            <span class="recommended">{ci_language line="recommended"}</span>
        {/if}

        {if isset($add_option) && $add_option}
            <i class="cb-plus good addSpecificField"></i>       
        {else if isset($removable_option) && $removable_option} 
            <i class="cb-plus bad removeSpecificField"></i>  
            {if isset($name)}
                <input type="hidden" value="{$name}" class="amzn_option_name"/>
            {/if}
        {/if} 
    </div>
</div>