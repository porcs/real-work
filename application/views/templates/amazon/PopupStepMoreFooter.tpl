{if isset($popup)}<div class="p-t40"><div class="p-t40">&nbsp;</div></div>{/if}
{if isset($popup_more) && $popup_more != ''} 
    
    <div class="row {if isset($popup)}popup_action{/if}">
        <div class="col-xs-12">
            <div class="b-Top p-t20">
                <div class="inline pull-right">  
                    <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                
                    <button class="pull-left link p-size m-tr10" type="button" id="back" >
                        {ci_language line="Previous"}
                    </button>

                    {if !isset($popup)}                        
                        <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'; ">
                            <i class="fa fa-save"></i> {ci_language line="Save"}
                        </button> &nbsp;                          
                    {/if}
                    {if isset($popup_more) && $popup_more != '' }
                        <button class="btn btn-save" disabled type="button">
                            {ci_language line="Next"}
                        </button>           
                        <button class="btn btn-save btn-danger" id="save_continue_data" type="submit">
                           {ci_language line="Save and continue"}
                        </button> 
                    {/if}
                </div>
            </div>
        </div>
    </div>

{else}
    
    <div class="row {if isset($popup)}popup_action{/if}">
        <div class="col-xs-12">
            <div class="b-Top p-t20">
                <div class="inline pull-left">
                    {if isset($popup) && !isset($not_previous)}
                        <button class="pull-left link p-size m-tr10" id="back" type="button">
                            {ci_language line="Previous"}
                        </button>
                    {/if}
				</div>
                <div class="inline pull-right">  

                    <!--Message--> 
                    <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                    <input type="hidden" id="country" value="{if isset($country)}{$country}{/if}" />
                    <input type="hidden" id="pay_now" value="{ci_language line="Pay now"}" />
                    <input type="hidden" id="field_required" value="{ci_language line="This field is required."}" />                
                    <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                    <input type="hidden" id="ext" name="ext" value="{$ext}" />
                    <input type="hidden" id="id_shop" name="id_shop" value="{$id_shop}" />  
                                        
                    {if isset($not_continue) && $not_continue == true && !isset($popup)}
                        <button class="btn btn-save" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'" >
                            {ci_language line="Save"}
                        </button>
                    {else}
                        {if !isset($popup)}
                            <button class="pull-left link p-size m-tr10" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'">
                                {ci_language line="Save"}
                            </button>
                        {/if}                    
                        <button class="btn btn-save btnNext" id="save_continue_data" type="submit">
                            {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}
                        </button>                        
                    {/if}               
                </div>
            </div>		
        </div>
    </div>

{/if}