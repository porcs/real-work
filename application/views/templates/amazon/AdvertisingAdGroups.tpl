{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  

            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Top-None">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Manage Ad Groups"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix"></div>
                        </div>          
                    </div><!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="ad_groups" class="amazon-responsive-table hover">
                                <thead class="table-head">
                                    <tr>
                                        <th class="center">{ci_language line="adGroup Id"}</th>
                                        <th class="center">{ci_language line="Name"}</th>
                                        <th class="center">{ci_language line="Campaign Id"}</th>
                                        <th class="center">{ci_language line="Default Bid"}</th>
                                        <th class="center">{ci_language line="State"}</th>
                                        <th class="center"></th>
                                    </tr>
                                </thead> 
                            </table>
                        </div> <!--col-xs-12-->
                    </div><!--row-->

                </div><!--col-xs-12-->
            </div><!--row--> 

            <div class="row m-t0 footer-step">
                <div class="col-xs-12">
                    <div class="b-Top p-t10">
                        {if isset($previous_page)}
                        <div class="inline pull-left">
                            <button class="btn btn-small p-size" id="back" type="button" value="{$previous_page}">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                {ci_language line="Manage Campaigns"}
                            </button>
                        </div>
                        {/if}
                        {if isset($next_page)}
                        <div class="inline pull-right">  
                            <button class="btn btn-small p-size" id="next" type="button" value="{$next_page}">
                                {if isset($next_sub_action)}
                                    {ci_language line="Ad Group Biddable Keywords"}
                                    <input type="hidden" value="{$next_sub_action}" id="next_sub_action">
                                {/if}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>                        
                        </div>
                        {/if}
                    </div>      
                </div>
            </div>

            <input type="hidden" id="archive-message" value="{ci_language line="Set the group status to archived successful"}" />
            <input type="hidden" id="confirm-archive-group-message" value="{ci_language line="Are you sure to set the group status to archived"}" />
            <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
            <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data"}" />
            <input type="hidden" id="please-try" value="{ci_language line="Please try again"}" />
            <input type="hidden" value="{ci_language line="Archive"}" id="Archive" />
            <input type="hidden" value="{ci_language line="Edit"}" id="Edit" />
            <input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
            <input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
            <input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
            <input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
            <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">   
            <input type="hidden" value="{ci_language line="AdGroup Id"}" id="id_title"> 
            <input type="hidden" value="adGroupCreation" id="addNewId"> 

            {include file="amazon/IncludeDataTableLanguage.tpl"}   

        </div> 

    {include file="amazon/PopupStepFooter.tpl"} 

    <div class="modal fade" id="groupDetails">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Edit AdGroup"}</h1>
                        </div>
                    </div>
                    <div id="groupDetail" class="m-t10"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="template" style="display:none">
        <div class="row">
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="AdGroup Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="adGroupId" name="adGroupId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Name"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="name" name="name" />
                        </div>
                    </div>
                </div>
            </div>  
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Campaign Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="campaignId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>           
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Start Date"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="startDate" name="startDate" />
                        </div>
                    </div>
                </div>
            </div>   
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="End Date"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" class="form-control" rel="endDate" name="endDate" />
                        </div>
                    </div>
                </div>
            </div>       
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="State"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-10 m-t5">
                            <label class="cb-radio">
                                <input type="radio" value="enabled" rel="state" name="state"/>
                                {ci_language line="Enabled"}
                            </label>
                            <label class="cb-radio m-l10">
                                <input type="radio" value="paused" rel="state" name="state"/>
                                {ci_language line="Paused"}
                            </label>
                            <label class="cb-radio m-l10">
                                <input type="radio" value="archived" rel="state" name="state"/>
                                {ci_language line="Archived"}
                            </label>
                        </div>
                    </div>
                </div>
            </div>  
           
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t20">
                    <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="save_group();" >
                        {ci_language line="Save"}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="adGroupCreation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Create New Groups"}</h1>                            
                        </div>
                    </div>
                    <div class="row custom-form">  
                        <div class="form-group col-md-12">
                            <div class="validate blue">
                                <div class="validateRow">
                                    <div class="validateCell">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="validateCell">
                                        <p class="pull-left">
                                            {ci_language line="Creates a new ad groups. Successfully created ad groups will be assigned unique adGroupIds"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>       
                                          
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="Name"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="name" />
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="The name of the ad group"}</p>
                            </div>
                        </div>  
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="Campaign Id"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <select class="search-select" name="campaignId">
                                            <option>--</option>
                                            {if isset($campaigns)}
                                                {foreach $campaigns as $campaign}
                                                    <option value="{$campaign['campaignId']}">{$campaign['campaignId']} ({$campaign['name']})</option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="The ID of the campaign to which this ad group belongs"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="Default Bid"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="defaultBid" />
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="The bid used when keywords belonging to this ad group don’t specify a bid (minimum 0.02)"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="State"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="cb-radio">
                                            </i>
                                            <input type="radio" value="enabled" name="state" checked="checked"/>
                                            {ci_language line="Enabled"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="paused" name="state"/>
                                            {ci_language line="Paused"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="archived" name="state"/>
                                            {ci_language line="Archived"}
                                        </label>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="Advertiser-specified state of the ad group"}</p>
                            </div>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="b-Top p-t20">
                                <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="create_group();" >
                                    {ci_language line="Create AdGroup"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</div> 
      
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/advertising.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}