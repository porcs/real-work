{if !isset($isCustomValue)}<div {if isset($isMain) && $isMain}{else}class="col-sm-3 p-r2"{/if}>{/if}   
    {*<pre>{print_r($select_options,true)}</pre>*}
    <select 
        id="{if isset($select_id)}{$select_id}{/if}" 
        name="{if isset($select_id)}{$select_id}{/if}" 
        {if !isset($select_rel)} rel="{if isset($select_id)}{$select_id}{/if}" {else} rel="{$select_rel}" {/if} 
        {if isset($select_cssClass)}{$select_cssClass}{/if}
        {if isset($select_disabled)}{$select_disabled}{/if}
        {if isset($select_data_content)} data-content="{$select_data_content}"{/if}
        >
        <option value="" {if isset($select_selected_non)}{$select_selected_non}{/if}> -- </option>
        {foreach from=$select_options item=option}
            <option value="{$option.value}" {if isset($option.selected)}{$option.selected}{/if} >{$option.desc}</option>
        {/foreach}
    </select>
{if !isset($isCustomValue)}</div>{/if}