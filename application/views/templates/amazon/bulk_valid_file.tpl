
{ci_config name="base_url"}
{include file="header.tpl"}
       
<div class="main-content">
    
    
    <div class="page-content">
            
        <div class="page-header position-relative">
            <h1>
                {ci_language line="Valid Value"}                
            </h1>
        </div>            
        
        {if isset($message)}<pre>{$message}</pre>{/if}
        <form name="" action="{$base_url}amazon/bulk_valid_value" method="POST" enctype="multipart/form-data">
            <div class="row-fluid">
                Product Type : 
                <input type="text" name="product_type"  />
            </div>
            <div class="row-fluid">
                ext. : 
                <select name="ext">
                    <option value="com" >com</option>
                    <option value="de" >de</option>
                    <option value="es" >es</option>
                    <option value="fr" >fr</option>
                    <option value="it" >it</option>
                    <option value="co_uk" >uk</option>
                </select>
            </div>
            <div class="row-fluid">
                File(csv) : 
                <input type="file" name="file_content"  multiple="multiple" />
            </div>
            <div class="row-fluid">
                <input type="submit" value="Upload" />
            </div>
        </form>
    </div><!--page-content-->
    
</div><!--main-content-->

{include file="footer.tpl"}          