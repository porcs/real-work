{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}  

<div class="row custom-form">
    <div class="col-xs-12">
        <div class="headSettings clearfix  b-Top-None">
            <div class="col-xs-6 clearfix">
                <div class="row">
                    <h4 class="p-0 headSettings_head">{ci_language line="by"} {ci_language line="Products"}</h4>
                </div>
            </div>
            <div class="col-xs-6 clearfix">
                <div class="showColumns clearfix">
                    <div class="" id="search-category">
                        <select id="category" class="search-select">
                            <option> - {ci_language line="Filter by category"} - </option>
                            {if isset($category)}{$category}{/if}
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <form method="post" id="product_options_form" autocomplete="off" >
                    <table class="responsive-table" id="product_options">
                        <thead class="table-head">	
                            <tr>
                                <th colspan="6">
                                    <div class="row">
                                        <div class="col-xs-7 m-t10">
                                            <a class="link" data-toggle="modal" data-target="#importProductStrategies" >
                                                <i class="fa fa-upload"></i>  {ci_language line="Import Product Strategies"}
                                            </a>

                                            <a class="link m-l20" data-toggle="modal" data-target="#DownloadProductStrategies" >
                                                <i class="fa fa-download"></i>  {ci_language line="Download Product Strategies"}
                                            </a>

                                            <div class="pull-right">
                                                <label ="cb-checkbox">
                                                    <input type="checkbox" id="apply_all" name="apply_all" value='1'> 
                                                        {ci_language line="Apply changes for all marketplaces"}
                                                </label>                                                
                                            </div>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="p-b0 m-b0 text-right">			                               
                                                <button type="button" class="btn btn-save m-b0" id="submit_product_strategies">
                                                    {ci_language line="Submit"}
                                                </button>
                                                <div class="status" style="display: none">		                                        	
                                                    <p class="p-t10 m-l5 p-b0 m-b0">
                                                        <i class="fa fa-spinner fa-spin"></i> 
                                                        <span>{ci_language line="Submiting"}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </th>								
                            </tr>
                            <tr>	
                                <th></th>							
                                <th></th>
                                <th class="text-left">{ci_language line="SKU"}</th>                           
                                <th class="text-center">{ci_language line="Minimum Price"}</th>
                                <th class="text-center">{ci_language line="Maximum Price"}</th>
                                <th></th>
                            </tr>
                        </thead>						 
                    </table>
                </form>
                <table class="table tableEmpty">
                    <tr>
                        <td>{ci_language line="No data available in table"}</td>
                    </tr>
                </table>					 
            </div>
        </div>		
    </div>			
</div>

<!-- Main child table -->
<div id="model" style="display:none">
    <div class="row-edit p-10 p-l40 custom-form">
        <table cellpadding="5" cellspacing="5" border="0" class="table_detail" >
            <!--Hidden Input-->
            <tr style="display:none">
                <td colspan="3">
                    <input type="hidden" class="id_product form-control" rel="id_product" />
                    <input type="hidden" class="id_product_attribute form-control" rel="id_product_attribute" />
                    <input type="hidden" class="sku form-control" rel="sku" />               			
                    <input type="hidden" class="parent_sku form-control" rel="parent_sku" />
                </td>
            </tr>	           
            <!-- Price Override -->
            <tr>
                <td width="5%"></td>
                <td width="20%"><p class="regRoboto poor-gray p-l5">{ci_language line="Minimum Price"} : </p></td>
                <td width="50%">
                    <div class="row clearfix m-b10">
                        <div class="col-xs-4">	     
                            <div class="clearfix">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <strong>{$currency_sign}</strong>
                                    </span>
                                    <input type="text" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" class="form-control" rel="minimum_price" />
                                </div> 
                            </div> 
                        </div>
                        <div class="col-xs-8">
                            <p class="m-t5 poor-grey help-text m-0">
                                ({ci_language line="on regular price"})
                            </p>
                            <p class="price-error p-0 m-0" style="display:none">
                                <i class="fa fa-exclamation-circle"></i>
                                <span>{ci_language line="A value must be a Number"}</span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><p class="regRoboto poor-gray p-l5">{ci_language line="Maximun Price"} : </p></td>
                <td>
                    <div class="row clearfix m-b10">
                        <div class="col-xs-4">       
                            <div class="clearfix">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <strong>{$currency_sign}</strong>
                                    </span>
                                    <input type="text" onkeyup="this.value = this.value.replace(/[^0-9.]/g, '')" class="form-control" rel="target_price" />
                                </div> 
                            </div> 
                        </div>
                        <div class="col-xs-8">
                            <p class="m-t5 poor-grey help-text m-0">
                                ({ci_language line="on regular price"})
                            </p>
                            <p class="price-error p-0 m-0" style="display:none">
                                <i class="fa fa-exclamation-circle"></i>
                                <span>{ci_language line="A value must be a Number"}</span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr> 
        </table>
    </div>
</div>

{include file="amazon/PopupStepFooter.tpl"}

<div class="modal fade" id="importProductStrategies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                            {ci_language line="Import Product Strategies"}
                        </h1>
                    </div>
                </div>
                <div class="row m-t10">	
                    <form class="upload-template" method="POST" enctype="multipart/form-data" >	
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-file-o"></i> {ci_language line="Browse"}&hellip; <input type="file" class="form-control">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <div class="progressbar col-md-12 p-t35 hidden">
                                <div class="relative">
                                    <div class="progressCondition">
                                        <div class="progressLine" data-value="5"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-save m-b0" id="upload_product_strategies" >
                                <i class="fa fa-upload"></i> {ci_language line="Upload File"}
                            </button>
                            <a href="{$base_url}/amazon/product_strategies_download_template" type="button" class="btn btn-default m-l5 download_template" target="_blank">	
                                <i class="fa fa-download"></i> {ci_language line="Download Template"}
                            </a>
                            <div class="status" style="display: none">		                                        	
                                <p class="p-t10 m-l5 p-b0 m-b0">
                                    <i class="fa fa-spinner fa-spin"></i> 
                                    <span>{ci_language line="Starting upload"}</span>
                                </p>
                            </div>
                        </div>
                    </form>					
                </div>	
                <div id="result_product" style="display:none">
                    <div id="product_summary" class="row">
                        <div class="col-xs-12">
                            <p class="head_text">{ci_language line="Summary"}</p>
                            <div class="col-xs-4">
                                <p>{ci_language line="Upload"} : <span id="product_summary_upload"></span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-xs-4">
                                <p>{ci_language line="Success"} : <span id="product_summary_success"></span> {ci_language line="product(s)"}</p>
                            </div>
                            <div class="col-xs-4">
                                <p>{ci_language line="Error"} : <span id="product_summary_error"></span> {ci_language line="product(s)"}</p>
                            </div>
                        </div>
                    </div>
                </div>		
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="DownloadProductStrategies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">
                            {ci_language line="Download Product Strategies"}
                        </h1>
                    </div>
                </div>
                <div class="m-t10 custom-form">	
                    <form action="{$base_url}/amazon/product_strategies_download/{$id_country}" id="download-product-options" method="POST" enctype="multipart/form-data" >	

                        <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Option"} :</p>

                        <div class="row">
                            <div class="col-xs-12">		
                                <div class="form-group"> 
                                    <div class="col-md-2">
                                        <label class="cb-checkbox">
                                            <input type="checkbox" id="only_active" name="only_active" value='1'> {ci_language line="Only Active"}
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="cb-checkbox">
                                            <input type="checkbox" id="only_in_stock" name="only_in_stock" value='1'> {ci_language line="Only in Stock"}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">  
                            <div class="col-xs-8">  
                                <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Filter by category"} : </p> 
                                <div id="download_product_options_categories">
                                    <div class="form-group withIcon clearfix">  
                                        <div class="col-xs-12">
                                            <select class="search-select" name="category[]">
                                                    <option value=''> - - </option>
                                                {if isset($category)}{$category}{/if}
                                            </select>
                                            <i class="cb-plus good" id="download_product_options_category"></i>
                                            <i class="cb-plus bad" style="display: none"></i>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="text-right m-t15 b-Top p-t10">	
                                <button type="submit" class="btn btn-save m-b0" id="download_product_options" >{ci_language line="Submit"}</button>
                                <div class="status" style="display: none">		                                        	
                                    <p class="p-t10 m-l5 p-b0 m-b0">
                                        <i class="fa fa-spinner fa-spin"></i> 
                                        <span>{ci_language line="Starting upload"}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>					
            </div>						
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
</div>

<input type="hidden" id="shop_name" value="{if isset($shop_name)}{$shop_name}{/if}" />
<input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="l_Error" value="{ci_language line="Error"}">
<input type="hidden" id="l_Success" value="{ci_language line="Success"}">
<input type="hidden" id="l_Submit" value="{ci_language line="Submiting"}">
<input type="hidden" id="l_Edit" value="{ci_language line="Edit"}">
<input type="hidden" id="l_Reset" value="{ci_language line="reset"}">
<input type="hidden" id="l_Mismatch" value="{ci_language line="Item unactivated: Multiple edition is possible only for matching values"}">
<input type="hidden" id="reset-confirm" value="{ci_language line="Do you want to reset "}">
<input type="hidden" id="l_upload_fail" value="{ci_language line="upload fail, please try again"}">
<input type="hidden" id="l_please_check" value="{ci_language line="Your file have many error please check"}">

{include file="IncludeDataTableLanguage.tpl"}

<!--inline scripts related to this page-->
<script src="{$cdn_url}assets/js/jquery.ajax-progress.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/product_strategies.js"></script>

{include file="footer.tpl"}
