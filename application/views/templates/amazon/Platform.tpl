{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}    

<div class="row">
    <div class="col-xs-12">
        <div class="headSettings clearfix b-Bottom p-b10">
            <div class="pull-md-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Amazon Site"}</h4>                      
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="validate blue m-t20">
            <div class="validateRow">
                <div class="validateCell">
                    <i class="note"></i>
                </div>
                <div class="validateCell">
                    <ul>
                        {if isset($mode) && $mode == 1}
                            <li>
                                <p>
                                    {ci_language line="amazon_wizard_instruction_mode_1"}
                                </p>
                            </li>                       
                        {elseif isset($mode) && $mode == 2}  
                            <li>
                                <p>
                                    {ci_language line="amazon_wizard_instruction_mode_2"}
                                </p>
                            </li>
                        {/if}
                        <li>
                            <p>
                                {ci_language line="This operation is automatic but could take one hour, please be patient and wait till the process is completed."}
                            </p>
                        </li>
                    </ul>
                    <i class="fa fa-remove pull-right"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-t10">
    <div class="col-xs-12">
         {if isset($submenu)}
            <ul class="selectWizard clearfix">
                {foreach from=$submenu item=menu}
                    <li class="selectWizard_point">
                        {$ext = ltrim($menu.ext, ".")}
                        <a href="{$base_url}amazon/parameters/{$menu.id}/{$popup}/{$mode}" title="{$menu.country}" class="link p-size">
                            <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", $ext)}.png" />
                            <p class="poor-gray">{$menu.country}</p>
                        </a>
                    </li>
                {/foreach}
            </ul>
        {/if}           
    </div>
</div>
            
{include file="amazon/PopupStepFooter.tpl"}
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"} 