{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
{include file="amazon/PopupStepMoreHeader.tpl"} 

{if isset($admin_remote) && $admin_remote}


	    <div class="row">
	        <div class="col-xs-12">
	            <p class="head_text p-tb10 m-b10">
	                 {ci_language line="Debugs"} 
	            </p>
	        </div>
	    </div>    

	    <div class="row m-t10">
		
			<div class="col-xs-12">
			    <p class="montserrat text-info text-uc m-b20">{ci_language line="Products Creations"}</p>  
			    
			    <form id="amazon_create" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_create}" enctype="multipart/form-data" class="custom-form" >
					<div class="col-sm-6 col-md-4 col-lg-3"> 
					    <p class="poor-gray">{ci_language line="Limit product(s)"} :</p> 
					    <div class="form-group">
						<input type="text" id="limiter" name="limiter" class="form-control" />			
					    </div> 
					</div>
					<div class="col-sm-6 col-md-4 col-lg-3"> 
					    <p class="poor-gray">{ci_language line="Product ID"} :</p> 
					    <div class="form-group">
						<input type="text" id="id_product" name="id_product" class="form-control" />			
					    </div> 
					</div>
					<div class="col-sm-6 col-md-4 col-lg-3"> 		    
					    <div class="form-group p-t25">
							<button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					    </div> 
					</div>	

					<div class="col-sm-12 col-md-12 col-lg-12 m-b15"> 
					    <p class="poor-gray">{ci_language line="Options"} :</p> 
					    <div class="form-group">
					    	<div class="col-md-3"> 
								<label class="cb-checkbox w-100">
									<input type="checkbox" name="exclude_manufacturer" value="1" style="opacity: 0;" checked="checked" />
									<p class="poor-gray m-b0">{ci_language line="Exclude manufacturer"}.</p>
							    </label>	
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="exclude_supplier" value="1" style="opacity: 0;" checked="checked" />
									<p class="poor-gray m-b0">{ci_language line="Exclude supplier"}.</p>
							    </label>
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="only_selected_carrier" value="1" style="opacity: 0;" checked="checked" />
									<p class="poor-gray m-b0">{ci_language line="Only selected carrier"}.</p>
							    </label>
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="send_image" value="1" style="opacity: 0;" checked="checked" />
									<p class="poor-gray m-b0">{ci_language line="Include image"}.</p>
							    </label>
						    </div>
						    <div class="col-md-3"> 
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="send_relation_ship" value="1" style="opacity: 0;"/>
									<p class="poor-gray m-b0">{ci_language line="Send only relationship"}.</p>
							    </label>	
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="send_only_image" value="1" style="opacity: 0;"/>
									<p class="poor-gray m-b0">{ci_language line="Send only image"}.</p>
							    </label>
							    <label class="cb-checkbox w-100">
									<input type="checkbox" name="send_shipping_override" value="1" style="opacity: 0;"/>
									<p class="poor-gray m-b0">{ci_language line="Send only shipping override"}.</p>
							    </label>
						    </div>
					    </div> 
					</div>
					   
			    </form> 
			    
		        </div><!--amazon_creats-->
			
			<div class="col-xs-12">
			    <div class="p-t25 b-Top" >
				<p class="montserrat text-info text-uc m-b20">{ci_language line="Update Offers"}</p>

				<form id="amazon_sync" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_sync}" enctype="multipart/form-data" class="custom-form" >
				    <div class="col-sm-6 col-md-4 col-lg-3"> 
					<p class="poor-gray">{ci_language line="Update type"} :</p> 
					<div class="form-group">
					    <select id="update_type" name="update_type"  class="search-select" >
						<option value="offer" selected="">{ci_language line="Offer"}</option>
						<option value="product">{ci_language line="Product"}</option>
						<option value="all">{ci_language line="All"}</option>
					    </select>
					</div> 
				    </div>
				    <div class="col-sm-6 col-md-4 col-lg-3"> 
					<p class="poor-gray">{ci_language line="Cron"} :</p> 
					<div class="form-group">
					    <label class="cb-checkbox w-100">
						<input type="checkbox" name="cron" value="1" style="opacity: 0;" />
						<p class="dark-gray montserrat m-b0">{ci_language line="Yes"}.</p>
					    </label>			
					</div> 
				    </div>
				    <div class="col-sm-6 col-md-4 col-lg-3"> 		    
					<div class="form-group p-t25">
					    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					</div> 
				    </div>		   
				</form> 
			    </div>
				    
		        </div><!--amazon_sync-->
				
			<div class="col-xs-12">
			    <div class="p-t25 b-Top" >
				 
					<p class="montserrat text-info text-uc m-b20">{ci_language line="Delete"}</p>

					<form id="amazon_delete" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_delete}" enctype="multipart/form-data" class="custom-form" >
					    <div class="col-md-6"> 
							<label class="cb-radio" style="display:inline">
							    <input name="delete" type="radio" id="amazon_delete_disabled" value="{$amazon_delete_disabled}" checked="checked">
							    <p class="poor-gray">{ci_language line="Disabled products"}</p>			    
							</label>
						   
							<label class="cb-radio" style="display:inline">
							    <input name="delete" type="radio" id="out_of_stock" value="{$amazon_delete_out_of_stock}">
							    <p class="poor-gray">{ci_language line="Out of stock products"}</p>			    
							</label>
						   
							<label class="cb-radio" style="display:inline">
							    <input name="delete" type="radio" id="all_sent" value="{$amazon_delete_all_sent}">
							    <p class="poor-gray">{ci_language line="All sent products"}</p>
							   
							</label>
						    
							<label class="cb-radio" style="display:inline">
							    <input name="delete" type="radio" id="inactive" value="{$amazon_delete_inactive}">
							    <p class="poor-gray">{ci_language line="Inactive"}</p>			   
							</label>
					    </div>

					    <div class="col-md-6"> 		    
							<div class="form-group">
							    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
							</div> 
					    </div>		   
					</form> 
			    </div>
			    
		        </div><!--amazon_delete-->
			
			<div class="col-xs-12">
			    
			    <div class="p-t25 b-Top" >
				 
				<p class="montserrat text-info text-uc m-b20">{ci_language line="Get orders"}</p>

				<form id="amazon_get_orders" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_get_orders}" enctype="multipart/form-data" class="custom-form" >
					
				    <div class="col-md-6">
					<p class="poor-gray">{ci_language line='Order Date Range'} (DD/MM/YYYY)</p>
					<div class="row">
					    <div class="col-sm-6">
						<div class="form-group calendar firstDate">
						    <input class="form-control order-date" type="text" id="datefrom" name="datefrom" value="{if isset($yesterday)}{$yesterday}{/if}">
						</div>
					    </div>
					    <div class="col-sm-6">
						<div class="form-group calendar lastDate">
						    <input class="form-control order-date" type="text" id="dateto" name="dateto"  value="{if isset($today)}{$today}{/if}">
						</div>
					    </div>
					</div>
				    </div>			
				    <div class="col-md-3">			
					<p class="poor-gray">{ci_language line='Order Status'}</p>
					<div class="form-group">
					    <select id="type" name="type" class="search-select" >			   
						<option value="All">{ci_language line='Retrieve all pending orders'}</option>
						<option value="Pending">{ci_language line='Pending - This order is pending in the market place'}</option>
						<option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order is waiting to be shipped'}</option>
						<option value="PartiallyShipped">{ci_language line='Partially shipped - This order was partially shipped'}</option>
						<option value="Shipped">{ci_language line='Shipped - This order was shipped'}</option>
						<option value="Canceled">{ci_language line='Cancelled - This order was cancelled'}</option>
					    </select>
					</div> 
				    </div>
				    <div class="col-md-3"> 		    
					<div class="form-group p-t25">
					    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					</div> 
				    </div>		   
				</form> 
			    </div>
		        </div><!--get_orders-->	
			
			<div class="col-xs-12">
			    
			    <div class="p-t25 b-Top" >
				
				<p class="montserrat text-info text-uc m-b20">{ci_language line="Get Reports"}</p>
							       
				<form id="amazon_reports" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_reports}" enctype="multipart/form-data" class="custom-form" >
					<div class="col-md-6">
						<p class="poor-gray">{ci_language line='Report Date Range'} (DD/MM/YYYY)</p>
						<div class="row">
						    <div class="col-sm-6">
							<div class="form-group calendar firstDate">
							    <input class="form-control report-date" type="text" id="datefrom" name="datefrom" value="{if isset($yesterday)}{$yesterday}{/if}">
							</div>
						    </div>
						    <div class="col-sm-6">
							<div class="form-group calendar lastDate">
							    <input class="form-control report-date" type="text" id="dateto" name="dateto"  value="{if isset($today)}{$today}{/if}">
							</div>
						    </div>
						</div>
					</div>
				   	<div class="col-md-3">	
				   		<p class="poor-gray">{ci_language line='Report Type'}</p>	
				    	<select id="report_mode" name="report_mode" class="search-select" >
							<option value="1" selected="">{ci_language line="Inventory Report"}</option>
							<option value="2">{ci_language line="Fba Estimated Report"}</option>
					    </select>
				    </div>
				    <div class="col-md-3">		    
					<div class="form-group p-t25">
					    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					</div> 
				    </div>		   
				</form> 
					
			    </div>
					
		        </div><!--reports-->
			
			<div class="col-xs-12">
			    
			    <div class="p-t25 b-Top" >
				
				<p class="montserrat text-info text-uc m-b20">{ci_language line="Shipping Group Name"}</p>
							       
				<form id="amazon_reports" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_shipping_group}" enctype="multipart/form-data" class="custom-form" >					
				    <div class="col-md-3">		    
					<div class="form-group">
					    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					</div> 
				    </div>		   
				</form> 
					
			    </div>
					
	        </div><!--reports-->

			<div class="col-xs-12">
			    <div class="p-t25 b-Top" >
				<p class="montserrat text-info text-uc m-b20">{ci_language line="Update Orders"}</p>
				
				<form id="amazon_update_order" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_update_order}" enctype="multipart/form-data" class="custom-form" >
					
				    <div class="col-md-3"> 		    
					<div class="form-group">
					    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
					</div> 
				    </div>		   
				</form> 
					
			    </div>
		        </div><!--update_order-->

		    <div class="col-xs-12">
			    <div class="p-t25 b-Top" >
					<p class="montserrat text-info text-uc m-b20">{ci_language line="Re-pricing"}</p>
					
					<form id="amazon_repricing" method="post" action="{$base_url}amazon/debugs/{$id_country}/{$amazon_repricing}" enctype="multipart/form-data" class="custom-form" >
					    <div class="col-md-3">			
							<p class="poor-gray">{ci_language line='Repricing Type'}</p>
							<div class="form-group">
							    <select id="type" name="type" class="search-select" >			   
									<option value="{$amazon_repricing_analyst}" selected="">{ci_language line='Repricing - Analyst'}</option>
									<option value="{$amazon_repricing_export}">{ci_language line='Repricing - Export'}</option>
							    </select>
							</div> 
					    </div>
						<div class="col-sm-3">
							<p class="poor-gray">{ci_language line='File name'}</p>
							<div class="form-group">
							    <input class="form-control" type="text" id="asin" name="asin" />
							</div>
					    </div>
					    <div class="col-md-3"> 		    
							<div class="form-group p-t25">
							    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
							</div> 
					    </div>		   
					</form> 
						
		        </div><!--update_order-->
		    </div>

		    <div class="col-xs-12">
			    <div class="p-t25 b-Top" >
					<p class="montserrat text-info text-uc m-b20">{ci_language line="Trigger Reprice"}</p>
					
					<form id="amazon_trigger_reprice" method="post" action="{$base_url}amazon/debugs/{$id_country}/trigger_reprice" enctype="multipart/form-data" class="custom-form" >
						<div class="col-sm-6" id="amazon_trigger_reprice_div">
							<p class="poor-gray">{ci_language line='SKU'}</p>
							<div class="form-group row">
								<div class="col-xs-10">
							    	<input class="form-control" type="text" name="sku[]" />
						    	</div>
							    <i class="cb-plus good amazon_trigger_reprice_addSku"></i>
								<i class="cb-plus bad amazon_trigger_reprice_removeSku" style="display: none"></i> 
							</div>
					    </div>
					    <div class="col-md-6"> 		    
							<div class="form-group p-t25">
							    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
							</div> 
					    </div>		   
					</form> 
						
		        </div><!--Trigger Reprice-->
		    </div>
		    

		    <div class="col-xs-12">
			    <div class="p-t25 b-Top" >
					<p class="montserrat text-info text-uc m-b20">{ci_language line="Set Prefix"}</p>
					
					<form id="amazon_PrefixTable" method="post" action="{$base_url}amazon/debugs/{$id_country}/PrefixTable" enctype="multipart/form-data" class="custom-form" >
						<div class="col-sm-3">
							<p class="poor-gray">{ci_language line='Old Prefix'}</p>
							<div class="form-group">
							    <input class="form-control" type="text" id="old_prefix" name="old_prefix" />
							</div>
					    </div>
					    <div class="col-sm-3">
							<p class="poor-gray">{ci_language line='New Prefix'}</p>
							<div class="form-group">
							    <input class="form-control" type="text" id="new_prefix" name="new_prefix" />
							</div>
					    </div>
					    <div class="col-md-3"> 		    
							<div class="form-group p-t25">
							    <button type="submit" class="btn btn-save">{ci_language line="Submit"}</button>			
							</div> 
					    </div>		   
					</form> 
						
		        </div><!--update_order-->
		    </div>

	    </div>

	    <div class="p-t25 m-t25" ></div>
	           
{else}
		 <div class="row">
	        <div class="col-xs-12">
	            <p class="head_text p-tb10 m-b10">
	                 {ci_language line="Debugs"} 
	            </p>
	        </div>
	    </div> 
{/if}

{include file="amazon/PopupStepFooter.tpl"}  

{include file="amazon/IncludeScript.tpl"}

{include file="footer.tpl"}