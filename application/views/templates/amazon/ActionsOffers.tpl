{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
    
{include file="amazon/PopupStepHeader.tpl"}

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#synchronize" data-toggle="tab" class="">
                    <i class="icon-syn"></i> <span>{ci_language line="Offers"}</span>
                </a>
            </li>                        
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="tab-content">
            
            <!--Synchronization-->
            <div id="synchronize" class="tab-pane active">
                <div class="row">
                    <div class="col-sm-10">
                        
                        <ul class="m-t15 p-l20">
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Offers Wizard"}</b></p>
                                <p class="poor-gray">
                                    {ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.'}
                                </p>
                            </li>
                            <li>
                                <p class="dark-gray"><b>{ci_language line="Update offers"}</b></p>
                                <p class="poor-gray">
                                    {ci_language line='This will update Amazon depending your offers, within the selected categories in your configuration.'}
                                </p>
                            </li>
                        </ul>                            
                        <div id="sync_options">
                            {if isset($message_code_inner.sync) && (isset($mode_default) && $mode_default > 1)}
                                <div class="m-t20">
                                    {$message_code_inner.sync}
                                </div>
                            {/if}
                            <input type="hidden" id="sync" value="update_offer" />
                            
                            <div class="clearfix m-t20 xs-center row">
                                <div class="col-xs-7 text-left">
                                    <button class="btn btn-wizard wizard_btn m-r5" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$sync_mode}" id="amazon_synchronize_wizard">
                                        <i class="icon-wizard"></i> {ci_language line="Offers Wizard"}
                                    </button>
                                </div>
                                                               
                                <div class="col-xs-5 text-right">
                                    <button class="btn btn-amazon m-r5" id="send-offers" type="button" value="synchronize">
                                        <i class="icon-amazon"></i> {ci_language line="Update offers"}
                                    </button>                                        
                                    <div class="status" style="display: none">
                                        <p>{ci_language line="Starting update"} ..</p>
                                    </div>
                                    <div class="error" style="display: none">
                                        <p class="m-t0">
                                            <i class="fa fa-exclamation-circle"></i>
                                            <span></span>
                                        </p>
                                    </div>
                                </div>

                            </div>                        
                                
                        </div>      
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
                
{include file="amazon/PopupStepFooter.tpl"}  
<input type="hidden" value="{ci_language line="You have just update your offers on other region. Please wait until the process complete."}" id="offer_running" />
<input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
<input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
<input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
<input type="hidden" value="" id="offer_sending" />
{if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
{if isset($country)}<input type="hidden" id="country" value="{$country}" />{/if}    
{if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 

<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/actions.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}