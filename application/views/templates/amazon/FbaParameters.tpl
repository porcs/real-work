{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
<form id="form-fba-setting" method="post" action="{$base_url}amazon/fba/save_fba/{$id_country}" enctype="multipart/form-data" autocomplete="off" >
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 

    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-tb10 m-b10">
                 {ci_language line="Fulfillment By Amazon"}
            </p>
        </div>
    </div>    

    <div class="row">
        <div class="col-xs-12">
            <div class="m-t10 custom-form">
                
                <p class="montserrat dark-gray text-uc">
                    {ci_language line="Amazon"} {if isset($region)}{$region}{/if}
                </p>
                 
                <div class="col-sm-6">
                     <label class="amazon-checkbox w-100 {if isset($fba.fba_master_platform) && $fba.fba_master_platform == 1}checked{/if}">
                        <span class="amazon-inner">
                            <i>
                                <input id="fba_master_platform" name="fba_master_platform" type="checkbox" value="1" {if isset($fba.fba_master_platform) && $fba.fba_master_platform == 1}checked{/if} />
                            </i>
                        </span>
                        <p class="montserrat dark-gray text-uc m-b0">
                            {ci_language line="FBA"} {ci_language line="Master Platform"}                            
                        </p>
                        <p class="m-l23 m-b0">
                            {ci_language line="Set"} {ci_language line="Amazon"}{$ext} {ci_language line="to FBA Master Platform for Amzon"} {if isset($region)}{$region}{/if}
                           
                        </p>
                    </label>  

                    <p id="fba_set_success" class="p-l23 true-green m-l0 m-r10" style="display :none"> 
                        <i class="fa fa-check"></i> {ci_language line="Save successful."}
                    </p>
                    <p id="fba_remove_success"  class="p-l23 true-green m-l0 m-r10" style="display :none"> 
                        <i class="fa fa-check"></i> {ci_language line="Save successful."}
                    </p>
                </div>   

                <div class="col-sm-6 m-t10">
                    {if isset($master_platform) && !empty($master_platform)}
                        <p class="master_platform true-green"> 
                            {ci_language line="Master Platform"} : {$master_platform}
                        </p>
                    {else}  
                        <p class="master_platform true-pink">
                            {ci_language line="No master platform"}
                        </p>
                    {/if}
                </div>

            </div>          
        </div>       
    </div> 

    <div class="row">
        <div class="col-xs-12">

            <p class="head_text m-t25">
                {ci_language line="Configuration"}
            </p>

            <div class="row m-t20">

                <div class="col-sm-12 custom-form">
                  <!--Behaviour-->
                    <p class="text-uc dark-gray montserrat m-b10">{ci_language line="Behaviour"} :</p>
                    <div class="form-group">
                        <label class="cb-radio w-100 {if isset($fba.fba_stock_behaviour) && $fba.fba_stock_behaviour == 1}checked{/if} ">
                            <input type="radio" name="fba_stock_behaviour" value="1" {if isset($fba.fba_stock_behaviour) && $fba.fba_stock_behaviour == 1}checked{/if} /> 
                            {ci_language line="Use Amazon FBA stock first, then switch to your own stock (AFN/MFN auto switching)"}
                        </label>
                        <label class="cb-radio w-100 {if isset($fba.fba_stock_behaviour) && $fba.fba_stock_behaviour == 2}checked{/if} ">
                            <input type="radio" name="fba_stock_behaviour" value="2" {if isset($fba.fba_stock_behaviour) && $fba.fba_stock_behaviour == 2}checked{/if} /> 
                            {ci_language line="Synchronize stocks from Amazon FBA, your shop's stock is overrode"}
                        </label>                        
                    </div>

                </div>

            </div>

            <hr class="m-t5 m-b5"/>

            <div class="row m-t20">

                <div class="col-sm-6 custom-form">

                    <!--Price Value-->
                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="FBA Price Value"} :</p>
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="m-t10 fba_estimated_fees"> 
                                <label class="amazon-checkbox w-100 {if isset($fba.fba_estimated_fees) && $fba.fba_estimated_fees}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input rel="fba_estimated_fees" type="checkbox" value="1" onclick="show_percentage(this)" {if isset($fba.fba_estimated_fees) && $fba.fba_estimated_fees}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="m-b0">
                                         {ci_language line="Estimated fees."}                          
                                    </p>
                                </label>  
                                <div class="m-tb5 p-l23 fees_value" {if !isset($fba.fba_estimated_fees) || empty($fba.fba_estimated_fees)}style="display:none;"{/if}> 
                                    <div class="input-group col-xs-10 p-0 m-0"> 
                                        <span class="input-group-addon">
                                            <strong>%</strong>
                                        </span> 
                                        <select class="search-select" name="fba_estimated_fees">    
                                            <option value="" selected >--</option>                                         
                                            {for $i=(-100); $i<= 100; $i++}
                                                <option value="{$i}" {if isset($fba.fba_estimated_fees) && $fba.fba_estimated_fees == $i}selected{elseif !isset($fba.fba_estimated_fees) && $i == 100}selected{/if} >{$i}</option>
                                            {/for}
                                        </select> 
                                    </div>  
                                </div>  
                                <p class="regRoboto poor-gray text-left m-l23">
                                    {ci_language line="Apply a percentage of estimated fees for FBA products."}                                   
                                </p> 
                            </div>      
                           
                            <div class="m-t10 fba_fulfillment_fees"> 
                                <label class="amazon-checkbox w-100 {if isset($fba.fba_fulfillment_fees) && $fba.fba_fulfillment_fees}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input rel="fba_fulfillment_fees" type="checkbox" value="1" onclick="show_percentage(this)" {if isset($fba.fba_fulfillment_fees) && $fba.fba_fulfillment_fees}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="m-b0">
                                         {ci_language line="Fulfillment fees."}                          
                                    </p>
                                </label>                               
                                <div class="m-tb5 p-l22 fees_value"  {if !isset($fba.fba_fulfillment_fees) || empty($fba.fba_fulfillment_fees)}style="display:none;"{/if}> 
                                    <div class="input-group col-xs-10 p-0 m-0"> 
                                        <span class="input-group-addon">
                                            <strong>%</strong>
                                        </span> 
                                        <select class="search-select" name="fba_fulfillment_fees">          
                                            <option value="" selected >--</option>                             
                                            {for $i=(-100); $i<= 100; $i++}
                                                <option value="{$i}" {if isset($fba.fba_fulfillment_fees) && $fba.fba_fulfillment_fees == $i}selected{elseif !isset($fba.fba_fulfillment_fees) && $i == 100}selected{/if} >{$i}</option>
                                            {/for}
                                        </select> 
                                    </div>   
                                </div>  
                                <p class="regRoboto poor-gray text-left m-l23">
                                    {ci_language line="Apply a percentage of fulfillment fees for FBA products."}                                   
                                </p>
                            </div>
                        </div>
                    </div>

                    <hr class="m-t5"/>

                    <!--FBA Default Price Value-->
                    <p class="text-uc dark-gray montserrat m-b0">{ci_language line="FBA Default Price Value"} :</p>
                    <div class="row custom-form m-b10">

                        <div class="col-sm-12">
                            <div class="row"> 
                                <div class="m-t10 default_fba_fees col-sm-3"> 
                                    <input name="default_fba_fees" class="form-control" type="text" {if isset($fba.default_fba_fees)}value="{$fba.default_fba_fees}"{/if} />
                                </div>  
                                <p class="m-t5 regRoboto poor-gray text-left col-sm-12">
                                    {ci_language line="Default fees when estimated fees are not yet available."}                                  
                                </p> 
                            </div> 
                        </div> 
                            
                        {*<div class="col-sm-12">
                            <div class="m-t10 default_fba_estimated_fees"> 
                                <label class="amazon-checkbox w-100 {if isset($fba.default_fba_estimated_fees) && $fba.default_fba_estimated_fees}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input rel="default_fba_estimated_fees" type="checkbox" value="1" onclick="show_percentage(this)" {if isset($fba.default_fba_estimated_fees) && $fba.default_fba_estimated_fees}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="m-b0">
                                         {ci_language line="Default Estimated fees."}                          
                                    </p>
                                </label>  
                                <div class="m-tb5 p-l23 fees_value" {if !isset($fba.default_fba_estimated_fees) || empty($fba.default_fba_estimated_fees)}style="display:none;"{/if}> 
                                    <div class="input-group col-xs-10 p-0 m-0"> 
                                        <span class="input-group-addon">
                                            <strong>%</strong>
                                        </span> 
                                        <select class="search-select" name="default_fba_estimated_fees">    
                                            <option value="" selected >--</option>                                         
                                            {for $i=(-100); $i<= 100; $i++}
                                                <option value="{$i}" {if isset($fba.default_fba_estimated_fees) && $fba.default_fba_estimated_fees == $i}selected{elseif !isset($fba.default_fba_estimated_fees) && $i == 100}selected{/if} >{$i}</option>
                                            {/for}
                                        </select> 
                                    </div>  
                                </div>  
                                <p class="regRoboto poor-gray text-left m-l23">
                                    {ci_language line="Default fees when estimated fees are not yet available. A percentage of product price."}                                  
                                </p> 
                            </div>      
                           
                            <div class="m-t10 default_fba_fulfillment_fees"> 
                                <label class="amazon-checkbox w-100 {if isset($fba.default_fba_fulfillment_fees) && $fba.default_fba_fulfillment_fees}checked{/if}">
                                    <span class="amazon-inner">
                                        <i>
                                            <input rel="default_fba_fulfillment_fees" type="checkbox" value="1" onclick="show_percentage(this)" {if isset($fba.default_fba_fulfillment_fees) && $fba.default_fba_fulfillment_fees}checked{/if} />
                                        </i>
                                    </span>
                                    <p class="m-b0">
                                         {ci_language line="Default Fulfillment fees."}                          
                                    </p>
                                </label>                               
                                <div class="m-tb5 p-l22 fees_value"  {if !isset($fba.default_fba_fulfillment_fees) || empty($fba.default_fba_fulfillment_fees)}style="display:none;"{/if}> 
                                    <div class="input-group col-xs-10 p-0 m-0"> 
                                        <span class="input-group-addon">
                                            <strong>%</strong>
                                        </span> 
                                        <select class="search-select" name="default_fba_fulfillment_fees">          
                                            <option value="" selected >--</option>                             
                                            {for $i=(-100); $i<= 100; $i++}
                                                <option value="{$i}" {if isset($fba.default_fba_fulfillment_fees) && $fba.default_fba_fulfillment_fees == $i}selected{elseif !isset($fba.default_fba_fulfillment_fees) && $i == 100}selected{/if} >{$i}</option>
                                            {/for}
                                        </select> 
                                    </div>   
                                </div>  
                                <p class="regRoboto poor-gray text-left m-l23">
                                     {ci_language line="Default fulfillment fees when estimated fees are not yet available. A percentage of product price."}                                  
                                </p>
                            </div>
                        </div>*}

                    </div>  

                    <hr class="m-t5"/>

                    <!--Price Formula-->
                    <p class="text-uc dark-gray montserrat m-b5 p-t0">{ci_language line="FBA Price Formula"} :</p>
                    <div class="row">
                        <div class="form-group m-b10">
                            <div class="col-xs-10 m-t10">                                 
                                <div class="form-group clearfix m-b0">                                    
                                    <div class="input-group col-xs-10 p-0"> 
                                        <span class="input-group-addon">
                                            <strong>%</strong>
                                        </span> 
                                        <select class="search-select" name="fba_formula" id="fba_formula" >                                            
                                            {for $i=(-100); $i<= 100; $i++}
                                                <option value="{$i}" {if isset($fba.fba_formula) && $fba.fba_formula == $i}selected{/if} >{$i}</option>
                                            {/for}
                                        </select> 
                                    </div>                                
                                </div>        
                            </div>
                            <div class="col-xs-12 m--t10">                    
                                <p class="regRoboto poor-gray text-left">{ci_language line="Apply a specific default price formula for FBA products."} {*ci_language line="eg: @ + 10 %"}</p>
                                <p class="regRoboto poor-gray text-left">{ci_language line="This formula could be a price a value or math (multiply, divide, addition, subtraction, percentages)."*}</p>                 
                            </div>
                        </div>
                    </div>
                    
                    <hr class="m-t5">

                    <p class="text-uc dark-gray montserrat m-b5">{ci_language line="Default Shipping Template"} :</p>
                    <div class="row">
                        <div class="form-group m-b10">
                            <div class="col-xs-10 m-t10">                                 
                                <div class="form-group clearfix m-b0">                                    
                                    <div class="col-xs-10 p-0"> 
                                        <select class="search-select" name="default_shipping_template" id="default_shipping_template" >
                                            <option value="">--</option>
                                            {if isset($shipping_groups)}
                                                {foreach $shipping_groups as $group_key => $shipping_group}
                                                <option value="{$group_key}" {if isset($fba.default_shipping_template) && $fba.default_shipping_template == $group_key}selected{/if}>{$shipping_group}
                                                </option>
                                                {/foreach}
                                            {/if}
                                        </select> 
                                    </div>                                
                                </div>        
                            </div>
                            <div class="col-xs-12 m-t10">                    
                                <p class="regRoboto poor-gray text-left">{ci_language line="You can specify here the shipping template belonging to the product FBA. If not specified, the default group, configured in the profile will be used."}</p>                 
                            </div>
                        </div>
                    </div> 

                    <hr>

                </div> 

                <div class="col-sm-6 custom-form">

                    <div class="form-group">

                        <div class="">
                            <p class="text-uc dark-gray montserrat m-b5">{ci_language line="Multi-Channel FBA"} :</p>
                            <div class="cb-switcher ">
                                <label class="inner-switcher">
                                    <input name="fba_multichannel" type="checkbox" value="1" {if isset($fba.fba_multichannel) && $fba.fba_multichannel == 1}checked="checked"{/if} data-state-on="{ci_language line="Yes"}" data-state-off="{ci_language line="No"}">
                                </label>
                                <span class="cb-state">{ci_language line="No"}</span>
                            </div>
                            <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Add Amazon FBA Multi-Channel support to feed.biz."}</p>                         
                         </div><!--Multi-Channel FBA-->

                        <div class="b-Top">
                            <p class="text-uc dark-gray montserrat m-b5 p-t10">{ci_language line="Automatic Multi-Channel"} :</p>
                            <div class="cb-switcher">
                                <label class="inner-switcher">
                                    <input name="fba_multichannel_auto" type="checkbox" value="1" {if isset($fba.fba_multichannel_auto) && $fba.fba_multichannel_auto == 1}checked="checked"{/if} data-state-on="{ci_language line="Yes"}" data-state-off="{ci_language line="No"}">
                                </label>
                                <span class="cb-state">{ci_language line="No"}</span>
                            </div>
                            <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Ship automatically through FBA Multi-Channel when an eligible order is created."}</p>
                        </div><!--Automatic Multi-Channel-->

                        <div class="b-Top"> 
                            <p class="text-uc dark-gray montserrat m-b5 p-t10">{ci_language line="Decrease Stock"} :</p>
                            <div class="cb-switcher">
                                <label class="inner-switcher">
                                    <input name="fba_decrease_stock" type="checkbox" value="1" {if isset($fba.fba_decrease_stock) && $fba.fba_decrease_stock == 1}checked="checked"{/if} data-state-on="{ci_language line="Yes"}" data-state-off="{ci_language line="No"}">
                                </label>
                                <span class="cb-state">{ci_language line="No"}</span>
                            </div>
                            <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Decrease the stock when importing orders."}</p>      
                        </div><!--Decrease Stock-->       
                    </div>
                   
                </div>
            </div>
        </div>
    </div> 
   
    {include file="amazon/PopupStepMoreFooter.tpl"} 
                
</form>    
  
<input type="hidden" id="set-fba-confirm" value="{ci_language line="Are you sure to set"} <span style='color:red'>{if isset($country)}{$country}{/if} </span> {ci_language line="to FBA Master platform for"} <span style='color:green'> {ci_language line="Amazon"} {if isset($region)}{$region}{/if} </span> ?" />
<input type="hidden" id="unset-fba-confirm" value="{ci_language line="Are you sure to"} <span style='color:red'> <b>{ci_language line="remove"}</b> {if isset($country)}{$country}{/if} </span> {ci_language line="from FBA Master platform for"} <span style='color:green'> {ci_language line="Amazon"} {if isset($region)}{$region}{/if} </span> ?" />
<input type="hidden" id="error-message-title" value="{ci_language line="Error encountered while saving data!"} <br/> {ci_language line="Please try again."}" />
<input type="hidden" id="success-message-title" value="{ci_language line="Save successful."}" />
<input type="hidden" id="l_Master-Platform" value="{ci_language line="Master Platform"}" />
<input type="hidden" id="l_No-master-platform" value="{if isset($country)}{$country}{/if} {ci_language line="are removed from"} {ci_language line="FBA Master Platform"}" />
 
<script src="{$cdn_url}assets/js/FeedBiz/amazon/fba.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}    
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}