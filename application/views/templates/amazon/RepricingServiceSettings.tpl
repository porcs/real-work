{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}
    
{include file="amazon/PopupStepMoreHeader.tpl"} 

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" class="">
                    <i class="fa fa-cog fa-fw"></i> <span>{ci_language line="service_settings"}</span>
                </a>
            </li>                                     
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-xs-12">
                <p class="head_text montserrat black p-t20 b-None">{ci_language line="Subscribe"}</p>   
                
                <div id="repricing-service-check-message" style="display: none;" class="col-md-12">
                    <div class="validate">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="fa"></i>
                            </div>
                            <div class="validateCell">
                                <p id ="repricing-service-check-message-content" class="pull-left"></p>                               
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="col-xs-12">                    
                    <button class="btn btn-danger" id="repricing-service-cancel" type="button" value="{$Repricing_CANCEL}">
                        <i class="fa fa-times"></i> 
                        <span class="check">{ci_language line="Cancel Subscription"} </span>
                        <span class="checking" style="display:none">{ci_language line="Processcing Cancel Subscription .."}</span>
                    </button>
                    
                    <button class="btn btn-rule m-l20" id="repricing-service-check" type="button" value="{$Repricing_SUBSCRIBE}">
                        <i class="fa fa-check"></i> 
                        <span class="check">{ci_language line="Check/Subscribe Service"} </span>
                        <span class="checking" style="display:none">{ci_language line="Processcing Subscribe Service .."}</span>
                    </button>
                    <!-- help -->
                    <div class="status-message" style="display:none">
                        <p></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<hr/>

<div class="row m-b20">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-xs-12">
                <p class="head_text montserrat black p-t10 b-None">{ci_language line="Maintenance"}</p>   
                <div class="col-xs-3">                    
                    <button class="btn btn-save" id="repricing-queue-check" type="button" value="{$Repricing_CHECK}">
                        <i class="fa fa-bars"></i> 
                        <span class="check">{ci_language line="List Queues"} </span>
                        <span class="checking" style="display:none">{ci_language line="Processcing .."}</span>
                    </button>
                </div>                    
            </div>
        </div>

        <div id="repricing-queue-check-message" style="display: none;" class="col-xs-12">

                <div id="purge-queue-section-message" style="display: none;">
                    <div class="validate">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="fa"></i>
                            </div>
                            <div class="validateCell">
                                <p id ="purge-queue-section-message-content" class="pull-left"></p>                               
                            </div>
                        </div>
                    </div>
                </div>

            <div id ="repricing-queue-check-message-content"></div> 

            <div id="purge-queue-section" style="display: none;" class="m-t15">
                <button class="btn btn-danger" id="repricing-queue-purge" type="button" value="{$Repricing_PURGE}">
                    <i class="fa"></i> 
                    <span class="check">{ci_language line="Purge Selected Queues"} </span>
                    <span class="checking" style="display:none">{ci_language line="Processcing Purge Queues .."}</span>
                </button>
                <!-- help -->
                <div class="status-message" style="display:none">
                    <p></p>
                </div>

            </div>           
        </div>

    </div>
</div>        
        
<input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
<input type="hidden" id="field_required" value="{ci_language line="This field is required."}" />                
<input type="hidden" id="ext" name="ext" value="{$ext}" />
<input type="hidden" id="id_shop" name="id_shop" value="{$id_shop}" /> 

<!--Message-->
<input type="hidden" id="connect-success" value="{ci_language line="Successful"}" /> 
<input type="hidden" id="connect-fail" value="{ci_language line="Failed"}" />
<input type="hidden" id="connect-error" value="{ci_language line="Data connection error! Please contact your support, error code:"}" />
<input type="hidden" value="{ci_language line="Loading"}.." id="loading">  
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">    
          
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/repricing_service_settings.js"></script> 
{include file="amazon/PopupStepFooter.tpl"}   
 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}