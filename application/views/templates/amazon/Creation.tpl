{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}    
<div class="row-fluid">

    <div class="clearfix text-center" id="page-load">
        <div class="feedbizloader m-t20"><div class="feedbizcolor"></div></div>        
        <h4 class="m-t20"></h4>
    </div>
        
    <!--Loading-->
    <div class="row" id="loading" style="display: none">
        <div class="col-xs-12 m-t20">
            <div class="pull-left">
                <div id="message">
                    <h4 class="m-t15"></h4>
                    <h5></h5>
                </div>
            </div>
            <div class="pull-right m-t20">
                <img src="{$cdn_url}assets/images/loader-connection.gif" />
            </div>
        </div>
    </div>    

    <!--Loaded-->
    <div id="loaded" style="display: none" class="m-b40 p-b20">

        <!--creation-->
        <div id="creation" style="display:none;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Bottom b-Top-None">
                        <h4  id="title_message" class="headSettings_head">{ci_language line="You have existing inventory on Amazon."}</h4>
                        <h4  id="no_product" class="headSettings_head" style="display: none">
                            <span class="text-primary">{ci_language line="You don't have existing inventory on Amazon."}</span>
                        </h4>
                    </div>                      
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 m-t20 m-b20">
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Local"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span id="feed_no_product" class="dark-gray bold"></span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Amazon"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span id="amazon_no_product" class="dark-gray bold">0</span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                    <div class="amazon_synch clearfix">
                        <div class="amazon_synch-text text-right">
                            <p class="p-text">{ci_language line="Marked as to be created"} :</p>
                        </div>
                        <div class="amazon_synch-text text-right">
                            <p class="poor-gray"><span  id="product_to_create" class="dark-gray bold">0</span> {ci_language line="item(s)"}</p>
                        </div>
                    </div>
                </div>
            </div>  
            
            <div id="option">
                <form id="create_options">

                    <div class="row custom-form">
                        <div class="col-xs-12">

                            <div class="b-Top">

                                <div class="col-sm-6 col-md-4 col-lg-3">
                                <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Options"}</p>
                                    <!-- Relationship -->
                                    <label class="amazon-checkbox w-100">
                                        <span class="amazon-inner">
                                            <i>
                                                <input type="checkbox" id="send_relation_ship" name="send_relation_ship" />
                                            </i>
                                        </span>
                                        {ci_language line="Send only relationship"}.
                                    </label>    
                                    <!-- image -->
                                    <label class="amazon-checkbox w-100">
                                        <span class="amazon-inner">
                                            <i>
                                                <input type="checkbox" id="send_only_image" name="send_only_image" /> 
                                            </i>
                                        </span>
                                        {ci_language line="Send only image"}.
                                    </label>                                 
                                    <!-- image -->
                                    <!-- <label class="amazon-checkbox w-100 checked">
                                        <span class="amazon-inner">
                                            <i>
                                                <input type="hidden" id="send_image" name="send_image" checked /> 
                                            </i>
                                        </span>
                                        {ci_language line="Send image"}.
                                    </label>  -->                                                                         
                                </div>
                                <div class="col-sm-6 col-md-4 col-lg-3 m-t25">     

                                    <p class="poor-gray m-t10">{ci_language line="Limit product(s)"} :</p> 

                                    <div class="form-group col-xs-10 p-l0">
                                        <select id="limiter" name="limiter" class="search-select ">
                                            <option value="0">{ci_language line="No limit"}</option>
                                            <option value="10"> 10 </option>
                                            <option value="50"> 50 </option>
                                            <option value="100"> 100 </option>
                                            <option value="500"> 500 </option>
                                            <option value="1000"> 1000 </option>
                                            <option value="2000"> 2000 </option>
                                            <option value="3000"> 3000 </option>
                                            <option value="5000"> 5000 </option>
                                            <option value="6000"> 6000 </option>
                                            <option value="7000"> 7000 </option>
                                            <option value="8000"> 8000 </option>
                                            <option value="9000"> 9000 </option>
                                            <option value="10000"> 10000 </option>
                                            <option value="12000"> 12000 </option>
                                        </select> 
                                    </div> 
                                </div>

                                
                                    {*if isset($mode_default) && $mode_default > 1}
                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                            <p class="montserrat dark-gray text-uc m-t15">{ci_language line="Advanced Options"}</p>
                                            
                                            <!-- manufacturer -->
                                            <label class="amazon-checkbox w-100">
                                                <span class="amazon-inner">
                                                    <i>
                                                        <input type="checkbox" id="exclude_manufacturer" name="exclude_manufacturer" /> 
                                                    </i>
                                                </span>
                                                {ci_language line="Exclude manufacturer filter"}.
                                            </label>
                                            <!-- filter -->
                                            <label class="amazon-checkbox w-100">
                                                <span class="amazon-inner">
                                                    <i>
                                                        <input type="checkbox" id="exclude_supplier" name="exclude_supplier" /> 
                                                    </i>
                                                </span>
                                                {ci_language line="Exclude supplier filter"}.
                                            </label>
                                            <!-- carrier -->
                                            <label class="amazon-checkbox w-100">
                                                <span class="amazon-inner">
                                                    <i>
                                                        <input type="checkbox" id="only_selected_carrier" name="only_selected_carrier" /> 
                                                    </i>
                                                </span>
                                                {ci_language line="Use only selected carriers"}.
                                            </label>     
                                            <p class="regRoboto poor-gray m-t10">
                                                {ci_language line="During the creation, suppliers, manufacturers and carriers in the product catalogue being imported matching the ones defined in the filters will be dropped from the product creations."}
                                            </p>                                  
                                        </div>
                                    {/if*}
                                
                            </div>
                               
                        </div>
                    </div>

                </form>
            </div>                         

            <div class="row">
                <div class="col-xs-12">
                    <div class="m-t5 p-t10 p-l15">
                        <div class="form-group has-error" id="send-message" style="display: none;">
                            <div class="error">
                                <p><i class="fa fa-exclamation-circle"></i><span></span></p>
                            </div>  
                        </div>
                        <button class="btn btn-amazon p-l12" type="button" id="create" style="display: none">
                            <i class="fa fa-upload"></i>
                            {ci_language line="Start send products to Amazon"}
                        </button>                                       
                    </div>      
                </div>
            </div>

        </div>
    </div>   

    <div id="popup_action" class="row popup_prev b-Top">
        <div class="col-xs-12">
            <div class="inline pull-left p-b20"> 
                <button class="pull-left link p-size m-tr10" id="back" type="button">
                    {ci_language line="Previous"}
                </button>
            </div>
            <input type="hidden" id="no_product_to_create" value="{ci_language line="Product marked as to be created is 0."}" />
            <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
            <input type="hidden" id="pay_now" value="{ci_language line="Pay now"}" />
            <input type="hidden" id="mode_match" value="{if isset($mode_match)}{$mode_match}{/if}" />
            <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
            {if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
        </div>
    </div>

</div><!-- row-fluid -->
{include file="amazon/PopupStepFooter.tpl"}

<input type="hidden" value="{if isset($country)}{str_replace(array(' '), '_', $country)}{/if}" id="country">
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">           

<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/creation.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}