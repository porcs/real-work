{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  

            <div class="row">
                <div class="col-xs-12 p-t10">
                    <div class="validate blue">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="fa fa-info-circle"></i>
                            </div>
                            <div class="validateCell">
                                <p>
                                    {ci_language line="You can manage the ad groups in "} 
                                    <a href="{$base_url}amazon/profiles/{$id_country}" target="_blank" class="link"><b>{ci_language line="Profiles"}</b></a>  
                                    {ci_language line="and specify the ad groups belonging to the product in "}
                                    <a href="{$base_url}amazon/offers_options/product/{$id_country}/2" target="_blank" class="link"><b>{ci_language line="Offers Options"}</b></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Top b-Bottom">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Product Ad"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix"></div>
                        </div>          
                    </div><!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="product_ad" class="amazon-responsive-table hover">
                                <thead class="table-head" style="display:none">
                                    <tr>
                                        <th class="center tableNoSort"></th>
                                        <th class="center"></th>
                                        <th class="center"></th>
                                    </tr>
                                </thead>
                                <!-- <thead class="table-head">
                                    <tr>
                                        <th class="center">{ci_language line="SKU"}</th>
                                        <th class="center">{ci_language line="ASIN"}</th>
                                        <th class="center">{ci_language line="Campaign Id"}</th>
                                        <th class="center">{ci_language line="AdGroup Id"}</th>
                                        <th class="center">{ci_language line="Ad Id"}</th>
                                        <th class="center">{ci_language line="State"}</th>
                                        <th class="center"></th>
                                    </tr>
                                </thead> --> 
                            </table>
                        </div> <!--col-xs-12-->
                    </div><!--row-->

                </div><!--col-xs-12-->
            </div><!--row--> 

            <div style="display:none">               
                <table id="details">
                    <thead class="table-head">
                        <tr>                        
                            <th class="tableNoSort">{ci_language line="Ad Id"}</th>
                            <th>{ci_language line="AdGroup Id"}</th>
                            <th>{ci_language line="Campaign Id"}</th>                            
                            <th>{ci_language line="SKU"}</th>
                            <th>{ci_language line="ASIN"}</th>
                            <th>{ci_language line="State"}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="row m-t0 footer-step">
                <div class="col-xs-12">
                    <div class="b-Top p-t10">
                        {if isset($previous_page)}
                        <div class="inline pull-left">
                            <button class="btn btn-small p-size" id="back" type="button" value="{$previous_page}">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                {ci_language line="Keywords Management"}
                            </button>
                        </div>
                        {/if}
                        {if isset($next_page)}
                        <div class="inline pull-right">  
                            <button class="btn btn-small p-size" id="next" type="button" value="{$next_page}">
                                {ci_language line="Reports"}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>                        
                        </div>
                        {/if}
                    </div>      
                </div>
            </div>

            <input type="hidden" id="archive-message" value="{ci_language line="Set the group status to archived successful"}" />
            <input type="hidden" id="confirm-archive-group-message" value="{ci_language line="Are you sure to set the group status to archived"}" />
            <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
            <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data"}" />
            <input type="hidden" id="please-try" value="{ci_language line="Please try again"}" />
            <input type="hidden" value="{ci_language line="Archive"}" id="Archive" />
            <input type="hidden" value="{ci_language line="Edit"}" id="Edit" />
            <input type="hidden" value="{ci_language line="Product Ads"}" id="productAds" />            
            <input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
            <input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
            <input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
            <input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
            <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">   
            <input type="hidden" value="{ci_language line="AdGroup Id"}" id="id_title"> 
            <input type="hidden" value="adGroupCreation" id="addNewId"> 
            <input type="hidden" value="{ci_language line="AdGroups"}" id="AdGroups"> 
            <input type="hidden" value="{ci_language line="Campaign"}" id="Campaign"> 
            {include file="amazon/IncludeDataTableLanguage.tpl"}   

        </div> 

    {include file="amazon/PopupStepFooter.tpl"} 

</div> 
      
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/advertising.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}