{if isset($popup) && $popup != ''}      
    <input type="hidden" id="popup_status" value="{$popup}">
    
    <div class="col-xs-12">
            <div class="headSettings clearfix m-b10">
                    <ul class="headSettings_point clearfix">
                            <li class="headSettings_point-item{if $popup >= '1'} active{/if}" style="width: 20%;">
                                    <span class="step">1</span>
                                    <span class="title">{ci_language line="Choose Amazon site"}</span>
                            </li>
                            <li class="headSettings_point-item{if $popup >= '2'}active{/if}" style="width: 20%;">
                                    <span class="step">2</span>
                                    <span class="title">{ci_language line="Config API setting"}</span>
                            </li>
                            <li class="headSettings_point-item{if $popup >= '3'} active{/if}" style="width: 20%;">
                                    <span class="step">3</span>
                                    <span class="title">{ci_language line="Choose category"}</span>
                            </li>
                            <li class="headSettings_point-item{if $popup >= '4'} active{/if}" style="width: 20%;">
                                    <span class="step">4</span>
                                    <span class="title">
                                    {if isset($mode) && $mode == 1}
                                        {ci_language line="Sync & match"}
                                    {else}
                                        {ci_language line="Creation"}
                                    {/if}
                                    </span>
                            </li>
                            <li class="headSettings_point-item{if $popup >= '5'} active{/if}" style="width: 20%;">
                                    <span class="step">5</span>
                                    <span class="title">{ci_language line="Send to Amazon"}</span>
                            </li>
                    </ul>
            </div>
    </div>
    
    {*<ul class="wizard-steps clearfix hidden-480" style="margin:20px 0 0 0; {if isset($popup_more) && $popup_more != ''}opacity: 0.5;{/if}"> 
        <li {if $popup >= '1'} class="active"{/if} style="min-width: 20%; max-width: 20%;">
            <span class="step">1</span>
            <span class="title">{ci_language line="Choose Amazon site"}</span>
        </li>
        
        <li {if $popup >= '2'} class="active"{/if} style="min-width: 20%; max-width: 20%;">
            <span class="step">2</span>
            <span class="title">{ci_language line="Config API setting"}</span>
        </li> 
        <li {if $popup >= '3'} class="active"{/if} style="min-width: 20%; max-width: 20%;">
            <span class="step">3</span>
            <span class="title">{ci_language line="Choose category"}</span>
        </li> 
        <li {if $popup >= '4'} class="active"{/if} style="min-width: 20%; max-width: 20%;">
            <span class="step">4</span>
            <span class="title">
                {if isset($mode) && $mode == 1}
                    {ci_language line="Sync & match"}
                {else}
                    {ci_language line="Creation"}
                {/if}
            </span>
        </li>
        <li {if $popup >= '5'} class="active"{/if} style="min-width: 20%; max-width: 20%;">
            <span class="step">5</span>
            <span class="title">{ci_language line="Send to Amazon"}</span>
        </li>
    </ul>   *}
{/if}
 