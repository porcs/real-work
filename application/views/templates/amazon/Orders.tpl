{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<!--messaging.css-->
<link rel="stylesheet" href="{$cdn_url}assets/css/feedbiz/amazon/orders.css" />

<div {if !isset($popup)}id="content"{/if}> 

    {include file="amazon/PopupStepHeader.tpl"}

    <div class="row-fluid">  

        <div id="send-products-message">
            <div class="status row" style="display:none;">
                <div class="col-xs-12 validate blue">
                    <div class="validateRow">
                        <div class="validateCell">
                            <i class="note"></i> 
                        </div>
                        <div class="validateCell">
                            <p class="pull-left"></p>
                        </div>
                    </div>
                </div>              
            </div>
        </div>

        <!--Import Order Message Here-->
        <div id="actions">
            <div class="status row" style="display:none;">
                <div class="col-xs-12 validate blue">
                    <div class="validateRow">
                        <div class="validateCell">
                            <i class="fa fa-spinner fa-spin"></i> 
                        </div>
                        <div class="validateCell">
                            <p class="pull-left">{ci_language line="Conecting to Amazon .."}</p>
                        </div>
                    </div>
                </div>              
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">


                <div class="headSettings clearfix">
                    <div class="pull-sm-left clearfix">
                        <h4 class="headSettings_head">{ci_language line="Orders :"}</h4>                      
                    </div>
                    <div class="pull-sm-right clearfix">
                        <div class="showColumns clearfix">                                
                            <div class="b-Right p-r10">
                                <p class="small light-gray pull-left m-t5">{ci_language line="Show only"} :&nbsp;</p> 
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" name="export-sent" id="export-sent" value="1" class="amazon-input"></i></span>
                                            {ci_language line="Sent"}
                                </label>
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" name="export-send" id="export-send" value="1" class="amazon-input"></i></span>
                                            {ci_language line="Send"}
                                </label>
                                <label class="amazon-checkbox">
                                    <span class="amazon-inner"><i><input type="checkbox" name="export-error" id="export-error" value="1" class="amazon-input"></i></span>
                                            {ci_language line="Error"}
                                </label>
                            </div>
                        </div>
                    </div>          
                </div><!--headSettings-->

                <div class="row">
                    <div class="col-xs-12">
                        <table id="statistics" class="amazon-responsive-table">
                            <thead class="table-head">                                    
                                <!-- <tr>
                                    <th colspan="13" class="">
                                        <div class="text-left">
                                            
                                        </div>
                                    </th>
                                </tr> -->

                                <tr>
                                    <th>{ci_language line="ID"}</th>
                                    <th>{ci_language line="Order ID Ref"}.</th>
                                        {*<th>{ci_language line="Buyer Name"}</th>*}
                                    <th>{ci_language line="Address Name"}</th>
                                        {*<th>{ci_language line="Payment"}</th>*}
                                    <th>{ci_language line="Amount"}</th>
                                    <th>{ci_language line="Status"}</th>
                                    <th>{ci_language line="Multi channel"}</th>
                                    <th>{ci_language line="Order Date"}</th>
                                    <th>{ci_language line="Shipping Date"}</th>
                                    <th>{ci_language line="Tracking No."}</th>
                                    <th>{ci_language line="Seller Order Id"}</th>
                                    <th>{ci_language line="Invoice No."}</th>

                                    <th {if isset($amazon_display[$ext]['messaging']) && $amazon_display[$ext]['messaging'] == 1}{else}style="display: none"{/if}>
                                        {ci_language line="Messaging"}
                                        {if isset($amazon_display[$ext]['messaging']) && $amazon_display[$ext]['messaging'] == 1}                                              
                                            <input type="hidden" id="display_messaging" value="1">
                                            <input type="hidden" id="flag_mail" value="{ci_language line="Sent"}">
                                            <input type="hidden" id="flag_mail_fail" value="{ci_language line="Send"}">
                                            <input type="hidden" id="flag_mail_invoice" value="{ci_language line="Sent Invoice"}">
                                            <input type="hidden" id="flag_mail_review" value="{ci_language line="Sent Review"}">
                                        {/if}
                                    </th>

                                    <th>{ci_language line="State"}</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="tr_search">
                                    <th>{ci_language line="ID"}</th>
                                    <th>{ci_language line="Order ID Ref"}.</th>
                                        {*<th>{ci_language line="Buyer Name"}</th>*}
                                    <th>{ci_language line="Address Name"}</th>
                                        {*<th>{ci_language line="Payment"}</th>*}
                                    <th>{ci_language line="Amount"}</th>
                                    <th>{ci_language line="Status"}</th>
                                    <th>{ci_language line="Multi channel"}</th>
                                    <th>{ci_language line="Order Date"}</th>
                                    <th>{ci_language line="Shipping Date"}</th>
                                    <th>{ci_language line="Tracking No."}</th>
                                    <th>{ci_language line="Seller Order Id"}</th>
                                    <th>{ci_language line="Invoice No."}</th>
                                    <th {if isset($amazon_display[$ext]['messaging']) && $amazon_display[$ext]['messaging'] == 1}{else}style="display: none"{/if}></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table><!--table-->
                        <table class="table tableEmpty">
                            <tr>
                                <td>{ci_language line="No data available in table"}</td>
                            </tr>
                        </table>
                    </div><!--col-xs-12-->
                </div><!--row--> 

            </div><!--col-xs-12-->
        </div><!--row-->   

        <!--Hidden-->
        <input type="hidden" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
        <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
        <input type="hidden" id="id_user" value="{if isset($id_user)}{$id_user}{/if}" />
        <input type="hidden" id="tag" value="{if isset($tag)}{$tag}{/if}" />
        <input type="hidden" id="id_marketplace" value="{if isset($id_marketplace)}{$id_marketplace}{/if}" />
        <input type="hidden" id="cron_send_orders" value="{if isset($cron_send_orders)}{$cron_send_orders}{/if}" />
        <input type="hidden" id="shop_name" value="{if isset($shop[0]['name'])}{$shop[0]['name']}{/if}" />
        <input type="hidden" id="pay_now" value="{ci_language line="Pay now"}" />
        <input type="hidden" id="Send" value="{ci_language line="Send"}" />
        <input type="hidden" id="Sent" value="{ci_language line="Sent"}" />
        <input type="hidden" id="Error" value="{ci_language line="Error"}" />
        <input type="hidden" id="Sending" value="{ci_language line="Sending"}" />
        <input type="hidden" id="Connecting" value="{ci_language line="Connecting to Amazon"}.." />
        <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
        <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data."}" />
        <input type="hidden" id="please-try" value="{ci_language line="Please try again."}" />
        <input type="hidden" id="confirm-message" value="{ci_language line="Are you sure to change Address name :"}" />         

        {include file="amazon/IncludeDataTableLanguage.tpl"}
    </div> 

    {include file="amazon/PopupStepFooter.tpl"} 

    <div class="modal fade" id="importOrder">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Parameters"}</h1>
                        </div>
                    </div>

                    <div class="row">						
                        <div class="col-sm-3">
                            <p class="poor-gray">{ci_language line='Order Date Range'}</p>
                            <div class="form-group calendar firstDate">
                                <input class="form-control order-date" type="text" id="order-date-from" value="{if isset($yesterday)}{$yesterday}{/if}">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group calendar lastDate m-t25">
                                <input class="form-control order-date" type="text" id="order-date-to" value="{if isset($today)}{$today}{/if}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p class="poor-gray">{ci_language line='Order Status'}</p>
                            <div class="form-group ">
                                <select id="order-status" class="search-select" data-placeholder="Choose">
                                    <option value="All">{ci_language line='Retrieve all pending orders'}</option>
                                    <option value="Pending">{ci_language line='Pending - This order is pending in the market place'}</option>
                                    <option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order is waiting to be shipped'}</option>
                                    <option value="PartiallyShipped">{ci_language line='Partially shipped - This order was partially shipped'}</option>
                                    <option value="Shipped">{ci_language line='Shipped - This order was shipped'}</option>
                                    <option value="Canceled">{ci_language line='Cancelled - This order was cancelled'}</option>
                                </select>								
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-save" data-dismiss="modal" id="import-orders">{ci_language line="Start importing orders"}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="orderDetails">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Order Details"}</h1>
                        </div>
                    </div>
                    <div id="orderDetail"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="order_details_template" style="display:none">
        {*<form method="post" action="#" enctype="multipart/form-data" autocomplete="off" >*}
        <div class="custom-form">
            <input type="hidden" name="id_orders" rel="id_orders" />

            <div class="row">
                <div class="col-md-12"> 
                    <div class="b-Top p-t20">
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div class="row p-size poor-gray">
                                    <div class="col-md-3 text-uc dark-gray montserrat">{ci_language line="Order ID."}</div>
                                    <div rel="id_orders" class="col-md-9"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            <div class="row p-size poor-gray">
                                <div class="col-md-3 text-uc dark-gray montserrat">{ci_language line="Amazon Order ID."}</div>
                                <div rel="id_marketplace_order_ref" class="col-md-9"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">  
                <div class="col-md-12">  
                    <p class="head_text">
                        {ci_language line="Shipping Address"}
                    </p>
                </div>
            </div>

            <div class="row flag_error" style="display:none">
                <div class="col-md-12"> 
                    <label class="dark-gray montserrat col-md-2 text-right p-t10"></label>
                    <div class="col-md-10">
                        <p class="true-pink p-l10 m-b5">
                            <i class="fa fa-exclamation-circle"></i> 
                            {ci_language line="The address infomation is invalid, please correct the highlighted fields, and submit."}<br/>
                        </p>
                        <p class="true-pink p-l25">
                            {ci_language line="This is not allow : "}  [`~!&lt;&gt;;?=+()@#"/[\]Â°{}_$%:.*&amp;|]
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 m-t10"> 
                    <div class="form-group col-md-12">
                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Address Name"}</label>
                        <div class="col-md-10">
                            <input type="text" name="address_name" class="form-control" rel="address_name" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">
                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Address 1"}</label>
                        <div class="col-md-10">
                            <input type="text" name="address1" class="form-control" rel="address1" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="The first line of a standard address. MaxLength : 60"}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">
                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Address 2"}</label>
                        <div class="col-md-10">
                            <input type="text" name="address2" class="form-control" rel="address2" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="The second line of a standard address. MaxLength : 60"}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">
                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="City"}</label>
                        <div class="col-md-4">
                            <input type="text" name="city" class="form-control" rel="city" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="The city of a standard address"}
                        </div>
                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="State/Region"}</label>
                        <div class="col-md-4">
                            <input type="text" name="state_region" class="form-control" rel="state_region" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="The state or region of a standard address"}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">

                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Country Code"}</label>
                        <div class="col-md-4">
                            <input type="text" name="country_code" class="form-control" rel="country_code" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="ISO 3166 standard two-letter country code "}</p>
                        </div>

                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Postal Code"}</label>
                        <div class="col-md-4">
                            <input type="text" name="postal_code" class="form-control" rel="postal_code" />
                            <p class="poor-gray m-t2 m-b0">{ci_language line="The postal (ZIP) code of a standard address"}</p>
                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12"> 
                    <div class="form-group col-md-12">

                        <label class="dark-gray montserrat col-md-2 text-right p-t10">{ci_language line="Phone"}</label>
                        <div class="col-md-4">
                            <input type="text" name="phone" class="form-control" rel="phone" />
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <p class="col-md-10 poor-gray m-t2 m-b0">{ci_language line="The phone number associated with an address if applicable"}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t20">
                    <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="save_detail();" >
                        {ci_language line="Save"}
                    </button>
                </div>
            </div>
        </div>

        {*</form>*}
    </div>

</div>
    
<div class="modal fade" id="amazonOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Order Result"}</h1>
                    </div>
                </div>

                <div class="row">                       
                    <div class="col-xs-12 p-0">
                        <div id="viewResult"></div>
                    </div>  
                </div>
             
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/orders.js"></script> 
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}