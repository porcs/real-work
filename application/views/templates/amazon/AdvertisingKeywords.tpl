{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}> 
    
    {include file="amazon/PopupStepHeader.tpl"}  

        <div class="row-fluid">  

            {*if isset($ads) }
            {foreach from=$ads key=k item=data}
                <div class="row amazon-profiles">

                    <div class="col-xs-12">
                        <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                            <p class="text-uc dark-gray montserrat pull-left p-t10 m-b0">
                                {if isset($data.campaignName)}{$data.campaignName}{/if}
                                &nbsp;<i class="fa fa-angle-right"></i> 
                                <span>
                                    {if isset($data.adGroupName)}{$data.adGroupName}{/if}
                                </span>
                            </p>
                            <ul class="pull-right amazonEdit">
                                <!-- <li class="editProfile">
                                    <button type="button" class="link editProfile_link edit_profile" value="{$data.adGroupId}">
                                    <i class="fa fa-pencil"></i> {ci_language line="edit"}
                                    </button>
                                </li>-->
                                <li class="remProfile">
                                    <button type="button" class="link" value="{$data.adGroupId}">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i> {ci_language line="Keywords"}
                                    </button>  
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>                
            {/foreach} 
            {/if*}

            <div class="row">
                <div class="col-xs-12">
                    <div class="headSettings clearfix b-Top-None b-Bottom">
                        <div class="pull-sm-left clearfix">
                            <h4 class="headSettings_head">{ci_language line="Keywords Management"}</h4>                      
                        </div>
                        <div class="pull-sm-right clearfix">
                           <div class="showColumns clearfix"></div>
                        </div>          
                    </div><!--headSettings-->
                             
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="ad_keywords" class="amazon-responsive-table hover">
                                <thead class="table-head" style="display:none">
                                    <tr>
                                        <th class="center tableNoSort"></th>
                                        <th class="center"></th>
                                        <th class="center"></th>
                                    </tr>
                                </thead> 
                            </table>
                        </div> <!--col-xs-12-->
                    </div><!--row-->

                </div><!--col-xs-12-->
            </div><!--row-->

            <div style="display:none">

                <div id="keyword_header">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" class="biddable_content">
                                        <i class="fa fa-check true-green"></i> <span>{ci_language line="Keywords"}</span>
                                    </a>
                                </li>  
                                <li>
                                    <a  data-toggle="tab" class="negative_content">
                                        <i class="fa fa-remove true-pink"></i> <span>{ci_language line="Negative keywords"}</span>
                                    </a>
                                </li>                      
                            </ul>
                        </div>
                    </div>
                </div>    
                <table id="biddable_details">
                    <thead class="table-head">
                        <tr>                        
                            <th class="center tableNoSort">{ci_language line="Keyword ID"}</th>
                            <th class="center"><i class="fa fa-check true-green"></i> {ci_language line="Keyword Text"}</th>
                            <th>{ci_language line="Match Type"}</th>                            
                            <th>{ci_language line="State"}</th>
                            <th>{ci_language line="Bid"}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <table id="negative_details">
                    <thead class="table-head">
                        <tr>                        
                            <th class="center tableNoSort">{ci_language line="Negative Keyword ID"}</th>
                            <th class="center"><i class="fa fa-remove true-pink"></i> {ci_language line="Negative Keyword Text"}</th>
                            <th>{ci_language line="Match Type"}</th>                            
                            <th>{ci_language line="State"}</th>
                            <th>{ci_language line="Bid"}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="row m-t0 footer-step">
                <div class="col-xs-12">
                    <div class="p-t10">
                        {if isset($previous_page)}
                        <div class="inline pull-left">
                            <button class="btn btn-small p-size" id="back" type="button" value="{$previous_page}">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> 
                                {ci_language line="Manage Ad Groups"}
                            </button>
                        </div>
                        {/if}
                        {if isset($next_page)}
                        <div class="inline pull-right">  
                            <button class="btn btn-small p-size" id="next" type="button" value="{$next_page}">
                                {ci_language line="Product Ads"}
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </button>                        
                        </div>
                        {/if}
                    </div>      
                </div>
            </div>

            <input type="hidden" id="archive-message" value="{ci_language line="Set the group status to archived successful"}" />
            <input type="hidden" id="confirm-archive-keyword-message" value="{ci_language line="Are you sure to set the keyword status to archived"}" />
            <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
            <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data"}" />
            <input type="hidden" id="please-try" value="{ci_language line="Please try again"}" />
            <input type="hidden" value="{ci_language line="Archive"}" id="Archive" />
            <input type="hidden" value="{ci_language line="Edit Bid"}" id="Edit_Bid" />
            <input type="hidden" value="{ci_language line="Edit"}" id="Edit" />
            <input type="hidden" value="{ci_language line="Keywords"}" id="Keywords" />
            <input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
            <input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
            <input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
            <input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
            <input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">   
            <input type="hidden" value="{ci_language line="Keyword Id"}" id="id_title"> 
            <input type="hidden" value="{ci_language line="AdGroups"}" id="AdGroups"> 
            <input type="hidden" value="{ci_language line="Campaign"}" id="Campaign"> 
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}

            {include file="amazon/IncludeDataTableLanguage.tpl"}   

        </div> 

    {include file="amazon/PopupStepFooter.tpl"} 

    <div class="modal fade" id="keywordDetails">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">{ci_language line="Edit Keyword Bid"}</h1>
                        </div>
                    </div>
                    <div id="keywordDetail" class="m-t10"></div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div id="template_biddable" style="display:none">
        <div class="row">
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Keyword Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="keywordId" name="keywordId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="AdGroup Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="adGroupId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Campaign Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="campaignId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Keyword Text"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <textarea class="form-control" rows="4" cols="100" rel="keywordText" readonly style="background: #ddd;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Keyword Bid"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="bid" rel="bid"/>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="State"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-10 m-t5">
                            <label class="cb-radio">
                                <input type="radio" value="enabled" rel="state" name="state"/>
                                {ci_language line="Enabled"}
                            </label>
                            <label class="cb-radio m-l10">
                                <input type="radio" value="paused" rel="state" name="state"/>
                                {ci_language line="Paused"}
                            </label>
                            <label class="cb-radio m-l10">
                                <input type="radio" value="archived" rel="state" name="state"/>
                                {ci_language line="Archived"}
                            </label>
                        </div>
                    </div>
                </div>
            </div>  
           
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t20">
                    <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="save_keyword(this, 'biddable');" >
                        {ci_language line="Save"}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="template_negative" style="display:none">
        <div class="row">
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Negative Keyword Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="keywordId" name="keywordId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="AdGroup Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="adGroupId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Campaign Id"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" rel="campaignId" readonly style="background: #ddd;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Negative Keyword Text"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <textarea class="form-control" rows="4" cols="100" rel="keywordText" readonly style="background: #ddd;"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            {*<div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="Negative Keyword Bid"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="bid" rel="bid"/>
                        </div>
                    </div>
                </div>
            </div> *}
            <div class="form-group col-md-12">
                <label class="text-uc dark-gray montserrat col-md-4 text-right p-t10" >
                    <span class="b-blue">{ci_language line="State"}</span>
                </label>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-10 m-t5">
                            <label class="cb-radio">
                                <input type="radio" value="enabled" rel="state" name="state"/>
                                {ci_language line="Enabled"}
                            </label>
                            <label class="cb-radio m-l10">
                                <input type="radio" value="archived" rel="state" name="state"/>
                                {ci_language line="Archived"}
                            </label>
                        </div>
                    </div>
                </div>
            </div>  
           
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="b-Top p-t20">
                    <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="save_keyword(this, 'negative');" >
                        {ci_language line="Save"}
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="biddable">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">
                                {ci_language line="Create New"} <span class="true-blue">{ci_language line="Keywords"}</span>
                            </h1>                            
                        </div>
                    </div>
                    <div class="row custom-form">  
                        <div class="form-group col-md-12">
                            <div class="validate blue">
                                <div class="validateRow">
                                    <div class="validateCell">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="validateCell">
                                        <p class="pull-left">
                                            {ci_language line="Creates one or more keywords. Successfully created keywords will be assigned unique keywordIds"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>       
                                          
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="AdGroup Id"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-10">
                                        <select class="search-select select_adGroupId" name="adGroupId">
                                            <option>--</option>
                                            {if isset($adGroups)}
                                                {foreach $adGroups as $adGroup}
                                                    <option value="{$adGroup['adGroupId']}" data-campaign="{$adGroup['campaignId']}">
                                                        {$adGroup['adGroupId']} ({$adGroup['name']})
                                                    </option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">
                                    {ci_language line="The ID of the campaign to which this keyword belongs. Specified for ad group level keywords"}
                                </p>                                
                            </div>
                        </div>
                        <div class="select_campaignId" style="display:none">
                             <div class="form-group col-md-12">
                                <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                    <span class="b-blue">{ci_language line="Campaign Id"}</span>
                                </label>
                                <div class="col-md-9">                                
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" rel="campaignName" readonly style="background: #ddd;">
                                            <input type="hidden" class="form-control" name="campaignId" rel="campaignId">
                                        </div>
                                    </div>
                                    <p class="poor-gray m-t5">{ci_language line="The ID of the campaign to which this keyword belongs"}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="true-blue">{ci_language line="Keyword Text"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group withIcon clearfix ">
                                            <input type="text" {*placeholder="{ci_language line="add more keyword"}"*} class="form-control tags-input" {*data-role="tagsinput"*} name="keywordText[]" />
                                            <i class="cb-plus good addKeywordText"></i>
                                            <i class="cb-plus bad removeKeywordText" style="cursor: pointer;display:none"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="poor-gray m-t0">
                                    {ci_language line="The expression to match against search queries."} 
                                    {ci_language line="You can have up to 1000 keywords to be created."} 
                                </p>
                            </div>
                        </div>  
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="Match Type"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">

                                        <label class="cb-radio">
                                            </i>
                                            <input type="radio" value="exact" name="matchType" checked="checked"/>
                                            {ci_language line="Exact"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="phrase" name="matchType"/>
                                            {ci_language line="Phrase"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="broad" name="matchType"/>
                                            {ci_language line="Broad"}
                                        </label>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="The match type used to match the keyword to search query"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="State"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="cb-radio">
                                            </i>
                                            <input type="radio" value="enabled" name="state" checked="checked"/>
                                            {ci_language line="Enabled"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="paused" name="state"/>
                                            {ci_language line="Paused"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="archived" name="state"/>
                                            {ci_language line="Archived"}
                                        </label>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="Advertiser-specified state of the ad group"}</p>
                            </div>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="b-Top p-t20">
                                <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="create_keyword(this, 'biddable');" >
                                    {ci_language line="Create Keyword"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="negative">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="lightRoboto text-uc head-size light-gray">
                                {ci_language line="Create New"} <span class="true-pink">{ci_language line="Negative Keywords"}</span>
                            </h1>                            
                        </div>
                    </div>
                    <div class="row custom-form">  
                        <div class="form-group col-md-12">
                            <div class="validate blue">
                                <div class="validateRow">
                                    <div class="validateCell">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                    <div class="validateCell">
                                        <p class="pull-left">
                                            {ci_language line="Creates one or more negative keywords. Successfully created keywords will be assigned unique keywordIds"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>       
                                          
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="AdGroup Id"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-10">
                                        <select class="search-select select_adGroupId" name="adGroupId">
                                            <option>--</option>
                                            {if isset($adGroups)}
                                                {foreach $adGroups as $adGroup}
                                                    <option value="{$adGroup['adGroupId']}" data-campaign="{$adGroup['campaignId']}">
                                                        {$adGroup['adGroupId']} ({$adGroup['name']})
                                                    </option>
                                                {/foreach}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">
                                    {ci_language line="The ID of the campaign to which this keyword belongs. Specified for ad group level keywords"}
                                </p>                                
                            </div>
                        </div>
                        <div class="select_campaignId" style="display:none">
                             <div class="form-group col-md-12">
                                <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                    <span class="b-blue">{ci_language line="Campaign Id"}</span>
                                </label>
                                <div class="col-md-9">                                
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" rel="campaignName" readonly style="background: #ddd;">
                                            <input type="hidden" class="form-control" name="campaignId" rel="campaignId">
                                        </div>
                                    </div>
                                    <p class="poor-gray m-t5">{ci_language line="The ID of the campaign to which this keyword belongs"}</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="true-pink">{ci_language line="Negative Keyword Text"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group withIcon clearfix ">
                                            <input type="text" {*placeholder="{ci_language line="add more keyword"}"*} class="form-control tags-input" {*data-role="tagsinput"*} name="keywordText[]" />
                                            <i class="cb-plus good addKeywordText"></i>
                                            <i class="cb-plus bad removeKeywordText" style="cursor: pointer;display:none"></i>
                                        </div>
                                    </div>
                                </div>
                                <p class="poor-gray m-t0">
                                    {ci_language line="The expression to match against search queries."} 
                                    {ci_language line="You can have up to 1000 keywords to be created."} 
                                </p>
                            </div>
                        </div>  
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="Match Type"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">

                                        <label class="cb-radio">
                                            </i>
                                            <input type="radio" value="negativeExact" name="matchType" checked="checked"/>
                                            {ci_language line="Negative Exact"}
                                        </label>
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="negativePhrase" name="matchType"/>
                                            {ci_language line="Negative Phrase"}
                                        </label>                                       
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="The match type used to match the keyword to search query"}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label class="text-uc dark-gray montserrat col-md-3 text-right p-t10" >
                                <span class="b-blue">{ci_language line="State"}</span>
                            </label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="cb-radio">
                                            </i>
                                            <input type="radio" value="enabled" name="state" checked="checked"/>
                                            {ci_language line="Enabled"}
                                        </label>                                        
                                        <label class="cb-radio m-l10">
                                            </i>
                                            <input type="radio" value="archived" name="state"/>
                                            {ci_language line="Archived"}
                                        </label>
                                    </div>
                                </div>
                                <p class="poor-gray m-t5">{ci_language line="Advertiser-specified state of the ad group"}</p>
                            </div>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="b-Top p-t20">
                                <button class="btn btn-save pull-right m-r25" rel="form_submit" onclick="create_keyword(this, 'negative');" >
                                    {ci_language line="Create Negative Keyword"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
</div> 
      
<script type="text/javascript" src="{$cdn_url}assets/js/tagsinput/bootstrap-tagsinput.min.js"></script> 
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/advertising.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}