{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

{include file="amazon/PopupStepHeader.tpl"}  
    
    {include file="amazon/PopupStepMoreHeader.tpl"} 

    <div id="main" class="row showSelectBlock amazon-models">
        <div class="col-md-12 col-lg-12 custom-form m-t10">
            <button type="button" class="link removeProfile remove">{ci_language line="Remove a model from list"}</button>

            <div class="profileBlock clearfix amazon-model">

                {if !isset($popup)}
                    <div class="validate blue">
                        <div class="validateRow">
                            <div class="validateCell">
                                <i class="note"></i>
                            </div>
                            <div class="validateCell">
                                <p class="pull-left">
                                {ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
                                </p>
                            </div>
                        </div>
                    </div>
                {/if}

                <!-- Model Name -->
                <p class="poor-gray">{ci_language line="Model name"}</p>
                <div class="form-group">
                    <input class="form-control" type="text" rel="name" value="{if isset($model_name)}{$model_name}{/if}" />
                </div>
                <p class="poor-gray text-right">
                    {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
                </p>

                <!-- Duplicate Model -->
                {if isset($models)}
                    <p class="poor-gray">{ci_language line="Duplicate From"}</p>
                    <div class="form-group">
                        <select id="duplicate-model" rel="duplicate-model" disabled>
                            <option value="">{ci_language line="Select a model"}</option>
                            {foreach from=$models item=models_item}
                                <option value="{$models_item.id_model}">{$models_item.name}</option>
                            {/foreach} 
                        </select> 
                    </div>
                    <p class="poor-gray text-right"> {ci_language line="Choose a model to duplicate"}</p>
                {/if}  

                <!-- Product Universe -->
                <p class="poor-gray">{ci_language line="Product Universe"}</p>
                <div class="form-group">
                    <select rel="universe" disabled data-placeholder="Choose">
                        {foreach from=$product_type_options item=product_category}
                           {$product_category} 
                        {/foreach} 
                    </select>                    
                </div>
                <p class="poor-gray text-right">{ci_language line="Choose the main (Root Node) universe for this model."}
                    <a href="https://sellercentral-europe.amazon.com/gp/help/200333160" target="_blank">{ci_language line="Learn more"}</a></p>
                <!--Product Type-->
                <div rel="product_type_container">
                    <p class="poor-gray">{ci_language line="Product Type"}</p>
                    <div class="form-group">
                        <select rel="product_type" disabled data-placeholder="Choose">
                            {foreach from=$product_type_options item=option}
                                {$option}
                            {/foreach} 
                        </select>                           
                    </div>
                    <p class="poor-gray text-right">{ci_language line="Choose the product type for this model"}.</p>
                </div>

                <!--Product Sub Type-->
                <div rel="product_subtype_container"></div>

                <!--mfr_part_number-->
                <div rel="mfr_part_number_container"></div>

                <!--Variation-->
                <div rel="variation_theme_container"></div>

                <!--Variation Data-->
                <div  rel="variation_data"></div>

                <!--Recommended Data-->
                <div rel="recommended_data"></div>

                <!-- Specific -->
                
                <div class="specific_fields_options">
                    <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                        {ci_language line="Specific Fields"}
                    </p>
                    <div class="row">
                        <div class="col-xs-12 m-b10">
                            <div class="form-group withIcon m-t20">
                                <select rel="specific_options" disabled data-placeholder="Choose">
                                    {if isset($specific_options)}{$specific_options}{/if}
                                </select>                                   
                                <i class="cb-plus good addSpecificField"></i>
                                <p class="poor-gray text-right">
                                    {ci_language line="Choose the value in specific fields and then click ADD button (+) to add more fields for this model."}
                                </p>                                                                      
                                <p class="poor-gray">
                                    <span class="addSpecificField_Loading" style="display:none">
                                        <i class="fa fa-spinner fa-spin"></i>
                                        {ci_language line="Loading"}..
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>  

                <div rel="specific_fields" class=""></div>
                             
            </div>
        </div>
    </div>
    
    <form id="amazon-form" method="POST" action="{$base_url}amazon/save_data" name="amazon-form" >

        <div id="tasks">

            {if isset($model) && !empty($model) }
                {foreach from=$model key=k item=data}
                    <div class="row amazon-models">

                        <div class="col-xs-12">
                            <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                                <p class="text-uc dark-gray montserrat pull-sm-left p-t10 m-b0">
                                    {if isset($data.name)}{$data.name}{/if} : 
                                    <span>
                                    {if isset($data.selected_universe)}{$data.selected_universe}{/if}
                                    {if isset($data.selected_product_type) && !empty($data.selected_product_type)} 
                                        &nbsp;<i class="fa fa-angle-right"></i> 
                                        {$data.selected_product_type}
                                    {/if}
                                    {if isset($data.selected_product_subtype) && !empty($data.selected_product_subtype)}
                                        &nbsp;<i class="fa fa-angle-right"></i> 
                                        {$data.selected_product_subtype}
                                    {/if}
                                    </span>
                                </p>
                                <ul class="pull-sm-right amazonEdit">
                                    <li class="editProfile">
                                        <button type="button" class="link editProfile_link edit_model" value="{$data.id_model}" onclick="edit(this,'{$data.key}');">
                                            <div class="content"><i class="fa fa-pencil"></i> {ci_language line="edit"}</div>
                                            <div class="edit-loading" style="display:none"><i class="fa fa-spinner fa-spin spin-loading"></i></div>
                                            
                                        </button>
                                        <!-- a class="editProfile_link" href="">edit</a> -->
                                    </li>
                                    <li class="remProfile">
                                        <button type="button" class="link removeProfile_link remove_model" rel="{$data.name}" value="{$data.id_model}">
                                            <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                                        </button>  
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="content_{$data.key}"></div>
                        {*<div class="col-md-12 col-lg-12 custom-form">
                            <div  rel="{$k}" class="profileBlock clearfix amazon-model showSelectBlock m-t10 m-b30">

                                <input type="hidden" rel="id_model" name="model[{$k}][id_model]" id="model_{$k}_id_model"  value="{$data.id_model}" />
                                <input type="hidden" rel="memory_usage" name="model[{$k}][memory_usage]" id="model_{$k}_memory_usage" data-content="{$k}" />     

                                <!-- Model Name -->
                                <p class="poor-gray">{ci_language line="Model name"}</p>
                                <div class="form-group">
                                    <input type="text" rel="name" value="{if isset($data.name)}{$data.name}{/if}" name="model[{$k}][name]" id="model_{$k}_name" data-content="{$k}" class="form-control"/>  
                                </div>
                                <p class="poor-gray text-right">
                                    {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
                                </p>

                                <!-- Product Universe -->
                                <p class="poor-gray">{ci_language line="Product Universe"}</p>
                                <div class="form-group">
                                    {if isset($data.universe)}
                                        <select rel="universe" name="model[{$k}][universe]" id="model_{$k}_universe" data-content="{$k}" class="search-select" data-placeholder="Choose">
                                            {foreach from=$data.universe item=universe}
                                               {$universe} 
                                            {/foreach} 
                                        </select>
                                    {/if}                   
                                </div>
                                <p class="poor-gray text-right">{ci_language line="Choose the main (Root Node) universe for this model."}</p>

                                <!--Product Type-->
                                <div rel="product_type_container">
                                    <p class="poor-gray">{ci_language line="Product Type"}</p>
                                    <div class="form-group">
                                        {if isset($data.product_type_options)}
                                            <select rel="product_type" name="model[{$k}][product_type]" id="model_{$k}_product_type" data-content="{$k}" class="search-select" data-placeholder="Choose">
                                                {foreach from=$data.product_type_options item=option}
                                                    {$option}
                                                {/foreach} 
                                            </select>
                                        {/if}                                                            
                                    </div>
                                    <p class="poor-gray text-right">{ci_language line="Please select the main category for this model"}.</p>
                                </div>

                                <!--Product Sub Type-->
                                <div rel="product_subtype_container" id="model_{$k}_product_subtype_container" data-content="{$k}">
                                    {if isset($data.product_subtype)}{$data.product_subtype}{/if}
                                </div>

                                <!--mfr_part_number-->
                                <div rel="mfr_part_number_container" id="model_{$k}_mfr_part_number_container" data-content="{$k}">
                                    {if isset($data.mfr_part_number)}                                    
                                        {$data.mfr_part_number}
                                    {/if}
                                </div>

                                <!--Variation-->
                                <div rel="variation_theme_container" id="model_{$k}_variation_theme_container" data-content="{$k}">
                                    {if isset($data.variation_theme)}
                                        <p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">
                                            {ci_language line="Variation"}
                                        </p>
                                        {$data.variation_theme}
                                    {/if}
                                </div>

                                <!--Variation Data-->
                                <div  rel="variation_data" id="model_{$k}_variation_data" data-content="{$k}">
                                    {if isset($data.variation_data)}{$data.variation_data}{/if}
                                </div>

                                <!--Recommended Data-->
                                <div rel="recommended_data" id="model_{$k}_recommended_data" data-content="{$k}">
                                    {if isset($data.recommended_data)}
                                        <p class="head_text p-t20 p-b20 b-Top clearfix m-b10" style="display: block;">
                                            {ci_language line="Recommended Fields"}
                                        </p>
                                        {$data.recommended_data}
                                    {/if}
                                </div>

                                <!-- Specific -->
                                <div class="specific_fields_options" id="model_{$k}_specific_fields_options" data-content="{$k}">
                                    <p class="head_text p-t20 p-b20 b-Top clearfix m-0">
                                        {ci_language line="Specific Fields"}
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12 m-b10">
                                            <div class="form-group withIcon m-t20">
                                                <select rel="specific_options" name="model[{$k}][specific_options]" id="model_{$k}_specific_options" data-content="{$k}" class="search-select" data-placeholder="Choose">
                                                    {if isset($data.specific_options)}{$data.specific_options}{/if} 
                                                </select> &nbsp; 
                                                <i class="cb-plus good addSpecificField"></i>
                                                <p class="poor-gray text-right">
                                                    {ci_language line="Choose the value in specific fields and then click ADD button (+) to add more fields for this model."}
                                                </p>                                                                      
                                                <p class="poor-gray">
                                                    <span class="addSpecificField_Loading" style="display:none">
                                                        <i class="fa fa-spinner fa-spin"></i>
                                                        {ci_language line="Loading"}..
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div rel="specific_fields" id="model_{$k}_specific_fields" data-content="{$k}">
                                    {if isset($data.specific_fields)}{$data.specific_fields}{/if}
                                </div>                             
                            </div>
                        </div>*}
                    </div>
                {/foreach}                         
            {/if}
            
        </div>

        {include file="amazon/PopupStepMoreFooter.tpl"} 
            
        <!--Message-->
        <input type="hidden" id="recommened_fields" value="{ci_language line="Recommended Fields"}" />
        <input type="hidden" id="variation" value="{ci_language line="Variation"}" />
        <input type="hidden" id="submit-confirm" value="{ci_language line="Do you want to save 'Models'?"}" />           
        <input type="hidden" id="error-message-title" value="{ci_language line="Some value are require."}" />
        <input type="hidden" id="error-message-text" value="{ci_language line="Please set a require value in model."}" />
        <input type="hidden" id="confirm-delete-model-message" value="{ci_language line="Do you want to delete"}" />
        <input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
        <input type="hidden" id="delete-success-message" value="{ci_language line="Model was deleted"}" />
        <input type="hidden" id="error-message" value="{ci_language line="Error"}" />
        <input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />
        <input type="hidden" id="selected_variation_data" value="{if isset($mapping_variation_data)}{$mapping_variation_data}{/if}" />
        <input type="hidden" id="selected_specific_fields" value="{if isset($mapping_specific_fields)}{$mapping_specific_fields}{/if}" />
        <input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
        <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
        {if isset($popup_more)}<input type="hidden" id="popup_more" name="popup_more" value="{$popup_more}" />{/if}
        <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
    </form>
            
{include file="amazon/PopupStepFooter.tpl"}
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">           
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/amazon/Amazon.js"></script>
<script src="{$cdn_url}assets/js/FeedBiz/amazon/models.js"></script> 

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}