{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css2/admin/module.admin.page.tables.min.css" />

<div {if !isset($popup)}id="content"{/if}>    
    {include file="amazon/PopupStepHeader.tpl"}    
<<<<<<< .mine
        {*<div class="row-fluid bg-white innerAll margin-bottom-none clearfix"> 
            <div class="pull-left text-left">
                <h3 class="heading">
                    {$country}
                </h3>  
            </div>
        </div>{*row-fluid*}
=======
>>>>>>> .r3072

    <!--Last Error Log-->
    {if isset($error_logs)}
        <div class="innerAll bg-default">
            <p><strong>{ci_language line="Last error from amazon"}</strong> </p>
            <div>{ci_language line="Batch ID"} : {$error_logs.batch_id}</div>
            <div>{ci_language line="Last Send Date"} : {$error_logs.date}</div>
            <div class=" space-10"></div>              
            {if isset($error_logs.error) & !empty($error_logs.error)}
                <div class="widget widget-inverse">
                    <div class="widget-head">
                        <h5 class="heading">
                            <i class="fa fa-warning-sign"></i> 
                            {ci_language line="Error report"}
                        </h5>
                    </div>

                    <div class="widget-body">
                        <table id="error_logs" class="dynamicTable colVis table table-striped ">
                            <thead class="bg-gray">
                                <tr>
                                    <th class="center">{ci_language line="Submission Feed ID"}</th>
                                    <th class="center">{ci_language line="SKU"}</th>
                                    <th class="center">{ci_language line="Code"}</th>
                                    <th class="center">{ci_language line="Message"}</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$error_logs.error item=error_item}
                                    <tr>
                                        <td>{$error_item.id_submission_feed}</td>
                                        <td>{$error_item.sku}</td>
                                        <td>{$error_item.result_message_code}</td>
                                        <td>{$error_item.result_description}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>    
            {/if}
        </div>   
        <div class=" space-10"></div>     
    {/if}

    <!--Product-->
    <div class="widget widget-inverse">
        <div class="widget-head">
            <h5 class="heading">
                {ci_language line="Feed report"}
            </h5>
        </div>

        <div class="widget-body">
            <table id="product_logs" class="dynamicTable colVis table table-striped " >
                <thead class="bg-gray">
                    <tr>
                        <th class="center">{ci_language line="Batch ID"}</th>
                        <th class="center">{ci_language line="Action type"}</th>
                        <th class="center">{ci_language line="Feed Type"}</th>
                        <th class="center">{ci_language line="Submission ID"}</th>
                        <th class="center">{ci_language line="Processed"}</th>
                        <th class="center">{ci_language line="Successful"}</th>
                        <th class="center">{ci_language line="Error"}</th>
                        <th class="center">{ci_language line="Warning"}</th>
                        <th class="center">{ci_language line="Send date"}</th>
                    </tr>
                </thead>
                <tbody>
                    {if isset($logs)}
                        {foreach from=$logs key=key item=value}
                            <tr>
                                <td>{$value.batch_id}</td>
                                <td>{if isset($value.action_type)}{$value.action_type}{/if}</td>
                                <td>{if isset($value.feed_type)}{$value.feed_type}{/if}</td>
                                <td>{if isset($value.feed_sunmission_id)}{$value.feed_sunmission_id}{/if}</td>
                                <td>{if isset($value.messages_processed)}{$value.messages_processed}{else}0{/if}</td>
                                <td>{if isset($value.messages_successful)}{$value.messages_successful}{else}0{/if}</td>
                                <td>
                                    {if isset($value.messages_with_error) && $value.messages_with_error > 0}
                                        {if isset($value.feed_type) && $value.feed_type != 'OrderFulfillment' }
                                            <a href="{$base_url}/amazon/downloadLog/{$id_country}/{$id_shop}/{$value.batch_id}/{$value.feed_sunmission_id}">
                                        {else}
                                            <a href="{$base_url}/amazon/downloadOrderLog/{$id_country}/{$id_shop}/{$value.batch_id}">
                                        {/if}
                                            <span class="red">{$value.messages_with_error}</span>
                                        </a>
                                    {else}0{/if}
                                </td>
                                <td>{if isset($value.messages_with_warning)}{$value.messages_with_warning}{else}0{/if}</td>
                                <td>{$value.datetime}</td>
                            </tr>
                        {/foreach}
                    {else}
                         <tr>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                             <td></td>
                        </tr>
                    {/if}
                </tbody>
            </table>
        </div>
    </div>

    {include file="amazon/PopupStepFooter.tpl"}  
    
</div> 
        
<!--page specific plugin scripts-->
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/js/jquery.dataTables.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/extras/TableTools/media/js/TableTools.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/lib/extras/ColVis/media/js/ColVis.min.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/custom/js/DT_bootstrap.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/datatables/assets/custom/js/datatables.init.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-select/assets/lib/js/bootstrap-select.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-select/assets/custom/js/bootstrap-select.init.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/tables/classic/assets/js/tables-classic.init.js?v=v1.2.3"></script>

<script type="text/javascript" src="{$base_url}assets/js/FeedBiz/amazon/report.js"></script>

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"} 