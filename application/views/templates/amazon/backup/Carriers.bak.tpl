{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}>     
    {include file="amazon/PopupStepHeader.tpl"}
    {*<div class="row-fluid bg-white innerAll margin-bottom-none clearfix "> 
        <div class="pull-left text-left">
            <h3 class="heading">
                <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" style="border: 1px solid #FFF;"/>
                {$country}
            </h3>  
        </div>
    </div>{*row-fluid*}
            
    <div class="innerAll spacing-x2 ">
        
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i>
            {ci_language line="Associated carrier, usefull to display your preferate carrier on the order page and on the invoice "}
        </div>
    
        <form id="form-mapping" method="post" action="{$base_url}amazon/save_carriers" enctype="multipart/form-data" >
            
            <!--Carrier--> 
            {if isset($carriers) && isset($amazon_carrier)}
                <div id="carrier-mapping" class=" form-horizontal">

                    <div class="widget widget-inverse clearfix margin-bottom-none" >
                        <div class="widget-head">
                            <h3 class="heading">
                                {ci_language line="Carriers Mapping"}
                            </h3>
                            <div class="pull-right">
                                <a class="btn btn-sm collapsible white">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            

<<<<<<< .mine
=======
                            <div class="innerAll half bg-gray text-small">
                                {ci_language line="Associated carrier, usefull to display your preferate carrier on the order page and on the invoice "}
                            </div>

>>>>>>> .r2733
                            <div id="carrier-group">

                                <div class="row-fluid center">

                                    <div id="main-carrier" class="clearfix innerTB half"> 
                                        <div class="col-xs-5">
                                            <select rel="carrier-select" disabled class="carrier-select-value form-control">
                                                <option value=""><span style="color: #f5f5f5">{ci_language line="Choose Amazon Carrier"}</span></option>
                                                {if isset($amazon_carrier)}
                                                    {foreach from=$amazon_carrier key=ckey item=cvalue}
                                                        <option value="{$ckey}">{$cvalue}</option>
                                                    {/foreach} 
                                                {/if}
                                            </select> 
                                        </div>
                                        <div class="col-xs-1"><i class="fa fa-chevron-right" style="margin-top:10px"></i></div>
                                        <div class="col-xs-6">
                                            <div class="col-xs-10 display-block-inline">
                                                <select rel="carrier-value" disabled class="carrier-value form-control">
                                                    <option value="">{ci_language line="Choose Carrier"}</option>
                                                    {if isset($carriers)}
                                                        {foreach $carriers key=akey item=value}
                                                            <option value="{$akey}">{$value.name}</option>
                                                        {/foreach} 
                                                    {/if}
                                                </select>
                                            </div>
                                            <div class="col-xs-2 display-block-inline">
                                                <a class="addnewmappingcarrier btn btn-stroke btn-success" rel="" style="cursor: pointer">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                                <a class="removemappingcarrier btn btn-stroke btn-danger" rel="" style="display:none;">
                                                    <i class="fa fa-minus"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div> {*main-carrier*}

                                    <div id="new-mapping-carrier">

                                        {if isset($mapping_carrier)}                                                   
                                            {foreach from=$mapping_carrier key=vk item=attr}
                                                {if $attr.selected == true}
                                                    <div class="row-odd-even clearfix innerTB half">

                                                        <div class="col-xs-5">
                                                            <select rel="carrier-select" class="carrier-select-value form-control" data-content="{$vk}" id="carrier-select-{$vk}">
                                                                <option value="">{ci_language line="Choose a attribute"}</option>
                                                                {foreach from=$amazon_carrier key=ckey item=cvalue}
                                                                    <option value="{$ckey}" {if $cvalue == $attr.mapping && $attr.selected == true}selected{/if}>{$cvalue}</option>
                                                                {/foreach} 
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1"><i class="fa fa-chevron-right" style="margin-top:10px"></i></div>
                                                        <div class="col-xs-6">
                                                            <div class="col-xs-10 display-block-inline">
                                                                 <select rel="carrier-value" class="carrier-value form-control" id="carrier-value-{$vk}" name="carrier[{$vk}][id_carrier]">
                                                                    {if isset($carriers)}
                                                                        {foreach from=$carriers key=akey item=value}
                                                                        <option value="{$akey}" {if $akey == $attr.id_carrier && $attr.selected == true}selected{/if}>{$value.name}</option>
                                                                        {/foreach} 
                                                                    {/if}
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-2 display-block-inline">
                                                                <a class="removemapping btn btn-stroke btn-danger" rel="">
                                                                    <i class="fa fa-minus"></i>
                                                                </a>
                                                            </div>
                                                        </div>                                                  
                                                     </div>
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    </div>

                                </div>

                            </div>
                                    
                        <!--Action-->
                        <div class="{if isset($popup)}popup_action{else}form-actions center no-margin{/if} clearfix">
                            <div class="inline text-left col-xs-6">
                                {if isset($popup)}
                                    <div class="inline pull-left">
                                         <button class="btn btn-light" type="button" disabled>
                                             <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                         </button>
                                     </div>
                                {else}
                                     <button class="btn text-left btn-light" id="back" type="button" >
                                         <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                     </button>
                                {/if}

                                <!--Message-->
                                <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                                <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                            </div>
                            <div class="inline text-right col-xs-6">                            
                                <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                                {if !isset($popup)}
                                    <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'; ">
                                         <i class="fa fa-save"></i> {ci_language line="Save"}
                                    </button> &nbsp;                          
                                {/if}
                                <button class="btn {if isset($popup_more) && $popup_more != '' }btn-danger{else}btn btn-info{/if}" id="save_continue_data" type="submit" style="margin: 0;">
                                    {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if} 
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                                {if isset($popup_more) && $popup_more != '' }
                                    <button class="btn btn-info" type="button" disabled>
                                        {ci_language line="Next"} 
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                {/if}
                            </div>
                        </div>

                        </div><!--/widget-body-->
                    </div>

                    <!-- end if not popup show widget footer-->
                </div> 
            {/if}
        </form>
    </div>  <!-- innerAll -->
                
    {include file="amazon/PopupStepFooter.tpl"}
    
</div>
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">           
<script src="{$base_url}assets/components/modules/admin/widgets/widget-collapsible/assets/widget-collapsible.init.js?v=v1.2.3"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}

<script src="{$base_url}assets/js/FeedBiz/amazon/carriers.js"></script>