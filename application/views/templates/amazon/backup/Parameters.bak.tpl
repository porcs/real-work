{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}>     
    {include file="amazon/PopupStepHeader.tpl"}
    <div class="innerAll spacing-x2 ">		
        <form id="form-api-setting" method="post" action="{$base_url}amazon/parameters" enctype="multipart/form-data" autocomplete="off" >
            <div class="widget widget-inverse clearfix">

                <div class="widget-head">
                    <h3 class="heading">
                        <img src="{$base_url}assets/images/geo_flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.gif" style="border: 1px solid #FFF;"/> &nbsp;
                        {$country}
                    </h3>
                    <div class="pull-right">
                        <small class="muted"> {ci_language line="Debug mode"} : </small>
                        <div class="make-switch" data-on="success" data-off="default">
                            <input name="active" id="debug" type="checkbox" value="1" {if isset($api_settings['active']) && $api_settings['active'] == 0}checked {/if}>
                        </div>
                    </div>
                </div><!-- Widget Heading -->

                <div class="widget-body">
                               
                    <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                    <div class="form-horizontal">
                        <div class="alert alert-warning" id="parems-message" style="display: none;">
                            <span></span>
                        </div>
                        
                        {if (isset($mode_default) && $mode_default == 2)}
                            <h4 class="innerAll half heading-buttons border-bottom">
                                {ci_language line="Preference"}
                            </h4>
                            <div class="form-group">
                                <label class="col-xs-3 control-label"></label>
                                <div class="col-xs-9">
                                    <div class="uniformjs">
                                        <label class="checkbox">
                                            <div class="checker">
                                                <span>
                                                    <input name="allow_automatic_offer_creation" type="checkbox" class="checkbox" value="1" {if isset($api_settings['allow_automatic_offer_creation']) && $api_settings['allow_automatic_offer_creation'] == 1}checked {/if} style="opacity: 0;">
                                                </span>
                                            </div>
                                            {ci_language line="Allow automatic offer creation."}
                                        </label>
                                        <label class="checkbox">
                                            <div class="checker">
                                                <span>
                                                    <input name="synchronize_quantity" type="checkbox"  class="checkbox" value="1" {if isset($api_settings['synchronize_quantity']) && $api_settings['synchronize_quantity'] == 1}checked {/if}  style="opacity: 0;">
                                                </span>
                                            </div>
                                            {ci_language line="Synchronize Quantity."}
                                        </label>
                                        <label class="checkbox">
                                            <div class="checker">
                                                <span>
                                                    <input name="synchronize_price" type="checkbox" class="checkbox" value="1" {if isset($api_settings['synchronize_price']) && $api_settings['synchronize_price'] == 1}checked {/if}  style="opacity: 0;">
                                                </span>
                                            </div>
                                            {ci_language line="Synchronize Price."}
                                        </label>
                                    </div>        
                                </div>
                            </div>
                        {/if}

                        <h4 class="innerAll half heading-buttons border-bottom">
                            {ci_language line="API Settings"}
                        </h4>

                        {if (isset($eu) && $eu == true) && !isset($popup)}
                            <div class="form-group">
                                <label class="col-xs-3 control-label"></label>
                                <div class="col-xs-9">
                                    <select class="span3 chzn-select" id="key_pairs" name="use_key_pairs_of">
                                        <option value="">{ci_language line="Enter API credentials (key pairs)"} </option>
                                        {if isset($key_pairs)}
                                            {foreach $key_pairs as $kp} 
                                                <option value="{$kp['ext']}" title="{$kp['id']}" {if isset($api_settings['ext']) && isset($api_settings['use_key_pairs_of']) && $api_settings['use_key_pairs_of'] == $kp['ext']}selected{/if}>
                                                    {ci_language line="Use key pairs of "} "amazon{$kp['ext']}"
                                                </option>
                                            {/foreach}
                                        {/if}
                                    </select>                                            
                                    <small class="text-error" style="margin-left: 5px"><i class="fa fa-asterisk red"></i></small>
                                    <div class=" help-block" style="display:none;">
                                        <b>"amazon<span class='ext'></span>"</b> {ci_language line="doesn't have api setting"}. 
                                        {ci_language line="Go to"}                                     
                                        <a>amazon<span class='ext'></span> &raquo; {ci_language line="parameters"}</a>
                                    </div>
                                </div>
                            </div>
                        {/if}

                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="merchant_id">
                                {ci_language line="Merchant ID"} :
                            </label>
                            <div class="col-xs-9">
                                <input class="form-control" type="text" id="merchant_id" name="merchant_id" {if isset($api_settings['merchant_id'])}value='{$api_settings['merchant_id']}'{/if}/>
                                {*<small class="text-error">*</small>*}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="aws_id">
                                {ci_language line="AWS Key Id"} :
                            </label>
                            <div class="col-xs-9">
                                <input class="form-control" type="text" id="aws_id" name="aws_id" {if isset($api_settings['aws_id'])}value='{$api_settings['aws_id']}'{/if}/>
                                {*<small class="text-error">*</small>*}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label" for="secret_key">
                                {ci_language line="AWS Secret Key"} :
                            </label>
                            <div class="col-xs-9">
                                <input class="form-control" type="password" id="secret_key" name="secret_key" {if isset($api_settings['secret_key'])}value='{$api_settings['secret_key']}'{/if}/>
                                {*<small class="text-error">*</small>*}
                                <span class="help-inline inline" style="padding: 8px;">
                                    {ci_language line="MWS URL for Keypairs"} : 
                                    <a href="https://sellercentral.amazon.fr/gp/mws/index.html">https://sellercentral.amazon.fr/gp/mws/index.html</a>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label"></label>
                            <div class="col-xs-9">
                                <button type="button" id="api_check" class="btn btn-inverse btn-stroke">
                                    <i class="fa fa-plug"></i>
                                    <span id="check">{ci_language line="Check Connectivity"}</span>
                                    <span id="checking" style="display:none">{ci_language line="Checking .."}</span>
                                </button>
                                <div class="space-10"></div>
                                
                                <div class="alert" style="display:none">                                    
                                    <button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>
                                    <ul id="message" class="spaced media-list"></ul>
                                </div>
                            </div>
                        </div>
                                
                                
                        {if isset($popup)}<div class="space-20"></div>{/if}
            
                        <div class="{if isset($popup)}popup_action{else}form-actions center no-margin{/if} clearfix">
                            <div class="inline col-xs-6 text-left">
                                {if isset($popup)}
                                    <button class="btn btn-default"  id="back" type="button">
                                        <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                    </button>
                                {/if}
                                <!--Message--> 
                                <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                                <input type="hidden" id="country" value="{if isset($country)}{$country}{/if}" />
                            </div>
                            <div class="inline col-xs-6 text-right">
                                <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                                {if !isset($popup)}                                
                                    <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'">
                                        <i class="fa fa-save"></i> {ci_language line="Save"}
                                    </button> &nbsp;                          
                                {/if}
                                <button class="btn btn-primary" id="save_continue_data" type="submit" >
                                    {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if}  
                                    <i class="fa fa-arrow-right"></i>
                                    <input type="hidden" id="ext" name="ext" value="{$ext}" />
                                    <input type="hidden" id="id_shop" name="id_shop" value="{$id_shop}" />
                                </button>
                            </div>
                        </div>
                        
                    </div><!--form-horizontal-->
                    
                </div><!--widget-body-->
            </div><!--widget-->
            
            

        </form>
    </div>  <!-- innerAll -->
                
    {include file="amazon/PopupStepFooter.tpl"}
    
</div>
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">    
<script src="{$base_url}assets/js/FeedBiz/amazon/parameters.js"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-switch/assets/lib/js/bootstrap-switch.js?v=v1.2.3"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/bootstrap-switch/assets/custom/js/bootstrap-switch.init.js?v=v1.2.3"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}