{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" type="text/css" href="{$base_url}assets/css/feedbiz/amazon/category.css">

<div {if !isset($popup)}id="content"{/if}>     
    {include file="amazon/PopupStepHeader.tpl"}    
    <div class="innerAll spacing-x2">
        <div class="alert alert-info">
            <i class="fa fa-info-circle"></i> {ci_language line='You can select many categories in one time by checking the first category then press "Shift" and click on the last element you wish to be in the same profile.'}
        </div>
        <form action="{$base_url}amazon/save_category{if isset($popup)}/{$popup}{/if}" method="post" id="category" autocomplete="off" novalidate>
            <div class="widget widget-inverse">
               {* {if !isset($popup)} *}
                    <!-- Widget Heading -->
                    <div class="widget-head">
                        <h3 class="heading show_thumbnails">
                             <img src="{$base_url}assets/images/geo_flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.gif" style="border: 1px solid #FFF;"/> &nbsp; {$country}
                        </h3>
                    </div>
                    <!-- // Widget Heading END -->
               {* {/if}*}

                <div class="widget-body">
                    <div class="row-fluid">
                        {*<div class="{if !isset($popup)} offset1 span10{else}span12{/if}">*}

                        <div id="category">
                            <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                            <div class="widget-box ">
                                <div class="widget-body padding-none">
<<<<<<< .mine
                                    
=======
                                    <div class="innerAll half bg-gray text-small">
                                        {ci_language line='You can select many categories in one time by checking the first category then press "Shift" and click on the last element you wish to be in the same profile.'}
                                    </div>
                                    <div class=" separator bottom"></div>

>>>>>>> .r2733
                                    <div class="col-md-12">
                                        <button id="expandAll" class="btn btn-success btn-stroke" type="button">
                                            <i class="fa fa-plus-square-o"></i>
                                            {ci_language line="Expand all"}
                                        </button>
                                        <button id="collapseAll" class="btn btn-warning btn-stroke" type="button">
                                            <i class="fa fa-minus-square-o"></i>
                                            {ci_language line="Collapse all"}
                                        </button>
                                        <button id="checkAll" class="btn btn-inverse btn-stroke" type="button">
                                            <i class="fa fa-check-square-o"></i>
                                            {ci_language line="Check all"}
                                        </button> 
                                        <button id="unCheckAll" class="btn btn-inverse btn-stroke" type="button">
                                            <i class="fa fa-square-o"></i>
                                            {ci_language line="Uncheck all"}
                                        </button>
                                    </div>
                                    <div class="widget-main">

                                        {if $prifile_mapping}
                                            <div id="main" style="display: none;">
                                                <div class="pull-right col-xs-5 col-md-4 col-md-3 col-lg-2">
                                                     <select class="col-xs-10">
                                                        <option value=""> - - </option>
                                                        {if (isset($profile) && !empty($profile) )}
                                                            {foreach from=$profile key=pkey item=pvalue}
                                                                <option value="{$pvalue.id_profile}" {if isset($selected) && $selected == $pvalue.id_profile} selected {/if}>{$pvalue.name}</option>
                                                            {/foreach}  
                                                        {/if}
                                                    </select>                                                      
                                                    <i class="fa fa-angle-double-down cp_down_btn"></i>                                                    
                                                </div>                                                
                                            </div>
                                        {/if}

                                        <div class="tree tree-unselectable" style="display: none" id="page-loaded">
                                            <div class="tree-folder" style="display: block">	
                                                <div class="tree-folder-header">					
                                                    <div class="tree-folder-name"> <i class="fa fa-folder-open orange"></i> &nbsp; {ci_language line="category"}</div>
                                                    {if !isset($popup)}
                                                        <div class="pull-right">
                                                            <div class="text-center">
                                                                <i class="fa fa-list-alt orange"></i>
                                                                {ci_language line="Profiles"}
                                                            </div>
                                                        </div>
                                                    {/if}
                                                </div>
                                            </div> 
                                        </div>

                                        <div class="clearfix center" id="page-load">
                                            <div class="space-10"></div>
                                            <i class="fa fa-spinner fa-spin blue fa-3x"></i>
                                        </div>

                                        <div id="tree1" class="tree tree-selectable overflow-visible"></div>

                                    </div>{*widget-main*}
                                    
                                     <!-- Action-->
                                    <div class="{if isset($popup)}popup_action{else}form-actions{/if} clearfix">
                                        <div class="col-xs-6 text-left">
                                            {if isset($popup)}
                                                <button class="btn btn-light" type="button" id="back">
                                                    <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                                </button>
                                            {else}
                                                <button class="btn pull-left btn-light" id="reset_data" type="button" >
                                                    <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                                </button>
                                            {/if}
                                            <!--Message-->
                                            <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                                        </div>
                                        <div class="col-xs-6 text-right">                            
                                            <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                                            {if !isset($popup)}
                                                <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'; ">
                                                    <i class="fa fa-save"></i> {ci_language line="Save"}
                                                </button> &nbsp;                          
                                            {/if}
                                            <button class="btn btn btn-info" id="save_continue_data" type="submit">
                                                {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if} 
                                                <i class="fa fa-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>                                    
                                    
                                </div>{*widget-body*}

                            </div>{*widget-box*}
                        </div>{*category*}

                    </div>{*span10*}

                </div>{*row-fluid*}

            </div>
            
        </form>
    </div>  <!-- innerAll -->

    {include file="amazon/PopupStepFooter.tpl"}
    
</div>
    
<script src="{$base_url}assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.min.js"></script>                        
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}

<!--page specific plugin scripts-->
{*<script src="{$base_url}assets/js/fuelux/fuelux.tree.offers.js"></script>*}
<script src="{$base_url}assets/js/jquery.blockUI.js"></script>
<script src="{$base_url}assets/js/FeedBiz/amazon/category.js"></script>