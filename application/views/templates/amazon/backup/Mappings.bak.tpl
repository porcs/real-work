{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}>     
    {include file="amazon/PopupStepHeader.tpl"}    
    
    
    {*if !isset($popup)}
     <div class="row-fluid bg-white innerAll margin-bottom-none clearfix "> 
        <div class="pull-left text-left">
            <h3 class="heading">
                {$country}
            </h3>  
        </div>
    </div>{*row-fluid*}
    {*/if*}
    
    
    
    <div class="innerAll spacing-x2">
        <div id="error-color" class="alert alert-warning" style="display: none;">
            <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
            </button>
            <strong>
                {ci_language line="Warning"} ! 
            </strong>, 
            {ci_language line="Please map all attribute color"}.
        </div>
    
        {if isset($mappings) && !empty($mappings)}{else}
            <div id="error-color" class="alert alert-warning" >
                <button type="button" class="close" data-dismiss="alert">
                    <i class="icon-remove"></i>
                </button>          
                <ul class=" list-regular">
                    <li>{ci_language line="They are no attribute to map"}.</li>
                    <li>{ci_language line="Please should category first"}.</li>
                    <li>{ci_language line="Note: some universe will have no attribute to map"}.</li>
                </ul>
            </div>
        {/if}
        <div class="widget widget-inverse clearfix">
            <div class="widget-body row">
        <form id="form-mapping" method="post" action="{$base_url}amazon/save_mappings" enctype="multipart/form-data" >
                
            {if isset($popup_more)}
                <h5 class="red">{ci_language line="Please mapping this attribute before send product to Amazon."}</h5>
            {/if}
            {*include file="amazon/PopupStepMoreHeader.tpl"*} 
           
            {*<pre>{print_r($mappings, true)}</pre>*}
            <!--Attribute--> 
            {if isset($mappings) && !empty($mappings)}
            <div id="attribute-mapping">                    
                {foreach $mappings as $key_mapping => $mapping}                        
                    <!--Product Type-->
                    {foreach $mapping as $key_product_type => $product_type}
                        
                        {if isset($product_type) && !empty($product_type)}
                        <div class="widget margin-bottom-none" data-toggle="collapse-widget">
                            <div class="widget-head">
                                <h3 class="heading">{$key_mapping} > {$key_product_type}</h3>
                                <span class="collapse-toggle"></span>
                            </div>

                            <div class="widget-body collapse in">
                                <!--Product Sub Type-->
                                {foreach $product_type as $key_product_sub_type => $product_sub_type}

                                    <!--Attribute-->
                                    {foreach $product_sub_type as $key_attribute => $attribute}
                                        {$valid = $product_sub_type['valid_value']}
                                        {if $key_attribute !== "valid_value"}
                                            <h4 class="clearfix blue">
                                                <div class="pull-left">{$key_attribute}</div>
                                                <div class="pull-right">{$key_product_sub_type}</div>
                                            </h4>
                                            <div class="hr hr-2 blue"></div>
                                            <div class="row-fluid form-horizontal">
                                                {if isset($attribute['attribute'])}
                                                    {foreach $attribute['attribute'] as $ak => $value}
                                                        <div style="padding: 5px 0" class="row-odd-even clearfix center">  
                                                            <div class="col-xs-5">
                                                                <select class="form-control"  style="margin-bottom: 0;">
                                                                    {if isset($attribute['is_color']) && $attribute['is_color'] == true}
                                                                        <option value="{$value.id_attribute}">{$value.attribute.color}</option>
                                                                    {else}
                                                                        <option value="{$value.id_attribute}">{$value.attribute}</option>
                                                                    {/if}
                                                                </select> 
                                                            </div>
                                                            <div class="col-xs-1"><i class="fa fa-chevron-right" style="margin: 0 10px"></i></div> 
                                                            <div class="col-xs-5">
                                                                {if isset($attribute['is_color']) && $attribute['is_color'] == true}
                                                                    
                                                                    <div class="attribute-color">
                                                                        <select rel="attribute-color" class="colorpicker" name="attribute[{$attribute['id_attribute_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]">
                                                                            <option value="" title="text"> -{ci_language line="select Color"}- </option>
                                                                           {if isset($valid)}
                                                                                {foreach $valid as $key_valid => $valid_value}
                                                                                    {$selected = false}
                                                                                    {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}])}
                                                                                        {$mapping = $value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]}
                                                                                        {if $mapping == $valid_value['value']}
                                                                                            {$selected = true}
                                                                                        {/if}                                                                            
                                                                                     {else}
                                                                                         {if $value.attribute.colorMap == $valid_value['value'] }
                                                                                             {$selected = true}
                                                                                         {/if}
                                                                                     {/if}
                                                                                    <option value="{$valid_value['value']}" title="text" {if $selected == true} selected {/if}>
                                                                                        {$valid_value['desc']}
                                                                                    </option>
                                                                                {/foreach}
                                                                            {/if}
                                                                        </select>
                                                                    </div>
                                                                {else}                                                                           
                                                                    <select class="form-control" name="attribute[{$attribute['id_attribute_group']}][{$value['id_attribute']}][{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]">
                                                                        <option value="">- {ci_language line="Mapping valid value"} -</option>
                                                                        {if isset($valid)}
                                                                            {foreach $valid as $key_valid => $valid_value}
                                                                                <option value="{$valid_value['value']}" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]) && $valid_value['value'] == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]}selected{/if}>{$valid_value['desc']}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    {/foreach}
                                                {/if}
                                                {if isset($attribute['feature'])}
                                                    {foreach $attribute['feature'] as $fk => $value}                                                        
                                                        <div style="padding: 5px 0" class="row-odd-even clearfix center">
                                                            <div class="col-xs-5">
                                                                <select class="form-control"  style="margin-bottom: 0;">                                                                    
                                                                    {if isset($attribute['is_color']) && $attribute['is_color'] == true}
                                                                        <option value="{$value.id_feature_value}">{$value.value.color}</option>
                                                                    {else}
                                                                        <option value="{$value.id_feature_value}">{$value.value}</option>
                                                                    {/if}
                                                                </select> 
                                                            </div>
                                                            <div class="col-xs-1"><i class="fa fa-chevron-right" style="margin: 0 10px"></i> </div>
                                                            <div class="col-xs-5">
                                                                {if isset($attribute['is_color']) && $attribute['is_color'] == true}
                                                                   <div class="attribute-color">
                                                                        <select rel="attribute-color" class="colorpicker" name="feature[{$attribute['id_feature']}][{$value['id_feature_value']}][{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]">
                                                                           {if isset($valid)}
                                                                                {foreach $valid as $key_valid => $valid_value}
                                                                                    {$selected = false}
                                                                                    {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}])}
                                                                                        {$mapping = $value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]}
                                                                                        {if $mapping == $valid_value['value']}
                                                                                            {$selected = true}
                                                                                        {/if}                                                                            
                                                                                     {else}
                                                                                         {if $value.value.colorMap == $valid_value['value'] }
                                                                                             {$selected = true}
                                                                                         {/if}
                                                                                     {/if}
                                                                                    <option value="{$valid_value['value']}" title="text" {if $selected == true} selected {/if}>
                                                                                        {$valid_value['desc']}
                                                                                    </option>
                                                                                {/foreach}
                                                                            {/if}
                                                                        </select> 
                                                                    </div>
                                                                {else}                                                                           
                                                                    <select class="form-control" name="feature[{$attribute['id_feature']}][{$value['id_feature_value']}][{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]">
                                                                        <option value="">- {ci_language line="Mapping valid value"} -</option>
                                                                        {if isset($valid)}
                                                                            {foreach $valid as $key_valid => $valid_value}
                                                                                <option value="{$valid_value['value']}" {if isset($value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]) && $valid_value['value'] == $value.mapping[{$key_mapping}][{$key_product_type}][{$key_product_sub_type}]}selected{/if}>{$valid_value['desc']}</option>
                                                                            {/foreach}
                                                                        {/if}
                                                                    </select>
                                                                {/if}
                                                            </div>
                                                        </div>
                                                    {/foreach}
                                                {/if}
                                            </div>
                                            <div class="space-10"></div>
                                        {/if}
                                    {/foreach}
                                {/foreach}
                            </div><!--/widget-body-->
                        </div>
                        {/if}
                    {/foreach}
                {/foreach}
                <!-- end if not popup show widget footer-->
<<<<<<< .mine
            </div>                
=======
            </div> 
            {else}
               {* <div id="error-color" class="alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>          
                    <ul class=" list-regular">
                        <li>{ci_language line="They are no attribute to map"}.</li>
                        <li>{ci_language line="Please should category first"}.</li>
                        <li>{ci_language line="Note: some universe will have no attribute to map"}.</li>
                    </ul>
                </div>*}
>>>>>>> .r2659
            {/if}      

            {*include file="amazon/PopupStepMoreFooter.tpl"*} 

            <div class='space-20'></div>

            <!--Action-->
            <div class="{if isset($popup)}popup_action{else}form-actions center no-margin{/if} clearfix">
                <div class="inline text-left col-xs-6">
                    {if isset($popup)}
                        <div class="inline text-left">
                             <button class="btn btn-light" type="button" disabled>
                                 <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                             </button>
                         </div>
                    {else}
                         <button class="btn pull-left btn-light" id="back" type="button" >
                             <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                         </button>
                    {/if}

                    <!--Message-->
                    <input type="hidden" id="error-message" value="{ci_language line="Mapping colors are require"}!" />
                    <input type="hidden" id="save-unsuccess-message" value="{ci_language line="Please map all attribute color."}" />
                    <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                    <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                </div>
                <div class="inline text-right col-xs-6">                            
                    <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                    {if !isset($popup)}
                        <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'; ">
                             <i class="fa fa-save"></i> {ci_language line="Save"}
                        </button> &nbsp;                          
                    {/if}
                    <button class="btn {if isset($popup_more) && $popup_more != '' }btn-danger{else}btn btn-info{/if}" id="save_continue_data" type="submit" style="margin: 0;">
                        {if isset($popup)}{ci_language line="Save Mapping & Continue"}{else}{ci_language line="Save and continue"}{/if} 
                        <i class="fa fa-arrow-right"></i>
                    </button>
                    {if isset($popup_more) && $popup_more != '' }
                        <button class="btn btn-info" type="button" disabled>
                            {ci_language line="Next"} 
                            <i class="fa fa-arrow-right"></i>
                        </button>
                    {/if}
                </div>

            </div>
            
                </form>
            </div>
        </div>
    </div>  <!-- innerAll -->
    {include file="amazon/PopupStepFooter.tpl"}
    
</div>
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">           

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}

<script type="text/javascript" src="{$base_url}assets/js/bootstrap-colorpicker.min.js"></script>
<script src="{$base_url}assets/components/modules/admin/widgets/widget-collapsible/assets/widget-collapsible.init.js?v=v1.2.3"></script>
<script src="{$base_url}assets/js/FeedBiz/amazon/mappings.js"></script>