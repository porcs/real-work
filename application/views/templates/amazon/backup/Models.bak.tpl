{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}>     
    {include file="amazon/PopupStepHeader.tpl"}    
    {*<div class="bg-white innerAll margin-bottom-none clearfix {if isset($popup_more) && $popup_more != ''} half {/if}"> 
        <div class="pull-left text-left">
            <h3 class="heading margin-none">
                <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" style="border: 1px solid #FFF;"/>
                {if isset($country)}{$country}{/if}
                {if isset($popup_more) && $popup_more != ''} <i class="fa fa-angle-right"></i> {ucwords($function)} {/if} 
            </h3>  
        </div>
        <div class="pull-right text-left">
            <span id="count">{if isset($model)}{sizeof($model)}{else}0{/if}</span>
            {ci_language line="Items(s)"} &nbsp;
            <button type="button" class="btn btn-inverse" id="add">
                <i class="icon-plus"></i>
                {ci_language line="Add a model"}
            </button>
        </div>
    </div>{*row-fluid*}
    
    <div class="innerAll spacing-x2">	            
       <div class="widget widget-inverse">
           <div class="widget-body row">
               <div class="row" style="margin-bottom:15px">
               <div class="col-xs-6 text-left">
                   <button type="button" class="btn btn-stroke btn-success" id="add">
                        <i class="icon-plus"></i>
                        {ci_language line="Add a model"}
                    </button>
               </div>
               <div class="col-xs-6 text-right">
                    <span id="count">{if isset($model)}{sizeof($model)}{else}0{/if}</span>
                    {ci_language line="Items(s)"}                    
                </div>
            </div>
            
            
        <form id="amazon-form" class="form-group col-md-12" method="POST" action="{$base_url}amazon/save_data" name="amazon-form" >
                    
            {include file="amazon/PopupStepMoreHeader.tpl"}

            <div class="form-horizontal">
                <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />

                <div id="main" class="widget" style="display: none;">
                    <div class="col-md-12 text-right" style="padding: 10px 0;right: 10px;">
                        <button class="btn btn-stroke btn-danger remove" type="button"><i class="fa fa-close"></i></button>
                    </div>

                    <div class="widget-body">

                        <div class="form-group has-error">
<<<<<<< .mine
                            <label class="col-md-2 control-label">{ci_language line="Model Name"}</label>
                            <div class="col-md-10">
=======
                            <label class="col-md-3 control-label">{ci_language line="Model name"}</label>
                            <div class="col-md-9">
>>>>>>> .r3009
                                <input class="form-control" type="text" rel="name" value="{if isset($model_name)}{$model_name}{/if}" />  
                                <p class="has-error help-block">{ci_language line="Set Model Name First"}</p>
                                <div class="help-block text-small">
                                   {ci_language line="Please give a friendly name to remind this model"}. <br/>
                                   {ci_language line="Do not forget to click on the save button at the bottom of the page !"}
                                </div>
                            </div>
                        </div>

                        {if isset($models)}
                        <div class="form-group">
                            <label class="col-md-2 control-label">{ci_language line="Duplicate From"}</label>
                            <div class="col-md-10">
                                <select class="chzn-select" id="duplicate-model" rel="duplicate-model" disabled>
                                    <option value="">{ci_language line="Select a model"}</option>
                                    {foreach from=$models item=models_item}
                                        <option value="{$models_item.id_model}">{$models_item.name}</option>
                                    {/foreach} 
                                </select> 
                                <div class="help-block text-small">
                                   {ci_language line="Choose a model to duplicate"}
                                </div>
                            </div>
                        </div>
                        {/if}  

                        <div class="form-group">
                            <label class="col-md-2 control-label">{ci_language line="Product Universe"}</label>
                            <div class="col-md-10">
                                <select rel="universe" disabled style="background: #eeeeee ;"  class="chzn-select">
                                    {foreach from=$product_type_options item=product_category}
                                       {$product_category} 
                                    {/foreach} 
                                </select>
                                <div class="help-block text-small">
                                   {ci_language line="Choose the main universe for this model"}.
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">{ci_language line="Product Type"}</label>
                            <div class="col-md-10">
                                <select rel="product_type" disabled style="background: #eeeeee ;"  class="chzn-select">
                                    {foreach from=$product_type_options item=option}
                                        {$option}
                                    {/foreach} 
                                </select>
                                <div class="help-block text-small">
                                    {ci_language line="Please select the main category for this model"}.
                                </div>
                            </div>
                        </div>

                        <div rel="product_subtype_container"></div>

                        <div rel="mfr_part_number_container"></div>

                        <div rel="variation_theme_container"></div>

                        <div  rel="variation_data"></div>

                        <div rel="recommended_data"></div>

                        <div class="specific_fields_options">
                            <h4 class="innerAll half heading-buttons border-bottom">{ci_language line="Specific"}</h4>

                            <div class="form-group">
                                <label class="col-md-2 control-label">{ci_language line="Specific Fields"}</label>
                                <div class="col-md-10">
                                    <select rel="specific_options" disabled style="background: #eeeeee ;" class="chzn-select">
                                        {if isset($specific_options)}{$specific_options}{/if}
                                    </select>  &nbsp; 
                                    <button class="btn btn-stroke btn-success addSpecificField" rel="2" type="button">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>

                        </div>

                        <div rel="specific_fields" class=""></div>
                    </div>
                </div>

                <div id="tasks">
 
                    {if isset($model) }
                        {foreach from=$model key=k item=data}
                            <div class="widget widget-inverse clearfix collapsed margin-bottom-none">

                                <div class="widget-head">
                                    <h3 class="heading ">
                                        <div class="marketplace" style="cursor: pointer;">
                                            {if isset($data.name)}{$data.name}{/if} : 
                                            <span>
                                            {if isset($data.selected_universe)}{$data.selected_universe}{/if}
                                            {if isset($data.selected_product_type) && !empty($data.selected_product_type)} 
                                                &nbsp;<i class="fa fa-angle-right"></i> 
                                                {$data.selected_product_type}
                                            {/if}
                                            {if isset($data.selected_product_subtype) && !empty($data.selected_product_subtype)}
                                                &nbsp;<i class="fa fa-angle-right"></i> 
                                                {$data.selected_product_subtype}
                                            {/if}
                                            </span>
                                        </div>
                                    </h3>                   
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-stroke btn-success marketplace"{* rel="{$data.name}" value="{$data.id_model}"*}>
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-stroke btn-danger remove_model" rel="{$data.name}" value="{$data.id_model}">
                                            <i class="fa fa-trash-o"></i>
                                        </button>                                             
                                    </div>
                                </div><!-- Widget Heading -->

                                <div class="widget-body">

                                    <div rel="{$k}">
                                        <input type="hidden" rel="id_model" name="model[{$k}][id_model]" id="model_{$k}_id_model"  value="{$data.id_model}" />
                                        <input type="hidden" rel="memory_usage" name="model[{$k}][memory_usage]" id="model_{$k}_memory_usage" data-content="{$k}" />

                                        <div class="form-group">
<<<<<<< .mine
                                            <label class="col-md-2 control-label">{ci_language line="Model Name"}</label>
=======
                                            <label class="col-md-3 control-label">{ci_language line="Model name"}</label>
>>>>>>> .r3009
                                            <div class="col-md-9">
                                                <input type="text" rel="name" value="{if isset($data.name)}{$data.name}{/if}" name="model[{$k}][name]" id="model_{$k}_name" data-content="{$k}" class="form-control"/>    
                                                <p class="has-error help-block" style="display: none;">{ci_language line="Set Model Name First"}</p>
                                                <div class="help-block text-small">
                                                   {ci_language line="Please give a friendly name to remind this model"}. <br/>
                                                   {ci_language line="Do not forget to click on the save button at the bottom of the page !"}
                                                </div>
                                            </div>
                                        </div>                                                

                                        <div class="form-group">
                                            <label class="col-md-10 control-label">{ci_language line="Product Universe"}</label>
                                            <div class="col-md-9">
                                                {if isset($data.universe)}
                                                    <select rel="universe" name="model[{$k}][universe]" id="model_{$k}_universe" data-content="{$k}" class="chzn-select">
                                                        {foreach from=$data.universe item=universe}
                                                           {$universe} 
                                                        {/foreach} 
                                                    </select>
                                                {/if}
                                                <div class="help-block text-small">
                                                    {ci_language line="Choose the main universe for this model"}.
                                                </div>
                                            </div>
                                        </div>  

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">{ci_language line="Product Type"}</label>
                                            <div class="col-md-10">
                                                {if isset($data.product_type_options)}
                                                <select rel="product_type" name="model[{$k}][product_type]" id="model_{$k}_product_type" data-content="{$k}" class="chzn-select">
                                                    {foreach from=$data.product_type_options item=option}
                                                        {$option}
                                                    {/foreach} 
                                                </select>
                                                {/if}
                                                <div class="help-block text-small">
                                                    {ci_language line="Please select the main category for this model"}.
                                                </div>
                                            </div>
                                        </div>  

                                        <div rel="product_subtype_container" id="model_{$k}_product_subtype_container" data-content="{$k}">
                                            {if isset($data.product_subtype)}{$data.product_subtype}{/if}
                                        </div>

                                        <div rel="variation_theme_container" id="model_{$k}_variation_theme_container" data-content="{$k}">
                                            {if isset($data.variation_theme)}
                                            <h4 class="header smaller lighter green">{ci_language line="Variation"}</h4>
                                            {$data.variation_theme}
                                            {/if}
                                        </div>

                                        <div  rel="variation_data" id="model_{$k}_variation_data" data-content="{$k}">
                                            {if isset($data.variation_data)}{$data.variation_data}{/if}
                                        </div>

                                        <div rel="recommended_data" id="model_{$k}_recommended_data" data-content="{$k}">
                                            {if isset($data.recommended_data)}
                                                <h4 class="header smaller lighter green">{ci_language line="Recommended Fields"}</h4>
                                                {$data.recommended_data}
                                            {/if}
                                        </div>

                                        <div class="specific_fields_options" id="model_{$k}_specific_fields_options" data-content="{$k}">
                                            <h4 class="header smaller lighter green">{ci_language line="Specific Fields"}</h4>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">{ci_language line="Specific Fields"}</label>
                                                <div class="col-md-10">
                                                    <select rel="specific_options" name="model[{$k}][specific_options]" id="model_{$k}_specific_options" data-content="{$k}"  class="chzn-select">
                                                        {if isset($data.specific_options)}{$data.specific_options}{/if} 
                                                    </select> &nbsp; 
                                                    <button class="btn btn-stroke btn-success addSpecificField" rel="2" type="button">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>                                                    
                                        </div>

                                        <div rel="specific_fields" id="model_{$k}_specific_fields" data-content="{$k}">
                                            {if isset($data.specific_fields)}{$data.specific_fields}{/if}
                                        </div>

                                    </div>  
                                </div><!--widget-body-->
                            </div><!--widget-->
                        {/foreach}                         
                    {/if}

                </div>
            </div>

            {include file="amazon/PopupStepMoreFooter.tpl"} 

            <div class="space-20"></div>
            
            <div class="{if isset($popup)}popup_action{else}form-actions center no-margin{/if} clearfix">
                <div class="inline text-left col-xs-6">
                    {if isset($popup)}
                        <div class="inline text-left">
                             <button class="btn btn-light" type="button" disabled>
                                 <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                             </button>
                         </div>
                    {else}
                         <button class="btn text-left btn-light" id="back" type="button" >
                             <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                         </button>
                    {/if}
                     
                    <!--Message-->
                    <input type="hidden" id="error-message-title" value="{ci_language line="Some value are require."}" />
                    <input type="hidden" id="error-message-text" value="{ci_language line="Please set a require value in model."}" />
                    <input type="hidden" id="confirm-delete-model-message" value="{ci_language line="Do you want to delete"}" />
                    <input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
                    <input type="hidden" id="delete-success-message" value="{ci_language line="Model was deleted"}" />
                    <input type="hidden" id="error-message" value="{ci_language line="Error"}" />
                    <input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />
                    <input type="hidden" id="selected_variation_data" value="{if isset($mapping_variation_data)}{$mapping_variation_data}{/if}" />
                    <input type="hidden" id="selected_specific_fields" value="{if isset($mapping_specific_fields)}{$mapping_specific_fields}{/if}" />
                    <input type="hidden" id="id_country" name="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                    <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                    <input type="hidden" id="mode" name="popup_more" value="{if isset($popup_more)}{$popup_more}{/if}" />
                    <input type="hidden" id="ext" value="{if isset($ext)}{$ext}{/if}" />
                </div>
                <div class="inline text-right col-xs-6">
                    <input type="hidden" id="save" name="save" {if isset($popup)}value="next"{else}value="continue"{/if} />
                    {if !isset($popup)}
                        <button class="btn btn-success" id="save_data" type="submit" onclick="document.getElementById('save').value = 'save'; ">
                            <i class="fa fa-save"></i> {ci_language line="Save"}
                        </button> &nbsp;                          
                    {/if}
                    <button class="btn {if isset($popup_more) && $popup_more != '' }btn-danger{else}btn btn-info{/if}" id="save_continue_data" type="submit" style="margin: 0;">
                        {if isset($popup)}{ci_language line="Save Models & Continue"}{else}{ci_language line="Save and continue"}{/if} 
                        <i class="fa fa-arrow-right"></i>
                    </button>
                    {if isset($popup_more) && $popup_more != '' }
                        <button class="btn btn-info" type="button" disabled>
                            {ci_language line="Next"}
                            <i class="fa fa-arrow-right"></i>
                        </button>
                    {/if}
                </div>
            </div>
                
        </form>
    </div>
    </div>
    </div><!-- innerAll -->
                
    {include file="amazon/PopupStepFooter.tpl"}
    
</div>
                        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions">           
<script type="text/javascript" src="{$base_url}assets/js/FeedBiz/amazon/Amazon.js"></script>

{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}
<script src="{$base_url}assets/js/FeedBiz/amazon/models.js"></script> 