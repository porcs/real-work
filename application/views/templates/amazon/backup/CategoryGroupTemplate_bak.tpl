<div class="tree-folder">				
    <div class="tree-folder-header"  >
        {if isset($type) && $type == "folder"}
            <i class="icon-minus add_category"></i>
        {/if}
        <div class="tree-folder-name">
            <label class="checkbox-custom">
                <i class="fa fa-fw fa-square-o" style="margin: 0"></i>
                <input type="checkbox" rel="id_category" name="category[{$id_category}][id_category]" id="category_{$id_category}" value="{$id_category}" class="checbox-tree-folder" />
                <script>
                    {if isset($checked) && ($checked == true)} 
                        $('#category_{$id_category}').checkbox('check');
                    {else} 
                        $('#category_{$id_category}').checkbox('uncheck');
                    {/if}
                </script>
                <span class="lbl" {if isset($type) && $type == "folder"}style="font-weight: bold;" {/if}>{$name}</span>
            </label>

            {if !isset($popup) || (isset($mode) && $mode == 2) }

                {if (isset($selected) && !empty($selected))}
                    <div class="pull-right col-xs-5 col-md-4 col-md-3 col-lg-2">
                        <select class="col-xs-10" name="category[{$id_category}][id_profile]" id="category{$id_category}_id_profile">
                            <option value=""> - - </option>
                            {if (isset($profile) && !empty($profile) )}
                                {foreach from=$profile key=pkey item=pvalue}
                                    <option value="{$pvalue.id_profile}" {if isset($selected) && $selected == $pvalue.id_profile} selected {/if}>{$pvalue.name}</option>
                                {/foreach}  
                            {/if}
                        </select>
                        <i class="fa fa-angle-double-down cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                    </div>

                    <script>
                        $("select#category{$id_category}_id_profile").select2();
                    </script>
                {else}
                    {if isset($checked)&& ($checked == true)  }
                        <div class="pull-right col-xs-5 col-md-4 col-md-3 col-lg-2">
                            <select class="col-xs-10" name="category[{$id_category}][id_profile]" id="category{$id_category}_id_profile_check">
                                <option value=""> - - </option>
                                <optgroup label="-- Action --">
                                    {foreach from=$profile key=pkey item=pvalue}
                                        <option value="{$pvalue.id_profile}">{$pvalue.name}</option>
                                    {/foreach}   
                                </optgroup>
                            </select>
                            <i class="fa fa-angle-double-down cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
                        </div>
                    {/if}

                    <script>
                        $("#category{$id_category}_id_profile_check").select2();
                    </script>
                {/if}
            {/if}
        </div>
    </div>				
    <div class="tree-folder-content">{if (isset($sub_html) && !empty($sub_html) )}{$sub_html}{/if}</div>
</div>    