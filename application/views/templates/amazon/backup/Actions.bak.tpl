{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<div {if !isset($popup)}id="content"{/if}>    
    {include file="amazon/PopupStepHeader.tpl"}    
    {*<div class="row-fluid bg-white innerAll margin-bottom-none clearfix"> 
        <div class="pull-left text-left">
            <h3 class="heading">
                {$country}
            </h3>  
        </div>
    </div>{*row-fluid*}
    
    
    <div class="innerAll spacing-x2">		
        
        {if (!isset($quantity) || !isset($price)) && (isset($mode_default) && $mode_default > 1)}
            <div class="alert alert-warning">
                <ul class=" list-regular list-unstyled ">
                    <li>
                        <i class="fa fa-exclamation-triangle"></i>
                        <b>{if !isset($quantity)}{ci_language line='Quantity'}{/if}
                        {if !isset($quantity) && !isset($price)}{ci_language line='and'}{/if}
                        {if !isset($price)}{ci_language line='Price'}{/if}</b>, 
                        {ci_language line='will not send to Amazon.'}
                        {ci_language line='To synchronize quantity or price please go on'}
                        <a href="{$base_url}amazon/parameters/{$id_country}">{ci_language line='Parameters'}</a>
                        {ci_language line='then check synchronize quantity / synchronize price and save'}.
                    </li>
                </ul>
            </div>
        {/if}
        <div id="actions" class="relativeWrap">
        <div class="widget widget-tabs widget-tabs-responsive">
	
		<!-- Tabs Heading -->
<<<<<<< .mine
		<div class="widget-head">
			<ul>
				<li class="active"><a class="glyphicons refresh" href="#tab-1" data-toggle="tab"><i></i>{ci_language line="Synchronization"}</a></li>
				{if isset($creation)}<li class=""><a class="glyphicons upload" href="#tab-2" data-toggle="tab"><i></i>{ci_language line="Creation"}</a></li>{/if}
				<li class=""><a class="glyphicons download" href="#tab-3" data-toggle="tab"><i></i>{ci_language line="Import Orders"}</a></li>
				{if (isset($mode_default) && $mode_default > 1)}<li class=""><a class="glyphicons bin" href="#tab-4" data-toggle="tab"><i></i>{ci_language line="Delete Products"}</a></li>{/if}
			</ul>
		</div>
		<!-- // Tabs Heading END -->
		
		<div class="widget-body">
			<div class="tab-content">
			
				<!-- Tab content synchronize -->
				<div class="tab-pane active" id="tab-1">					
                                    <h4 class="innerAll half heading-buttons border-bottom">{ci_language line='Synchronization'}</h4>
                                    <ul class="list-regular">
                                        <li>
                                            <b>{ci_language line="Amazon Synchronize Wizard"}</b>
                                            <p>{ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.'}</p>
                                        </li>
                                        <li>
                                            <b>{ci_language line="Update product offers"}</b>
                                            <p>{ci_language line='This will update Amazon depending your stocks moves, within the selected categories in your configuration.'}</p>
                                        </li>
                                    </ul>
                                    <hr/>
                                    <div class="status" style="display: none">
                                        <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                                    </div>
                                    <div id="sync_options">  
                                        
                                        <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
=======
		<div class="tabsbar">
                    <ul>
                        <li class="glyphicons refresh active">
                            <a href="#synchronize" data-toggle="tab" class="">
                                <i{* class="fa fa-refresh fa-3x orange"*}></i> 
                                <span>{ci_language line="Synchronization"}</span>
                            </a>
                        </li>
                        {if isset($creation)}
                            <li class="glyphicons upload">
                                <a data-toggle="tab" href="#creation">
                                    <i {*class="fa fa-upload fa-3x green"*}></i> 
                                    <span>{ci_language line="Creation"}</span>
                                </a>
                            </li>
                        {/if}
                        <li class="glyphicons download">
                            <a data-toggle="tab" href="#orders">
                                <i {*class="fa fa-download fa-3x blue"*}></i> 
                                <span>{ci_language line="Import Orders"}</span>
                            </a>
                        </li> 
                        {if (isset($mode_default) && $mode_default > 1)}
                            <li class="glyphicons bin">
                                <a data-toggle="tab" href="#delete">
                                    <i {*class="fa fa-trash fa-3x red"*}></i> 
                                    <span>{ci_language line="Delete Products"}</span>
                                </a>
                            </li>
                        {/if}
                    </ul>
                </div>
                
                <div class="tab-content">    

                    <!--synchronize-->
                    <div id="synchronize" class="tab-pane active">
                        <div class="space-4"></div>
                        <h4 class="orange">{ci_language line='Synchronization'}</h4>
                        <div class="space-10"></div>
                        <ul class="list-regular">
                            <li>
                                <b>{ci_language line="Amazon Synchronize Wizard"}</b>
                                <p>{ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.'}</p>
                            </li>
                            <li>
                                <b>{ci_language line="Update product offers"}</b>
                                <p>{ci_language line='This will update Amazon depending your stocks moves, within the selected categories in your configuration.'}</p>
                            </li>
                        </ul>
                        <hr/>
                        <div class="status" style="display: none">
                            <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                        </div>
                        <div id="sync_options">  
                            {if isset($message_code_inner.sync)}{$message_code_inner.sync}{/if}
                            <input type="hidden" id="sync" value="{if isset($sync)}{$sync}{/if}" />
>>>>>>> .r2659

<<<<<<< .mine
                                        <div class="form-actions clearfix">
                                            <div class="col-xs-6 text-left">
                                                {*parameters($country = null, $popup = null, $mode = 1)*}
                                                <button class="btn btn-primary btn-icon-stacked wizard_btn" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$sync_mode}" id="amazon_synchronize_wizard">
                                                    <i class="fa fa-magic fa-2x"></i>
                                                    <span class="center">
                                                        {ci_language line="Amazon Synchronize"} <br/>
                                                        {ci_language line="Wizard"}
                                                    </span>
                                                </button>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <button class="btn btn-success btn-icon-stacked" id="send-offers" type="button" value="synchronize">
                                                    <i class="fa fa-upload fa-2x"></i>
                                                    <span class="center send">
                                                        {ci_language line="Update product offers"} <br/>
                                                        {ci_language line="to Amazon"}
                                                    </span>
                                                </button>
                                            </div>
=======
                            <div class="clearfix">
                                <div class="pull-left">
                                    {*parameters($country = null, $popup = null, $mode = 1)*}
                                    <button class="btn btn-danger btn-icon-stacked wizard_btn" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$sync_mode}" id="amazon_synchronize_wizard">
                                        <i class="fa fa-magic fa-2x"></i>
                                        <span class="center">
                                            {ci_language line="Amazon Synchronize"} <br/>
                                            {ci_language line="Wizard"}
                                        </span>
                                    </button>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-info btn-icon-stacked" id="send-offers" type="button" value="synchronize">
                                        <i class="fa fa-upload fa-2x"></i>
                                        <span class="center send">
                                            {ci_language line="Update product offers"} <br/>
                                            {ci_language line="to Amazon"}
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--creation-->
                    {if isset($creation)}
                    <div id="creation" class="tab-pane">
                        <div class="space-4"></div>
                        <h4 class="green">{ci_language line='Creation'}</h4>
                        <div class="space-10"></div>
                        <ul class="list-regular">
                            <li>
                                <b>{ci_language line="Amazon Creation Wizard"}</b>
                                <p>{ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be created the unknown products on Amazon but present in your database. The goal is to create the unknown items on Amazon.'}</p>
                            </li>
                        </ul>
                        <hr/>
                        <div class="status" style="display: none">
                            <h5 class="blue"><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                        </div>
                        <div id="creat_options" class="clearfix">                        
                            <div class="pull-left">
                                <button class="btn btn-danger btn-icon-stacked wizard_btn" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$create_mode}" id="amazon_creation_wizard">
                                    <i class="fa fa-magic fa-2x"></i>
                                    <span class="center">
                                        {ci_language line="Amazon Creation"} <br/>
                                        {ci_language line="Wizard"}
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    {/if}

                    <!--orders-->
                    <div id="orders" class="tab-pane">
                        <div class="space-4"></div>
                        <h4 class="blue">{ci_language line="Import Orders"}</h4>
                        <div class="space-10"></div>
                        <p>{ci_language line='This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'}</p>                
                        <hr/>
                        {if isset($message_code_inner.carrier)}
                            {$message_code_inner.carrier}
                        {else}
                        <div>
                            <div class="status" style="display: none">
                                <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                            </div>

                            <div id="parameter_options">
                                <div class=" label label-info">{ci_language line="Parameters"}</div>                            
                                <div class="alert alert-info clearfix form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="order-date-range">{ci_language line='Order Date Range'} : </label>
                                        <div class="col-md-9">
                                            <div class="input-group date">
                                                <input class="form-control" type="text" id="order-date-range" value="{if isset($today)}{$today}{/if}">
                                                <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                            </div>     
>>>>>>> .r2659
                                        </div>
                                    </div>
				</div>
				<!-- // Tab content END synchronize -->
				
				<!-- Tab content creation -->
                                {if isset($creation)}
				<div class="tab-pane" id="tab-2">
                                    <h4 class="innerAll half heading-buttons border-bottom">{ci_language line='Creation'}</h4>
                                    <ul class="list-regular">
                                        <li>
                                            <b>{ci_language line="Amazon Creation Wizard"}</b>
                                            <p>{ci_language line='This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be created the unknown products on Amazon but present in your database. The goal is to create the unknown items on Amazon.'}</p>
                                        </li>
                                    </ul>
                                    <hr/>
                                    <div class="status" style="display: none">
                                        <h5 class="blue"><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                                    </div>
                                    <div id="creat_options" class="clearfix">                        
                                        <div class="pull-left">
                                            <button class="btn btn-primary btn-icon-stacked wizard_btn" type="button" href="{$base_url}/amazon/parameters/{$id_country}/2/{$create_mode}" id="amazon_creation_wizard">
                                                <i class="fa fa-magic fa-2x"></i>
                                                <span class="center">
                                                    {ci_language line="Amazon Creation"} <br/>
                                                    {ci_language line="Wizard"}
                                                </span>
                                            </button>
                                        </div>
                                    </div>
				</div>
                                {/if}
				<!-- // Tab content END creation -->
				
				<!-- Tab content orders -->
				<div class="tab-pane" id="tab-3">
                                    <h4 class="innerAll half heading-buttons border-bottom">{ci_language line="Import Orders"}</h4>
                                    <p>{ci_language line='This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'}</p>                
                                    <hr/>
                                    {if !isset($carrier)}
                                    <div class="alert alert-warning">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        <b>{ci_language line='Warning!'}</b>
                                        {ci_language line='Please mapping carriers before import order.'}
                                        [<a href="{$base_url}amazon/carriers/{$id_country}">{ci_language line='Go to Mappings'}</a>]
                                    </div>
                                    {else}
                                    <div>
                                        <div class="status" style="display: none">
                                            <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                                        </div>

                                        <div id="parameter_options">
                                            <div class=" label label-info">{ci_language line="Parameters"}</div>                            
                                            <div class="alert alert-info clearfix form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="order-date-range">{ci_language line='Order Date Range'} : </label>
                                                    <div class="col-md-9">
                                                        <div class="input-group date">
                                                            <input class="form-control" type="text" id="order-date-range" value="{if isset($today)}{$today}{/if}">
                                                            <span class="input-group-addon"><i class="fa fa-th"></i></span>
                                                        </div>     
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="order-status" >{ci_language line='Order Status'} : </label>
                                                    <div class="col-md-9">
                                                        <select id="order-status" class="form-control" >
                                                            {*<option value=""></option>*}
                                                            <option value="All">{ci_language line='Retrieve all pending orders'}</option>
                                                            <option value="Pending">{ci_language line='Pending - This order is pending on the Market Place'}</option>
                                                            <option value="Unshipped" selected="selected">{ci_language line='Unshipped - This order wait to be shipped'}</option>
                                                            <option value="PartiallyShipped">{ci_language line='Partially Shipped - This order was Partially Shipped'}</option>
                                                            <option value="Shipped">{ci_language line='Shipped - This order was Shipped'}</option>
                                                            <option value="Canceled">{ci_language line='Canceled - This order was canceled'}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="clearfix">
                                                <div class="pull-right">
                                                     <button class="btn btn-info btn-icon-stacked" id="import-orders" type="button" value="orders">
                                                        <i class="fa fa-download fa-2x"></i>
                                                        <span class="center">
                                                            {ci_language line="Import orders"} <br/>
                                                            {ci_language line="from Amazon"}
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {/if}
				</div>
				<!-- // Tab content END orders -->
				
				<!-- Tab content delete -->
                                {if (isset($mode_default) && $mode_default > 1)}
				<div class="tab-pane" id="tab-4">
                                     <h4 class="innerAll half heading-buttons border-bottom">{ci_language line="Delete Products"}</h4>
                                    <p>{ci_language line='This will send all the products having a profile and within the selected categories in your configuration.'}</p>  
                                    <hr/>
                                    <div class="status" style="display: none">
                                        <h5><i class="fa fa-spin fa-spinner fa-2x "></i> {ci_language line="Conecting to Amazon .."}</h5>
                                    </div>
                                    <div id="delete_options">
                                        <div class="label label-info">{ci_language line="Parameters"}</div>                            
                                        <div class="alert alert-info clearfix form-horizontal">
                                            <div class="col-md-6">
                                                <label>
                                                    <h5>
                                                        <input name="delete-parameters" type="radio" id="out_of_stock" value="{$out_of_stock}" checked="checked">
                                                        {ci_language line="Delete out of stock products"}
                                                    </h5>
                                                    <div class="smaller-90" style=" padding-left: 15px; ">
                                                        {ci_language line="This will delete only out of stock products on Amazon."}
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                <label>
                                                    <h5>
                                                        <input name="delete-parameters" type="radio" id="all_sent" value="{$all_sent}">
                                                        {ci_language line="Delete all sent products"}
                                                    </h5>
                                                    <div class="smaller-90" style=" padding-left: 15px; ">
                                                        {ci_language line="This will delete all sent products on Amazon."}
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="pull-right">
                                                <button class="btn btn-danger btn-icon-stacked" id="delete-products" type="button" value="delete">
                                                    <i class="fa fa-trash fa-2x"></i>
                                                    <span class="center send">
                                                        {ci_language line="Delete Products"} <br/>
                                                        {ci_language line="from Amazon"}
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
				</div>
                                {/if}
				<!-- // Tab content END delete -->
				
			</div>
		</div>
	</div>
        
        
            
            
            
            <div class="space-32"></div>
            <input type="hidden" value="{ci_language line="Are you sure to update offers?"}" id="send-offers-confirm" />
            <input type="hidden" value="{ci_language line="Are you sure to get orders?"}" id="get-orders-confirm" />
            <input type="hidden" value="{ci_language line="Are you sure to delete product?"}" id="delete-products-confirm" />
            <input type="hidden" value="{ci_language line="Warning! Configuration parameter not complete."}" id="missing_data" />
            <input type="hidden" value="{ci_language line="Pay now"}" id="pay_now" />
            <input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
            <input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />
            
            <input type="hidden" value="{if isset($ext)}{$ext}{/if}" id="ext" />
            <input type="hidden" value="{if isset($id_user)}{$id_user}{/if}" id="id_user" />
            <input type="hidden" value="" id="offer_sending" />
            {if isset($debug)}<input type="hidden" id="debug" value="{$debug}" />{/if}
            {if isset($id_country)}<input type="hidden" id="id_country" value="{$id_country}" />{/if}
        </div><!--actions-->
    </div>  
        
    {include file="amazon/PopupStepFooter.tpl"}  
    
</div> 
        
<input type="hidden" value="{if isset($action)}{$action}{/if}" id="actions"> 
<script type="text/javascript" src="{$base_url}assets/js/date-time/moment.min.js"></script>
<script type="text/javascript" src="{$base_url}assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{$base_url}assets/js/date-time/daterangepicker.min.js"></script>
<script type="text/javascript" src="{$base_url}assets/js/jquery.colorbox-m.js"></script>
<script type="text/javascript" src="{$base_url}assets/js/FeedBiz/amazon/AmazonWizard.js"></script>
{include file="amazon/IncludeScript.tpl"}
{include file="footer.tpl"}

<script src="{$base_url}assets/js/FeedBiz/amazon/actions.js"></script>