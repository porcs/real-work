{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}
<link rel="stylesheet" href="{$base_url}assets/css2/admin/module.admin.page.form_elements.min.css" />
<script src="{$base_url}assets/components/modules/admin/forms/elements/select2/assets/lib/js/select2.min.js"></script>
<script src="{$base_url}assets/components/modules/admin/forms/elements/fuelux-checkbox/fuelux-checkbox.js?v=v1.2.3"></script>

{literal}
    <style>
        .profile{ width: 90%; height: 25px; font-size: 12px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; }
        div.tree-folder i.cp_down_btn.fa-arrow-down { transform: rotate(0deg); }  
    </style>
{/literal}

<div {if !isset($popup)}id="content"{/if}><div class="heading-buttons bg-white border-bottom innerAll">
    <h1 class="content-heading padding-none pull-left">{ci_language line="category"}</h1>
    <div class="clearfix"></div>
</div>

{if isset($popup) && $popup != ''}{include file="amazon/PopupStep.tpl"}{/if}

<!-- innerAll -->
<div class="innerAll spacing-x2" >

    <div class="row">
        <div class="col-md-12">
            <div class="widget widget-inverse">
                {if !isset($popup)} 
                    <!-- Widget Heading -->
                    <div class="widget-head">
                        <h3 class="heading show_thumbnails"><i class="fa fa-archive"></i> {ci_language line="Choose Categories"}</h3>
                    </div>
                    <!-- // Widget Heading END -->
                {/if}

                <div class="widget-body">
                    <div class="row-fluid">
                        {*<div class="{if !isset($popup)} offset1 span10{else}span12{/if}">*}

                        <div id="category">
                            <form action="{$base_url}amazon/save_category{if isset($popup)}/{$popup}{/if}" method="post" id="category" autocomplete="off" novalidate>
                                <input type="hidden" id="mode" name="mode" value="{if isset($mode)}{$mode}{/if}" />
                                <div class="widget-box form-horizontal">
                                    <div class="widget-body">
                                        <div class="alert alert-info">
                                            {ci_language line='You can select many categories in one time by checking the first category then press "Shift" and click on the last element you wish to be in the same profile.'}
                                        </div>

                                        <div id="none_category" class="alert alert-warning" style="display: none">
                                            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button><i class="fa fa-exclamation-triangle"></i>
                                                {ci_language line='Please choose category in'} 
                                            <a href="{$base_url}/my_feeds/parameters/profiles/category"> 
                                                {if isset($mode) && $mode == 1}
                                                    {ci_language line='My feeds > Parameters > Offers > Category'}
                                                {else}
                                                    {ci_language line='My feeds > Parameters > Profiles > Category'}
                                                {/if}
                                            </a>
                                        </div> 

                                        <div class="col-md-12">
                                            <button id="expandAll" class="btn btn-success btn-stroke" type="button">
                                                <i class="fa fa-plus-square-o"></i>
                                                {ci_language line="Expand all"}
                                            </button>
                                            <button id="collapseAll" class="btn btn-warning btn-stroke" type="button">
                                                <i class="fa fa-minus-square-o"></i>
                                                {ci_language line="Collapse all"}
                                            </button>
                                            <button id="checkAll" class="btn btn-inverse btn-stroke" type="button">
                                                <i class="fa fa-check-square-o"></i>
                                                {ci_language line="Check all"}
                                            </button> 
                                            <button id="unCheckAll" class="btn btn-inverse btn-stroke" type="button">
                                                <i class="fa fa-square-o"></i>
                                                {ci_language line="Uncheck all"}
                                            </button>
                                        </div>
                                        <div class="widget-main">

                                            {if !isset($popup)}
                                                <div id="main" style="display: none;">
                                                    <div class="pull-right col-xs-5 col-md-4 col-md-3 col-lg-2">
                                                        <select class="col-xs-10">
                                                            <option value=""/>
                                                            {if isset($mode_default) && ($mode_default == 1)}
                                                                 - {ci_language line="Prices Mapping"} - 
                                                            {else}
                                                                 - {ci_language line="use profiles mapping"} - 
                                                            {/if}   
                                                            </option>
                                                            {if (isset($profile) && !empty($profile) )}
                                                                <optgroup label="-- More price modifier --">
                                                                    {foreach from=$profile key=pkey item=pvalue}
                                                                        <option value="{$pvalue.id_profile}" {if isset($pvalue.is_default) && $pvalue.is_default == 1} selected {/if}>
                                                                            {$pvalue.name}
                                                                        </option>
                                                                    {/foreach}  
                                                                </optgroup>
                                                            {/if}
                                                        </select>

                                                        <div class="col-xs-2">
                                                            <i class="fa fa-arrow-down cp_down_btn"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            {/if}

                                            <div class="tree tree-unselectable" style="display: none" id="page-loaded">
                                                <div class="tree-folder" style="display: block">	
                                                    <div class="tree-folder-header">					
                                                        <div class="tree-folder-name"> <i class="fa fa-folder-open orange"></i> &nbsp; {ci_language line="category"}</div>
                                                        {if !isset($popup)}
                                                            <div class="pull-right">
                                                                <div class="text-center">
                                                                    <i class="fa fa-list-alt orange"></i>
                                                                    {if isset($mode_default) && ($mode_default == 1)}
                                                                        {ci_language line="Prices Mapping"}
                                                                    {else}
                                                                        {ci_language line="Profiles Mapping"}
                                                                    {/if}                                                       
                                                                </div>
                                                            </div>
                                                        {/if}
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="clearfix center" id="page-load">
                                                <div class="space-10"></div>
                                                <i class="fa fa-spinner fa fa-spin blue fa-3x"></i>
                                            </div>

                                            <div id="tree1" class="tree tree-selectable"></div>

                                        </div>{*widget-main*}
                                    </div>{*widget-body*}

                                </div>{*widget-box*}

                                <div class="form-actions center">
                                    <div class=" row-fluid">
                                        <div class="col-md-6 text-left">
                                            {if isset($popup)}
                                                <button class="btn btn-light" id="back" type="button">
                                                    <i class="fa fa-arrow-left"></i> {ci_language line="Prev"}
                                                </button>
                                            {/if}
                                            <!--Message-->
                                            <input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
                                        </div>
                                        <div class="col-md-6 text-right">

                                            {if !isset($popup)}
                                                <button class="btn btn-success" id="save_data" type="submit">
                                                    <i class="fa fa-save"></i> {ci_language line="Save"}
                                                </button> &nbsp;
                                            {/if}

                                            <button class="btn btn-primary" id="save_continue_data" type="submit" name="save" {if isset($popup)}value="next"{else}value="continue"{/if}>
                                        {if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if} 
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>                         
                    </form>

                </div>{*category*}

            </div>{*span10*}

        </div>{*row-fluid*}

    </div>
</div>
</div>
{*</div>*}
</div>
</div>


{*<div {if !isset($popup)}class="main-content"{/if}>
    
{include file="breadcrumbs.tpl"} <!--breadcrumb-->
{include file="amazon/PopupStep.tpl"} 

<div class="page-content">
      
{if !isset($popup)} 
<div class="page-header position-relative">
<h1>
{ci_language line="Amazon"}
<small><i class="icon-double-angle-right"></i> {$country} </small>
<small><i class="icon-double-angle-right"></i> {ci_language line="category"} </small>
</h1>
</div>
{else}
<div class="widget-header header-color-blue">            
<h5> 
<img src="{$base_url}assets/images/geo_flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.gif" style="vertical-align: baseline; border: 1px solid #fff"/>&nbsp;{$country}
</h5>
</div>
{*<div class="widget-header header-color-blue">
<h5><i class="icon-tint"></i> {$country}</h5>
</div>*}
{*/if}
<div class="row-fluid">
    
<div class="{if !isset($popup)} offset1 span10{else}span12{/if}">
    
<div id="category">
   
<form action="{$base_url}amazon/save_category{if isset($popup)}/{$popup}{/if}" method="post" id="category" autocomplete="off" novalidate>

<div class="widget-box form-horizontal">
{if !isset($popup)} 
<div class="widget-header header-color-purple">
<h4 class="lighter smaller">{ci_language line="Choose Categories"}</h4>
<div class="widget-toolbar">
<a href="#"></a>
<a href="#" data-action="collapse"><i class="icon-only bigger-110 icon-chevron-up"></i></a>
</div>
</div>
{/if}

<div class="widget-body">
<div class="widget-toolbox padding-8 clearfix">
<button id="expandAll" class="btn btn-mini btn-success" type="button">
<i class=" icon-folder-open"></i>
{ci_language line="Expand all"}
</button>
<button id="collapseAll" class="btn btn-mini btn-warning" type="button">
<i class=" icon-folder-close"></i>
{ci_language line="Collapse all"}
</button>
<button id="checkAll" class="btn btn-mini btn-pink" type="button">
<i class=" icon-check "></i>
{ci_language line="Check all"}
</button> 
<button id="unCheckAll" class="btn btn-mini btn-purple" type="button">
<i class="  icon-check-empty"></i>
{ci_language line="Uncheck all"}
</button>
</div>
<div class="widget-main padding-20">

<div id="none_category" class="alert alert-danger smaller-90" style="display: none">
<i class="icon-warning-sign bigger-125"></i> 
{ci_language line='Please choose category in'} 
<a href="{$base_url}/my_feeds/parameters/profiles/category"> 
{if isset($mode) && $mode == 1}
{ci_language line='My feeds > Parameters > Offers > Category'}
{else}
{ci_language line='My feeds > Parameters > Profiles > Category'}
{/if}
</a>
</div> 

<div id="has_category" class="alert alert-info smaller-90" style="display: none">
<i class="icon-info-sign bigger-125"></i> 
{ci_language line='You can select many categories in one time by checking the first category then press "Shift" and click on the last element you wish to be in the same profile.'}
</div>
      
{if !isset($popup)}
<div id="main" style="display: none;">
<div class="pull-right">
<select class="profile position-relative">
<option value=""/> -
{if isset($mode_default) && ($mode_default == 1)}
{ci_language line="Prices Mapping"}
{else}
{ci_language line="use profiles mapping"}
{/if}   
- </option>
{if (isset($profile) && !empty($profile) )}
<optgroup label="-- More price modifier --">
{foreach from=$profile key=pkey item=pvalue}
<option value="{$pvalue.id_profile}" {if isset($pvalue.is_default) && $pvalue.is_default == 1} selected {/if}>
{$pvalue.name}
</option>
{/foreach}  
</optgroup>
{/if}
</select>
<i class="icon-arrow-down cp_down_btn" onclick="click_copy_val_sel($(this))"></i>
</div>
</div>
{/if}
        
<div class="tree tree-unselectable" style="display: none" id="page-loaded">
<div class="tree-folder" style="display: block; background: rgba(98,168,209,0.1); border: 1px solid #FFF">	
<div class="tree-folder-header">					
<div class="tree-folder-name"> <i class="icon-folder-close orange"></i> &nbsp; {ci_language line="category"}</div>
{if !isset($popup)}
<div class="pull-right" style="border: 1px solid #FFF">
<div class="text-center">
<i class="icon-list-alt orange"></i>
{if isset($mode_default) && ($mode_default == 1)}
{ci_language line="Prices Mapping"}
{else}
{ci_language line="Profiles Mapping"}
{/if}                                                       
</div>
</div>
{/if}
</div>
</div> 
</div>

<div class="clearfix center" id="page-load">
<div class="space-10"></div>
<i class="icon-spinner icon-spin blue bigger-280"></i>
</div>
                
<div id="tree1" class="tree tree-selectable"></div>

</div>{*widget-main*}
{*</div>{*widget-body*}

{*</div>{*widget-box*}

{*<div class="form-actions center">
<div class=" row-fluid">
<div class="inline pull-left">
{if isset($popup)}
<button class="btn btn-light" id="back" type="button">
<i class="icon-arrow-left bigger-110"></i> {ci_language line="Prev"}
</button>
{/if}
<!--Message-->
<input type="hidden" name="id_country" id="id_country" value="{if isset($id_country)}{$id_country}{/if}" />
</div>
<div class="inline pull-right text-right">

{if !isset($popup)}
<button class="btn btn-success" id="save_data" type="submit">
<i class="icon-save bigger-110"></i> {ci_language line="Save"}
</button> &nbsp;
{/if}

<button class="btn btn-info" id="save_continue_data" type="submit" name="save" {if isset($popup)}value="next"{else}value="continue"{/if}>
{if isset($popup)}{ci_language line="Next"}{else}{ci_language line="Save and continue"}{/if} 
<i class="icon-arrow-right bigger-110 icon-on-right"></i>
</button>
</div>
</div>
</div>                         
</form>

</div>{*category*}

{*</div>{*span10*}

{*</div>{*row-fluid*}

{*</div>{*page-content*}

{*</div>{*main-content*}

<input type="hidden" id="popup_state" value="{if isset($popup_state)}{$popup_state}{/if}" />

{include file="footer.tpl"}



<!--page specific plugin scripts-->
<script src="{$base_url}assets/js/fuelux/fuelux.tree.offers.js"></script>
<script src="{$base_url}assets/js/jquery.blockUI.js"></script>

<!--inline scripts related to this page-->
{*<script src="{$base_url}assets/js/FeedBiz/amazon/category.js"></script>*}  

<script>

    var base_url = location.origin;
    var just_cal = false;
    var just_check_all = false;
    $(document).ready(function() {

        $('head').append('<style>.profile{ width: 100%; height: 25px; font-size: 12px; border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; }<style>');

        $("#checkAll").click(function() {
            $("input[type=checkbox]").each(function() {
                if (!$(this).prop("checked")) {
                    $(this).prop('checked', true);
                    $(this).checkbox('check');

                    getProfile($(this));
                    if (!just_check_all) {
                        just_cal = false;
                        just_check_all = true;
                    }
                }
            });
        });

        $("#unCheckAll").click(function() {
            $("input[type=checkbox]").each(function() {
                $(this).prop('checked', false);
                $(this).parent().parent().find('.pull-right').remove();
                $(this).checkbox('uncheck');
            });
        });

        $('.add_category').each(function() {
            $(this).parent().parent().find('.tree-folder-content').append('<i class="icon-spinner icon-spin orange"></i>');
            expandCollapseAll($(this));
        });

        if ($('#popup_status').length != 0)
        {
            $('#back').on('click', function() {
                window.location.href = base_url + '/amazon/parameters/' + $('#id_country').val() + '/1';
            });
        }

        /* $('body').on('click', "input[type=checkbox]", function() {
         var chk = Boolean($(this).attr('checked'));
         $(this).attr('checked', !chk);
         });*/

        getSelectedAllCategory();
        apply();

    });

    function init_rel_all_row() {
        var i = 1;
        if (just_cal)
            return;

        just_cal = true;
        $('.cp_down_btn').each(function() {
            if ($(this).parents('#main').length > 0)
                return;
            var row = $(this).parents('.tree-folder-header');
            if (row.length == 0)
                return;
            row.attr('rel', i);
            i++;
        });


    }
    function showWaiting(type) {
        if (type) {
            $.blockUI({ css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }});
        } else {
            $.unblockUI();
        }
    }

    function click_copy_val_sel(obj) {
        showWaiting(true);
        var time = 0;
        var num = $('body').find('select.profile').length;
        if ({*!just_cal &&*} num > 200) {
            time = 200;
        }

        setTimeout(function() {
            init_rel_all_row();
            var p_row = obj.parents('.tree-folder-header');
            var cur_pos = p_row.attr('rel');
            var cur_sel_val = p_row.find('select').val();
            $('body').find('.tree-folder-header').each(function() {
                var rel = $(this).attr('rel');

                if (rel * 1 > cur_pos * 1) {
                    $(this).find('select').val(cur_sel_val).trigger("change");
                }
            });
            showWaiting(false);
        }, time);

    }

    function apply() {

        $('#collapseAll').unbind('click').on('click', function() {
            $('input[rel="id_category"]').each(function() {
                if ($(this).parent().parent().find('.icon-minus').length > 0) {
                    $(this).parent().parent().parent().find('.tree-folder-content').hide();
                    $(this).parent().parent().find('.icon-minus').removeClass('icon-minus').addClass('icon-plus');
                }
            });
        });

        $('#expandAll').unbind('click').on('click', function() {
            $('.add_category').each(function() {
                $(this).parent().parent().parent().find('.tree-folder-content').show();
                $(this).parent().parent().find('.icon-plus').removeClass('icon-plus').addClass('icon-minus');
            });
        });

        $('.add_category').unbind('click').on('click', function() {
            expandCollapse($(this));
        });

        $('input[rel="id_category"]').unbind('click').on('click', function() {
            getProfile($(this));
            if (!just_check_all) {
                just_cal = false;
            }
        });

        $.fn.enableCheckboxRangeSelection = function() {
            var lastCheckbox = null;
            var $spec = this;
            $spec.unbind("click.checkboxrange");
            $spec.bind("click.checkboxrange", function(e) {
                if (lastCheckbox !== null && (e.shiftKey || e.metaKey)) {
                    $spec.slice(
                            Math.min($spec.index(lastCheckbox), $spec.index(e.target)),
                            Math.max($spec.index(lastCheckbox), $spec.index(e.target)) + 1
                            ).each(function() {
                        var chk = Boolean($(this).parent().find('input').attr('checked'));
                        if ($(this)[0] !== lastCheckbox) {
                            switch (true) {
                                case chk :
                                    $(this).parent().find('input').checkbox('uncheck');
                                    $(this).parent().parent().find('.pull-right').remove();
                                    break;
                                case !chk :
                                    $(this).parent().find('input').checkbox('check');
                                    getProfile($(this).parent().find($("input[type=checkbox]")));
                                    break;
                                default:
                                    $(this).parent().find('input').checkbox('check');
                                    getProfile($(this).parent().find($("input[type=checkbox]")));
                                    break;
                            }
                        }
                    });
                }
                lastCheckbox = e.target;
            });
        };

        $('.checkbox-custom > i').enableCheckboxRangeSelection();

        //$('input[rel="id_category"]').shiftClick();
    }

    /*$.fn.shiftClick = function () {
     var lastSelected; // Firefox error: LastSelected is undefined
     var selected = { };
     var checkBoxes = $(this);
     this.each(function () {
     
     $(this).click(function (ev) {
     var selectedValue = this.value ;
     if (ev.shiftKey) {
     var last = checkBoxes.index(lastSelected);
     var first = checkBoxes.index(this);
     var start = Math.min(first, last);
     var end = Math.max(first, last);
     var chk = lastSelected.checked;
     
     for (var i = start; i <= end; i++) 
     {
     checkBoxes[i].checked = chk;
     
     selected[i] = getProfile(checkBoxes[i]);
     if(selected[i])
     { 
     selected[i].each(function () {
     if(i === start)
     selectedValue = this.value;
     
     $(this).on('change', function(){
     selectedValue = $(this).val();
     });
     
     $(this).val(selectedValue);
     });
     }
     
     }
     if(!just_check_all){
     just_cal=false; 
     }
     } else {
     lastSelected = this;
     }
     });
     });
     };*/

    //Get Profile
    function getProfile(e) {

        var select;
        var ele = $(e).parent().parent();

        if ($(e).is(':checked'))
        {
            if (ele.find('select.profile').length === 0)
            {
                var cloned = $('#main').find('.pull-right').clone();
                cloned.appendTo(ele);

                cloned.find('.cp_down_btn').click(function() {
                    click_copy_val_sel($(this));
                });

                if ($('#popup_status').length !== 0)
                    cloned.find('select.profile').attr('name', 'category[' + $(e).val() + '][id_profile_price]');
                else
                    cloned.find('select.profile').attr('name', 'category[' + $(e).val() + '][id_profile]');

                cloned.find('select.profile').attr('style', 'background : #DAFFF0 !important;');

                select = cloned.find('select.profile');
            }
            else
                select = ele.find('select.profile');
        }
        else
            ele.find('.pull-right').remove();

        ele.find('select').select2();

        return select;

    }

    //Get All Selected Category
    function getSelectedAllCategory() {

        var url = base_url + "/amazon/getSelectedAllCategory/" + $('#id_country').val();
        if ($('#popup_status').length != 0)
        {
            url = url + '/' + $('#popup_state').val();
        }

        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(data) {

                if (data && data != "")
                {
                    $('#has_category').show();
                    $('#tree1').html(data);
                }
                else
                {
                    $('#none_category').show();
                }

                $('#page-loaded').show();
                $('#page-load').hide();

                apply();

            },
            error: function(data, error) {
                console.log(data);
                console.log(error);
            }
        });
    }

    function expandCollapse(e) {

        var content = e.parent().parent().find('.tree-folder-content');

        if (e.hasClass('icon-plus'))
        {
            if (content.children().length > 1) {
                content.show();
                content.find('.icon-spinner').remove();
            }
            e.removeClass('icon-plus').addClass('icon-minus');
        }
        else
        {
            content.hide();
            e.removeClass('icon-minus').addClass('icon-plus');
        }
    }

    function expandCollapseAll(e) {

        var content = e.parent().parent().find('.tree-folder-content');

        if (content.children().length > 1) {
            content.show();
        }
    }

</script>