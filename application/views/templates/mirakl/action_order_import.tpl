
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#" data-toggle="tab" class="">
                    <i class="icon-creation-down"></i><span>{ci_language line="Orders"}</span>
                </a>
            </li>                        
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
            <h4 class="headSettings_head">{ci_language line="Parameter"}</h4>
        </div>
    </div>
</div>

<div class="row m-b10 m-t20"> 
    <div class="col-md-3">
        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Date Range"} :</p>
    </div>
    <div class="col-md-3">
        <div class="form-group calendar firstDate">
            <input class="form-control order-date" type="text" id="date_start" value="{if isset($date_start)}{$date_start}{/if}">
        </div>
    </div>
</div>               

<div class="row m-b10"> 
    <div class="col-md-3">
        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Order Status"} :</p>
    </div>
    <div class="col-md-3">
        <select class="search-select" id="order_status">
            <option value="{MiraklParameter::ORDER_IMPORT_STATUS_IMPORT}">{ci_language line='Ready to be Imported'}</option>
            <option value="{MiraklParameter::ORDER_IMPORT_STATUS_ALL}">{ci_language line='All Pending Orders'}</option>
        </select>
    </div>
</div>               

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-save pull-right" id="btn_import_order" type="button">
            <i class="fa fa-download"></i> {ci_language line="Import Orders"} 
        </button>
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to import orders ?"}" id="send-order-confirm" />
<script src="{$cdn_url}assets/js/FeedBiz/mirakl/action_order_import.js"></script>