<!-- Text Title -->
<h1 class="lightRoboto text-uc head-size light-gray">{$function}</h1>
{include file="breadcrumbs.tpl"} 
<!-- Add -->
<div class="row">
  <div class="col-xs-12">
    {if strtolower($function) == "orders" || strtolower($function) == "mappings"}<div class="b-Top p-t10 b-Bottom clearfix">{/if}
      <p {if strtolower($function) == "Category"}class="head_text p-t10 b-Top clearfix m-0"{else if strtolower($function) == "orders" || strtolower($function) == "mappings"} class="head_text pull-sm-left b-None clearfix m-0" {else}class="head_text p-t10 b-Top clearfix m-0"{/if}>
        <img src="{$base_url}assets/images/flags/{str_replace(array(".",".."), "_", ltrim($ext, "."))}.png" class="flag m-r10" alt="">
        {if isset($country)}<span class="p-t10">{$country}</span>{/if}
      </p>
      {if strtolower($function) == "orders"} 
          {*<div class="pull-sm-right">
          <button class="btn btn-save m-b10" id="btn_header_import_order" data-toggle="modal" data-target="#importOrder" type="button"><i class="fa fa-download"></i> {ci_language line="Import Orders"}</button>  
          <button class="btn btn-save m-b10" id="send-orders" type="button">{ci_language line="Send orders"}</button>
          </div>*}
      {/if}
      {if strtolower($function) == "orders" || strtolower($function) == "mappings"}</div>{/if}
  </div>
</div>

{if strtolower($function) == "models"}
    <div class="row">
      <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
          <p class="head_text p-b20">{ci_language line="Model Create"}</p>
        </div>
      </div>
    </div>
{/if}

{if strtolower($function) == "profiles"}
    <div class="row">
      <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
          <p class="head_text p-b20">{ci_language line="Profile Create"}</p>
        </div>
      </div>
    </div>
{/if}


{* add data eg, add profile.  $this->smarty->assign("add_data", $this->lang->line('Add a profile to the list')); *}
{if isset($add_data) && !empty($add_data)}
    <div class="row">
      <div class="col-xs-12 p-t10">
        <div class="b-Bottom p-b10">
          <button type="button" class="link showProfile p-size" id="add"><i class="fa fa-plus"></i> {$add_data}</button>
          <p class="pull-right montserrat poor-gray">
            <span class="dark-gray countProfile" id="count">{if isset($size_data)}{$size_data}{else}0{/if}</span> 
            {ci_language line="Items(s)"}
          </p>
        </div>
      </div>
    </div>   
{/if}

{if isset($no_data)}
    <div class="m-t10">
      {$no_data}
    </div>
{/if}