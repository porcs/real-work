
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#" data-toggle="tab" class="">
                    <i class="icon-creation-up"></i><span>{ci_language line="Offer"}</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="headSettings clearfix p-b10 m-t20 b-Bottom m-b10">
            <h4 class="headSettings_head">{ci_language line="Parameter"}</h4>
        </div>
    </div>
</div>
        
<div class="row m-b10 m-t20">
    <div class="col-md-4">
        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Export All Offers"} :</p>
    </div>
    <div class="col-md-4">
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="active" type="checkbox" value="1" id="export_all" {if (isset($active) && $active == 1)}checked="checked"{/if} data-state-on="YES" data-state-off="NO">
            </label>
            <span class="cb-state">{ci_language line="YES"}</span>
        </div>
    </div>
</div>

<div class="row m-b10">
    <div class="col-md-4">
        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Purge and Replace"} :</p>
    </div>
    <div class="col-md-4">                       
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="active" type="checkbox" value="1" id="purge" {if (isset($active) && $active == 1)}checked="checked"{/if} data-state-on="YES" data-state-off="NO">
            </label>
            <span class="cb-state">{ci_language line="YES"}</span>
        </div>
    </div>
</div>

<div class="row m-b10"> 
    <div class="col-md-4">
        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Send to Mirakl"} :</p>
    </div>
    <div class="col-md-4">                        
        <div class="cb-switcher pull-left">
            <label class="inner-switcher">
                <input name="active" type="checkbox" checked value="1" id="send_mirakl" {if (isset($active) && $active == 1)}checked="checked"{/if} data-state-on="YES" data-state-off="NO">
            </label>
            <span class="cb-state">{ci_language line="YES"}</span>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <button class="btn btn-save pull-right" id="send_offers" type="button">
            <i class="fa fa-upload"></i><span class="send-status"> {ci_language line="Update offers"}</span>              
        </button>

        <button class="btn btn-save pull-right m-r10" id="download_offers" type="button">
            <i class="fa fa-download"></i><span class="send-status"> {ci_language line="Downloads"}</span>              
        </button>
    </div>
</div>

<input type="hidden" value="{ci_language line="Are you sure to update offers ?"}" id="send-offers-confirm" />
<input type="hidden" value="{ci_language line="Warning! Configuration parameter is not complete."}" id="missing_data" />
<input type="hidden" value="{ci_language line="Go to Configuration."}" id="go_to" />
<input type="hidden" value="{ci_language line="Product sending"}" id="product_sending" />

<script src="{$cdn_url}assets/js/FeedBiz/mirakl/action_offers.js"></script> 