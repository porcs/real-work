{if isset($value_list) && !empty($value_list)}
    {foreach $value_list as $list}

        <div class="row m-t3 m-b3 with_value_list" id_config_value_list="{$list['id_config_value_list']}" >
          <div class="col-md-3">
            <p class="text-right m-t5 poor-gray">{$list['label']}</p>
          </div>
          <div class="col-md-3">
            <select name="attribute_with_list[{$id_config_attribute}][field_value][{$list['id_config_value_list']}]" class="search-select attribute_with_list_value">
              <option value="">---</option>
              {if isset($selected) && !empty($selected)}
                  {foreach $selected as $select}
                      <option value="{$select[0]}" {if isset($list['shop_attribute_value']) && $list['shop_attribute_value'] == $select[0]} selected {/if} >{$select[1]}</option>
                  {/foreach}
              {/if}
            </select>
          </div>
        </div>

    {/foreach}
{/if}