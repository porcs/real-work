
<div class="row">  

    <div id="send-products-message">
        <div class="status row" style="display:none;">
            <div class="col-xs-12 validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i> 
                    </div>
                    <div class="validateCell">
                        <p class="pull-left"></p>
                    </div>
                </div>
            </div>              
        </div>
    </div>


        <div class="col-md-12">
            <div class="headSettings clearfix">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Orders"}</h4>                      
                </div>
                <div class="pull-sm-right clearfix">
                    <div class="showColumns clearfix">                                
                        <div class="b-Right p-r10">
                            <p class="small light-gray pull-left m-t5">Show:&nbsp;</p> 
                            <label class="mirakl-checkbox checked">
                                <span class="mirakl-inner"><i>
                                        <input type="checkbox" name="export-sent" id="export-sent" value="1" checked class="mirakl-input"></i>
                                </span>
                                {ci_language line="Sent"}
                            </label>
                            <label class="mirakl-checkbox checked">
                                <span class="mirakl-inner"><i><input type="checkbox" name="export-send" id="export-send" value="1" checked class="mirakl-input"></i>

                                </span>
                                {ci_language line="Send"}
                            </label>
                        </div>
                    </div>
                </div>          
            </div><!--headSettings-->

            <div class="row">
                <div class="col-md-12">
                    <table id="statistics" class="mirakl-responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th>{ci_language line="ID"}</th>
                                <th>{ci_language line="Order ID Ref"}.</th>
                                <th>{ci_language line="Name"}</th>
                                <th>{ci_language line="Amount"}</th>
                                <th>{ci_language line="Status"}</th>
                                <th>{ci_language line="Order Date"}</th>
                                <th>{ci_language line="Shipping Date"}</th>
                                <th>{ci_language line="Tracking No"}</th>
                                <th>{ci_language line="Invoice No"}</th>

                                <th>
                                    {ci_language line="Messaging"}
                                    <input type="hidden" id="display_messaging" value="1">
                                    <input type="hidden" id="flag_mail" value="{ci_language line="Sent"}">
                                    <input type="hidden" id="flag_mail_fail" value="{ci_language line="Send"}">
                                    <input type="hidden" id="flag_mail_invoice" value="{ci_language line="Sent Invoice"}">
                                    <input type="hidden" id="flag_mail_review" value="{ci_language line="Sent Review"}">
                                </th>


                                <th>{ci_language line="Exported"}</th>
                            </tr>
                        </thead>
                    </table><!--table-->
                </div><!--col-md-12-->
            </div><!--row--> 
        </div><!--col-md-12-->


    <input type="hidden" id="Send" value="{ci_language line="Send"}" />
    <input type="hidden" id="Sent" value="{ci_language line="Sent"}" />
    <input type="hidden" id="Sending" value="{ci_language line="Sending"}" />
    <input type="hidden" id="success-message" value="{ci_language line="Save successful"}" />
    <input type="hidden" id="unsuccess-message" value="{ci_language line="Error encountered while saving data."}" />

</div> 

<div class="modal fade" id="importOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>					
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Parameters"}</h1>
                    </div>
                </div>
                <div class="row m-b10"> 
                    <div class="col-md-3">
                        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Date Range"} :</p>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group calendar firstDate">
                            <input class="form-control order-date" type="text" id="date_start" value="{if isset($date_start)}{$date_start}{/if}">
                        </div>
                    </div>
                </div>      
                <div class="row m-b10"> 
                    <div class="col-md-3">
                        <p class="pull-right poor-gray m-t3 m-r10">{ci_language line="Order Status"} :</p>
                    </div>
                    <div class="col-md-4">
                        <select class="search-select" id="order_status">
                            <option value="{MiraklParameter::ORDER_IMPORT_STATUS_IMPORT}">{ci_language line='Ready to be Imported'}</option>
                            <option value="{MiraklParameter::ORDER_IMPORT_STATUS_ALL}">{ci_language line='All Pending Orders'}</option>
                        </select>
                    </div>
                </div>   
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-save pull-right" data-dismiss="modal" id="btn_import_order" type="button">
                            <i class="fa fa-download"></i> {ci_language line="Import Orders"} 
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        
<div class="modal fade" id="generalOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="lightRoboto text-uc head-size light-gray p-b10 b-Bottom">{ci_language line="Order Result"}</h1>
                    </div>
                </div>

                <div class="row">                       
                    <div class="col-xs-12 p-0">
                        <div id="viewResult"></div>
                    </div>  
                </div>

            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/page_order.js"></script> 
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>
