
<div class="row">
    <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
            <p class="head_text p-b20">
                {ci_language line="Condition Mappings"}
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="validate blue">
            <div class="validateRow">
                <div class="validateCell">
                    <i class="note"></i>
                </div>
                <div class="validateCell">
                    <p class="pull-left">{ci_language line="Mirakl conditions side / Feed.biz conditions side, please associate and map the parameters desired between Mirakl and Feed.biz"}</p>
                    <i class="fa fa-remove pull-right"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="Marketplace - Conditions"}</p>
    </div>
    <div class="col-md-4">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="Shop - Conditions"}</p>
    </div>
</div>

{if isset($marketplace_condition) && !empty($marketplace_condition)}
    {foreach from=$marketplace_condition key=mk item=market}
        <div class="row m-t5 m-b5">
            <div class="col-md-3">
                <p class="text-right m-t5">{$market['condition_text']}</p>
            </div>
            <div class="col-sm-4">
                <select class="search-select" name="condition[{$market['id']}]">
                    <option value="">--------</option>                                                  
                    {foreach $shop_condition as $shop_cond}
                        <option value="{$shop_cond['txt']}" {if isset($shop_cond['condition_value']) && !empty($shop_cond['condition_value']) && ($shop_cond['condition_value'] ==  $market['condition_value'])}selected="selected"{/if}>{ucfirst(strtolower($shop_cond['txt']))}</option>
                    {/foreach}                                                                                           
                </select>
            </div>
        </div>
    {/foreach}
{/if}



{*<div class="clearfix">
<div class="row">
<div class="col-md-6">
<label class="montserrat text-info text-left text-uc m-b20">{ci_language line="Marketplace - Conditions"}</label>
</div>
<div class="col-md-6">
<label class="montserrat text-info text-left text-uc m-b20">{ci_language line="Shop - Conditions"}</label>
</div>
</div>

{if isset($marketplace_condition) && !empty($marketplace_condition)}
{foreach from=$marketplace_condition key=mk item=market}
<div class="row">
<div class="b-Top p-t5">
<div class="col-md-6">
<p class="poor-gray p-t10">{$market['condition_text']}</p>
<i class="blockRight noBorder"></i>
</div>
<div class="col-sm-6">
<div class="form-group">
<select class="select-main search-select" name="condition[{$market['id']}]">
<option value="">--------</option>                                                  
{foreach $shop_condition as $shop_cond}
<option value="{$shop_cond['txt']}" {if isset($shop_cond['condition_value']) && !empty($shop_cond['condition_value']) && ($shop_cond['condition_value'] ==  $market['condition_value'])}selected="selected"{/if}>{ucfirst(strtolower($shop_cond['txt']))}</option>
{/foreach}                                                                                           
</select>
</div>
</div>
</div>
</div>
{/foreach}
{/if}
</div><!--condition-group-->*}


<script src="{$cdn_url}assets/js/FeedBiz/mirakl/config_conditions.js"></script>