
{if isset($sub_hierarchy) && !empty($sub_hierarchy)}
    <div class="sub_hierarchy_level_{$sub_hierarchy[0]['level']}" data-level="{$sub_hierarchy[0]['level']}">
      <p class="poor-gray">{ci_language line="Sub Hierarchy"}</p>
      <div class="form-group">
        <select rel="hierarchy_code" select-level="{$sub_hierarchy[0]['level']}" onchange="getSubHierarchy($(this))">
          <option value="">--------</option>
          {foreach $sub_hierarchy as $hierarchy}
              <option value="{$hierarchy['id_config_hierarchy']}">{$hierarchy['label']} (H - {$hierarchy['count_sub_hierarchy']}) , (A - {$hierarchy['count_attribute']})</option>
          {/foreach}
        </select>
      </div>
    </div>
{/if}

{if isset($attribute_with_hierarchy) && !empty($attribute_with_hierarchy)}
    {foreach $attribute_with_hierarchy as $attribute_hierarchy}
        
        <div class="panel_attribute">
          <div class="row m-t5">
            
            <div class="col-md-3">
              <p class="text-right m-t5">{$attribute_hierarchy['label']} {if $attribute_hierarchy['required'] == 'Y'}<span style="color:red">*</span>{else}&nbsp;{/if}</p>
              <p class="text-right poor-gray">{ci_language line="TYPE"} : {$attribute_hierarchy['type']}</p>  {*<abbr title="HyperText Markup Language" class="initialism">{$attribute_hierarchy['type']}</abbr>*}  
            </div>

            <div class="col-md-3">
              <select rel="attribute_with_hierarchy" data-name="{$attribute_hierarchy['id_config_attribute']}" onchange="getSelectShopAttributeGroup($(this))">
                <option value="">--------</option>

{*                {if isset($attribute_hierarchy['value_list']) && !empty($attribute_hierarchy['value_list'])}
                    {foreach $attribute_hierarchy['value_list'] as $value_list}
                        <option value="{$value_list['id_config_value_list']}">{$value_list['label']}</option>
                    {/foreach}

                {else}
*}
{*                    {if isset($shop_attribute['E']) && !empty($shop_attribute['E'])}
                        {foreach $shop_attribute['E'] as $value}
                            <option value="{$value['code']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value['code']}selected{/if} >{$value['label']}</option>
                        {/foreach}
                    {/if}*}

{*                    {if isset($shop_attribute['S']) && !empty($shop_attribute['S'])}
                        <optgroup label="{ci_language line="Field Shop"}">
                          {foreach $shop_attribute['S'] as $value}
                              {if ($value['type'] == 'VL')}
                                  <option value="{$value['code']}">{$value['label']}</option>
                              {/if}
                          {/foreach}
                        </optgroup>
                    {/if}*}

                    {if isset($shop_attribute['A']) && !empty($shop_attribute['A'])}
                        <optgroup label="{ci_language line="Attributes"}">
                          {foreach $shop_attribute['A'] as $value}
                              <option value="{$value['code']}">{$value['label']}</option>
                          {/foreach}
                        </optgroup>
                    {/if}

                    {if isset($shop_attribute['F']) && !empty($shop_attribute['F'])}
                        <optgroup label="{ci_language line="Features"}">
                          {foreach $shop_attribute['F'] as $value}
                              <option value="{$value['code']}">{$value['label']}</option>
                          {/foreach}
                        </optgroup>
                    {/if}
                    
               {* {/if}*}
              </select>
            </div>
              
            <div class="col-md-3">
              <input type="text" class="form-control" rel="default_value" data-name="{$attribute_hierarchy['id_config_attribute']}" data-type="{$attribute_hierarchy['type']}" {if $attribute_hierarchy['type'] == 'LIST' }disabled{/if} >
            </div>
              
            <div class="col-md-3">
              <p class="poor-gray text-left m-t5">{$attribute_hierarchy['description']}{if !empty($attribute_hierarchy['example'])} (E.g. {$attribute_hierarchy['example']}){/if}</p>
            </div>
            
          </div>
        </div>
    {/foreach}
{/if}