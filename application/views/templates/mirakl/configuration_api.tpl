{ci_config name="base_url"}
<link rel="stylesheet" href="{$cdn_url}assets/css/bootstrap/bootstrap.css"> 
<link rel="stylesheet" href="{$cdn_url}assets/css/font-awesome-4.3.0/css/font-awesome.min.css">

<script src="{$cdn_url}assets/js/jquery/jquery-1.11.2.min.js"></script>
<script src="{$cdn_url}assets/js/jquery/jquery-ui.min.js"></script>


<table class="table table-bordered table-striped" border="1">
    <tr>
        <th>Menu</th>
        <th>Marketplace</th>
        <th>Api</th>
        <th>Option</th>
    </tr>
    <tr>
        <td>Carrier</td>
        <td>
            <select id="carrier_sub_marketplace">
                <option value="">-----</option>
                {foreach $mirakl_marketplace as $key => $value}
                    <option value="{$value['id_offer_sub_marketplace']}">{$value['name_marketplace']}</option>
                {/foreach}
            </select>
        </td>
        <td><input type="text" id="carrier_api_key" class="form-control"></td>
        <td><button onclick="start_api('carrier', $('#carrier_sub_marketplace').val(), $('#carrier_api_key').val())" class="btn btn-success">Start</button></td>
    </tr>
    <tr>
        <td>Mirakl Hierarchy</td>
        <td>
            <select id="hierarchy_sub_marketplace">
                <option value="">-----</option>
                {foreach $mirakl_marketplace as $key => $value}
                    <option value="{$value['id_offer_sub_marketplace']}">{$value['name_marketplace']}</option>
                {/foreach}
            </select>
        <td><input type="text" id="hierarchy_api_key" class="form-control"></td>
        <td><button onclick="start_api('hierarchy', $('#hierarchy_sub_marketplace').val(), $('#hierarchy_api_key').val())" class="btn btn-success">Start</button></td>
    </tr>
    <tr>
        <td>Mirakl Attribute</td>
        <td>
            <select id="attribute_sub_marketplace">
                <option value="">-----</option>
                {foreach $mirakl_marketplace as $key => $value}
                    <option value="{$value['id_offer_sub_marketplace']}">{$value['name_marketplace']}</option>
                {/foreach}
            </select>
        </td>
        <td><input type="text" id="attribute_api_key" class="form-control"></td>
        <td><button onclick="start_api('attribute', $('#attribute_sub_marketplace').val(), $('#attribute_api_key').val())" class="btn btn-success">Start</button></td>
    </tr>
    <tr>
        <td>Mirakl Value List</td>
        <td>
            <select id="value_sub_marketplace">
                <option value="">-----</option>
                {foreach $mirakl_marketplace as $key => $value}
                    <option value="{$value['id_offer_sub_marketplace']}">{$value['name_marketplace']}</option>
                {/foreach}
            </select>
        </td>
        <td><input type="text" id="value_api_key" class="form-control"></td>
        <td><button onclick="start_api('value', $('#value_sub_marketplace').val(), $('#value_api_key').val())" class="btn btn-success">Start</button></td>
    </tr>

</table>
            
            
<script type="text/javascript">
    var base_url = '{$base_url}';

            
    function start_api(menu, sub_marketplace, api_key){
        
       {* var url = base_url + 'mirakl/startConfiguration';
        $.ajax({
            type: 'POST',
            data: { menu: menu, sub_marketplace: sub_marketplace, api_key: api_key },
            url: url,
            success: function(data){
                console.log(data);
            }
        });*}
        
        
        
    }
</script>
            
        