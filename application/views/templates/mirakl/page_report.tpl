
<!--Product-->
<div class="row">
    <div class="col-xs-12">
        <div class="headSettings clearfix b-Top-None">
            <div class="pull-sm-left clearfix">
                <h4 class="headSettings_head">{ci_language line="Feed report"}</h4>                      
            </div>
            <div class="pull-sm-right clearfix">
               <div class="showColumns clearfix"></div>
            </div>          
        </div><!--headSettings-->
                 
        <div class="row">
            <div class="col-xs-12">
                <table id="product_logs" class="mirakl-responsive-table hover">
                    <thead class="table-head">
                        <tr>
                            <th class="center">{ci_language line="Batch ID"}</th>
                            <th class="center">{ci_language line="Type"}</th>
                            <th class="center">{ci_language line="Import ID"}</th>
                            <th class="center">{ci_language line="Mode"}</th>
                            <th class="center">{ci_language line="Message"}</th>
                            <th class="center">{ci_language line="Process"}</th>
                            <th class="center">{ci_language line="Success"}</th>
                            <th class="center">{ci_language line="Warning"}</th>
                            <th class="center">{ci_language line="Error"}</th>
                            <th class="center">{ci_language line="Send date"}</th>
                        </tr>
                    </thead>              
                </table>
            </div> <!--col-xs-12-->
        </div><!--row-->

    </div><!--col-xs-12-->
</div><!--row--> 

<!--Product/Offer Error Log-->
<div class="m-t20 error_logs" style="opacity:0; height: 0px; overflow: hidden;" > <!-- -->
    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-t10 b-Top clearfix m-0">
                <span class="p-t10">{ci_language line="Message With Error"}</span>                         
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix b-Top-None">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Submission ID"}</h4>                      
                </div>
                <div class="pull-sm-right clearfix">
                   <div class="showColumns clearfix"></div>
                </div>          
            </div><!--headSettings-->
            <div class="row">
                <div class="col-xs-12">
                    <table id="error_logs" class="mirakl-responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th>{ci_language line="Batch ID"}</th>
                                <th>{ci_language line="Type"}</th>
                                <th>{ci_language line="Refenence"}</th>
                                <th>{ci_language line="Error"}</th>
                                <th>{ci_language line="Warning"}</th>
                                <th>{ci_language line="Date"}</th>
                            </tr>
                        </thead>                        
                    </table>
                </div><!-- col-xs-12 -->
            </div><!-- row --> 
            
        </div><!-- col-xs-12 -->
    </div><!-- row --> 
</div>      

<!--Order Error Log-->
<div class="m-t20 error_order_logs" style="opacity:0; height: 0px; overflow: hidden;" > <!-- -->
    <div class="row">
        <div class="col-xs-12">
            <p class="head_text p-t10 b-Top clearfix m-0">
                <span class="p-t10">{ci_language line="Message With Error"}</span>                         
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="headSettings clearfix b-Top-None">
                <div class="pull-sm-left clearfix">
                    <h4 class="headSettings_head">{ci_language line="Submission ID"}</h4>                      
                </div>
                <div class="pull-sm-right clearfix">
                   <div class="showColumns clearfix"></div>
                </div>          
            </div><!--headSettings-->
            <div class="row">
                <div class="col-xs-12">
                    <table id="error_order_logs" class="mirakl-responsive-table">
                        <thead class="table-head">
                            <tr>
                                <th>{ci_language line="SKU"}</th>
                                <th>{ci_language line="Code"}</th>
                                <th>{ci_language line="Message"}</th>
                            </tr>
                        </thead>                        
                    </table>
                </div><!-- col-xs-12 -->
            </div><!-- row --> 
            
        </div><!-- col-xs-12 -->
    </div><!-- row --> 
</div>  

<!--Hidden-->
<input type="hidden" id="emptyError" value="{ci_language line="No Error Message"}" />


<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/page_report.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>
