<div id="has_category" class="row">
  <div class="col-md-12">
    <div class="validate blue">
      <div class="validateRow">
        <div class="validateCell">
          <i class="note"></i>
        </div>
        <div class="validateCell">
          <p class="pull-left">
            {ci_language line="There are informations for model should you know, (H) short from Hierarchy, (A) short from Attribute."}
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="main" class="row showSelectBlock mirakl-models">
  <div class="col-md-12 m-t10">
    <button type="button" class="link removeProfile remove">{ci_language line="Remove a model from list"}</button>
    <div class="profileBlock clearfix mirakl-model">

      <!-- Notice -->
      <div class="validate blue">
        <div class="validateRow">
          <div class="validateCell">
            <i class="note"></i>
          </div>
          <div class="validateCell">
            <p class="pull-left">
              {ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
            </p>
          </div>
        </div>
      </div>

      <!-- Model Name -->
      <p class="poor-gray">{ci_language line="Model name"}</p>
      <div class="form-group has-error">
        <input type="text" rel="model_name" value="{*{if isset($model_name)}{$model_name}{/if}*}" class="form-control" />
        <div class="error">
          <p>
            <i class="fa fa-exclamation-circle"></i>
            Define the model name first
          </p>
        </div>
      </div>
      <p class="poor-gray text-right">
        {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
      </p>

      <div class="row">
        <div class="col-md-12">
          <div class="b-Top p-t20 m-t10 clearfix">
            <p class="head_text p-b20">
              {ci_language line="Hierarchy"}
            </p>
          </div>
        </div>
      </div>

      <div class="hierarchy">

        <p class="poor-gray">{ci_language line="Hierarchy"}</p>
        <div class="form-group">
          <select rel="hierarchy_code" >
            <option value="">--------</option>

            {if isset($config_hierarchy) && !empty($config_hierarchy)}
                {foreach $config_hierarchy as $hierarchy}
                    <option value="{$hierarchy['id_config_hierarchy']}">{$hierarchy['label']} (H - {$hierarchy['count_sub_hierarchy']}) , (A - {$hierarchy['count_attribute']})</option>
                {/foreach}
            {/if}

          </select>
        </div>

        <!-- sub hierarchy -->
        <div class="sub_hierarchy">

        </div>

        <!-- attribute with hierarchy -->
        <div class="row">
          <div class="col-md-12">
            <div class="b-Top p-t20 m-t10 clearfix">
              <p class="head_text p-b20">
                {ci_language line="Attributes"}
              </p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
          </div>
          <div class="col-md-3">
            <p class="montserrat text-left text-info  text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
          </div>
          <div class="col-md-3">
            <p class="montserrat text-left text-info  text-uc m-b20">{ci_language line="DEFAULT VALUE"}</p>
          </div>
          <div class="col-md-3">
            <p class="montserrat text-left text-info text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
          </div>
        </div>

        <div class="hierarchy_attribute">

        </div>
      </div> <!-- #.hierarchy -->
    </div>
  </div>
</div>


<div id="tasks">
  {if isset($mapping_model) && !empty($mapping_model)}
      {foreach $mapping_model as $id_model => $model}
          <div class="row mirakl-models">

            <!-- list -->
            <div class="col-md-12">
              <div class="b-Bottom m--t1"><!-- clearfix b-Top -->
                <p class="dark-gray montserrat pull-left p-t10 m-b0">
                  {$model['model_name']}	                            
                </p>
                <ul class="pull-right miraklEdit">
                  <li class="editProfile">
                    <button type="button" class="link editProfile_link edit_model">
                      <i class="fa fa-pencil"></i> {ci_language line="edit"}
                    </button>
                  </li>
                  <li class="remProfile">
                    <button type="button" class="link removeProfile_link remove_model" rel="{$model['model_name']}" value="{$model['id_model']}" >
                      <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                    </button>  
                  </li>
                </ul>
              </div>
            </div>
            <!-- end list -->

            <div class="col-md-12 m-t5">
              <div class="profileBlock clearfix showSelectBlock mirakl-model">

                <!-- Model Name -->
                <p class="poor-gray">{ci_language line="Model name"}</p>
                <div class="form-group">
                  <input type="text" rel="model_name" name="model[{$id_model}][model_name]" value="{if isset($model['model_name'])}{$model['model_name']}{/if}" data-content="{if isset($model['model_name'])}{$model['model_name']}{/if}" class="form-control" />
                  <input type="hidden" rel="id_model" name="model[{$id_model}][id_model]" value="{if isset($model['id_model'])}{$model['id_model']}{/if}" data-content="{if isset($model['id_model'])}{$model['id_model']}{/if}" />
                </div>
                <p class="poor-gray text-right">
                  {ci_language line="Model names are used for Profiles tab. Please associate a friendly name to remember this model."}
                </p>

                <div class="row">
                  <div class="col-md-12">
                    <div class="b-Top p-t20 m-t10 clearfix">
                      <p class="head_text p-b20">
                        {ci_language line="Hierarchy"}
                      </p>
                    </div>
                  </div>
                </div>

                <div class="hierarchy">

                  <p class="poor-gray">{ci_language line="Hierarchy"}</p>
                  <div class="form-group">
                    <select rel="hierarchy_code" name="model[{$id_model}][hierarchy_code][]" class="search-select" onchange="getSubHierarchy($(this))">
                      <option value="">--------</option>
                      {if isset($config_hierarchy) && !empty($config_hierarchy)}
                          {foreach $config_hierarchy as $hierarchy}
                              <option value="{$hierarchy['id_config_hierarchy']}" {if isset($model['hierarchy_code'][0]) && $model['hierarchy_code'][0]['id_config_hierarchy'] == $hierarchy['id_config_hierarchy']}selected{/if}>{$hierarchy['label']} (H - {$hierarchy['count_sub_hierarchy']}) , (A - {$hierarchy['count_attribute']})</option>
                          {/foreach}
                      {/if}
                    </select>
                  </div>

                  <!-- sub hierarchy from mirakl -->
                  <div class="sub_hierarchy">
                    {if isset($model['hierarchy_code'][1])} <!-- sub hierarchy start with index 1 -->
                        {foreach $model['hierarchy_code'] as $index => $hierarchy}
                            {if ($index != 0)} <!-- sub hierarchy only with out index 1 -->
                                <div class="sub_hierarchy_level_{$index}" data-level="{$index}"> {* start index 1 *}
                                  <p class="poor-gray">{ci_language line="Sub Hierarchy"}</p>
                                  <div class="form-group">
                                    <select rel="hierarchy_code" name="model[{$id_model}][hierarchy_code][]" select-level="{$index}" class="search-select" onchange="getSubHierarchy($(this))">
                                      <option value="">--------</option>
                                      {if isset($model['hierarchy_code'][$index]['hierarchy_code_level']) && !empty($model['hierarchy_code'][$index]['hierarchy_code_level'])}
                                          {foreach $model['hierarchy_code'][$index]['hierarchy_code_level'] as $hierarchy_code_level}
                                              <option value="{$hierarchy_code_level['id_config_hierarchy']}" {if $hierarchy['id_config_hierarchy'] == $hierarchy_code_level['id_config_hierarchy']}selected{/if}>{$hierarchy_code_level['label']} (H - {$hierarchy['count_sub_hierarchy']}) , (A - {$hierarchy['count_attribute']})</option>
                                          {/foreach}
                                      {/if}
                                    </select>
                                  </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                  </div>

                  <!-- attribute with hierarchy -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="b-Top p-t20 m-t10 clearfix">
                        <p class="head_text p-b20">
                          {ci_language line="Attributes"}
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3">
                      <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
                    </div>
                    <div class="col-md-3">
                      <p class="montserrat text-left text-info  text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
                    </div>
                    <div class="col-md-3">
                      <p class="montserrat text-left text-info  text-uc m-b20">{ci_language line="DEFAULT VALUE"}</p>
                    </div>
                    <div class="col-md-3">
                      <p class="montserrat text-left text-info text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
                    </div>
                  </div>

                  <div class="hierarchy_attribute">
                    {if isset($model['attribute_with_hierarchy'])}
                        {foreach $model['attribute_with_hierarchy'] as $id_config_attribute => $attribute_with_hierarchy}
                            <div class="panel_attribute">
                              <div class="row m-t3 m-b3">
                                <div class="col-md-3">
                                  <p class="text-right m-t5">{$attribute_with_hierarchy['label']} {if $attribute_with_hierarchy['required'] == 'Y'}<span style="color:red">*</span>{else}&nbsp;{/if}</p>
                                  <p class="text-right poor-gray">{ci_language line="TYPE"} : {$attribute_with_hierarchy['type']}</p>
                                </div>
                                <div class="col-md-3">
                                  <select rel="attribute_with_hierarchy" name="model[{$id_model}][attribute_with_hierarchy][{$id_config_attribute}][shop_attribute]" class="search-select" data-name="{$id_config_attribute}" onchange="getSelectShopAttributeGroup($(this))">
                                    <option value="">--------</option>
                                    {*<option value="-" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == "-"}selected{/if}>{ci_language line="None"}</option>*}

                  {*                  {if isset($attribute_with_hierarchy['value_list']) && !empty($attribute_with_hierarchy['value_list'])}

                                        {foreach $attribute_with_hierarchy['value_list'] as $value_list}
                                            <option value="{$value_list['id_config_value_list']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value_list['id_config_value_list']}selected{/if} >{$value_list['label']}</option>
                                        {/foreach}

                                    {else}*}

                                        {if isset($shop_attribute['E']) && !empty($shop_attribute['E'])}
                                            {foreach $shop_attribute['E'] as $value}
                                                <option value="{$value['code']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value['code']}selected{/if} >{$value['label']}</option>
                                            {/foreach}
                                        {/if}


                                        {if isset($shop_attribute['S']) && !empty($shop_attribute['S'])}
                                            <optgroup label="{ci_language line="Field Shop"}">
                                              {foreach $shop_attribute['S'] as $value}
                                                  {if ($value['type'] == 'VL')}
                                                      <option value="{$value['code']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value['code']}selected{/if} >{$value['label']}</option>
                                                  {/if}
                                              {/foreach}
                                            </optgroup>
                                        {/if}

                                        {if isset($shop_attribute['A']) && !empty($shop_attribute['A'])}
                                            <optgroup label="{ci_language line="Attributes"}">
                                              {foreach $shop_attribute['A'] as $value}
                                                  <option value="{$value['code']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value['code']}selected{/if} >{$value['label']}</option>
                                              {/foreach}
                                            </optgroup>
                                        {/if}

                                        {if isset($shop_attribute['F']) && !empty($shop_attribute['F'])}
                                            <optgroup label="{ci_language line="Features"}">
                                              {foreach $shop_attribute['F'] as $value}
                                                  <option value="{$value['code']}" {if isset($attribute_with_hierarchy['shop_attribute']) && $attribute_with_hierarchy['shop_attribute'] == $value['code']}selected{/if} >{$value['label']}</option>
                                              {/foreach}
                                            </optgroup>
                                        {/if}

                               {*     {/if}*}
                                  </select>
                                </div>

                                <div class="col-md-3">
                                  <input type="text" class="form-control" name="model[{$id_model}][attribute_with_hierarchy][{$id_config_attribute}][default_value]" value="{$attribute_with_hierarchy['default_value']}" {if $attribute_with_hierarchy['type'] == 'LIST' }disabled{/if} >
                                </div>

                                <div class="col-md-3">
                                  <p class="poor-gray text-left m-t5">{$attribute_with_hierarchy['description']}{if !empty($attribute_with_hierarchy['example'])} (E.g. {$attribute_with_hierarchy['example']}){/if}</p>
                                </div>

                              </div>

                              {*                              {if isset($attribute_with_hierarchy['value_list']) && !empty($attribute_with_hierarchy['value_list'])}
                              {foreach $attribute_with_hierarchy['value_list'] as $value_list}
                              <div class="row m-t3 m-b3 panel_attribute_value">
                              <div class="col-md-3">
                              <p class="poor-gray text-right m-t5">{$value_list['label_list']}</p>
                              </div>
                              <div class="col-md-3">
                              <select rel="attribute_with_list" name="model[{$id_model}][attribute_with_hierarchy][{$id_config_attribute}][attribute_value][{$value_list['id_config_value_list']}]" data-attribute="{$id_config_attribute}"  data-name="{$value_list['id_config_value_list']}" class="search-select">
                              <option value="">---</option>

                              {if (isset($attribute_with_hierarchy['attribute']) && !empty($attribute_with_hierarchy['attribute']))}
                              {foreach $attribute_with_hierarchy['attribute'] as $attribute}
                              <option value="{$attribute['field_shop_code']}" {if isset($value_list['shop_attribute_value']) &&  $value_list['shop_attribute_value'] == $attribute['field_shop_code']}selected{/if} >{$attribute['field_shop_label']}</option>
                              {/foreach}
                              {/if}

                              </select>
                              </div>
                              <div class="col-md-3">

                              </div>
                              </div>
                              {/foreach}
                              {/if}*}

                            </div>
                        {/foreach}
                    {/if}
                  </div>
                </div> <!-- #.hierarchy -->
              </div>
            </div>
          </div>
      {/foreach}
  {/if}
</div>

<input type="hidden" id="success-message" value="{ci_language line="Delete complete"}" />
<input type="hidden" id="delete-success-message" value="{ci_language line="Model was deleted"}" />
<input type="hidden" id="error-message" value="{ci_language line="Error"}" />
<input type="hidden" id="delete-unsuccess-message" value="{ci_language line="Can not delete"}" />
<script src="{$cdn_url}assets/js/FeedBiz/mirakl/config_models.js"></script>