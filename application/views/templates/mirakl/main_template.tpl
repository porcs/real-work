{include file="header.tpl"}
{include file="sidebar.tpl"}
{ci_config name="base_url"}

<form id="form_submit" class="custom-form" method="POST" action="{$base_url}mirakl/{$action}" enctype="multipart/form-data" autocomplete="off">
  <div class="main p-rl40 p-xs-rl20">

    <!-- include header page -->
    {include file="mirakl/main_header.tpl"}

    <!-- include current page -->
    {include file="mirakl/{$current_page}.tpl"}

    <!-- include footer page -->
    {include file="mirakl/main_footer.tpl"}

    <!-- parameter --> 
    <input type="hidden" id="id_user"         name="id_user"          value="{if isset($id_user)}{$id_user}{/if}" />
    <input type="hidden" id="ext"             name="ext"              value="{if isset($ext)}{$ext}{/if}" />
    <input type="hidden" id="marketplace"     name="marketplace"      value="{if isset($marketplace)}{$marketplace}{/if}" />
    <input type="hidden" id="id_country"      name="id_country"       value="{if isset($id_country)}{$id_country}{/if}" />
    <input type="hidden" id="sub_marketplace" name="sub_marketplace"  value="{if isset($sub_marketplace)}{$sub_marketplace}{/if}" />

    <input type="hidden" id="message-delete" value="{ci_language line="Do you want to delete"}" />
    <input type="hidden" id="emptyTable" value="{ci_language line="No data available in table"}" />
    <input type="hidden" id="info" value="{ci_language line="Showing _START_ to _END_ of _TOTAL_ entries"}" />
    <input type="hidden" id="infoEmpty" value="{ci_language line="Showing 0 to 0 of 0 entries"}" />
    <input type="hidden" id="infoFiltered" value="{ci_language line="(filtered from _MAX_ total entries)"}" />
    <input type="hidden" id="infoPostFix" value />
    <input type="hidden" id="thousands" value="," />
    <input type="hidden" id="lengthMenu" value="{ci_language line="Per page: _MENU_"}" />
    <input type="hidden" id="loadingRecords" value="{ci_language line="Loading"}..." />
    <input type="hidden" id="processing" value="{ci_language line="Processing..."}" />
    <input type="hidden" id="search" value="{ci_language line="Search:"}" />
    <input type="hidden" id="zeroRecords" value="{ci_language line="No matching records found"}" />
    <input type="hidden" id="paginate-first" value="{ci_language line="First"}" />
    <input type="hidden" id="paginate-last" value="{ci_language line="Last"}" />
    <input type="hidden" id="paginate-next" value="{ci_language line="Next"}" />
    <input type="hidden" id="paginate-previous" value="{ci_language line="Previous"}" />
    <input type="hidden" id="aria-sortAscending" value=": {ci_language line="activate to sort column ascending"}" />
    <input type="hidden" id="aria-sortDescending" value=": {ci_language line="activate to sort column descending"}" />

    <input type="hidden" value="{if isset($active_api)}{$active_api}{/if}" id="active_api">
    <input type="hidden" value="{ci_language line="No API Connection established due to API connection keys missing or inactive. Please go to Configurations &gt; Parameters Tab to set API keys."}" id="error-inactive-message" />

    <input type="hidden" value="{if isset($active_file)}{$active_file}{/if}" id="active_file">
    <input type="hidden" id="error-active-file" value="{ci_language line="A server-side error has occured. Please contact your server administrator, hostmaster or webmaster."}" />

    <script type="text/javascript" src="{$cdn_url}assets/js/jquery.validate.min.js"></script>
    {*<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>*}

  </div>
</form>
{include file="footer.tpl"}