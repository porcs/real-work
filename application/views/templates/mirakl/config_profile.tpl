
<div id="main" class="mirakl-profiles showSelectBlock">
    <div class="col-md-10 col-lg-9 custom-form m-t10 m-b30">   
        <button class="link removeProfile remove" type="button">{ci_language line="Remove a profile from list"}</button>

        <div class="profileBlock clearfix mirakl-profile">

            <!--Information-->
            <div class="validate blue">
                <div class="validateRow">
                    <div class="validateCell">
                        <i class="note"></i>
                    </div>
                    <div class="validateCell">
                        <p class="pull-left">
                            {ci_language line="Do not forget to click on the SAVE button at the bottom of the page !"}
                        </p>
                    </div>
                </div>
            </div>

            <!--Profile Name-->
            <p class="regRoboto poor-gray">{ci_language line="Profile Name"}</p>
            <div class="form-group">
                <input type="text" rel="name" class="form-control" /> 
            </div>
            <p class="poor-gray text-right">
                {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
            </p>

            <!--Model-->
            <p class="regRoboto poor-gray">{ci_language line="Model"}</p>
            <div class="form-group">
                <select rel="id_model" class="form-control">
                    <option value="">{ci_language line="Choose a field"}</option>
                    {if isset($models) && !empty($models)}
                        {foreach $models as $model}
                            <option value="{$model.id_model}">{$model.model_name}</option>
                        {/foreach}
                    {/if}
                </select>
            </div>

            <!--Price Rule-->
            <div class="price_rules">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Price Rules"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row price_rule_main">
                    <input type="hidden" rel="price_rule_count" value="1" />
                    <div class="col-xs-12">
                        <p class="poor-gray">{ci_language line="Default Price Rule"}</p>
                    </div>
                    <div class="price_rule_li" rel="1">
                        <div class="col-sm-3">
                            <div class="form-group clearfix m-b0">
                                <select rel="type" class="price_rule" disabled>
                                    <option value="percent" selected="">{ci_language line="Percentage"}</option>
                                    <option value="value">{ci_language line="Value"}</option>
                                </select>	                               
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                </span>
                                <input rel="from" class="form-control price_rule" type="text" disabled placeholder="{ci_language line="From"}"> 
                            </div>
                            <i class="blockRight noBorder"></i>
                        </div>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                </span>
                                <input rel="to" class="form-control price_rule" type="text" disabled placeholder="{ci_language line="To"}"> 
                            </div>
                            <i class="blockRight noBorder"></i>
                            <i class="blockRightDouble noBorder"></i>
                        </div>
                        <div class="col-sm-3 pr-0">
                            <div class="form-group withIcon clearfix disabled-none">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <strong>%</strong>
                                    </span>
                                    <input rel="value" class="form-control price_rule m-b0" type="text" disabled >
                                </div>
                                <i class="cb-plus good addPriceRule m-0"></i>
                                <i class="cb-plus bad removePriceRule m-0" style="display: none"></i>
                            </div>							
                        </div>
                        <div class="form-group has-error" style="display:none">
                            <div class="error text-right">
                                <p class="m--t10 p-0">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="poor-gray">
                            {ci_language line="You should configure a price rule in value or in percentage for one or several prices ranges."}
                        </p>
                    </div>
                </div>

                <div class="row m-t20">
                    <!-- synchronization Field -->
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <p class="regRoboto poor-gray">{ci_language line="Synchronization Field"}</p>
                        <div class="m-b10 synch">
                            <div class="choZn">
                                <select data-placeholder="Choose" rel="synchronization_field" disabled>
                                    <option value="" disabled="disabled">{ci_language line="Choose a field"}</option>
                                    <option value="{MiraklParameter::SYNC_FIELD_EAN}">{ci_language line="EAN13 (Europe)"}</option>
                                    <option value="{MiraklParameter::SYNC_FIELD_UPC}">{ci_language line="UPC (United States)"}</option>
                                    <option value="{MiraklParameter::SYNC_FIELD_SKU}">{ci_language line="SKU"}</option>
                                </select>  							
                            </div>
                        </div>	
                        <p class="regRoboto poor-gray text-left">{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Mirakl."}</p>
                    </div>
                </div>

                <div class="row m-t20">
                    <div class="col-sm-5 col-md-6 col-lg-5">
                        <p class="montserrat dark-gray text-uc">{ci_language line="Product Name"}</p>
                        <label class="mirakl-radio w-100 checked">
                            <input rel="product_format" type="radio" checked="checked" value="1" disabled>
                            {ci_language line="Name Only"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="product_format" type="radio" value="2" disabled>
                            {ci_language line="Name and Attributes"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="product_format" type="radio" value="3" disabled>
                            {ci_language line=" Brand, Name and Attributes"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="product_format" type="radio" value="4" disabled>
                            {ci_language line=" Name, Brand and Attributes"}
                        </label>
                    </div>
                    <!-- Description field -->
                    <div class="col-xs-12">
                        <p class="poor-gray m-t10">
                            {ci_language line="Format of product name"}
                        </p>
                    </div>
                </div>

                <div class="row m-t20">
                    <div class="col-sm-5 col-md-6 col-lg-5">
                        <p class="montserrat dark-gray text-uc">{ci_language line="Ignore Products without Images"}</p>
                        <label class="mirakl-radio w-100 checked">
                            <input rel="no_image" type="radio" checked="checked" value="1" disabled>
                            {ci_language line="Yes"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="no_image" type="radio" value="0" disabled>
                            {ci_language line="No (Not Recommended)"}
                        </label>
                    </div>
                    {*<!-- Description field -->
                    <div class="col-xs-12">
                        <p class="poor-gray m-t10">
                            {ci_language line="Description fields to be send"}
                        </p>
                    </div>*}
                </div>

                <div class="row m-t20">
                    <!-- Description Field -->
                    <div class="col-sm-5 col-md-6 col-lg-5">
                        <p class="montserrat dark-gray text-uc">{ci_language line="Description Field"}</p>
                        <label class="mirakl-radio w-100 checked">
                            <input rel="description_field" type="radio" checked="checked" value="1" disabled>
                            {ci_language line="Description"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="description_field" type="radio" value="2" disabled>
                            {ci_language line="Short Description"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="description_field" type="radio" value="3" disabled>
                            {ci_language line="Both"}
                        </label>
                        <label class="mirakl-radio w-100">
                            <input rel="description_field" type="radio" value="4" disabled>
                            {ci_language line="None"}
                        </label>
                    </div>
                    <!-- Description field -->
                    <div class="col-xs-12">
                        <p class="poor-gray m-t10">
                            {ci_language line="Whether to send short or long description of the product to Mirakl."}
                        </p>
                    </div>
                </div>
                <div class="row"> 
                    <!-- HTML Descriptions -->
                    <div class="col-xs-12">
                        <p class="montserrat dark-gray text-uc m-t20">{ci_language line="HTML Descriptions"}</p>
                        <label class="mirakl-checkbox">
                            <input rel="html_description" type="checkbox" disabled value="1">{ci_language line="Yes"}                             
                        </label>							
                        <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
                    </div>
                </div>
                <!--Rounding-->
                <div class="row">
                    <div class="col-xs-12">
                        <p class="montserrat dark-gray text-uc m-t20">{ci_language line="Rounding"}</p>
                        <label class="mirakl-radio w-100">
                            <input rel="[price_rules][rounding]" type="radio" value="1"  disabled>
                            {ci_language line="One Digit"}
                        </label>	
                        <label class="mirakl-radio w-100 checked">
                            <input rel="[price_rules][rounding]" checked type="radio" value="2" disabled>
                            {ci_language line="Two Digits"}
                        </label>	
                        <label class="mirakl-radio w-100">
                            <input rel="[price_rules][rounding]" type="radio" value="0" disabled>
                            {ci_language line="None"}
                        </label>						
                    </div>
                </div>
            </div>

            <!--Shipping Increase/Decrease-->
            <div class="shipping">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Shipping Increase/Decrease"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-12">                  
                        <input type="text" rel="shipping" class="form-control" />                 										
                    </div>
                </div>
            </div>

            <!--Warranty-->
            <div class="warranty">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Warranty"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-3">                  
                        <input type="text" rel="warranty" class="form-control" />                										
                    </div>
                </div>
            </div>                              

            <!--Combinations Parameters-->
            <div class="combinations">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Combinations Parameters"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-12">
                        <label class="mirakl-checkbox checked">
                            <input rel="combinations_long_attr" checked type="checkbox" value="1" disabled  /> Combinations Parameters
                        </label>
                    </div>
                </div>
            </div>

            <!--Min Quantity Alert-->
            <div class="quantity">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Min Quantity Alert"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-3">                  
                        <input type="text" rel="min_quantity_alert" class="form-control" />              										
                    </div>
                </div>
            </div>

            <!--Logistic Class-->
            <div class="logistic">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="b-Top p-t20 m-t10 clearfix">
                            <p class="head_text p-b20">
                                {ci_language line="Logistic Class"}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col-sm-3">                  
                        <input type="text" rel="logistic_class" class="form-control" />            										
                    </div>
                </div>
            </div>

            {if isset($additionnals) &&  !empty($additionnals)}
                {foreach $additionnals as $additionnal}

                    <div class="additionnals">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="b-Top p-t20 m-t10 clearfix">
                                    <p class="head_text p-b20">
                                        {$additionnal.label}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-3">                  
                                <select rel="[additionnals][{$additionnal.code}][option]" class="form-control">
                                    <option value="">{ci_language line="Choose a field"}</option>
                                    <option value="{MiraklParameter::DEFAULT_VALUE}">{ci_language line="Default Value"}</option>
                                    {if isset($attributes) && !empty($attributes)}
                                        <optgroup label="{ci_language line="Attributes"}">
                                            {foreach from=$attributes key=attribute_key item=attribute_item}
                                                <option value="a-{$attribute_key}">{$attribute_item.name}</option>
                                            {/foreach}
                                        </optgroup>
                                    {/if}

                                    {if isset($features) && !empty($features)}
                                        <optgroup label="{ci_language line="Features"}">
                                            {foreach from=$features key=feature_key item=feature_item}
                                                <option value="f-{$feature_key}">{$feature_item.name}</option>
                                            {/foreach}
                                        </optgroup>
                                    {/if}

                                    {if isset($prestashop_fields) && is_array($prestashop_fields) && !empty($prestashop_fields)}
                                        <optgroup label="{ci_language line="Field"}">
                                            {foreach from=$prestashop_fields key=id item=prestashop_field}
                                                <option value="p-{$id}">{$prestashop_field}</option>
                                            {/foreach}
                                        </optgroup>
                                    {/if}
                                </select>          										
                            </div>
                            <div class="col-sm-9">  
                                <input type="text" rel="[additionnals][{$additionnal.code}][text]" class="form-control" style="display: none;" />  
                            </div>
                        </div>
                    </div>                  

                {/foreach}
            {/if}
        </div>
    </div>
</div><!-- id main -->

<div id="tasks">
    {if isset($profile) && !empty($profile)}
        {foreach from=$profile key=k item=data}
            <!-- mirakl-profiles -->
            <div class="row mirakl-profiles">

                <!-- list -->
                <div class="col-xs-12">

                    <div class="b-Bottom clearfix m--t1"><!-- b-Top -->
                        <p class="dark-gray montserrat pull-left p-t10 m-b0">
                            {if isset($data.name)}
                                {$data.name}
                            {/if}
                            {if isset($data.id_model) && !empty($data.id_model) && isset($data.model_name) && !empty($data.model_name)}
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                                &nbsp;
                                {$data.model_name}
                            {/if}	                            
                        </p>

                        <ul class="pull-right miraklEdit">
                            <li class="editProfile">
                                <button type="button" class="link editProfile_link edit_profile">
                                    <i class="fa fa-pencil"></i> {ci_language line="edit"}
                                </button>
                            </li>
                            <li class="remProfile">
                                <button type="button" class="link removeProfile_link remove_profile" rel="{$data.name}" value="{$data.id_profile}" >
                                    <i class="fa fa-trash-o"></i> {ci_language line="delete"}
                                </button>  
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- end list -->

                <!-- form -->
                <div class="col-md-10 col-lg-9">
                    <div rel="{$k}" class="profileBlock clearfix {if !isset($popup_more['no_model'])}showSelectBlock{/if} m-t35 m-b30 mirakl-profile">
                        <input type="hidden" rel="id_profile" name="profile[{$k}][id_profile]" value="{$data.id_profile}"/>

                        <!--Profile Name-->
                        <p class="regRoboto poor-gray">{ci_language line="Profile Name"}</p>
                        <div class="form-group">
                            <input type="text" rel="name" name="profile[{$k}][name]" value="{if isset($data.name)}{$data.name}{/if}" class="form-control"/>	
                        </div>
                        <p class="poor-gray text-right">
                            {ci_language line="Profile names are used for Categories. Please give a friendly name to remember this profile."}
                        </p>       

                        <!--Model-->
                        <p class="regRoboto poor-gray">{ci_language line="Model"}</p>
                        <div class="form-group">
                            <select rel="id_model" name="profile[{$k}][id_model]" class="form-control">
                                <option value="">{ci_language line="Choose a field"}</option>
                                {if isset($models) && !empty($models)}
                                    {foreach from=$models key=model_key item=model}
                                        <option value="{$model.id_model}" {if  $model.id_model == $data.id_model}selected{/if}  >{$model.model_name}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>

                        <!-- Price Rules -->
                        <div class="price_rules">
                            <!--head-->
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Price Rules"}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <!-- Default Price Rule -->
                            <div class="row price_rule_main">
                                <input type="hidden" rel="price_rule_count" value="{if isset($data.price_rules.value)}{count($data.price_rules.value)-1}{else}0{/if}" />
                                <div class="col-xs-12">
                                    <p class="poor-gray">{ci_language line="Default Price Rule"}</p>
                                </div>  
                            </div>  

                            {if isset($data.price_rules.value) && !empty($data.price_rules.value)}
                                {$i = 0}
                                {foreach from=$data.price_rules.value key=price_rules_key item=price_rules_item}
                                    <div class="price_rule_li" rel="{$i++}">
                                        <!--select-->
                                        <div class="col-sm-3">
                                            <div class="form-group clearfix m-b0">
                                                <select rel="type" class="search-select price_rule" name="profile[{$k}][price_rules][value][{$i}][type]" id="profile_{$k}_price_rules_value_{$i}_type" data-row="{$i}" data-content="{$k}">
                                                    <option value="percent" {if isset($price_rules_item['type']) && $price_rules_item['type'] == "percent"}selected{/if}>{ci_language line="Percentage"}</option>
                                                    <option value="value" {if isset($price_rules_item['type']) && $price_rules_item['type'] == "value"}selected{/if}>{ci_language line="Value"}</option>
                                                </select>	                               
                                            </div>
                                        </div>
                                        <!--from-->
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                                </span>
                                                <input rel="from" class="form-control price_rule" type="text" {if isset($price_rules_item['from'])}value="{$price_rules_item['from']}"{/if} name="profile[{$k}][price_rules][value][{$i}][from]" id="profile_{$k}_price_rules_value_{$i}_from" data-row="{$i}" data-content="{$k}"> 
                                            </div>
                                            <i class="blockRight noBorder"></i>
                                        </div>       
                                        <!--to-->
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                                </span>
                                                <input rel="to" class="form-control price_rule" type="text" {if isset($price_rules_item['to'])}value="{$price_rules_item['to']}"{/if} name="profile[{$k}][price_rules][value][{$i}][to]" id="profile_{$k}_price_rules_value_{$i}_to" data-row="{$i}" data-content="{$k}"> 
                                            </div>
                                            <i class="blockRight noBorder"></i>
                                            <i class="blockRightDouble noBorder"></i>
                                        </div>


                                        <!--value-->
                                        <div class="col-sm-3 pr-0">
                                            <div class="form-group withIcon clearfix disabled-none">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <strong>
                                                            {if isset($price_rules_item['type']) && $price_rules_item['type'] == "percent"}
                                                                %
                                                            {else}
                                                                {if isset($currency_sign)}
                                                                    {$currency_sign}
                                                                {/if}
                                                            {/if}
                                                        </strong>
                                                    </span>
                                                    <input rel="value" class="form-control price_rule m-b0" type="text" {if isset($price_rules_item['value'])}value="{$price_rules_item['value']}"{/if} name="profile[{$k}][price_rules][value][{$i}][value]" id="profile_{$k}_price_rules_value_{$i}_value" data-row="{$i}" data-content="{$k}">
                                                </div>

                                                <!--add/remove-->														
                                                {if $i == count($data.price_rules.value)}
                                                    <i class="cb-plus good addPriceRule m-0"></i>
                                                {/if}
                                                <i class="cb-plus bad removePriceRule m-0" {if $i == count($data.price_rules.value)}style="display: none"{/if}></i>
                                            </div>							
                                        </div>

                                        <!--error-->
                                        <div class="form-group has-error" style="display:none">
                                            <div class="error text-right">
                                                <p class="m--t10 p-0">
                                                    <i class="fa fa-exclamation-circle"></i>
                                                    <span></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                            {else}
                                <div class="price_rule_li" rel="0">
                                    <!--select-->
                                    <div class="col-sm-3">
                                        <div class="form-group clearfix m-b0">
                                            <select rel="type" class="form-control price_rule" name="profile[{$k}][price_rules][value][1][type]" id="profile_{$k}_price_rules_value_1_type" data-row="1" data-content="{$k}">
                                                <option value="percent" selected="">{ci_language line="Percentage"}</option>
                                                <option value="value">{ci_language line="Value"}</option>
                                            </select>                               
                                        </div>
                                    </div>
                                    <!--from-->
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                            </span>
                                            <input rel="from" class="form-control price_rule" type="text" name="profile[{$k}][price_rules][value][1][from]" id="profile_{$k}_price_rules_value_1_from" data-row="1" data-content="{$k}"> 
                                        </div>
                                        <i class="blockRight noBorder"></i>
                                    </div>
                                    <!--to-->
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <strong>{if isset($currency_sign)}{$currency_sign}{/if}</strong>
                                            </span>
                                            <input rel="to" class="form-control price_rule" type="text" name="profile[{$k}][price_rules][value][1][to]" id="profile_{$k}_price_rules_value_1_to" data-row="1" data-content="{$k}">  
                                        </div>
                                        <i class="blockRight noBorder"></i>
                                        <i class="blockRightDouble noBorder"></i>
                                    </div>
                                    <!--value-->
                                    <div class="col-sm-3 pr-0">
                                        <div class="form-group withIcon clearfix disabled-none">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <strong>%</strong>
                                                </span>
                                                <input rel="value" class="form-control price_rule m-b0" type="text" name="profile[{$k}][price_rules][value][1][value]" id="profile_{$k}_price_rules_value_1_value" data-row="1" data-content="{$k}" > 
                                            </div>
                                            <!--add/remove-->														
                                            <i class="cb-plus good addPriceRule m-0"></i>
                                            <i class="cb-plus bad removePriceRule m-0" style="display: none"></i>
                                        </div>							
                                    </div>
                                    <!--error-->
                                    <div class="form-group has-error" style="display:none">
                                        <div class="error text-right">
                                            <p class="m--t10 p-0">
                                                <i class="fa fa-exclamation-circle"></i>
                                                <span></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            {/if}
                        </div>
                        <!-- end Price Rules -->        

                        <div class="row">
                            <div class="col-xs-12">
                                <p class="poor-gray">
                                    {ci_language line="You should configure a price rule in value or in percentage for one or several prices ranges."}
                                </p>
                            </div>
                        </div>

                        <div class="row m-t20">
                            <!-- synchronization Field -->
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <p class="regRoboto poor-gray">{ci_language line="Synchronization Field"}</p>
                                <div class="m-b10 synch">
                                    <div class="choZn">
                                        <select class="search-select" rel="synchronization_field" name="profile[{$k}][synchronization_field]">
                                            <option value="" disabled="disabled">{ci_language line="Choose one of the following"}</option>
                                            <option value="{MiraklParameter::SYNC_FIELD_EAN}" {if isset($data.synchronization_field) && $data.synchronization_field == "ean13"}selected{/if}>{ci_language line="EAN13 (Europe)"}</option>
                                            <option value="{MiraklParameter::SYNC_FIELD_UPC}"{if isset($data.synchronization_field) && $data.synchronization_field == "upc"}selected{/if}>{ci_language line="UPC (United States)"}</option>
                                            <option value="{MiraklParameter::SYNC_FIELD_SKU}"{if isset($data.synchronization_field) && $data.synchronization_field == "reference"}selected{/if}>{ci_language line="SKU"}</option>
                                        </select>   							
                                    </div>
                                </div>	
                                <p class="regRoboto poor-gray text-left">{ci_language line="Choose the Product Attribute field which will be used for determination of presence of this product on Mirakl."}</p>
                            </div>
                        </div>

                        <div class="row m-t20">
                            <div class="col-sm-5 col-md-6 col-lg-5">
                                <p class="montserrat dark-gray text-uc">{ci_language line="Product Name"}</p>
                                <label class="cb-radio w-100 {if isset($data.product_format) && $data.product_format == 1}checked{/if}">
                                    <input rel="product_format" type="radio" {if isset($data.product_format) && $data.product_format == 1}checked{/if} value="1"  name="profile[{$k}][product_format]">
                                    {ci_language line="Name Only"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.product_format) && $data.product_format == 2}checked{/if}">
                                    <input rel="product_format" type="radio" {if isset($data.product_format) && $data.product_format == 2}checked{/if} value="2"  name="profile[{$k}][product_format]">
                                    {ci_language line="Name and Attributes"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.product_format) && $data.product_format == 3}checked{/if}">
                                    <input rel="product_format" type="radio" {if isset($data.product_format) && $data.product_format == 3}checked{/if} value="3"  name="profile[{$k}][product_format]">
                                    {ci_language line="Brand, Name and Attributes"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.product_format) && $data.product_format == 4}checked{/if}">
                                    <input rel="product_format" type="radio" {if isset($data.product_format) && $data.product_format == 4}checked{/if} value="4"  name="profile[{$k}][product_format]">
                                    {ci_language line="Name, Brand and Attributes"}
                                </label>
                            </div>
                            <!-- Description field -->
                            <div class="col-xs-12">
                                <p class="poor-gray m-t10">
                                    {ci_language line="Format of product name"}
                                </p>
                            </div>
                        </div>

                        <div class="row m-t20">
                            <div class="col-sm-5 col-md-6 col-lg-5">
                                <p class="montserrat dark-gray text-uc">{ci_language line="Ignore Products without Images"}</p>
                                <label class="cb-radio w-100 {if isset($data.no_image) && $data.no_image == 1}checked{/if}">
                                    <input rel="no_image" type="radio" {if isset($data.no_image) && $data.no_image == 1}checked{/if} value="1"  name="profile[{$k}][no_image]">
                                    {ci_language line="Yes"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.no_image) && $data.no_image == 0}checked{/if}">
                                    <input rel="no_image" type="radio" {if isset($data.no_image) && $data.no_image == 0}checked{/if} value="0"  name="profile[{$k}][no_image]">
                                    {ci_language line="No (Not Recommended)"}
                                </label>
                            </div>
                            {*<!-- Description field -->
                            <div class="col-xs-12">
                                <p class="poor-gray m-t10">
                                    {ci_language line="Description fields to be send"}
                                </p>
                            </div>*}
                        </div>

                        <div class="row m-t20">
                            <!-- Description Field -->
                            <div class="col-sm-5 col-md-6 col-lg-5">
                                <p class="montserrat dark-gray text-uc">{ci_language line="Description Field"}</p>
                                <label class="cb-radio w-100 {if isset($data.description_field) && $data.description_field == 1}checked{/if}">
                                    <input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 1}checked{/if} value="1"  name="profile[{$k}][description_field]">
                                    {ci_language line="Description"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.description_field) && $data.description_field == 2}checked{/if} ">
                                    <input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 2}checked{/if} value="2"  name="profile[{$k}][description_field]">
                                    {ci_language line="Short Description"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.description_field) && $data.description_field == 3}checked{/if}">
                                    <input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 3}checked{/if} value="3"  name="profile[{$k}][description_field]">
                                    {ci_language line="Both"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.description_field) && $data.description_field == 4}checked{/if}">
                                    <input rel="description_field" type="radio" {if isset($data.description_field) && $data.description_field == 4}checked{/if} value="4"  name="profile[{$k}][description_field]">
                                    {ci_language line="None"}
                                </label>
                            </div>
                            <!-- Description field -->
                            <div class="col-xs-12">
                                <p class="poor-gray m-t10">
                                    {ci_language line="Whether to send short or long description of the product to Mirakl."}
                                </p>
                            </div>
                        </div>

                        <div class="row"> 
                            <!-- HTML Descriptions -->
                            <div class="col-xs-12">
                                <p class="montserrat dark-gray text-uc m-t20">{ci_language line="HTML Descriptions"}</p>
                                <label class="cb-checkbox {if isset($data.html_description) && $data.html_description == 1}checked{/if}">
                                    <input rel="html_description" type="checkbox" {if isset($data.html_description) && $data.html_description == 1}checked{/if} value="1"  name="profile[{$k}][html_description]">{ci_language line="Yes"}  
                                </label>							
                                <p class="regRoboto poor-gray text-left m-t5">{ci_language line="Export HTML Descriptions instead of Text Only"}</p>
                            </div>
                        </div>

                        <!--rounding-->
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="montserrat dark-gray text-uc m-t20">{ci_language line="Rounding"}</p>
                                <label class="cb-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 1 }checked{/if}">
                                    <input rel="[price_rules][rounding]" type="radio" value="1" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 1 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
                                    {ci_language line="One Digit"}
                                </label>	
                                <label class="cb-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 2 }checked{/if}">
                                    <input rel="[price_rules][rounding]" type="radio" value="2" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 2 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
                                    {ci_language line="Two Digits"}
                                </label>
                                <label class="cb-radio w-100 {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 0 }checked{/if}">
                                    <input rel="[price_rules][rounding]" type="radio" value="0" {if isset($data.price_rules.rounding) && $data.price_rules.rounding == 0 }checked{/if} name="profile[{$k}][price_rules][rounding]" id="profile_{$k}_price_rules_rounding" data-content="{$k}">
                                    {ci_language line="None"}
                                </label>							
                            </div>
                        </div>

                        <!--Shipping Increase/Decrease-->
                        <div class="shipping">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Shipping Increase/Decrease"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-sm-12">                  
                                    <input type="text" rel="shipping" name="profile[{$k}][shipping]" value="{if isset($data.shipping)}{$data.shipping}{/if}" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <!--Warranty-->
                        <div class="warranty">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Warranty"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-sm-3">                  
                                    <input type="text" rel="warranty" name="profile[{$k}][warranty]" value="{if isset($data.warranty)}{$data.warranty}{/if}" class="form-control"/>
                                </div>
                            </div>
                        </div>  

                        <!--Combinations Parameters-->
                        <div class="combinations">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Combinations Parameters"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-sm-3">
                                    {if isset($data.combinations_long_attr) && $data.combinations_long_attr == 1}
                                        <label class="cb-checkbox checked">
                                            <input rel="combinations_long_attr" name="profile[{$k}][combinations_long_attr]" value="1" type="checkbox" checked > Combinations Parameters
                                        </label>
                                    {else}
                                        <label class="cb-checkbox">
                                            <input rel="combinations_long_attr" name="profile[{$k}][combinations_long_attr]"  type="checkbox" > Combinations Parameters
                                        </label>
                                    {/if}
                                </div>
                            </div>
                        </div>

                        <!--Min Quantity Alert-->
                        <div class="quantity">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Min Quantity Alert"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-sm-3">                  
                                    <input type="text" rel="min_quantity_alert" name="profile[{$k}][min_quantity_alert]" value="{if isset($data.min_quantity_alert)}{$data.min_quantity_alert}{/if}" class="form-control"/>                            
                                </div>
                            </div>
                        </div>

                        <!--Logistic Class-->
                        <div class="logistic">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="b-Top p-t20 m-t10 clearfix">
                                        <p class="head_text p-b20">
                                            {ci_language line="Logistic Class"}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-sm-12">
                                    <input type="text" rel="logistic_class" name="profile[{$k}][logistic_class]" value="{if isset($data.logistic_class)}{$data.logistic_class}{/if}" class="form-control"/> 
                                </div>
                            </div>
                        </div>

                        {if isset($additionnals) && is_array($additionnals) && !empty($additionnals)}
                            {foreach from=$additionnals key=additionnal_key item=additionnal}
                                <div class="additionnals">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="b-Top p-t20 m-t10 clearfix">
                                                <p class="head_text p-b20">
                                                    {$additionnal.label}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-sm-3">              
                                            <select rel="[additionnals][{$additionnal.code}][option]"  name="profile[{$k}][additionnals][{$additionnal.code}][option]" id="profile_{$data.name}_[additionnals][{$additionnal.code}][option]"   class="form-control">
                                                <option value="" >{ci_language line="Choose a field"}</option>
                                                <option value="{MiraklParameter::DEFAULT_VALUE}" {if isset($data.other[{$additionnal.code}]['option']) && $data.other[{$additionnal.code}]['option'] == "{MiraklParameter::DEFAULT_VALUE}"}selected{/if} >{ci_language line="Default Value"}</option>

                                                {if isset($attributes) && !empty($attributes)}
                                                    <optgroup label="{ci_language line="Attributes"}">
                                                        {foreach from=$attributes key=attribute_key item=attribute_item}
                                                            <option value="a-{$attribute_key}"  {if isset($data.other[{$additionnal.code}]['option']) && $data.other[{$additionnal.code}]['option'] == "a-{$attribute_key}" }selected{/if}>{$attribute_item.name}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}

                                                {if isset($features) && !empty($features)}
                                                    <optgroup label="{ci_language line="Features"}">
                                                        {foreach from=$features key=feature_key item=feature_item}
                                                            <option value="f-{$feature_key}"    {if isset($data.other[{$additionnal.code}]['option']) && $data.other[{$additionnal.code}]['option'] == "f-{$feature_key}" }selected{/if}>{$feature_item.name}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}

                                                {if isset($prestashop_fields) && is_array($prestashop_fields) && !empty($prestashop_fields)}
                                                    <optgroup label="{ci_language line="Field"}">
                                                        {foreach from=$prestashop_fields key=id item=prestashop_field}
                                                            <option value="p-{$id}"         {if isset($data.other[{$additionnal.code}]['option']) && $data.other[{$additionnal.code}]['option'] == "p-{$id}" }selected{/if}>{$prestashop_field}</option>
                                                        {/foreach}
                                                    </optgroup>
                                                {/if}
                                            </select>           										
                                        </div>
                                        <div class="col-sm-9">  
                                            <input type="text" rel="[additionnals][{$additionnal.code}][text]" name="profile[{$k}][additionnals][{$additionnal.code}][text]" value="{if isset($data.other[{$additionnal.code}]['text'])}{$data.other[{$additionnal.code}]['text']}{/if}" id="profile_{$data.name}_[additionnals][{$additionnal.code}][text]" class="form-control" {if isset($data.other[{$additionnal.code}]['text']) && !empty($data.other[{$additionnal.code}]['text'])} style="display: block;"  {else} style="display: none;" {/if}  />  
                                        </div>
                                    </div>
                                </div>                  
                            {/foreach}
                        {/if}
                    </div>
                </div>
                <!-- end form -->
            </div>
            <!-- end mirakl-profiles -->                     
        {/foreach}        
    {/if}

</div>

<script src="{$cdn_url}assets/js/FeedBiz/mirakl/config_profiles.js"></script>