<div class="row">
    <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
            <p class="head_text p-b20">
                {ci_language line="Carrier Mapping"}
            </p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="CARRIERS MARKETPLACE"}</p>
    </div>
    <div class="col-md-4">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="CARRIERS SHOP"}</p>
    </div>
    <div class="col-md-4">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="TRACKING URL"}</p>
    </div>
</div>

{if isset($carriers_marketplace) && !empty($carriers_marketplace)}
    {foreach $carriers_marketplace as $index_carrier => $carrier_marketplace}
        <div class="row m-t5 m-b5">
            <div class="col-md-3">
                <p class="text-right m-t5">{$carrier_marketplace['label']}</p>
                <input type="hidden" name="mapping_carrier[{$index_carrier}][id_config_carrier]" value ="{$carrier_marketplace['id_config_carrier']}" >
            </div>

            <div class="col-md-4">
                <select name="mapping_carrier[{$index_carrier}][id_carrier]" class="search-select">
                    <option value="">--------</option>
                    {if isset($carriers_shop) && !empty($carriers_shop)}
                        {foreach $carriers_shop as $carrier_shop}
                            <option value="{$carrier_shop['id_carrier']}" {if isset($carrier_marketplace['id_carrier']) && $carrier_marketplace['id_carrier'] == $carrier_shop['id_carrier']}selected{/if}  >{$carrier_shop['name']}</option>
                        {/foreach}
                    {/if}
                </select>
            </div>
            <div class="col-md-5">
                <p class="poor-gray text-left">{$carrier_marketplace['tracking_url']}</p>
            </div>
        </div>
    {/foreach}
{/if}
