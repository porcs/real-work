
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 statistics">
                <table id="my_task_list" ext="{$ext}" market="{$market}">
                    <thead class="table-head">
                        <tr>
                            <th><label class="cb-checkbox"><input class="sel_all" type="checkbox" ><span class="th_txt">{ci_language line="Allow automatic update"}</span></label></th>
                            <th>{ci_language line="Tasks"}</th>
                                {*<th>{ci_language line="Checked time"}</th>
                                <th class='center-align'>{ci_language line="Status"}</th>*}
                            <th class='center-align'>{ci_language line="Next time"}</th>
                        </tr>
                    </thead>
                    {if isset($tasks)}
                        <tbody>
                            {foreach from=$tasks key=key item=value}
                                <tr class="task_row" rel="{$value['task_name']}">
                                    <td><label class="cb-checkbox"><input class="sel_row" type="checkbox" rel="{$value['task_key']}" crel='{$value['task_id']}'  srel='{$value['sub_market']}' {if $value['task_running'] == true}checked{/if}>{$value['task_display_name']}</label></td>
                                    <td>{ci_language line=$value['task_name']}</td>
                                    {*<td class="t_time">{$value['task_lastupdate']}</td>*}
                                    {*<td class='center-align'><div class="t_status {$value['task_status']}"></div></td>*}
                                    <td class='center-align'><div class="countdown_next" rel="{$countdown[$value['task_key']]}"></div></td>
                                </tr>
                            {/foreach}
                        </tbody>
                    {/if}
                </table>

            </div>
        </div>
    </div>			
</div>


<input type="hidden" id="processing" value="Processing...">
<link rel="stylesheet" href="{$base_url}assets/css/feedbiz/mirakl/schedule_task.css" />
<script src="{$base_url}assets/js/countdown.js"></script> 
<script language="javascript" src="{$base_url}assets/js/FeedBiz/mirakl/schedule_task.js"></script>
