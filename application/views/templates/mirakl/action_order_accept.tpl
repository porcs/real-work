
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#accept_order" data-toggle="tab" class="">
            <i class="icon-creation-down"></i><span>{ci_language line="Accept Orders"}</span>
        </a>
    </li> 
    <li class="">
        <a href="#accept_list" data-toggle="tab" class="">
            <i class="icon-creation-down"></i><span>{ci_language line="Accept Lists"}</span>
        </a>
    </li> 
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="accept_order">
        <!-- begin -->
        <div class="row">
            <div class="col-md-12">
                <div class="headSettings clearfix">
                    <div class="pull-sm-left clearfix">
                        <h4 class="headSettings_head">{ci_language line="Accept Orders"}</h4>                      
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_accept_order" class="table">
                        <thead class="table-head">
                            <tr>
                                <th></th>
                                <th>{ci_language line="ROW"}</th>
                                <th>{ci_language line="ORDER REF"}.</th>
                                <th>{ci_language line="ORDER DATE"}</th>
                                <th>{ci_language line="NAME"}</th>
                                <th>{ci_language line="SHIPPING"}</th>
                                <th>{ci_language line="TOTAL"}</th>
                                <th>{ci_language line="STATUS"}</th>
                                <th>{ci_language line="EXPORT"}</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row m-t15">
            <div class="col-md-12">
                <button type="button" id="btn_lookup" class="btn btn-save pull-right"><i class="fa fa-search"></i> {ci_language line="Lookup"}</button>
            </div>
        </div>
        <!-- end -->
    </div>
    <div class="tab-pane" id="accept_list">
        <!-- begin -->
        <div class="row">
            <div class="col-md-12">
                <div class="headSettings clearfix">
                    <div class="pull-sm-left clearfix">
                        <h4 class="headSettings_head">{ci_language line="Accept Orders List"}</h4>                      
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_accept_list" class="table">
                        <thead class="table-head">
                            <tr>
                                <th>{ci_language line="ID"}</th>
                                <th>{ci_language line="ORDER REF"}.</th>
                                <th>{ci_language line="ORDER DATE"}</th>
                                <th>{ci_language line="NAME"}</th>
                                <th>{ci_language line="SHIPPING"}</th>
                                <th>{ci_language line="TOTAL"}</th>
                                <th>{ci_language line="STATUS"}</th>
                                <th>{ci_language line="TRACKING NUMBER"}</th>
                                <th>{ci_language line="INVOICE"}</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end -->
    </div>
</div>

<!-- Child rows -->
<div style="display:none"> 
    <table id="table_accept_order_detail">
        <thead class="table-head">
            <tr>
                <th></th>
                <th>{ci_language line="Row"}</th>
                <th>{ci_language line="Produt ID"}</th>
                <th>{ci_language line="Name"}</th>
                <th>{ci_language line="Status"}</th>
                <th>{ci_language line="SKU"}</th>
                <th>{ci_language line="Ship."}</th>
                <th>{ci_language line="Qty."}</th>
                <th>{ci_language line="Price"}</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<input type="hidden" id="msg_accept" value="{ci_language line="accept"}" />
<input type="hidden" id="msg_accept_auto" value="{ci_language line="accept auto"}" />
<input type="hidden" id="msg_sending" value="{ci_language line="sending"}" />
<input type="hidden" value="{ci_language line="Are you sure to accept orders ?"}" id="send-accept-confirm" />

<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/action_order_accept.js"></script> 
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>