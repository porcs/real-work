<div class="row">
  <div class="col-md-12">
    <div class="b-Top p-t20 clearfix">
      <p class="head_text p-b20">
        {ci_language line="Authentification"}
      </p>
    </div>
  </div>
</div>

<div class="row m-t5 m-b5">
  <div class="col-md-3">
    <p class="text-uc dark-gray dark-gray montserrat text-right m-t5">{ci_language line="Logo"}</p>
  </div>
  <div class="col-md-9">
    <img id="logo" src="{$logo}" >
    <input type="hidden" id="input_logo" name="logo" value="{$logo}" >
  </div>
</div>

<div class="row m-t10 m-b5">
  <div class="col-md-3">
    <p class="text-uc dark-gray dark-gray montserrat text-right m-t5">{ci_language line="API KEY"}</p>
  </div>
  <div class="col-md-4">
    <input type="text" name="api_key" id="api_key" value="{if isset($api_key)}{$api_key}{/if}" class="form-control" />
  </div>
  <div class="col-md-3">
    <button type="button" id="api_vertify" class="btn btn-warning m-0">
      <i class="fa fa-plug"></i>
      <span id="vertify"> {ci_language line="Vertify"}</span>
    </button>
  </div>
</div>

<div class="row m-t10 m-b5">
  <div class="col-md-3">
    <p class="text-uc dark-gray dark-gray montserrat text-right m-t5">{ci_language line="Auto Accept"}</p>
  </div>
  <div class="col-md-9">
    <label class="cb-checkbox">
      <input type="checkbox" name="auto_accept" {if (isset($auto_accept) && $auto_accept == 1)}checked="checked"{/if} />
    </label>
  </div>
</div>

<div class="row m-t10 m-b5">
  <div class="col-md-3">
    <p class="text-uc dark-gray dark-gray montserrat text-right m-t5">{ci_language line="Active"}</p>
  </div>
  <div class="col-md-9">
    <div class="cb-switcher">
      <label class="inner-switcher">
        <input type="checkbox" name="active" data-state-on="ON" data-state-off="OFF" value="1" {if (isset($active) && $active == 1)}checked="checked"{/if}>
      </label>
      <span class="cb-state">{ci_language line="ON"}</span>
    </div>
  </div>

</div>


<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/config_parameter.js"></script>



{*


<div class="row m-t15">

  <div class="col-md-12 form-group">
    <label class="text-uc dark-gray montserrat m-t3 col-md-2 text-right">{ci_language line="Logo"}</label>
    <div class="col-md-6">
      <img id="logo" src="{$logo}" >
      <input type="hidden" id="input_logo" name="logo" value="{$logo}" >
    </div>
  </div>

  <div class="col-md-12 form-group">
    <label class="text-uc dark-gray montserrat m-t10 col-md-2 text-right">{ci_language line="API KEY"}</label>            
    <div class="col-md-5 m-b10">
      <input type="text" name="api_key" id="api_key" value="{if isset($api_key)}{$api_key}{/if}" class="form-control" />
    </div>
    <div class="col-md-3">
      <button type="button" id="api_vertify" class="btn btn-check m-0">
        <i class="icon-check"></i>
        <span id="vertify">{ci_language line="Vertify"}</span>
      </button>
    </div>
  </div>
  <div class="col-md-12 form-group">
    <label class="text-uc dark-gray montserrat m-t3 col-md-2 text-right">{ci_language line="Auto Accept"}</label>
    <div class="col-md-6">
      <label class="cb-checkbox">
        <input type="checkbox" name="auto_accept" {if (isset($auto_accept) && $auto_accept == 1)}checked="checked"{/if} />
      </label>
    </div>
  </div>


  <div class="col-md-12 form-group"> 
    <label class="text-uc dark-gray montserrat m-t3 col-md-2 text-right">{ci_language line="Active"} :</label>
    <div class="col-md-6">
      <div class="cb-switcher">
        <label class="inner-switcher">
          <input type="checkbox" name="active" data-state-on="ON" data-state-off="OFF" value="1" {if (isset($active) && $active == 1)}checked="checked"{/if}>
        </label>
        <span class="cb-state">{ci_language line="ON"}</span>
      </div>
    </div>
  </div>
</div>

*}