
<div class="clearfix">
  <div class="miraklLogs">
    <div class="logs m-t20">
      <h4 class="logs_head">{ci_language line="Latest Updates"}</h4>
    </div>

    <div class="logs">
      <h5 class="logs_text">{ci_language line="Normal Update"}</h5>
      <div class="row">
        <div class="col-xs-1">
          <i class="icon-log_date"></i>
        </div>
        <div class="col-xs-5 logsContent">
          <p class="logs_text-content"><span>{ci_language line="Date"}</span></p>
          <p class="logs_text-content">{$last_update['NU']['date_add_date']}</p>							
        </div>
        <div class="col-xs-5">
          <p class="logs_text-content"><span>{ci_language line="Time"}</span></p>
          <p class="logs_text-content">{$last_update['NU']['date_add_time']}</p>
        </div>
      </div>
    </div>

    <div class="logs drown">
      <h5 class="logs_text">{ci_language line="Cron update"}</h5>
      <div class="row">
        <div class="col-xs-1">
          <i class="icon-log_clock"></i>
        </div>
        <div class="col-xs-5 logsContent">
          <p class="logs_text-content"><span>{ci_language line="Date"}</span></p>
          <p class="logs_text-content">{$last_update['CU']['date_add_date']}</p>							
        </div>
        <div class="col-xs-5">
          <p class="logs_text-content"><span>{ci_language line="Time"}</span></p>
          <p class="logs_text-content">{$last_update['CU']['date_add_time']}</p>
        </div>
      </div>
    </div>
  </div>	

  <div class="miraklTableLogs">
    <p class="head_text p-t25 b-None m-0 col-xs-3">
      <span>{ci_language line="History"}</span>
    </p>
    <form id="logs-filter" method="post" enctype="multipart/form-data" autocomplete="off" >
      <p class="head_text p-t25 b-None m-0  col-xs-3">
        <span>{ci_language line="From"}</span>
        <input type="text" class="inputDate form-control logs-filter" name="date_from" id="date_from">  
      </p>
      <p class="head_text p-t25 b-None m-0  col-xs-3">
        <span>{ci_language line="To"}</span>
        <input type="text" class="inputDate form-control logs-filter" name="date_to" id="date_to"> 
      </p>
      <p class="head_text p-t25 b-None m-0 col-xs-3 p-r0">
        <span>{ci_language line="Action"}</span>
        <select  class="search-select logs-filter" data-placeholder="Choose" name="feed_type" id="feed_type">
          <option value="">--</option>
          <option value="{MiraklParameter::FEED_TYPE_PRODUCT}">{ci_language line="{MiraklParameter::ACTION_TYPE_PRODUCT}"}</option>
          <option value="{MiraklParameter::FEED_TYPE_OFFER}">{ci_language line="{MiraklParameter::ACTION_TYPE_OFFER}"}</option>
          <option value="{MiraklParameter::FEED_TYPE_DELETE}">{ci_language line="{MiraklParameter::ACTION_TYPE_DELETE}"}</option>
          <option value="{MiraklParameter::FEED_TYPE_ACCEPT}">{ci_language line="{MiraklParameter::ACTION_TYPE_ACCEPT}"}</option>
          <option value="{MiraklParameter::FEED_TYPE_IMPORT}">{ci_language line="{MiraklParameter::ACTION_TYPE_IMPORT}"}</option>
          <option value="{MiraklParameter::FEED_TYPE_UPDATE}">{ci_language line="{MiraklParameter::ACTION_TYPE_UPDATE}"}</option>
        </select>  
      </p>
    </form>


    <table id="main-logs" style="display: none;">
      <tr>
        <td data-th="Date / Time:" data-name="" width="15%">
          <p class="logRow date"></p>
          <p class="logRow time"></p>
        </td>
        <td data-th="Action:" width="15%">
          <p class="logRow feed_type"><span></span></p>
          <p class="logRow batch_id_log"><button type="button" value="{if isset($data) && isset($data.batch_id)}{$data.batch_id}{/if}" class="link batch_id"></button></p>
        </td>							
        <td class="tableStatus status_log" data-th="Status:" width="35%"></td>
        <td class="tableJoined result_log" data-th="Result:" width="35%">
          <p class="logRow"></p>
        </td>
      </tr>
    </table>

    <table class="table tableLog m-b0">
      <thead class="table-head">
        <tr>
          <th width="15%">{ci_language line="Date / Time"}</th>
          <th width="15%">{ci_language line="Action"}</th>
          <th width="25%">{ci_language line="Status"}</th>
          <th width="45%">{ci_language line="Result"}</th>
        </tr>
      </thead>				
    </table>

    <div id="history_log" style="height: 280px; overflow: hiddden">
      <table id="history_log_inner" class="table tableLog">				
        <tbody>					
          {if isset($histories) && !empty($histories)}
              {foreach $histories as $history}
                  <tr>
                    <td data-th="Date / Time:" data-name="" width="15%">
                      <p class="logRow date">{$history['date_add_date']}</p>
                      <p class="logRow time">{$history['date_add_time']}</p>
                    </td>
                    <td class="tableUserName" data-th="Action:" width="15%">
                      <p class="logRow feed_type"><span>{$history['feed_type']}{$history['cron']}</span></p>
                      <p class="logRow batch_id_log"><button type="button" value="{$history['batch_id']}" class="link batch_id">{$history['batch_id']}</button></p>
                    </td>		
                    <td class="tableStatus status_log" data-th="Status:" width="25%">
                      {$history['log_status']}
                    </td>

                    {*                    <td class="tableStatus status_log" data-th="Status:" width="35%">
                    {if $data.log_status == 1}
                    <p class="logRow logSuccess">
                    <i class="fa fa-check green"></i> {ci_language line="Success"}
                    </p>
                    {else}
                    <p class="logRow logError">
                    <i class="fa fa-warning red"></i> {$data.message}
                    </p>
                    {/if}
                    </td>*}
                    <td class="tableJoined result_log" data-th="Result:" width="45%">
                      <p class="logRow">
                        {$history['result']}
                      </p>

                      {*<p class="logRow">
                      {if $data.feed_type == "{MiraklParameter::FEED_TYPE_ACCEPT}" || $data.feed_type == "{MiraklParameter::FEED_TYPE_IMPORT}" || $data.feed_type == "{MiraklParameter::FEED_TYPE_UPDATE}" }
                      {ci_language line="Result"} : {$data.count_success} {ci_language line="success"}, {$data.count_skipped} {ci_language line="skipped orders"}  
                      {else}
                      {ci_language line="Result"} : {$data.count_success} {ci_language line="items to send"}, {$data.count_skipped} {ci_language line="skipped items"} 
                      {/if}
                      </p>*}
                    </td>
                  </tr>
              {/foreach}
          {/if}  											
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">

    <div class="headSettings clearfix">
      <div class="pull-sm-left clearfix">
        <h4 class="headSettings_head">{ci_language line="Skipped Details"}</h4>                      
      </div>
      <div class="pull-sm-right clearfix">
        <div class="showColumns clearfix"><label class="cb-checkbox check_filter">
            <input type="checkbox" id="chk_ignore_inactive" checked="checked"/>
            {ci_language line="Ignore inactive products"}
          </label></div>
      </div>          
    </div><!--headSettings-->

    <div class="row">
      <div class="col-xs-12">	            
        <table id="product_logs" class="mirakl-responsive-table">
          <thead class="table-head">
            <tr>
              <th class="center">{ci_language line="Batch ID"}</th>
              <th class="center">{ci_language line="Action"}</th>
              <th class="center">{ci_language line="Reference Type"}</th>
              <th class="center">{ci_language line="Reference"}</th>
              <th class="center">{ci_language line="Messages"}</th>
              <th class="center">{ci_language line="Date"}</th>
            </tr>
          </thead>
          <tbody>

          </tbody>                        
        </table><!--table-->

      </div><!--col-xs-12-->
    </div><!--row-->

  </div><!--col-xs-12-->
</div><!--row--> 

{include file="IncludeDataTableLanguage.tpl"}  

<input type="hidden" id="success_log" value="{ci_language line='Success'}" />
<input type="hidden" id="success_order" value="{ci_language line='success orders'}" />
<input type="hidden" id="form_log" value="{ci_language line='form'} : " />
<input type="hidden" id="result_log" value="{ci_language line="Result"} : " />
<input type="hidden" id="skipped_order" value="{ci_language line="skipped orders"}" />
<input type="hidden" id="items_to_send" value="{ci_language line="items to send"}" />
<input type="hidden" id="skipped_items" value="{ci_language line="skipped items"}" />
<input type="hidden" id="cron" value="{ci_language line="cron"}" />
<input type="hidden" id="orders_to_send" value="{ci_language line="orders to send"}" />
<input type="hidden" id="success" value="{ci_language line="success"}" />
<input type="hidden" id="skipped" value="{ci_language line="skipped"}" />
<input type="hidden" id="batch_id" value="{if isset($batch_id)}{$batch_id}{/if}" />
<input type="hidden" id="FEED_TYPE_PRODUCT" value="{MiraklParameter::FEED_TYPE_PRODUCT}" />
<input type="hidden" id="FEED_TYPE_DELETE" value="{MiraklParameter::FEED_TYPE_DELETE}" />
<input type="hidden" id="FEED_TYPE_OFFER" value="{MiraklParameter::FEED_TYPE_OFFER}" />
<input type="hidden" id="FEED_TYPE_ACCEPT" value="{MiraklParameter::FEED_TYPE_ACCEPT}" />
<input type="hidden" id="FEED_TYPE_IMPORT" value="{MiraklParameter::FEED_TYPE_IMPORT}" />
<input type="hidden" id="FEED_TYPE_UPDATE" value="{MiraklParameter::FEED_TYPE_UPDATE}" />
{include file="IncludeDataTableLanguage.tpl"}
<!--page specific plugin scripts-->
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/page_log.js"></script>
<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>