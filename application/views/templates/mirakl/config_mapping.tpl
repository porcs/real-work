{*<link rel="stylesheet" href="{$cdn_url}assets/css/bootstrap/bootstrap-colorpicker.css">*}
<div class="row">
  <div class="col-md-12">
    <div class="b-Top p-t20 m-t10 clearfix">
      <p class="head_text p-b20">
        {ci_language line="Attribute Mapping"}
      </p>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
  </div>
  <div class="col-md-3">
    <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
  </div>
  <div class="col-md-3">
    <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
  </div>
</div>
{if isset($config_attribute) && !empty($config_attribute)}

    {foreach $config_attribute as $index => $attribute}
        <div class="row m-t5 m-b5">
          <div class="col-md-3">
            <p class="text-right m-t5">{$attribute['label']} {if $attribute['required'] == 'Y'}<span style="color:red">*</span>{else}&nbsp;{/if}</p>
            <input type="hidden" name="field[{$index}][id_config_attribute]" value ="{$attribute['id_config_attribute']}" >
          </div>

          <div class="col-md-3">
            <select name="field[{$index}][shop_attribute]" class="search-select">
              <option value="">-----</option>

              {if isset($shop_attribute['S']) && !empty($shop_attribute['S'])}
                  <optgroup label="{ci_language line="Field Shop"}">
                    {foreach $shop_attribute['S'] as $value}
                        <option value="{$value['code']}" {if isset($attribute['shop_attribute']) && $value['code'] == $attribute['shop_attribute']}selected{/if} >{$value['label']}</option>
                    {/foreach}
                  </optgroup>
              {/if}

              {if isset($shop_attribute['A']) && !empty($shop_attribute['A'])}
                  <optgroup label="{ci_language line="Field Attribute"}">
                    {foreach $shop_attribute['A'] as $value}
                        <option value="{$value['code']}" {if isset($attribute['shop_attribute']) && $value['code'] == $attribute['shop_attribute']}selected{/if} >{$value['label']}</option>
                    {/foreach}
                  </optgroup>
              {/if}

              {if isset($shop_attribute['F']) && !empty($shop_attribute['F'])}
                  <optgroup label="{ci_language line="Field Feature"}">
                    {foreach $shop_attribute['F'] as $value}
                        <option value="{$value['code']}" {if isset($attribute['shop_attribute']) && $value['code'] == $attribute['shop_attribute']}selected{/if}>{$value['label']}</option>
                    {/foreach}
                  </optgroup>
              {/if}

            </select>
          </div>

          <div class="col-md-3">
            <p class="poor-gray text-left m-t5">{$attribute['description']}{if !empty($attribute['example'])} (E.g. {$attribute['example']}){/if}</p>
          </div>

        </div>
    {/foreach}
{/if}


<!-- attribute with list -->
{if isset($mirakl_attribute_with_list) && !empty($mirakl_attribute_with_list)}

    <div class="row">
      <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
          <p class="head_text p-b20">
            {ci_language line="Attribute with list"}
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <p class="montserrat text-right text-info text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
      </div>
    </div>

    {foreach $mirakl_attribute_with_list as $id_config_attribute => $attribute_with_list}
        <div class="attribute_with_list_container" id_config_attribute="{$id_config_attribute}" id_config_value="{$attribute_with_list['id_config_value']}" >

          <div class="row m-t3 m-b3">

            <div class="col-md-3">
              <p class="text-right m-t5">{$attribute_with_list['label_attribute']} {if $attribute_with_list['required'] == 'Y'}<span style="color:red">*</span>{else}&nbsp;{/if}</p>
              <input type="hidden" name="attribute_with_list[{$id_config_attribute}][id_config_attribute]" value ="{$id_config_attribute}" >
            </div>

            <div class="col-md-3 ">
              <select name="attribute_with_list[{$id_config_attribute}][shop_attribute]" class="search-select attribute_with_list">
                <option value="">--------</option>

                {if isset($attribute_with_list['value_list']) && !empty($attribute_with_list['value_list'])}
                    {$indexs = 0}
                    {foreach $attribute_with_list['value_list'] as $id_config_value_list => $value_list}

                        <option value="{$id_config_value_list}" {if isset($attribute_with_list['shop_attribute']) && ($attribute_with_list['shop_attribute'] == $id_config_value_list)}selected{/if} >{$value_list['label_value_list']}</option>

                        {*                        {if $indexs > 7}
                        {break}
                        {/if}

                        <div class="row m-t3 m-b3 with_value_list" id_config_value_list="{$id_config_value_list}">
                        <!-- value list label left -->
                        <div class="col-md-3">
                        <p class="text-right m-t5 poor-gray">{$value_list['label_value_list']}</p>
                        </div>
                        <!-- select attribute right -->
                        <div class="col-md-3">
                        <select name="attribute_with_list[{$id_config_attribute}][field_value][{$id_config_value_list}]" class="search-select attribute_with_list_value">
                        <option value="">---</option>
                        {if isset($attribute_with_list['shop_attribute_value']) && !empty($attribute_with_list['shop_attribute_value'])}
                        {foreach $attribute_with_list['shop_attribute_value'] as $shop_attribute_value}
                        <option value="{$shop_attribute_value['field_shop_code']}" {if isset($value_list['shop_attribute_value']) && ($value_list['shop_attribute_value'] == $shop_attribute_value['field_shop_code'])}selected{/if}>{$shop_attribute_value['field_shop_label']}</option>
                        {/foreach}
                        {/if}
                        </select>
                        </div>
                        </div>

                        {$indexs = $indexs + 1}*}
                    {/foreach}
                {/if}



                {*
                {if isset($shop_attribute['E']) && !empty($shop_attribute['E'])}
                {foreach $shop_attribute['E'] as $value}
                <option value="{$value['code']}" {if isset( $attribute_with_list['shop_attribute']) && $value['code'] == $attribute_with_list['shop_attribute']}selected{/if} >{$value['label']}</option>
                {/foreach}
                {/if}

                {if isset($shop_attribute['S']) && !empty($shop_attribute['S'])}
                <optgroup label="{ci_language line="Field Shop"}">
                {foreach $shop_attribute['S'] as $value}
                {if ($value['type'] == 'VL')}
                <option value="{$value['code']}" {if isset( $attribute_with_list['shop_attribute']) && $value['code'] == $attribute_with_list['shop_attribute']}selected{/if} >{$value['label']}</option>
                {/if}
                {/foreach}
                </optgroup>
                {/if}

                {if isset($shop_attribute['A']) && !empty($shop_attribute['A'])}
                <optgroup label="{ci_language line="Field Attribute"}">
                {foreach $shop_attribute['A'] as $value}
                <option value="{$value['code']}" {if isset( $attribute_with_list['shop_attribute']) && $value['code'] == $attribute_with_list['shop_attribute']}selected{/if} >{$value['label']}</option>
                {/foreach}
                </optgroup>
                {/if}

                {if isset($shop_attribute['F']) && !empty($shop_attribute['F'])}
                <optgroup label="{ci_language line="Field Feature"}">
                {foreach $shop_attribute['F'] as $value}
                <option value="{$value['code']}" {if isset( $attribute_with_list['shop_attribute']) && $value['code'] == $attribute_with_list['shop_attribute']}selected{/if}>{$value['label']}</option>
                {/foreach}
                </optgroup>
                {/if}
                *}
              </select>
            </div>

            <div class="col-md-3">
              <p class="poor-gray text-left m-t5">{$attribute_with_list['description']}{if !empty($attribute_with_list['example'])} (E.g. {$attribute_with_list['example']}){/if}</p>
            </div>

          </div>


        </div>
    {/foreach}
{/if}






{if isset($mirakl_mapping_model_attribute['A']) && !empty($mirakl_mapping_model_attribute['A'])}
    <div class="row">
      <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
          <p class="head_text p-b20">
            {ci_language line="Mapping Attribute"}
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <p class="montserrat text-info text-right text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
      </div>
    </div>

    {foreach $mirakl_mapping_model_attribute['A'] as $attribute}
        <div class="row m-t3 m-b3">
          
          {$id_config_attribute = $attribute['id_config_attribute']}
          
          <div class="col-md-3">
            <p class="text-right m-t5">{$attribute['field_shop_name']}</p>

          </div>
          <div class="col-md-3">
            <p class="text-left m-t5">{$attribute['label_attribute']}</p>
            <input type="hidden" name="model_attribute_list[{$id_config_attribute}][id_config_attribute]" value="{$id_config_attribute}">
            <input type="hidden" name="model_attribute_list[{$id_config_attribute}][shop_type]" value="A">
          </div>

        </div>

        {foreach $attribute['field_shop_list'] as $index => $field_shop_list}
            <div class="row m-t3 m-b3">

              <div class="col-md-3">
                <p class="text-right m-t5 poor-gray">{$field_shop_list['field_shop_label']}</p>
              </div>

              <div class="col-md-3">
                <select name="model_attribute_list[{$id_config_attribute}][shop_attribute_value][A-{$field_shop_list['field_shop_code']}]" class="search-select">
                  <option value="">---</option>
                  {foreach $attribute['value_list'] as $id_config_value_list => $value_list}
                      <option value="{$id_config_value_list}" {if  $field_shop_list['id_config_value_list'] == $id_config_value_list}selected{/if}  >{$value_list['label_value_list']}</option>
                  {/foreach}
                </select>
              </div>

            </div>
        {/foreach}

    {/foreach}
{/if}

{if isset($mirakl_mapping_model_attribute['F']) && !empty($mirakl_mapping_model_attribute['F'])}
    <div class="row">
      <div class="col-md-12">
        <div class="b-Top p-t20 m-t10 clearfix">
          <p class="head_text p-b20">
            {ci_language line="Mapping Feature"}
          </p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-3">
        <p class="montserrat text-info text-right text-uc m-b20">{ci_language line="ATTRIBUTE SHOP"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="ATTRIBUTE MARKETPLACE"}</p>
      </div>
      <div class="col-md-3">
        <p class="montserrat text-info text-left text-uc m-b20">{ci_language line="DESCRIPTION"}</p>
      </div>
    </div>

    {foreach $mirakl_mapping_model_attribute['F'] as $attribute}
        <div class="row m-t3 m-b3">
          
          {$id_config_attribute = $attribute['id_config_attribute']}
          
          <div class="col-md-3">
            <p class="text-right m-t5">{$attribute['field_shop_name']}</p>

          </div>
          <div class="col-md-3">
            <p class="text-left m-t5">{$attribute['label_attribute']}</p>
            <input type="hidden" name="model_attribute_list[{$id_config_attribute}][id_config_attribute]" value="{$id_config_attribute}">
            <input type="hidden" name="model_attribute_list[{$id_config_attribute}][shop_type]" value="F">
          </div>

        </div>

        {foreach $attribute['field_shop_list'] as $field_shop_list}
            <div class="row m-t3 m-b3">
              <div class="col-md-3">
                <p class="text-right m-t5 poor-gray">{$field_shop_list['field_shop_label']}</p>
              </div>

              <div class="col-md-3">
                <select name="model_attribute_list[{$id_config_attribute}][shop_attribute_value][F-{$field_shop_list['field_shop_code']}]" class="search-select">
                  <option value="">---</option>
                  {foreach $attribute['value_list'] as $id_config_value_list => $value_list}
                      <option value="{$id_config_value_list}" {if  $field_shop_list['id_config_value_list'] == $id_config_value_list}selected{/if}>{$value_list['label_value_list']}</option>
                  {/foreach}
                </select>
              </div>

            </div>
        {/foreach}

    {/foreach}
{/if}

<script type="text/javascript" src="{$cdn_url}assets/js/FeedBiz/mirakl/includescript.js"></script>
{*<script type="text/javascript" src="{$cdn_url}assets/js/bootstrap-colorpicker.min.js"></script>*}
{*<script type="text/javascript" src="{$cdn_url}assets/js/jquery.colorbox-m.js"></script>*}
<script src="{$cdn_url}assets/js/FeedBiz/mirakl/config_mapping.js"></script>