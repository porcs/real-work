
<div class="row">
    <div class="col-xs-12">
        <div class="b-Top p-t20">
            <div class="inline pull-right">  

                {if isset($save_page) && !empty($save_page)}
                    <button type="button" id="save_page" value="{$save_page}" class="pull-left link p-size m-tr10">
                        {ci_language line="Save"}
                    </button>
                {/if}
                
                {if isset($save_page_static) && !empty($save_page_static)}
                    <button type="button" id="save_page_static" value="{$save_page_static}" class="btn btn-save">
                        {ci_language line="Save"}
                    </button>
                {/if}

                {if isset($save_continue) && !empty($save_continue)}
                    <button type="button" id="save_continue" value="{$save_continue}" class="btn btn-save btnNext">
                        {ci_language line="Save and continue"}
                    </button>
                {/if}

                <input type="hidden" id="check_save" name="save">
            </div>
        </div>		
    </div>
</div>

<script type="text/javascript">
    $("#save_page,#save_page_static,#save_continue").click(function (e) {
        e.preventDefault();
        $("#check_save").attr('value', this.value);
        $("#form_submit").submit();
    });
</script>
