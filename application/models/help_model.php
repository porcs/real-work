<?php

class help_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
        
    public function get_message($message_code=null, $limit=null, $search=null)
    {
        if (isset($limit) && $limit > 0) {
            $q = $this->db->limit($limit);
        }
        if (isset($search)) {
            $q = $this->db->or_like($search);
        }
        if ($message_code === null) {
            $q = $this->db->order_by('message_code', 'ASC')->get('message');
        } elseif (is_array($message_code)) {
            $q = $this->db->order_by('message_code', 'ASC')->get_where('message', $message_code);
        } else {
            $q = $this->db->order_by('message_code', 'ASC')->get_where('message', array('message_code' => $message_code));
        }
        
        return $q->result_array();
    }
}
