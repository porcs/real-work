<?php

require_once dirname(__FILE__)."/../libraries/Ebay/table.config.php";

class ebayv1_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }



    ////////////////////////////////////////////////////////////////////////////
    public function __call($function, $parameter)
    {   
        if ($function == 'checkVariable') {
            
            $check_default = true;
            //function
            $check_varibable = function($parameter) use (&$check_default) 
            {
                $function   = $parameter[0];
                $data       = $parameter[1];
                
                //function
                $check = function($type, $key) use (&$check_default, $data)
                {
                    switch (Configuration::$model_definition_type[$type]) 
                    {
                        case 'int' :
                            if ( !isset($data[$key]) 
                              || !is_numeric($data[$key])) {
                                $check_default = false;
                            }
                            break;
                    }
                };
                
                array_walk(Configuration::$model_definition[$function], $check);
            };

            $check_varibable($parameter);
            
            return $check_default;
        }
        
        return false;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    function getConfiguration($user_id, $optional = null) 
    {
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            if (!empty($optional)) { 
                $this->db->where('name', $optional);
            } 
            $this->db->where('id_customer', $user_id); 
            $query = $this->db->get('configuration');       

            if ($query->num_rows > 0) {
                $result = $query->result_array();
                $query->free_result();
                if (!empty($optional)) {
                    return $result[0]['value'];
                }
                return $result;
            }
        }
        return array();
    }

    ////////////////////////////////////////////////////////////////////////////
    function geteBayConfiguration($user_id, $id_site) 
    {
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            $this->db->where('id_customer', $user_id);
            $this->db->where('id_site', $id_site);
            $query = $this->db->get('ebay_configuration');

            if ( $query->num_rows > 0 ) {
                $result = $query->result_array();
                $query->free_result();
                return $result;
            }
        }
        
        return array();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public function getSiteFromPackages($id_marketplace, $id_packages) 
    {
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            $this->db->select('id_site_ebay');
            $this->db->where('id_offer_pkg', $id_marketplace);
            $this->db->where('id_offer_sub_pkg', $id_packages);
            $this->db->where_in('offer_pkg_type', array('0', '1'));
            $query = $this->db->get('offer_price_packages');
            
            if ($query->num_rows > 0) {
                $result = $query->result_array();
                $query->free_result();
                return current($result[0]);
            }
        }
        
        return null;
    }
    
         
    public function getStringSite($id_marketplace = null, $id_packages = null) {
        if ((!isset($id_packages) && empty($id_packages)) 
            || (!isset($id_marketplace) && empty($id_marketplace)) 
        ) {
            return false;
        }
        $this->db->select('s.iso_code AS iso_code, s.NAME AS name');
        $this->db->join('ebay_sites s', 'p.id_site_ebay = s.id_site');
        $this->db->where('p.id_offer_sub_pkg', $id_packages);
        $this->db->where('p.id_offer_pkg', $id_marketplace);
        $this->db->where_in('p.offer_pkg_type', array('0', '1'));
        $query = $this->db->get('offer_price_packages p');
        if ( $query->num_rows > 0 ) {
            $result = $query->result_array();
            $query->free_result();
            return current($result);
        }
        return false;
    }
    
     
    public function getPackagesFromSite($id_marketplace, $id_site)
    {
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            $this->db->select('id_offer_sub_pkg as id_packages');
            $this->db->where('id_site_ebay', $id_site);
            $this->db->where('id_offer_pkg', $id_marketplace);
            $this->db->where_in('offer_pkg_type', array('0', '1'));
            $query = $this->db->get('offer_price_packages');

            if ( $query->num_rows > 0) {
                $result = $query->result_array();
                $query->free_result();
                return current($result[0]);
            }
        }
        
        return null;
    }
    

    public function getDataPackagesFromSite($id_marketplace, $user_id, $id_site)
    {
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            $this->db->select('
                opp.id_region, opp.id_offer_sub_pkg as packages, 
                opp.currency_offer_price_pkg as currency,
                opp.ext_offer_sub_pkg as ext,
                opp.domain_offer_sub_pkg as domain,
                opp.id_site_ebay,
                r.name_region as region,
                osp.title_offer_sub_pkg as country'
            );
            $this->db->join(
                'user_package_details upd', 
                'opp.id_offer_price_pkg = upd.id_package'
            );
            $this->db->join(
                'region r', 
                'opp.id_region = r.id_region'
            );
            $this->db->join(
                'offer_sub_packages osp', 
                'opp.id_offer_sub_pkg = osp.id_offer_sub_pkg'
            );
            $this->db->where('opp.id_site_ebay', $id_site);
            $this->db->where('opp.id_offer_pkg', $id_marketplace);
            $this->db->where('upd.id_users', $user_id);
            $this->db->where_in('opp.offer_pkg_type', array('0', '1'));
            $query = $this->db->get('offer_price_packages opp');

            if ( $query->num_rows > 0 ) {
                $result = $query->result_array();
                $query->free_result();
                return current($result);
            }
        }
        
        return null;
    }
    

    public function getCountryFromPackages($id_packages)
    {
        echo $id_packages;exit();
        if ($this->checkVariable(__FUNCTION__, func_get_args())) {
            $this->db->select('title_offer_sub_pkg as country');
            $this->db->where('id_offer_sub_pkg', $id_packages);
            $query = $this->db->get('offer_sub_packages');

            if ( $query->num_rows > 0 ) {
                $result = $query->result_array();
                $query->free_result();
                return current($result[0]);
            }
        }
        
        return null;
    }
}

