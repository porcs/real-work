<?php
require_once APPPATH.'libraries/mydate.php';

class PayPal_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('encrypt');
    }
    
    public function getLastBilling($userid, $sort = "DESC")
    {
        $this->db->where('users_id', $userid)
                ->where('bill_status', 'active')
                ->order_by('bill_created_date', $sort);
        $result = $this->db->get('billings', 1, 0);
        $query = $result->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function getPayment($userid)
    {
        $this->db->where('id_user', $userid)
                ->order_by('created_date_preapproval', 'DESC');
        $result = $this->db->get('payments_preapproval', 1, 0);
        $query = $result->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function setPreapprovalPayment($userid, $preapprovalKey, $logs='')
    {
        $paymentData = $this->getPayment($userid);
        if (is_array($paymentData)) {
            $dataUpdate = array(
                'key_preapproval' => $preapprovalKey,
                'logs_preapproval' => $logs,
            );
            $this->db->where('id_user', $userid);
            return $this->db->update('payments_preapproval', $dataUpdate);
        } else {
            $arrData = array(
                'id_user' => $userid,
                'status_preapproval' => 'active',
                'key_preapproval' => $preapprovalKey,
                'logs_preapproval' => $logs,
                'created_date_preapproval' => date('Y-m-d H:i:s'),
            );
            return $this->db->insert('payments_preapproval', $arrData);
        }
    }
    
    public function updatePreapprovalPayment($userid, $status='inactive')
    {
        $this->db->where('id_user', $userid);
        return $this->db->update('payments_preapproval', array('status_preapproval'=>$status));
    }

    public function getCustomer($userid)
    {
        $this->db->where('id', $userid);
        $result = $this->db->get('customer', 1, 0);
        $query = $result->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function setBilling($arrData)
    {
        return $this->db->insert('billings', $arrData);
    }
    
    public function setBillingDetail($arrData)
    {
        return $this->db->insert('billings_detail', $arrData);
    }
    
    public function getBillingDetailLists($id_billing)
    {
        $this->db->where('id_billing', $id_billing);
        $result = $this->db->get('billings_detail');
        return $result->result_array();
    }
    
    public function getBillingByUser($id_user)
    {
        $sql = "SELECT billings.id_billing,billings.id_users,billings.status_billing,billings_detail.id_package,billings_detail.start_date_bill_detail,billings_detail.end_date_bill_detail ";
        $sql .= "FROM billings LEFT OUTER JOIN billings_detail ON(billings_detail.id_billing=billings.id_billing) ";
        $sql .= "WHERE billings.id_users = '".(int)$id_user."' AND ('".date('Y-m-d')."' BETWEEN billings_detail.start_date_bill_detail AND billings_detail.end_date_bill_detail) ";
        $sql .= "ORDER BY billings_detail.start_date_bill_detail DESC";
        $result = $this->db->query($sql);
        $query = $result->result_array();
        return $query;
    }
    
    public function getBilling($billID)
    {
        $this->db->where('id_billing', $billID);
        $result = $this->db->get('billings', 1, 0);
        $query = $result->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function billingLists($user_id)
    {
        $this->db->select('id_billing, id_users, amt_billing, currency_billing, status_billing, created_date_billing, payment_method_billing');
        $this->db->where('id_users', $user_id);
        $this->db->where('status_billing', 'active');
        $this->db->order_by('created_date_billing', 'DESC');
        $result = $this->db->get('billings');
        return $result->result_array();
    }
    
    public function updateBilling($billID, $arrData)
    {
        $this->db->where('bill_id', $billID);
        return $this->db->update('billings', $arrData);
    }
    
    public function removeBilling($billID)
    {
        return $this->db->delete('billings', array('bill_id' => $billID));
    }
    
    public function getPayerEmail($userid)
    {
        $this->db->where('id_customer', $userid);
        $this->db->where('name', 'PAYER_EMAIL');
        $result = $this->db->get('configuration', 1, 0);
        $query = $result->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function setPayerEmail($userid, $email)
    {
        $query = $this->getPayerEmail($userid);
        if ($query) {
            $dataArr = array(
                'value' => $email,
                'config_date' => date('Y-m-d H:i:s'),
            );
            $this->db->where('id_customer', $userid);
            $this->db->where('name', 'PAYER_EMAIL');
            return $this->db->update('configuration', $dataArr);
        } else {
            $dataArr = array(
                'id_customer' => $userid,
                'name' => 'PAYER_EMAIL',
                'value' => $email,
                'config_date' => date('Y-m-d H:i:s'),
            );
            return $this->db->insert('configuration', $dataArr);
        }
    }
    
    public function checkBillID($billID)
    {
        $this->db->select('id_billing');
        $this->db->where('id_billing', $billID);
        $query = $this->db->get('billings', 1, 0)->result_array();
        if (sizeof($query) > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function setPlanning($arrData)
    {
        if (isset($arrData['id_planning']) && !empty($arrData['id_planning'])) {
            $tmpQuery = $this->getPlanning($arrData['id_planning']);
            if ($tmpQuery != null) {
                $this->db->where('id_planning', $arrData['id_planning']);
                unset($arrData['id_planning']);
                return $this->db->update('payments_planning', $arrData);
            } else {
                return $this->db->insert('payments_planning', $arrData);
            }
        } else {
            return null;
        }
    }
    
    public function checkPlanningID($id_planning)
    {
        $this->db->select('id_planning');
        $this->db->where('id_planning', $id_planning);
        $result = $this->db->get('payments_planning', 1, 0);
        $query = $result->result_array();
        if (sizeof($query) > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getUserEmail($id_user)
    {
        $sql = "SELECT value FROM configuration WHERE id_customer = '".(int)$id_user."' AND name = 'PAYER_EMAIL' LIMIT 0,1";
        $query = $this->db->query($sql)->result_array();
        if (!empty($query)) {
            return $query[0]['value'];
        } else {
            $sql = "SELECT user_email FROM users WHERE id = '".(int)$id_user."' AND user_email IS NOT NULL LIMIT 0,1";
            $query = $this->db->query($sql)->result_array();
            if (is_array($query)) {
                return $query[0]['user_email'];
            } else {
                return null;
            }
        }
    }
    
    public function getRegion()
    {
        $this->db->where('status_region', 'active');
        return $this->db->get('region')->result_array();
    }
    
    public function getRegionByID($id_region)
    {
        $this->db->where('id_region', $id_region);
        $this->db->where('status_region', 'active');
        return $this->db->get('region', 1, 0)->result_array();
    }
    
    //Add new 14/7/2014
    public function getMarketplaceOffer()
    {
        $sql = "SELECT * FROM offer_packages ";
        $sql .=" WHERE is_marketplace = 1 AND status_offer_pkg = 'active' ";
        $sql .= "ORDER BY sort_offer_pkg ASC";
        
        return $this->db->query($sql)->result_array();
    }
    
    //Add new 14/7/2014
    public function getSubMarketplaceOffer($id_offer_pkg)
    {
        $sql = "SELECT * FROM offer_sub_packages sub ";
        $sql .= "WHERE id_offer_pkg = '".$id_offer_pkg."' AND sort_offer_sub_pkg != 0 AND status_offer_sub_pkg = 'active' ";
        $sql .= "ORDER BY sub.sort_offer_sub_pkg ASC ";
        
        return $this->db->query($sql)->result_array();
    }
    
    //Add new 14/7/2014
    public function getMarketplaceOfferPrice($id_offer_pkg, $id_offer_sub_pkg)
    {
        $sql = "SELECT * FROM offer_price_packages ";
        $sql .= "WHERE id_offer_pkg = " . (int)$id_offer_pkg . " AND id_offer_sub_pkg = " . (int)$id_offer_sub_pkg . " AND status_offer_price_pkg = 'active' ";
        $sql .= "LIMIT 0,1";
        
        return $this->db->query($sql)->result_array();
    }
    public function getBasePackageID($pk_id)
    {
        $pk_id = (int)$pk_id;
        $sql = "select id_offer_price_pkg as id from offer_price_packages"
             . " where id_region = '' and  id_offer_pkg in (SELECT id_offer_pkg   from offer_price_packages where id_offer_price_pkg = '$pk_id') limit 1";
        
        $out = $this->db->query($sql)->result_array();
        return $out[0]['id'];
    }
    public function getOfferPackage($id_region='', $inc_sub=false)
    {
        $add='';
        if (!$inc_sub) {
            $add.=" AND price.id_offer_sub_pkg IS NULL ";
        }
        if ($id_region!='') {
            $id_region = $this->db->escape_str($id_region);
            $add.=" AND price.id_region = '".$id_region."' ";
        }
        $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,offer.sort_offer_pkg,offer.submenu_offer_pkg,";
        $sql .= "price.*,r.*";//"price.id_region,price.amt_offer_price_pkg,price.currency_offer_price_pkg,price.priod_offer_price_pkg,price.offer_pkg_type,price.id_offer_price_pkg";
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg=price.id_offer_pkg) ";
        $sql .= "LEFT JOIN region as r on price.id_region = r.id_region and r.status_region = 'active'";
        
        $sql .= "WHERE offer.status_offer_pkg = 'active' and price.id_offer_pkg <> 1 $add ORDER BY offer.id_offer_pkg, price.id_region ASC";
        //echo $sql;
        return $this->db->query($sql)->result_array();
    }
    public function getMainOfferPackage($id_region='', $inc_sub=false, $offer_pk=0)
    {
        $add='';
        if (!$inc_sub) {
            //$add.=" AND price.id_offer_sub_pkg IS NULL and price.id_region='' ";
            $add.=" and price.offer_pkg_type = '2' ";
        } else {
            $offer_pk = (int)$offer_pk;
            $add.=" AND price.id_offer_sub_pkg IS NOT NULL and price.id_region <> '' and price.id_offer_pkg = '$offer_pk'    ";
        }
        if ($id_region!='') {
            $id_region = $this->db->escape_str($id_region);
            $add.=" AND price.id_region = '".$id_region."' ";
        }
        $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,offer.sort_offer_pkg,offer.submenu_offer_pkg,";
        $sql .= "price.*,r.* ";//"price.id_region,price.amt_offer_price_pkg,price.currency_offer_price_pkg,price.priod_offer_price_pkg,price.offer_pkg_type,price.id_offer_price_pkg";
        if ($inc_sub) {
            $sql.=",sub.* ";
        }
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg=price.id_offer_pkg) ";
        $sql .= "LEFT JOIN region as r on price.id_region = r.id_region and r.status_region = 'active'";
        if ($inc_sub) {
            $sql .= "LEFT JOIN offer_sub_packages as sub on price.id_offer_sub_pkg = sub.id_offer_sub_pkg ";
        }
        
        $sql .= " WHERE offer.status_offer_pkg = 'active' and price.id_offer_pkg <> 1 and price.offer_pkg_type <> '3' $add ORDER BY r.sort_by, price.id_region ASC";
      
        return $this->db->query($sql)->result_array();
    }
    public function getMainPackageZone()
    {
        $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,offer.sort_offer_pkg,offer.submenu_offer_pkg,";
        $sql .= "price.*,r.* ";
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg=price.id_offer_pkg) ";
        $sql .= "LEFT JOIN region as r on price.id_region = r.id_region and r.status_region = 'active'";
        $sql .= " WHERE offer.status_offer_pkg = 'active' and price.offer_pkg_type = '9'  ORDER BY r.sort_by, price.id_region ASC";
        return $this->db->query($sql)->result_array();
    }
    public function getMainOfferPackageZone($id_region='', $inc_sub=false, $offer_pk=0)
    {
        $add='';
        if (!$inc_sub) {
            //$add.=" AND price.id_offer_sub_pkg IS NULL and price.id_region='' ";
            $add.=" and price.offer_pkg_type = '2' ";
        } else {
            $offer_pk = (int)$offer_pk;
            $add.="  and price.id_region <> '' and price.id_offer_pkg = '$offer_pk' and price.offer_pkg_type = '4'    ";
        }
        if ($id_region!='') {
            $id_region = $this->db->escape_str($id_region);
            $add.=" AND price.id_region = '".$id_region."' ";
        }
        $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,offer.sort_offer_pkg,offer.submenu_offer_pkg,";
        $sql .= "price.*,r.* ";//"price.id_region,price.amt_offer_price_pkg,price.currency_offer_price_pkg,price.priod_offer_price_pkg,price.offer_pkg_type,price.id_offer_price_pkg";
        if ($inc_sub) {
            $sql.=",sub.* ";
        }
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg=price.id_offer_pkg) ";
        $sql .= "LEFT JOIN region as r on price.id_region = r.id_region and r.status_region = 'active'";
        if ($inc_sub) {
            $sql .= "LEFT JOIN offer_sub_packages as sub on price.id_offer_sub_pkg = sub.id_offer_sub_pkg ";
        }
        
        $sql .= " WHERE offer.status_offer_pkg = 'active' and price.id_offer_pkg <> 1 and price.offer_pkg_type <> '3' $add ORDER BY r.sort_by, price.id_region ASC";
         
        return $this->db->query($sql)->result_array();
    }
    
    
    public function getOfferPrice($id_offer, $id_region)
    {
        $id_offer = (int)$id_offer;
        $id_region = $this->db->escape_str($id_region);
        $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,price.amt_offer_price_pkg,price.currency_offer_price_pkg ";
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg = price.id_offer_pkg) ";
        $sql .= "WHERE offer.id_offer_pkg = '".$id_offer."' AND offer.status_offer_pkg = 'active' AND price.id_offer_sub_pkg IS NULL ";
        $sql .= "AND price.id_region = '".$id_region."' LIMIT 0,1";
        
        return $this->db->query($sql)->result_array();
    }
    public function getOfferPkPrice($id_offer)
    {
        $id_offer = (int)$id_offer;
        $sql = "SELECT price.id_offer_price_pkg,offer.name_offer_pkg,price.domain_offer_sub_pkg,price.amt_offer_price_pkg,price.currency_offer_price_pkg ";
        $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg = price.id_offer_pkg) ";
        $sql .= "WHERE price.id_offer_price_pkg  = '".$id_offer."' AND offer.status_offer_pkg = 'active' ";
        $sql .= "  LIMIT 0,1";
         
        return $this->db->query($sql)->result_array();
    }
    
    public function getOfferAmazonEUPrice($id_offer_sub, $id_region)
    {
        $id_offer_sub = (int)$id_offer_sub;
        $id_region = $this->db->escape_str($id_region);
        $sql = "SELECT sub.id_offer_sub_pkg,sub.title_offer_sub_pkg,price.amt_offer_price_pkg,price.currency_offer_price_pkg ";
        $sql .= "FROM offer_sub_packages sub LEFT OUTER JOIN offer_price_packages price ON(sub.id_offer_sub_pkg = price.id_offer_sub_pkg) ";
        $sql .= "WHERE price.id_offer_pkg = '2' AND sub.status_offer_sub_pkg = 'active' AND (price.id_region = '".$id_region."') AND sub.id_offer_sub_pkg = '".$id_offer_sub."' LIMIT 0,1";
     
        return $this->db->query($sql)->result_array();
    }
    
    public function getOfferSubPackage($id_offer_pkg, $id_region)
    {
        $id_offer_pkg = (int)$id_offer_pkg;
        $id_region = $this->db->escape_str($id_region);
        $sql = "SELECT sub.id_offer_sub_pkg,sub.title_offer_sub_pkg,sub.value_offer_sub_pkg,sub.icon_offer_sub_pkg,";
        $sql .= "price.id_offer_price_pkg,price.id_region,price.amt_offer_price_pkg,price.currency_offer_price_pkg,price.priod_offer_price_pkg,";
        $sql .= "price.status_offer_price_pkg,price.element_offer_sub_pkg/*,price.input_offer_sub_pkg*/ ";
        $sql .= "FROM offer_sub_packages sub LEFT OUTER JOIN offer_price_packages price ON(sub.id_offer_sub_pkg = price.id_offer_sub_pkg) ";
        $sql .= "WHERE price.id_offer_pkg = '".$id_offer_pkg."' AND sub.status_offer_sub_pkg = 'active' ";
        $sql .= "AND (price.id_region = '".$id_region."') ORDER BY sub.sort_offer_sub_pkg ASC";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getGiftCode($id_giftcode)
    {
        $this->db->where('id_giftcode', $id_giftcode);
        $this->db->where('status_giftcode', 'active');
        
        return $this->db->get('gift_code')->result_array();
    }
    
    public function getGiftCodeDetail($id_giftcode)
    {
        $id_giftcode = $this->db->escape_str($id_giftcode);
        $sql = "SELECT opt.id_giftopt,opt.name_giftopt,opt.amt_giftopt,opt.currency_giftopt,c.id_giftcode FROM gift_code c ";
        $sql .= "LEFT OUTER JOIN gift_option opt ON(c.id_giftopt=opt.id_giftopt) ";
        $sql .= "WHERE c.id_giftcode = '".$id_giftcode."' AND status_giftcode = 'active' LIMIT 0,1";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getGiftCodeDetailPrint($id_giftcode)
    {
        $id_giftcode = $this->db->escape_str($id_giftcode);
        $sql = "SELECT opt.name_giftopt FROM gift_code code LEFT OUTER JOIN gift_option opt ON(code.id_giftopt=opt.id_giftopt) ";
        $sql .= "WHERE code.id_giftcode = '".$id_giftcode."' LIMIT 0,1";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function packageList($session_feed=null, $session_region=null, $session_amazon=null)
    {
        $mydate = new Mydate();
        
        $packageArr = array();
        $main_pk = array();
        foreach ($session_feed as $key => $val) {
            foreach ($val as $key2 => $val2) {
                if (in_array($val2, $main_pk)) {
                    continue;
                }
                $main_pk[] = $val2;
                $key2 = $this->db->escape_str($key2);
                $sql = "SELECT offer.id_offer_pkg,offer.name_offer_pkg,price.id_offer_price_pkg,price.id_region,price.amt_offer_price_pkg,";
                $sql .= "price.currency_offer_price_pkg,price.priod_offer_price_pkg,price.offer_pkg_type ";
                $sql .= "FROM offer_packages offer LEFT OUTER JOIN offer_price_packages price ON(offer.id_offer_pkg = price.id_offer_pkg) ";
                $sql .= "WHERE 1/*price.id_region='".$key."'*/ AND price.id_offer_pkg='".$key2."' AND price.id_region ='' LIMIT 0,1";
                
                $query = $this->db->query($sql)->result_array();

                if ($query) {
                    $amount = $mydate->floatFormat($query[0]['amt_offer_price_pkg']);
                    $checkBilling = $this->getBillingFromDate($query[0]['id_offer_price_pkg'], date('Y-m-d H:i:s'));
                    $isPackage=true;
                    if (!empty($checkBilling)) {
                        $queryBilling = $this->getBilling($checkBilling[0]['id_billing']);

                        if ($queryBilling['status_billing']=='active') {
                            $isPackage = false;
                        }
                    }
                    if ($isPackage) {
                        $packageArr[] = array(
                        'name' => $query[0]['name_offer_pkg'].' Offer Package',
                        //'desc' => $query[0]['name_offer_pkg'].' Offer Package, '.$mydate->moneySymbol($query[0]['currency_offer_price_pkg']).$amount.'/'.$query[0]['priod_offer_price_pkg'].' ('.$session_region[$query[0]['id_region']].' Region)',
                        'desc' => $query[0]['name_offer_pkg'].' Offer Package, '.$mydate->moneySymbol($query[0]['currency_offer_price_pkg']).$amount.'/'.$query[0]['priod_offer_price_pkg'].' (Base Package)',
                        'amt' => $amount,
                        'number' => $query[0]['id_offer_price_pkg'],
                        'qty' => '1',
                        'taxamt' => '',
                        'type'=>'main',
                        'pk_type'=>$query[0]['offer_pkg_type']
                    );
                    }

//                    if($query[0]['id_offer_pkg']=='2' && $query[0]['id_region']=='eu' && !empty($session_amazon['eu']))
//                    {
                        $m_pk_id = $val2;
                    if (is_array($session_amazon)) {
                        foreach ($session_amazon as $key2 => $val2) {
                            foreach ($val2 as $val3) {
                                $val3 = (int)$val3;
                                $m_pk_id = (int)$m_pk_id;
                                $query[0]['id_region'] = $this->db->escape_str($query[0]['id_region']);
                                $sql = "SELECT price.offer_pkg_type ,price.id_offer_pkg,sub.id_offer_sub_pkg,sub.title_offer_sub_pkg,price.id_offer_price_pkg,price.amt_offer_price_pkg,price.domain_offer_sub_pkg,";
                                $sql .= "price.currency_offer_price_pkg,price.priod_offer_price_pkg ,price.offer_pkg_type ";
                                $sql .= "FROM offer_sub_packages sub LEFT OUTER JOIN offer_price_packages price ON(sub.id_offer_sub_pkg=price.id_offer_sub_pkg) ";
                                $sql .= "WHERE price.id_offer_price_pkg = '".$val3."' AND sub.status_offer_sub_pkg = 'active' and price.id_offer_pkg = '$m_pk_id' ";
                                $sql .= "/*AND (price.id_region = '".$query[0]['id_region']."')*/    LIMIT 0,1";
                             
                                $query2 = $this->db->query($sql)->result_array();
                            
                                if ($query2) {
                                    $amount = $mydate->floatFormat($query2[0]['amt_offer_price_pkg']);
                                    $url = $query2[0]['domain_offer_sub_pkg'];
                                    if ($query2[0]['offer_pkg_type'] == 3) {
                                        $packageArr[] = array(
                                        'name' => $query[0]['name_offer_pkg'].' '.$query2[0]['title_offer_sub_pkg'].' ('.$url .') ',
                                        'desc' => $query[0]['name_offer_pkg'].' '.$query2[0]['title_offer_sub_pkg'].' ('.$url .') , '.$mydate->moneySymbol($query2[0]['currency_offer_price_pkg']).$amount.'/'.$query2[0]['priod_offer_price_pkg'].'  ',
                                        'amt' => $amount,
                                        'number' => $query2[0]['id_offer_price_pkg'],
                                        'qty' => '1',
                                        'taxamt' => '',
                                        'type'=>'sub',
                                        'pk_type'=>$query2[0]['offer_pkg_type']

                                    );
                                    } else {
                                        $packageArr[] = array(
                                    'name' => $query[0]['name_offer_pkg'].' '.$query2[0]['title_offer_sub_pkg'].' ('.$url .') Package',
                                    'desc' => $query[0]['name_offer_pkg'].' '.$query2[0]['title_offer_sub_pkg'].' ('.$url .') Package, '.$mydate->moneySymbol($query2[0]['currency_offer_price_pkg']).$amount.'/'.$query2[0]['priod_offer_price_pkg'].'  ',
                                    'amt' => $amount,
                                    'number' => $query2[0]['id_offer_price_pkg'],
                                    'qty' => '1',
                                    'taxamt' => '',
                                    'type'=>'sub',
                                    'pk_type'=>$query2[0]['offer_pkg_type']
                                    
                                );
                                    }
                                }
                            }
                        }
                    }
//                    }
                }
            }
        }
        return $packageArr;
    }
    
    public function packageZoneList($session_feed=null)
    {
        $mydate = new Mydate();
        
        $packageArr = array();
        $main_pk = array();
        if (isset($session_feed['base']) && !empty($session_feed['base'])) {
            $session_feed['base'] = (int)$session_feed['base'];
            $sql = "SELECT price.offer_pkg_type ,price.id_offer_pkg, price.id_offer_price_pkg,price.amt_offer_price_pkg,price.domain_offer_sub_pkg,";
            $sql .= "price.currency_offer_price_pkg,price.priod_offer_price_pkg ,price.offer_pkg_type ";
            $sql .= "FROM   offer_price_packages price  ";
            $sql .= "WHERE price.id_offer_price_pkg = '".$session_feed['base']."'  ";
            $sql .= " and price.offer_pkg_type = '9'  LIMIT 0,1";
            
            $query = $this->db->query($sql)->result_array();

            if ($query) {
                $amount = $mydate->floatFormat($query[0]['amt_offer_price_pkg']);
                $checkBilling = $this->getBillingFromDate($query[0]['id_offer_price_pkg'], date('Y-m-d H:i:s'));
                $isPackage=true;
                if (!empty($checkBilling)) {
                    $queryBilling = $this->getBilling($checkBilling[0]['id_billing']);

                    if ($queryBilling['status_billing']=='active') {
                        $isPackage = false;
                    }
                }
                if ($isPackage) {
                    $packageArr[] = array(
                        'name' => $query[0]['domain_offer_sub_pkg'] ,
                        //'desc' => $query[0]['name_offer_pkg'].' Offer Package, '.$mydate->moneySymbol($query[0]['currency_offer_price_pkg']).$amount.'/'.$query[0]['priod_offer_price_pkg'].' ('.$session_region[$query[0]['id_region']].' Region)',
                        'desc' => $query[0]['domain_offer_sub_pkg'].' , '.$mydate->moneySymbol($query[0]['currency_offer_price_pkg']).$amount.'/'.$query[0]['priod_offer_price_pkg'].' (Base Package)',
                        'amt' => $amount,
                        'number' => $query[0]['id_offer_price_pkg'],
                        'qty' => '1',
                        'taxamt' => '',
                        'type'=>'main',
                        'period' => $query[0]['priod_offer_price_pkg'],
                        'pk_type'=>$query[0]['offer_pkg_type']
                    );
                }
            }
        }
        
         
          
        if (!empty($session_feed['sub']) && is_array($session_feed['sub'])) {
            foreach ($session_feed['sub'] as $key2 => $val2) {
                foreach ($val2 as $val3) {
                    $val3 = (int)$val3;
                    $sql = "SELECT price.offer_pkg_type ,price.id_offer_pkg, price.id_offer_price_pkg,price.amt_offer_price_pkg,price.domain_offer_sub_pkg,";
                    $sql .= "price.currency_offer_price_pkg,price.priod_offer_price_pkg ,price.offer_pkg_type ";
                    $sql .= "FROM   offer_price_packages price  ";
                    $sql .= "WHERE price.id_offer_price_pkg = '".$val3."'  ";
                    $sql .= " and price.offer_pkg_type = '4'  LIMIT 0,1";
                
                    $query2 = $this->db->query($sql)->result_array();
                
                    if ($query2) {
                        $amount = $mydate->floatFormat($query2[0]['amt_offer_price_pkg']);
                        $url = $query2[0]['domain_offer_sub_pkg'];
                        if ($query2[0]['priod_offer_price_pkg']=='time') {
                            $query2[0]['priod_offer_price_pkg']='livetime';
                        }
                    
                        $packageArr[] = array(
                            'name' =>  $url,
                            'desc' => $url.' , '.$mydate->moneySymbol($query2[0]['currency_offer_price_pkg']).$amount.'/'.$query2[0]['priod_offer_price_pkg'].'  ',
                            'amt' => $amount,
                            'number' => $query2[0]['id_offer_price_pkg'],
                            'qty' => '1',
                            'taxamt' => '',
                            'type'=>'sub',
                            'period' => $query2[0]['priod_offer_price_pkg'],
                            'pk_type'=>$query2[0]['offer_pkg_type']

                        );
                    }
                }
            }
        }
        return $packageArr;
    }
    
    public function inactiveGiftCode($id_giftcode)
    {
        $this->db->where('id_giftcode', $id_giftcode);
        return $this->db->update('gift_code', array('status_giftcode'=>'inactive'));
    }
    
    public function getOfferCurrency($id_offer_price_pkg)
    {
        $id_offer_price_pkg = (int)$id_offer_price_pkg;
        $sql = "SELECT currency_offer_price_pkg as currency FROM offer_price_packages ";
        $sql .= "WHERE id_offer_price_pkg = '".$id_offer_price_pkg."' LIMIT 0,1";
        return $this->db->query($sql)->result_array();
    }
    
    public function getAdminBillings($search='', $page=1, $order='b.created_date_billing', $sort='DESC', $limit=70)
    {
        $num_rows = $this->db->count_all_results('billings');
        $sql = "SELECT b.id_billing,b.amt_billing,b.currency_billing,b.status_billing,b.created_date_billing,b.payment_method_billing,b.discount_log_billing,b.discount_cmd_billing,u.user_name ";
        $sql .= "FROM billings b LEFT OUTER JOIN users u ON(b.id_users=u.id)";
        if ($search!='') {
            $search = $this->db->escape_like_str($search);
            $sql .= " WHERE (b.id_billing like '%".$search."%' || u.user_name like '%".$search."%' || b.amt_billing like '%".$search."%' || b.payment_method_billing like '%".$search."%' ";
            $sql .="|| DATE_FORMAT(b.created_date_billing,'%d/%m/%Y') like '%".$search."%' || b.status_billing like '%".$search."%')";
        }
        $page -= 1;
        $page = (int)$page;
        $limit = (int)$limit;
        $firstRec = $page*$limit;
        $order = $this->db->escape_str($search);
        $sql .= " ORDER BY ".$order." ".$sort." LIMIT ".$firstRec.",".$limit;
        
        $query = array();
        $query['query'] = $this->db->query($sql)->result_array();
        $query['page'] = ($page+1);
        $query['limit'] = $limit;
        $query['num'] = $num_rows;
        
        return $query;
    }
    
    public function getAdminBillingID($id_billing)
    {
        $id_billing = $this->db->escape_str($id_billing);
        $sql = "SELECT * FROM billings LEFT OUTER JOIN users ON(billings.id_users=users.id) WHERE billings.id_billing = '".$id_billing."' LIMIT 0,1";
        $query = $this->db->query($sql)->result_array();
        if ($query) {
            return $query[0];
        } else {
            return null;
        }
    }
    
    public function setUserPackage($arrUserPkg)
    {
        $this->db->where('id_users', $arrUserPkg['id_users']);
        $query = $this->db->get('user_packages', 1, 0)->result_array();
        if ($query) {
            unset($arrUserPkg['id_users']);
            unset($arrUserPkg['created_date_upkg']);
            $this->db->where('id_upkg', $query[0]['id_upkg']);
            return $this->db->update('user_packages', $arrUserPkg);
        } else {
            return $this->db->insert('user_packages', $arrUserPkg);
        }
    }
    public function cleanUserPackageDetail($id_users)
    {
        //        $this->db->delete('user_package_details', array('id_users' => $id_users)); 
        $sql = "select u.id_upkg_detail as id from user_package_details as u, offer_price_packages as p where u.id_users = '".(int)$id_users."' "
                . " and p.id_offer_price_pkg = u.id_package and p.offer_pkg_type = '4'";
        $id = $this->db->query($sql)->result_array();
        $o = array();
        foreach ($id as $i) {
            $o[]= $i['id'];
        }
        if (empty($o)) {
            return;
        }
        $sql = "delete from user_package_details where id_upkg_detail in   (".implode(',', $o).")";
        $this->db->query($sql);
    }
    
    public function setUserPackageDetail($arrUserPkgDetail)
    {
        $this->db->where('id_users', $arrUserPkgDetail['id_users']);
        $this->db->where('id_package', $arrUserPkgDetail['id_package']);
        
        $query = $this->db->get('user_package_details', 1, 0)->result_array();
        if (!$query) {
            return $this->db->insert('user_package_details', $arrUserPkgDetail);
        }
    }
    
    public function getUserPackage($id_user)
    {
        $this->db->where('id_users', $id_user);
        return $this->db->get('user_packages', 1, 0)->result_array();
    }
    
    public function getUserRegion($id_user)
    {
        $sql = "SELECT offer.id_region,reg.name_region FROM user_package_details detail LEFT OUTER JOIN offer_price_packages offer ON(detail.id_package = offer.id_offer_price_pkg) ";
        $sql .= "LEFT OUTER JOIN region reg ON(reg.id_region = offer.id_region) WHERE detail.id_users = '".(int)$id_user."' AND offer.id_region IS NOT NULL GROUP BY offer.id_region ";
        $sql .= "ORDER BY offer.id_region ASC";
       
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getUserPackageID($id_user, $sub=false)
    {
        $sql = "SELECT user_package_details.id_package,offer_price_packages.id_offer_pkg,offer_price_packages.id_region,offer_price_packages.id_offer_sub_pkg FROM user_package_details "
                . "LEFT OUTER JOIN offer_price_packages";
        $sql .= " ON (user_package_details.id_package = offer_price_packages.id_offer_price_pkg) ";
        /*if($sub)
            $sql .= "WHERE offer_price_packages.id_offer_sub_pkg IS NOT NULL ";
        else*/
        if (!$sub) {
            $sql .= "WHERE offer_price_packages.id_offer_sub_pkg IS NULL ";
        } else {
            $sql .= "WHERE 1 ";
        }
        $sql .= " and offer_price_packages.offer_pkg_type <> '3' AND user_package_details.id_users = '".(int)$id_user."' ORDER BY offer_price_packages.id_region ASC,offer_price_packages.id_offer_pkg ASC,offer_price_packages.id_offer_sub_pkg ASC";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getUserPackageZoneID($id_user, $sub=false)
    {
        $sql = "SELECT user_package_details.id_package,offer_price_packages.id_offer_pkg,offer_price_packages.id_region,offer_price_packages.id_offer_sub_pkg FROM user_package_details "
                . "LEFT OUTER JOIN offer_price_packages";
        $sql .= " ON (user_package_details.id_package = offer_price_packages.id_offer_price_pkg) ";
        /*if($sub)
            $sql .= "WHERE offer_price_packages.id_offer_sub_pkg IS NOT NULL ";
        else*/
        if (!$sub) {
            $sql .= "WHERE offer_price_packages.id_offer_sub_pkg IS NULL ";
        } else {
            $sql .= "WHERE 1 ";
        }
        $sql .= " and offer_price_packages.offer_pkg_type = '4' AND user_package_details.id_users = '".(int)$id_user."' ORDER BY offer_price_packages.id_region ASC,offer_price_packages.id_offer_pkg ASC,offer_price_packages.id_offer_sub_pkg ASC";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getAllUserPackageID($id_user)
    {
        $sql = "SELECT dt.id_upkg_detail,dt.id_users,dt.id_package,dt.option_package,ppkg.id_offer_price_pkg,ppkg.id_region,ppkg.id_offer_pkg,ppkg.id_offer_sub_pkg,";
        $sql .= "ppkg.amt_offer_price_pkg,ppkg.currency_offer_price_pkg,ppkg.priod_offer_price_pkg,ppkg.status_offer_price_pkg,opkg.name_offer_pkg,opkg.sort_offer_pkg,";
        $sql .= "opkg.submenu_offer_pkg,opkg.status_offer_pkg,ospkg.sort_offer_sub_pkg,ospkg.title_offer_sub_pkg,ospkg.value_offer_sub_pkg,ospkg.icon_offer_sub_pkg,";
        $sql .= "ppkg.element_offer_sub_pkg/*,ospkg.input_offer_sub_pkg*/,ospkg.status_offer_sub_pkg ";
        $sql .= "FROM user_package_details dt LEFT OUTER JOIN offer_price_packages ppkg ON(dt.id_package = ppkg.id_offer_price_pkg) ";
        $sql .= "LEFT OUTER JOIN offer_packages opkg ON(ppkg.id_offer_pkg = opkg.id_offer_pkg) LEFT OUTER JOIN offer_sub_packages ospkg ";
        $sql .= "ON(ppkg.id_offer_sub_pkg = ospkg.id_offer_sub_pkg) WHERE dt.id_users = '".(int)$id_user."' and ppkg.offer_pkg_type <> '3' ORDER BY ppkg.id_region ASC,ppkg.id_offer_pkg ASC,ppkg.id_offer_sub_pkg ASC";
        
        return $this->db->query($sql)->result_array();
    }
    
    public function getUserPackageByID($id_user, $id_offer_price, $dont_care = false)
    {
        $id_offer_price = (int)$id_offer_price;
        if (!$dont_care) {
            $sql = "SELECT dt.id_upkg_detail,dt.id_users,dt.id_package,dt.option_package,ppkg.id_offer_price_pkg,ppkg.id_region,ppkg.id_offer_pkg,ppkg.id_offer_sub_pkg,";
            $sql .= "ppkg.amt_offer_price_pkg,ppkg.currency_offer_price_pkg,ppkg.priod_offer_price_pkg,ppkg.status_offer_price_pkg,opkg.name_offer_pkg,opkg.sort_offer_pkg,";
            $sql .= "opkg.submenu_offer_pkg,opkg.status_offer_pkg,ospkg.sort_offer_sub_pkg,ospkg.title_offer_sub_pkg,ospkg.value_offer_sub_pkg,ospkg.icon_offer_sub_pkg,";
            $sql .= "ppkg.element_offer_sub_pkg,/*ospkg.input_offer_sub_pkg,*/ospkg.status_offer_sub_pkg ";
            $sql .= "FROM user_package_details dt LEFT OUTER JOIN offer_price_packages ppkg ON(dt.id_package = ppkg.id_offer_price_pkg) ";
            $sql .= "LEFT OUTER JOIN offer_packages opkg ON(ppkg.id_offer_pkg = opkg.id_offer_pkg) LEFT OUTER JOIN offer_sub_packages ospkg ";
            $sql .= "ON(ppkg.id_offer_sub_pkg = ospkg.id_offer_sub_pkg) WHERE ppkg.id_offer_price_pkg = '".$id_offer_price."' AND dt.id_users = '".(int)$id_user."' ";
            $sql .= "LIMIT 0,1";
        } else {
            $sql = "SELECT dt.id_upkg_detail,dt.id_users,dt.id_package,dt.option_package,ppkg.id_offer_price_pkg,ppkg.id_region,ppkg.id_offer_pkg,ppkg.id_offer_sub_pkg,";
            $sql .= "ppkg.amt_offer_price_pkg,ppkg.currency_offer_price_pkg,ppkg.priod_offer_price_pkg,ppkg.status_offer_price_pkg,opkg.name_offer_pkg,opkg.sort_offer_pkg,";
            $sql .= "opkg.submenu_offer_pkg,opkg.status_offer_pkg,ospkg.sort_offer_sub_pkg,ospkg.title_offer_sub_pkg,ospkg.value_offer_sub_pkg,ospkg.icon_offer_sub_pkg,";
            $sql .= "ppkg.element_offer_sub_pkg,/*ospkg.input_offer_sub_pkg,*/ospkg.status_offer_sub_pkg ";
            $sql .= "FROM offer_price_packages ppkg LEFT OUTER JOIN  user_package_details dt  ON(dt.id_package = ppkg.id_offer_price_pkg  AND dt.id_users = '".(int)$id_user."' ) ";
            $sql .= "LEFT OUTER JOIN offer_packages opkg ON(ppkg.id_offer_pkg = opkg.id_offer_pkg) LEFT OUTER JOIN offer_sub_packages ospkg ";
            $sql .= "ON(ppkg.id_offer_sub_pkg = ospkg.id_offer_sub_pkg) WHERE ppkg.id_offer_price_pkg = '".$id_offer_price."'";
            $sql .= "LIMIT 0,1";
        }
        return $this->db->query($sql)->result_array();
    }
    public function getUserPackageZoneByID($id_user, $id_offer_price, $dont_care = false)
    {
        $id_offer_price = (int)$id_offer_price;
        if (!$dont_care) {
            $sql = "SELECT dt.id_upkg_detail,dt.id_users,dt.id_package,dt.option_package,ppkg.id_offer_price_pkg,ppkg.id_region,ppkg.id_offer_pkg,ppkg.id_offer_sub_pkg,";
            $sql .= "ppkg.amt_offer_price_pkg,ppkg.currency_offer_price_pkg,ppkg.priod_offer_price_pkg,ppkg.status_offer_price_pkg,opkg.name_offer_pkg,opkg.sort_offer_pkg,";
            $sql .= "opkg.submenu_offer_pkg,opkg.status_offer_pkg,ospkg.sort_offer_sub_pkg,ospkg.title_offer_sub_pkg,ospkg.value_offer_sub_pkg,ospkg.icon_offer_sub_pkg,";
            $sql .= "ppkg.element_offer_sub_pkg,/*ospkg.input_offer_sub_pkg,*/ospkg.status_offer_sub_pkg ";
            $sql .= "FROM user_package_details dt LEFT OUTER JOIN offer_price_packages ppkg ON(dt.id_package = ppkg.id_offer_price_pkg) ";
            $sql .= "LEFT OUTER JOIN offer_packages opkg ON(ppkg.id_offer_pkg = opkg.id_offer_pkg) LEFT OUTER JOIN offer_sub_packages ospkg ";
            $sql .= "ON(ppkg.id_offer_sub_pkg = ospkg.id_offer_sub_pkg) WHERE ppkg.id_offer_price_pkg = '".$id_offer_price."' AND dt.id_users = '".(int)$id_user."' ";
            $sql .= "LIMIT 0,1";
        } else {
            $sql = "SELECT dt.id_upkg_detail,dt.id_users,dt.id_package,dt.option_package,ppkg.id_offer_price_pkg,ppkg.id_region,ppkg.id_offer_pkg,ppkg.id_offer_sub_pkg,";
            $sql .= "ppkg.amt_offer_price_pkg,ppkg.currency_offer_price_pkg,ppkg.priod_offer_price_pkg,ppkg.status_offer_price_pkg,opkg.name_offer_pkg,opkg.sort_offer_pkg,";
            $sql .= "opkg.submenu_offer_pkg,opkg.status_offer_pkg,ospkg.sort_offer_sub_pkg,ospkg.title_offer_sub_pkg,ospkg.value_offer_sub_pkg,ospkg.icon_offer_sub_pkg,";
            $sql .= "ppkg.element_offer_sub_pkg,/*ospkg.input_offer_sub_pkg,*/ospkg.status_offer_sub_pkg ";
            $sql .= "FROM offer_price_packages ppkg LEFT OUTER JOIN  user_package_details dt  ON(dt.id_package = ppkg.id_offer_price_pkg  AND dt.id_users = '".(int)$id_user."' ) ";
            $sql .= "LEFT OUTER JOIN offer_packages opkg ON(ppkg.id_offer_pkg = opkg.id_offer_pkg) LEFT OUTER JOIN offer_sub_packages ospkg ";
            $sql .= "ON(ppkg.id_offer_sub_pkg = ospkg.id_offer_sub_pkg) WHERE ppkg.id_offer_price_pkg = '".$id_offer_price."'";
            $sql .= "LIMIT 0,1";
        }
        return $this->db->query($sql)->result_array();
    }
    public function get_date_first_pk($id_user=0)
    {
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $sql = "select DATE_FORMAT(bd.created_date_bill_detail,'%e') as day from billings_detail bd , billings b "
                 . "where b.id_users = '".(int)$id_user."' and b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "order by bd.created_date_bill_detail asc limit 1";
        $out = $this->db->query($sql)->result_array();
        if (!empty($out)) {
            return $out[0]['day'];
        } else {
            return date('j');
        }
    }
    public function get_pk_next_charge($id_user=0)
    {
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $day = $this->get_date_first_pk($id_user);
        
        $add = 'now';
        if (date('j') >= $day) {
            $add='next month';
        }
        return date('Y-m-',  strtotime($add)).str_pad($day, 2, '0', STR_PAD_LEFT);
    }
    public function get_pk_remain_day($id_user=0)
    {
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $now = strtotime(date('Y-m-d')); // or your date as well
        $date = strtotime($this->get_pk_next_charge($id_user));
        $datediff = $date-$now;
        return floor($datediff/(60*60*24));
    }
    public function get_percent_discount()
    {
        return 100-($this->get_pk_remain_day()/date('t'))*100;
    }
    public function check_has_cronj_bill($id_user=0)
    {
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $sql ="select cronjob_flag from billings where id_users = '".(int)$id_user."' and status_billing = 'active'";
        $out = $this->db->query($sql)->result_array();
        if (!empty($out)) {
            return true;
        } else {
            return false;
        }
    }
    public function get_pk_n2end_day($day=5, $id_user=0)
    {
        if ($day>0) {
            $dt = "+$day days";
        } elseif ($day<0) {
            $dt = "".($day).' days';
        } else {
            $dt = "now";
        }
        $add="";
        if ($id_user==0 && strtolower($id_user) != 'all') {
            $id_user = $this->session->userdata['id'];
        }
        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '".(int)$id_user."' and ";
        }
        $sql = "select * from billings_detail bd , billings b , offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where  b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail= '".  date('Y-m-d', strtotime(" $dt "))."' "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type in ('9') "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc ";
        $out = $this->db->query($sql)->result_array();
        $return = array();
        foreach ($out as $v) {
            $return[$v['id_users']][] = $v;
        }
        return $return;
    }
    public function get_pk_expert_only($id_user=0)
    {
        $add = '';
        if ($id_user!=0) {
            $add = " and id_users = '".(int)$id_user."' ";
        }
        $sql = "select * from offer_price_packages where id_offer_pkg in (
        ( SELECT offer_price_packages.id_offer_pkg FROM  user_package_details INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg
        $add  group by offer_price_packages.id_offer_pkg )
        ) and offer_pkg_type = '3'";
        $out = $this->db->query($sql)->result_array();
        return $out;
    }
    public function get_pk_expert_mode($id_user, $market_place_type=array())
    {
        $x = array();
//        print_r($x); echo 'xxx'; return;
        $this->db->where(array('id_customer' => $id_user, 'name' => 'FEED_MODE'));
        $query = $this->db->get('configuration');
        $rows = $query->result();
        $return = array();
        foreach ($rows as $row) {
            $x = unserialize(base64_decode($row->value));
        }
        
        if (isset($x['mode']) && $x['mode']==2) {
            $sql = "select * from   offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                . "left join billings_detail bd on bd.id_package = pk.id_offer_price_pkg  "
                . "left join billings b on b.id_users = '".(int)$id_user."' and b.id_billing= bd.id_billing and status_billing = 'active' and end_date_bill_detail < '".date('Y-m-d')."'"
                . "where  opk.id_offer_pkg = pk.id_offer_pkg  and pk.id_offer_pkg in (".implode(',', $market_place_type).") and pk.offer_pkg_type = '3' "
                . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
          
            $out = $this->db->query($sql)->result_array();
              
            foreach ($out as $v) {
                $bdt_id = $v['id_bill_detail'];
                if (trim($bdt_id)=='') {
                    $return[] = $v;
                    continue;
                }
                $pk_id = (int)$v['id_package'];
                $u_id = (int)$id_user;
                $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                           . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                           . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
                $ch =$this->db->query($sql)->result_array();
                if ($bdt_id == $ch[0]['id_bill_detail']) {
                    $return[] = $v;
                }
            }
        }
        return $return;
    }
    public function get_pk_expired($id_user=0)
    {
        if ($id_user==0 && strtolower($id_user) != 'all') {
            $id_user = $this->session->userdata['id'];
        }
        $add=$add2= '';
        
        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '".(int)$id_user."' and ";
        } else {
            $add2 = " left join payments_preapproval pp on pp.id_user = b.id_users ";
        }
        
        $sql = "select * from billings_detail bd , billings b $add2, offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail < '".date('Y-m-d')."' "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg  and pk.offer_pkg_type <> '3'  "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
       
        $out = $this->db->query($sql)->result_array();
        $return = array();
        $market_type = array();
        foreach ($out as $v) {
            $bdt_id = $v['id_bill_detail'];
            $pk_id = (int)$v['id_package'];
            $u_id = (int)$v['id_users'];
            $market_type[$u_id][$v['id_offer_pkg']] = $v['id_offer_pkg'];
            $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
            $ch =$this->db->query($sql)->result_array();
            if ($bdt_id == $ch[0]['id_bill_detail']) {
                $return[(int)$v['id_users']][] = $v;
            }
        }
        $u_id = '';
        foreach (($return) as $uid => $pk) {
            if ($uid==$u_id) {
                continue;
            } else {
                $u_id = $uid;
            }
//             print_r($market_type[$uid]);

             $expert_pk = $this->get_pk_expert_mode($uid, $market_type[$uid]);
            foreach ($expert_pk as $pk) {
                $return[$uid][] = $pk;
            }
        }
          
        return $return;
    }
    public function get_pk_zone_expired($id_user=0)
    {
        if ($id_user==0 && strtolower($id_user) != 'all') {
            $id_user = $this->session->userdata['id'];
        }
        $add=$add2= '';
        
        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '".(int)$id_user."' and ";
        } else {
            $add2 = " left join payments_preapproval pp on pp.id_user = b.id_users ";
        }
        
        $sql = "select * from billings_detail bd , billings b $add2, offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail < '".date('Y-m-d')."' "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg  and pk.offer_pkg_type in ('9')  "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
       
        $out = $this->db->query($sql)->result_array();
        $return = array();
        $market_type = array();
        foreach ($out as $v) {
            $bdt_id = $v['id_bill_detail'];
            $pk_id = (int)$v['id_package'];
            $u_id = (int)$v['id_users'];
            $market_type[$u_id][$v['id_offer_pkg']] = $v['id_offer_pkg'];
            $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
            $ch =$this->db->query($sql)->result_array();
            if ($bdt_id == $ch[0]['id_bill_detail']) {
                $return[$v['id_users']][] = $v;
            }
        }
        $u_id = '';
        foreach (($return) as $uid => $pk) {
            if ($uid==$u_id) {
                continue;
            } else {
                $u_id = $uid;
            }
//             print_r($market_type[$uid]);

             $expert_pk = $this->get_pk_expert_mode($uid, $market_type[$uid]);
            foreach ($expert_pk as $pk) {
                $return[$uid][] = $pk;
            }
        }
          
        return $return;
    }
    public function get_pk_by_site($site, $ext)
    {
        $site = $this->db->escape_like_str($site);
        $ext = $this->db->escape_str($ext);
        $sql = "select id_offer_price_pkg as id, domain_offer_sub_pkg as url "
                . " from offer_packages opk ,offer_price_packages pk  "
                . " where opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type <>2  "
                . " and opk.name_offer_pkg like '$site' and pk.ext_offer_sub_pkg like '$ext' ";
       
        $out = $this->db->query($sql)->result_array();
        return isset($out[0]) ? $out[0] : array();
    }
    public function get_pk_zone_by_site($site, $ext)
    {
        $site = $this->db->escape_like_str($site);
        $ext = $this->db->escape_str($ext);
        $sql = "select pk.id_offer_price_pkg as id, pk.domain_offer_sub_pkg as url "
                . " from offer_packages opk ,offer_price_packages pk ,offer_price_packages tpk  "
                . " where opk.id_offer_pkg = tpk.id_offer_pkg and pk.offer_pkg_type = '4' and tpk.offer_pkg_type <>'2' "
                . " and opk.name_offer_pkg like '$site' and tpk.ext_offer_sub_pkg like '$ext' and tpk.id_region = pk.id_region ";
       
        $out = $this->db->query($sql)->result_array();
        return isset($out[0]) ? $out[0] : array();
    }
    public function get_site_by_id($id, $ebay=false)
    {
        $id = (int)$id;
        $field = $ebay?'id_site_ebay':'id_offer_sub_pkg';
        $sql = "select domain_offer_sub_pkg as domain, id_offer_sub_pkg as id_country from offer_price_packages where $field = '$id' ";
        $out = $this->db->query($sql)->result_array();
        return isset($out[0])?$out[0]:false;
    }
    
    public function get_base_pk_zone_not_expire($id_user)
    {
        $return = array();
        $add = '';
        if (strtolower($id_user) != 'all') {
            $add=" b.id_users = '".(int)$id_user."' and ";
        }
        $sql = "select bd.id_bill_detail,bd.id_package,id_users from billings_detail bd , billings b  , offer_packages opk  ,offer_price_packages pk "
                . "left join offer_sub_packages s on s.id_offer_sub_pkg = pk.id_offer_sub_pkg "
                 . "where $add b.id_billing = bd.id_billing and status_billing = 'active' "
                 . "and end_date_bill_detail >= '".date('Y-m-d')."'  "
                 . "and pk.id_offer_price_pkg = bd.id_package and opk.id_offer_pkg = pk.id_offer_pkg and pk.offer_pkg_type = 9  "
                 . "order by opk.id_offer_pkg asc,pk.id_offer_price_pkg asc";
       
        $q = $this->db->query($sql);
        $vx =  $q->result_array();
        foreach ($vx as $v) {
            $bdt_id = $v['id_bill_detail'];
            $pk_id = $v['id_package'];
            $u_id = $v['id_users'];
            $sql = "select bd.id_bill_detail from billings_detail bd , billings b "
                    . " where  b.id_billing = bd.id_billing and status_billing = 'active' "
                    . "and bd.id_package = '$pk_id' and b.id_users = '$u_id' order by bd.id_bill_detail desc limit 1";
             
            $q2 = $this->db->query($sql);
            $return = array();
            $ch = $q2->result_array();
             
            if ($bdt_id == $ch[0]['id_bill_detail']) {
                $return = $v;
                break;
            }
        }
         
        return $return;
    }
    
    public function get_pk_not_expire($id_user=0, $site_name=null, $ext=null)
    {
        $return=array();
        if ($id_user==0 && strtolower($id_user) != 'all') {
            $id_user = $this->session->userdata['id'];
        }
        $add = '';
        if (strtolower($id_user) != 'all') {
            $id_user = (int)$id_user;
            $out = $this->get_base_pk_zone_not_expire($id_user);
            if (empty($out)) {
                return $return;
            }
            $add=" b.id_users = '$id_user' and ";
        }
        $site_name = $this->db->escape_like_str($site_name);
        $sql = "SELECT bd.id_bill_detail,bd.id_package,id_users,bd.
         b.id_billing,bd.start_date_bill_detail,bd.end_date_bill_detail,bd.payment_method_billing,
        tpk.domain_offer_sub_pkg,tpk.name_offer_pkg,tpk.ext_offer_sub_pkg,tpk.offer_pkg_type FROM billings_detail bd, billings b, offer_packages opk, 	
	offer_price_packages pk
        LEFT JOIN offer_sub_packages s ON s.id_offer_sub_pkg = pk.id_offer_sub_pkg
        join offer_price_packages tpk  on tpk.ext_offer_sub_pkg LIKE '$ext' and pk.offer_pkg_type = '4' 
        WHERE 	$add
          b.id_billing = bd.id_billing
        AND status_billing = 'active'
        AND (tpk.id_offer_price_pkg = bd.id_package || pk.id_offer_price_pkg = bd.id_package)
        AND opk.id_offer_pkg = tpk.id_offer_pkg
        AND opk.id_offer_pkg = pk.id_offer_pkg
        AND opk.name_offer_pkg LIKE '$site_name' 
        AND tpk.id_region = pk.id_region   group by opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC ORDER BY opk.id_offer_pkg ASC, pk.id_offer_price_pkg ASC";
        
        $q = $this->db->query($this->db, $sql);
       
        $r = $q->result_array();
        if (!empty($r)) {
            $return = $r;
        }
        return $return;
    }
    
    public function checkBasePackageAvail($pkg_type=2, $targetDate=null, $id_user=0)
    {
        //return true=available ,false=unavailable;
        if ($targetDate==null) {
            $targetDate=date('Y-m-d H:i:s');
        }
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $id_user = (int)$id_user;
        $pkg_type = (int)$pkg_type;
        $sql = "SELECT offer_price_packages.id_offer_price_pkg, offer_price_packages.id_offer_pkg, billings_detail.start_date_bill_detail, billings_detail.end_date_bill_detail
                FROM offer_price_packages 
                INNER JOIN billings_detail ON (offer_price_packages.id_offer_price_pkg = billings_detail.id_package 
                and ('".$targetDate."' BETWEEN (billings_detail.start_date_bill_detail) AND (billings_detail.end_date_bill_detail)))
                WHERE 
                offer_price_packages.id_offer_pkg = '".$pkg_type."' AND 
                billings_detail.id_user = '".$id_user."' AND 
                offer_price_packages.offer_pkg_type = '2'";
        return $this->db->query($sql)->result_array();
    }
    
    public function checkPackageAvail($pkg_id=0, $targetDate=null, $id_user=0)
    {
        //return true=available ,false=unavailable;
        if ($pkg_id==0) {
            return 0;
        }
        if ($targetDate==null) {
            $targetDate=date('Y-m-d H:i:s');
        }
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $id_user = (int)$id_user;
        $sql = "SELECT offer_price_packages.id_offer_price_pkg, offer_price_packages.id_offer_pkg, billings_detail.start_date_bill_detail as start, billings_detail.end_date_bill_detail as end
                FROM offer_price_packages 
                INNER JOIN billings_detail ON (offer_price_packages.id_offer_price_pkg = billings_detail.id_package 
                 
                /*and ('".$targetDate."' BETWEEN (billings_detail.start_date_bill_detail) AND (billings_detail.end_date_bill_detail))*/)
                INNER JOIN billings ON (billings.id_billing = billings_detail.id_billing and  billings.status_billing = 'active')
                WHERE 
                offer_price_packages.id_offer_price_pkg = '".$pkg_id."'  and billings_detail.id_user = '".$id_user."' order by billings_detail.created_date_bill_detail desc limit 1";
        $pk = $this->db->query($sql)->result_array();
        if (sizeof($pk)==0) {
            return 0;  //not paid before
        } else {
            $start = $pk[0]['start'];
            $end = $pk[0]['end'];
            //echo $pkg_id.'. '.$start.'-'.$end." ($targetDate) ".'<br>';
            if (strtotime($start) <= strtotime($targetDate) && strtotime($end) >= strtotime($targetDate)) {
                return 1; //available
            } elseif (strtotime($end) < strtotime($targetDate)) {
                return 2; //expired
            } else {
                return -1; //future purchased / ERROR
            }
        }
    }
    
    public function getBillingFromDate($id_package, $targetDate=null, $id_user=0)
    {
        if ($targetDate==null) {
            $targetDate=date('Y-m-d H:i:s');
        }
        if ($id_user==0) {
            $id_user = $this->session->userdata['id'];
        }
        $id_user = (int)$id_user;
        //echo $targetDate.' '.strtotime('2014-07-10');exit;
        //$sql = "SELECT * FROM billings_detail WHERE ('".$targetDate."' BETWEEN UNIX_TIMESTAMP(start_date_bill_detail) AND UNIX_TIMESTAMP(end_date_bill_detail)) ";
        $sql = "SELECT * FROM billings_detail , billings WHERE ('".$targetDate."' BETWEEN (start_date_bill_detail) AND (end_date_bill_detail)) ";
        $sql .= "AND id_package = '".$id_package."' and id_user = '$id_user' and billings_detail.id_billing = billings.id_billing and billings.status_billing = 'active' order by billings_detail.id_bill_detail desc LIMIT 0,1";
        
       
        return $this->db->query($sql)->result_array();
    }
    
    public function getPaypalTimezone($date)
    {
        $offset = $this->get_timezone_offset('US/Pacific');
        $time = strtotime($date);
        $paypal_time = $time-$offset;
        //echo $offset;exit;
        return date('Y-m-d\TH:i:s', $paypal_time);
    }
    private function get_timezone_offset($remote_tz, $origin_tz = null)
    {
        if ($origin_tz === null) {
            if (!is_string($origin_tz = date_default_timezone_get())) {
                return false; // A UTC timestamp was returned -- bail out!
            }
        }
        $origin_dtz = new DateTimeZone($origin_tz);
        $remote_dtz = new DateTimeZone($remote_tz);
        $origin_dt = new DateTime("now", $origin_dtz);
        $remote_dt = new DateTime("now", $remote_dtz);
        $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
        return $offset;
    }
}
