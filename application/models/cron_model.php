<?php

class cron_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function getCronList($marketplace='')
    {
        $add = '';
        if ($marketplace!=='') {
            $marketplace = $this->db->escape_str($marketplace);
            $add = " and cron_marketplace = '$marketplace'";
        }
        $sql = "select * from cron_tasks where cron_status = 1 $add and display_task = 1 order by sort_by";
        $res = $this->db->query($sql)->result_array();
        return $res;
    }
    
    public function getUserCronList($user_id=0, $marketplace='', $ext='', $comp = false, $id_sub_marketplace=0)
    {
        $add =$add2=$add3= '';
        $out= array();
        $user_id=(int)$user_id;
        if ($marketplace!=='') {
            $marketplace = $this->db->escape_like_str($marketplace);
            $add = " and c.cron_marketplace like '$marketplace' ";
        } else {
            $add = " and c.cron_marketplace  is null ";
        }
        if ($ext!=='') {
            $ext = $this->db->escape_str($ext);
            $add2 = " and cs.ext = '$ext' ";
        }
        if ((int)$id_sub_marketplace!=0) {
            $add3 = " and cs.site = '{$id_sub_marketplace}' ";
        }
        if ($user_id!=0 && !$comp) {
            $sql = "select c.cron_id as id_cron,c.*,cs.* from cron_tasks c  left join cronjob_status cs on  c.cron_id = cs.cron_id and cs.id_customer = '$user_id' $add2 $add3 where   c.cron_status  not in (0,3) and c.display_task = 1 and c.cron_for_all_user=0 $add order by c.cron_type asc";
        } elseif ($user_id!=0 && $comp) {
            $sql = "SELECT c.cron_type AS 'task_name', CONCAT('".$marketplace.$ext." ', c.cron_action, ' ', c.cron_type) AS 'task_display_name', c.last_update AS 'task_lastupdate',c .cron_id AS 'task_id', '' AS 'task_status', CASE WHEN cs.`status` IS NOT NULL THEN '' ELSE 1 END AS 'task_running', CASE WHEN REPLACE(c.cron_key, '{ext}', '$ext') IS NULL THEN CONCAT('".$marketplace.$ext."_', c.cron_action, '_', c.cron_type) ELSE REPLACE(c.cron_key, '{ext}', '$ext') END AS  'task_key' FROM cron_tasks c LEFT JOIN cronjob_status cs ON c.cron_id = cs.cron_id AND cs.id_customer = '$user_id' $add2 where c.cron_status NOT IN(0, 3) AND c.cron_for_all_user = 0 $add order by c.cron_type asc";
        } else {
            $sql = "select c.cron_id as id_cron,c.*,cs.* from cron_tasks c  left join cronjob_status cs on  c.cron_id = cs.cron_id  $add2 where   c.cron_status   not in (0,3) $add order by c.cron_type asc";
        }
        $out = $this->db->query($sql)->result_array();
        
        return $out;
    }
    
    public function updateUserCron($user_id, $cron_id, $ext, $status, $id_sub_market=null)
    {
        $user_id = (int)$user_id;
        $ext = $this->db->escape_str($ext);
        $cron_id = (int)$cron_id;
        $status = $this->db->escape_str($status);
        if (!empty($id_sub_market)) {
            $sql = "replace into cronjob_status (id_customer,cron_id,ext,status,site) values ('$user_id','$cron_id','$ext','$status','$id_sub_market')";
        } else {
            $sql = "replace into cronjob_status (id_customer,cron_id,ext,status) values ('$user_id','$cron_id','$ext','$status')";
        }
//        echo $sql;
        $this->db->query($sql);
    }
    
    public function getTimeLastCronRun($user_id, $history_key)
    {
        $user_id = (int)$user_id;
        $history_key = $this->db->escape_str($history_key);
        $sql = "select history_data from histories where user_id = '$user_id' and history_action like '$history_key' order by history_date_time desc limit 1 ";
        if (defined('ADMIN_TEST_ONLY')) {
            echo $sql.'<br>';
        }
        $out = $this->db->query($sql)->result_array();
        $return = 0;
        if (isset($out[0]['history_data'])) {
            $data = $out[0]['history_data'];
            $d = json_decode($data, true);
            if (isset($d['next_time'])) {
                $return = $d['next_time'];
            }
        }
        return $return;
    }
    
    public function getBestBackendHost($user_id=null)
    {
        if (!empty($user_id)) {
            $user_id = (int)$user_id;
            $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb ,users u"
                    . "  where u.backend_host_id = fb.id and fb.status = 1  and u.id = '$user_id' order by  fb.server_load  asc limit 1 ";
            $q = $this->db->query($sql);
            if ($q->num_rows()==0) {
                $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb "
                    . "  where  fb.status = 1  order by  fb.server_load  asc limit 1 ";
            }
        } else {
            $sql = "select DISTINCT  fb.id,fb.ip_target,fb.ip, fb.server_load  from  feedbiz_backend fb "
                    . "  where  fb.status = 1  order by  fb.server_load  asc limit 1 ";
        }
        $out = $this->db->query($sql)->result_array();
        if (empty($out)) {
            return '';
        } else {
            $o = isset($out[0]['ip_target'])&&!empty($out[0]['ip_target'])?$out[0]['ip_target']:'';
            return $o;
        }
    }
    
    public function getIdByTargetIp($target_ip)
    {
        $target_ip = $this->db->escape_str($target_ip);
        $sql="select  id from feedbiz_backend where   ip_target = '$target_ip' ";
        $d = $this->db->query($sql)->result_array();
        return isset($d[0]['id'])?$d[0]['id']:false;
    }
    
    public function setLastHost($user_id, $id)
    {
        $id = (int)$id;
        $user_id = (int)$user_id;
        $sql = "update users set backend_host_id = '$id' where id = '$user_id' ";
        $q = $this->db->query($sql);
        return $q;
    }
    
    public function get_amazon_log_display_filter()
    {
        $user_name = $this->session->userdata('user_name');
        $id_shop = $this->session->userdata('id_shop');
        if (empty($user_name) || empty($id_shop)) {
            return array();
        }
        require_once dirname(__FILE__) . '/../libraries/db.php';
        $sql = "select field_name,id_country from  amazon_log_display where id_shop= '$id_shop'  and menu = 'features' and  field_toggle = 1 and field_disabled = 0 and field_name in ('repricing','fba')";
        
        $db = new Db($user_name);
        $out = $db->db_query_string_fetch($sql);
        $ret = array();
        foreach ($out as $o) {
            $ret[$o['id_country']][$o['field_name']] = true;
        }
        return $ret;
    }
}
