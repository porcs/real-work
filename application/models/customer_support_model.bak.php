<?php
    class customer_support_model extends CI_Model {
        public $support_id = 0;
        public $support_email = '';
        public $support_name = '';
        public $support_count = 0;
        public $support_get_loop = 0;
        private $req_last_contact = false;
       public function __construct() {
            parent::__construct();
            $id=null;
            if(  $this->session->userdata('support_employee_id')!=''){
                $id = $this->session->userdata('support_employee_id');
            }else if($this->req_last_contact){
                $id = $this->get_last_contact();
            }
            $this->support_get_loop=0;
            $this->get_support_employee($id);
            
        }
        
        public function get_support_employee($id=null){
            $out = array();
            $today = date('D');
            if($id===null){
                if($this->support_get_loop>5){
                    $sql = "select * from support_employee where  status = 1  order by   RAND() ";          
                }else{
                    $sql = "select * from support_employee where avail_date like '%$today%' and status = 1 and online_status = 1 order by contact_count asc , RAND() ";          
                }
            }else{
                $sql = "select * from support_employee where id = '$id' and avail_date like '%$today%' and status = 1 and online_status = 1 limit 1 ";
            }
            $result = $this->db->query($sql);
            $query = $result->result_array();
                if(isset($query[0])){$out = $query[0];
                    $this->support_id = $out['id'];
                    $this->support_email = $out['email'];
                    $this->support_name = $out['name'];
                    $this->support_count = $out['contact_count'];
                    $this->session->set_userdata('support_employee_id',$out['id']);
                    $this->session->set_userdata('support_employee_name',$out['name']);
                    $this->session->set_userdata('support_employee_info',$out);
                }else{
                    $this->support_get_loop++;
                    $out = get_support_employee();
                }
            return $out;
        }
        public function inc_count_contact($condition=''){
            $this->db->update('support_employee',array('contact_count'=>$this->support_count+1),array('id'=>$this->support_id));
            $this->insert_history($condition);
        }
        public function insert_history($condition=''){
            if($condition=='')$condition='Registration';
            $ip_addr = $_SERVER['REMOTE_ADDR'];
            $user_id = $this->session->userdata('id');
            $cus_email = $this->session->userdata('user_email');
            if($user_id=='')return;
            $this->db->insert('support_history',array(
               'user_id'=>$user_id,
               'emp_id'=>$this->support_id,
               'user_email'=>$cus_email,
               'emp_email'=>$this->support_email,
               'user_id'=>$ip_addr,
               'create_date'=>date('Y-m-d H:i:s'),
               'condition'=>$condition
            ));
        }
        public function get_last_support_id(){
            $out = null;
            if($this->session->userdata('id')!=''){
             $sql = "select * from support_history where user_id = '$id' order by id desc limit 1";
             $result = $this->db->query($sql);
                $query = $result->result_array();
                if(isset($query[0]))$out = $query[0]['emp_id'];
            }
             
            return $out;
        }
        public function send_email2supporter($cus_email='',$email=''){
            if($email==''){
                $email = $this->support_email;
            }
            if($cus_email=='' && $this->session->userdata('user_email')!=''){
                $cus_email = $this->session->userdata('user_email');
            }else{
                return false;
            }
            require_once APPPATH.'libraries/Swift/swift_required.php'; 
                
                $arrEmail = array($email);  
                $f_name = 'notify_default_email.html'; 
                $reason = "Got new register : ".$cus_email."<br>Please assist to registration and submit feed.";
                try
                {

                    $filePath = str_replace('\\', '/', FCPATH).'assets/template/'.$f_name;
                    $oriEmailTemplate = file_get_contents($filePath); 
                    $transport = Swift_SmtpTransport::newInstance();
                    $logo = '<img src="'.str_replace('https','http',base_url().'/nblk/images/feedbiz_logob.png').'" alt="Feed.biz">';

                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);
                    $msg ='@ '.date('r').'<br>reason: '.$reason;      
                    if(!empty($arrEmail))
                    {
                        foreach($arrEmail as $email)
                        { 

                            $email_template = $oriEmailTemplate;
                            $email_template = str_replace("{#logo}", $logo, $email_template);
                            $email_template = str_replace("{#msg}", $msg , $email_template);
                            $email_template = str_replace("{#refemail}", $email, $email_template);

                            $swift_message = Swift_Message::newInstance();
                            $swift_message->setSubject('[Sys]Feed.biz get new register!')
                                    //->setFrom(array($u_email=>$u_name))
                                    ->setFrom(array($email=>$email))
                                    ->setTo($email)
                                    ->setBody($email_template,'text/html');

                            $mailer->send($swift_message);
                        }
                    }
                      
                }
                catch(Exception $ex)
                {}
            
        }
    }

    
    
    
    

