<?php

class Mirakl_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

//    public function addParameter($data)
//    { // must use in save parameter
//        return $this->db->replace('mirakl_configuration', $data);
//    }
    
    public function getApiKey($id_customer, $sub_marketplace, $id_country, $id_shop)
    {
        // must use in parameters 
        $sql = "SELECT api_key, auto_accept, active FROM mirakl_configuration ";
        $sql.= "WHERE id_customer = $id_customer AND "
            . " sub_marketplace = $sub_marketplace AND id_country = $id_country "
            . " AND id_shop = $id_shop ";

        $query = $this->db->query($sql);
        return $query->row_array();
    }
    
    public function isActiveApi($id_customer, $sub_marketplace, $id_country, $id_shop)
    {
        $sql = "SELECT active FROM mirakl_configuration "
            . "WHERE id_customer = $id_customer AND "
            . " sub_marketplace = $sub_marketplace AND "
            . " id_country = $id_country AND id_shop = $id_shop ";
        $query = $this->db->query($sql);
        return isset($query->row()->active) ? $query->row()->active : false;
    }

    public function get_parameters($id_customer, $sub_marketplace, $id_country, $id_shop)
    { // to use :: Mirakl/orders_page and task
        if (empty($id_customer) || empty($sub_marketplace)
            || empty($id_country) || empty($id_shop)) {
            return '';
        }

        $data = $where = array();
        $where['mirakl_configuration.id_customer'] = $id_customer;
        $where['mirakl_configuration.sub_marketplace'] = $sub_marketplace;
        $where['mirakl_configuration.id_country'] = $id_country;
        $where['mirakl_configuration.id_shop'] = $id_shop;

        $this->db->where($where);
        $query = $this->db->from('mirakl_configuration');
        $query = $this->db->join('users',
            'mirakl_configuration.id_customer = users.id', 'left');
        $query = $this->db->join('languages',
            'users.user_default_language = languages.id', 'left');

        $get = $query->get();
        $rows = $get->result();

        $marketplace = $this->db->query("SELECT name_marketplace "
            . " FROM offer_sub_marketplace "
            . " WHERE id_offer_sub_marketplace = $sub_marketplace ")->row()->name_marketplace;
        $api_key = $this->session->userdata('key');

        foreach ($rows as $row) {
            $data['id_customer'] = $row->id_customer;
            $data['sub_marketplace'] = $row->sub_marketplace;
            $data['id_country'] = $row->id_country;
            $data['id_shop'] = $row->id_shop;
            $data['ext'] = $row->ext;
            $data['user_name'] = $row->user_name;
            $data['countries'] = str_replace(array(' '), '_', $row->countries);
            $data['currency'] = $row->currency;
            $data['id_lang'] = $row->id_lang;
            $data['iso_code'] = $row->iso_code;
            $data['id_region'] = $row->id_region;
            $data['operator'] = $row->operator;
            $data['api_key'] = !empty($api_key) ? $api_key : $row->api_key;
            $data['active'] = isset($row->active) && $row->active == 1 ? true : false;
            $data['id_marketplace'] = 6;
            $data['marketplace'] = $marketplace;
            $data['user_code'] = $row->user_code;
            $data['user'] = array(
                'email' => $row->user_email,
                'firstname' => $row->user_first_name,
                'lastname' => $row->user_las_name);
            $data['language'] = isset($row->language_name) ?
                $row->language_name : 'english';
            $data['auto_accept'] = $row->auto_accept;
        }

        return $data;
    }

    public function getCurrency($currency)
    {
        $currency = $this->db->escape_str($currency);
        $query = $this->db->query("SELECT id FROM currency "
            . " WHERE currency = '$currency' ");
        $resut = $query->result_array();
        return isset($resut[0]['id']) ? $resut[0]['id'] : 0;
    }

    public function check_allow_cron($user_id)
    {
        $row = 0;
        if (isset($this->db) && !empty($this->db)) {
            $user_id = (int) $user_id;
            $sql = "select user_id as id from histories "
                . " where history_action like 'export_mirakl%_synchronize' "
                . " and user_id ='{$user_id}' group by user_id";

            $result = $this->db->db_sqlit_query($sql);
            $row = $this->db->db_num_rows($result);
        }
        return $row > 0 ? true : false;
    }

    

   
    
    public function getAttributes($sub_marketplace)
    {
        $data = array();
        $sql = "SELECT * FROM mirakl_attribute "
            . " WHERE sub_marketplace = $sub_marketplace ORDER BY id_attribute ";
        $results = $query = $this->db->query($sql)->result_array();
        foreach ($results as $key => $result) {
            $data[$key]['id_attribute'] = $result['id_attribute'];
            $data[$key]['sub_marketplace'] = $result['sub_marketplace'];
            $data[$key]['code'] = str_replace(" ", "_", $result['code']);
            $data[$key]['type'] = $result['type'];
            $data[$key]['label'] = str_replace(" ", "_", $result['label']);
        }

        return $data;
    }

    public function getMiraklValue($sub_marketplace)
    {
        $data = array();
        $sql = "SELECT * FROM mirakl_value "
            . " WHERE sub_marketplace = $sub_marketplace ORDER BY label ";
        $results = $query = $this->db->query($sql)->result_array();
        foreach ($results as $key => $result) {
            $data[$key]['id_value'] = $result['id_value'];
            $data[$key]['label'] = $result['label'];
        }
        return $data;
    }
    
//    public function getAttributeByHierarchy($hierarchy_code)
//    {
//        $data = array();
//        
//        if(!isset($hierarchy_code) || empty($hierarchy_code)){
//            return $data;
//        }
//        
//        $sql = "SELECT * FROM mirakl_attribute "
//            . " WHERE hierarchy_code = '$hierarchy_code' ORDER BY id_attribute ";
//        $results = $this->db->query($sql)->result_array();
//        
//        if (isset($results) && !empty($results)) {
//            foreach ($results as $key => $result) {
//                $data['mapping'][$result['code']]['id_attribute'] = $result['id_attribute'];
//                $data['mapping'][$result['code']]['code'] = $result['code'];
//                $data['mapping'][$result['code']]['hierarchy_code'] = $result['hierarchy_code'];
//                $data['mapping'][$result['code']]['label'] = $result['label'];
//                $data['mapping'][$result['code']]['type_parameter'] = $result['type_parameter'];
//                $data['mapping'][$result['code']]['values_list'] = $result['values_list'];
//            }
//        }
//        
//        return $data;
//    }
    
    public function getMiraklValueList($id_value)
    {
        $data = array();
        $sql = "SELECT id_value_list, label "
            . " FROM mirakl_value_list "
            . " WHERE id_value = $id_value ORDER BY label ";
        $results = $query = $this->db->query($sql)->result_array();
        foreach ($results as $key => $result) {
            $data[$key]['id_value_list'] = $result['id_value_list'];
            $data[$key]['label'] = $result['label'];
        }
        return $data;
    }
    
    public function getMiraklMarketplace()
    {
        $sql = "SELECT * FROM offer_sub_marketplace ";
        $results = $query = $this->db->query($sql)->result_array();
        return $results;
    }
}
