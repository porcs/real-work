<?php

class Admin_common extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    function get_mfa_status_users(){
        $out = array();
        $target_file = USERDATA_PATH.'mfa_activate_users.json';
        if(file_exists($target_file)){
            $out = json_decode(file_get_contents($target_file),true);
        }
        return $out;
    }
    public function login() {
        $return = false;
        
        $sysUser = $this->config->item('sysUser');
        $crudUser = $this->input->post('crudUser', true);
        $key_list = $this->get_mfa_status_users();
        $otp = !empty($crudUser) &&!empty($crudUser['mfa_otp'])?$crudUser['mfa_otp']:'';
        
        if (!empty($crudUser) && isset($crudUser['name']) && isset($crudUser['password'])) {
            if ($sysUser['enable'] == true) {
                if ($crudUser['name'] == $sysUser['name'] &&
                        $crudUser['password'] == $sysUser['password']) {
                    $auth = array();
                    $auth['user_name'] = $sysUser['name'];
                    
                    $group = array('group_name' => 'SystemAdmin',
                    		'group_manage_flag' => 3,
                    		'group_setting_management' => 1,
                    		'group_global_access' => 1
                    );
                    
                    $auth['group'] = $group;
                    $auth['__system_admin__'] = 1;

                    $this->session->set_userdata('AUTH', $auth);
                    $return = true;
                }
            }
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('user_name', $crudUser['name']);
            $this->db->where('user_password', sha1($crudUser['password']));
            
            $mfa_mail = $crudUser['name'];
            if($crudUser['name']=='admin'){
                $mfa_mail='admin@demo.com';
            }
            $otp_pass=false;
//            if(!empty($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR']=='58.8.64.111')$otp_pass=true;
            if(isset($key_list[$mfa_mail])){
               include(dirname(__FILE__).'/../../libraries/mfa/config.php');
               $key = base64_decode($key_list[$mfa_mail]);
               if(TokenAuth6238::verify($key,$otp)){
                   $otp_pass=true;
               }
            }else{
                $otp_pass=true;
            }

            $query = $this->db->get();
            $rs = $query->row_array();

            if (!empty($rs) && $otp_pass) {

                $this->db->select('*');
                $this->db->from('groups');
                $this->db->where('id', $rs['group_id']);

                $query = $this->db->get();
                $rs1 = $query->row_array();

                if (!empty($rs1)) {
                    $rs['group'] = $rs1;
                } else {
                    $rs['group'] = array('group_name' => 'None',
							'group_manage_flag' => 0,
							'group_setting_management' => 0,
							'group_global_access' => 0
					);
                }
                unset($rs['group_id']);
                unset($rs['user_password']);
                unset($rs['user_info']);
                $rs['__system_admin__'] = 0;
                $this->session->set_userdata('AUTH', $rs);
                $return = true;
                require_once FCPATH . 'application/third_party/scrud/class/Cookie.php';
                
                if (isset($_POST['remember_me']) && (int)$_POST['remember_me'] == 1  ){
                	Cookie::Set('AUTH', serialize(array(base64_encode($crudUser['name']), base64_encode(sha1($crudUser['password'])))),Cookie::SevenDays);
                }else{
                	Cookie::Delete('AUTH');
                }
            }
        }

        return $return;
    }

}