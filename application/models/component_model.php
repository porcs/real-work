<?php

class component_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }    
    
    public function check_allow_cron($user_id = null, $action = null) {
        if 
            ((!isset($user_id) && empty($user_id)) ||
             (!isset($action) && empty($action)))
            return false;
        
        $action = $this->db->escape_str($action);
        $user_id = (int)$user_id;
        $sql = "Select user_id as id From histories Where history_action Like '{$action}' and user_id ='{$user_id}' group by user_id";
        $result = $this->db->query($sql);

        return $result->num_rows > 0 ? true : false;
    }
    
    public function get_error_resolutions($marketplace_name = null, $error_code = null, $lang = null) {      
        if ((!isset($marketplace_name) && empty($marketplace_name))) return false;
        
        $language = $this->config->item('language');
        $iso_code = mb_substr($language, 0, 2);
        
        $table = strtolower($marketplace_name).'_error_resolutions';
        $content = array();
        
        if (!isset($lang) || empty($lang)) {
            $lang = $iso_code;
        }
        $lang = $this->db->escape_str($lang);
        $table = $this->db->escape_str($table);
	
	if(isset($error_code) && !empty($error_code)) {
	    $error_code = " AND  `error_code` = '" .  $this->db->escape_str($error_code) . "' ";
	}
	
        $iso_code = $this->db->escape_str($iso_code);
        $sql = "SELECT *,
                CASE WHEN `error_lang` =  '$lang' THEN `error_lang` ELSE null END AS primary_lang
                FROM (`$table`)
                WHERE (`error_lang` =  '$lang' OR `error_lang` = '$iso_code') $error_code";
	
        $get = $this->db->query($sql);
        $result = $get->result();
        
        foreach ($result as $value) {
		$key = $value->error_code;
		if(isset($value->primary_lang) && !empty($value->primary_lang)) {
		    $content[$key]['error_details'] = $value->error_details;
		    $content[$key]['error_priority'] = $value->error_priority;
		    $content[$key]['error_resolution'] = $value->error_resolution;
		    $content[$key]['allow_overide'] = $value->allow_overide;
		    $content[$key]['show_message'] = $value->show_message;
		    $content[$key]['message_pattern'] = $value->message_pattern;
		    $content[$key]['other'] = $value->other;
		    
		    if(isset($error_code) && !empty($error_code)) {
			return $content[$key];
		    }
		   
		} else {
		    $content[$key]['error_details'] = $value->error_details;
		    $content[$key]['error_priority'] = $value->error_priority;
		    $content[$key]['error_resolution'] = $value->error_resolution;
		    $content[$key]['show_message'] = $value->show_message;
		    $content[$key]['message_pattern'] = $value->message_pattern;
		    $content[$key]['other'] = $value->other;
		}
        }
		
	if ( isset($error_code) && !empty($error_code) ) {
	    return $content[$error_code];
	} else {
	    return $content;
	}
    }
}