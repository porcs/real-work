<?php

class manager_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    function getManagerToken($id_manager)
    {
        $token = '';
        $this->db->select('token_assist');
        $this->db->where(array('id' => $id_manager) );
        $query = $this->db->get('users');
        $rows = $query->result();
        
        foreach($rows as $row)
            $token = $row->token_assist;
        return $token;
    }

    function getUserInService($token)
    { 
        $this->db->select('*');
        $this->db->where(array('token_assist' => $token,'user_status'=>1) );
        $query = $this->db->get('users');
        $rows = $query->result_array();
        return empty($rows)?array():$rows;
    }

    function updateManagerToken($new_token,$old_token)
    {
        $this->db->select('token_assist');
        $this->db->where(array('token_assist' => $new_token,'user_status'=>2) );
        $query = $this->db->get('users');
        $rows = $query->result_array();
        if(!empty($rows)){
            return false;
        }
        $data = array('token_assist' => $new_token);
        $where = array('token_assist' => $old_token ) ;
        $query = $this->db->update('users',$data,$where);
        return empty($query)?false:true;
    }
    function setManagerToken($id_manager)
    {
        $token = md5($id_manager.date('c').$id_manager);
        $data = array('token_assist' => $token);
        $where = array('id' => $id_manager ) ;
        $query = $this->db->update('users',$data,$where);
        return $token;
    }
}

