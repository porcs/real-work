<?php

require_once dirname(__FILE__) . '/../libraries/db.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.database.php';

class amazon_model extends CI_Model 
{
    
    public function __construct() 
    {
        parent::__construct();
    }
    
    public function get_error_resolutions($type, $error_code=null, $lang=null)
    {       
        $language = $this->config->item('language');
        $iso_code = mb_substr($language, 0, 2);
        
        $content = array();
        
        if(!isset($lang) || empty($lang)){
           $lang = $iso_code;
        }
        $lang = $this->db->escape_str($lang);
        $table = $this->db->escape_str('amazon_error_resolutions');
	
	if(isset($error_code) && !empty($error_code)) {
	    $error_code = " AND  `error_code` = '" .  $this->db->escape_str($error_code) . "' ";
	}
	
        $sql = "SELECT *,
                CASE WHEN `error_lang` = '$lang' THEN `error_lang` ELSE null END AS primary_lang
                FROM (`$table`)
                WHERE (`error_lang` =  '$lang') $error_code AND `error_type` = '$type' ";

        $get = $this->db->query($sql);
        $result = $get->result();
        
        foreach ($result as $value) {
           
		$key = $value->error_code;
		if(isset($value->primary_lang) && !empty($value->primary_lang)) {
		    $content[$key]['error_type'] = $value->error_type;
		    $content[$key]['error_details'] = $value->error_details;
		    $content[$key]['error_solved_type'] = $value->error_solved_type;
		    $content[$key]['error_resolution'] = $value->error_resolution;
		    $content[$key]['allow_overide'] = $value->allow_overide;
		    $content[$key]['show_message'] = $value->show_message;
		    $content[$key]['message_pattern'] = $value->message_pattern;
		    $content[$key]['error_priority'] = $value->error_priority;
		    $content[$key]['other'] = $value->other;
		    $content[$key]['notification_summary'] = $value->notification_summary;
		    
		    if(isset($error_code) && !empty($error_code)) {
			return $content[$key];
		    }
		   
		} else {
                    $content[$key]['error_type'] = $value->error_type;
		    $content[$key]['error_details'] = $value->error_details;
		    $content[$key]['error_solved_type'] = $value->error_solved_type;
		    $content[$key]['error_resolution'] = $value->error_resolution;
		    $content[$key]['show_message'] = $value->show_message;
		    $content[$key]['message_pattern'] = $value->message_pattern;
		    $content[$key]['error_priority'] = $value->error_priority;
		    $content[$key]['other'] = $value->other;
		    $content[$key]['notification_summary'] = $value->notification_summary;
		}
        }
		
	if(isset($error_code) && !empty($error_code)) {
	    return $content[$error_code];
	} else {
	    return $content;
	}
    }
    
    public function get_mode($id_customer, $username) 
    {
        if(empty($id_customer)) { return ''; }              
	
        $data = $priority = array();
        $this->db->where(array('amazon_configuration.id_customer' => $id_customer));
        $query = $this->db->from('amazon_configuration');
        $get = $query->get();
        $rows = $get->result();
	
	$AmazonDatabase = new AmazonDatabase($username);
	
	//get display field
	$fields = $AmazonDatabase->get_configuration_features(null, null, true);

        $types = array('products', 'offers');
        foreach ($types as $type){
            $msgs = $this->get_error_resolutions($type, null, mb_substr($this->language, 0, 2));
            foreach ($msgs as $key => $message){
                if(isset($message['error_priority']) && $message['error_priority']){
                    $priority[$key] = $message;
                }
            }
        }

        //get error notification
        $AmazonDatabase->set_error_notification($priority);

	//get error resolution
	$error_resolutions = $AmazonDatabase->get_error_resolutions_rows(null, null, true, $priority);

        foreach($rows as $row){              
            
	    if(isset($row->ext)) {
		$data[$row->id_shop]['mode']['creation'][$row->ext] = isset($row->creation) && $row->creation == 1 ? true : false;  
		$data[$row->id_shop]['fields'][$row->ext] = isset($fields[$row->id_shop][$row->id_country]) ? $fields[$row->id_shop][$row->id_country] : null ;
		$data[$row->id_shop]['fields'][$row->ext]['create_out_of_stock'] = isset($row->create_out_of_stock) && $row->create_out_of_stock == 1 ? true : false;  
		$data[$row->id_shop]['fields'][$row->ext]['allow_send_order_cacelled'] = isset($row->allow_send_order_cacelled) && $row->allow_send_order_cacelled == 1 ? true : false;  
		$data[$row->id_shop]['error_resolution'][$row->ext] = isset($error_resolutions[$row->id_shop][$row->id_country]) ?
                                                                        $error_resolutions[$row->id_shop][$row->id_country] : null ;
	    }
            //$data[$row->id_shop]['error_notification'] = isset($log_notifications[$row->id_shop]) ? $log_notifications[$row->id_shop] : null ;
        }   

        return $data;
    }        
    
    public function get_repricing_parameters($id_customer, $id_shop, $id_country=null) 
    {
        if(empty($id_customer) || empty($id_shop))
            return ''; 
        
        $table = 'amazon_repricing';
        $data = $where = array();
        $where[$table.'.id_customer'] = $id_customer;
        $where[$table.'.id_shop'] = $id_shop;
        
        if(isset($id_country) && !empty($id_country))
            $where[$table.'.id_country'] = $id_country;
        
        $this->db->where($where);
        $query = $this->db->from($table);
        $get = $query->get();
        $rows = $get->result();
       
        foreach($rows as $row) {
            $data[$row->id_country][$row->name] = $row->value;
            $data[$row->id_country]['ext'] = $row->ext;
        }
        
        return $data;
    }
    
    public function get_parameters($id_customer, $ext, $id_shop, $use_ext_like_country = false) 
    {
        if(empty($id_customer) || empty($ext) || empty($id_shop))
            return ''; 
        
        $data = $where = array();
        $where['amazon_configuration.id_customer'] = $id_customer;
        
        if($use_ext_like_country) 
            $where['amazon_configuration.id_country'] = $ext;
        else  
            $where['amazon_configuration.ext'] = $ext;
        
        $where['amazon_configuration.id_shop'] = $id_shop;
        
        $this->db->where($where);
        $query = $this->db->from('amazon_configuration');
        $query = $this->db->join('users', 'amazon_configuration.id_customer = users.id', 'left');
        $query = $this->db->join('languages', 'users.user_default_language = languages.id', 'left');
        $get = $query->get();
        $rows = $get->result();
        foreach($rows as $row)
        {
            if(!empty($row->merchant_id) && !empty($row->aws_id) && !empty($row->secret_key)){
                
                $data['id_customer'] = $row->id_customer;
                $data['user_name'] = $row->user_name;
                $data['user_code'] = $row->user_code;
                $data['user'] = array('email' => $row->user_email , 'firstname' => $row->user_first_name, 'lastname' => $row->user_las_name) ;
                $data['id_country'] = $row->id_country;
                $data['ext'] = $row->ext;
                $data['countries'] = str_replace(array(' '), '_', $row->countries);
                $data['currency'] = $row->currency;
                $data['merchant_id'] = $row->merchant_id;
                $data['aws_id'] = $row->aws_id;
                $data['secret_key'] = $row->secret_key;
                $data['auth_token'] = $row->auth_token;
                $data['id_mode'] = $row->id_mode;
                $data['id_shop'] = $row->id_shop;
                $data['id_lang'] = $row->id_lang;
                $data['iso_code'] = $row->iso_code;
                $data['id_region'] = $row->id_region;
                $data['allow_automatic_offer_creation'] = isset($row->allow_automatic_offer_creation) && $row->allow_automatic_offer_creation == 1 ? true : false;
                $data['synchronize_quantity'] = isset($row->synchronize_quantity) && $row->synchronize_quantity == 1 ? true : false;
                $data['synchronize_price'] = isset($row->synchronize_price) && $row->synchronize_price == 1 ? true : false;
                $data['synchronization_field'] = 'ean13';
                $data['active'] = isset($row->active) && $row->active == 1 ? true : false;
                $data['creation'] = isset($row->creation) && $row->creation == 1 ? true : false;
                $data['language'] = $row->language_name;     
                $data['cron_update_offers'] = isset($row->cron_update_offers) && $row->cron_update_offers == 1 ? true : false;
                $data['cron_send_orders'] = isset($row->cron_send_orders) && $row->cron_send_orders == 1 ? true : false;    
                $data['cron_update_order_status'] = isset($row->cron_update_order_status) && $row->cron_update_order_status == 1 ? true : false;  
                $data['cron_create_products'] = isset($row->cron_create_products) && $row->cron_create_products == 1 ? true : false;  
                $data['cron_delete_products'] = isset($row->cron_delete_products) && $row->cron_delete_products == 1 ? true : false; 
                $data['allow_send_order_cacelled'] = isset($row->allow_send_order_cacelled) && $row->allow_send_order_cacelled == 1 ? true : false; 
                $data['create_out_of_stock'] = isset($row->create_out_of_stock) && $row->create_out_of_stock == 1 ? true : false; 
                
            }
        }
        
        return $data;
    }
    
    public function get_fba($id_customer, $ext, $id_shop) {       
        //ALTER TABLE `amazon_fba` ADD COLUMN `shipment_details`  text NULL AFTER `date_add`;
	if(empty($id_customer) || empty($ext) || empty($id_shop))
            return ''; 
        
        $where = array();
        $where['id_customer'] = $id_customer;       
	$where['ext'] = $ext;
        $where['id_shop'] = $id_shop;
        
        $this->db->where($where);
        $query = $this->db->from('amazon_fba');
        $get = $query->get();
        $rows = $get->result();
	
        foreach($rows as $row)
        {
            if(isset($row->shipment_details) && !empty($row->shipment_details)){
                $row->shipment_details =  unserialize($row->shipment_details);
            }
	    return (array)$row;
	}
	
        return (false);
    }
    
    public function get_fba_master($id_customer, $id_shop, $region) {       
        
	if(empty($id_customer) || empty($region) || empty($id_shop))
            return ''; 
        
        $where = array();
        $where['amazon_fba.id_customer'] = $id_customer;       
	$where['amazon_fba.region'] = $region;
        $where['amazon_fba.id_shop'] = $id_shop;
        $where['amazon_fba.fba_master_platform'] = 1;
        $this->db->where($where);
        
        $query = $this->db->from('amazon_fba');
	$query = $this->db->join('amazon_configuration', 'amazon_configuration.id_customer = amazon_fba.id_customer AND amazon_configuration.ext = amazon_fba.ext ', 'left');
        $this->db->group_by('amazon_fba.id_customer');
        $get = $query->get();
        $rows = $get->result();
	
        foreach($rows as $row)
        {
	    return $row->countries;
	}
	
        return (false);
    }
    
    public function check_allow_cron($user_id){
        $row = 0;
        if(isset($this->db) && !empty($this->db)) { 
            $user_id = (int)$user_id;
                $sql = "select user_id as id from histories where history_action like 'amazon%_synchronize' and user_id ='{$user_id}' group by user_id";

                $result = $this->db->db_sqlit_query($sql);
                $row = $this->db->db_num_rows($result); 
        }
        return $row>0?true:false;
    }
    
    public function set_fba($data, $region=null) {       
	$table = 'amazon_fba';	
        if (!empty($data)){

            $filte_data = _filter_data($table, $data);   
	    
	    $where = array('id_customer' => $data['id_customer'], 'id_shop' => $data['id_shop']);
	    
	    if(isset($region) && !empty($region))
		$where['region'] = $region;
	    
	    if(isset($data['region']) && !empty($data['region']))
		$where['region'] = $data['region'];
	    
	    if(isset($data['id_country']) && !empty($data['id_country']))
		$where['id_country'] = $data['id_country'];
	    
            $this->db->where($where);
	    
            $this->db->get($table);
	   
            if($this->db->affected_rows() != 0) {		
                $result = $this->db->update($table, $filte_data, $where);
            } else {
		if(!isset($region) || empty($region)){
		    $result = $this->db->insert($table, $filte_data);
		} else {
		    return (true);
		}
            }
	    
            if(!$result) return (false);
        }
	
        return (true);	
    }    
    
    public function set_repricing_parameters($repricing) 
    {
        $table = 'amazon_repricing';
        
        foreach ($repricing as $data){
            
            $filte_data = _filter_data($table, $data);        
            $this->db->where(array('id_customer' => $data['id_customer'], 'id_country' => $data['id_country'], 'id_shop' => $data['id_shop'], 'name'=>$data['name']));
            $this->db->get($table);

            if($this->db->affected_rows() != 0) {
                $id_customer = $data['id_customer'];
                unset($data['id_customer']);
                $where = array('id_customer'=>$id_customer,'id_country'=>$data['id_country'],'id_shop'=>$data['id_shop'],'name'=>$data['name']);
                $result = $this->db->update($table, $filte_data, $where);
            } else {
                $result = $this->db->insert($table, $filte_data);
            }
            if(!$result) return FALSE;
        }
        return TRUE;

    }
    
    public function set_parameters($data) 
    {
        $this->load->helper('custom_helper');
        $filte_data = _filter_data('amazon_configuration', $data);
        $this->db->where(array('id_customer' => $data['id_customer'], 'id_country' => $data['id_country'], 'id_shop' => $data['id_shop']));
        $this->db->get('amazon_configuration');
        
        if($this->db->affected_rows() != 0)
        {
            $id_customer = $data['id_customer'];
            unset($data['id_customer']);
            $result = $this->db->update('amazon_configuration', $filte_data, array('id_customer'=>$id_customer, 'id_country'=>$data['id_country'], 'id_shop'=>$data['id_shop']));
        }
        else
            $result = $this->db->insert('amazon_configuration', $filte_data);
        
        if($result) return TRUE;
        
        return FALSE;

    }
    
}

