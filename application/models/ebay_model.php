<?php
class ebay_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
	    public function get_error_resolutions($error_code, $lang) 
	    {       
	        $language = $this->config->item('language');
	        $iso_code = mb_substr($language, 0, 2);
	        
	        $table = 'ebay_error_resolutions';
	        $content = array();
	        
	        if(!isset($lang) || empty($lang)){
	           $lang = $iso_code;
	        }
	        
	        $sql = "SELECT *,
	                CASE WHEN `error_lang` =  '$lang' THEN `error_lang` ELSE null END AS primary_lang
	                FROM (`$table`)
	                WHERE `error_code` =  '$error_code'
	                AND (`error_lang` =  '$lang' OR `error_lang` = '$iso_code')";
	        $get =$this->db->query($sql);
	        $result = $get->result();
	        
	        foreach ($result as $value) {
	            
	            if(isset($value->primary_lang) && !empty($value->primary_lang)) {
	                $content['error_details'] = htmlspecialchars($value->error_details);
	                $content['error_priority'] = htmlspecialchars($value->error_priority);
	                $content['error_resolution'] = htmlspecialchars($value->error_resolution);
	                $content['allow_overide'] = htmlspecialchars($value->allow_overide);
	                $content['show_message'] = htmlspecialchars($value->show_message);
	                $content['message_pattern'] = htmlspecialchars($value->message_pattern);
	                $content['other'] = htmlspecialchars($value->other);
	                return $content;
	            } else {
	                $content['error_details'] = $value->error_details;
	                $content['error_priority'] = $value->error_priority;
	                $content['error_resolution'] = $value->error_resolution;
	                $content['show_message'] = $value->show_message;
	                $content['message_pattern'] = $value->message_pattern;
	                $content['other'] = $value->other;
	            }
	        }
	        
	        return $content;
	    }
        
        function set_configuration($user_id = '', $ebay_data = '') {
                if ( empty($user_id) || !is_numeric($user_id) || empty($ebay_data) ) {
                        return FALSE;
                }
                foreach ( $ebay_data as $ebay_key => $ebay_value ) {
                        if ( isset($ebay_value) ) {
                                $sources_data                   = array(
                                        'id_customer'           => $user_id,
                                        'name'                  => $ebay_key,
                                        'value'                 => $ebay_value,
                                        'config_date'           => date("Y-m-d H:i:s", strtotime("now")),
                                );
                                $this->db->where(array('id_customer' => $user_id, 'name' => $ebay_key));
                                $this->db->get('configuration');

                                if ( $this->db->affected_rows() == 1 )
                                        $query                  = $this->db->update('configuration', $sources_data, array('id_customer' => $user_id, 'name' => $ebay_key));
                                else
                                        $query                  = $this->db->insert('configuration', $sources_data);

                                if( !$query )
                                        return FALSE;
                        }
                        else {
                            return FALSE;
                        }
                }
                return TRUE;
        }
        
        function get_configuration($user_id = '', $optional = null) {
            if ( empty($user_id) || !is_numeric($user_id) ) {
                    return array();
            }
            $this->db->where('id_customer', $user_id);
            if (isset($optional) && $optional != null) {
                $this->db->where('name', $optional);
            }
            $query = $this->db->get('configuration');

            if ( $query->num_rows > 0 ) {
                $result = $query->result();
                $query->free_result();
                return $result;
            }
            return array();
        }
        
        function unset_configuration($user_id = '', $ebay_data = '') {
                if ( empty($user_id) || !is_numeric($user_id) || empty($ebay_data) ) {
                        return FALSE;
                }

                foreach ( $ebay_data as $ebay_key => $ebay_value ) {
                        if ( isset($ebay_value) ) {
                                $this->db->where(array('id_customer' => $user_id, 'name' => $ebay_key));
                                $query = $this->db->delete('configuration');
                                
                                if( !$query )
                                        return FALSE;
                                else 
                                        return TRUE;
                        }
                        else {
                            return FALSE;
                        }
                }
        }
        
        function unset_ebay_configuration($user_id = '', $ebay_data = '', $id_site = '') {
                if ( empty($user_id) || !is_numeric($user_id) || empty($ebay_data) || !isset($id_site) ) {
                        return FALSE;
                }
                $sql                                            = "SHOW TABLES LIKE 'ebay_configuration'";
                $query                                          = $this->db->query($sql);
                if ( $query->num_rows == 0 ) :
                        $this->db->query("CREATE TABLE ebay_configuration(id_customer int(11) NOT NULL, name varchar(255) NOT NULL, value varchar(255) NOT NULL, id_site int(11) NOT NULL, config_date datetime, PRIMARY KEY (id_customer, name, id_site))");
                endif;
                foreach ( $ebay_data as $ebay_key => $ebay_value ) {
                        if ( isset($ebay_value) ) {
                                $this->db->where(array('id_customer' => $user_id, 'name' => $ebay_key, 'id_site' => $id_site));
                                $query = $this->db->delete('ebay_configuration');
                                
                                if( !$query )
                                        return FALSE;
                                else 
                                        return TRUE;
                        }
                        else {
                            return FALSE;
                        }
                }
        }
        
        function set_ebay_configuration($user_id = null, $ebay_data = null, $id_site = null) {
                if ( empty($user_id) || !is_numeric($user_id) || empty($ebay_data) || !isset($id_site) ) {
                        return FALSE;
                }
                $sql                                            = "SHOW TABLES LIKE 'ebay_configuration'";
                $query                                          = $this->db->query($sql);
                if ( $query->num_rows == 0 ) :
                        $this->db->query("CREATE TABLE ebay_configuration(id_customer int(11) NOT NULL, name varchar(255) NOT NULL, value varchar(255) NOT NULL, id_site int(11) NOT NULL, config_date datetime, PRIMARY KEY (id_customer, name, id_site))");
                endif;
                foreach ( $ebay_data as $ebay_key => $ebay_value ) {
                        if ( isset($ebay_value) ) {
                                $sources_data                   = array(
                                        'id_customer'           => $user_id,
                                        'name'                  => $ebay_key,
                                        'value'                 => $ebay_value,
                                        'id_site'               => $id_site,
                                        'config_date'           => date("Y-m-d H:i:s", strtotime("now")),
                                );
                                $this->db->where(array('id_customer' => $user_id, 'name' => $ebay_key, 'id_site' => $id_site));
                                $this->db->get('ebay_configuration');
                                if ( $this->db->affected_rows() == 1 )
                                        $query                  = $this->db->update('ebay_configuration', $sources_data, array('id_customer' => $user_id, 'name' => $ebay_key, 'id_site' => $id_site));
                                else
                                        $query                  = $this->db->insert('ebay_configuration', $sources_data);

                                if( !$query )
                                        return FALSE;
                        }
                        else {
                            return FALSE;
                        }
                }
                return TRUE;
        }
        
    function getEbayConfiguration($user_id = null, $id_site = null) {
        if ((!isset($user_id) && empty($user_id)) 
            || (!isset($id_site) && empty($id_site)) 
        ) {
            return array();
        }
        $this->db->where('id_customer', $user_id);
        $this->db->where('id_site', $id_site);
        $query = $this->db->get('ebay_configuration');

        if ( $query->num_rows > 0 ) {
            $result = $query->result();
            $query->free_result();
            return $result;
        }
        return array();
    }
        
        function set_default_site($user_id = '', $id_site = '') {
                if ( empty($user_id) || !is_numeric($user_id) || !isset($id_site) ) {
                        return FALSE;
                }
                $sql                                            = "SHOW TABLES LIKE 'ebay_sites_default'";
                $query                                          = $this->db->query($sql);
                if ( $query->num_rows == 0 ) :
                        $this->db->query("CREATE TABLE ebay_sites_default(id_customer int(11) NOT NULL, id_site int(11) NOT NULL, is_default int(1) NOT NULL, is_active int(1) NOT NULL, config_date datetime, PRIMARY KEY (id_customer, id_site))");
                endif;
                $sources_data                                   = array(
                        'id_customer'                           => $user_id,
                        'id_site'                               => $id_site,
                        'is_default'                            => 1,
                        'is_active'                             => 1,
                        'config_date'                           => date("Y-m-d H:i:s", strtotime("now")),
                );
                $this->db->where(array('id_customer' => $user_id));
                $this->db->get('ebay_sites_default');
                if ( $this->db->affected_rows() > 0 ) {
                        $query                                  = $this->db->update('ebay_sites_default', array('is_default' => 0), array('id_customer' => $user_id));
                }
                $this->db->where(array('id_customer' => $user_id, 'id_site' => $id_site));
                $this->db->get('ebay_sites_default');
                if ( $this->db->affected_rows() == 1 ) {
                        $query                  = $this->db->update('ebay_sites_default', $sources_data, array('id_customer' => $user_id, 'id_site' => $id_site));
                }
                else {
                        $query                  = $this->db->insert('ebay_sites_default', $sources_data);
                }
                if( $query )
                        return FALSE;
                return TRUE;
        }
        
        
        function get_category($site = '', $top = FALSE, $where = '') {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('ebay_categories.id_category, ebay_categories_sites.name, ebay_categories.id_parent, ebay_categories_sites.level');
                $this->db->from('ebay_categories');
                $this->db->join('ebay_categories_sites', 'ebay_categories.id_category = ebay_categories_sites.id_category AND ebay_categories.id_site = ebay_categories_sites.id_site');
                ($top == TRUE) ? $this->db->where('is_root_category', "1") : '';
                if (!empty($where)) {
                        foreach ( $where as $key => $value ) {
                                $this->db->where($key, $value);
                        }
                }
                $this->db->where('ebay_categories_sites.id_site', $site);

                $query                                          = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_category_array($site = '', $top = FALSE, $where = '') {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('ebay_categories.id_category, ebay_categories_sites.name, ebay_categories.id_parent, ebay_categories_sites.level');
                $this->db->from('ebay_categories');
                $this->db->join('ebay_categories_sites', 'ebay_categories.id_category = ebay_categories_sites.id_category AND ebay_categories.id_site = ebay_categories_sites.id_site');
                ($top == TRUE) ? $this->db->where('is_root_category', "1") : '';
                if (!empty($where)) {
                        foreach ( $where as $key => $value ) {
                                $this->db->where($key, $value);
                        }
                }
                $this->db->where('ebay_categories_sites.id_site', $site);

                $query                                          = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_category_by_id($id_category = '') {
                if ( !isset($id_category) || !is_numeric($id_category) ) {
                        return FALSE;
                }
                $this->db->select('id_category, id_parent, is_root_category');
                $this->db->from('ebay_categories');
                $this->db->where('id_category', $id_category);
                $query                                          = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }
   
        function get_attribute_group($where = null, $config = FALSE) {
                if ( empty($where) ) {
                        return FALSE;
                }
                
                $sql = 'SELECT '
                        . "a.id_category, "
                        . "a.id_group, "
                        . "a.id_selection_mode, "
                        . "g.name "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_group g ON a.id_group = g.id_group "
                        . "WHERE ".$where." "
                        . "ORDER BY g.name ASC";
                
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        if ($config == FALSE) :
                                $result             = $query->result();
                        else :
                                $result             = $query->result_array();
                        endif;
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
   
        function get_variation_group($id_category = null, $id_site = null) {
                if ( (!isset($id_category) && empty($id_category)) || (!isset($id_site) && empty($id_site)) ) {
                        return FALSE;
                }
                
                $sql = 'SELECT '
                        . "a.id_category, "
                        . "a.id_group, "
                        . "a.id_selection_mode, "
                        . "a.enable_validation, "
                        . "g.name "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_group g ON a.id_group = g.id_group "
                        . "WHERE a.id_category IN (".$id_category.") "
                        . "AND a.id_site = '".$id_site."' "
                        . "AND a.enable_validation = '1' "
                        . "GROUP BY id_group "
                        . "ORDER BY g.name ASC";
                
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        $query->free_result();
                        return FALSE;
                }
                return NULL;
        }
 
        function get_variation_value($id_category = null, $id_site = null) {
                if ( (!isset($id_category) && empty($id_category)) || (!isset($id_site) && empty($id_site)) ) {
                        return FALSE;
                }
                
                $sql = 'SELECT DISTINCT '
                        . "s2.id_group As id_group, "
                        . "s2.value As value, "
                        . "s1.name As name "
                        . "FROM ( "
                        . 'SELECT '
                        . "a.id_group As id_group, "
                        . "g.name As name "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_group g ON a.id_group = g.id_group "
                        . "WHERE a.id_category IN (".$id_category.") "
                        . "AND a.id_site = '".$id_site."' "
                        . "AND a.enable_validation = '1' "
                        . "ORDER BY g.name ASC "
                        . ") s1 "
                        . "LEFT JOIN ebay_attributes_value s2 ON (s1.id_group = s2.id_group) "
                        . "WHERE s2.id_site = '".$id_site."' "
                        . "ORDER BY s2.id_group";


                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        $query->free_result();
                        return FALSE;
                }
                return NULL;
        }
        
   
        function get_specific_group($id_category = null, $id_site = null, $without = null) {
                if ( (!isset($id_category) && empty($id_category)) || (!isset($id_site) && empty($id_site)) || (!isset($without) && empty($without)) ) {
                        return FALSE;
                }
                
                $where = '';
                
                if ( !empty($without) ) :
                        foreach ( $without as $none_spacific ) :
                                $where      .= 'AND g.name <> "'.htmlspecialchars($none_spacific['name_attribute']).'" ';
                        endforeach;
                endif;
                
                $sql = 'SELECT '
                        . "a.id_category, "
                        . "a.id_group, "
                        . "a.id_selection_mode, "
                        . "a.enable_validation, "
                        . "g.name "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_group g ON a.id_group = g.id_group "
                        . "WHERE a.id_category = '".$id_category."' "
                        . $where
                        . "AND a.id_site = '".$id_site."' "
                        . "GROUP BY id_group "
                        . "ORDER BY g.name ASC";
                
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        $query->free_result();
                        return FALSE;
                }
                return FALSE;
        }
 
        function get_specific_value($id_category = null, $id_site = null, $without = null) {
                if ( (!isset($id_category) && empty($id_category)) || (!isset($id_site) && empty($id_site)) || (!isset($without) && empty($without)) ) {
                        return FALSE;
                }
                
                $where = '';
                
                if ( !empty($without) ) :
                        foreach ( $without as $none_spacific ) :
                                $where      .= 'AND g.name <> "'.htmlspecialchars($none_spacific['name_attribute']).'" ';
                        endforeach;
                endif;
                
                $sql = 'SELECT DISTINCT '
                        . "s2.id_group As id_group, "
                        . "s2.value As value, "
                        . "s1.name As name "
                        . "FROM ( "
                        . 'SELECT '
                        . "a.id_group As id_group, "
                        . "g.name As name "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_group g ON a.id_group = g.id_group "
                        . "WHERE a.id_category = '".$id_category."' "
                        . $where
                        . "AND a.id_site = '".$id_site."' "
                        . "AND g.name <> 'Brand' "
                        . "AND g.name <> 'BPA-Free Plastic' "
                        . "AND g.name <> 'Buckwheat' "
                        . "AND g.name <> 'Cutler-Hammer' "
                        . "AND g.name <> 'Diamondback' "
                        . "AND g.name <> 'Silver' "
                        . "AND g.name <> 'Dark Wood Tone' "
                        . "ORDER BY g.name ASC "
                        . ") s1 "
                        . "LEFT JOIN ebay_attributes_value s2 ON (s1.id_group = s2.id_group) "
                        . "WHERE s2.id_site = '".$id_site."' "
                        . "ORDER BY s2.id_group";

                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        $query->free_result();
                        return FALSE;
                }
                return FALSE;
        }
 
        function get_specific_processing($id_site = null, $q = null, $type = null) {
                if ( (!isset($id_site) && empty($id_site)) || (!isset($q) && empty($q)) || (!isset($type) && empty($type))) {
                        return FALSE;
                }

                $sql = 'SELECT DISTINCT '
                        . "s2.`value` AS id, "
                        . "s2.`value` AS text "
                        . "FROM ebay_attributes_group s1 "
                        . "LEFT JOIN ebay_attributes_value s2 ON(s1.id_group = s2.id_group) "
                        . "WHERE "
                        . "s2.id_site = '".$id_site."' "
                        . "AND s1.`name` = '".htmlspecialchars($type)."' "
                        . "AND s2.`value` LIKE '".htmlspecialchars($q)."%' "
                        . "ORDER BY "
                        . "s2.`value` ASC";

                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        $query->free_result();
                        return FALSE;
                }
                return FALSE;
        }
        
        function get_attribute_value($name = null, $config = FALSE) {
                if ( empty($name) ) {
                        return FALSE;
                }

                $sql = 'SELECT DISTINCT '
                        . "v.`value` as `value`, "
                        . "v.id_group as id_group "
                        . "FROM ebay_attributes_group g "
                        . "LEFT JOIN ebay_attributes_value v on g.id_group = v.id_group "
                        . "WHERE g.`name` LIKE '".html_escape($name)."' "
                        . "ORDER BY v.`value` ASC";
                
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        if ($config == FALSE) :
                                $result             = $query->result();
                        else :
                                $result             = $query->result_array();
                        endif;
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_attribute_mode($id_group = null, $config = FALSE) {
                if ( empty($id_group) ) {
                        return FALSE;
                }
                
                $sql = 'SELECT DISTINCT '
                        . "m.selection_mode as selection_mode "
                        . "FROM ebay_attributes a "
                        . "LEFT JOIN ebay_attributes_mode m on a.id_selection_mode = m.id_selection_mode "
                        . "WHERE a.id_group = '".$id_group."'";
                
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        if ($config == FALSE) :
                                $result             = $query->result();
                        else :
                                $result             = $query->result_array();
                        endif;
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }

        function get_carrier($site = '', $where = array()) {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('*');
                $this->db->from('ebay_shipping');
                (!empty($where)) ? $this->db->where($where) : '';
                $this->db->where('id_site', $site);

                $query                                          = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_sites($site = '') {
                $this->db->select('*');
                $this->db->from('ebay_sites');
                isset($site) ? $this->db->where('id_site', $site) : '';

                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }

        function get_level_sites($site = '') {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('MAX(level) AS level');
                $this->db->from('ebay_categories_sites');
                isset($site) ? $this->db->where('id_site', $site) : '';

                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }

        function get_category_max_date() {
                $this->db->select('MAX(date_add) AS Date');
                $this->db->from('ebay_categories');
                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result_array();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }
        
        function get_carrier_country($site = '') {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('*');
                $this->db->from('ebay_shipping_country');
                $this->db->where('id_site', $site);

                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_attributes($where = '') {
                $this->db->select('*');
                $this->db->from('ebay_attributes');
                if ( !empty($where) ) :
                        foreach ( $where as $key => $value ) {
                                $this->db->where($key, $value);
                        }
                endif;

                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_selected($table = '', $where = '') {
                $this->db->select('*');
                $this->db->from($table);
                if ( !empty($where) ) :
                        foreach ( $where as $key => $value ) {
                                $this->db->where($key, $value);
                        }
                endif;

                $query                              = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                     = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
        
        function get_sql($sql = '', $config = FALSE) {
                if ( !isset($sql) ) {
                        return FALSE;
                }
                $query                              = $this->db->query($sql);
                if ( $query->num_rows > 0 ) {
                        if ($config == FALSE) :
                                $result             = $query->result();
                        else :
                                $result             = $query->result_array();
                        endif;
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_packages_from_site($id_site = null, $marketplace_id = null) {
                if ( (!isset($id_site) && empty($id_site)) ||  (!isset($marketplace_id) && empty($marketplace_id)) ) : 
                        return Null;
                endif;
                $this->db->select('id_offer_sub_pkg as id_packages');
                $this->db->where('id_site_ebay', $id_site);
                $this->db->where('id_offer_pkg', $marketplace_id);
                $this->db->where_in('offer_pkg_type', array('0', '1'));
                $query                                          = $this->db->get('offer_price_packages');

                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return current($result[0]);
                }
                return FALSE;
        }
        
        public function get_data_packages_from_site($id_site = null, $marketplace_id = null) {
                if ( (!isset($id_site) && empty($id_site)) ||  (!isset($marketplace_id) && empty($marketplace_id)) ) : 
                        return Null;
                endif;

                $this->db->where('id_site_ebay', $id_site);
                $this->db->where('id_offer_pkg', $marketplace_id);
                $this->db->where_in('offer_pkg_type', array('0', '1'));
                $query                                          = $this->db->get('offer_price_packages');

                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }
         
        public function get_configuration_name_value($username = null, $user_id = null) {
                if ( !isset($username) || empty($username) || !isset($user_id) || empty($user_id) ) : 
                        return Null;
                endif;

                $this->db->where('value', $username);
                $this->db->where('name', 'EBAY_USER');
                $this->db->where('id_customer <>', $user_id);
                $query                                          = $this->db->get('configuration');

                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return $query->num_rows;
                }
                return 0;
        }
         
        public function get_category_root($site = null) {
                if ( !isset($site) || !is_numeric($site) ) {
                        return FALSE;
                }
                $this->db->select('*');
                $this->db->from('ebay_categories c');
                $this->db->join('ebay_categories_sites s', 'c.id_category = s.id_category AND c.id_site = s.id_site');
                $this->db->where('c.is_root_category', "1");
                $this->db->where('s.id_site', $site);

                $query                                          = $this->db->get();
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_postalcode_valid($iso_code = null, $zip = null) {
                if ( !isset($iso_code) || empty($iso_code) || !isset($zip) || empty($zip) ) {
                        return FALSE;
                } 
                $this->db->where(array('iso_code' => $zip, 'zip' => $iso_code));
                $this->db->limit('1');
                $query = $this->db->get('postal_code');
                if ( $query->num_rows > 0 ) {
                        $query->free_result();
                        return TRUE;
                }
                return FALSE;
        }
         
        public function get_postalcode_real($id_site = null) {
                if ( !isset($id_site) && empty($id_site) ) {
                        return FALSE;
                }
                $this->db->where(array('id_site' => $id_site));
                $query = $this->db->get('ebay_code');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }
         
        public function get_listing_duration() {
                $query = $this->db->get('ebay_duration');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_visitor_counter($id_site = null) {
                if ( !isset($id_site) && empty($id_site) ) {
                        return FALSE;
                }
                $this->db->where('id_site', $id_site);
                $query = $this->db->get('ebay_counter');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_return_within() {
                $query = $this->db->get('ebay_return');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_condition_values($id_site = null, $id_category = null) {
                if ( !isset($id_site) && empty($id_site) || (!isset($id_category) && empty($id_category)) ) {
                        return FALSE;
                }

                $sql = 'SELECT * '
                        . "FROM ebay_condition_values "
                        . "WHERE id_site = '".$id_site."' "
                        . "AND id_category IN (".$id_category.") "
                        . "AND is_enabled = '1'"
                        . "GROUP BY condition_value;";
                
                $query                              = $this->db->query($sql);
                
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_dispatch_time() {
                $query = $this->db->get('ebay_dispatch_time');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_offer_packages($where = null) {
                if ( !empty($where) ) :
                        $this->db->where($where);
                endif;
                $query = $this->db->get('offer_packages');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_offer_packages_by_name($name = null) {
                if ( !isset($name) && empty($name) ) {
                        return FALSE;
                }
                $this->db->where('name_offer_pkg', $name);
                $query = $this->db->get('offer_packages');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return current($result);
                }
                return FALSE;
        }
         
    public function getSiteFromPackages($id_packages = null, $id_marketplace = null) 
    {
        if ((!isset($id_packages) && empty($id_packages))
            || (!isset($id_marketplace) && empty($id_marketplace)) 
        ) {
            return false;
        }
        $this->db->select('id_site_ebay');
        $this->db->where('id_offer_pkg', $id_marketplace);
        $this->db->where('id_offer_sub_pkg', $id_packages);
        $this->db->where_in('offer_pkg_type', array('0', '1'));
        $query = $this->db->get('offer_price_packages');
        if ($query->num_rows > 0) {
            $result = $query->result_array();
            $query->free_result();
            return current($result);
        }
        return false;
    }
         
    public function getStringSite($id_packages = null, $id_marketplace = null) {
        if ((!isset($id_packages) && empty($id_packages)) 
            || (!isset($id_marketplace) && empty($id_marketplace)) 
        ) {
            return false;
        }
        $this->db->select('s.iso_code AS iso_code, s.NAME AS name');
        $this->db->join('ebay_sites s', 'p.id_site_ebay = s.id_site');
        $this->db->where('p.id_offer_sub_pkg', $id_packages);
        $this->db->where('p.id_offer_pkg', $id_marketplace);
        $this->db->where_in('p.offer_pkg_type', array('0', '1'));
        $query = $this->db->get('offer_price_packages p');
        if ( $query->num_rows > 0 ) {
            $result = $query->result_array();
            $query->free_result();
            return current($result);
        }
        return false;
    }
         
        public function get_ebay_payment($id_site = null) {
                if ( (!isset($id_site) && empty($id_site)) ) {
                        return FALSE;
                }
                $this->db->select('s.payment_method AS payment_method, s.payment_name AS payment_name, s.priority');
                $this->db->join('ebay_payment_status s', 'p.payment_method = s.payment_method');
                $this->db->where('p.id_site', $id_site);
                $this->db->where('s.is_enabled', 1);
                $this->db->order_by('s.payment_name');
                $query = $this->db->get('ebay_payment_method p');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
         
        public function get_ebay_features($id_site = null, $id_categories = null) {
                if ( (!isset($id_site) && empty($id_site)) || (!isset($id_categories) && empty($id_categories)) ) {
                        return FALSE;
                }
                $this->db->select('id_category, is_enabled');
                $this->db->where('id_site', $id_site);
                $this->db->where_in('id_category', $id_categories);
                $query = $this->db->get('ebay_categories_features');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }

        public function get_ebay_parent_list($id_site = null) {
                if ( (!isset($id_site) && empty($id_site)) ) {
                        return FALSE;
                }
                $id_site=(int)$id_site;
                $query = $this->db->query('SELECT DISTINCT id_parent, id_site FROM ebay_categories where id_site = "'.$id_site.'"');
                if ( $query->num_rows > 0 ) {
                        $result                                 = $query->result_array();
                        $query->free_result();
                        return $result;
                }
                return FALSE;
        }
       
        public function get_ebay_categories_not_leaf($id_site = -1){
        	$result = array();
        	$this->db->select('id_parent');
                $this->db->where('id_site', $id_site);
                $this->db->group_by ('id_parent');
        	$query = $this->db->get('ebay_categories');
        	if ( $query->num_rows > 0 ) {
        		$q = $query->result_array();
        		$query->free_result();
        		
        		foreach ($q as $r){
        			$result[] = $r['id_parent'];
        		}
        		return $result;
        	}
        }
       
        public function get_ebay_message_document($class = null, $function = null, $iso_code = null) { 
                if ( (!isset($class) && empty($class)) || (!isset($function) && empty($function)) || (!isset($iso_code) && empty($iso_code)) ) {
                        return FALSE;
                }
                $result = array();
                $this->db->select(array('message_problem', 'message_cause'));
                $this->db->where('message_name', strtolower($class) . "_". strtolower($function));
                $this->db->where('message_language', $iso_code);
                $query = $this->db->get('message');
                if ( $query->num_rows > 0 ) {
                        $q = $query->result_array();
                        $query->free_result();

                        foreach ($q as $r) {
                                $result['message_text'] = $r['message_problem'];
                                $result['message_link'] = $r['message_cause'];
                        }
                        return $result;
                }
                return $result;
        }
}

