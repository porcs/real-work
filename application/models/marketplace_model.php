<?php

class Marketplace_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getMarketplaceOffer($name = null) {
        $sql = "SELECT * FROM offer_packages ";
        if (isset($name) && !empty($name)) {
            $name = $this->db->escape_str($name);
            $sql .="WHERE name_offer_pkg = '$name' ";
        } else {
            $sql .=" WHERE is_marketplace = 1 AND status_offer_pkg = 'active' ";
        }
        $sql .= "ORDER BY sort_offer_pkg ASC";

        return $this->db->query($sql)->result_array();
    }

    public function getMarketplaceRegion($id_region = null) {
        $sql = "SELECT * FROM region ";
        if (isset($id_region) && !empty($id_region)) {
            $id_region = $this->db->escape_str($id_region);
            $sql .= "WHERE id_region = '$id_region' ";
        }

        $sql .= "ORDER BY name_region ASC";

        return $this->db->query($sql)->result_array();
    }

    public function getSubMarketplaceOffer($id_offer_pkg, $id_region = null) {
        $region = '';

        if (isset($id_region) && $id_region != null) {
            $id_region = $this->db->escape_str($id_region);
            $region = " AND opp.id_region = '" . $id_region . "' ";
        }
        $id_offer_pkg = (int) $id_offer_pkg;
        $sql = "SELECT * FROM offer_sub_packages sub ";
        $sql .= "LEFT JOIN offer_price_packages opp ON (sub.id_offer_sub_pkg = opp.id_offer_sub_pkg) and opp.offer_pkg_type != '3'";
        $sql .= "WHERE opp.id_offer_pkg = '{$id_offer_pkg}'" . $region . " AND sub.sort_offer_sub_pkg != 0 AND opp.status_offer_price_pkg = 'active' ";

        return $this->db->query($sql)->result_array();
    }

    public function getSubMarketplaceOfferSet($id_offer_pkg=array()) {
        
        if(empty($id_offer_pkg))return array(); 
        $sql = "SELECT * FROM offer_sub_packages sub ";
        $sql .= "LEFT JOIN offer_price_packages opp ON (sub.id_offer_sub_pkg = opp.id_offer_sub_pkg) and opp.offer_pkg_type != '3'";
        $sql .= "WHERE opp.id_offer_pkg in ('".implode("','",$id_offer_pkg)."') AND sub.sort_offer_sub_pkg != 0 AND opp.status_offer_price_pkg = 'active' ";

        $out = $this->db->query($sql)->result_array();
        $return = array();
        foreach($out as $v){
            $return[$v['id_offer_pkg']][$v['id_offer_sub_pkg']]=$v;
        }
        return $return;
    }

    public function getMarketplaceOfferPrice($id_offer_pkg, $id_offer_sub_pkg) {
        $id_offer_pkg = (int) $id_offer_pkg;
        $id_offer_sub_pkg = (int) $id_offer_sub_pkg;
        $sql = "SELECT * FROM offer_price_packages ";
        $sql .= "WHERE id_offer_pkg = '" . $id_offer_pkg . "' AND id_offer_sub_pkg = '" . $id_offer_sub_pkg . "' AND status_offer_price_pkg = 'active' ";
        $sql .= "LIMIT 0,1";

        return $this->db->query($sql)->result_array();
    }

    public function getMarketplaceCondition($market_id = null) {
        $add = '';
        if ($market_id != null && is_numeric($market_id))
            $add = " and id_marketplace = '$market_id' ";
        $sql = "select * from marketplace_condition where status = 'enable' $add order by id asc";
        return $this->db->query($sql)->result_array();
    }

    public function getMarketplaceConditionByID($id_cond, $market_id = null) {
        $id_cond = (int) $id_cond;
        $where = '';
        if (!empty($market_id) && $market_id == 6) {
            $where = ' and id_marketplace = 6  order by id asc';
        }
        $sql = "select * from marketplace_condition where status = 'enable' and id = '$id_cond' $where";
        $res = $this->db->query($sql)->result_array();
        return array('id_marketplace' => $res[0]['id_marketplace'], 'condition_value' => $res[0]['condition_value']);
    }

    public function getMarketplaceMenu($id_user, $sub = false) {
        $id_user = (int) $id_user;
        $sql = " 
		SELECT
		    upd.id_package,
		    opp.id_offer_pkg,
		    opp.id_region,
		    r.name_region,
		    opp.id_offer_sub_pkg,
		    osp.title_offer_sub_pkg,
		    opp.ext_offer_sub_pkg,
		    opp.domain_offer_sub_pkg,
		    opp.currency_offer_price_pkg,
		    op.name_offer_pkg,
		    es.name AS `name`,
		    opp.comments AS comments,
		    opp.id_site_ebay AS id_site_ebay,
		    opp.status_offer_price_pkg,
		    opp.sub_marketplace,
		    opp.iso_code
		FROM user_package_details upd
		LEFT JOIN offer_price_packages opp ON (	opp.id_offer_price_pkg = upd.id_package )
		LEFT JOIN offer_packages op ON (	op.id_offer_pkg = opp.id_offer_pkg )
		LEFT JOIN offer_sub_packages osp ON ( osp.id_offer_sub_pkg = opp.id_offer_sub_pkg )
		LEFT JOIN ebay_sites es ON (	es.id_site = opp.id_site_ebay ) 
		LEFT JOIN region r ON (opp.id_region = r.id_region) ";

        if ($sub)
            $sql .= " WHERE opp.id_offer_sub_pkg IS NOT NULL ";
        else
            $sql .= " WHERE opp.id_offer_sub_pkg IS NULL ";

        $sql .= " AND upd.id_users = '" . $id_user . "' AND opp.status_offer_price_pkg = 'active' "
                . "ORDER BY opp.id_region ASC, opp.id_offer_pkg ASC, opp.id_offer_sub_pkg ASC ";

        return $this->db->query($sql)->result_array();
    }

    public function setUserPackageDetail($arrUserPkgDetail) {
        return $this->db->replace('user_package_details', $arrUserPkgDetail);
    }

    public function deleteUserPackageDetail($id_user, $date_time, $type = null) {
        if (isset($date_time) && !empty($date_time)) {
            $id_user = (int) $id_user;
            $this->db->where('id_users', $id_user);
            $this->db->where('created_date_upkg_detail <', $date_time);

            if ($type != null)
                $this->db->where('option_package', $type);

            return $this->db->delete('user_package_details');
        }
    }

    public function getIdMarketplaceByName($marketplace) {
        $marketplace = $this->db->escape_str($marketplace);
        $sql = "SELECT * FROM offer_packages WHERE LOWER(name_offer_pkg) = LOWER(" . trim($marketplace) . ") AND status_offer_pkg = 'active' ";
        return $this->db->query($sql)->result_array();
    }

    public function getIdSiteByExt($ext) {
        $ext = $this->db->escape_str($ext);
        $sql = "SELECT * FROM offer_price_packages WHERE ext_offer_sub_pkg = '$ext' AND status_offer_price_pkg = 'active' ";
        return $this->db->query($sql)->result_array();
    }

    public function getOfferPricePackages($id_offer_pkg, $comment) {
        $comment = $this->db->escape_str($comment);
        $id_offer_pkg = (int) $id_offer_pkg;
        $sql = "SELECT * FROM offer_price_packages WHERE id_offer_pkg = '$id_offer_pkg' AND comments = '$comment' AND status_offer_price_pkg = 'active' AND offer_pkg_type != '3' ORDER BY id_offer_sub_pkg ";
        return $this->db->query($sql)->result_array();
    }

    public function getSubMarketplaceOfferGeneral($id_marketplace, $sub_marketplace) { // use in select marketplace general but now not use controllers/marketplace.php/get_general_country

        if (!isset($id_marketplace) || !isset($sub_marketplace)) {
            return;
        }

        $sql = "SELECT * FROM offer_sub_packages sub ";
        $sql.= "LEFT JOIN offer_price_packages opp ON (sub.id_offer_sub_pkg = opp.id_offer_sub_pkg) and opp.offer_pkg_type != '3' ";
        $sql.= "LEFT JOIN offer_sub_marketplace osm ON opp.sub_marketplace = osm.id_offer_sub_marketplace ";
        $sql.= "WHERE opp.id_offer_pkg = $id_marketplace AND sub.sort_offer_sub_pkg != 0 AND opp.status_offer_price_pkg = 'active' AND osm.id_offer_sub_marketplace = $sub_marketplace ";
        $sql.= "ORDER BY sort_offer_sub_pkg ";

        return $this->db->query($sql)->result_array();
    }

    public function getSubMarketplaceName($sub_marketplace) { // get name in use hook_marketplace, hook_breadcrumb, controller/marketplace/configuration

        if (!isset($sub_marketplace)) {
            return;
        }

        $sql = "SELECT name_marketplace FROM offer_sub_marketplace WHERE id_offer_sub_marketplace = '$sub_marketplace' ";
        return isset($this->db->query($sql)->row()->name_marketplace) ? $this->db->query($sql)->row()->name_marketplace : '';
    }
    
    public function getMarketplaceConditionByMirakl($market_id = '', $sub_marketplace = '') {
        $condition = '';
        if(empty($market_id) || empty($sub_marketplace)){
            return;
        }
        
        $condition = " and id_marketplace = '$market_id' AND sub_marketplace = $sub_marketplace ";
         
        $sql = "select * from marketplace_condition where status = 'enable' $condition order by id asc";
        return $this->db->query($sql)->result_array();
    }

}
