<?php

class my_feed_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    function get_token($id_customer) 
    {
        $token = array();
        $this->db->select('user_code');
        $this->db->where(array('id' => $id_customer) );
        $query = $this->db->get('users');
        $rows = $query->result();
        
        foreach($rows as $row)
            $token = $row->user_code;
        
        if(trim($token)==''){
            $token = mt_rand(0,999999999);
            $this->set_token($token,$id_customer);
        }
        
        return $token;
    }
    function get_mfa_info($id_customer){
        $info = array();
        $this->db->select(array('mfa_enable','mfa_secret_key' ));
        $this->db->where(array('id' => $id_customer) );
        $query = $this->db->get('users');
        $rows = $query->result_array(); 
        if(!empty($rows[0])){
            $info = $rows[0];
        }
        return $info;
    }
    function update_mfa_info($id_customer,$secret_key=null,
        $enable=null,$status_case=null){
        if(!empty($secret_key)){
            $data['mfa_secret_key'] = $secret_key;
        }
        if(isset($enable)){
            $data['mfa_enable'] = $enable;
        }
        if(empty($data))return false;
        $where = array('id' => $id_customer);
        if(isset($status_case)){
            $where['mfa_enable'] = (int)$status_case;
        }
         $out = $this->db->update('users', $data,$where  );
//         $this->db->last_query();exit;
         return $out;
    }
    
    
    
    function update_mfa_status($id_customer,$status_case=0){ 
        $where = array('id' => $id_customer);
        if(isset($status_case)){
            $data['mfa_enable'] = (int)$status_case;
        }
         $out = $this->db->update('users', $data,$where  );
         return $out;
    }
    
    function update_mfa_status_users(){
        $info = array();
        $this->db->select(array('user_email','mfa_secret_key'));
        $this->db->where(array('mfa_enable' => 1) );
        $query = $this->db->get('users');
        $rows = $query->result_array(); 
        foreach($rows as $r){
            $info[$r['user_email']] = trim(base64_encode($r['mfa_secret_key']),'=');
        } 
        $target_file = USERDATA_PATH.'mfa_activate_users.json';
        file_put_contents($target_file,json_encode($info));
        $target_file = dirname(__FILE__).'/../../script/log/mfa_activate_users.json';
        file_put_contents($target_file,json_encode($info));
    }
    
    function get_mfa_status_users(){
        $out = array();
        $target_file = USERDATA_PATH.'mfa_activate_users.json';
        if(file_exists($target_file)){
            $out = json_decode(file_get_contents($target_file),true);
        }
        return $out;
    }
    
    function update_store_user_info() 
    {
        $token = array();
        $this->db->select(array('user_email','id',
            'user_status','user_provider','mfa_enable'));
        $query = $this->db->get('users');
        $rows = $query->result_array();
        $out = array();
        foreach($rows as $user){ 
            $key = $user['user_email'];
            $type = -1;
            if($user['user_status']==1){
                $type=0;
            }
            if($user['user_status']==1&&$user['mfa_enable']==1){
                $type=1;
            }
            if(!empty($user['user_provider']) == 'Facebook'){
                $type=2;
            }
            if(!empty($user['user_provider']) == 'Gmail'){
                $type=3;
            }
            $out[$key]= $type;
        } 
        if(!file_exists(dirname(__FILE__).'/../../script/log/')){
            mkdir(dirname(__FILE__).'/../../script/log/',0775);
        }
        $target_file = dirname(__FILE__).'/../../script/log/users_info4fbstore.json';
        file_put_contents($target_file,json_encode($out));
    }
    function check_exists_user_email($email){
        $target_file = dirname(__FILE__).'/../../script/log/users_info4fbstore.json';
        $out = json_decode(file_get_contents($target_file),true);
        if(!empty($out)){
            if(isset($out[$email])){
                return true;
            }
        }
        return false;
        
    }
    
    
    function set_token($id_token, $id_customer) 
    {
        $data['user_code'] = $id_token;
        
        $result = $this->db->update('users', $data, array('id' => $id_customer) );
        
        if($result) 
            return TRUE;
        
        return FALSE;
    }
    
    function set_time_zone($time_zone, $id_customer) 
    {
        $data['user_time_zone'] = $time_zone;
        
        $result = $this->db->update('users', $data, array('id' => $id_customer) );
        
        if($result) 
            return TRUE;
        
        return FALSE;
    }
    
    function get_general($id_customer=null) 
    {
        $mainconfiguration = array();
        $this->db->where_in( 'name' , array('FEED_BIZ',
            'FEED_MODE','FEED_SHOP_INFO','FIRST_CONF') );
        if($id_customer!==null){
            $this->db->where(array('id_customer' => $id_customer) );
        }
        $query = $this->db->get('configuration');
        $rows = $query->result();
        
        foreach($rows as $row)
        {
            $mainconfiguration[$row->name]['value'] = $row->value;
            $mainconfiguration[$row->name]['config_date'] = $row->config_date;
        }
        
        return $mainconfiguration;
    }
    
    function get_categories($id_customer=null) {
        $mainconfiguration = array();
        $this->db->where_in( 'name' , array('FEED_CATEGORY') );
        if($id_customer!==null){
            $this->db->where(array('id_customer' => $id_customer) );
        }
        $query = $this->db->get('configuration');
        $rows = $query->result();
        
        foreach($rows as $row)
        {
            $mainconfiguration[$row->name]['value'] = $row->value;
            $mainconfiguration[$row->name]['config_date'] = $row->config_date;
        }
        
        return $mainconfiguration;
    }
    
    function get_conj_conf($id_customer=null) 
    {
        $mainconfiguration = array();
        $this->db->like( 'name' , 'FEED_CRONJ_','after');
        if($id_customer!==null){
            $this->db->where(array('id_customer' => $id_customer) );
        }
        $query = $this->db->get('configuration');
        $rows = $query->result();
        
        foreach($rows as $row)
        {
            $mainconfiguration[$row->name]['value'] = $row->value;
            $mainconfiguration[$row->name]['config_date'] = $row->config_date;
        }
        
        return $mainconfiguration;
    }
        
    function set_general($data) 
    {
        //echo '<pre>' . print_r($data, true) . '</pre>';
        $this->db->where(array('id_customer' => $data['id_customer'] ,
            'name' => $data['name']) );
        $this->db->get('configuration');
        
        if($this->db->affected_rows() == 1)
            $result = $this->db->update('configuration', 
                $data,
                array('id_customer' => $data['id_customer'] ,
                    'name' => $data['name']) );
        else
            $result = $this->db->insert('configuration', $data);
        
        if($result) return TRUE;
        
        return FALSE;

    }
    
    function delete_general($data) 
    {
        $this->db->where(array('id_customer' => $data['id_customer'] ,
            'name' => $data['name']));
        $result = $this->db->delete('configuration');
        
        if($result) return TRUE;
        
        return FALSE;

    }
    
    function get_default_mode($id_customer) 
    {
        $mainconfiguration = array();
        $this->db->where(array('id_customer' => $id_customer,
            'name' => 'FEED_MODE') );
        $query = $this->db->get('configuration');
        $rows = $query->result();
        
        foreach($rows as $row)
            $mainconfiguration = unserialize(base64_decode ($row->value));
        
        return $mainconfiguration;
    }
    function using_expert_mode($id_customer){
        $mainconfiguration = array();
        $this->db->where(array('id_customer' => $id_customer,
            'name' => 'FEED_MODE') );
        $query = $this->db->get('configuration');
        $rows = $query->result();
        
        foreach($rows as $row)
            $mainconfiguration = unserialize(base64_decode ($row->value));
        if($mainconfiguration['mode']==2){
            return true;
        }
        return false;
    }
    
    function check_exist_domain($id_customer,$domain){
//        $this->db->where('id_customer !=' , $id_customer);
//        $this->db->where('value', $domain) ;
//        $this->db->like('name','FEED_BASE_URL_');
//        $query = $this->db->get('configuration'); 
//        if($this->db->affected_rows() >= 1){
//            return true;
//        }
        return false;
    }
    
    function set_mode($data) 
    {
        $this->db->where(array('id_customer' => $data['id_customer'] ,
            'name' => $data['name']) );
        $this->db->get('configuration');
        
        if($this->db->affected_rows() == 1)
            $result = $this->db->update('configuration', $data,
                array('id_customer' => $data['id_customer'] ,
                    'name' => $data['name']) );
        else
            $result = $this->db->insert('configuration', $data);
        
        if($result) return TRUE;
        
        return FALSE;

    }
//    function get_user_conj_feed_active(){
//        $this->db->where('user_status','1')
//                ->where('user_cronj_status','1')
//                ->join('configuration','users.id = configuration.id_customer')
//                ->where('name','FEED_BIZ')
//                ->order_by('id','asc');
//        $result = $this->db->get('users'); 
//        $out = $result->result_array();
//         
//        foreach($out as $k=> $r){
//            $val = unserialize(base64_decode($r['value']));
//            if(!(isset($val['verified'])&& $val['verified']!='')){
//                unset($out[$k]); 
//            }
//        } 
//        return $out;
//    }
    function check_verified_import($id){
        $this->db->where(array('id_customer' => $id , 'name' => 'FEED_BIZ') );
        $result = $this->db->get('configuration');
        if($this->db->affected_rows() == 1){
            $q = $result->result_array();
            $out = unserialize(base64_decode($q[0]['value']));
            if(isset($out['verified'])&& $out['verified']!=''){
                return true;
            } 
        }        
        return false;
    }
    function unverify_import($id){
        $this->db->where(array('id_customer' => $id , 'name' => 'FEED_BIZ') );
        $result = $this->db->get('configuration');
        if($this->db->affected_rows() == 1){
            $q = $result->result_array();
            $out = unserialize(base64_decode($q[0]['value']));
            if(isset($out['verified'])&& $out['verified']!=''){
                  $out['verified'] = '';
                  $today  = date('Y-m-d H:i:s', time());
                  $ex = base64_encode(serialize($out));
                  $data = array('id_customer'=>$id,'name'=>'FEED_BIZ',
                      'value'=>$ex,'config_date'=>$today);
                  $this->set_general($data);
                  return true;
            } 
        }        
        return false;
    }
    function check_export_status($id,$site,$ext){
        $this->db->where(array('id_customer' => $id ,'type'=>'export',
            'site' => $site,'ext'=>$ext) );
        $result = $this->db->get('cronjob_status');
        if($this->db->affected_rows() == 1){
            $q = $result->result_array();
            return $q[0]['status'];
        }else{
            return 'default';
        }
    }
    function check_last_import($id, $key = 'import_', $min = 60, $user_name = '') {

        if ($user_name == '') {
            $this->db->where(array('user_id' => $id))
                    ->like('history_action', $key, 'after')
                    ->limit(1)
                    ->order_by('history_date_time', 'desc');
        } else {
            $this->db->where(array('user_id' => $id, 'user_name' => $user_name))
                    ->like('history_action', $key, 'after')
                    ->limit(1)
                    ->order_by('history_date_time', 'desc');
        }
        
        $result = $this->db->get('histories');
        $q = $result->result_array();

        // check history status : fail return true
        if (isset($q) && !empty($q))
            $history_data = json_decode($q[0]['history_data']);

        if (isset($history_data->status) && $history_data->status == 'fail')
            return true;
        
        if(isset($history_data->transaction) && $history_data->transaction == 0){
            return true;
        }

        if ($this->db->affected_rows() == 1) {
            $time = strtotime($q[0]['history_date_time']);
            $threshold = strtotime("-$min mins");

            if ($threshold < $time) {
                return $time - $threshold;
            }
        }
        return true;
    }
    function get_next_time_action_multi_key($id,$key=array()){
        if(empty($key) || !is_array($key))return;
        $key_list = array();
        $key_size = sizeof($key);
        $time_return = array();
//        $this->db->select(array('history_data','history_action'));
//        $this->db->where(array('user_id' => $id))
//                ->where("history_date_time >= SUBDATE(CURDATE(),1) ") 
//                
////                ->not_like('history_action' , 'import_order','after')
//                
//                ->order_by('history_date_time','desc');
//        $sql=array();
//        foreach($key as $k){
//             $sql[]=" history_action like '$k' ";
//             $key_list[$k] = '/'.str_replace('%','*',$k).'/';
//        }
//        $this->db->where(" (".implode(' or ',$sql)." )", NULL, FALSE);
        $add1=array();
        $add2=array();
        foreach($key as $i => $kk){
            $key_list[$kk] = '/'.str_replace('%','*',$kk).'/';
            $i=$i+1;
            $add1[]= "(select id from histories where history_action like '{$kk}' "
            . " and `user_id` =  '{$id}' "
            . " ORDER BY `history_date_time` desc limit 1)  ";
             
        } 
        $sql = "SELECT `history_data`, `history_action` "
            . " FROM  histories   as h , (".implode(' UNION ALL ',$add1)." ) as k
                WHERE `user_id` =  '{$id}'  AND "
                . " h.id = k.id ORDER BY `history_date_time` desc";
        $result = $this->db->query($sql);
        $q = $result->result_array(); 
//        echo $this->db->last_query();
            if(sizeof($key_list)>0){
                foreach($q as $data){
                    $action = $data['history_action'];
                    $pass=false; 
                    foreach($key_list as $kk => $kc){
                        if(preg_match($kc, $action)){
                            $pass = $kk;
                            break;
                        } 
                    }
                    if($pass==false || (!empty($time_return[$pass])&&$pass!==false)){
                        continue;
                    }
                    $val=-1;
                    $out = json_decode($data['history_data'],true);
                    if(isset($out['next_time'])){
                        $feed_offer = $out['next_time'];
                        if($feed_offer > time()){
                            $feed_offer = $feed_offer - time();
                       }else{
                           $feed_offer = 0;
                       }
                       $val= $feed_offer;
                    }  
//                    var_dump($val);var_dump($pass);
                    $time_return[$pass]=$val;
                    
                    if(sizeof($time_return)==$key_size){
                        break;
                    }
                }
                
            }
        foreach($key as $k){ 
            if(empty($time_return[$k])){
                $time_return[$k]=-1;
            }
        } 
        return $time_return; 
    }
    function get_next_time_action($id,$key='import_offer'){
        $this->db->select(array('history_data'));
        $this->db->where(array('user_id' => $id))
                ->where("history_date_time >= SUBDATE(CURDATE(),1) ") 
                 ->where("history_action like '$key%'") 
//                ->not_like('history_action' , 'import_order','after')
                ->limit(1)
                ->order_by('history_date_time','desc');
        $result = $this->db->get('histories');
        $q = $result->result_array();
//        echo $this->db->last_query();
//         echo $this->db->affected_rows();exit;
        // check history status : fail return true
        
         
        if($this->db->affected_rows() == 1){
            $out = json_decode($q[0]['history_data'],true);
            if(isset($out['next_time'])){
                $feed_offer = $out['next_time'];
                if($feed_offer > time()){
                    $feed_offer = $feed_offer - time();
               }else{
                   $feed_offer = 0;
               }
               return $feed_offer;
            }else{
                return -1;
            }
        }else{
            return -1;
        }
    }
    function check_history_key($id,$key='import_'){
        $this->db->where(array('user_id' => $id))
                ->like('history_action' , $key,'after') 
                ->limit(1)
                ->order_by('history_date_time','desc');
        $result = $this->db->get('histories');
        $q = $result->result_array();
        if ($result->num_rows() > 0){
            return TRUE;
        } 
            return false;
         
        
    }
    function get_next_action($id,$key = 'import_',$min = 15){
        $this->db->where(array('user_id' => $id))
                ->like('history_action' , $key,'after') 
                ->limit(1)
                ->order_by('history_date_time','desc');
        $result = $this->db->get('histories');
        $q = $result->result_array();
         
        // check history status : fail return true
        if(isset($q) && !empty($q))
            $history_data = json_decode($q[0]['history_data']);  
        
        $min_no=2;
        if($this->db->affected_rows() == 1){
            $time =  strtotime($q[0]['history_date_time']. " +{$min} minutes") ;
            $cur_time = strtotime("-2 minutes");
            $run_time = $time; 
//            echo $this->db->last_query().'<br>';
//            echo $key.' '.$cur_time.' '.date('r',$cur_time) . ' '. $time.' '.date('r',$time).' '.$q[0]['history_date_time']. " +{$min} minutes".'<br>';
            if($cur_time > $time){
                
;                $run_time = strtotime("+{$min_no} minutes"); 
            }
        }else{
//            echo '2x<br>';
            $run_time = strtotime("+{$min_no} mins");
        }
        return $run_time;
    }
    function get_current_shop_name($id){
        $this->db->where(array('id_customer' => $id , 'name' => 'FEED_SHOP_INFO') );
        $result = $this->db->get('configuration');
        if($this->db->affected_rows() == 1){
            $q = $result->result_array();
            $v = unserialize(base64_decode($q[0]['value']));
            return isset($v['name'])?$v['name']:false;
        }
        
        return false;
    }
    
    /*function get_histories($id,$key = 'import_'){
        $this->db->where(array('user_id' => $id))
               ->like('history_action' , $key) 
               ->order_by('history_date_time','desc');
        $result = $this->db->get('histories');
        return $result->result_array();
         
    }*/
    public function get_histories($id, $key = 'import_', $limit=null,
        $start = null, $order_by = null, $keywords = null, $num_rows = false){
        $list = array();
        $key = $this->db->escape_like_str($key);
        $sql  = "SELECT * FROM histories WHERE user_id = '". $id .
            "' AND history_action LIKE '%" .$key. "%' ";
           
        if(isset($keywords) && !empty($keywords)){
                $keywords = $this->db->escape_like_str($keywords);
                $sql  .= "AND (history_date_time LIKE '%" . $keywords . "%' )";
        }
        if(isset($order_by) && !empty($order_by)){
                $order_by = $this->db->escape_str($order_by);
                $sql  .= "ORDER BY " . $order_by ." ";
        }  else {
                $sql  .= "ORDER BY history_date_time DESC ";            
        }
        //echo $start.' -> '.$limit;
        if(isset($limit) && !empty($limit)){
            $limit = (int)$limit;
            if(isset($start) && !empty($start)){
                $start = (int)$start;
                $sql  .= "LIMIT " . $start . "," . $limit;
            }  else {
                $sql  .= "LIMIT " . $limit;
            }
        } 
        
        
        if($num_rows){
            $result = $this->db->query($sql);
            $row =  $result->num_rows();
            return $row;
            
        } else {
            $get =$this->db->query($sql);
            $result = $get->result();            
            return $result;
        }
         
    }
    function insert_last_import($id,$type='feed',$user_name='system',$other=array()){
        #if($id=='328')return TRUE;
        $shop = $this->get_current_shop_name($id);
        $data=array(
            'user_id'=>$id,
            'user_name'=>$user_name,
            'history_action'=>'import_'.$type,
            'history_date_time'=>date('Y-m-d H:i:s'),
            'history_table_name'=>$shop,
            'history_data'=>  json_encode($other),
        );
        $result = $this->db->insert('histories', $data); 
         
        if($result) return 1; 
        return 0;
    }
    function insert_last_export($id,$site,$type='feed',
        $user_name='system',$other=array()){
        
        if($id=='328')return TRUE;
        
        $shop = $this->get_current_shop_name($id);
        $data=array(
            'user_id'=>$id,
            'user_name'=>$user_name,
            'history_action'=>'export_'.$site.'_'.$type,
            'history_date_time'=>date('Y-m-d H:i:s'),
            'history_table_name'=>$shop,
            'history_data'=>  json_encode($other),
        );
        $result = $this->db->insert('histories', $data); 
         
        if($result) return TRUE; 
        return FALSE;
    }
    
    function update_history($user_id, $site, $action, $history_data=null){
                 
        $this->db->where(array('user_id' => $user_id,
            'history_action' => 'export_' . $site . '_' . $action ))
                ->limit(1)
                ->order_by('history_date_time','desc');
        $result = $this->db->get('histories');
        $rows = $result->result_array();
        
        $update = null;
        if(isset($rows) && !empty($rows) && $rows > 0)
        {
            $history_id = $rows[0]['id'];
	    
	    if(!isset($history_data) || empty($history_data))
		$history_data = json_encode(array('status'=>'fail'));
	    	    
            $data = array('history_data'=> $history_data);        
            $where = array('id' => $history_id);        
            $update = $this->db->update('histories', $data, $where);
        }
        
        if($update) return TRUE; 
        return FALSE;
    }
    
    function get_json_for_import($id){
        $config_data = $this->get_general($id);
        
        foreach ($config_data as $key => $value)
            if(strtolower($key) != "feed_mode")
                $data[strtolower($key)] = unserialize(str_replace(' ','_',base64_decode($value['value'])));
        $data['feed_token']=$this->get_token($id);        
        $data['feed_biz']['user_id'] = $id;
        if(isset($data['feed_biz']['verified'])&&$data['feed_biz']['verified']!='')
        return json_encode($data);
        return array();
    }
    function dateDiff($time1, $time2, $precision = 1,$lang ='en') {
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        } 
        $expire=false;
        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
                $expire=true;
        }
        $intervals = array('year','month','day','hour','minute','second'); 
        $intervals_txt = array('year','month','day','hour','min','sec');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $k=>$interval) {
            // Set default diff to 0
            $diffs[$k] = 0;
                if($expire)continue;
            // Create temp time from time1 and interval
            $ttime = strtotime("+1 " . $interval, $time1);
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                $time1 = $ttime;
                $diffs[$k]++;
                // Create new temp time from time1 and interval
                $ttime = strtotime("+1 " . $interval, $time1);
            }
        }

        $count = 0;
        $times = array(); 
        foreach ($diffs as $interval => $value) { 
            if ($count > $precision-1) {
                break;
            }
            if ($value > 0) { 
                if ($value != 1 && $lang=='en') {
                    $intervals_txt[$interval] .= "s";
                } 
                $times[] = '<span>'.$value . "</span> " .$intervals_txt[$interval];
                $count++;
            }
        }
        // Return string with times
        return implode(", ", $times);
    }
//     public function getUserUpdateMarketCron($market = 'amazon'){
//        if($market=='amazon'){
//            $sql = "select ac.*,h.history_date_time,u.user_created_on from amazon_configuration ac , histories h ,users u where "
//                    . " CONCAT('amazon',ac.ext,'_synchronize') = h.history_action "
//                    . " and  ac.active = '1' and ac.allow_automatic_offer_creation = '1' and ac.id_customer = h.user_id and u.id = ac.id_customer "
//                    . " group by ac.id_customer,id_country,id_shop ";
//            
//        }else if($market=='ebay'){
//            $sql = "SELECT
//                    c.id_customer,
//                    u.user_name,
//                    offer_price_packages.ext_offer_sub_pkg as ext,
//                    offer_price_packages.domain_offer_sub_pkg as domain,
//                    offer_price_packages.id_site_ebay as id_site
//                    from configuration c ,users u  
//                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
//                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
//                    where c.name = 'EBAY_TOKEN' and u.id = c.id_customer
//                    ";
//        } 
//        return $this->db->query($sql)->result_array();
//    }
    public function getDomainEbayByIDSite($id_site){
        if(empty($id_site)&&$id_site!=0) return array();
        $id_site = (int)$id_site;
        $sql = "select domain_offer_sub_pkg as domain , "
            . "ext_offer_sub_pkg as ext from offer_price_packages "
            . " where id_site_ebay = '$id_site' ";
        return $this->db->query($sql)->result_array();
    }
    
    public function checkShopInfo($id_customer,$id_shop,$insert=true){
        if(empty($id_shop)) return false;
        $id_customer = (int)$id_customer;
        $sql = "select * from configuration "
            . " where name like 'FEED_SHOP_INFO%' and id_customer = '$id_customer' ";
    }
    
    public function checkCurrentModuleVersion($uid){
        $sql = "select value from configuration "
            . " where name = 'FEED_BIZ_CURRENT_MODULE_VERSION' and id_customer=0";
        $d = $this->db->query($sql)->result_array();
        $current_ver = isset($d[0]['value'])?$d[0]['value']:'';
        if(empty($current_ver))return true;
        if($uid==0){
            return $current_ver; 
        }
        $sql = "select user_module_ver from  users where     id = '$uid'";
        $d = $this->db->query($sql)->result_array();
        if(!empty($d[0]['user_module_ver'])){
            return version_compare($d[0]['user_module_ver'],$current_ver)==-1?$current_ver:true;
        } 
        return $current_ver; 
    }
}

