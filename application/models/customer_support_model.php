<?php
    class customer_support_model extends CI_Model
    {
        public $support_id = 0;
        public $support_email = '';
        public $support_name = '';
        public $support_count = 0;
        public $support_get_loop = 0;
        public $support_greeting_txt ='';
        private $req_last_contact = true;
        public function __construct()
        {
            parent::__construct();
            $id=null;

            $t_id = $this->session->userdata('sem_id');

            if ($this->req_last_contact) {
                $id = $this->get_last_support_id();
            }

            if (!empty($t_id) && empty($id)) {
                $id = $t_id;
            }
            $this->support_get_loop=0;
            $this->get_support_employee($id);
        }

        public function get_support_employee($id=null)
        {
            $out = array();
            $today = date('D');
            if ($id===null) {
                $id=(int)$id;
                if ($this->support_get_loop>5) {
                    $sql = "select id,name,greeting_txt,email,contact_count,image "
                        . " from support_employee where  status = 1 "
                        . " order by   RAND() ";
                } else {
                    $sql = "select id,name,greeting_txt,email,contact_count,image "
                        . " from support_employee where avail_date like '%$today%' "
                        . " and status = 1 and online_status = 1 "
                        . " order by contact_count asc , RAND() ";
                }
            } else {
                $sql = "select id,name,greeting_txt,email,contact_count,image "
                    . " from support_employee where id = '$id' "
                    . " and avail_date like '%$today%' and status = 1 "
                    . " and online_status = 1 limit 1 ";
            }
            $result = $this->db->query($sql);
            $query = $result->result_array();
            if (isset($query[0])) {
                $out = $query[0];
                $this->support_id = $out['id'];
                $this->support_email = $out['email'];
                $this->support_name = $out['name'];
                $this->support_count = $out['contact_count'];

                $data = array();
                $data['sem_id'] = $out['id'];
                $data['sem_name'] = $out['name'];
                $data['sem_email'] = $out['email'];
                $data['sem_image'] = trim($out['image'], '/');

//                    $data['sem_info'] = $out;
		$greeting_txt_lang = $this->_unserialize($out['greeting_txt']);
                $lang = substr($this->session->userdata('language'),0,2);
                if(!empty($lang) && isset($greeting_txt_lang[$lang])){
                    $this->support_greeting_txt = $greeting_txt_lang[substr($this->session->userdata('language'),0,2)];
                }else{
                    $this->support_greeting_txt = '';
                }
                    //$this->support_greeting_txt = $out['greeting_txt'];
                unset($out['greeting_txt']);
                $this->session->set_userdata($data);
                $this->smarty->assign("support_email",  $out['email']);
                if ($id===null) {
                    $this->insert_history("main support not available");
                }
            } else {
                $this->support_get_loop++;
                $out = $this->get_support_employee();
            }
            return $out;
        }

        protected function _unserialize($data)
	{
                $data = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
                            function($match) {
                                return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                            },
                        $data );
//		$datax = @unserialize(strip_slashes($data));
//
//                if(empty($datax)){
                    $dataA = preg_replace_callback ( '!s:(\d+):"(.*?)";!',
                            function($match) {
                                return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                            },
                        $data );
//                    $data = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $data);
                    $dataA = @unserialize(stripslashes ($dataA));
                    if(!empty($dataA)){ $data= $dataA;}
//                }else{
//                    $data=$datax;
//                }
		if (is_array($data))
		{
			array_walk_recursive($data, array(&$this, '_unescape_slashes'));
			return $data;
		}

		return (is_string($data)) ? str_replace('{{slash}}', '\\', $data) : $data;
	}
        protected function _unescape_slashes(&$val, $key)
	{
		if (is_string($val))
		{
	 		$val= str_replace('{{slash}}', '\\', $val);
		}
	}

        public function get_support_employee3($id = null)
        {
            $out = array();
            $today = date('D');
       // if ($id === null) {
           // if ($this->support_get_loop > 5) {
                $last_id_support = $this->session->userdata('sem_id');
            $sql = "select * from support_employee "
                . " WHERE id NOT IN(".$last_id_support.") "
                . " order by RAND() limit 3 ";
           // } else {
          //      $sql = "select * from support_employee where avail_date like '%$today%' and status = 1 and online_status = 1 order by contact_count asc , RAND() ";
          //  }
       // } else {
           // $sql = "select * from support_employee where id = '$id' and avail_date like '%$today%' and status = 1 and online_status = 1 limit 3 ";
       // }
        $result = $this->db->query($sql);
            $query = $result->result_array();
        //for ($i = 0; $i < 3; $i++) {
           // if (isset($query[0])) {
                $out = $query;
                //$this->support_id = $out['id'];
                //$this->support_email = $out['email'];
                //$this->support_name = $out['name'];
                //$this->support_count = $out['contact_count'];
                //$this->session->set_userdata('sem_id', $out['id']);
                //$this->session->set_userdata('sem_name', $out['name']);
                //$this->session->set_userdata('sem_info', $out);
            //} else {
           //     $this->support_get_loop++;
                //$out = get_support_employee3();
            //}
        //}
        return $out;
        }
        public function get_support_rate()
        {
            $all_star = 5;
            $star_list=array();
            $user_id =(int)$this->session->userdata('id');
            $sql = "select * from support_rate where user_id = '$user_id' and support_id = '".$this->support_id."' order by id desc limit 1";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            $last_score = isset($result[0])?$result[0]['score']:0;

            for ($i=$all_star; $i>=1; $i--) {
                $star_list[]=array('score'=>$i,'selected'=>$i==$last_score);
            }
            return $star_list;
        }
        public function set_support_rate($score=0)
        {
            $hitted = $this->session->userdata('hitted');
            if (!empty($hitted)) {
                return 0;
            }
            $user_id = (int)$this->session->userdata('id');
            $sql = "select * from support_rate where user_id = '$user_id' "
                . " and support_id = '".$this->support_id."' order by id desc limit 1";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            $last_score = isset($result[0])?$result[0]['score']:0;
            $time = date('Y-m-d H:i:s');
            if ($score!=$last_score) {
                if (isset($result[0])) {
                    $hits = (int)$result[0]['hits']+1;
                    $this->db->update('support_rate',
                        array('score'=>$score,
                            'hits'=>$hits,
                            'date_updated'=>$time),
                        array('support_id'=>$this->support_id,
                            'user_id'=> $user_id));
                } else {
                    $hits=1;
                    $this->db->insert('support_rate',
                        array('score'=>$score,
                            'hits'=>$hits,
                            'date_updated'=>$time,
                            'date_created'=>$time,
                            'support_id'=>$this->support_id,
                            'user_id'=> $user_id));
                }
            }
            $this->session->set_userdata('hitted', $hits);
            return $hits;
        }
        public function inc_count_contact($condition='')
        {
            $this->db->update('support_employee',
                array('contact_count'=>$this->support_count+1),
                array('id'=>$this->support_id));
            $this->insert_history($condition);
        }
        public function insert_history($condition='')
        {
            if ($condition=='') {
                $condition='Registration';
            }
            $ip_addr = $_SERVER['REMOTE_ADDR'];
            $user_id = $this->session->userdata('id');
            $cus_email = $this->session->userdata('user_email');
            if ($user_id=='') {
                return;
            }
            $this->db->insert('support_history', array(
               'user_id'=>(int)$user_id,
               'emp_id'=>$this->support_id,
               'user_email'=>$cus_email,
               'emp_email'=>$this->support_email,
               'user_ip'=>$ip_addr,
               'create_date'=>date('Y-m-d H:i:s'),
               'condition'=>$condition
            ));
        }
        public function get_last_support_id()
        {
            $out = null;
            if ($this->session->userdata('id')!='') {
                $email = $this->session->userdata('user_email');
                $today = date('D');
                $email = $this->db->escape_str($email);
                $sql = "select * from support_history sh , support_employee sm "
                    . " where  sm.status = 1 and sh.emp_id = sm.id and "
                    . " sh.user_email = '$email' order by sh.id desc ";

                $result = $this->db->query($sql);
                $query = $result->result_array();
                foreach ($query as $s) {
                    if (strpos($s['avail_date'], $today)!==false) {
                        $out = $s['emp_id'];
                        break;
                    }
                }
            }

            return $out;
        }
        public function send_email2supporter($cus_email='', $email='')
        {
            if ($email=='') {
                $email = $this->support_email;
            }
            if ($cus_email=='' && $this->session->userdata('user_email')!='') {
                $cus_email = $this->session->userdata('user_email');
            } else {
                return false;
            }
            require_once APPPATH.'libraries/Swift/swift_required.php';

            $arrEmail = array($email);
            $f_name = 'notify_default_email.html';
            $reason = "Got new register : ".$cus_email.
                "<br>Please assist to registration and submit feed.";
            try {
                $filePath = str_replace('\\', '/', FCPATH).'assets/template/'.$f_name;
                $oriEmailTemplate = file_get_contents($filePath);
                $transport = Swift_SmtpTransport::newInstance();
                $logo = '<img src="'.str_replace('http', 'https', base_url().
                    '/nblk/images/feedbiz_logob.png').'" alt="Feed.biz">';

                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);
                $msg ='@ '.date('r').'<br>reason: '.$reason;
                if (!empty($arrEmail)) {
                    foreach ($arrEmail as $email) {
                        $email_template = $oriEmailTemplate;
                        $email_template = str_replace("{#logo}", $logo, $email_template);
                        $email_template = str_replace("{#msg}", $msg, $email_template);
                        $email_template = str_replace("{#refemail}", $email, $email_template);

                        $swift_message = Swift_Message::newInstance();
                        $swift_message->setSubject('[Sys]Feed.biz get new register!')
                                    //->setFrom(array($u_email=>$u_name))
                                    ->setFrom(array($email=>$email))
                                    ->setTo($email)
                                    ->setBody($email_template, 'text/html');

                        $mailer->send($swift_message);
                    }
                }
            } catch (Exception $ex) {
            }
        }

        public function get_error_notify()
        {
            $sql = "select * from users_error_notify where 1 order by date_add desc";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        }

        public function get_restrict_mode()
        {
            $sql = "select * from restrict_mode where status = 1 order by id desc";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        }

        public function manage_restrict_mode($action,$data)
        {
            $tb = 'restrict_mode';
            if($action=='update'){
                $result = $this->db->update($tb,
                $data,
                array('id' => $data['id']) );
            }else if($action=='add'){
                $result = $this->db->insert($tb, $data);
            }else if($action=='del'){
                $result = $this->db->update($tb ,
                array('status'=>0),
                array('id' => $data['id']) );
            }
            $list=array();
            $sql = "select * from $tb where status = 1 ";
            $query = $this->db->query($sql);
            $res = $query->result_array();
            foreach($res as $d){
                $list[$d['controller']][$d['method']][$d['additional']] =
                    array('free'=>$d['free_mode'],'pro'=>$d['pro_mode'],'adv'=>$d['advance_mode'],'man'=>$d['manager_mode'],'url'=> $d['url'] );
            }


            file_put_contents(APPPATH.'../assets/apps/users/json/restrict_mode.json',json_encode($list));



            if($result) return TRUE;
            return false;

        }

    }
