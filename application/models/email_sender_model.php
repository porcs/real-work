<?php

class email_sender_model extends CI_Model
{
    public $system_email = '';
    public $system_name = 'Feed.biz';
    public $target_email_arr = array();
    public $target_email = '';
    public $subject = '';
    public $message = '';
        
    public function __construct()
    {
        parent::__construct();
        $this->system_email = $this->config->item('admin_email');
    }
    public function addTarget($email='')
    {
        if ($email=='') {
            return;
        }
        array_push($this->target_email_arr, $email);
    }
       
    public function setSender($email='')
    {
        if ($email=='') {
            return;
        }
        $this->system_email = $email;
    }
       
    public function sendMail($email='', $subject = '', $msg = '')
    {
        if ($email=='') {
            if (sizeof($this->target_email_arr)==0) {
                if ($this->target_email=='') {
                    return;
                } else {
                    $email = $this->target_email;
                }
            } else {
                $email = $this->target_email_arr;
            }
        }
        require_once APPPATH.'libraries/Swift/swift_required.php';
                
        $arrEmail = array($email);
        try {
            $transport = Swift_SmtpTransport::newInstance();
                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);
            if (!empty($arrEmail)) {
                foreach ($arrEmail as $eml) {
                    $email_template = $msg;
                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject($subject)
                                    //->setFrom(array($u_email=>$u_name))
                    ->setFrom(array($this->system_email=>$this->system_name))
                    ->setTo($eml)
                    ->setBody($email_template, 'text/html');

                    $mailer->send($swift_message);
                }
            }
        } catch (Exception $ex) {
        }
    }
}
