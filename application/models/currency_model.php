<?php
    class currency_model extends CI_Model
    {
        public function __construct()
        {
            parent::__construct();
        }
        
        public function update_user_currency($id_user=null, $currency='EUR')
        {
            if ($id_user==null) {
                $this->session->userdata('id');
            }
            $this->db->update('users',
                array('user_currency'=>$currency),
                array('id'=>$id_user));
            $this->session->set_userdata('currency', $currency);
            return true;
        }
        public function get_user_currency($id_user=null)
        {
            if ($id_user==null) {
                $this->session->userdata('id');
            }
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('id', $id_user);
            $query = $this->db->get();
            $rs = $query->row_array();
             
            return $rs['user_currency'];
        }
        public function get_currency()
        {
            $this->db->select('*');
            $this->db->from('currency');
            $query = $this->db->get();
            $rs = $query->result_array();
            foreach ($rs as $k=>$v) {
                $arrCurrency[$v['currency']] = $v['name'];
            }

            ksort($arrCurrency);
            return $arrCurrency;
        }
    }
