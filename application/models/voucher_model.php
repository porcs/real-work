<?php
class voucher_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getGiftOption()
    {
        $sql = "SELECT opt.id_giftopt,opt.name_giftopt,opt.code_qty_giftopt,"
            . "opt.start_date_giftopt,opt.end_date_giftopt,"
            . "(SELECT count(c.id_giftcode) FROM gift_code c ";
        $sql .= "WHERE c.id_giftopt = opt.id_giftopt) AS used "
            . "FROM gift_option opt WHERE opt.status_giftopt = 'active' "
            . "ORDER BY opt.id_giftopt DESC";
        return $this->db->query($sql)->result_array();
    }
    
    public function setGiftVoucher($arrData)
    {
        return $this->db->insert('gift_option', $arrData);
    }
    
    public function getGiftOptionID($id_giftopt)
    {
        $id_giftopt = (int)$id_giftopt;
        $sql = "SELECT *,(SELECT count(c.id_giftcode) FROM gift_code c ";
        $sql .= "WHERE c.id_giftopt = opt.id_giftopt) AS used "
            . "FROM gift_option opt WHERE opt.id_giftopt = '".$id_giftopt."' "
            . "AND opt.status_giftopt = 'active' ORDER BY opt.id_giftopt DESC";
        return $this->db->query($sql)->result_array();
    }
    
    public function updateGiftOption($arrData)
    {
        $id_giftopt = $arrData['id_giftopt'];
        unset($arrData['id_giftopt']);
        
        $this->db->where('id_giftopt', $id_giftopt);
        return $this->db->update('gift_option', $arrData);
    }
    
    public function getGiftCode($id_giftopt)
    {
        $this->db->where('id_giftopt', $id_giftopt);
        $this->db->order_by('created_date_giftcode', 'DESC');
        
        return $this->db->get('gift_code')->result_array();
    }
    
    public function checkGiftUniqCode($id_giftcode)
    {
        $this->db->select('id_giftcode');
        $this->db->where('id_giftcode', $id_giftcode);
        $query = $this->db->get('gift_code')->result_array();
        
        if (!empty($query)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function setGiftCode($arrData)
    {
        return $this->db->insert('gift_code', $arrData);
    }
    
    public function deleteGiftCode($id_giftcode)
    {
        $this->db->where('id_giftcode', $id_giftcode);
        $query = $this->db->get('gift_code')->result_array();
        if ($query) {
            $id_giftopt = $query[0]['id_giftopt'];
            $this->db->where('id_giftcode', $id_giftcode);
            if ($this->db->delete('gift_code')) {
                return $id_giftopt;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    public function deleteGiftOption($id_giftopt)
    {
        $this->db->where('id_giftopt', $id_giftopt);
        $this->db->delete('gift_option');
        
        $this->db->where('id_giftopt', $id_giftopt);
        return $this->db->delete('gift_code');
    }
}
