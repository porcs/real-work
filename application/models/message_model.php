<?php
class message_model extends CI_Model
{
    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_INFO = 'info';
    public function __construct()
    {
        parent::__construct();
    }
    public function get_messages($lang = 'en')
    {
        $this->db->select('*');
        $this->db->where(array(
                'message_language' => $lang
        ));
        $query = $this->db->get('message');
        $rows = $query->result_array();
        return $rows;
    }
    public function get_message($message_code)
    {
        $message = array();
        $this->db->select('*');
        $this->db->where(array(
                'message_code' => $message_code
        ));
        $query = $this->db->get('message');
        $rows = $query->result_array();
        foreach ($rows as $row) {
            $message [$row ['message_language']] = $row;
        }
        return $message;
    }
    public function save_message($message_code, $message)
    {
        $this->delete_message($message_code);
        foreach ($message as $message_ele) {
            $this->db->insert('message', $message_ele);
        }
        return true;
    }
    public function delete_message($message_code)
    {
        $this->db->delete('message', array(
                'message_code' => $message_code
        ));
        return true;
    }
    public function validate_duplicate($message_code)
    {
        $this->db->select('message_code');
        $this->db->where(array(
                'message_code' => $message_code
        ));
        $query = $this->db->get('message');
        $rows = $query->result_array();
        return sizeof($rows) == 0;
    }
    public function get_languages()
    {
        return array(
                'en' => 'English'
        );
    }
}
