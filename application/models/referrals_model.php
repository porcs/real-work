<?php 

class referrals_model extends CI_Model
{
    private $amt=0,$num=0;
    private $percent = array('0' => '50','1' => '30','2' => '10');
    private $tier_percent = array(20,10,5,3,2);
    private $default = '10';
    private $default_currency = 'EUR';
    private $billingList = array();
    
    public function __construct()
    {
        parent::__construct();
    }

    public function getDefaultCurrency()
    {
        return $this->default_currency;
    }
    
    public function getUser($id_user, $endtimestamp=null)
    {
        $id_user = (int)$id_user;
        $endtimestamp = $endtimestamp==null?'now()':"'$endtimestamp'";
        $sql = "SELECT id,user_name,user_referral FROM users "
            . " WHERE user_referral = '".$id_user."' AND "
            . " unix_timestamp(user_created_on) < ".$endtimestamp." "
            . " ORDER BY id ASC";
      
        return $this->db->query($sql)->result_array();
    }
    
    public function getParent($id_user)
    {
        //$endtimestamp = $endtimestamp==null?'now()':"'$endtimestamp'";
        $id_user = (int)$id_user;
        $sql = "SELECT  user_referral as id FROM users "
            . " WHERE id = '".$id_user."'   ORDER BY id ASC limit 1";
         
        $id = $this->db->query($sql)->result_array();
        
        return $id[0]['id'];
    }
    
    public function getReferrerBilling($id_user, $timestamp)
    {
        $id_user = (int)$id_user;
        $sql = "SELECT id_billing,amt_billing,currency_billing, "
            . "discount_log_billing,discount_cmd_billing FROM billings ";
        $sql .= "WHERE id_users = '".$id_user."' AND status_billing = 'active' ";
        $sql .= "AND DATE_FORMAT(created_date_billing,'%Y-%m') = "
            . " '".date('Y-m', $timestamp)."' ";
        $sql .= "ORDER BY created_date_billing ASC";
        
        return $this->db->query($sql)->result_array();
    }
    public function getAvailUserCommission($id_user)
    {
        $id_user = (int)$id_user;
        $sql ="select * from user_commission , billings "
            . " where parents_id = '$id_user' and status = 1 and "
            . " billings.id_billing = user_commission.billing_id and "
            . " billings.status_billing = 'active' and "
            . " billings.created_date_billing <= now()";
        
        $remain_com = 0;
        foreach ($this->db->query($sql)->result_array() as $k=>$v) {
            $remain_com += $v['remain_amt'];
        }
       
        return $remain_com;
    }
    public function getUserCommission($id_user, $start=null, $end=null)
    {
        $id_user = (int)$id_user;
        if ($start!=null&& $end!=null) {
            $sql ="select * from user_commission ,billings "
                . " where parents_id = '$id_user' and "
                . " create_date > '".date('Y-m-d', $start)."' and "
                . " create_date <= date_add('".date('Y-m-d', $end)."',interval +1 day) "
                . " and billings.id_billing = user_commission.billing_id  ";
        } else {
            $sql ="select * from user_commission , billings "
                . " where parents_id = '$id_user' and "
                . " billings.id_billing = user_commission.billing_id  ";
        }
        
        $com = 0;
        $tier_com = 0;
        $payout = 0;
        $out = null;
        $i=0;
        foreach ($this->db->query($sql)->result_array() as $k=>$v) {
            if (trim($v['status_billing']) != 'active') {
                continue;
            }
            $i++;
            $tier = $v['tier_lv'];
            $remain_com = $v['remain_amt'];
            $max = $v['max_amt'];
            if ($tier==0) {
                $com+=$max;
            } else {
                $tier_com+=$max;
            }
            $payout += ($max-$remain_com);
        }
        if ($i!=0) {
            $out=array('referral'=>$i,'sale'=>$com,'tier'=>$tier_com,'pay'=>$payout);
        }
        return $out;
    }
    public function recurringSpreadCommission($id_user, $amt=0, $bill_id='', $curr='EUR')
    {
        if ($amt == 0 || $bill_id=='') {
            return;
        }
        $tier = 0;
        $parrent_tree=array();
        $currency = $curr;
        $max_tier = sizeof($this->tier_percent);
        for ($i=0;$i<$max_tier;$i++) {
            if (isset($parrent_tree[$i-1]) || $i==0) {
                $id = $i==0?$id_user:$parrent_tree[$i-1];
            }
            if (!$id) {
                continue;
            }
            
            $perc = $this->tier_percent[$i];
            $par_id=$this->getParent($id);
            $parrent_tree[] = $par_id;
            $comm_amt = round(($amt * $perc) /100, 2);
            if ($comm_amt<0.01 || (int)$par_id==0) {
                continue;
            }
            $insert = array(
                'user_id'=>$id_user,
                'parents_id'=>$par_id,
                'tier_lv'=>$i,
                'create_date'=>date('Y-m-d H:i:s'),
                'last_date'=>date('Y-m-d H:i:s'),
                'max_amt'=>$comm_amt,
                'remain_amt'=>$comm_amt,
                'billing_id'=>$bill_id,
                'currency'=>$curr,
                'billing_amt'=>$amt,
                'comm_percent'=>$perc,
                'status'=>'1',
            );
             
            $this->db->insert('user_commission', $insert);
        }
    }
    
    public function recurringUsers($id_user, $timestamp=null, $endtimestamp=null)
    {
        if (!$timestamp) {
            $timestamp = time();
        }
        $tmpQuery = $this->getUser($id_user, $endtimestamp);
        if (!empty($tmpQuery)) {
            $query = array();
            foreach ($tmpQuery as $val) {
                $query[$val['id']] = $val;
                        
                $itemQuery = $this->getReferrerBilling($val['id'], $timestamp);
                if (!empty($itemQuery)) {
                    $query[$val['id']]['billings'] = $itemQuery;
                }
                $query[$val['id']]['child'] = $this->recurringUsers($val['id'], $timestamp, $endtimestamp);
            }
        
            return $query;
        } else {
            return null;
        }
    }
    
    public function recurringNum($arrData, $reset=true)
    {
        if (!empty($arrData['child'])) {
            if ($reset) {
                $this->num = 0;
            }
            
            foreach ($arrData['child'] as $val) {
                if (!empty($val['id'])) {
                    $this->num += 1;
                }
                $this->recurringNum($val, false);
            }
            
            return $this->num;
        } else {
            return false;
        }
    }
    public function payoutSpreadCommission($id_user, $discount=0, $bill_id='', $curr='EUR')
    {
        $amt = $discount;
        $id_user = (int)$id_user;
        $sql = "select * from user_commission where parents_id = '$id_user' and "
            . " status = 1 and remain_amt > 0 order by id asc";
        foreach ($this->db->query($sql)->result_array() as $k=>$v) {
            if ($amt==0) {
                break;
            }
            $new_remain = ($v['remain_amt'] - $amt) <0?0:($v['remain_amt'] - $amt);
            $amt = ($v['remain_amt'] - $amt) <0?($amt-$v['remain_amt']):0;
            $this->db->update('user_commission', array('remain_amt'=>$new_remain,
                'last_date'=>date('Y-m-d H:i:s')), array('id'=>$v['id']));
             
            $insert = array(
                'user_id'=>$id_user,
                'payout_user_id'=>$v['user_id'],
                'tier_lv'=>$v['tier_lv'],
                'create_date'=>date('Y-m-d H:i:s'),
                'max_amt'=>$v['max_amt'],
                'old_remain_amt'=>$v['remain_amt'],
                'remain_amt'=>$new_remain,
                'payout_amt'=>($v['remain_amt']-$new_remain),
                'billing_id'=>$bill_id,
                'currency'=>$curr,
                'status'=>'1',
            );
             
            $this->db->insert('payout_commission', $insert);
            if ($amt==0) {
                break;
            }
        }
        return ;
    }
    public function getCommList($id_user, $discount=0)
    {
        $out = array();
        $amt = $discount;
        $id_user = (int)$id_user;
        $sql = "select * from user_commission ,billings "
            . " where parents_id = '$id_user' and status = 1 and remain_amt > 0 "
                . " and billings.id_billing = user_commission.billing_id "
            . " and billings.status_billing = 'active'"
            . " and billings.created_date_billing <= now() order by id asc";
        
        foreach ($this->db->query($sql)->result_array() as $k=>$v) {
            if ($amt==0) {
                break;
            }
            $new_remain = ($v['remain_amt'] - $amt) <0?0:($v['remain_amt'] - $amt);
            $amt = ($v['remain_amt'] - $amt) <0?($amt-$v['remain_amt']):0;
            $out_t = array(
                 'comm_id'=>$v['id'],
                 'user_id'=>$v['user_id'],
                 'parents_id'=>$v['parents_id'],
                 'old_remain'=>$v['remain_amt'],
                 'new_remain'=>$new_remain,
              );
            $out[]=$out_t;
            if ($amt==0) {
                break;
            }
        }
        return $out;
    }
    public function recurringAmount($parentID, $arrData, $index=0, $reset=true)
    {
        $parentID = (int)$parentID;
        if (!empty($arrData['child'])) {
            if ($reset) {
                $this->amt = 0;
                $this->billingList = array();
            }
            
            foreach ($arrData['child'] as $val) {
                if (empty($this->percent[$index])) {
                    $tmpPercent = $this->default;
                } else {
                    $tmpPercent = $this->percent[$index];
                }
                
                if (!empty($val['billings'])) {
                    foreach ($val['billings'] as $val2) {
                        $sql = "SELECT id_billing FROM billings ";
                        $sql .= "WHERE id_users = '".$parentID."' "
                            . " AND discount_log_billing like '%".$val2['id_billing']."%'";
                        $result = $this->db->query($sql);
                        $num_rows = $result->num_rows();
                        if ($num_rows <= 0) {
                            if ($this->default_currency == $val2['currency_billing']) {
                                if (!empty($val2['discount_cmd_billing'])) {
                                    $tmp = unserialize($val2['discount_cmd_billing']);
                                    if (!empty($tmp['total']['total_amt'])) {
                                        $commission = $tmp['total']['total_amt']*$tmpPercent/100;
                                    } else {
                                        $commission = $val2['amt_billing']*$tmpPercent/100;
                                    }
                                } else {
                                    $commission = $val2['amt_billing']*$tmpPercent/100;
                                }
                            } else {
                                // todo for another currency // default EUR
                            }
                            $this->billingList[] = $val2['id_billing'];
                        } else {
                            $commission = 0;
                        }
                    }
                } else {
                    $commission = 0;
                }
                
                //echo '<p>'.$val['last']['amt_billing'].','.$index.','.$tmpPercent.','.$commission.'</p>';
                $this->amt += $commission;
                $this->recurringAmount($parentID, $val, $index+1, false);
            }
            return $this->amt;
        } else {
            return false;
        }
    }
    
    public function getBillingList()
    {
        return $this->billingList;
    }
    
    public function getReferralUser($id_user, $month=null)
    {
        $this->db->where('user_referral', $id_user);
        
        
        $this->db->select('id,user_name,user_email,user_first_name,'
            . 'user_las_name,user_provider,user_status,user_created_on,'
            . 'user_referral,count(billings.id_billing) as count,'
            . ' sum(billings.amt_total_billing) as sum');
        
        
        if (!is_null($month)) { //$month in time()
            $m = date('Y-m-', $month);
            $start = $m.'1';
            $end_day = date('t', $month);
            $end = $m.$end_day;
            $this->db->join('billings', "billings.id_users = users.id "
                . " and billings.status_billing = 'active' "
                . " and billings.created_date_billing > '$start' and "
                . " billings.created_date_billing <= '$end'", "left");
        } else {
            $this->db->join('billings', "billings.id_users = users.id "
                . " and billings.status_billing = 'active' and "
                . " billings.created_date_billing <= now()", "left");
        }
       
        $this->db->group_by("users.id");
        $out =$this->db->get('users')->result_array();
         //echo $this->db->last_query();
        return $out;
    }
    public function getAccReferralUser($id_user, $month=null)
    {
        $add = '';
        $id_user = (int)$id_user;
        if (!is_null($month)) { //$month in time()
            $m = date('Y-m-', $month);
            $start = $m.'1';
            $end_day = date('t', $month);
            $end = $m.$end_day;
            $add = " and referral_click_history.create_date > '$start' "
                . " and referral_click_history.create_date <= '$end' ";
        }
        $sql = "select referral_history.* , count(referral_click_history.id) as num ,"
            . "users.* from referral_history "
                . " left join referral_click_history on "
            . " referral_history.ip = referral_click_history.ip $add "
                . " left join users on users.id = referral_history.id_user  "
                . " where parent_id = '$id_user' and status = 'active' "
            . " order by referral_history.last_date desc ";
         
        $out = $this->db->query($sql)->result_array();
        return sizeof($out)>0?$out:array();
    }
    
    public function getSaleCommission($arrReferral, $id_user)
    {
        if (!empty($arrReferral['child'])) {
            $id_user = (int)$id_user;
            $commission = 0;
            $percent = $this->percent[0];
            foreach ($arrReferral['child'] as $val) {
                if (!empty($val['billings'])) {
                    foreach ($val['billings'] as $val2) {
                        $sql = "SELECT id_billing FROM billings ";
                        $sql .= "WHERE id_users = '".$id_user.
                            "' AND discount_log_billing like '%".
                            $val2['id_billing']."%'";
                        $result = $this->db->query($sql);
                        $num_rows = $result->num_rows();
                        if ($num_rows <= 0) {
                            if (!empty($val2['discount_cmd_billing'])) {
                                $tmp = unserialize($val2['discount_cmd_billing']);
                                if (!empty($tmp['total']['total_amt'])) {
                                    $commission += $tmp['total']['total_amt']*$percent/100;
                                } else {
                                    $commission += $val2['amt_billing']*$percent/100;
                                }
                            } else {
                                $commission += $val2['amt_billing']*$percent/100;
                            }
                        }
                    }
                }
            }
            
            return $commission;
        } else {
            return 0;
        }
    }
    
    public function getTierCommission($arrReferral)
    {
        if (!empty($arrReferral['child'])) {
            $commission = 0;
            $initLevel = 1;
            foreach ($arrReferral['child'] as $val) {
                if (!empty($val['id']['child'])) {
                    $commission += $this->recurringAmount($val['id'], $val, $initLevel, true);
                }
            }
            
            return $commission;
        } else {
            return 0;
        }
    }
    
    public function getTotalResult($arrReferral, $id_user)
    {
        $tmpArr = array(
            'referral' => 0,
            'sale' => 0,
            'tier' => 0,
            'pay' => 0,
        );
        $tmpArr['referral'] = $this->recurringNum($arrReferral);
        $tmpArr['sale'] = $this->getSaleCommission($arrReferral, $id_user);
        $tmpArr['tier'] = $this->getTierCommission($arrReferral);
        return $tmpArr;
    }
}
