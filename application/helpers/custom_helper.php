<?php
if (!function_exists('cdn_url')) :
    function cdn_url($uri='')
    {
        $ci =& get_instance();
        $out = $ci->config->item('cdn_url');
        return empty($out)?$ci->config->base_url($uri):$out.$uri;
    }
endif;

if (!function_exists('_filter_data')) :
    function _filter_data($table, $data)
    {
        $ci =& get_instance();
        $filtered_data = array();
        $columns =  $ci->db->list_fields($table);

        if (is_array($data)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data)) {
                    $filtered_data[$column] = trim($data[$column]);
                }
            }
        }

        return $filtered_data;
    }
endif;

/*
$type = 'global'; - set message to header.
$type = 'inner'; - return html.
*/
if (!function_exists('_get_message_code')) :
    global $message_code_history;

    function _get_message_code($code, $type = 'global')
    {
        $ci =& get_instance();
        $language = $ci->session->userdata('language');
        $iso_code = mb_substr($language, 0, 2);
        $ci->db->where(array('message_code' => "$code", 'message_language' => "$iso_code"));
        $query = $ci->db->from('message');
        $get = $query->get();
        $rows = $get->result();

        if (isset($rows[0])) {
            $row = (array)$rows[0];
            if (isset($row) && !empty($row)) {
                switch ($type) {
                    case 'inner':
                        $ci->smarty->assign('message_code_inner', $row);
                        $html = $ci->smarty->fetch('message_code_inner.tpl');
                        return $html;
                    case 'global':
                        global $message_code_history;

                        if ($row['message_type'] == "error") {
                            $message_code_history['error'][$code] = $row;
                        }
                        if ($row['message_type'] == "warning") {
                            $message_code_history['warning'][$code] = $row;
                        }
                        if ($row['message_type'] == "info") {
                            $message_code_history['info'][$code] = $row;
                        }

                        $ci->smarty->assign("message_code", $message_code_history);
                        break;
                }
            }
        }
    }
endif;

if (!function_exists('_get_message_help')) :
    function _get_message_help($code)
    {
        $ci =& get_instance();
        $language = $ci->session->userdata('language');
        $iso_code = mb_substr($language, 0, 2);
        $ci->db->where(array('message_code' => "$code", 'message_language' => "$iso_code"));
        $query = $ci->db->from('message');
        $get = $query->get();
        $rows = $get->result();

        if (isset($rows[0])) {
            $row = (array)$rows[0];
            if (isset($row) && !empty($row)) {
                $ci->smarty->assign('message_code_help', $row);
                $ci->smarty->assign('message_code_id', '');
                $html = $ci->smarty->fetch('message_code_inner.tpl');
                return array('title' => $row['message_name'], 'content' => $html);
            }
        }
        return array('title' => $code, 'content' => $code);
    }
endif;

if (!function_exists('_get_message_document')) :
    function _get_message_document($class, $function)
    {
        $ci =& get_instance();
        $language = $ci->session->userdata('language');
        $iso_code = mb_substr($language, 0, 2);
        $ci->db->where(array('message_name' => $class . "_". $function, 'message_language' => "$iso_code"));
        $query = $ci->db->from('message');
        $get = $query->get();
        $rows = $get->result();

        if (isset($rows[0])) {
            $message_document = array(
               'message_text' => isset($rows[0]->message_problem) ? $rows[0]->message_problem : '',
               'message_link' => isset($rows[0]->message_cause) ? $rows[0]->message_cause : '',
               'message_solution' => isset($rows[0]->message_solution) ? $rows[0]->message_solution : '',
            );
            $ci->smarty->assign('message_document', $message_document);
        }
    }
endif;

if (!function_exists('_get_icon_help')) :
    function _get_icon_help($code)
    {
        $ci =& get_instance();
        $ci->smarty->assign('message_code_id', $code);
        $html = $ci->smarty->fetch('message_code_inner.tpl');
        return $html;
    }
endif;

if (!function_exists('update_track_message_using')) :
    function update_track_message_using($code, $type='', $name=false)
    {
        $ci =& get_instance();
        $url =  $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $url = str_replace('//', '/', $url);
        $ref='';
        if (isset($_SERVER["HTTP_REFERER"])) {
            $ref =  $_SERVER["HTTP_REFERER"];
        }
        if (!is_array($type)) {
            $types=array($type);
        } else {
            $types=$type;
        }
        foreach ($types as $type) {
            $key = md5($code.json_encode($type).$url);
            $ci = get_instance();
            $session_key = date("Y-m-d H:i:s");
            if ($name) {
                $data = array('status'=>2, 'id_key'=>$key, 'key_full'=>$code,'value'=>$code,'url'=>$url,'ref'=>$ref,'last_update'=>$session_key);
            } else {
                $data = array('status'=>1, 'id_key'=>$key, 'key_full'=>'message#'.$type,'value'=>$code,'url'=>$url,'ref'=>$ref,'last_update'=>$session_key);
            }
            $insert_query = $ci->db->insert_string('crowdin_path_of_word', $data);
            $insert_query = str_replace('INSERT INTO', 'REPLACE INTO', $insert_query);
//            $ci->db->query($insert_query);
        }
    }
endif;

if (!function_exists('_get_tooltip_icon')) :
    function _get_tooltip_icon($msg,$opt=null)
    {
        $ci =& get_instance();
        if(strpos($msg,'="')!==false){
            return $ci->smarty->fetch('other/tooltips_icon.tpl',array('msg'=>$msg,'opt'=>$opt));
        }else{
            return $ci->smarty->fetch('other/tooltips_icon.tpl',array('msg'=>str_replace('"','&rdquo;',$msg),'opt'=>$opt));
        }
    }
endif;

if (!function_exists('_lock_icon_by_mode')) :
    function _lock_icon_by_mode()
    {
        $ci =& get_instance();
        $mode = $ci->session->userdata('mode_default');
        if(!empty($mode) && $mode=='1'){
            return $ci->smarty->fetch('other/sidebar_lock_icon.tpl');
        } return '';
    }
endif;