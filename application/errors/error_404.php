<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
</head>
<body>
	<div class="main" id="container">
            <div class="p-rl40 p-xs-rl20">
                <div class="notFound">
                            <div class="headSettings clearfix b-Bottom p-b10">
                                    <h4 class="headSettings_head"><span>Oops...</span> <?php echo $heading; ?></h4>	
                            </div>	
                            <div class="notFound_reason">
                                    <ul class="main_reason">
                                            <li>Errors can be caused due to various reasons, such as:</li>
                                            <li><?php echo $message; ?></li>
                                    </ul>
                                    <div class="notFound_reason-point">
                                            <ul class="pull-sm-left m-r30">
                                                    <li>Page requested does not exist.</li>
                                                    <li>Server is down.</li>
                                                    <li>Internet connection is down.</li>
                                            </ul>
                                            <ul>
                                                    <li>Broken links</li>
                                                    <li>Incorrect URL</li>
                                                    <li>Page has been moved to a different address</li>
                                            </ul>
                                    </div>
                            </div>
                    </div>
                    <div class="row">
                            <div class="col-xs-12">
                                    <div class="b-Top m-t20 p-t20">
                                            <a href="" class="link p-size" onclick="window.history.back();"><i class="fa fa-long-arrow-left m-r10"></i>Back</a>
                                    </div>
                            </div>
                    </div>
            </div>
	</div>
</body>
</html>