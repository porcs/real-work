<?php

$lang['page_title']                     = "Offers";
$lang['offers']                         = "Offers";
$lang['my_feeds']                       = "My feeds";
$lang['parameters']                     = "Parameters";

//Profile
$lang['profile']                        = "Profile";
$lang['price']                          = "Price";

//Rules
$lang['rules']                          = "Rules";

//Category
$lang['category']                       = "Category";

//Message
$lang['save_unsuccessful']              = "Save unsuccessful.";
$lang['save_successful']                = "Save successful.";
$lang['confirm_message']                = "Do you want to delete?";
$lang['Change default shop successful'] = "Changed default shop successfully";


$lang['Add a price to the list']        = "Add a price to the list";
$lang['Price(s)']                       = "Price(s)";
$lang['Price Name']                     = "Price name";
$lang['Percentage']                     = "Percentage";
$lang['Allow only digits.']             = "Allow only digits.";
$lang['Rounding']                       = "Rounding";
$lang['One Digit']                      = "One digit";
$lang['Two Digit']                      = "Two digit";
$lang['None']                           = "None";
$lang['Use a familiar name to remember it.']
                                        = "Use a familiar name to remember it.";
$lang['Formula to be applied on all exported products prices (multiplication, division, addition, subtraction, percentages).']
                                        = "Formula to be applied on all exported products prices (multiplication, division, addition, subtraction, percentages).";
$lang['Apply a specific price formula for selected categories which will override the main setting.']
                                        = "Apply a specific price formula for selected categories which will override the main setting.";
$lang['Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required']
                                        = "Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
$lang['Do you want to delete']          = "Do you want to delete";
$lang['Delete successfully']            = "Delete successfully";
$lang['deleted']                        = "deleted";
$lang['can not delete']                 = "Can not delete";
$lang['Save and Continue']              = "Save and continue";




$lang['Rules'] = "Rules";
$lang['Add a rule to the list'] = "Add a rule to the list";
$lang['Rule(s)'] = "Rule(s)";
$lang['Rule Name'] = "Rule name";
$lang['Add Rule Name first'] = "Add rule name first";
$lang['Add item'] = "Add item";
$lang['Item(s)'] = "Item(s)";
$lang['Manufacturer'] = "Manufacturer";
$lang['Supplier'] = "Supplier";
$lang['Price Range'] = "Price range";
$lang['to'] = "to";
$lang['Action'] = "Action";
$lang['Drop'] = "Drop";
$lang['Item'] = "Item";
$lang['Manufactory'] = "Manufactory";
$lang['Supplier'] = "Supplier";
$lang['Price range'] = "Price range";
$lang['Successfully'] = "Successfully";
$lang['Delete was unsuccessful'] = "Delete was unsuccessful";
$lang['Delete was successful'] = "Delete was successful";
$lang['Are you sure to delete rule'] = "Are you sure to delete rule?";
$lang['Are you sure to delete rule item'] = "Are you sure to delete rule item?";
$lang['Are you sure to delete this Price range'] = "Are you sure to delete this price range?";
$lang['Price range error'] = "Price range error";
$lang['check_shop'] = "Check shop";
$lang['rules_item_delete'] = "Delete item rules";
$lang['rules_save'] = "Save rules";
$lang['price_save'] = "Save price";
$lang['Warning!'] = "Warning!";
$lang['Please wait until importing is successfully completed'] = "Please wait until importing is successfully completed.";

//new
$lang['Rule name'] = "Rule name";
$lang['Please enter rule name first'] = "Please enter rule name first.";
$lang['Please enter unique rule name'] = "Please enter unique rule name.";
$lang['To'] = "To";
$lang['Are you sure to delete this price range'] = "Are you sure to delete this price range?";
$lang['New price range'] = "New price range";
$lang['Add a rule to the list'] = "Add a rule to the list";
$lang['Remove a rule from list'] = "Remove a rule from list";
$lang['Remove set'] = "Remove set";
$lang['Add set'] = "Add set";