<?php

//Feed Message
$lang['See_more_error_in_Logs']                     = 'See more error in logs';
$lang['Amazon_Marketplace_Automaton_started_on']    = 'Amazon marketplace automaton started on';
$lang['Amazon_s_started_on_s']                      = 'Amazon %s started on %s';
$lang['AmazonS_s_started_on_s']                     = 'Amazon%s %s started on %s';
$lang['Amazon_synchronize_started_on']              = 'Amazon synchronization started on';
$lang['Amazon_s_synchronize_started_on_s']          = 'Amazon%s synchronization started on %s.';
$lang['Preparing_products']                         = 'Preparing products';
$lang['Sorry_no_items_found_process_completed_No_items_to_be_send'] = /*'Sorry, no items found. Process is completed.*/ 'No items to be sent.';
$lang['Products_preparing_successful_start_validation']  = 'Products preparing successful, validating products.';
$lang['completely']                                 = 'Completely';
$lang['See_result_in_Report_tab']                   = 'See result in report tab.';
$lang['Nothing_to_send']                            = 'Nothing to send';
$lang['Sending_s_feed_to_Amazon_s']                 = 'Sending %s feed to Amazon %s';
$lang['Save_file_xml_error']                        = 'Save file xml error.';
$lang['Update']                                     = 'Update';
$lang['Error']                                      = 'Error';
$lang['Getting_Submission_List_ID_s_s']             = 'Getting submission list (ID: %s) %s';
$lang['Get_submission_List_Error_s_s_s']            = 'Get submission list error : %s (#%s)%s';
$lang['Getting_Submission_Result_ID_s_s']           = 'Getting submission result (ID: %s)%s';
$lang['s_the_process_will_restart_in_s_minutes']    = '%s, the process will restart in %s minutes.';
$lang['Get_submission_Result_Error_s_s']            = 'Get submission result error : %s (#%s)';
$lang['No_message_from_Amazon']                     = 'No message from Amazon.';
$lang['Validation_Error']                           = 'Validation error.';
$lang['Unable_to_login']                            = 'Unable to login.';
$lang['Unable_to_connect_Amazon']                   = 'Unable to connect Amazon.';
$lang['processing']                                 = 'Processing';
$lang['products']                                   = 'Products';
$lang['s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'] = '%s products are running, you cannot run this process again just wait withing %s minutes.';
$lang['items_to_xx_with_Amazon']                    = 'Items to xx with Amazon.';
$lang['skipped_items']                              = 'Skipped items.';

//New 26-01-2015 (Error)
$lang['ProductID']                                  = 'Product ID'; // 20/5/2015
$lang['ProductID(s)']                               = '%s';
$lang['Missing_model_in_s_profile_setting']         = 'Missing model in %s profile setting.';
$lang['Missing_profile_setting']                    = 'Missing profile setting.';
$lang['Duplicate_entry_for_s_Previously_used_by_Product_ID_s_This_is_not_allowed'] = 'Duplicate entry for %s. Previously used by : %s This is not allowed.';
$lang['are_inactive'] = 'are inactive.';
$lang['Missing_s'] = 'Missing %s.';
$lang['Missing_reference'] = 'Missing reference.';
$lang['s_is_incorrect_EAN_UPC'] = '#%s is incorrect EAN/UPC.';
$lang['Has_No_s'] = 'Has No %s.';
$lang['Feed_submission_error'] = 'Feed submission error.';

//Update Order

// $lang['Amazon_s_started_update_order_shipment'] = 'Amazon%s started update order shipment';
$lang['Amazon_s_started_s_on'] = 'Amazon%s started %s %s on'; // changed on 25/11/2015 
$lang['Amazon_s_started_s_s_on'] = 'Amazon%s started %s %s on'; // changed on 25/11/2015 
$lang['Getting_order_shipment_list'] = 'Getting order shipment list.';
$lang['No_order_update'] = 'No order update.';
$lang['Sending_order_shipment_lists'] = 'Sending order shipment list.';
$lang['Empty_Feedsubmission_Id'] = 'Empty Feedsubmission Id.';
$lang['See_result'] = 'See result.';
//$lang['Confirm_Order_Shipment_Processing'] = 'Confirm Order Shipment Processing.';
$lang['Amazon_s_s_Processing'] = 'Amazon, %s %s'; // changed // 25/11/2015

// delete product
$lang['out_of_stock'] = 'Out of stock.';
$lang['not_out_of_stock'] = 'Available quantities for this product.';

//AmazonXmlValidator
$lang['element_was_not_found_on_XSD'] = 'Element was not found on XSD.';
$lang['element_was_not_found_on_generated_XML'] = 'Element was not found on generated XML.';
$lang['Choice_of_content_could_not_be_set_for_this_Schema'] = 'Choice of content could not be set for this Schema. e.g. Product, Inventory, etc.';
$lang['Type_s_could_not_be_included_in_envelope'] = 'Type: %s could not be included in envelope.';
$lang['No_items_found'] = 'No items found. Product must exists at least: %s And currently %s were found.';
$lang['element_must_exists_at_least'] = 'Element "%s" must exist at least %s, currently %s were found.';
$lang['element_must_exists_at_most'] = 'Element "%s" must exist at most %s, currently %s were found.';
$lang['One_of_these_values_s_must_be_set'] = 'One of these values: "%s" must be set.';
$lang['element_s_must_have_a_s_value_Currently_an_object_is_set'] = 'Element: %s must have a %s value (true/false). Currently an object is set.';
$lang['element_s_must_have_a_boolean_value_Currently_value_s'] = 'Element: %s must have a boolean value (true/false). Current value: %s.';
$lang['element_s_must_have_a_s_value_negativeInteger_Currently_an_object_is_set'] = 'Element: %s must have a %s value (..,-2,-1). Currently an object is set.';
$lang['element_s_must_have_a_valid_negativeInteger_Currently_value_s'] = 'Element: %s value must be a valid Negative Integer (..,-2,-1). Current value: %s.';
$lang['element_s_must_have_a_s_nonNegativeInteger_Currently_an_object_is_set'] = 'Element: %s must have a %s value (0,1,2,..). Currently an object is set.';
$lang['element_s_must_have_a_nonNegativeInteger_Currently_value_s'] = 'Element: %s value must be a Non Negative Integer (0,1,2,..). Current value: %s.';
$lang['element_s_must_have_a_s_nonPositiveInteger_Currently_an_object_is_set'] = 'Element: %s must have a %s value (..,-2,-1,0). Currently an object is set.';
$lang['element_s_must_have_a_nonPositiveInteger_Currently_value_s'] = 'Element: %s value must be a valid Non Positive Integer (..,-2,-1,0). Current value: %s.';
$lang['element_s_must_have_a_s_positiveInteger_Currently_an_object_is_set'] = 'Element: %s must have a %s value (1,2,..). Currently an object is set.';
$lang['element_s_must_have_a_positiveInteger_Currently_value_s'] = 'Element: %s value must be a valid Positive Integer (1,2,..). Current value: %s.';
$lang['element_s_must_have_a_s_decimal_Currently_an_object_is_set'] = 'Element: %s must have a %s value (..,-2,-1). Currently an object is set.';
$lang['element_s_must_have_a_decimal_Currently_value_s'] = 'Element: %s value must be a valid Negative Integer (..,-2,-1). Current value: %s.';
$lang['element_s_must_have_a_s_integer_Currently_an_object_is_set'] = 'Element: %s must have a %s value. Currently an object is set.';
$lang['element_s_must_have_a_integer_Currently_value_s'] = 'Element: %s value must be a valid Integer. Current value: %s.';
$lang['Value_for_s_is_non_valid'] = 'Value for %s is non valid (according to its pattern).';
$lang['s_is_not_a_valid_value_se_for_s'] = '"%s" is not a valid value set for: %s.';
$lang['s_length_must_be_greater_or_equal_to_s_for_the_element'] = '"%s" length must be greater or equal to: %s for the element: %s';
$lang['s_length_must_be_less_or_equal_to_s_for_the_element'] = '"%s" length must be less or equal to: %s for the element: %s';
$lang['s_is_invalid_digits_must_be_less_or_equal_to_s_for_the_element'] = '"%s" is invalid, digits must be less or equal to: %s for the element: %s';
$lang['s_is_invalid_fraction_digits_must_be_less_or_equal_to_s_for_the_element'] = '"%s" is invalid, fraction digits must be less or equal to: %s for the element: %s';

// 11/02/2015
$lang['s_length_must_be_greater_or_equal_to_8_and_less_or_equal_to_16_Currently_value_s'] = '"%s" length must be greater or equal to: 8 and less or equal to: 16. Current value: %s';
$lang['s_length_must_be_greater_or_equal_to_1_and_less_or_equal_to_40_Currently_value_s'] = '"%s" length must be greater or equal to: 1 and less or equal to: 40. Current value: %s';
$lang['Skipped'] = "Skipped";

// 17/06/2015
$lang['Disabled_product'] = "are disabled.";

// 23/06/2015
$lang['Minimum of quantity required to be in stock to export the product: %s Current stock: %s'] = "Minimum of quantity required to be in stock to export the product: %s Current stock: %s";

// 22/07/2015
$lang['Retrieve_Messages'] = "Retrieve Messages";

// 27/07/2015
$lang['inactivate_delete_products'] = 'Delete products on Amazon is inactive, to allow delete products on Amazon please activate this on Features tab';
$lang['inactivate_create_products'] = 'Create products on Amazon is inactive, to allow create products on Amazon please activate this on Features tab';
$lang['inactivate_repricing'] = 'Repricing product is inactive, to allow repricing products on Amazon please activate this on Features tab';

/**/
$lang['Condition_S_not_allow'] = 'Condition %s not allow, feature second hand not activate. To allow to sell second hand, collectible or refurbished products, please active feature second hand.';

/*05/10/2015*/
$lang['zero_price'] = '0.00 price (standard or sales) will not be accepted. Please ensure that price of at least equal to or greater than 0.01.';

/*FBA 10/11/2015*/
$lang['inactivate_fba'] = 'Fulfillment by Amazon is inactive, to allow Fulfillment by Amazon please activate this on Features tab.';
$lang['Listting Inventory'] = 'Listting Inventory';
$lang['Unable to find product'] = 'Unable to find product';

// FBA message
//$lang['01'] = 'Product became out of stock';
//$lang['02'] = 'Product in stock (FBA)';
//$lang['03'] = 'Product in stock (FBA)';

// amazon.order.status // 25/11/2015
$lang['Update Order'] = 'update order';
$lang['Shipment'] = 'shipment';
$lang['Acknowledgement'] = 'acknowledgement';
$lang['Sending_order_acknowledgement_lists'] = 'Sending order acknowledgement list.';

// amazon.fba.order.php // 08/12/2015
$lang['Listting_S_S'] = 'Listting %s %s.';

// amazon product //08/03/2016
$lang['Product_S_has_several_profiles_in_serveral_categories'] = 'Product "%s" has several profiles in serveral categories !';
$lang['Product_S_has_no_category'] = 'Product "%s" has no category';
$lang['Product_S_is_not_in_selected_categories_D'] = 'Product "%s" is not in selected categories "%d"';

// send shipping override // 2016-03-30
$lang['shipping_override_missing_mapping_carrier'] = 'Mapping of Standard Carriers is required for send Shipping Override to Amazon.';

// 2016-05-24
$lang['Disabled_product_global'] = 'are disabled (Global).';