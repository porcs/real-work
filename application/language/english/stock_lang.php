<?php

$lang['page_title'] = "Stock";
$lang['stock'] = "Stock";
$lang['report'] = "Report";
$lang['Date'] = "Date";
$lang['Detail'] = "Detail";
$lang['Quantity'] = "Quantity";
$lang['Balance Quantity'] = "Balance Quantity";
$lang['No data available in table'] = "No data available in table";
$lang['Download CSV'] = "Download CSV";
$lang['Stock Movement'] = "Stock Movement";
$lang['Reference'] = "Reference";
$lang['Movement Quantity'] = "Movement Quantity";
$lang['products'] = "products";
$lang['Next'] = "Next";

/*19-11-2015*/
$lang['Order from %s (order id:%s)'] = 'Order from %s (order id:%s)';
$lang['Feed update offer from'] = 'Feed update offer from';
$lang['Other'] = 'Other';
$lang['%s from %s'] = '%s from %s';