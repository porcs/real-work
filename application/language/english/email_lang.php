<?php

/**activate.tpl**/
$lang['Dear Customer']                         
	= "Dear Customer";
$lang['You are receiving this email because you just registered on Feed.biz.']                         
	= "You are receiving this email because you just registered on Feed.biz.";
$lang['Your account has been created, please click here to activate it now']                         
	= "Your account has been created, please click here to activate it now";
$lang['Click here to activate account']                         
	= "Click here to activate account";
$lang['If you are having difficulties activating your account, then please contact the support service at']                         
	= "If you are having difficulties activating your account, then please contact the support service at";
$lang['This message comes from an unmonitored mailbox. Please do not reply to this message.']                         
	= "This message comes from an unmonitored mailbox. Please do not reply to this message.";
$lang['Thanks']                         
	= "Thanks";
$lang['This e-mail was sent by']                         
	= "This e-mail was sent by";
$lang['All right reserved.']                         
	= "All right reserved.";

/**after_activate.tpl**/
$lang['A warm welcome to']                         
	= "A warm welcome to";
$lang['It is my great pleasure to be appointed as your dedicated Feed.biz supporter.']                         
	= "It is my great pleasure to be appointed as your dedicated Feed.biz supporter.";
$lang['Please do not hesitate to contact me or your Feed.biz supporter at the following contact details whenever your need assistance.']                         
	= "Please do not hesitate to contact me or your Feed.biz supporter at the following contact details whenever your need assistance.";
$lang['Click on the link to access your back office']                         
	= "Click on the link to access your back office";
$lang['Click here to your back office']                         
	= "Click here to your back office";
$lang['Consider adding this link to your bookmarks.']                         
	= "Consider adding this link to your bookmarks.";
$lang['Yours sincerely']                         
	= "Yours sincerely";
$lang['This e-mail was sent to']                         
	= "This e-mail was sent to";
$lang['Please note that you must use an email to access to Feed.biz management system.']                         
	= "Please note that you must use an email to access to Feed.biz management system.";

/**errHandler.tpl**/
$lang['Hello']                         
	= "Hello";
$lang['We found an error on']                         
	= "We found an error on";
$lang['See more error in file']                         
	= "See more error in file";

/**export_monitor.tpl**/
$lang['Summary on']                         
	= "Summary on";
$lang['Sources']                         
	= "Sources";
$lang['Orders']                         
	= "Orders";
$lang['Product Creation Feed']                         
	= "Product Creation Feed";
$lang['Update Offers Feed']                         
	= "Update Offers Feed";
$lang['Repricing']                         
	= "Repricing";
$lang['Send']                         
	= "Send";
$lang['Success']                         
	= "Success";
$lang['Error']                         
	= "Error";
$lang['Statistics']                         
	= "Statistics";
$lang['Message']                         
	= "Message";
$lang['Number of product']                         
	= "Number of product";
$lang['Percentage']                         
	= "Percentage";
$lang['Totals']                         
	= "Totals";

/**export_monitor_body.tpl**/
$lang['summary on']                         
	= "summary on";
$lang['Out of Stock']                         
	= "Out of Stock";
$lang['Inactive Product']                         
	= "Inactive Product";

/**export_monitor_header.tpl**/
$lang['Dear']                         
	= "Dear";

/**forgot_password.tpl**/
$lang['Email Forgot Password']                         
	= "Email Forgot Password";
$lang['Welcome To']                         
	= "Welcome To";
$lang['Click here to reset password']                         
	= "Click here to reset password";
$lang['The Support Team at']                         
	= "The Support Team at";
$lang['Happy To Help!']                         
	= "Happy To Help!";

/**new_password.tpl**/
$lang['Email New Password']                         
	= "Email New Password";
$lang['email_new_password_heading']                         
	= "email_new_password_heading";
$lang['email_new_password_subheading']                         
	= "email_new_password_subheading";

/**welcome.tpl**/
$lang['Email Welcome']                         
	= "Email Welcome";
$lang['Your']                         
	= "Your";
$lang['account has now been created and your login details are below.']                         
	= "account has now been created and your login details are below.";
$lang['Before you log into your account we strongly urge you to have a look at our Start Up Guide to avoid issues with your setup.']                         
	= "Before you log into your account we strongly urge you to have a look at our Start Up Guide to avoid issues with your setup.";
$lang['It will only take you a few minutes and will save you a lot of time in the long run.']                         
	= "It will only take you a few minutes and will save you a lot of time in the long run.";
$lang['Your login details are']                         
	= "Your login details are";
$lang['Email']                         
	= "Email";
$lang['Password']                         
	= "Password";
$lang['Login address']                         
	= "Login address";
$lang['Login']                         
	= "Login";

/*eBay*/
$lang['Feed Submission ID']                         
	= "Feed Submission ID";
$lang['Feed Submission date']                         
	= "Feed Submission date";
$lang['Total Product on shop']                         
	= "Total Product on shop";
$lang['Total Product on eBay']                         
	= "Total Product on eBay";
$lang['Throughput']                         
	= "Throughput";
$lang['Download Error Message']                         
	= "Download Error Message";
$lang['From']                         
	= "From";
$lang['Message']                         
	= "Message";
$lang['Level']                         
	= "Level";
$lang['Count']                         
	= "Count";

/*Amazon*/
$lang['Items to']                         
	= "Items to";
$lang['with Amazon']                         
	= "with Amazon";
$lang['Skipped items']                         
	= "Skipped items";
$lang['View more on website']                         
	= "View more on website";
$lang['Result']                         
	= "Result";
$lang['Feed Sunmission Id']                         
	= "Feed Sunmission Id";
$lang['Feed Sunmission date']                         
	= "Feed Sunmission date";
$lang['Messages Processed']                         
	= "Messages Processed";
$lang['Messages Successful']                         
	= "Messages Successful";
$lang['Messages With Warning']                         
	= "Messages With Warning";
$lang['Messages With Error']                         
	= "Messages With Error";
