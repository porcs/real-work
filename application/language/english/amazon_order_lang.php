<?php

// 19/02/2015
$lang['Amazon%s Started Order Query'] = 'Amazon%s Started Order Query';
$lang['Start Get Order List'] = 'Start Get Order List';
$lang['See all messages'] = 'See all messages';
$lang['Getting %s Order List'] = 'Getting %s Order List';
$lang['Amazon Get Orders Error'] = 'Amazon Get Orders Error';
$lang['No pending order for %s to %s'] = 'No pending order for %s to %s';
$lang['Order Query Processing'] = 'Order Query Processing';
$lang['Unable to connect Amazon'] = 'Unable to connect Amazon';
$lang['Order #%s, Was already imported'] = 'Order #%s, Was already imported';
$lang['Order #%s, Missing Buyer Address for this order'] = 'Order #%s, Missing Buyer Address for this order';
$lang['Order #%s, Missing Buyer Name for this order'] = 'Order #%s, Missing Buyer Name for this order';
$lang['Order #%s, Missing Buyer Email for this order'] = 'Order #%s, Missing Buyer Email for this order';
$lang['Order ID (%s) Could not import incomplete order'] = 'Order ID (%s) Could not import incomplete order';
$lang['Order ID (%s) Item does not have SKU - #%s'] = 'Order ID (%s) Item does not have SKU - #%s';
$lang['Order ID (%s) Could not import product SKU : %s'] = 'Order ID (%s) Could not import product SKU : %s';
$lang['Order ID (%s) Could not add buyer'] = 'Order ID (%s) Could not add buyer';
$lang['Unable to associate the carrier (%s) for order #%s'] = 'This is not an error ! You must configure the carrier mapping in the configuration. Unable to associate the carrier (%s) for order #%s';
$lang['Unable to execute parameters for order #%s'] = 'Unable to execute parameters for order #%s';
$lang['Unable to save order items'] = 'Unable to save order items';
$lang['Success'] = 'Success';

// 28/07/2015
$lang['inactivate_import_orders'] = 'Import orders is inactive, to allow import orders please activate this on Features tab';