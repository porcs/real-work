<?php

$lang['page_title']                     = "Marketplaces";
$lang['configuration']                  = "Configuration";

$lang['Currency'] = "Currency";
$lang['Save'] = "Save";
$lang['Reset'] = "Reset";
//message
$lang['incorrect_url']                  = "Incorrect url";
$lang['save_unsuccessful']              = "Save unsuccessful.";
$lang['save_successful']                = "Save successful.";

//popup
$lang['Please select your market places.'] = "Please select your market places.";
$lang['Continue'] = "Continue";
$lang['lease select your market place sites.'] = "Please select your market place sites.";
$lang['Back'] = "Back";
$lang['Save and continue'] = "Save and continue";
$lang['Please select your market place site at least once.'] = "Please select your market place site at least once.";

$lang['Please select your market place sites.'] = "Please select your market place sites.";
$lang['marketplace'] = "Marketplaces";
$lang['configuration_data'] = "Configuration data";
$lang['Congatulations, you have successfully configured your first login.'] = "Congratulations, you have successfully configured your first login.";
$lang['And, we recommend to configuration your marketplace account and send your items to marketplaces.'] = "And, we recommend to configuration your marketplace account and send your items to marketplaces.";
$lang['eBay Wizard'] = "eBay wizard";
$lang['Close'] = "Close";
$lang['Amazon Wizard'] = "Amazon Wizard";
$lang['display_conf_link'] = "Display configuration link";
$lang['configuration_popup'] = "Configuration popup";
$lang['Do you really want to reset?'] = "Do you really want to reset?";

$lang['Marketplace'] = "Marketplaces";
$lang['Setting'] = "Setting";
$lang['please select'] = "please select";
$lang['Connector'] = "Connector";
$lang['Name'] = "Name";