<?php
$lang['page_title']                     = "Statistics";
$lang['statistics']                     = "Statistics";
$lang['products']                       = "Products";
$lang['manufacturers']                  = "Manufacturers";
$lang['categories']                     = "Categories";
$lang['suppliers']                      = "Suppliers";
$lang['carriers']                       = "Carriers";
$lang['results_for']                    = "Results for";


//product detail
$lang['information']                    = "Information";
$lang['price']                          = "Price";
$lang['topology']                       = "Topology";
$lang['combination']                    = "Combination";
$lang['image']                          = "Image";
$lang['feature']                        = "Feature";
$lang['supplier']                       = "Supplier";
$lang['name']                           = "Name";
$lang['reference']                      = "Reference";
$lang['ean13']                          = "EAN-13";
$lang['upc']                            = "UPC";
$lang['status']                         = "Status";
$lang['condition']                      = "Condition";
$lang['description']                    = "Description";
$lang['tags']                           = "Tags";
$lang['enter_tags']                     = "Enter tags";
$lang['tax']                            = "Tax";
$lang['currency']                       = "Currency";
$lang['specific_prices']                = "Specific prices";
$lang['available']                      = "Available";
$lang['apply_a_discount_of']            = "Apply a discount of";
$lang['general']                        = "General";
$lang['width']                          = "Width";
$lang['height']                         = "Height";
$lang['depth']                          = "Depth";
$lang['weight']                         = "Weight";

