<?php

//menu
$lang['dashboard']                          = "Dashboard";
$lang['page_title']                         = "Dashboard";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";
$lang['welcome']                            = "Welcome";                 
$lang['validation'] = 'Validation';
$lang['Log'] = 'Log';
$lang['Last Import Log'] = 'Last import log';
$lang['Batch ID'] = 'Batch ID';
$lang['Source'] = 'Source';
$lang['Shop'] = 'Shop';
$lang['Type'] = 'Type';
$lang['Total'] = 'Total';
$lang['No. Success'] = 'No. Success';
$lang['No. Warning'] = 'No. Warning';
$lang['No. Error'] = 'No. Error';
$lang['Import Date'] = 'Import date';