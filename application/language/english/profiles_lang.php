<?php

$lang['page_title']                     = "Profiles";
$lang['profiles']                       = "Profiles";
$lang['my_feeds']                       = "My feeds";
$lang['parameters']                     = "Parameters";
$lang['configuration']                  = "Configuration";
$lang['category']                       = "Category";

//Message
$lang['save_unsuccessful']              = "Save unsuccessful.";
$lang['save_successful']                = "Save successful.";
$lang['confirm_message']                = "Do you want to delete ";

$lang['please_enter_your_first_name']       = "Please enter your first name.";
$lang['please_enter_your_last_name']        = "Please enter your last name.";
$lang['please_enter_your_email']            = "Please enter your email.";
$lang['please_enter_your_username']         = "Please enter your user name.";
$lang['please_provide_a_password']          = "Please provide a password.";
$lang['your_password_must_be_long']         = "Your password must be at least 6 characters long.";
$lang['please_enter_the_same_value_again']  = "Please enter the same value.";
$lang['please_agree']                       = "Please accept our policy.";
$lang['use profiles mapping'] = "Use profiles mapping";
$lang['Categories'] = "Categories";
$lang['Choose Categories'] = "Choose categories";
$lang['Expand all'] = "Expand all";
$lang['Collapse all'] = "Collapse all";
$lang['Check all'] = "Check all";
$lang['Uncheck all'] = "Uncheck all";
$lang['You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.'] = 'You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.';
$lang['Profiles mapping'] = "Profiles mapping";
$lang['Categories'] = "Categories";
$lang['Categories'] = "Categories";
$lang['Prices Mapping'] = "Prices mapping";
$lang['Add a profile to the list'] = "Add a profile to the list";
$lang['Profile(s)'] = "Profile(s)";
$lang['Default Profile'] = "Default profile";
$lang['Profile name'] = "Profile name";
$lang['Rule'] = "Rule";
$lang['Add Rule Item(s)'] = "Add rule item(s)";
$lang['Default Price Modifier'] = "Default price modifier";
$lang['Default Profile'] = "Default profile";
$lang['Do you want to delete'] = "Do you want to delete";
$lang['Delete successfully'] = "Delete successfully";
$lang['deleted'] = "deleted";
$lang['can not delete'] = "cannot delete";

$lang['default profiles mapping'] = "Default profiles mapping";
$lang['Use a familiar name to remember it.'] = "Use a familiar name to remember it.";
$lang['category_save'] = "Category save";
$lang['configuration_save'] = "Configuration save";
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";

//new
$lang['Add profile name first'] = "Add profile name first";
$lang['Delete successful'] = "Delete successful";
$lang['Deleted'] = "Deleted";
$lang['Can not delete'] = "Can not delete";
$lang['Profiles Mapping'] = "Profiles Mapping";
$lang['Default profiles mapping'] = "Default profiles mapping";
$lang['Choose categories'] = "Choose categories";
$lang['Add a profile to the list'] = "Add a profile to the list";
$lang['Remove a profile from list'] = "Remove a profile from list";
$lang['Please enter rule name first'] = "Please enter rule name first";
$lang['Please enter unique rule name'] = "Please enter unique rule name";
$lang['Add Rule Items(s)'] = "Add Rule Items(s)";