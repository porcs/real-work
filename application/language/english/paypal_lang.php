<?php

/*
* PayPal Configuration
*/
$lang['cfg_page_title'] = "Payment Configuration";
$lang['cfg_tab_settings'] = "Settings";
$lang['cfg_tab_preapproval'] = "PayPal pre-approval Profile";
$lang['cfg_username'] = "Username";
$lang['cfg_email'] = "Email";
$lang['cfg_regdate'] = "Joined";
$lang['cfg_preapproval'] = "PayPal pre-approval";
$lang['cfg_preapproval_detail'] = "Pre-approved payments, in which a sender logs into PayPal and sets up pre-approved payments. The sender logs into Paypal.com once to set up the pre-approval. Once the sender agrees to the pre-approval, specific approval is no longer necessary.";
$lang['cfg_preapproval_link_text'] = "See details.";
$lang['cfg_preapproval_key'] = "Pre-approval Key";
$lang['cfg_preapproval_active'] = "Active";
$lang['cfg_preapproval_title'] = "Pre-approval Title";
$lang['cfg_preapproval_email'] = "Payer email";
$lang['cfg_preapproval_approved'] = "Approved";
$lang['cfg_preapproval_maxpay'] = "Max number of payments";
$lang['cfg_preapproval_curpay'] = "Current payments";
$lang['cfg_preapproval_remainpay'] = "Remaining payments";
$lang['cfg_preapproval_amountpay'] = "Current payment amount";
$lang['cfg_preapproval_maxamount'] = "Max amount per payment";
$lang['cfg_preapproval_maxallamount'] = "Max total amount of all payments";
$lang['cfg_preapproval_exp'] = "Expiration date";
$lang['cfg_preapproval_date'] = "Approved date";
$lang['cfg_btn_save'] = "Save settings";
$lang['cfg_message_complete'] = "Save complete!";
$lang['finish'] = "Finish";

/*
* PayPal Preapproval agreement
*/
$lang['pagree_page_title'] = "PayPal pre-approval agreement";
$lang['pagree_title_name'] = "Title";
$lang['pagree_title_detail'] = "Feed.biz pre-approval payment";
$lang['pagree_start_date'] = "Start date";
$lang['pagree_end_date'] = "End date";
$lang['pagree_max_per'] = "Max amount per payment";
$lang['pagree_total'] = "Total amount";
$lang['pagree_pay_round'] = "Max number of payments";
$lang['pagree_btn_agree'] = "Agree and continue";
$lang['pagree_btn_disagree'] = "Disagree";

/*
* PayPal Checkout
*/
$lang['checkout_page_title'] = "Checkout form";
$lang['checkout_tab_step1'] = "Order information";
$lang['checkout_tab_step2'] = "Billing information";
$lang['checkout_tab_step3'] = "Payment";
$lang['checkout_tab_step4'] = "Complete";
$lang['checkout_order_pkg_id'] = "Package ID";
$lang['checkout_order_pkg_num'] = "Num";
$lang['checkout_order_pkg_name'] = "Package name";
$lang['checkout_order_pkg_amount'] = "Amount";
$lang['checkout_order_pkg_total_amount'] = "Total amount";
$lang['checkout_order_pkg_period'] = "Period";
$lang['checkout_order_pkg_pre_no'] = "No";
$lang['checkout_order_pkg_pre_three'] = "3 months";
$lang['checkout_order_pkg_pre_six'] = "6 months";
$lang['checkout_order_pkg_pre_nine'] = "9 months";
$lang['checkout_order_pkg_pre_year'] = "1 year";
$lang['checkout_bill_use'] = "Use billing address from user profile.";
$lang['checkout_pay_method'] = "Choose your payment method";
$lang['checkout_pay_credit'] = "Pay with a credit card";
//$lang['checkout_pay_express'] = "Pay with PayPal express checkout";
$lang['checkout_pay_express'] = "Pay only this bill";
//$lang['checkout_pay_preapproval'] = "Pay with PayPal pre-approval";
$lang['checkout_pay_preapproval'] = "Pay automatically each month";
$lang['checkout_pay_cc_ccnum'] = "Credit card number";
$lang['checkout_pay_cc_cvv'] = "CVV";
$lang['checkout_pay_cc_firstname'] = "First name";
$lang['checkout_pay_cc_lastname'] = "Last name";
$lang['checkout_pay_cc_exp'] = "EXP";
$lang['checkout_pay_cc_street'] = "Street";
$lang['checkout_pay_cc_city'] = "City";
$lang['checkout_pay_cc_state'] = "State";
$lang['checkout_pay_cc_country'] = "Country";
$lang['checkout_pay_cc_zip'] = "Zip";
$lang['checkout_pay_cc_month'] = "Month";
$lang['checkout_pay_cc_year'] = "Year";
$lang['checkout_btn_print'] = "Print";
$lang['checkout_btn_prev'] = "Prev";
$lang['checkout_btn_next'] = "Next";
$lang['checkout_btn_finish'] = "Finish";
$lang['checkout_confirm'] = "Remove this package?";
$lang['checkout_title_alert'] = "Can not complete your payment.";
$lang['checkout_alert'] = "This transaction is declined.";

/*
* PayPal Complete
*/
$lang['complete_title'] = "Your packages profile has been saved.";
$lang['complete_detail'] = "You just complete the package profile.";
$lang['complete_pay_title'] = "You just completed your payment.";
$lang['complete_pay_detail'] = "Thanks for your order. Your billing ID for this payment is :";
$lang['complete_finish'] = "Thank you ! Your information was successfully saved !";
$lang['complete_service_name'] = "Feed.biz package service";

/*
* PayPal History
*/
$lang['history_title'] = "Payment history";
$lang['history_table_datetime'] = "Date/Time";
$lang['history_table_title'] = "Billing Title";
$lang['history_table_status'] = "Status";
$lang['history_table_method'] = "Payment method";
$lang['history_paypal_credit'] = "Credit card";
$lang['history_paypal_express'] = "PayPal express checkout";
$lang['history_paypal_preapproval'] = "PayPal pre-approval";
$lang['history_paypal_logdetail'] = "Log detail";

/*
* PayPal Download
*/
$lang['downl_list_title'] = "Download invoice list";
$lang['downl_title'] = "Download invoice";
$lang['downl_table_id'] = "Invoice ID";
$lang['downl_table_title'] = "Invoice Detail";
$lang['downl_table_amount'] = "Amount";
$lang['downl_table_date'] = "Date/Time";
$lang['downl_table_method'] = "Payment Method";
$lang['downl_btn_print'] = "Print Invoice";
$lang['downl_bill_active'] = "Active date";
$lang['downl_bill_expire'] = "Expire date";

/*
* Select Packages
*/
$lang['package_title'] = "Packages";
$lang['package_region_select'] = "Select region";
$lang['package_region'] = "Choose a region";
$lang['package_region_detail'] = "Choose a region on which you would like to sell";
$lang['package_region_currency'] = "Please select currency";
$lang['package_package'] = "Choose your packages";
$lang['package_customize'] = "Customize your packages";
$lang['package_overview'] = "Packages overview";
$lang['package_summary'] = "Packages summary";
$lang['package_gift'] = "Gift voucher code";
$lang['package_options'] = "Options";
$lang['package_preplanning'] = "Package pre-planning";
$lang['package_info'] = "Billing information";
$lang['package_info_title'] = "Enter the following information";
$lang['package_payment'] = "Payment method";
$lang['package_complete'] = "Complete payment";
$lang['package_wizard_region'] = "Region";
$lang['package_wizard_package'] = "Packages";
$lang['package_wizard_info'] = "Information";
$lang['package_wizard_payment'] = "Payment";
$lang['package_wizard_complete'] = "Complete";
$lang['package_btn_send'] = "Send";
$lang['package_offer_title'] = "Offer packages";
$lang['package_select_referral'] = "Referral discount";
$lang['package_select_discount_date'] = " Discount for until next pay day : ";
$lang['package_select_subtotal'] = "Sub total";
$lang['package_select_total'] = "Total";

$lang['currency'] = "Currency";
/*
* Preapproval
*/
$lang['preapproval_inactive'] = "Your pre-approval key is inactive";
$lang['preapproval_inactive_btn'] = "Change it now.";
$lang['preapproval_active'] = "Your pre-approval key is available.";
$lang['preapproval_no'] = "You have no pre-approval key, create it now.";
$lang['preapproval_date'] = "We will automatically charge you every <b>{ch_date}</b> of each month.";
$lang['express_date'] = "We will notify you when your packages expire.";

//bill & payment
$lang['Expired'] = "Expired";
$lang['Purchased'] = "Purchased";
$lang['per month'] = "per month";
$lang['New'] = "New";
$lang['Extend'] = "Extend";
$lang['days'] = "Days";
$lang['service'] = "Service";
$lang['packages'] = "Packages";
$lang['complete'] = "Complete";
$lang['billing'] ="Billing";
$lang['download'] ="Download";
$lang['amazon'] = "Amazon";
$lang['payment'] = "Payment";
$lang['Mode'] = "Mode";
$lang['Currency'] = "Currency";

$lang['note_expert_mode_notify'] = "*Please note that you are using the expert mode. You will be charged an additional &euro;5 per month.";
$lang['per/month'] = "per/month";
$lang['Show / Hide Columns'] = "Show / Hide Columns";
$lang['Click here to print'] = "Click here to print";
$lang['paybillall'] = "paybillall";
$lang['extend'] = "extend";
$lang['MasterCard'] = "MasterCard";
$lang['Visa'] = "Visa";
$lang['Discover'] = "Discover";
$lang['Amex'] = "Amex";
$lang['January'] = "January";
$lang['February'] = "February";
$lang['March'] = "March";
$lang['April'] = "April";
$lang['May'] = "May";
$lang['June'] = "June";
$lang['July'] = "July";
$lang['August'] = "August";
$lang['September'] = "September";
$lang['October'] = "October";
$lang['November'] = "November";
$lang['December'] = "December";
$lang['images'] = "images";
$lang['transaction'] = "transaction";
$lang['favicon.ico'] = "favicon.ico";
$lang['configuration'] = "configuration";
$lang['preapproval'] = "preapproval";
$lang['agreement'] = "agreement";
$lang['Feed.biz service'] = "Feed.biz service";
$lang['for livetime'] = "for lifetime";
$lang['hove_not_select_pkg'] = "You have not select any packages.";