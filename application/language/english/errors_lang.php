<?php

$lang['page_title']                         = "Error";
$lang['Error Message']                      = "Error message";
$lang['We found some error, on']            = "We found some error, on";
$lang['this error has send to admin']       = "This error was sent to admin";
$lang['We found an error, on']             = "We found an error, on";
$lang['errors']                             = "errors";
$lang['getErrorHandler']                    = "getErrorHandler"; 

$lang['Oops...'] = "Oops...";
$lang['the page you are looking for cannot be found'] = "the page you are looking for cannot be found";
$lang['Errors can be caused due to various reasons, such as:'] = "Errors can be caused due to various reasons, such as:";
$lang['Page requested does not exist.'] = "Page requested does not exist.";
$lang['Server is down.'] = "Server is down.";
$lang['Internet connection is down.'] = "Internet connection is down.";
$lang['Broken links'] = "Broken links";
$lang['Incorrect URL'] = "Incorrect URL";
$lang['Page has been moved to a different address'] = "Page has been moved to a different address";
$lang['Back'] = "Back";