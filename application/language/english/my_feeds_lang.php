<?php

$lang['page_title']                     = "My feeds";
$lang['my_feeds']                       = "My feeds";
$lang['main_configuration']             = "Main configuration";
$lang['standard_feed_source']           = "Source : Feed.biz module";
$lang['prestashop']                     = "Prestashop";
$lang['magento']                        = "Magento";
$lang['opencart']                       = "OpenCart";
$lang['os_commerce']                    = "OS Commerce";
$lang['custom_feed_source']             = "Source : Custom";
$lang['gmerchant']                      = "Google merchant center";
$lang['feed_source']                    = "Feed source";
$lang['feed_url']                       = "Feed URL";
$lang['feed_type']                      = "Feed type";
$lang['product']                        = "Product";
$lang['offer']                          = "Offer";
$lang['offers']                         = "Offers";
$lang['language']                       = "Feed language";
$lang['iso']                            = "ISO";
$lang['iso_code']                       = "ISO code";
$lang['product_url']                    = "Product URL";
$lang['reset']                          = "Reset";
$lang['save']                           = "Save";
$lang['products']                       = "Products";
$lang['shops']                          = "Shops";
$lang['configuration']                  = "Configuration";
$lang['data_source']                    = "Data Source";
$lang['Shops']                          = "Shops";

//general page
$lang['Warning'] = "Warning";
$lang['You must set general mode.'] = "You must set general mode.";
$lang['Mode'] = "Mode";
$lang['Authentification'] = "Authentication";
$lang['Beginner'] = "Beginner";
$lang['Choose Beginner mode'] = "Basic marketplace package";
$lang['Choose Expert mode'] = "Provides optional functions e.g. price rule, price modification.";
$lang['Default Mode'] = "Default mode";
$lang['Choose Advanced mode'] = "Provides advanced marketplace options e.g. sandbox.";
$lang['Username'] = "User Name";
$lang['Use token when you export'] = "Use token when you export.";
$lang['Token'] = "Token";
$lang['Save & continue'] = "Save & Continue";
$lang['Token'] = "Token";
$lang['Errors'] = "Errors";
//shop page
$lang['Default Shop'] = "Default shop";
$lang['No shop in database!'] = "No shop in database !";
$lang['please_choose_feed_source'] = "Please choose a feed source";
$lang['Do you want to change default shop'] = "Do you want to change default shop?";
$lang['Cannot change shop offer'] = "Cannot change shop offers";
$lang['Cannot change shop product'] = "Cannot change shop products";

//data source
$lang['xml'] = "xml";
$lang['csv'] = "csv";
$lang['URL'] = "URL";
$lang['Google merchant - Basic settings'] = "Google merchant - Basic settings";
$lang['Default'] = "Default";
$lang['Currency code'] = "Currency code";
$lang['Import mode'] = "Import mode";
$lang['Auto importing'] = "Auto importing";
$lang['Recommend'] = "Recommended";
$lang['Feed.biz will periodically update your products'] = "Feed.biz will periodically update your products";
$lang['Manual import'] = "Manual import";
$lang['Your products will be updated after clicking the save button.'] = "Your products will be updated after clicking the save button.";
$lang['Are you sure to change mode'] = "Are you sure about changing mode?";
$lang['Please choose mode'] = "Please choose a mode";
$lang['All importing are completed.'] = "All imports have been successfully completed.";
$lang['Feed data'] = "Feed data";
$lang['Import history'] = "Import history";
$lang['time'] = "time";
$lang['Feed'] = "Feed";
$lang['waiting'] = "waiting";
/*status*/
$lang['my_feeds']                       = "My feeds";
$lang['status']                         = "Status";
$lang['shop_name']                      = "Shop name";
$lang['run_by']                     = "Run by";
$lang['system']                         = "System";
$lang['user']                           = "User";
$lang['task']                           = "Task";

/*Parameter */
// Category
$lang['parameters']                     = "Parameters";
$lang['category']                       = "Category";
$lang['please_config']                  = "Please configure your feed source.";
$lang['choose_categories']              = "Choose categories.";
$lang['expand_all']                     = "Expand all";
$lang['collapse_all']                   = "Collapse all";
// Carriers
$lang['carriers']                       = "Carriers";
$lang['choose_carriers']                = "Choose carriers.";
$lang['check_all']                      = "Check all";
$lang['uncheck_all']                    = "Uncheck all";

//Mapping
$lang['mapping']                        = "Mapping";
$lang['attributes']                     = "Attributes";
$lang['choose_a_attribute']             = "Choose an attribute";
//characteristics
$lang['characteristics']                = "Characteristics";
$lang['enter_a_characteristics']        = "Enter a characteristics";
//manufacturers
$lang['manufacturers']                  = "Manufacturers";
$lang['choose_a_manufacturer']          = "Choose a manufacturer";
$lang['Enter Appropriate manufacture name']          = "Enter Appropriate manufacture name";

/*Filter*/
$lang['filters']                        = "Filters";
$lang['suppliers']                      = "Suppliers";
$lang['manufacturers']                  = "Manufacturers";
$lang['Manufacturers']                  = "Manufacturers";
$lang['excluded_manufacturers']         = "Excluded manufacturers";
$lang['included_manufacturers']         = "Included manufacturers";
$lang['excluded_suppliers']             = "Excluded suppliers";
$lang['included_suppliers']             = "Included suppliers";

/*Price rules*/
$lang['price_rules']                    = "Price rules.";

//message
$lang['form_validation_false']          = "form_validation_false";
$lang['validation_upload_label']        = "validation_upload_label";
$lang['upload_successful']              = "Upload successful";
$lang['importing']                      = "Please wait while importing ";
$lang['from']                           = "From ";
$lang['duplicate_domain_with_other_user']              = "Your domain was used by another user.";
$lang['save_unsuccessful']              = "Save unsuccessful.";
$lang['save_successful']                = "Save successful.";
$lang['main_configuration_save']        = "";
$lang['getSelectedCategories']          = "";
$lang['filters_save']                   = "";
$lang['attributes_save']                = "";
$lang['characteristics_save']           = "";
$lang['manufacturers_save']             = "";
$lang['category_save']                  = "";
$lang['Change default shop successful'] = "Default shop changed successfully";
$lang['incorrect_user']                 = "Incorrect user";
$lang['please_wait_for_verifying']      = "Please wait while verifying";
$lang['verify']                         = "Verify";
$lang['please_check_url_and_verify_again'] = "Please check URL and verify again";
$lang['can_not_connect_server']         = "Cannot connect to your server, try again";
$lang['please_paste_your_uri_connector']= "Paste the URL provided by your Feed.biz module here";
$lang['save_feed_data']                 = "Save";
$lang['URL provided by your Feed.biz module'] = "URL provided by your Feed.biz module";
$lang['Verified'] = "Verified";
$lang['Verify'] = "Verify";
$lang['Import offers now']=  "Import offers now";
$lang['Import products now']=  "Import products now";
$lang['Import NOW']=  "Import now";

/*tasks*/
$lang['your_website_has_not_verified']  ="Your website has not been verified";
$lang['you_have_just_updated_your_feed_please_wait_for_a_few_minutes']  ="You have just updated your feed and the processes are still busy. Please wait for a few minutes..";
$lang['you_have_update_too_often_wait_for_xxx_mins']  ="You have updated too often. Please wait for xxx mins.";
$lang['you_have_just_update_feed_please_wait_for_xxx_mins']  ="You have just updated your feed. Please wait for xxx mins.";
$lang['you_have_just_update_product_please_wait_for_xxx_mins']  ="You have just updated your product. Please wait for xxx mins.";
$lang['you_have_just_update_offer_please_wait_for_xxx_mins']  ="You have just updated your offer. Please wait for xxx mins.";
$lang['You have just update your %s. Please wait for %s mins.']  ="You have just updated your %s. Please wait for %s mins.";
$lang['You have update to often. Please wait for %s mins.']  ="You have updated to often. Please wait for %s mins.";
$lang['your_offer_are_importing'] = "Your offers are imported";//not found in using
$lang['Access Denied.'] = "Access Denied.";//new
$lang['Not found your shop.'] = "Not found your shop.";//new

//statistic product
$lang['Reference'] = "Reference";
$lang['Category'] = "Category";
$lang['Quantity'] = "Quantity";
$lang['Price'] = "Price";
$lang['price'] = "Price";
$lang['No data source.'] = "No data source.";
$lang['statistics'] = "Statistics";
//log batches page
$lang['Last Import Log'] = "Last Import Log";
$lang['dashboard']                          = "Dashboard";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";
$lang['welcome']                            = "Welcome";
$lang['validation'] = 'Validation';
$lang['Log'] = 'Log';
$lang['Last import log'] = 'Last import log';
$lang['Batch ID'] = 'Batch ID';
$lang['Source'] = 'Source';
$lang['Shop'] = 'Shop';
$lang['Type'] = 'Type';
$lang['Total'] = 'Total';
$lang['No. Successful'] = 'No. Successful';
$lang['No. Warning'] = 'No. Warning';
$lang['No. Error'] = 'No. Error';
$lang['Import Date'] = 'Import date';

$lang['messages'] = "messages";
$lang['Search batch ID'] = "Search batch ID";//not found
$lang['Search Product ID'] = "Search product ID";//not found


$lang['All'] = "All";
$lang['Error'] = "Error";
$lang['Search message'] = "Search message";//not found in using
$lang["Search date"] = "Search date";//not found using
$lang['Warnings'] = "Warnings";
//parameter
$lang['Please choose default shop.'] = "Please choose default shop.";
$lang['Warning!'] = "Warning!";
$lang['Add a price to the list'] = "Add a price to the list";
$lang['Price(s)'] = "Price(s)";
$lang['Price name'] = "Price name";
$lang['Price modifier'] = "Price modifier";
$lang['Percentage'] = "Percentage";
$lang['Allow only digits.'] = "Allow only digits.";
$lang['Rounding'] = "Rounding";
$lang['One digit'] = "One digit";
$lang['Two digit']= "Two digit";
$lang['None']= "None";
$lang['Value']= "Value";
$lang['Use a familiar name to remember it.'] = "Use a familiar name to remember it.";
$lang['Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).']= "Formula to be applied on all the exported product prices (multiplication, division, addition, subtraction, percentages).";
$lang['Apply a specific price formula to selected categories which will override the main setting.']= "Apply a specific price formula to selected categories which will override the main setting.";
$lang['Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required']= "Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
$lang['Do you want to delete']= "Do you want to delete";
$lang['Delete successful']= "Delete successful";
$lang['Deleted']= "Deleted";
$lang['Error']= "Error";
$lang['Can not delete']= "Can not delete";

$lang['Rules'] = "Rules";
$lang['Add a rule to the list'] = "Add a rule to the list";

$lang['Rule name'] = "Rule name";
$lang['Add rule name first'] = "Add rule name first";
$lang['Add item'] = "Add item";
$lang['Item(s)'] = "Item(s)";
$lang['Manufacturer'] = "Manufacturer";
$lang['Supplier'] = "Supplier";
$lang['Price range'] = "Price range";
$lang['Action'] = "Action";
$lang['To'] = "To";
$lang['Summary'] = "Summary";
$lang['Manufacturer'] = "Manufacturer";
$lang['Add rule name first'] = "Add rule name first";
$lang['Drop'] = "Drop";
$lang['Messages log'] = "Messages log";//new
$lang['Messages product'] = "Messages product";//new
$lang['Messages offer'] = "Messages offer";//new

$lang['Are you sure to delete this price range'] = "Are you sure to delete this price range?";
$lang['Price range error'] = "Price range error";
$lang['Successfully'] = "Successfully";
$lang['Delete was successful'] = "Delete was successful";
$lang['Delete was unsuccessful'] = "Delete was unsuccessful";
$lang['Are you sure to delete rule'] = "Are you sure to delete rule?";
$lang['Are you sure to delete rule item'] = "Are you sure to delete rule item?";
$lang['Records'] = "Records";
$lang['Your process are running. Please wait.'] = "Process is running.";
$lang['log'] = "log";
$lang['batches'] = "batches";
$lang['Product ID'] = "Product ID";
$lang['Default profiles mapping'] = "Default profiles mapping";
$lang['Carriers_save'] = "Saving carriers";//not found in using
$lang['log_download'] = "log_download";
$lang['Condition'] = "Condition";
$lang['Conditions'] = "Conditions";
$lang['reset_condition'] = "reset_condition";
$lang['next'] = "Next";
$lang['condition_save'] = "condition_save";
$lang['condition'] = "Condition";
$lang['Marketplace condition'] = "Marketplace condition";//not found in using
$lang['Your conditions'] = "Your conditions";
$lang['profiles'] = "Profiles";
$lang[' Percentage '] = " Percentage ";
$lang[' Do you want to delete '] = " Do you want to delete ";
$lang[' Deleted '] = " Deleted ";
$lang[' Error '] = " Error ";
$lang[' Can not delete '] = " Cannot delete ";
$lang['System can not download data. Please try again'] = "System can not download data. Please try again";
$lang['System can not download some part of your data.'] = "System can not download some part of your data.";

$lang['general_save'] = "General save";
$lang['data_source_save'] = "Data source save";
$lang['price_save'] = "Price save";
$lang['iframe_popup_conf'] = "iframe popup conf";
$lang['verify_feed'] = "Verify feed";
$lang['your_feed_are_importing'] = "Your feed are importing.";
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";
$lang['Expert Mode will be charged xx per month'] = "Expert Mode will be charged %s per month";

$lang['Please choose category in'] = 'Please choose category in';
$lang['My feeds > Parameters > Offers > Category'] = 'My feeds > Parameters > Offers > Category';
$lang['My feeds > Parameters > Profiles > Category'] = "My feeds > Parameters > Profiles > Category";
$lang['my_shop'] = "My shop";
$lang['connect'] = "Connect";
$lang['Import history'] = "Import history";
$lang['import_history'] = "Import history";
$lang['Import my catalog'] = "Import my catalog";
$lang['connect_save'] = "Connect save";
$lang['Mode'] = "Mode";
$lang['Information'] = "Information";
$lang[' Percentage '] = " Percentage ";
$lang[' Delete successfully '] = " Deleted successfully ";
$lang[' Do you want to delete '] = " Do you want to delete ";

$lang[' Error '] = " Error ";
$lang[' Can not delete '] = " Cannot delete ";
$lang['Mode'] = "Mode";

$lang['Feed.biz will periodically update your products'] = "Feed.biz will periodically update your products";//new
$lang['Security token used between your Shop and Feed.biz'] = "Security token used between your Shop and Feed.biz";//new

//new word (did not verify) (my shop page)
$lang['All categories'] = "All categories";//new
$lang['All conditions'] = "All conditions";//new
$lang['my_products'] = "My products";
$lang['My products'] = "My products";//not found in using
$lang['Filter by category'] = "Filter by category";
$lang['Filter by conditions'] = "Filter by conditions";
$lang['All categories'] = "All categories";
$lang['All conditions'] = "All conditions";
$lang['Product name > SKU'] = "Product name > SKU";
$lang['> Listing'] = "> Listing";
$lang['Variants'] = "Variants";
$lang['My_product_data'] = "My product data";//not found in using
$lang['getJsonCategory'] = "Get Json category";
$lang['Module'] = "Module";//new
$lang['Extension'] = "Extension";//new
$lang['Prestashop'] = "Prestashop";//new
$lang['Download'] = "Download";//new
$lang['Opencart'] = "Opencart";//new
$lang['Shopify'] = "Shopify";//new
$lang['Magento'] = "Magento";//new
$lang['Install'] = "Install";//new
$lang['Woo Commerce'] = "Woo Commerce";//new
$lang['Feed Data'] = "Feed Data";
$lang['Auto Importing'] = "Auto Importing";
$lang['Manual Importing'] = "Manual Importing";
$lang['my_product_data'] = "my_product_data";
$lang['carriers_save'] = "carriers_save";
$lang['Rule name'] = "Rule name";
$lang['Are you sure to delete this price range'] = "Are you sure to delete this price range?";
$lang['Add rule name first'] = "Add rule name first";
$lang['Add profile name first'] = "Add profile name first";
$lang['Delete successful'] = "Deleted successfully";
$lang['Deleted'] = "Deleted";
$lang['Can not delete'] = "Cannot delete";
$lang['rofiles Mapping'] = "profiles mapping";
$lang['To'] = "To";
$lang['rules_item_price_range_delete'] = "Rules item price range delete";
$lang['rules_delete'] = "Rules delete";
$lang['rules_delete'] = "Rules delete";
$lang['Profiles Mapping'] = "Profiles mapping";
$lang['Rule name'] = "Rule name";
$lang['Add rule name first'] = "Add rule name first";
$lang['To'] = "To";

$lang['Default profiles mapping'] = "Default profiles mapping";
$lang['Access Denied.'] = "Access Denied.";
$lang['Empty Data'] = "Empty Data";
$lang['XML DOM Parse Error'] = "XML DOM Parse Error";
$lang['Setting Feed Error'] = "Setting Feed Error";

$lang['empty_connector_data'] = "Empty data from this connector. Please check URL and try again.";
$lang['data_connect'] = "Data connect";
$lang['data_configuration'] = "Data configuration";
$lang['Show / Hide Columns'] = "Show / Hide Columns";

$lang['Run by'] = "Run by";
$lang['messages_page'] = "messages";
$lang['Next'] = "Next";
$lang['import_history_page'] = "import history page";
$lang['+ Add Rule Items(s)'] = "+ Add Rule Items(s)";

/*product_options*/ //22-05-2015
$lang['offers_options'] = 'Offers Options';
$lang['General mode'] = "General mode";
$lang['Feed.biz will periodically update your products'] = "Feed.biz will periodically update your products";
$lang['amazon'] = "Amazon";
$lang['ebay'] = "eBay";