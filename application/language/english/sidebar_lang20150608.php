<?php

$lang['dashboard']  = "Dashboard";
$lang['my_account']  = "My Account";
$lang['edit_profile']  = "Edit My profile";
$lang['edit_billing']  = "Edit billing information";
$lang['affiliation'] = "Affiliation";
$lang['afflist'] = "Affiliate lists";
$lang['affstat'] = "Statistics";
$lang['affinvite'] = "Invitations";
$lang['billing']  = "Billing";
$lang['payment_configuration']  = "Payment configuration";
$lang['packages']  = "Packages";
$lang['download_bill']  = "Download Invoice";
$lang['pay_expire_bill']  = "Extend your packages";
$lang['my_feed']  = "My Feeds";
$lang['configuration']  = "Configuration";
$lang['general']  = "General";
$lang['data_source']  = "Data source";
$lang['shop']  = "Shop";
$lang['xml']  = "XML";
$lang['csv']  = "CSV";
$lang['url']  = "URL";
$lang['parameters']  = "Parameters";
$lang['Parameters']  = "Parameters";
$lang['products']                       = "Products";
$lang['offers']                       = "Offers";
$lang['category']  = "Category";
$lang['feed_enhancement']  = "Enhancement";
$lang['rules']  = "Rules";
$lang['mapping']  = "Mapping";
$lang['mappings']  = "Mappings";
$lang['attributes']  = "Attributes";
$lang['characteristics']  = "Characteristics";
$lang['manufacturers']  = "Manufacturers";
$lang['filters']  = "Filters";
$lang['carriers']  = "Carriers";
$lang['price_rules']  = "Price rules";
$lang['statistics']  = "Statistics";
$lang['nb_products']  = "Products";         
$lang['nb_manufacturers']  = "Manufacturers";
$lang['nb_categories']  = "Categories";
$lang['nb_suppliers']  = "Suppliers";
$lang['nb_carriers']  = "Carriers";
$lang['marketplace'] = "Marketplaces";
$lang['ebay']                                   = "Ebay";
$lang['nb_auth_security']                       = "Authentication";
$lang['nb_integration']                         = "Integration";
$lang['nb_auth_setting']                        = "Settings";
$lang['nb_ebay_tax']                            = "Tax";
$lang['nb_ebay_mapping']                        = "Mapping";
$lang['nb_ebay_mapping_attributes']             = "Attributes";
$lang['nb_ebay_mapping_attributes_value']       = "Attributes value";
$lang['nb_ebay_mapping_carriers']               = "Carriers";
$lang['nb_ebay_mapping_carriers_cost']          = "Carriers rules";
$lang['nb_ebay_mapping_categories']             = "Categories";
$lang['nb_ebay_statistics_product']             = "Batch log details";
$lang['nb_ebay_error_resolutions']              = "Error Resolutions";
$lang['nb_ebay_templates']                      = "Templates";
$lang['nb_ebay_univers']                        = "Universe";
$lang['nb_ebay_log']                            = "Batch log";
$lang['nb_synchronization']                     = "Synchronization";
$lang['nb_orders']                              = "Orders import log";
$lang['nb_ebay_mapping_condition']              = "Condition";
$lang['Condition']                              = "Condition";
$lang['conditions']                             = "Conditions";
$lang['nb_ebay_advance']                        = "Advanced settings";
$lang['nb_ebay_price_modifier']                 = "Price modifier";
$lang['nb_payment_method']                      = "Payment";
$lang['nb_return_method']                       = "Return";
$lang['nb_ebay_attribute_univers']              = "Attribute universe";
$lang['nb_ebay_actions_products']               = "Products";
$lang['nb_ebay_actions_offers']                 = "Offers";
$lang['nb_ebay_actions_orders']                 = "Imports orders";
$lang['nb_ebay_variation']                      = "Variation";
$lang['nb_ebay_variation_values']              	= "Variation values";


$lang['amazon'] = "Amazon";
$lang['models'] = "Models";
$lang['profiles'] = "Profiles";
$lang['category'] = "Category";
$lang['Mappings'] = "Mappings";
$lang['validation'] = "Validation";
$lang['log'] = "Log";
$lang['Logs'] = "Logs";
$lang['error'] = "Error";

$lang['Batches'] = "Batches";
$lang['Price Modifier']= "Price modifier";
$lang['Category']= "Category";
$lang['Profiles'] = "Profiles";
$lang['Message'] = "Message";
$lang['Messages'] = "Messages";
$lang['Task'] = "Task";
$lang['Status'] = "Status";
$lang['Actions'] = "Actions";
$lang['Report'] = "Report";
$lang['You must feed data source before use this menu.'] = "You must configure your data source in configuration tab first.";
$lang['Orders'] = "Orders";
$lang['my_shop'] = "My Shop";
$lang['connect'] = "Connect"; 
$lang['my_products'] = "My Products";
$lang['Import history'] = "Import history";
$lang['import_history'] = "Import history";
$lang['conditions'] = "Conditions";
$lang['Mode'] = "Mode";

// Stock 20/03/2015
$lang['stock'] = "Stock";
$lang['Please wait'] = 'Please wait';
$lang['Stock'] = "Stock";
$lang['Per Page'] = "Per Page";
$lang['images'] = 'images';
$lang['favicon.ico'] = 'favicon.ico';

//Error Resolution
$lang['Error Resolutions'] = 'Error Resolutions';


$lang['Features'] = "Features";
$lang['Scheduled Tasks'] = "Scheduled Tasks";
$lang['Product Options'] = 'Product Options';
$lang['Product'] = 'Product';
$lang['by Product'] = 'by Product';
$lang['by Category'] = 'by Category';

