<?php

//automaton.php
$lang['Unable to init Amazon Service']= 'Unable to initialize Amazon service.';
$lang['Inventory processing completed']= 'Inventory processing completed.';
$lang['Amazon Marketplace Automaton started on']= 'Amazon marketplace automaton started on';
$lang['Inventory processing not complete, the process will restart in %s hours']= 'Inventory processing not complete, the process will restart in %s hours.';
$lang['File downloaded successfully, wait for processing']= 'File downloaded successfully, wait for processing.';
$lang['Unable to find any inventory, the process will restart in %s hours.']= 'Unable to find any inventory, the process will restart in %s hours.';
$lang['The inventory is ready, wait for processing.']= 'The inventory is ready, wait for processing.';
$lang['Wait for the inventory, it should take a while, up to one hour']= 'Wait for the inventory, it should take a while, up to one hour.';
$lang['Unable to find any inventory, the process will restart in 2 hours']= 'Unable to find any inventory, the process will restart in 2 hours.';
$lang['The inventory is ready, wait for processing']= 'The inventory is ready, wait for processing.';
$lang['Wait for the inventory, it should take a while, up to one hour']= 'Wait for the inventory, it should take a while, up to one hour.';
$lang['Unable to find any inventory, the process will restart in 2 hours']= 'Unable to find any inventory, the process will restart in 2 hours.';
$lang['Requesting an inventory to Amazon']= 'Requesting inventory from Amazon.';
$lang['Inventory file exists already and is not expired, reprocessing this feed']= 'Inventory file exists already and is not expired, reprocessing this feed...';
$lang['Request has been accepted, wait for processing']= 'Request has been accepted, wait for processing.';
$lang['Request failed, the request will be resubmitted in a while']= 'Request failed, the request will be resubmitted in a while.';
$lang['XML GetMatchingProductForIdResult was expected']= 'XML GetMatchingProductForIdResult was expected.';
$lang['XML was expected']= 'XML was expected.';
$lang['Failed']= 'Failed';
$lang['Inventory is empty']= 'Inventory is empty.';
$lang['Unable to write to output file']= 'Unable to write to output file.';
$lang['Unable to read input file']= 'Unable to read input file.';
$lang['Unable to save report']= 'Unable to save report.';
$lang['Unable to create path']= 'Unable to create path.';
$lang['Unable to create import directory']= 'Unable to create import directory.';

//amazon.report.php
$lang['Amazon get report started on']= 'Amazon get report started on';
$lang['Unable to connect Automaton']= 'Unable to connect automaton.';
$lang['TimeOut, Limit time : s, Process Time : s']= 'TimeOut, limit time : %s, Process time : %s.';
$lang['Success']= 'Success';
$lang['success_remark']= 'Get report successfully.';
$lang['Unable to login']= 'Unable to login.';
$lang['REPORT_REQUEST']= 'REPORT REQUEST';
$lang['GET_REPORT_REQUEST_LIST']= 'GET REPORT REQUEST LIST';
$lang['GET_REPORT']= 'GET REPORT';
$lang['STEP_PROCESS_REPORT']= 'STEP PROCESS REPORT';
$lang['STEP_POST_PROCESS']= 'STEP POST PROCESS';
$lang['Skipped'] = "Skipped";