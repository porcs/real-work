<?php

$lang['page_title']                         = "Amazon";
$lang['amazon']                             = "Amazon";
$lang['models']                             = "Models";
$lang['profiles']                           = "Profiles";
$lang['mappings']                           = "Mappings";
$lang['category']                           = "Category";
$lang['parameters']                         = "Parameters";
$lang['logs']                               = "Logs";
$lang['carriers']                           = "Carriers";
$lang['scheduled_tasks']                    = "Scheduled Tasks";
$lang['actions']                            = "Actions";
$lang['conditions']                         = "Conditions";
$lang['Infomation'] = "Infomation";
$lang['This will send all products having a profile within the selected categories in your module configuration.']
        ='This will send all products having a profile within the selected categories in your module configuration.';
$lang['To add a product to the list, from the product sheet, select "create" in the Amazon options tab.']
        ='To add a product to the list, from the product sheet, select "create" in the Amazon options tab.';
$lang['Once completed, please generate and save the report. (For support, you will need the XML and the report.).']
        ='Once completed, please generate and save the report. (For support, you will need the XML and the report.).';
$lang['Send products to Amazon.'] = 'Send products to Amazon.';
$lang['Sending..'] = 'Sending..';
$lang['Send offers to Amazon.'] = 'Send offers to Amazon.';//$lang['Send Offers To Amazon'] = 'Send Offers To Amazon';//not found in using
$lang['Starting update.'] = 'Starting update.';//$lang['Starting Update'] = 'Starting Update';//not found in using
$lang['Delete products on Amazon.'] = "Delete products on Amazon.";	//$lang['Delete Products To Amazon'] = "Delete Products To Amazon";//not found in using
$lang['Delete offers on Amazon.'] = "Delete offers on Amazon.";	//$lang['Delete Offers To Amazon'] = "Delete Offers To Amazon";//not found in using
$lang['Warning! Configuration parameter is not complete.']
        ="Warning! Configuration parameter is not complete.";
$lang['Choose Categories'] = 'Choose Categories';
$lang['Expand all'] = 'Expand all';
$lang['Collapse all'] = 'Collapse all';
$lang['Check all'] = 'Check all';
$lang['Uncheck all'] = 'Uncheck all';
$lang['My feeds > Parameters > Profiles > Category'] = 'My feeds > Parameters > Profiles > Category';
$lang['You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.']
        = 'You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.';
$lang['Profiles Mapping'] = 'Profiles Mapping';
$lang['save'] = 'Save';
$lang['rest'] = 'Reset';
$lang['Color'] = 'Color';
$lang['Batch ID'] = 'Batch ID';
$lang['Feed submission ID'] = 'Feed submission ID';//$lang['Feed Submission Id'] = 'Feed Submission Id';//not found in using
$lang['Status code'] = 'Status code';//$lang['Status Code'] = 'Status Code';//not found in using
$lang['Messages processed'] = 'Messages processed';//$lang['Messages Processed'] = 'Messages Processed';//not found in using
$lang['Messages successful'] = 'Messages successful';//$lang['Messages Successful'] = 'Messages Successful';//not found in using
$lang['Messages with error'] = 'Messages with error';//$lang['Messages With Error'] = 'Messages With error';//not found in using
$lang['Messages with Warning'] = 'Messages with warning';//$lang['Messages With Warning'] = 'Messages With Warning';//not found in using
$lang['Send date'] = 'Send date';
$lang['Color'] = 'Color';
$lang['platform'] = 'platform';
$lang['Amazon Site'] = 'Amazon site';
$lang['API Setting'] = 'API setting';
$lang['Category'] = 'Category';
$lang['Send Product'] = 'Send product';
$lang['Select Amazon Site'] = 'Select Amazon site';
$lang['Preference'] = 'Preference';
$lang['Allow automatic offer creation.'] = 'Allow automatic offer creation.';
$lang['Quantity.'] = 'Stock.';
$lang['Price.'] = 'Price.';
$lang['Merchant ID'] = 'Merchant ID';
$lang['AWS Key Id'] = 'AWS key ID';
$lang['AWS Secret Key'] = 'AWS Secret Key';
$lang['MWS URL for Keypairs'] = 'MWS URL for Keypairs';
$lang['Check Connectivity'] = 'Check Connectivity';
$lang['Checking ..'] = 'Checking ..';
$lang['Prev'] = 'Prev';
$lang['Next'] = 'Next';
$lang['use profiles mapping'] = 'use profiles mapping';
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";
$lang['Local'] = "Local";
$lang['product(s)'] = "product(s)";
$lang['Amazon'] = "Amazon";
$lang['Synchronization successful '] = "Synchronization successful ";//$lang['Synchronize Success '] = "Synchronize Success ";//not found in using
$lang['What would you like to do?'] = "What would you like to do?";
$lang['Offers only'] = "Offers only";
$lang['Offers and match'] = "Offers and match";
$lang['Load images'] = "Load images"; 
$lang['Display unmatched'] = "Display unmatched";
$lang['Select All'] = "Select All";
$lang['Confirm'] = "Confirm";
$lang['Reject'] = "Reject";
$lang['Finish'] = "Finish";
$lang['Mismatch'] = "Mismatch";
$lang['Brand Mismatch !'] = "Brand Mismatch !";
$lang['Offers on the shop side, but not in your Amazon inventory.'] = "Offers on the shop side, but not in your Amazon inventory.";
$lang['Matching products on Amazon which allows you to create an offer.'] = "Matching products on Amazon which allows you to create an offer.";
$lang['Load More'] = "Load more";//not found in using
$lang["You don't have products yet on Amazon."] = "You don't have products yet on Amazon.";
$lang['Pay now'] = "Pay now";
$lang['get_report_inventory'] = "get_report_inventory";
$lang['matching_products'] = "matching_products";
$lang['You have existing inventory on Amazon.'] = "You have existing inventory on Amazon.";
$lang["You don't have existing inventory on Amazon."] = "You don't have existing inventory on Amazon.";
$lang['API Settings'] = "API Settings";
$lang['Enter API credentials (key pairs)'] = "Enter API credentials (key pairs)";// no need to change because in <option>
$lang["doesn't have api setting"] = "doesn't have api setting";// no need to change
$lang['Go to'] = "Go to";
$lang['Save and continue'] = "Save and continue";
$lang['profiles'] = "Profiles";
$lang['Add a profile to the list'] = "Add a profile to the list";
$lang['Profile(s)'] = "Profile(s)";
$lang['Profile Name'] = "Profile name";
$lang['Set Profile Name First'] = "Define the profile name first";
$lang['Please use a familiar name to remember this profile'] = "Please use a familiar name to remember this profile";
$lang['Do not forget to click on the SAVE button at the bottom of the page !'] = "Do not forget to click on the SAVE button at the bottom of the page !";
$lang['Out of stock'] = "Out of stock";
$lang['Digits only allowed'] = "Digits only allowed";
$lang['Minimum quantity of stock to export product'] = "Minimum quantity of stock to export product";
$lang['Synchronization field'] = "Synchronization field";
$lang['Choose one of the following'] = "Choose one of the following";
$lang['EAN-13 (Europe)'] = "EAN-13 (Europe)";
$lang['UPC (United States)'] = "UPC (United States)";
$lang['Both (EAN-13 then UPC)'] = "Both (EAN-13 then UPC)";
$lang['SKU'] = "SKU";
$lang['Choose the Product Attribute field which will be used for determination of presence of this product on Amazon.'] = "Choose the Product Attribute field which will be used for determination of presence of this product on Amazon.";
$lang['Title format'] = "Title format";
$lang['Standard title, attributes'] = "Standard title, attributes";
$lang['Manufacturer, title, attributes'] = "Manufacturer, title, attributes";
$lang['Manufacturer, title, reference, attributes'] = "Manufacturer, title, reference, attributes";
$lang['Type of product name, Amazon recommends to format the title like manufacturer, title, attributes'] = "Type of product name, Amazon recommends to format the title like manufacturer, title, attributes";
$lang['HTML descriptions'] = "HTML descriptions";
$lang['Yes'] = "Yes";
$lang['Export the HTML descriptions instead of the text only.'] = "Export the HTML descriptions instead of the text only.";
$lang['SKU as the supplier reference.'] = "SKU as the supplier reference.";
$lang['Send the reference/SKU as the supplier reference'] = "Send the reference/SKU as the supplier reference";
$lang['Unconditionnaly'] = "Unconditionally";
$lang['Code exemption'] = "Code exemption";
$lang['Use EAN/UPC'] = "Use EAN/UPC";
$lang['Model number'] = "Model number";
$lang['Model name'] = "Model name";
$lang['Manufacturer part number'] = "Manufacturer part number";
$lang['Catalog number'] = "Catalog number";
$lang['Generic'] = "Generic";
$lang['Private label'] = "Private label";
$lang['Use EAN/UPC exemption for this profile'] = "Use EAN/UPC exemption for this profile";
$lang['Concerns'] = "Concerns";
$lang['Select an option'] = "Select an option";
$lang['Field'] = "Field";
$lang['Recommended browse node'] = "Recommended browse node";
$lang['Browse node ID'] = "Browse node ID";
$lang['This option is mandatory for Canada, Europe, and Japan'] = "This option is mandatory for Canada, Europe, and Japan";
$lang['Amazon browse node ID  -   You can use the product classifier to get browse node IDs:'] = "Amazon browse node ID  -   You can use the product classifier to get browse node IDs:";
$lang['Do you want to delete '] = "Do you want to delete ";
$lang['Delete complete'] = "Delete complete";
$lang['Profile was deleted'] = "Profile was deleted";
$lang['Can not delete'] = "Can not delete";
$lang['No products'] = "No products";
$lang['Match'] = "Match";
$lang['Choose Amazon site'] = "Choose Amazon site";
$lang['Config API setting'] = "Config API setting";
$lang['Choose category'] = "Choose category";
$lang['Sync & match'] = "Sync & match";
$lang['Send to Amazon'] = "Send to Amazon";
$lang['Auto loading offer'] = "Auto loading offer";
$lang['Load more'] = "Load more";
$lang['send'] = "send";
$lang['ID submission'] = "ID submission";
$lang['Processed'] = "Processed";
$lang['Success'] = "Success";
$lang['Error report'] = "Error report";
$lang['Code'] = "Code";
$lang['Product feed'] = "Product feed";
$lang['Inventory feed'] = "Inventory feed";
$lang['Price feed'] = "Price feed";
$lang['Warning'] = "Warning";
$lang['Successful'] = "Successful";
$lang['Submission ID'] = "Submission ID";
$lang['Action type'] = "Action type";
$lang['Feed report'] = "Feed report";
$lang['report'] = "report";
$lang['Offer sending'] = "Offer sending";
$lang['Product sending'] = "Product sending";
$lang['Go to Configuration.'] = "Go to Configuration.";
$lang['Delete'] = "Delete";
$lang['Send'] = "Send";
$lang['actions'] = "Actions"; 
$lang['Select the first option if your product titles are formatted for Amazon.']
                 = "Select the first option if your product titles are formatted for Amazon.";
$lang['save_profiles'] = "save profiles";
$lang['save_category'] = "save category";
$lang['default profiles mapping'] = "default profiles mapping";
$lang['Please  map all attribute color'] = "Please  map all attribute color";
$lang['Send'] = "Send";
$lang['Attributes Mapping'] = "Attributes Mapping";
$lang['Is color'] = "Is color";
$lang['Choose an attribute'] = "Choose an attribute";
$lang['Choose a color mapping'] = "Choose a color mapping";
$lang['Features Mapping'] = "Features mapping";
$lang['Mapping error'] = "Mapping error";
$lang['mapping error'] = "Mapping error";
$lang['Reset'] = "Reset";
$lang['matching'] = "matching";
$lang['Auto load'] = "Auto load";
$lang['SYNCHRONIZE_AND_MATCH'] = "Offers and match";
$lang['Use key pairs of '] = "Use key pairs of ";
$lang['Prices Mapping'] = "Prices mapping";
$lang['Add a model'] = "Add a model";
$lang['Model(s)'] = "Model(s)";
$lang['Set Model Name First'] = "Set Model Name First";
$lang['Select a familiar name to remember this model.'] = "Select a familiar name to remember this model.";
$lang['Product Universe'] = "Product universe";
$lang['Choose the main (Root Node) universe for this model.'] = "Choose the main (Root Node) universe for this model.";
$lang['Product Type'] = "Product type";
$lang['Please select the main category for this model'] = "Please select the main category for this model";
$lang['Specific'] = "Specific";
$lang['Specific Fields'] = "Specific Fields";
$lang['Model was deleted'] = "Model was deleted";
$lang['Model'] = "Model";
$lang['Select a model'] = "Select a model";
$lang['Choose a model'] = "Choose a model";
$lang['Welcome to Amazon synchronization wizard.'] = "Welcome to Amazon synchronization wizard.";
$lang['This operation is automatic but could take one hour, please be patient and wait till the process is completed.'] = "This operation is automatic but could take one hour, please be patient and wait till the process is completed.";
$lang["If you don't want to wait to check on"] = "If you don't want to wait to check on";
$lang['Send results to my email when the process is completed'] = "Send results to my email when the process is completed";
$lang['and click done'] = "and click done";
$lang['the module will send the result to your email automatically.'] = "the module will send the result to your email automatically.";
$lang['Done'] = "Done";
$lang['Image Feed'] = "Image feed";
$lang['Relationship Feed'] = "Relationship feed";
$lang['Last Send Date'] = "Last send date";
$lang['creation'] = "creation";
$lang['Marked as to be created'] = "Marked as to be created";
$lang['Option'] = "Option";
$lang['Exclude manufacturer filter'] = "Exclude manufacturer filter";
$lang['Use only selected carriers'] = "Use only selected carriers";
$lang['Exclude supplier filter'] = "Exclude supplier filter";
$lang['Send image'] = "Send image";
$lang['Limit'] = "Limit";
$lang['No limit'] = "No limit";
$lang['Send products to Amazon'] = "Send products to Amazon";
$lang['Last Send Date'] = "Last Send Date";
$lang['confirm_products'] = "confirm_products";
$lang['Records'] = "Records";
$lang['Add Mapping Attributes'] = "Add Mapping Attributes";
$lang['Welcome to Amazon Creation wizard.'] = "Welcome to Amazon Creation wizard.";
$lang["You don't have a profile setting, please config this tab before sending products to Amazon."] = "You don't have a profile setting, please config this tab before sending products to Amazon.";//$lang["You don't have profile's setting, please config on this tab before send products to Amazon."] = "You don't have profile's setting, please config on this tab before send products to Amazon.";// not found to using
$lang['None'] = "None";
$lang['Mapping colors are require'] = "Mapping colors are required";
$lang['Please map all color attributes.'] = "Please map all color attributes.";
$lang['Save and continue'] = "Save and continue";
$lang["You don't have a profile setting, please config this tab before sending products to Amazon."] = "You don't have a profile setting,please config this tab before sending products to Amazon.";//$lang["You don't have profile's setting yet, please config on this tab before send products to Amazon."] = "You don't have profile's setting yet, please config on this tab before send products to Amazon.";//not found to using
$lang['form_data'] = "form data";
$lang['Attribute'] = "Attribute";
$lang['Custom Value'] = "Custom value";
$lang['Marked as to be created'] = "Marked as to be created";
$lang['Use only selected carriers'] = "Use only selected carriers";
$lang['This will send all the products having a profile and within the selected categories in this site configurations.'] = "This will send all the products having a profile and within the selected categories in this site configurations.";
$lang['If you want to use wizard, please go on the dashboard.'] = "If you want to use wizard, please go on the dashboard.";//$lang['If you want to use the wizard, please go to the dashboard.'] = "If you want to use the wizard, please go to the dashboard.";// not found to using
$lang['Then, you will see the results in the report..'] = "Then, you will see the results in the report..";//$lang['After that, you will see result in Report.'] = "After that, you will see result in Report.";//not found to using
$lang['Partial Update'] = "Partial update";
$lang['Update offers'] = "Update offers";
$lang['Start to deletion'] = "Start to deletion";
$lang['This will send all products with a profile and within the selected categories in this site configurations.'] = "This will send all products with a profile and within the selected categories in this site configurations.";//$lang['This will send all the products having a profile and within the selected categories in this site configurations.'] = "This will send all the products having a profile and within the selected categories in this site configurations.";//not found to use
$lang['To create unknown and unreferenced products on Amazon, please go to the Dashbord and use the "Amazon creation wizard" in the market place wizard tab.'] = 'To create unknown and unreferenced products on Amazon, please go to the Dashbord and use the "Amazon creation wizard" in the market place wizard tab.';//$lang['To create unknown and unreferenced products on Amazon, please go on Dashbord and use "Amazon Creation Wizard" in Marketplace wizard tab.'] = 'To create unknown and unreferenced products on Amazon, please go on Dashbord and use "Amazon Creation Wizard" in Marketplace wizard tab.';//not found to using
$lang['Conecting to Amazon ..'] = "Conecting to Amazon ..";
$lang['Synchronize products'] = "Synchronize products";
$lang['Import Orders'] = "Import Orders";
$lang['This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'] = "This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.";
$lang['Order Date Range'] = "Order date range";
$lang['Order Status'] = "Order status";
$lang['Are you sure to update offers?'] = "Are you sure to update offers?";
$lang['Models'] = "Models";
$lang['Duplicate From'] = "Duplicate From";
$lang['Choose a model to duplicate'] = "Choose a model to duplicate";
$lang['Variation'] = "Variation";
$lang['Submission Feed ID'] = "Submission feed ID";
$lang['Feed Type'] = "Feed Type";
$lang['Submission Feed ID'] = "Submission feed ID";
$lang['It is also recommended that you "match" your products with the "Amazon synchronize wizard" in the Dashbord.'] = 'It is also recommended that you "match" your products with the "Amazon synchronize wizard" in the Dashbord.';//$lang['It is also recommended to try first to "match" your products with Please map all color attributesthe "Amazon Synchronize Wizard" in Dashbord.'] = 'It is also recommended to try first to "match" your products with the "Amazon Synchronize Wizard" in Dashbord.';// not found in using
$lang['Offers'] = "Offers"; 
$lang['checkWS'] = "checkWS";
$lang['get_submission_list'] = "Get submission list";
$lang['save_mappings'] = "save mappings";
$lang['save_data'] = "save_data";
$lang['Please map all attribute color'] = "Please map all attribute color";
$lang['Please map all attribute color.'] = "Please map all attribute color.";
$lang['Associated carrier, useful to display your preferred carrier on the order page and on the invoice'] = "Associated carrier, useful to display your preferred carrier on the order page and on the invoice";
$lang['Choose Carrier'] = "Choose carrier";
$lang['Choose Amazon Carrier'] = "Choose Amazon carrier";
$lang['Are you sure to get orders?'] = "Are you sure to get orders?";
$lang['Carriers Mapping'] = "Carriers mapping";
$lang['Last Logs'] = "Last Logs";
$lang['Action Process'] = "Action Process";
$lang['Orders Report'] = "Orders Report";
$lang['Order ID'] = "Order ID";
$lang['Order ID Ref'] = "Order ID Ref";
$lang['Buyer'] = "Buyer";
$lang['Payment'] = "Payment";
$lang['Amount'] = "Amount";
$lang['Order Date'] = "Order Date";
$lang['Exported'] = "Exported";
$lang['orders'] = "orders";
$lang['All'] = "All";
$lang['Synchronize'] = "Offers";
$lang['Retrieve all pending orders'] = "Retrieve all pending orders";
$lang['Pending - This order is pending in the market place'] = "Pending - This order is pending in the market place";
$lang['Unshipped - This order is waiting to be shipped'] = "Unshipped - This order is waiting to be shipped";
$lang['Partially shipped - This order was partially shipped'] = "Partially shipped - This order was partially shipped";
$lang['Shipped - This order was shipped'] = "Shipped - This order was shipped";
$lang['Cancelled - This order was cancelled'] = "Cancelled - This order was cancelled";
$lang['Wait to process'] = "Wait to process";
$lang['get_key_pairs'] = "get_key_pairs";
$lang['Amazon.com :'] = "Amazon.com :";
$lang['Amazon.co.uk : '] = "Amazon.co.uk : ";
$lang['Please map carriers before importing order.'] = "Please map carriers before importing order.";//$lang['Please mapping carriers before import order.'] = "Please mapping carriers before import order."; // not found to using
$lang['Go to Mappings'] = "Go to Mappings";
$lang['Warning!'] = "Warning!";
$lang['Warnings'] = "Warnings";
$lang['Send product offers'] = "Send product offers";
$lang['Sent'] = "Sent";
$lang['Size'] = "Size";
$lang['Offers Wizard'] = "Offers Wizard";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.']
       //= "This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.";
        = "This wizard will download & compare your Amazon Inventory with Feed.biz and identify all unmatched products in Feed.biz to update on Amazon.";
$lang['This will send all the products having a profile and within the selected categories in your configuration.']
        = "The products identified will be from the selected categories and having a profile.";
$lang['Update product offers'] = "Update product offers";
$lang['This will update Amazon depending your offers, within the selected categories in your configuration.'] = "This will update Amazon depending your offers, within the selected categories in your configuration.";
$lang['Products Wizard'] = "Products wizard";
$lang['Are you sure to delete product?'] = "Are you sure to delete product?";
$lang['Delete'] = "Delete";
$lang['Delete out of stock products'] = "Delete out of stock products";
$lang['This will delete all sent products on Amazon.'] = "This will delete all sent products on Amazon.";
$lang['Quantity'] = "Quantity";
$lang['and'] = "and";
$lang['Price'] = "Price";
$lang['will not send to Amazon.'] = "will not send to Amazon.";// not found in using
$lang['To synchronize quantity or price please go on'] = "To synchronize quantity or price please go on";
$lang['then check synchronize quantity / synchronize price and save'] = "Then check synchronize quantity / synchronize price and save";//not found in using
$lang['Save Profiles & Continue'] = "Save profiles & continue";
$lang['This will delete only out of stock products on Amazon.'] = "This will delete only out of stock products on Amazon.";
$lang['Delete all sent products'] = "Delete all sent products";
$lang['This will send all the products having a profile and within the selected categories in your module configuration and selected for deletion.'] = "This will send all the products having a profile and within the selected categories in your module configuration and selected for deletion.";
$lang['Save Mappings & Continue'] = "Save Mappings & Continue";
$lang['Save Models & Continue'] = "Save Models & Continue";
$lang['get_duplicate_model'] = "get duplicate model";
$lang['get_manufacturers'] = "get manufacturers";
$lang['delete_profile'] = "delete profile";
$lang['Debug mode'] = "Debug mode";
$lang['delete_data'] = "Delete data";
$lang['Recommended Fields'] = "Recommended fields";
$lang['Some values are required.'] = "Some values are required.";
$lang['Please set a required value in the model.'] = "Please set a required value in the model.";
$lang['auth_setting'] = "auth_setting";
$lang['downloadLog'] = "downloadLog";
$lang['Feature'] = "Feature";
$lang['Mode'] = "Mode";
$lang['Items(s)'] = "Items(s)";
$lang['Description Field'] = "Description field";
$lang['Description'] = "Description";
$lang['Short Description'] = "Short description";
$lang['Both'] = "Both";
$lang['Description field to send to Amazon'] = "Description field to send to Amazon";
$lang['Key Product Features'] = "Key product features";
$lang['Use Product Features (Recommended)'] = "Use product features (Recommended)";
$lang['Add Feature Name before Feature Value (ie: Material: Tissue)'] = "Add feature name before feature value (ie: Material: Tissue)";
$lang['Expand description into bullets points'] = "Expand description into bullets points";
$lang['Mapping valid value'] = "Mapping valid value";
$lang['Select color'] = "Select color";
$lang["Not recommended, result could be unexpected. Use this option if you don't have product features and no other options"] = "Not recommended, result could be unexpected. Use this option if you don't have product features and no other options";
$lang['Do not use key product features (Europe, Japan, and Canada only)'] = "Do not use key product features (Europe, Japan, and Canada only)";
$lang['save_carriers'] = 'save carriers';
$lang['Condition Mappings'] = "Condition Mappings";
$lang['Amazon conditions side / Feed.biz conditions side, please associate and map the parameters desired between Amazon and Feed.biz'] = "Amazon conditions side / Feed.biz conditions side, please associate and map the parameters desired between Amazon and Feed.biz";
$lang['save_conditions'] = "save conditions";
$lang['Synchronize Amazon'] = "Synchronize Amazon";
$lang['Wizard'] = "Wizard";
$lang['to Amazon'] = "to Amazon";
$lang['Amazon Creation'] = "Amazon creation";
$lang['Save Mapping & Continue'] = "Save mapping & continue";
$lang['Product marked as to be created is 0.'] = "Product marked as to be created is 0.";
$lang['Statistics'] = "Statistics";
$lang['Feature'] = "Feature";
$lang['There are no attributes to map'] = "There are no attributes to map";
$lang['Please select category first'] = "Please select category first";
$lang['Note: Some categories will have no attributes to map'] = "Note: Some categories will have no attributes to map";
$lang['Start importing orders'] = "Start importing orders";
$lang['get_valid_value'] = "Get valid value";
$lang['Test'] = "Test";
$lang['CE'] = "CE";
$lang['VCR'] = "VCR";
$lang['specific_fields'] = "Specific fields";
$lang['TotalCoaxialInputs'] = "TotalCoaxialInputs";
$lang['from Amazon'] = "from Amazon";
$lang['Please wait'] = "Please wait";
$lang['Processing'] = "Processing";
$lang['Creation Mode'] = "Creation mode";
$lang['Connecting to Amazon'] = "Connecting to Amazon";
$lang["You don't have"] = "You don't have";
$lang['Item Type'] = "Item type";
$lang['setting yet, please config on this tab before sending products to Amazon.'] = "setting yet, please config on this tab before sending products to Amazon.";
$lang['recommended_data'] = "recommended_data";
$lang['U.S.A. / India Only'] = "U.S.A. / India Only";
$lang['From'] = "From";
$lang['Configure a price rule in values or percentages for one or several price ranges.'] = "Configure a price rule in values or percentages for one or several price ranges.";
$lang['To'] = "To";
$lang['Two Digits'] = "Two Digits";
$lang['Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required.'] = "Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required.";
$lang['A value must be set for this element'] = "A value must be set for this element";
$lang['Invalid price range, price to must be greater than price from.'] = "Invalid price range, price to must be greater than price from.";
$lang['Please map this attribute before sending product to Amazon.'] = "Please map this attribute before sending product to Amazon.";
$lang['Item Type  -   You can use the product classifier to get the item type:']
     = "Item Type  -   You can use the product classifier to get the item type:";
$lang['This option is mandatory only for U.S.A. and could be mandatory for India']
     = "This option is mandatory only for U.S.A. and could be mandatory for India";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.']
     = "This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.";
$lang['(This operation is automatic but could take one hour, please be patient and wait for the processing to be completed.)']
     = "(This operation is automatic but could take one hour, please be patient and wait for the processing to be completed.)";
$lang['item(s)'] = "item(s)";
$lang['For incoming orders'] = "For incoming orders";
$lang['Choose Amazon carrier side'] = "Choose Amazon carrier side";
$lang['Choose Feed.biz carrier side'] = "Choose Feed.biz carrier side";
$lang['For Outgoing Orders'] = 'For outgoing orders';

//Report
$lang['Last error from amazon'] = "Last error from Amazon";

//Category
$lang['use default price modifier'] = "Use default price modifier";

//message
$lang['save_unsuccessful']                  = "Save unsuccessful";
$lang['save_successful']                    = "Save successful";
$lang['Merchant Id is missing']                    = 'Merchant ID is missing';
$lang['AWS KeyId Id is missing']                   = 'AWS KeyId ID is missing';
$lang['AWS Secret Key Id is missing']              = 'AWS Secret Key ID is missing';
$lang['Market Place Platform is missing']          = 'Market place platform is missing';
$lang['Market Place Currency is missing']          = 'Market Place currency is missing';
$lang['Unable to login']                           = 'Unable to login';
$lang['Connection to Amazon Failed']               = 'Connection to Amazon failed';
$lang['Connection to Amazon successful']           = 'Connection to Amazon successful';
$lang['No data to save']                           = 'No data to save';
$lang['Nothing to send']                           = 'Nothing to send';
$lang['Preparing product to send']                 = 'Preparing product to send';
$lang['Preparing inventory to send']               = 'Preparing inventory to send';
$lang['Preparing price to send']                   = 'Preparing price to send';
$lang['Preparing image to send']                   = 'Preparing image to send';
$lang['Preparing relationship to send']            = 'Preparing relationship to send';

//new words (did not verify)
$lang['API settings'] = "API settings";
$lang['Debug'] = "Debug";
$lang['This field is required.'] = "This field is required.";
$lang['Are you sure to update order shipments?'] = "Are you sure to update order shipments?";
$lang['Start send products to Amazon'] = "Start send products to Amazon";
$lang['debugs'] = "debugs";
$lang['Debug'] = "Debug";
$lang['Start send products to Amazon'] = "Start send products to Amazon";
$lang['Please give a friendly name to remind this profile'] = "Please give a friendly name to remind this profile";
$lang['Out of Stock'] = "Out of stock";
$lang['Minimum of quantity required to be in stock to export the product.'] = "Minimum of quantity required to be in stock to export the product.";
$lang['Title Format'] = "Title format";
$lang['Standard Title, Attributes'] = "Standard title, Attributes";
$lang['Manufacturer, Title, Attributes'] = "Manufacturer, Title, Attributes";
$lang['Manufacturer, Title, Reference, Attributes'] = "Manufacturer, Title, Reference, Attributes";
$lang['Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes.'] = "Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes.";
$lang['Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon.'] = "Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon.";
$lang['HTML Descriptions'] = "HTML descriptions";
$lang['Export HTML descriptions instead of Text Only'] = "Export HTML descriptions instead of text only";
$lang['Whether to send short or long description of the product to Amazon.'] = "Whether to send short or long description of the product to Amazon.";
$lang['Recommended Browse Node'] = "Recommended browse node";
$lang['Browse Node ID'] = "Browse node ID";
$lang['Allow only digit'] = "Allow only digit";
$lang['Amazon Browse Node ID: You can use the product classifier to get Browse Node IDs: '] = "Amazon Browse Node ID - You can use the product classifier to get browse node IDs:";
$lang['This option is mandatory for Canada, Europe, Japan'] = "This option is mandatory for Canada, Europe, Japan";
$lang['Explode Description into Bullets Points'] = "Explode description into bullets points";
$lang["Not recommended, result could be unpredictable.. Use this option only if you don't have product features and no other choice"] = "Not recommended, result could be unpredictable.. Use this option only if you don't have product features and no other choice";
$lang['Do not use key product features (Europe, Japan, Canada only)'] = "Do not use key product features (Europe, Japan, Canada only)";
$lang['Price Rules'] = "Price rules";
$lang['Default Price Rule'] = "Default price rule";
$lang['Percentage'] = "Percentage";
$lang['Value'] = "Value";
$lang['You should configure a price rule in value or in percentage for one or several prices ranges.'] = "You should configure a price rule in value or in percentage for one or several prices ranges.";
$lang['Rounding'] = "Rounding";
$lang['One Digit'] = "One digit";
$lang['Invalid price range, price to must be more than price from.'] = "Invalid price range, price to must be more than price from.";
$lang['Select all'] = "Select all";
$lang['No items found.'] = "No items found.";
$lang['Manufacturer'] = "Manufacturer";
$lang['Please give a friendly name to remind this model'] = "Please give a friendly name to remind this model";
$lang['Some value are require.'] = "Some value are require.";
$lang['Please set a require value in model.'] = "Please set a require value in model.";
$lang['Do you want to delete'] = "Do you want to delete";
$lang['Synchronization Field'] = "Synchronization field";
$lang['EAN13 (Europe)'] = "EAN13 (Europe)";
$lang['Both (EAN13 then UPC)'] = "Both (EAN13 then UPC)";
$lang['SKU as Supplier Reference'] = "SKU as supplier reference";
$lang['Code Exemption'] = "Code exemption";
$lang['Manufacturer Part Number'] = "Manufacturer part number";
$lang['Catalog Number'] = "Catalog number";
$lang['Private Label'] = "Private label";
$lang['Select an Option'] = "Select an option";
$lang['Default Condition Note'] = "Default condition note";
$lang['Short text about product condition / state which will appear on the product details sheet on Amazon.'] = "Short text about product condition / state which will appear on the product details sheet on Amazon.";
$lang['Latest Updates'] = "Latest updates";
$lang['Normal Update'] = "Normal update";
$lang['Cron Update'] = "Cron update";
$lang['Update History'] = "Update history";
$lang['Validation Logs'] = "Validation logs";
$lang['Result'] = "Result";
$lang['items to send'] = "items to send";
$lang['skipped items'] = "skipped items";
$lang['Manufacturer'] = "Manufacturer";
$lang['Fail'] = "Fail";
$lang['Skipped'] = "Skipped";
$lang['Export HTML Descriptions instead of Text Only'] = "Export HTML descriptions instead of Text Only";
$lang['Send only relationship'] ="Send only relationship";
$lang['Update Order Shipments'] = "Update order shipments";
$lang['This will confirm orders shipment to Amazon. The order was Shipped will update to Amazon by clicking the button below.'] = "Upon clicking the \"Update order shipments\" Button the status of shipment of your orders will be updated on Amazon";
$lang['Confirm Order Shipment'] = "Confirm order shipment";
$lang['Other'] = "Other";
$lang['Associated carrier, useful to display your preferate carrier on the order page and on the invoice '] = "Associated carrier, useful to display your prefered carrier on the order page and on the invoice ";
$lang['Model Name'] = "Model name";
$lang['Model Number'] = "Model number";
$lang['Shipped - This order was Shipped'] = "Shipped - this order was shipped";
$lang['cron'] = "cron";
$lang['from'] = "from";
$lang['History'] = "History";
$lang['success orders'] = "success orders";
$lang['skipped orders'] = "skipped orders";
$lang['The'] = "The";
$lang['value has mapping automatically'] = "value had mapping defined as automatic";
$lang['This attribute provide the free text.'] = "This attribute provides for free text.";
$lang['You can'] = "You can";
$lang['mapping a new value'] = "mapping a new value";
$lang['mapping a new value'] = "enter and map a new value";
$lang['use this value'] = "use attribute value";
$lang['for'] = "for";
$lang["Do you want to save 'Mappings'?"] = "Do you want to save 'Mappings'?";
$lang['Send the Reference/SKU as the Supplier Reference'] = "Send the Reference/SKU as the Supplier Reference";
$lang["Do you want to save 'Profiles'?"] = "Do you want to save 'Profiles'?";
$lang["Do you want to save 'Models'?"] = "Do you want to save 'Models'?";
$lang['Tracking No.'] = "Tracking No.";
$lang['Shipping Date'] = "Shipping Date";

/* Features 22/04/2015 */
$lang['features'] = "Features";
$lang['Catalog Features'] = "Catalog Features";

// Error Resolution 24/04/2015
$lang['error_resolutions'] = "Error Resolutions";
$lang["Time"] = "Time";
$lang['Action'] = "Action";
$lang['UPDATE OFFERS'] = "UPDATE OFFERS";
$lang["PRODUCT CREATION"] = "PRODUCT CREATION";
$lang["IMPORT ORDERS"] = "IMPORT ORDERS";
$lang["EXPORT ORDERS"] = "EXPORT ORDERS";
$lang["ORDERFULFILLMENT"] = "ORDER FULFILLMENT";
$lang["DELETE"] = "DELETE";
$lang["Date / Time"] = "Date / Time";
$lang["import orders"] = "import orders";
$lang["orders to send"] = "orders to send";
$lang["skipped"] = "skipped";
$lang["Skipped Details"] = "Skipped Details";
$lang["form"] = "form";
$lang["validation_log"] = "validation log";
$lang["get_log"] = "get log";
$lang["report_page"] = "report page";
$lang["Products Creation"] = "Products Creation";
$lang["Create new products on Amazon"] = "Create new products on Amazon";
$lang["Activate import of orders"] = "Activate import of orders";
$lang["Delete Products"] = "Delete Products";
$lang["Activate delete products on Amazon"] = "Activate delete products on Amazon";
$lang["Adjust and format your prices for Amazo"] = "Adjust and format your prices for Amazo";
$lang["Second Hand"] = "Second Hand";
$lang["Sell second hand, collectible or refurbished products"] = "Sell second hand, collectible or refurbished products";
$lang["Expert Mode"] = "Expert Mode";
$lang["Activate a GCID Code Exemption"] = "Activate a GCID Code Exemption";
$lang["SKU as supplier reference"] = "SKU as supplier reference";
$lang["Allow to send (synchronize) stock to amazon"] = "Allow to send (synchronize) stock to amazon";
$lang["doesn't have api setting / inactive"] = "doesn't have api setting / inactive";
$lang["See the setting on"] = "See the setting on";
$lang["fields are required."] = "fields are required.";
$lang["Active"] = "Active";
$lang["Data connection error! Please contact your support, error code:"] = "Data connection error! Please contact your support, error code:";
$lang["Remove a model from list"] = "Remove a model from list";
$lang["Model name are used for Profiles tab. Please give a friendly name to remind this model."] = "Model name are used for Profiles tab. Please give a friendly name to remind this model.";
$lang["Model names are used for Profiles tab. Please associate a friendly name to remember this model."] = "Model names are used for Profiles tab. Please associate a friendly name to remember this model.";
$lang["The product's universe may either require approval or be restricted"] = "The product's universe may either require approval or be restricted";
$lang["Learn more"] = "Learn more";
$lang["Choose the product type for this model"] = "Choose the product type for this model";
$lang["Choose the value in specific fields and then click ADD button (+) to add more fields for this model."] = "Choose the value in specific fields and then click ADD button (+) to add more fields for this model.";
$lang["edit"] = "edit";
$lang["Allow automatic update offer"] = "Allow automatic update offer";
$lang["Automatic update your stock levels on Amazon every hour"] = "Automatic update your stock levels on Amazon every hour";
$lang["Allow automatic export orders to shop"] = "Allow automatic export orders to shop";
$lang["Automatic delete the deleted products on Amazon every days"] = "Automatic delete the deleted products on Amazon every days";
$lang["Automatic import of your orders every hour"] = "Automatic import of your orders every hour";
$lang["Allow send status to Amazon"] = "Allow send status to Amazon";
$lang["Automatically create new products on Amazon every day"] = "Automatically create new products on Amazon every day";
$lang["Allow Automatic deletion of products"] = "Allow Automatic deletion of products";
$lang["Automatically delete the selected products on Amazon every days"] = "Automatically delete the selected products on Amazon every days";
$lang["Send orders"] = "Send orders";
$lang["Invoice No."] = "Invoice No.";
$lang["Sending"] = "Sending";
$lang["orders_page"] = "orders";
$lang['Adjust and format your prices for Amazon'] = "Adjust and format your prices for Amazon";
$lang['Allow to send (synchronize) price to amazon'] = "Allow to send (synchronize) price to amazon";
$lang['ON'] = 'ON';
$lang["Remove a profile from list"] = "Remove a profile from list";
$lang["Profile names are used for Categories. Please give a friendly name to remember this profile."] = "Profile names are used for Categories. Please give a friendly name to remember this profile.";
$lang["Amazon.com"] = "Amazon.com";
$lang["Amazon.co.uk"] = "Amazon.co.uk";
$lang["Choose categories"] = "Choose categories";
$lang["The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized."] = "The products associated with selected categories will only be eligible for export.  If you select profile, then the selected profile will be used for product creation process else products will only be synchronized.";
$lang["Please choose category in My feeds > Parameters > Category"] = "Please choose category in My feeds > Parameters > Category";
$lang["This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon."] = "This wizard will download & compare your Amazon Inventory with Feed.biz and identify all unmatched offers in Feed.biz to update on Amazon.";
$lang["Send Relations Only"] = "Send Relations Only";
$lang["This will update Amazon Relations, within the selected categories in your configuration."] = "This will update Amazon Relations, within the selected categories in your configuration.";
$lang["Send Images Only"] = "Send Images Only";
$lang["This will update Amazon Images, within the selected categories in your configuration."] = "This will update Amazon Images, within the selected categories in your configuration.";
$lang["Send Images Only"] = "Send Images Only";
$lang["Starting send images"] = "Starting send images";
$lang["Send Relations Only"] = "Send Relations Only";
$lang["Starting update"] = "Starting update";
$lang["Are you sure to send relations?"] = "Are you sure to send relations?";
$lang["Are you sure to send images?"] = "Are you sure to send images?";
$lang["Adjust and format your prices for Amazon"] = "Adjust and format your prices for Amazon"; 
//new
$lang["delete"] = "delete";
$lang["Fields"] = "Fields";
$lang["Please select color."] = "Please select color.";
$lang["Products"] = "Products";
$lang["Starting send relations"] = "Starting send relations";
$lang["Import orders"] = "Import orders";
$lang["Starting import"] = "Starting import";
$lang["create offers"] = "create offers";
$lang["save_parameters"] = "save parameters";
$lang["Marked as to be synchronize"] = "Marked as to be synchronize";
$lang["Synchronize only"] = "Synchronize only";
$lang["Synchronize and match"] = "Synchronize and match";
$lang["Offers on the shop side, but not in your Amazon inventory"] = "Offers on the shop side, but not in your Amazon inventory";
$lang["Matching products on Amazon which allows you to create an offer"] = "Matching products on Amazon which allows you to create an offer";
$lang["Matching Product"] = "Matching Product";
$lang["The goal is to create your offers on Amazon."] = "The goal is to create your offers on Amazon.";
$lang["Amazon product creation"] = "Amazon product creation";
$lang["The goal is to create the unknown items on Amazon."] = "The goal is to create the unknown items on Amazon.";
$lang["none"] = "none";
$lang["product creation"] = "product creation"; 

//new
$lang["Amazon carrier side"] = "Amazon carrier side";
$lang["Feed.biz carrier side"] = "Feed.biz carrier side";  
$lang["Automatic update your order status on Amazon every hour"] = "Automatic update your order status on Amazon every hour";
$lang["Product Creation"] = "Product Creation";
$lang["Alllow automatic creation of New products"] = "Alllow automatic creation of New products";
$lang["save_configuration_features"] = "save configuration features";
$lang["synchronize and match"] = "synchronize and match";
$lang["Amazon - Conditions"] = "Amazon - Conditions";
$lang["Send only image"] = "Send only image";
$lang["Limit product(s)"] = "Limit product(s)";
$lang["success"] = "success"; 
$lang["Search: SKU"] = "Search: SKU";
$lang["Search: Message"] = "Search: Message"; 
$lang["Totals SKU"] = "Totals SKU"; 
$lang["Download Template"] = "Download Template"; 
$lang["Upload file"] = "Upload file"; 
$lang["Uploading file"] = "Uploading file"; 
$lang["Edit Attribute"] = "Edit Attribute"; 
$lang["Override"] = "Override"; 
$lang["Solved"] = "Solved"; 
$lang["Submitting"] = "Submitting"; 
$lang["Delete & Recreate"] = "Delete & Recreate"; 
$lang["Variants"] = "Variants"; 
$lang["Nothing found!"] = "Nothing found!"; 
$lang["Error!"] = "Error!"; 
$lang["error_data"] = "error data"; 
$lang["error_resolution_details"] = "error resolution details";  
$lang["error_edit_data"] = "error edit data";
$lang["EAN/UPC"] = "EAN/UPC";
$lang["Your Override"] = "Your Override";
$lang["Attribute Override"] = "Attribute Override";
$lang["amazon_wizard_instruction_mode_1"] = "This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
                                    After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. 
                                    The goal is to create your offers on Amazon.";

$lang["amazon_wizard_instruction_mode_2"] ="This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
                                    After that, the wizard will mark as to be created the unknown products on Amazon but present in your database. 
                                    The goal is to create the unknown items on Amazon.";


/*29-05-2015*/
$lang['offers']                            = "Offers";
$lang['products']                          = "Products";
$lang['orders']                            = "Orders";
$lang['relations']                         = "Relations";
$lang['images']                            = "Images";
$lang['delete']                            = "Delete";
$lang['Relations']                         = "Relations";
$lang['Images']                            = "Images";

/* 1/7/2015 */
$lang['Tips']   =  'Tips';
$lang['Sample'] =  'Sample';

/* 2/7/2015 */
$lang['required'] = 'Required';
$lang['recommended'] = 'Recommended';

/* Repricing 09/07/2015 */
$lang['repricing'] = 'Repricing';
$lang['Activate Repricing Features.'] = 'Activate Repricing Features';
$lang['service_settings'] = 'Service Settings';
$lang['strategies'] = 'Strategies';
$lang['Add a new repricing strategy'] = 'Add a new repricing strategy';

/* product_strategies 22/07/2015 */
$lang['product_strategies'] = 
$lang['category_strategies'] = 'Strategies';

/* feature fba 24/08/2015*/
$lang['Activate Fulfillment by Amazon (FBA).'] = "Activate Fulfillment by Amazon (FBA)";

/* Action Delete 25/08/2015*/
$lang['This will delete all the products having a profile and within the selected categories in your module configuration and selected for deletion.'] 
= "This will delete all the products having a profile and within the selected categories in your module configuration and selected for deletion.";

// Amazon FBA 5/11/2015
$lang['fba'] = 'FBA';
$lang['Fulfillment By Amazon'] = 'Fulfillment By Amazon';
$lang['Behaviour'] = 'Behaviour';
$lang['Use Amazon FBA stock first, then switch to your own stock (AFN/MFN auto switching)'] = 'Use Amazon FBA stock first, then switch to your own stock (AFN/MFN auto switching)';
$lang['Synchronize stocks from Amazon FBA, your shop\'s stock is overrode'] = 'Synchronize stocks from Amazon FBA, your shop\'s stock is overrode';
$lang['FBA Price Formula'] = 'FBA Price Formula';
$lang['Allow only digit.'] = 'Allow only digit.';
$lang['Apply a specific default price formula for FBA products.'] = 'Apply a specific default price formula for FBA products.';
$lang['This formula could be a price a value or math (multiply, divide, addition, subtraction, percentages).'] = 'This formula could be a price a value or math (multiply, divide, addition, subtraction, percentages).';
$lang['Use the @ symbol as price reference (eg: @ + 10 % mean you\'ll add 10 % to the initial price) If you specify nothing, the main setting will be applied.'] = 'Use the @ symbol as price reference (eg: @ + 10 % mean you\'ll add 10 % to the initial price) If you specify nothing, the main setting will be applied.';
$lang['Multi-Channel FBA'] = 'Multi-Channel FBA';
$lang['No'] = 'No';
$lang['Yes'] = 'Yes';
$lang['Add Amazon FBA Multi-Channel support to feed.biz.'] = 'Add Amazon FBA Multi-Channel support to feed.biz.';
$lang['Automatic Multi-Channel'] = 'Automatic Multi-Channel';
$lang['Ship automatically through FBA Multi-Channel when an eligible order is created.'] = 'Ship automatically through FBA Multi-Channel when an eligible order is created.';
$lang['Decrease Stock'] = 'Decrease Stock';
$lang['Decrease the stock when importing orders.'] = 'Decrease the stock when importing orders.';

// FBA Carrier 5/11/2015
$lang['Associate as relevant as possible your Store\'s carrier with the Amazon carrier for FBA Multi-Channel.'] = 'Associate as relevant as possible your Store\'s carrier with the Amazon carrier for FBA Multi-Channel.';

// Mappings 09/10/2015
$lang['No mappings to display'] = "No mappings to display";

$lang["analysis"] 	 = "analysis";		 
$lang["create"] 	 = "create";		 
$lang["export"] 	 = "export";		 
$lang["get"] 	 = "get";		 
$lang["update"] 	 = "update";		 
$lang["update shop"] 	 = "update shop";		 
$lang["analysis repricing"] 	 = "analysis repricing";		 
$lang["create product"] 	 = "create product";		 
$lang["delete product"] 	 = "delete product";		 
$lang["export repricing"] 	 = "export repricing";		 
$lang["get order"] 	 = "get order";		 
$lang["send order"] 	 = "send order";		 
$lang["update offer"] 	 = "update offer";		 
$lang["update shop order"] 	 = "update shop order";
$lang["trigger_repricing repricing"] = "Repricing all offers";

// Amazon Parameters 25/04/2016
$lang["Please click 'Get Access Data' to establish an authenticated connection between Feed.biz and Amazon"]="Please click 'Get Access Data' to establish an authenticated connection between Feed.biz and Amazon";

/* Amazon Advertising : 02/08/2016 */
$lang['Add a new campaign'] = 'Add a new campaign';
$lang['Add a new groups'] = 'Add a new groups';
$lang['Add a new keyword'] = 'Add a new keyword';
$lang['campaigns'] = 'Campaigns';
$lang['groups'] = 'Groups';
$lang['keywords'] = 'Keywords';
$lang['biddable'] = 'Biddable';
$lang['negative'] = 'Negative';
$lang['Login Fail'] = 'Login Fail';
$lang['Request access token fail, please try again'] = 'Request access token fail, please try again';
$lang['Login fail, please try again'] = 'Login fail, please try again';
