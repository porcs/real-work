<?php

$lang['profile']  = "Profile";
$lang['billing_information']  = "Billing information";
$lang['logout']  = "Logout";
$lang['Login']  = "Login";
$lang['sign_up']  = "Sign up";
$lang['home']  = "Home";
$lang['Beginner'] = "Beginner";
$lang['Expert'] = "Expert";
$lang['Advanced'] = "Advanced";
$lang['Choose shop default'] = "Choose default shop";

//footer
$lang['Loading'] = "Loading";
$lang['Cannot change shop offer'] = "Can not change shop offers";
$lang['Cannot change shop product'] = "Can not change shop products";
$lang['Error'] = "Error";
$lang['Loading'] = "Loading";
$lang['data_was_save'] = "Your data has been successfully saved.";

//popup step status
$lang['popup_step1'] = "Edit your profile";
$lang['popup_step2'] = "Verify your web store";
$lang['popup_step3'] = "Set Default price modifier";
$lang['popup_step4'] = "Category Settings"; 
$lang['popup_step5'] = "Marketplace configuration"; 
$lang['ajax_update_process'] = "ajax_update_process";
$lang['welcome'] = "welcome";
$lang['Configuration'] = "Configuration";
$lang['Configurations'] = "Configurations";  
$lang['English'] = "English";
$lang['France'] = "France";
$lang['Type in here'] = "Type in here";

$lang['French'] = "French";
$lang['Success'] = "Success";
$lang['An error has occurred on language translation'] = "An error has occurred on language translation";
$lang['Completed'] = "Completed";
$lang['Tasks '] = "Tasks ";
$lang['Running'] = "Running";
$lang['No tasks running.'] = "No tasks running.";
$lang['Success'] = "Success";
$lang['language'] = "language";
$lang['Please wait'] = "Please wait";
$lang['Processing...'] = "Processing...";
$lang['Search:'] = 'Search:';
$lang["No data available in table"] = "No data available in table";
$lang["Showing _START_ to _END_ of _TOTAL_ entries"] = "Showing _START_ to _END_ of _TOTAL_ entries";
$lang["Showing 0 to 0 of 0 entries"] = "Showing 0 to 0 of 0 entries";
$lang["(filtered from _MAX_ total entries)"] = "(filtered from _MAX_ total entries)";
$lang["Per page: _MENU_"] = "Per page: _MENU_";
$lang['No matching records found'] = "No matching records found";
$lang['First'] = "First";
$lang['Last'] = "Last";
$lang['Previous'] = "Previous";
$lang['activate to sort column ascending'] = "activate to sort column ascending";
$lang['activate to sort column descending'] = "activate to sort column descending"; 
$lang['View more'] = "View more";

$lang['txt_other_login'] = "Your account has login by another session in other place. Please refresh this page.";
$lang['txt_admin_login'] = "Your account has login by Feed.biz Administrator in other place. Please refresh this page.";
