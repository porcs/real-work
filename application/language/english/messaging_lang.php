<?php

$lang['page_title']                     = "Messaging";
$lang['configuration']                  = "Configuration";
$lang['Save']                           = "Save";
$lang['Reset']                          = "Reset";

//Mail invoice
$lang['mail_invoice']                   = "Send invoice by email";

//message
$lang['incorrect_url']                  = "Incorrect url";
$lang['save_unsuccessful']              = "Save unsuccessful.";
$lang['save_successful']                = "Save successful.";

$lang['create_table_unsuccessful']                = "Create table unsuccessful.";
$lang['<p>You did not select a file to upload.</p>']      = "You did not select a file to upload.";
$lang['no_file']      = "no file";

// messaging
$lang['Sending mail invoice']      = "Sending mail invoice";
$lang['Sending mail review']      = "Sending mail review";
$lang['Cannot connect to IMAP serveur']      = "Cannot connect to IMAP serveur";