<?php 
$lang["google_shopping"] 	 = "Google Shopping";		 //not approved2 ec3
$lang["index"] 	 = "index";		 //not approved2 ec3
$lang["by Products"] 	 = "by Product";		 //approved2 ec3
$lang["Categories"] 	 = "Categories";		 //approved2 ec3
$lang["categories"] 	 = "categories";		 //not approved2 ec3
$lang["Check all"] 	 = "Check all";		 //approved2 ec3
$lang["Choose categories"] 	 = "Choose Categories";		 //approved2 ec3
$lang["Collapse all"] 	 = "Collapse all";		 //approved2 ec3
$lang["Conditions"] 	 = "Conditions";		 //approved2 ec3
$lang["Expand all"] 	 = "Expand all";		 //approved2 ec3
$lang["Google Shopping"] 	 = "Google Shopping";		 //not approved2 ec3
$lang["Models"] 	 = "Models";		 //approved2 ec3
$lang["Parameter"] 	 = "Parameter";		 //not approved2 ec3
$lang["Save and continue"] 	 = "Save and continue";		 //approved2 ec3
$lang["The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized."] 	 = "The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized";		 //not approved2 ec3
$lang["This field is required."] 	 = "This is required";		 //approved2.1 ec3
$lang["Uncheck all"] 	 = "Uncheck all";		 //approved2 ec3
$lang["Accept Orders"] 	 = "Accept Orders";		 //not approved2 ec3
$lang["Condition Mappings"] 	 = "Condition Mappings";		 //approved2 ec3
$lang["conditions"] 	 = "conditions";		 //not approved2 ec3
$lang["Do you want to update the running status of automatic task(s)?"] 	 = "Do you want to update the running status of automatic task(s)?";		 //not approved2 ec3
$lang["Feed.biz - Conditions"] 	 = "Feed.biz - Conditions";		 //approved2 ec3
$lang["Mapping colors are require"] 	 = "Mapping colors are require";		 //not approved2 ec3
$lang["My Orders"] 	 = "My Orders";		 //not approved2 ec3
$lang["Order"] 	 = "Order";		 //not approved2 ec3
$lang["Please map all attribute color."] 	 = "Please map all attributes colors";		 //approved2 ec3
$lang["Select an option"] 	 = "Select an option";		 //approved2 ec3
$lang["Action Process"] 	 = "Action Process";		 //not approved2 ec3
$lang["Action type"] 	 = "Action type";		 //approved2 ec3
$lang["Batch ID"] 	 = "Batch ID";		 //approved2 ec3
$lang["Cancelled - This order was cancelled"] 	 = "Cancelled - This order was cancelled";		 //approved2 ec3
$lang["Carts"] 	 = "Carts";		 //not approved2 ec3
$lang["form"] 	 = "Form";		 //approved2 ec3
$lang["import orders"] 	 = "Import Orders";		 //approved2 ec3
$lang["items to send"] 	 = "Items to send";		 //approved2 ec3
$lang["logs"] 	 = "logs";		 //not approved2 ec3
$lang["orders to send"] 	 = "Orders to send";		 //approved2 ec3
$lang["Partially shipped - This order was partially shipped"] 	 = "Partially shipped - This Order was partially shipped";		 //approved2 ec3
$lang["Pending - This order is pending in the market place"] 	 = "Pending - This order is pending in the marketplace";		 //approved2 ec3
$lang["Result"] 	 = "Result";		 //approved2 ec3
$lang["Retrieve all pending orders"] 	 = "Retrieve all pending Orders";		 //approved2 ec3
$lang["Shipped - This order was shipped"] 	 = "Shipped - This Order was shipped";		 //approved2 ec3
$lang["skipped"] 	 = "Skipped";		 //approved2 ec3
$lang["skipped items"] 	 = "Skipped Items";		 //approved2 ec3
$lang["skipped orders"] 	 = "Skipped Orders";		 //approved2 ec3
$lang["success"] 	 = "Success";		 //approved2 ec3
$lang["success orders"] 	 = "success orders";		 //not approved2 ec3
$lang["Unshipped - This order is waiting to be shipped"] 	 = "Unshipped - This order is waiting to be shipped";		 //approved2 ec3
$lang["+ Add a Model"] 	 = "+ Add a Model";		 //not approved2 ec3
$lang["Can not delete"] 	 = "Can't delete";		 //approved2 ec3
$lang["Choose a Product Type option"] 	 = "Choose a Product Type option";		 //not approved2 ec3
$lang["Choose a Product Universe option"] 	 = "Choose a Product Universe option";		 //not approved2 ec3
$lang["Choose a Specific Field option"] 	 = "Choose a Specific Field option";		 //not approved2 ec3
$lang["Choose a Variation option"] 	 = "Choose a Variation option";		 //not approved2 ec3
$lang["Choose the main (Root Node) universe for this model."] 	 = "Choose the main (Root Node) universe for this model";		 //not approved2 ec3
$lang["Choose the product type for this model"] 	 = "Choose the product type for this model";		 //not approved2 ec3
$lang["Custom label"] 	 = "Custom label";		 //not approved2 ec3
$lang["Default value"] 	 = "Default value";		 //not approved2 ec3
$lang["delete"] 	 = "delete";		 //approved2 ec3
$lang["Delete complete"] 	 = "Delete complete";		 //not approved2 ec3
$lang["Do not forget to click on the SAVE button at the bottom of the page !"] 	 = "Do not forget to click on the SAVE button at the bottom of the page !";		 //not approved2 ec3
$lang["Do you want to delete"] 	 = "Delete ";		 //approved2 ec3
$lang["Do you want to save 'Models'?"] 	 = "Do you want to save 'Models'?";		 //not approved2 ec3
$lang["edit"] 	 = "Edit";		 //approved2 ec3
$lang["Group by"] 	 = "Group by";		 //not approved2 ec3
$lang["Item(s)"] 	 = "Item/s";		 //approved2 ec3
$lang["Model name"] 	 = "Model name";		 //approved2 ec3
$lang["Model names are used for Profiles tab. Please associate a friendly name to remember this model."] 	 = "Model names are used for Profiles tab. Please associate a friendly name to remember this model";		 //not approved2 ec3
$lang["Model was deleted"] 	 = "Deleted";		 //approved2 ec3
$lang["models"] 	 = "models";		 //not approved2 ec3
$lang["Please select the main category for this model"] 	 = "Please select the main category for this model";		 //approved2 ec3
$lang["Please set a require value in model."] 	 = "Please set a require value in model";		 //not approved2 ec3
$lang["Product Type"] 	 = "Product type";		 //approved2 ec3
$lang["Product Universe"] 	 = "Product Universe";		 //approved2 ec3
$lang["Recommended Fields"] 	 = "Recommended Fields";		 //not approved2 ec3
$lang["Remove a model from list"] 	 = "Remove a model from list";		 //not approved2 ec3
$lang["Set Model Name First"] 	 = "Set Model Name First";		 //not approved2 ec3
$lang["Some value are require."] 	 = "Some values are required";		 //approved2 ec3
$lang["Specific Fields"] 	 = "Custom Fields";		 //approved2 ec3
$lang["Variation"] 	 = "Mapping Variations";		 //approved2 ec3
$lang["No API Connection established due to API connection keys missing or inactive. Please go to Configurations &gt; Parameters Tab to set API keys."] 	 = "No API Connection. Set API keys in Configuration>Parameters";		 //approved2.1 ec3
$lang["A value must be a Number"] 	 = "This must be a number";		 //approved2 ec3
$lang["Click to edit ASIN for"] 	 = "Click to edit ASIN for";		 //approved2 ec3
$lang["Description"] 	 = "Long description";		 //approved2 ec3
$lang["Download Offers Options"] 	 = "Download Offer Options";		 //approved2 ec3
$lang["Feilds"] 	 = "Fields";		 //approved2 ec3
$lang["Force in stock"] 	 = "Force in stock";		 //approved2 ec3
$lang["If this checkbox is checked then the quantity mentioned in the associated quantity field will appear as current stock in the marketplace in cases where this product is out of stock."] 	 = "Check to force the selected quantity to appear as current Stock in the marketplace when this Product is out of Stock";		 //approved2.1 ec3
$lang["If this checkbox is checked, this product will be disabled i.e. never exported to marketplace."] 	 = "Check to force the selected quantity to appear as current Stock in the marketplace when this Product is out of Stock";		 //approved2.1 ec3
$lang["offers_options"] 	 = "offers_options";		 //not approved2 ec3
$lang["Only Active"] 	 = "Only active";		 //approved2 ec3
$lang["Only in Stock"] 	 = "Only in Stock";		 //approved2 ec3
$lang["Option"] 	 = "Options";		 //approved2 ec3
$lang["product"] 	 = "Products";		 //approved2 ec3
$lang["Product SKU"] 	 = "Product SKU";		 //approved2 ec3
$lang["Require"] 	 = "Required";		 //approved2 ec3
$lang["The amount mentioned here will replace the Net Price in the Market Place."] 	 = "The amount indicated here will replace the Net Price in the marketplace";		 //approved2.1 ec3
$lang["true"] 	 = "True";		 //approved2 ec3
$lang["Active"] 	 = "Active";		 //approved2 ec3
$lang["Copy URL"] 	 = "Copy URL";		 //not approved2 ec3
$lang["Export Shipping(carrier) ?"] 	 = "Export Shipping(carrier) ?";		 //not approved2 ec3
$lang["ON"] 	 = "ON";		 //approved2 ec3
$lang["parameter"] 	 = "parameter";		 //not approved2 ec3
$lang["Setting"] 	 = "Settings";		 //approved2 ec3
$lang["This URL use Google Feed"] 	 = "This URL use Google Feed";		 //not approved2 ec3
$lang["URL"] 	 = "URL";		 //approved2 ec3
$lang["+ Add a profile to the list"] 	 = "+ Add a profile to the list";		 //not approved2 ec3
$lang["Allow only digit"] 	 = "Allow digits only";		 //approved2 ec3
$lang["Both"] 	 = "Both";		 //approved2 ec3
$lang["Both (EAN13 then UPC)"] 	 = "Both (EAN13 then UPC)";		 //approved2 ec3
$lang["Choose a model"] 	 = "Choose a Model";		 //approved2 ec3
$lang["Choose one of the following"] 	 = "Choose one of the following";		 //approved2 ec3
$lang["Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping."] 	 = "Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping";		 //not approved2 ec3
$lang["Description Field"] 	 = "Description Field";		 //not approved2 ec3
$lang["EAN13 (Europe)"] 	 = "EAN13 (Europe)";		 //approved2 ec3
$lang["Export HTML Descriptions instead of Text Only"] 	 = "Export HTML Descriptions instead of Text Only";		 //not approved2 ec3
$lang["GTIN Field"] 	 = "GTIN Field";		 //not approved2 ec3
$lang["HTML Descriptions"] 	 = "HTML Descriptions";		 //not approved2 ec3
$lang["Manufacturer, Title, Attributes"] 	 = "Manufacturer, Title, Attributes";		 //approved2 ec3
$lang["Manufacturer, Title, Reference, Attributes"] 	 = "Manufacturer, Title, Reference, Attributes";		 //approved2 ec3
$lang["Minimum of quantity required to be in stock to export the product."] 	 = "Minimum quantity required to be in Stock to export the Product";		 //approved2.1 ec3
$lang["Model"] 	 = "Model";		 //approved2 ec3
$lang["Name"] 	 = "Name";		 //approved2 ec3
$lang["None"] 	 = "None";		 //approved2 ec3
$lang["Out of Stock"] 	 = "Out of Stock";		 //approved2 ec3
$lang["Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping."] 	 = "Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping";		 //not approved2 ec3
$lang["Profile names are used for Categories. Please give a friendly name to remember this profile."] 	 = "Profile names are used for Categories. Please give a friendly name to remember this profile";		 //not approved2 ec3
$lang["profiles"] 	 = "profiles";		 //not approved2 ec3
$lang["Remove a profile from list"] 	 = "Remove a profile from list";		 //not approved2 ec3
$lang["Select a model"] 	 = "Select a Model";		 //approved2 ec3
$lang["Set Profile Name First"] 	 = "Set Profile Name First";		 //not approved2 ec3
$lang["Short Description"] 	 = "Short Description";		 //not approved2 ec3
$lang["SKU"] 	 = "SKU";		 //approved2 ec3
$lang["Standard Title, Attributes"] 	 = "Standard Title, Attributes";		 //not approved2 ec3
$lang["Title Format"] 	 = "Title Format";		 //not approved2 ec3
$lang["Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes."] 	 = "Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes";		 //not approved2 ec3
$lang["UPC (United States)"] 	 = "UPC (United States)";		 //approved2 ec3
$lang["Whether to send short or long description of the product to Google Shopping."] 	 = "Whether to send short or long description of the product to Google Shopping";		 //not approved2 ec3
$lang["Yes"] 	 = "Yes";		 //approved2 ec3
$lang["Synchronization Field"] 	 = "Synchronization Field";		 //not approved2 ec3
$lang["com"] 	 = "com";		 //not approved2 ec3
$lang["Action"] 	 = "Action";		 //approved2 ec3
$lang["Date / Time"] 	 = "Date / Time";		 //approved2 ec3
$lang["DELETE"] 	 = "Delete";		 //approved2 ec3
$lang["EXPORT ORDERS"] 	 = "Export Orders";		 //approved2 ec3
$lang["From"] 	 = "From";		 //approved2 ec3
$lang["History"] 	 = "History";		 //approved2 ec3
$lang["IMPORT ORDERS"] 	 = "Import Orders";		 //approved2 ec3
$lang["Latest Updates"] 	 = "Latest Updates";		 //not approved2 ec3
$lang["Normal Update"] 	 = "Normal Update";		 //not approved2 ec3
$lang["ORDERFULFILLMENT"] 	 = "ORDERFULFILLMENT";		 //not approved2 ec3
$lang["PRODUCT CREATION"] 	 = "Product Creation";		 //approved2 ec3
$lang["REPRICING"] 	 = "Repricing";		 //approved2 ec3
$lang["Skipped Details"] 	 = "Skipped Items";		 //approved2 ec3
$lang["Time"] 	 = "Time";		 //approved2 ec3
$lang["To"] 	 = "To";		 //approved2 ec3
$lang["UPDATE OFFERS"] 	 = "Update Offers";		 //approved2 ec3
$lang["Group?"] 	 = "Group?";		 //not approved2 ec3
$lang["Learn more"] 	 = "Learn more";		 //approved2 ec3
$lang["API settings"] 	 = "API Settings";		 //approved2 ec3
$lang["Choose the Product Attribute field which will be used for determination of presence of this product on Amazon."] 	 = "Choose the Product Attribute used to determine the presence of this product on Amazon";		 //approved2.1 ec3
$lang["Default Price Rule"] 	 = "Default Price Rule";		 //approved2 ec3
$lang["Percentage"] 	 = "Percentage";		 //approved2 ec3
$lang["Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon."] 	 = "Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon";		 //not approved2 ec3
$lang["Price Rules"] 	 = "Price Rules";		 //approved2 ec3
$lang["Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes."] 	 = "Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes";		 //not approved2 ec3
$lang["Value"] 	 = "Value";		 //approved2 ec3
$lang["Whether to send short or long description of the product to Amazon."] 	 = "Choose whether to send a short description, long description, both or no description of the Product to Amazon";		 //approved2.1 ec3
$lang["You should configure a price rule in value or in percentage for one or several prices ranges."] 	 = "You should configure a Price Rule by amount or by percentage for one or several Price ranges";		 //approved2.1 ec3
$lang["Multichannel Orders"] 	 = "Multichannel Orders";		 //not approved2 ec3
$lang["Shipping"] 	 = "Shipping";		 //approved2 ec3
$lang["Carrefour"] 	 = "Carrefour";		 //not approved2 ec3
$lang["u00000000000085"] 	 = "u00000000000085";		 //not approved2 ec3
$lang["u00000000001"] 	 = "u00000000001";		 //not approved2 ec3

  