<?php 
$lang["page_title"] 	 = "eBay";		 //approved2 ec3
$lang["ebay"] 	 = "eBay";		 //approved2 ec3
$lang["product_key"] 	 = "Production Keys (App mode)";		 //not approved2 ec3
$lang["product_key_mode"] 	 = "Production Keys | Mode";		 //not approved2 ec3
$lang["sanbox_key"] 	 = "Sandbox Keys (App mode)";		 //not approved2 ec3
$lang["configuration"] 	 = "Configuration";		 //approved2 ec3
$lang["sub_title"] 	 = "eBay Authentification & Security API";		 //not approved2 ec3
$lang["sub_setting"] 	 = "eBay Settings";		 //not approved2 ec3
$lang["reset"] 	 = "Reset";		 //approved2 ec3
$lang["back"] 	 = "Back";		 //approved2 ec3
$lang["prev"] 	 = "Previous";		 //approved2 ec3
$lang["save"] 	 = "Save and Continue";		 //not approved2 ec3
$lang["save_only"] 	 = "Save";		 //approved2 ec3
$lang["next"] 	 = "Next";		 //approved2 ec3
$lang["check_all"] 	 = "Check all";		 //approved2 ec3
$lang["uncheck_all"] 	 = "Uncheck all";		 //approved2 ec3
$lang["collapse_all"] 	 = "Collapse all";		 //approved2 ec3
$lang["expand_all"] 	 = "Expand all";		 //approved2 ec3
$lang["submit"] 	 = "Submit";		 //approved2 ec3
$lang["new_token"] 	 = "Generate a New Token";		 //not approved2 ec3
$lang["load_catagories"] 	 = "Load Categories";		 //not approved2 ec3
$lang["hack"] 	 = "You are trying to hacked in system";		 //not approved2 ec3
$lang["user_id"] 	 = "eBay user ID";		 //approved2 ec3
$lang["user_id:"] 	 = "eBay UserID :";		 //not approved2 ec3
$lang["postcode"] 	 = "Postal/Zip code";		 //approved2 ec3
$lang["postcode:"] 	 = "Postcode :";		 //not approved2 ec3
$lang["paypal_email"] 	 = "Paypal Email";		 //not approved2 ec3
$lang["paypal_email:"] 	 = "Paypal Email :";		 //not approved2 ec3
$lang["address"] 	 = "Address";		 //approved2 ec3
$lang["city:"] 	 = "City :";		 //not approved2 ec3
$lang["visa_mastercard"] 	 = "Visa/MasterCard";		 //approved2 ec3
$lang["discover"] 	 = "Discover";		 //approved2 ec3
$lang["american_express"] 	 = "American Express";		 //approved2 ec3
$lang["money_order"] 	 = "Money order/Cashier's check";		 //approved2 ec3
$lang["personal_check"] 	 = "Personal check";		 //approved2 ec3
$lang["pay_on_pickup"] 	 = "Pay on pickup";		 //approved2 ec3
$lang["other_payment:"] 	 = "Other payment :";		 //not approved2 ec3
$lang["dispatch_time:"] 	 = "Dispatch time :";		 //not approved2 ec3
$lang["other_option"] 	 = "Other Option";		 //not approved2 ec3
$lang["other_option:"] 	 = "Other Option :";		 //not approved2 ec3
$lang["option_add_product:"] 	 = "Export Product :";		 //not approved2 ec3
$lang["option_orders_product:"] 	 = "Import Orders :";		 //not approved2 ec3
$lang["merchant_credit_cards:"] 	 = "Merchant credit cards :";		 //not approved2 ec3
$lang["category"] 	 = "Categories";		 //approved2 ec3
$lang["processed:"] 	 = "Processed :";		 //not approved2 ec3
$lang["product(s)"] 	 = "Product/s";		 //approved2 ec3
$lang["success:"] 	 = "Success :";		 //not approved2 ec3
$lang["warning:"] 	 = "Warning :";		 //not approved2 ec3
$lang["error:"] 	 = "Error :";		 //not approved2 ec3
$lang["see_more_detail:"] 	 = "See More Detail :";		 //not approved2 ec3
$lang["click_this"] 	 = "Click This";		 //not approved2 ec3
$lang["user_not_available"] 	 = "Username is not available";		 //not approved2 ec3
$lang["auth_is_processing"] 	 = "Authentication process is running";		 //approved2.1 ec3
$lang["update_auth_security_success"] 	 = "Update Auth Security Success";		 //not approved2 ec3
$lang["update_auth_security_fail"] 	 = "Data Incorrect : Please, try again";		 //not approved2 ec3
$lang["update_auth_setting_success"] 	 = "Update Configuration Success";		 //not approved2 ec3
$lang["update_auth_setting_fail"] 	 = "Update Configuration Failed : Please, try again";		 //not approved2 ec3
$lang["update_univers_success"] 	 = "Update Categories Selected Success";		 //not approved2 ec3
$lang["update_univers_fail"] 	 = "Update Categories Selected Failed : Please, try again";		 //not approved2 ec3
$lang["update_mapping_categories_success"] 	 = "Update Categories Success";		 //not approved2 ec3
$lang["update_mapping_categories_fail"] 	 = "Update Categories Failed : Please, try again";		 //not approved2 ec3
$lang["update_mapping_templates_success"] 	 = "Update Templates Success";		 //not approved2 ec3
$lang["update_mapping_templates_fail"] 	 = "Update Templates Failed : Please, try again";		 //not approved2 ec3
$lang["update_templates_success"] 	 = "Update Templates Success";		 //not approved2 ec3
$lang["update_templates_fail"] 	 = "Update Templates Failed : Please, try again";		 //not approved2 ec3
$lang["update_mapping_conditions_success"] 	 = "Update Conditions Success";		 //not approved2 ec3
$lang["update_mapping_conditions_fail"] 	 = "Update Conditions Failed : Please, try again";		 //not approved2 ec3
$lang["update_mapping_carriers_success"] 	 = "Update Shipping Success";		 //not approved2 ec3
$lang["update_mapping_carriers_fail"] 	 = "Update Shipping Failed : Please, try again";		 //not approved2 ec3
$lang["update_tax_success"] 	 = "Update Tax Success";		 //not approved2 ec3
$lang["update_tax_fail"] 	 = "Update Tax Failed : Please, try again";		 //not approved2 ec3
$lang["update_price_modifier_success"] 	 = "Update Price Success";		 //not approved2 ec3
$lang["update_price_modifier_fail"] 	 = "Update Price Failed : Please, try again";		 //not approved2 ec3
$lang["update_actions_delete_success"] 	 = "Delete Product Success";		 //not approved2 ec3
$lang["update_actions_delete_fail"] 	 = "Delete Product Failed : Please, try again";		 //not approved2 ec3
$lang["update_actions_export_success"] 	 = "Export Product Success";		 //not approved2 ec3
$lang["update_actions_send_mail"] 	 = "Send Product to eBay and Waiting Email response when send product finish";		 //not approved2 ec3
$lang["update_actions_export_fail"] 	 = "Failed to export Product: Please try again";		 //approved2.1 ec3
$lang["update_actions_orders_success"] 	 = "Get Orders Success";		 //not approved2 ec3
$lang["update_actions_orders_fail"] 	 = "Get Orders Failed : Please, try again";		 //not approved2 ec3
$lang["update_actions_error"] 	 = "Connection failed! Please try again";		 //approved2.1 ec3
$lang["update_actions_error_msg"] 	 = "There has error something that is found, especially an unexpectedly discovery";		 //not approved2 ec3
$lang["update_wizard_choose_success"] 	 = "Update Country Site Success";		 //not approved2 ec3
$lang["update_wizard_choose_fail"] 	 = "Update Country Site Failed : Please, try again";		 //not approved2 ec3
$lang["update_wizard_univers_success"] 	 = "Update Categories Selected Success";		 //not approved2 ec3
$lang["update_wizard_univers_fail"] 	 = "Update Categories Selected Failed : Please, try again";		 //not approved2 ec3
$lang["update_wizard_categories_success"] 	 = "Update Categories Success";		 //not approved2 ec3
$lang["update_wizard_categories_fail"] 	 = "Update Categories Failed : Please, try again";		 //not approved2 ec3
$lang["update_wizard_tax_success"] 	 = "Update Tax Success";		 //not approved2 ec3
$lang["update_wizard_tax_fail"] 	 = "Update Tax Failed : Please, try again";		 //not approved2 ec3
$lang["update_wizard_authorization_success"] 	 = "Update Auth Security Success";		 //not approved2 ec3
$lang["update_wizard_authorization_fail"] 	 = "Data Incorrect : Please, try again";		 //not approved2 ec3
$lang["update_wizard_shippings_success"] 	 = "Update Auth Security Success";		 //not approved2 ec3
$lang["update_wizard_shippings_fail"] 	 = "Data Incorrect : Please, try again";		 //not approved2 ec3
$lang["update_wizard_exports_export_success"] 	 = "Export Product Success";		 //not approved2 ec3
$lang["update_wizard_exports_export_fail"] 	 = "Failed to export Product: Please try again";		 //approved2.1 ec3
$lang["update_wizard_exports_success"] 	 = "Export Product Success";		 //not approved2 ec3
$lang["update_wizard_exports_fail"] 	 = "Failed to export Product: Please try again";		 //approved2.1 ec3
$lang["update_wizard_exports_error"] 	 = "Connection failed! Please try again";		 //approved2.1 ec3
$lang["update_wizard_exports_error_msg"] 	 = "There has error something that is found, especially an unexpectedly discovery";		 //not approved2 ec3
$lang["update_wizard_exports_send_mail"] 	 = "Send Product to eBay and Waiting Email response when send product finish";		 //not approved2 ec3
$lang["update_templates_preview_success"] 	 = "Preview Template Success";		 //not approved2 ec3
$lang["update_templates_preview_fail"] 	 = "Template does not able to preview : Please, try again";		 //not approved2 ec3
$lang["update_templates_delete_success"] 	 = "Delete Template Success";		 //not approved2 ec3
$lang["update_templates_delete_fail"] 	 = "Delete Template Failed : Please, try again";		 //not approved2 ec3
$lang["update_templates_add_success"] 	 = "Add Template Success";		 //not approved2 ec3
$lang["update_templates_add_fail"] 	 = "Add Template Failed : Please, try again";		 //not approved2 ec3
$lang["validation_error_required"] 	 = "Please select a value";		 //approved2.1 ec3
$lang["validation_required_form_user_id"] 	 = "Please enter a user name";		 //approved2.1 ec3
$lang["validation_minlength_form_user_id"] 	 = "Your username must be at least 2 characters long";		 //approved2.1 ec3
$lang["validation_required_form_postal_code"] 	 = "Please enter your postal/zip code";		 //approved2.1 ec3
$lang["validation_regex_form_postal_code"] 	 = "Please enter a valid {1} postcode{BR}Example: {2}";		 //approved2 ec3
$lang["validation_postal_valid_form_postal_code"] 	 = "Please enter a valid postcode{BR}Example: {2}";		 //approved2 ec3
$lang["validation_select_required"] 	 = "Please select a value";		 //approved2.1 ec3
$lang["validation_range_required"] 	 = "Invalid price range, price to must be more than price from";		 //approved2.1 ec3
$lang["validation_between_required"] 	 = "Invalid price range, price to must not be between another price";		 //not approved2 ec3
$lang["validation_carrier_rule_required"] 	 = "Please assign Carrier Rules";		 //approved2.1 ec3
$lang["header_auth_security"] 	 = "eBay Authentication & Security";		 //not approved2 ec3
$lang["title_auth_security"] 	 = "Link to your eBay account ID";		 //not approved2 ec3
$lang["header_auth_setting"] 	 = "Setting Configuration";		 //not approved2 ec3
$lang["title_auth_setting"] 	 = "Product Configuration";		 //not approved2 ec3
$lang["header_tax"] 	 = "Tax Setting";		 //not approved2 ec3
$lang["title_tax"] 	 = "Tax Configuration";		 //not approved2 ec3
$lang["header_price_modifier"] 	 = "Price Rule";		 //approved2 ec3
$lang["title_price_modifier"] 	 = "Price Rules";		 //approved2 ec3
$lang["header_univers"] 	 = "Univers";		 //not approved2 ec3
$lang["title_univers"] 	 = "Selected Main Categories";		 //not approved2 ec3
$lang["header_mapping_categories"] 	 = "Mapping Categories";		 //not approved2 ec3
$lang["title_mapping_categories"] 	 = "Choose Categories";		 //approved2 ec3
$lang["header_mapping_templates"] 	 = "Mapping Templates";		 //not approved2 ec3
$lang["title_mapping_templates"] 	 = "Choose Templates";		 //not approved2 ec3
$lang["header_templates"] 	 = "Manager Templates";		 //not approved2 ec3
$lang["title_templates"] 	 = "Upload Templates";		 //not approved2 ec3
$lang["header_mapping_conditions"] 	 = "Mapping Conditions";		 //not approved2 ec3
$lang["title_mapping_conditions"] 	 = "Choose Conditions";		 //not approved2 ec3
$lang["header_mapping_carriers"] 	 = "Mapping Carriers";		 //not approved2 ec3
$lang["title_mapping_carriers"] 	 = "Choose Shipping";		 //not approved2 ec3
$lang["header_actions"] 	 = "Actions";		 //approved2 ec3
$lang["title_actions"] 	 = "Product Actions";		 //not approved2 ec3
$lang["header_log"] 	 = "Log";		 //approved2 ec3
$lang["title_log"] 	 = "Batch Log";		 //approved2 ec3
$lang["header_statistics"] 	 = "Statistics";		 //approved2 ec3
$lang["title_statistics"] 	 = "Batch Log details";		 //approved2 ec3
$lang["header_orders"] 	 = "Orders";		 //approved2 ec3
$lang["title_orders"] 	 = "Orders Report";		 //approved2 ec3
$lang["header_wizard_choose"] 	 = "Select eBay Country";		 //not approved2 ec3
$lang["title_wizard_choose"] 	 = "Select eBay Country";		 //not approved2 ec3
$lang["header_wizard_univers"] 	 = "Univers";		 //not approved2 ec3
$lang["title_wizard_univers"] 	 = "Selected Main Categories";		 //not approved2 ec3
$lang["header_wizard_categories"] 	 = "Univers";		 //not approved2 ec3
$lang["title_wizard_categories"] 	 = "Selected Main Categories";		 //not approved2 ec3
$lang["header_wizard_tax"] 	 = "Tax Setting";		 //not approved2 ec3
$lang["title_wizard_tax"] 	 = "Tax Configuration";		 //not approved2 ec3
$lang["header_wizard_authorization"] 	 = "eBay Authentication & Security";		 //not approved2 ec3
$lang["title_wizard_authorization"] 	 = "Link to your eBay account ID";		 //not approved2 ec3
$lang["header_wizard_shippings"] 	 = "Mapping Carriers";		 //not approved2 ec3
$lang["title_wizard_shippings"] 	 = "Choose Shipping";		 //not approved2 ec3
$lang["header_wizard_finish"] 	 = "Wizard Statictic Export Products";		 //not approved2 ec3
$lang["title_wizard_finish"] 	 = "Wizard Statictic Export Products";		 //not approved2 ec3
$lang["header_wizard_exports"] 	 = "Actions";		 //approved2 ec3
$lang["title_wizard_exports"] 	 = "Wizard Export Products";		 //not approved2 ec3
$lang["auth_security"] 	 = "Auth & Security";		 //not approved2 ec3
$lang["auth_setting"] 	 = "Setting Configuration";		 //not approved2 ec3
$lang["tax"] 	 = "Tax Setting";		 //not approved2 ec3
$lang["price_modifier"] 	 = "Price Rule";		 //approved2 ec3
$lang["univers"] 	 = "Selected Categories";		 //not approved2 ec3
$lang["mapping_categories"] 	 = "Categories";		 //approved2 ec3
$lang["mapping_templates"] 	 = "Templates";		 //approved2 ec3
$lang["mapping_conditions"] 	 = "Selected Condition";		 //not approved2 ec3
$lang["mapping_carriers"] 	 = "Carriers";		 //approved2 ec3
$lang["actions"] 	 = "Actions";		 //approved2 ec3
$lang["log"] 	 = "Log";		 //approved2 ec3
$lang["total"] 	 = "Total";		 //approved2 ec3
$lang["primary_categories"] 	 = "Primary Categories";		 //not approved2 ec3
$lang["secondary_categories"] 	 = "Secondary Categories";		 //not approved2 ec3
$lang["primary_store_categories"] 	 = "Primary Store Categories";		 //not approved2 ec3
$lang["secondary_store_categories"] 	 = "Secondary Store Categories";		 //not approved2 ec3
$lang["payment_location"] 	 = "Payment & Location";		 //not approved2 ec3
$lang["returns_policy:"] 	 = "Returns Policy :";		 //not approved2 ec3
$lang["returns_within:"] 	 = "Returns Within :";		 //not approved2 ec3
$lang["who_pays:"] 	 = "Who pays :";		 //not approved2 ec3
$lang["information:"] 	 = "Information :";		 //not approved2 ec3
$lang["listing_duration"] 	 = "Listing Duration";		 //not approved2 ec3
$lang["listing_duration:"] 	 = "Listing Duration :";		 //not approved2 ec3
$lang["selected_tax:"] 	 = "Selected Tax :";		 //not approved2 ec3
$lang["rounding:"] 	 = "Rounding :";		 //not approved2 ec3
$lang["shipping_rules:"] 	 = "Shipping Rules :";		 //not approved2 ec3
$lang["one_digit"] 	 = "One Digit";		 //not approved2 ec3
$lang["two_digit"] 	 = "Two Digit";		 //not approved2 ec3
$lang["none"] 	 = "None";		 //approved2 ec3
$lang["weight"] 	 = "Weight";		 //approved2 ec3
$lang["price"] 	 = "Price";		 //approved2 ec3
$lang["item"] 	 = "Item";		 //approved2 ec3
$lang["shipping_time"] 	 = "Carrier & Shipping time";		 //not approved2 ec3
$lang["mapping_domestic_shipping"] 	 = "Mapping Domestic Shipping";		 //not approved2 ec3
$lang["mapping_international_shipping"] 	 = "Mapping International Shipping";		 //not approved2 ec3
$lang["choose_a_carrier:"] 	 = "Choose a Carrier :";		 //not approved2 ec3
$lang["country:"] 	 = "Country :";		 //not approved2 ec3
$lang["rules_surcharge"] 	 = "Set Shipping Rules applies if the total cost of items purchased";		 //not approved2 ec3
$lang["rules_weight"] 	 = "Set Shipping Weight Rules";		 //not approved2 ec3
$lang["rules_price"] 	 = "Set Shipping Price Rules";		 //not approved2 ec3
$lang["rules_item"] 	 = "Set Shipping Item Rules";		 //not approved2 ec3
$lang["default_price_rule"] 	 = "Default Price Rule";		 //approved2 ec3
$lang["default_price_rule:"] 	 = "Default Price Rule :";		 //not approved2 ec3
$lang["dolla"] 	 = "$";		 //approved2 ec3
$lang["weight_kg"] 	 = "Weight (Kg)";		 //approved2 ec3
$lang["price_min"] 	 = "Price (Min)";		 //approved2 ec3
$lang["price_max"] 	 = "Price (Max)";		 //approved2 ec3
$lang["cost"] 	 = "Cost";		 //approved2 ec3
$lang["additional"] 	 = "Additional cost";		 //approved2 ec3
$lang["batch_id"] 	 = "Batch ID";		 //approved2 ec3
$lang["source"] 	 = "Source";		 //approved2 ec3
$lang["type"] 	 = "Type";		 //approved2 ec3
$lang["error"] 	 = "Error";		 //approved2 ec3
$lang["export_date"] 	 = "Export Date";		 //not approved2 ec3
$lang["product"] 	 = "Products";		 //approved2 ec3
$lang["offer"] 	 = "Offer";		 //approved2 ec3
$lang["all"] 	 = "All";		 //approved2 ec3
$lang["all:"] 	 = "All :";		 //not approved2 ec3
$lang["ebay_not_found"] 	 = "File or directory not found";		 //approved2.1 ec3
$lang["default_templates"] 	 = "Default Templates";		 //not approved2 ec3
$lang["noaction_noimage"] 	 = "Can export products to eBay account if there is no image for the product";		 //not approved2 ec3
$lang["calculate_discount_price"] 	 = "Calculate discount price from Feed.biz if there is discount price";		 //not approved2 ec3
$lang["synchronize_with_products"] 	 = "Synchronize with products in ebay account. Before export products to eBay account";		 //not approved2 ec3
$lang["import_orders"] 	 = "Can import orders from eBay account. Even there is not to export products from Feed.biz to eBay account";		 //not approved2 ec3
$lang["info_tax"] 	 = "Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).{BR}Apply a specific price formula for eBay selected categories which will override the main setting.{BR}Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)";		 //not approved2 ec3
$lang["info_wizard_tax"] 	 = "Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).{BR}Apply a specific price formula for eBay selected categories which will override the main setting.{BR}Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)";		 //not approved2 ec3
$lang["info_actions"] 	 = "This will send all the products having a profile and within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go on the mapping and setting configuration in eBay options tab.{BR}After that, please do not forget to generate and save a report (For any support you will need the XML and the Report)";		 //not approved2 ec3
$lang["info_wizard_exports"] 	 = "This will send all the products having a profile and within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go on the mapping and setting configuration in eBay options tab.{BR}After that, please do not forget to generate and save a report (For any support you will need the XML and the Report)";		 //not approved2 ec3
$lang["info_mapping_categories"] 	 = "You can select many categories in one time by checking the first category then press {B}'Shift'{/B} and click on the last element you wish to be in the same profile.{BR}{B}1. Primary eBay Category{/B} - This is the main category where Buyers will find your item on eBay (not in your store, but on general eBay shopping). {BR}{B}2. Secondary eBay Category (optional){/B} - This field is optional. If you add a secondary category to a listing, you may later change it to a different one, but you will not be able to remove the use of a secondary category entirely. {BR}{B}3. Primary eBay Store Category (optional){/B} - if you have imported Store Categories, you will also be able to choose eBay Store categories when doing your Product Mapping. Store Categories are optional and will only apply if you have an eBay Store and have imported them for use in Feed.biz. {BR}{B}4. Secondary eBay Store Category (optional){/B} - you may choose a second category from your eBay Store category structure as well, if desired";		 //not approved2 ec3
$lang["info_mapping_templates"] 	 = "You can select many categories in one time by checking the first category then press {B}'Shift'{/B} and click on the last element you wish to be in the same profile";		 //not approved2 ec3
$lang["info_wizard_categories"] 	 = "You can select many categories in one time by checking the first category then press {B}'Shift'{/B} and click on the last element you wish to be in the same profile";		 //not approved2 ec3
$lang["info_ebay_wizard"] 	 = "If you are an newbie user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay Platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account";		 //not approved2 ec3
$lang["info_congratulation"] 	 = "Congratulation, your product have been sent to eBay successfully, you now can publish your offer in Action Tab";		 //not approved2 ec3
$lang["info_price_modifier"] 	 = "1. You could configure a price rule in value or percentage for one or several prices ranges.{BR}2. Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).{BR}3. Apply a specific price formula for eBay selected categories which will override the main setting.{BR}4. Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)";		 //not approved2 ec3
$lang["parameters"] 	 = "Parameters";		 //approved2 ec3
$lang["statistics"] 	 = "Statistics";		 //approved2 ec3
$lang["statistics_products"] 	 = "Product Statistics";		 //not approved2 ec3
$lang["mapping"] 	 = "Mapping";		 //approved2 ec3
$lang["mapping_attributes"] 	 = "Attributes";		 //approved2 ec3
$lang["mapping_attributes_value"] 	 = "Attribute Values";		 //not approved2 ec3
$lang["templates"] 	 = "Templates";		 //approved2 ec3
$lang["get_orders"] 	 = "Orders";		 //approved2 ec3
$lang["orders"] 	 = "Orders";		 //approved2 ec3
$lang["choose_a_attributes"] 	 = "Choose an Attributes";		 //not approved2 ec3
$lang["choose_ebay_attributes"] 	 = "Choose eBay Attributes";		 //not approved2 ec3
$lang["choose_a_categories"] 	 = "Choose a Categories";		 //not approved2 ec3
$lang["choose_ebay_categories"] 	 = "Choose eBay Categories";		 //approved2 ec3
$lang["choose_ebay_store_categories"] 	 = "Choose eBay Store Categories";		 //not approved2 ec3
$lang["get_store"] 	 = "Get Store Categories";		 //not approved2 ec3
$lang["choose_a_shippings"] 	 = "Choose a Shipping";		 //not approved2 ec3
$lang["choose_ebay_shippings"] 	 = "Choose eBay Shipping";		 //not approved2 ec3
$lang["choose_a_country"] 	 = "Choose a Country";		 //not approved2 ec3
$lang["choose_a_report"] 	 = "Choose a Report";		 //not approved2 ec3
$lang["associate_carrier"] 	 = "Choose an Associate Carrier";		 //not approved2 ec3
$lang["ebay_carrier"] 	 = "Choose an eBay Carrier";		 //not approved2 ec3
$lang["default_template"] 	 = "Default Template Mapping";		 //not approved2 ec3
$lang["select_a_option"] 	 = "Select a Option";		 //not approved2 ec3
$lang["returns_accepted"] 	 = "Returns Accepted";		 //not approved2 ec3
$lang["returns_not_accepted"] 	 = "Returns not accepted";		 //approved2 ec3
$lang["no_tax"] 	 = "No Tax";		 //not approved2 ec3
$lang["days_1"] 	 = "1 Day";		 //not approved2 ec3
$lang["days_2"] 	 = "2 Days";		 //not approved2 ec3
$lang["days_3"] 	 = "3 Days";		 //not approved2 ec3
$lang["days_4"] 	 = "4 Days";		 //not approved2 ec3
$lang["days_5"] 	 = "5 Days";		 //not approved2 ec3
$lang["days_7"] 	 = "7 Days";		 //not approved2 ec3
$lang["days_10"] 	 = "10 Days";		 //not approved2 ec3
$lang["days_14"] 	 = "14 Days";		 //not approved2 ec3
$lang["days_15"] 	 = "15 Days";		 //not approved2 ec3
$lang["days_20"] 	 = "20 Days";		 //not approved2 ec3
$lang["days_30"] 	 = "30 Days";		 //not approved2 ec3
$lang["days_60"] 	 = "60 Days";		 //not approved2 ec3
$lang["buyer"] 	 = "Buyer";		 //approved2 ec3
$lang["seller"] 	 = "Seller";		 //approved2 ec3
$lang["green"] 	 = "green";		 //not approved2 ec3
$lang["greenNew"] 	 = "greenNew";		 //not approved2 ec3
$lang["blue"] 	 = "blue";		 //not approved2 ec3
$lang["percent"] 	 = "%";		 //not approved2 ec3
$lang["percentage"] 	 = "Percentage";		 //approved2 ec3
$lang["value"] 	 = "Value";		 //approved2 ec3
$lang["export"] 	 = "Export";		 //approved2 ec3
$lang["endExport"] 	 = "Delete Export";		 //not approved2 ec3
$lang["sub_export"] 	 = "eBay Export Product";		 //not approved2 ec3
$lang["sub_end_export"] 	 = "eBay Delete Export Product";		 //not approved2 ec3
$lang["wizard_authorization"] 	 = "Authorization";		 //approved2 ec3
$lang["wizard_choose"] 	 = "Choose Site";		 //not approved2 ec3
$lang["wizard_univers"] 	 = "Select Univers";		 //not approved2 ec3
$lang["wizard_categories"] 	 = "Categories";		 //approved2 ec3
$lang["wizard_tax"] 	 = "Tax";		 //approved2 ec3
$lang["wizard_shippings"] 	 = "Shippings";		 //not approved2 ec3
$lang["wizard_exports"] 	 = "Export Products";		 //approved2 ec3
$lang["wizard_finish"] 	 = "Finish";		 //approved2 ec3
$lang["conditions"] 	 = "Conditions";		 //approved2 ec3
$lang["marketplace_condition"] 	 = "Marketplace Condition";		 //not approved2 ec3
$lang["reset_condition"] 	 = "Reset Condition";		 //not approved2 ec3
$lang["your_conditions"] 	 = "Your Condition";		 //not approved2 ec3
$lang["auth_security_token"] 	 = "Auth security token";		 //approved2 ec3
$lang["new_auth_security"] 	 = "New Auth security";		 //not approved2 ec3
$lang["auth_security_session"] 	 = "Authentication security session";		 //approved2 ec3
$lang["welcome"] 	 = "welcome";		 //not approved2 ec3
$lang["Configuration"] 	 = "Configuration";		 //approved2 ec3
$lang["choosesite"] 	 = "Choose eBay Site";		 //not approved2 ec3
$lang["importing"] 	 = "Importing";		 //approved2 ec3
$lang["product_id"] 	 = "Product ID";		 //approved2 ec3
$lang["user"] 	 = "User";		 //approved2 ec3
$lang["name"] 	 = "Name";		 //approved2 ec3
$lang["response"] 	 = "Response";		 //approved2 ec3
$lang["message"] 	 = "Message";		 //approved2 ec3
$lang["id_order"] 	 = "ID Order";		 //not approved2 ec3
$lang["id_marketplace_order_ref"] 	 = "ID Order Ref";		 //not approved2 ec3
$lang["id_buyer"] 	 = "ID Buyer";		 //not approved2 ec3
$lang["payment_method"] 	 = "Payment By";		 //not approved2 ec3
$lang["total_paid"] 	 = "Paid Amount";		 //not approved2 ec3
$lang["order_date"] 	 = "Order date";		 //approved2 ec3
$lang["orders_processing"] 	 = "Processing: Orders";		 //approved2 ec3
$lang["invoice_no"] 	 = "ID Invoice";		 //not approved2 ec3
$lang["country_name"] 	 = "Country Name";		 //not approved2 ec3
$lang["product_export_statistic"] 	 = "Statistic Export Product";		 //not approved2 ec3
$lang["ebay_wizard_configuration"] 	 = "eBay Configuration Wizard";		 //not approved2 ec3
$lang["warning"] 	 = "Warning";		 //approved2 ec3
$lang["You can't save token in this time. Please try again."] 	 = "You can't save token in this time. Please try again";		 //not approved2 ec3
$lang["ebay_wizard"] 	 = "eBay Wizard";		 //not approved2 ec3
$lang["exported"] 	 = "Exported";		 //approved2 ec3
$lang["email"] 	 = "Email";		 //approved2 ec3
$lang["Amount"] 	 = "Amount";		 //approved2 ec3
$lang["Site"] 	 = "Site";		 //approved2 ec3
$lang["eBay"] 	 = "eBay";		 //approved2 ec3
$lang["infomation"] 	 = "Information";		 //approved2 ec3
$lang["reset_categories"] 	 = "Reset Categories";		 //not approved2 ec3
$lang["reset_templates"] 	 = "Reset Templates";		 //not approved2 ec3
$lang["send"] 	 = "Send to eBay Account";		 //not approved2 ec3
$lang["send_products_to_ebay"] 	 = "Send Products to eBay";		 //approved2 ec3
$lang["send_offers_to_ebay"] 	 = "Send Offers to eBay";		 //approved2 ec3
$lang["sending.."] 	 = "Sending";		 //approved2.1 ec3
$lang["get_from_ebay"] 	 = "Get Orders from eBay";		 //approved2 ec3
$lang["removing.."] 	 = "Removing";		 //approved2.1 ec3
$lang["starting_get"] 	 = "Starting Get";		 //not approved2 ec3
$lang["starting_delete"] 	 = "Delete";		 //approved2 ec3
$lang["starting_send"] 	 = "Starting Send";		 //not approved2 ec3
$lang["delete"] 	 = "Delete on eBay Account";		 //not approved2 ec3
$lang["Statistic Export Product"] 	 = "Statistic Export Product";		 //not approved2 ec3
$lang["eBay Username"] 	 = "eBay Username";		 //not approved2 ec3
$lang["Product Name"] 	 = "Product Name";		 //not approved2 ec3
$lang["AddFixedPriceItem"] 	 = "AddFixedPriceItem";		 //not approved2 ec3
$lang["RelistFixedPriceItem"] 	 = "Relist Fixed Price Items";		 //approved2 ec3
$lang["ReviseFixedPriceItem"] 	 = "Revise Fixed Price Items";		 //approved2 ec3
$lang["Compressed"] 	 = "Compressed";		 //approved2 ec3
$lang["validation"] 	 = "Validation";		 //approved2 ec3
$lang["success"] 	 = "Success";		 //approved2 ec3
$lang["failure"] 	 = "Failure";		 //not approved2 ec3
$lang["Search Message"] 	 = "Search Message";		 //not approved2 ec3
$lang["order"] 	 = "Import orders from eBay ";		 //approved2 ec3
$lang["delete_on_ebay"] 	 = "Delete Products on eBay";		 //approved2 ec3
$lang["Warning! Configuration parameter not complete."] 	 = "Warning! Configuration parameter not complete";		 //not approved2 ec3
$lang["You can't save configuration in this time. Please try again."] 	 = "You can't save configuration in this time. Please try again";		 //not approved2 ec3
$lang["Carrier Configuration"] 	 = "Carrier Configuration";		 //not approved2 ec3
$lang["You can't save mapping shipping in this time. Please try again."] 	 = "You can't save mapping shipping in this time. Please try again";		 //not approved2 ec3
$lang["You can't save mapping category in this time. Please try again."] 	 = "You can't save mapping category in this time. Please try again";		 //not approved2 ec3
$lang['You can select many category name in one time by selecting the first category name then press "Shift" and click on the last element you wish to be in the same category.'] 	 = 'You can select many category name in one time by selecting the first category name then press "Shift" and click on the last element you wish to be in the same category';		 //not approved2 ec2
$lang["Last Export Log"] 	 = "Last Export Log";		 //not approved2 ec3
$lang["This will send all the products having a profile and within the selected categories in your module configuration to ebay accout and quantity was 0."] 	 = "This will send all the products having a profile and within the selected categories in your module configuration to ebay accout and quantity was 0";		 //not approved2 ec3
$lang['If you want to active a product to the ebay list, please go on the task and choose "action" in eBay options tab and choose "Send Product to eBay" again.'] 	 = 'If you want to active a product to the ebay list, please go on the task and choose "action" in eBay options tab and choose "Send Product to eBay" again';		 //not approved2 ec2
$lang["After that, please do not forget to send product in task menu and click action menu  (For any support you will need the XML and the Report)."] 	 = "After that, please do not forget to send product in task menu and click action menu  (For any support you will need the XML and the Report)";		 //not approved2 ec3
$lang["export_from_wizard"] 	 = "Export from wizard";		 //not approved2 ec3
$lang["ebay_wizard_clear"] 	 = "ebay_wizard_clear";		 //not approved2 ec3
$lang["ebay_wizard_start"] 	 = "ebay_wizard_start";		 //not approved2 ec3
$lang["mapping_ebay_save"] 	 = "mapping_ebay_save";		 //not approved2 ec3
$lang["statistic_processing"] 	 = "Processing: Statistics";		 //approved2 ec3
$lang["Reset Categories"] 	 = "Reset Categories";		 //not approved2 ec3
$lang["Reset Templates"] 	 = "Reset Templates";		 //not approved2 ec3
$lang["mapping_carriers_save"] 	 = "mapping carriers save";		 //not approved2 ec3
$lang["auth_security_save"] 	 = "auth security save";		 //not approved2 ec3
$lang["wizard_statistic_link"] 	 = "wizard statistic link";		 //not approved2 ec3
$lang["Save Mappings & Continue"] 	 = "Save Mappings and click Continue";		 //approved2 ec3
$lang["ebay_wizard_shipping"] 	 = "ebay_wizard_shipping";		 //not approved2 ec3
$lang["attribute_ebay_save"] 	 = "attribute_ebay_save";		 //not approved2 ec3
$lang["attribute_ebay_value_save"] 	 = "attribute_ebay_value_save";		 //not approved2 ec3
$lang["univers_save"] 	 = "univers_save";		 //not approved2 ec3
$lang["test_ebay"] 	 = "test_ebay";		 //not approved2 ec3
$lang["Exporting"] 	 = "Exporting";		 //approved2 ec3
$lang["Confirm Export"] 	 = "Confirm Export";		 //not approved2 ec3
$lang["Export"] 	 = "Export";		 //approved2 ec3
$lang["ebay_univers"] 	 = "Select Univers";		 //not approved2 ec3
$lang["tax_save"] 	 = "tax_save";		 //not approved2 ec3
$lang["get_postalcode_valid"] 	 = "get_postalcode_valid";		 //not approved2 ec3
$lang["save_successful"] 	 = "Changes Updated";		 //approved2 ec3
$lang["records"] 	 = "Records";		 //approved2 ec3
$lang["Add Mapping Attributes"] 	 = "Add Attributes Mapping";		 //approved2 ec3
$lang["get_configuration"] 	 = "get_configuration";		 //not approved2 ec3
$lang["templates_download"] 	 = "templates download";		 //not approved2 ec3
$lang["download"] 	 = "Download";		 //approved2 ec3
$lang["delete_only"] 	 = "Delete";		 //approved2 ec3
$lang["preview_only"] 	 = "Preview";		 //approved2 ec3
$lang["file_only"] 	 = "Choose/Upload a File";		 //approved2 ec3
$lang["new_only"] 	 = "New";		 //approved2 ec3
$lang["choose_file"] 	 = "Choose File";		 //not approved2 ec3
$lang["upload_template"] 	 = "Upload Template";		 //not approved2 ec3
$lang["custom_templates"] 	 = "Custom Templates";		 //not approved2 ec3
$lang["are_you_sure"] 	 = "Are you sure?";		 //approved2 ec3
$lang["add_update_template"] 	 = "Add / Update Template";		 //not approved2 ec3
$lang["ebay_wizard_site_change"] 	 = "ebay wizard site change";		 //not approved2 ec3
$lang["ebay_wizard_finish"] 	 = "eBay wizard finish";		 //not approved2 ec3
$lang["ebay_univers_not_selected"] 	 = "You have not selected univers. Please select univers first";		 //not approved2 ec3
$lang["category_not_selected"] 	 = "You have not selected categories. Please select category first, on menu <b>'My feeds > Parameters > Category'</b>";		 //not approved2 ec3
$lang["mode"] 	 = "Mode";		 //approved2 ec3

  