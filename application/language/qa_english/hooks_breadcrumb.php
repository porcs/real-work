<?php

class Hooks_breadcrumb
{
    const GENERAL = 'General';
    function set_breadcrumb()
    {
        $ci =&get_instance();
        
        $ci->breadcrumb->clear();
        $uri = array();
        $skip = false;   
        $common_path='';
        $lcp=array();
        foreach ($ci->uri->segments as $key => $segment){
            if($key <= 0 ) $key = 1;  
            if(($segment!='' && ($segment != 'tasks') && strpos($segment,'ajax') === false && strpos($segment,'remote') === false && $segment != 'errors' && $segment != 'webservice' && !is_numeric($segment))){
               $skip=false;
            }else{
                $skip=true;
            }
            if($skip)break;
            $lcp[($key - 1)] = $segment; 
        }
        if(!empty($lcp)){
            $common_path = implode('/',$lcp);
        }
        
        $this->language = $ci->session->userdata('language') ? $ci->session->userdata('language') : $ci->config->item('language'); 
        $ci->lang->load("page_title", $this->language); 
        if($ci->lang->check_file_line('page_title',$common_path)){
            $ci->smarty->assign("page_title_specify", $ci->lang->line($common_path,'page_title')); 
        }
        $skip_nbc=true;
        $ci->lang->load("breadcrumb", $this->language); 
        $t=array();
        if($ci->lang->check_file_line('breadcrumb',$common_path)){
            $txt_bc = $ci->lang->line($common_path,'breadcrumb');
//            file_put_contents('/var/www/backend/assets/apps/fifo_dir/check_bc.txt', print_r(array($txt_bc,$ci->lang->language_by_file['breadcrumb']),true),FILE_APPEND);

            $list_bc = explode('>',$txt_bc);
            if(is_array($list_bc)){
                foreach($list_bc as $k=>$bc_elem){
                    $bc_elem = trim($bc_elem);
                    $ci->breadcrumb->add_crumb($bc_elem);
                    $skip_nbc=false;
                    $t[] = $bc_elem;
                }
            }else{
                if(!is_bool($list_bc) && !empty($list_bc)){
                    $ci->breadcrumb->add_crumb($list_bc);
                    $skip_nbc=false;
                }  
            } 
            
        }
        
        
        if($skip_nbc===true){ 
            $ci->breadcrumb->clear();
            $skip = false;
            $ci->load->model('marketplace_model', 'marketplace'); 
            //echo '<pre>'; print_r($ci->uri->segments); exit;
            foreach ($ci->uri->segments as $key => $segment)
            {            
                if($skip == true) {
                    break;
                }

                if($key <= 0 ) $key = 1;

                $uri[($key - 1)] = $segment;
                if($segment!='' && ($segment != 'tasks') && strpos($segment,'ajax') === false && strpos($segment,'remote') === false && $segment != 'errors' && $segment != 'webservice' && !is_numeric($segment)){
                    if(isset($uri[0]) && $uri[0] != strtolower(self::GENERAL) ){
                        $ci->breadcrumb->add_crumb(str_replace("_", " ", ($ci->lang->line($segment)) ? $ci->lang->line($segment) : ucfirst($segment))); 
                    }
                    $skip = false;
                }else{
                    if(isset($uri[0]) && $uri[0] == strtolower(self::GENERAL) ) {

                        if( isset($uri[2]) && isset($uri[3]) ) {
                            // get sub marketplace from offer_price_package
                            $general = $ci->marketplace->getMarketplaceOffer(self::GENERAL);
                            $this->id_general = isset($general[0]['id_offer_pkg']) ? $general[0]['id_offer_pkg'] : 0;                        
                            if($uri[1] == 'actions'){
                                $sub_marketplace = $ci->marketplace->getSubMarketplaceOfferGeneral($this->id_general, null, null, $ci->uri->segments[5], $uri[3]);
                            }else{
                                $sub_marketplace = $ci->marketplace->getSubMarketplaceOfferGeneral($this->id_general, null, null, $uri[3], $uri[2]);
                            }
                            $sub_marketplace_segment = isset($sub_marketplace[0]['comments']) ? $sub_marketplace[0]['comments'] : $uri[2];
                            // add to breadcrumb
                            $ci->breadcrumb->add_crumb(str_replace("_", " ", $sub_marketplace_segment)); 
                        } 
                        if( $key == 4 && isset($uri[1]) ){
                            $ci->breadcrumb->add_crumb(str_replace("_", " ", $ci->lang->line($uri[1]) )); 
                        }

                    }else{
                        $skip = true;
                    }
                }
                //
            }
        }
        //var_dump($uri); exit;
        //exit;
                    file_put_contents('/var/www/backend/assets/apps/fifo_dir/check_bc.txt', print_r(array($ci->breadcrumb->output()),true),FILE_APPEND);

        $ci->smarty->assign( "url" , $uri );
        $ci->smarty->assign("breadcrumb" , $ci->breadcrumb->output() );
    }
}
