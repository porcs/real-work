<?php 
$lang["Amazon%s Started Order Query"] 	 = "Amazon%s: Order query started ";		 //approved1 ec3
$lang["Start Get Order List"] 	 = "Start Get Order List";		 //not approved2 ec3
$lang["See all messages"] 	 = "See all messages";		 //not approved2 ec3
$lang["Getting %s Order List"] 	 = "Getting %s Order List";		 //not approved2 ec3
$lang["Amazon Get Orders Error"] 	 = "Amazon Get Orders Error";		 //not approved2 ec3
$lang["No pending order for %s to %s"] 	 = "No pending order for %s to %s";		 //not approved2 ec3
$lang["Order Query Processing"] 	 = "Order Query Processing";		 //not approved2 ec3
$lang["Unable to connect Amazon"] 	 = "Unable to connect Amazon";		 //not approved2 ec3
$lang["Order #%s, Was already imported"] 	 = "Order #%s, Was already imported";		 //not approved2 ec3
$lang["Order #%s, Missing Buyer Address for this order"] 	 = "Order #%s: Buyer's Address is missing for this order";		 //approved1 ec3
$lang["Order #%s, Missing Buyer Name for this order"] 	 = "Order #%s: Buyer's name is missing for this order";		 //approved1 ec3
$lang["Order #%s, Missing Buyer Email for this order"] 	 = "Order #%s: Buyer's email is missing for this order";		 //approved1 ec3
$lang["Order ID (%s) Could not import incomplete order"] 	 = "Order ID (%s): Couldn't import Order as it is incomplete";		 //approved1 ec3
$lang["Order ID (%s) Item does not have SKU - #%s"] 	 = "Order ID (%s): Item doesn't have an SKU - #%s";		 //approved1 ec3
$lang["Order ID (%s) Could not import product SKU : %s"] 	 = "Order ID (%s): Couldn't import Product with SKU: %s";		 //approved1 ec3
$lang["Order ID (%s) Could not add buyer"] 	 = "Order ID (%s): Couldn't add buyer";		 //approved1 ec3
$lang["Unable to associate the carrier (%s) for order #%s"] 	 = "This is not an error! You must complete Carrier Mapping in Configuration. Currently, unable to find the carrier (%s) for order #%s";		 //approved1 ec3
$lang["Unable to execute parameters for order #%s"] 	 = "Unable to execute Parameters for order #%s";		 //approved1 ec3
$lang["Unable to save order items"] 	 = "Unable to save Order items";		 //approved1 ec3
$lang["Success"] 	 = "Success";		 //approved1 ec3
$lang["inactivate_import_orders"] 	 = "Unable to Import Orders: Go to Features to activate Import Orders";		 //approved1 ec3
$lang["Order #%s, Skipping Cancelled Order"] 	 = "Order #%s: Skipping cancelled Orders";		 //approved2 ec3
$lang["Product items mismatch"] 	 = "Products do not match";		 //approved1 ec3

  