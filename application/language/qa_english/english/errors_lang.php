<?php 
$lang["page_title"] 	 = "Error";		 //approved2 ec3
$lang["Error Message"] 	 = "Error Message";		 //approved1 ec3
$lang["We found some error, on"] 	 = "We found some error, on";		 //not approved2 ec3
$lang["this error has send to admin"] 	 = "This error was sent to Support";		 //approved1 ec3
$lang["We found an error, on"] 	 = "We found an error, in";		 //approved1 ec3
$lang["errors"] 	 = "Error";		 //approved1 ec3
$lang["getErrorHandler"] 	 = "getErrorHandler";		 //not approved2 ec3
$lang["Oops..."] 	 = "Oops";		 //approved1 ec3
$lang["the page you are looking for cannot be found"] 	 = "The page you are looking for couldn't be found";		 //approved1 ec3
$lang["Errors can be caused due to various reasons, such as:"] 	 = "Errors can be due to various reasons, such as:";		 //approved1 ec3
$lang["Page requested does not exist."] 	 = "Page does not exist";		 //approved1 ec3
$lang["Server is down."] 	 = "Server is down";		 //approved1 ec3
$lang["Internet connection is down."] 	 = "Your internet connection is down";		 //approved1 ec3
$lang["Broken links"] 	 = "Broken links";		 //approved1 ec3
$lang["Incorrect URL"] 	 = "Incorrect URL";		 //approved1 ec3
$lang["Page has been moved to a different address"] 	 = "This page has been moved";		 //approved1 ec3
$lang["Back"] 	 = "Back";		 //approved2 ec3

  