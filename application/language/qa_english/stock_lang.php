<?php 
$lang["page_title"] 	 = "Stock";		 //not approved2 ec3
$lang["stock"] 	 = "Stock";		 //not approved2 ec3
$lang["report"] 	 = "Report";		 //approved2 ec3
$lang["Date"] 	 = "Date";		 //approved1 ec3
$lang["Detail"] 	 = "Details";		 //approved2 ec3
$lang["Quantity"] 	 = "Quantity";		 //approved2 ec3
$lang["Balance Quantity"] 	 = "Balance";		 //approved1 ec3
$lang["No data available in table"] 	 = "No data available ";		 //approved2 ec3
$lang["Download CSV"] 	 = "Download CSV";		 //approved1 ec3
$lang["Stock Movement"] 	 = "Stock Movement";		 //approved1 ec3
$lang["Reference"] 	 = "Reference";		 //approved2 ec3
$lang["Movement Quantity"] 	 = "Moved";		 //approved1 ec3
$lang["products"] 	 = "Products";		 //approved1 ec3
$lang["Next"] 	 = "Next";		 //approved2 ec3
$lang["Order from %s (order id:%s)"] 	 = "Orders from %s (Order id: %s)";		 //approved1 ec3
$lang["Delete"] 	 = "Delete";		 //approved2 ec3
$lang["Product Strategies"] 	 = "Product Strategies";		 //approved2 ec3
$lang["Feed update offer from"] 	 = "Updated Offers from";		 //approved1 ec3
$lang["Repricing"] 	 = "Repricing";		 //approved2 ec3
$lang["Other"] 	 = "Other";		 //approved2 ec3
$lang["%s from %s"] 	 = "%s from %s";		 //approved1 ec3
$lang["Service Settings"] 	 = "Service Settings";		 //approved2 ec3
$lang["Feed update offer from "] 	 = "Updated Offers from";		 //approved1 ec3
$lang["Strategies"] 	 = "Strategies";		 //approved2 ec3

  