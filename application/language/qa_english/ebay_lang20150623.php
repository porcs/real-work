<?php

$lang['page_title']                     = "eBay";
$lang['ebay']                           = "eBay";
$lang['host_url']                       = "FeedBiz";

$lang['production_key']                 = "Production mode";
$lang['sandbox_key']                    = "Sandbox mode";
$lang['configuration']                  = "Configuration";

$lang['sub_title']                      = "eBay authentication & security API";
$lang['sub_setting']                    = "eBay settings";
$lang['reset']                          = "Reset";
$lang['back']                           = "Back";
$lang['show']                           = "Show";
$lang['hide']                           = "Hide";
$lang['prev']                           = "Previous";
$lang['save']                           = "Save and continue";
$lang['save_only']                      = "Save";
$lang['next']                           = "Next";
$lang['check_all']                      = "Check all";
$lang['uncheck_all']                    = "Uncheck all";
$lang['collapse_all']                   = "Collapse all";
$lang['expand_all']                     = "Expand all";
$lang['submit']                         = "Submit";
$lang['new_token']                      = "Generate a new token";
$lang['load_catagories']                = "Load categories";
$lang['hack']                           = "You are trying to hack the system.";
$lang['new']                            = "New";
$lang['good']                           = "Good";
$lang['used']                           = "Used";
$lang['refurbished']                    = "Refurbished";
$lang['acceptable']                     = "Acceptable";
$lang['to']                             = "to";
$lang['show_all']                       = "Show All";
$lang['yes_to_all']                     = "Yes to all (If click will skip this popup.) ";
$lang['confirm_synchronize']            = "Confirm";
$lang['get_start']                      = "Get Started";
$lang['let_get_start']                  = "Let's get started";
$lang['generate_token']                 = "Generate eBay Token";
$lang['default']                        = "Default";

$lang['user_id']                        = "eBay UserID";
$lang['postcode']                       = "Postcode";
$lang['paypal_email']                   = "Paypal email";
$lang['paypal']                         = "Paypal";
$lang['address']                        = "Address";
$lang['visa_mastercard']                = "Visa / MasterCard";
$lang['discover']                       = "Discover";
$lang['american_express']               = "American Express";
$lang['money_order']                    = "Money order / Cashier's check";
$lang['personal_check']                 = "Personal check";
$lang['pay_on_pickup']                  = "Pay on pickup";
$lang['other_payment']                  = "Other payment";
$lang['dispatch_time']                  = "Dispatch time";
$lang['other_option']                   = "Other Options";
$lang['option_add_product']             = "Export Product";
$lang['option_orders_product']          = "Import Orders";
$lang['merchant_credit_cards']          = "Accepted Payment";
$lang['category']                       = "Categories";
$lang['processed']                      = "Processed";
$lang['product(s)']                     = "Product(s)";
$lang['success']                        = "Success";
$lang['warning']                        = "Warning";
$lang['error']                          = "Error";
$lang['see_more_detail']                = "See more details";
$lang['click_this']                     = "Click this";
$lang['gtc']                            = "Good Till Cancelled (Renews for additional fees every 30 days)";
$lang['not_have_store']                 = "You have not set up any store categories on eBay.";
$lang['payment_instructions']           = "Payment Instructions";
$lang['product_item']                   = "ID";
$lang['product_title']                  = "Title";
$lang['db_product_title']               = "Database Title";
$lang['product_manufacturer']           = "Manufacturer";
$lang['product_supplier']               = "Supplier";
$lang['product_quantity']               = "Quantity";
$lang['product_price']                  = "Price";
$lang['product_reference']              = "Reference";
$lang['product_sku']                    = "SKU";
$lang['product_ean13']                  = "EAN13";
$lang['product_upc']                    = "Upc";
$lang['product_image']                  = "Picture";
$lang['product_content']                = "Contents";
$lang['product_information']            = "Information";
$lang['product_detail']                 = "Details";
$lang['active']                         = "Active";
$lang['unactive']                       = "Inactive";
$lang['category_name']                  = "Category";
$lang['condition_name']                 = "Condition";
$lang['choose_mapping']                 = "Choose Mapping";
$lang['choose_mapping_against']         = "Choose Mapping Against";
$lang['choose_profile']                 = "Choose Profile";
$lang['clear']                          = "Clear";
$lang['replace_profile']                = "Replace Mapping Profile Name";
$lang['confirm_auto_map']               = "Confirm Auto Matching";
$lang['confirm_all']                    = "click to confirm replace all (If click will confirm replace all and skip this popup.) ";
$lang['skip_all']                       = "click to skip replace all  (If click will not replace all and skip this popup.) ";
$lang['order_status']                   = "Order status";
$lang['variation_warning_title']        = "The page you are looking for cannot be shown.";
$lang['variation_warning']              = "Warning can be caused due to various reasons, such as:";
$lang['categories_no_attributes']       = "Categories requested does not exist.";
$lang['no_attributes']                  = "Categories requested cannot have attribute.";
$lang['id_profile_not_found']           = "ID Profile not found.";
$lang['condition_not_found'] 			= "Condition not found under the mapping categories. All products are labeled as NEW automatically.";
$lang['mapping_not_found']              = "Category Mapping is not found. Please do mapping on previous step.";
$lang['mapping_invalid']                = "Mapping category is invalid.";
$lang['add_data']                       = "Add a model";
$lang['add_profile']                    = "Add a profile to the list";




$lang['user_not_available']             = "This eBay account is already used.";
$lang['auth_is_processing']             = "Authentication is processing....";
$lang['synchronization_is_processing']             = "Synchronization is processing....";



$lang['update_auth_security_success']               = "Update security authorization successful.";
$lang['update_auth_security_fail']                  = "Data incorrect : Please, try again.";

$lang['update_integration_success']                 = "Generation of eBay token successful.";
$lang['update_integration_fail']                    = "Generation of eBay token failed : Please, try again.";

$lang['update_authentication_success']              = "Generation of eBay token successful.";
$lang['update_authentication_fail']                 = "Generation of eBay token failed : Please, try again.";

$lang['update_auth_setting_success']                = "Configuration update successful."; 											
$lang['update_auth_setting_fail']                   = "Configuration update failed : Please, try again."; 														
$lang['update_univers_success']                     = "Update selected categories successful."; 
													
$lang['update_univers_fail']                        = "Update selected categories failed : Please, try again."; 
														 
$lang['update_mapping_categories_success']          = "Update categories successful.";

$lang['update_mapping_categories_fail']             = "Update categories failed : Please, try again.";
$lang['update_mapping_templates_success']           = "Update templates successful.";

$lang['update_mapping_templates_fail']              = "Update templates failed : Please, try again.";
$lang['update_templates_success']                   = "Update templates successful.";

$lang['update_templates_fail']                      = "Update templates failed : Please, try again.";
$lang['update_mapping_conditions_success']          = "Update conditions successful.";
$lang['update_mapping_conditions_fail']             = "Update conditions failed : Please, try again."; 

$lang['update_wizard_conditions_success']           = "Update conditions successful."; 
$lang['update_wizard_conditions_fail']              = "Update conditions failed : Please, try again."; 

$lang['update_mapping_carriers_success']            = "Update shipping successful."; 												 
$lang['update_mapping_carriers_fail']               = "Update shipping failed : Please, try again.";
$lang['update_mapping_carriers_cost_success']       = "Update shipping rules successful."; 												 
$lang['update_mapping_carriers_cost_fail']          = "Update shipping rules failed : Please, try again.";
$lang['update_tax_success']                         = "Update tax successful.";

$lang['update_tax_fail']                            = "Update tax failed : Please, try again.";
$lang['update_price_modifier_success']              = "Update price successful.";

$lang['update_price_modifier_fail']                 = "Update price failed : Please, try again.";
$lang['update_actions_delete_success']              = "Delete product Successful.";

$lang['update_actions_delete_fail']                 = "Delete product failed : Please, try again.";
$lang['update_actions_delete_success']              = "Delete product Successful.";

$lang['update_actions_delete_fail']                 = "Delete product failed : Please, try again.";
$lang['update_actions_export_success']              = "Export product successful.";

$lang['update_actions_send_mail']                   = "Send product to eBay and waiting for email response when completed.";
$lang['update_actions_export_fail']                 = "Export product failed : Please, try again.";
$lang['update_actions_orders_success']              = "Get orders successful.";

$lang['update_actions_orders_fail']                 = "Get orders failed : Please, try again.";
$lang['update_actions_error']                       = "Connection failed! Please try again.";
$lang['update_actions_error_msg']                   = "An error has occurred.";//"There has error something that is found, especially an unexpectedly discovery.";
$lang['update_wizard_choose_success']               = "Update country site successful.";
$lang['update_wizard_choose_fail']                  = "Update country site failed : Please, try again.";
$lang['update_wizard_univers_success']              = "Update selected categories successful.";

$lang['no_online_product']              			= "'%s' never upload product to Ebay.";
$lang['xml_validation_error']              			= "XML Data is invalid. Please contact administrator.";
$lang['empty_products']              				= "There are no product for upload. Please check your imported product and Ebay configuration again."; 
												
$lang['update_wizard_univers_fail']                 = "Update categories selected failed : Please, try again.";
$lang['update_attribute_univers_success']           = "Update selected attribute successful.";
												
$lang['update_attribute_univers_fail']              = "Update selected attribute failed : Please, try again.";
$lang['update_wizard_categories_success']           = "Update categories successful.";

$lang['update_wizard_categories_fail']              = "Update categories failed : Please, try again.";
$lang['update_wizard_tax_success']                  = "Update tax successful.";

$lang['update_wizard_tax_fail']                     = "Update tax failed : Please, try again.";
$lang['update_wizard_authorization_success']        = "Update security auth successful.";

$lang['update_wizard_authorization_fail']           = "Data incorrect : Please, try again.";
$lang['update_wizard_shippings_success']            = "Update security auth successful.";


$lang['update_wizard_shippings_fail']               = "Data incorrect : Please, try again.";
$lang['update_wizard_exports_export_success']       = "Export product successful.";

$lang['update_wizard_exports_export_fail']          = "Export product failed : Please, try again.";
$lang['update_wizard_exports_success']              = "Export product successful.";

$lang['update_actions_offers_fail']          = "Export offers failed : Please, try again.";
$lang['update_actions_offers_success']              = "Export offers successful.";

$lang['update_variation_success']						= "Update variation Success";
$lang['update_variation_fail']						= "Update variation Fail";
$lang['update_variation_values_success']				= "Update variation Values Success";
$lang['update_variation_values_fail']					= "Update variation Values Fail";
														 
$lang['update_wizard_exports_fail']                 = "Export Product Failed : Please, try again."; 
$lang['update_wizard_exports_error']                = "Connection Failed! Please try again."; 
$lang['update_wizard_exports_error_msg']            = "An error has occurred.";//"There has error something that is found, especially an unexpectedly discovery."; 
$lang['update_wizard_exports_send_mail']            = "Send product to eBay and waiting for email response when completed.";
$lang['update_wizard_payments_success']             = "Update Profile Payment Success."; 
$lang['update_wizard_payments_fail']                = "Update Profile Payment Failed : Please, try again."; 
$lang['update_wizard_returns_success']              = "Update Profile Return Success."; 
$lang['update_wizard_returns_fail']                 = "Update Profile Return Failed : Please, try again."; 
$lang['update_templates_preview_success']           = "Preview Template successful.";
														 
$lang['update_templates_preview_fail']              = "Not able to preview template: Please try again.";
$lang['update_templates_delete_success']            = "Delete template successful.";
														 
$lang['update_templates_delete_fail']               = "Delete template failed : Please, try again.";
$lang['update_templates_add_success']               = "Add template successful.";
														
$lang['update_templates_add_fail']                  = "Add template failed : Please, try again.";
$lang['update_advance_categories_main_status_success'] = "Update Profile Status Successful.";
$lang['update_advance_categories_main_status_fail'] = "Update Profile Status Failed : Please, try again.";
$lang['update_advance_categories_main_success']     = "Update Profile Successful.";
$lang['update_advance_categories_main_fail']        = "Update Profile Failed : Please, try again.";
$lang['update_advance_categories_main_delete_success'] = "Delete Profile Successful.";
$lang['update_advance_categories_main_delete_fail'] = "Delete Profile Failed : Please, try again.";
$lang['update_advance_categories_main_add_success'] = "Create Profile Successful.";
$lang['update_advance_categories_main_add_fail']    = "Create Profile Failed : Please, try again.";
$lang['update_advance_main_success']                = "Update Profile Rule Successful.";
$lang['update_advance_main_fail']                   = "Update Profile Rule Failed : Please, try again.";
$lang['update_advance_profile_configuration_success']   = "Update Profile Detail Successful.";
$lang['update_advance_profile_configuration_fail']  = "Update Profile Detail Failed : Please, try again.";
$lang['update_advance_profile_payment_success']     = "Update Profile Payment Successful.";
$lang['update_advance_profile_payment_fail']        = "Update Profile Payment Failed : Please, try again.";
$lang['update_advance_profile_return_success']      = "Update Profile Return Successful.";
$lang['update_advance_profile_return_fail']         = "Update Profile Return Failed : Please, try again.";
$lang['update_advance_profile_description_success'] = "Update Profile Description Successful.";
$lang['update_advance_profile_description_fail']    = "Update Profile Description Failed : Please, try again.";
$lang['update_advance_profile_condition_success']   = "Update Profile Condition Successful.";
$lang['update_advance_profile_condition_fail']      = "Update Profile Condition Failed : Please, try again.";
$lang['update_advance_profile_shipping_success']    = "Update Profile Shipping Successful.";
$lang['update_advance_profile_shipping_fail']       = "Update Profile Shipping Failed : Please, try again.";
$lang['update_advance_profile_variation_success']   = "Update Profile Variation Successful.";
$lang['update_advance_profile_variation_fail']      = "Update Profile Variation Failed : Please, try again.";
$lang['update_advance_profile_specific_success']    = "Update Profile Item Specific Successful.";
$lang['update_advance_profile_specific_fail']       = "Update Profile Item Specific Failed : Please, try again.";
$lang['update_advance_profile_price_modifier_success']    = "Update Price Successful.";
$lang['update_advance_profile_price_modifier_fail']       = "Update Price Failed : Please, try again.";
$lang['update_advance_profile_categories_mapping_success']    = "Update Profile Mapping Successful.";
$lang['update_advance_profile_categories_mapping_fail']       = "Update Profile Mapping Failed : Please, try again.";
$lang['update_synchronization_success']             = "Get Inventory Successful.";
$lang['update_synchronization_fail']                = "Get Inventory Failed : Please, try again.";
$lang['update_synchronization_matching_success']    = "Update Synchronization Successful.";
$lang['update_synchronization_matching_fail']       = "Update Synchronization Failed : Please, try again.";
$lang['update_payment_method_success']              = "Update profile payment success.";
$lang['update_payment_method_fail']                 = "Update profile payment failed : Please, try again.";
$lang['update_return_method_success']               = "Update profile return success.";
$lang['update_return_method_fail']                  = "Update profile return failed : Please, try again.";
$lang['update_mapping_attributes_success']          = "Update profile attributes success.";
$lang['update_mapping_attributes_fail']             = "Update profile attributes failed : Please, try again.";
$lang['update_wizard_attributes_success']           = "Update profile attributes success.";
$lang['update_wizard_attributes_fail']              = "Update profile attributes failed : Please, try again.";
$lang['update_mapping_values_success']              = "Update attributes values success.";
$lang['update_mapping_values_fail']                 = "Update attributes values failed : Please, try again.";



$lang['validation_error_required']                  = "Please check your input.";
$lang['validation_required_form_user_id']           = "Please enter a username.";
$lang['validation_minlength_form_user_id']          = "Your username must consist of at least 2 characters.";
$lang['validation_required_form_postal_code']       = "Please enter your postal code.";
$lang['validation_regex_form_postal_code']          = "Please enter a valid {1} postcode{BR}Example : {2}";
$lang['validation_postal_valid_form_postal_code']   = "Please enter a valid postcode{BR}Example : {2}";
$lang['validation_select_required']                 = "Please select item.";
$lang['validation_range_required']                  = "Invalid price range, This price must be more than {1}.";
$lang['validation_positive_required']               = "Invalid price value, This price must be more than 0.";
$lang['validation_between_required']                = "Invalid price range, This price must not be between {1} and {2}.";
$lang['validation_range_weight_required']           = "Invalid weight range, This weight must be more than {1}.";
$lang['validation_between_weight_required']         = "Invalid weight range, This weight must not be between {1} and {2}.";
$lang['validation_carrier_rule_required']           = "Please assign carrier rule.";
$lang['validation_out_of_stock_min']                = "Please enter a number greater than or equal to 0 and less then or equal to 1000!";
$lang['validation_maximum_quantity_max']            = "Please enter a number greater than or equal to 1 and less then or equal to 1000!";
$lang['validation_variation_type']                  = "Duplicate variations type name, Please select another type name!";
$lang['validation_variation_include']               = "Invalid include variations type name, Please filter exclude variations type name!";
$lang['validation_specific_include']                = "Invalid include item specifics name, Please filter exclude item specifics name!";
$lang['validation_profile_name_checked']            = "Invalid profile name, Please fill another profile name!";



$lang['header_auth_security']           = "Integration eBay account";
$lang['title_auth_security']            = "Link to your eBay account";
$lang['header_integration']             = "Authentication eBay account";
$lang['title_integration']              = "eBay Authentication wizard";
$lang['header_integration_authorization']   = "Generate eBay Token";
$lang['title_integration_authorization']    = "eBay Authentication wizard";
$lang['header_integration_finish']      = "Authentication Successful";
$lang['title_integration_finish']       = "eBay Authentication wizard";
$lang['header_auth_setting']            = "Setting configuration";
$lang['title_auth_setting']             = "Product configuration";
$lang['header_tax']                     = "Tax setting";
$lang['title_tax']                      = "Tax configuration";
$lang['header_price_modifier']          = "Price modifier";
$lang['title_price_modifier']           = "Price rules";
$lang['header_univers']                 = "Universe";
$lang['title_univers']                  = "Selected main categories";
$lang['header_mapping_categories']      = "Mapping categories";
$lang['title_mapping_categories']       = "Choose categories";
$lang['header_mapping_templates']       = "Mapping templates";
$lang['title_mapping_templates']        = "Choose templates";
$lang['header_templates']               = "Manage templates";
$lang['title_templates']                = "Upload templates";
$lang['header_mapping_conditions']      = "Mapping conditions";
$lang['title_mapping_conditions']       = "Choose conditions";
$lang['header_wizard__conditions']      = "Mapping conditions";
$lang['title_wizard__conditions']       = "Choose conditions";
$lang['header_mapping_attributes']      = "Mapping Attributes";
$lang['title_mapping_attributes']       = "Choose Attributes";
$lang['header_wizard_attributes']       = "Mapping Attributes";
$lang['title_wizard_attributes']        = "Choose Attributes";
$lang['header_mapping_carriers']        = "Mapping carriers";
$lang['title_mapping_carriers']         = "Choose shipping";
$lang['header_mapping_carriers_cost']   = "Mapping carriers rules";
$lang['title_mapping_carriers_cost']    = "Choose shipping rules";
$lang['header_actions']                 = "Actions";
$lang['title_actions']                  = "Product actions";
$lang['header_actions_products']        = "Actions";
$lang['title_actions_products']         = "Product actions";
$lang['header_actions_offers']          = "Actions";
$lang['title_actions_offers']           = "Product actions";
$lang['header_actions_orders']          = "Actions";
$lang['title_actions_orders']           = "Product actions";
$lang['header_log']                     = "Batch Log";
$lang['title_log']                      = "Batch log";
$lang['header_statistics']              = "Batch Log Details";
$lang['title_statistics']               = "Batch log details";
$lang['header_orders']                  = "Orders";
$lang['title_orders']                   = "Orders report";
$lang['header_wizard_choose']           = "Select eBay country";
$lang['title_wizard_choose']            = "Select eBay country";
$lang['header_wizard_univers']          = "Category";
											
$lang['title_wizard_univers']           = "Selected main categories";
$lang['header_wizard_categories']       = "Category";
$lang['title_error_resolutions']           = "Error Resolutions";
$lang['header_error_resolutions']       = "Error Resolutions";
											
$lang['title_wizard_categories']        = "Selected main categories";
$lang['header_wizard_tax']              = "Tax setting";
$lang['title_wizard_tax']               = "Tax configuration";
$lang['header_wizard_authorization']    = "eBay authentication & security";
$lang['title_wizard_authorization']     = "Link to your eBay account";
$lang['header_wizard_shippings']        = "Mapping carriers";
$lang['title_wizard_shippings']         = "Choose shipping";
$lang['header_wizard_finish']           = "Wizard export products statistics";
											
$lang['title_wizard_finish']            = "Wizard export products statistics";
											
$lang['header_wizard_exports']          = "Actions";
$lang['title_wizard_exports']           = "Wizard export products";
$lang['header_advance_categories']      = "Advanced Setting Profile";
$lang['title_advance_categories']       = "Choose eBay categories";
$lang['header_advance_categories_main'] = "Advanced Manager Profile";
$lang['title_advance_categories_main']  = "Manage Categories Profiles";
$lang['header_advance_main']            = "Advanced Profile Setting";
$lang['title_advance_main']             = "Profile Configuration";
$lang['header_advance_profile_configuration']           = "Advanced Profile Setting";
$lang['title_advance_profile_configuration']            = "Profile Configuration Details";
$lang['header_advance_profile_payment'] = "Advanced Profile Setting";
$lang['title_advance_profile_payment']  = "Profile Payment";
$lang['header_advance_profile_return']  = "Advanced Profile Setting";
$lang['title_advance_profile_return']   = "Profile Return";
$lang['header_advance_profile_description']  = "Advanced Profile Setting";
$lang['title_advance_profile_description']   = "Profile Description";
$lang['header_advance_profile_condition']  = "Advanced Profile Setting";
$lang['title_advance_profile_condition']   = "Profile Condition";
$lang['header_advance_profile_shipping']  = "Advanced Profile Shipping";
$lang['title_advance_profile_shipping'] = "Profile Shipping";
$lang['header_advance_profile_variation']   = "Advanced Profile Variation";
$lang['title_advance_profile_variation']    = "Profile Variation";
$lang['header_advance_profile_specific']    = "Advanced Profile Item Specific";
$lang['title_advance_profile_specific'] = "Profile Item Specific";
$lang['header_advance_ebay_categories']    = "Advanced eBay Categories";
$lang['title_advance_ebay_categories'] = "Choose eBay Categories";
$lang['header_advance_profile_price_modifier']    = "Advanced Price Rules";
$lang['title_advance_profile_price_modifier'] = "Price Rules";
$lang['header_advance_profile_categories_mapping']    = "Advanced Profile Mapping";
$lang['title_advance_profile_categories_mapping'] = "Profile Mapping";
$lang['header_synchronization']         = "Synchronization";
$lang['title_synchronization']          = "Synchronization Wizard";
$lang['header_synchronization_matching']    = "Synchronization Wizard";
$lang['title_synchronization_matching']     = "Synchronization and Matching";
$lang['header_synchronization_finish']    = "Synchronization Wizard";
$lang['title_synchronization_finish']     = "Synchronization Finish";
$lang['header_payment_method']          = "Payment method";
$lang['title_payment_method']           = "Payment method";
$lang['header_return_method']           = "Return method";
$lang['title_return_method']            = "Return method";
$lang['header_attribute_univers']       = "Attribute universe";
$lang['title_attribute_univers']        = "Attribute universe";
$lang['header_variation']          		= "Variation";
$lang['title_variation']           		= "Variation";
$lang['header_variation_values']        = "Variation values";
$lang['title_variation_values']         = "Variation values";
$lang['header_authentication']          = "Authentication";
$lang['title_authentication']           = "Authentication";



$lang['integration']                    = "Authentication";
$lang['authentication']                 = "Authentication";
$lang['auth_security']                  = "Auth & security";
$lang['auth_setting']                   = "Setting configuration";
$lang['tax']                            = "Tax setting";
$lang['price_modifier']                 = "Price modifier";
$lang['univers']                        = "Selected categories";
$lang['mapping_categories']             = "Categories";
$lang['mapping_templates']              = "Templates";
$lang['mapping_conditions']             = "Selected condition";
$lang['mapping_carriers']               = "Carriers";
$lang['mapping_carriers_cost']          = "Carriers rules";
$lang['actions']                        = "Actions";
$lang['actions_products']               = "Actions";
$lang['actions_offers']                 = "Actions";
$lang['actions_orders']                 = "Actions";
$lang['log']                            = "Log";
$lang['total']                          = "Total";
$lang['solved']                          = "Solved";
$lang['advance_main']                   = "Advanced Manager Rule";
$lang['advance_categories_main']        = "Advanced Manager Profile";
$lang['advance_categories_main_manager']    = "All Profile";
$lang['advance_profile_configuration']  = "Configuration";
$lang['advance_profile_payment']        = "Payment";
$lang['advance_profile_return']         = "Return";
$lang['advance_profile_description']    = "Description";
$lang['advance_profile_condition']      = "Condition";
$lang['advance_profile_shipping']       = "Shipping";
$lang['advance_profile_variation']      = "Variation";
$lang['advance_profile_specific']       = "Item Specific";
$lang['advance_profile_price_modifier'] = "Price Modifier";
$lang['advance_ebay_categories']        = "Categories";
$lang['advance_profile_categories_mapping'] = "Profile Mapping";
$lang['synchronization_matching_mapping'] = "Synchronization Mapping";
$lang['synchronization']                = "Synchronization";
$lang['synchronization_start']          = "Start";
$lang['synchronization_detail']         = "Detail";
$lang['synchronization_matching']       = "Matching";
$lang['matching_product']               = "Matching Products";
$lang['synchronization_finish']         = "Finish";
$lang['integration_start_wizard']       = "Start";
$lang['integration_authorization_wizard']   = "Authorization";
$lang['integration_synchronize_wizard']   = "synchronization";
$lang['integration_finish']             = "Finish";
$lang['return_method']                  = "Return policy";
$lang['attribute_univers']              = "Universe";
$lang['mapping_values']                 = "Attribute value";
$lang['product_sku']                    = "Product SKU";
$lang['varian_sku']                     = "Varian SKU";
$lang['cause']                          = "Cause";
$lang['solution']                       = "Solution";
$lang['help']                           = "Help";



$lang['synchronization_wizard']         = "eBay synchronization wizard";
$lang['get_synchronization_wizard']     = "Would you like to get eBay inventory?";
$lang['get_integration_wizard']         = "Would you like to integration eBay account?";
$lang['get_generate_wizard']            = "Why you must to generate a token with eBay account?";
$lang['primary_categories']             = "Primary categories";
$lang['secondary_categories']           = "Secondary categories";
$lang['primary_store_categories']       = "Primary store categories";
$lang['secondary_store_categories']     = "Secondary store categories";
$lang['id_ebay_categories_only']        = "eBay Categories ID";


$lang['payment_location']               = "Items location";
$lang['returns_policy']                 = "Returns Policy";
$lang['returns_within']                 = "Returns Within";
$lang['who_pays']                       = "Who pays";
$lang['information:']                   = "Information";
$lang['listing_duration']               = "Listing duration";
$lang['selected_tax']                   = "Selected tax";
$lang['rounding']                       = "Rounding";
$lang['shipping_rules']                 = "Shipping rules";
$lang['one_digit']                      = "One digit";
$lang['two_digit']                      = "Two digit";
$lang['none']                           = "None";
$lang['weight']                         = "Weight";
$lang['price']                          = "Price";
$lang['quantity']                       = "Quantity";
$lang['item']                           = "Item";

$lang['shipping_time']                  = "Carrier & shipping time";
$lang['mapping_domestic_shipping']      = "Mapping domestic shipping";
$lang['mapping_international_shipping'] = "Mapping international shipping";
$lang['choose_a_carrier']              = "Choose a carrier";
$lang['country:']                       = "Country:";


$lang['rules_surcharge']                = "Set shipping rules to apply to the total cost of items purchased";
$lang['rules_weight']                   = "Set shipping weight rules";
$lang['rules_price']                    = "Set shipping price rules";
$lang['rules_item']                     = "Set shipping item rules";
$lang['default_price_rule']             = "Price modifier";
$lang['adjustment_price']               = "Price adjustment rules";
$lang['default_tax_term']               = "Default taxes";
$lang['default_price_type']             = "Type terms";
$lang['default_min_price_rule']         = "Minimum price";
$lang['default_max_price_rule']         = "Maximum price";
$lang['dolla']                          = "$";
$lang['weight_kg']                      = "Weight (Kg)";
$lang['price_min']                      = "Price (Min)";
$lang['price_max']                      = "Price (Max)";
$lang['cost']                           = "Cost";
$lang['additional']                     = "Additional";
$lang['add_more']                       = "Add more";
$lang['remove_more']                    = "Remove";
$lang['go_to']                          = "Link to";
$lang['manager']                        = "manager";
//Log//
$lang['batch_id']                       = "Batch ID";
$lang['source']                         = "Source";
$lang['type']                           = "Type";
$lang['error']                          = "Error";
$lang['export_date']                    = "Export date";
$lang['product']                        = "Product";
$lang['offer']                          = "Offer";
$lang['all']                            = "All";
$lang['all:']                           = "All :";
$lang['ebay_not_found']                 = "File or directory not found.";
$lang['search_not_found']               = "Not found";
$lang['search']                         = "Search";
$lang['setting_profile']                = "Choose profile rules";
$lang['find_categories']                = "eBay category finder";
$lang['list_categories']                = "eBay category lists";
$lang['categories_list']                = "Category list";
$lang['create_your_listing']            = "Profile listing";
$lang['create_your_payment']            = "Profile payment";
$lang['create_your_return']             = "Profile return";
$lang['create_your_description']        = "Profile description";
$lang['create_your_condition']          = "Profile condition";
$lang['create_your_variation']          = "Profile variation";
$lang['create_your_shipping']           = "Profile shipping";
$lang['create_your_specific']           = "Profile item specific";
$lang['create_your_categories']         = "Profile categories";
$lang['create_your_finish']             = "Congratulations, You have successfully advance configuration.";
$lang['synchronize_finish']             = "Congratulations, You have successfully Synchronization.";
$lang['integration_finish_success']     = "Congratulations, You have successfully Integration eBay acount.";
$lang['out_of_stock']                   = "Out of stock";
$lang['maximum_quantity']               = "Maximum quantity";
$lang['identifier_with_items']          = "Identifier with items";
$lang['stock_keeping_unit:']            = "Stock keeping unit :";
$lang['stock_keeping_unit']             = "SKU";
$lang['varians']                        = "Variations";
$lang['sku']                            = "SKU";
$lang['ean13']                          = "EAN13";
$lang['upc']                            = "UPC";
$lang['reference_id']                   = "Reference ID";
$lang['item_id_only']                   = "Item ID";
$lang['title_format']                   = "Title format";
$lang['description_format']             = "Description format";
$lang['standard_title']                 = "Standard title (Recommended)";
$lang['manufacturer_title']             = "Manufacturer, title";
$lang['manufacturer_title_reference']   = "Manufacturer, title (Reference)";
$lang['auto_pay']                       = "Auto pay";
$lang['open_only']                      = "Open";
$lang['close_only']                     = "Close";
$lang['currency_only']                  = "Currency";
$lang['visitor_counter']                = "Visitor counter";
$lang['basic_style']                    = "Basic style";
$lang['retro_computer_style']           = "Retro-computer style";
$lang['hidden_only']                    = "Hidden";
$lang['suggestions_only']               = "Suggestions";
$lang['gallery_plus']                   = "Gallery plus";
$lang['payment_setting']                = "Specifying payment methods";
$lang['returns_setting']                = "Specifying returns policy details";
$lang['description_setting']            = "Specifying description";
$lang['condition_description']          = "Condition description";
$lang['category_setting']               = "Choose eBay categories";
$lang['condition_setting']              = "Specifying condition";
$lang['variation_setting']              = "Specifying variation type";
$lang['variation_setting_value']        = "Specifying variation values";
$lang['specific_setting']               = "Specifying item specific name";
$lang['specific_setting_value']         = "Specifying item specific value";
$lang['condition_details']              = "Condition details";
$lang['condition_id']                   = "Typical name";
$lang['variation_value']                = "Typical value";
$lang['listing_type']                   = "Listing type";
$lang['remaining']                      = "Remaining";
$lang['return_details']                 = "Return descriptions details";
$lang['html_descriptions']              = "HTML descriptions";
$lang['short_descriptions']             = "Short description";
$lang['long_descriptions']              = "Standard description";
$lang['selected_templates']             = "Selected templates";
$lang['holiday_return']                 = "Extended holiday return";
$lang['learn_more']                     = "Learn more";
$lang['use_matchings']                  = "Matching options";
$lang['use_specifics']                  = "Fixed options";
$lang['profile_category_config']        = "Category configuration";
$lang['profile_listing_config']         = "Listing configuration";
$lang['exclude_variation']              = "Excluded variation Type Name";
$lang['include_variation']              = "Included variation Type Name";
$lang['exclude_specific']               = "Excluded item Specific";
$lang['include_specific']               = "Included item specific";
$lang['mapping_profile']                = "Mapping profile";
$lang['information_inventory']          = "Information on an existing product in your inventory";
$lang['your_store']                     = "Your inventory";
$lang['database_store']                 = "Database inventory";
$lang['ebay_store']                     = "eBay inventory";
$lang['unknown_product']                = "Unknown product";
$lang['synchronize_product']            = "Synchronize product";
$lang['product_s']                      = "Product(s)";
$lang['synchronize_title']              = "You have existing";
$lang['synchronize_matching']           = "Matching products";
$lang['synchronize_and_matching']       = "Synchronization and matching";
$lang['reference_type']                 = "Reference type";
$lang['choose_reference']               = "Choose a reference";
$lang['auto_matching']                  = "Select matching type";
$lang['insert_key']                     = "Insert key";
$lang['reference_key']                  = "Reference key";
$lang['synchronize_name']               = "Unsynchronize name";
$lang['custom_synchronize']             = "Custom synchronize";

//Default Templates/
$lang['default_template']              = "Default template mapping";
$lang['default_templates']              = "Default template mapping";
$lang['default_profile']                = "Default profiles";
$lang['categories_profile']             = "Categories profiles";
$lang['products_profile']               = "Products profiles";
$lang['categories_default_profile']     = "Both (categories profile and default profile)";
$lang['products_default_profile']       = "Both (products profile and default profile)";
$lang['all_default_profile']            = "All (products profile, categories profile and default profile)";





$lang['noaction_noimage']               = "Can export products to eBay account even if there is no image for the product."; 
$lang['calculate_discount_price']       = "Calculate discount price from Feed.biz if there is a discount price in Prestashop (your shopping site)."; 
$lang['synchronize_with_products']      = "Synchronize products in eBay account, prior to exporting products to eBay."; 
$lang['import_orders']                  = "Can import orders from eBay account, even if there are no products to export from Feed.biz to eBay account."; 
$lang['profile_description']            = "This will send all the products with a profile within the selected categories in your configuration."; 
$lang['defalut_profile_help']           = "This will use only defaults profile are primary.";// If this was selected all products will having default configuration same eBay Menu Tab » Setting and can export to eBay account."; 
$lang['categories_profile_help']        = "This will use only categories profile are primary.";//  If this was selected will filter products configuration within the categories profile. This will can export to eBay account but products without profile will can't export to eBay account."; 
$lang['products_profile_help']          = "This will use only products profile are primary.";//  If this was selected will filter products configuration within the products profile. This will can export to eBay account but products without profile will can't export to eBay account."; 
$lang['both_categories_profile_help']   = "This will use only categories profile are primary and without are default.";//  If this was selected will filter products configuration within the categories profile and products without profile will having default configuration same eBay Menu Tab » Setting and can export to eBay account."; 
$lang['both_products_profile_help']     = "This will use only products profile are primary and without are default.";//  If this was selected will filter products configuration within the products profile and products without profile will having default configuration same eBay Menu Tab » Setting and can export to eBay account."; 
$lang['all_profile_help']               = "This will use only products profile are primary, categories profile are secondary and without are default.";//  If this was selected will filter products configuration within the products profile after that another products will filter products configuration within the categories profile and products without profile will having default configuration same eBay Menu Tab » Setting and can export to eBay account."; 
$lang['categories_search_help']         = "Enter an ID or category name to jump straight to a category.";
$lang['sku_help']                       = "Using a SKU Or Item ID Identifier with Items.";
$lang['title_format_help']              = "Type product name. Please select Standard Title if your products are already formatted for eBay.";//"Type product name, eBay recommend to format the title like Standard Title. Please choose the first choice if your products titles are already formated for eBay.";
$lang['description_format_help']        = "Description field to send to eBay Account. (Export HTML Descriptions instead of Text Only)";
$lang['auto_pay_help']                  = "To enforce that a buyer pays immediately (through Paypal) for an auction (Buy it now only) or for a fixed priced item.";//"To create a requirement that a buyer pays immediately (through PayPal) for an auction (Buy It Now only) or fixed-price item.";//, the seller can include and set the Auto Pay field to 'Open'. If the seller does not want to create an immediate payment item, this field is either omitted, or included and set to 'Close'. ";
$lang['country_name_help']              = "A two-letter code that represents the country in which the item is located.";
$lang['currency_help']                  = "Currency associated with the item's price information. 3-letter currency code that corresponds to the site specified in the listing request.";
$lang['visitor_counter_help']           = "eBay counters track the number of buyers who have viewed your listing. Multiple views from the same user are counted only once. The counter appears at the bottom of your listing. <a href='http://developer.ebay.com/devzone/xml/docs/reference/ebay/types/HitCounterCodeType.html' target='_blank'>Read More.</a>";
$lang['out_of_stock_help']              = "Minimum quantity required to be in stock, for exporting the product to eBay";
$lang['maximum_quantity_help']          = "Maximum quantity of the product that can be exported to eBay.";
$lang['primary_categories_help']        = "This is the main category where Buyers will find your item on eBay. (not in your store, but on general eBay shopping)";
$lang['second_category_warning_help']   = "Use a second category (additional eBay fees apply)";
$lang['second_category_help']           = "This field is optional. If you add a secondary category to a listing, you may later change it to a different one, but you will not be able to remove the use of a secondary category entirely.";
$lang['store_category_warning_help']    = "A custom category is a category that the seller created in your eBay Store.";
$lang['store_category_help']            = "if you have imported Store Categories, you will also be able to choose eBay Store categories when doing your Product Mapping. Store Categories are optional and will only apply if you have an eBay Store and have imported them for use in Feed.biz.";
$lang['store_secondary_category_help']  = "Store Categories are optional and will only apply if you have an eBay Store and you may choose a seconddary store category from your eBay Store category structure as well, if desired.";
$lang['gallery_plus_warning_help']      = "Use a gallery plus (additional eBay fees apply)";
$lang['gallery_plus_help']              = "Display a large picture in search results, capture special details or provide different views for buyers.";//"Display a large picture in search results-capture special details or different views for buyers.";
$lang['listing_duration_help']          = "Number of days for which listing is desired by seller.";
$lang['listing_type_help']              = "Specifies the selling format used for a listing.";
$lang['payment_help']                   = "Only for sellers accepting credit card purchases through their own merchant account.";
$lang['payment_instructions_help']      = "Give clear instructions to assist your buyer with payment and shipping.";
$lang['payment_instructions_max_help']  = "Max. Length: 500 Characters";
$lang['return_instructions_max_help']   = "Max. Length: 1000 Characters";
$lang['condition_description_max_help'] = "Max. Length: 1000 Characters";
$lang['return_specify_help']            = "Indicates whether the seller allows the buyer to return the item. ";//Require that all sellers specify whether they accept returns or not. Indicates whether the seller allows the buyer to return the item. This field is required when Return Policy is specified.";
$lang['return_within_help']             = "The buyer can return the item within this period of time from the day they receive the item. ";//After receiving the item, your buyer should start a return within. The buyer can return the item within this period of time from the day they receive the item. ";
$lang['return_pays_help']               = "The party who pays the shipping cost for a returned item. ";//Return shipping will be paid by. The party who pays the shipping cost for a returned item.";
$lang['return_information_help']        = "A detailed explanation of seller’s Returns & Extended Returns Policy. Ensure that the description is consistent with the parameters defined.";
$lang['holiday_return_label']            = "Offering an extra generous return policy during the holidays.";
$lang['holiday_return_help']            = "Offering an extra generous return policy during the holidays is a great way to capture buyer interest and close the sale.";
$lang['condition_description_help']     = 'Note: The condition description field will be ignored for "new" conditions. ("New", "New in Box", etc.)';
$lang['condition_new_help']             = 'Note: The condition description field A new, unused item with absolutely no signs of wear. The item may be missing the original packaging, or in the original packaging but not sealed. The item may be a factory second or a new, unused item with defects. ("New", "New in Box", "New with box", "New with tags", etc.)';
$lang['condition_used_help']            = 'Note: The condition description field An item that has been used previously. ("Used", etc.)';//The item may have some signs of cosmetic wear, but is fully operational and functions as intended. This item may be a floor model or store return that has been used. See the seller’s listing for full details and description of any imperfections. ("Used", etc.)';
$lang['condition_refurbished_help']     = 'Note: This means the item has been inspected, cleaned, and repaired to full working order and is in excellent condition. This item may or may not be in original packaging. ("Manufacturer refurbished", "Seller refurbished", etc.)';//The condition description field An item that has been restored to working order by the eBay seller or a third party not approved by the manufacturer or that has been professionally restored to working order by a manufacturer or manufacturer-approved vendor. This means the item has been inspected, cleaned, and repaired to full working order and is in excellent condition. This item may or may not be in original packaging. ("Manufacturer refurbished", "Seller refurbished", etc.)';
$lang['condition_good_help']            = 'Note: The condition description field An item that has been good condition. Very minimal damage to the product. ("Good", "Very Good", etc.)';
$lang['condition_acceptable_help']      = 'Note: The condition description field An item that has been used previously.  May have some damage to the product but integrity still intact. ("Acceptable", etc.)';
$lang['condition_help']                 = "Describe the condition of your product.";
$lang['variation_help']                 = 'Multi-variation listings contain multiple items that are essentially the same product, but that vary in their manufacturing details or packaging. (For example, a particular brand and style of shirt could be available in different sizes and colors, such as "large blue" and "medium black".)  Each variation specifies a combination of one of these colors and sizes. Each variation can have a different quantity and price. You can buy multiple items from one variation at the same time. (That is, one order line item can contain multiple items from a single variation.) ';
$lang['specific_help']                  = 'A list of Item Specific name and value pairs that the seller specified for the item. Item Specifics describe well-known aspects of an item or product in a standard way, to help buyers find that type of item or product more easily. or example, "Publication Year" is a typical aspect of books, and "Megapixels" is a typical aspect of digital cameras. ';
$lang['variation_value_help']           = 'A value that is associated with variation type name. For example, suppose this set of pictures is showing blue shirts, and some of the variations include Color = Blue in their variation specifics. If variation specific type name is "Color", then variation type value would be "Blue".';
$lang['variation_prepare_value_help']   = 'A value that is associated with variation type name. For example, suppose this set of pictures is showing {A} objects, and some of the variations include {A} = {B} in their variation specifics. If variation specific type name is "{A}", then variation type value would be "{B}".';
$lang['specific_prepare_value_help']    = 'A value that is associated with item specific name. For example, suppose this set of pictures is showing {A} objects, and some of the item specific include {A} = {B} in their item specifics. If item specific type name is "{A}", then item specific value would be "{B}".';
$lang['postcode_help']                  = "Postal code of the place where the item is located. This value is used for proximity searches.";
$lang['dispatch_time_help']             = "Maximum number of business days required by seller to keep an item ready for shipping.";//"Specifies the maximum number of business days the seller commits to for preparing an item to be shipped after receiving a cleared payment. This time does not include the shipping time (the carrier's transit time).";
$lang['shipping_rules_help']            = "Specifies basis for shipping price per Item of item shipped.";
$lang['country_shipping_help']          = "Specifies international regions you will ship to.";
$lang['domestic_shipping_help']         = "Specifies your shipping that you will ship to. The list of first items in selected shipping must specify at least one domestic shipping service.";
$lang['international_shipping_help']    = "Specifies the shipping that you will ship to.";
$lang['ebay_shipping_help']             = "Specifies eBay domestic shipping that you will ship to. The list first items in selected shipping must specify at least one domestic shipping service.";
$lang['ebay_international_shipping_help'] = "Specify eBay international shipping that you will ship to.";
$lang['first_shipping_help']            = "The list first items in selected shipping must specify at least one domestic shipping service.";
$lang['additional_help']                = "Specifies shipping price per Item for an additional Item.";//"The additional cost for each identical item.";//The additional cost to ship each additional identical item to the buyer.";
$lang['cost_help']                      = "Specifies basis for shipping price per item shipped.";//The shipping service cost to ship one item to the buyer.";
$lang['price_min_help']                 = "Price range, from.";//The min price shipping when calculated shipping for one item to the buyer.";
$lang['price_max_help']                 = "Price range, to.";//The max price shipping when calculated shipping for one item to the buyer.";
$lang['weight_min_help']                = "Weight range, from.";
$lang['weight_max_help']                = "Weight range, to.";
$lang['html_descriptions_help']         = "if you want to use HTML or XML-reserved characters in the description.";//This field is used by the seller to provide a html description of the item. In listing requests, you can submit your description. if you want to use HTML or XML-reserved characters in the description.";
$lang['short_descriptions_help']        = "This field is used by the seller to provide a short description of the item.";// This field is required for all listings except when creating a listing using an eBay catalog product.";
$lang['long_descriptions_help']         = "This field is used by the seller to provide a standard description of the item.";// This field is required for all listings except when creating a listing using an eBay catalog product.";
$lang['selected_templates_help']        = "One template can be specified, each in product own container.";//The template whose data will be show. A Seller can select contains the data needed to list an item. One template can be specified, each in product own container.";
$lang['payment_help']                   = "Identifies the payment method that the seller will accept when the buyer pays for the item.";
$lang['tax_term_help']                  = "You should configure a taxes rule in value or percentage for one or no taxes. Calculate is percentage of a value before and after, or Calculate is price value of a increase or decrease. The default taxes will be calculated the increase or decrease by price of item have price between price ranges will be calculated to the buyer.";
//$lang['price_modifier_rules_help']      = "You should configure a price rule in value or in percentage for one or several prices ranges. Calculate is percentage of a value before and after, or Calculate is price value of a increase or decrease. The default price rules will be calculated the increase or decrease by price modifier and one item have price between price ranges will be calculated to the buyer.";
$lang['price_modifier_rules_help']      = "This section allows upward revision of selling prices within a price range, by percentage or absolute amount; on eBay marketplace.";//"This section allows to adjust selling prices on eBay marketplace, in percentage, value, by price ranges.";
//$lang['price_modifier_min_help']        = "The minimum price rules when calculated price modifier for one item to the buyer.";
//$lang['price_modifier_max_help']        = "The maximum price rules when calculated price modifier for one item to the buyer.";
//$lang['price_modifier_value_help']      = "The default price rules will be calculated the increase or decrease by price modifier in {A} terms.";
$lang['price_modifier_type_help']       = "Calculate is percentage of a value before and after, or Calculate is price value of a increase or decrease.";
$lang['price_modifier_min_help']        = "Price range, from.";
$lang['price_modifier_max_help']        = "Price range, to.";
$lang['price_modifier_value_help']      = "Price Modifier.";
$lang['price_modifier_rounding_help']   = "Rounding mode to be applied to the price. (example: price will be equal to 10.17 or 10.20)";
$lang['synchronization_start_help']      = "This wizard will download your inventory from eBay Account. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on eBay. The goal is to create your offers on eBay. When you click get started, you will get your inventory from eBay. Let do it.";
$lang['synchronization_detail_select_help']     = "Specifies the reference type of products the seller commits to for preparing an item to mapping after receiving a synchronize inventory from ebay account.";
$lang['synchronization_detail_key_help']     = "Specifies the reference string of products from synchronize inventory to generate string from all product that matching string. [ProductID] as eBay_[ProductID]";
$lang['synchronization_detail_reference_help']     = "Specifies the reference string of products in database to generate string from all product that matching string. [ProductID] as Feedbiz_[ProductID]";
$lang['synchronization_matching_help']   = "";
$lang['synchronization_finish_help']     = "Ebay product template accepts HTML, HTM, TXT, PHP, TPL file types.";//"The Seller commits to for preparing templates. Upload and Specifies the default templates of products. ";
$lang['advance_profile_finish_help']     = "When you preparing profile finishing. Click to mapping profile to mapping profile against your categories in inventory.";

$lang['your_store_help']                = "The list of items from your import feed items or products from your store or wholesaler.";
$lang['ebay_store_help']                = "The list of items the user is actively selling on eBay account.";
$lang['unknown_product_help']           = "The list of items are not mapping synchronize or the items from eBay that the user cannot mapping with your item.";
$lang['synchronize_product_help']       = "The list of items was mapping synchronize or the item was export from feedbiz.";
$lang['update_offer_product_help']      = "This will transfer Inventory from eBay and revise quantity, price, discount of listed products.";//"This will synchronize inventory moves in eBay account, within the selected categories in your configuration and revise quantity and prices of listing.";
$lang['export_product_help']            = "Upon completing mapping & module configuration, you can export products to eBay by clicking on Export Products.";//"If you complete your mapping and module configuration. You can send all products to eBay account. If you want to export products click export products this will send all the products having a configuration and within the selected categories in your module configuration to eBay account.";
$lang['admin_compress_product_help']            = "Administrator can use this tool for testing compress feed file without sending product to Ebay.";
$lang['import_order_help']              = "Imports orders from eBay, offering an Order Summary and a link to order in Feed.biz";//"If you want to get order from eBay account click import order this will import orders from eBay account. For every order you import there will be an order summary and a link to the order in your modules.";
$lang['confirm_order_help']             = "If you want to export order tracking number to eBay account click confirm order shipment this will confirm tracking number to eBay account. For every order you export it will be specified in order summary.";
$lang['order_status_help']              = "Status used along with a date range to select orders from eBay for download to Feed.biz";//"The field is used to retrieve orders that are in a specific state. If this field is used with a date filter, only orders that satisfy both the date range and the order status value are retrieved. Enumerated type that defines the possible values for order states.";
$lang['delete_product_help']            = "The list of items in eBay account will be deleted to unsold list. If you click, this will delete all products within the selected categories in your configuration.";
$lang['delete_out_of_stock_help']       = "The list of items out of stock in eBay account will be deleted to unsold list. This will delete only out of stock products on eBay.";
$lang['delete_all_in_stock_help']       = "The list of items that were exported from feedbiz will be deleted to unsold list. This will delete products within the selected categories in your configuration.";
$lang['delete_full_in_stock_help']      = "This will delete products in your eBay account.";
$lang['templates_default_help']         = "Download Default Template, useful to create new templates. Information, help & support about Templates available online at http://documentation.feed.biz/version-1.0/manual/ebay";//"This will present the variable of templates to used for feedbiz export products into eBay accounts. Layout templates determine the relative positioning of the text and images within the rectangular area in which an item's description is displayed. For example, one templates specifies that HTML code which the item description is displayed in eBay description).";
$lang['templates_custom_help']          = "The list of templates were uploaded from feedbiz, Each of these fields can be downloaded, deleted or previewed.";
$lang['integration_ebay_help']          = "This wizard will link with eBay Account. Securely link your eBay account to Feedbiz by signing in to eBay.  This will activate your inventory, orders, feedback, and your store, and allow you to import, post listings, and manage eBay orders on FeedBiz without sharing your eBay password. Fulfill orders quickly with streamlined order processing. When you click get start will get begin link FeedBiz against your eBay Account. Let do it.";
$lang['integration_ebay_authorization_help']    = "<ul><li>You must obtain an eBay token to link your Feed.biz and eBay Accounts.</li><li>When you click on \"AUTHORIZATION\" button, the Feed.biz module will sign into your eBay account. If the sign in is successful, system will generate a token to your eBay account.</li><li>Once you generate a token, you will be able to use all of the tools in Feed.biz Seller Module.  Please <span style=\"color:red;\">close</span> pop up window after a successful authentication to eBay Account.</li></ul>";
$lang['integration_ebay_authorization_help_close_popup']    = "&nbsp;";
$lang['integration_ebay_finish_help']    = "You have successfully generate eBay token. You can click to synchronize inventory from your eBay account to matching against product within FeedBiz account or if you nothing any products click setting for configuration to Feedbiz Account. Let do it.";
$lang['integration_ebay_token_help']    = "A token is a security feature that allows you to perform tasks on eBay directly from FeedBiz.";

$lang['integration_ebay_authorization_help_01']    = "You must obtain an eBay token to link your Feed.biz and eBay Accounts.  When you click on \"AUTHORIZATION\" button, the Feed.biz module will sign into your eBay account. If the sign in is successful, system will generate a token to your eBay account";
$lang['integration_ebay_authorization_help_02']    = "Once you generate a token, you will be able to use all of the tools in Feed.biz Seller Module. Please <span style=\"color:red;\">close</span> pop up window after a successful authentication to eBay Account.";

$lang['info_tax']                               = "Informations, help and support about {B}Taxes{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-taxes/?lang=en";//Formula to be applied on all exported product prices (multiplication, division, addition, subtraction, percentages).{BR}Apply a specific price formula for eBay selected categories which will override the main setting.{BR}Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
$lang['info_wizard_tax']                        = "Informations, help and support about {B}Taxes{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-taxes/?lang=en";//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}Formula to be applied on all exported product prices (multiplication, division, addition, subtraction, percentages).{BR}Apply a specific price formula for eBay selected categories which will override the main setting.{BR}Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
//$lang['info_actions']                           = "This will send all the products having a profile and within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go on the mapping and setting configuration in eBay options tab.{BR}After that, please do not forget to generate and save a report (For any support you will need the XML and the Report)";
$lang['info_advance_profile_price_modifier']    = "1. You could configure a price rule in value or percentage for one or several prices ranges.{BR}2. Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).{BR}3. Apply a specific price formula for eBay selected categories which will override the main setting.{BR}4. Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
$lang['info_advance_profile_price_modifier']    = "Informations, help and support about {B}Price rules{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-price-rules/?lang=en";
$lang['info_advance_main']                      = "First time all products will have same default configuration in eBay Menu Tab » Setting and can export to eBay account.";
$lang['info_wizard_exports']            = "This will send all products with a profile within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go to the mapping and setting configuration in the eBay options tab.{BR} Then, do not forget to generate and save a report (For support you will need the XML and the Report)";
$lang['info_mapping_categories']        = "Informations, help and support about {B}Categories{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-categories/?lang=en";//You can select multiple categories at once by checking the first category then pressing {B}'Shift'{/B} and clicking on the last element you wish to include in the same profile.{BR}1. Primary eBay Category - This is the main category where Buyers will find your item on eBay (not in your store, but on general eBay shopping). {BR}2. Secondary eBay Category (optional) - This field is optional. If you add a secondary category to a listing, you may later change it to a different one, but you will not be able to remove the use of a secondary category entirely. {BR}3. Primary eBay Store Category (optional) - if you have imported Store Categories, you will also be able to choose eBay Store categories when doing your Product Mapping. Store Categories are optional and will only apply if you have an eBay Store and have imported them for use in Feed.biz. {BR}4. Secondary eBay Store Category (optional) - you may choose a second category from your eBay Store category structure as well, if desired.";
$lang['info_mapping_templates']         = "You can select multiple categories at once by checking the first category then pressing {B}'Shift'{/B} and clicking on the last element you wish to include in the same profile.";
$lang['info_wizard_categories']         = "Informations, help and support about {B}Categories{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-categories/?lang=en";//You can select multiple categories at once by checking the first category then pressing {B}'Shift'{/B} and clicking on the last element you wish to include in the same profile.";
$lang['info_ebay_wizard']               = "If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export products to your ebay account.";
$lang['info_congratulation']            = "Congratulation, your products have been sent to eBay successfully, you now can publish your offer in the action tab.";
//$lang['info_price_modifier']            = "1. You could configure a price rule in values or percentages for one or several price ranges.{BR}2. Formula to be applied on all the exported product prices (multiplication, division, addition, subtraction, percentages).{BR}3. Apply a specific price formula for eBay selected categories which will override the main setting.{BR}4. Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required";
$lang['info_price_modifier']            = "Informations, help and support about {B}Price rules{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-price-rules/?lang=en";
$lang['info_wizard_choose']             = "If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}This list of country from eBay account that that can to export products now. You can choose 1 country that would like to export products.";
$lang['info_wizard_univers']            = "If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}Make sure you have “1 category” selected from the checkbox in the below of the page. This list from the “Root category from eBay site”, choose the root category you would like to export.";
$lang['info_univers']                   = "This list displays Root Category of eBay - ensure that you have selected at least one root.";
$lang['info_attribute_univers']         = "Make sure you have “1 attribute” selected from the checkbox in the bottom of the page.";
$lang['info_wizard_payments']           = "Informations, help and support about {B}Payments{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-payment/?lang=en";//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}When choosing a payment method, look for options that are convenient, secure, and traceable in case of problems. Use the list below to compare payment methods.";
$lang['info_payment_method']            = "Informations, help and support about {B}Payments{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-payment/?lang=en";//When choosing a payment method, look for options that are convenient, secure, and traceable in case of problems. Use the list below to compare payment methods.";
$lang['info_advance_profile_payment']   = "Informations, help and support about {B}Payments{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-payment/?lang=en";//When choosing a payment method, look for options that are convenient, secure, and traceable in case of problems. Use the list below to compare payment methods.";
$lang['info_wizard_returns']            = 'Informations, help and support about {B}Returns{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-return/?lang=en';//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}Buyers are more comfortable shopping with sellers who accept returns, even though most buyers will never return an item.{BR}If you accept returns, a buyer can return an item for any reason, even if they change their mind about a purchase.{BR}Important: Even if you specify "no returns accepted," under the eBay Money Back Guarantee the buyer can still return an item if it doesn\'t match the listing description. {BR}The recommendations and instructions on this page are for sellers who handle returns outside of eBay hassle-free returns. If you use eBay hassle-free returns, the requirements are a little different.';
//$lang['info_return_method']             = 'Buyers are more comfortable shopping with sellers who accept returns, even though most buyers will never return an item.{BR}If you accept returns, a buyer can return an item for any reason, even if they change their mind about a purchase.{BR}Important: Even if you specify "no returns accepted," under the eBay Money Back Guarantee the buyer can still return an item if it doesn\'t match the listing description. {BR}The recommendations and instructions on this page are for sellers who handle returns outside of eBay hassle-free returns. If you use eBay hassle-free returns, the requirements are a little different.';
//$lang['info_advance_profile_return']    = 'Buyers are more comfortable shopping with sellers who accept returns, even though most buyers will never return an item.{BR}If you accept returns, a buyer can return an item for any reason, even if they change their mind about a purchase.{BR}Important: Even if you specify "no returns accepted," under the eBay Money Back Guarantee the buyer can still return an item if it doesn\'t match the listing description. {BR}The recommendations and instructions on this page are for sellers who handle returns outside of eBay hassle-free returns. If you use eBay hassle-free returns, the requirements are a little different.';
$lang['info_return_method']             = 'Informations, help and support about {B}Returns{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-return/?lang=en';//If you accept returns, a buyer can return an item for any reason, even if they change their mind about a purchase.';
$lang['info_advance_profile_return']    = 'Informations, help and support about {B}Returns{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-return/?lang=en';//If you accept returns, a buyer can return an item for any reason, even if they change their mind about a purchase.';
$lang['info_advance_profile_shipping']  = "Informations, help and support about {B}Shipping{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-shippings/?lang=en";//When you list an item, you can offer buyers one of two possible shipping options: free shipping, flat fee shipping.{BR}Remember that your rank in search results is determined by adding your item’s price and its shipping cost.{BR}Free shipping: Offering free shipping is a great way to attract more buyers and get a higher placement in search results.{BR}Flat fee shipping: If you choose flat fee shipping, it’s a good idea to research average shipping costs for similar items or use the Shipping Calculator to determine a fair rate. If you’re listing internationally, you can enter a separate flat fee for destinations outside the United States.";
$lang['info_mapping_carriers']          = "Informations, help and support about {B}Shipping{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-shippings/?lang=en";//When you list an item, you can offer buyers one of two possible shipping options: free shipping, flat fee shipping.{BR}Remember that your rank in search results is determined by adding your item’s price and its shipping cost.{BR}Free shipping: Offering free shipping is a great way to attract more buyers and get a higher placement in search results.{BR}Flat fee shipping: If you choose flat fee shipping, it’s a good idea to research average shipping costs for similar items or use the Shipping Calculator to determine a fair rate. If you’re listing internationally, you can enter a separate flat fee for destinations outside the United States.";
$lang['info_wizard_shippings']          = "Informations, help and support about {B}Shipping{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-shippings/?lang=en";//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account.When you list an item, you can offer buyers one of two possible shipping options: free shipping, flat fee shipping.{BR}Remember that your rank in search results is determined by adding your item’s price and its shipping cost.{BR}Free shipping: Offering free shipping is a great way to attract more buyers and get a higher placement in search results.{BR}Flat fee shipping: If you choose flat fee shipping, it’s a good idea to research average shipping costs for similar items or use the Shipping Calculator to determine a fair rate. If you’re listing internationally, you can enter a separate flat fee for destinations outside the United States.";
$lang['info_mapping_conditions']        = "Informations, help and support about {B}Conditions{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-conditions/?lang=en";//When you list your item, you'll need to specify its condition.{BR}Note: It's important that you specify the condition accurately in order to avoid violating our selling practices policy. Find out more about describing an item's condition.{BR}Condition descriptions differ by item category. Use the list below of page to specify the condition you need to specify for your item.";
$lang['info_wizard_conditions']         = "Informations, help and support about {B}Conditions{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-conditions/?lang=en";//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}When you list your item, you'll need to specify its condition.{BR}Note: It's important that you specify the condition accurately in order to avoid violating our selling practices policy. Find out more about describing an item's condition.{BR}Condition descriptions differ by item category. Use the list below of page to specify the condition you need to specify for your item.";
$lang['info_advance_profile_condition'] = "Informations, help and support about {B}Conditions{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-conditions/?lang=en";//When you list your item, you'll need to specify its condition.{BR}Note: It's important that you specify the condition accurately in order to avoid violating our selling practices policy. Find out more about describing an item's condition.{BR}Condition descriptions differ by item category. Use the list below of page to specify the condition you need to specify for your item.";
$lang['info_advance_profile_variation'] = "Informations, help and support about {B}Attributes{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-attributes/?lang=en";//Instead of creating a number of separate listings for an item that has multiple options, you can create a single multi-quantity fixed price listing that includes all the variations you offer. For example, a t-shirt might come in multiple colors and sizes. Buyers can select which variation they want to purchase from the single listing.{BR}What categories can have listings with variations?{BR}You can create listings with variations in several eBay categories:{BR}Baby, Clothing, Shoes & Accessories, Crafts, Health & Beauty, Home & Garden, Jewelry & Watches, Pet Supplies, Sporting Goods, eBay Motors Apparel & Merchandise{BR}Not all categories support listings with variations. To find out if listings with variations are available in your category, use our category look-up tool.{BR}Best Offer and Lots options aren't available when listing items with variations.{BR}1. You can create a listing with variations in the advanced listing tool. Begin creating your listing by selecting a category. If eBay allow listings with variations in that category, you to opt to create a listing with variations.{BR}2. Variation details describe the differences between your items, such as color or size.{BR}3. Item specifics are characteristics that apply to all the items in your listing. For example, if all of the shoes in your listing are leather, you can add that to your listing as an item specific.{BR}4. While many variations are possible, you can only have 250 variations in your listing.";
$lang['info_mapping_attributes']        = "Informations, help and support about {B}Attributes{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-attributes/?lang=en";//Instead of creating a number of separate listings for an item that has multiple options, you can create a single multi-quantity fixed price listing that includes all the variations you offer. For example, a t-shirt might come in multiple colors and sizes. Buyers can select which variation they want to purchase from the single listing.{BR}What categories can have listings with variations?{BR}You can create listings with variations in several eBay categories:{BR}Baby, Clothing, Shoes & Accessories, Crafts, Health & Beauty, Home & Garden, Jewelry & Watches, Pet Supplies, Sporting Goods, eBay Motors Apparel & Merchandise{BR}Not all categories support listings with variations. To find out if listings with variations are available in your category, use our category look-up tool.{BR}Best Offer and Lots options aren't available when listing items with variations.{BR}1. You can create a listing with variations in the advanced listing tool. Begin creating your listing by selecting a category. If eBay allow listings with variations in that category, you to opt to create a listing with variations.{BR}2. Variation details describe the differences between your items, such as color or size.{BR}3. Item specifics are characteristics that apply to all the items in your listing. For example, if all of the shoes in your listing are leather, you can add that to your listing as an item specific.{BR}4. While many variations are possible, you can only have 250 variations in your listing.";
$lang['info_wizard_attributes']         = "Informations, help and support about {B}Attributes{/B} on our online documentation : http://documentation.feed.biz/version-1.0/manual/ebay-attributes/?lang=en";//If you are a new user, I would recommend you sign up for the ebay account. This wizard will cover the basic concepts of the eBay platform and get you familiarized with some of our popular program. At the end of the wizard, you will can export product to your ebay account. {BR}Instead of creating a number of separate listings for an item that has multiple options, you can create a single multi-quantity fixed price listing that includes all the variations you offer. For example, a t-shirt might come in multiple colors and sizes. Buyers can select which variation they want to purchase from the single listing.{BR}What categories can have listings with variations?{BR}You can create listings with variations in several eBay categories:{BR}Baby, Clothing, Shoes & Accessories, Crafts, Health & Beauty, Home & Garden, Jewelry & Watches, Pet Supplies, Sporting Goods, eBay Motors Apparel & Merchandise{BR}Not all categories support listings with variations. To find out if listings with variations are available in your category, use our category look-up tool.{BR}Best Offer and Lots options aren't available when listing items with variations.{BR}1. You can create a listing with variations in the advanced listing tool. Begin creating your listing by selecting a category. If eBay allow listings with variations in that category, you to opt to create a listing with variations.{BR}2. Variation details describe the differences between your items, such as color or size.{BR}3. Item specifics are characteristics that apply to all the items in your listing. For example, if all of the shoes in your listing are leather, you can add that to your listing as an item specific.{BR}4. While many variations are possible, you can only have 250 variations in your listing.";
$lang['info_actions']                   = "This will send all products with a profile within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go to the mapping and setting configuration in the eBay options tab.{BR} Then, do not forget to generate and save a report (For support you will need the XML and the Report)";
$lang['info_product_help']              = "Use this module to create and publish a new item listing and list it on an eBay site. Upon successful creation of an item listing, an Item ID is returned in the response along with the estimated fees for listing the item. Note that the fees returned from eBay account do not include the Final Value Fee; that fee cannot be calculated until the item is sold. ";


$lang['parameters']                     = "Parameters"; 
$lang['country_list']                   = "Country lists";
$lang['statistics']                     = "Statistics";
$lang['statistics_products']            = "Product statistics";
$lang['mapping']                        = "Mapping";
$lang['mapping_attributes']             = "Attributes";
$lang['mapping_attributes_value']       = "Attribute values";
$lang['ebay_root_category']             = "Root categories from eBay account";



$lang['templates']                      = "Templates";
$lang['get_orders']                     = "Orders";


$lang['orders']                         = "Orders report";
$lang['choose_a_attributes']            = "Choose an attribute";
$lang['choose_ebay_attributes']         = "Choose eBay attributes";
$lang['choose_specific_value']          = "Choose item specific value";
$lang['choose_variation_value']         = "Choose variation value";
$lang['choose_variation_type']          = "Choose variation type";
$lang['choose_a_categories']            = "Choose a category";
$lang['choose_a_condition']            = "Choose a condition";
$lang['choose_ebay_categories']         = "Choose eBay categories";
$lang['choose_ebay_store_categories']   = "Choose eBay store categories";
$lang['get_store']                      = "Import store categories";
$lang['insert_variation_type']          = "Insert variation type";
$lang['choose_a_shippings']             = "Choose a shipping";
$lang['choose_ebay_shippings']          = "Choose eBay shipping";
$lang['choose_a_country']               = "Choose a country";
$lang['choose_a_report']                = "Choose a report";
$lang['associate_carrier']              = "Choose an associate carrier";
$lang['ebay_carrier']                   = "Choose an eBay carrier";
$lang['default_template']               = "Default template mapping";
$lang['select_a_option']                = "Select a option";
$lang['returns_accepted']               = "Returns accepted";
$lang['returns_not_accepted']           = "No returns accepted";
$lang['no_tax']                         = "No tax";
$lang['days_1']                         = "1 day";
$lang['days_2']                         = "2 days";
$lang['days_3']                         = "3 days";
$lang['days_4']                         = "4 days";
$lang['days_5']                         = "5 days";
$lang['days_7']                         = "7 days";
$lang['days_10']                        = "10 days";
$lang['days_14']                        = "14 days";
$lang['days_15']                        = "15 days";
$lang['days_20']                        = "20 days";
$lang['days_30']                        = "30 days";
$lang['days_60']                        = "60 days";
$lang['buyer']                          = "Buyer";
$lang['seller']                         = "Seller";

$lang['green']                          = "Green";
$lang['greenNew']                       = "GreenNew";
$lang['blue']                           = "Blue";
$lang['percent']                        = "%";
$lang['percentage']                     = "Percentage";
$lang['value']                          = "Value";
$lang['optional']                       = "(Optional)";
$lang['optional_label']                 = "Optional";



$lang['export']                         = "Export";

$lang['endExport']                      = "Delete export";
$lang['sub_export']                     = "eBay export product";
$lang['export_product']                 = "Export products";
$lang['export_shipment']                = "Export shipment";
$lang['import_order']                   = "Import orders";
$lang['delete_product']                 = "Delete products";
$lang['sub_end_export']                 = "eBay delete export product";
$lang['offer_product_update']           = "Update Offers to Ebay";
$lang['export_product_update']          = "Export Products";
$lang['Admin Tools']                 	= "Admin Tools";
$lang['compress_a_zip_file']            = "Compress A Zip File";
$lang['import_order_update']            = "Import orders from eBay";
$lang['export_shipment_update']         = "Confirm orders shipment to eBay";
$lang['synchronize_wizard']             = "Synchronization wizard";
$lang['update_product_offer']           = "Update product offers";
$lang['delete_type']                    = "Delete product type";
$lang['delete_out_of_stock']            = "Delete out of stock products";
$lang['delete_all_sent_product']        = "Delete all sent products";
$lang['delete_full_sent_product']       = "Delete all product in eBay account";
$lang['delete_product_update']          = "Delete products from eBay";



//!!! Wizard  !!!
$lang['wizard_authorization']           = "Authorization";
$lang['wizard_choose']                  = "Choose site";
$lang['wizard_payments']                = "Payments";
$lang['wizard_returns']                 = "Returns";
$lang['wizard_univers']                 = "Universe";
											
$lang['wizard_categories']              = "Categories";
$lang['wizard_tax']                     = "Tax";
$lang['wizard_shippings']               = "Shippings";
$lang['wizard_exports']                 = "Export products";
$lang['wizard_finish']                  = "Finish";
$lang['wizard_attributes']              = "Attributes";
$lang['wizard_conditions']              = "Conditions";


//Condition
$lang['conditions']                     = "Conditions";
$lang['marketplace_condition']          = "Marketplace condition";
$lang['reset_condition']                = "Reset condition";
$lang['your_conditions']                = "Your condition";


//other
$lang['auth_security_token']            = "Auth security token";
$lang['new_auth_security']              = "New auth security";
$lang['auth_security_session']          = "Auth security session";
$lang['welcome']                        = "Welcome";
$lang['Configuration']                  = "Configuration";
$lang['choosesite']                     = "Choose eBay site";
$lang['importing']                      = "importing";
//statistic
$lang['product_id']                     = "Product ID";
$lang['user']                           = "User";
$lang['name']                           = "Name";
$lang['response']                           = "Response";
$lang['message']                           = "Message";
//order
$lang['orders']                                                 = "Orders";
$lang['id_order']                                               = "Order ID";
$lang['id_marketplace_order_ref']                               = "Order ID ref.";
$lang['id_buyer']                                               = "Buyer ID";
$lang['payment_method']                                         = "Payment by";
$lang['total_paid']                                             = "Paid amount";
$lang['order_date']                                             = "Order date";
$lang['order_completed']                                        = "Completed";
$lang['order_active']                                           = "Active";
$lang['orders_processing']                                      = "orders processing";
$lang['invoice_no']                                             = "Invoice ID";
$lang['country_name']                                           = "Country name";
$lang['product_export_statistic']                               = "Statistic export product";
//Profile
$lang['profile_name']                                           = "Profile name";
$lang['profile_completed']                                      = "Completed";
$lang['profile_status']                                         = "Status";
$lang['profile_date']                                           = "Creation date";
$lang['profile_edit']                                           = "Edit";
$lang['create_profile']                                         = "Create a profile";
$lang['delete_profile']                                         = "Delete a profile";
$lang['enabled_profile']                                        = "Enabled profile";
$lang['disabled_profile']                                       = "Disabled profile";
$lang['created_date']                                           = "Created Date";
$lang['profile_create_by']                                      = "Created by";
$lang['profile_completed_percent']                              = "Completed (%)";

//wizard
$lang['ebay_wizard_configuration']                              = "eBay Configuration wizard";
$lang['ebay_advance_configuration']                             = "Advanced profile wizard";



$lang['warning']                                                = "Warning";
$lang['ebay_wizard']                                            = "eBay wizard";
$lang['exported']                                               = "Exported";

$lang['email']                                                  = "Email";
$lang['Amount']                                                 = "Amount";
$lang['Site']                                                   = "Site";
$lang['eBay']                                                   = "eBay";
$lang['infomation']                                             = "Infomation";
$lang['reset_categories']                                       = "Reset categories";
$lang['reset_templates']                                        = "Reset templates";

$lang['send']                                                   = "Send to eBay account";
$lang['send_products_to_ebay']                                  = "Send products to eBay";
$lang['send_offers_to_ebay']                                    = "Send offers to eBay";
$lang['sending..']                                              = "Sending..";
$lang['get_from_ebay']                                          = "Get orders from eBay";
$lang['removing..']                                             = "Removing..";
$lang['starting_get']                                           = "Starting get";
$lang['starting_delete']                                        = "Start delete";
$lang['starting_send']                                          = "Starting send";
$lang['delete']                                                 = "Delete on eBay account";
$lang['Statistic Export Product']                               = "Product export statistics";

$lang['eBay Username']                                          = "eBay username";
$lang['Product Name']                                           = "Product name";
$lang['AddFixedPriceItem']                                      = "AddFixedPriceItem";
$lang['RelistFixedPriceItem']                                   = "RelistFixedPriceItem";
$lang['ReviseFixedPriceItem']                                   = "ReviseFixedPriceItem";
$lang['Compressed']                                             = "Compressed";
$lang['validation']                                             = "Validation";
$lang['success']                                                = "Success";
$lang['failure']                                                = "Failure";
$lang['Search Message']                                         = "Search message";
$lang['order']                                                  = "Import order from eBay Account";
$lang['delete_on_ebay']                                         = "Delete products on eBay";
$lang['Warning! Configuration parameter not complete.']         = "Warning! Parameter configuration is not complete.";
$lang['Carrier Configuration']                                  = "Carrier configuration";
$lang['info_mapping_attributes_value']                          = "You can select many attribute value in one time by selecting the first attribute name then press 'Shift' and click on the last element you wish to be in the same attribute";
$lang['export_from_wizard']                                     = "Export from wizard";

$lang['ebay_wizard_clear']                                      = "eBay_wizard_clear";
$lang['ebay_wizard_start']                                      = "eBay_wizard_start";
$lang['mapping_ebay_save']                                      = "Mapping_ebay_save";
$lang['statistic_processing']                                   = "Statistic processing";
$lang['Reset Categories']                                       = "Reset categories";
$lang['Reset Templates']                                        = "Reset templates";
$lang['mapping_carriers_save']                                  = "Save carriers mappings";
$lang['auth_security_save']                                     = "Save security auth";
$lang['wizard_statistic_link']                                  = "Wizard statistic link";
$lang['Save Mappings & Continue']                               = "Save mappings & continue";


$lang['ebay_wizard_shipping']                                   = "eBay_wizard_shipping";
$lang['attribute_ebay_save']                                    = "Attribute_ebay_save";
$lang['attribute_ebay_value_save']                              = "Attribute_ebay_value_save";

$lang['univers_save']                                           = "Universe_save";
$lang['test_ebay']                                              = "Test_eBay";
$lang['Exporting']                                              = "Exporting";
$lang['Confirm Export']                                         = "Confirm export";
$lang['Export']                                                 = "Export";
$lang['univers_save']                                           = "universe_save";

$lang['ebay_univers']                                           = "Select universe";
$lang['tax_save']                                               = "Tax_save";
$lang['get_postalcode_valid']                                   = "Get_postalcode_valid";





$lang['save_successful']                                        = "Save successful";
$lang['records']                                                = "Records";
$lang['Add Mapping Attributes']                                 = "Add mapping attributes";
$lang['get_configuration']                                      = "Get_configuration";


$lang['greenNew']                                               = "GreenNew";
$lang['templates_download']                                     = "Templates download";
$lang['download']                                               = "Download";
$lang['delete_only']                                            = "Delete";
$lang['preview_only']                                           = "Preview";
$lang['file_only']                                              = "File";
$lang['new_only']                                               = "New";
$lang['choose_file']                                            = "Choose file";
$lang['upload_template']                                        = "Upload template";
$lang['custom_templates']                                       = "Custom templates";
$lang['are_you_sure']                                           = "Are you sure?";

$lang['are_you_sure_insert']                                    = "Insert you variation type name : ";
$lang['are_you_sure_replace_profile']                           = "Are you sure you want to replace profile {B}{A}{/B}? If not, click Cancel.";
$lang['are_you_sure_synchronize_matching']                      = "Are you sure you want to matching {B}{A}{/B} against {B}{D}{/B}? If not, click Cancel.";
$lang['add_update_template']                                    = "Add / Update template";
$lang['ebay_wizard_site_change']                                = "ebay wizard site change";
$lang['ebay_wizard_finish']                                     = "eBay wizard finish";

$lang['ebay_univers_not_selected']                              = "You have not selected a category on <b>Universe</b> menu. Please back to previous step.";
$lang['ebay_attribute_univers_not_selected']                    = "You have not selected a attribute type. Please select a attribute type first, on the menu <b>'Configurations > Mapping > Attributes universe'</b>"; 
$lang['category_not_selected']                                  = "You have not a selected category. Please select a category first, on the menu <b>'My feeds > Parameters > Category'</b>"; 
$lang['mode']                                                   = "Mode"; 



//Link
$lang['advance_categories_processing']                          = "advance_categories_processing";
$lang['advance_categories_main_save']                           = "advance_categories_main_save";

$lang['mode']                                                   = "Mode";
$lang['Start Delete']                                           = "Start delete";
$lang['update_actions_success']                                 = "update actions success";
$lang['update_actions_fail']                                    = "update actions fail";
$lang['update_orders_success']                                  = "update orders success";
$lang['update_orders_fail']                                     = "update orders fail";
$lang['update_statistics_success']                              = "update statistics success";
$lang['update_statistics_fail']                                 = "update statistics fail";
$lang['export_compress']                                        = "export compress";
$lang['price_modifier_save']                                    = "price modifier save";
$lang['setJsonAllCategory']                                     = "set Json all Category";
$lang['mapping_templates_save']                                 = "mapping templates save";
$lang['mapping_categories_save']                                = "mapping_categories_save";
$lang['Records']                                                = "Records";
$lang['update_log_success']                                     = "update log success";
$lang['update_log_fail']                                        = "update log fail";
$lang['log_processing']                                         = "log processing";
$lang['update_log_success']                                     = "update_log_success";
$lang['log_download']                                           = "log_download";
$lang['mapping_conditions_save']                                = "mapping_conditions_save";


// new word (did not verify)
$lang['info_mapping_attributes_value']                          = "Info mapping attributes value";


$lang['This attribute provide the free text.'] = "This attribute provides the free text.";
$lang['You can'] = "You can";
$lang['mapping a new value'] = "mapping a new value";
$lang['use this value for'] = "use this value for";
$lang['mapping a new value'] = "mapping to a new value";
$lang['for'] = "for";
$lang['go_to_setting']      = "Please go to menu setting in configuration tab.";
$lang['go_to_payment']      = "Please go to menu payment in configuration tab.";
$lang['go_to_return']       = "Please go to menu return in configuration tab.";
$lang['require_setting']      = "Please complete Settings %s.";
$lang['require_carrier_mapping']      = "Please complete Carrier Mapping %s.";
$lang['require_category_mapping']      = "Please complete Category Mapping %s.";
$lang['require_delete_products']      = 'There are some products on Ebay did not be uploaded from Feedbiz. You can delete those products <a href="javascript:$(\'a[href=\\\'#tab-2\\\']\').click();void(0);">here</a>';

$lang['ON'] = "ON";
$lang['variation_save'] = "variation save";
$lang['variation_values'] = "variation values";
$lang['additions'] = "additions";
$lang['advanced'] = "advanced";
$lang['advance_profile_configuration_save'] = "advance profile configuration save";
$lang['advance_profile_payment_save'] = "advance profile payment save";
$lang['advance_profile_return_save'] = "advance profile return save";
$lang['advance_profile_condition_save'] = "advance profile condition save";
$lang['mapping_carriers_cost_save'] = "mapping carriers cost save";
$lang['variation'] = "variation";
$lang['Specific Fields'] = "Specific Fields";
$lang['use this value'] = "use this value";
$lang['Choose the value in specific fields and then click ADD button (+) to add more fields for this model.'] = "Choose the value in specific fields and then click ADD button (+) to add more fields for this model.";
$lang['variation_value_save'] = "variation value save"; 
$lang['wizard_choose_save'] = "wizard choose save";
$lang['templates_deleted'] = "templates deleted";
$lang['templates_upload'] = "templates_upload";
$lang['update_actions_products_success'] = "update_ actions products success";
$lang['update_actions_products_fail'] = "update actions products fail";
$lang['update_error_resolutions_success'] = "update error resolutions success";
$lang['update_error_resolutions_fail'] = "update error resolutions fail";
$lang['actions_compress_file_products_success'] = "Compress file products success";
$lang['actions_compress_file_products_fail'] = "Compress file products fail";

//new 
$lang['getStoreCategory'] = "get Store Category";
$lang['Run by'] = "Run by";
$lang['import_history_page'] = "import_history_page";
$lang['get_product_file'] = "get product file";
$lang['error_resolutions'] = 'error resolutions'; 
$lang['code'] = "code";
$lang['error_resolutions_products_processing'] = "error resolutions products processing";
$lang['error_resolutions_processing'] = "error resolutions processing";



$lang['revise_product'] = "Export revise inventory";
$lang['verify_product'] = "Export verify module";
$lang['debug_import_order'] = "Debug import order";
$lang['get_orders_debug'] = "Get orders debug";
$lang['test_action'] = "test_action";