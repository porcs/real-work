<?php

$lang['page_title']                     = "Offres";
$lang['offers']                         = "Offres";
$lang['my_feeds']                       = "Mes flux";
$lang['parameters']                     = "Paramètres";  

//Profile
$lang['profile']                        = "Profil";
$lang['price']                          = "Prix";

//Rules
$lang['rules']                          = "Règles";

//Category
$lang['category']                       = "Catégorie";

//Message
$lang['save_unsuccessful']              = "Échec de l'enregistrement.";
$lang['save_successful']                = "Enregistrement avec succès.";
$lang['confirm_message']                = "Voulez-vous supprimer ";
$lang['Change default shop successful'] = "Changement de la boutique par défaut avec succès";


$lang['Add a price to the list']        = "Ajouter un prix à la liste";
$lang['Price(s)']                       = "Prix";
$lang['Price Name']                     = "Nom du prix";
$lang['Percentage']                     = "Pourcentage";
$lang['Allow only digits.']             = "Autoriser uniquement les chiffres.";
$lang['Rounding']                       = "Arrondissement";
$lang['One Digit']                      = "Un chiffre";
$lang['Two Digit']                      = "Deux chiffres";
$lang['None']                           = "Aucun";
$lang['Use a familiar name to remember it.']                          
                                        = "Utiliser un nom familier pour s'en souvenir.";
$lang['Formula to be applied on all exported products prices (multiplication, division, addition, subtraction, percentages).']                          
                                        = "Formule à appliquer sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).";
$lang['Apply a specific price formula for selected categories which will override the main setting.']                          
                                        = "Appliquer une formule de prix spécifique pour certaines catégories qui remplacera le paramètre principal.";
$lang['Rounding mode to be applied to the price (ie: price will be equal to 10.17 or 10.20)']                          
                                        = "Mode d'arrondi à appliquer au prix (exemple: prix sera égal à 10,17 ou 10,20)";
$lang['Do you want to delete']          = "Voulez-vous supprimer";
$lang['Delete successfully']            = "Supprimé avec succès";
$lang['deleted']                        = "Supprimé";
$lang['can not delete']                 = "Impossible de supprimer";
$lang['Save and Continue']              = "Enregistrer et continuer";  




$lang['Rules'] = "Règles";
$lang['Add a rule to the list'] = "Ajouter une règle à la liste";
$lang['Rule(s)'] = "Règle(s)";
$lang['Rule Name'] = "Nom de règle";
$lang['Add Rule Name first'] = "Ajouter nom de règle en premier";
$lang['Add item'] = "Ajouter un article";
$lang['Item(s)'] = "Article(s)";
$lang['Manufacturer'] = "Fabricant";
$lang['Supplier'] = "Fournisseur";
$lang['Price Range'] = "Plage de prix";
$lang['to'] = "À";
$lang['Action'] = "Action";
$lang['Drop'] = "Déposer";
$lang['Item'] = "Article";
$lang['Manufactory'] = "Manufacture";
$lang['Supplier'] = "Fournisseur";
$lang['Price range'] = "Plage de prix";
$lang['Successfully'] = "Avec succès";
$lang['Delete was unsuccessful'] = "Échec de la suppression";
$lang['Delete was successful'] = "Suppression avec succès";
$lang['Are you sure to delete rule'] = "Êtes-vous sûr de vouloir supprimer la règle";
$lang['Are you sure to delete rule item'] = "Êtes-vous sûr de supprimer la règle d'article";
$lang['Are you sure to delete this Price range'] = "Êtes-vous sûr de supprimer cette palge de prix";
$lang['Price range error'] = "Erreur de plage de prix";
$lang['check_shop'] = "Vérifier la boutique";
$lang['rules_item_delete'] = "Supprimer les règles d'article";
$lang['rules_save'] = "Enregistrer les règles";
$lang['price_save'] = "Enregistrer le prix";
$lang['Warning!'] = "Avertissement !";
$lang['Please wait until importing is successfully completed'] = "Veuillez attendre jusqu'à ce que l'importation se termine avec succès";

//new
$lang['Rule name'] = "Nom de règle";
$lang['Please enter rule name first'] = "Veuillez entrer un nom de règle en premier";
$lang['Please enter unique rule name'] = "Veuillez entrer un nom de règle unique";
$lang['To'] = "À";
$lang['Are you sure to delete this price range'] = "Êtes-vous sûr de vouloir supprimer cette plage de prix";
$lang['New price range'] = "Nouvelle plage de prix";