<?php

//menu
$lang['dashboard']                          = "Tableau de bord";
$lang['page_title']                         = "Tableau de bord";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";
$lang['welcome']                            = "Bienvenue";                 
$lang['validation'] = 'Validation';
$lang['Log'] = 'Journal';
$lang['Last Import Log'] = 'Journal de dernière importation';
$lang['Batch ID'] = 'Identifiant Lot';
$lang['Source'] = 'Source';
$lang['Shop'] = 'Boutique';
$lang['Type'] = 'Type';
$lang['Total'] = 'Total';
$lang['No. Success'] = 'No. Succès';
$lang['No. Warning'] = 'No. Avertissement';
$lang['No. Error'] = 'No. Erreur';
$lang['Import Date'] = 'Date d\'importation';