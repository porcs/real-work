<?php

//automaton.php
$lang['Unable to init Amazon Service']= 'Impossible d\'initialiser le service Amazon.';
$lang['Inventory processing completed']= 'Traitement de l\'inventaire terminé.';
$lang['Amazon Marketplace Automaton started on']= 'L\'automate de la place de marché Amazon a commencé le';
$lang['Inventory processing not complete, the process will restart in %s hours']= 'Traitement de l\'inventaire non terminé, le processus redémarrera dans %s heures.';
$lang['File downloaded successfully, wait for processing']= 'Fichier téléchargé avec succès, veuillez attendre pour le traitement.';
$lang['Unable to find any inventory, the process will restart in %s hours.']= 'Impossible de trouver un inventaire, le processus redémarrera dans %s heures.';
$lang['The inventory is ready, wait for processing.']= 'L\'inventaire est prêt, veuillez attendre pour le traitement.';
$lang['Wait for the inventory, it should take a while, up to one hour']= 'Veuillez attendre pour l\'inventaire, cela peut prendre un certain temps, jusqu\'à une heure.'
$lang['Unable to find any inventory, the process will restart in 2 hours']= 'Impossible de trouver un inventaire, le processus redémarrera dans deux heures.';
$lang['The inventory is ready, wait for processing']= 'L\'inventaire est prêt, veuillez attendre pour le traitement.';
$lang['Wait for the inventory, it should take a while, up to one hour']= 'Veuillez attendre pour l\'inventaire, cela peut prendre un certain temps, jusqu\'à une heure.'
$lang['Unable to find any inventory, the process will restart in 2 hours']= 'Impossible de trouver un inventaire, le processus redémarrera dans deux heures.';
$lang['Requesting an inventory to Amazon']= 'Demande d\'inventaire d\'Amazon.';
$lang['Inventory file exists already and is not expired, reprocessing this feed']= 'Le fichier d\'inventaire existe déjà et n\'est pas expiré, retraitement le retraiter ce flux ...';
$lang['Request has been accepted, wait for processing']= 'La demande a été acceptée, veuillez attendre pour le traitement.';
$lang['Request failed, the request will be resubmitted in a while']= 'Échec de la demande, la demande sera soumise dans un moment.';
$lang['XML GetMatchingProductForIdResult was expected']= 'XML GetMatchingProductForIdResult  était attendu.';
$lang['XML was expected']= 'XML était attendu.';
$lang['Failed']= 'Échec';
$lang['Inventory is empty']= 'L\'inventaire est vide.';
$lang['Unable to write to output file']= 'Impossible d\'écrire dans le fichier de sortie.';
$lang['Unable to read input file']= 'Impossible de lire le fichier d\'entrée.';
$lang['Unable to save report']= 'Impossible d\'enregistrer le rapport.';
$lang['Unable to create path']= 'Impossible de créer le chemin.';
$lang['Unable to create import directory']= 'Impossible de créer le répertoire d\'importation.';

//amazon.report.php
$lang['Amazon get report started on']= 'Obtention de rapport Amazon démarré le';
$lang['Unable to connect Automaton']= 'Impossible de connecter l\'automate.';
$lang['TimeOut, Limit time : s, Process Time : s']= 'TimeOut,temps limite : %s, temps de traitement : %s.';
$lang['Success']= 'Succès';
$lang['success_remark']= 'Rapport obtenu avec succès.';
$lang['Unable to login']= 'Impossible de se connecter.';
$lang['REPORT_REQUEST']= 'REPORT REQUEST';
$lang['GET_REPORT_REQUEST_LIST']= 'GET REPORT REQUEST LIST';
$lang['GET_REPORT']= 'GET REPORT';
$lang['STEP_PROCESS_REPORT']= 'STEP PROCESS REPORT';
$lang['STEP_POST_PROCESS']= 'STEP POST PROCESS';
$lang['Skipped'] = "Ignoré";