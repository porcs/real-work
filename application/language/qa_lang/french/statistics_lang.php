<?php 
$lang["page_title"] 	 = "Rapport du Flux";		 //approved2 ec3
$lang["statistics"] 	 = "Rapport du Flux";		 //approved2 ec3
$lang["products"] 	 = "Produits";		 //approved2 ec3
$lang["manufacturers"] 	 = "Fabricants";		 //approved2 ec3
$lang["categories"] 	 = "Catégories";		 //approved2 ec3
$lang["suppliers"] 	 = "Fournisseurs";		 //approved2 ec3
$lang["carriers"] 	 = "Transporteurs";		 //approved2 ec3
$lang["results_for"] 	 = "Résultats pour";		 //approved1 ec3
$lang["information"] 	 = "Informations";		 //approved2 ec3
$lang["price"] 	 = "Prix";		 //approved2 ec3
$lang["topology"] 	 = "Topologie";		 //approved1 ec3
$lang["combination"] 	 = "Déclinaison";		 //not approved2 ec3
$lang["image"] 	 = "Image";		 //approved1 ec3
$lang["feature"] 	 = "Features";		 //not approved2 ec3
$lang["supplier"] 	 = "Fournisseur";		 //approved2 ec3
$lang["name"] 	 = "Nom";		 //approved2 ec3
$lang["reference"] 	 = "SKU";		 //approved2 ec3
$lang["ean13"] 	 = "EAN13";		 //approved2 ec3
$lang["upc"] 	 = "UPC";		 //approved2 ec3
$lang["status"] 	 = "Statut";		 //approved2 ec3
$lang["condition"] 	 = "Condition";		 //not approved2 ec3
$lang["description"] 	 = "Long description";		 //not approved2 ec3
$lang["tags"] 	 = "Mots clés";		 //approved1 ec3
$lang["enter_tags"] 	 = "Entrer les mots clés";		 //approved1 ec3
$lang["tax"] 	 = "Taxe";		 //approved2 ec3
$lang["currency"] 	 = "Devise";		 //approved2 ec3
$lang["specific_prices"] 	 = "Prix spécifiques";		 //approved1 ec3
$lang["available"] 	 = "Disponible";		 //approved1 ec3
$lang["apply_a_discount_of"] 	 = "Appliquer une réduction de";		 //approved1 ec3
$lang["general"] 	 = "Général";		 //approved2 ec3
$lang["width"] 	 = "Largeur";		 //approved1 ec3
$lang["height"] 	 = "Hauteur";		 //not approved2 ec3
$lang["depth"] 	 = "Profondeur";		 //approved1 ec3
$lang["weight"] 	 = "Poids";		 //approved2 ec3

  