<?php

$lang['Update Status Error']                         = "Update status error";
$lang['Fail']                                        = "Fail";
$lang['Send order #%s status : %s']                  = "Send order #%s status : %s";
$lang['Order #%s, %s [%s]']                          = "Order #%s, %s [%s]";
$lang['webservice']                                  = "Webservice";
$lang['send_orders']                                 = "Send_orders";
$lang['ebay']                                        = "eBay";
$lang['waiting order items']                         = "Missing order items";
