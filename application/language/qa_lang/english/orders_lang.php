<?php 
$lang["Exporting your orders to "] 	 = "Exporting your Orders to";		 //approved1 ec3
$lang["COMPLETED"] 	 = "Completed";		 //approved2 ec3
$lang["EMPTY ORDERS"] 	 = "No pending Orders";		 //approved1 ec3
$lang["NO STOCK TO UPDATE"] 	 = "No Stock to Update";		 //approved1 ec3
$lang["Product items mismatch"] 	 = "Products do not match";		 //approved2 ec3
$lang["Update stock movement to "] 	 = "Updating Stock movement to";		 //approved1 ec3
$lang["All right reserved."] 	 = "All rights reserved";		 //approved2.1 ec3
$lang["Dear"] 	 = "Dear";		 //approved2 ec3
$lang["Error Detail"] 	 = "Error";		 //approved2 ec3
$lang["Here is the order error list"] 	 = "Here is the list of Order errors";		 //approved1 ec3
$lang["Import Order Error"] 	 = "Errors: Imported Orders";		 //approved1 ec3
$lang["Marketplace"] 	 = "Marketplace";		 //approved1 ec3
$lang["Marketplace Order ID"] 	 = "Marketplace Order ID";		 //approved1 ec3
$lang["Order Date"] 	 = "Date";		 //approved2 ec3
$lang["Order ID"] 	 = "Order ID";		 //approved2 ec3
$lang["Order Status"] 	 = "Status";		 //approved1 ec3
$lang["Please note that you must use an email to access to Feed.biz management system."] 	 = "You will have to use your account, entering your email, to access Feed.biz";		 //approved2.1 ec3
$lang["Thanks"] 	 = "Thanks";		 //approved2 ec3
$lang["This e-mail was sent to"] 	 = "This email was sent to";		 //approved2 ec3

  