<?php 
$lang["page_title"] 	 = "Statistics";		 //approved2 ec3
$lang["statistics"] 	 = "Statistics";		 //approved2 ec3
$lang["products"] 	 = "Products";		 //approved2 ec3
$lang["manufacturers"] 	 = "Manufacturers";		 //approved2 ec3
$lang["categories"] 	 = "Categories";		 //approved2 ec3
$lang["suppliers"] 	 = "Suppliers";		 //approved2 ec3
$lang["carriers"] 	 = "Carriers";		 //approved2 ec3
$lang["results_for"] 	 = "Results for";		 //approved1 ec3
$lang["information"] 	 = "Information";		 //approved2 ec3
$lang["price"] 	 = "Price";		 //approved2 ec3
$lang["topology"] 	 = "Topology";		 //approved1 ec3
$lang["combination"] 	 = "Combination";		 //not approved2 ec3
$lang["image"] 	 = "Image";		 //approved1 ec3
$lang["feature"] 	 = "Features";		 //not approved2 ec3
$lang["supplier"] 	 = "Supplier";		 //approved2 ec3
$lang["name"] 	 = "Name";		 //approved2 ec3
$lang["reference"] 	 = "SKU";		 //approved2 ec3
$lang["ean13"] 	 = "EAN13";		 //approved2 ec3
$lang["upc"] 	 = "UPC";		 //approved2 ec3
$lang["status"] 	 = "Status";		 //approved2 ec3
$lang["condition"] 	 = "Product Conditions";		 //approved2 ec3
$lang["description"] 	 = "Long description";		 //approved2 ec3
$lang["tags"] 	 = "Tags";		 //approved1 ec3
$lang["enter_tags"] 	 = "Enter tags";		 //approved1 ec3
$lang["tax"] 	 = "Tax";		 //approved2 ec3
$lang["currency"] 	 = "Currency";		 //approved2 ec3
$lang["specific_prices"] 	 = "Specific Prices";		 //approved1 ec3
$lang["available"] 	 = "Available";		 //approved1 ec3
$lang["apply_a_discount_of"] 	 = "Apply a discount of";		 //approved1 ec3
$lang["general"] 	 = "General";		 //approved2 ec3
$lang["width"] 	 = "Width";		 //approved1 ec3
$lang["height"] 	 = "Height";		 //not approved2 ec3
$lang["depth"] 	 = "Depth";		 //approved1 ec3
$lang["weight"] 	 = "Weight";		 //approved2 ec3

  