<?php

//menu
$lang['users']                              = "Users";
$lang['page_title']                         = "My account";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";

/*Login*/
$lang['please_login']                       = "Please login";
$lang['password']                           = "Password";
$lang['email']                              = "Email";
$lang['remember_me']                        = "Remember me";
$lang['or_login_using']                     = "Or login using";
$lang['forgot_password']                    = "Forgotten password";
$lang['invalid_username']                   = "Invalid username/ password";
$lang['error_account_not_active']           = "An error occured, your account is not active.";

/*Registration*/
$lang['new_user_registration']              = "New user registration";
//$lang['enter_your_details_to_begin']        = "Enter your details to start";//not found
$lang['username']                           = "Username";
$lang['firstname']                          = "First name";
$lang['lastname']                           = "Last name";
$lang['password']                           = "Password";
$lang['passwordconf']                       = "Confirm password";
$lang['email']                              = "Email";
$lang['submit']                             = "Submit";
$lang['user_agreement']                     = "Terms and Conditions";
$lang['i_accept']                           = "I agree with";
$lang['Download']                           = "Read Feedbiz 's";
$lang['reset']                              = "Reset";
$lang['back_to_login']                      = "Back to login";
//$lang['please_wait_while_creating_user']    = "Please wait while user is being created";//not found

//message
$lang['please_enter_your_first_name']       = "Please enter your first name.";
$lang['please_enter_your_last_name']        = "Please enter your last name.";
$lang['please_enter_your_email']            = "Please enter your email.";
$lang['please_enter_your_username']         = "Please enter your username.";
$lang['please_provide_a_password']          = "Please provide a password";
$lang['your_password_must_be_long']         = "Your password must be at least 6 characters long";
$lang['please_enter_the_same_value_again']  = "Please enter the same value.";
$lang['please_agree']                       = "Join our newsletter.";
$lang['username_already_taken']             = "Username already taken.";
$lang['username_available']                 = "Username available.";
$lang['email_already_taken']                = "Email already taken.";
$lang['email_available']                    = "Email available.";
//$lang['account_creation_successful']        = "Account was created successfully.";//not found
$lang['account_creation_unsuccessful']      = "An error occured during the account creation.";
$lang['deactivate_unsuccessful']            = "Deactivation unsuccessful.";
$lang['please_activate_account_by_email_click']    = "Please activate your account by clicking on the link you received by email.";
$lang['deactivate_unsuccessful']            = "Deactivation unsuccessful. Please contact the admin.";
$lang['already_activated']                  = "Your email is already activated"; // , please contact an admin.
$lang['error_login_mismatch']               = "Error login mismatch.";
$lang['error_login_maxtries']               = "Error login max attempts.";
$lang['invalid_secure_code']               = "Invalid secure code. Please try again";
$lang['google_signup_btn']                  = "Sign up with Google account";
$lang['facebook_signup_btn']                = "Sign up with Facebook account";
$lang['or']                                 = "Or";
$lang['security_challenge']                = "Security challenge";
$lang['support_informations']               = "Support information";

$lang['Directory does not exists']          = "Directory does not exist";
$lang['Directory is not writable']          = "Directory is not writable";

//email
$lang['email_activation_subject']           = "Email activation.";
$lang['email_welcome_subject']              = "Welcome.";
$lang['account_can_not_send_activate_email']    = "Cannot send email.";

/*Retrieve Password*/
$lang['retrieve_password']                  = "Retrieve password";
$lang['enter_your_email_to_receive_instructions']    = "Enter your email to receive instructions.";
$lang['send_me']                            = "Send me";

//message
$lang['error_email_empty']                  = "Please enter your email address.";
$lang['email_require']                      = "Please enter your email address.";
$lang['email_forgotten_password_subject']   = "Forgot password email subject";
$lang['email_new_password_subject']         = "New password email subject.";
$lang['forgot_password_successful']         = "Please check your email box to reset FeedBiz password";
$lang['forgot_password_cant_send_mail']     = "Cannot send mail.";
$lang['forgot_password_unsuccessful']       = "Forgot password unsuccessful.";
$lang['code_wrong']                         = "Sorry, your link is wrong. Please try again.";
$lang['activate_unsuccessful']              = "Activation unsuccessful.";
$lang['activate_not_complete']              = "Activation not complete.";

//email
$lang['email_forgot_password_subject']      = "Email forgotten password.";
$lang['forgot_password_cant_send_mail']     = "Can not send email.";

/*Reset Password*/
$lang['reset_password']                     = "Reset password";
$lang['enter_your_new_password']            = "Enter your new password";
$lang['save']                               = "Save";
$lang['save_continue']                      = "Save and continue";

//message
$lang['invalid_user_id']  = "Invalid user ID.";
$lang['post_change_password_unsuccessful']  = "Please enter your email address.";
$lang['password_change_unsuccessful']       = "Forgot password email subject";
$lang['password_change_successful']         = "New Password email subject";
$lang['reset_password_validation_error']    = "Reset password validation error";
$lang['forgotten_password_code_wrong']      = "Sorry, the password is wrong. Please try again.";
$lang['forgot_password_email_not_found']    = "Sorry, we have not found your email.";
/*Profile*/
$lang['profile']                            = "Profile";
$lang['edit_profile']                       = "Edit profile";
$lang['general']                            = "General";
$lang['basic_info']                         = "Basic info";
$lang['username_from']                      = "Username from ";
$lang['note_can_not_change_email']          = "Note that if you use an email address, you can not change it later.";

//form
$lang['firstname']                          = "First name";
$lang['lastname']                           = "Last name";
$lang['new_password']                       = "New password";
$lang['new_passwordconf']                   = "Confirm password";
$lang['newsletter']                         = "Newsletter";
$lang['birthday']                           = "Birthday";
$lang['email']                              = "Email";
$lang['newsletter_detail']                  = "Join our newsletter";
$lang['current_password']                   = "Current password";
$lang['format_day_month_year']              = "Year-month-day";

//message
$lang['Invalid username/password']          = "Invalid username/password";
$lang['login_first']                        = "Please login.";
$lang['update_unsuccessful']                = "Update unsuccessful.";
$lang['update_successful']                  = "Profile edited successfully.";
$lang['please_enter_your_current_password'] = "Please enter your current password.";

/*Address*/
$lang['billing_information']                = "Billing information";
$lang['edit_billing_information']           = "Edit billing information";
$lang['address']                            = "Address";
$lang['information']                        = "Information";

//form
$lang['company']                            = "Company name";
$lang['complement']                         = "Floor/Office";
$lang['address1']                           = "Address";
$lang['address2']                           = "Address 2";
$lang['state_region']                       = "State/Region";
$lang['zipcode']                            = "Zip code";
$lang['city']                               = "City";
$lang['country']                            = "Country";
$lang['account_save_unsuccessful']          = "Billing information edited unsuccessful.";
$lang['account_save_successful']            = "Billing information edited successfully.";
$lang['error_current_password_empty']       = "You must enter your current password.";
$lang['error_email_empty']                  = "Please fill your email before adding password.";
$lang['current_password_mismat']            = "Your current password is not correct.";

/*authenticate*/
$lang['waiting_for_create_user']            = "Waiting for user creation";
$lang['sorry']                              = "Sorry";
$lang['something_may_have_gone_wrong']      = "Something may have gone wrong";
$lang['try_again']                          = "Please try again.";

/*affiliation*/
$lang['Let Feed.biz to management your e-commerce business'] = 'Let Feed.biz manage your e-commerce business';
$lang['affiliation'] = "Affiliation";
$lang['listReferrals'] = "Referrals list";
$lang['affiliateLists'] = "Affiliate Lists";
$lang['statistic'] = "Statistics";
$lang['invite_friend'] = "Invitations";
$lang['aff_title'] = "Affiliation";
$lang['aff_inv_title'] = "Invitations";
$lang['aff_stat_title'] = "Statistics";
$lang['aff_list_title'] = "Affiliate lists";
$lang['aff_inv_email_title'] = "Enter emails";
$lang['aff_inv_email_placeholder'] = "Add emails";
$lang['aff_inv_btn_send'] = "Send invite";

$lang['aff_acc_title'] = "Affiliate activity";

$lang['aff_inv_topic_share'] = "Connect & share";
$lang['aff_inv_topic_link'] = "Referrer link";
$lang['aff_inv_topic_email'] = "Invite friends via email";
$lang['aff_inv_descript_share'] = "With friend on Facebook, Twitter, or Google.";
$lang['aff_inv_descript_link'] = "Copy + paste your personal link into your website, blog, email or IM.";
$lang['aff_inv_descript_email'] = "Send invitation email to your friends and family.";
$lang['aff_inv_title_email'] = "Your friend's email";

$lang['aff_stat_com'] = "Commissions";
$lang['aff_stat_num'] = "Number of affiliate";

//forgot password page
$lang['Please provide a valid email.'] = "Please provide a valid email address.";
$lang['Please specify a password.'] = "Please specify a password.";
$lang['Enter your email address.'] = "Enter your email address.";
$lang['Enter your password.'] = "Enter your password.";

//affiliation
$lang['Join with us'] = "Join us";
$lang['ID'] = "ID";
$lang['User name'] = "User name";
$lang['Email'] = "Email";
$lang['Status'] = "Status";
$lang['Joined'] = "Joined";
$lang['Summary'] = "Summary";
$lang['IP address'] = "IP address";
$lang['Date'] = "Date";
$lang['Number of commission bills'] = "Number of commission bills";
$lang['Sales commission'] = "Sales commission";
$lang['Tier commission'] = "Tier commission";
$lang['Payout'] = "Payout";


//general
$lang['Login'] = "Login";
$lang['login'] = "login";
$lang['register'] = "register";
$lang['Create Account'] = "Create account";
$lang['billing_information_edit'] = "Billing information edit";
//$lang['Feed Mode'] = "Feed mode";//not found
$lang['Creation'] = "Creation";
$lang['Create products'] = "Create products";
$lang['General mode'] = "General mode";
$lang['Save'] = "Save";
$lang['Import history'] = "Import history";
//$lang['register'] = "Register";//not found
$lang['edit_user_validation_fname_label'] = "Edit user validation first name label";
$lang['edit_user_validation_lname_label'] = "Edit user validation last name label";
//$lang['login'] = "Login";//not found
//$lang['logout'] = "Logout";//not found
$lang['IP addres'] = "IP address";//new
$lang[' Join with us '] = " Join with us ";//new
$lang['authenticate_delete_user'] = "authenticate delete user";//new
$lang['invalide_secure_code'] = "Invalid secure code";//new
$lang['Create a new Account'] = "Create a new Account";//new
$lang['security_challlenge'] = "security challlenge";//new

$lang['get_default_shop'] = "Get default shop";
$lang['Checking'] = "Checking";
$lang['check_register_email'] = "Check register email";
$lang['captcha'] = "captcha";
$lang['register_save'] = "Register save";
$lang['profile_edit'] = "Profile edit";
$lang['clr_history'] = "clr_history";
//$lang['activate'] = "Activate";//not found
$lang['email_friend'] = "Email friend";
$lang['No data to save'] = "No data to save";
//$lang['remote'] = "Remote";//not found
//$lang['facebook'] = "Facebook";//not found
//$lang['int_callback'] = "Internation callback";//not found
//$lang['authenticate'] = "Authenticate";//not found
$lang['Log in your'] = "Log in your";
$lang['Facebook Account'] = "Facebook Account";
$lang['Google Account'] = "Google Account";
$lang['Create a new account?'] = "Create a new account?";

//new word (did not verify)
$lang['Login to your Account'] = "Login to your Account";//new
$lang['Clicks'] = "Clicks";//new
$lang['Last activity'] = "Last activity";//new
$lang['Share on your'] = "Share on your";//new
$lang['logout'] = "logout";
$lang['forgotten_password'] = "Forgotten password";
$lang['sent_invite_successful'] = "Send invite successful";
$lang['Please wait'] = "Please wait";
$lang['Twitter Account'] = "Twitter Account";
$lang['Show / Hide Columns'] = "Show / Hide Columns";

$lang['Image'] = "Image";
$lang['Add image'] = "Add image";
$lang['clr_original_redirect'] = "clr_original_redirect";
$lang['fields are required'] = 'fields are required';
$lang['Click here to activate account'] = "Click here to activate account";