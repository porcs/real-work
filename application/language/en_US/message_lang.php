<?php
// shared
$lang['user_name'] = 'User name';
$lang['status'] = 'Status';
$lang['user_information'] = 'User information';
$lang['user_password'] = 'User password';
$lang['password'] = 'Password';
$lang['confirm_password'] = 'Confirm Password';
$lang['group_manager'] = 'Group manager';
$lang['group_name'] = 'Group name';
$lang['company_manager'] = 'Company manager';
$lang['company_name'] = 'Company name';
$lang['description'] = 'Description';
$lang['active'] = 'Active';
$lang['inactive'] = 'InActive';
$lang['back'] = 'Back';
$lang['save'] = 'Save';
$lang['edit'] = 'Edit';
$lang['print'] = 'Print';
$lang['delete'] = 'Delete';
$lang['cancel'] = 'Cancel';
$lang['confirm'] = 'Confirm';
$lang['search'] = 'Search';
$lang['add'] = 'Add';
$lang['view'] = 'View';
$lang['export_csv'] = 'Export CSV';
$lang['export_csv_all'] = 'Export CSV (All)';
$lang['no_data_to_display'] = 'No data to display.';
$lang['total'] = 'Total';
$lang['page'] = 'page';
$lang['first'] = 'First';
$lang['last'] = 'Last';
$lang['admin_section'] = 'Admin Section';
$lang['reset_password'] = 'Reset Password';
$lang['components'] = 'Components';
$lang['group_component'] = 'Group components';
$lang['table_builder']  = 'Tables builder';
$lang['language_manager'] = 'Language manager';
$lang['message_manager'] = 'Message manager';
$lang['language_code_m'] = 'Language code';
$lang['language_name'] = 'Language name';
$lang['translate'] = 'Translate';
$lang['group_component_manager'] = 'Group Component Manager';
$lang['component_manager'] = 'Component manager';
$lang['name'] = 'Name';
$lang['groups'] = 'Groups';
$lang['group'] = 'Group';
$lang['general'] = 'General';
$lang['email_templates'] = 'Email templates';
$lang['new_user'] = 'New User';
$lang['email_address'] = 'Email Address';
$lang['email'] = 'Email';
$lang['provider'] = 'Provider';
$lang['provider_id'] = 'Provider ID';
$lang['last_login'] = 'Last login';
$lang['enable'] = 'Enable';
$lang['error'] = 'Error';
$lang['no_'] = 'No.';
$lang['tables'] = 'Tables';
$lang['actions'] = 'Actions';
$lang['change_password'] = 'Change Password';
$lang['take_me_home'] = 'Take Me Home';
$lang['save_change'] = 'Save changes';
$lang['subject'] = 'Subject';
$lang['message_body'] = 'Message body';
$lang['short_code'] = 'Shortcodes:';
$lang['site_address'] = 'Site address:';
$lang['full_name'] = 'Full name:';
$lang['user_email'] = 'User email:';
$lang['successfully_changed_setting'] = '<strong>Success!</strong> You successfully changed the setting';
$lang['user_manager_permission'] = 'User Manager: Permissions';
$lang['you_could_just_press'] = 'Or you could just press this neat little button:';
$lang['please_do_not_reply'] = 'Please Do Not Reply';
$lang['password_confirm_password_do_not_match'] = 'Password and Confirmation password do not match.';
$lang['you_want_to_remove'] = 'Are you sure you want to remove';
$lang['configure'] = 'configure';
$lang['language'] = 'Language';
$lang['first_name'] = 'First name';
$lang['last_name'] = 'Last name';
$lang['birthday'] = 'Birthday';
$lang['datepicker_sunday'] = 'Sunday';
$lang['datepicker_monday'] = 'Monday';
$lang['datepicker_tuesday'] = 'Tuesday';
$lang['datepicker_wednesday'] = 'Wednesday';
$lang['datepicker_thursday'] = 'Thursday';
$lang['datepicker_friday'] = 'Friday';
$lang['datepicker_saturday'] = 'Saturday';

$lang['datepicker_sun'] = 'Sun';
$lang['datepicker_mon'] = 'Mon';
$lang['datepicker_tue'] = 'Tue';
$lang['datepicker_wed'] = 'Wed';
$lang['datepicker_thu'] = 'Thu';
$lang['datepicker_fri'] = 'Fri';
$lang['datepicker_sat'] = 'Sat';

$lang['datepicker_su'] = 'Su';
$lang['datepicker_mo'] = 'Mo';
$lang['datepicker_tu'] = 'Tu';
$lang['datepicker_we'] = 'We';
$lang['datepicker_th'] = 'Th';
$lang['datepicker_fr'] = 'Fr';
$lang['datepicker_sa'] = 'Sa';

$lang['datepicker_january'] = 'January';
$lang['datepicker_february'] = 'February';
$lang['datepicker_march'] = 'March';
$lang['datepicker_april'] = 'April';
$lang['datepicker_may'] = 'May';
$lang['datepicker_june'] = 'June';
$lang['datepicker_july'] = 'July';
$lang['datepicker_august'] = 'August';
$lang['datepicker_september'] = 'September';
$lang['datepicker_october'] = 'October';
$lang['datepicker_november'] = 'November';
$lang['datepicker_december'] = 'December';

$lang['datepicker_jan'] = 'Jan';
$lang['datepicker_feb'] = 'Feb';
$lang['datepicker_mar'] = 'Mar';
$lang['datepicker_apr'] = 'Apr';
$lang['datepicker_may'] = 'May';
$lang['datepicker_jun'] = 'Jun';
$lang['datepicker_jul'] = 'Jul';
$lang['datepicker_aug'] = 'Aug';
$lang['datepicker_sep'] = 'Sep';
$lang['datepicker_oct'] = 'Oct';
$lang['datepicker_nov'] = 'Nov';
$lang['datepicker_dec'] = 'Dec';


$lang['date_text'] = array('days' => array($lang['datepicker_sunday'], $lang['datepicker_monday'], $lang['datepicker_tuesday'], $lang['datepicker_wednesday'], $lang['datepicker_thursday'], $lang['datepicker_friday'],  $lang['datepicker_saturday'], $lang['datepicker_sunday']),
		'daysShort' => array($lang['datepicker_sun'], $lang['datepicker_mon'], $lang['datepicker_tue'], $lang['datepicker_wed'], $lang['datepicker_thu'], $lang['datepicker_fri'], $lang['datepicker_sat'], $lang['datepicker_sun']),
		'daysMin' => array($lang['datepicker_su'], $lang['datepicker_mo'], $lang['datepicker_tu'], $lang['datepicker_we'], $lang['datepicker_th'], $lang['datepicker_fr'], $lang['datepicker_sa'], $lang['datepicker_su']),
		'months' => array($lang['datepicker_january'], $lang['datepicker_february'], $lang['datepicker_march'], $lang['datepicker_april'], $lang['datepicker_may'], $lang['datepicker_june'], $lang['datepicker_july'], $lang['datepicker_august'], $lang['datepicker_september'], $lang['datepicker_october'], $lang['datepicker_november'], $lang['datepicker_december']),
		'monthsShort' => array($lang['datepicker_jan'], $lang['datepicker_feb'], $lang['datepicker_mar'], $lang['datepicker_apr'], $lang['datepicker_may'], $lang['datepicker_jun'], $lang['datepicker_jul'], $lang['datepicker_aug'], $lang['datepicker_sep'], $lang['datepicker_oct'], $lang['datepicker_nov'], $lang['datepicker_dec']));


// into objects/admin/common/activate_complete.tpl.php file.
$lang['your_account_is_activated'] = '<h3>Thank You!</h3> Your account is activated. You may now <a href="%s">sign in</a>.';

// into objects/admin/common/dashboard.tpl.php file.
$lang['easy_manage_users_database'] = 'Easy manage Users, Database and without writing a single line of code';
$lang['easy_install'] = 'Easy to Install';
$lang['get_setup_in_minutes'] = 'Get setup in minutes! Enjoy the super easy installation wizard to walk you through the setup process.';
$lang['security'] = 'Security';
$lang['anti_sql_injections_xss_csrf'] = 'Anti sql injections, XSS and CSRF';
$lang['users_manager'] = 'Users manager';
$lang['manage_users_groups'] = 'Manage Users, Groups and Permissions';
$lang['tools_manager'] = 'Tools manager';
$lang['easy_create_tables_components'] = 'Easy create tables and components (Drag and drop builder) without writing a single line of code.';
$lang['setting_manager'] = 'Settings manager';
$lang['change_configure_and_email_templates'] = 'Change configure and email templates.';

// into objects/admin/common/footer.tpl.php
$lang['copyright_company'] = '&copy; Company 2012';
$lang['log_out'] = 'Log out';

// into objects/admin/common/login.tpl.php
$lang['sign_in'] = 'Sign in';
$lang['incorrect_username_or_password'] = 'Incorrect Username or Password or OTP!';
$lang['sign_in'] = 'Sign in';
$lang['remember_me'] = 'Remember me';
$lang['can_not_access_your_account'] = "Can't access your account?";
$lang['create_an_account'] = 'Create an Account';
$lang['sent_password_reset_email'] = 'We have sent you an password reset instructions email. Please check your email.';
$lang['please_provide_a_correct_email'] = '<strong>Error</strong> !, Please provide a correct email address.';

// into objects/admin/common/reset_password_complete.tpl.php
$lang['successfully_reset_your_password'] = '<h3>Thank You!</h3>  Successfully reset your password. You may now <a href="%s">sign in</a>.';

// into objects/admin/common/reset_password.tpl.php
$lang['new_password'] = 'New Password';
$lang['reset_password_submit'] = 'Submit';

// into objects/admin/common/signup_complete.tpl.php
$lang['you_have_been_registered'] = '<h3>Thank You!</h3> You have been registered, you have been sent an e-mail to the address you specified before. Please check your e-mails to activate your account. <a href="%s">Sign In</a>';


// into objects/admin/common/signup.tpl.php
$lang['create_account'] = 'Create an Account';
$lang['recaptcha'] = 'ReCaptcha';
$lang['create_my_account'] = 'Create my account';
$lang['have_an_account'] = 'Have an account? Sign in!';

// into objects/admin/component/builder.tpl.php
$lang['tool_component_builder'] = 'Tools: Component builder';

// into objects/admin/component/groups.tpl.php
$lang['tool_group_component'] = 'Tools: Group components';

// into objects/admin/language/browse.tpl.php
$lang['tool_language_manager'] = 'Tools: Language manager';

// into objects/admin/menu/menu.tpl.php
$lang['main'] = 'Main';
$lang['users'] = 'Users';
$lang['user_manager'] = 'User manager';
$lang['company'] = 'Company';
$lang['permissions'] = 'Permissions';
$lang['tools'] = 'Tools';
$lang['management'] = 'Management';
$lang['component_builder'] = 'Component builder';
$lang['settings'] = 'Settings';
$lang['edit_profile'] = 'Edit Profile';
$lang['messages'] = 'Messages';
$lang['message'] = 'Message';
$lang['add_message'] = 'Add Message';
$lang['message_info'] = 'Message Information';
$lang['message_code'] = 'Code';
$lang['message_type'] = 'Type';
$lang['message_name'] = 'Name';
$lang['message_problem'] = 'Problem';
$lang['message_cause'] = 'Cause';
$lang['message_solution'] = 'Solution';

// into objects/admin/scrud/include/config_data_list.php
$lang['title'] = 'Title';
$lang['rows_per_page'] = 'Rows per page';
$lang['order_by'] = 'Order by';
$lang['asc'] = 'ASC';
$lang['desc'] = 'DESC';
$lang['no_column'] = 'No. column';
$lang['join'] = 'Join';
$lang['add_join'] = 'Add join';
$lang['filter_elements'] = 'Filter elements';
$lang['column_elements'] = 'Column elements';

// into objects/admin/scrud/config.tpl.php
$lang['tool_configure'] = 'Tools: Configure';
$lang['form'] = 'Form';
$lang['list'] = 'List';
$lang['normal'] = 'Normal';
$lang['horizontal'] = 'Horizontal';

// into objects/admin/setting/email_new_user.tpl.php
$lang['setting_email_new_user'] = 'Settings: Email (New User)';
$lang['send_link'] = 'Send link';
$lang['activated'] = 'Activated';
$lang['activation_link'] = 'Activation link:';

// into objects/admin/setting/email_reset_password.tpl.php
$lang['setting_email_reset_password'] = 'Settings: Email (Reset Password)';
$lang['request'] = 'Request';
$lang['reset_link'] = 'Reset link';

// into objects/admin/setting/index.tpl.php
$lang['general_options'] = 'General Options';
$lang['admin_email'] = 'Admin Email';
$lang['default_group'] = 'Default Group';
$lang['disable_registrations'] = 'Disable registrations';
$lang['disable_reset_password'] = 'Disable reset password';
$lang['require_email_activation'] = 'Require email activation for new users';
$lang['recaptcha_options'] = 'ReCaptcha Options';
$lang['public_key'] = 'Public Key';
$lang['private_key'] = 'Private Key';
$lang['smtp'] = 'SMTP';
$lang['smtp_host'] = 'SMTP hostname <br /> (eg: mail.yoursite.com)';
$lang['smtp_server'] = 'SMTP server';
$lang['smtp_port'] = 'SMTP Port';
$lang['smtp_security'] = 'SMTP Security';
$lang['nothing_default'] = 'Nothing (default)';
$lang['ssl'] = 'SSL';
$lang['tls'] = 'TLS (use for Google SMTP)';
$lang['use_smtp_authentication'] = 'Use SMTP authentication';
$lang['smtp_username'] = 'SMTP Username';
$lang['smtp_account_username'] = 'SMTP account username';
$lang['smtp_password'] = 'SMTP Password';
$lang['default_language'] = 'Default language';


// into objects/admin/table/add.tpl.php
$lang['tools_table_builder'] = 'Tools: Table builder';
$lang['create'] = 'Create';
$lang['table'] = 'Table';
$lang['table_name'] = 'Table Name';
$lang['storage_engine'] = 'Storage Engine';
$lang['collation'] = 'Collation';
$lang['table_comments'] = 'Table Comments';
$lang['config_of_this_table_will_be_removed'] = 'Config of this table will be removed when save button is clicked.';

// into objects/admin/table/browse.tpl.php
$lang['tool_table_builder'] = 'Tools: Table builder';
$lang['add_table'] = 'Add table';

// into objects/admin/user/group/browse.tpl.php
$lang['user_manager_groups'] = 'User Manager: Groups';

// into objects/admin/user/permission/browse.tpl.php
$lang['group_permissions'] = 'Group permissions';
$lang['user_permissions'] = 'User permissions';
$lang['administrator_levels'] = 'Administrator levels';
$lang['user_management'] = 'User management';
$lang['tool_management'] = 'Tool management';
$lang['setting_management'] = 'Setting management';
$lang['global_access'] = 'Global access';
$lang['manage_components'] = 'Manage Components';
$lang['component_name'] = 'Component name';
$lang['export_list_search_view'] = 'Export/List/Search/View';
$lang['you_successfully_saved'] = '<strong>Success!</strong> You successfully saved';

// into objects/admin/user/permission/user_browse.tpl.php
$lang['choose_user'] = 'Choose User';
$lang['search_for_a_user'] = 'Search for a User';

// into objects/admin/user/user/browse.tpl.php
$lang['user_manager_users'] = 'User Manager: Users';

// into objects/user/menu/menu.tpl.php
$lang['account_settings'] = 'Account Settings';

// into objects/user/password/password.tpl.php
$lang['you_have_successfully_changed_your_password'] = '<strong>Success!</strong> You have successfully changed your password.';
$lang['current_password'] = 'Current Password';
$lang['new_password'] = 'New Password';
$lang['re_type_new_password'] = 'Re-type New Password';

// into pages/error/404.php
$lang['page_not_found'] = 'Page Not Found';
$lang['404_messages'] = 'The page you requested could not be found, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from';

// into pages/error/404.php
$lang['data_not_found'] = 'Data Not Found';

// into pages/error/no_access.php
$lang['access_is_denied'] = 'Access is denied';
$lang['403_messages'] = 'The page you requested could not be Access, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from';

// into pages/admin/activate_complete.php
$lang['activated_complete'] = ' Activated Complete';

// into pages/admin/reset_password_complete.php
$lang['reset_password_complete'] = 'Reset Password Complete';

// into pages/admin/reset_password.php
$lang['reset_password'] = 'Reset Password';

// into pages/admin/signup_complete.php
$lang['sign_up_complete'] = 'Sign Up Complete';

// into pages/admin/signup.php
$lang['sign_up'] = 'Sign Up';

// into objects/admin/common/Common.php
$lang['account_already_exits'] = 'Account already exists, please try another account';
$lang['email_already_exits'] = 'Email already exists, please try another email';
$lang['recaptcha_was_not_correct'] = "The reCAPTCHA wasn't entered correctly. Try it again.";
$lang['your_reset_password_code_is_expired'] = 'Your reset password code is expired';
$lang['your_reset_password_code_id_not_existed'] = 'Your reset password code is not existed';

// into objects/admin/scrud/Scrud.php
$lang['please_enter_value'] = "Please enter the value for %s.";
$lang['please_provide_alphabetic_input'] = "Please provide alphabetic input for %s.";
$lang['please_provide_alphabetic_with_space_input'] = '"Please provide alphabetic with space input for %s."';
$lang['please_provide_numeric_input'] = "Please provide numeric input for %s.";
$lang['please_provide_alphan_numeric_input'] = "Please provide an alpha-numeric input for %s.";
$lang['please_provide_alpha_numeric_with_space_input'] = "Please provide an alpha-numeric with space input for %s.";
$lang['please_provide_valid_date'] = "Please provide a valid date for %s.";
$lang['please_provide_valid_date_time'] = "Please provide a valid date time for %s.";
$lang['please_provide_valid_email'] = "Please provide a valid email address for %s.";
$lang['please_provide_valid_ip'] = "Please provide a valid IP for %s.";
$lang['please_provide_valid_url'] = "Please provide a valid URL for %s.";

// into objects/admin/setting/Setting.php
$lang['please_provide_valid_email_for_admin_email'] = 'Please provide a valid email address for Admin Email';

// into objects/admin/table/Table.php
$lang['please_enter_value_for_table_name'] = 'Please enter the value for Table Name';

// into library/FileUpload.php
$lang['only_files_with_extensions_allwed'] = "Only files with the following extensions are allowed: <b>%s</b>";
$lang['upload_directory_not_allowed_write'] = "Sorry, %s upload directory is not allowed to write";
$lang['upload_directory_does_not_exist'] = "Sorry, %s upload directory doesn't exist!";
$lang['file_already_exists'] = "Uploading <b>%s...Error!</b> Sorry, a file with this name already exitst.";
$lang['upload_file_exceeds_the_max'] = "The uploaded file exceeds the max. upload filesize directive in the server configuration.";
$lang['upload_file_exceeds_max_file_size'] = '"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form."';
$lang['upload_file_was_not_partially_uploaded'] = "The uploaded file was only partially uploaded";
$lang['no_file_was_uploaded'] = "No file was uploaded";

// into pages/admin/language/index.php
$lang['language_code_is_existed'] = 'Language code is existed, Try another?';
$lang['directory_is_not_allowed_write'] = 'Sorry, <b>%s</b> directory is not allowed to write.';
$lang['file_is_not_allowed_write'] = 'Sorry, <b>%s</b> file is not allowed to write.';

// into pages/admin/user/company
$lang['complement'] = 'Floor/Office';
$lang['address1'] = 'Address';
$lang['address2'] = 'Address 2';
$lang['stateregion'] = 'State/Region';
$lang['zipcode'] = 'Zipcode';
$lang['city'] = 'City';
$lang['country'] = 'Country';
$lang['view_user_profile'] = 'Profile';
$lang['view_user_company'] = 'Company';
$lang['remote_login'] = 'Remote';

$lang['newsletter'] = 'Newsletter';

// gift voucher
$lang['giftvoucher_manager'] = 'Gift Voucher Generator';
$lang['giftvoucher_table_number'] = 'Number';
$lang['giftvoucher_table_title'] = 'Gift Voucher Title';
$lang['giftvoucher_table_code'] = 'Code';
$lang['giftvoucher_table_used'] = 'Used';
$lang['giftvoucher_table_remain'] = 'Remain';
$lang['giftvoucher_table_active'] = 'Active Date';
$lang['giftvoucher_table_exp'] = 'Expire Date';
$lang['giftvoucher_opt_add_title'] = 'Create Gift Voucher Option';
$lang['giftvoucher_opt_edit_title'] = 'Edit Gift Voucher Option';
$lang['giftvoucher_detail_title'] = 'Detail';

$lang['giftvoucher_opt_add_name'] = 'Title';
$lang['giftvoucher_opt_add_cmd'] = 'Command Parameter';
$lang['giftvoucher_opt_add_amt'] = 'Discount Amount';
$lang['giftvoucher_opt_add_currency'] = 'Amount Currency';
$lang['giftvoucher_opt_add_qty'] = 'Code Quantity';
$lang['giftvoucher_opt_add_opt'] = 'Period';
$lang['giftvoucher_opt_add_start'] = 'Start Date';
$lang['giftvoucher_opt_add_exp'] = 'Exp Date';
$lang['giftvoucher_opt_add_end'] = 'End Date';
$lang['giftvoucher_opt_add_created'] = 'Created Date';
$lang['giftvoucher_code_id'] = 'Code ID';

$lang['giftvoucher_gen_title'] = 'Generate Gift Code';

// management billing sub menu
$lang['manager_bill'] = 'Billing Manager';
$lang['manager_bill_detail'] = 'Billing Detail';
$lang['billing'] = 'Billings';

$lang['billing_title_id'] = 'Billing ID';
$lang['billing_title_username'] = 'User name';
$lang['billing_title_amount'] = 'Total Amount';
$lang['billing_title_method'] = 'Payment Method';
$lang['billing_title_created'] = 'Created Date';
$lang['billing_first_name'] = 'Customer First Name';
$lang['billing_last_name'] = 'Customer Last Name';
$lang['billing_company'] = 'Company Name';
$lang['billing_payment_method'] = 'Payment Method';
$lang['billing_dc_title'] = 'Discount Title';
$lang['billing_dc_detail'] = 'Discount Detail';
$lang['billing_dc_code'] = 'Gift Code';
$lang['billing_dc_full'] = 'Full Amount';
$lang['billing_dc_amt'] = 'Discount Amount';
$lang['billing_dc_total'] = 'Total Amount';
$lang['billing_payment_log'] = 'Payment Log';
$lang['billing_table_id'] = 'ID';
$lang['billing_table_package'] = 'Package Name';
$lang['billing_table_type'] = 'Type';
$lang['billing_table_amt'] = 'AMT';
$lang['billing_table_startdate'] = 'Start Date';
$lang['billing_table_enddate'] = 'End Date';

// management package sub menu
$lang['packages'] = 'Packages';
$lang['packages_manager'] = 'Packages manager';
$lang['region'] = "Region";
$lang['marketplace'] = "Marketplace";
$lang['country'] = "Country";
$lang['price'] = "Price";
$lang['zone'] = "Zone";
$lang['packages_manager'] = "Packages Management";
$lang['currency'] = "Currency";
$lang['priod'] = "Priod";
$lang['site'] = "Site";
$lang['comments'] = "Comments";
$lang['id_site_ebay'] = "ID site eBay (for Dev.)";
$lang['extension'] = "Site extension";
$lang['pkg_type'] = "Package type (for Dev.)";
$lang['marketplace_manager'] = "Marketplace manager";
$lang['sort_by'] = "Sort No.";
$lang['has_sub'] = "Have sub package?";
$lang['codeigniter_admin_pro'] = "Feedbiz Admin page";
$lang['admin'] = "admin";
$lang['user'] = "user";
$lang['dashboard'] = "dashboard";
$lang['permission'] = "permission";

$lang['Amazon bulk valid values'] = "Amazon bulk valid values";
$lang['mapping_colors'] = "Mapping colors";
$lang['mapping'] = "Mapping";
$lang['color_name'] = "Color name";
$lang['color_com'] = "Color for .com";
$lang['color_uk'] = "Color for .uk";
$lang['color_fr'] = "Color for .fr";
$lang['color_de'] = "Color for .de";
$lang['color_es'] = "Color for .es";
$lang['color_it'] = "Color for .it";
$lang['amazon'] = "Amazon";
$lang['fbmapping'] = "FeedBiz mapping";

/* Error Code Explanations 11/05/2015*/
$lang['Ebay Error Code Explanations'] = "Ebay Error Code Explanations";
$lang['ebay_error_resolutions'] = "Ebay error resolutions";
$lang['error_code'] = "Error code";
$lang['error_details'] = "Error details";
$lang['error_resolution'] = "Error resolution";
$lang['error_solved_type'] = "Error priority";
$lang['show_message'] = "Show message";
$lang['show_message_inline'] = "Show message inline";
$lang['allow_override'] = "Allow override";
$lang['allow_override_attribute'] = "Allow override attribute";
$lang['error_priority'] = "Priority";
$lang['error_lang'] = "Language";
$lang['msg_pattern'] = "Pattern";
$lang['msg_field'] = "Field";
$lang['other'] = "Other";
$lang['type'] = "Solved Type";
$lang['date'] = "Last update date";
$lang['error_code_explanations'] = "Error Code Explanations";
$lang['value'] = "Value";
$lang['Error type'] = "Error of";
$lang['notification_summary'] = "Notification Summary";