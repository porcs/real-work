<?php

// 19/02/2015
$lang['Amazon%s Started Order Query'] = 'Amazon %s début de requête de commandes';
$lang['Start Get Order List'] = 'Début de l\'obtention de la liste des commandes';
$lang['See all messages'] = 'Voir tous les messages';
$lang['Getting %s Order List'] = 'Obtention de la liste de commandes %s';/*AVA*/
$lang['Amazon Get Orders Error'] = 'Obtenir les erreurs des commandes Amazon';
$lang['No pending order for %s to %s'] = 'Aucune commande en attente de %s à %s';
$lang['Order Query Processing'] = 'Traitement de la requête de commandes';
$lang['Unable to connect Amazon'] = 'Impossible de se connecter Amazon';
$lang['Order #%s, Was already imported'] = 'Commande #%s, a été déjà importé';
$lang['Order #%s, Missing Buyer Address for this order'] = 'Commande #%s, Adresse de l\'acheteur manquante pour cette commande';
$lang['Order #%s, Missing Buyer Name for this order'] = 'Commande #%s, Nom de l\'acheteur manquantes pour cette commande';
$lang['Order #%s, Missing Buyer Email for this order'] = 'Commande #%s, E-mail de l\'acheteur manquant pour cette commande';
$lang['Order ID (%s) Could not import incomplete order'] = 'Identifiant Commande (%s) Impossible d\'importer une commande incomplète';
$lang['Order ID (%s) Item does not have SKU - #%s'] = 'Identifiant Commande (%s) L\'article n\'a pas de SKU - #%s';
$lang['Order ID (%s) Could not import product SKU : %s'] = 'Identifiant Commande (%s) Impossible d\'importer le SKU produit : %s';
$lang['Order ID (%s) Could not add buyer'] = 'Identifiant Commande (%s) Impossible d\'ajouter un acheteur';
$lang['Unable to associate the carrier (%s) for order #%s'] = 'Ce n\'est pas une erreur ! Vous devez configurer le mapping du transporteur dans la configuration. Impossible d\'associer le transporteur (%s) pour la commande #%s';
$lang['Unable to execute parameters for order #%s'] = 'Impossible d\'exécuter les paramètres de la commande #%s';
$lang['Unable to save order items'] = 'Impossible d\'enregistrer les articles de la commande';
$lang['Success'] = 'Succès';