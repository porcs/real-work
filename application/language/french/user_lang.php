<?php

//menu
$lang['users']                              = "Utilisateur";
$lang['page_title']                         = "Mon compte";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";

/*Login*/
$lang['please_login']                       = "Veuillez vous identifier";
$lang['password']                           = "Mot de passe";
$lang['email']                              = "Email";
$lang['remember_me']                        = "Se souvenir de moi";
$lang['or_login_using']                     = "Ou se connecter en utilisant";
$lang['forgot_password']                    = "Mot de passe oublié";/*ACA*/
$lang['invalid_username']                   = "Nom d'utilisateur/mot de passe non valide(s)";
$lang['error_account_not_active']           = "Une erreur s'est produite, votre compte n'est pas actif.";

/*Registration*/
$lang['new_user_registration']              = "Nouvel enregistrement d'utilisateur";
//$lang['enter_your_details_to_begin']        = "Enter your details to start";//not found
$lang['username']                           = "Nom d'utilisateur";
$lang['firstname']                          = "Prénom";
$lang['lastname']                           = "Nom";
$lang['password']                           = "Mot de passe";
$lang['passwordconf']                       = "Confirmer le mot de passe";
$lang['email']                              = "E-mail";
$lang['submit']                             = "Soumettre";
$lang['user_agreement']                     = "termes et conditions";
$lang['i_accept']                           = "J'accepte";/*AVA*/
$lang['Download']                           = "Lire Feedbiz's";/*ACA*/
$lang['reset']                              = "réinitialiser";
$lang['back_to_login']                      = "Retour à la connexion";
//$lang['please_wait_while_creating_user']    = "Please wait while user is being created";//not found

//message
$lang['please_enter_your_first_name']       = "Veuillez entrer votre prénom.";
$lang['please_enter_your_last_name']        = "Veuillez entrer votre nom.";
$lang['please_enter_your_email']            = "Veuillez entrer votre e-mail.";
$lang['please_enter_your_username']         = "Veuillez entrer votre nom d'utilisateur.";
$lang['please_provide_a_password']          = "Veuillez fournir un mot de passe";
$lang['your_password_must_be_long']         = "Votre mot de passe doit comporter au moins 6 caractères";
$lang['please_enter_the_same_value_again']  = "Veuillez entrer la même valeur";
$lang['please_agree']                       = "Veuillez accepter notre politique.";
$lang['username_already_taken']             = "Nom d'utilisateur déjà utilisé.";
$lang['username_available']                 = "Nom d'utilisateur disponible.";
$lang['email_already_taken']                = "Adresse e-mail déjà prise.";
$lang['email_available']                    = "E-mail disponible.";
//$lang['account_creation_successful']        = "Account was created successfully.";//not found
$lang['account_creation_unsuccessful']      = "Une erreur est survenue lors de la création du compte.";/*ACA*/
$lang['deactivate_unsuccessful']            = "Échec de la désactivation.";
$lang['please_activate_account_by_email_click']    = "Veuillez activer votre compte en cliquant sur le lien que vous avez reçu par e-mail.";
$lang['deactivate_unsuccessful']            = "Échec de la désactivation. Veuillez contacter l'administrateur.";
$lang['already_activated']                  = "Votre e-mail est déjà activé"; // , please contact an admin.
$lang['error_login_mismatch']               = "Erreur discordance de connexion.";
$lang['error_login_maxtries']               = "Erreur dûe au nombre maximal de connexions.";/*ACA*/
$lang['invalid_secure_code']               = "Code de secureité invalide. Veuillez essayer à nouveau.";
$lang['google_signup_btn']                  = "S'inscrire avec un compte Google";
$lang['facebook_signup_btn']                = "S'inscrire avec un compte Facebook";
$lang['or']                                 = "Ou";
$lang['security_challenge']                = "Challenge de securité";
$lang['support_informations']               = "Informations de support";

$lang['Directory does not exists']          = "Répertoire n'existe pas";/*ACA*/
$lang['Directory is not writable']          = "Impossible d'écrire dans le répertoire";

//email
$lang['email_activation_subject']           = "Activation d'e-mail.";
$lang['email_welcome_subject']              = "Bienvenue.";
$lang['account_can_not_send_activate_email']    = "Impossible d'envoyer l'e-mail.";/*ACA*/

/*Retrieve Password*/
$lang['retrieve_password']                  = "Récupérer le mot de passe";
$lang['enter_your_email_to_receive_instructions']    = "Entrez votre email pour recevoir des instructions.";
$lang['send_me']                            = "Envoyez-moi";/*ACA*/

//message
$lang['error_email_empty']                  = "Veuillez entrer votre adresse e-mail.";
$lang['email_require']                      = "Veuillez entrer votre adresse e-mail.";
$lang['email_forgotten_password_subject']   = "Mot de passe objet de l'email oublié";
$lang['email_new_password_subject']         = "Nouveau mot de passe objet de l'email";
$lang['forgot_password_successful']         = "Mot de passe oublié envoi email réussi";
$lang['forgot_password_cant_send_mail']     = "Impossible d'envoyer l'e-mail";
$lang['forgot_password_unsuccessful']       = "Échec de mot de passe oublié";
$lang['code_wrong']                         = "Nous sommes désolés, votre lien est erroné. Veuillez essayer de nouveau.";
$lang['activate_unsuccessful']              = "Échec de l'activation";
$lang['activate_not_complete']              = "Activation non terminée";

//email
$lang['email_forgot_password_subject']      = "mot de passe e-mail oublié";
$lang['forgot_password_cant_send_mail']     = "Impossible d'envoyer l'e-mail.";

/*Reset Password*/
$lang['reset_password']                     = "Réinitialiser le mot de passe";
$lang['enter_your_new_password']            = "Entrez votre nouveau mot de passe";
$lang['save']                               = "Enregistrer";
$lang['save_continue']                      = "Enregistrer et continuer";

//message
$lang['invalid_user_id']  = "Identifiant d'utilisateur non valide.";
$lang['post_change_password_unsuccessful']  = "Veuillez entrer votre adresse e-mail.";
$lang['password_change_unsuccessful']       = "Mot de passe objet de l'email oublié";
$lang['password_change_successful']         = "Nouveau mot de passe objet de l'email";
$lang['reset_password_validation_error']    = "Réinitialiser le mot de passe erreur de validation";
$lang['forgotten_password_code_wrong']      = "Nous sommes désolés, le mot de passe est erroné. Veuillez essayer de nouveau.";
$lang['forgot_password_email_not_found']    = "Nous sommes désolés, nous n'avons pas trouvé votre e-mail.";
/*Profile*/
$lang['profile']                            = "Profil";
$lang['edit_profile']                       = "Modifier le profil";
$lang['general']                            = "Général";
$lang['basic_info']                         = "Informations de base";
$lang['username_from']                      = "Nom d'utilisateur de ";
$lang['note_can_not_change_email']          = "Notez que si vous utilisez une adresse e-mail, vous ne pouvez pas lea changer plus tard.";

//form
$lang['firstname']                          = "Prénom";
$lang['lastname']                           = "Nom";
$lang['new_password']                       = "Nouveau mot de passe";
$lang['new_passwordconf']                   = "Confirmer le mot de passe";
$lang['newsletter']                         = "Newsletter";
$lang['birthday']                           = "Anniversaire";
$lang['email']                              = "E-mail";
$lang['newsletter_detail']                  = "Rejoignez notre newsletter.";
$lang['current_password']                   = "Mot de passe actuel";
$lang['format_day_month_year']              = "Année-mois-jour";

//message
$lang['Invalid username/password']          = "Nom d'utilisateur/mot de passe invalide";
$lang['login_first']                        = "Veuillez vous identifier.";
$lang['update_unsuccessful']                = "Échec de la mise à jour.";
$lang['update_successful']                  = "Profil modifié avec succès.";
$lang['please_enter_your_current_password'] = "Veuillez saisir votre mot de passe actuel.";

/*Address*/
$lang['billing_information']                = "Informations de facturation";
$lang['edit_billing_information']           = "Modifier les informations de facturation";
$lang['address']                            = "Adresse";
$lang['information']                        = "Informations";

//form
$lang['company']                            = "Nom de l'entreprise";
$lang['complement']                         = "Étage/Bureau";
$lang['address1']                           = "Adresse";
$lang['address2']                           = "Adresse 2";
$lang['state_region']                       = "État/Région";
$lang['zipcode']                            = "Code postal";
$lang['city']                               = "Ville";
$lang['country']                            = "Pays";
$lang['account_save_unsuccessful']          = "Échec de la modification des informations de facturation.";
$lang['account_save_successful']            = "Informations de facturation modifiées avec succès.";
$lang['error_current_password_empty']       = "Vous devez entrer votre mot de passe actuel.";
$lang['error_email_empty']                  = "Veuillez remplir votre email avant d'ajouter un mot de passe.";
$lang['current_password_mismat']            = "Votre mot de passe actuel n'est pas correcte.";

/*authenticate*/
$lang['waiting_for_create_user']            = "Attente de la création de l'utilisateur";
$lang['sorry']                              = "Nous sommes désolés";
$lang['something_may_have_gone_wrong']      = "Quelque chose a pu mal se passer";
$lang['try_again']                          = "Veuillez essayer à nouveau.";

/*affiliation*/
$lang['Let Feed.biz to management your e-commerce business'] = 'Laissez Feed.biz gérer votre entreprise e-commerce';
$lang['affiliation'] = "Affiliation";
$lang['listReferrals'] = "Liste des références";
$lang['affiliateLists'] = "Listes d'affiliation";
$lang['statistic'] = "Statistique";
$lang['invite_friend'] = "Invitations";
$lang['aff_title'] = "Affiliation";
$lang['aff_inv_title'] = "Invitations";
$lang['aff_stat_title'] = "Statistiques";
$lang['aff_list_title'] = "Listes d'affiliation";
$lang['aff_inv_email_title'] = "Entrez les e-mails";
$lang['aff_inv_email_placeholder'] = "Ajouter e-mails";
$lang['aff_inv_btn_send'] = "Envoyer l'invitation";

$lang['aff_acc_title'] = "Activité d'affiliation";

$lang['aff_inv_topic_share'] = "Se connecter & partager";
$lang['aff_inv_topic_link'] = "Lien référent";
$lang['aff_inv_topic_email'] = "Inviter vos amis par e-mail";
$lang['aff_inv_descript_share'] = "Avec un ami sur Facebook, Twitter ou Google.";
$lang['aff_inv_descript_link'] = "Copier + coller votre lien personnel dans votre site web, blog, e-mail ou messagerie instantanée.";
$lang['aff_inv_descript_email'] = "Envoyer l'e-mail d'invitation à vos amis et votre famille.";
$lang['aff_inv_title_email'] = "L'e-mail de votre ami";

$lang['aff_stat_com'] = "Commissions";
$lang['aff_stat_num'] = "Nombre d'affiliations";

//forgot password page
$lang['Please provide a valid email.'] = "Veuillez fournir une adresse e-mail valide.";
$lang['Please specify a password.'] = "Veuillez spécifier un mot de passe.";
$lang['Enter your email address.'] = "Entez votre adresse e-mail.";
$lang['Enter your password.'] = "Tapez votre mot de passe.";

//affiliation
$lang['Join with us'] = "Rejoignez-nous";
$lang['ID'] = "Identifiant";
$lang['User name'] = "Nom d'utilisateur";
$lang['Email'] = "E-mail";
$lang['Status'] = "Statut";
$lang['Joined'] = "Inscrit";
$lang['Summary'] = "Résumé";
$lang['IP address'] = "Adresse IP";
$lang['Date'] = "Date";
$lang['Number of commission bills'] = "Nombre de projets de loi de la commission";
$lang['Sales commission'] = "Commission de vente";
$lang['Tier commission'] = "Commission de Tier";
$lang['Payout'] = "Paiement";


//general
$lang['Login'] = "Se connecter";
$lang['login'] = "connexion";
$lang['register'] = "s'inscrire";
$lang['Create Account'] = "Créer un compte";
$lang['billing_information_edit'] = "Modification d'informations de facturation";/*ACA*/
//$lang['Feed Mode'] = "Feed mode";//not found
$lang['Creation'] = "Création";
$lang['Create products'] = "Créer les produits";
$lang['General mode'] = "Mode Général";
$lang['Save'] = "Enregistrer";
$lang['Import history'] = "Importer l'historique";
//$lang['register'] = "Register";//not found
$lang['edit_user_validation_fname_label'] = "Modifier la validation de l'utilisateur prénom étiquette";/*ACA*/
$lang['edit_user_validation_lname_label'] = "Modifier la validation de l'utilisateur nom étiquette";
//$lang['login'] = "Login";//not found
//$lang['logout'] = "Logout";//not found
$lang['IP addres'] = "Adresse IP";//new
$lang[' Join with us '] = " Rejoignez-nous ";//new
$lang['authenticate_delete_user'] = "authentifier la suppression de l'utilisateur";//new
$lang['invalide_secure_code'] = "Code de sécurité non valide";//new
$lang['Create a new Account'] = "Créer un nouveau compte";//new
$lang['security_challlenge'] = "Défi de sécurité";//new
        
$lang['get_default_shop'] = "Obtenir la boutique par défaut";
$lang['Checking'] = "Vérification";
$lang['check_register_email'] = "Vérifiez l'e-mail enregistrée";
$lang['captcha'] = "captcha";
$lang['register_save'] = "Enregistrer l'inscription";
$lang['profile_edit'] = "Modification de profil";
$lang['clr_history'] = "clr_history";/*ACA*/
//$lang['activate'] = "Activate";//not found
$lang['email_friend'] = "E-mail ami";
$lang['No data to save'] = "Aucune donnée à enregistrer";
//$lang['remote'] = "Remote";//not found
//$lang['facebook'] = "Facebook";//not found
//$lang['int_callback'] = "Internation callback";//not found
//$lang['authenticate'] = "Authenticate";//not found
$lang['Log in your'] = "Accédez à votre";
$lang['Facebook Account'] = "Compte Facebook";
$lang['Google Account'] = "Compte Google";
$lang['Create a new account?'] = "Créer un nouveau compte ?";

//new word (did not verify)
$lang['Login to your Account'] = "Connectez-vous à votre compte";//new
$lang['Clicks'] = "Clics";//new
$lang['Last activity'] = "Dernière activité";//new
$lang['Share on your'] = "Partager sur votre";//new
$lang['logout'] = "se déconnecter";
$lang['forgotten_password'] = "Mot de passe oublié";
$lang['sent_invite_successful'] = "Invitation envoyée avec succès";
$lang[''] = "";
$lang['Please wait'] = "Please wait";//english
$lang['Twitter Account'] = "Twitter Account";
$lang['Show / Hide Columns'] = "Show / Hide Columns";

$lang['Image'] = "Image";
$lang['Add image'] = "Add image";
$lang['clr_original_redirect'] = "clr_original_redirect";
$lang['fields are required'] = 'fields are required';
$lang['Click here to activate account'] = "Click here to activate account";
//end

