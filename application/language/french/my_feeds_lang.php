<?php

$lang['page_title']                     = "Mes flux";
$lang['my_feeds']                       = "Mes flux";
$lang['main_configuration']             = "Configuration principale";
$lang['standard_feed_source']           = "Source : module Feed.biz module";
$lang['prestashop']                     = "Prestashop";
$lang['magento']                        = "Magento";
$lang['opencart']                       = "OpenCart";
$lang['os_commerce']                    = "OS Commerce";
$lang['custom_feed_source']             = "Source : Personnalisée";
$lang['gmerchant']                      = "Centre marchand de Google";
$lang['feed_source']                    = "Source du flux";
$lang['feed_url']                       = "URL du flux";
$lang['feed_type']                      = "Type de flux";
$lang['product']                        = "Produit"; 
$lang['offer']                          = "Offre";
$lang['offers']                         = "Offres";
$lang['language']                       = "Langue du flux";  
$lang['iso']                            = "ISO";  
$lang['iso_code']                       = "Code ISO";  
$lang['product_url']                    = "URL du produit";
$lang['reset']                          = "Réinitialiser";  
$lang['save']                           = "Enregistrer";  
$lang['products']                       = "Produits";  
$lang['shops']                          = "Boutiques";  
$lang['configuration']                  = "Configuration";  
$lang['data_source']                    = "Source de données";  
$lang['Shops']                          = "Boutiques";

//general page
$lang['Warning'] = "Avertissement";
$lang['You must set general mode.'] = "Vous devez régler le mode général.";
$lang['Mode'] = "Mode";
$lang['Authentification'] = "Authentification";
$lang['Beginner'] = "Débutant";
$lang['Choose Beginner mode'] = "Choisir le mode débutant";
$lang['Choose Expert mode'] = "Choisir le mode expert";
$lang['Default Mode'] = "Mode par défaut";
$lang['Choose Advanced mode'] = "Choisir le mode avancé";
$lang['Username'] = "Nom d'utilisateur";
$lang['Use token when you export'] = "Utiliser le jeton lorsque vous exportez";
$lang['Token'] = "Jeton";
$lang['Save & continue'] = "Enregistrer & continuer";
$lang['Token'] = "Jeton";
$lang['Errors'] = "Erreurs";
//shop page
$lang['Default Shop'] = "Boutique par défaut";
$lang['No shop in database!'] = "Aucune boutique dans la base de données !";
$lang['please_choose_feed_source'] = "Veuillez choisir une source de flux";
$lang['Do you want to change default shop'] = "Voulez-vous changer de boutique par défaut";
$lang['Cannot change shop offer'] = "Impossible de modifier les offres de la boutique";
$lang['Cannot change shop product'] = "Impossible de modifier les produits de la boutique";

//data source
$lang['xml'] = "xml";
$lang['csv'] = "csv";
$lang['URL'] = "URL";
$lang['Google merchant - Basic settings'] = "Marchand Google - Paramètres de base";
$lang['Default'] = "Par défaut";
$lang['Currency code'] = "Code de la devise";
$lang['Import mode'] = "Mode d'importation";
$lang['Auto importing'] = "Importation automatique";
$lang['Recommend'] = "Recommandé";
$lang['Our system will offen update your products.'] = "Notre système va souvent mettre à jour vos produits.";
$lang['Manual import'] = "Importation manuelle";
$lang['Your products will be updated after clicking the save button.'] = "Vos produits seront mis à jour après avoir cliqué sur le bouton Enregistrer.";
$lang['Are you sure to change mode'] = "Êtes-vous sûr de changer de mode ?";
$lang['Please choose mode'] = "Veuillez choisir un mode";
$lang['All importing are completed.'] = "Toutes les importations ont été réalisées.";
$lang['Feed data'] = "Données de flux";
$lang['Import history'] = "Importer l'historique";
$lang['time'] = "temps";
$lang['Feed'] = "Flux";
$lang['waiting'] = "attente";
/*status*/
$lang['my_feeds']                       = "Mes flux";
$lang['status']                         = "Statut";
$lang['shop_name']                      = "Nom de la boutique";
$lang['running_by']                     = "Géré par";
$lang['system']                         = "Système";
$lang['user']                           = "Utilisateur";
$lang['task']                           = "Tâche";

/*Parameter */
// Category
$lang['parameters']                     = "Paramètres";  
$lang['category']                       = "Catégorie";  
$lang['please_config']                  = "Veuillez configurer votre source d'alimentation.";  
$lang['choose_categories']              = "Choisir les catégories."; 
$lang['expand_all']                     = "Développer tout";
$lang['collapse_all']                   = "Réduire tout";
// Carriers
$lang['carriers']                       = "Transporteurs";  
$lang['choose_carriers']                = "Choisir les transporteurs."; 
$lang['check_all']                      = "Cocher tout";
$lang['uncheck_all']                    = "Décocher tout";

//Mapping
$lang['mapping']                        = "Mapping";  
$lang['attributes']                     = "Attributs"; 
$lang['choose_a_attribute']             = "Choisir un attribut"; 
//characteristics
$lang['characteristics']                = "Caractéristiques";
$lang['enter_a_characteristics']        = "Entrez une caractéristique"; 
//manufacturers
$lang['manufacturers']                  = "Fabricants"; 
$lang['choose_a_manufacturer']          = "Choisir un fabricant"; 
$lang['Enter Appropriate manufacture name']          = "Entrez le nom de fabricant approprié";

/*Filter*/
$lang['filters']                        = "Filtres"; 
$lang['suppliers']                      = "Fournisseurs"; 
$lang['manufacturers']                  = "Fabricants"; 
$lang['Manufacturers']                  = "Fabricants";
$lang['excluded_manufacturers']         = "Fabricants exclus"; 
$lang['included_manufacturers']         = "Fabricants inclus"; 
$lang['excluded_suppliers']             = "Fournisseurs exclus"; 
$lang['included_suppliers']             = "Fournisseurs inclus"; 

/*Price rules*/
$lang['price_rules']                    = "Règles de prix.";

//message
$lang['form_validation_false']          = "form_validation_false";
$lang['validation_upload_label']        = "validation_upload_label";
$lang['upload_successful']              = "Téléchargement réussi";
$lang['importing']                      = "Veuillez patienter pendant l'importation ";
$lang['from']                           = "De ";
$lang['duplicate_domain_with_other_user']              = "Votre domaine a été utilisé par un autre utilisateur.";
$lang['save_unsuccessful']              = "Échec de l'enregistrement.";
$lang['save_successful']                = "Enregistrement réussi.";	
$lang['main_configuration_save']        = "";
$lang['getSelectedCategories']          = "";
$lang['filters_save']                   = "";
$lang['attributes_save']                = "";
$lang['characteristics_save']           = "";
$lang['manufacturers_save']             = "";
$lang['category_save']                  = "";
$lang['Change default shop successful'] = "Boutique par défaut modifiée avec succès";
$lang['incorrect_user']                 = "Utilisateur incorrect";
$lang['please_wait_for_verifying']      = "Veuillez patienter pendant la vérification";
$lang['verify']                         = "Vérifier";
$lang['please_check_url_and_verify_again'] = "Veuillez vérifier l'URL et vérifier à nouveau";
$lang['can_not_connect_server']         = "Impossible de se connecter à votre serveur, essayez de nouveau";
$lang['please_paste_your_uri_connector']= "Coller l'URL fournie par votre module Feed.biz ici";
$lang['save_feed_data']                 = "Enregistrer";
$lang['URL provided by your Feed.biz module'] = "L'URL fournie par votre module Feed.biz";
$lang['Verified'] = "Vérifié";
$lang['Verify'] = "Vérifier";
$lang['Import offers now']=  "Importer les offres maintenant";
$lang['Import products now']=  "Importer ls produits maintenant";
$lang['Import NOW']=  "Importer maintenant";

/*tasks*/
$lang['your_website_has_not_verified']  ="Votre site web n'a pas été vérifié";
$lang['you_have_just_updated_your_feed_please_wait_for_a_few_minutes']  ="Vous venez de mettre à jour vos flux. Veuillez attendre quelques minutes..";
$lang['you_have_update_too_often_wait_for_xxx_mins']  ="Vous avez mis à jour trop souvent. Veuillez attendre xxx minutes.";
$lang['you_have_just_update_feed_please_wait_for_xxx_mins']  ="Vous venez de mettre à jour vos flux. Veuillez attendre xxx minutes.";
$lang['you_have_just_update_product_please_wait_for_xxx_mins']  ="Vous venez de mettre à jour vos produits. Veuillez attendre xxx minutes.";
$lang['you_have_just_update_offer_please_wait_for_xxx_mins']  ="Vous venez de mettre à jour vos offres. Veuillez attendre xxx minutes.";
$lang['You have just update your %s. Please wait for %s mins.']  ="Vous venez de mettre à jour votre %s. Veuillez attendre %s minutes.";
$lang['You have update to often. Please wait for %s mins.']  ="Vous avez mis à jour trop souvent. Veuillez attendre %s minutes.";
$lang['your_offer_are_importing'] = "Vos offres sont importées";//not found in using
$lang['Access Denied.'] = "Accès Refusé.";//new
$lang['Not found your shop.'] = "Votre boutique est introuvable.";//new

//statistic product
$lang['Reference'] = "Référence";
$lang['Category'] = "Catégorie";
$lang['Quantity'] = "Quantité";
$lang['Price'] = "Prix";
$lang['price'] = "Prix";
$lang['No data source.'] = "Aucune source de données.";
$lang['statistics'] = "Statistiques";
//log batches page
$lang['Last Import Log'] = "Dernier journal d'importation";
$lang['dashboard']                          = "Tableau de bord";
$lang['page_description']                   = "";
$lang['page_keyword']                       = "";
$lang['welcome']                            = "Bienvenue";                 
$lang['validation'] = 'Validation';
$lang['Log'] = 'Journal';
$lang['Last import log'] = 'Dernier journal d\'importation';
$lang['Batch ID'] = 'Identifiant Lot';
$lang['Source'] = 'Source';
$lang['Shop'] = 'Boutique';
$lang['Type'] = 'Type';
$lang['Total'] = 'Total';
$lang['No. Successful'] = 'No. Réussi';
$lang['No. Warning'] = 'No. Avertissement';
$lang['No. Error'] = 'No. Erreur';
$lang['Import Date'] = 'Date d\'importation';

$lang['messages'] = "messages";
$lang['Search batch ID'] = "Rechercher identifiant lot";//not found
$lang['Search Product ID'] = "Rechercher identifiant produit";//not found


$lang['All'] = "Tout";
$lang['Error'] = "Erreur";
$lang['Search message'] = "Rechercher le message";//not found in using
$lang["Search date"] = "Rechercher la date";//not found using
$lang['Warnings'] = "Avertissements";
//parameter
$lang['Please choose default shop.'] = "Veuillez choisir la boutique par défaut.";
$lang['Warning!'] = "Avertissement !";
$lang['Add a price to the list'] = "Ajouter un prix à la liste";
$lang['Price(s)'] = "Prix";
$lang['Price name'] = "Nom du prix";
$lang['Price modifier'] = "Modificateur de prix";
$lang['Percentage'] = "Pourcentage";
$lang['Allow only digits.'] = "Autoriser uniquement les chiffres.";
$lang['Rounding'] = "Arrondi";
$lang['One digit'] = "Un chiffre";
$lang['Two digit']= "Deux chiffres";
$lang['None']= "Aucun";
$lang['Value']= "Valeur";
$lang['Use a familiar name to remember it.'] = "Utiliser un nom familier pour vous en rappeler.";
$lang['Formula to be applied on all the exported products prices (multiply, divide, addition, subtraction, percentages).']= "Formule à appliquer sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).";
$lang['Apply a specific price formula to selected categories which will override the main setting.']= "Appliquer une formule de prix spécifique aux catégories sélectionnées qui remplacera le paramètre principal.";
$lang['Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required']= "Mode d'arrondi à appliquer au prix (exemple: prix sera égal à 10,17 ou 10,20)";
$lang['Do you want to delete']= "Voulez-vous supprimer";
$lang['Delete successful']= "Suppression réussie";
$lang['Deleted']= "Supprimé";
$lang['Error']= "Erreur";
$lang['Can not delete']= "Impossible de supprimer";

$lang['Rules'] = "Règles";
$lang['Add a rule to the list'] = "Ajouter une règle à la liste";

$lang['Rule name'] = "Nom de la règle";
$lang['Add rule name first'] = "Ajouter le nom de règle en premier";
$lang['Add item'] = "Ajouter l'article";
$lang['Item(s)'] = "Article(s)";
$lang['Manufacturer'] = "Fabricant";
$lang['Supplier'] = "Fournisseur";
$lang['Price range'] = "Plage de prix";
$lang['Action'] = "Action";
$lang['To'] = "À";
$lang['Summary'] = "Résumé";
$lang['Manufacturer'] = "Fabricant";
$lang['Add rule name first'] = "Ajouter le nom de la règle en premier";
$lang['Drop'] = "Déposer"; 
$lang['Messages log'] = "Journal de messages";//new
$lang['Messages product'] = "Messages Produits";//new /*AVA*/
$lang['Messages offer'] = "Messages Offres";//new /*AVA*/

$lang['Are you sure to delete this price range'] = "Êtes-vous sûr de vouloir supprimer cette plage de prix";
$lang['Price range error'] = "Erreur de plage de prix";
$lang['Successfully'] = "Avec Succès";
$lang['Delete was successful'] = "Suppression avec succès.";
$lang['Delete was unsuccessful'] = "Échec de la suppression";/*ACA*/
$lang['Are you sure to delete rule'] = "Êtes-vous sûr de vouloir supprimer la règle";
$lang['Are you sure to delete rule item'] = "Êtes-vous sûr de vouloir supprimer l'article de la règle";/*ACA*/
$lang['Records'] = "Enregistrements";
$lang['Your process are running. Please wait.'] = "Votre processus est en cours d'exécution. Veuillez attendre.";
$lang['log'] = "journal";
$lang['batches'] = "lots";
$lang['Product ID'] = "Identifiant Produit";
$lang['Default profiles mapping'] = "Mapping par défaut de profils";
$lang['Carriers_save'] = "Enregistrer les transporteurs";//not found in using
$lang['log_download'] = "log_download";
$lang['Condition'] = "Condition";
$lang['Conditions'] = "Conditions";
$lang['reset_condition'] = "reset_condition";
$lang['next'] = "Suivant";
$lang['condition_save'] = "condition_save";
$lang['condition'] = "Condition"; 
$lang['Marketplace condition'] = "Condition de place de marché";//not found in using
$lang['Your conditions'] = "Vos conditions";
$lang['profiles'] = "Profils";
$lang[' Percentage '] = "Pourcentage ";
$lang[' Do you want to delete '] = " Voulez-vous supprimer ";
$lang[' Deleted '] = " Supprimé ";
$lang[' Error '] = " Erreur ";
$lang[' Can not delete '] = " Impossible de supprimer ";
$lang['System can not download data. Please try again'] = "Le système ne peut pas télécharger les données. Veuillez essayer à nouveau";/*ACA*/
$lang['System can not download some part of your data.'] = "Le système ne peut pas télécharger une partie de vos données.";

$lang['general_save'] = "Général enregistrer";
$lang['data_source_save'] = "Enregistrer source de données";
$lang['price_save'] = "Enregistrer Prix";
$lang['iframe_popup_conf'] = "iframe popup conf";
$lang['verify_feed'] = "Verifier le flux"; 
$lang['your_feed_are_importing'] = "Votre flux est en cours d'importation."; 
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";
$lang['Expert Mode will be charged xx per month'] = "Le mode expert sera facturé %s par mois";

$lang['Please choose category in'] = 'Veuillez choisir la catégorie dans';
$lang['My feeds > Parameters > Offers > Category'] = 'Mes flux > Paramètres > Offres > Catégorie';
$lang['My feeds > Parameters > Profiles > Category'] = "Mes flux > Paramètres > Profils > Catégorie";
$lang['my_shop'] = "Ma boutique";
$lang['connect'] = "Connecter";
$lang['Import history'] = "Importer l'historique";
$lang['import_history'] = "Importer l'historique";
$lang['Import my catalog'] = "Importer mon catalogue";
$lang['connect_save'] = "Enregitrement de la connexion";
$lang['Mode'] = "Mode";
$lang['Information'] = "Information";
$lang[' Percentage '] = " Pourcentage ";
$lang[' Delete successfully '] = " Suppression réussie ";
$lang[' Do you want to delete '] = " Voulez-vous supprimer ";

$lang[' Error '] = " Erreur ";
$lang[' Can not delete '] = " Impossible de supprimer ";
$lang['Mode'] = "Mode";

$lang['Our system will offen update your products.'] = "Notre système va souvent mettre à jour vos produits.";//new
$lang['Security token used between your Shop and Feed.biz'] = "Jeton de sécurité utilisé entre votre boutique et Feed.biz";//new

//new word (did not verify) (my shop page)
$lang['All categories'] = "Toutes les catégories";//new
$lang['All conditions'] = "Toutes les conditions";//new
$lang['my_products'] = "Mes produits";
$lang['My products'] = "Mes produits";//not found in using
$lang['Filter by category'] = "Filtrer par catégorie";
$lang['Filter by conditions'] = "Filtrer par condition";
$lang['All categories'] = "Toutes les catégories";
$lang['All conditions'] = "Toutes les conditions";
$lang['Product name > SKU'] = "Nom de produit > SKU";
$lang['> Listing'] = "> Liste";
$lang['Variants'] = "Variants";
$lang['My_product_data'] = "Mes données de produit";//not found in using
$lang['getJsonCategory'] = "Obtenir la catégorie Json";
$lang['Module'] = "Module";//new
$lang['Extension'] = "Extension";//new
$lang['Prestashop'] = "Prestashop";//new
$lang['Download'] = "Télécharger";//new
$lang['Opencart'] = "Opencart";//new
$lang['Shopify'] = "Shopify";//new
$lang['Magento'] = "Magento";//new
$lang['Install'] = "Installer";//new
$lang['Woo Commerce'] = "Woo Commerce";//new
$lang['Feed Data'] = "Données du flux";
$lang['Auto Importing'] = "Importation Automatique";
$lang['Manual Importing'] = "Importation Manuelle";
$lang['my_product_data'] = "my_product_data";
$lang['carriers_save'] = "carriers_save";
$lang['Rule name'] = "Nom de la règle";
$lang['Are you sure to delete this price range'] = "Êtes-vous sûr de vouloir supprimer cette plage de prix";
$lang['Add rule name first'] = "Ajouter le nom de la règle en premier";
$lang['Add profile name first'] = "Ajouter le nom du profil en premier"; 
$lang['Delete successful'] = "Suppression réussie";
$lang['Deleted'] = "Supprimé"; 
$lang['Can not delete'] = "Suppression impossible";
$lang['rofiles Mapping'] = "Mapping de profils";
$lang['To'] = "À"; 
$lang['rules_item_price_range_delete'] = "Suppression règles article plage de prix"; 
$lang['rules_delete'] = "Suppresion de règles"; 
$lang['rules_delete'] = "Suppresion de règles"; 
$lang['Profiles Mapping'] = "Mapping de profils";  
$lang['Rule name'] = "Nom de la règle";
$lang['Add rule name first'] = "Ajouter le nom de la règle en premier";
$lang['To'] = "À";
 
$lang['Default profiles mapping'] = "Mapping de profils par défaut";
$lang['Access Denied.'] = "Accès Refusé.";
$lang['Empty Data'] = "Données vide";
$lang['XML DOM Parse Error'] = "Analyser erreurs XML DOM";
$lang['Setting Feed Error'] = "Définition erreur de flux";
$lang['General mode'] = "Mode Général";

$lang['empty_connector_data'] = "Empty data from this connector. Please check URL and try again.";//english
$lang['data_connect'] = "Data connect";
$lang['data_configuration'] = "Data configuration";
$lnag['Show / Hide Columns'] = "Show / Hide Columns";
$lang['running_by'] = "Run by";
$lang['messages_page'] = "messages";
$lang['Next'] = "Next";

//new
$lang['Run by'] = "Run by";
$lang['import_history_page'] = "import history page"; 
$lang['+ Add Rule Items(s)'] = "+ Add Rule Items(s)";

/*product_options*/ //22-05-2015
$lang['offers_options'] = 'Offers Options'; 
$lang['General mode'] = "General mode";
$lang['Feed.biz will periodically update your products'] = "Feed.biz will periodically update your products";
//end