﻿<?php

$lang['page_title']                     = "eBay";
$lang['ebay']                           = "eBay";
$lang['host_url']                       = "FeedBiz";

$lang['product_key']                    = "Clés de production keys (mode App)";
$lang['product_key_mode']               = "Clés de production | Mode";
$lang['sanbox_key']                     = "Clés de Sandbox (mode App)";
$lang['configuration']                  = "Configuration";

$lang['sub_title']                      = "authentification eBay & API de sécurité";/*ACA*/
$lang['sub_setting']                    = "paramètres eBay";
$lang['reset']                          = "Réinitialiser";  
$lang['back']                           = "Retour";  
$lang['prev']                           = "Précédent";  
$lang['save']                           = "Enregistrer et continuer";
$lang['save_only']                      = "Enregistrer";
$lang['next']                           = "Suivant";
$lang['check_all']                      = "Cocher tout";
$lang['uncheck_all']                    = "Décocher tout";
$lang['collapse_all']                   = "Réduire tout";
$lang['expand_all']                     = "Développer tout";
$lang['submit']                         = "Soumettre";
$lang['new_token']                      = "Générer un nouveau jeton";
$lang['load_catagories']                = "Charger les catégories"; 
$lang['hack']                           = "Vous essayez de pirater le système."; 
$lang['new']                            = "Nouveau"; 
$lang['good']                           = "Bon"; 
$lang['used']                           = "Occasion"; 
$lang['refurbished']                    = "Remis à neuf"; 
$lang['acceptable']                     = "Acceptable"; 
$lang['to']                             = "à"; 
$lang['show_all']                       = "Afficher tout"; 
$lang['yes_to_all']                     = "Oui à tous (S'il y a un clic ce popup sera ignoré.) ";
$lang['confirm_synchronize']            = "Confirmer"; 
$lang['get_start']                      = "Commencer";
$lang['let_get_start']                  = "Commençons"; 
$lang['generate_token']                 = "Générer le jeton eBay"; 

$lang['user_id']                        = "UserID eBay";
$lang['user_id:']                       = "UserID eBay:"; 
$lang['postcode']                       = "Code Postal"; 
$lang['postcode:']                      = "Code Postal :"; 
$lang['paypal_email']                   = "email Paypal"; 
$lang['paypal_email:']                  = "email Paypal :"; 
$lang['address']                        = "Adresse"; 
$lang['city:']                          = "Ville:"; 
$lang['visa_mastercard']                = "Visa / MasterCard"; 
$lang['discover']                       = "Discover"; 
$lang['american_express']               = "American Express"; 
$lang['money_order']                    = "Mandat / Chèque de caisse"; 
$lang['personal_check']                 = "Chèque personnel"; 
$lang['pay_on_pickup']                  = "Payer au ramassage";
$lang['other_payment']                  = "Autres paiements"; 
$lang['dispatch_time']                  = "Temps d'expédition"; 
$lang['other_option']                   = "Autre option"; 
$lang['other_option:']                  = "Autre option :"; 
$lang['option_add_product:']            = "Exporter le produit :"; 
$lang['option_orders_product:']         = "Importer les commandes"; 
$lang['merchant_credit_cards']          = "Paiement Accepté"; 
$lang['category']                       = "Catégories";
$lang['processed:']                     = "Traité:";
$lang['product(s)']                     = "Produit(s)";
$lang['success:']                       = "Succès :";
$lang['warning:']                       = "Avertissement :";
$lang['error:']                         = "Erreur :";
$lang['see_more_detail:']               = "Voir plus de détails:";
$lang['click_this']                     = "Cliquez sur";
$lang['gtc']                            = "Bon jusqu'à être Annulé (renouvelé pour frais supplémentaires tous les 30 jours)";
$lang['not_have_store']                 = "Vous n'avez pas mis en place toutes les catégories de boutiques sur eBay.";
$lang['payment_instructions']           = "Instructions de paiement";
$lang['product_item']                   = "Identifiant";
$lang['product_title']                  = "Titre";
$lang['db_product_title']               = "Titre de base de données";
$lang['product_manufacturer']           = "Fabricant";
$lang['product_supplier']               = "Fournisseur";
$lang['product_quantity']               = "Quantité";
$lang['product_price']                  = "Prix";
$lang['product_reference']              = "Référence";
$lang['product_sku']                    = "Sku";
$lang['product_ean13']                  = "Ean13";
$lang['product_upc']                    = "Upc";
$lang['product_image']                  = "Image";
$lang['product_content']                = "Contenu";
$lang['product_information']            = "Informations";
$lang['product_detail']                 = "Détails";
$lang['active']                         = "Actif";
$lang['unactive']                       = "Inactif";
$lang['category_name']                  = "Catégorie";
$lang['condition_name']                 = "Condition";
$lang['choose_mapping']                 = "Choisir le mapping";
$lang['choose_mapping_against']         = "Choisir le mapping par rapport à";
$lang['choose_profile']                 = "Choisir le Profil";
$lang['clear']                          = "Effacer";
$lang['replace_profile']                = "Remplacer mapper le nom du profil";
$lang['confirm_auto_map']               = "Confirmer la correspondance automatique";
$lang['confirm_all']                    = "cliquer pour confirmer remplacer tout (si vous cliquez, cela confirmera Remplacer tout et passera ce popup.) ";/*AVA*/
$lang['skip_all']                       = "cliquer pour ignorer remplacer tout (si vous cliquez, cela ne confirmera pas Remplacer tout et passera ce popup.) ";
$lang['order_status']                   = "Statut de la commande";
$lang['variation_warning_title']        = "la page que vous cherchez ne peut être affichée.";
$lang['variation_warning']              = "Un avertissement peut être dû à diverses raisons, telles que:";
$lang['categories_no_attributes']       = "Les catégories demandées n'existent pas.";
$lang['no_attributes']                  = "Les catégories demandées n'ont pas d'attribut.";
$lang['id_profile_not_found']           = "L'identifiant Profil est introuvable.";
$lang['mapping_not_found']              = "La catégorie Mapping est introuvable.";
$lang['mapping_invalid']                = "La catégorie Mapping est non valide.";




$lang['user_not_available']             = "Ce compte eBay est déjà utilisé."; 
$lang['auth_is_processing']             = "L'authentification est en cours de traitement...."; 



$lang['update_auth_security_success']               = "Mise à jour auth de sécurité réussie."; 												
$lang['update_auth_security_fail']                  = "Données incorrectes : veuillez essayer de nouveau."; 

$lang['update_integration_authorization_success']   = "Génération d'un jeton eBay réussie.";
$lang['update_integration_authorization_fail']      = "Échec de la génération d'un jeton eBay : veuillez essayer de nouveau."; 

$lang['update_auth_setting_success']                = "Mise à jour de la configuration réussie."; 											
$lang['update_auth_setting_fail']                   = "Échec de la mise à jour de la configuration : veuillez essayer de nouveau."; 														
$lang['update_univers_success']                     = "Mise à jour des catégories sélectionnées réussie.";
													
$lang['update_univers_fail']                        = "Échec de la mise à jour des catégories sélectionnées : veuillez essayer de nouveau.";
														 
$lang['update_mapping_categories_success']          = "Mise à jour des catégories réussie.";
														
$lang['update_mapping_categories_fail']             = "Échec de la mise à jour des catégories : veuillez essayer de nouveau."; 
$lang['update_mapping_templates_success']           = "Mise à jour des templates réussie.";
														
$lang['update_mapping_templates_fail']              = "Échec de la mise à jour des templates : veuillez essayer de nouveau."; /*ACA AVA*/
$lang['update_templates_success']                   = "Mise à jour des templates réussie."; 
											
$lang['update_templates_fail']                      = "Échec de la mise à jour des templates : veuillez essayer de nouveau."; 
$lang['update_mapping_conditions_success']          = "Mise à jour des conditions réussie."; 
														
$lang['update_mapping_conditions_fail']             = "Échec de la mise à jour des conditions : veuillez essayer de nouveau."; 
$lang['update_mapping_carriers_success']            = "Mise à jour de l'expédition réussie."; 
														 
$lang['update_mapping_carriers_fail']               = "Échec de la mise à jour de l'expédition : veuillez essayer de nouveau."; 
$lang['update_tax_success']                         = "Mise à jour de la taxe réussie."; 
														 
$lang['update_tax_fail']                            = "Échec de la mise à jour de la taxe : veuillez essayer de nouveau."; 
$lang['update_price_modifier_success']              = "Mise à jour du prix réussie."; 
														
$lang['update_price_modifier_fail']                 = "Échec de la mise à jour du prix : veuillez essayer de nouveau."; 
$lang['update_actions_delete_success']              = "Suppression du produit réussie."; 
														
$lang['update_actions_delete_fail']                 = "Échec de la Suppression du produit : veuillez essayer de nouveau."; 
$lang['update_actions_delete_success']              = "Suppression du produit réussie."; 
														
$lang['update_actions_delete_fail']                 = "Échec de la Suppression du produit : veuillez essayer de nouveau."; 
$lang['update_actions_export_success']              = "Exportation du produit réussie."; 
														
$lang['update_actions_send_mail']                   = "Envoyer le produit à eBay et en attente de la réponse par e-mail une fois terminée."; 
$lang['update_actions_export_fail']                 = "Échec de l'exportation du produit : veuillez essayer de nouveau."; 
$lang['update_actions_orders_success']              = "Obtention des commandes réussie."; 
													 
$lang['update_actions_orders_fail']                 = "Échec de l'obtention des commandes : veuillez essayer de nouveau."; 
$lang['update_actions_error']                       = "Échec de la connexion ! veuillez essayer à nouveau."; 
$lang['update_actions_error_msg']                   = "Une erreur est survenue.";//"There has error something that is found, especially an unexpectedly discovery."; 
$lang['update_wizard_choose_success']               = "Mise à jour du site de pays réussie.";  
$lang['update_wizard_choose_fail']                  = "Échec de la mise à jour du site de pays : veuillez essayer de nouveau."; 
$lang['update_wizard_univers_success']              = "Mise à jour des catégories séléctionnées réussie."; 
														
$lang['update_wizard_univers_fail']                 = "Échec de la mise à jour des catégories séléctionnées : veuillez essayer de nouveau."; 
$lang['update_wizard_categories_success']           = "Mise à jour des catégories séléctionnées réussie.";
														
$lang['update_wizard_categories_fail']              = "Échec de la mise à jour des catégories : veuillez essayer de nouveau."; 
$lang['update_wizard_tax_success']                  = "Mise à jour de la taxe réussie.";
														
$lang['update_wizard_tax_fail']                     = "Échec de la mise à jour de la taxe : veuillez essayer de nouveau."; 
$lang['update_wizard_authorization_success']        = "Mise à jour de l'auth sécurité réussie.";
														
$lang['update_wizard_authorization_fail']           = "Données incorrectes: veuillez essayer de nouveau."; 
$lang['update_wizard_shippings_success']            = "Mise à jour de l'auth de sécurité réussie.";
														

$lang['update_wizard_shippings_fail']               = "Données incorrectes: veuillez essayer de nouveau."; 
$lang['update_wizard_exports_export_success']       = "Exportation du produit réussie.";
														 
$lang['update_wizard_exports_export_fail']          = "Échec de l\'exportation du produit : veuillez essayer de nouveau."; 
$lang['update_wizard_exports_success']              = "Mise à jour de l'exportation du produit réussie.";
														 
$lang['update_wizard_exports_fail']                 = "Échec de l\'exportation du produit : veuillez essayer de nouveau."; 
$lang['update_wizard_exports_error']                = "Échec de la connexion ! veuillez essayer à nouveau."; 
$lang['update_wizard_exports_error_msg']            = "Une erreur est survenue.";//"There has error something that is found, especially an unexpectedly discovery."; 
$lang['update_wizard_exports_send_mail']            = "Envoi du produit à eBay et attente de la réponse par e-mail une fois terminée.";
$lang['update_templates_preview_success']           = "Aperçu de template réussi.";
														 
$lang['update_templates_preview_fail']              = "Impossible de prévisualiser le template: veuillez essayer de nouveau.";
$lang['update_templates_delete_success']            = "Suppression de template réussie."; 
														 
$lang['update_templates_delete_fail']               = "Échec de la Suppression du template : veuillez essayer de nouveau."; 
$lang['update_templates_add_success']               = "Ajout de template réussi."; 
														
$lang['update_templates_add_fail']                  = "Échec de l'ajout du template : veuillez essayer de nouveau."; 
$lang['update_advance_categories_main_status_success'] = "Mise à jour du statut du profil réussie."; 
$lang['update_advance_categories_main_status_fail'] = "Échec de la mise à jour du statut du profil : veuillez essayer de nouveau."; 
$lang['update_advance_categories_main_success']     = "Mise à jour du profil réussie."; 
$lang['update_advance_categories_main_fail']        = "Échec de la mise à jour du profil : veuillez essayer de nouveau."; 
$lang['update_advance_categories_main_delete_success'] = "Suppression du profil réussie."; 
$lang['update_advance_categories_main_delete_fail'] = "Échec de la suppression du profil : veuillez essayer de nouveau."; 
$lang['update_advance_categories_main_add_success'] = "Création du profil réussie."; 
$lang['update_advance_categories_main_add_fail']    = "Échec de la création du profil : veuillez essayer de nouveau."; 
$lang['update_advance_main_success']                = "Mise à jour de la règle du profil réussie."; 
$lang['update_advance_main_fail']                   = "Échec de la règle du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_configuration_success']   = "Mise à jour du détail du profil réussie."; 
$lang['update_advance_profile_configuration_fail']  = "Échec de la mise à jour du détail du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_payment_success']     = "Mise à jour du paiement du profil réussie."; 
$lang['update_advance_profile_payment_fail']        = "Échec de la mise à jour du paiement du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_return_success']      = "Mise à jour du retour du profil réussie."; 
$lang['update_advance_profile_return_fail']         = "Échec de la mise à jour du retour du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_description_success'] = "Mise à jour de la description du profil réussie."; 
$lang['update_advance_profile_description_fail']    = "Échec de la mise à jour de la description du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_condition_success']   = "Mise à jour de la condition du profil réussie."; 
$lang['update_advance_profile_condition_fail']      = "Échec de la mise à jour de la condition du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_shipping_success']    = "Mise à jour de l'expédition du profil réussie."; 
$lang['update_advance_profile_shipping_fail']       = "Échec de la mise à jour de l'expédition du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_variation_success']   = "Mise à jour de la variation du profil réussie."; 
$lang['update_advance_profile_variation_fail']      = "Échec de la mise à jour de la variation du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_specific_success']    = "Mise à jour de la spécificité de l'article du profil réussie."; 
$lang['update_advance_profile_specific_fail']       = "Échec de la mise à jour de la spécificité de l'article du profil : veuillez essayer de nouveau."; 
$lang['update_advance_profile_price_modifier_success']    = "Mise à jour du prix réussie."; 
$lang['update_advance_profile_price_modifier_fail']       = "Échec de la mise à jour du prix : veuillez essayer de nouveau."; 
$lang['update_advance_profile_categories_mapping_success']    = "Mise à jour du mapping du profil réussie."; 
$lang['update_advance_profile_categories_mapping_fail']       = "Échec de la mise à jour du mapping du profil : veuillez essayer de nouveau."; 
$lang['update_synchronization_success']             = "Obtention de l'inventaire réussie."; 
$lang['update_synchronization_fail']                = "Échec de l'obtention de l'inventaire : veuillez essayer de nouveau."; 
$lang['update_synchronization_matching_success']    = "Mise à jour de la synchronisation réussie."; 
$lang['update_synchronization_matching_fail']       = "Échec de la mise à jour de la synchronisation : veuillez essayer de nouveau."; 




$lang['validation_error_required']                  = "Veuillez vérifier vos informations saisies."; 
$lang['validation_required_form_user_id']           = "Veuillez entrer un nom d'utilisateur."; 
$lang['validation_minlength_form_user_id']          = "Votre nom d'utilisateur doit être composé d'au moins 2 caractères."; 
$lang['validation_required_form_postal_code']       = "Veuillez entrer votre code postal."; 
$lang['validation_regex_form_postal_code']          = "Veuillez saisir un code postal {1} valide{BR}Exemple : {2}"; 
$lang['validation_postal_valid_form_postal_code']   = "Veuillez entrer un code postal valide{BR}Exemple : {2}"; 
$lang['validation_select_required']                 = "Veuillez sélectionner l'article."; 
$lang['validation_range_required']                  = "Plage de prix non valide, ce prix doit être supérieur à {1}."; 
$lang['validation_between_required']                = "Plage de prix non valide, ce prix ne doit pas être entre {1} et {2}.";
$lang['validation_range_weight_required']           = "Plage de poids non valide, ce poids doit être supérieur à {1}."; 
$lang['validation_between_weight_required']         = "Plage de poids non valide, ce poids ne doit être entre {1} et {2}.";
$lang['validation_carrier_rule_required']           = "Veuillez affecter une règle de transporteur."; 
$lang['validation_out_of_stock_min']                = "Veuillez entrer un numéro supérieur ou égal à 0 et inférieur ou égal à 1000 !";
$lang['validation_maximum_quantity_max']            = "Veuillez entrer un numéro supérieur ou égal à 1 et inférieur ou égal à 1000 !"; 
$lang['validation_variation_type']                  = "Nom de type de variations dupliqué, veuillez sélectionner un autre nom de type!"; 
$lang['validation_variation_include']               = "Nom de type de variations inclu non valide, veuillez filtrer les noms de type de variations à exclure !"; /*ACA*/
$lang['validation_specific_include']                = "Nom des spécificités de l'article inclu non valide, veuillez filtrer le nom des spécificités de l'article à exclure !"; /*ACA*/
$lang['validation_profile_name_checked']            = "Nom de profil non valide, veuillez remplir un autre nom de profil!"; 



$lang['header_auth_security']           = "Intégration compte eBay";
$lang['title_auth_security']            = "Lier à votre compte eBay";
$lang['header_integration']             = "Intégration compte eBay";
$lang['title_integration']              = "Assistant d'intégration eBay";
$lang['header_integration_authorization']   = "Générer jeton eBay";
$lang['title_integration_authorization']    = "Assistant d'intégration eBay";
$lang['header_integration_finish']      = "Intégration réussie";
$lang['title_integration_finish']       = "Assistant d'intégration eBay";
$lang['header_auth_setting']            = "Configuration de paramètre";
$lang['title_auth_setting']             = "Configuration de produit";
$lang['header_tax']                     = "Paramètre de taxe";
$lang['title_tax']                      = "Configuration de taxe";
$lang['header_price_modifier']          = "Modificateur de prix";
$lang['title_price_modifier']           = "Règles de prix";
$lang['header_univers']                 = "Univers";										
$lang['title_univers']                  = "Catégories principales sélectionnées";
$lang['header_mapping_categories']      = "Mapping des catégories";
$lang['title_mapping_categories']       = "Choisir des catégories";
$lang['header_mapping_templates']       = "Mapping des templates";
$lang['title_mapping_templates']        = "Choisir les templates";
$lang['header_templates']               = "Gérer les templates";
$lang['title_templates']                = "Charger les templates";
$lang['header_mapping_conditions']      = "Mapping des conditions";
$lang['title_mapping_conditions']       = "Choisir les conditions";
$lang['header_mapping_attributes']      = "Mapping des attributs";
$lang['title_mapping_attributes']       = "Choisir les attributs";
$lang['header_mapping_carriers']        = "Mapping des transporteurs";
$lang['title_mapping_carriers']         = "Choisir l'expédition";
$lang['header_actions']                 = "Actions";
$lang['title_actions']                  = "Actions de produit";
$lang['header_log']                     = "Journal de statistiques";
$lang['title_log']                      = "Journal de lots";
$lang['header_statistics']              = "Statistiques";
$lang['title_statistics']               = "Détails du journal des lots";
$lang['header_orders']                  = "Commandes";
$lang['title_orders']                   = "Rapport de commandes";
$lang['header_wizard_choose']           = "Sélectionner le pays eBay";
$lang['title_wizard_choose']            = "Sélectionner le pays eBay";
$lang['header_wizard_univers']          = "Catégorie";
											
$lang['title_wizard_univers']           = "Catégories principales sélectionnées";
$lang['header_wizard_categories']       = "Catégorie";
											
$lang['title_wizard_categories']        = "Catégories principales sélectionnées";
$lang['header_wizard_tax']              = "Paramètre de taxe";
$lang['title_wizard_tax']               = "Configuration de taxe";
$lang['header_wizard_authorization']    = "Authentification & sécurité eBay";
$lang['title_wizard_authorization']     = "Lier à votre compte eBay";
$lang['header_wizard_shippings']        = "Mapping des transporteurs";
$lang['title_wizard_shippings']         = "Choisir le transporteur";
$lang['header_wizard_finish']           = "Assistant d'exportation des statistiques du produit";
											
$lang['title_wizard_finish']            = "Assistant d'exportation des statistiques du produit";
											
$lang['header_wizard_exports']          = "Actions";
$lang['title_wizard_exports']           = "Assistant d'exportation des produits";
$lang['header_advance_categories']      = "Paramètrage avancé du profil";
$lang['title_advance_categories']       = "Choisir les catégories eBay";
$lang['header_advance_categories_main'] = "Gestionnaire avancé du profil";
$lang['title_advance_categories_main']  = "Gestionnaires des profils de catégories";
$lang['header_advance_main']            = "Paramétrage avancé du profil";
$lang['title_advance_main']             = "Configuration du profil";
$lang['header_advance_profile_configuration']           = "Paramétrage avancé du profil";
$lang['title_advance_profile_configuration']            = "Détails de configuration du profil";
$lang['header_advance_profile_payment'] = "Paramétrage avancé du profil";
$lang['title_advance_profile_payment']  = "Paiement du profil";
$lang['header_advance_profile_return']  = "Paramètres avancés du profil";
$lang['title_advance_profile_return']   = "Retour du profil";
$lang['header_advance_profile_description']  = "Paramétrage avancé du profil";
$lang['title_advance_profile_description']   = "Description du profil";
$lang['header_advance_profile_condition']  = "Paramétrage avancé du profil";
$lang['title_advance_profile_condition']   = "Condition du profil";
$lang['header_advance_profile_shipping']  = "Expédition avancée du profil";
$lang['title_advance_profile_shipping'] = "Expédition du profil";
$lang['header_advance_profile_variation']   = "Variation avancée du profil";
$lang['title_advance_profile_variation']    = "Variation du profil";
$lang['header_advance_profile_specific']    = "Spécificité de l'article avancée du profil";
$lang['title_advance_profile_specific'] = "Spécificité de l'article du profil";
$lang['header_advance_ebay_categories']    = "Catégories avancées eBay";
$lang['title_advance_ebay_categories'] = "Choisir les catégories eBay";
$lang['header_advance_profile_price_modifier']    = "Règles de prix avancées";
$lang['title_advance_profile_price_modifier'] = "Règles de prix";
$lang['header_advance_profile_categories_mapping']    = "Mapping du profil avancé";
$lang['title_advance_profile_categories_mapping'] = "Mapping du profil";
$lang['header_synchronization']         = "synchronisation";
$lang['title_synchronization']          = "Assistant de synchronisation";
$lang['header_synchronization_matching']    = "Assistant de synchronisation";
$lang['title_synchronization_matching']     = "Synchronisation et correspondance";
$lang['header_synchronization_finish']    = "Assistant de synchronisation";
$lang['title_synchronization_finish']     = "Synchronisation terminée";



$lang['integration']                    = "Intégration";
$lang['auth_security']                  = "Auth & securité";
$lang['auth_setting']                   = "Configuration de paramètres";
$lang['tax']                            = "Paramètre de taxe";
$lang['price_modifier']                 = "Modificateur de prix";
$lang['univers']                        = "Catégories sélectionnées";
$lang['mapping_categories']             = "Catégories";
$lang['mapping_templates']              = "Templates";
$lang['mapping_conditions']             = "Condition sélectionnée";
$lang['mapping_carriers']               = "Transporteurs";
$lang['actions']                        = "Actions";
$lang['log']                            = "Journal";
$lang['total']                          = "Total";
$lang['advance_main']                   = "Gestionnaire de règles avancé";
$lang['advance_categories_main']        = "Gestionnaire de profil avancé";
$lang['advance_profile_configuration']  = "Configuration";
$lang['advance_profile_payment']        = "Paiement";
$lang['advance_profile_return']         = "Retour";
$lang['advance_profile_description']    = "Description";
$lang['advance_profile_condition']      = "Condition";
$lang['advance_profile_shipping']       = "Expédition";
$lang['advance_profile_variation']      = "Variation";
$lang['advance_profile_specific']       = "Élément spécifique";
$lang['advance_profile_price_modifier'] = "Modificateur de prix";
$lang['advance_ebay_categories']        = "Catégories";
$lang['advance_profile_categories_mapping'] = "Mapping du profil";
$lang['synchronization_matching_mapping'] = "Mapping de synchronisation";
$lang['synchronization']                = "Synchronisation";
$lang['synchronization_start']          = "Démarrer";
$lang['synchronization_detail']         = "Détail";
$lang['synchronization_matching']       = "Correspondance";
$lang['matching_product']               = "Correspondance de produits";
$lang['synchronization_finish']         = "Terminer";
$lang['integration_start_wizard']       = "Démarrer";
$lang['integration_authorization_wizard']   = "Autorisation";
$lang['integration_synchronize_wizard']   = "synchronisation";
$lang['integration_finish']             = "Terminer";



$lang['synchronization_wizard']         = "Assistant de synchronisation eBay";
$lang['get_synchronization_wizard']     = "Souhaitez-vous obtenir l'inventaire eBay ?";
$lang['get_integration_wizard']         = "Voulez-vous intégrer un compte eBay ?";
$lang['get_generate_wizard']            = "Pourquoi devez-vous générer un jeton avec le compte eBay ?";
$lang['primary_categories']             = "Catégories principales";
$lang['secondary_categories']           = "Catégories secondaires";
$lang['primary_store_categories']       = "Catégories principales de boutique";
$lang['secondary_store_categories']     = "Catégories secondaires de boutique";
$lang['id_ebay_categories_only']        = "Identifiant catégories eBay";


$lang['payment_location']               = "Emplacement des articles"; 
$lang['returns_policy']                 = "Politique de retour"; 
$lang['returns_within']                 = "Retours dans"; 
$lang['who_pays']                       = "Qui paie"; 
$lang['information:']                   = "Information"; 
$lang['listing_duration']               = "Durée de liste"; /*AVA*/
$lang['listing_duration:']              = "Durée de liste :"; 
$lang['selected_tax:']                  = "Taxe selectionnée :"; 
$lang['rounding']                       = "Arrondissement"; 
$lang['shipping_rules']                = "Règles d'expédition"; 
$lang['one_digit']                      = "Un chiffre"; 
$lang['two_digit']                      = "Deux chiffres"; 
$lang['none']                           = "Aucun"; 
$lang['weight']                         = "Poids"; 
$lang['price']                          = "Prix"; 
$lang['item']                           = "Article"; 

$lang['shipping_time']                  = "Temps d'expédition & de transport"; 
$lang['mapping_domestic_shipping']      = "Mapping de transport domestique"; 
$lang['mapping_international_shipping'] = "Mapping de transport international"; 
$lang['choose_a_carrier']              = "Choisir un transporteur"; 
$lang['country:']                       = "Pays :"; 


$lang['rules_surcharge']                = "Définir les règles d'expédition à appliquer au coût total des articles achetés"; 
$lang['rules_weight']                   = "Définir les règles de poids d'expédition"; 
$lang['rules_price']                    = "Définir les règles de prix d'expédition"; 
$lang['rules_item']                     = "Définir les règles d'expédition d'article"; 
$lang['default_price_rule']             = "Modificateur de prix"; 
$lang['adjustment_price']               = "Règles d'ajustement de prix"; 
$lang['default_tax_term']               = "Taxes par défaut"; 
$lang['default_price_type']             = "Saisir des termes"; /*ACA*/
$lang['default_min_price_rule']         = "Prix minimum"; 
$lang['default_max_price_rule']         = "Prix maximum"; 
$lang['dolla']                          = "$"; 
$lang['weight_kg']                      = "Poids (Kg)"; 
$lang['price_min']                      = "Prix (Min)"; 
$lang['price_max']                      = "Prix (Max)"; 
$lang['cost']                           = "Coût"; 
$lang['additional']                     = "Supplémentaire"; 
$lang['add_more']                       = "Ajouter plus"; 
$lang['remove_more']                    = "Supprimer"; 
$lang['go_to']                          = "Lier à"; 
$lang['manager']                        = "gestionnaire"; 
//Log//
$lang['batch_id']                       = "Identifiant de lot"; 
$lang['source']                         = "Source";
$lang['type']                           = "Type";
$lang['error']                          = "Erreur";
$lang['export_date']                    = "Date d'exportation";
$lang['product']                        = "Produit";
$lang['offer']                          = "Offre";
$lang['all']                            = "Tout";
$lang['all:']                           = "Tout :";
$lang['ebay_not_found']                 = "Fichier ou dossier introuvable.";
$lang['search_not_found']               = "Introuvable";
$lang['search']                         = "Recherche";
$lang['setting_profile']                = "Choisir les règles du profil";
$lang['find_categories']                = "Chercheur de catégorie eBay";
$lang['list_categories']                = "Listes de catégories eBay";
$lang['categories_list']                = "Liste de catégories";
$lang['create_your_listing']            = "Liste du profil";
$lang['create_your_payment']            = "Paiement du profil";
$lang['create_your_return']             = "Retour du profil";
$lang['create_your_description']        = "Description du profile";
$lang['create_your_condition']          = "Condition du profil";
$lang['create_your_variation']          = "Variation du profile";
$lang['create_your_shipping']           = "Expédition du profil";
$lang['create_your_specific']           = "Spécificité de l'article du profil";
$lang['create_your_categories']         = "catégories du profil";
$lang['create_your_finish']             = "Félicitations, vous avez terminé la configuration avancée avec succès.";
$lang['synchronize_finish']             = "Félicitations, vous avez terminé la synchronisation avec succès.";
$lang['integration_finish_success']     = "Félicitations, vous avez terminé l'intégration du compte eBay avec succès.";
$lang['out_of_stock']                   = "Rupture de stock";
$lang['maximum_quantity']               = "Quantité maximale";
$lang['identifier_with_items']          = "Identificateur avec articles";
$lang['stock_keeping_unit:']            = "Unité de Gestion de Stock :";
$lang['stock_keeping_unit']             = "SKU";
$lang['sku']                            = "SKU";
$lang['ean13']                          = "EAN13";
$lang['upc']                            = "UPC";
$lang['reference_id']                   = "Identifiant Référence";
$lang['item_id_only']                   = "Identifiant Article";
$lang['title_format']                   = "Format du titre";
$lang['description_format']             = "Format de la description";
$lang['standard_title']                 = "Titre standard";
$lang['manufacturer_title']             = "Fabricant, titre";
$lang['manufacturer_title_reference']   = "Fabricant, titre (Référence)";
$lang['auto_pay']                       = "Paiement automatique";
$lang['open_only']                      = "Ouvrir";
$lang['close_only']                     = "Fermer";
$lang['currency_only']                  = "Devise";
$lang['visitor_counter']                = "Compteur de visiteurs";
$lang['basic_style']                    = "Style de base";
$lang['retro_computer_style']           = "Style rétro-ordinateur";
$lang['hidden_only']                    = "Caché";
$lang['suggestions_only']               = "Suggestions";
$lang['gallery_plus']                   = "Galerie plus";
$lang['payment_setting']                = "Spécification des méthodes de paiement";
$lang['returns_setting']                = "Spécification des détails de la politique de retours";
$lang['description_setting']            = "Spécification de la description";
$lang['condition_description']          = "Description de la Condition ";
$lang['category_setting']               = "Choisir les catégories eBay";
$lang['condition_setting']              = "Spécification de la condition";
$lang['variation_setting']              = "Spécification du type de variation";
$lang['variation_setting_value']        = "Spécification des valeurs de la variation";
$lang['specific_setting']               = "Précision du nom de la spécificité de l'article";
$lang['specific_setting_value']         = "Précision de la valeur de la spécificité de l'article";
$lang['condition_details']              = "Détails de la condition";
$lang['condition_id']                   = "Nom typique";
$lang['variation_value']                = "Valeur typique";
$lang['listing_type']                   = "Type de liste";
$lang['remaining']                      = "Restant";
$lang['return_details']                 = "Détails des descriptions de retours";
$lang['html_descriptions']              = "Descriptions HTML";
$lang['short_descriptions']             = "Description courte";
$lang['long_descriptions']              = "Description standard";
$lang['selected_templates']             = "Templates selectionnés";
$lang['holiday_return']                 = "Retour Vacances Prolongées";/*AVA*/
$lang['learn_more']                     = "Apprendre plus";
$lang['use_matchings']                  = "Options de correspondance";
$lang['use_specifics']                  = "Options fixes";
$lang['profile_category_config']        = "Configuration de catégorie";
$lang['profile_listing_config']         = "Configuration de liste";
$lang['exclude_variation']              = "Nom de type de variation exclu";
$lang['include_variation']              = "Nom de type de variation inclu";
$lang['exclude_specific']               = "Spécificité de l'article exclu";
$lang['include_specific']               = "Spécificité de l'article inclu";
$lang['mapping_profile']                = "Mapping du profil";
$lang['information_inventory']          = "Informations sur un produit existant dans votre inventaire";
$lang['your_store']                     = "Votre inventaire";
$lang['database_store']                 = "Base de données de l'inventaire";
$lang['ebay_store']                     = "Inventaire eBay";
$lang['unknown_product']                = "Produit inconnu";
$lang['synchronize_product']            = "Synchroniser produit";
$lang['product_s']                      = "Produit(s)";
$lang['synchronize_title']              = "Vous avez déjà";
$lang['synchronize_matching']           = "Correspondance produits";
$lang['synchronize_and_matching']       = "Synchronisation et correspondance";
$lang['reference_type']                 = "Type de référence";
$lang['choose_reference']               = "Choisir une référence";
$lang['auto_matching']                  = "Sélectionner un type de correspondance";
$lang['insert_key']                     = "Insérer la clé";
$lang['reference_key']                  = "Clé de référence";
$lang['synchronize_name']               = "Désynchroniser le nom";
$lang['custom_synchronize']             = "Synchronisation personnalisée";

//Default Templates/
$lang['default_template']              = "Mapping de template par défaut ";
$lang['default_templates']              = "Mapping de template par défaut";
$lang['default_profile']                = "Profils par défaut";
$lang['categories_profile']             = "Profils de catégories";
$lang['products_profile']               = "Profils de produits";
$lang['categories_default_profile']     = "Les deux (profil de catégorie et profil par défaut)";
$lang['products_default_profile']       = "Les deux (profil de produit et profil par défaut)";
$lang['all_default_profile']            = "Tout (produits du profil, catégories du profil et profil par défaut)";





$lang['noaction_noimage']               = "Peut exporter des produits au compte eBay s'il n'y a pas d'image pour le produit."; 
$lang['calculate_discount_price']       = "Calculer le prix de remise à partir de Feed.biz s'il y a un prix réduit."; 
$lang['synchronize_with_products']      = "Synchroniser les produits dans le compte eBay, avant d'exporter des produits sur eBay."; 
$lang['import_orders']                  = "Peut importer des commandes du compte eBay, même s'il n'y a pas de produits à exporter de Feed.biz au compte eBay."; 
$lang['profile_description']            = "Ceci enverra tous les produits avec un profil dans les catégories sélectionnées dans votre configuration."; 
$lang['defalut_profile_help']           = "Ceci ne va utiliser que les profils principaux par défaut. Si sélectionné, tous les produits auront une configuration par défaut égale à celle de l'onglet Menu eBay » Paramètre et pourront être exporté vers le compte eBay.";
$lang['categories_profile_help']        = "Ceci ne va utiliser que des profils principaux de catégories. Si sélectionné, cela va filtrer la  configuration des produits dans le profil des catégories. Ceci va exporter vers le compte eBay mais les produits sans profil ne seront pas exportés vers le compte eBay.";
$lang['products_profile_help']          = "Cela ne va utiliser que les profils principaux de produits. Si sélectionné, cela va filtrer la configuration des produits dans le profil des produits. Cela va permettre d'exporter vers le compte eBay mais les produits sans profil ne pourront être exportés vers le compte eBay.";
$lang['both_categories_profile_help']   = "Ceci ne va utiliser que les profils principaux de catégories et ceux sans profil par défaut. Si sélectionné, cela va filtrer la configuration des produits dans le profil des catégories et les produits sans profil auront une configuration par défaut égale à celle de l'onglet Menu eBay » Pramètre et pourront être exportés vers le compte eBay mais les produits sans profil ne seront pas exportés vers le compte eBay.";
$lang['both_products_profile_help']     = "Ceci ne va utiliser que les profils principaux de produits et ceux sans profil par défaut. Si sélectionné, cela va filtrer la configuration des produits dans le profil des produits et les produits sans profil auront une configuration par défaut égale à celle de l'onglet Menu eBay » Pramètre et pourront être exportés vers le compte eBay."; 
$lang['all_profile_help']               = "Ceci ne va utiliser que les profils principaux de produits, les profils secondaires de catégories, et ceux sans profil par défaut. Si sélectionné, cela va filtrer la configuration des produits dans le profil des produits après que les autres produits filtrent la configuration de produits dans le profil des catégories et les produits sans profil auront une configuration par défaut égale à celle de l'onglet Menu eBay » Pramètre et pourront être exportés vers le compte eBay.";
$lang['categories_search_help']         = "Entrez un identifiant ou un nom de catégorie pour aller directement à une catégorie.";
$lang['sku_help']                       = "Utiliser un SKU un un identificateur d'article pour les articles.";
$lang['title_format_help']              = "Saisissez le nom du produit, eBay recommande de formater le titre comme Titre Standard. Veuillez choisir le premier choix si vos titres de produits sont déjà formatés pour eBay.";
$lang['description_format_help']        = "Champ Description pour envoyer au compte eBay. (Exporter les descriptions HTML à la place du texte seulement)";
$lang['auto_pay_help']                  = "Pour créer une exigence afin que l'acheteur paie immédiatement (par PayPal) pour une vente aux enchères (Achat immédiat uniquement) ou un article à prix fixe, le vendeur peut inclure et définir le champ Payer Automatiquement à 'Ouvrir'. Si le vendeur ne veut pas créer un article de paiement immédiat, ce champ est soit omis ou inclus et défini à 'Fermer'. ";
$lang['country_name_help']              = "Un code à deux lettres qui représente le pays dans lequel se trouve l'article.";
$lang['currency_help']                  = "Devise associée aux informations de prix de l'article. Code de devise à 3 lettres qui correspond au site indiqué dans la demande d'inscription.";
$lang['visitor_counter_help']           = "Les compteurs eBay suivent le nombre d'acheteurs qui ont vu votre liste. Plusieurs vues du même utilisateur ne sont comptés qu'une seule fois. Le compteur s'affiche en bas de votre liste.";
$lang['out_of_stock_help']              = "Minimum de la quantité en stock pour exporter le produit.";
$lang['maximum_quantity_help']          = "Maximum de la quantité pour l'exportation vers le compte eBay.";
$lang['primary_categories_help']        = "Ceci est la catégorie principale où les acheteurs pourront trouver votre article sur eBay. (pas dans votre boutique, mais sur les achats généraux d'eBay)";
$lang['second_category_warning_help']   = "Utiliser une deuxième catégorie (frais supplémentaires eBay appliqués)";
$lang['second_category_help']           = "Ce champ est facultatif. Si vous ajoutez une catégorie secondaire à une liste, vous pouvez plus tard la changer pour une autre, mais vous ne serez pas en mesure d'éliminer entièrement l'utilisation d'une catégorie secondaire.";
$lang['store_category_warning_help']    = "Une catégorie personnalisée est une catégorie que le vendeur a créée dans votre boutique eBay.";
$lang['store_category_help']            = "si vous avez importé les catégories de la boutique, vous serez également en mesure de choisir les catégories de la boutique eBay lorsque vous effectuez votre mapping de produit. Les catégories de boutique sont facultatives et ne s'appliquent que si vous avez une Boutique eBay et que vous les avez importées pour une utilisation dans Feed.biz.";
$lang['store_secondary_category_help']  = "Les catégories de boutique sont facultatives et ne s'appliquent que si vous avez une boutique eBay et que vous pouvez choisir une catégorie de boutique secondaire de la structure de catégories de votre Boutique eBay aussi, si désiré.";
$lang['gallery_plus_warning_help']      = "Utiliser une galerie plus (frais supplémentaires eBay s'appliquent)";
$lang['gallery_plus_help']              = "Afficher une grande image dans les résultats de recheche-capture des détails spéciaux ou des vues différentes pour les acheteurs.";
$lang['listing_duration_help']          = "La durée précise la volonté initiale du vendeur au moment du listing.";
$lang['listing_type_help']              = "Indique le format de vente utilisée pour une liste.";
$lang['payment_help']                   = "Uniquement pour les vendeurs qui acceptent les achats par carte de crédit à travers leur propre compte marchand.";
$lang['payment_instructions_help']      = "Donner des instructions claires pour aider votre acheteur avec le paiement et l'expédition.";
$lang['payment_instructions_max_help']  = "Max. Longueur: 500 caractères";
$lang['return_instructions_max_help']   = "Max. Longueur: 1000 caractères";
$lang['condition_description_max_help'] = "Max. Longueur: 1000 caractères";
$lang['return_specify_help']            = "Exiger que tous les vendeurs précisent s'ils acceptent ou non les retours. Indique si le vendeur permet à l'acheteur de retourner l'article. Ce champ est obligatoire quand la politique de retour est spécifiée.";
$lang['return_within_help']             = "Après réception de l'article, la durée durant laquelle votre acheteur devrait commencer un retour. L'acheteur peut renvoyer l'article dans cette période de la journée, ils reçoivent l'article. ";
$lang['return_pays_help']               = "Par qui l'expédition de retour sera payée. La partie qui paie les frais de transport pour un article retourné.";
$lang['return_information_help']        = "Une explication détaillée de la politique de retour du vendeur. D'autres détails de la politique de retour. Assurez-vous que ces détails supplémentaires et la description de votre liste correspond à ce que vous avez choisi ci-dessus.";
$lang['holiday_return_help']            = "Offrir une politique supplémentaire de retour généreuse pendant les vacances est un excellent moyen de capter l'intérêt de l'acheteur et conclure la vente.";
$lang['condition_description_help']     = 'Remarque : Le champ de description de l\'état sera ignoré pour les conditions "Nouveau". ("Nouveau", "Nouveau dans la boîte", etc.)';
$lang['condition_new_help']             = 'Remarque : Le champ de description de l\'état A nouveau, produit inutilisé avec absolument aucun signe d\'usure. Il se peut qu\'il manque à l\'article l\'emballage d\'origine, ou qu\'il soit dans l\'emballage d\'origine, mais pas scellé. L\'article peut être une un article remis à neuf de l\'usine ou un nouveau, inutilisé avec des défauts. Consultez la liste du vendeur pour plus de détails et une description de toutes les imperfections. ("Nouveau", "Nouveau dans la boîte", "Nouveau avec la boîte", "Nouveau avec des étiquettes", etc.)';
$lang['condition_used_help']            = 'Remarque : Le champ de description de l\'état Un article qui a été utilisé précédemment. L\'article peut avoir des signes d\'usure, mais est pleinement opérationnel et fonctionne comme prévu. Cet article peut être un template de démonstration ou un retour de la boutique qui a été utilisé. Consultez la liste du vendeur pour plus de détails et une description de toutes les imperfections. ("D\'occasion", etc.)';
$lang['condition_refurbished_help']     = 'Remarque : Le champ de description de l\'état Un article qui a été remis en état de fonctionnement par le vendeur eBay ou un tiers non approuvé par le fabricant ou qui a été professionnellement remis en état de fonctionnement par un fabricant ou un fournisseur approuvé par le fabricant. Cela signifie que le produit a été inspecté, nettoyé et réparé à un état de fonctionnement complet et en excellent état. Cet article peut ou non être dans son emballage d\'origine. ("Rénové fabricant", "Rénové vendeur", etc.)';
$lang['condition_good_help']            = 'Remarque : Le champ de description de l\'état Un article qui a été en bon état. Dommages très minimes au produit. ("Bon", "très bon", etc.)';
$lang['condition_acceptable_help']      = 'Remarque : Le champ de description de l\'état Un article qui a été utilisé précédemment. Peut y avoir des dommages dans le produit, mais son intégrité reste encore intacte. ("Acceptable", etc.)';
$lang['condition_help']                 = "L'état de votre article apparaît en haut de la description de votre liste. Vous pouvez fournir des éclaircissements au sujet de la condition dans la description de la liste.";
$lang['variation_help']                 = 'Listes multi-variation contenant plusieurs articles qui sont essentiellement le même produit, mais varient dans leurs détails de fabrication ou d\'emballage. (Par exemple, une marque et un style particulier de la chemise pourraient être disponibles dans différentes tailles et couleurs, comme "large bleu" et "moyen noir".) Chaque variation spécifie une déclinaison de l\'une de ces couleurs et tailles. Chaque variation peut avoir une différente de quantité et de prix. Vous pouvez acheter plusieurs articles d\'une variation en même temps. (C\'est à dire, une ligne de commande d\'article peut contenir plusieurs articles d\'une seule variation.) ';
$lang['specific_help']                  = 'Une liste de paires de noms et valeurs de spécificités d\'articles que le vendeur a précisé pour l\'article. Les spécificités de l\'objet décrivent des aspects bien connus d\'un article ou un produit de manière standard, pour aider les acheteurs à trouver ce type d\'article ou produit plus facilement. Par exemple, "Année de publication" est un aspect typique de livres, et "mégapixels" est un aspect typique des appareils photo numériques. ';
$lang['variation_value_help']           = 'Une valeur qui est associée avec le nom du type de variation. Par exemple, supposons que cette série de photos montre des chemises bleues, et certaines des variations comprennent Couleur = Bleue dans leurs spécificités de variation. Si le nom du type de la spécificité de la variation est "Couleur", alors la valeur de type de variation serait "Bleue".';
$lang['variation_prepare_value_help']   = 'Une valeur qui est associée avec le nom du type de variation. Par exemple, supposons que cette série de photos montre des objets {A}, et certaines des variations comprennent {A} = {B} dans leurs spécificités de variation. Si le nom du type de la spécificité de la variation est "{A}", alors la valeur de type de variation serait "{B}".';
$lang['specific_prepare_value_help']    = 'Une valeur qui est associée au nom de la spécificité de l\'article. Par exemple, supposons que cette série de photos montre les objets {A}, et une partie des spécificité de l\'article comprend {A} = {B} dans leurs spécificités d\'article. Si le nom du type de la spécificité de l\'article est "{A}", puis la valeur de la spécificité de l\'article serait "{B}".';
$lang['postcode_help']                  = "Code postal de l'endroit où se trouve l'article. Cette valeur est utilisée pour les recherches de proximité.";
$lang['dispatch_time_help']             = "Indique le nombre maximal de jours ouvrables auquel le vendeur s'engage pour préparer un article à être expédié après avoir reçu un paiement autorisé. Ce temps n'inclut pas le temps d'expédition (le temps de transit du transporteur).";
$lang['shipping_rules_help']            = "Précise les règles de prix de livraison pour préparer le prix de l'article à exporter vers le compte eBay.";/*ACA*/
$lang['country_shipping_help']          = "Précise les régions internationales vers lesquelles vous livrez.";
$lang['domestic_shipping_help']         = "Précise l'expédition vers laquelle vous livrez. La liste des premiers articles dans l'expédition sélectionnée doit préciser au moins un service d'expédition nationale.";
$lang['international_shipping_help']    = "Précise l'expédition vers laquelle vous livrez.";/*AVA*/
$lang['ebay_shipping_help']             = "Spécifie l'expédition domestique eBay vers laquelle vous livrez. La liste des premiers articles de l'expédition sélectionnée doit préciser au moins un service d'expédition national.";
$lang['ebay_international_shipping_help'] = "Spécifie l'expédition internationale eBay vers laquelle vous livrez.";
$lang['first_shipping_help']            = "La liste des premiers articles de l'expédition sélectionnée doit préciser au moins un service d'expédition national.";
$lang['additional_help']                = "Le coût supplémentaire pour expédier chaque article identique supplémentaire à l'acheteur.";
$lang['cost_help']                      = "Le coût du service d'expédition pour expédier un article à l'acheteur.";
$lang['price_min_help']                 = "Le prix min de l'expédition lorsqu'elle est calculée pour un article à l'acheteur.";
$lang['price_max_help']                 = "Le prix max de l'expédition lorsqu'elle est calculée pour un article à l'acheteur.";
$lang['weight_min_help']                = "Le poids du produit min lorsqu'elle est calculée pour un article à l'acheteur.";
$lang['weight_max_help']                = "Le poids du produit max lorsqu'elle est calculée pour un article à l'acheteur.";
$lang['html_descriptions_help']         = "Ce champ est utilisé par le vendeur pour fournir une description html à l'article. Dans la liste des demandes, vous pouvez soumettre votre description. si vous souhaitez utiliser du HTML ou des caractères réservés XML dans la description.";
$lang['short_descriptions_help']        = "Ce champ est utilisé par le vendeur pour fournir une brève description à l'article. Ce champ est obligatoire pour toutes les listes, sauf lors de la création d'un article en utilisant un produit du catalogue eBay.";
$lang['long_descriptions_help']         = "Ce champ est utilisé par le vendeur pour fournir une description standard à l'article. Ce champ est obligatoire pour toutes les listes, sauf lors de la création d'un article en utilisant un produit du catalogue eBay.";
$lang['selected_templates_help']        = "Le template dont les données seront affichées. Un vendeur peut sélectionner le conteneur des données nécessaires pour lister un article. Un template peut être spécifié, chacun dans le conteneur propre du produit.";
$lang['payment_help']                   = "Identifie la méthode de paiement que le vendeur acceptera lorsque l'acheteur paiera pour l'article.";
$lang['tax_term_help']                  = "Vous devez configurer une règle de taxes en valeur ou en pourcentage pour une ou aucune taxe. Le calcul est un pourcentage ou une valeur avant et après, ou le calcul est une valeur du prix d'une augmentation ou d'une diminution. Les taxes par défaut seront calculées, l'augmentation ou la diminution du prix de l'article aura un prix dans les plages de prix qui sera calculé à l'acheteur.";
$lang['price_modifier_rules_help']      = "Vous devez configurer une règle de prix en valeur ou en pourcentage pour une ou plusieurs plages de prix. Le calcul est un pourcentage ou une valeur avant et après, ou le calcul est une valeur du prix d'une augmentation ou d'une diminution. Les règles de prix par défaut seront calculées, l'augmentation ou la diminution par modificateur de prix, et les articles qui ont des prix dans les plages de prix seront calculé à l'acheteur.";
$lang['price_modifier_min_help']        = "Les règles de prix minimum quand est calculé le modificateur de prix pour pour un article à l'acheteur.";/*AVA*/
$lang['price_modifier_max_help']        = "Les règles de prix maximum quand est calculé le modificateur de prix pour un article à l'acheteur.";/*AVA*/
$lang['price_modifier_value_help']      = "Les règles de prix par défaut calculreont l'augmentation ou la diminution par le modificateur de prix dans les termes {A}.";
$lang['price_modifier_type_help']       = "Le calcul est un pourcentage ou une valeur avant et après, ou le calcul est une valeur de prix d'une augmentation ou une diminution.";
$lang['price_modifier_rounding_help']   = "Mode d'arrondi à appliquer sur le prix. (exemple: le prix sera égal à 10,17 ou 10,20)";
$lang['synchronization_start_help']      = "Cet assistant va télécharger votre inventaire du compte eBay. L'inventaire sera comparé à votre inventaire local. Après cela, l'assistant va marquer « à correspondre » les offres inconnues présentes dans votre base de données, mais pas sur eBay. Le but est de créer vos offres sur eBay. Lorsque vous cliquez sur Commencer, vous obtiendrez votre inventaire d'eBay. Faisons-le.";
$lang['synchronization_detail_select_help']     = "Indique le type de référence de produits auquel le vendeur s'engage pour préparer un article au mapping après avoir reçu un inventaire synchronisé du compte ebay.";
$lang['synchronization_detail_key_help']     = "Indique la chaîne de caractères de références de produits de l'inventaire synchronisé pour générer la chaîne de caractères de tous les produits qui correspondent à la chaîne. [ProductID] comme eBay_[ProductID]";
$lang['synchronization_detail_reference_help']     = "Indique la chaîne de caractères de références de produits dans une base de données pour générer la chaîne de caractères de tous les produits qui correspondent à la chaîne. [ProductID] comme Feedbiz_[ProductID]";
$lang['synchronization_matching_help']   = "";
$lang['synchronization_finish_help']     = "Le vendeur s'engage à préparer les templates. Téléchargez et précisez les templates de produits par défaut. ";
$lang['advance_profile_finish_help']     = "Lorsque vous finissez de préparer le profil. Cliquez sur le mapping du profil pour mapper le profil par rapport aux catégories de votre inventaire.";

$lang['your_store_help']                = "La liste des articles venant du flux d'importation des articles ou produits de votre magasin ou grossiste.";
$lang['ebay_store_help']                = "La liste des articles que l'utilisateur vend activement sur le compte eBay.";
$lang['unknown_product_help']           = "La liste des articles non mappés et synchronisés, ou les articles d'eBay que l'utilisateur ne peut pas mapper avec votre article.";
$lang['synchronize_product_help']       = "La liste des articles avec mapping synchronisés ou les articles exportés de feedbiz.";
$lang['update_offer_product_help']      = "Ceci va synchroniser les mouvements de l'inventaire dans le compte eBay, dans les catégories sélectionnées dans votre configuration et réviser la quantité et les prix de la liste.";
$lang['export_product_help']            = "Si vous terminez votre mapping et la configuration de votre module. Vous pouvez envoyer tous les produits sur le compte eBay. Si vous voulez exporter des produits, cliquez sur Exporer les produits, ce qui enverra tous les produits ayant une configuration et inclus dans les catégories sélectionnées dans la configuration de votre module vers le compte eBay.";
$lang['import_order_help']              = "Si vous voulez obtenir une commande du compte d'eBay, cliquez Importer la commande cela importera les commandes du compte eBay. Pour chaque commande que vous importez il y aura un récapitulatif de la commande et un lien vers la commande dans vos modules.";
$lang['confirm_order_help']             = "Si vous souhaitez exporter le numéro de suivi de la commande au compte eBay, cliquez sur Confirmer l'expédition de la commande cela confirmera le numéro de suivi au compte eBay. Chaque commande que vous exportez précisée dans le résumé de la commande.";
$lang['order_status_help']              = "Le champ est utilisé pour récupérer les commandes qui sont dans un état spécifique. Si ce champ est utilisé avec un filtre de date, seules les commandes satisfaisant à la fois la plage de dates et la valeur du statut de la commande sont récupérées. Le type énumération qui définit les valeurs possibles pour les Etats de la commande.";
$lang['delete_product_help']            = "La liste des articles dans le compte eBay qui seront supprimés vers la liste des invendus. Si vous cliquez dessus, cela supprimera tous les produits dans les catégories sélectionnées dans votre configuration.";
$lang['delete_out_of_stock_help']       = "La liste des articles en rupture de stock dans le compte eBay qui seront supprimés vers la liste des invendus. Cela supprimera les produits en rupture de stock d'eBay.";
$lang['delete_all_in_stock_help']       = "La liste des articles qui ont été exportés de feedbiz qui seront supprimés vers la liste des invendus. Cette opération va supprimer les produits relevant des catégories sélectionnées dans votre configuration.";
$lang['templates_default_help']         = "Ceci présentera les divers templates à utiliser pour que feedbiz exporte les produits vers les comptes eBay. La mise en page des templates détermine le positionnement relatif du texte et des images dans la zone rectangulaire dans laquelle la description d'un article est affichée. Par exemple, un template précise le code HTML qui représente la description de l'article qui s'affiche dans la description eBay.";
$lang['templates_custom_help']          = "La liste des templates ont été téléchargés depuis feedbiz, chacun de ces champs peut être téléchargé, supprimé, ou visualisé.";
$lang['integration_ebay_help']          = "Cet assistant va faire la liaison avec le compte eBay. Liez en toute sécurité votre compte eBay à Feedbiz en vous connectant à eBay. Cela permettra d'activer votre inventaire, vos commandes, vos commentaires, et votre boutique, et vous permettre d'importer, soumettre des listes, et gérer les commandes eBay sur FeedBiz sans partager votre mot de passe eBay. Acquittez-vous des commandes rapidement avec le traitement des commandes simplifié. Lorsque vous cliquez sur Commencer, cela va faire le lien entre FeedBiz et votre compte eBay. Faisons-le.";
$lang['integration_ebay_authorization_help']    = "Vous devez avoir un jeton eBay pour faire le lien entre votre compte eBay et votre compte FeedBiz. Une fois que vous générez un jeton, vous serez en mesure d'utiliser tous les outils de vendeur du module. Lorsque vous cliquez sur Commencer, cela va établir la connexion à votre compte eBay. Si cela réussit, un jeton à votre compte eBay va être généré. Faisons-le.";
$lang['integration_ebay_finish_help']    = "Vous avez réussi à générer eBay jeton. Vous pouvez cliquer pour synchroniser l'inventaire de votre compte eBay afin de faire la correspondance des produits de votre compte FeedBiz ou s'il n'y a aucun produit, cliquez sur le paramètre pour la configuration au compte Feedbiz. Faisons-le.";
$lang['integration_ebay_token_help']    = "Un jeton est une fonction de sécurité qui vous permet d'effectuer des tâches sur eBay directement à partir de FeedBiz.";



$lang['info_tax']                               = "Formule à appliquer sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).{BR}Appliquer une formule de prix spécifique pour les catégories sélectionnées d'eBay qui remplacera le paramètre principal.{BR}Le mode d'arrondi à être appliqué au prix (ex: prix sera égal à 10,17 ou 10,20)";
$lang['info_wizard_tax']                        = "Formule à appliquer sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).{BR}Appliquer une formule de prix spécifique pour les catégories sélectionnées d'eBay qui remplacera le paramètre principal.{BR}Le mode d'arrondi à être appliqué au prix (ex: prix sera égal à 10,17 ou 10,20)";
//$lang['info_actions']                           = "This will send all the products having a profile and within the selected categories in your module configuration.{BR}If you want to add a product to the list, please go on the mapping and setting configuration in eBay options tab.{BR}After that, please do not forget to generate and save a report (For any support you will need the XML and the Report)";
$lang['info_advance_profile_price_modifier']    = "1. Vous pouvez configurer une règle de prix en valeur ou en pourcentage pour une ou plusieurs plages de prix.{BR}2. Formule à être appliquée sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).{BR}3. Appliquer une formule de prix spécifique pour les catégories sélectionnées d'eBay qui remplacera le paramètre principal.{BR}4. Mode d'arrondi à appliquer au prix (ex: prix sera égal à 10,17 ou 10,20)";
$lang['info_advance_main']                      = "la première fois tous les produits auront la même configuration par défaut de l'onglet Menu eBay » Paramètrer pour pouvoir exporter vers le compte eBay.";
$lang['info_actions']                   = "Ceci enverra tous les produits avec un profil relevant des catégories sélectionnées dans la configuration de votre module.{BR}Si vous souhaitez ajouter un produit à la liste, veuillez aller au mapping et parametrer la configuration dans l'onglet des options d'eBay.{BR}Ensuite, n'oubliez pas de générer et enregistrer un rapport (pour le support vous aurez besoin du XML et du Rapport)";
$lang['info_wizard_exports']            = "Ceci enverra tous les produits avec un profil relevant des catégories sélectionnées dans la configuration de votre module.{BR}Si vous souhaitez ajouter un produit à la liste, veuillez aller au mapping et parametrer la configuration dans l'onglet des options d'eBay.{BR}Ensuite, n'oubliez pas de générer et enregistrer un rapport (pour le support vous aurez besoin du XML et du Rapport)";
$lang['info_mapping_categories']        = "Vous pouvez sélectionner plusieurs catégories à la fois en cochant la première catégorie puis en appuyant sur {B}'Shift'{/B} et en cliquant sur le dernier élément que vous souhaitez inclure dans le même profil.{BR}{B}1. Catégorie principale eBay {/B} - Il s'agit de la catégorie principale où les acheteurs puissent trouver votre article sur eBay (pas dans votre boutique, mais sur les achats généraux d'eBay).{BR}{B}2. Catégorie eBay secondaire (facultative){/B} - Ce champ est facultatif. Si vous ajoutez une catégorie secondaire à une liste, vous pouvez plus tard la changer pour une autre, mais vous ne serez pas en mesure d'éliminer entièrement l'utilisation d'une catégorie secondaire.{BR}{B}3. Catégorie de boutique eBay principale (optionnel){/B} - si vous avez importé les catégories de la boutique, vous serez également en mesure de choisir les catégories de la boutique eBay lorsque vous effectuez votre mapping de produit. Les catégories de boutique sont facultatives et ne s'appliquent que si vous avez une Boutique eBay et vous les avez importées pour une utilisation dans Feed.biz.{BR}{B}4. Catégorie de boutique secondaire eBay (optionnel){/B} - vous pouvez choisir une deuxième catégorie de votre structure de catégorie de boutique eBay aussi, si désiré.";
$lang['info_mapping_templates']         = "Vous pouvez sélectionner plusieurs catégories à la fois en cochant la première catégorie puis en appuyant sur {B}'Shift'{/B} et en cliquant sur le dernier élément que vous souhaitez inclure dans le même profil.";
$lang['info_wizard_categories']         = "Vous pouvez sélectionner plusieurs catégories à la fois en cochant la première catégorie puis en appuyant sur {B}'Shift'{/B} et en cliquant sur le dernier élément que vous souhaitez inclure dans le même profil.";
$lang['info_ebay_wizard']               = "Si vous êtes un nouvel utilisateur, je vous recommande de vous inscrire pour le compte ebay. Cet assistant va couvrir les concepts de base de la plate-forme eBay et vous aidera à vous familiariser avec certains de nos programmes populaires. À la fin de l'assistant, vous pouvez exporter les produits à votre compte eBay.";
$lang['info_congratulation']            = "Félicitations, vos produits ont été envoyés à eBay avec succès, vous pouvez maintenant publier votre offre dans l'onglet d'action.";
$lang['info_price_modifier']            = "1. Vous pouvez configurer une règle de prix en valeurs ou en pourcentages pour une ou plusieurs plages de prix.{BR}2. Formule à être appliquée sur tous les prix des produits exportés (multiplication, division, addition, soustraction, pourcentages).{BR}3.Appliquer une formule de prix spécifique pour les catégories sélectionnées eBay qui remplacera le paramètre principal.{BR}4.Mode d'arrondi à appliquer au prix (ex: prix sera égal à 10,17 ou 10,20)";


$lang['parameters']                     = "Paramètres";  
$lang['statistics']                     = "Statistiques";
$lang['statistics_products']            = "Statistiques de produits";
$lang['mapping']                        = "Mapping";
$lang['mapping_attributes']             = "Attributs";
$lang['mapping_attributes_value']       = "Valeurs d'attributs";



$lang['templates']                      = "Templates";
$lang['get_orders']                     = "Commandes";


$lang['orders']                         = "Rapport de commandes";
$lang['choose_a_attributes']            = "Choisir un attribut";
$lang['choose_ebay_attributes']         = "Choisir les attributs eBay";
$lang['choose_specific_value']          = "Choisir la valeur de spécificité d'article";
$lang['choose_variation_value']         = "Choisir la valeur de variation";
$lang['choose_variation_type']          = "Choisir le type de variation";
$lang['choose_a_categories']            = "Choisir une catégorie";
$lang['choose_ebay_categories']         = "Choisir les catégories eBay";
$lang['choose_ebay_store_categories']   = "Choisir les catégories de boutique eBay";
$lang['get_store']                      = "Catégories de boutiques importées";
$lang['insert_variation_type']          = "Insérer le type de variation";
$lang['choose_a_shippings']             = "Choisir une expédition";
$lang['choose_ebay_shippings']          = "Choisir l'expédition eBay";
$lang['choose_a_country']               = "Choisir un pays";
$lang['choose_a_report']                = "Choisir un rapport";
$lang['associate_carrier']              = "Choisir un transporteur associé";
$lang['ebay_carrier']                   = "Choisir un transporteur eBay";
$lang['default_template']               = "Mapping de template par défaut";
$lang['select_a_option']                = "Sélectionnez une option";
$lang['returns_accepted']               = "Retours acceptés";
$lang['returns_not_accepted']           = "Aucun retour accepté";
$lang['no_tax']                         = "Aucune taxe";
$lang['days_1']                         = "1 jour";
$lang['days_2']                         = "2 jours";
$lang['days_3']                         = "3 jours";
$lang['days_4']                         = "4 jours";
$lang['days_5']                         = "5 jours";
$lang['days_7']                         = "7 jours";
$lang['days_10']                        = "10 jours";
$lang['days_14']                        = "14 jours";
$lang['days_15']                        = "15 jours";
$lang['days_20']                        = "20 jours";
$lang['days_30']                        = "30 jours";
$lang['days_60']                        = "60 jours";
$lang['buyer']                          = "Acheteur";
$lang['seller']                         = "Vendeur";

$lang['green']                          = "Vert";
$lang['greenNew']                       = "Vert Nouveau";
$lang['blue']                           = "Bleu";
$lang['percent']                        = "%";
$lang['percentage']                     = "Pourcentage";
$lang['value']                          = "Valeur";
$lang['optional']                       = "(Optionnel)";



$lang['export']                         = "Exporter";

$lang['endExport']                      = "Supprimer l'exportation";
$lang['sub_export']                     = "exporter produit eBay";
$lang['export_product']                 = "Exporter les produits";
$lang['export_shipment']                = "Exporter l'envoi";
$lang['import_order']                   = "Importer les commandes";
$lang['delete_product']                 = "Supprimer les produits";
$lang['sub_end_export']                 = "supprimer l'exportation de produit eBay";
$lang['offer_product_update']           = "Mettre à jour les offres de produits à eBay";
$lang['export_product_update']          = "Exporter l'inventaire à eBay";
$lang['import_order_update']            = "Importer les commandes d'eBay";
$lang['export_shipment_update']         = "Confirmer l'envoi des commandes à eBay";
$lang['synchronize_wizard']             = "Assistant de synchronisation";
$lang['update_product_offer']           = "Mettre à jour les offres de produits";
$lang['delete_type']                    = "Supprimer le type de produit";
$lang['delete_out_of_stock']            = "Supprimer les produits en rupture de stock";
$lang['delete_all_sent_product']        = "Supprimer tous les produits envoyés";
$lang['delete_product_update']          = "Supprimer tous les produits d'eBay";



//!!! Wizard  !!!
$lang['wizard_authorization']           = "Autorisation";
$lang['wizard_choose']                  = "Choisir le site";
$lang['wizard_univers']                 = "Sélectionnez l'univers";
											
$lang['wizard_categories']              = "Catégories";
$lang['wizard_tax']                     = "Taxe";
$lang['wizard_shippings']               = "Expéditions";
$lang['wizard_exports']                 = "Exporter les produits";
$lang['wizard_finish']                  = "Terminer";


//Condition
$lang['conditions']                     = "Conditions";
$lang['marketplace_condition']          = "Condition de place de marché";
$lang['reset_condition']                = "Réinitialiser la condition";
$lang['your_conditions']                = "Votre condition";


//other
$lang['auth_security_token']            = "Jeton de auth sécurité";
$lang['new_auth_security']              = "Nouvel auth securité";
$lang['auth_security_session']          = "Session Auth securité";
$lang['welcome']                        = "Bienvenue";
$lang['Configuration']                  = "Configuration";
$lang['choosesite']                     = "Choisir le site eBay";
$lang['importing']                      = "importation";
//statistic
$lang['product_id']                     = "Identifiant Produit";
$lang['user']                           = "Utilisateur";
$lang['name']                           = "Nom";
$lang['response']                           = "Réponse";
$lang['message']                           = "Message";
//order
$lang['orders']                                                 = "Commandes";
$lang['id_order']                                               = "Identifiant Commande";
$lang['id_marketplace_order_ref']                               = "Identifiant ref. Commande";
$lang['id_buyer']                                               = "Identifiant Acheteur";
$lang['payment_method']                                         = "Paiement par";
$lang['total_paid']                                             = "Montant payé";
$lang['order_date']                                             = "Date de la commande";
$lang['order_completed']                                        = "Terminé";
$lang['order_active']                                           = "Active";
$lang['orders_processing']                                      = "Traitement des commandes";
$lang['invoice_no']                                             = "Identifiant de la facture";
$lang['country_name']                                           = "Nom du pays";
$lang['product_export_statistic']                               = "Statistique d'exportation de produits";
//Profile
$lang['profile_name']                                           = "Nom de profil";
$lang['profile_create_by']                                      = "Créé par";
$lang['profile_completed']                                      = "Terminé";
$lang['profile_status']                                         = "Statut";
$lang['profile_date']                                           = "Date de création";
$lang['profile_edit']                                           = "Modifier";
$lang['create_profile']                                         = "Créer un profil";
$lang['delete_profile']                                         = "Supprimer un profil";
$lang['enabled_profile']                                        = "Profil activé";
$lang['disabled_profile']                                       = "Profil désactivé";

//wizard
$lang['ebay_wizard_configuration']                              = "Assistant de configuration eBay";
$lang['ebay_advance_configuration']                             = "Assistant de profil avancé";



$lang['warning']                                                = "avertissement";
$lang['ebay_wizard']                                            = "Assistant eBay";
$lang['exported']                                               = "Exporté";

$lang['email']                                                  = "Email";
$lang['Amount']                                                 = "Montant";
$lang['Site']                                                   = "Site";
$lang['eBay']                                                   = "eBay";
$lang['infomation']                                             = "Infomation";
$lang['reset_categories']                                       = "Réinitialiser les catégories";
$lang['reset_templates']                                        = "Réinitialiser les templates";

$lang['send']                                                   = "Envoyer au compte eBay";
$lang['send_products_to_ebay']                                  = "Envoyer les produits à eBay";
$lang['send_offers_to_ebay']                                    = "Envoyer les offres à eBay";
$lang['sending..']                                              = "Envoi...";
$lang['get_from_ebay']                                          = "Obtenir les commandes d'eBay";
$lang['removing..']                                             = "Suppression..";
$lang['starting_get']                                           = "Début de reception";
$lang['starting_delete']                                        = "Début de suppression";
$lang['starting_send']                                          = "Début d'envoi";
$lang['delete']                                                 = "Supprimer dans le compte eBay";
$lang['Statistic Export Product']                               = "Statistiques d'exportation de produits";

$lang['eBay Username']                                          = "Nom d'utilisateur eBay";
$lang['Product Name']                                           = "Nom du produit";
$lang['AddFixedPriceItem']                                      = "AddFixedPriceItem";
$lang['RelistFixedPriceItem']                                   = "RelistFixedPriceItem";
$lang['ReviseFixedPriceItem']                                   = "ReviseFixedPriceItem";
$lang['Compressed']                                             = "Compressé";
$lang['validation']                                             = "Validation";
$lang['success']                                                = "Succès";
$lang['failure']                                                = "Échec";
$lang['Search Message']                                         = "Rechercher un message";
$lang['order']                                                  = "Importer la commande du compte eBay";
$lang['delete_on_ebay']                                         = "Supprimer les poduits sur eBay";
$lang['Warning! Configuration parameter not complete.']         = "Avertissement ! La configuration des paramètres n'est pas terminée.";
$lang['Carrier Configuration']                                  = "Configuration de transporteur";
$lang['info_mapping_attributes_value']                          = "Vous pouvez choisir plusieurs valeurs d'attributs en même temps en sélectionnant le premier nom de l'attribut puis en appuyant sur \"Shift\" et en cliquant sur le dernier élément que vous voulez mettre dans le même attribut";
$lang['export_from_wizard']                                     = "Exporter depuis l'assistant";

$lang['ebay_wizard_clear']                                      = "eBay_wizard_clear";
$lang['ebay_wizard_start']                                      = "eBay_wizard_start"; 
$lang['mapping_ebay_save']                                      = "Mapping_ebay_save";
$lang['statistic_processing']                                   = "Traitement des statistiques"; 
$lang['Reset Categories']                                       = "Réinitialiser les catégories";
$lang['Reset Templates']                                        = "Réinitialiser les templates";
$lang['mapping_carriers_save']                                  = "Enregistrer les mappings de transporteurs";
$lang['auth_security_save']                                     = "Enregistrer auth securité";
$lang['wizard_statistic_link']                                  = "Assistant statistiques de lien";
$lang['Save Mappings & Continue']                               = "Enregistrer les mappings & continuer";


$lang['ebay_wizard_shipping']                                   = "eBay_wizard_shipping";/*AVA*/
$lang['attribute_ebay_save']                                    = "Attribute_ebay_save";/*AVA*/
$lang['attribute_ebay_value_save']                              = "Attribute_ebay_value_save";/*AVA*/

$lang['univers_save']                                           = "Univers_save";/*AVA*/
$lang['test_ebay']                                              = "Test_eBay";/*AVA*/
$lang['Exporting']                                              = "Exportation";
$lang['Confirm Export']                                         = "Confirmer l'exportation";
$lang['Export']                                                 = "Exporter";
$lang['univers_save']                                           = "univers_save";/*AVA*/

$lang['ebay_univers']                                           = "Sélectionner l'univers";
$lang['tax_save']                                               = "Tax_save";/*AVA*/
$lang['get_postalcode_valid']                                   = "Get_postalcode_valid";/*AVA*/





$lang['save_successful']                                        = "Enregistré avec succès";/*ACA*/
$lang['records']                                                = "Enregistrements";
$lang['Add Mapping Attributes']                                 = "Ajouter des attributs de mapping";  
$lang['get_configuration']                                      = "Get_configuration";/*AVA*/


$lang['greenNew']                                               = "GreenNew";/*AVA*/
$lang['templates_download']                                     = "Téléchargement de templates";
$lang['download']                                               = "Télécharger";
$lang['delete_only']                                            = "Effacer";
$lang['preview_only']                                           = "Visualiser";
$lang['file_only']                                              = "Fichier";
$lang['new_only']                                               = "Nouveau";
$lang['choose_file']                                            = "Choisir le fichier";
$lang['upload_template']                                        = "Télécharger le template";
$lang['custom_templates']                                       = "Templates personnalisés";
$lang['are_you_sure']                                           = "Êtes-vous sûr ?";

$lang['are_you_sure_insert']                                    = "Insérer votre nom du typede variation : ";
$lang['are_you_sure_replace_profile']                           = "Êtes-vous sûr que vous voulez remplacer le profil {B}{A}{/B}. Sinon, cliquez sur Annuler.";/*ACA*/
$lang['are_you_sure_synchronize_matching']                      = "Êtes-vous sûr que vous voulez correspondre {B}{A}{/B} avec {B}{D}{/B}. Sinon, cliquez sur Annuler.";
$lang['add_update_template']                                    = "Ajouter / Mettre à jour le template";
$lang['ebay_wizard_site_change']                                = "Assistant de changement du site ebay";
$lang['ebay_wizard_finish']                                     = "Terminer l'assistant d'eBay"; 

$lang['ebay_univers_not_selected']                              = "Vous n'avez pas sélectionné une catégorie. Veuillez sélectionner une catégorie en premier.";
$lang['category_not_selected']                                  = "Vous n'avez pas une catégorie sélectionnée. Veuillez sélectionner une catégorie en premier, dans le menu <b>'Mes flux > Paramètres > Catégorie'</b>"; 
$lang['mode']                                                   = "Mode"; 



//Link
$lang['advance_categories_processing']                          = "advance_categories_processing";/*AVA*/
$lang['advance_categories_main_save']                           = "advance_categories_main_save";/*AVA*/

$lang['mode']                                                   = "Mode"; 
$lang['Start Delete']                                           = "Commencer la suppression"; 
$lang['update_actions_success']                                 = "Mise à jour des actions réussie"; 
$lang['update_actions_fail']                                    = "Échec de la mise à jour des actions"; 
$lang['update_orders_success']                                  = "Mise à jour des commandes réussie"; 
$lang['update_orders_fail']                                     = "Échec de la mise à jour des commandes"; 
$lang['update_statistics_success']                              = "Mise à jour des statistiques réussie"; 
$lang['update_statistics_fail']                                 = "Échec de la mise à jour des statistiques"; 
$lang['export_compress']                                        = "Exporter compressé";/*ACA*/
$lang['price_modifier_save']                                    = "Modificateur de prix enregistré";
$lang['setJsonAllCategory']                                     = "Définir Json toutes Catégories"; 
$lang['mapping_templates_save']                                 = "mapping templates enregistré"; 
$lang['mapping_categories_save']                                = "mapping_categories_save";/*AVA*/
$lang['Records']                                                = "Enregistrements"; 
$lang['update_log_success']                                     = "Mise à jour du journal réussie"; 
$lang['update_log_fail']                                        = "Échec de la mise à jour du journal"; 
$lang['log_processing']                                         = "Traitement du journal"; 
$lang['update_log_success']                                     = "update_log_success";/*AVA*/
$lang['log_download']                                           = "log_download";/*AVA*/
$lang['mapping_conditions_save']                                = "mapping_conditions_save";/*AVA*/


// new word (did not verify)
$lang['info_mapping_attributes_value']                          = "Info mapping valeur d'attributs";/*AVA*/
