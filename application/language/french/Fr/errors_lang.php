<?php

$lang['page_title']                         = "Erreur";
$lang['Error Message']                      = "Message d'erreur";
$lang['We found some error, on']            = "Des erreurs ont été trouvées, sur";
$lang['this error has send to admin']       = "Cette erreur a été envoyée à un administrateur";
$lang['We found an error, on']             = "Nous avons trouvé une erreur sur";
$lang['errors']                             = "erreurs";
$lang['getErrorHandler']                    = "getErrorHandler"; /*AVA*/