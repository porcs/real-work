<?php

$lang['page_title']                     = "Place de marché";
$lang['configuration']                  = "Configuration";

$lang['Currency'] = "Devise";
$lang['Save'] = "Enregistrer";
$lang['Reset'] = "Rénitialiser";
//message
$lang['incorrect_url']                  = "URL incorrecte";
$lang['save_unsuccessful']              = "Échec de l'enregistrement.";
$lang['save_successful']                = "Enregistrement avec succès.";

//popup 
$lang['Please select your market places.'] = "Veuillez sélectionner vos places de marché.";
$lang['Continue'] = "Continuer";
$lang['lease select your market place sites.'] = "Veuillez sélectionner vos sites de place de marché.";/*ACA*/
$lang['Back'] = "Retour";
$lang['Save and continue'] = "Enregistrer et continuer";
$lang['Please select your market place site at least once.'] = "Veuillez sélectionner votre site de place de marché au moins une fois.";

$lang['Please select your market place sites.'] = "Veuillez sélectionner vos sites de place de marché.";
$lang['marketplace'] = "Place de marché";
$lang['configuration_data'] = "Données de configuration";
$lang['Congatulations, you have successfully configured your first login.'] = "Félicitations, vous avez réussi à configurer votre première connexion.";
$lang['And, we recommend to configuration your marketplace account and send your items to marketplaces.'] = "Et, nous recommandons de configurer votre compte de place de marché et envoyer vos articles aux places de marché.";
$lang['eBay Wizard'] = "Assistant eBay";
$lang['Close'] = "Fermer";
$lang['Amazon Wizard'] = "Assistant Amazon";
$lang['display_conf_link'] = "Lien de configuration d'affichage";
$lang['configuration_popup'] = "Popup de configuration";
$lang['Do you really want to reset?'] = "Voulez-vous vraiment réinitialiser ?";