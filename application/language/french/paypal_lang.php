<?php

/*
* PayPal Configuration
*/
$lang['cfg_page_title'] = "Configuration du paiement";
$lang['cfg_tab_settings'] = "Paramètres";
$lang['cfg_tab_preapproval'] = "Profil d'approbation préalable PayPal";
$lang['cfg_username'] = "Nom d'utilisateur";
$lang['cfg_email'] = "E-mail";
$lang['cfg_regdate'] = "Inscrit";
$lang['cfg_preapproval'] = "Approbation préalable PayPal";
$lang['cfg_preapproval_detail'] = "Paiements pré-approuvés, dans lesquels un expéditeur se connecte à PayPal et met en place les paiements préautorisés. Les journaux de l'expéditeur dans Paypal.com une fois mise en place la pré-approbation. Une fois que l'expéditeur accepte l'approbation préalable, l'approbation spécifique n'est plus nécessaire.";
$lang['cfg_preapproval_link_text'] = "Voir les détails.";
$lang['cfg_preapproval_key'] = "Clé de pré-approbation";
$lang['cfg_preapproval_active'] = "Active";
$lang['cfg_preapproval_title'] = "Titre de pré-approbation";
$lang['cfg_preapproval_email'] = "E-mail du payeur";
$lang['cfg_preapproval_approved'] = "Apprové";
$lang['cfg_preapproval_maxpay'] = "Nombre maximum de paiements";
$lang['cfg_preapproval_curpay'] = "Paiements actuels";
$lang['cfg_preapproval_remainpay'] = "Paiements restants";
$lang['cfg_preapproval_amountpay'] = "Montant des paiements actuels";
$lang['cfg_preapproval_maxamount'] = "Montant max par paiement";
$lang['cfg_preapproval_maxallamount'] = "Montant total maximum de tous les paiements";
$lang['cfg_preapproval_exp'] = "Date d'expiration";
$lang['cfg_preapproval_date'] = "Date d'approbation";
$lang['cfg_btn_save'] = "Enregistrer les paramètres";
$lang['cfg_message_complete'] = "Enregistrement terminé !";
$lang['finish'] = "Terminer";

/*
* PayPal Preapproval agreement
*/
$lang['pagree_page_title'] = "Préautorisation PayPal";
$lang['pagree_title_name'] = "Titre";
$lang['pagree_title_detail'] = "Paiement de pré-approbation Feed.biz";
$lang['pagree_start_date'] = "Date début";
$lang['pagree_end_date'] = "Date fin";
$lang['pagree_max_per'] = "Montant max par paiement";
$lang['pagree_total'] = "Montant Total";
$lang['pagree_pay_round'] = "Nombre maximum de paiements";
$lang['pagree_btn_agree'] = "Accepter et continuer";
$lang['pagree_btn_disagree'] = "Ne pas accepter";

/*
* PayPal Checkout
*/
$lang['checkout_page_title'] = "Formulaire de paiement";
$lang['checkout_tab_step1'] = "Informations de commande";
$lang['checkout_tab_step2'] = "Informations de facturation";
$lang['checkout_tab_step3'] = "Paiement";
$lang['checkout_tab_step4'] = "Terminé";
$lang['checkout_order_pkg_id'] = "Identifiant du paquet";
$lang['checkout_order_pkg_num'] = "Num";
$lang['checkout_order_pkg_name'] = "Nom du paquet";
$lang['checkout_order_pkg_amount'] = "Montant";
$lang['checkout_order_pkg_total_amount'] = "Montant total";
$lang['checkout_order_pkg_period'] = "Période";
$lang['checkout_order_pkg_pre_no'] = "No";
$lang['checkout_order_pkg_pre_three'] = "3 mois";
$lang['checkout_order_pkg_pre_six'] = "6 mois";
$lang['checkout_order_pkg_pre_nine'] = "9 mois";
$lang['checkout_order_pkg_pre_year'] = "1 année";
$lang['checkout_bill_use'] = "Utiliser l'adresse de facturation du profil d'utilisateur.";
$lang['checkout_pay_method'] = "Choisissez votre méthode de paiement";
$lang['checkout_pay_credit'] = "Payer avec une carte de crédit";
//$lang['checkout_pay_express'] = "Pay with PayPal express checkout";
$lang['checkout_pay_express'] = "Payer seulement cette facture";
//$lang['checkout_pay_preapproval'] = "Pay with PayPal pre-approval";
$lang['checkout_pay_preapproval'] = "Payer automatiquement chaque mois";
$lang['checkout_pay_cc_ccnum'] = "Numéro de carte de crédit";
$lang['checkout_pay_cc_cvv'] = "CVV";
$lang['checkout_pay_cc_firstname'] = "Prénom";
$lang['checkout_pay_cc_lastname'] = "Nom";
$lang['checkout_pay_cc_exp'] = "EXP";
$lang['checkout_pay_cc_street'] = "Rue";
$lang['checkout_pay_cc_city'] = "Ville";
$lang['checkout_pay_cc_state'] = "État";
$lang['checkout_pay_cc_country'] = "Pays";
$lang['checkout_pay_cc_zip'] = "Code postal";
$lang['checkout_pay_cc_month'] = "Mois";
$lang['checkout_pay_cc_year'] = "Année";
$lang['checkout_btn_print'] = "Imprimer";
$lang['checkout_btn_prev'] = "Précédent";
$lang['checkout_btn_next'] = "Suivant";
$lang['checkout_btn_finish'] = "Terminer";
$lang['checkout_confirm'] = "Retirer ce paquet ?";
$lang['checkout_title_alert'] = "Impossible d'effectuer votre paiement.";
$lang['checkout_alert'] = "Cette transaction est refusée.";

/*
* PayPal Complete
*/
$lang['complete_title'] = "Les profils de paquets profil a été enregistré.";
$lang['complete_detail'] = "Vous venez de compléter le profil du paquet.";
$lang['complete_pay_title'] = "Vous venez de terminer votre paiement.";
$lang['complete_pay_detail'] = "Merci pour votre commande. L'identifiant de facturation pour ce paiement est :";
$lang['complete_finish'] = "Merci ! Vos informations ont été enregistrées avec succès !";
$lang['complete_service_name'] = "Service de paquet Feed.biz";

/*
* PayPal History
*/
$lang['history_title'] = "Historique des paiements";
$lang['history_table_datetime'] = "Date/Heure";
$lang['history_table_title'] = "Titre de facturation";
$lang['history_table_status'] = "Statut";
$lang['history_table_method'] = "Mode de paiement";
$lang['history_paypal_credit'] = "Carte de crédit";
$lang['history_paypal_express'] = "Paiement express PayPal";
$lang['history_paypal_preapproval'] = "Pré-approbation PayPal";
$lang['history_paypal_logdetail'] = "Détail de journal";

/*
* PayPal Download 
*/
$lang['downl_list_title'] = "Télécharger la liste de la facture";
$lang['downl_title'] = "Télécharger la facture";
$lang['downl_table_id'] = "Identifiant facture";
$lang['downl_table_title'] = "Détails de la facture";
$lang['downl_table_amount'] = "Montant";
$lang['downl_table_date'] = "Date/Heure";
$lang['downl_table_method'] = "Mode de paiement";
$lang['downl_btn_print'] = "Imprimer la facture";
$lang['downl_bill_active'] = "Date d'activation";
$lang['downl_bill_expire'] = "Date d'expiration";

/*
* Select Packages
*/
$lang['package_title'] = "Paquets";
$lang['package_region_select'] = "Sélectionner la région";
$lang['package_region'] = "Choisir une région";
$lang['package_region_detail'] = "Choisir une région sur laquelle vous souhaitez vendre";
$lang['package_region_currency'] = "Veuillez choisir la devise";
$lang['package_package'] = "Choisir vos paquets";
$lang['package_customize'] = "Personnaliser vos colis";
$lang['package_overview'] = "Vue d'ensemble des colis";
$lang['package_summary'] = "Résumé des colis";
$lang['package_gift'] = "Cadeau code de réduction";
$lang['package_options'] = "Options";
$lang['package_preplanning'] = "Pré-planification de colis";
$lang['package_info'] = "Informations de facturation";
$lang['package_info_title'] = "Entrez les informations suivantes";
$lang['package_payment'] = "Mode de paiement";
$lang['package_complete'] = "Finaliser le paiement";
$lang['package_wizard_region'] = "Région";
$lang['package_wizard_package'] = "Colis";
$lang['package_wizard_info'] = "Informations";
$lang['package_wizard_payment'] = "Paiement";
$lang['package_wizard_complete'] = "Terminer";
$lang['package_btn_send'] = "Envoyer";
$lang['package_offer_title'] = "Offre de colis";
$lang['package_select_referral'] = "Réduction de références";
$lang['package_select_discount_date'] = " Réduction jusqu'au prochain jour de paie : ";
$lang['package_select_subtotal'] = "Total";
$lang['package_select_total'] = "Total";

$lang['currency'] = "Devise";
/*
* Preapproval
*/
$lang['preapproval_inactive'] = "Votre clé de pré-approbation est inactive";
$lang['preapproval_inactive_btn'] = "Changer maintenant.";
$lang['preapproval_active'] = "Votre clé de pré-approbation est disponible.";
$lang['preapproval_no'] = "Vous n'avez pas de clé de pré-approbation, créez-la maintenant.";
$lang['preapproval_date'] = "Nous allons automatiquement vous facturer tous les <b>{ch_date}</b> de chaque mois.";
$lang['express_date'] = "Nous vous informerons lorsque vos colis arrivent à expiration.";						

//bill & payment
$lang['Expired'] = "Expiré";
$lang['Purchased'] = "Acheté";
$lang['per month'] = "par mois";
$lang['New'] = "Nouveau";
$lang['Extend'] = "Étendre";
$lang['days'] = "Jours";
$lang['service'] = "Service";
$lang['packages'] = "Colis";
$lang['complete'] = "Terminer";
$lang['billing'] ="Facturation";
$lang['download'] ="Télécharger";
$lang['amazon'] = "Amazon";
$lang['payment'] = "Paiement";
$lang['Mode'] = "Mode";
$lang['Currency'] = "Devise";

$lang['note_expert_mode_notify'] = "*Veuillez noter que vous utilisez le mode expert. Vous serez facturé un supplément de 5 &euro; par mois.";									
$lang['per/month'] = "per/month";//english
$lang['Show / Hide Columns'] = "Show / Hide Columns";
$lang['Click here to print'] = "Click here to print";
$lang['paybillall'] = "paybillall";
$lang['extend'] = "extend";
$lang['MasterCard'] = "MasterCard";
$lang['Visa'] = "Visa";
$lang['Discover'] = "Discover";
$lang['Amex'] = "Amex";
$lang['January'] = "January";
$lang['February'] = "February";
$lang['March'] = "March";
$lang['April'] = "April";
$lang['May'] = "May";
$lang['June'] = "June";
$lang['July'] = "July";
$lang['August'] = "August";
$lang['September'] = "September";
$lang['October'] = "October";
$lang['November'] = "November";
$lang['December'] = "December";
$lang['images'] = "images";
$lang['transaction'] = "transaction";
$lang['favicon.ico'] = "favicon.ico";
$lang['configuration'] = "configuration";
$lang['preapproval'] = "preapproval";
$lang['agreement'] = "agreement";
$lang['Feed.biz service'] = "Feed.biz service";
$lang['for livetime'] = "for lifetime";
$lang['hove_not_select_pkg'] = "You have not select any packages.";//end