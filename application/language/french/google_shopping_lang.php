<?php 
$lang["page_title"] 	 = "Google shopping";		 //not approved2 ec3
$lang["update_categories_fail"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["update_categories_success"] 	 = "Modifications enregistrées";		 //approved2 ec3
$lang["update_models_fail"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["update_models_success"] 	 = "Modifications enregistrées";		 //approved2 ec3
$lang["update_profiles_fail"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["update_profiles_success"] 	 = "Modifications enregistrées";		 //approved2 ec3
$lang["update_parameter_fail"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["update_parameter_success"] 	 = "Modifications enregistrées";		 //approved2 ec3
$lang["update_conditions_fail"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["update_conditions_success"] 	 = "Modifications enregistrées";		 //approved2 ec3
$lang["categories"] 	 = "categories";		 //not approved2 ec3
$lang["Check all"] 	 = "Sélectionner tout";		 //approved2 ec3
$lang["Choose categories"] 	 = "Choisissez les Catégories";		 //approved2 ec3
$lang["Collapse all"] 	 = "Réduire tout";		 //approved2 ec3
$lang["Customer Service"] 	 = "Customer Service";		 //not approved2 ec3
$lang["Expand all"] 	 = "Développer tout";		 //approved2 ec3
$lang["Expert Mode"] 	 = "Mode Expert";		 //approved2 ec3
$lang["google_shopping"] 	 = "Google Shopping";		 //approved2 ec3
$lang["Messaging"] 	 = "Messaging";		 //not approved2 ec3
$lang["Reply Template"] 	 = "Reply Template";		 //not approved2 ec3
$lang["Save and continue"] 	 = "Enregistrer & Continuer";		 //approved2 ec3
$lang["Seller Review Incentive"] 	 = "Seller Review Incentive";		 //not approved2 ec3
$lang["Send invoice by email"] 	 = "Send invoice by email";		 //not approved2 ec3
$lang["The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized."] 	 = "The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized";		 //not approved2 ec3
$lang["This field is required."] 	 = "This field is required";		 //not approved2 ec3
$lang["Uncheck all"] 	 = "Décocher tout";		 //approved2 ec3
$lang["Condition Mappings"] 	 = "Mappings des Conditions";		 //approved2 ec3
$lang["conditions"] 	 = "conditions";		 //not approved2 ec3
$lang["Feed.biz - Conditions"] 	 = "Feed.biz - Condition";		 //approved2 ec3
$lang["Mapping colors are require"] 	 = "Mapping colors are require";		 //not approved2 ec3
$lang["Please map all attribute color."] 	 = "Please map all attribute color";		 //not approved2 ec3
$lang["Select an option"] 	 = "Sélectionnez une option";		 //approved2 ec3
$lang["feed"] 	 = "feed";		 //not approved2 ec3
$lang["Attribute"] 	 = "Attribut";		 //approved2 ec3
$lang["+ Add a Model"] 	 = "+ Ajouter un modèle";		 //approved1 ec3
$lang["Choose a Product Type option"] 	 = "Choose a Product Type option";		 //not approved2 ec3
$lang["Can not delete"] 	 = "Impossible de supprimer";		 //approved2 ec3
$lang["Choose a Product Universe option"] 	 = "Choose a Product Universe option";		 //not approved2 ec3
$lang["Choose a Specific Field option"] 	 = "Choose a Specific Field option";		 //not approved2 ec3
$lang["Choose the main (Root Node) universe for this model."] 	 = "Choose the main (Root Node) universe for this model";		 //not approved2 ec3
$lang["Choose a Variation option"] 	 = "Choose a Variation option";		 //not approved2 ec3
$lang["Custom label"] 	 = "Custom label";		 //not approved2 ec3
$lang["Choose the product type for this model"] 	 = "Choose the product type for this model";		 //not approved2 ec3
$lang["Default value"] 	 = "Default value";		 //not approved2 ec3
$lang["delete"] 	 = "supprimer";		 //approved2 ec3
$lang["Delete complete"] 	 = "Delete complete";		 //not approved2 ec3
$lang["Do you want to delete"] 	 = "Supprimer";		 //approved2 ec3
$lang["Do not forget to click on the SAVE button at the bottom of the page !"] 	 = "Do not forget to click on the SAVE button at the bottom of the page !";		 //not approved2 ec3
$lang["Do you want to save 'Models'?"] 	 = "Do you want to save 'Models'?";		 //not approved2 ec3
$lang["edit"] 	 = "Modifier";		 //approved2 ec3
$lang["Feature"] 	 = "Caractéristiques";		 //approved2 ec3
$lang["Group by"] 	 = "Group by";		 //not approved2 ec3
$lang["Item(s)"] 	 = "Article(s)";		 //approved2 ec3
$lang["Model names are used for Profiles tab. Please associate a friendly name to remember this model."] 	 = "Model names are used for Profiles tab. Please associate a friendly name to remember this model";		 //not approved2 ec3
$lang["Model name"] 	 = "Nom du Modèle";		 //approved2 ec3
$lang["Model was deleted"] 	 = "Supprimé";		 //approved2 ec3
$lang["models"] 	 = "models";		 //not approved2 ec3
$lang["Please set a require value in model."] 	 = "Please set a require value in model";		 //not approved2 ec3
$lang["Please select the main category for this model"] 	 = "Veuillez sélectionner la Catégorie principale pour ce Modèle";		 //approved2 ec3
$lang["Product Type"] 	 = "Type de Produit";		 //approved2 ec3
$lang["Recommended Fields"] 	 = "Recommended Fields";		 //not approved2 ec3
$lang["Product Universe"] 	 = "Univers Produit";		 //approved2 ec3
$lang["Set Model Name First"] 	 = "Set Model Name First";		 //not approved2 ec3
$lang["Remove a model from list"] 	 = "Remove a model from list";		 //not approved2 ec3
$lang["Some value are require."] 	 = "Certaines valeurs sont obligatoires";		 //approved2 ec3
$lang["Specific Fields"] 	 = "Champs Personnalisés";		 //approved2 ec3
$lang["Variation"] 	 = "Mapping des Variations";		 //approved2 ec3
$lang["Copy URL"] 	 = "Copy URL";		 //not approved2 ec3
$lang["Active"] 	 = "Actif";		 //approved2 ec3
$lang["Export Shipping(carrier) ?"] 	 = "Export Shipping(carrier) ?";		 //not approved2 ec3
$lang["ON"] 	 = "ON";		 //not approved2 ec3
$lang["parameter"] 	 = "parameter";		 //not approved2 ec3
$lang["Setting"] 	 = "Paramètres";		 //approved2 ec3
$lang["This URL use Google Feed"] 	 = "This URL use Google Feed";		 //not approved2 ec3
$lang["URL"] 	 = "URL";		 //approved2 ec3
$lang["ago"] 	 = "ago";		 //not approved2 ec3
$lang["hours"] 	 = "hours";		 //not approved2 ec3
$lang["Notifications"] 	 = "Notifications";		 //not approved2 ec3
$lang["+ Add a profile to the list"] 	 = "Ajouter un profil à la liste";		 //approved1 ec3
$lang["Order"] 	 = "Commande";		 //approved2 ec3
$lang["Allow only digit"] 	 = "Autoriser uniquement les chiffres";		 //approved2 ec3
$lang["Both"] 	 = "Les deux";		 //approved2 ec3
$lang["Both (EAN13 then UPC)"] 	 = "Les deux (EAN-13 puis UPC)";		 //approved2 ec3
$lang["Choose a model"] 	 = "Choisissez un Modèle";		 //approved2 ec3
$lang["Choose one of the following"] 	 = "Choisissez une des options suivantes";		 //approved2 ec3
$lang["Description"] 	 = "Description longue";		 //approved2 ec3
$lang["Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping."] 	 = "Choose the Product Attribute field which will be used for determination of presence of this product on Google Shopping";		 //not approved2 ec3
$lang["Description Field"] 	 = "Description Field";		 //not approved2 ec3
$lang["Export HTML Descriptions instead of Text Only"] 	 = "Export HTML Descriptions instead of Text Only";		 //not approved2 ec3
$lang["EAN13 (Europe)"] 	 = "EAN13 (Europe)";		 //approved2 ec3
$lang["HTML Descriptions"] 	 = "HTML Descriptions";		 //not approved2 ec3
$lang["GTIN Field"] 	 = "GTIN Field";		 //not approved2 ec3
$lang["Manufacturer, Title, Attributes"] 	 = "Fabricant, Titre, Attributs";		 //approved2 ec3
$lang["Manufacturer, Title, Reference, Attributes"] 	 = "Fabricant, Titre, Référence, Attributs";		 //approved2 ec3
$lang["Minimum of quantity required to be in stock to export the product."] 	 = "Quantité minimale en Stock pour exporter le Produit";		 //approved2.1 ec3
$lang["Model"] 	 = "Modèle";		 //approved2 ec3
$lang["Name"] 	 = "Nom";		 //approved2 ec3
$lang["None"] 	 = "Aucun";		 //approved2 ec3
$lang["Out of Stock"] 	 = "Rupture de Stock";		 //approved2 ec3
$lang["Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping."] 	 = "Please select the first alternative (Manufacturer) if your products titles are already formatted for Google Shopping";		 //not approved2 ec3
$lang["Profile names are used for Categories. Please give a friendly name to remember this profile."] 	 = "Profile names are used for Categories. Please give a friendly name to remember this profile";		 //not approved2 ec3
$lang["Remove a profile from list"] 	 = "Remove a profile from list";		 //not approved2 ec3
$lang["profiles"] 	 = "profiles";		 //not approved2 ec3
$lang["Set Profile Name First"] 	 = "Set Profile Name First";		 //not approved2 ec3
$lang["Select a model"] 	 = "Sélectionnez un Modèle";		 //approved2 ec3
$lang["Short Description"] 	 = "Short Description";		 //not approved2 ec3
$lang["Standard Title, Attributes"] 	 = "Standard Title, Attributes";		 //not approved2 ec3
$lang["SKU"] 	 = "SKU";		 //approved2 ec3
$lang["Title Format"] 	 = "Title Format";		 //not approved2 ec3
$lang["Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes."] 	 = "Type of product Description. Google Shopping recommend to format the Product Description to reflect Manufacturer, Title. Attributes";		 //not approved2 ec3
$lang["UPC (United States)"] 	 = "UPC (États Unis)";		 //approved2 ec3
$lang["Whether to send short or long description of the product to Google Shopping."] 	 = "Whether to send short or long description of the product to Google Shopping";		 //not approved2 ec3

  