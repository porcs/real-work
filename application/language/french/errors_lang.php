<?php

$lang['page_title']                         = "Erreur";
$lang['Error Message']                      = "Message d'erreur";
$lang['We found some error, on']            = "Des erreurs ont été trouvées, sur";
$lang['this error has send to admin']       = "Cette erreur a été envoyée à un administrateur";
$lang['We found an error, on']             = "Nous avons trouvé une erreur sur";
$lang['errors']                             = "erreurs";
$lang['getErrorHandler']                    = "getErrorHandler"; /*AVA*/
$lang['Oops...'] = "Oops...";//english
$lang['the page you are looking for cannot be found'] = "the page you are looking for cannot be found";
$lang['Errors can be caused due to various reasons, such as:'] = "Errors can be caused due to various reasons, such as:";
$lang['Page requested does not exist.'] = "Page requested does not exist.";
$lang['Server is down.'] = "Server is down.";
$lang['Internet connection is down.'] = "Internet connection is down.";
$lang['Broken links'] = "Broken links";
$lang['Incorrect URL'] = "Incorrect URL";
$lang['Page has been moved to a different address'] = "Page has been moved to a different address";
$lang['Back'] = "Back";//end