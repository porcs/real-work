<?php

$lang['page_title']                     = "Profils";
$lang['profiles']                       = "Profils";
$lang['my_feeds']                       = "Mes flux";
$lang['parameters']                     = "Paramètres";  
$lang['configuration']                  = "Configuration"; 
$lang['category']                       = "Catégorie"; 

//Message
$lang['save_unsuccessful']              = "Échec de l'enregistrement.";
$lang['save_successful']                = "Enregistrement avec succès.";
$lang['confirm_message']                = "Voulez-vous supprimer ";

$lang['please_enter_your_first_name']       = "Veuillez indiquer votre prénom.";
$lang['please_enter_your_last_name']        = "Veuillez entrer votre nom.";
$lang['please_enter_your_email']            = "Veuillez entrer votre e-mail.";
$lang['please_enter_your_username']         = "Veuillez entrer votre nom d'utilisateur.";
$lang['please_provide_a_password']          = "Veuillez donner un mot de passe";
$lang['your_password_must_be_long']         = "Votre mot de passe doit comporter au moins 6 caractères";
$lang['please_enter_the_same_value_again']  = "Veuillez entrer la même valeur";
$lang['please_agree']                       = "Veuillez accepter notre politique.";
$lang['use profiles mapping'] = "Utiliser le mapping profils";
$lang['Categories'] = "Catégories";
$lang['Choose Categories'] = "Choisir les catégories";
$lang['Expand all'] = "Développer tout";
$lang['Collapse all'] = "Réduire tout";
$lang['Check all'] = "Sélectionner tout";
$lang['Uncheck all'] = "Désélectionner tout";
$lang['You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.'] = 'Vous pouvez sélectionner plusieurs catégories à la fois en cochant la première catégorie puis en appuyant sur "Shift" et en cliquant sur le dernier élément que vous souhaitez inclure dans le même profil.';
$lang['Profiles mapping'] = "Mapping des profils";
$lang['Categories'] = "Catégories";
$lang['Categories'] = "Catégories";
$lang['Prices Mapping'] = "Mapping des prix";
$lang['Add a profile to the list'] = "Ajouter un profil à la liste";
$lang['Profile(s)'] = "Profil(s)";
$lang['Default Profile'] = "Profil par défaut";
$lang['Profile name'] = "Nom de profil";
$lang['Rule'] = "Règle";
$lang['Add Rule Item(s)'] = "Ajouter le(s) article(s) de règle";
$lang['Default Price Modifier'] = "Modificateur de prix par défaut";
$lang['Default Profile'] = "Profil par défaut";
$lang['Do you want to delete'] = "Voulez-vous supprimer";
$lang['Delete successfully'] = "Supprimé avec succès";
$lang['deleted'] = "supprimé";
$lang['can not delete'] = "impossible de supprimer";/*AVA*/ 

$lang['default profiles mapping'] = "Mapping de profils par défaut";
$lang['Use a familiar name to remember it.'] = "Utiliser un nom familier pour s'en souvenir.";
$lang['category_save'] = "Enregistrement de catégorie";
$lang['configuration_save'] = "Enregistrement de configuration";
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";

//new
$lang['Add profile name first'] = "Ajouter un nom de profil en premier";
$lang['Delete successful'] = "Échec de la suppression";
$lang['Deleted'] = "Supprimé";
$lang['Can not delete'] = "ne peut pas supprimer";/*AVA*/
$lang['Profiles Mapping'] = "Mapping de profils";
$lang['Default profiles mapping'] = "Mapping de profils par défaut";
$lang['Choose categories'] = "Choose categories";//english
$lang['Add a profile to the list'] = "Add a profile to the list";
$lang['Remove a profile from list'] = "Remove a profile from list";
$lang['Please enter rule name first'] = "Please enter rule name first";
$lang['Please enter unique rule name'] = "Please enter unique rule name";
$lang['Add Rule Items(s)'] = "Add Rule Items(s)";//end