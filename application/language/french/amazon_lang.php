<?php

$lang['page_title']                         = "Amazon";
$lang['amazon']                             = "Amazon";
$lang['models']                             = "Modèles";
$lang['profiles']                           = "Profils";
$lang['mappings']                           = "Mappings";
$lang['category']                           = "Catégorie";
$lang['parameters']                         = "Paramètres";
$lang['logs']                               = "Journaux";
$lang['carriers']                           = "Transporteurs";
$lang['status']                             = "Statut";
$lang['actions']                            = "Actions";
$lang['conditions']                         = "Conditions";
$lang['Infomation'] = "Infomation";
$lang['This will send all products having a profile within the selected categories in your module configuration.'] 
        ='Ceci enverra tous les produits ayant un profil dans les catégories sélectionnées dans la configuration de votre module.';
$lang['To add a product to the list, from the product sheet, select "create" in the Amazon options tab.']
        ='Pour ajouter un produit à la liste, à partir de la fiche produit, sélectionnez "créer" dans l\'onglet des Options Amazon.';
$lang['Once completed, please generate and save the report. (For support, you will need the XML and the report.).']
        ='Une fois terminé, veuillez générer et enregistrer le rapport. (Pour l\'assistance, vous aurez besoin du XML et du rapport.).';		
$lang['Send products to Amazon.'] = 'Envoyer des produits à Amazon.';	
$lang['Sending..'] = 'Envoi en cours..';/*Point de suspension?*/
$lang['Send offers to Amazon.'] = 'Envoyer des offres à Amazon.';//$lang['Send Offers To Amazon'] = 'Envoyer des offres à Amazon';//not found in using
$lang['Starting update.'] = 'Démarrage de la mise à jour.';//$lang['Starting Update'] = 'Démarrage de la mise à jour';//not found in using
$lang['Delete products on Amazon.'] = "Supprimer des produits sur Amazon.";	//$lang['Delete Products To Amazon'] = "Supprimer des produits sur Amazon";//not found in using
$lang['Delete offers on Amazon.'] = "Supprimer des offres sur Amazon.";	//$lang['Delete Offers To Amazon'] = "Supprimer des offres sur Amazon";//not found in using
$lang['Warning! Configuration parameter is not complete.']
        ="Attention! Le paramétrage de la configuration n\'est pas terminé.";
$lang['Choose Categories'] = 'Choisir des catégories';
$lang['Expand all'] = 'Développer tout';
$lang['Collapse all'] = 'Réduire tout';
$lang['Check all'] = 'Cocher tout';
$lang['Uncheck all'] = 'Décocher tout';
$lang['My feeds > Parameters > Profiles > Category'] = 'Mes flux > Paramètres > Profils > Catégorie';
$lang['You can select multiple categories at once by checking the first category then pressing "Shift" and clicking on the last element that you wish to include in the same profile.']
        = 'Vous pouvez sélectionner plusieurs catégories à la fois en cochant la première catégorie puis en appuyant sur "Shift" et en cliquant sur le dernier élément que vous souhaitez inclure dans le même profil.';
$lang['Profiles Mapping'] = 'Mapping Profils';
$lang['save'] = 'Enregistrer';
$lang['rest'] = 'Réinitialiser';
$lang['Color'] = 'Couleur';
$lang['Batch ID'] = 'Identifiant Lot';
$lang['Feed submission ID'] = 'Identifiant soumission flux';//$lang['Feed Submission Id'] = 'Id soumission flux';//not found in using
$lang['Status code'] = 'Code statut';//$lang['Status Code'] = 'Code Statut';//not found in using
$lang['Messages processed'] = 'Messages traités';//$lang['Messages Processed'] = 'Messages Processed';//not found in using
$lang['Messages successful'] = 'Messages réussis';//$lang['Messages Successful'] = 'Messages Successful';//not found in using
$lang['Messages with error'] = 'Messages avec erreur';//$lang['Messages With Error'] = 'Messages With error';//not found in using
$lang['Messages with Warning'] = 'Messages avec avertissement';//$lang['Messages With Warning'] = 'Messages With Warning';//not found in using
$lang['Send date'] = 'Date d\'envoi';
$lang['Color'] = 'Couleur';

$lang['platform'] = 'platforme';
$lang['Amazon Site'] = 'site Amazon';
$lang['API Setting'] = 'paramètre API';
$lang['Category'] = 'Catégorie';
$lang['Send Product'] = 'Envoyer le produit';
$lang['Select Amazon Site'] = 'Sélectionner le site Amazon';
$lang['Preference'] = 'Préférence';
$lang['Allow automatic offer creation.'] = 'Permettre la création automatique de l\'offre.';
$lang['Synchronize Quantity.'] = 'Synchroniser la quantité.';
$lang['Synchronize Price.'] = 'Synchroniser le prix.';
$lang['Merchant ID'] = 'Identifiant Marchand';
$lang['AWS Key Id'] = 'Identifiant clé AWS';
$lang['AWS Secret Key'] = 'Clé secrète AWS';
$lang['MWS URL for Keypairs'] = 'URL MWS pour les paires de clés';
$lang['Check Connectivity'] = 'Vérifier la connectivité';
$lang['Checking ..'] = 'Vérification ..';
$lang['Prev'] = 'Précédent';
$lang['Next'] = 'Suivant';
$lang['use profiles mapping'] = 'utiliser mapping profils';
$lang['getSelectedAllCategory'] = "getSelectedAllCategory";
$lang['Local'] = "Local";
$lang['product(s)'] = "produit(s)";
$lang['Amazon'] = "Amazon";
$lang['Synchronization successful '] = "Synchronisation réussie ";//$lang['Synchronize Success '] = "Synchronize Success ";//not found in using
$lang['What would you like to do?'] = "Que souhaiteriez-vous faire ?";
$lang['Offers only'] = "Synchroniser uniquement";
$lang['Offers and match'] = "Synchroniser et faire la correspondance";
$lang['Load images'] = "Charger des images"; 
$lang['Display unmatched'] = "Afficher sans correspondance";
$lang['Select All'] = "Sélectionner tout";
$lang['Confirm'] = "Confirmer";
$lang['Reject'] = "Rejeter";
$lang['Finish'] = "Terminer";
$lang['Mismatch'] = "Discordance";
$lang['Brand Mismatch !'] = "Discordance de la marque !";
$lang['Offers on the shop side, but not in your Amazon inventory.'] = "Les offres du côté de la boutique, mais pas dans votre inventaire Amazon.";
$lang['Matching products on Amazon which allows you to create an offer.'] = "Faire la correspondance des produits sur Amazon qui vous permet de créer une offre.";
$lang['Load More'] = "Charger plus";//not found in using
$lang["You don't have products yet on Amazon."] = "Vous n'avez pas encore de produits sur Amazon.";
$lang['Pay now'] = "Payez maintenant";
$lang['get_report_inventory'] = "get_report_inventory";
$lang['matching_products'] = "matching_products";
$lang['You have existing inventory on Amazon.'] = "Vous avez un inventaire existant sur Amazon.";
$lang['API Settings'] = "Paramètres API";
$lang['Enter API credentials (key pairs)'] = "Entrez les informations d'identification de l'API (paires de clés)";// no need to change because in <option>
$lang["doesn't have api setting"] = "n'a pas de paramètre api";// no need to change
$lang['Go to'] = "Aller à";
$lang['Save and continue'] = "Enregistrer et continuer";
$lang['profiles'] = "Profils";
$lang['Add a profile to the list'] = "Ajouter un profil à la liste";
$lang['Profile(s)'] = "Profil(s)";
$lang['Profile Name'] = "Nom du profil";
$lang['Set Profile Name First'] = "Définir en premier le nom du profil";
$lang['Please use a familiar name to remember this profile'] = "Veuillez utiliser un nom familier pour vous rappeler de ce profil";
$lang['Do not forget to click on the SAVE button at the bottom of the page !'] = "N'oubliez pas de cliquer sur le bouton Enregistrer au bas de la page !";
$lang['Out of stock'] = "Rupture de stock";
$lang['Digits only allowed'] = "Seuls les chiffres sont autorisés";
$lang['Minimum quantity of stock to export product'] = "La quantité minimum de stock pour exporter le produit";
$lang['Synchronization field'] = "Champ de synchronisation";
$lang['Choose one of the following'] = "Choisissez une des options suivantes";
$lang['EAN-13 (Europe)'] = "EAN-13 (Europe)";
$lang['UPC (United States)'] = "UPC (États Unis)";
$lang['Both (EAN-13 then UPC)'] = "Les deux (EAN-13 puis UPC)";
$lang['SKU'] = "SKU";
$lang['Choose the Product Attribute field which will be used for determination of presence of this product on Amazon.'] = "Choisissez le champ qui sera utilisé pour traiter avec Amazon";
$lang['Title format'] = "Format du titre";
$lang['Standard title, attributes'] = "Titre standard, attributs";
$lang['Manufacturer, title, attributes'] = "Fabricant, titre, attributs";
$lang['Manufacturer, title, reference, attributes'] = "Fabricant, titre, référence, attributs";
$lang['Type of product name, Amazon recommends to format the title like manufacturer, title, attributes'] = "Type de nom de produit, Amazon recommande de formater le titre comme le fabricant, le titre, les attributs";
$lang['HTML descriptions'] = "Descriptions HTML";
$lang['Yes'] = "Oui";
$lang['Export the HTML descriptions instead of the text only.'] = "Exporter les descriptions HTML au lieu du texte seulement.";
$lang['SKU as the supplier reference.'] = "SKU comme référence de fournisseur.";
$lang['Send the reference/SKU as the supplier reference'] = "Envoyer la référence/SKU comme référence de fournisseur";
$lang['Unconditionnaly'] = "Sans condition";
$lang['Code exemption'] = "Code exemption";
$lang['Use EAN/UPC'] = "Utiliser EAN/UPC";
$lang['Model number'] = "Numéro de modèle";
$lang['Model name'] = "Nom du modèle";
$lang['Manufacturer part number'] = "Référence fabricant";
$lang['Catalog number'] = "Numéro de catalogue";
$lang['Generic'] = "Générique";
$lang['Private label'] = "Marque du distributeur";
$lang['Use EAN/UPC exemption for this profile'] = "Utilisez une exemption EAN/ UPC pour ce profil";
$lang['Concerns'] = "Concerne";
$lang['Select an option'] = "Sélectionnez une option";
$lang['Field'] = "Champ";
$lang['Recommended browse node'] = "Nœud d\'Arborescence Recommandé";
$lang['Browse node ID'] = "Identifiant Nœud d\'Arborescence Amazon";
$lang['This option is mandatory for Canada, Europe, and Japan'] = "Cette option est obligatoire pour le Canada, l'Europe, et le Japon";
$lang['Amazon browse node ID  -   You can use the product classifier to get browse node IDs:'] = "Identifiant Nœud d\'Arborescence Amazon  -   Vous pouvez utiliser le classificateur de produit pour obtenir des identifiants de nœuds d'arborescence:";
 
$lang['Do you want to delete '] = "Voulez-vous supprimer ";
$lang['Delete complete'] = "Suppression terminée";
$lang['Profile was deleted'] = "Le profil a été supprimé";
$lang['Can not delete'] = "Impossible de supprimer";
$lang['No products'] = "Aucun produit";
$lang['Match'] = "Correspondre";
$lang['Choose Amazon site'] = "Choisir le site Amazon";
$lang['Config API setting'] = "Configurer paramètre API";
$lang['Choose category'] = "Choisir la catégorie";
$lang['Sync & match'] = "Synchroniser & correspondre";
$lang['Send to Amazon'] = "Envoyer à Amazon";
$lang['Auto loading offer'] = "Chargement automatique de l'offre";
$lang['Load more'] = "Charger plus";
$lang['send'] = "envoyer";
$lang['ID submission'] = "Identifiant soumission";
$lang['Processed'] = "Traité";
$lang['Success'] = "Succès";
$lang['Error report'] = "Rapport d'erreurs";
$lang['Code'] = "Code";
$lang['Product feed'] = "Flux de produits";
$lang['Inventory feed'] = "Flux d'inventaire";
$lang['Price feed'] = "Flux de prix"; 
$lang['Warning'] = "Avertissement";
$lang['Successful'] = "Succès";
$lang['Submission ID'] = "Identifiant Soumission";
$lang['Action type'] = "Type d'action";
$lang['Feed report'] = "Rapport de flux";
$lang['report'] = "Rapport";
$lang['Offer sending'] = "Envoi d'offre";
$lang['Product sending'] = "Envoi de produit";
$lang['Go to Configuration.'] = "Aller à la configuration.";
$lang['Delete'] = "Supprimer";
$lang['Send'] = "Envoyer";
$lang['actions'] = "Actions"; 
$lang['Select the first option if your product titles are formatted for Amazon.']
                 = "Sélectionnez la première option si les titres des produits sont formatés pour Amazon.";

$lang['save_profiles'] = "enregistrer les profils";
$lang['save_category'] = "enregistrer categorie";
$lang['default profiles mapping'] = "mapping des profils par défaut";
$lang['Please  map all attribute color'] = "Veuillez faire le mapping de tous les attributs de couleur";
$lang['Send'] = "Envoyer";
$lang['Attributes Mapping'] = "Mapping des attributs";
$lang['Is color'] = "Est une couleur";
$lang['Choose an attribute'] = "Choisissez un attribut";
$lang['Choose a color mapping'] = "Choisissez un mapping de couleur";
$lang['Features Mapping'] = "Mapping de caractéristiques";
$lang['Mapping error'] = "Erreur de mapping";
$lang['mapping error'] = "Erreur de mapping";
$lang['Reset'] = "Réinitialiser";
$lang['matching'] = "correspondance";
$lang['Auto load'] = "Charger automatiquement";
$lang['SYNCHRONIZE_AND_MATCH'] = "Synchroniser et faire la correspondance";
$lang['Use key pairs of '] = "Utiliser les paires de clés de ";
$lang['Prices Mapping'] = "Mapping des prix"; 
$lang['Add a model'] = "Ajouter un modèle";
$lang['Model(s)'] = "Modèle(s)";
$lang['Set Model Name First'] = "Définir le nom du modèle en premier";
$lang['Select a familiar name to remember this model.'] = "Sélectionnez un nom familier pour vous rappeler de ce modèle.";
$lang['Product Universe'] = "Univers de produit";
$lang['Choose the main (Root Node) universe for this model.'] = "Choisissez l'univers principal pour ce modèle";
$lang['Product Type'] = "Type de produit";
$lang['Please select the main category for this model'] = "Veuillez sélectionner la catégorie principale pour ce modèle";
$lang['Specific'] = "Specificité";
$lang['Specific Fields'] = "Champs Spécifiques";
$lang['Model was deleted'] = "Le modèle a été supprimé";
$lang['Model'] = "Modèle";
$lang['Select a model'] = "Sélectionnez un modèle";
$lang['Choose a model'] = "Choisissez un modèle";
$lang['Welcome to Amazon synchronization wizard.'] = "Bienvenue à l'assistant de synchronisation d'Amazon.";
$lang['This operation is automatic but could take one hour, please be patient and wait till the process is completed.'] = "Cette opération est automatique, mais pourrait prendre une heure, veuillez être patient et attendez jusqu'à ce que le processus se termine.";
$lang["If you don't want to wait to check on"] = "Si vous ne voulez pas attendre pour vérifier";
$lang['Send results to my email when the process is completed'] = "Envoyer les résultats à mon e-mail lorsque le processus est terminé";
$lang['and click done'] = "et cliquez sur Terminé";
$lang['the module will send the result to your email automatically.'] = "le module envoie le résultat à votre e-mail automatiquement."; 
$lang['Done'] = "Terminé";
$lang['Image Feed'] = "Flux d'images";
$lang['Relationship Feed'] = "flux de relations"; 
$lang['Last Send Date'] = "Dernière date d'envoi";
$lang['creation'] = "création";
$lang['Marked as to be created'] = "Marqué comme \"À être créé\"";
$lang['Option'] = "Option";
$lang['Exclude manufacturer filter'] = "Exclure le filtre fabricant";
$lang['Use only selected carriers'] = "Utiliser uniquement les transporteurs sélectionnés";
$lang['Exclude supplier filter'] = "Exclure le filtre fournisseurs";
$lang['Send image'] = "Envoyer l'image";
$lang['Limit'] = "Limite";
$lang['No limit'] = "Aucune limite";
$lang['Send products to Amazon'] = "Envoyer les produits à Amazon";
$lang['Last Send Date'] = "Dernière date d'envoi"; 
$lang['confirm_products'] = "confirm_products";
$lang['Records'] = "Enregistrements";
$lang['Add Mapping Attributes'] = "Ajouter le mapping attributs";
$lang['Welcome to Amazon Creation wizard.'] = "Bienvenue à l'assistant de création d'Amazon.";
$lang["You don't have a profile setting, please config this tab before sending products to Amazon."] = "Vous n'avez pas de paramètre de profil, veuillez configurer cet onglet avant d'envoyer les produits à Amazon.";//$lang["You don't have profile's setting, please config on this tab before send products to Amazon."] = "You don't have profile's setting, please config on this tab before send products to Amazon.";// not found to using
$lang['None'] = "Aucun";
$lang['Mapping colors are require'] = "Le mapping des couleurs est obligatoire";
$lang['Please map all color attributes.'] = "Veuillez mapper tous les attributs de couleur.";
$lang['Save and continue'] = "Enregistrer et continuer";
$lang["You don't have a profile setting, please config this tab before sending products to Amazon."] = "Vous n'avez pas de paramètre de profil, veuillez configurer cet onglet avant d'envoyer les produits à Amazon.";//$lang["You don't have profile's setting yet, please config on this tab before send products to Amazon."] = "You don't have profile's setting yet, please config on this tab before send products to Amazon.";//not found to using
$lang['form_data'] = "données de formulaire";
$lang['Attribute'] = "Attribut";
$lang['Custom Value'] = "Valeur personnalisée";
$lang['Marked as to be created'] = "Marqué comme \"À être créé\"";  
$lang['Use only selected carriers'] = "Utiliser uniquement les transporteurs sélectionnés"; 
$lang['This will send all the products having a profile and within the selected categories in this site configurations.'] = "Ceci enverra tous les produits ayant un profil et dans les catégories sélectionnées dans les configurations de ce site.";
$lang['If you want to use wizard, please go on the dashboard.'] = "Si vous désirez utiliser l'assistant, veuillez aller au tableau de bord.";//$lang['If you want to use the wizard, please go to the dashboard.'] = "If you want to use the wizard, please go to the dashboard.";// not found to using
$lang['Then, you will see the results in the report..'] = "Ensuite, vous verrez les résultats dans le rapport..";//$lang['After that, you will see result in Report.'] = "After that, you will see result in Report.";//not found to using
$lang['Partial Update'] = "Mise à jour partielle";
$lang['Update offers'] = "Mise à jour des offres";
$lang['Delete Products from Amazon'] = "Supprimer des produits d'Amazon";
$lang['This will send all products with a profile and within the selected categories in this site configurations.'] = "Ceci enverra tous les produits avec un profil et dans les catégories sélectionnées dans les configurations de ce site.";//$lang['This will send all the products having a profile and within the selected categories in this site configurations.'] = "This will send all the products having a profile and within the selected categories in this site configurations.";//not found to use
  
$lang['Update offers'] = "Mettre à jour les offres";
$lang['Delete Products from Amazon'] = "Supprimer des produits d'Amazon";
$lang['To create unknown and unreferenced products on Amazon, please go to the Dashbord and use the "Amazon product creation wizard" in the market place wizard tab.'] = 'Pour créer des produits inconnus et non référencés sur Amazon, veuillez aller au Tableau de Bord et utiliser l\'"assistant de création d\'Amazon" dans l\'onglet de l\'assistant de la place de marché.';//$lang['To create unknown and unreferenced products on Amazon, please go on Dashbord and use "Amazon Creation Wizard" in Marketplace wizard tab.'] = 'To create unknown and unreferenced products on Amazon, please go on Dashbord and use "Amazon Creation Wizard" in Marketplace wizard tab.';//not found to using /*A corriger en Anglais*/
$lang['Conecting to Amazon ..'] = "Connexion à Amazon ..";/*A corriger en Anglais*/
$lang['Synchronize products'] = "Synchroniser les produits";
$lang['Import Orders'] = "Importer les commandes";
$lang['This will import orders from Amazon. For every order you import there will be an order summary and a link to the order in your modules.'] = "Cela importera les commandes d'Amazon. Pour chaque commande que vous importez il y aura un récapitulatif de la commande et un lien vers la commande dans vos modules.";
$lang['Order Date Range'] = "Période de la commande";
$lang['Order Status'] = "État des commandes";
$lang['Import orders'] = "Importer les commandes";
$lang['Are you sure to synchronize product?'] = "Êtes-vous sûr de synchroniser le produit ?";
$lang['Models'] = "Modèles";
$lang['Duplicate From'] = "Dupliquer De";
$lang['Choose a model to duplicate'] = "Choisissez un modèle à dupliquer";
$lang['Variation'] = "Variation";
$lang['Submission Feed ID'] = "Identification de flux de soumission";
$lang['Feed Type'] = "Type de Flux";
$lang['Submission Feed ID'] = "Identification de flux de soumission";
$lang['It is also recommended that you "match" your products with the "Amazon synchronize wizard" in the Dashbord.'] = 'Il est également recommandé que vous "fassiez la correspondance" de vos produits avec l\'« assistant de synchronisation d\'Amazon » dans le Tableau de bord.';//$lang['It is also recommended to try first to "match" your products with Please map all color attributesthe "Amazon Synchronize Wizard" in Dashbord.'] = 'It is also recommended to try first to "match" your products with the "Amazon Synchronize Wizard" in Dashbord.';// not found in using /*A corriger en anglais*/
$lang['Synchronization'] = "Synchronisation"; 

$lang['This will synchronize Amazon depending your stocks moves, within the selected categories in this site configurations and paramerters in My feeds tab.'] = 'Cela va synchroniser Amazon selon vos mouvements de stock, dans les catégories sélectionnées dans les configurations de ce site et paramètres dans l\'onglet Mes flux.';
$lang['checkWS'] = "checkWS";
$lang['get_submission_list'] = "Obtenir la liste de soumission";
$lang['save_mappings'] = "enregistrer les mappings";
$lang['save_data'] = "save_data";
$lang['Please map all attribute color'] = "Veuillez mapper tous les attributs de couleur"; 
$lang['Please map all attribute color.'] = "Veuillez mapper tous les attributs de couleur."; 
$lang['Associated carrier, useful to display your preferred carrier on the order page and on the invoice'] = "Transporteur associé, utile pour afficher votre transporteur préféré sur la page de la commande et sur la facture";
$lang['Choose Carrier'] = "Choisir le transportateur";
$lang['Choose Amazon Carrier'] = "Choisir le transporteur d'Amazon";
$lang['Are you sure to update offers?'] = "Êtes-vous sûr de synchroniser les produits ?";
$lang['Are you sure to get orders?'] = "Êtes-vous sûr d'obtenir les commandes ?";
$lang['Carriers Mapping'] = "Mapping des transporteurs";
$lang['Last Logs'] = "Derniers journaux";
$lang['Action Process'] = "Processs d'action";/*à vérifier*/
$lang['Orders Report'] = "Rapport des commandes";
$lang['Order ID'] = "Identification de la commande";
$lang['Order ID Ref'] = "Réf. Identification de la commande";
$lang['Buyer'] = "Acheteur";
$lang['Payment'] = "Paiement";
$lang['Amount'] = "Montant";
$lang['Order Date'] = "Date de la commande";
$lang['Exported'] = "Exporté"; 
$lang['orders'] = "commandes";
$lang['All'] = "Tout";
$lang['Synchronize'] = "Synchroniser";
$lang['Retrieve all pending orders'] = "Récupérer toutes les commandes en cours";
$lang['Pending - This order is pending in the market place'] = "En attente - cette commande est en attente dans la place du marché";
$lang['Unshipped - This order is waiting to be shipped'] = "Non expédiée - Cette commande est en attente d'expédition";
$lang['Partially shipped - This order was partially shipped'] = "Partiellement expédiée - Cette commande a été partiellement expédiée"; 
$lang['Shipped - This order was shipped'] = "Expédiée - Cette commande a été expédiée";
$lang['Cancelled - This order was cancelled'] = "Annulée - Cette commande a été annulée";
$lang['Wait to process'] = "Attendez pour le traitement";
$lang['get_key_pairs'] = "get_key_pairs";
$lang['Amazon.com :'] = "Amazon.com :";
$lang['Amazon.co.uk : '] = "Amazon.co.uk : ";
$lang['Please map carriers before importing order.'] = "Veuillez mapper les transporteurs avant d'importer la commande.";//$lang['Please mapping carriers before import order.'] = "Please mapping carriers before import order."; // not found to using
$lang['Go to Mappings'] = "Allez aux Mappings";
$lang['Warning!'] = "Avertissement !";
$lang['Warnings'] = "Avertissements"; 
$lang['Send product offers'] = "Envoyer les offres de produits";
$lang['This will synchronize Amazon within the synchronize and created products, depending on how your stock moves. '] = "Cela va synchroniser Amazon avec les produits synchronisés et créés, en fonction des mouvements de votre stock.";// $lang['This will synchronize Amazon depending your stocks moves, within the synchronize and creadted products. '] = "This will synchronize Amazon depending your stocks moves, within the synchronize and creadted products. ";//not found in using
$lang['Sent'] = "Envoyé";
$lang['Size'] = "Taille";
$lang['Amazon Synchronize Wizard'] = "Assistant de synchronisation d'Amazon";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.'] 
        = "Cet assistant va télécharger votre inventaire d'Amazon. L'inventaire sera comparé à votre inventaire local.
Ensuite, l'assistant va marquer comme « À être créé » les produits inconnus sur Amazon, mais présents dans votre base de données. L'objectif est de créer les articles inconnus sur Amazon.";
$lang['Update product offers'] = "Mettre à jour les offres de produit";
$lang['This will update Amazon depending your stocks moves, within the selected categories in your configuration.'] = "Cela va synchroniser Amazon selon les mouvements de votre stock, dans les catégories sélectionnées dans votre configuration.";
$lang['Amazon Creation Wizard'] = "Assistant de synchronisation d'Amazon";
 
$lang['Are you sure to delete product?'] = "Êtes-vous sûr de vouloir supprimer le produit ?";
$lang['Delete Products'] = "Supprimer les produits";
$lang['Delete out of stock products'] = "Supprimer les produits en rupture de stock";
$lang['This will delete all sent products on Amazon.'] = "Cette opération va supprimer tous les produits envoyés sur Amazon.";
$lang['Quantity'] = "Quantité";
$lang['and'] = "et";
$lang['Price'] = "Prix";
$lang['will not send to Amazon.'] = "n'enverra pas à Amazon.";// not found in using
$lang['To synchronize quantity or price please go on'] = "Pour synchroniser la quantité ou le prix, veuillez continuer";
$lang['then check synchronize quantity / synchronize price and save'] = "Vérifiez ensuite synchroniser quantité / synchroniser prix et enregistrez";//not found in using
$lang['Save Profiles & Continue'] = "Enregistrer les profils & continuer";
$lang['This will delete only out of stock products on Amazon.'] = "Ceci va supprimer les produits en rupture de stock d'Amazon.";
$lang['Delete all sent products'] = "Supprimer tous les produits envoyés";
$lang['This will delete all products with a profile and within the selected categories in your configuration.'] = "Cette opération va supprimer tous les produits avec un profil et dans les catégories sélectionnées dans votre configuration.";
$lang['Save Mappings & Continue'] = "Enregistrer les Mappings & Continuer";
$lang['Save Models & Continue'] = "Enregistrer les Modèles & Continuer";
$lang['get_duplicate_model'] = "obtenir une copie du modèle ";
$lang['get_manufacturers'] = "obtenir les fabricants";
$lang['delete_profile'] = "supprimer le profil";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.']
        = "Cet assistant va télécharger votre inventaire d'Amazon. L'inventaire sera comparé à votre inventaire local. Ensuite, l'assistant marquera vos produits inconnus sur Amazon comme « À être créé ». L'objectif est de créer les articles inconnus sur Amazon.";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.Then, the wizard will mark your unknown products on Amazon as to be created. The goal is to create the unknown items on Amazon.'] 
        = "Cet assistant va télécharger votre inventaire d'Amazon. L'inventaire sera comparé à votre inventaire local. Ensuite, l'assistant marquera vos produits inconnus sur Amazon comme « À être créé ». L'objectif est de créer les articles inconnus sur Amazon.";
$lang['Debug mode'] = "Mode de Debug";
$lang['delete_data'] = "Supprimer les données";
$lang['Recommended Fields'] = "Champs recommandés";
$lang['Some values are required.'] = "Certaines valeurs sont obligatoires.";
$lang['Please set a required value in the model.'] = "Veuillez définir une valeur requise dans le modèle.";
$lang['auth_setting'] = "auth_setting";
$lang['downloadLog'] = "downloadLog";
$lang['Feature'] = "Caractéristique";
$lang['Mode'] = "Mode";
$lang['Items(s)'] = "Article(s)";
$lang['Description Field'] = "Champ Description";
$lang['Description'] = "Description";
$lang['Short Description'] = "Description courte";
$lang['Both'] = "Les deux";
$lang['Description field to send to Amazon'] = "Champ Description à envoyer à Amazon";
$lang['Key Product Features'] = "Caractéristiques principales du produit";
$lang['Use Product Features (Recommended)'] = "Utiliser les caractéristiques du produit (recommandé)";
$lang['Add Feature Name before Feature Value (ie: Material: Tissue)'] = "Ajouter le nom de la caractéristique avant la valeur de la caractéristique (ex: Matériel: Tissu)";
$lang['Expand description into bullets points'] = "Développer la description en puces";/*corriger en anglais*/
$lang['Mapping valid value'] = "Valeur de mapping valide";
$lang['Select color'] = "Choisir la couleur";
$lang["Not recommended, result could be unexpected. Use this option if you don't have product features and no other options"] = "Non recommandé, le résultat pourrait être inattendu. Utilisez cette option si vous n'avez pas les caractéristiques du produit et pas d'autres options";
$lang['Do not use key product features (Europe, Japan, and Canada only)'] = "Ne pas utiliser les caractéristiques clés du produit (Europe, Japon et Canada uniquement)";
$lang['save_carriers'] = 'enregistrer les transporteurs';
$lang['Condition Mappings'] = "Mappings de condition";
$lang['Amazon conditions side / Feed.biz conditions side, please associate and map the parameters desired between Amazon and Feed.biz'] = "Côté Conditions d'Amazon / Côté Conditions de Feed.biz, veuillez associer les paramètres souhaités";
$lang['save_conditions'] = "enregistrer les conditions";
$lang['Synchronize Amazon'] = "Synchroniser Amazon";
$lang['Wizard'] = "Assistant";
$lang['to Amazon'] = "à Amazon";
$lang['Amazon Creation'] = "création Amazon";
$lang['Save Mapping & Continue'] = "Enregistrer le mapping & continuer";
$lang['Product marked as to be created is 0.'] = "Produit marqué comme « À être créé » est 0.";
$lang['Statistics'] = "Statistiques";
$lang['Feature'] = "Caractéristique";
$lang['There are no attributes to map'] = "Il n'y a pas d'attribut à mapper";
$lang['Please select category first'] = "Veuillez sélectionner la catégorie en premier";
$lang['Note: Some categories will have no attributes to map'] = "Remarque : Certaines catégories n'auront aucun attribut à mapper";
$lang['Start importing orders'] = "Commencer à importer des commandes";
$lang['get_valid_value'] = "Obtenir une valeur valide";
$lang['Test'] = "Tester";
$lang['CE'] = "CE";
$lang['VCR'] = "VCR";
$lang['specific_fields'] = "Champs spécifiques";
$lang['TotalCoaxialInputs'] = "TotalCoaxialInputs";
$lang['from Amazon'] = "d'Amazon";
$lang['Please wait'] = "Veuillez attendre";
$lang['Processing'] = "Traitement";
$lang['Creation Mode'] = "Mode Création";

$lang['Connecting to Amazon'] = "Connexion à Amazon";
$lang["You don't have"] = "Vous n'avez pas";
$lang['Item Type'] = "Type d'élément";
$lang['setting yet, please config on this tab before sending products to Amazon.'] = "encore en paramètrage, veuillez configurer sur cet onglet avant d'envoyer les produits à Amazon.";
$lang['recommended_data'] = "recommended_data";
$lang['U.S.A. / India Only'] = "U.S.A. / India Uniquement";
$lang['From'] = "De";
$lang['Configure a price rule in values or percentages for one or several price ranges.'] = "Configurer une règle de prix en valeurs ou pourcentages pour une ou plusieurs plages de prix.";
$lang['To'] = "À";
$lang['Two Digits'] = "Deux Chiffres";
$lang['Rounding mode to be applied to the price ((i.e.: price will be equal to 10.17 or 10.20).or rounding is not required.'] = "Mode d'arrondi à appliquer au prix (ex: le prix sera égal à 10,17 ou 10,20).";
$lang['A value must be set for this element'] = "Une valeur doit être définie pour cet élément";
$lang['Invalid price range, price to must be greater than price from.'] = "Plage de prix non valide, prix À doit être supérieur au prix De.";
$lang['Please map this attribute before sending product to Amazon.'] = "Veuillez mapper cet attribut avant d'envoyer le produit à Amazon.";
$lang['Item Type  -   You can use the product classifier to get the item type:']                                                 
     = "Type d'article - Vous pouvez utiliser le classificateur de produit pour obtenir le type d'article :";
$lang['This option is mandatory only for U.S.A. and could be mandatory for India']                                                 
     = "Cette option n'est obligatoire que pour les Etats-Unis et pourrait être obligatoire pour l'Inde";
$lang['This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory.']                                                 
     = "Cet assistant va télécharger votre inventaire d'Amazon. L'inventaire sera comparé à votre inventaire local.";
$lang['(This operation is automatic but could take one hour, please be patient and wait for the processing to be completed.)']                                                 
     = "(Cette opération est automatique, mais pourrait prendre une heure, veuillez être patient et attendre que le traitement soit terminé.)";
$lang['item(s)'] = "article(s)";
$lang['For incoming orders'] = "Pour les commandes entrantes";
$lang['Choose Amazon carrier side'] = "Choisir le côté transporteur d'Amazon";
$lang['Choose Feed.biz carrier side'] = "Choisir le côté transporteur de Feed.biz";
$lang['For Outgoing Orders'] = 'Pour les commandes sortants';


//Report
$lang['Last error from amazon'] = "Dernière erreur sur Amazon";

//Category
$lang['use default price modifier'] = "Utilisez le modificateur de prix par défaut"; 

//message
$lang['save_unsuccessful']                  = "Échec de l'enregistrement";
$lang['save_successful']                    = "Succès de l'enregistrement";

$lang['Merchant Id is missing']                    = 'L\'identifiant marchand est manquant';
$lang['AWS KeyId Id is missing']                   = 'L\'identifiant AWS KeyId est manquant';
$lang['AWS Secret Key Id is missing']              = 'L\'identifiant clé secrète AWS est manquant';
$lang['Market Place Platform is missing']          = 'La plateforme de place de marché est manquante';
$lang['Market Place Currency is missing']          = 'La devise de place de marché est manquante';
$lang['Unable to login']                           = 'Impossible de se connecter';
$lang['Connection to Amazon Failed']               = 'La connexion à Amazon a échoué';
$lang['Connection to Amazon successful']                = 'Vérification vers Amazon réussie';
$lang['No data to save']                           = 'Aucune donnée à enregistrer';
$lang['Nothing to send']                           = 'Rien à envoyer';
$lang['Preparing product to send']                 = 'Préparation des produits à envoyer';
$lang['Preparing inventory to send']               = 'Préparation de l\'inventaire à envoyer';
$lang['Preparing price to send']                   = 'Préparation du prix à envoyer';
$lang['Preparing image to send']                   = 'Préparation de l\'image à envoyer';
$lang['Preparing relationship to send']            = 'Préparation de la relation à envoyer';


//new words (did not verify)
$lang['API settings'] = "Paramètres API";
$lang['Debug'] = "Debug";
$lang['This field is required.'] = "Ce champ est obligatoire.";
$lang['Are you sure to update order shipments?'] = "Êtes-vous sûr de mettre à jour les expéditions de la commande ?";
$lang['Start send products to Amazon'] = "Commencer à envoyer les produits à Amazon";/*à vérifier en anglais*/
$lang['debugs'] = "debugs";
$lang['Debug'] = "Debug";
$lang['Start send products to Amazon'] = "Commencer à envoyer les produits à Amazon"; 
$lang['Please give a friendly name to remind this profile'] = "Veuillez donner un nom convivial pour vous rappeler de ce profil";
$lang['Out of Stock'] = "Rupture de stock";
$lang['Minimum of quantity required to be in stock to export the product.'] = "Quantité minimale en stock pour exporter le produit";
$lang['Title Format'] = "Format du titre";
$lang['Standard Title, Attributes'] = "Titre standard, Attributs";
$lang['Manufacturer, Title, Attributes'] = "Fabricant, Titre, Attributs";
$lang['Manufacturer, Title, Reference, Attributes'] = "Fabricant, Titre, Référence, Attributs";
$lang['Type of product Description. Amazon recommend to format the Product Description to reflect Manufacturer, Title. Attributes.'] = "Type de nom de produit, Amazon recommande de formater le titre comme fabricant, Titre, Attributs";
$lang['Please select the first alternative (Manufacturer) if your products titles are already formatted for Amazon.'] = "Veuillez choisir le premier choix si les titres de vos produits sont déjà formatés pour Amazon";
$lang['HTML Descriptions'] = "Descriptions HTML";

$lang['Export HTML descriptions instead of Text Only'] = "Exporter les descriptions HTML au lieu du texte uniquement";
$lang['Whether to send short or long description of the product to Amazon.'] = "Champ Description à envoyer à Amazon, courte ou longue";
$lang['Recommended Browse Node'] = "Nœud d\'Arborescence Recommandé";
$lang['Browse Node ID'] = "Identifiant Nœud d\'Arborescence Amazon";
$lang['Allow only digit'] = "Permettre uniquement les chiffres";
$lang['Amazon Browse Node ID: You can use the product classifier to get Browse Node IDs: '] = "Identifiant Nœud d\'Arborescence Amazon - Vous pouvez utiliser le classificateur de produit pour obtenir les identifiants de nœuds d\'arborescence:";
$lang['This option is mandatory for Canada, Europe, Japan'] = "Cette option est obligatoire pour le Canada, l'Europe, le Japon";
$lang['Explode Description into Bullets Points'] = "Éclater la description en puces";
$lang["Not recommended, result could be unpredictable.. Use this option only if you don't have product features and no other choice"] = "Non recommandé, le résultat pourrait être inattendu. Utilisez cette option si vous n'avez pas les caractéristiques du produit et pas d'autres choix";
$lang['Do not use key product features (Europe, Japan, Canada only)'] = "Ne pas utiliser les caractéristiques clés du produit (Europe, Japon et Canada uniquement)";
$lang['Price Rules'] = "Règles de prix";
$lang['Default Price Rule'] = "Règle de prix par défaut";
$lang['Percentage'] = "Pourcentage";
$lang['Value'] = "Valeur";
$lang['You should configure a price rule in value or in percentage for one or several prices ranges.'] = "Vous devrez configurer une règle de prix en valeur ou en pourcentage pour une ou plusieurs plages de prix.";
$lang['Rounding'] = "Arrondi";
$lang['One Digit'] = "Un chiffre"; 
$lang['Invalid price range, price to must be more than price from.'] = "Plage de prix non valide, le prix À doit être supérieur au prix De.";
$lang['Select all'] = "Tout sélectionner";
$lang['No items found.'] = "Pas d'objets trouvés.";
 
$lang['Manufacturer'] = "Fabricant";
$lang['Please give a friendly name to remind this model'] = "Veuillez donner un nom convivial pour vous rappeler de ce modèle";
$lang['Some value are require.'] = "Certaines valeurs sont obligatoires.";/*a arranger en anglais*/
$lang['Please set a require value in model.'] = "Veuillez définir une valeur requise dans le modèle.";/*A corriger en anglais*/
$lang['Do you want to delete'] = "Voulez-vous supprimer";
$lang['Synchronization Field'] = "Champ de synchronisation";
$lang['EAN13 (Europe)'] = "EAN13 (Europe)";
$lang['Both (EAN13 then UPC)'] = "Both (EAN13 then UPC)";
$lang['SKU as Supplier Reference'] = "SKU comme référence de fournisseur";
$lang['Code Exemption'] = "Exemption de code"; 
$lang['Manufacturer Part Number'] = "Référence fabricant";
$lang['Catalog Number'] = "Numéro de catalogue";
$lang['Private Label'] = "Marque de distributeur";
$lang['Select an Option'] = "Sélectionnez une option";
$lang['Default Condition Note'] = "Note sur la condition par défaut";
$lang['Short text about product condition / state which will appear on the product details sheet on Amazon.'] = "Texte court sur la condition/état du produit qui apparaîtra sur la fiche produit sur Amazon";
$lang['Latest Updates'] = "Dernières mises à jour";
$lang['Normal Update'] = "Mise à jour normale";
$lang['Cron Update'] = "Mise à jour de cron";
$lang['Update History'] = "Mettre à jour l'historique";
$lang['Validation Logs'] = "Journaux de validation";
$lang['Result'] = "Résultat";
$lang['items to send'] = "articles à envoyer";
$lang['skipped items'] = "articles ignorés";
$lang['Manufacturer'] = "Fabricant";
$lang['Fail'] = "Échouer";/*Pas Failure ?*/
$lang['Skipped'] = "Ignoré";
$lang['Export HTML Descriptions instead of Text Only'] = "Exporter les descriptions HTML au lieu du texte uniquement";
$lang['Send only relationship'] ="Envoyer seulement la relation";
$lang['Update Order Shipments'] = "Mise à jour des expéditions de la commande";
$lang['This will confirm orders shipment to Amazon. The order was Shipped will update to Amazon by clicking the button below.'] = "Ceci va confirmer l'expédition des commandes à Amazon. La commande expédiée sera mise à jour sur Amazon en cliquant sur le bouton ci-dessous.";/*A mettre à jour en anglais*/
$lang['Confirm Order Shipment'] = "Confirmez l'expédition de la commande";
$lang['Other'] = "Autre";
$lang['Associated carrier, useful to display your preferate carrier on the order page and on the invoice '] = "Transporteur associé, utile pour afficher votre transporteur favori sur la page de commande et sur la facture ";
$lang['Model Name'] = "Nom du modèle";
$lang['Model Number'] = "Numéro du modèle";
$lang['Shipped - This order was Shipped'] = "Expédiée - cette commande a été expédiée";
$lang['cron'] = "cron";

$lang['from'] = "de";
$lang['History'] = "Historique";
$lang['success orders'] = "commandes réussies";
$lang['skipped orders'] = "commandes ignorées";
$lang['The'] = "Le";/*Masculin ou féminin*/
$lang['value has mapping automatically'] = "La valeur a été mappée automatiquement";
$lang['This attribute provide the free text.'] = "Cet attribut fournit le texte libre.";
$lang['You can'] = "Vous pouvez";
$lang['mapping a new value'] = "mapping d'une nouvelle valeur";
$lang['use this value'] = "utiliser cette valeur";
$lang['mapping a new value'] = "mapping d'une nouvelle valeur";
$lang['for'] = "pour";
$lang["Do you want to save 'Mappings'?"] = "Voulez-vous enregistrer les 'Mappings'?";
$lang['Send the Reference/SKU as the Supplier Reference'] = "Envoyer la référence/SKU comme référence de fournisseur";
$lang["Do you want to save 'Profiles'?"] = "Voulez-vous enregistrer les 'Profils'?";
$lang["Do you want to save 'Models'?"] = "Voulez-vous enregistrer les 'Modèles'?";
$lang['Tracking No.'] = "N° de suivi";
$lang['Shipping Date'] = "Date d'expédition"; 

/* Features 22/04/2015 */
$lang['features'] = "Features";//english
$lang['Catalog Features'] = "Catalog Features";

// Error Resolution 24/04/2015
$lang['error_resolutions'] = "Error Resolutions";

$lang["Time"] = "Time";
$lang['Action'] = "Action";
$lang['UPDATE OFFERS'] = "UPDATE OFFERS";
$lang["PRODUCT CREATION"] = "PRODUCT CREATION";
$lang["IMPORT ORDERS"] = "IMPORT ORDERS";
$lang["EXPORT ORDERS"] = "EXPORT ORDERS";
$lang["ORDERFULFILLMENT"] = "ORDER FULFILLMENT";
$lang["DELETE"] = "DELETE";
$lang["Date / Time"] = "Date / Time";
$lang["import orders"] = "import orders";
$lang["orders to send"] = "orders to send";
$lang["skipped"] = "skipped";
$lang["Skipped Details"] = "Skipped Details";
$lang["form"] = "form";
$lang["validation_log"] = "validation log";
$lang["get_log"] = "get log";
$lang["report_page"] = "report page";
$lang["Products Creation"] = "Products Creation";
$lang["Create new products on Amazon"] = "Create new products on Amazon";
$lang["Activate import of orders"] = "Activate import of orders";
$lang["Delete Products"] = "Delete Products";
$lang["Activate delete products on Amazon"] = "Activate delete products on Amazon";
$lang["Adjust and format your prices for Amazo"] = "Adjust and format your prices for Amazo";
$lang["Second Hand"] = "Second Hand";
$lang["Sell second hand, collectible or refurbished products"] = "Sell second hand, collectible or refurbished products";
$lang["Expert Mode"] = "Expert Mode";
$lang["Activate a GCID Code Exemption"] = "Activate a GCID Code Exemption";
$lang["SKU as supplier reference"] = "SKU as supplier reference";
$lang["Allow to send (synchronize) stock to amazon"] = "Allow to send (synchronize) stock to amazon";
$lang["doesn't have api setting / inactive"] = "doesn't have api setting / inactive";
$lang["See the setting on"] = "See the setting on";
$lang["fields are required."] = "fields are required.";
$lang["Active"] = "Active";
$lang["Data connection error! Please contact your support, error code:"] = "Data connection error! Please contact your support, error code:";
$lang["Remove a model from list"] = "Remove a model from list";
$lang["Model names are used for Profiles tab. Please associate a friendly name to remember this model."] = "Model names are used for Profiles tab. Please associate a friendly name to remember this model.";
$lang["The product's universe may either require approval or be restricted"] = "The product's universe may either require approval or be restricted";
$lang["Learn more"] = "Learn more";
$lang["Choose the product type for this model"] = "Choose the product type for this model";
$lang["Choose the value in specific fields and then click ADD button (+) to add more fields for this model."] = "Choose the value in specific fields and then click ADD button (+) to add more fields for this model.";
$lang["edit"] = "edit";
$lang["Allow automatic update offer"] = "Allow automatic update offer";
$lang["Automatic update your stock levels on Amazon every hour"] = "Automatic update your stock levels on Amazon every hour";
$lang["Allow automatic export orders to shop"] = "Allow automatic export orders to shop";
$lang["Automatic import of your orders every hour"] = "Automatic import of your orders every hour";
$lang["Allow send status to Amazon"] = "Allow send status to Amazon";
$lang["Automatically create new products on Amazon every day"] = "Automatically create new products on Amazon every day";
$lang["Allow Automatic deletion of products"] = "Allow Automatic deletion of products";
$lang["Automatically delete the selected products on Amazon every days"] = "Automatically delete the selected products on Amazon every days";
$lang["Send orders"] = "Send orders";
$lang["Invoice No."] = "Invoice No.";
$lang["Sending"] = "Sending";
$lang["orders_page"] = "orders";
$lang['Adjust and format your prices for Amazon'] = "Adjust and format your prices for Amazon";
$lang['Allow to send (synchronize) price to amazon'] = "Allow to send (synchronize) price to amazon";
$lang['ON'] = 'ON';
$lang["Remove a profile from list"] = "Remove a profile from list";
$lang["Profile names are used for Categories. Please give a friendly name to remember this profile."] = "Profile names are used for Categories. Please give a friendly name to remember this profile.";
$lang["Amazon.com"] = "Amazon.com";
$lang["Amazon.co.uk"] = "Amazon.co.uk";
$lang["Choose categories"] = "Choose categories";
$lang["The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized."] = "The selected categories will be used when exporting products. Only those products under these categories will be marked as eligible. If you select profiles, these profiles will be used for product creation process. If you don't select profiles the products will be only synchronized.";
$lang["Please choose category in My feeds > Parameters > Category"] = "Please choose category in My feeds > Parameters > Category";
$lang["This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon."] = "This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. The goal is to create your offers on Amazon.";
$lang["Send Relations Only"] = "Send Relations Only";
$lang["This will update Amazon Relations, within the selected categories in your configuration."] = "This will update Amazon Relations, within the selected categories in your configuration.";
$lang["Send Images Only"] = "Send Images Only";
$lang["This will update Amazon Images, within the selected categories in your configuration."] = "This will update Amazon Images, within the selected categories in your configuration.";
$lang["Send Images Only"] = "Send Images Only";
$lang["Starting send images"] = "Starting send images";
$lang["Send Relations Only"] = "Send Relations Only";
$lang["Starting update"] = "Starting update";
$lang["Are you sure to send relations?"] = "Are you sure to send relations?";
$lang["Are you sure to send images?"] = "Are you sure to send images?";
$lang["Adjust and format your prices for Amazon"] = "Adjust and format your prices for Amazon"; //end

//new english
$lang["delete"] = "delete";
$lang["Fields"] = "Fields";
$lang["Please select color."] = "Please select color.";
$lang["Products"] = "Products";
$lang["Starting send relations"] = "Starting send relations";
$lang["Import orders"] = "Import orders";
$lang["Starting import"] = "Starting import";
$lang["create offers"] = "create offers";
$lang["save_parameters"] = "save parameters";
$lang["Marked as to be synchronize"] = "Marked as to be synchronize";
$lang["Synchronize only"] = "Synchronize only";
$lang["Synchronize and match"] = "Synchronize and match";
$lang["Offers on the shop side, but not in your Amazon inventory"] = "Offers on the shop side, but not in your Amazon inventory";
$lang["Matching products on Amazon which allows you to create an offer"] = "Matching products on Amazon which allows you to create an offer";
$lang["Matching Product"] = "Matching Product";
$lang["The goal is to create your offers on Amazon."] = "The goal is to create your offers on Amazon.";
$lang["Amazon product creation"] = "Amazon product creation";
$lang["The goal is to create the unknown items on Amazon."] = "The goal is to create the unknown items on Amazon.";
$lang["none"] = "none";
$lang["product creation"] = "product creation"; 

$lang["Amazon carrier side"] = "Amazon carrier side";
$lang["Feed.biz carrier side"] = "Feed.biz carrier side"; 
 
$lang["Automatic update your order status on Amazon every hour"] = "Automatic update your order status on Amazon every hour";
$lang["Product Creation"] = "Product Creation";
$lang["Alllow automatic creation of New products"] = "Alllow automatic creation of New products";

$lang["save_configuration_features"] = "save configuration features";
$lang["synchronize and match"] = "synchronize and match";
$lang["Amazon - Conditions"] = "Amazon - Conditions";
$lang["Send only image"] = "Send only image";
$lang["Limit product(s)"] = "Limit product(s)";
$lang["success"] = "success"; 

$lang["Search: SKU"] = "Search: SKU";
$lang["Search: Message"] = "Search: Message"; 
$lang["Totals SKU"] = "Totals SKU"; 
$lang["Download Template"] = "Download Template"; 
$lang["Upload file"] = "Upload file"; 
$lang["Uploading file"] = "Uploading file"; 
$lang["Edit Attribute"] = "Edit Attribute"; 
$lang["Override"] = "Override"; 
$lang["Solved"] = "Solved"; 
$lang["Submitting"] = "Submitting"; 
$lang["Delete & Recreate"] = "Delete & Recreate"; 
$lang["Variants"] = "Variants"; 
$lang["Nothing found!"] = "Nothing found!"; 
$lang["Error!"] = "Error!"; 
$lang["error_data"] = "error data"; 
$lang["error_resolution_details"] = "error resolution details";  
$lang["error_edit_data"] = "error edit data";
$lang["EAN/UPC"] = "EAN/UPC";
$lang["Your Override"] = "Your Override";
$lang["Attribute Override"] = "Attribute Override";

$lang["amazon_wizard_instruction_mode_1"] = "This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
                                    After that, the wizard will mark as to be matched the unknown offers present in your database but not on Amazon. 
                                    The goal is to create your offers on Amazon.";

$lang["amazon_wizard_instruction_mode_2"] ="This wizard will download your inventory from Amazon. The inventory will be compared to your local inventory. 
                                    After that, the wizard will mark as to be created the unknown products on Amazon but present in your database. 
                                    The goal is to create the unknown items on Amazon.";


$lang['offers']                            = "Offers";
$lang['products']                          = "Products";
$lang['orders']                            = "Orders";
$lang['relations']                         = "Relations";
$lang['images']                            = "Images";
$lang['delete']                            = "Delete";
$lang['Relations']                            = "Relations";
$lang['Images']                            = "Images";

//end

/* 1/7/2015 */
$lang['Tips']   =  'Conseils';
$lang['Sample'] =  'échantillon';

/* 2/7/2015 */
$lang['required'] = 'Required';
$lang['recommended'] = 'Recommended';