<?php

//Feed Message
$lang['See_more_error_in_Logs']                     = 'Voir plus d\'erreurs dans les journaux';													
$lang['Amazon_Marketplace_Automaton_started_on']    = 'Automate de la place de marché Amazon démarré le';										
$lang['Amazon_s_started_on_s']                      = 'Amazon %s a démarré le %s';
$lang['AmazonS_s_started_on_s']                     = 'Amazon%s %s a démarré le %s';
$lang['Amazon_synchronize_started_on']              = 'Mettre à jour offres Amazon démarrée le'; //'Synchronisation Amazon démarrée le'; //Mettre à jour
$lang['Amazon_s_synchronize_started_on_s']          = 'Mettre à jour offres Amazon%s démarrée le %s'; //'Synchronisation Amazon%s démarrée le %s.';
$lang['Preparing_products']                         = 'Preparation de produits';													
$lang['Sorry_no_items_found_process_completed_No_items_to_be_send'] = /*'Sorry, no items found. Process is completed.*/ 'Aucun élément à envoyer.';
$lang['Products_preparing_successful_start_validation']  = 'Préparation de produits réussie, validation de produits.';								
$lang['completely']                                 = 'Complètement';														
$lang['See_result_in_Report_tab']                   = 'Voir le résultat dans l\'onglet du rapport.';												
$lang['Nothing_to_send']                            = 'Rien à envoyer';
$lang['Sending_s_feed_to_Amazon_s']                 = 'Envoi %s flux à Amazon %s';
$lang['Save_file_xml_error']                        = 'Enregistrer le fichier d\'erreurs xml.';
$lang['Update']                                     = 'Mettre à jour';
$lang['Error']                                      = 'Erreur';
$lang['Getting_Submission_List_ID_s_s']             = 'Obtention de la liste de soumission (ID: %s) %s';	
$lang['Get_submission_List_Error_s_s_s']            = 'Obtenir l\'erreur de liste de soumission: %s (#%s)%s';				
$lang['Getting_Submission_Result_ID_s_s']           = 'Obtention du résultat de la soumission (ID: %s)%s';	
$lang['s_the_process_will_restart_in_s_minutes']    = '%s, le processus redémarrera dans %s minutes.';
$lang['Get_submission_Result_Error_s_s']            = 'Obtenir l\'erreur du résultat de la soumission : %s (#%s)';								
$lang['No_message_from_Amazon']                     = 'Aucun message d\'Amazon.';
$lang['Validation_Error']                           = 'Erreur de validation.';							
$lang['Unable_to_login']                            = 'Impossible de se connecter.';
$lang['Unable_to_connect_Amazon']                   = 'Impossible de se connecter à Amazon.';
$lang['processing']                                 = 'Traitement';
$lang['products']                                   = 'Produits';
$lang['s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'] = '%s produits sont en cours d\'exécution, vous ne pouvez pas exécuter de nouveau ce processus, attendez simplement %s minutes.';
$lang['items_to_xx_with_Amazon']                    = 'Éléments à xx avec Amazon.';
$lang['skipped_items']                              = 'Éléments ignorés.';

//New 26-01-2015 (Error)
$lang['ProductID']                                  = 'Produit ID'; // 20/5/2015
$lang['ProductID(s)']                               = 'Identifiants Produit(%s)';
$lang['Missing_model_in_s_profile_setting']         = 'Modèle manquant dans le paramètre profil %s.';
$lang['Missing_profile_setting']                    = 'Paramètre de profil manquant.';
$lang['Duplicate_entry_for_s_Previously_used_by_Product_ID_s_This_is_not_allowed'] = 'Référence dupliquée pour %s. Déjà utilisé par  : %s Ceci n\'est pas autorisé.';//'Référence dupliquée pour %s. Déjà utilisé par identifiant produit : %s Ceci n\'est pas autorisé.';
$lang['are_inactive'] = 'sont inactifs.';
$lang['Missing_s'] = 'Manquant %s.';
$lang['Missing_reference'] = 'Référence manquante.';
$lang['s_is_incorrect_EAN_UPC'] = '#%s est un EAN/UPC incorrect.';
$lang['Has_No_s'] = 'N\'a pas de %s.';
$lang['Feed_submission_error'] = 'Erreur de soumission de flux.';

//Update Order
$lang['Amazon_s_started_update_order_shipment'] = 'Amazon %s a démarré la mise à jour pour l\'expédition de la commande.';
$lang['Getting_order_shipment_list'] = 'Obtention de la liste du expédition.';
$lang['No_order_update'] = 'Aucune mise à jour de commande.';
$lang['Sending_order_shipment_lists'] = 'Envoi des listes d\'expédition de la commande.';
$lang['Empty_Feedsubmission_Id'] = 'Id. soumission de flux vide.';
$lang['See_result'] = 'Voir résultat.'; //View feed report
$lang['Confirm_Order_Shipment_Processing'] = 'Confirmer le traitement de l\'expédition de la commande.';

// delete product 
$lang['out_of_stock'] = 'Rupture de stock.';
$lang['not_out_of_stock'] = 'Les quantités disponibles pour ce produit.';

//AmazonXmlValidator
$lang['element_was_not_found_on_XSD'] = 'Élément %s non trouvé dans le XSD';
$lang['element_was_not_found_on_generated_XML'] = 'Éléments %s non trouvé dans le XML généré';
$lang['Choice_of_content_could_not_be_set_for_this_Schema'] = 'Choix de contenu ne peut pas être réglé pour ce schéma. Par exemple Produit, inventaire, etc.';
$lang['Type_s_could_not_be_included_in_envelope'] = 'Type: %s ne peut pas être inclus dans l\'enveloppe';
$lang['No_items_found'] = 'Aucun élément trouvé. Produit doit exister au moins: %s Et actuellement %s ont été trouvés';
$lang['element_must_exists_at_least'] = '%s : élément <%s> doit exister au moins: %s. Et actuellement %s ont été trouvés';
$lang['element_must_exists_at_most'] = '%s : élément <%s> doit exister au plus: %s. Et actuellement %s ont été trouvés';
$lang['One_of_these_values_s_must_be_set'] = 'Une de ces valeurs: "%s" doit être défini';
$lang['element_s_must_have_a_s_value_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (vrai/faux). Actuellement un objet est défini.';
$lang['element_s_must_have_a_boolean_value_Currently_value_s'] = '%s : élément: %s doit avoir une valeur booléenne (vrai/faux). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_value_negativeInteger_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (..,-2,-1). Actuellement un objet est défini.';
$lang['element_s_must_have_a_valid_negativeInteger_Currently_value_s'] = '%s : élément: valeur %s doit avoir une valeur entière négative valide (..,-2,-1). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_nonNegativeInteger_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (0,1,2,..). Actuellement un objet est défini.';
$lang['element_s_must_have_a_nonNegativeInteger_Currently_value_s'] = '%s : élément: %s doit avoir une valeur entière négative valide (0,1,2,..). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_nonPositiveInteger_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (..,-2,-1,0). Actuellement un objet est défini.';
$lang['element_s_must_have_a_nonPositiveInteger_Currently_value_s'] = '%s : élément: valeur %s doit avoir une valeur entière non positive (..,-2,-1,0). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_positiveInteger_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (1,2,..). Actuellement un objet est défini.';
$lang['element_s_must_have_a_positiveInteger_Currently_value_s'] = '%s : élément: valeur %s doit avoir une valeur entière positive (1,2,..). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_decimal_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s (..,-2,-1). Actuellement un objet est défini.';
$lang['element_s_must_have_a_decimal_Currently_value_s'] = '%s : élément: %s doit avoir une valeur entière négative valide(..,-2,-1). Valeur actuelle: %s.';
$lang['element_s_must_have_a_s_integer_Currently_an_object_is_set'] = '%s : élément: %s doit avoir une valeur %s. Actuellement un objet est défini.';
$lang['element_s_must_have_a_integer_Currently_value_s'] = '%s : élément: valeur %s doit avoir une valeur entière valide. Valeur actuelle: %s.';
$lang['Value_for_s_is_non_valid'] = '%s : La valeur pour %s n\'est pas valide (selon son format).';
$lang['s_is_not_a_valid_value_se_for_s'] = '%s : "%s" n\'est pas une valeur valide définie pour: %s.';
$lang['s_length_must_be_greater_or_equal_to_s_for_the_element'] = '%s : la longueur de "%s" doit être supérieure ou égale à: %s pour l\'élément: %s';
$lang['s_length_must_be_less_or_equal_to_s_for_the_element'] = '%s : la longueur de "%s" doit être inférieure ou égale à: %s pour l\'élément: %s';
$lang['s_is_invalid_digits_must_be_less_or_equal_to_s_for_the_element'] = '%s : "%s" est invalide, les chiffres doivent être inférieurs ou égal à: %s pour l\'élément: %s';
$lang['s_is_invalid_fraction_digits_must_be_less_or_equal_to_s_for_the_element'] = '%s : "%s" est invalide, les chiffres après la virgule doivent être inférieurs ou égal à: %s pour l\'élément: %s';

// 11/02/2015
$lang['s_length_must_be_greater_or_equal_to_8_and_less_or_equal_to_16_Currently_value_s'] = 'la longueur de "%s" doit être supérieure ou égale à: 8 et inférieure ou égale à: 16. Valeur actuelle: %s';
$lang['s_length_must_be_greater_or_equal_to_1_and_less_or_equal_to_40_Currently_value_s'] = 'la longueur de "%s" doit être supérieure ou égale à: 1 et inférieure ou égale à: 40. Valeur actuelle: %s';
$lang['Skipped'] = "Ignoré";