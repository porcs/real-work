<?php

$lang['profile']  = "Profil";
$lang['billing_information']  = "Informations de facturation";
$lang['logout']  = "Se déconnecter";
$lang['Login']  = "Se connecter";
$lang['sign_up']  = "S'inscrire";
$lang['home']  = "Accueil";
$lang['Beginner'] = "Débutant";
$lang['Expert'] = "Expert";
$lang['Advanced'] = "Advancé";
$lang['Choose shop default'] = "Choisir la boutique par défaut";

//footer
$lang['Loading'] = "Chargement";
$lang['Cannot change shop offer'] = "Impossible de modifier les offres de la boutique";
$lang['Cannot change shop product'] = "Impossible de modifier les produits de la boutique";
$lang['Error'] = "Erreur";
$lang['Loading'] = "Chargement";
$lang['data_was_save'] = "Vos données ont bien été enregistrées.";

//popup step status
$lang['popup_step1'] = "Modifier votre profil";
$lang['popup_step2'] = "Vérifier votre boutique en ligne";
$lang['popup_step3'] = "Définir le modificateur de prix par défaut";
$lang['popup_step4'] = "Paramètres de catégorie"; 
$lang['popup_step5'] = "Configuration de place de marché"; 
$lang['ajax_update_process'] = "ajax_update_process";
$lang['welcome'] = "bienvenue";
$lang['Configuration'] = "Configuration";
$lang['Configurations'] = "Configurations";  
$lang['English'] = "Anglais";
$lang['France'] = "France";
$lang['Type in here'] = "Tapez ici";
$lang['French'] = "French";//english
$lang['Success'] = "Success";
$lang['An error has occurred on language translation'] = "An error has occurred on language translation";
$lang['Completed'] = "Completed";
$lang['Tasks '] = "Tasks ";
$lang['Running'] = "Running";
$lang['No tasks running.'] = "No tasks running.";
$lang['Success'] = "Success";
$lang['language'] = "language";
$lang['Please wait'] = "Please wait";
$lang['Processing...'] = "Processing...";
$lang['Search:'] = 'Search:';
$lang["No data available in table"] = "No data available in table";
$lang["Showing _START_ to _END_ of _TOTAL_ entries"] = "Showing _START_ to _END_ of _TOTAL_ entries";
$lang["Showing 0 to 0 of 0 entries"] = "Showing 0 to 0 of 0 entries";
$lang["(filtered from _MAX_ total entries)"] = "(filtered from _MAX_ total entries)";
$lang["Per page: _MENU_"] = "Per page: _MENU_";
$lang['No matching records found'] = "No matching records found";
$lang['First'] = "First";
$lang['Last'] = "Last";
$lang['Previous'] = "Previous";
$lang['activate to sort column ascending'] = "activate to sort column ascending";
$lang['activate to sort column descending'] = "activate to sort column descending"; 
$lang['View more'] = "View more";


$lang['txt_other_login'] = "Your account has login by another session in other place. Please refresh this page.";
$lang['txt_admin_login'] = "Your account has login by Feed.biz Administrator in other place. Please refresh this page.";
//end