<?php 
$lang["Exporting your orders to "] 	 = "Export de vos Commandes à ";		 //approved1 ec3
$lang["COMPLETED"] 	 = "TERMINÉ";		 //approved1 ec3
$lang["EMPTY ORDERS"] 	 = "Pas de commandes en attente";		 //approved1 ec3
$lang["NO STOCK TO UPDATE"] 	 = "Aucun Stock à Mettre à jour";		 //approved1 ec3
$lang["Product items mismatch"] 	 = "Les Produits ne correspondent pas";		 //approved2 ec3
$lang["Update stock movement to "] 	 = "Mise a jour du mouvement de Stock à";		 //approved1 ec3
$lang["All right reserved."] 	 = "Tous droits réservés";		 //approved2 ec3
$lang["Dear"] 	 = "Cher";		 //approved2 ec3
$lang["Error Detail"] 	 = "Erreur";		 //approved1 ec3
$lang["Here is the order error list"] 	 = "Voici la liste des erreurs concernant les Commandes";		 //approved1 ec3
$lang["Import Order Error"] 	 = "Erreurs : Commandes importées";		 //approved1 ec3
$lang["Marketplace"] 	 = "Marketplace";		 //approved1 ec3
$lang["Marketplace Order ID"] 	 = "ID Commande Marketplace";		 //approved1 ec3
$lang["Order Date"] 	 = "Date";		 //approved2 ec3
$lang["Order ID"] 	 = "ID Commande";		 //approved2 ec3
$lang["Order Status"] 	 = "Statut";		 //approved1 ec3
$lang["Please note that you must use an email to access to Feed.biz management system."] 	 = "Vous aurez à utiliser votre compte, en entrant votre e-mail, pour accéder à Feed.biz";		 //approved2 ec3
$lang["Thanks"] 	 = "Merci";		 //approved2 ec3
$lang["This e-mail was sent to"] 	 = "Cet e-mail a été envoyé à";		 //approved2 ec3

  