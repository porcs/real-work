<?php 
$lang["page_title"] 	 = "stock";		 //not approved2 ec3
$lang["stock"] 	 = "Stock";		 //not approved2 ec3
$lang["report"] 	 = "Rapport";		 //approved2 ec3
$lang["Date"] 	 = "Date";		 //approved1 ec3
$lang["Detail"] 	 = "Détails";		 //approved2 ec3
$lang["Quantity"] 	 = "Quantité";		 //approved2 ec3
$lang["Balance Quantity"] 	 = "Solde";		 //approved1 ec3
$lang["No data available in table"] 	 = "No data available ";		 //not approved2 ec3
$lang["Download CSV"] 	 = "Télécharger CSV";		 //approved1 ec3
$lang["Stock Movement"] 	 = "Mouvement de Stock";		 //approved1 ec3
$lang["Reference"] 	 = "SKU";		 //approved2 ec3
$lang["Movement Quantity"] 	 = "Mouvement de Stock";		 //approved1 ec3
$lang["products"] 	 = "Produits";		 //approved1 ec3
$lang["Next"] 	 = "Suivant";		 //approved2 ec3
$lang["Order from %s (order id:%s)"] 	 = "Commandes de %s (id Commande : %s)";		 //approved1 ec3
$lang["Delete"] 	 = "Supprimer";		 //approved2 ec3
$lang["Product Strategies"] 	 = "Stratégies de Produits";		 //approved2 ec3
$lang["Feed update offer from"] 	 = "Offres mises à jour depuis";		 //approved1 ec3
$lang["Repricing"] 	 = "Paramètres de Repricing";		 //approved2 ec3
$lang["Other"] 	 = "Autre";		 //approved2 ec3
$lang["%s from %s"] 	 = "%s de %s";		 //approved1 ec3
$lang["Service Settings"] 	 = "Paramètres du service";		 //approved2 ec3
$lang["Feed update offer from "] 	 = "Offres mises à jour depuis ";		 //approved1 ec3
$lang["Strategies"] 	 = "Stratégies";		 //approved2 ec3
$lang["Cart Report"] 	 = "Cart Report";		 //not approved2 ec3
$lang["carts"] 	 = "carts";		 //not approved2 ec3
$lang["Carts Report"] 	 = "Carts Report";		 //not approved2 ec3
$lang["Marketplace"] 	 = "Marketplace";		 //approved2 ec3
$lang["Multichannel Orders"] 	 = "Multichannel Orders";		 //not approved2 ec3
$lang["nb_ebay_explode_products"] 	 = "nb_ebay_explode_products";		 //not approved2 ec3
$lang["Order"] 	 = "Commande";		 //approved2 ec3
$lang["Order Date"] 	 = "Date";		 //approved2 ec3
$lang["Order Ref."] 	 = "Order Ref";		 //not approved2 ec3
$lang["Quantity Reserved"] 	 = "Quantity Reserved";		 //not approved2 ec3
$lang["Shipping"] 	 = "Expédition";		 //approved2 ec3

  