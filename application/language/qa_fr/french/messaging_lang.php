<?php 
$lang["page_title"] 	 = "Messaging";		 //not approved2 ec3
$lang["configuration"] 	 = "Configuration";		 //approved2 ec3
$lang["Save"] 	 = "Enregistrer";		 //approved2 ec3
$lang["Reset"] 	 = "Réinitialiser";		 //approved2 ec3
$lang["mail_invoice"] 	 = "Send invoice by email";		 //not approved2 ec3
$lang["incorrect_url"] 	 = "Incorrect url";		 //not approved2 ec3
$lang["save_unsuccessful"] 	 = "Save unsuccessful";		 //not approved2 ec3
$lang["save_successful"] 	 = "Modifications enregistrées";		 //approved2.1 ec3
$lang["create_table_unsuccessful"] 	 = "Create table unsuccessful";		 //not approved2 ec3
$lang["<p>You did not select a file to upload.</p>"] 	 = "Vous n'avez sélectionné aucun fichier à télécharger";		 //approved2 ec3
$lang["no_file"] 	 = "no file";		 //not approved2 ec3
$lang["Sending mail invoice"] 	 = "Sending mail invoice";		 //not approved2 ec3
$lang["Sending mail review"] 	 = "Sending mail review";		 //not approved2 ec3
$lang["Cannot connect to IMAP serveur"] 	 = "Cannot connect to IMAP serveur";		 //not approved2 ec3
$lang["Active"] 	 = "Actif";		 //approved2 ec3
$lang["and to configure the email on"] 	 = "and to configure the email on";		 //not approved2 ec3
$lang["apply the label"] 	 = "apply the label";		 //not approved2 ec3
$lang["are on"] 	 = "are on";		 //not approved2 ec3
$lang["as shown on the picture below"] 	 = "as shown on the picture below";		 //not approved2 ec3
$lang["Customer Service"] 	 = "Customer Service";		 //not approved2 ec3
$lang["customer_thread"] 	 = "customer_thread";		 //not approved2 ec3
$lang["Do not forget to setup the"] 	 = "Do not forget to setup the";		 //not approved2 ec3
$lang["Email Provider"] 	 = "Email Provider";		 //not approved2 ec3
$lang["Email provider. For a better compatibility, <br />we recommend Gmail. If you have any other email provider, <br />please contact our support. &#8220;IMAP&#8221; <br />support has to be configured in email settings."] 	 = "Email provider. For a better compatibility, <br />we recommend Gmail. If you have any other email provider, <br />please contact our support. &#8220;IMAP&#8221; <br />support has to be configured in email settings";		 //not approved2 ec3
$lang["Enter the email account login, for instance ;"] 	 = "Enter the email account login, for instance ;";		 //not approved2 ec3
$lang["Export"] 	 = "Exporter";		 //approved2 ec3
$lang["Filters"] 	 = "Filtres";		 //approved2 ec3
$lang["In Seller Central, you will redirect the &#8220;Customer Service Email&#8221; to your Gmail mailbox"] 	 = "In Seller Central, you will redirect the &#8220;Customer Service Email&#8221; to your Gmail mailbox";		 //not approved2 ec3
$lang["Labels"] 	 = "Labels";		 //not approved2 ec3
$lang["Labels you will have to create on Gmail (Imap Labels)"] 	 = "Labels you will have to create on Gmail (Imap Labels)";		 //not approved2 ec3
$lang["messaging"] 	 = "messaging";		 //not approved2 ec3
$lang["Messaging"] 	 = "Messaging";		 //not approved2 ec3
$lang["My shop > Messaging > Reply Template"] 	 = "My shop > Messaging > Reply Template";		 //not approved2 ec3
$lang["nb_ebay_explode_products"] 	 = "nb_ebay_explode_products";		 //not approved2 ec3
$lang["Note: don&#39;t use the &#8220;notification&#8221; menu, it won&#39;t work. Only the configuration, as shown on the picture above works !"] 	 = "Note: don&#39;t use the &#8220;notification&#8221; menu, it won&#39;t work. Only the configuration, as shown on the picture above works !";		 //not approved2 ec3
$lang["ON"] 	 = "ON";		 //not approved2 ec3
$lang["Password"] 	 = "Mot de passe";		 //approved2 ec3
$lang["Please Choose a Email Provider"] 	 = "Please Choose a Email Provider";		 //not approved2 ec3
$lang["Please note the presence of the asterisk (*@"] 	 = "Please note the presence of the asterisk (*@";		 //not approved2 ec3
$lang["Reply Email Template"] 	 = "Reply Email Template";		 //not approved2 ec3
$lang["Reply Template"] 	 = "Reply Template";		 //not approved2 ec3
$lang["Seller Central"] 	 = "Seller Central";		 //not approved2 ec3
$lang["Seller Review Incentive"] 	 = "Seller Review Incentive";		 //not approved2 ec3
$lang["Send invoice by email"] 	 = "Send invoice by email";		 //not approved2 ec3
$lang["tab."] 	 = "tab";		 //not approved2 ec3
$lang["This feature allows to receive customers messages from Email and answer directly from Customer Service."] 	 = "This feature allows to receive customers messages from Email and answer directly from Customer Service";		 //not approved2 ec3
$lang["You will have to create a filter to apply automatically the suitable label:<br><br>For instance, all messages coming from"] 	 = "You will have to create a filter to apply automatically the suitable label:<br><br>For instance, all messages coming from";		 //not approved2 ec3
$lang["You will repeat the operation for each marketplace (.fr, .de, etc.)"] 	 = "You will repeat the operation for each marketplace (.fr, .de, etc.)";		 //not approved2 ec3
$lang["Activate invoice per email automation."] 	 = "Activate invoice per email automation";		 //not approved2 ec3
$lang["Additionnal File"] 	 = "Additionnal File";		 //not approved2 ec3
$lang["Additionnal PDF file to send as attachment, which could be for example: Terms & Conditions."] 	 = "Additionnal PDF file to send as attachment, which could be for example: Terms & Conditions";		 //not approved2 ec3
$lang["Browse?"] 	 = "Browse?";		 //not approved2 ec3
$lang["Choose the order status which will trigger the invoice sending."] 	 = "Choose the order status which will trigger the invoice sending";		 //not approved2 ec3
$lang["Duplicate SMTP"] 	 = "Duplicate SMTP";		 //not approved2 ec3
$lang["Effect to All Marketplace site."] 	 = "Effect to All Marketplace site";		 //not approved2 ec3
$lang["Encryption"] 	 = "Encryption";		 //not approved2 ec3
$lang["Feed.biz will send the email to the customers with the invoice as an attached document (PDF)."] 	 = "Feed.biz will send the email to the customers with the invoice as an attached document (PDF)";		 //not approved2 ec3
$lang["File template which will be used to announce the invoice."] 	 = "File template which will be used to announce the invoice";		 //not approved2 ec3
$lang["IP address or server name (e.g. smtp.mydomain.com)."] 	 = "IP address or server name (e.g. smtp.mydomain.com)";		 //not approved2 ec3
$lang["is use to add or delete the template for email."] 	 = "is use to add or delete the template for email";		 //not approved2 ec3
$lang["Mail Template"] 	 = "Mail Template";		 //not approved2 ec3
$lang["Manage Additionnal File"] 	 = "Manage Additionnal File";		 //not approved2 ec3
$lang["Manage Mail Template"] 	 = "Manage Mail Template";		 //not approved2 ec3
$lang["Maximum email"] 	 = "Maximum email";		 //not approved2 ec3
$lang["None"] 	 = "Aucun";		 //approved2 ec3
$lang["Only the eligible states are shown (email set to off, invoice to on)."] 	 = "Only the eligible states are shown (email set to off, invoice to on)";		 //not approved2 ec3
$lang["Orders Statuses"] 	 = "Orders Statuses";		 //not approved2 ec3
$lang["Per hour."] 	 = "Per hour";		 //not approved2 ec3
$lang["Please Choose a Additoinal File"] 	 = "Please Choose a Additoinal File";		 //not approved2 ec3
$lang["Please Choose a Mail Template"] 	 = "Please Choose a Mail Template";		 //not approved2 ec3
$lang["Please Choose a Orders Status"] 	 = "Please Choose a Orders Status";		 //not approved2 ec3
$lang["Port"] 	 = "Port";		 //not approved2 ec3
$lang["Port number to use. (gmail use 465)"] 	 = "Port number to use. (gmail use 465)";		 //not approved2 ec3
$lang["Preview"] 	 = "Prévisualiser";		 //approved2 ec3
$lang["SMTP parameters"] 	 = "SMTP parameters";		 //not approved2 ec3
$lang["SMTP server"] 	 = "SMTP server";		 //not approved2 ec3
$lang["SSL mod seems to not be installed on your server. (gmail use SSL)"] 	 = "SSL mod seems to not be installed on your server. (gmail use SSL)";		 //not approved2 ec3
$lang["Starting upload"] 	 = "Envoi en cours";		 //approved2 ec3
$lang["The invoice email will send  by the SMTP you provide."] 	 = "The invoice email will send  by the SMTP you provide";		 //not approved2 ec3
$lang["Upload File"] 	 = "Envoyer le fichier";		 //approved2 ec3
$lang["Upload new file"] 	 = "Upload new file";		 //not approved2 ec3
$lang["Uploaded Additional file"] 	 = "Uploaded Additional file";		 //not approved2 ec3
$lang["Uploaded Mail template"] 	 = "Uploaded Mail template";		 //not approved2 ec3
$lang["You can add or remove files by useing Manage Additionnal File."] 	 = "You can add or remove files by useing Manage Additionnal File";		 //not approved2 ec3
$lang["You can add or remove files by using Manage Mail Template."] 	 = "You can add or remove files by using Manage Mail Template";		 //not approved2 ec3
$lang["Your mail server limits residential to send messages per hour."] 	 = "Your mail server limits residential to send messages per hour";		 //not approved2 ec3
$lang["Activate review incentive per email automation. It'll be asked to the customer to rate its purchase."] 	 = "Activate review incentive per email automation. It'll be asked to the customer to rate its purchase";		 //not approved2 ec3
$lang["Choose the order status which will trigger the action."] 	 = "Choose the order status which will trigger the action";		 //not approved2 ec3
$lang["File template which will be used to ask the customer review."] 	 = "File template which will be used to ask the customer review";		 //not approved2 ec3
$lang["mail_review"] 	 = "mail_review";		 //not approved2 ec3
$lang["Maximum Delay"] 	 = "Maximum Delay";		 //not approved2 ec3
$lang["Opening Days"] 	 = "Opening Days";		 //not approved2 ec3
$lang["The mail server limits residential to send a messages per hour."] 	 = "The mail server limits residential to send a messages per hour";		 //not approved2 ec3
$lang["The review incentive email will send  by the SMTP you provide."] 	 = "The review incentive email will send  by the SMTP you provide";		 //not approved2 ec3
$lang["This is the delay, in business days, the order has been placed and shipped. Over this delay, the incentive mail will not be sent."] 	 = "This is the delay, in business days, the order has been placed and shipped. Over this delay, the incentive mail will not be sent";		 //not approved2 ec3
$lang["get_file_name"] 	 = "get_file_name";		 //not approved2 ec3
$lang["preview_file"] 	 = "preview_file";		 //not approved2 ec3

  