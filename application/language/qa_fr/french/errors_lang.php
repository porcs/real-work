<?php 
$lang["page_title"] 	 = "Erreur";		 //approved2 ec3
$lang["Error Message"] 	 = "Message d'erreur";		 //approved1 ec3
$lang["We found some error, on"] 	 = "Des erreurs ont été trouvées, sur";		 //not approved2 ec3
$lang["this error has send to admin"] 	 = "Cette erreur a été envoyée à l'Assistance";		 //approved1 ec3
$lang["We found an error, on"] 	 = "Nous avons trouvé une erreur, dans";		 //approved1 ec3
$lang["errors"] 	 = "Erreur";		 //approved1 ec3
$lang["getErrorHandler"] 	 = "getErrorHandler";		 //not approved2 ec3
$lang["Oops..."] 	 = "Oups";		 //approved1 ec3
$lang["the page you are looking for cannot be found"] 	 = "La page que vous cherchez n'a pas pu être trouvée";		 //approved1 ec3
$lang["Errors can be caused due to various reasons, such as:"] 	 = "Les erreurs peuvent être dues à diverses raisons, telles que :";		 //approved1 ec3
$lang["Page requested does not exist."] 	 = "La page n'existe pas";		 //approved1 ec3
$lang["Server is down."] 	 = "Le serveur est indisponible";		 //approved1 ec3
$lang["Internet connection is down."] 	 = "Votre connexion Internet n'est pas fonctionnelle";		 //approved1 ec3
$lang["Broken links"] 	 = "Liens brisés";		 //approved1 ec3
$lang["Incorrect URL"] 	 = "URL incorrecte";		 //approved1 ec3
$lang["Page has been moved to a different address"] 	 = "Cette page a été déplacée";		 //approved1 ec3
$lang["Back"] 	 = "Précédent";		 //approved2 ec3

  