<?php 
$lang["Amazon%s Started Order Query"] 	 = "Amazon%s : la requête des Commandes a commencé";		 //approved1 ec3
$lang["Start Get Order List"] 	 = "Début de l'obtention de la liste des commandes";		 //not approved2 ec3
$lang["See all messages"] 	 = "Voir tous les messages";		 //not approved2 ec3
$lang["Getting %s Order List"] 	 = "Obtention de la liste de commandes %s";		 //not approved2 ec3
$lang["Amazon Get Orders Error"] 	 = "Obtenir les erreurs des commandes Amazon";		 //not approved2 ec3
$lang["No pending order for %s to %s"] 	 = "Aucune commande en attente de %s à %s";		 //not approved2 ec3
$lang["Order Query Processing"] 	 = "Traitement de la requête de commandes";		 //not approved2 ec3
$lang["Unable to connect Amazon"] 	 = "Impossible de se connecter Amazon";		 //not approved2 ec3
$lang["Order #%s, Was already imported"] 	 = "Commande #%s, a été déjà importé";		 //not approved2 ec3
$lang["Order #%s, Missing Buyer Address for this order"] 	 = "Commande #%s : adresse de l'acheteur manquante pour cette Commande";		 //approved1 ec3
$lang["Order #%s, Missing Buyer Name for this order"] 	 = "Commande #%s : nom de l'acheteur manquant pour cette Commande";		 //approved1 ec3
$lang["Order #%s, Missing Buyer Email for this order"] 	 = "Commande #%s, : e-mail de l'acheteur manquant pour cette Commande";		 //approved1 ec3
$lang["Order ID (%s) Could not import incomplete order"] 	 = "ID Commande (%s) : Impossible d'importer la Commande car elle est incomplète";		 //approved1 ec3
$lang["Order ID (%s) Item does not have SKU - #%s"] 	 = "ID Commande (%s) : L'article n'a pas de SKU - #%s";		 //approved1 ec3
$lang["Order ID (%s) Could not import product SKU : %s"] 	 = "ID Commande (%s) : Impossible d'importer le Produit avec le SKU : %s";		 //approved1 ec3
$lang["Order ID (%s) Could not add buyer"] 	 = "ID Commande (%s) : Impossible d'ajouter un acheteur";		 //approved1 ec3
$lang["Unable to associate the carrier (%s) for order #%s"] 	 = "Ceci n'est pas une erreur ! Vous devez remplir le Mapping des Transporteurs dans la Configuration. Il n'est pas possible maintenant de trouver le Transporteur (%s) pour la Commande #%s";		 //approved1 ec3
$lang["Unable to execute parameters for order #%s"] 	 = "Impossible d'exécuter les Paramètres pour la Commande #%s";		 //approved1 ec3
$lang["Unable to save order items"] 	 = "Impossible d'enregistrer les articles de la Commande";		 //approved1 ec3
$lang["Success"] 	 = "Succès";		 //approved1 ec3
$lang["inactivate_import_orders"] 	 = "Impossible d'importer les Commandes : Allez à Fonctionnalités pour activer l'import des Commandes";		 //approved1 ec3
$lang["Order #%s, Skipping Cancelled Order"] 	 = "Order #%s, Skipping Cancelled Order";		 //not approved2 ec3
$lang["Product items mismatch"] 	 = "Les Produits ne correspondent pas";		 //approved1 ec3
$lang["on"] 	 = "on";		 //not approved2 ec3

  