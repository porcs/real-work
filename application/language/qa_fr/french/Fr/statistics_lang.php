<?php
$lang['page_title']                     = "Statistiques";
$lang['statistics']                     = "Statistiques";
$lang['products']                       = "Produits";
$lang['manufacturers']                  = "Fabricants";
$lang['categories']                     = "Catégories";
$lang['suppliers']                      = "Fournisseurs";
$lang['carriers']                       = "Transporteurs";
$lang['results_for']                    = "Résultats pour";


//product detail
$lang['information']                    = "Information";
$lang['price']                          = "Prix";
$lang['topology']                       = "Topologie";
$lang['combination']                    = "Déclinaison";
$lang['image']                          = "Image";
$lang['feature']                        = "Caractéristique";
$lang['supplier']                       = "Fournisseur";
$lang['name']                           = "Nom";
$lang['reference']                      = "Référence";
$lang['ean13']                          = "EAN-13";
$lang['upc']                            = "UPC";
$lang['status']                         = "Statut";
$lang['condition']                      = "Condition";
$lang['description']                    = "Description";
$lang['tags']                           = "Mots clés";
$lang['enter_tags']                     = "Entrer des mots clés";
$lang['tax']                            = "Taxe";
$lang['currency']                       = "Devise";
$lang['specific_prices']                = "Prix spécifiques";
$lang['available']                      = "Disponible";
$lang['apply_a_discount_of']            = "Appliquer une réduction de";
$lang['general']                        = "Général";
$lang['width']                          = "Largeur";
$lang['height']                         = "Hauteur";
$lang['depth']                          = "Profondeur";
$lang['weight']                         = "Poids";

