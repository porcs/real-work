<?php

$lang['profile']  = "Profil";
$lang['billing_information']  = "Informations de facturation";
$lang['logout']  = "Se déconnecter";
$lang['Login']  = "Se connecter";
$lang['sign_up']  = "S'inscrire";
$lang['home']  = "Accueil";
$lang['Beginner'] = "Débutant";
$lang['Expert'] = "Expert";
$lang['Advanced'] = "Advancé";
$lang['Choose shop default'] = "Choisir la boutique par défaut";

//footer
$lang['Loading'] = "Chargement";
$lang['Cannot change shop offer'] = "Impossible de modifier les offres de la boutique";
$lang['Cannot change shop product'] = "Impossible de modifier les produits de la boutique";
$lang['Error'] = "Erreur";
$lang['Loading'] = "Chargement";
$lang['data_was_save'] = "Vos données ont bien été enregistrées.";

//popup step status
$lang['popup_step1'] = "Modifier votre profil";
$lang['popup_step2'] = "Vérifier votre boutique en ligne";
$lang['popup_step3'] = "Définir le modificateur de prix par défaut";
$lang['popup_step4'] = "Paramètres de catégorie"; 
$lang['popup_step5'] = "Configuration de place de marché"; 
$lang['ajax_update_process'] = "ajax_update_process";
$lang['welcome'] = "bienvenue";
$lang['Configuration'] = "Configuration";
$lang['Configurations'] = "Configurations";  
$lang['English'] = "Anglais";
$lang['France'] = "France";
$lang['Type in here'] = "Tapez ici";