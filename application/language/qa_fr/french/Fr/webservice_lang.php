<?php

$lang['Update Status Error']                         = "Mettre à jour du statut d'erreur";
$lang['Fail']                                        = "Échec";
$lang['Send order #%s status : %s']                  = "Envoyer la commande #%s statut : %s";
$lang['Order #%s, %s [%s]']                          = "Commande #%s, %s [%s]";
$lang['webservice']                                  = "Webservice";
$lang['send_orders']                                 = "Send_orders";
$lang['ebay']                                        = "eBay";
