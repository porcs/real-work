<?php

$lang['page_title'] = "stock";
$lang['stock'] = "Stock";
$lang['report'] = "Rapport";
$lang['Date'] = "Date";
$lang['Detail'] = "Détail";
$lang['Quantity'] = "Quantité";
$lang['Balance Quantity'] = "Quantité Solde";
$lang['No data available in table'] = "Aucune données disponible dans le tabeau";
