<?php 
$lang["Update Status Error"] 	 = "Mettre à jour du statut d'erreur";		 //not approved2 ec3
$lang["Fail"] 	 = "Échec";		 //approved2 ec3
$lang["Send order #%s status : %s"] 	 = "Envoi de la Commande #%s statut : %s";		 //approved1 ec3
$lang["Order #%s, %s [%s]"] 	 = "Commande #%s, %s [%s]";		 //not approved2 ec3
$lang["webservice"] 	 = "Webservice";		 //not approved2 ec3
$lang["send_orders"] 	 = "Send_orders";		 //not approved2 ec3
$lang["ebay"] 	 = "eBay";		 //approved2 ec3
$lang["missing_order_items"] 	 = "Éléments de la Commande manquants, veuillez importer les Commandes à nouveau";		 //approved1 ec3
$lang["Order_ID"] 	 = "ID Commande";		 //approved2 ec3
$lang["server_side_error"] 	 = "Une erreur côté serveur s'est produite. Veuillez contacter votre support";		 //approved1 ec3

  