<?php

function removeDir($dirPath) {
    if (!is_dir($dirPath)) {
        throw new Exception("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            removeDir($file);
        } else {
            @unlink($file);
        }
    }
    @rmdir($dirPath);
}

function recurse_copy($src, $dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ( $file = readdir($dir))) {
        if (( $file != '.' ) && ( $file != '..' ) && ( $file != '.svn' )) {
            if (is_dir($src . '/' . $file)) {
                recurse_copy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function str2mysqltime($str,$format = 'Y-m-d H:i:s'){
	$date = '';
	$date = date ( $format, strtotime ( $str ) );
	if (date ( 'Y-m-d', strtotime ( $date ) ) <= '1970-01-01') {
		$str = str_replace ( '/', '-', $str );
		$date = date ( $format, strtotime ( $str ) );
		if (date ( 'Y-m-d', strtotime ( $date ) ) <= '1970-01-01') {
			$str = str_replace ( '-', '/', $str );
			$date = date ( $format, strtotime ( $str ) );
		}
	}

	return $date;
}

function is_date($str){
	if (!empty($str) && trim($str) != ''){
		$str = str2mysqltime($str,'Y-m-d');
		if ($str <= '1970-01-01'){
			return false;
		}else{
			return true;
		}
	}else{
		return false;
	}
}

function f_sinit($conf) {
    $CI = & get_instance();
    $CI->load->model('auth');
    $CI->auth->checkBrowsePermission();

    return $conf;
}

function f_global_access($conf){
	
	if (isset($_GET['com_id'])){
		$CI = & get_instance();
		$CI->load->model('auth');
		$conf['global_access'] = $CI->auth->isGlobalAccess();
	}

	return $conf;
}

function addPasswordConfirmElement($element) {
    $tmp = array();
    foreach ($element as $k => $v) {
        if (isset($_REQUEST['key']) && $k == 'users.user_name') {
            $v['element'][1]['readonly'] = "readonly";
        }

        $tmp[$k] = $v;
        if ($k == 'users.user_password') {
            $tmp['users.user_password_confirm'] = Array(
                'alias' => 'User confirm password ',
                'element' => Array(
                    0 => 'password',
                    1 => Array(
                        'style' => 'width:210px;'
                    )
                )
            );
        }
    }
    $element = $tmp;

    return $element;
}

function passwordConfirmValidate($validate) {
    if (isset($_GET['xtype']) && $_GET['xtype'] != 'update') {
        $validate['users.user_password_confirm'] = array('rule' => 'notEmpty',
            'message' => 'Please enter the value for User confirm password .');
    }
    return $validate;
}

function comparePassAndConfirmPass($error) {
    $CI = & get_instance();
    $data = $CI->input->post('data');
    if (!empty($data['users']['user_password']) &&
            !empty($data['users']['user_password_confirm'])) {
        if ($data['users']['user_password'] != $data['users']['user_password_confirm']) {
            $error['users.user_password'][] = 'User password doesn\'t match User confirm password ';
            $error['users.user_password_confirm'] = array();
        }
    }

    return $error;
}

function encryptPassword($data) {
    if(isset($data['users']['user_password']) && !empty($data['users']['user_password']) && trim($data['users']['user_password']) != ''){
    $data['users']['user_password'] = sha1($data['users']['user_password']);
    }else{
        if(isset($data['users']['user_password'])) unset($data['users']['user_password']);
    }

    return $data;
}

function checkUser($error) {
    $CI = & get_instance();
    $key = $CI->input->post('key');
    $data = $CI->input->post('data');
    if (empty($key)) {
        $CI->db->select('*');
        $CI->db->from('users');
        $CI->db->where('user_name', $data['users']['user_name']);

        $query = $CI->db->get();
        $rs = $query->row_array();

        if (!empty($rs)) {
            $error['users.user_name'][] = 'Someone already has that username. Try another? ';
        }
    }

    return $error;
}

function removeElement($element) {
    unset($element['users.user_name']);
    unset($element['users.user_password']);
    unset($element['users.group_id']);
    unset($element['users.user_status']);

    return $element;
}

function removeElementData($data) {

    if (isset($data['users']['user_name'])) {
        unset($data['users']['user_name']);
    }
    if (isset($data['users']['user_password'])) {
        unset($data['users']['user_password']);
    }
    if (isset($data['users']['group_id'])) {
        unset($data['users']['group_id']);
    }

    return $data;
}

function removeValidate($validate) {
    unset($validate['users.user_name']);
    unset($validate['users.user_password']);
    unset($validate['users.group_id']);

    return $validate;
}

function completeUpdate($data) {
    redirect('/user/editprofile');
}

function validate_language_code($error){
	$CI = & get_instance();
	if (empty($_POST['key'])) {
		$CI->db->select('*');
		$CI->db->from('languages');
		$CI->db->where('language_code',trim($_POST['data']['languages']['language_code']));
		$query = $CI->db->get();
		$rs = $query->row_array();

		if (!empty($rs)) {
			$error['languages.language_code'][] = $CI->lang->line('language_code_is_existed');
		}

		if (!is_writable(FCPATH.'application/language')) {
			$error['error_directory_write'][] = sprintf($CI->lang->line('directory_is_not_allowed_write'), FCPATH.'application/language');
		}
	}
	return $error;
}

function edit_form($element){
	$tmp = array();
	foreach ($element as $k => $v) {
		if (isset($_REQUEST['key']) && $k == 'languages.language_code'){
			$v['element'][1]['readonly'] = "readonly";
		}
		$tmp[$k] = $v;
	}
	$element = $tmp;

	return $element;
}

function before_update($data){
	if (isset($data['languages']['language_code'])){
		unset($data['languages']['language_code']);
	}

	return $data;
}

function complete_save($data){
	if (empty($_POST['key'])) {
		if (!is_dir(FCPATH.'application/language/'.$data['languages']['language_code'])) {
			$oldumask = umask(0);
			mkdir(FCPATH.'application/language/'.$data['languages']['language_code']);
			umask($oldumask);
		}
		if (!file_exists(FCPATH.'application/language/'.$data['languages']['language_code'].'/message_lang.php')){
			$oldumask = umask(0);
			$fcontent = file_get_contents(FCPATH.'application/language/default/message_lang.php');
			file_put_contents(FCPATH.'application/language/'.$data['languages']['language_code'].'/message_lang.php', $fcontent);
			umask($oldumask);
		}
	}

}

function complete_delete($data){
	if (!empty($data['languages']['language_code']) && trim($data['languages']['language_code']) != ''){
		if (is_dir(FCPATH.'application/language/'.$data['languages']['language_code'])) {
			removeDir(FCPATH.'application/language/'.$data['languages']['language_code']);
		}
	}
}