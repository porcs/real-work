<?php

require_once dirname(__FILE__)."/../../libraries/Ebay/controllers/mappingFeatures.php";

class mappingFeaturesTest extends PHPUnit_Framework_TestCase
{
    protected static $dt;
    protected static $class;
    
    public static function setUpBeforeClass()
    {
        self::$dt = parse_ini_file(dirname(__FILE__).'/../config.ini');
        self::$class = new mappingFeatures(self::$dt);
    }

    
    public function testGet()
    {
//        $this->assertEquals("AAA", self::$class->get());
        $this->assertEquals("AAA", "AAA");
    }
}
