<?php

require_once dirname(__FILE__)."/../../libraries/Ebay/models/feedModel.php";

class feedModelTest extends PHPUnit_Framework_TestCase
{
    private $ci;
    
    protected static $dt;
    protected static $class;
    
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invoke(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    public static function setUpBeforeClass()
    {
        self::$dt = parse_ini_file(dirname(__FILE__).'/../config.ini');
    }

    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetTemplate()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('template' => 'SELECT column FROM ebay');
//        $expact = 'SELECT column FROM ebay';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getTemplate', array(array('template' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getTemplate', array($test)));
//    }
//
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetColumn()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('column' => 'id_site');
//        $expact = 'id_site';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getColumn', array(array('column' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getColumn', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetWhere()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('where' => 'WHERE id_site = "::ID_SITE::"');
//        $expact = 'WHERE id_site = "::ID_SITE::"';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getWhere', array(array('where' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getWhere', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetGroupby()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('groupby' => 'GROUP BY id_category');
//        $expact = 'GROUP BY id_category';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getGroupby', array(array('groupby' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getGroupby', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetOrderby()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('orderby' => 'ORDER BY id_category ASC');
//        $expact = 'ORDER BY id_category ASC';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getOrderby', array(array('orderby' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getOrderby', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetLimit()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('limit' => 'LIMIT 10');
//        $expact = 'LIMIT 10';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getLimit', array(array('limit' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getLimit', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetOffset()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test   = array('offset' => 'OFFSET 10');
//        $expact = 'OFFSET 10';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(0)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(1)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(false)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(true)));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(array())));
//        $this->assertEquals(null, $this->invoke($stub, 'getOffset', array(array('offset' => ''))));
//        $this->assertEquals($expact, $this->invoke($stub, 'getOffset', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetPath()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $path = '/application/libraries/Ebay/models/bin/';
//        $name = 'mappingFeatures';
//        $ext  = '.ini';
//        
//        $expact = realpath(DIR_SERVER . '/application/libraries/Ebay/models/bin/mappingFeatures.ini');
//
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array()));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array('')));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array('', '')));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array('', null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array(null, '')));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array(null, null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getPath', array($path, $name, '.php')));
//        $this->assertEquals($expact, $this->invoke($stub, 'getPath', array($path, $name, $ext)));
//        $this->assertEquals($expact, $this->invoke($stub, 'getPath', array($path, $name)));
//        
//        return $expact;
//    }
//
//    /**
//     * @depends testGetPath
//     */
//    ////////////////////////////////////////////////////////////////////////////
//    public function testGetConfig($path)
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $expact = array(
//            'config' => array(
//                'template' => 'mapping_category',
//                'column' => array(
//                    'id_ebay_category'
//                ),
//                'where' => array(
//                    'id_site' => '::ID_SITE::',
//                    'id_shop' => '::ID_SHOP::'
//                )
//            ),
//            'current' => array(
//                'value' => 1
//            ),
//            'key' => array(
//                'value' => 1
//            )
//        );
//        
//        $this->assertEquals(null, $this->invoke($stub, 'getConfig', array()));
//        $this->assertEquals(null, $this->invoke($stub, 'getConfig', array(null)));
//        $this->assertEquals(null, $this->invoke($stub, 'getConfig', array('')));
//        $this->assertEquals($expact, $this->invoke($stub, 'getConfig', array($path)));
//    }
//    
//    /**
//     * @depends testGetPath
//     */
//    ////////////////////////////////////////////////////////////////////////////
//    public function testValidate($path)
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $expact = array(
//            'config' => array(
//                'template' => 'mapping_category',
//                'column' => array(
//                    'id_ebay_category'
//                ),
//                'where' => array(
//                    'id_site' => '::ID_SITE::',
//                    'id_shop' => '::ID_SHOP::'
//                )
//            ),
//            'current' => 1,
//            'key' => 1
//        );
//        
//        $expactDefault = array(
//            'config' => false,
//            'current' => false,
//            'key' => false
//        );
//        
//        $expactConfig = array(
//            'config' => array(
//                'template' => 'mapping_category',
//                'column' => array(
//                    'id_ebay_category'
//                ),
//                'where' => array(
//                    'id_site' => '::ID_SITE::',
//                    'id_shop' => '::ID_SHOP::'
//                )
//            ),
//            'current' => false,
//            'key' => false
//        );
//        
//        $testCurrent = array(
//            'config' => array(
//                'template' => 'mapping_category',
//                'column' => array(
//                    'id_ebay_category'
//                ),
//                'where' => array(
//                    'id_site' => '::ID_SITE::',
//                    'id_shop' => '::ID_SHOP::'
//                )
//            ),
//            'current' => array(
//                'value' => 1
//            ),
//            'key' => array(
//                'value' => 1
//            )
//        );
//        
//        $this->assertEquals($expactDefault, $this->invoke($stub, 'validate', array()));
//        $this->assertEquals($expactConfig, $this->invoke($stub, 'validate', array($expactConfig)));
//        $this->assertEquals($expact, $this->invoke($stub, 'validate', array($testCurrent)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testTemplate()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $expact = 'SELECT column FROM ebay_mapping_category where groupby orderby limit offset';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'template', array()));
//        $this->assertEquals(null, $this->invoke($stub, 'template', array('mapping')));
//        $this->assertEquals($expact, $this->invoke($stub, 'template', array('mapping_category')));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testColumn()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test = array('id_ebay_category');
//        $expact = 'id_ebay_category';
//        
//        $testKey = array('id_category' => 'id_ebay_category');
//        $expactKey = 'id_ebay_category AS id_category';
//        
//        $this->assertEquals('*', $this->invoke($stub, 'column', array()));
//        $this->assertEquals($expact, $this->invoke($stub, 'column', array($test)));
//        $this->assertEquals($expactKey, $this->invoke($stub, 'column', array($testKey)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testWhere()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test = array('id_site' => 71, 'id_shop' => 1);
//        $expact = 'WHERE id_site = "71" AND id_shop = "1"';
//        
//        $test1 = array('id_site' => "71", 'id_shop' => "1");
//        $expact1 = 'WHERE id_site = "71" AND id_shop = "1"';
//        
//        $test2 = array('id_site' => 71);
//        $expact2 = 'WHERE id_site = "71"';
//        
//        $this->assertEquals('', $this->invoke($stub, 'where', array()));
//        $this->assertEquals($expact, $this->invoke($stub, 'where', array($test)));
//        $this->assertEquals($expact1, $this->invoke($stub, 'where', array($test1)));
//        $this->assertEquals($expact2, $this->invoke($stub, 'where', array($test2)));
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testInitTemplate()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $expact = 'SELECT column FROM ebay_mapping_category where groupby orderby limit offset';
//        
//        $this->assertEquals($expact, $this->invoke($stub, 'init', array('mapping_category', 'template')));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testInitColumn()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//        
//        $test = array('id_ebay_category');
//        $expact = 'id_ebay_category';
//        
//        $testKey = array('id_category' => 'id_ebay_category');
//        $expactKey = 'id_ebay_category AS id_category';
//        
//        $this->assertEquals($expact, $this->invoke($stub, 'init', array(array('id_ebay_category'), 'column')));
//        $this->assertEquals($expactKey, $this->invoke($stub, 'init', array(array('id_category' => 'id_ebay_category'), 'column')));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testCombineSQL()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//
//        $test = array(
//            'template' => 'SELECT column FROM ebay_mapping_category where groupby orderby limit offset',
//            'column' => 'id_ebay_category',
//            'where' => 'WHERE id_site = "::ID_SITE::" AND id_shop = "::ID_SHOP::"'
//        );
//        
//        $expact = 'SELECT id_ebay_category FROM ebay_mapping_category WHERE id_site = "::ID_SITE::" AND id_shop = "::ID_SHOP::"';
//        
//        $this->assertEquals(null, $this->invoke($stub, 'combineSQL', array()));
//        $this->assertEquals($expact, $this->invoke($stub, 'combineSQL', array($test)));
//    }
//    
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testCombineWhere()
//    {
//        $stub = $this->getMockBuilder('feedModel')
//                ->setConstructorArgs(array(self::$dt))
//                ->setMethods(null)
//                ->getMockForAbstractClass();
//
//        $test = 'SELECT id_ebay_category FROM ebay_mapping_category WHERE id_site = "::ID_SITE::" AND id_shop = "::ID_SHOP::"';
//        $expact = 'SELECT id_ebay_category FROM ebay_mapping_category WHERE id_site = "71" AND id_shop = "1"';
//        
//        $config = array(
//            'template' => 'mapping_category',
//            'column' => array(
//                'id_ebay_category'
//            ),
//            'where' => array(
//                'id_site' => '::ID_SITE::',
//                'id_shop' => '::ID_SHOP::'
//            )
//        );
//        
//        $this->assertEquals(null, $this->invoke($stub, 'combineWhere', array()));
//        $this->assertEquals($expact, $this->invoke($stub, 'combineWhere', array($config, self::$dt, $test)));
//    }
}
