<?php

//require_once dirname(__FILE__)."/../../controllers/eBay/eBayController.php";

//class eBayControllerTest extends PHPUnit_Framework_TestCase
//{    
//    public $class_name = 'eBayController';
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testNotNull()
//    {
//        $eBay = $this->getMockBuilder($this->class_name)
//                ->disableOriginalConstructor()
//                ->setMethods(null)
//                ->getMock();
//
//        $this->assertEquals(0, $eBay->not_null(null));
//        $this->assertEquals(1, $eBay->not_null(true));
//        $this->assertEquals(0, $eBay->not_null(false));
//        $this->assertEquals(1, $eBay->not_null(1));
//        $this->assertEquals(0, $eBay->not_null(0));
//        $this->assertEquals(1, $eBay->not_null(array('eBayControllerTest')));
//        $this->assertEquals(0, $eBay->not_null(array()));
//        $this->assertEquals(1, $eBay->not_null('1'));
//        $this->assertEquals(0, $eBay->not_null(''));
//    }
//    
//    ////////////////////////////////////////////////////////////////////////////
//    public function testCamelName()
//    {
//        $eBay = $this->getMockBuilder($this->class_name)
//                ->disableOriginalConstructor()
//                ->setMethods(null)
//                ->getMock();
//
//        $this->assertEquals('mappingFeature', $eBay->camelName('mapping_Feature'));
//        $this->assertEquals('mappingFeature', $eBay->camelName('mapping+Feature'));
//        $this->assertEquals('mappingFeature', $eBay->camelName('mapping-Feature'));
//        $this->assertEquals('mappingFeature', $eBay->camelName('mappingFeature'));
//        $this->assertEquals('mappingFeature', $eBay->camelName('mapping Feature'));
//    }
//}
