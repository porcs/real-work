<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "dashboard";
$route['404_override'] = 'errors/error404';

$route["users/authenticate_delete_user/(.*)"] = 'users/authenticate_delete_user/$1';
$route["users/reset_password/(.*)"] = 'users/reset_password/$1';
$route["statistics/products/(.*)"] = 'statistics/products/$1';
$route["my_feeds/configuration/(.*)"] = 'my_feeds/$1';
$route["my_feeds/parameters/mapping/(.*)"] = 'my_feeds/$1';
$route["my_feeds/parameters/products/(.*)"] = 'my_feeds/$1'; 
$route["my_feeds/parameters/rules"] = 'offers/rules';
$route["my_feeds/parameters/offers/(.*)"] = 'offers/$1';
$route["my_feeds/parameters/profiles"] = 'profiles/configuration'; 
$route["my_feeds/parameters/profiles/condition"] = 'my_feeds/condition'; 
$route["my_feeds/parameters/category"] = 'profiles/category';
$route["my_feeds/parameters/profiles/(.*)"] = 'profiles/$1';
$route["my_feeds/parameters/(.*)"] = 'my_feeds/$1';
$route["offers/price_delete/(.*)"] = 'offers/price_delete/$1';
$route["offers/rule_delete/(.*)"] = 'offers/rule_delete/$1';
$route["offers/rule_item_delete/(.*)"] = 'offers/rule_item_delete/$1';
$route["offers/rules_item_price_range_delete/(.*)"] = 'offers/rules_item_price_range_delete/$1';
$route["offers/check_shop/(.*)"] = 'offers/check_shop/$1';
$route["validation/dashboard/(.*)"] = 'validation/$1';
$route["validation/log_download/(.*)"] = 'validation/log_download/$1';
$route["validation/log_download_products/(.*)"] = 'validation/log_download_products/$1';
$route["ebay/mapping/(.*)"] = 'ebay/$1';
$route["ebay/statistics/(.*)"] = 'ebay/$1';
$route["users/affiliation/(.*)"] = 'affiliation/$1';
$route["amazon/models/(.*)"] = 'amazon/models/$1';
$route["amazon/offers_options/(.*)"] = 'my_shop/offers_options/$1';
$route["ebay/offers_options/(.*)"] = 'my_shop/offers_options/$1';
$route["general/(.*)"] = 'mirakl/$1';
$route["(.*)/messaging/mail_invoice/(.*)"] = 'messaging/mail_invoice/$1/$2'; // messaging // added : 09/05/2016
$route["(.*)/messaging/mail_review/(.*)"] = 'messaging/mail_review/$1/$2'; // messaging // added : 09/05/2016
$route["(.*)/messaging/customer_thread/(.*)"] = 'messaging/customer_thread/$1/$2'; // messaging // added : 09/05/2016
$route["my_shop/messaging/reply_template/(.*)"] = 'messaging/reply_template/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */