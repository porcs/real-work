<?php 
$tb = 'offer_price_packages';
$tb_sub = 'offer_sub_packages';
$tb_mkp = 'offer_packages';
$conf = 
    array (
        'title' => $this->lang->line('packages_manager'),
        'limit' => '20',
        'frm_type' => '2',
        'join' => array (),
        'order_field' => $tb.".id_offer_pkg",
        'order_type' => 'asc',
        'search_form' => array (
            0 =>  array (
                'alias' => $this->lang->line('region'),
                'field' => $tb.".id_region",
            ),
            1 => array (
                'alias' => $this->lang->line('country'),
                'field' => $tb_sub.'.title_offer_sub_pkg',
            ),
            2 => array (
                'alias' => $this->lang->line('marketplace'),
                'field' => $tb_mkp.'.nave_offer_pkg',
            ),
           
        ),
        'validate' => array (
//            'users.user_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_name')),
//            ),
//            'users.user_password' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_password')),
//            ),
//            'users.user_email' => array (
//                0 => array (
//                    'rule' => 'notEmpty',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//                1 => array (
//                    'rule' => 'email',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//            ),
//            'users.user_first_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('first_name')),
//            ),
//            'users.user_las_name' =>  array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('last_name')),
//            ),
        ),
        'data_list' =>  array (
            'no' =>
                array (
                    'alias' => $this->lang->line('no_'),
                    'width' => '20',
                    'align' => 'center',
                    'format' => '{no}',
                ),
            $tb_mkp.'.name_offer_pkg' =>
                array (
                    'alias' => $this->lang->line('marketplace'),
                    'width' => '60',
                ),
            $tb.'.domain_offer_sub_pkg' =>
                array (
                    'alias' => $this->lang->line('marketplace'),
                    'width' => '80',
                ),
            $tb_sub.'.title_offer_sub_pkg' =>
                array (
                    'alias' => $this->lang->line('country'),
                    'width' => '100',
                ),
            $tb.'.amt_offer_price_pkg' =>
                array (
                    'alias' => $this->lang->line('price'),
                    'width' => '50',
                    'align' => 'right',

                ),
            $tb.'.currency_offer_price_pkg' =>
                array (
                    'alias' => $this->lang->line('currency'),
                    'width' => '50',
                    'align' => 'center',

                ),
            $tb.'.element_offer_sub_pkg' =>
                array (
                    'alias' => $this->lang->line('zone'),
                    'width' => '60',
                    'align' => 'center',

                ),
            $tb.'.id_region' =>
                array (
                    'alias' => $this->lang->line('region'),
                    'width' => '60',
                    'align' => 'center',

                ),
             
            'action' =>
                array (
                    'alias' => $this->lang->line('actions'),
                    'format' => '<a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini btn-primary">'.$this->lang->line('view').'</a> '
//                    . '<a href="company?xtype=view&key[customer.id]={users.id}" class="btn btn-mini btn-success">'.$this->lang->line('view_user_company').'</a>'
                    . '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> '
                    . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
                    'width' => '150',
                    'align' => 'center',
                ),
        ),
        
        'form_elements' =>
            array (
                $tb.'.id_offer_pkg' =>
                    array (
                        'alias' => $this->lang->line('marketplace'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'offer_packages',
                                    'option_key' => 'id_offer_pkg',
                                    'option_value' => 'name_offer_pkg',
                                ),
                            ),
                    ),
                $tb.'.id_offer_sub_pkg' =>
                    array (
                        'alias' => $this->lang->line('country'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'offer_sub_packages',
                                    'option_key' => 'id_offer_sub_pkg',
                                    'option_value' => 'title_offer_sub_pkg',
                                ),
                            ),
                    ),
              
                $tb.'.id_region' =>
                    array (
                        'alias' => $this->lang->line('region'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.amt_offer_price_pkg' =>
                    array (
                        'alias' => $this->lang->line('price'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                 $tb.'.currency_offer_price_pkg' =>
                    array (
                        'alias' => $this->lang->line('currency'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.priod_offer_price_pkg' =>
                    array (
                        'alias' => $this->lang->line('priod'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.element_offer_sub_pkg' =>
                    array (
                        'alias' => $this->lang->line('zone'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.domain_offer_sub_pkg' =>
                    array (
                        'alias' => $this->lang->line('site'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.domain_offer_sub_pkg' =>
                    array (
                        'alias' => $this->lang->line('site'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.ext_offer_sub_pkg' =>
                    array (
                        'alias' => $this->lang->line('extension'), 
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                 
                $tb.'.status_offer_price_pkg' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    'active' => 'Active',
                                    'inactive' => 'InActive',
                                ),
                            ),
                    ),
                $tb.'.comments' =>
                    array (
                         'alias' => $this->lang->line('comments'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                 $tb.'.id_site_ebay' =>
                    array (
                         'alias' => $this->lang->line('id_site_ebay'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                $tb.'.offer_pkg_type' =>
                    array (
                         'alias' => $this->lang->line('pkg_type'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                 
        ),
        
        'elements' =>
            array (
                'users.user_name' =>
                    array (
                        'alias' => $this->lang->line('user_name'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.user_password' =>
                    array (
                        'alias' => $this->lang->line('user_password'),
                        'element' =>
                        array (
                            0 => 'password',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.group_id' =>
                    array (
                        'alias' => $this->lang->line('group'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'groups',
                                    'option_key' => 'id',
                                    'option_value' => 'group_name',
                                ),
                            ),
                    ),
                'users.user_email' =>
                    array (
                        'alias' => $this->lang->line('email'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_first_name' =>
                    array (
                        'alias' => $this->lang->line('first_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_las_name' =>
                    array (
                        'alias' => $this->lang->line('last_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_status' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => $this->lang->line('active'),
                                    0 => $this->lang->line('inactive'),
                                ),
                            ),
                    ),
                'users.user_info' =>
                    array (
                        'alias' => $this->lang->line('user_information'),
                        'element' =>
                        array (
                            0 => 'editor',
                        ),
                    ),
    ),
);