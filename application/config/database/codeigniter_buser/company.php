<?php 
$conf = array (
    'title' => $this->lang->line('company_manager'),
    'limit' => '20',
    'frm_type' => '2',
    'join' => array (),
    'order_field' => 'user_packages.id_upkg',
    'order_type' => 'asc',
    
    'search_form' =>
        array (
            0 =>
            array (
                'alias' => $this->lang->line('company_name'),
                'field' => 'user_packages.company_upkg',
            ),
            1 =>
            array (
                'alias' => $this->lang->line('first_name'),
                'field' => 'user_packages.firstname_upkg',
            ),
            2 =>
            array (
                'alias' => $this->lang->line('last_name'),
                'field' => 'user_packages.lastname_upkg',
            ),
        ),
    
    'validate' =>
        array (
            'user_packages.company_upkg' =>
            array (
                'rule' => 'notEmpty',
                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('company_name')),
            ),
        ),
    
    'data_list' =>
        array (
            'no' =>
                array (
                    'alias' => $this->lang->line('no_'),
                    'width' => 40,
                    'align' => 'center',
                    'format' => '{no}',
                ),
            'user_packages.company_upkg' =>
                array (
                    'alias' => $this->lang->line('company_name'),
                ),
            'user_packages.firstname_upkg' =>
                array (
                    'alias' => $this->lang->line('first_name'),
                ),
            'user_packages.lastname_upkg' =>
               array (
                   'alias' => $this->lang->line('last_name'),
               ),
            'user_packages.city_upkg' =>
               array (
                   'alias' => $this->lang->line('city'),
               ),
            'user_packages.country_upkg' =>
               array (
                   'alias' => $this->lang->line('country'),
               ),
            'action' =>
                array (
                    'alias' => $this->lang->line('actions'),
                    'format' => '<a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini btn-primary">'.$this->lang->line('view').'</a> '
                    . '<a href="user?xtype=view&key[users.id]={user_packages.id_users}" class="btn btn-mini btn-success">'.$this->lang->line('view_user_profile').'</a>'
                    . '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> '
                    . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
                    'width' => 130,
                    'align' => 'center',
                ),
    ),
    
    'form_elements' =>
        array (
            'user_packages.company_upkg' =>
                array (
                    'alias' => $this->lang->line('company_name'),
                    'element' =>
                    array (
                        0 => 'text',
                        1 =>
                        array (
                            'style' => 'width:650px;',
                        ),
                    ),
                ),
            'user_packages.firstname_upkg' =>
                array (
                    'alias' => $this->lang->line('first_name'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.lastname_upkg' =>
                array (
                    'alias' => $this->lang->line('last_name'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
//            'customer.complement' =>
//                array (
//                    'alias' => $this->lang->line('complement'),
//                    'element' =>
//                        array (
//                            0 => 'text',
//                            1 =>
//                            array (
//                                'style' => 'width:650px;',
//                            ),
//                        ),
//                ),
            'user_packages.address1_upkg' =>
                array (
                    'alias' => $this->lang->line('address1'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.address2_upkg' =>
                array (
                    'alias' => $this->lang->line('address2'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.state_upkg' =>
                array (
                    'alias' => $this->lang->line('stateregion'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.zip_upkg' =>
                array (
                    'alias' => $this->lang->line('zipcode'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.city_upkg' =>
                array (
                    'alias' => $this->lang->line('city'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.country_upkg' =>
                array (
                    'alias' => $this->lang->line('country'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
        ),
    
    'elements' =>
        array (
            'user_packages.company_upkg' =>
                array (
                    'alias' => $this->lang->line('company_name'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 => array (
                                    'style' => 'width:650px;',
                                ),
                        ),
                ),
//            'groups.group_description' =>
//                array (
//                    'alias' => $this->lang->line('description'),
//                    'element' => 
//                        array (
//                            0 => 'editor',
//                        ),
//                ),
            'user_packages.firstname_upkg' =>
                array (
                    'alias' => $this->lang->line('first_name'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.lastname_upkg' =>
                array (
                    'alias' => $this->lang->line('last_name'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
//            'customer.complement' =>
//                array (
//                    'alias' => $this->lang->line('complement'),
//                    'element' =>
//                        array (
//                            0 => 'text',
//                            1 =>
//                            array (
//                                'style' => 'width:650px;',
//                            ),
//                        ),
//                ),
            'user_packages.address1_upkg' =>
                array (
                    'alias' => $this->lang->line('address1'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.address2_upkg' =>
                array (
                    'alias' => $this->lang->line('address2'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.state_upkg' =>
                array (
                    'alias' => $this->lang->line('stateregion'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.zip_upkg' =>
                array (
                    'alias' => $this->lang->line('zipcode'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.city_upkg' =>
                array (
                    'alias' => $this->lang->line('city'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            'user_packages.country_upkg' =>
                array (
                    'alias' => $this->lang->line('country'),
                    'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:650px;',
                            ),
                        ),
                ),
            
        ),
);