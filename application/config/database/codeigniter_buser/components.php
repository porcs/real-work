<?php 
$conf = array (
		'title' => $this->lang->line('component_manager'),
		'limit' => '20',
		'frm_type' => '2',
		'join' =>
		array (
		),
		'order_field' => 'components.id',
		'order_type' => 'asc',
		'search_form' =>
		array (
				0 =>
				array (
						'alias' => $this->lang->line('name'),
						'field' => 'components.component_name',
				),
				1 =>
				array (
						'alias' => $this->lang->line('group'),
						'field' => 'components.group_id',
				),
		),
		'validate' =>
		array (
				'components.component_name' =>
				array (
						'rule' => 'notEmpty',
						'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('name')),
				),
		),
		'data_list' =>
		array (
				'no' =>
				array (
						'alias' => $this->lang->line('no_'),
						'width' => 40,
						'align' => 'center',
						'format' => '{no}',
				),
				'components.component_name' =>
				array (
						'alias' => $this->lang->line('name'),
				),
				'components.group_id' =>
				array (
						'alias' => $this->lang->line('group'),
				),
				'components.component_table' =>
				array (
						'alias' => $this->lang->line('table_name'),
				),
				'action' =>
				array (
						'alias' => $this->lang->line('actions'),
						'format' => '<a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini">'.$this->lang->line('view').'</a> <a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> <a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
						'width' => 130,
						'align' => 'center',
				),
		),
		'form_elements' =>
		array (
				'components.component_name' =>
				array (
						'alias' => $this->lang->line('name'),
						'element' =>
						array (
								0 => 'text',
								1 =>
								array (
										'style' => 'width:208px;',
								),
						),
				),
				'components.group_id' =>
				array (
						'alias' => $this->lang->line('group'),
						'element' =>
						array (
								0 => 'autocomplete',
								1 =>
								array (
										'option_table' => 'group_components',
										'option_key' => 'id',
										'option_value' => 'name',
								),
						),
				),
		),
		'elements' =>
		array (
				'components.component_name' =>
				array (
						'alias' => $this->lang->line('name'),
						'element' =>
						array (
								0 => 'text',
								1 =>
								array (
										'style' => 'width:208px;',
								),
						),
				),
				'components.group_id' =>
				array (
						'alias' => $this->lang->line('group'),
						'element' =>
						array (
								0 => 'autocomplete',
								1 =>
								array (
										'option_table' => 'group_components',
										'option_key' => 'id',
										'option_value' => 'name',
								),
						),
				),
		),
);