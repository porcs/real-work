<?php 
$tb = 'mapping_colors'; 
$conf = 
    array (
        'title' => $this->lang->line('mapping_colors'),
        'limit' => '20',
        'frm_type' => '2',
        'join' => array (),
        'order_field' => $tb.".color_name",
        'order_type' => 'asc',
        'search_form' => array (
            0 =>  array (
                'alias' => $this->lang->line('color_name'),
                'field' => $tb.".color_name",
            ),
//            1 => array (
//                'alias' => $this->lang->line('country'),
//                'field' => $tb_sub.'.title_offer_sub_pkg',
//            ),
//            2 => array (
//                'alias' => $this->lang->line('marketplace'),
//                'field' => $tb_mkp.'.nave_offer_pkg',
//            ),
           
        ),
        'validate' => array (
//            'users.user_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_name')),
//            ),
//            'users.user_password' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_password')),
//            ),
//            'users.user_email' => array (
//                0 => array (
//                    'rule' => 'notEmpty',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//                1 => array (
//                    'rule' => 'email',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//            ),
//            'users.user_first_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('first_name')),
//            ),
//            'users.user_las_name' =>  array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('last_name')),
//            ),
        ),
        'data_list' =>  array (
            'no' =>
                array (
                    'alias' => $this->lang->line('no_'),
                    'width' => '20',
                    'align' => 'center',
                    'format' => '{no}',
                ),
           
            $tb.'.color_name' =>
                array (
                    'alias' => $this->lang->line('color_name'),
                    'width' => '80',
                ),
              
            'action' =>
                array (
                    'alias' => $this->lang->line('actions'),
                    'format' => ''//<a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini btn-primary">'.$this->lang->line('view').'</a> '
//                    . '<a href="company?xtype=view&key[customer.id]={users.id}" class="btn btn-mini btn-success">'.$this->lang->line('view_user_company').'</a>'
                    . '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> '
                    . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
                    'width' => '150',
                    'align' => 'center',
                ),
        ),
        
        'form_elements' =>
            array (
                
              
                $tb.'.color_name' =>
                    array (
                        'alias' => $this->lang->line('color_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.color_com' =>
                    array (
                        'alias' => $this->lang->line('color_com'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                $tb.'.color_uk' =>
                    array (
                        'alias' => $this->lang->line('color_uk'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                $tb.'.color_fr' =>
                    array (
                        'alias' => $this->lang->line('color_fr'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                $tb.'.color_de' =>
                    array (
                        'alias' => $this->lang->line('color_de'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                $tb.'.color_es' =>
                    array (
                        'alias' => $this->lang->line('color_es'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                $tb.'.color_it' =>
                    array (
                        'alias' => $this->lang->line('color_it'),
                        'element' =>
                            array (
                                0 => 'textarea',
                                1 =>
                                array (
                                    'style' => 'width:530px; height:130px;',
                                ),
                            ),
                    ),
                  
                 
        ),
        
        'elements' =>
            array (
                'users.user_name' =>
                    array (
                        'alias' => $this->lang->line('user_name'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.user_password' =>
                    array (
                        'alias' => $this->lang->line('user_password'),
                        'element' =>
                        array (
                            0 => 'password',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.group_id' =>
                    array (
                        'alias' => $this->lang->line('group'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'groups',
                                    'option_key' => 'id',
                                    'option_value' => 'group_name',
                                ),
                            ),
                    ),
                'users.user_email' =>
                    array (
                        'alias' => $this->lang->line('email'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_first_name' =>
                    array (
                        'alias' => $this->lang->line('first_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_las_name' =>
                    array (
                        'alias' => $this->lang->line('last_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_status' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => $this->lang->line('active'),
                                    0 => $this->lang->line('inactive'),
                                ),
                            ),
                    ),
                'users.user_info' =>
                    array (
                        'alias' => $this->lang->line('user_information'),
                        'element' =>
                        array (
                            0 => 'editor',
                        ),
                    ),
    ),
);