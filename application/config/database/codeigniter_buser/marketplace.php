<?php 
$tb = 'offer_packages'; 
$conf = 
    array (
        'title' => $this->lang->line('marketplace_manager'),
        'limit' => '20',
        'frm_type' => '2',
        'join' => array (),
        'order_field' => $tb.".sort_offer_pkg",
        'order_type' => 'asc',
        'search_form' => array (
            0 =>  array (
                'alias' => $this->lang->line('marketplace'),
                'field' => $tb.".name_offer_pkg",
            ),
           
           
        ),
        'validate' => array (
//            'users.user_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_name')),
//            ),
//            'users.user_password' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_password')),
//            ),
//            'users.user_email' => array (
//                0 => array (
//                    'rule' => 'notEmpty',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//                1 => array (
//                    'rule' => 'email',
//                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
//                ),
//            ),
//            'users.user_first_name' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('first_name')),
//            ),
//            'users.user_las_name' =>  array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('last_name')),
//            ),
        ),
        'data_list' =>  array (
            'no' =>
                array (
                    'alias' => $this->lang->line('no_'),
                    'width' => '20',
                    'align' => 'center',
                    'format' => '{no}',
                ),
            $tb.'.name_offer_pkg' =>
                array (
                    'alias' => $this->lang->line('marketplace'),
                    'width' => '60',
                ),
            $tb.'.status_offer_pkg' =>
                array (
                    'alias' => $this->lang->line('status'),
                    'width' => '80',
                ),
            
             
            'action' =>
                array (
                    'alias' => $this->lang->line('actions'),
                    'format' => ''
//                    .'<a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini btn-primary">'.$this->lang->line('view').'</a> '
//                    . '<a href="company?xtype=view&key[customer.id]={users.id}" class="btn btn-mini btn-success">'.$this->lang->line('view_user_company').'</a>'
                    . '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> '
                    . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
                    'width' => '50',
                    'align' => 'center',
                ),
        ),
        
        'form_elements' =>
            array (
                $tb.'.name_offer_pkg' =>
                    array (
                        'alias' => $this->lang->line('marketplace'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                $tb.'.sort_offer_pkg' =>
                    array (
                        'alias' => $this->lang->line('sort_by'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
              
                $tb.'.submenu_offer_pkg' =>
                    array (
                        'alias' => $this->lang->line('has_sub'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    0 => 'No sub package',
                                    1 => 'Have sub packages',
                                ),
                            ),
                    ),
                $tb.'.status_offer_pkg' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    'active' => 'Active',
                                    'inactive' => 'Inactive',
                                ),
                            ),
                    ),
                $tb.'.is_marketplace' =>
                    array (
                        'alias' => $this->lang->line('region'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    0 => 'not marketplace',
                                    1 => 'is marketplace',
                                ),
                            ),
                    ),
                 
                 
        ),
        
        'elements' =>
            array (
                'users.user_name' =>
                    array (
                        'alias' => $this->lang->line('user_name'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.user_password' =>
                    array (
                        'alias' => $this->lang->line('user_password'),
                        'element' =>
                        array (
                            0 => 'password',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.group_id' =>
                    array (
                        'alias' => $this->lang->line('group'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'groups',
                                    'option_key' => 'id',
                                    'option_value' => 'group_name',
                                ),
                            ),
                    ),
                'users.user_email' =>
                    array (
                        'alias' => $this->lang->line('email'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_first_name' =>
                    array (
                        'alias' => $this->lang->line('first_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_las_name' =>
                    array (
                        'alias' => $this->lang->line('last_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_status' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => $this->lang->line('active'),
                                    0 => $this->lang->line('inactive'),
                                ),
                            ),
                    ),
                'users.user_info' =>
                    array (
                        'alias' => $this->lang->line('user_information'),
                        'element' =>
                        array (
                            0 => 'editor',
                        ),
                    ),
    ),
);