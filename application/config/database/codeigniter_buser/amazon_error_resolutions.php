<?php

$tb = 'amazon_error_resolutions';
$conf = array(
    'title' => $this->lang->line('amazon_error_resolutions'),
    'limit' => '20',
    'frm_type' => '2',
    'join' => array(),
    'order_field' => $tb . ".error_code",
    'order_type' => 'asc',
    'search_form' => array(
        0 => array(
            'alias' => $this->lang->line('error_code'),
            'field' => $tb . ".error_code",
        ),
        1 => array(
            'alias' => $this->lang->line('error_details'),
            'field' => $tb . '.error_details',
        )
    ),
    'validate' => array(
        $tb . '.error_code' => array(
            'rule' => 'notEmpty',
            'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('error_code')),
        )
    ),
    'data_list' => array(
        $tb . '.error_code' =>
        array(
            'alias' => $this->lang->line('error_code'),
            'width' => '20',
            'align' => 'left',
        ),
        $tb . '.error_type' =>
        array(
            'alias' => $this->lang->line('Error type'),
            'width' => '20',
            'align' => 'left',
        ),
         $tb . '.error_lang' =>
        array(
            'alias' => $this->lang->line('error_lang'),
            'width' => '10',
        ),
        $tb . '.error_details' =>
        array(
            'alias' => $this->lang->line('error_details'),
            'width' => '30',
        ),
        $tb . '.error_solved_type' =>
        array(
            'alias' => $this->lang->line('type'),
            'width' => '20',
        ), 
        $tb . '.error_priority' =>
        array(
            'alias' => $this->lang->line('error_priority'),
            'width' => '10',
        ),
        /*$tb . '.show_message' =>
        array(
            'alias' => $this->lang->line('show_message'),
            'width' => '10',
        ), */
        'action' =>
        array(
            'alias' => $this->lang->line('actions'),
            'format' => '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">' . $this->lang->line('edit') . '</a> '
            . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">' . $this->lang->line('delete') . '</a>',
            'width' => '150',
            'align' => 'center',
        ),
    ),
    'form_elements' =>
    array(
        $tb . '.error_code' =>
        array(
            'alias' => $this->lang->line('error_code'),
            'element' =>
            array(
                0 => 'text',
                1 =>
                array(
                    'style' => 'width:210px;',
                ),
            ),
        ),
        $tb . '.error_type' =>
        array(
            'alias' => $this->lang->line('Error type'),
            'element' =>
            array(
                0 => 'select',
                1 => array(
                    'products' => 'Products',
                    'offers' => 'Offers',
                ),
            ),
        ),
        $tb . '.error_details' =>
        array(
            'alias' => $this->lang->line('error_details'),
            'element' =>
            array(
                0 => 'editor',
            ),
        ),
        $tb . '.error_resolution' =>
        array(
            'alias' => $this->lang->line('error_resolution'),
            'element' =>
            array(
                0 => 'editor',
            ),
        ),
        $tb . '.error_solved_type' =>
        array(
            'alias' => $this->lang->line('type'),
            'element' =>
            array(
                0 => 'select',
                1 => array('edit_attribute' => 'Attribute Override',
                    'creation' => 'Creation',
                    'override' => 'Override',
                    'recreate' => 'Re-create',
                    'solved' => 'Solved',
                ),
            ),
        ),
        $tb . '.error_priority' =>
        array(
            'alias' => $this->lang->line('error_priority'),
            'element' =>
            array(
                0 => 'select',
                1 => array('0' => '0', '1' => '1'),
            ),
        ),
        $tb . '.error_lang' =>
        array(
            'alias' => $this->lang->line('error_lang'),
            'element' =>
            array(
                0 => 'select',
                1 => array('en' => 'English', 'fr' => 'French',),
            ),
        ),
        $tb . '.msg_pattern' =>
        array(
            'alias' => $this->lang->line('msg_pattern'),
            'element' =>
            array(
                0 => 'text',
                1 =>
                array(
                    'style' => 'width:400px;',
                ),
            ),
        ),
        $tb . '.msg_field' =>
        array(
            'alias' => $this->lang->line('msg_field'),
            'element' =>
            array(
                0 => 'hidden',
                1 =>
                array(
                    'style' => 'width:400px;',
                ),
            ),
        ),
        $tb . '.show_message' =>
        array(
            'alias' => $this->lang->line('show_message_inline'),
            'element' =>
            array(
                0 => 'checkbox',
            ),
        ),
        $tb . '.allow_overide' =>
        array(
            'alias' => $this->lang->line('allow_override_attribute'),
            'element' =>
            array(
                0 => 'checkbox',
            ),
        ),
        $tb . '.other' =>
        array(
            'alias' => $this->lang->line('other'),
            'element' =>
            array(
                0 => 'editor'
            ),
        ),
        $tb . '.notification_summary' =>
        array(
            'alias' => $this->lang->line('notification_summary'),
            'element' =>
            array(
                0 => 'editor'
            ),
        ),
        $tb . '.date_add' =>
        array(
            'alias' => $this->lang->line('date'),
            'element' =>
            array(
                0 => 'datetime',
                1 => array(
                    'readonly' => 'readonly',
                    'value' => date('Y-m-d H:i:s')
                )
            ),
        ),
    ),
    'elements' =>
    array(
        $tb . '.error_code' =>
        array(
            'alias' => $this->lang->line('error_code'),
            'element' =>
            array(
                0 => 'text',
                1 =>
                array(
                    'style' => 'width:210px;',
                ),
            ),
        ),
        $tb . '.error_type' =>
        array(
            'alias' => $this->lang->line('error_type'),
            'element' =>
            array(
                0 => 'select',
                1 => array('products' => 'Products', 'offers' => 'Offers',),
            ),
        ),
        $tb . '.error_details' =>
        array(
            'alias' => $this->lang->line('error_details'),
            'element' =>
            array(
                0 => 'editor',
            ),
        ),
        $tb . '.error_resolution' =>
        array(
            'alias' => $this->lang->line('error_resolution'),
            'element' =>
            array(
                0 => 'editor',
            ),
        ),
        $tb . '.error_solved_type' =>
        array(
            'alias' => $this->lang->line('type'),
            'element' =>
            array(
                0 => 'text',
                1 =>
                array(
                    'style' => 'width:210px;',
                ),
            ),
        ),
        $tb . '.error_priority' =>
        array(
            'alias' => $this->lang->line('error_priority'),
            'element' =>
            array(
                0 => 'select',
                1 => array('0' => '0', '1'),
            ),
        ),
        $tb . '.error_lang' =>
        array(
            'alias' => $this->lang->line('error_lang'),
            'element' =>
            array(
                0 => 'select',
                1 => array('en' => 'English', 'fr' => 'French',),
            ),
        ),
        $tb . '.show_message' =>
        array(
            'alias' => $this->lang->line('show_message_inline'),
            'element' =>
            array(
                0 => 'checkbox',
            ),
        ),
        $tb . '.allow_overide' =>
        array(
            'alias' => $this->lang->line('allow_override_attribute'),
            'element' =>
            array(
                0 => 'checkbox',
            ),
        ),
        $tb . '.other' =>
        array(
            'alias' => $this->lang->line('other'),
            'element' =>
            array(
                0 => 'editor'
            ),
        ),
        $tb . '.notification_summary' =>
        array(
            'alias' => $this->lang->line('notification_summary'),
            'element' =>
            array(
                0 => 'editor'
            ),
        ),
        $tb . '.date_add' =>
        array(
            'alias' => $this->lang->line('date'),
            'element' =>
            array(
                0 => 'datetime',
                1 => array(
                    'readonly' => 'readonly',
                    'value' => date('Y-m-d H:i:s')
                )
            ),
        ),
    ),
);
