<?php 
        $time = time();
        $time_en = md5($time);
        $tf = substr($time, -1,1);
        $ta = substr($time, -2,1);
        $tb = substr($time, -3,1);
        $tg = substr($time, -4,1);
        $pf = substr($time_en, $tf+$tg, 1);
        $pb = substr($time_en, $ta+$tb, 1);
        $time_end = $pb.$time_en.$pf;
        $input = array('k'=> bin2hex(gzcompress($time_end)) ,'a'=>base64_encode(time()));
        $f = mt_rand(0,99999);
        $b = mt_rand(0,99999);
        $st = (base64_encode(($f.'!'.base64_encode(json_encode($input)).'!'.$b)));
        
        
$conf = 
    array (
        'title' => $this->lang->line('user_manager'),
        'limit' => '20',
        'frm_type' => '2',
        'join' => array (),
        'order_field' => 'users.user_name',
        'order_type' => 'asc',
        'search_form' => array (
            0 =>  array (
                'alias' => $this->lang->line('user_name'),
                'field' => 'users.user_name',
            ),
            1 => array (
                'alias' => $this->lang->line('group'),
                'field' => 'users.group_id',
            ),
            2 => array (
                'alias' => $this->lang->line('first_name'),
                'field' => 'users.user_first_name',
            ),
            3 => array (
                'alias' => $this->lang->line('last_name'),
                'field' => 'users.user_las_name',
            ),
            4 => array (
                'alias' => $this->lang->line('email'),
                'field' => 'users.user_email',
            ),
        ),
        'validate' => array (
            'users.user_name' => array (
                'rule' => 'notEmpty',
                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_name')),
            ),
//            'users.user_password' => array (
//                'rule' => 'notEmpty',
//                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('user_password')),
//            ),
            'users.user_email' => array (
                0 => array (
                    'rule' => 'notEmpty',
                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
                ),
                1 => array (
                    'rule' => 'email',
                    'message' => sprintf($this->lang->line('please_provide_valid_email'), $this->lang->line('email')),
                ),
            ),
            'users.user_first_name' => array (
                'rule' => 'notEmpty',
                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('first_name')),
            ),
            'users.user_las_name' =>  array (
                'rule' => 'notEmpty',
                'message' => sprintf($this->lang->line('please_enter_value'), $this->lang->line('last_name')),
            ),
        ),
        'data_list' =>  array (
            'no' =>
                array (
                    'alias' => $this->lang->line('no_'),
                    'width' => '20',
                    'align' => 'center',
                    'format' => '{no}',
                ),
            'users.user_name' =>
                array (
                    'alias' => $this->lang->line('user_name'),
                    'width' => '105',
                ),
            'users.group_id' =>
                array (
                    'alias' => $this->lang->line('group'),
                    'width' => '80',
                ),
            'users.user_first_name' =>
                array (
                    'alias' => $this->lang->line('first_name'),
                    'width' => '100',

                ),
            'users.user_las_name' =>
                array (
                    'alias' => $this->lang->line('last_name'),
                    'width' => '100',

                ),
            'users.user_email' =>
                array (
                    'alias' => $this->lang->line('email'),
                    'width' => '180',
                ),
            'users.user_provider' =>
                array (
                    'alias' => $this->lang->line('provider'),
                    'width' => '80',
                    'align' => 'center',
                ),
            'users.user_status' =>
                array (
                    'alias' => $this->lang->line('status'),
                    'width' => '50',
                    'align' => 'center',
                ),
            'action' =>
                array (
                    'alias' => $this->lang->line('actions'),
                    'format' => '<a href="/users/remote/'.$st.'/{users.id}" class="btn btn-mini btn-info" target=_blank>'.$this->lang->line('remote_login').'</a> <a type="button" onclick="__view(\'{ppri}\'); return false;" class="btn btn-mini btn-primary">'.$this->lang->line('view').'</a> '
                    . '<a href="company?xtype=view&key[customer.id]={users.id}" class="btn btn-mini btn-success">'.$this->lang->line('view_user_company').'</a>'
                    . '<a type="button" onclick="__edit(\'{ppri}\'); return false;" class="btn btn-mini btn-info">'.$this->lang->line('edit').'</a> '
                    . '<a type="button" onclick="__delete(\'{ppri}\'); return false;" class="btn btn-mini btn-danger">'.$this->lang->line('delete').'</a>',
                    'width' => '150',
                    'align' => 'center',
                ),
        ),
        
        'form_elements' =>
            array (
                'users.user_name' =>
                    array (
                        'alias' => $this->lang->line('user_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_password' =>
                    array (
                        'alias' => $this->lang->line('user_password'),
                        'element' =>
                            array (
                                0 => 'password',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.group_id' =>
                    array (
                        'alias' => $this->lang->line('group'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'groups',
                                    'option_key' => 'id',
                                    'option_value' => 'group_name',
                                ),
                            ),
                    ),
                'users.user_email' =>
                    array (
                        'alias' => $this->lang->line('email'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_first_name' =>
                    array (
                        'alias' => $this->lang->line('first_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_las_name' =>
                    array (
                        'alias' => $this->lang->line('last_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_birthday' =>
                    array (
                        'alias' => $this->lang->line('birthday'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_status' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => 'Active',
                                    0 => 'InActive',
                                ),
                            ),
                    ),
                'users.user_provider' =>
                    array (
                        'alias' => $this->lang->line('provider'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'disabled' => 'disabled',
                            ),
                        ),
                    ),
                'users.user_auth_id' =>
                    array (
                        'alias' => $this->lang->line('provider_id'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'disabled' => 'disabled',
                                ),
                            ),
                    ),
                'users.user_info' =>
                    array (
                        'alias' => $this->lang->line('user_information'),
                        'element' =>
                            array (
                                0 => 'editor',
                            ),
                    ),
                'users.user_newsletter' =>
                    array (
                        'alias' => $this->lang->line('newsletter'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => 'Yes',
                                    0 => 'No',
                                ),
                            ),
                    ),
                'users.user_last_login' =>
                    array (
                        'alias' => $this->lang->line('last_login'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'disabled' => 'disabled',
                            ),
                        ),
                    ),
        ),
        
        'elements' =>
            array (
                'users.user_name' =>
                    array (
                        'alias' => $this->lang->line('user_name'),
                        'element' =>
                        array (
                            0 => 'text',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.user_password' =>
                    array (
                        'alias' => $this->lang->line('user_password'),
                        'element' =>
                        array (
                            0 => 'password',
                            1 =>
                            array (
                                'style' => 'width:210px;',
                            ),
                        ),
                    ),
                'users.group_id' =>
                    array (
                        'alias' => $this->lang->line('group'),
                        'element' =>
                            array (
                                0 => 'select',
                                1 =>
                                array (
                                    'option_table' => 'groups',
                                    'option_key' => 'id',
                                    'option_value' => 'group_name',
                                ),
                            ),
                    ),
                'users.user_email' =>
                    array (
                        'alias' => $this->lang->line('email'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:210px;',
                                ),
                            ),
                    ),
                'users.user_first_name' =>
                    array (
                        'alias' => $this->lang->line('first_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_las_name' =>
                    array (
                        'alias' => $this->lang->line('last_name'),
                        'element' =>
                            array (
                                0 => 'text',
                                1 =>
                                array (
                                    'style' => 'width:390px;',
                                ),
                            ),
                    ),
                'users.user_status' =>
                    array (
                        'alias' => $this->lang->line('status'),
                        'element' =>
                            array (
                                0 => 'radio',
                                1 =>
                                array (
                                    1 => $this->lang->line('active'),
                                    0 => $this->lang->line('inactive'),
                                ),
                            ),
                    ),
                'users.user_info' =>
                    array (
                        'alias' => $this->lang->line('user_information'),
                        'element' =>
                        array (
                            0 => 'editor',
                        ),
                    ),
    ),
);