<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox Mode - TRUE/FALSE
 * Check the domain of the current page and set $sandbox accordingly.
 * This allows you to automatically use Sandbox or Live credentials throughout 
 * your application based on what server the app is running from.
 * 
 * I like to do this so I don't forget to update Sandbox credentials to Live
 * prior to uploading files to a production server.
 * 
 * In this case, it's checking to see if the current URL is http://sandbox.domain.*
 * If so, $sandbox is true and the PayPal sandbox will be used throughout.  If not, 
 * we'll assume it must be a live transaction and will use live credentials throughout.
 *
 * Following this pattern will allow you to create your own http://sandbox.domain.com test server, 
 * and then any time your code runs from that server, PayPal's sandbox will be used automatically.
 * 
 * If you would rather just set $sandbox to true/false on your own that's fine, 
 * but you have to make sure your live server always uses false and your test server
 * always uses true.  It's easy to forget this and up with real customers processing 
 * payments from your live site on the PayPal sandbox.
 */

$config['pay']['sandbox'] = TRUE;
$config['pay']['domain'] = $config['pay']['sandbox'] ? 'http://devcommon-services.com/' : 'http://www.domain.com/';


/* 
 * PayPal API Version
 * ------------------
 * The library is currently using PayPal API version 109.0.  
 * You may adjust this value here and then pass it into the PayPal object when you create it within your scripts to override if necessary.
 */
$config['pay']['api_version'] = '109.0';

/*
 * PayPal Application ID
 * --------------------------------------
 * The application is only required with Adaptive Payments applications.
 * You obtain your application ID but submitting it for approval within your 
 * developer account at http://developer.paypal.com
 *
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * The sandbox value included here is a global value provided for developrs to use in the PayPal sandbox.
 */
$config['pay']['application_id'] = $config['pay']['sandbox'] ? 'APP-80W284485P519543T' : '';

/*
 * PayPal Developer Account Email Address
 * This is the email address that you use to sign in to http://developer.paypal.com
 */
$config['pay']['developer_account_email'] = 'kittisak.kpkas@gmail.com';

/*
 * PayPal Gateway API Credentials
 * ------------------------------
 * These are your PayPal API credentials for working with the PayPal gateway directly.
 * These are used any time you're using the parent PayPal class within the library.
 * 
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * 
 * You may obtain these credentials by logging into the following with your PayPal account: https://www.paypal.com/us/cgi-bin/webscr?cmd=_login-api-run
 */
$config['pay']['api_username'] = $config['pay']['sandbox'] ? 'kittisak.kpkas-facilitator_api1.gmail.com' : 'LIVE_API_USERNAME';
$config['pay']['api_password'] = $config['pay']['sandbox'] ? '1391504558' : 'LIVE_API_PASSWORD';
$config['pay']['api_signature'] = $config['pay']['sandbox'] ? 'AQ0gLJxbXY8QLJ2hgImpk0zQdAa-AtBMIKmF-UU8dHWTSmg8K9DR8MRs' : 'LIVE_API_SIGNATURE';

/*
 * Payflow Gateway API Credentials
 * ------------------------------
 * These are the credentials you use for your PayPal Manager:  http://manager.paypal.com
 * These are used when you're working with the PayFlow child class.
 * 
 * We're using shorthand if/else statements here to set both Sandbox and Production values.
 * Your sandbox values go on the left and your live values go on the right.
 * 
 * You may use the same credentials you use to login to your PayPal Manager, 
 * or you may create API specific credentials from within your PayPal Manager account.
 */
$config['pay']['payflow_username'] = $config['pay']['sandbox'] ? 'SANDBOX_PAYFLOW_USERNAME' : 'LIVE_PAYFLOW_USERNAME';
$config['pay']['payflow_password'] = $config['pay']['sandbox'] ? 'SANDBOX_PAYFLOW_PASSWORD' : 'LIVE_PAYFLOW_PASSWORD';
$config['pay']['payflow_vendor'] = $config['pay']['sandbox'] ? 'SANDBOX_PAYFLOW_VENDOR' : 'LIVE_PAYFLOW_VENDOR';
$config['pay']['payflow_partner'] = $config['pay']['sandbox'] ? 'SANDBOX_PAYFLOW_PARTNER' : 'LIVE_PAYFLOW_PARTNER';

/* 
 * PayPal REST API Credentials
 * ---------------------------
 * These are the API credentials used for the PayPal REST API.
 * These are used any time you're working with the REST API child class.
 * 
 * You may obtain these credentials from within your account at http://developer.paypal.com
 */
$config['pay']['rest_client_id'] = $config['pay']['sandbox'] ? 'AeJZDxAAQxMBySX_YzppMLUNFXdZWjBnn4kVgJANmbrHBGFZdGwTuyNxh03_' : 'LIVE_CLIENT_ID';
$config['pay']['rest_client_secret'] = $config['pay']['sandbox'] ? 'EHqEoBBX99G5B-xQFne752nGf6tYTLJvEg9zhEXXAB9j3jB-zJvsy3YDSBUH' : 'LIVE_SECRET_ID';

/*
 * PayPal Finance Portal API
 * -------------------------
 * These are credentials used for obtaining a PublisherID used in Bill Me Later Banner code.
 * As of now, these are specialized API's and you must obtain credentials directly from a PayPal rep.
 */
$config['pay']['finance_access_key'] = $config['pay']['sandbox'] ? 'SANDBOX_ACCESS_KEY' : 'LIVE_ACCESS_KEY';
$config['pay']['finance_client_secret'] = $config['pay']['sandbox'] ? 'SANDBOX_CLIENT_SECRET' : 'LIVE_CLIENT_SECRET';

/**
 * Third Party User Values
 * These can be setup here or within each caller directly when setting up the PayPal object.
 */
$config['pay']['api_subject'] = 'kittisak.kpkas-facilitator@gmail.com';	// If making calls on behalf a third party, their PayPal email address or account ID goes here.
$config['pay']['device_id'] = '';
$config['pay']['device_ip_address'] = array_key_exists('REMOTE_ADDR',$_SERVER) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';

/**
 * User Defined Paypal Config
 */
$config['pay']['buttonsource'] = 'MyPHP_Class'; // Set Paypal 3rd PArty Button Source
//$config['pay']['amount'] = '500'; // Set Monthly Amount
$config['pay']['currencycode'] = 'EUR'; // Set Currency Code
//$config['pay']['taxrate'] = 9.5; // Set TaxRate ex. 9.5
$config['pay']['taxrate'] = 0.0; // Set TaxRate ex. 9.5