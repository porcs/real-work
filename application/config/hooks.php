<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_access',
 'function' => 'auth',
 'filename' => 'hooks_access.php',
 'filepath' => 'hooks' 
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_access',
 'function' => 'csrf',
 'filename' => 'hooks_access.php',
 'filepath' => 'hooks' 
);

/*$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_menu',
 'function' => 'get_menu',
 'filename' => 'hooks_menu.php',
 'filepath' => 'hooks' 
);*/

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_breadcrumb',
 'function' => 'set_breadcrumb',
 'filename' => 'hooks_breadcrumb.php',
 'filepath' => 'hooks' 
);

#Don't move hook shop, we need shop id
$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_shops',
 'function' => 'get_default_shop',
 'filename' => 'hooks_shops.php',
 'filepath' => 'hooks'   
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_shops',
 'function' => 'get_shop_language',
 'filename' => 'hooks_shops.php',
 'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_mode',
 'function' => 'get_default_mode',
 'filename' => 'hooks_mode.php',
 'filepath' => 'hooks'  
);

$hook['post_controller_constructor'][] = array( #after hooks_mode
 'class' => 'Hooks_notifications',
 'function' => 'get_notifications',
 'filename' => 'hooks_notifications.php',
 'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_marketplace',
 'function' => 'get_marketplace_menu',
 'filename' => 'hooks_marketplace.php',
 'filepath' => 'hooks'   
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_referral',
 'function' => 'getReferral',
 'filename' => 'hooks_referral.php',
 'filepath' => 'hooks'   
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_support',
 'function' => 'getCustomerSupport',
 'filename' => 'hooks_support.php',
 'filepath' => 'hooks'   
);

$hook['post_controller_constructor'][] = array(
 'class' => 'Hooks_ebay',
 'function' => 'check_token',
 'filename' => 'hooks_ebay.php',
 'filepath' => 'hooks'   
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */