<?php

/* To assign session to view */

class Hooks_access
{
    public function csrf()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci = &get_instance();
        $csrf_key = $ci->security->get_csrf_token_name();
        $csrf_value = $ci->security->get_csrf_hash();
        $ci->smarty->assign("csrf_val", $csrf_value);
        
        
        $ci->smarty->assign("csrf_key", $csrf_key);
        $ci->smarty->assign("cdn_url", (function_exists('cdn_url')?cdn_url():base_url()));
    }
    public function auth()
    {
        $ci = &get_instance();
        if (isset($ci->uri)) {
            $func = $ci->uri->segment(2);
            $skip_hook = array('display_popup_template','ajax_get_sig_process','ajax_get_next_time');
            if (in_array($func, $skip_hook)) {
                define('_SKIP_HOOK_', true);
            }
        }
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $privs = array();
        $baseURL = $ci->config->config['base_url'];

        if(!empty($_SERVER['HTTP_REFERER'])){
            if(strpos($_SERVER['HTTP_REFERER'],trim($baseURL,'/'))===false){
                $rec=array();
                $rec['time'] = date('Y-m-d H:i:s');
                $rec['u_id'] =  isset($ci->session->userdata['id'])?$ci->session->userdata['id']:'';
                $rec['uri'] = current_url() . $_SERVER['QUERY_STRING'];
                $rec['ref'] = $_SERVER['HTTP_REFERER'];
                
                $tar = __DIR__.'/../../script/log/origin_outside.csv';
                $str = '';
                if(!file_exists($tar)){
                    $str .= implode(',',array_keys($rec))."\n";
                }
                $str .= implode(',',($rec))."\n";
                file_put_contents($tar, $str,FILE_APPEND);
            }
        }
        
        $ci->load->library('router');
        $type = $ci->router->fetch_method();
        
        //assign smarty admin remote flag
        if (isset($ci->session->userdata['remote']) && $ci->session->userdata['remote'] == true) {
            $ci->smarty->assign("admin_remote", true);
        } else {
            $ci->smarty->assign("admin_remote", false);
        }

        if (isset($type) && $type != "logout") {
            if ($ci->authentication->logged_in()) {
                $user_data = $ci->authentication->user($ci->session->userdata['id'])->row();

                if (isset($user_data->user_name) && !empty($user_data->user_name)) {
                    $ci->session->set_userdata('user_currency', $user_data->user_currency);
                    
                    if ($ci->session->userdata['id']) {
                        $ci->smarty->assign("user_id", $ci->session->userdata['id']);
                        if (!defined('_FB_USER_ID_')) {
                            define('_FB_USER_ID_', $ci->session->userdata['id']);
                        }
                    }

                    if (!$ci->session->userdata('user_name')) {
                        $ci->session->set_userdata('user_name', $user_data->user_name);
                    }

                    if ($ci->session->userdata('user_name')) {
                        $ci->smarty->assign("user_name", $ci->session->userdata('user_name'));
                    }

                    if ($ci->session->userdata('logged')) {
                        $ci->smarty->assign("logged", "true");
                    }

                    if ($ci->session->userdata('user_first_name')) {
                        $ci->smarty->assign("user_first_name", $ci->session->userdata('user_first_name'));
                    }

                    if ($ci->session->userdata('user_las_name')) {
                        $ci->smarty->assign("user_las_name", $ci->session->userdata('user_las_name'));
                    }

                    $ci->smarty->assign("page_name", $ci->router->method);

                    $ci->load->model('paypal_model', 'model');
                    $pk = $ci->model->get_pk_zone_expired();
                    if (!empty($pk) && isset($pk[$ci->session->userdata['id']])) {
                        $num_pk_exp = sizeof($pk[$ci->session->userdata['id']]);
                        $ci->smarty->assign("expired_txt", str_replace(' ', '&nbsp;', "$num_pk_exp "));
                    }

                    return true;
                }
            } else {
                if ($ci->session->userdata('id')) {
                    if (!isset($privs[$ci->session->userdata('id')])) {
                        return true;
                    } else {
                        /////////////////////
                        $_SERVER['QUERY_STRING'] = $_SERVER['QUERY_STRING'] == '' ? '' : '?' . $_SERVER['QUERY_STRING'];
                        $uri = current_url() . $_SERVER['QUERY_STRING'];
                        $ci->session->set_userdata('error', 'session_error');
                        $ci->session->set_userdata('original_redirect', $uri);
                        ///redirect('users/login', 'refresh');
                        //////////
                        redirect($baseURL . '/users/login');  //redirect to not-authorized page
                    }
                }
            }
        }
    }
}
