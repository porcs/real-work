<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//*********************************************************************************************************///
class Hooks_referral
{
    
    
    public function getReferral()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci =&get_instance();
        if (!isset($ci->session)) {
            $ci->load->library('session');
        }
        $ip_addr = $_SERVER['REMOTE_ADDR'];
        $user_agent = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
        $ses =$ci->session->userdata('referral');
        $uri = current_url().$_SERVER['QUERY_STRING'];
         
        if (strpos('ajax_', $uri)!==false) {
            return;
        }
         
        if (empty($ses) && $ses =='') {
            $ci->db->select('*');
            $ci->db->from('referral_history');
            $ci->db->where('ip', $ip_addr);
            //$ci->db->where('user_agent',$user_agent);
            $ci->db->limit(1);
            $q = $ci->db->get();
            $uid=0;
            if ($q->num_rows()>0) {
                $d = $q->row_array();
                $uid = $d['parent_id'];
                $ci->session->set_userdata('referral', $uid);
                $id = $ci->session->userdata('id');
                if ($id != $ses) {
                    $data = array('last_date'=>date('Y-m-d H:i:s'));
                    if ($id!=''&& $id!=0) {
                        $data['id_user'] = $id;
                    }
                    $ci->db->update('referral_history', $data, "ip = '$ip_addr'");
                    if (strpos($uri, 'ajax') === false) {
                        $ci->db->insert('referral_click_history', array('ip'=>$ip_addr, 'url'=>$uri, 'user_agent'=>$user_agent));
                    }
                }
            } else {
                if ($ci->input->get($ci->config->item('aff_ref_name'))) {
                    $ci->load->library('Cipher', array('key'=>$ci->config->item('encryption_key')));
                    $key = str_replace('plus', '+', $ci->input->get($ci->config->item('aff_ref_name')));
                    $uname =$ci->cipher->decrypt($key);
                    $uid = $ci->authentication->get_username_id($uname);
                    $data = array(
                    'ip' => $ip_addr,
                    'user_agent' => $user_agent,
                    'parent_id' => $uid,
                    'key'=>$key,
                    'create_date'=>date('Y-m-d H:i:s'),
                     
                 );
               
                    $ci->db->insert('referral_history', $data);
                    if (strpos($uri, 'ajax') === false) {
                        $ci->db->insert('referral_click_history', array('ip'=>$ip_addr, 'url'=>$uri, 'user_agent'=>$user_agent));
                    }
                    $ci->session->set_userdata('referral', $uid);
                }
            }
            
//           echo 'qqq'; 
//           echo $uname.' '.$uid;
        } else {
            $id = $ci->session->userdata('id');
            if ($id!='' && $id!=0 && $id != $ses) {
                $data = array('last_date'=>date('Y-m-d H:i:s'));
                if ($id!=''&& $id!=0) {
                    $data['id_user'] = $id;
                }
                $ci->db->update('referral_history', $data, "ip = '$ip_addr'");
                if (strpos($uri, 'ajax') === false) {
                    $ci->db->insert('referral_click_history', array('ip'=>$ip_addr, 'url'=>$uri, 'user_agent'=>$user_agent));
                }
            }
        }
        
        
        return true;
    }
}
//*********************************************************************************************************///
