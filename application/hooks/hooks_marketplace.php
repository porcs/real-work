<?php

class Hooks_marketplace
{

    const ID_GENERAL = 6; // ID:General or Custom
    public function get_marketplace_manu()
    {
        $this->get_marketplace_menu();
    }
    public function get_marketplace_menu()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci = &get_instance();
        $data = array();

        if ($ci->authentication->logged_in() && isset($ci->session->userdata['id']) && !empty($ci->session->userdata['id']) && isset($ci->session->userdata['user_name'])) {
            $this->id_user = $ci->session->userdata['id'];
            if (!$ci->session->userdata("menu")) {
                $ci->load->model('marketplace_model', 'marketplace');
                $offerUserPackage = $ci->marketplace->getMarketplaceMenu($this->id_user, true);

                foreach ($offerUserPackage as $marketplace) {
                    $mk_name = $marketplace['name_offer_pkg'] ;
                    $sub_marketplace = $marketplace['sub_marketplace'] ; // sub_marketplace
            $sub_mk_id = $marketplace['id_offer_sub_pkg'] ; // id_country

            if ($marketplace['id_offer_pkg'] == self::ID_GENERAL) { // effect to : \controllers\mirakl.php 
               
                        $name_marketplace = $ci->marketplace->getSubMarketplaceName($sub_marketplace);
                $data[$mk_name]['id'] = $marketplace['id_offer_pkg'];
                        
                $data[$mk_name]['submenu'][$sub_marketplace]['name_marketplace'] = $name_marketplace;
                $data[$mk_name]['submenu'][$sub_marketplace]['sub_marketplace'] = $sub_marketplace;
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['name_marketplace'] = $name_marketplace;
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['id'] = $sub_mk_id;
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['id_region'] = $marketplace['id_region'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['region'] = $marketplace['name_region'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['country'] = $marketplace['title_offer_sub_pkg'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['ext'] = $marketplace['ext_offer_sub_pkg'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['active'] = $marketplace['status_offer_price_pkg'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['currency'] = $marketplace['currency_offer_price_pkg'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['ebay_site'] = $marketplace['name'];
                $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['id_site_ebay'] = $marketplace['id_site_ebay'];

                if (isset($sub_marketplace) && !empty($sub_marketplace)) {
                    $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['sub_marketplace'] = $sub_marketplace;
                }
                        
                if (isset($marketplace['domain_offer_sub_pkg'])) {
                    $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['domain'] = $marketplace['domain_offer_sub_pkg'];
                } elseif (isset($marketplace['title_offer_sub_pkg'])) {
                    $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['domain'] = $marketplace['title_offer_sub_pkg'];
                } else {
                    $data[$mk_name]['submenu'][$sub_marketplace][$sub_mk_id]['domain'] = $mk_name;
                }
            } else {
                $data[$mk_name]['id'] = $marketplace['id_offer_pkg'];

                $data[$mk_name]['submenu'][$sub_mk_id]['id'] = $marketplace['id_offer_sub_pkg'];
                $data[$mk_name]['submenu'][$sub_mk_id]['id_region'] = $marketplace['id_region'];
                $data[$mk_name]['submenu'][$sub_mk_id]['region'] = $marketplace['name_region'];
                $data[$mk_name]['submenu'][$sub_mk_id]['country'] = $marketplace['title_offer_sub_pkg'];
                $data[$mk_name]['submenu'][$sub_mk_id]['ext'] = $marketplace['ext_offer_sub_pkg'];
                $data[$mk_name]['submenu'][$sub_mk_id]['currency'] = $marketplace['currency_offer_price_pkg'];
                $data[$mk_name]['submenu'][$sub_mk_id]['ebay_site'] = $marketplace['name'];
                $data[$mk_name]['submenu'][$sub_mk_id]['id_site_ebay'] = $marketplace['id_site_ebay'];

                if (isset($marketplace['comments']) && !empty($marketplace['comments'])) {
                    $data[$mk_name]['submenu'][$sub_mk_id]['comments'] = $marketplace['comments'];
                }

                if (isset($marketplace['domain_offer_sub_pkg'])) {
                    $data[$mk_name]['submenu'][$sub_mk_id]['domain'] = $marketplace['domain_offer_sub_pkg'];
                } elseif (isset($marketplace['title_offer_sub_pkg'])) {
                    $data[$mk_name]['submenu'][$sub_mk_id]['domain'] = $marketplace['title_offer_sub_pkg'];
                } else {
                    $data[$mk_name]['submenu'][$sub_mk_id]['domain'] = $mk_name;
                }
            }

                    unset($mk_name) ;
                    unset($sub_marketplace) ;
                    unset($sub_mk_id) ;
                }

                $ci->session->set_userdata("id_general", self::ID_GENERAL);
                $ci->session->set_userdata("menu", $data);

                unset($data);
            }
            $ci->smarty->assign("menu", $ci->session->userdata("menu"));
        }
    }
}
