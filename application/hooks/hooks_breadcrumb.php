<?php

class Hooks_breadcrumb
{

    const GENERAL = 'General'; // for mirakl only!

    public function set_breadcrumb()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci = & get_instance();
        $ci->breadcrumb->clear();

        $uri = array();
        $skip = false;
        $common_path = '';
        $lcp = array();
        
        $name_marketplace = ''; // for mirakl only!
        $segs = array();
        $segs = isset($ci->uri->segments) ? $ci->uri->segments : '';
                  
        if (isset($segs[1]) && $segs[1] == strtolower(self::GENERAL) && isset($segs[3]) && is_numeric($segs[3]) && isset($segs[4]) && is_numeric($segs[4])) { // for mirakl only! edit: 29/03/2016
            $sub_marketplace = $segs[3];
            $id_country = $segs[4];
            $ci->load->model('marketplace_model', 'marketplace');
            $name_marketplace = $ci->marketplace->getSubMarketplaceName($sub_marketplace);
        }

        foreach ($ci->uri->segments as $key => $segment) {
            if ($key <= 0) {
                $key = 1;
            }

            if (($segment != '' && ($segment != 'tasks') && strpos($segment, 'ajax') === false && strpos($segment, 'remote') === false && $segment != 'errors' && $segment != 'webservice' && !is_numeric($segment))) {
                $skip = false;
            } else {
                $skip = true;
            }

            if ($skip) {
                break;
            }

            $lcp[($key - 1)] = $segment;
        }

        if (!empty($lcp)) {
            $common_path = 'url:'.implode('/', $lcp);
        }

        $this->language = $ci->session->userdata('language') ? $ci->session->userdata('language') : $ci->config->item('language');

        $skip_nbc = true;

        if (method_exists($ci->lang, 'check_file_line')) {
            $file = 'breadcrumb';

            $ci->lang->load("page_title", $this->language);

            if ($ci->lang->check_file_line('page_title', $common_path)) {
                $ci->smarty->assign("page_title_specify", $ci->lang->line($common_path, 'page_title'));
            }

            $ci->lang->load($file, $this->language);
            $t = array();

            if ($ci->lang->check_file_line($file, $common_path)) {
                $txt_bc = $ci->lang->line($common_path, $file);
                
                if (isset($name_marketplace) && !empty($name_marketplace)) { // for mirakl only!
                    $txt_bc = sprintf($txt_bc, $name_marketplace);
                }

                $list_bc = explode('>', $txt_bc);

                if (is_array($list_bc)) {
                    foreach ($list_bc as $k => $bc_elem) {
                        $bc_elem = trim($bc_elem);
                        $ci->breadcrumb->add_crumb($bc_elem);
                        $skip_nbc = false;
                        $t[] = $bc_elem;
                    }
                } else {
                    if (!is_bool($list_bc) && !empty($list_bc)) {
                        $ci->breadcrumb->add_crumb($list_bc);
                        $skip_nbc = false;
                    }
                }
            }
        }

        if ($skip_nbc === true) {
            $ci->breadcrumb->clear();
            $skip = false;
            $ci->load->model('marketplace_model', 'marketplace');
            $name_marketplace = ''; // for mirakl only!

            foreach ($ci->uri->segments as $key => $segment) {
                if ($skip == true) {
                    break;
                }

                if ($key <= 0) {
                    $key = 1;
                }

                $uri[($key - 1)] = $segment;

                if ($segment != '' && ($segment != 'tasks') && strpos($segment, 'ajax') === false && strpos($segment, 'remote') === false && $segment != 'errors' && $segment != 'webservice' && !is_numeric($segment)) {
                    if (isset($uri[0]) && $uri[0] != strtolower(self::GENERAL)) {
                        $ci->breadcrumb->add_crumb(str_replace("_", " ", ($ci->lang->line($segment)) ? $ci->lang->line($segment) : ucfirst($segment)));
                    }

                    $skip = false;
                } else {
                    if (isset($uri[0]) && $uri[0] == strtolower(self::GENERAL)) { // for mirakl only!

                        if (isset($uri[2]) && isset($uri[3])) {
                            if ($uri[1] == 'actions') {
                                $name_marketplace = $ci->marketplace->getSubMarketplaceName($uri[3]);
                            } else {
                                $name_marketplace = $ci->marketplace->getSubMarketplaceName($uri[2]);
                            }
                            
                            if ($key == 4) {
                                $ci->breadcrumb->add_crumb(str_replace("_", " ", $name_marketplace));
                                $ci->breadcrumb->add_crumb(str_replace("_", " ", $ci->lang->line($uri[1])));
                            }
                            
                            if ($key == 5) {
                                $ci->breadcrumb->add_crumb(str_replace("_", " ", $ci->lang->line($uri[2])));
                            }
                        }
                    } else {
                        $skip = true;
                    }
                }
            }
        } else {
            foreach ($ci->uri->segments as $key => $segment) {
                if ($key <= 0) {
                    $key = 1;
                }
                $uri[($key - 1)] = $segment;
            }
            $ci->smarty->assign("breadcrumb_specify", $ci->breadcrumb->output());
        }

        $ci->smarty->assign("url", $uri);
        $ci->smarty->assign("breadcrumb", $ci->breadcrumb->output());
    }
}
