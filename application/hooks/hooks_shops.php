<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Hooks_shops
{
    
    public static $exclude_popup = array('dashboard' => 1,'users' => 1,'service' => 1,'my_shop' => 1);
        
    public function get_default_shop()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci =&get_instance();
        
        $ci->load->library('router');
        
        if ($ci->authentication->logged_in() && isset($ci->session->userdata['id']) && !empty($ci->session->userdata['id']) && isset($ci->session->userdata['user_name'])) {
            $username = $ci->session->userdata['user_name'];
        
            if (!isset($ci->authentication)) {
                $ci->load->library('authentication');
            }
        
            if (isset($ci->authentication)) {
                if ($ci->authentication->logged_in() && isset($ci->session->userdata['id']) && !empty($ci->session->userdata['id'])) {
                    $type = $ci->router->fetch_class();
                    $shop_default = '';
                    if (isset($type) && $type != "feed_biz_shop") {
                        if ($type == "offers" || $type == "profiles") {
                            $ci->load->library('FeedBiz_Offers', array($username));
                            if (isset($ci->feedbiz_offers)) {
                                $shop_data = $ci->feedbiz_offers->getShops($username);
                                $shop_default = $ci->feedbiz_offers->getDefaultShop($username);
                            }
                        } elseif ($type == "ebay") {
                            $method = $ci->router->fetch_method();

                            if ($method == "getSelectedAllCategory" || $method == "getSelectedAllTemplate") {
                                $ci->load->library('FeedBiz_Offers', array($username));
                                if (isset($ci->feedbiz_offers)) {
                                    $shop_data = $ci->feedbiz_offers->getShops($username);
                                    $shop_default = $ci->feedbiz_offers->getDefaultShop($username);
                                }
                            } else {
                                $ci->load->library('FeedBiz', array($username));
                                if (isset($ci->feedbiz)) {
                                    $shop_data = $ci->feedbiz->getShops($username);
                                    $shop_default = $ci->feedbiz->getDefaultShop($username);
                                }
                            }
                        } else {
                            $ci->load->library('FeedBiz', array($username));

                            if (isset($ci->feedbiz)) {
                                $shop_data = $ci->feedbiz->getShops($username);
                                $shop_default = $ci->feedbiz->getDefaultShop($username);
                            }
                        }
                    }
            
                    if (isset($shop_default) && !empty($shop_default)) {
                        $ci->smarty->assign("shop", $shop_data);
                        $ci->smarty->assign("shop_default", $shop_default['name']);
                        $ci->session->set_userdata('id_shop', $shop_default['id_shop']);
                        $ci->session->set_userdata('shop_default', $shop_default['name']);
                        $ci->smarty->assign('id_shop', $shop_default['id_shop']);
                    } else {
                        if (isset($type) && !isset(self::$exclude_popup[$type])) {
                            $ci->smarty->assign('missing_id_shop', true);
                        }
                    }
                    
                    return $ci->smarty;
                }
            }
        }
    }

    public function get_shop_language()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci =&get_instance();

        $ci->load->library('router');

        if ($ci->authentication->logged_in() && isset($ci->session->userdata['id_shop']) && isset($ci->session->userdata['user_name'])) {

            $username = $ci->session->userdata['user_name'];
            $id_shop = $ci->session->userdata['id_shop'];
            $type = $ci->router->fetch_class();

            if (isset($type) && $type != "feed_biz_shop") {
                if ($type != "offers" && $type != "profiles") {
                    $ci->load->library('FeedBiz', array($username));
                    if (isset($ci->feedbiz)) {
                        $language_data = $ci->feedbiz->getLanguage($id_shop);
                    }
                    if(isset($language_data) && !empty($language_data)){
                        $ci->session->set_userdata('shop_language', $language_data);
                    }
                }
            }

            if(isset($ci->session->userdata['shop_language'])) {
                $ci->smarty->assign("shop_language", $ci->session->userdata['shop_language']);
            }

            return $ci->smarty;

        }
    }
}
