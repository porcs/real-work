<?php

class Hooks_language
{
    
    public function initialize($label, $language = 'english')
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci =& get_instance();
        $ci->load->helper('language');
        $ci->lang->load($label, $language);
    }
    
    public function get_language()
    {
    }
}
