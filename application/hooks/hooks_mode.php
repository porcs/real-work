<?php

class Hooks_mode
{
    
    public function get_default_mode()
    {
        if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci = &get_instance();
        
        if ($ci->authentication->logged_in() && isset($ci->session->userdata['id']) && !empty($ci->session->userdata['id'])) {
            $ci->load->model('my_feed_model');
            $mode_default = $ci->my_feed_model->get_default_mode($ci->session->userdata['id']);
            $u_data = array();
        
            if (isset($mode_default['mode']) && !empty($mode_default['mode'])) {
                $ci->smarty->assign("mode_default", $mode_default['mode']);
                $u_data['mode_default'] = $mode_default['mode'];
            }
            
            $type = $ci->router->fetch_class();
            if ($type == "offers" || $type == "profiles") {
                return;
            }
            
            // Amazon Creation
            if (empty($ci->session->userdata['amazon_mode_creation']) && !empty($ci->session->userdata['id'])) {
                $ci->load->model("amazon_model", "amazon_model_hook");
                $amazon_models = $ci->amazon_model_hook->get_mode($ci->session->userdata['id'], $ci->session->userdata('user_name'));

                $u_data['amazon_mode_creation'] = $amazon_models;
            } else {
                $amazon_models = $ci->session->userdata['amazon_mode_creation'];
                if (empty($amazon_models)) {
                    $ci->load->model("amazon_model", "amazon_model_hook");
                    $amazon_models = $ci->amazon_model_hook->get_mode($ci->session->userdata['id'], $ci->session->userdata('user_name'));
                }
            }
        
            if (isset($ci->session->userdata['id_shop']) && isset($amazon_models[$ci->session->userdata['id_shop']])) {
                $amazon_model = $amazon_models[$ci->session->userdata['id_shop']];
            }
            
            if (!empty($u_data)) {
                $ci->session->set_userdata($u_data);
            }
            
            if (isset($amazon_model) && !empty($amazon_model)) {
                if (isset($amazon_model['mode']['creation']) && !empty($amazon_model['mode']['creation'])) {
                    $ci->smarty->assign("amazon_creation_model", $amazon_model['mode']['creation']);
                }
        
                $ci->smarty->assign("amazon_display", $amazon_model['fields']);

                // Error Resolation
                if (isset($amazon_model['error_resolution']) && !empty($amazon_model['error_resolution'])) {
                    $ci->smarty->assign("amazon_error", $amazon_model['error_resolution']);
                }              
            }

            //check restrict
            $mode = $ci->session->userdata('mode_default');
            $file_name = USERDATA_PATH.'/json/restrict_mode.json';
            if(!empty($mode) && file_exists($file_name)){
                $json = file_get_contents($file_name);
                $dlist = json_decode($json,true);
                $seg = $ci->uri->segments ;
                $controller = $ci->router->fetch_class();
                $method = $ci->router->fetch_method();
                $additional = isset($seg[3])?$seg[3]:'';
                $rule=array();
                if(isset($dlist[$controller][$method][$additional])){
                    $rule=$dlist[$controller][$method][$additional];
//                 }else if(isset($dlist[$controller][$method])){
                 }else if(isset($dlist[$controller][$method][''])){
                    $rule=$dlist[$controller][$method][''];
                 }else{
                    //not found restrict;
                    if(!empty($seg)){
                        file_put_contents(USERDATA_PATH.'/json/not_found_restrict_mode.txt',implode('/',$seg)."\n",FILE_APPEND);
                    }
                }
                $need=0;
                  if(!empty($rule) && !empty($rule['free']) && !empty($rule['pro'])  && !empty($rule['adv'])){
//                if(!empty($rule) && isset($rule['free']) && isset($rule['pro'])  && isset($rule['adv'])){
                    switch($mode*1){
                        case 1:
                            if($rule['free']!=1&&$rule['pro']!=1){
                                $need=2;
                            }elseif($rule['free']!=1){
                                $need=1;
                            }
                            break;
                        case 2:
                            if($rule['pro']!=1){
                                $need=3;
                            }
                            break;
                        case 3:
                            //allow all pages
                            break;
                    }
                }else{
//                    file_put_contents(USERDATA_PATH.'/json/found_restrict_mode.txt',implode('/',$seg).'#'.implode('-',array($controller,$method,$additional))."\n".print_r($rule,true)."\n",FILE_APPEND);
                }
                        
                if($need==1){
                     $ci->smarty->assign("expert_mode", true);
                }elseif($need==2||$need==3){
                     $ci->smarty->assign("advance_mode", true);
                }

                $mr = $ci->session->userdata('manager_remote');
                if($mr===true && isset($rule['man'])){
                    if($rule['man']!=1){
                        $ci->smarty->assign("manager_mode", true);
                    }
                }
 			$wz = $ci->session->userdata('open_wizard');
                if(!empty($wz)){
                    $ci->smarty->assign("open_wizard", true);
                }
                
                        
            }
        }
    }
}
