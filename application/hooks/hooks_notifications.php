<?php

/* To assign session to view */

class Hooks_notifications
{
    
    public function get_notifications()
    {
       if (defined('_SKIP_HOOK_')) {
            return;
        }
        $ci = &get_instance();
        if ($ci->authentication->logged_in() && isset($ci->session->userdata['user_name']) && isset($ci->session->userdata['id_shop'])) {

            $user_name = $ci->session->userdata['user_name'];
            
            // 1 get noti from db
            $ci->load->library('notifications', array($user_name));
            $notifications = $ci->notifications->getNotifications();
           
            if(isset($notifications[$ci->session->userdata['id_shop']]) && is_array($notifications[$ci->session->userdata['id_shop']])){
                $ci->smarty->assign("notifications", $notifications[$ci->session->userdata['id_shop']]);
            }
        }
    }
}
