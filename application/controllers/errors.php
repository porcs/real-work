<?php
require_once dirname(__FILE__) . '/../libraries/errorHandler.php';

class errors extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("errors", $this->language);
        $this->smarty->assign("label", "errors");
    }
    
    public function display($page)
    {
        $data = array('page' => $page);
        $this->smarty->view('errors.tpl', $data);
    }

    public function getErrorHandler($code, $content)
    {
        if (isset($code) && !empty($code) && isset($content) && !empty($content)) {
            $this->db->select('id');
            $this->db->from('users');
            $this->db->where('user_code', $code);
            $query = $this->db->get()->row();

            if (isset($query) && !empty($query)) { 
                $err_handler = new errorHandler();
                $err_handler->get_error_handle($content);
                
                exit;
            }
        } 
        echo 'Access denied!';
        exit;
    }
    
    public function error404()
    {
        $this->smarty->view('error404.tpl');
    }
}
