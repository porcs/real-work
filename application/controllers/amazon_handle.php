<?php

require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.advertising.class.php';

class Amazon_Handle extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function login($id_country){
        if(isset($_GET['code']) && !empty($_GET['code'])){
            $code = $_GET['code'];
            $this->session->set_userdata('message', 'login_success');
            //$code = "ANpBYqQGeHrjjJjVwRqS";
            $this->session->set_userdata('ad_authen_code', $code);
            helper_redirect('amazon/advertising/login/', $id_country.'/2');
        } else {
            $this->session->set_userdata('error', 'login_fail');
            helper_redirect('amazon/advertising/login/', $id_country);
        }
        exit;
    }
   
    public function authorization($code){
        if(!empty($code)){
            $api = $this->_get_api();
            $config =
                array(
                "clientId" => "amzn1.application-oa2-client.8782dff0b54e4c3baa0e18d792f13c25",
                "clientSecret" => "87d5c83ed261693b7683ef12d74f950268d7008fed1610cf14cd1caea04e682d",
                "region" => "na",
            );
            $redirectUri = 'https://dev.feed.biz/amazon_handle/login';
            $client = new AmazonAdvertising($config);
            $request = $client->requestAccessToken($code, $redirectUri);
            echo json_encode($request);
            exit;
        }
    }

    public function registerProfile($token_access){
        $config = array(
            "clientId" => "amzn1.application-oa2-client.8782dff0b54e4c3baa0e18d792f13c25",
            "clientSecret" => "87d5c83ed261693b7683ef12d74f950268d7008fed1610cf14cd1caea04e682d",
            "region" => "na",
            "accessToken" => rawurldecode($token_access),
            "sandbox" => true);

        $client = new AmazonAdvertising($config);
        $request = $client->registerProfile(array("countryCode" => "US"));
        //echo json_encode(array('token_access'=>rawurldecode($token_access), 'request' => $request)); exit;
        echo json_encode($request); exit;
    }

    public function registerProfileStatus($token_access, $registerProfileId){
        $config = array(
            "clientId" => "amzn1.application-oa2-client.8782dff0b54e4c3baa0e18d792f13c25",
            "clientSecret" => "87d5c83ed261693b7683ef12d74f950268d7008fed1610cf14cd1caea04e682d",
            "accessToken" => rawurldecode($token_access),
            "region" => "na",
            "sandbox" => true);

        $client = new AmazonAdvertising($config);
        $request = $client->registerProfileStatus(rawurldecode($registerProfileId));
        echo json_encode($request); exit;
    }

    private function _get_api(){
        include dirname(__FILE__) . '/../libraries/Amazon/parameters/api.settings.php';
        return isset($amazon_advertising_api) ? $amazon_advertising_api : array();
    }
  
}