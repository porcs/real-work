<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once(dirname(__FILE__) . '/component/component.class.php');
require_once(dirname(__FILE__) . '/../libraries/GoogleShopping/classes/google.install.php');

class new_marketplace extends Component_controller
{
    public function __construct($a = null)
    {
        parent::__construct(array(
            'prefix' => 'google',
            'marketplace_name' => 'google_shopping'),
            2);
        new GoogleInstall($this->user_name);
    }
    
    public function scheduled_tasks()
    {
        $this->component_schedule();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }

    public function parameter()
    {
        $this->component_parameter();
        $this->smarty->view('google_shopping/google_shopping_template.tpl', $this->data);
    }

    public function models()
    {
        $this->component_models();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function profiles($a)
    {
        $this->component_profiles();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function categories()
    {
        $this->component_categories();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function carriers()
    {
        $this->component_carriers();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function conditions()
    {
        $this->component_conditions();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function attributes()
    {
        $this->component_attributes();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function reports()
    {
        $this->component_reports();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function logs()
    {
        $this->component_logs();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function order_report()
    {
        $this->component_order_report();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function actions_product()
    {
        $this->component_actions_product();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function actions_offer()
    {
        $this->component_actions_product();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function actions_order()
    {
        $this->component_actions_product();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function actions_delete()
    {
        $this->component_actions_product();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function error_resolutions()
    {
        if ($this->input->post()) {
            $this->component_error_resolutions();
        } else {
            $this->smarty->view('components/components_template.tpl', $this->data);
        }
    }
    
    public function error_resolutions_show()
    {
        $values = $this->input->post();
            
        $value = array('draw' => 1, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array());
        echo json_encode($value);
    }
}
