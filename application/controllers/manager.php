<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class manager extends CI_Controller
{

    public function __construct()
    {
             
        // load controller parent
        parent::__construct();
        
        $this->id_user = $this->session->userdata('m_id');
        $this->user_name = $this->session->userdata('user_name'); 
        
        
        if ((!isset($this->user_name) || empty($this->user_name))
            || (!isset($this->id_user) || empty($this->id_user))) {
            redirect('users/login');
        }
        $this->load->library('authentication');
//        if ($this->user_name!='') {
//            $this->load->library('FeedBiz', array($this->user_name));
//        }
//        $this->load->library('Cipher',
//            array('key'=>$this->config->item('encryption_key')));
         
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("manager", $this->language);

        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->db_type = $this->config->item('backend_db');
        //load Module
        $this->load->model('manager_model'); 
        $this->token_assist = $this->manager_model->getManagerToken($this->id_user);
        if(empty($this->token_assist)){
            $this->token_assist = $this->manager_model->setManagerToken($this->id_user);
            $this->session->set_userdata('token_assist',$this->token_assist);
        }
        $this->smarty->assign("token_assist", $this->token_assist);
        $this->smarty->assign("label", "manager");
        $this->smarty->assign("logged", $this->id_user);
        $this->smarty->assign("user_id", $this->id_user);
    }

     
        
    public function index()
    {
        $userdata = $this->authentication->user($this->id_user)->row();
        if (!empty($userdata->user_name)) {
            $this->smarty->assign("user_name", $userdata->user_name);
        }
        if (!empty($userdata->user_first_name)  ) {
            $this->smarty->assign("user_first_name",
                $userdata->user_first_name);
        }
        if (!empty($userdata->user_las_name)  ) {
            $this->smarty->assign("user_las_name",
                $userdata->user_las_name);
        }
        $data = array();
        $user_list = $this->manager_model->getUserInService($this->token_assist); 
        $data['user_list']  = $user_list;
        $this->smarty->assign($data);
        $this->smarty->view('manager/dashboard.tpl', $data);
    }
    public function ajax_update_token(){
        $post = $this->input->post();
        if (!empty($post['token'])) {
            if($this->manager_model->updateManagerToken($post['token'],$this->token_assist)){
                $this->session->set_userdata('token_assist',$post['token']);
                echo json_encode(array('result'=>'pass'));
            }else{
                echo json_encode(array('result'=>false));
            }
        }
    }
    public function remote($u_id){
        $user_list = $this->manager_model->getUserInService($this->token_assist);
        $id_list = array();
        foreach($user_list as $k=>$u){
            $id_list[$u['id']]=$k;
        }
        if(isset($id_list[$u_id])){
            $userdata = $user_list[$id_list[$u_id]];
            $language = $this->authentication->getLanguageNameByID($userdata['user_default_language']);
            $session_data = array(
                'm_id'=>$this->id_user,
                'id' => $userdata['id'],
                'user_email' => $userdata['user_email'],
                'user_name' => $userdata['user_name'] ,
                'user_first_name' => $userdata['user_first_name'],
                'user_las_name' => $userdata['user_las_name'],
                'language' => !empty($language) ? $language : 'english',
                'logged' => true,
                'time' => time(),
                'ip' => $this->input->ip_address(),
                'first_login' => true,
                'manager_remote' => true
            );
            $this->session->set_userdata($session_data);
            $this->authentication->generateNewOTP($userdata['id'], true);
            redirect('dashboard', 'refresh');
        }else{
            redirect('manager', 'refresh');
        }
    }

    public function security($action='')
    {
//        $this->authentication->checkAuthen();

        include(dirname(__FILE__).'/../libraries/mfa/config.php');
        $this->load->model('my_feed_model', 'user_model');

        if ($this->input->post()) {
            $mfa_info = $this->user_model->get_mfa_info($this->id_user);
            if (!empty($mfa_info['mfa_secret_key'])) {
                $mfa_secret_key = $mfa_info['mfa_secret_key'];
            } else {
                $word = $this->lang->line('Please scan your QR code again. The key is wrong.');
                $this->session->set_userdata('error', $word);
                redirect('/manager/security', 'refresh');
                return;
            }
            $post = $this->input->post();
            if (!empty($post['otp_reverify'])) {
                $currentcode=$post['otp_reverify'];
                if (TokenAuth6238::verify($mfa_secret_key, $currentcode,10)) {
                    $status_case=1;
                    $this->user_model->update_mfa_info($this->id_user, null, '0');
                    $word = $this->lang->line('Successful!, the login security was deactivated.');
                    $this->session->set_userdata('message', $word);
                    $this->user_model->update_mfa_status_users();
                } else {
                    $word = $this->lang->line('Please try again. Your OTP is wrong.');
                    $this->session->set_userdata('error', $word);
                }
            } elseif (!empty($post['otp_verify'])) {
                $currentcode=$post['otp_verify'];
                if (TokenAuth6238::verify($mfa_secret_key, $currentcode,10)) {
                    $this->user_model->update_mfa_info($this->id_user, null, 1);
                    $word = $this->lang->line('Successful!, the login security was activated on your account.');
                    $this->session->set_userdata('message', $word);
                    $this->user_model->update_mfa_status_users();
                } else {
                    $word = $this->lang->line('Please try again. Your OTP is wrong.');
                    $this->session->set_userdata('error', $word);
                }
            } else {
                $word = $this->lang->line('Please try again. Your OTP is wrong.');
                $this->session->set_userdata('error', $word);
            }
            $this->user_model->update_store_user_info();
            redirect('/manager/security', 'refresh');
            return;
        }

        if (!empty($action)&&$action=='reset_otp') {
            $g = new GoogleAuthenticator();
            $mfa_secret_key =$g->generateSecret();
            $status_case = 0;
            $this->user_model->update_mfa_info($this->id_user, $mfa_secret_key, 0, $status_case);
            $word = $this->lang->line('Successful!, Your QR code was reset.');
            $this->session->set_userdata('message', $word);
            redirect('/manager/security', 'refresh');
            return;
        }
        $mfa_info = $this->user_model->get_mfa_info($this->id_user);
        if (!empty($mfa_info['mfa_secret_key'])) {
            $mfa_secret_key = $mfa_info['mfa_secret_key'];
        } else {
            $g = new GoogleAuthenticator();
            $mfa_secret_key =$g->generateSecret();
            $this->user_model->update_mfa_info($this->id_user, $mfa_secret_key);
        }

        $site_name = $this->config->item('site_name');
        if (!empty($site_name)) {
            $title = $site_name;
        } else {
            $title = $this->config->item('base_url');
        }

        $data['mfa_enable'] = !empty($mfa_info['mfa_enable'])&&$mfa_info['mfa_enable']==1?true:false;
        if (!$data['mfa_enable']) {
            $data['qr_code'] = TokenAuth6238::get_qr_code($this->session->userdata('user_email'), $mfa_secret_key, $title);
        } else {
            $data['qr_code']='';
        }
        $data['mfa_first_time']  = empty($mfa_info['mfa_enable']) || (!empty($mfa_info['mfa_enable']) && empty($mfa_info['mfa_enable'])==0)?true:false;
        if ($this->session->userdata('error')) {
            $data['error'] = $this->session->userdata('error');
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
//        $this->smarty->assign('cdn_url', base_url('/'));
        $this->smarty->view('users/security.tpl', $data);
    }
}
