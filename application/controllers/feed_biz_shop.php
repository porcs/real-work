<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Feed_biz_shop extends CI_Controller
{
    
    public $id_shop;
    public $user_name ;

    public function __construct()
    {
        
        // load controller parent
        parent::__construct();
        $this->language = $this->config->item('language');
        //1. library
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("feed_biz_shop", $this->language);
        $this->user_name = $this->session->userdata('user_name');
    }
     
    public function set_default_shop_product($id_shop)
    {
        if (isset($id_shop) && !empty($id_shop)) {
            $value['id_shop'] = $id_shop;
            $this->load->library('FeedBiz', array($this->user_name));
            
            $shop = $this->feedbiz->setDefaultShop($this->user_name, $value);
            
            if (!$shop) {
                echo "false";
            } else {
                $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
                
                if (isset($DefaultShop['description']) && !empty($DefaultShop['description']) && is_serialized($DefaultShop['description'])) {
                    $DefaultShop = unserialize(base64_decode($DefaultShop['description']));
                }
                //echo '<pre>' . print_r($DefaultShop, true) . '</pre>';
                 //echo '<pre>' . print_r(unserialize(base64_decode($DefaultShop['FEED_BASE_URL'])), true) . '</pre>';
                 //
                //FEED_SHOP_INFO
                if (isset($DefaultShop['FEED_SHOP_INFO']) && !empty($DefaultShop['FEED_SHOP_INFO'])) {
                    $this->load->model("my_feed_model", "my_feed_model");
                    $data = array();
                    $data['id_customer'] = $this->session->userdata('id') ;
                    $data['name'] = 'FEED_SHOP_INFO' ;
                    $data['value'] = $DefaultShop['FEED_SHOP_INFO'];
                    //echo '<pre>' . print_r($data, true) . '</pre>';
                    $this->my_feed_model->set_general($data);
                }
                //FEED_BASE_URL_1
                if (isset($DefaultShop['FEED_BIZ']) && !empty($DefaultShop['FEED_BIZ'])) {
                    $this->load->model("my_feed_model", "my_feed_model");
                    $data = array();
                    $data['id_customer'] = $this->session->userdata('id') ;
                    $data['name'] = 'FEED_BIZ' ;
                    $data['value'] = $DefaultShop['FEED_BIZ'];
                    //echo '<pre>' . print_r($data, true) . '</pre>';
                    $this->my_feed_model->set_general($data);
                }
                
                $this->session->set_userdata(array('id_shop'=> $value['id_shop'], 'shop_name'=> $shop));
//                $this->session->set_userdata('id_shop', $value['id_shop']);
//                $this->session->set_userdata('shop_name', $shop);
                echo "true";
            }
        } else {
            echo "false";
        }
    }
    
    public function set_default_shop_offer($id_shop)
    {
        if (isset($id_shop) && !empty($id_shop)) {
            $value['id_shop'] = $id_shop;
            $this->load->library('FeedBiz_Offers', array($this->user_name));
            
            $shop = $this->feedbiz_offers->setDefaultShop($this->user_name, $value);
            
            if (!$shop) {
                echo "false";
            } else {
                echo "true";
            }
        } else {
            echo "false";
        }
    }
    
    public function get_default_shop_offer()
    {
        $this->load->library('feedBiz_offers', array($this->user_name));
        $shop = $this->feedbiz_offers->getShops($this->user_name);
            
        if ($shop) {
            echo json_encode(array('pass' => true, 'offer_shop' => $shop));
        } else {
            echo json_encode(array('pass' => false));
        }
    }
    
    public function check_shop_offer()
    {
        $this->load->library('FeedBiz_Offers', array($this->user_name));
        $shop = $this->feedbiz_offers->getShops($this->user_name);
            
        if ($shop) {
            echo true;
        } else {
            echo false;
        }
    }
}
