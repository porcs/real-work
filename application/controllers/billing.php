<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class billing extends CI_Controller
{
    private $config_pay;

    public function __construct()
    {
        parent::__construct();
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');
        
        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen(true);//check logged in and redirect to login page

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("paypal", $this->language);
        $this->lang->load("user", $this->language);
        $this->smarty->assign("label", "paypal");
        $this->iso_code = mb_substr($this->language, 0, 2);
        
        // load paypal config
        $this->config->load('paypal_settings');
        $this->config_pay = $this->config->item('pay');
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
    
    public function configuration()
    {
        $data = array();
        
        $user_data = (array)$this->authentication->user($this->id_user)->row();
        $this->load->model('paypal_model', 'model');

        if ($this->input->post()) {
            $cfgArr = (array)$this->input->post('cfg');
            $preapprovalStatus = false;
            $preapprovalProfile = $this->model->getPayment($this->id_user);
            if ($preapprovalProfile) {
                $preapprovalDetails =
                    $this->preapprovalDetail($preapprovalProfile['key_preapproval']);
                                    
                if ($preapprovalDetails['Ack'] == 'Success' &&
                    $preapprovalDetails['Approved'] == 'true' &&
                    $preapprovalDetails['SenderEmail'] != '' &&
                    $preapprovalDetails['Status'] == 'ACTIVE') {
                    $preapprovalStatus = true;
                }
            }

            if (isset($cfgArr['email']) && !empty($cfgArr['email'])) {
                $this->model->setPayerEmail($this->id_user, $cfgArr['email']);
            }

            if (isset($cfgArr['preapproval']) &&
                !empty($cfgArr['preapproval']) &&
                !$preapprovalStatus) {
                $this->session->set_userdata('preapprovalStatus', true);
                redirect(site_url('billing/preapproval/agreement'));
            } else {
                $this->session->set_userdata('payStatus', 'complete');
                redirect(site_url('billing/configuration'));
            }
        } else {
            $payerEmail = $this->model->getPayerEmail($this->id_user);
            if (is_array($payerEmail)) {
                $data['payerEmail'] = $payerEmail['value'];
            } elseif (isset($user_data['user_email']) &&
                !empty($user_data['user_email'])) {
                $data['payerEmail'] = $user_data['user_email'];
            }

            if ($this->session->userdata('payStatus')) {
                $data['payStatus'] = true;
                $this->session->unset_userdata('payStatus');
            }

            $preapprovalProfile = $this->model->getPayment($this->id_user);
            if ($preapprovalProfile) {
                $preapprovalDetails =
                    $this->preapprovalDetail($preapprovalProfile['key_preapproval']);
                if ($preapprovalDetails['Ack'] == 'Success' &&
                    $preapprovalDetails['Approved'] == 'true' &&
                    $preapprovalDetails['SenderEmail'] != '' &&
                    $preapprovalDetails['Status'] == 'ACTIVE') {
                    $this->load->library('mydate');

                    $data['sign'] =
                        $this->mydate->moneySymbol($preapprovalDetails['CurrencyCode']);
                    $data['preapprovalProfile'] = $preapprovalProfile;
                    $data['preapprovalDetails'] = $preapprovalDetails;
                 
                    $data['isPreapproval'] = true;
                }
            }

            $preClick = '';
            if ($this->session->userdata('preapprovalTab')) {
                $this->session->unset_userdata('preapprovalTab');
                $preClick = 'true';
            }

            /* Breadcrumb */
            $this->breadcrumb->clear();
            $this->breadcrumb->add_crumb('Billing');
            $this->breadcrumb->add_crumb('Configuration');
            $this->breadcrumb->change_link(
                '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
            $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /* End ... */
            
            $this->smarty->assign('preClick', $preClick);
            $this->smarty->assign('profile', $user_data);
            $this->smarty->assign('siteurl', site_url());

            $this->smarty->view('billing/configuration.tpl', $data);
        }
    }
    
    public function preapproval($param='')
    {
        $data = array();
        
        $this->load->library('mydate');
        $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));
        
        //$user_data = $data['userData'];

            
//        if(!empty($user_data))
//        {

            if (isset($param) && $param == 'agreement') {
                //                $preapprovalStatus = $this->session->userdata('preapprovalStatus');
//                  //echo$preapprovalStatus?'true':'false';exit; 
//                   $preapprovalStatus = true; 
//                if($this->session->userdata('preapprovalStatus'))
//                {

                    $this->session->unset_userdata('preapprovalStatus');

                $symbol = $this->mydate->moneySymbol($this->config_pay['currencycode']);

                $amount = 200;
                $maxNumber = 40;
                $maxAmount = $amount*12;

                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d',
                    strtotime('+1 year', strtotime($data['startDate'])));
                $data['amount'] = $symbol.number_format($amount, 2);
                $data['maxAmount'] = $symbol.number_format($maxAmount, 2);
                $data['maxNumber'] = $maxNumber;
                    
                $arrData = array(
                        'startDate' => $data['startDate'].' '.date('H:i:s'),
                        'endDate' => $data['endDate'].' '.date('H:i:s'),
                        'amount' => $amount,
                        'maxAmount' => $maxAmount,
                        'maxNumber' => $maxNumber,
                    );
                $data['hash'] = $this->cipher->encrypt(serialize($arrData));

                    /* Breadcrumb */
                    $this->breadcrumb->clear();
                $this->breadcrumb->add_crumb('Billing');
                $this->breadcrumb->add_crumb('PayPal Preapproval');
                $this->breadcrumb->change_link(
                    '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
                $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
                    /* End ... */
                    
                    $this->smarty->assign('siteurl', site_url());
                $this->smarty->view('billing/preapproval.tpl', $data);
//                }
            } elseif (isset($param) && $param == 'approve') {
                if (($this->input->get('hash') &&
                    $this->input->get('hash') != '') &&
                    ($this->input->get('hash') == $this->session->userdata('payenc'))) {
                    $this->load->model('paypal_model', 'model');
                    $prekey = $this->session->userdata('preapprovalKey');

                    $PayPalPreDetail = $this->preapprovalDetail($prekey);
                    if ($PayPalPreDetail['Ack'] == 'Success' &&
                        $PayPalPreDetail['Approved'] == 'true' &&
                        $PayPalPreDetail['SenderEmail'] != '' &&
                        $PayPalPreDetail['Status'] == 'ACTIVE') {
                        $logs = $this->cipher->encrypt(serialize($PayPalPreDetail));
                        $result = $this->model->setPreapprovalPayment($this->id_user, $prekey, $logs);

                        if ($result) {
                            $this->session->unset_userdata('preapprovalKey');
                            $this->session->unset_userdata('payenc');
                        }

                        if ($this->session->userdata('billID') &&
                            $this->session->userdata('billID')) {
                            redirect(site_url('service/payment'));
                            return;
                        } else {
                            $this->session->set_userdata('payStatus', 'complete');
                            redirect(site_url('billing/configuration'));
                            return;
                        }
                    } else {
                        redirect(site_url('billing/configuration'));
                        return;
                    }
                } else {
                    redirect(site_url());
                    return;
                }
            } elseif (isset($param) && $param == 'edit') {
                if ($this->input->post('profile')) {
                    $this->load->model('paypal_model', 'model');
                    $arrData = (array)$this->input->post('profile');
                    if ($arrData['action'] == 'yes') {
                        if (isset($arrData['preapproval']) &&
                            $arrData['preapproval'] == 'on') {
                            $status = 'active';
                        } else {
                            $status = 'inactive';
                        }
                        $result = $this->model->updatePreapprovalPayment($this->id_user, $status);
                        if ($result) {
                            redirect(site_url('billing/configuration'));
                        }
                    }
                }
            } else {
                if ($this->input->post('hash')) {
                    $enc = md5(time());
                    $this->session->set_userdata('payenc', $enc);
                    $this->load->model('paypal_model', 'model');
                    $this->load->library('PayPal/preapproval', $this->config_pay);
                    $arrData = unserialize($this->cipher->decrypt($this->input->post('hash')));
                                    
                    /*
                    * Setting Variable
                    */
                    $payerEmail = $this->model->getPayerEmail($this->id_user);
                    if (is_array($payerEmail)) {
                        $payerEmail = $payerEmail['value'];
                    }
                                    
                                    
                    $startDate = $this->model->getPaypalTimezone(date('Y-m-d\TH:i:s'));
                    $endDate = date('Y-m-d', strtotime($arrData['endDate'])).'T'.
                        date('H:i:s', strtotime($arrData['endDate']));
                    $returnURL = site_url().'billing/preapproval/approve/?hash='.
                        urlencode($enc);
                    $cancelURL = site_url().'billing/configuration';

                    $amount = number_format($arrData['amount'], 2);
                    $maxRound = $arrData['maxNumber'];
                    $totalAmount = sprintf('%.2f', $amount*$maxRound);
                    /*
                    * End Setting
                    */

                    $arrPreapprovalFields = array(
                        'ReturnURL' => $returnURL,
                        'CancelURL' => $cancelURL,
                        'CurrencyCode' => $this->config_pay['currencycode'],
                        'DateOfMonth' => '0',
                        'MaxAmountPerPayment' => $amount,
                        'MaxNumberOfPayments' => $maxRound,
                        'MaxTotalAmountOfAllPayments' => $totalAmount,
                        'Memo' => $this->lang->line('pagree_title_detail'),
                        'StartingDate' => $startDate,
                        'EndingDate' => $endDate,
                        //'SenderEmail'=>$payerEmail,
                    );
                    $this->preapproval->setPreapprovalFields($arrPreapprovalFields);
                    
                    $arrClientDetailsFields = array(
                        'CustomerID'=>$this->id_user,
                        'CustomerType'=>'',
                        'GeoLocation'=>'',
                        'Model'=>'',
                        'PartnerName'=>'',
                        
                    );
                    $this->preapproval->setClientDetailsFields($arrClientDetailsFields);
                    $PayPalResult = $this->preapproval->doTransaction();
                    
                    if ($PayPalResult['Ack'] == 'Success') {
                        $this->session->set_userdata('preapprovalKey',
                            $PayPalResult['PreapprovalKey']);
                        redirect($PayPalResult['RedirectURL']);
                        return;
                    } else {
                        redirect($cancelURL);
                        return;
                    }
                }
            }
        //}
    }
    
    public function address()
    {
        if ($this->authentication->logged_in() && isset($this->session->userdata['id'])) {
            $this->load->model('paypal_model', 'model');
            $json = $this->model->getCustomer($this->session->userdata['id']);
            echo json_encode($json);
        }
    }
    
    public function download()
    {
        $data = array();
//        $user_data = $data['userData'];
//        
//        if(!empty($user_data))
//        {
            $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $this->load->library('Cipher', array(
            'key'=>$this->config->item('encryption_key')));
        $data['query'] = $this->model->billingLists($this->id_user);
                                    
        if (!empty($data['query'])) {
            foreach ($data['query'] as $key => $val) {
                $data['query'][$key]['detail'] =
                    $this->model->getBillingDetailLists($val['id_billing']);
                if (!empty($data['query'][$key]['detail'])) {
                    $tmp = array();
                    foreach ($data['query'][$key]['detail'] as $field) {
                        $field['package_detail'] = unserialize(
                            $this->cipher->decrypt($field['package_detail']));
                        $tmp[] = $field;
                    }
                    $data['query'][$key]['detail'] = $tmp;
                }
                $data['query'][$key]['sign'] =
                    $this->mydate->moneySymbol($val['currency_billing']);
            }
        }

            /* Breadcrumb */
            $this->breadcrumb->clear();
        $this->breadcrumb->add_crumb('Billing');
        $this->breadcrumb->add_crumb('Download');
        $this->breadcrumb->change_link(
            '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
        $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /* End ... */
            
            //$this->mydate->debugging($data['query']);

            $this->smarty->assign('siteurl', site_url());
        $this->smarty->view('billing/download.tpl', $data);
//        }
    }
    
    public function printBilling()
    {
        $data = $this->checkAuthen();
        //$data = array();
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $this->load->library('Cipher', array(
            'key'=>$this->config->item('encryption_key')));
        
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
            
            try {
                $fileDir = str_replace('\\', '/', FCPATH);
                $this->load->library('MPDF/mpdf');
                $this->mpdf->mPDF('', 'A4', 10, 'garuda', 10, 10, 15, 15, 9, 9, 'P');
                $this->mpdf->SetDisplayMode('fullpage');
                
                $query = $this->model->getBilling($id);
                if ($query) {
                    if ($query['id_users'] == $this->id_user) {
                        $queryDetail = $this->model->getBillingDetailLists($id);
                    
                        if ($queryDetail) {
                            $billDetail = '';
                            $totalAmount = 0;
                            $defaultSign = '';
                            $count = 0;
                            
                            foreach ($queryDetail as $key => $val) {
                                $queryDetail[$key]['package_detail']
                                    = unserialize($this->cipher->decrypt($val['package_detail']));
                                switch ($val['option_package']) {
                                    case 'offer':
                                        $cQuery = $this->model->getOfferCurrency($val['id_package']);
                                        break;
                                }
                                $queryDetail[$key]['currency'] = $cQuery[0]['currency'];
                                
                                $billDetail .= '<div style="margin: 0px 0px 5px 0px;">
                                    <div style="float: left;text-align: left;width: 30px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                        '.($key+1).'
                                    </div>
                                    <div style="float: left;text-align: left;width: 440px;padding: 5px 5px 0px 5px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                        <div>'.$queryDetail[$key]['package_detail']['desc'].'</div>
                                        <div>(Period: '.date('j F Y', strtotime($queryDetail[$key]['start_date_bill_detail'])).' - '.date('j F Y', strtotime($queryDetail[$key]['end_date_bill_detail'])).')</div>
                                    </div>
                                    <div style="float: left;text-align: left;width: 130px;padding: 0px 5px;line-height: 30px;text-transform: capitalize;font-family: Verdana, sans-serif;background-color: #FFF;">
                                        '.$queryDetail[$key]['option_package'].'
                                    </div>
                                    <div style="float: left;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                        '.$this->mydate->moneySymbol($queryDetail[$key]['currency']).$queryDetail[$key]['package_detail']['amt'].'
                                    </div>
                                </div>';
                                if ($key == 0) {
                                    $defaultSign = $this->mydate->moneySymbol($queryDetail[$key]['currency']);
                                }
                                $totalAmount += $queryDetail[$key]['package_detail']['amt'];
                                $count = $key;
                            }
                            
                            if (!empty($query['discount_cmd_billing'])) {
                                $count += 2;

                                $arrDiscountLog = unserialize($query['discount_log_billing']);
                                $arrDiscountCmd = unserialize($query['discount_cmd_billing']);

                                foreach ($arrDiscountCmd as $key => $val) {
                                    $dcText = '';
                                    switch ($val['param']) {
                                        case 'referrals':
                                            $dcText = $this->lang->line('Referrals Discount');
                                            break;
                                        case 'discount_date':
                                            $dcText = $this->lang->line('Until next pay day discount  ');
                                            break;
                                        case 'gift':
                                            $giftQuery = $this->model->getGiftCodeDetailPrint($arrDiscountLog['gift']);

                                            if ($giftQuery) {
                                                $dcText = $this->lang->line($giftQuery[0]['name_giftopt']);
                                            }
                                            break;
                                        case 'total':
                                            $totalAmount = $val['total_amt'];
                                            break;
                                    }

                                    if ($dcText != '') {
                                        $billDetail .= '<div style="margin: 0px 0px 5px 0px;">
                                            <div style="float: left;text-align: left;width: 30px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                                '.$count.'
                                            </div>
                                            <div style="float: left;text-align: left;width: 440px;padding: 5px 5px 0px 5px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                                <div>'.$dcText.'</div>
                                            </div>
                                            <div style="float: left;text-align: left;width: 130px;padding: 0px 5px;line-height: 30px;text-transform: capitalize;font-family: Verdana, sans-serif;background-color: #FFF;">
                                                &nbsp;
                                            </div>
                                            <div style="float: left;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                                -'.$this->mydate->moneySymbol($this->config_pay['currencycode']).$val['dc_amt'].'
                                            </div>
                                        </div>';
                                    }

                                    $count += 1;
                                }
                            }
                        }
                        
                        /*
                        * Billing Addr
                        */
                        $billAddr = '<div>'.$query['company_billing'].'</div>';
                        $billAddr .= '<div>'.$query['first_name_billing'].' '.$query['last_name_billing'].'</div>';
                        $billAddr .= '<div>'.$query['address1_billing'];
                        if (!empty($query['address2_billing'])) {
                            $billAddr .= ', '.$query['address2_billing'];
                        }
                        $billAddr .= '</div>';
                        $billAddr .= '<div>'.$query['stateregion_billing'].', '.$query['city_billing'].'</div>';
                        $billAddr .= '<div>'.$query['country_billing'].', '.$query['zipcode_billing'].'</div>';
                        /*
                        * End Billing Addr
                        */
                        
                        $content = file_get_contents($fileDir.'assets/template/billing_template.tpl');

                        $content = str_replace("{#billID}", $query['id_billing'], $content);
                        $content = str_replace("{#billDate}", date('j F Y', strtotime($query['created_date_billing'])), $content);
                        $content = str_replace("{#billAddress}", $billAddr, $content);
                        $content = str_replace("{#billDetail}", $billDetail, $content);
                        $content = str_replace("{#totalAmount}", $defaultSign.number_format($totalAmount, 2), $content);
                        
                        $this->mpdf->AddPage();
                        $this->mpdf->WriteHTML($content);
                        $this->mpdf->Output();
                    }
                }
            } catch (Exception $ex) {
            }
        }
    }
    
    /*private function sendBillingMail($arrData)
    {
        require_once APPPATH.'libraries/Swift/swift_required.php';
        $this->load->helper('file');

        try
        {
            $wrapper = read_file(str_replace('\\', '/', FCPATH).'assets/template/email_template.html');
            $wrapper = str_replace('<!--rep_logo-->','<img src="'.site_url().'assets/images/company_logo.jpg">',$wrapper);
            $wrapper = str_replace('<!--rep_date-->',date('r',strtotime($arrData['created_date_billing'])),$wrapper);
            $wrapper = str_replace('<!--rep_cus-->','<span style="color: #006CB4;">'.$arrData['first_name_billing'].' '.$arrData['last_name_billing'].'</span><p>&nbsp;</p>',$wrapper);
            $wrapper = str_replace('<!--rep_text-->','You have paid a total of <span style="color: #006CB4;">'.$arrData['sign'].$arrData['amt_billing'].'</span> to Feed.biz for monthly service.',$wrapper);
            $wrapper = str_replace('<!--rep_billing_id-->','<span style="font-weight: bold;color: #006CB4;">'.$arrData['id_billing'].'</span>',$wrapper);
            $wrapper = str_replace('<!--rep_payment_method-->','<span style="text-transform: capitalize;color: #006CB4;">'.$arrData['payment_method_billing'].'</span>',$wrapper);
            
            $strRow = '';
            foreach($arrData['details'] as $val)
            {
                $strRow .= '<tr>
                    <td style = "font-size: 0.9em;padding: 10px 5px;border-bottom: #CCC 1px solid;"><span style="color: #006CB4;">'.$val['id_package'].'</span></td>
                    <td style = "font-size: 0.9em;padding: 10px 5px;border-bottom: #CCC 1px solid;"><span style="color: #006CB4;">'.$val['package_detail']['name_package'].'</span></td>
                    <td style = "font-size: 0.9em;padding: 10px 5px;border-bottom: #CCC 1px solid;"><span style="color: #006CB4;">'.date('j F Y',strtotime($val['start_date_bill_detail'])).'</span></td>
                    <td style = "font-size: 0.9em;padding: 10px 5px;border-bottom: #CCC 1px solid;"><span style="color: #006CB4;">'.date('j F Y',strtotime($val['end_date_bill_detail'])).'</span></td>
                    <td style = "font-size: 0.9em;padding: 10px 5px;border-bottom: #CCC 1px solid;text-align: right;"><span style="color: #006CB4;">'.$arrData['sign'].$val['amount_bill_detail'].'</span></td>
                </tr >';
            }
            $wrapper = str_replace('<!--rep_table_detail-->',$strRow,$wrapper);
            
            $wrapper = str_replace('<!--rep_table_totalamount-->','<span style="color: #006CB4;">'.$arrData['sign'].$arrData['amt_billing'].'</span>',$wrapper);           
            $wrapper = str_replace('<!--rep_pdf_url-->','<a href="'.site_url().'billing/printBilling/?id='.$arrData['id_billing'].'" target="_blank">'.site_url().'billing/printBilling/?id='.$arrData['id_billing'].'</a>',$wrapper);
            $wrapper = str_replace('<!--rep_sign-->','<span style="color: #006CB4;">Feed.biz</span>',$wrapper);
            
            // Create the Transport
            $transport = Swift_SmtpTransport::newInstance();

            // Create the Mailer using your created Transport
            $mailer = Swift_Mailer::newInstance($transport);

            // Create a message
            $message = Swift_Message::newInstance()
                    ->setSubject($arrData['title'])
                    ->setFrom(array('support@feed.biz' => 'Feed.biz Support'))
                    ->setTo(array($arrData['email']))
                    ->setBody($wrapper,'text/html')
            ;

            // Send the message
            return $mailer->send($message);
        } catch (Exception $ex) {
            return null;
        }
    }*/
    
    private function preapprovalDetail($prekey)
    {
        $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
        $this->preapprovaldetails->setPreapprovalKey($prekey);
        $PayPalResult = $this->preapprovaldetails->doTransaction();
        return $PayPalResult;
    }
    
                                        
    private function checkAuthen()
    {
        $data = array();
        if ($this->authentication->logged_in() && isset($this->session->userdata['id'])) {
            $data['userData'] = (array)$this->authentication->user($this->session->userdata['id'])->row();
            return $data;
        } else {
            $_SERVER['QUERY_STRING'] = $_SERVER['QUERY_STRING']==''?'':'?'.$_SERVER['QUERY_STRING'];
            $uri = current_url().$_SERVER['QUERY_STRING'];
            $this->session->set_userdata(array('error'=> 'session_error', 'original_redirect'=>$uri)); 
            redirect('users/login', 'refresh');
        }
        
        return $data;
    }
}
