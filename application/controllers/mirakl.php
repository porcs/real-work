<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.parameter.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.create.database.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.order.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.database.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.config.class.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/functions/mirakl.account.php';


//require_once dirname(__FILE__).'/../libraries/Mirakl/classes/interface.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/functions/mirakl.products.update.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/functions/mirakl.products.create.php';
require_once dirname(__FILE__).'/../libraries/Mirakl/functions/mirakl.products.delete.php';

class Mirakl extends CI_Controller {

    public $marketplace; // modifier should public only!
    public $language;
    private $ext;
    private $user_name;
    private $id_user;
    private $id_shop;
    private $shop_default;
    private $id_mode;
    private $id_lang;
    private $iso_code;
    private $mirakl_database;
    private $mirakl_order;
    private $id_marketplace;
    private $tag_general_uc;
    private $tag_mirakl_uc;
    private $mirakl_scheme;
    private $mirakl_account;
    private $lang_default;

    public function __construct() {
        parent::__construct();

        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');

        if ((!isset($this->user_name) || empty($this->user_name)) && (!isset($this->id_user) || empty($this->id_user))) {
            redirect('users/login');
        }

        $this->id_shop = $this->session->userdata('id_shop');
        $this->shop_default = $this->session->userdata('shop_default'); // shopname

        if (!isset($this->id_shop) || empty($this->id_shop) || !($this->shop_default) || empty($this->shop_default)) {
            die('Shop is not definition.');
            redirect('dashboard');
        }

        //load library
        $this->load->library('FeedBiz', array($this->user_name));

        // id mode
        $this->id_mode = 1; //for all mode
        //
        //set init
        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;
        $this->tag_general_uc = MiraklParameter::TAG_GENERAL_UC;
        $this->tag_mirakl_uc = MiraklParameter::TAG_MIRAKL_UC;

        //set language
        $lang = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $iso_code = mb_substr($lang, 0, 2);
        $language = $this->feedbiz->checkLanguage($iso_code, $this->id_shop);
        $this->language = $lang;
        $this->id_lang = $language['id_lang'];
        $this->iso_code = $language['iso_code'];

        $lang_default = $this->feedbiz->getLanguageDefault($this->id_shop);
        $this->lang_default = reset($lang_default);

        $this->lang->load(MiraklParameter::TAG_MIRAKL_LW, $this->language);
        $this->smarty->assign("lang", $this->language);
        $this->smarty->assign("label", MiraklParameter::TAG_MIRAKL_LW); // langage file (lower only).
        // Mirakl model
        $this->load->model('marketplace_model');
        $this->load->model("mirakl_model");
        $this->mirakl_database = new MiraklDatabase($this->user_name); //Database
        $this->mirakl_order = new MiraklOrder($this->user_name); //Database Order
        $this->mirakl_scheme = new MiraklScheme();
        $this->mirakl_account = new MiraklAccount($this->language);

        $this->smarty->assign("function", $this->lang->line($this->router->fetch_method())); // IT IS A TEXT TITLE. 
        $this->checkTableExists();
        $this->checkFileExists();
    }

    public function test() {
//        $s = $this->mirakl_database->getSettingCurrencyIdByIsoCode('THB', 1);
//        echo '<pre>';
//        print_r($s);
//        exit;
    }

    private function checkTableExists() {
        $mirakl_create_tb = new MiraklCreateDatabase($this->mirakl_database->getDatabase());
        $mirakl_create_tb->create_table();
        $mirakl_create_tb->create_table_order();
    }

    private function checkFileExists() {

        $file_mirakl = USERDATA_PATH.$this->user_name.'/mirakl/';

        if (!file_exists($file_mirakl)) {
            mkdir($file_mirakl, 0777);
            chmod($file_mirakl, 0777);
        }

        $file_logs = USERDATA_PATH.$this->user_name.'/mirakl/logs/';

        if (!file_exists($file_logs)) {
            mkdir($file_logs, 0777);
            chmod($file_logs, 0777);
        }

        $file_download = USERDATA_PATH.$this->user_name.'/mirakl/download/';

        if (!file_exists($file_download)) {
            mkdir($file_download, 0777);
            chmod($file_download, 0777);
        }

        $file_export = USERDATA_PATH.$this->user_name.'/mirakl/export/';

        if (!file_exists($file_export)) {
            mkdir($file_export, 0777);
            chmod($file_export, 0777);
        }

        $file_products = USERDATA_PATH.$this->user_name.'/mirakl/download/products/';

        if (!file_exists($file_products)) {
            mkdir($file_products, 0777);
            chmod($file_products, 0777);
        }

        $file_offers = USERDATA_PATH.$this->user_name.'/mirakl/download/offers/';

        if (!file_exists($file_offers)) {
            mkdir($file_offers, 0777);
            chmod($file_offers, 0777);
        }

        $file_create = USERDATA_PATH.$this->user_name.'/mirakl/export/create/';

        if (!file_exists($file_create)) {
            mkdir($file_create, 0777);
            chmod($file_create, 0777);
        }

        $file_update = USERDATA_PATH.$this->user_name.'/mirakl/export/update/';

        if (!file_exists($file_update)) {
            mkdir($file_update, 0777);
            chmod($file_update, 0777);
        }

        $file_delete = USERDATA_PATH.$this->user_name.'/mirakl/export/delete/';

        if (!file_exists($file_delete)) {
            mkdir($file_delete, 0777);
            chmod($file_delete, 0777);
        }
    }

    private function assign() {
        $assigns = func_get_args();
        foreach ($assigns as $assign) :
            foreach ($assign as $key => $value) :
                $this->smarty->assign($key, $value);
            endforeach;
        endforeach;
    }

    private function setSessionMessage() {
        if ($this->session->userdata('error')) {
            $this->smarty->assign("error", $this->lang->line($this->session->userdata('error')));
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $this->smarty->assign("message", $this->lang->line($this->session->userdata('message')));
            $this->session->unset_userdata('message');
        }
    }

    private function displaySessionMessage($status = true) {
        if ($status) {
            $this->session->set_userdata('message', 'save_successful');
        } else {
            $this->session->set_userdata('error', 'save_unsuccessful');
        }
    }

    private function isCountry($sub_marketplace = '', $id_country = '') {
        if (!isset($sub_marketplace) || empty($sub_marketplace) || !isset($id_country) || empty($id_country)) {
            redirect('/marketplace/configuration');
        }
    }

    private function isActiveApi($sub_marketplace, $id_country) { // if active return true else false
        return $this->mirakl_model->isActiveApi($this->id_user, $sub_marketplace, $id_country, $this->id_shop);
    }

    private function isActiveFile() {
        $mirakl_params = MiraklConfig::marketplaceParams($this->marketplace);
        return is_array($mirakl_params); // true or false
    }

    private function assignCountryData($sub_marketplace, $id_country) {
        $this->smarty->assign("sub_marketplace", $sub_marketplace);
        $this->smarty->assign("id_country", $id_country);
        $this->smarty->assign("id_shop", $this->id_shop);
        $this->smarty->assign("id_user", $this->id_user);
        $this->smarty->assign("id_marketplace", $this->id_marketplace);

        if (isset($this->session->userdata['menu'])) {
            $marketplaces = $this->session->userdata['menu'];
        }

        $has_page = true;
        $has_page = isset($marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]) ? true : false;

        if (!$has_page) { // get url is wrong.
            show_404();
        }

        if (isset($marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['ext'])) {
            $this->ext = $marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['ext'];
            $this->smarty->assign("ext", $this->ext);

            // display country in title right img country.
            $country = $marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['country'];
            $this->smarty->assign("country", $country);

            // use comment for marketplace name
            if (isset($marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['name_marketplace'])) {
                $this->marketplace = $marketplaces[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['name_marketplace'];
                $this->smarty->assign("marketplace", $this->marketplace);
            }
        }
    }

    public function parameters($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $api_key = $this->mirakl_scheme->getApiByUserId($this->id_user, $sub_marketplace, $id_country, $this->id_shop);

        $data = array();
        $data['api_key'] = isset($api_key['api_key']) ? $api_key['api_key'] : '';
        $data['auto_accept'] = isset($api_key['auto_accept']) ? $api_key['auto_accept'] : '';
        $data['logo'] = isset($api_key['logo']) ? $api_key['logo'] : '';
        $data['active'] = isset($api_key['active']) ? $api_key['active'] : '';

        $assign = array(
            'action' => 'save_parameters',
            'save_page' => 'parameters',
            'save_continue' => 'models',
            'current_page' => 'config_parameter',
        );

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function verify($sub_marketplace = '', $id_country = '', $marketplace = '') { // check api_key in page parameters
        if (empty($sub_marketplace) || empty($id_country) || empty($marketplace)) {
            echo json_encode(array('code' => '0', 'msg' => 'No data to vertify.'));
            return false;
        }

        if ($this->input->post()) {

            $values = $this->input->post('data');
            $result = $this->mirakl_account->verify($marketplace, $values['api_key']);
            //print_r($result); exit;
            if ($result['code'] == 1) {
                $data = array();
                $data['sub_marketplace'] = $sub_marketplace;
                $data['id_country'] = $id_country;
                $data['api_key'] = $values['api_key'];
                $data['auto_accept'] = $values['auto_accept'];
                $data['logo'] = isset($result['logo']) && !empty($result['logo']) ? $result['logo'] : '';
                $data['active'] = $values['active'];
                $this->save_parameters($data);
            }

            echo json_encode($result);
        }
    }

    public function models($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $this->smarty->assign("add_data", $this->lang->line('Add a model'));

        $data = array();
        $data['config_hierarchy'] = $this->mirakl_database->getMiraklConfigHierarchy($sub_marketplace, true);
        $data['shop_attribute'] = $this->mirakl_database->getShopAttribute($this->id_shop, $this->id_lang);
        unset($data['shop_attribute']['S']);
        $data['mapping_model'] = $this->mirakl_database->getMiraklMappingModel($sub_marketplace, $id_country, $this->id_shop, $this->id_lang);

        $assign = array(
            'action' => 'save_models',
            'save_page' => 'models',
            'save_continue' => 'profiles',
            'current_page' => 'config_models',
        );
        //echo '<pre>'; print_r($data['shop_attribute']); exit;
        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function profiles($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $this->config->load('regex');
        $currencys = $this->config->item('currency_code');

        //Profile
        $profiles = $this->mirakl_database->getMiraklProfile($sub_marketplace, $id_country, $this->id_shop);

        $data = array();
        if (empty($profiles)) {
            $this->smarty->assign("no_data", _get_message_code('23-00002', 'inner'));
        } else {
            $data['profile'] = $profiles;
        }

        $this->smarty->assign("add_data", $this->lang->line('Add a profile to the list'));

        if (isset($profiles) && !empty($profiles)) {
            $this->smarty->assign("size_data", sizeof($profiles)); // to use count profile
        }

        $additional_field = $this->mirakl_database->getMiraklAdditionalField($sub_marketplace);
        $models = $this->mirakl_database->getMiraklMappingModel($sub_marketplace, $id_country, $this->id_shop, $this->id_lang);
        $data['models'] = $models;

        if (isset($additional_field) && !empty($additional_field)) {
            $this->smarty->assign("additionnals", $additional_field);

            //Attribute
//            $attributes = array();
//            $attrs = $this->feedbiz->getAttributes($this->user_name, $this->language, $this->id_shop);
//            if (isset($attrs) && !empty($attrs)) {
//                foreach ($attrs as $key => $attr) {
//
//                    if (!isset($attr['name'][$lang_iso])) {
//                        $lang_iso = $lang_default['iso_code']; // default lang
//                    }
//
//                    if (!isset($attr['name'][$lang_iso])) {
//                        continue; // if is not exists name at lang
//                    }
//
//                    $attributes[$key]['name'] = $attr['name'][$lang_iso];
//
//                    foreach ($attr['value'] as $key_value => $value) {
//                        $attributes[$key]['value'][$key_value] = $value['name'];
//                    }
//                }
//            }
//            $this->smarty->assign("attributes", $attributes);
            //Feature
            $features = $this->feedbiz->getFeatures($this->id_shop, $this->id_lang);
            if (isset($features) && !empty($features)) {
                $this->smarty->assign("features", $features);
            }

            //shop field
            $shop_fields = array();
            foreach (MiraklParameter::$shop_fields as $id_shop_field) {
                switch ($id_shop_field) {
                    case MiraklParameter::SUPPLIER_REFERENCE:
                        $name = 'Supplier Reference';
                        break;
                    case MiraklParameter::REFERENCE:
                        $name = 'Reference';
                        break;
                    case MiraklParameter::CATEGORY:
                        $name = 'Category';
                        break;
                    case MiraklParameter::MANUFACTURER:
                        $name = 'Manufacturer';
                        break;
                    case MiraklParameter::META_TITLE:
                        $name = 'Meta Title';
                        break;
                    case MiraklParameter::META_DESCRIPTION:
                        $name = 'Meta Description';
                        break;
                    case MiraklParameter::UNITY:
                        $name = 'Unit';
                        break;
                    case MiraklParameter::WEIGHT:
                        $name = 'Weight';
                        break;
                    default:
                        $name = null;
                        break;
                }
                if ($name == null) {
                    continue;
                }
                $shop_fields[$id_shop_field] = $name;
            }

            if (!empty($shop_fields)) {
                $this->smarty->assign("prestashop_fields", $shop_fields);
            }
        }

        $assign = array(
            'action' => 'save_profiles',
            'save_page' => 'profiles',
            'save_continue' => 'category',
            'current_page' => 'config_profile',
        );
        //echo '<pre>'; print_r($data); exit;
        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function category($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $data['mode'] = $this->id_mode;
        $data['prifile_mapping'] = true;
        $data['feed_mode'] = $this->id_mode;

        $profile = array();
        $profile = $this->mirakl_database->getMiraklProfile($sub_marketplace, $id_country, $this->id_shop);
        $data['profile'] = $profile;

        $assign = array(
            'action' => 'save_category',
            'save_page' => 'category',
            'save_continue' => 'mappings',
            'current_page' => 'config_category',
        );

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function mappings($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $mirakl_params['additionnals'] = '';
        $data['config_attribute'] = $this->mirakl_database->getMiraklAttributeNoHierarchy($sub_marketplace);
        $data['shop_attribute'] = $this->mirakl_database->getShopAttribute($this->id_shop, $this->id_lang);
        $data['mirakl_attribute_with_list'] = $this->mirakl_database->getMiraklAttributeNoHierarchyHaveValueList($sub_marketplace, $this->id_shop, $this->id_lang);

        $data['mirakl_mapping_model_attribute'] = $this->mirakl_database->getMiraklMappingModelAttribute($sub_marketplace, $id_country, $this->id_shop, $this->id_lang);



        $assign = array(
            'action' => 'save_mapping_value',
            'save_page' => 'mappings',
            'save_continue' => 'conditions',
            'current_page' => 'config_mapping',
        );
//       echo '<pre>'; print_r($data); exit;
        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function getMapping() {
        if ($this->input->post()) {
            $value = $this->input->post();
            //echo '<pre>'; print_r($value); exit;
            $data = array();
            $data['value_list'] = $this->mirakl_database->getMiraklMapping($value['data']);
            $data['id_config_attribute'] = $value['data']['id_config_attribute'];
            $data['selected'] = $value['data']['selected'];
            //echo '<pre>'; print_r($data); exit;
            $mapping = $this->smarty->fetch('mirakl/template_mapping.tpl', $data);
            //echo '<pre>'; print_r($mapping); exit;
            echo $mapping;
        } else {
            //echo json_encode($this->mirakl_attribute_with_list);
            echo 'F';
        }
    }

    public function conditions($sub_marketplace, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);
        $data = array();
        $marketplace_condition = $this->marketplace_model->getMarketplaceConditionByMirakl($this->id_marketplace, $sub_marketplace);
        $data['marketplace_condition'] = $marketplace_condition;

        $shop_condition = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop, $this->id_marketplace);
        $data['shop_condition'] = $shop_condition;

        $condition = $this->feedbiz->getConditions($this->user_name, $this->id_shop);
        $data['condition'] = $condition;

        $assign = array(
            'action' => 'save_conditions',
            'save_page' => 'conditions',
            'save_continue' => 'carriers',
            'current_page' => 'config_condition',
        );

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function carriers($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->setSessionMessage();
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $data['carriers_marketplace'] = $this->mirakl_database->getMiraklConfigCarrier($sub_marketplace, $id_country, $this->id_shop);
        $data['carriers_shop'] = $this->mirakl_database->getProductCarrier($this->id_shop);

        $assign = array(
            'action' => 'save_carrier',
            'save_page_static' => 'carriers',
            'current_page' => 'config_carrier',
        );
        $this->assign($assign);
        //echo '<pre>'; print_r($data); exit;
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function save_parameters($data = array()) {

        if ($this->input->post() || !empty($data)) {

            if (!empty($data)) { // from vertify.
                $sub_marketplace = $data['sub_marketplace'];
                $id_country = $data['id_country'];
                $logo = $data['logo'];
                $api_key = !empty($data['api_key']) ? $data['api_key'] : '';
                $active = isset($data['active']) && $data['active'] == 1 ? 1 : 0;
                $save = 'params';
                $auto_accept = isset($data['auto_accept']) && $data['auto_accept'] == 1 ? 1 : 0;
            } else {
                $values = $this->input->post();
                //echo '<pre>'; print_r($values); exit;
                $sub_marketplace = $values['sub_marketplace'];
                $id_country = $values['id_country'];
                $api_key = !empty($values['api_key']) ? $values['api_key'] : '';
                $logo = !empty($values['logo']) ? $values['logo'] : '';
                $active = isset($values['active']) ? $values['active'] : 0;
                $save = isset($values['save']) ? $values['save'] : 'save';
                $auto_accept = isset($values['auto_accept']) ? 1 : 0;
            }

            if (isset($this->session->userdata['menu'])) {
                $marketplace = $this->session->userdata['menu'];
            }

            if (isset($marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country])) {
                $data['id_customer'] = $this->id_user;
                $data['sub_marketplace'] = $sub_marketplace;
                $data['id_country'] = $id_country;
                $data['id_shop'] = $this->id_shop;
                $data['user_name'] = $this->user_name;
                $data['ext'] = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['ext'];
                $data['countries'] = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['country'];
                $data['currency'] = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['currency'];
                $data['id_region'] = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['id_region'];
                $data['operator'] = '';
                $data['api_key'] = $api_key;
                $data['auto_accept'] = $auto_accept;
                $data['active'] = $active;
                $data['date_add'] = date("Y-m-d H:i:s");
            }
            $data['logo'] = $logo;
            $data['id_lang'] = $this->id_lang;
            $data['iso_code'] = $this->mirakl_scheme->getLanguageCode($id_country);

            $flag = $this->mirakl_scheme->saveMiraklConfiguration($data);

            if (!$flag && $save != 'params') {
                $this->displaySessionMessage(false);
                redirect('general/parameters/'.$sub_marketplace.'/'.$id_country);
            } else if ($save == 'params') {
                return; // save in vertify.
            } else {
                $this->displaySessionMessage(true);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            }
        }
    }

    public function save_models() {
        if ($this->input->post()) {
            $values = $this->input->post();
            //echo '<pre>'; print_r($values); exit;
            $save = $values['save'];
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];

            $save_model = $this->mirakl_database->saveModel($values);
            //echo '<pre>'; print_r($save_model); exit;

            if ($save_model) {
                $this->displaySessionMessage(true);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            } else {
                $this->displaySessionMessage(false);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            }
        }
    }

    public function save_profiles() {

        if ($this->input->post()) {
            $values = $this->input->post();
            //echo '<pre>'; print_r($values); exit;
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];
            $marketplace = $values['marketplace'];
            $save = $values['save'];
            $save_profile = $this->mirakl_database->saveProfile($values);

            if ($save_profile) {
                $this->displaySessionMessage(true);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            } else {
                $this->displaySessionMessage(false);
                redirect('general/profiles/'.$marketplace.'/'.$id_country);
            }
        }
    }

    public function save_category($popup = null) {
        if ($this->input->post()) {
            $values = $this->input->post();
            //echo '<pre>'; print_r($values); exit;
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];
            $marketplace = $values['marketplace'];
            $save = $values['save'];
            $save_category = $this->mirakl_database->saveMiraklCategorySelect($values);

            redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
        }
    }

    public function save_mapping_value() {
        if ($this->input->post()) {
            $values = $this->input->post();
            //echo '<pre>'; print_r($values); exit;
            $save = $values['save'];
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];
            $id_shop = $values['id_shop'];

            if (!isset($values['field']) || empty($values['field'])) {
                $this->displaySessionMessage(false);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            }

            $save_field = $this->mirakl_database->saveMappingField($sub_marketplace, $id_shop, $values['field']);

            if (isset($values['attribute_with_list']) && !empty($values['attribute_with_list'])) {
                $this->mirakl_database->saveMappingAttributeWithList($sub_marketplace, $id_shop, $values['attribute_with_list']);
            }

            if (isset($values['model_attribute_list']) && !empty($values['model_attribute_list'])) {
                $this->mirakl_database->saveMappingAttributeList($sub_marketplace, $id_country, $id_shop, $values['model_attribute_list']);
            }

            if ($save_field) {
                $this->displaySessionMessage(true);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            } else {
                $this->displaySessionMessage(false);
                redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
            }
        }
    }

    public function save_conditions() {
        if ($this->input->post()) {
            $values = $this->input->post();
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];
            $save = $values['save'];

            if (isset($values['condition']) && !empty($values['condition'])) {
                foreach ($values['condition'] as $condition_id => $value) {
                    $condition = $this->marketplace_model->getMarketplaceConditionByID($condition_id, $this->id_marketplace);
                    //echo '<pre>'; print_r($cond); exit;
                    if (!empty($value)) {                       // $user,         $id_shop, $id_marketplace,          $market_condition,      $shop_condition
                        $out = $this->feedbiz->setConditionMapping($this->user_name, $this->id_shop, $condition['id_marketplace'], $condition['condition_value'], $value);
                        //echo 'Y';
                    } else {
                        $out = $this->feedbiz->removeConditionMapping($this->user_name, $this->id_shop, $condition['id_marketplace'], $condition['condition_value']);
                        //echo 'x';
                    }
                }
            }

            $this->session->set_userdata('message', 'save_successful');
            $this->smarty->assign('message', $this->lang->line($this->session->userdata('message')));
            redirect('general/'.$save.'/'.$sub_marketplace.'/'.$id_country);
        }
    }

    public function save_carrier() {
        if ($this->input->post()) {
            $values = $this->input->post();
            //echo '<pre>'; print_r($values); exit;
            $save = $values['save'];
            $sub_marketplace = $values['sub_marketplace'];
            $id_country = $values['id_country'];

            $save_carrier = $this->mirakl_database->saveMiraklCarrier($values);

            if ($save_carrier) {
                $this->displaySessionMessage(true);
            } else {
                $this->displaySessionMessage(false);
            }

            redirect('general/carriers/'.$sub_marketplace.'/'.$id_country);
        }
    }

    public function getSubHierarchy() {
        if ($this->input->post()) {
            $data = array();
            $result = array();
            $value = $this->input->post();

            /* hierachy */
            $data['sub_hierarchy'] = $this->mirakl_database->getMiraklSubHierarchy($value['id_config_hierarchy'], $value['sub_marketplace']);
            $result['result_hierarchy'] = $this->smarty->fetch('mirakl/template_sub_hierarchy.tpl', $data);

            /* attribute */
            $data = array();
            $data['attribute_with_hierarchy'] = $this->mirakl_database->getAttributeWithHierarchy($value['id_config_hierarchy_array'], $value['sub_marketplace']);
            $data['shop_attribute'] = $this->mirakl_database->getShopAttribute($this->id_shop, $this->id_lang);

            $result['result_attribute'] = $this->smarty->fetch('mirakl/template_sub_hierarchy.tpl', $data);
            echo json_encode($result);
        }
    }

    public function getSelectedAllCategory($sub_marketplace, $id_country, $id_shop) {
        $html = '';
        $this->assignCountryData($sub_marketplace, $id_country);
        $lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
        $roots = $this->feedbiz->getAllRootCategories($this->user_name, $this->id_shop, $lang);

        $arr_selected_categories = array();
        if (!empty($roots)) {
            foreach ($roots as $root) {
                $arr_selected_categories_res = $this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $this->id_mode, $root['id'], false, $lang['id_lang']);
                $name = isset($root['name'][$this->iso_code]) ? $root['name'][$this->iso_code] : (isset($root['name'][$lang['iso_code']]) ? $root['name'][$lang['iso_code']] : '');
                if (empty($name)) {
                    continue;
                }
                $arr_selected_categories[] = array('id_category' => $root['id'], 'name' => $name, 'parent' => 0, 'type' => 'folder', 'child' => sizeof($arr_selected_categories_res));
            }
        }
        $profile = $this->mirakl_database->getMiraklProfile($sub_marketplace, $id_country, $id_shop);

        foreach ($arr_selected_categories as $asc) {
            $html .= $this->getChildCategory($sub_marketplace, $id_country, $id_shop, $this->id_mode, $lang['id_lang'], $asc, $profile);
        }

        echo json_encode($html);
    }

    public function getChildCategory($sub_marketplace, $id_country, $id_shop, $mode = null, $id_lang, $category, $profile = null) {
        $html = $subchild = '';
        if (isset($mode) && !empty($mode)) {
            $this->smarty->assign("popup", 3);
            $this->smarty->assign("mode", $mode);
        }
        if (isset($category) && !empty($category)) {
            if ($category['type'] == "folder") {
                if (isset($category['child']) && !empty($category['child'])) {

                    $categories = $this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $this->id_mode, $category['id_category'], false, $id_lang);

                    foreach ($categories as $child) {
                        $subchild .= $this->getChildCategory($sub_marketplace, $id_country, $id_shop, $mode, $id_lang, $child, $profile);
                    }
                }
            }
        }

        $html .= $this->getSelectedCategoryTemplate($sub_marketplace, $id_country, $id_shop, $mode, $category, $subchild, $profile);
        return $html;
    }

    public function getSelectedCategoryTemplate($sub_marketplace, $id_country, $id_shop, $mode = null, $selected_category, $sub_html = null, $profile) {
        $mode = '';
        if (!isset($this->list_selected)) {
            $this->list_selected = $this->mirakl_database->getMiraklCategorySelect($sub_marketplace, $id_country, $id_shop);
        }

        if (isset($this->list_selected[$selected_category['id_category']]) && !empty($this->list_selected[$selected_category['id_category']])) {
            $this->smarty->assign('checked', true);
            $this->smarty->assign('selected', $this->list_selected[$selected_category['id_category']]['id_profile']);
        } else {
            $this->smarty->assign('checked', false);
            $this->smarty->assign('selected', '');
        }

        $this->smarty->assign('profile', $profile);

        if (isset($sub_html) && !empty($sub_html)) {
            $this->smarty->assign('sub_html', $sub_html);
        } else {
            $this->smarty->assign('sub_html', '');
        }

        $this->smarty->assign('type', $selected_category['type']);
        $this->smarty->assign('id_category', $selected_category['id_category']);
        $this->smarty->assign('name', $selected_category['name']);
        $this->smarty->assign('mode', $mode);

        return $this->smarty->fetch('mirakl/CategoryGroupTemplate.tpl');
    }

    public function getModels($sub_marketplace, $id_country, $id_shop) {
        $models = $this->mirakl_database->getMiraklMappingModel($sub_marketplace, $id_country, $id_shop, $this->id_lang);
        echo json_encode($models);
    }

    public function getValueList($id_value) {
        $value_list = $this->mirakl_model->getMiraklValueList($id_value);
        echo json_encode($value_list);
    }

    public function getValueByFieldShop() {
        // getAttributeFieldShop
        $html = '<option value="">---</option>';
        if ($this->input->post()) {

            $value = $this->input->post();
            if (isset($value['field_shop']) && !empty($value['field_shop'])) {
                $results = $this->mirakl_database->getValueByFieldShop($this->id_shop, $this->id_lang, $value);

                if (isset($results['results']) && !empty($results['results'])) {
                    foreach ($results['results'] as $result) {
                        $html.= "<option value='{$result['field_shop_code']}'>{$result['field_shop_label']}</option>";
                    }
                }
            }
        }

        echo $html;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function delete_profile() {
        if ($this->input->post()) {
            $values = $this->input->post();
            $flag = $this->mirakl_database->deleteProfile($values);

            echo $flag;
        } else {
            echo false;
        }
    }

    public function delete_model() {
        if ($this->input->post()) {
            $values = $this->input->post();
            $flag = $this->mirakl_database->deleteModel($values);

            echo $flag;
        } else {
            echo false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public function actions($type = null, $sub_marketplace = null, $id_country = null) {
        $this->isCountry($sub_marketplace, $id_country);
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $assign = array();
        $assign['action'] = '#';
        $assign['active_api'] = $this->isActiveApi($sub_marketplace, $id_country);
        $assign['active_file'] = $this->isActiveFile();
        $data['date_start'] = date('d-m-Y', strtotime('now -7 days'));

        if ($type == 'offers') {
            $assign['current_page'] = 'action_offer';
        } else if ($type == 'products') {
            $assign['current_page'] = 'action_product';
        } else if ($type == 'delete') {
            $assign['current_page'] = 'action_delete';
        } else if ($type == 'orders_import') {
            $assign['current_page'] = 'action_order_import';
        } else if ($type == 'orders_accept') {
            $assign['current_page'] = 'action_order_accept';
        }

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function orders($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $assign = array();
        $assign['action'] = '#';
        $assign['active_api'] = $this->isActiveApi($sub_marketplace, $id_country);
        $assign['active_file'] = $this->isActiveFile();
        $assign['current_page'] = 'page_order';
        $data['date_start'] = date('d-m-Y', strtotime('now -7 days'));

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function scheduled_tasks($sub_marketplace = null, $id_country = null) {

        $data = array();
        $countdown = array();
        $marketplace = $this->session->userdata['menu'];
        $this->assignCountryData($sub_marketplace, $id_country);
        $data['no_data'] = _get_message_code('23-00006', 'inner');
        $this->load->model('cron_model', 'cron');

        $ext = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['ext'];
        $site_id = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['id'];
        $market = strtolower($this->tag_general_uc);
        $marketplace_name = $this->tag_mirakl_uc;
        $sub_marketplace_name = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['name_marketplace'];
        $domain = $marketplace[$this->tag_general_uc]['submenu'][$sub_marketplace][$id_country]['domain'];
        $list = $this->cron->getUserCronList($this->id_user, $marketplace_name, $ext, false, $sub_marketplace);

        $data['ext'] = $ext;
        $data['market'] = $market;
        $data['tasks'] = array();

        foreach ($list as $l) {
            $task = array('task_name' => $l['cron_type'],
                //'task_display_name' => $domain.' '.$l['cron_action'].' '.$l['cron_type'],
                'task_display_name' => $domain.' '.$this->lang->line($l['cron_action'].' '.$l['cron_type']),
                'task_lastupdate' => '',
                'task_id' => $l['id_cron'],
                'sub_market' => $sub_marketplace,
                'task_status' => '',
                'task_running' => $l['status'] || $l['status'] == '' ? true : false
            );

            $next_time = time();
            if ($l['cron_key'] == '') {
                $task['task_key'] = $l['cron_marketplace'].$ext.'_'.$l['cron_action'].'_'.$l['cron_type'];
            } else {
                $task['task_key'] = str_replace('{ext}', $ext, $l['cron_key']);
                $task['task_key'] = str_replace('{sub}', $sub_marketplace, $task['task_key']);
                $key = str_replace('{site_id}', $site_id, $task['task_key']);
                $next_time = $this->cron->getTimeLastCronRun($this->id_user, $key);
                if ($next_time <= time())
                    $next_time = time();
            }
            $data['tasks'][] = $task;
            $countdown[$task['task_key']] = $next_time - time();
            $data['countdown'] = $countdown;
        }
        $data['allow_cron'] = $this->mirakl_model->check_allow_cron($this->id_user);
        $this->smarty->assign("not_continue", true);

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $assign = array();
        $assign['action'] = '#';
        $assign['current_page'] = 'task_scheduled';

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function getOrderList($sub_marketplace = null, $id_country = null, $type = null, $order_id = null, $option = null) {
        $this->isCountry($sub_marketplace, $id_country);

        if ($values = $this->input->post()) {

            $values = $this->input->post();
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];

            //Status
            $status = null;
            if (isset($values['columns'][9]['search']['value'])) {
                $search_status = $values['columns'][9]['search']['value'];
                if ($search_status != 'all') {
                    $status = $search_status;
                }
            }

            $sort = array();
            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "O.id_orders";
                        break;
                    case 1 : $column = "O.id_marketplace_order_ref";
                        break;
                    case 2 : $column = "OB.name";
                        break;
                    case 3 : $column = "O.payment_method";
                        break;
                    case 4 : $column = "O.total_paid";
                        break;
                    case 5 : $column = "O.order_status";
                        break;
                    case 6 : $column = "O.order_date";
                        break;
                    case 7 : $column = "O.shipping_date";
                        break;
                    case 8 : $column = "O.tracking_number";
                        break;
                    case 9 : $column = "OS.tracking_number";
                        break;
                }
                $sort[] = $column.' '.$order_sort['dir'];
            }

            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page; // $sub_marketplace, $id_country, $this->id_shop, $batch_id, null, null, true, $ignore_case
            $data['recordsTotal'] = $this->mirakl_order->getOrderList($sub_marketplace, $id_country, $this->id_shop, null, null, $order_by, $search, $status, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $results = $this->mirakl_order->getOrderList($sub_marketplace, $id_country, $this->id_shop, $start, $limit, $order_by, $search, $status);

            if (!empty($results)) {
                foreach ($results as $key => $result) {
                    $set = array();
                    $set['id_orders'] = $result['id_orders'];
                    $set['id_marketplace_order_ref'] = $result['id_marketplace_order_ref'];
                    $set['name'] = $result['address_name'];
                    $set['payment_method'] = $result['payment_method'];
                    $set['total_paid'] = number_format($result['total_paid'], 2);
                    $set['status'] = $result['status'];
                    $set['order_status'] = $result['order_status'];
                    $set['order_date'] = date('Y-m-d H:i:s', strtotime($result['order_date']));
                    $set['invoice_no'] = $result['invoice_no'];
                    $set['shipping_date'] = !empty($result['tracking_number']) ? date('Y-m-d H:i:s', strtotime($result['shipping_date'])) : '';
                    $set['tracking_number'] = $result['tracking_number'];
                    $set['comment'] = $result['comment'];
                    $set['error'] = ($result['status'] == 'error') ? 'error' : '';

                    // message
                    $set['flag_mail_invoice'] = isset($result['flag_mail_invoice']) ? $result['flag_mail_invoice'] : '';
                    $set['flag_mail_review'] = isset($result['flag_mail_review']) ? $result['flag_mail_review'] : '';

                    array_push($data['data'], $set);
                }
            }
            echo json_encode($data);
        }
    }

    public function ajax_update_cron_status() {
        if ($this->input->post()) {
            $user_id = $this->id_user;
            $this->load->model('cron_model', 'cron');
            $cron_id = $this->input->post('cid');
            $is_sub_market = $this->input->post('srel');
            $status = $this->input->post('status') == 'true' ? 1 : 0;
            $ext = $this->input->post('ext');
            $this->cron->updateUserCron($user_id, $cron_id, $ext, $status, $is_sub_market);
        }
    }

    public function report($sub_marketplace = null, $id_country = null) {
        $this->isCountry($sub_marketplace, $id_country);
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();
        $assign = array();
        $assign['action'] = '#';
        $assign['current_page'] = 'page_report';

        $this->assign($assign);
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function logs($sub_marketplace = null, $id_country = null) {

        $this->isCountry($sub_marketplace, $id_country);
        $this->assignCountryData($sub_marketplace, $id_country);

        $data = array();

        $last_update = $this->mirakl_database->getMiraklLogLast($sub_marketplace, $id_country, $this->id_shop);
        $data['last_update'] = $last_update;

        $logs = $this->mirakl_database->getMiraklLog($sub_marketplace, $id_country, $this->id_shop);
        $data['histories'] = $logs;


//        echo '<pre>'; print_r($last_update); exit;
//
////        $last_normal_update = $this->mirakl_database->getMiraklLog($sub_marketplace, $id_country, $this->id_shop, true, false);
////        $last_cron_update = $this->mirakl_database->getMiraklLog($sub_marketplace, $id_country, $this->id_shop, true, true);
//        
//        $logs = $this->mirakl_database->getMiraklLog($sub_marketplace, $id_country, $id_country, $this->id_shop, false, null, null, 0, true);
//        
//        $log = $this->getLog($sub_marketplace, $id_country, $this->id_shop, 0, true);
//
//
//        
//        $data['last_normal_update'] = isset($last_normal_update['0']) && !empty($last_normal_update['0']) ? $last_normal_update['0'] : array('date_add' => 'Never'); // Never check in page_log
//        $data['last_cron_update'] = isset($last_cron_update['0']) && !empty($last_cron_update['0']) ? $last_cron_update['0'] : array('date_add' => 'Never'); // Never check in page_log
//        $data['history'] = $log;
//        if (isset($log[0]['batch_id']) && !empty($log[0]['batch_id'])) { //If its not popup
//            $batch_id = $log[0]['batch_id'];
//            $this->smarty->assign("batch_id", $batch_id);
//        }

        $assign = array();
        $assign['action'] = '#';
        $assign['current_page'] = 'page_log';

        $this->assign($assign);
        //echo '<pre>'; print_r($data); exit;
        $this->smarty->view('mirakl/main_template.tpl', $data);
    }

    public function getLog($sub_marketplace, $id_country, $id_shop, $page = null, $return_array = false) { // to use in this and page_log.js
        $data = array();
        $logs = array();
        $option = array();
        $value = $this->input->post(); // for fitter in logs



        if (!empty($value)) {

            if (!empty($value['date_from'])) {
                $option['date_from'] = date('Y-m-d', strtotime($value['date_from']));
            }

            if (!empty($value['date_to'])) {
                $option['date_to'] = date('Y-m-d', strtotime($value['date_to']));
            }

            if (!empty($value['feed_type'])) {
                $option['feed_type'] = trim($value['feed_type']); // change name feed type
            }
        }

        $logs = $this->mirakl_database->getMiraklLog($sub_marketplace, $id_country, $id_shop, $page = null, $option);
         echo json_encode($logs);
        


////echo '<pre>'; print_r($logs); exit;
//
//        if (isset($logs) && !empty($logs)) {
//            foreach ($logs as $index => $log) {
//                $logs[$index]['message'] = sprintf($this->lang->line($log['log_key']), $log['log_value']);
//                //$logs[$index]['message'] = sprintf(Mirakl_Tools::l($log['log_key']), $log['log_value']);
//            }
//        }
//
//        if (!$return_array) {
//            echo json_encode($logs);
//        }
//
//        return $logs;
    }

    public function getLogDetail($sub_marketplace = null, $id_country = null) {
        $this->assignCountryData($sub_marketplace, $id_country);

        if ($this->input->post()) {
            $values = $this->input->post();
            if (isset($values) && !empty($values)) {

                $page = $values['draw'];
                $limit = $values['length'];
                $start = $values['start'];

                $search = '';
                $batch_id = '';

                if (isset($values['search']['value']) && !empty($values['search']['value'])) {

                    if (isset($values['search']['regex']) && !empty($values['search']['regex'])) {
                        $batch_id = $values['search']['regex'];
                    }

                    $search = $values['search']['value'];
                    //$batch_id = $values['search']['value'];
                } else {
                    $batch_id = '';
                }

                $ignore_case = false;

//                 [search] => Array
//        (
//            [value] => RC00372700111xxs
//            [regex] => false
//        )
//            if (isset($values['ignore_case']) && $values['ignore_case'] == 'true') {
//                $ignore_case = true;
//            }

                $data = array();
                $data['draw'] = $page;
                $data['recordsTotal'] = $this->mirakl_database->getMiraklLogDetail($sub_marketplace, $id_country, $this->id_shop, $batch_id, null, null, true, $ignore_case, $search);
                $data['recordsFiltered'] = $data['recordsTotal'];
                $data['data'] = array();
                $logs = $this->mirakl_database->getMiraklLogDetail($sub_marketplace, $id_country, $this->id_shop, $batch_id, $start, $limit, false, $ignore_case, $search);

                if (!empty($logs)) {
                    $set = array();
                    foreach ($logs as $key => $log) {
                        $set['batch_id'] = $log['batch_id'];
                        $set['feed_type'] = $log['feed_type'];
                        $set['reference_type'] = $log['reference_type'];
                        $set['reference'] = $log['reference'];
                        //$set['message'] = sprintf($this->lang->line($log['log_key']), $log['log_value']);
                        $set['message'] = sprintf(Mirakl_Tools::l($log['log_key']), $log['log_value']);
                        $set['date_add'] = $log['date_add'];

                        array_push($data['data'], $set);
                    }
                }
                echo json_encode($data);
            }
        }
    }

    public function getLogReport($sub_marketplace = null, $id_country = null) { // used to page_report.js master
        $this->assignCountryData($sub_marketplace, $id_country);

        if ($this->input->post()) {
            $batch_id = '';
            $search = array();

            $values = $this->input->post();
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];

//        debug
//        $page = 0;
//        $limit = 10;
//        $start = 1;
//            if (isset($values['columns'])) {
//                foreach ($values['columns'] as $columns) {
//                    if (!empty($columns['search']['value'])) {
//                        $search[$columns['data']] = $columns['search']['value'];
//                    }
//                }
//            }

            if (isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search['all'] = $values['search']['value'];
            }

            $sort = array();
            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = 'id_log_report';
                        break;
                    case 1 : $column = 'id_log_report';
                        break;
                    default : $column = 'id_log_report';
                }
                $sort[] = $column.' '.$order_sort['dir'];
            }
            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->mirakl_database->getMiraklLogReport($sub_marketplace, $id_country, $this->id_shop, $batch_id, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $report_detail = $this->mirakl_database->getMiraklLogReport($sub_marketplace, $id_country, $this->id_shop, $batch_id, $start, $limit, $order_by, $search);

            if (!empty($report_detail)) {
                $set = array();
                foreach ($report_detail as $detail) {
                    $set['batch_id'] = $detail['batch_id'];
                    $set['feed_type'] = $detail['feed_type'];
                    $set['detail'] = $detail['detail'];
                    array_push($data['data'], $set);
                }
            }

            echo json_encode($data);
        }
    }

    public function getLogReportDetail($sub_marketplace = null, $id_country = null) { // used to page_report.js detail
        $this->assignCountryData($sub_marketplace, $id_country);

        if ($this->input->post()) {

            $values = $this->input->post();
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];

            $batch_id = isset($values['search']['value']) && !empty($values['search']['value']) ? $values['search']['value'] : '';
            $sort = array();
            $search = array();

            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "id_log_report_detail";
                        break;
                    case 1 : $column = "id_log_report_detail";
                        break;
                    default:
                        $column = "id_log_report_detail";
                }
                $sort[] = $column.' '.$order_sort['dir'];
            }

            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->mirakl_database->getMiraklLogReportDetail($sub_marketplace, $id_country, $this->id_shop, $batch_id, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $report_detail = $this->mirakl_database->getMiraklLogReportDetail($sub_marketplace, $id_country, $this->id_shop, $batch_id, $start, $limit, $order_by, $search);


            if (isset($report_detail) && !empty($report_detail)) {
                $set = array();
                foreach ($report_detail as $key => $detail) {
                    $set['batch_id'] = $detail['batch_id'];
                    $set['feed_type'] = $detail['feed_type'];
                    $set['reference'] = $detail['reference'];
                    $set['log_error'] = !empty($detail['log_error']) ? $detail['log_error'] : '-';
                    $set['log_warning'] = !empty($detail['log_warning']) ? $detail['log_warning'] : '-';
                    $set['date_add'] = $detail['date_add'];

                    array_push($data['data'], $set);
                }
            }

            echo json_encode($data);
        }
    }

    public function getOrderAcceptList($sub_marketplace = null, $id_country = null, $id_shop = '') { // use in accept order
        $this->isCountry($sub_marketplace, $id_country);

        if ($this->input->post()) {
            $values = $this->input->post();
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $sort = array();
            $search = array();

            if (isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search['all'] = $values['search']['value'];
            }

            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "order_id";
                        break;
                    case 1 : $column = "order_id";
                        break;
                }
                $sort[] = $column.' '.$order_sort['dir'];
            }

            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->mirakl_order->getMiraklOrderAcceptList($sub_marketplace, $id_country, $id_shop, null, null, null, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $results = $this->mirakl_order->getMiraklOrderAcceptList($sub_marketplace, $id_country, $id_shop, $start, $limit, $order_by, $search);

            if (!empty($results)) {
                foreach ($results as $key => $result) {
                    $data['data'][$key]['id_order_accept'] = $result['id_order_accept'];
                    $data['data'][$key]['order_id'] = $result['order_id'];
                    $data['data'][$key]['order_date'] = $result['order_date'];
                    $data['data'][$key]['customer_name'] = $result['customer_name'];
                    $data['data'][$key]['shipping_price'] = $result['shipping_price'];
                    $data['data'][$key]['total_price'] = $result['total_price'];
                    $data['data'][$key]['order_state'] = $result['order_state'];
                    $data['data'][$key]['tracking_no'] = $result['tracking_no'];
                    $data['data'][$key]['invoice_no'] = $result['invoice_no'];
                }
            }

            echo json_encode($data);
        }
    }

    public function download_product($sub_marketplace, $id_country, $mode, $path_download = '') {

        $this->assignCountryData($sub_marketplace, $id_country);
        $get_config = $this->mirakl_scheme->getMiraklConfiguration($this->id_user, $sub_marketplace, $id_country, $this->id_shop);

        if (empty($get_config)) {
            redirect('general/parameters/'.$sub_marketplace.'/'.$id_country);
            return;
        }

        $get_config['download'] = true;

        $rawmode = json_decode(rawurldecode($mode));

        $params = array();
        $params['in_stock'] = $rawmode->in_stock;
        $params['send_mirakl'] = 0;

        $mirakl_function = new MiraklProductCreate($get_config, false);
        if (!empty($path_download)) {
            $return_value = $mirakl_function->downloadFile($path_download);
        } else {
            //$return_value = $mirakl_function->productCreate($params, true);
            $return_value = $mirakl_function->processProduct($params, true);
        }

        if (!empty($return_value)) {
            echo json_encode($return_value);
        }
    }

    public function download_offer($sub_marketplace, $id_country, $mode, $path_download = '') {

        $this->assignCountryData($sub_marketplace, $id_country);
        $get_config = $this->mirakl_scheme->getMiraklConfiguration($this->id_user, $sub_marketplace, $id_country, $this->id_shop);

        if (empty($get_config)) {
            redirect('general/parameters/'.$sub_marketplace.'/'.$id_country);
            return;
        }

        $get_config['download'] = true;

        $rawmode = json_decode(rawurldecode($mode));

        $params = array();
        $params['export_all'] = $rawmode->export_all;
        $params['purge'] = $rawmode->purge;
        $params['send_mirakl'] = 0;

        $mirakl_function = new MiraklProductUpdate($get_config, false);
        if (!empty($path_download)) {
            $return_value = $mirakl_function->downloadFile($path_download);
        } else {
            $return_value = $mirakl_function->processProduct($params, true);
        }

        if (!empty($return_value)) {
            echo json_encode($return_value);
        }
    }

    public function download_delete_product($sub_marketplace, $id_country, $mode, $path_download = '') {

        $this->assignCountryData($sub_marketplace, $id_country);
        $get_config = $this->mirakl_scheme->getMiraklConfiguration($this->id_user, $sub_marketplace, $id_country, $this->id_shop);

        if (empty($get_config)) {
            redirect('general/parameters/'.$sub_marketplace.'/'.$id_country);
            return;
        }

        $get_config['download'] = true;

        $rawmode = json_decode(rawurldecode($mode));

        $params = array();
        $params['reason_delete'] = $rawmode->reason_delete;
        $params['send_mirakl'] = 0;

        //echo '<pre>'; print_r($params); exit;

        $mirakl_function = new MiraklDeleteProducts($get_config, false);

        if (!empty($path_download)) {
            $return_value = $mirakl_function->downloadFile($path_download);
        } else {
            $return_value = $mirakl_function->processProduct($params, true);
        }

        if (!empty($return_value)) {
            echo json_encode($return_value);
        }
    }

}

// end definition
