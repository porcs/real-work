<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class validation extends CI_Controller
{
    
    public $user_name;
    public $id_user;
    public $iso_code;
    public $id_shop;
    public $id_mode;

    public function __construct()
    {
        
        // load controller parent
        parent::__construct();
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');

        //1. library
        $this->load->library('authentication');

        $this->load->library('form_validation');
        $this->load->library('FeedBiz', array($this->user_name));
        
        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("validation", $this->language);
        $this->smarty->assign("label", "validation");
        $this->iso_code = mb_substr($this->language, 0, 2);
    }
    
    public function statistics()
    {
        $data = array();
        
        $config_data = $this->my_feed_model->get_general($this->id_user);
        
        if (isset($config_data) && !empty($config_data)) {
            foreach ($config_data as $key => $value) {
                if (strtolower($key) != "feed_mode") {
                    $data[strtolower($key)] = unserialize(base64_decode($value['value']));
                }
            }
        
            if (isset($config_data['FEED_BIZ']) && !empty($config_data['FEED_BIZ'])) {
                $data['feed_biz']['shop']   = $this->feedbiz->getDefaultShop($this->user_name);
                $data['feed_biz']['time']   = $config_data['FEED_BIZ']['config_date'];
            }
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('validation/dashboard/statistics.tpl', $data);
    }
    
    public function messages()
    {
        $data = array();
        $list_product = $this->feedbiz->get_log_product($this->user_name);
        $data['shop'] = $this->feedbiz->getShops($this->user_name);
        
        foreach ($list_product as $logs) {
            foreach ($logs as $key => $log) {
                $data['product'][$key] = $log;
            }
        }
        
        
        $list_offer = $this->feedbiz->get_log_offer($this->user_name);
        
        foreach ($list_offer as $logs) {
            foreach ($logs as $key => $log) {
                $data['offer'][$key] = $log;
            }
        }
            
//        echo '<pre>' . print_r($list_product, true) . '</pre>'; 
//        exit;

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('validation/dashboard/messages.tpl', $data);
    }
    
    public function log()
    {
        $data = array();
        $data['logs'] = $this->feedbiz->getImportLog($this->user_name);
                
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('validation/log.tpl', $data);
    }
    
    public function log_last_import($type, $shop)
    {
        $shop= html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($shop)), null, 'UTF-8');
        $logs = $this->feedbiz->get_last_import_log($this->user_name, $type, $shop);
        
        if (isset($logs) && !empty($logs)) {
            echo json_encode($logs);
        } else {
            echo json_encode(array("error" => "true" ));
        }
    }
    
    public function log_download($type, $batch_id)
    { 
        $this->feedbiz->downloadLog($this->user_name, $type, $batch_id);
    }
    
    public function log_download_products($param)
    {
        $data = null;
        
//        echo '<pre>' .print_r($param, true) . '</pre>'; exit;
        $decode= html_entity_decode(
            preg_replace("/%u([0-9a-f]{3,4})/i",
                "&#x\\1;", urldecode($param)), null, 'UTF-8');
        $value = json_decode($decode);
        
        if (isset($value) && !empty($value)) {
            $data = array(
                "batch_id" => $value->batch_id,
                "id_product" => $value->id_product,
                "id_shop" => (int)$value->id_shop,
                "severity" => $value->type,
                "message" => $value->message,
                "date_add" => $value->date_add,
            );
        }
        //echo '<pre>' . print_r($data, true) . '</pre>'; exit;
        $this->feedbiz->downloadLog($this->user_name, 'products', $data);
    }
    
    public function log_download_offers($param)
    {
        $data = null;
        $decode= html_entity_decode(
            preg_replace("/%u([0-9a-f]{3,4})/i",
                "&#x\\1;", urldecode($param)), null, 'UTF-8');
        $value = json_decode($decode);
        
        if (isset($value) && !empty($value)) {
            $data = array(
                "batch_id" => $value->batch_id,
                "id_product" => $value->id_product,
                "id_shop" => (int)$value->id_shop,
                "severity" => $value->type,
                "message" => $value->message,
                "date_add" => $value->date_add,
            );
        }
        
        $this->feedbiz->downloadLog($this->user_name, 'offers', $data) ;
    }
}
