<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class service extends CI_Controller
{
    private $config_pay;
    private $packageArr;
    private $discount;
    private $discount_date_percent;
    private $discount_date;
    private $giftDC;
    private $queryReferral;
    const BASE_PK_ID = 48;
    
    public function __construct()
    {
        parent::__construct();
        
        error_reporting(0);
        
        $this->load->library('authentication');
        $this->load->database();
        
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->lang->load("user", $this->language);
        $this->lang->load('paypal', $this->language);
        
        // load paypal config
        $this->config->load('paypal_settings');
        $this->config_pay = $this->config->item('pay');
        $this->smarty->assign("lang", $this->language);
        $this->smarty->assign("label", "paypal");
        _get_message_document($this->router->fetch_class(),
            $this->router->fetch_method());
    }
    
    public function region()
    {
        /*
        * Initial
        */
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'model');
        $this->load->model('currency_model', 'currency');
        $this->load->library('mydate');
        
        if ($this->input->post()) {
            if ($this->input->post('region')) {
                $arrRegion = array();
                foreach ($this->input->post('region') as $val) {
                    $arrVal = explode('_', $val);
                    $arrRegion[$arrVal[0]] = $arrVal[1];
                }
                $this->session->set_userdata('region', $arrRegion);
            }

            if ($this->input->post('currency')) {
                $this->currency->update_user_currency($data['userData']['id'],
                    $this->input->post('currency'));
//                $this->session->set_userdata('currency',$this->input->post('currency'));
//                $this->db->update('users',array('user_currency'=>$this->input->post('currency')),array('id'=>$data['userData']['id']));
            }

            redirect(site_url('service/packages'));
            return;
        } else {
            /* 
            * Breadcrumb
            */
            $this->breadcrumb->clear();
            $this->breadcrumb->add_crumb('Billing');
            $this->breadcrumb->add_crumb('Packages');
            $this->breadcrumb->change_link(
                '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
            $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /*
            * End Breadcrumb
            */
            
            if ($this->session->userdata('paymentComplete')) {
                $this->session->unset_userdata('paymentComplete');
            }
            
            $sessionRegion = null;
            if ($this->session->userdata('region')) {
                $sessionRegion = $this->session->userdata('region');
            } else {
                $tmpRegion = $this->model->getUserRegion($data['userData']['id']);
                if (!empty($tmpRegion)) {
                    $sessionRegion = array();
                    foreach ($tmpRegion as $val) {
                        $sessionRegion[$val['id_region']] = $val['name_region'];
                    }
                    $this->session->set_userdata('region', $sessionRegion);
                }
            }
            
            $initBtn='';
            if (!$sessionRegion) {
                $initBtn = ' disabled="disabled"';
            }
            
            $region = $this->model->getRegion();
            
            $arrCurrency = array(
                'AUD' => 'Australian Dollar',
                'BRL' => 'Brazilian Real',
                'CAD' => 'Canadian Dollar',
                'CZK' => 'Czech Koruna',
                'DKK' => 'Danish Krone',
                'EUR' => 'Euro',
                'HKD' => 'Hong Kong Dollar',
                'HUF' => 'Hungarian Forint',
                'ILS' => 'Israeli New Sheqel',
                'JPY' => 'Japanese Yen',
                'MYR' => 'Malaysian Ringgit',
                'MXN' => 'Mexican Peso',
                'NOK' => 'Norwegian Krone',
                'NZD' => 'New Zealand Dollar',
                'PHP' => 'Philippine Peso',
                'PLN' => 'Polish Zloty',
                'GBP' => 'Pound Sterling',
                'RUB' => 'Russian Ruble',
                'SGD' => 'Singapore Dollar',
                'SEK' => 'Swedish Krona',
                'CHF' => 'Swiss Franc',
                'THB' => 'Thai Baht',
                'TRY' => 'Turkish Lira',
                'USD' => 'U.S. Dollar',
            );
            ksort($arrCurrency);
            
            if ($this->session->userdata('currency')) {
                $currency = $this->session->userdata('currency');
            } else {
                $currency = "EUR";
            }
            
            $this->smarty->assign('region', $region);
            $this->smarty->assign('sessionRegion', $sessionRegion);
            $this->smarty->assign('initBtn', $initBtn);
            $this->smarty->assign('currency', $currency);
            $this->smarty->assign('arrCurrency', $arrCurrency);
            $this->smarty->assign('siteurl', site_url());
            
            $this->smarty->view('service/region.tpl', $data);
        }
    }
    
    public function packages()
    {
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $this->load->model('currency_model', 'currency');
        $currency = $this->currency->get_user_currency($data['userData']['id']);
        
        if ($this->input->post('currency') && $this->input->post('currency')!= $currency) {
            $this->currency->update_user_currency($data['userData']['id'],
                $this->input->post('currency'));
            redirect('service/packages');
            return;
        }
        
        $data['currency'] = $currency;
        $data['arrCurrency'] = $this->currency->get_currency();
        $pk_all = $this->model->getUserPackageID($data['userData']['id'], true);
//        print_r($this->input->post());exit;
        if ($this->input->post() /*&& sizeof($pk_all)>0*/) {
            $region_sel = $this->session->userdata('region_sel');
            
            if ($this->input->post('offer_pkgs')) {
                $sessOffer = (array)$this->session->userdata('offer');
                $offerArr = $this->input->post('offer_pkgs');
                
                unset($sessOffer[$region_sel]);
                
                foreach ($offerArr as $val) {
                    $sessOffer[$region_sel][$val] = $val;
                }
                $this->session->set_userdata('offer', $sessOffer);
            } else {
                $this->session->set_userdata('error',
                    $this->lang->line('hove_not_select_pkg'));
                redirect('service/packages');
                retuurn;
            }
            
            if ($this->input->post('amazon_eu')) {
                $sessAmazonEU = (array)$this->session->userdata('amazon_eu');
                $aeuArr = $this->input->post('amazon_eu');
                
                unset($sessAmazonEU[$region_sel]);
                
                foreach ($aeuArr as $val) {
                    $sessAmazonEU[$region_sel][$val] = $val;
                }
                $this->session->set_userdata('amazon_eu', $sessAmazonEU);
            }
//            echo 'xxx';exit;
            redirect('service/information');
            return;
        } else {
            if ($this->session->userdata('error')) {
                $data['error'] = $this->session->userdata('error');
                if ($this->session->userdata('error') == 'session_error') {
                    $data['error'] =
                        $this->lang->line('Session Timeout Please Login again.');
                }
                $this->session->unset_userdata('error');
            }
                    
//            if(sizeof($pk_all)==0){
//                redirect(site_url().'marketplace/configuration'); 
//                return;
//            }else{
                $tmpRegion = $this->model->getUserRegion($data['userData']['id']);
            if (!empty($tmpRegion)) {
                $sessionRegion = array();
                foreach ($tmpRegion as $val) {
                    $sessionRegion[$val['id_region']] = $val['name_region'];
                }
                $this->session->set_userdata('region', $sessionRegion);
            }
//            }
            $this->load->library('eucurrency');
            
            /* Breadcrumb */
            $this->breadcrumb->clear();
            $this->breadcrumb->add_crumb('Billing');
            $this->breadcrumb->add_crumb('Packages');
            $this->breadcrumb->change_link(
                '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
            $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /* End Breadcrumb */
            
            $totalAmount = 0;
            
            /* Region and Select Region*/
            /*if($this->session->userdata('region'))
            {
                $arrRegion = $this->session->userdata('region');
                if($this->input->get('region'))
                {
                    $id = $this->input->get('region');
                    if($arrRegion[$id])
                    {
                        $this->session->set_userdata('region_sel',$id);
                    }
                }
                
                if($this->session->userdata('region_sel'))
                {
                    $regionSel = $this->session->userdata('region_sel');
                }
                else
                {
                    $regionSel = array_shift(array_keys($arrRegion));
                    $this->session->set_userdata('region_sel',$regionSel);
                }
            }
            else//redirect when empty
            {
                redirect(site_url().'marketplace/configuration');
                //redirect(site_url().'service/region');
                return;
            }*/
            /* End */
            
//            if($this->session->userdata('currency'))
//            {
//                $currency = $this->session->userdata('currency');
//            }
//            else
//            {
//                $currency = 'EUR';
//            }
//            if( $this->session->userdata('offer')){
//                $offer = $this->session->userdata('offer');
//            }else{
                $offer = array();
            $tmpOffer = $this->model->getUserPackageZoneID($data['userData']['id'], true);
                    
            if (!empty($tmpOffer)) {
                foreach ($tmpOffer as $val) {
                    $offer[$val['id_offer_pkg']][$val['id_package']] = $val['id_package'];
                }
            }
            $this->session->set_userdata('offer', $offer);
//            }  
            /*if($this->session->userdata('amazon_eu'))
            {
                $amazon_eu = $this->session->userdata('amazon_eu');
            }
            else
            {
                $amazon_eu = array();
                $tmpOfferAmazonEU = $this->model->getUserPackageID($data['userData']['id'],true);
                if(!empty($tmpOfferAmazonEU))
                {
                    foreach($tmpOfferAmazonEU as $val)
                    {
                        $amazon_eu[$val['id_region']][$val['id_offer_sub_pkg']] = $val['id_offer_sub_pkg'];
                    }
                }
                
            }
            $region = $this->session->userdata('region');*/
                    
            //$offerPackage = $this->model->getOfferPackage($regionSel);
            $offerPackage = $this->model->getMainOfferPackageZone();
//            $this->session->set_userdata('mainPackage',$offerPackage);
            $main_pkg_offer = array_keys($offer);
//            echo '<pre>'.print_r($offerPackage,true).'</pre>';
            if ($offerPackage) {
                $arrOffer = array();
                
                $chk = ' checked="checked"';
                $count = 0;
                foreach ($offerPackage as $field) {
                    /*if(!empty($chk))
                    {
                        $field['chk'] = true;
                        if(empty($offer[$field['id_offer_pkg']][$field['id_offer_price_pkg']]))
                        {
                            $offer[$field['id_offer_pkg']] = $field['id_offer_pkg'];
                        }
                        $packageCurrency = $field['currency_offer_price_pkg'];
                    }*/
                    $packageCurrency = $field['currency_offer_price_pkg'];
                    
                    if (in_array($field['id_offer_pkg'], $main_pkg_offer)) {
                        $field['checked_item'] = true;
                    }
                    
                    $packageAmount = 0;
                    if ($currency != $field['currency_offer_price_pkg']) {
                        $packageAmount = $this->mydate->floatFormat(
                            $this->eucurrency->doConvert($field['amt_offer_price_pkg'],
                                $currency));
                    } else {
                        $packageAmount = $this->mydate->floatFormat(
                            $field['amt_offer_price_pkg']);
                    }
                    $field['packageAmount'] = $packageAmount;
                    
                    if (isset($regionSel) && isset($offer[$regionSel]) &&
                        !empty($offer[$regionSel][$field['id_offer_pkg']])) {
                        $totalAmount += $packageAmount;
                    }
                    $field['pk_status'] = $this->model->checkPackageAvail(
                        $field['id_offer_price_pkg']);
                    /*
                    * Sub Menu
                    */
                    if ($field['submenu_offer_pkg'] ==1) {
                        $subOfferQuery =
                            $this->model->getMainOfferPackageZone('',
                                true, $field['id_offer_pkg']);
                    
                        if ($subOfferQuery) {
                            $def_id = '';
                            foreach ($subOfferQuery as $key => $field2) {
                                $subPackageAmount = 0;
                                if ($currency != $field2['currency_offer_price_pkg']) {
                                    $subPackageAmount = $this->mydate->floatFormat(
                                        $this->eucurrency->doConvert(
                                            $field2['amt_offer_price_pkg'],
                                            $currency));
                                } else {
                                    $subPackageAmount = $this->mydate->floatFormat(
                                        $field2['amt_offer_price_pkg']);
                                }
                                $subOfferQuery[$key]['subPackageAmount'] = $subPackageAmount;
                                
                                if (!empty($offer[$field2['id_offer_pkg']][$field2['id_offer_price_pkg']])) {
                                    $totalAmount += $subPackageAmount;
                                    $subOfferQuery[$key]['checked'] = true;
                                }
                                $subOfferQuery[$key]['pk_status'] =
                                    $this->model->checkPackageAvail(
                                        $subOfferQuery[$key]['id_offer_price_pkg']);
                                if ($field2['offer_pkg_type']==1) {
                                    $field['default_id']=$field2['id_offer_price_pkg'];
                                }
                            }
                            $field['sub'] = $subOfferQuery;
                        }
                    }
                    /*
                    * End Sub Menu
                    */
                    
                    $field['order'] = $count%2;
                    $arrOffer[] = $field;

                    $chk = '';
                    $count += 1;
                }
            }
            
            if (!empty($offer)) {
                $this->session->set_userdata('offer', $offer);
            }
            if (!empty($offerPackage)) {
                $this->session->set_userdata('arrOffer', $arrOffer);
            }
            $basePackage = $this->model->getMainPackageZone();
            
            if (!empty($basePackage)) {
                $basePackage=$basePackage[0];
                $packageAmount = 0;
                if ($currency != $basePackage['currency_offer_price_pkg']) {
                    $packageAmount = $this->mydate->floatFormat(
                        $this->eucurrency->doConvert(
                            $basePackage['amt_offer_price_pkg'],
                            $currency));
                } else {
                    $packageAmount = $this->mydate->floatFormat(
                        $basePackage['amt_offer_price_pkg']);
                }
                $basePackage['packageAmount'] = $packageAmount;
                $basePackage['pk_status'] = $this->model->checkPackageAvail(
                    $basePackage['id_offer_price_pkg']);

                    
                $this->smarty->assign('basepackage', $basePackage);
                $this->session->set_userdata('mainPackage', $basePackage);
            }
//            if(!empty($amazon_eu))
//            {
//                $this->session->set_userdata('amazon_eu', $amazon_eu);
//            }
//                echo '<pre>';
////            print_r($data );
////            print_r($region); 
////            print_r($offer);
////            print_r($offerPackage);
//            echo '<pre>'; 
//            print_r($basePackage);exit;

            /* Overviw Section*/
            $this->overview();
            /* End */
            
            
            $this->smarty->assign('region', isset($region)?$region:null);
            $this->smarty->assign('regionSel', isset($regionSel)?$regionSel:null);
            $this->smarty->assign('arrBgColor', array('bg-white', 'bg-gray'));
            $this->smarty->assign('currency', $currency);
            $this->smarty->assign('packageCurrency', $packageCurrency);
            $this->smarty->assign('currencySign',
                $this->mydate->moneySymbol($currency));
            $this->smarty->assign('offer', $arrOffer);
            $this->smarty->assign('totalAmount',
                $this->mydate->floatFormat($totalAmount));
            $this->smarty->assign('siteurl', site_url());

            $this->smarty->view('service/packages.tpl', $data);
        }
    }
    
    public function ajaxPackage()
    {
        $this->authentication->validateAjaxToken();
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $this->load->library('eucurrency');
        
        if ($this->input->post('checked') &&
            $this->input->post('id') &&
            $this->input->post('name')) {
            $id = $this->input->post('id');
            $pkt = $this->input->post('pkt');
            $checked = $this->input->post('checked');
            $name = str_replace('[]', '', $this->input->post('name'));
            $region = $this->session->userdata('region_sel');
                    
            if ($this->session->userdata('offer')) {
                $offer = $this->session->userdata('offer');
            } else {
                $offer = array();
            }
//            if($this->session->userdata('amazon_eu'))
//                $amazon_eu = $this->session->userdata('amazon_eu');
//            else
//                $amazon_eu = array();
            if ($name=='offer_pkgs') {
                $def = $this->input->post('pkt');
                if ($checked == 'true') {
                    $offer[$id][$def] = $def;
                } else {
                    unset($offer[$id]);
                }
            } else {
                if ($checked == 'true') {
                    $offer[$pkt][$id] = $id;
//                switch($name)
//                {
//                    case 'offer_pkgs':
//                        $offer[$pkt][$id] = $id;
//                        break;
//                    case 'amazon_eu':
//                        $amazon_eu[$region][$id] = $id;
//                        break;
//                }
                } else {
                    unset($offer[$pkt][$id]);
//                switch($name)
//                {
//                    case 'offer_pkgs':
//                        if($id=='2')
//                            unset($amazon_eu[$region]);
//                        unset($offer[$region][$id]);
//                        break;
//                    case 'amazon_eu':
//                        unset($amazon_eu[$region][$id]);
//                        break;
//                }
                }
            }
            
            $this->session->set_userdata('offer', $offer);
            //$this->session->set_userdata('amazon_eu',$amazon_eu);
            $this->model->cleanUserPackageDetail($data['userData']['id']);
                    
            foreach ($offer as $k =>$v) {
                $main_pk_id = $k;
                foreach ($v as $pk_id) {
                    $arrUserPkgDetail = array(
                        'id_users' => $data['userData']['id'],
                        'id_package' => $pk_id,
                        'option_package' => 'product',
                        'created_date_upkg_detail' => date('Y-m-d H:i:s'),
                    );
                    $this->model->setUserPackageDetail($arrUserPkgDetail);
                }
            }
            if ($this->session->userdata('currency')) {
                $currency = $this->session->userdata('currency');
            } else {
                $currency = 'EUR';
            }
            
            $this->overview();
            
            
            //$this->smarty->assign('region', $this->session->userdata('region'));
            $this->smarty->assign('currency', $currency);
            $this->smarty->assign('currencySign', $this->mydate->moneySymbol($currency));
            
            $this->smarty->view('service/overview.tpl');
        }
    }
    
    public function ajaxGiftCode()
    {
        $this->checkAuthen();
        
        if ($this->input->post('param') &&
            $this->input->post('code') &&
            $this->input->post('param') == 'giftcode') {
            $this->load->model('paypal_model', 'model');
            $code = $this->input->post('code');
            
            $query = $this->model->getGiftCode($code);
            if ($query) {
                $this->session->set_userdata('giftCode',
                    $query[0]['id_giftcode']);
                echo 'true';
                return;
            } else {
                $this->session->unset_userdata('giftCode');
                echo 'This gift code can not be use.';
                return;
            }
        }
    }
    
    public function information()
    {
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'model');
        $this->load->model('country_model', 'country');
        $this->load->library('mydate');
        
        if ($this->input->post('info')) {
            $this->session->set_userdata('info', $this->input->post('info'));
            redirect(site_url('service/complete'));
            return;
        } else {
            /* 
            * Breadcrumb
            */
            $this->breadcrumb->clear();
            $this->breadcrumb->add_crumb('Billing');
            $this->breadcrumb->add_crumb('Information');
            $this->breadcrumb->change_link(
                '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
            $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /*
            * End Breadcrumb
            */
            
            if (!$this->session->userdata('offer')) {
                redirect(site_url('service/packages'));
                return;
            }
            
            $info = null;
            if ($this->session->userdata('info')) {
                $info = (array)$this->session->userdata('info');
            } else {
                $tmpInfo = $this->model->getUserPackage($data['userData']['id']);
                if ($tmpInfo) {
                    $info = array(
                        'firstname' => $tmpInfo[0]['firstname_upkg'],
                        'lastname' => $tmpInfo[0]['lastname_upkg'],
                        'complement' => $tmpInfo[0]['company_upkg'],
                        'address1' => $tmpInfo[0]['address1_upkg'],
                        'address2' => $tmpInfo[0]['address2_upkg'],
                        'stateregion' => $tmpInfo[0]['state_upkg'],
                        'city' => $tmpInfo[0]['city_upkg'],
                        'country' => $tmpInfo[0]['country_upkg'],
                        'zipcode' => $tmpInfo[0]['zip_upkg'],
                    );
                }
            }
            
            if (isset($tmpInfo[0]['country_upkg']) &&
                $tmpInfo[0]['country_upkg'] != '') {
                $ctry = $tmpInfo[0]['country_upkg'];
            } else {
                $ctry = '';
            }
            $country_option = $this->country->getOptionCountry($ctry);
            
            $this->smarty->assign('info', $info);
            $this->smarty->assign('country_option', $country_option);
            $this->smarty->assign('siteurl', site_url());
        
            $this->smarty->view('service/information.tpl', $data);
        }
    }
    
    public function address()
    {
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'model');
        
        if ($this->input->post('isAddr') && $this->input->post('isAddr') == 'true') {
            $json = $this->model->getCustomer($data['userData']['id']);
            echo json_encode($json);
        }
    }
    
    public function payment($type=null)
    {
        $this->load->model('currency_model', 'currency');
        $data = $this->checkAuthen();
        $popup = $this->session->userdata('popup_payment');
        $currency = $this->currency->get_user_currency($data['userData']['id']);
        if ($this->input->post('currency') &&
            $this->input->post('currency')!= $currency) {
            $this->currency->update_user_currency(
                $data['userData']['id'],
                $this->input->post('currency'));
            redirect('service/payment/cur');
            return;
        }
        
                    
        if ($this->session->userdata('pay')) {
            $this->load->model('paypal_model', 'model');
            $this->load->library('eucurrency');
            $this->load->library('mydate');
            $this->load->model('referrals_model', 'refModel');
            
          
            $id_user = $data['userData']['id'];
            $pay = (array)$this->session->userdata('pay');
            
            $region = null;
//                      echo '<pre>';print_r($pay);exit;
            if (isset($pay['offer']) && !empty($pay['offer'])) {
                foreach ($pay['offer'] as $key => $val) {
                    $region = $this->model->getRegionByID($key);
                    break;
                }
            }
//             $this->packageArr = $this->model->packageList(
//             $pay['offer'],
//             array($region[0]['id_region'] => $region[0]['name_region']),
//             $pay['sub']);
            $this->packageArr = $this->model->packageZoneList($pay);
                    
            /* Referral Discount */
             $this->discount = $this->refModel->getAvailUserCommission($id_user);
            $this->discount_date_percent = $this->model->get_percent_discount();
             
            $totalAMT = 0;
            $totalAmount_DateDiscount = 0;
            foreach ($this->packageArr as $val) {
                $totalAMT += $val['amt'];
                if ($this->model->checkPackageAvail($val['number']) == 0 &&
                    $val['pk_type'] != '3') {
                    $totalAmount_DateDiscount += $val['amt'];
                }
            }
            if ($type=='extend') {
                $this->discount_date = 0;
                $this->discount_date_percent = 0;
            } else {
                $this->discount_date =
                    $this->mydate->floatFormat(
                        $totalAmount_DateDiscount * floor($this->discount_date_percent)*0.01);
            }
             
                    
            /*$this->queryReferral = array();
            $this->queryReferral[$id_user]['child'] = $this->refModel->recurringUsers($id_user);
            if(!empty($this->queryReferral[$id_user]))
                $this->discount = $this->refModel->recurringAmount($id_user,$this->queryReferral[$id_user]);
             
             */
            /* End Referral Discount */
            
            /* Gift Code Check */
                    
            $this->giftDC = 0;
            if ($this->session->userdata('giftCode')) {
                $giftCode = $this->session->userdata('giftCode');
                $query = $this->model->getGiftCodeDetail($giftCode);
                if ($query) {
                    if ($query[0]['currency_giftopt'] == '%') {
                        $tmpTotal = 0;
                        foreach ($this->packageArr as $val) {
                            $tmpTotal += $val['amt'];
                        }
                        $this->giftDC = $tmpTotal*$query[0]['amt_giftopt']/100;
                    } else {
                        $this->giftDC = $query[0]['amt_giftopt'];
                    }
                }
            }
            /* End Gift Code Check */
            
            /* GiftCode */
            if (!$this->session->userdata('billID')) {
                $this->session->set_userdata('billID', $this->getBillID());
            }
            /* End GiftCode */
            
            if ($this->input->post('pay')) {
                if ($this->session->userdata('opt')) {
                    $this->session->unset_userdata('opt');
                }

                /* input post */
                $payArr = (array)$this->input->post('pay');
                $payOpt = $payArr['method'];
                /* end input post */
                    
                switch ($payOpt) {
                    case 'credit':
                        $this->session->set_userdata('opt', $payOpt);
                        $creArr = (array)$this->input->post('cre');
                        $this->payCredit($creArr);
                        break;
                    case 'express':
                        $this->session->set_userdata('opt', $payOpt);
                        $this->payExpress();
                        break;
                    case 'preapproval':
                        $this->session->set_userdata('opt', $payOpt);
                        $this->payPreapproval();
                        break;
                }
            } else {
                $billID = $this->session->userdata('billID');

                /* Breadcrumb */
                $this->breadcrumb->clear();
                $this->breadcrumb->add_crumb('Billing');
                $this->breadcrumb->add_crumb('Payment');
                $this->breadcrumb->change_link(
                    '<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
                $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
                /* End Breadcrumb */

                /* Preapproval Payment */
                $preapprovalProfile = $this->model->getPayment($data['userData']['id']);
                $isPreapproval = 'none';
                $preapprovalStatus = false;
                if ($preapprovalProfile) {
                    $preapprovalDetails = $this->preapprovalDetail(
                        $preapprovalProfile['key_preapproval']);
                    
                    if ($preapprovalDetails['Ack'] == 'Success' &&
                        $preapprovalDetails['Approved'] == 'true' &&
                        $preapprovalDetails['SenderEmail'] != '' &&
                        $preapprovalDetails['Status'] == 'ACTIVE') {
                        $preapprovalStatus = true;
                        $isPreapproval = 'active';
                    } else {
                        $isPreapproval = 'inactive';
                    }
                }
//                $preapprovalProfile = $this->model->getPayment($data['userData']['id']); 
//                $isPreapproval = 'none';
//                if($preapprovalProfile)
//                {
//                    if($preapprovalProfile['status_preapproval'] == 'active')
//                    {
//                        $isPreapproval = 'active';
//                    }
//                    else
//                    {
//                        $this->session->set_userdata('preapprovalTab',true);
//                        $isPreapproval = 'inactive';
//                    }
//                }
                /* End ... */

                //$currency = $this->config_pay['currencycode'];
                /*if($this->session->userdata('currency'))
                {
                    $currency = $this->session->userdata('currency');
                }
                else
                {
                    $currency = $this->config_pay['currencycode'];
                }*/
                    
                $currency = $this->currency->get_user_currency($data['userData']['id']);
                $data['arrCurrency'] = $this->currency->get_currency();
                $data['currency'] = $currency;
                
                // overview, total amount
                $totalAMT = 0;
                $totalAmount_DateDiscount = 0;
                foreach ($this->packageArr as $val) {
                    $totalAMT += $val['amt'];
                    if ($this->model->checkPackageAvail($val['number']) == 0 &&
                        $val['pk_type'] != '3') {
                        $totalAmount_DateDiscount += $val['amt'];
                    }
                }
                if ($type=='extend') {
                    $this->discount_date = 0;
                    $this->discount_date_percent = 0;
                } else {
                    $this->discount_date = $this->mydate->floatFormat(
                        $totalAmount_DateDiscount * floor($this->discount_date_percent)*0.01);
                }
                if ($this->discount > 0 ||
                    $this->giftDC > 0 ||
                    $this->discount_date>0) {
                    $tmpTotal = $totalAMT - $this->discount - $this->giftDC - $this->discount_date;
                    if ($tmpTotal < 0) {
                        $this->discount += $tmpTotal;
                    }
                    if ($this->discount<0) {
                        $this->discount=0;
                    }
                    if ($tmpTotal < 0) {
                        $tmpTotal = 0;
                    }
                } else {
                    $tmpTotal = $totalAMT;
                }
            
                if ($tmpTotal == 0) {
                    $this->session->set_userdata('opt', 'credit');
                    $this->session->set_userdata('paymentTransaction', array(
                        'status' => true,
                        'billID' => $billID,
                        'errorMessage' => '',
                        'paypalResult' => '',
                    ));
                    redirect(site_url('service/transaction'));
                    return;
                } else {
                    $this->paymentOverview();
                }
                   
                /* Error Check */
                $err = '';
                
                if ($this->session->userdata('paymentError')) {
                    $err = $this->session->userdata('paymentError');
                    $this->session->unset_userdata('paymentError');
                }
                /* End Error */
                $next_charge_date = $this->model->get_pk_next_charge();
                    
                $this->smarty->assign('isPreapproval', $isPreapproval);
                $this->smarty->assign('err', $err);
                $this->smarty->assign('popup', $popup);
                $this->smarty->assign('startDate', date('j F Y'));
                $this->smarty->assign('endDate', date('j F Y', strtotime($next_charge_date)));
                $this->smarty->assign('currencySign', $this->mydate->moneySymbol($currency));
                $this->smarty->assign('region', array(
                    $region[0]['id_region'] => $region[0]['name_region']));
                $this->smarty->assign('siteurl', site_url());
                
                $this->smarty->view('service/payment.tpl', $data);
            }
        } else {
            redirect(site_url('service/packages'));
        }
    }
    private function preapprovalDetail($prekey)
    {
        $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
        $this->preapprovaldetails->setPreapprovalKey($prekey);
        $PayPalResult = $this->preapprovaldetails->doTransaction();
                    
        return $PayPalResult;
    }
    private function overview()
    {
        $currency = $this->session->userdata('currency');
        if (empty($currency)) {
            $currency = $this->session->userdata('user_currency');
            //$currency = $ud->user_currency;
        }
        //$sessRegion = $this->session->userdata('region');
        $offer = $this->session->userdata('offer');
        $offerPackage = $this->session->userdata('arrOffer');
        $mainPackage = $this->session->userdata('mainPackage');
        //$amazon_eu = $this->session->userdata('amazon_eu');

        $totalAmount = 0;
        $arrOffer = array();
        
                    
                    
        foreach ($offerPackage as $key => $v) {
            $pk_id = $v['id_offer_price_pkg'];
            $pktype_id = $v['id_offer_pkg'];
                    
            $amount = 0;
            $packageCurrency = '';
            if (!empty($offer)) {
                ksort($offer);
                    
                    
                $query = $this->model->getOfferPkPrice($pk_id);
                if (empty($offer[$pktype_id])) {
                    continue;
                }
                if ($query) {
                    $thisAmount = 0;
//                        if($query[0]['currency_offer_price_pkg'] != $currency)
//                            $thisAmount = $this->mydate->floatFormat($this->eucurrency->doConvert($query[0]['amt_offer_price_pkg'], $currency));
//                        else
//                            $thisAmount = $this->mydate->floatFormat($query[0]['amt_offer_price_pkg']);
//                        $query[0]['thisAmount'] = $thisAmount;
//                        $amount += $thisAmount;
//                        $packageCurrency = $query[0]['currency_offer_price_pkg'];

                        if (!empty($v['sub'])) {
                            $amazonArr = $v['sub'];
                            //ksort($amazonArr);
                            $arrAmazonEU = array();
                    
                            foreach ($amazonArr as $amazonVar) {
                                if (empty($offer[$pktype_id][$amazonVar['id_offer_price_pkg']])) {
                                    continue;
                                }
                                
                                $id = $amazonVar['id_offer_price_pkg'];
                                $query2 = $this->model->getOfferPkPrice($id);
                                if ($query2) {
                                    $thisAmountAmazon = 0;
                                    if ($query2[0]['currency_offer_price_pkg']!=$currency) {
                                        $thisAmountAmazon =
                                            $this->mydate->floatFormat(
                                                $this->eucurrency->doConvert(
                                                    $query2[0]['amt_offer_price_pkg'],
                                                    $currency));
                                    } else {
                                        $thisAmountAmazon = $this->mydate->floatFormat(
                                            $query2[0]['amt_offer_price_pkg']);
                                    }
                                    $amount += $thisAmountAmazon;

                                    $query2[0]['thisAmountAmazon'] = $thisAmountAmazon;
                                    $arrAmazonEU[] = $query2[0];
                                }
                            }
                            $query[0]['arrAmazonEU'] = $arrAmazonEU;
                        }

                    $arrOffer[$key+1][] = $query[0];
                }
            } else {
                $arrOffer=array();
            }
            
//            echo '<pre>';
//            print_r($mainPackage);
//            print_r($arrOffer);exit;

            $totalAmount += $amount;
        }
        $mainPackage['thisAmount']= $mainPackage['packageAmount'];
        $totalAmount+=$mainPackage['thisAmount'];
        $arrOffer[0][0]=$mainPackage;
//        echo'<pre>'; 
//        print_r($offer);
//        print_r($arrOffer);
//        echo '</pre>';
        $this->smarty->assign('arrOverviewOffer', $arrOffer);
        $this->smarty->assign('allTotalAmount', $this->mydate->floatFormat($totalAmount));
    }
    
    private function paymentOverview()
    {
        //$currency = $this->config_pay['currencycode'];
        if ($this->session->userdata('currency')) {
            $currency = $this->session->userdata('currency');
        } else {
            $currency = $this->config_pay['currencycode'];
        }
        $pay = (array)$this->session->userdata('pay');
        $region = null;

        if (isset($pay['offer'])) {
            foreach ($pay['offer'] as $key => $val) {
                $region = $this->model->getRegionByID($key);
                break;
            }
            $offer = $pay['offer'];
        }
        if (!empty($pay['sub'])) {
            $amazon_eu = $pay['sub'];
        }
        $sessRegion = array($region[0]['id_region'] => $region[0]['name_region']);
        $packageArr = $this->packageArr ;
        $totalAmount = 0;
        /*$arrOffer = array();
        
        foreach($sessRegion as $key => $v)
        {
            $amount = 0;
            $packageCurrency = '';
            if(!empty($offer))
            {
                ksort($offer);
                foreach($offer[$key] as $val)
                {
                    $query = $this->model->getOfferPrice($val,$key);
                    if($query)
                    {
                        $thisAmount = 0;
                        if($query[0]['currency_offer_price_pkg'] != $currency)
                            $thisAmount = $this->mydate->floatFormat($this->eucurrency->doConvert($query[0]['amt_offer_price_pkg'], $currency));
                        else
                            $thisAmount = $this->mydate->floatFormat($query[0]['amt_offer_price_pkg']);
                        $query[0]['thisAmount'] = $thisAmount;
                        $amount += $thisAmount;
                        $packageCurrency = $query[0]['currency_offer_price_pkg'];

                        if($val == '2' && !empty($amazon_eu[$key]))
                        {
                            $amazonArr = $amazon_eu[$key];
                            ksort($amazonArr);
                            $arrAmazonEU = array();
                            foreach($amazonArr as $amazonVar)
                            {
                                $query2 = $this->model->getOfferAmazonEUPrice($amazonVar,$key);
                                if($query2)
                                {
                                    $thisAmountAmazon = 0;
                                    if($query2[0]['currency_offer_price_pkg']!=$currency)
                                        $thisAmountAmazon = $this->mydate->floatFormat($this->eucurrency->doConvert($query2[0]['amt_offer_price_pkg'], $currency));
                                    else
                                        $thisAmountAmazon = $this->mydate->floatFormat($query2[0]['amt_offer_price_pkg']);
                                    $amount += $thisAmountAmazon;

                                    $query2[0]['thisAmountAmazon'] = $thisAmountAmazon;
                                    $arrAmazonEU[] = $query2[0];
                                }
                            }
                            $query[0]['arrAmazonEU'] = $arrAmazonEU;
                        }

                        $arrOffer[$key][] = $query[0];
                    }
                }
            }
            $totalAmount += $amount;
        }*/
        $totalAmount_DateDiscount=0;
        $arrOffer=array();
        foreach ($packageArr as $v) {
            $amt = $this->mydate->floatFormat($this->eucurrency->doConvert($v['amt'], $currency));
            $res =$this->model->checkPackageAvail($v['number']);
                    
            if ($res==1) {
                continue;
            }
            $temp = array(
                'id_offer_price_pkg'=>$v['number'],
                'name_offer_pkg'=>$v['name'],
                'amt_offer_price_pkg'=>$v['amt'],
                'currency_offer_price_pkg'=>$currency,
                'thisAmount'=>$amt,
                'type'=>$v['type'],
                'expired'=>$res,
                'period'=>$v['period']
            );
            
            if ($res == 0) {
                $totalAmount_DateDiscount += $amt;
            }
            if ($res == 2) {
            }
            
            $arrOffer[]=$temp;
            $totalAmount += $amt;
        }
        $subtotalAmount = $totalAmount;
        if (empty($arrOffer)) {
            redirect('service/packages', 'refresh');
        }
        /* Gift Voucher Calculate */
        $giftCode = '';
        if ($this->session->userdata('giftCode')) {
            $giftCode = $this->session->userdata('giftCode');
        }
        $giftDC = 0;
        $dcQuery = null;
        
        if (!empty($giftCode)) {
            $query = $this->model->getGiftCodeDetail($giftCode);
            if ($query) {
                $dcQuery = $query[0];
                
                if ($dcQuery['currency_giftopt'] == '%') {
                    $giftDC = $totalAmount*$dcQuery['amt_giftopt']/100;
                } else {
                    if ($currency!=$dcQuery['currency_giftopt']) {
                        $giftDC = $this->eucurrency->doConvert(
                            $dcQuery['amt_giftopt'],
                            $currency);
                    } else {
                        $giftDC = $dcQuery['amt_giftopt'];
                    }
                }
                
                if ($giftDC > 0) {
                    $totalAmount -= $giftDC;
                }
            }
        }
        /* End Gift Voucher Calculate */
        
        /* Referrals Calculate*/
        $discount = 0;
        $userID = $this->session->userdata('id');
        $discount = $this->refModel->getAvailUserCommission($userID);
        /*$queryReferral = array();
        $queryReferral[$userID]['child'] = $this->refModel->recurringUsers($userID);
        if(!empty($queryReferral[$userID]))
        {
            $discount = $this->refModel->recurringAmount($userID,$queryReferral[$userID]);
            if($currency != $this->refModel->getDefaultCurrency())
            {
                $discount = $this->eucurrency->doConvert($discount,$currency);
            }
            
            if($discount > 0)
            {
                $totalAmount -= $discount;
            }
        }*/
        $discount_date = $this->discount_date;
        
        if ($currency != $this->refModel->getDefaultCurrency()) {
            $discount = $this->eucurrency->doConvert($discount, $currency);
            $discount_date = $this->eucurrency->doConvert($discount_date, $currency);
        }
        if ($discount_date > 0) {
            $totalAmount -= $discount_date;
        }
        if ($discount > 0) {
            $totalAmount -= $discount;
        }
        if ($totalAmount<0) {
            $discount -= $totalAmount;
            $totalAmount=0;
        }
        $f_date = $this->model->get_date_first_pk();
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if (($f_date %100) >= 11 && ($f_date%100) <= 13) {
            $abbreviation = $f_date. 'th';
        } else {
            $abbreviation = $f_date. $ends[$f_date % 10];
        }
        $ch_date = $this->lang->line('preapproval_date');
        $ch_date = str_replace('{ch_date}', $abbreviation, $ch_date);
        /* End Referrals Calculate*/
        
        /* fix $totalAmount < 0 */
        if ($totalAmount < 0) {
            $totalAmount = 0;
        }
        $this->smarty->assign('discount', $discount);
        $this->smarty->assign('discount_date', $discount_date);
        $this->smarty->assign('remain_date', $this->model->get_pk_remain_day());
        $this->smarty->assign('giftDC', $giftDC);
        $this->smarty->assign('chDate', $ch_date);
        
        $this->smarty->assign('giftQuery', $dcQuery);
        $this->smarty->assign('arrOverviewOffer', $arrOffer);
        $this->smarty->assign('allTotalAmount',
            $this->mydate->floatFormat($totalAmount));
        $this->smarty->assign('allsubTotalAmount',
            $this->mydate->floatFormat($subtotalAmount));
    }
    
    private function payCredit($creArr)
    {
        $this->load->library('PayPal/dodirectpayment', $this->config_pay);
        
        $billID = $this->session->userdata('billID');
        $totalAMT = 0;
        foreach ($this->packageArr as $val) {
            $totalAMT += $val['amt'];
            if ($this->discount == 0 && $this->giftDC == 0) {
                $this->dodirectpayment->setProduct($val);
            }
        }
        
        if ($this->discount > 0 || $this->giftDC > 0) {
            $tmpTotal = $totalAMT - $this->discount - $this->giftDC;
            // check total amount if < 0
            if ($tmpTotal < 0) {
                $tmpTotal = 0;
            }
            
            $this->dodirectpayment->setProduct(array(
                'name' => 'Feed.biz Offer Package',
                'desc' => 'Feed.biz Offer Package Discount Price (Full Price '.
                $this->mydate->moneySymbol($this->config_pay['currencycode']).
                $totalAMT.')',
                'amt' => $this->mydate->floatFormat($tmpTotal),
                'number' => $billID,
                'qty' => '1',
                'taxamt' => '',
            ));
        } else {
            $tmpTotal = $totalAMT;
        }
        
        if ($tmpTotal == 0) {
            $this->session->set_userdata('paymentTransaction', array(
                'status' => true,
                'billID' => $billID,
                'errorMessage' => '',
                'paypalResult' => '',
            ));
            redirect(site_url('service/transaction'));
            return;
        }
        
        $email = $this->model->getUserEmail($this->session->userdata('id'));
        $paymentData = array(
            'cctype' => $creArr['cctype'],
            'ccnum' => $creArr['card_number'],
            'ccexp' => $creArr['exp_mm'].$creArr['exp_yy'],
            'cvv2' => $creArr['cvv'],
            'email' => $email,
            'firstname' => $creArr['first_name'],
            'lastname' => $creArr['last_name'],
            'street' => $creArr['street'],
            'city' => $creArr['city'],
            'state' => $creArr['state'],
            'countrycode' => $creArr['country'],
            'zip' => $creArr['zip'],
            'desc' => 'Feed.biz Package Payment #'.$billID,
            'currencycode' => $this->config_pay['currencycode'],
            'invnum' => $billID,
        );
        $this->dodirectpayment->setPayment($paymentData);
        $paypalResult = $this->dodirectpayment->doTransaction();
        
        if (!empty($paypalResult)) {
            unset($paypalResult['REQUESTDATA']);
            unset($paypalResult['RAWREQUEST']);
            unset($paypalResult['RAWRESPONSE']);
        }
        
        if (!empty($paypalResult['ACK']) &&
            strtolower($paypalResult['ACK']) == 'success') {
            $this->session->set_userdata('paymentTransaction', array(
                'status' => true,
                'billID' => $billID,
                'errorMessage' => '',
                'paypalResult' => $paypalResult,
            ));
            
            $creArr['ccnum'] = $this->mydate->blindString(
                strlen($creArr['card_number'])-4).
                substr($creArr['card_number'], -4);
            unset($creArr['card_number']);
            $creArr['cvv'] = $this->mydate->blindString(strlen($creArr['cvv']));
            $this->session->set_userdata('credit', $creArr);
        } else {
            if (!empty($paypalResult['ERRORS'][0]['L_LONGMESSAGE'])) {
                $err = $paypalResult['ERRORS'][0]['L_LONGMESSAGE'];
            } else {
                $err = "Transaction Error";
            }
            $this->session->set_userdata('paymentTransaction', array(
                'status' => false,
                'billID' => $billID,
                'errorMessage' => $err,
                'paypalResult' => $paypalResult,
            ));
        }
        
        redirect(site_url('service/transaction'));
        return;
    }
    
    private function payExpress()
    {
        $this->load->library('PayPal/SetExpressCheckout', $this->config_pay);
        
        $billID = $this->session->userdata('billID');
        $totalAMT = 0;
        foreach ($this->packageArr as $val) {
            $totalAMT += $val['amt'];
            if ($this->discount_date == 0 &&
                $this->discount == 0 &&
                $this->giftDC == 0) {
                $this->setexpresscheckout->setProduct($val);
            }
        }
        
        if ($this->discount_date > 0 ||
            $this->discount > 0 ||
            $this->giftDC > 0) {
            $tmpTotal = $totalAMT - $this->discount - $this->giftDC - $this->discount_date;
            // check total amount if < 0
            if ($tmpTotal < 0) {
                $tmpTotal = 0;
            }
            
            $this->setexpresscheckout->setProduct(array(
                'name' => 'Feed.biz Offer Package',
                'desc' => 'Feed.biz Offer Package Discount Price (Full Price '.
                $this->mydate->moneySymbol(
                    $this->config_pay['currencycode']).$totalAMT.')',
                'amt' => $this->mydate->floatFormat($tmpTotal),
                'number' => $billID,
                'qty' => '1',
                'taxamt' => '',
            ));
        } else {
            $tmpTotal = $totalAMT;
        }
        
        if ($tmpTotal == 0) {
            $this->session->set_userdata('paymentTransaction', array(
                'status' => true,
                'billID' => $billID,
                'errorMessage' => '',
                'paypalResult' => '',
            ));
            redirect(site_url('service/transaction'));
            return;
        }
        
        $this->setexpresscheckout->setFields(array(
            'returnurl' => site_url('service/express'),
            'cancelurl' => site_url('service/payment'),
        ));
        
        $this->setexpresscheckout->setPayment(array(
            'amt' => $this->mydate->floatFormat($tmpTotal),
            'currencycode' => $this->config_pay['currencycode'],
            'itemamt' => $this->mydate->floatFormat($tmpTotal),
            'invnum' => $billID,
            'desc' => 'Feed.biz Package Payment #'.$billID,
            'paymentaction' => 'Sale',
        ));
        $paypalResult = $this->setexpresscheckout->doTransaction();
        if ($paypalResult) {
            unset($paypalResult['REQUESTDATA']);
            unset($paypalResult['RAWREQUEST']);
            unset($paypalResult['RAWRESPONSE']);
        }
        
        if ((!empty($paypalResult['ACK']) &&
            strtolower($paypalResult['ACK']) == 'success') &&
            !empty($paypalResult['REDIRECTURL'])) {
            redirect($paypalResult['REDIRECTURL']);
            return;
        } else {
            if (!empty($paypalResult['L_LONGMESSAGE0'])) {
                $err = $paypalResult['L_LONGMESSAGE0'];
            } else {
                $err = 'Transaction Error';
            }
            $this->session->set_userdata('paymentTransaction', array(
                'status' => false,
                'billID' => $billID,
                'errorMessage' => $err,
                'paypalResult' => $paypalResult,
            ));
            redirect(site_url('service/transaction'));
            return;
        }
    }

    public function cronj_payPreapproval()
    {
        $this->load->model('paypal_model', 'model');
        $this->load->model('my_feed_model', 'my_feed');
        $pk_exp = $this->model->get_pk_zone_expired('all');
        $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
        $this->load->model('referrals_model', 'refModel');
        $this->load->library('PayPal/paypreapproval', $this->config_pay);
        $this->load->library('mydate');
                    
        $bill_list = array();
                    
                    
        foreach ($pk_exp as $user_id => $v) {
            $preapprovalProfile = $v[0];
                    
            if ($preapprovalProfile) {
                if ($preapprovalProfile['status_preapproval'] == 'active') {
                    $this->preapprovaldetails->setPreapprovalKey(
                        $preapprovalProfile['key_preapproval']);
                    $detailResult = $this->preapprovaldetails->doTransaction();

                    if ($detailResult['Ack'] == 'Success' &&
                        $detailResult['Approved'] == 'true' &&
                        $detailResult['SenderEmail'] != '' &&
                        $detailResult['Status'] == 'ACTIVE') {
                        $totalAMT = 0;
                        $this->packageArr = $v;
                        foreach ($v as $val) {
                            $totalAMT += $val['amt_offer_price_pkg'];
                        }
                            
                        $discount = $this->refModel->getAvailUserCommission($user_id);
                        if ($discount > 0) {
                            $totalAMT = $totalAMT - $discount  ;
                            if ($totalAMT<0) {
                                $discount += $totalAMT;
                            }
                            if ($totalAMT < 0) {
                                $totalAMT = 0;
                            }
                        }
                        $billID = $this->getBillID();
                        $this->session->set_userdata('billID', $billID);
                        if ($totalAMT == 0) {
                            $this->session->set_userdata('paymentTransaction', array(
                                    'status' => true,
                                    'billID' => $billID,
                                    'errorMessage' => '',
                                    'paypalResult' => '',
                                ));
                                //$this->cronj_transaction($user_id);
                                continue;
                        }

                        $this->paypreapproval->setPayRequestFields(array(
                                'Memo' => 'Feed.biz Package Payment #'.$billID,
                                'PreapprovalKey' =>
                            $preapprovalProfile['key_preapproval'],
                            ));

                        $this->paypreapproval->setReceiver(array(
                                'Amount' => $this->mydate->floatFormat($totalAMT),
                                'InvoiceID' => $billID,
                                'PaymentType' => 'DIGITALGOODS',
                            ));

                        $paypalResult = $this->paypreapproval->doTransaction();
                            //print_r($paypalResult);exit;
                            if ($paypalResult) {
                                unset($paypalResult['REQUESTDATA']);
                                unset($paypalResult['RAWREQUEST']);
                                unset($paypalResult['RAWRESPONSE']);
                            }

                        if ($paypalResult['Ack'] === 'Success' &&
                            $paypalResult['PaymentExecStatus'] === 'COMPLETED') {
                            $this->session->set_userdata('paymentTransaction', array(
                                    'status' => true,
                                    'billID' => $billID,
                                    'errorMessage' => '',
                                    'paypalResult' => $paypalResult,
                                ));
                            $bill_list[]=$billID;
                        } else {
                            $err = 'Error Transaction.';
                            if (!empty($paypalResult['Errors'][0]['Message'])) {
                                $err = $paypalResult['Errors'][0]['Message'];
                            }
                                
                            $this->session->set_userdata('paymentTransaction', array(
                                    'status' => false,
                                    'billID' => $billID,
                                    'errorMessage' => $err,
                                    'paypalResult' => $paypalResult,
                                ));
                        }
                            
                        $this->cronj_transaction($user_id);
                            //redirect(site_url('service/transaction'));
                            continue;
                    }
//                        else
//                        {
//                            $this->session->set_userdata('paymentTransaction',array(
//                                    'status' => false,
//                                    'billID' => $billID,
//                                    'errorMessage' => 'Cannot find your preapproval key, please try again.',
//                                    'paypalResult' => '',
//                                ));
//                            $this->cronj_transaction($user_id);
//                            //redirect(site_url('service/transaction'));
//                            continue;
//                        }
                }
            }
                // for non PayPal pre approval 
                if (in_array(date('j') -
                    $this->model->get_date_first_pk($user_id), array(0, 3, 5))) {
                    $this->cronj_notify_payment($user_id);
                }
        }
        if (!empty($bill_list)) {
            echo implode(', ', $bill_list).'<br>';
        }
        echo 'Done';
    }
    private function payPreapproval()
    {
        $billID = $this->session->userdata('billID');
                    
        $preapprovalProfile = $this->model->getPayment(
            $this->session->userdata('id'));
                    
        if ($preapprovalProfile) {
            if ($preapprovalProfile['status_preapproval'] == 'active') {
                $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
                $this->preapprovaldetails->setPreapprovalKey($preapprovalProfile['key_preapproval']);
                $detailResult = $this->preapprovaldetails->doTransaction();
                    
                if ($detailResult['Ack'] == 'Success' &&
                    $detailResult['Approved'] == 'true' &&
                    $detailResult['SenderEmail'] != '' &&
                    $detailResult['Status'] == 'ACTIVE') {
                    $totalAMT = 0;
                    foreach ($this->packageArr as $val) {
                        $totalAMT += $val['amt'];
                    }
                    
                    if ($this->discount > 0 ||
                        $this->giftDC > 0 ||
                        $this->discount_date) {
                        $tmpTotal = $totalAMT - $this->discount - $this->giftDC - $this->discount_date;
                        if ($totalAMT < 0) {
                            $totalAMT = 0;
                        }
                    }

                    if ($totalAMT == 0) {
                        $this->session->set_userdata('paymentTransaction', array(
                            'status' => true,
                            'billID' => $billID,
                            'errorMessage' => '',
                            'paypalResult' => '',
                        ));
                        redirect(site_url('service/transaction'));
                        return;
                    }
                    
                    $this->load->library('PayPal/paypreapproval', $this->config_pay);
                    $this->paypreapproval->setPayRequestFields(array(
                        'Memo' => 'Feed.biz Package Payment #'.$billID,
                        'PreapprovalKey' => $preapprovalProfile['key_preapproval'],
                    ));
                    
                    $this->paypreapproval->setReceiver(array(
                        'Amount' => $this->mydate->floatFormat($totalAMT),
                        'InvoiceID' => $billID,
                        'PaymentType' => 'DIGITALGOODS',
                    ));
                    
                    $paypalResult = $this->paypreapproval->doTransaction();
                    //print_r($paypalResult);exit;
                    if ($paypalResult) {
                        unset($paypalResult['REQUESTDATA']);
                        unset($paypalResult['RAWREQUEST']);
                        unset($paypalResult['RAWRESPONSE']);
                    }
                    
                    if ($paypalResult['Ack'] === 'Success' &&
                        $paypalResult['PaymentExecStatus'] === 'COMPLETED') {
                        $this->session->set_userdata('paymentTransaction', array(
                            'status' => true,
                            'billID' => $billID,
                            'errorMessage' => '',
                            'paypalResult' => $paypalResult,
                        ));
                    } else {
                        $err = 'Error Transaction.';
                        if (!empty($paypalResult['Errors'][0]['Message'])) {
                            $err = $paypalResult['Errors'][0]['Message'];
                        }
                        
                        $this->session->set_userdata('paymentTransaction', array(
                            'status' => false,
                            'billID' => $billID,
                            'errorMessage' => $err,
                            'paypalResult' => $paypalResult,
                        ));
                    }
                    
                    redirect(site_url('service/transaction'));
                    return;
                }
                if ($detailResult['Ack'] == 'Success' &&
                    $detailResult['Approved'] == 'true' &&
                    $detailResult['SenderEmail'] != '' &&
                    $detailResult['Status'] == 'CANCELED') {
                    redirect(site_url('billing/configuration'));
                } else {
                    $this->session->set_userdata('paymentTransaction', array(
                            'status' => false,
                            'billID' => $billID,
                            'errorMessage' => 'Cannot find your preapproval key, please try again.',
                            'paypalResult' => '',
                        ));
                    
                    redirect(site_url('service/transaction'));
                    return;
                }
            } else {
                redirect(site_url('billing/configuration'));
                return;
            }
        } else {
            $this->session->set_userdata('preapprovalStatus', true);
            redirect(site_url('billing/preapproval/agreement'));
            return;
        }
    }
    
    public function express()
    {
        $this->checkAuthen();
        $billID = $this->session->userdata('billID');
        
        if ($this->input->get('token')) {
            $this->load->model('paypal_model', 'model');
            $this->load->library('mydate');
            $this->load->library('PayPal/GetExpressCheckoutDetails',
                $this->config_pay);
            
            $token = $this->input->get('token');
            
            $this->getexpresscheckoutdetails->setToken($token);
            $detailResult = $this->getexpresscheckoutdetails->doTransaction();
            
            if (!empty($detailResult)) {
                unset($detailResult['REQUESTDATA']);
                unset($detailResult['RAWREQUEST']);
                unset($detailResult['RAWRESPONSE']);
            }
            
            if (!empty($detailResult) &&
                strtolower($detailResult['ACK']) === 'success') {
                $this->load->library('PayPal/DoExpressCheckoutPayment',
                    $this->config_pay);
                
                $this->doexpresscheckoutpayment->setPaymentAction('Sale');
                $this->doexpresscheckoutpayment->setDECPFields($detailResult);
                $paypalResult = $this->doexpresscheckoutpayment->doTransaction();
                
                if (!empty($paypalResult)) {
                    unset($paypalResult['REQUESTDATA']);
                    unset($paypalResult['RAWREQUEST']);
                    unset($paypalResult['RAWRESPONSE']);
                }
                
                if ((!empty($paypalResult['ACK']) &&
                    strtolower($paypalResult['ACK']) === 'success') &&
                        (!empty($paypalResult['PAYMENTINFO_0_ACK']) &&
                        strtolower($paypalResult['PAYMENTINFO_0_ACK']) === 'success')) {
                    $this->session->set_userdata('paymentTransaction', array(
                        'status' => true,
                        'billID' => $billID,
                        'errorMessage' => '',
                        'paypalResult' => $paypalResult,
                    ));
                    redirect(site_url('service/transaction'));
                    return;
                } else {
                    if (!empty($paypalResult['ERRORS'][0]['L_LONGMESSAGE'])) {
                        $err = $paypalResult['ERRORS'][0]['L_LONGMESSAGE'];
                    } else {
                        $err = 'Transaction Error';
                    }
                    $this->session->set_userdata('paymentTransaction', array(
                        'status' => false,
                        'billID' => $billID,
                        'errorMessage' => $err,
                        'paypalResult' => $paypalResult,
                    ));
                    redirect(site_url('service/transaction'));
                    return;
                }
            } else {
                if (!empty($detailResult['ERRORS'][0]['L_LONGMESSAGE'])) {
                    $err = $detailResult['ERRORS'][0]['L_LONGMESSAGE'];
                } else {
                    $err = 'Transaction Error';
                }
                $this->session->set_userdata('paymentTransaction', array(
                    'status' => false,
                    'billID' => $billID,
                    'errorMessage' => $err,
                    'paypalResult' => $detailResult,
                ));
                redirect(site_url('service/transaction'));
                return;
            }
        } else {
            redirect(site_url('service/payment'));
            return;
        }
    }
    private function cronj_transaction($user_id=0)
    {
        $this->load->model('paypal_model', 'model');
        $this->load->model('referrals_model', 'refModel');
        $this->load->library('Cipher', array(
            'key'=>$this->config->item('encryption_key')));
        $this->load->library('mydate');
            
        $pk_list = $this->packageArr;
        $packageArr = array();
        foreach ($pk_list as $v) {
            $amount = $this->mydate->floatFormat($v['amt_offer_price_pkg']);
            if ($v['offer_pkg_type'] == 3) {
                $url = $v['domain_offer_sub_pkg'];
                $packageArr[] = array(
                                        'name' => $v['name_offer_pkg'].' '.
                    $v['title_offer_sub_pkg'].' ('.$url .') ',
                                        'desc' => $v['name_offer_pkg'].' '.
                    $v['title_offer_sub_pkg'].' ('.$url .') , '.
                    $this->mydate->moneySymbol($v['currency_offer_price_pkg']).
                    $amount.'/'.$v['priod_offer_price_pkg'].'  ',
                                        'amt' => $amount,
                                        'number' => $v['id_offer_price_pkg'],
                                        'qty' => '1',
                                        'taxamt' => '',
                                        'type'=>'sub'

                                    );
            } elseif ($v['id_region']=='') {
                $packageArr[] = array(
                            'name' => $v['name_offer_pkg'].' Offer Package',
                            //'desc' => $query[0]['name_offer_pkg'].' Offer Package, '.$mydate->moneySymbol($query[0]['currency_offer_price_pkg']).$amount.'/'.$query[0]['priod_offer_price_pkg'].' ('.$session_region[$query[0]['id_region']].' Region)',
                            'desc' => $v['name_offer_pkg'].' Offer Package, '.
                    $this->mydate->moneySymbol($v['currency_offer_price_pkg']).
                    $amount.'/'.$v['priod_offer_price_pkg'].' (Base Package)',
                            'amt' => $amount,
                            'number' => $v['id_offer_price_pkg'],
                            'qty' => '1',
                            'taxamt' => '',
                            'type'=>'main'
                        );
            } else {
                $url = $v['domain_offer_sub_pkg'];
                $packageArr[] = array(
                                        'name' => $v['name_offer_pkg'].' '.
                    $v['title_offer_sub_pkg'].' ('.$url .') Package',
                                        'desc' => $v['name_offer_pkg'].' '.
                    $v['title_offer_sub_pkg'].' ('.$url .') Package, '.
                    $this->mydate->moneySymbol($v['currency_offer_price_pkg']).
                    $amount.'/'.$v['priod_offer_price_pkg'].'  ',
                                        'amt' => $amount,
                                        'number' => $v['id_offer_price_pkg'],
                                        'qty' => '1',
                                        'taxamt' => '',
                                        'type'=>'sub'

                                    );
            }
        }
        if ($this->session->userdata('billID') &&
            !empty($packageArr) &&
            $this->session->userdata('paymentTransaction') &&
            $user_id != 0) {
            $userPkg = $this->model->getUserPackage($user_id);
            if (!empty($userPkg)) {
                $paymentTransaction =
                    (array)$this->session->userdata('paymentTransaction');
                if ($paymentTransaction['status']) {
                    if (!$this->model->checkBillID(
                        $paymentTransaction['billID'])) {
                        $billID = $paymentTransaction['billID'];
                    } else {
                        $billID = $this->getBillID();
                    }

                    $totalAMT = 0;
                    $dbPackageArr = array();
                    $next_charge_date = $this->model->get_pk_next_charge($user_id);

                    foreach ($packageArr as $field) {
                        $dbPackageArr[] = array(
                                'id_billing' => $paymentTransaction['billID'],
                                'id_package' => $field['number'],
                                'option_package' => 'offer',
                                'id_user' => $user_id,
                                'start_date_bill_detail' => date('Y-m-d'),
                                'end_date_bill_detail' => $next_charge_date,
                                'package_detail' => $this->cipher->encrypt(serialize($field)),
                                'amount_bill_detail' => $this->mydate->floatFormat($field['amt']),
                                'created_date_bill_detail' => date('Y-m-d H:i:s'),
                            );
                        $totalAMT += $field['amt'];
                    }

                    
                    $discount = 0;
                    $userID = $user_id;
                    $discount = ($this->refModel->getAvailUserCommission($userID));
                        
                    if ($totalAMT-$discount <0) {
                        $tmpTotal =$totalAMT-$discount ;
                        $discount += $tmpTotal;
                    }
                    $discountBillIDArr=$this->refModel->getCommList($userID, $discount);
        
                        /*
                        * End Referral Discount
                        */

                        $paypalResult = $paymentTransaction['paypalResult'];
                    if ($this->session->userdata('opt') == 'credit') {
                        $paypalResult['credit'] = (array)$this->session->userdata('credit');
                    }

                    $dataArr = array(
                            'id_billing' => $paymentTransaction['billID'],
                            'id_users' => $userID,
                            'amt_billing' => $this->mydate->floatFormat($totalAMT),
                    
                            'itemamt_billing' => $this->mydate->floatFormat($totalAMT),
                            'itemtax_billing' => '0',
                            'currency_billing' => $this->config_pay['currencycode'],
                            'company_billing' => $userPkg[0]['company_upkg'],
                            'first_name_billing' => $userPkg[0]['firstname_upkg'],
                            'last_name_billing' => $userPkg[0]['lastname_upkg'],
                            'complement_billing' => $userPkg[0]['company_upkg'],
                            'address1_billing' => $userPkg[0]['address1_upkg'],
                            'address2_billing' => $userPkg[0]['address2_upkg'],
                            'stateregion_billing' => $userPkg[0]['state_upkg'],
                            'zipcode_billing' => $userPkg[0]['zip_upkg'],
                            'city_billing' => $userPkg[0]['city_upkg'],
                            'country_billing' => $userPkg[0]['country_upkg'],
                            'status_billing' => 'active',
                            'created_date_billing' => date('Y-m-d H:i:s'),
                            'payment_method_billing' => 'preapproval',
                            'payment_log_billing' => $this->cipher->encrypt(
                                serialize($paypalResult)),
                            'cronjob_flag'=>'yes',
                        );

                        /* Log Discount */
                        $arrDCLog = array();
                    $arrDCCmd = array();

                        /* Referrals DC */
                        if ($discount > 0) {
                            $arrDCLog['referrals'] = $discountBillIDArr;
                            $arrDCCmd['referrals'] = array(
                                'param' => 'referrals',
                                'dc_amt' => $this->mydate->floatFormat($discount),
                            );
                        }
                        /* End Referrals DC */
                    
                        /* Total DC */
                        if ($discount > 0) {
                            $tmpFull = $totalAMT - $discount  ;
                            $tmpTotalDC = $discount ;
                            if ($tmpFull <= 0) {
                                $tmpFull = 0;
                            }
                            if ($tmpTotalDC > $totalAMT) {
                                $tmpTotalDC = $totalAMT;
                            }
                            $arrDCCmd['total'] = array(
                                'param' => 'total',
                                'full_amt' => $this->mydate->floatFormat($totalAMT),
                                'dc_amt' => $this->mydate->floatFormat($tmpTotalDC),
                                'total_amt' => $this->mydate->floatFormat($tmpFull),
                            );
                            $dataArr['amt_total_billing'] = $this->mydate->floatFormat($tmpFull);
                        }
                        /* End Total ... */
                        /* End Log ...*/

                        if (!empty($arrDCLog)) {
                            $dataArr['discount_log_billing'] = serialize($arrDCLog);
                            $dataArr['discount_cmd_billing'] = serialize($arrDCCmd);
                        }
                    if (!empty($dbPackageArr)) {
                        if ($this->model->setBilling($dataArr)) {
                            if (!empty($dbPackageArr)) {
                                foreach ($dbPackageArr as $val) {
                                    $this->model->setBillingDetail($val);
                                }
                            }

                            $this->session->set_userdata('paymentComplete', array(
                                'status' => true,
                                'billID' => $paymentTransaction['billID'],
                            ));
                            //$user_id = $data['userData']['id'];
                             $bill_id = $paymentTransaction['billID'];
                            $curr = $this->config_pay['currencycode'];
                            if ($discount>0) {
                                $this->refModel->payoutSpreadCommission($user_id,
                                    $discount, $bill_id, $curr);
                                //spend money from child
                            }
                            
                            //manage commission for parents.
                            $tmpFull = $totalAMT - $discount  ;
                            if ($tmpFull <= 0) {
                                $tmpFull = 0;
                            }
                            $this->refModel->recurringSpreadCommission($user_id,
                                $tmpFull, $bill_id, $curr);
                        //email report payment;
                            $this->cronj_report_payment($user_id, $bill_id);
                        }

                        
                        return;
                    }
                }
            }
        }
        return;
    }
    
    public function transaction()
    {
        $data = $this->checkAuthen();
         
        if ($this->session->userdata('pay')) {
            $this->load->model('paypal_model', 'model');
            $this->load->model('referrals_model', 'refModel');
            $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));
            $this->load->library('mydate');

            $pay = (array)$this->session->userdata('pay');
                    
            if (isset($pay['offer']) && !empty($pay['offer'])) {
                foreach ($pay['offer'] as $key => $val) {
                    $tmpQuery = $this->model->getRegionByID($key);
                    $pay['region'][$tmpQuery[0]['id_region']] = $tmpQuery[0]['name_region'];
                    break;
                }
            }
       
        
            $packageArr = $this->model->packageZoneList($pay);
//            $packageArr = $this->model->packageList($pay['offer'],$pay['region'],$pay['sub']);

            if ($this->session->userdata('billID') &&
                !empty($packageArr) &&
                $this->session->userdata('paymentTransaction') &&
                    $this->session->userdata('opt') && !empty($data['userData'])) {
                $userPkg = $this->model->getUserPackage($data['userData']['id']);
//                 
//                if(!empty($userPkg))
//                {
                    $paymentTransaction = (array)$this->session->userdata('paymentTransaction');
                if ($paymentTransaction['status']) {
                    if (!$this->model->checkBillID($paymentTransaction['billID'])) {
                        $billID = $paymentTransaction['billID'];
                    } else {
                        $billID = $this->getBillID();
                    }

                    $totalAMT = 0;
                    $dbPackageArr = array();
                    $next_charge_date = $this->model->get_pk_next_charge();
                    $totalAmount_DateDiscount = 0;
                    foreach ($packageArr as $field) {
                        $dbPackageArr[] = array(
                                'id_billing' => $paymentTransaction['billID'],
                                'id_package' => $field['number'],
                                'option_package' => 'offer',
                                'id_user' => $data['userData']['id'],
                                'start_date_bill_detail' => date('Y-m-d'),
                                'end_date_bill_detail' => $next_charge_date,
                                'package_detail' => $this->cipher->encrypt(serialize($field)),
                                'amount_bill_detail' => $this->mydate->floatFormat($field['amt']),
                                'created_date_bill_detail' => date('Y-m-d H:i:s'),
                            );
                        $totalAMT += $field['amt'];
                        if ($this->model->checkPackageAvail($field['number']) == 0) {
                            $totalAmount_DateDiscount += $field['amt'];
                        }
                    }

                    $this->discount_date_percent = $this->model->get_percent_discount();
                    $this->discount_date = $this->mydate->floatFormat(
                        $totalAmount_DateDiscount *
                        floor($this->discount_date_percent)*0.01);
                    $discount_date = $this->discount_date;

                        /*
                        * Gift Code Check
                        */
                        $giftDC = 0;
                    if ($this->session->userdata('giftCode')) {
                        $giftCode = $this->session->userdata('giftCode');
                        $query = $this->model->getGiftCodeDetail($giftCode);
                            
                        if ($query) {
                            if ($query[0]['currency_giftopt'] == '%') {
                                $tmpTotal = $totalAMT;
                                $giftDC = $tmpTotal*$query[0]['amt_giftopt']/100;
                            } else {
                                $giftDC = $query[0]['amt_giftopt'];
                            }
                        }
                    }
                        /*
                        * End Gift Code
                        */
                        
                        /*
                        * Referral Discount
                        */
                        $discount = 0;
//                        $queryReferral = array();
//                        $queryReferral[$data['userData']['id']]['child'] = $this->refModel->recurringUsers($data['userData']['id']);
//                        if(!empty($queryReferral[$data['userData']['id']]))
//                        {
//                            $discount = $this->refModel->recurringAmount($data['userData']['id'],$queryReferral[$data['userData']['id']]);
//                        }
//                        $discountBillIDArr = $this->refModel->getBillingList();

                        $userID = $data['userData']['id'];
                    $discount = $this->refModel->getAvailUserCommission($userID);
                        
                    if ($totalAMT-$discount-$giftDC-$discount_date <0) {
                        $tmpTotal =$totalAMT-$discount-$giftDC-$discount_date;
                        $discount += $tmpTotal;
                    }
                        
                    $discountBillIDArr=$this->refModel->getCommList($userID, $discount);
        
                        /*
                        * End Referral Discount
                        */

                        $paypalResult = $paymentTransaction['paypalResult'];
                    if ($this->session->userdata('opt') == 'credit') {
                        $paypalResult['credit'] = (array)$this->session->userdata('credit');
                    }

                    $dataArr = array(
                            'id_billing' => $paymentTransaction['billID'],
                            'id_users' => $data['userData']['id'],
                            'amt_billing' => $this->mydate->floatFormat($totalAMT),
                            'itemamt_billing' => $this->mydate->floatFormat($totalAMT),
                            'itemtax_billing' => '0',
                            'currency_billing' => $this->config_pay['currencycode'],
                            'company_billing' => isset($userPkg[0]['company_upkg'])?$userPkg[0]['company_upkg']:'',
                            'first_name_billing' => isset($userPkg[0]['firstname_upkg'])?$userPkg[0]['firstname_upkg']:'',
                            'last_name_billing' => isset($userPkg[0]['lastname_upkg'])?$userPkg[0]['lastname_upkg']:'',
                            'complement_billing' => isset($userPkg[0]['company_upkg'])?$userPkg[0]['company_upkg']:'',
                            'address1_billing' => isset($userPkg[0]['address1_upkg'])?$userPkg[0]['address1_upkg']:'',
                            'address2_billing' => isset($userPkg[0]['address2_upkg'])?$userPkg[0]['address2_upkg']:'',
                            'stateregion_billing' => isset($userPkg[0]['state_upkg'])?$userPkg[0]['state_upkg']:'',
                            'zipcode_billing' => isset($userPkg[0]['zip_upkg'])?$userPkg[0]['zip_upkg']:'',
                            'city_billing' => isset($userPkg[0]['city_upkg'])?$userPkg[0]['city_upkg']:'',
                            'country_billing' => isset($userPkg[0]['country_upkg'])?$userPkg[0]['country_upkg']:'',
                            'status_billing' => 'active',
                            'created_date_billing' => date('Y-m-d H:i:s'),
                            'payment_method_billing' => $this->session->userdata('opt'),
                            'payment_log_billing' => $this->cipher->encrypt(serialize($paypalResult)),
                        );

                        /* Log Discount */
                        $arrDCLog = array();
                    $arrDCCmd = array();

                        /* Referrals DC */
                        if ($discount > 0) {
                            $arrDCLog['referrals'] = $discountBillIDArr;
                            $arrDCCmd['referrals'] = array(
                                'param' => 'referrals',
                                'dc_amt' => $this->mydate->floatFormat($discount),
                            );
                        }
                        /* End Referrals DC */
                        
                        if ($discount_date>0) {
                            $remain_day = $this->model->get_pk_remain_day();
                            $arrDCLog['discount_date'] = 'remain day until next charge:'.$remain_day;
                            $arrDCCmd['discount_date'] = array(
                                'param' => 'discount_date',
                                'dc_amt' => $this->mydate->floatFormat($discount_date),
                            );
                        }
                        
                        /* Gift Voucher DC */
                        if ($giftDC > 0) {
                            $arrDCLog['gift'] = $giftCode;
                            $arrDCCmd['gift'] = array(
                                'param' => 'gift',
                                'dc_amt' => $this->mydate->floatFormat($giftDC),
                            );

                            /* Update Gift Voucher Code */
                            $this->model->inactiveGiftCode($giftCode);
                            /* End Update */
                        }
                        /* End Gift Voucher DC */


                        /* Total DC */
                        if ($discount_date > 0 || $discount > 0 || $giftDC > 0) {
                            $tmpFull = $totalAMT - $discount - $giftDC-$discount_date;
                            $tmpTotalDC = $discount + $giftDC+$discount_date;
                            if ($tmpFull <= 0) {
                                $tmpFull = 0;
                            }
                            if ($tmpTotalDC > $totalAMT) {
                                $tmpTotalDC = $totalAMT;
                            }
                            $arrDCCmd['total'] = array(
                                'param' => 'total',
                                'full_amt' => $this->mydate->floatFormat($totalAMT),
                                'dc_amt' => $this->mydate->floatFormat($tmpTotalDC),
                                'total_amt' => $this->mydate->floatFormat($tmpFull),
                            );
                            $dataArr['amt_total_billing'] = $this->mydate->floatFormat($tmpFull);
                        }
                        /* End Total ... */
                        /* End Log ...*/

                        if (!empty($arrDCLog)) {
                            $dataArr['discount_log_billing'] = serialize($arrDCLog);
                            $dataArr['discount_cmd_billing'] = serialize($arrDCCmd);
                        }
                    if (!empty($dbPackageArr)) {
                        if ($this->model->setBilling($dataArr)) {
                            if (!empty($dbPackageArr)) {
                                foreach ($dbPackageArr as $val) {
                                    $this->model->setBillingDetail($val);
                                }
                            }

                            $this->session->set_userdata('paymentComplete', array(
                                    'status' => true,
                                    'billID' => $paymentTransaction['billID'],
                                ));
                            $user_id = $data['userData']['id'];
                            $bill_id = $paymentTransaction['billID'];
                            $curr = $this->config_pay['currencycode'];
                            if ($discount>0) {
                                $this->refModel->payoutSpreadCommission(
                                    $user_id, $discount, $bill_id, $curr);
                                    //spend money from child
                            }

                                //manage commission for parents.
                                $tmpFull = $totalAMT - $discount - $giftDC;
                            if ($tmpFull <= 0) {
                                $tmpFull = 0;
                            }
                            $this->refModel->recurringSpreadCommission(
                                $user_id, $tmpFull, $bill_id, $curr);
                                /* Unset Session*/
                                if ($this->session->userdata('region')) {
                                    $this->session->unset_userdata('region');
                                }
                            if ($this->session->userdata('currency')) {
                                $this->session->unset_userdata('currency');
                            }
                            if ($this->session->userdata('region_sel')) {
                                $this->session->unset_userdata('region_sel');
                            }
                            if ($this->session->userdata('offer')) {
                                $this->session->unset_userdata('offer');
                            }
                            if ($this->session->userdata('amazon_eu')) {
                                $this->session->unset_userdata('amazon_eu');
                            }
                            if ($this->session->userdata('giftCode')) {
                                $this->session->unset_userdata('giftCode');
                            }
                            if ($this->session->userdata('info')) {
                                $this->session->unset_userdata('info');
                            }
                            if ($this->session->userdata('billID')) {
                                $this->session->unset_userdata('billID');
                            }
                            if ($this->session->userdata('opt')) {
                                $this->session->unset_userdata('opt');
                            }
                            if ($this->session->userdata('paymentTransaction')) {
                                $this->session->unset_userdata('paymentTransaction');
                            }
                            if ($this->session->userdata('credit')) {
                                $this->session->unset_userdata('credit');
                            }
                            if ($this->session->userdata('pay')) {
                                $this->session->unset_userdata('pay');
                            }
                                /* End Unset */
                        }
                            
                        redirect(site_url('service/paymentComplete'));
                        return;
                    } else {
                        redirect(site_url('service/payment'));
                        return;
                    }
                } else {
                    $this->session->set_userdata('paymentError',
                        $paymentTransaction['errorMessage']);
                    $this->session->unset_userdata('paymentTransaction');
                    redirect(site_url('service/payment'));
                    return;
                }
//                }
            } else {
                redirect(site_url('service/payment'));
                return;
            }
            //echo 'not normal';
            //exit;
        }
    }
    
    public function complete()
    {
        $data = $this->checkAuthen();
        
        if ($this->session->userdata('offer') &&
            /*$this->session->userdata('region') &&*/
            $this->session->userdata('info')) {
            $this->load->model('paypal_model', 'model');
            $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));
            $this->load->library('mydate');
            $offer = $this->session->userdata('offer');
            /*
            * Breadcrumb
            */
            $this->breadcrumb->clear();
            $this->breadcrumb->add_crumb('Billing');
            $this->breadcrumb->add_crumb('Complete');
            $this->breadcrumb->change_link('<span class="divider"><i class="icon-angle-right arrow-icon"></i></span>');
            $this->smarty->assign("breadcrumb", $this->breadcrumb->output());
            /*
            * End Breadcrumb
            */

            /*
            * Save Package
            */
            $packageArr = $this->model->packageList($this->session->userdata('offer'), $this->session->userdata('region'), $this->session->userdata('amazon_eu'));
            $infoArr = (array)$this->session->userdata('info');

            $arrUserPkg = array(
                'id_users' => $data['userData']['id'],
                'company_upkg' => $infoArr['complement'],
                'firstname_upkg' => $infoArr['firstname'],
                'lastname_upkg' => $infoArr['lastname'],
                'address1_upkg' => $infoArr['address1'],
                'address2_upkg' => $infoArr['address2'],
                'state_upkg' => $infoArr['stateregion'],
                'city_upkg' => $infoArr['city'],
                'country_upkg' => $infoArr['country'],
                'zip_upkg' => $infoArr['zipcode'],
                'status_upkg' => 'active',
                'created_date_upkg' => date('Y-m-d H:i:s'),
            );
            $this->model->setUserPackage($arrUserPkg);

//            if(!empty($packageArr))
//            {
//                foreach($packageArr as $val)
//                {
//                    $arrUserPkgDetail = array(
//                        'id_users' => $data['userData']['id'],
//                        'id_package' => $val['number'],
//                        'option_package' => 'offer',
//                        'created_date_upkg_detail' => date('Y-m-d H:i:s'),
//                    );
//                    $this->model->setUserPackageDetail($arrUserPkgDetail);
//                }
//            } 

            
            $this->model->cleanUserPackageDetail($data['userData']['id']);
            foreach ($offer as $k =>$v) {
                $main_pk_id = $k;
                foreach ($v as $pk_id) {
                    $arrUserPkgDetail = array(
                        'id_users' => $data['userData']['id'],
                        'id_package' => $pk_id,
                        'option_package' => 'offer',
                        'created_date_upkg_detail' => date('Y-m-d H:i:s'),
                    );
                    $this->model->setUserPackageDetail($arrUserPkgDetail);
                }
            }
            
            /*
            * End Save Package
            */
            $this->session->unset_userdata('offer');
            $this->smarty->assign('siteurl', site_url());

            $this->smarty->view('service/complete.tpl', $data);
        } else {
            if ($this->session->userdata('billID')) {
                $this->session->unset_userdata('billID');
            }
            
            redirect(site_url('service/information'));
            return;
        }
    }
    
    public function paymentComplete()
    {
        $this->load->model('referrals_model', 'refModel');
       
        $data = $this->checkAuthen();
        $popup = $this->session->userdata('popup_payment');
        $paymentComplete = (array)$this->session->userdata('paymentComplete');
        
        if (!empty($paymentComplete) && $paymentComplete['status'] == '1') {
            $this->load->library('mydate');
                    
            $this->smarty->assign('return2package', ''.$this->session->userdata('payment_return_back'));
                    
            $this->smarty->assign('billID', $paymentComplete['billID']);
            $this->smarty->assign('siteurl', site_url());
            $this->smarty->assign('popup', $popup);
            $this->smarty->view('service/paymentComplete.tpl', $data);
        }
    }


    public function finish()
    {
        $this->checkAuthen();
        
        if ($this->session->userdata('paymentComplete')) {
            $this->session->unset_userdata('paymentComplete');
            if ($this->session->userdata('billID')) {
                $this->session->unset_userdata('billID');
            }
        }
        
        redirect(site_url('billing/download'));
        return;
    }
    
    /* demo function delete it */
    public function exportSample()
    {
        $data = $this->checkAuthen();
        
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        
        $query = $this->model->getAllUserPackageID($data['userData']['id']);
        if ($query) {
            foreach ($query as $val) {
                $sub='';
                if (!empty($val['title_offer_sub_pkg']) && $val['title_offer_sub_pkg'] !='') {
                    $sub = '('.$val['title_offer_sub_pkg'].')';
                    echo '<a href="'.site_url('service/sessionSample/?id='.$val['id_offer_price_pkg']).'">('.$val['id_region'].')'.$val['name_offer_pkg'].$sub.'</a><br>';
                }
            }
        }
        //$this->mydate->debugging($query);
    }
    public function paybillall()
    {
        $data = $this->checkAuthen();
        $isPackage=true;
        $this->session->unset_userdata('popup_payment');
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $payPackage = array();
        $pk_exp = $this->model->get_pk_zone_expired();
                    
        foreach ($pk_exp[$data['userData']['id']] as $pk) {
            if ($pk['id_region']!='') {
                $payPackage['pay']['offer'][$pk['id_region']][$pk['id_offer_pkg']] = $pk['id_offer_pkg'];
                $payPackage['pay']['sub'][$pk['id_region']][$pk['id_offer_sub_pkg']] = $pk['id_offer_price_pkg'];
            } else {
                if ($pk['offer_pkg_type']=='9') {
                    $payPackage['pay']['base'] = $pk['id_offer_price_pkg'];
                }
            }
        }
        if (!empty($payPackage)) {
            $this->session->set_userdata('pay', $payPackage['pay']);
            redirect(site_url('service/payment/extend'));
        } else {
            $this->session->unset_userdata('pay');
            redirect(site_url('service/packages'));
        }
    }
    public function package_payment($site, $ext)
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $uri =  $_SERVER['HTTP_REFERER'];
            $this->session->set_userdata('payment_return_back', $uri);
        }
        $this->load->model('paypal_model', 'model');
        $pk_id = $this->model->get_pk_zone_by_site($site, $ext);
                    
        $this->sessionSample(isset($pk_id['id'])?$pk_id['id']:false, false);
    }
    public function sessionSample($id='', $popup = false)
    {
        $data = $this->checkAuthen();
       
        $isPackage=true;
        $this->session->unset_userdata('popup_payment');
        if ($popup) {
            $popup = 'popup';
            $this->session->set_userdata('popup_payment', $popup);
            $url_add = '/popup';
        }
        
        $this->load->model('paypal_model', 'model');
        $this->load->library('mydate');
        $payPackage = array();
        if ($id=='' && $this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($id!='') {
            if (!isset($payPackage['pay'])) {
                $payPackage['pay']=array();
            }
            if ($this->model->checkPackageAvail(service::BASE_PK_ID)!=1) {
                $payPackage['pay']['base'] = service::BASE_PK_ID;
            }
            if (strtolower($id)=='all') {
                $pk_exp = $this->model->get_pk_zone_expired();
                foreach ($pk_exp[$data['userData']['id']] as $pk) {
                    if ($pk['id_region']!='') {
                        $payPackage['pay']['offer'][$pk['id_region']][$pk['id_offer_pkg']] = $pk['id_offer_pkg'];
                        $payPackage['pay']['sub'][$pk['id_region']][$pk['id_offer_sub_pkg']] = $pk['id_offer_price_pkg'];
                    }
                }
                                
                if (!empty($payPackage)) {
                    $this->session->set_userdata('pay', $payPackage['pay']);
                    redirect(site_url('service/payment'));
                } else {
                    $this->session->unset_userdata('pay');
                    redirect(site_url('service/exportSample'));
                }
            } else {
                if ($this->model->checkPackageAvail($id)== 1) {
                    $id = $this->model->getBasePackageID($id);
                }
                    
                $checkBilling = $this->model->getBillingFromDate($id, date('Y-m-d H:i:s'));
                
                if (!empty($checkBilling)) {
                    $queryBilling = $this->model->getBilling($checkBilling[0]['id_billing']);
                    if ($queryBilling['status_billing']=='active') {
                        $isPackage = false;
                    }
                }
                
                    
                
                if ($isPackage) {
                    $arrUserPkgDetail = array(
                        'id_users' => $data['userData']['id'],
                        'id_package' => $id,
                        'option_package' => 'product',
                        'created_date_upkg_detail' => date('Y-m-d H:i:s'),
                    );
                    $this->model->setUserPackageDetail($arrUserPkgDetail);
                    
                    $query = $this->model->getUserPackageZoneByID($data['userData']['id'], $id);
                    if ($query) {
                        $payPackage['pay'][$query[0]['option_package']][$query[0]['id_region']][$query[0]['id_offer_pkg']] = $query[0]['id_offer_pkg'];

                        if (!empty($query[0]['id_offer_sub_pkg'])) {
                            //$payPackage['pay'][$query[0]['element_offer_sub_pkg']][$query[0]['id_region']][$query[0]['id_offer_sub_pkg']] = $query[0]['id_offer_sub_pkg'];
                            $payPackage['pay']['sub'][$query[0]['id_region']][$query[0]['id_offer_sub_pkg']] = $query[0]['id_offer_price_pkg'];
                        } else {
                            $payPackage['pay']['sub'][$query[0]['id_region']][] = $query[0]['id_offer_price_pkg'];
                        }
                    
                    

                        $this->session->set_userdata('pay', $payPackage['pay']);

                        redirect(site_url('service/payment'));
                        
                        return;
                    } else {
                        $query = $this->model->getUserPackageZoneByID($data['userData']['id'], $id, true);
                        $payPackage['pay']['offer']['us'][$query[0]['id_offer_pkg']] = $query[0]['id_offer_pkg'];
                        $this->session->set_userdata('pay', $payPackage['pay']);
                        redirect(site_url('service/payment'));
                    }
                } else {
                    if (isset($payPackage['pay']) && !empty($payPackage['pay'])) {
                        $this->session->set_userdata('pay', $payPackage['pay']);
                        redirect(site_url('service/payment'));
                        return;
                    } else {
                        redirect(site_url('service/packages'));
                        return;
                    }
                }
            }
        } else {
            $this->session->unset_userdata('pay');
            redirect(site_url('service/exportSample'));
            return;
        }
    }
    
    /* end demo */
    
    private function getBillID()
    {
        do {
            $billID = strtoupper('i-'.$this->mydate->generateRandomString(8));
        } while ($this->model->checkBillID($billID));
        
        return $billID;
    }
    
    private function checkAuthen()
    {
        $data = array();
        if ($this->authentication->logged_in() && isset($this->session->userdata['id'])) {
            $data['userData'] = (array)$this->authentication->user($this->session->userdata['id'])->row();
            return $data;
        } else {
            $_SERVER['QUERY_STRING'] = $_SERVER['QUERY_STRING']==''?'':'?'.$_SERVER['QUERY_STRING'];
            $uri = current_url().$_SERVER['QUERY_STRING'];
            $this->session->set_userdata('error', 'session_error');
            $this->session->set_userdata('original_redirect', $uri);
            redirect('users/login', 'refresh');
        }
        
        return $data;
    }
    public function testfunc2()
    {
        $this->load->model('paypal_model', 'model');
        $out = $this->model->get_pk_expired('all');
        echo '<pre>';
        print_r($out);
    }
    public function testfunc()
    {
        $data = $this->checkAuthen();
        $this->load->model('paypal_model', 'm');
        $this->load->model('referrals_model', 'refModel');

        $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));
        $f = $this->m;
        echo '<pre>';
        $id_user = '300';
         //$sql ="select * from user_commission , billings where parents_id = '$id_user' and billings.id_billing = user_commission.billing_id limit 1";
         $sql ="select * from payments_preapproval limit 1";
        $o = $this->db->query($sql)->result_array();
         //print_r($_SERVER);

         //echo $this->refModel->getAvailUserCommission($id_user).'xx';
         print_r($o);
        print_r(unserialize($this->cipher->decrypt($o[0]['logs_preapproval'])));
//         print_r($f->get_pk_expired('all'));
//         echo $f->get_pk_remain_day().' : remain<br>';
//         echo $f->get_percent_discount().' : discount<br>';
//         echo print_r($f->get_pk_n2end_day(30),true).' : get pk at day<br>';
//         echo $f->get_pk_next_charge().' : expire date<br>';
//         echo $f->get_date_first_pk().' : first day'; 
    }
    private function cronj_report_payment($uid, $bill_id)
    {
        $this->load->model('paypal_model', 'model');
        $this->load->library('PayPal/PreapprovalDetails', $this->config_pay);
        $billing_detail='';
                    
        $user = $this->authentication->get_user_by_id($uid);
        $u_name = $user[0]['user_first_name']. ' ' . $user[0]['user_las_name'];
        $u_email = $user[0]['user_email'];
        $URL = base_url().'billing/download';
        $logo = '<img src="'.str_replace('https', 'http', base_url().'/assets/images/main-logo.png').'" alt="feed.biz">';
        $id = $bill_id;
        $query = $this->model->getBilling($id);
        if ($query) {
            if ($query['id_users'] == $uid) {
                $queryDetail = $this->model->getBillingDetailLists($id);
                    
                if ($queryDetail) {
                    $billDetail = '';
                    $totalAmount = 0;
                    $defaultSign = '';
                    $count = 0;
                            
                    foreach ($queryDetail as $key => $val) {
                        $queryDetail[$key]['package_detail'] = unserialize($this->cipher->decrypt($val['package_detail']));
                        switch ($val['option_package']) {
                                    case 'offer':
                                        $cQuery = $this->model->getOfferCurrency($val['id_package']);
                                        break;
                                }
                        $queryDetail[$key]['currency'] = $cQuery[0]['currency'];
                                
                        $billing_detail .= '<div style="margin: 0px 0 5px 20px; float:left;clear:both; width:90%;">
                                    <div style="float: left;text-align: left;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                        '.($key+1).'
                                    </div>
                                    <div style="float: left;text-align: left;width: 440px;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                        <div style="float: left; clear:both;text-align: left;width: 440px; font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif; font-size: 13px;">'.$queryDetail[$key]['package_detail']['desc'].'</div>
                                        <div style="float: left; clear:both;text-align: left;width: 440px; font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif; font-size: 13px;">(Period: '.date('j F Y', strtotime($queryDetail[$key]['start_date_bill_detail'])).' - '.date('j F Y', strtotime($queryDetail[$key]['end_date_bill_detail'])).')</div>
                                    </div>
                                    
                                    <div style="float: right;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;background-color: #FFF; font-size:13px;">
                                        '.$this->mydate->moneySymbol($queryDetail[$key]['currency']).$queryDetail[$key]['package_detail']['amt'].'
                                    </div>
                                </div>';/*<div style="float: left;text-align: left;width: 130px;padding: 0px 5px;font-size:13px;line-height: 30px;text-transform: capitalize; background-color: #FFF;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;">
                                        '.$queryDetail[$key]['option_package'].'
                                    </div>*/
                                if ($key == 0) {
                                    $defaultSign = $this->mydate->moneySymbol($queryDetail[$key]['currency']);
                                }
                        $totalAmount += $queryDetail[$key]['package_detail']['amt'];
                        $count = $key;
                    }
                            
                    if (!empty($query['discount_cmd_billing'])) {
                        $count += 2;

                        $arrDiscountLog = unserialize($query['discount_log_billing']);
                        $arrDiscountCmd = unserialize($query['discount_cmd_billing']);

                        foreach ($arrDiscountCmd as $key => $val) {
                            $dcText = '';
                            switch ($val['param']) {
                                        case 'referrals':
                                            $dcText = 'Referrals Discount';
                                            break;
                                        case 'discount_date':
                                            $dcText = 'Until the next payday discount  ';
                                            break;
                                        case 'gift':
                                            $giftQuery = $this->model->getGiftCodeDetailPrint($arrDiscountLog['gift']);

                                            if ($giftQuery) {
                                                $dcText = $giftQuery[0]['name_giftopt'];
                                            }
                                            break;
                                        case 'total':
                                            $totalAmount = $val['total_amt'];
                                            break;
                                    }

                            if ($dcText != '') {
                                $billing_detail .= '<div style="margin: 0px 0 5px 20px; float:left;clear:both; width:90%;">
                                            <div style="float: left;text-align: left;width: 30px;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                                '.$count.'
                                            </div>
                                            <div style="float: left;text-align: left;width: 440px;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                                <div>'.$dcText.'</div>
                                            </div>
                                             
                                            <div style="float: right;text-align: left;width: 70px;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                                -'.$this->mydate->moneySymbol($this->config_pay['currencycode']).$val['dc_amt'].'
                                            </div>
                                        </div>';
                            }

                            $count += 1;
                        }
                    }
                }
            }
        }
        $billing_detail .='<div style="margin: 0px 0 5px 20px; float:left;clear:both; width:90%;">
                                        <div style="text-align: right; float:right; width: 440px; font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif; font-size: 13px;">
                                            <span style="font-weight: bold;">Total Amount:</span> '.$this->mydate->moneySymbol($queryDetail[$key]['currency']).$this->mydate->floatFormat($totalAmount).'&nbsp;
                                        </div>
                                    </div>';
                
                
                
        require_once APPPATH.'libraries/Swift/swift_required.php';
        $arrEmail = array($u_email);
                                        
        $f_name = 'report_billing_email.html';
                
        try {
            $filePath = str_replace('\\', '/', FCPATH).'assets/template/'.$f_name;
            $oriEmailTemplate = file_get_contents($filePath);
            $transport = Swift_SmtpTransport::newInstance();

                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);

            if (!empty($arrEmail)) {
                foreach ($arrEmail as $email) {
                    $email_template = $oriEmailTemplate;
                    $email_template = str_replace("{#logo}", $logo, $email_template);
                    $email_template = str_replace("{#name}", $u_name, $email_template);
                            //$email_template = str_replace("{#expiredate}", $expire_date, $email_template);
                            $email_template = str_replace("{#url}", $URL, $email_template);
                    $email_template = str_replace("{#date}", date('j F Y'), $email_template);
                            
                    $email_template = str_replace("{#bill_id}", $bill_id.'', $email_template);
                    $email_template = str_replace("{#billing_detail}", $billing_detail, $email_template);
                    $email_template = str_replace("{#refemail}", $email, $email_template);

                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject('Your Feed.biz packages have extended!')
                                    ->setFrom(array($u_email=>$u_name))
                                    ->setTo($email)
                                    ->setBody($email_template, 'text/html');

                    $mailer->send($swift_message);
                }
            }
                    //echo $email_template;

                   // $this->session->set_userdata('invMes',true);
                    //redirect(site_url('users/affiliation/invite_friend'));
                    return;
        } catch (Exception $ex) {
        }
    }
    private function cronj_notify_payment($uid)
    {
        $this->load->model('paypal_model', 'model');
        $this->load->model('my_feed_model', 'my_feed');
        $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
        $billing_detail='';
        $expert_mode = $this->my_feed->using_expert_mode($uid);
        $note_expert_mode = $expert_mode?$this->lang->line('note_expert_mode_notify'):'';
         
        $user = $this->authentication->get_user_by_id($uid);
        $u_name = $user[0]['user_first_name']. ' ' . $user[0]['user_las_name'];
        $u_email = $user[0]['user_email'];
        $URL = base_url().'service/paybillall';
        $logo = '<img src="'.str_replace('https', 'http', base_url().'/assets/images/main-logo.png').'" alt="feed.biz">';
                
        $query = $this->model->get_pk_zone_expired($uid);
        if ($query) {
            $queryDetail = $query[$uid];

            if ($queryDetail) {
                $billDetail = '';
                $totalAmount = 0;
                $defaultSign = '';
                $count = 0;

                foreach ($queryDetail as $key => $val) {
                    $desc = $val['title_offer_sub_pkg']==''?' Base package ':' '.$val['title_offer_sub_pkg'].' ('.$val['domain_offer_sub_pkg'].')';

                    $billing_detail .= '<div style="margin: 0px 0 5px 20px; float:left;clear:both; width:90%;">
                                <div style="float: left;text-align: left;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                    '.($key+1).'
                                </div>
                                <div style="float: left;text-align: left;width: 440px;padding: 0px 5px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;font-size: 13px;">
                                    <div style="float: left; clear:both;text-align: left;width: 440px; font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif; font-size: 13px;">'.$queryDetail[$key]['name_offer_pkg'].$desc .'</div> 
                                </div>

                                <div style="float: right;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;background-color: #FFF; font-size:13px;">
                                    '.$this->mydate->moneySymbol($queryDetail[$key]['currency_offer_price_pkg']).$queryDetail[$key]['amt_offer_price_pkg'].'
                                </div>
                            </div>';
                                        
                    $totalAmount += $queryDetail[$key]['amt_offer_price_pkg'];
                    $count = $key;
                }
            }
        }
        $billing_detail .='<div style="margin: 0px 0 5px 20px; float:left;clear:both; width:90%;">
                                        <div style="text-align: right; float:right; width: 440px; font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif; font-size: 13px;">
                                            <span style="font-weight: bold;">Total Amount:</span> '.$this->mydate->moneySymbol($queryDetail[$key]['currency_offer_price_pkg']).$this->mydate->floatFormat($totalAmount).'&nbsp;
                                        </div>
                                    </div>';
                
                
                
        require_once APPPATH.'libraries/Swift/swift_required.php';
        $arrEmail = array($u_email);
                                        
        $f_name = 'notify_expired_email.html';
                
        try {
            $filePath = str_replace('\\', '/', FCPATH).'assets/template/'.$f_name;
            $oriEmailTemplate = file_get_contents($filePath);
            $transport = Swift_SmtpTransport::newInstance();

                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);

            if (!empty($arrEmail)) {
                foreach ($arrEmail as $email) {
                    $email_template = $oriEmailTemplate;
                    $email_template = str_replace("{#logo}", $logo, $email_template);
                    $email_template = str_replace("{#name}", $u_name, $email_template);
                            //$email_template = str_replace("{#expiredate}", $expire_date, $email_template);
                            $email_template = str_replace("{#url}", $URL, $email_template);
                    $email_template = str_replace("{#date}", date('j F Y'), $email_template);
                    $email_template = str_replace("{#note_expert_mode}", $note_expert_mode, $email_template);
                    $email_template = str_replace("{#billing_detail}", $billing_detail, $email_template);
                    $email_template = str_replace("{#refemail}", $email, $email_template);

                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject('Your Feed.biz packages have already expired!')
                                    ->setFrom(array($u_email=>$u_name))
                                    ->setTo($email)
                                    ->setBody($email_template, 'text/html');

                    $mailer->send($swift_message);
                }
            }
                    //echo $email_template;

                   // $this->session->set_userdata('invMes',true);
                    //redirect(site_url('users/affiliation/invite_friend'));
                    return;
        } catch (Exception $ex) {
        }
    }
    public function cronj_notify_expire()
    {
        $this->load->model('paypal_model', 'model');
        $this->load->model('my_feed_model', 'my_feed');
        $this->load->library('PayPal/preapprovaldetails', $this->config_pay);
        $day_notify = array(5,3);
        $bill_id='';
        $billing_detail='';
                    
        foreach ($day_notify as $day) {
            $pk = $this->model->get_pk_n2end_day($day, 'all');
            $user_list = array_keys($pk);
            $data = $this->checkAuthen();
            
        
            foreach ($user_list as $uid) {
                $user = $this->authentication->get_user_by_id($uid);
                $u_name = $user[0]['user_first_name']. ' ' . $user[0]['user_las_name'];
                $u_email = $user[0]['user_email'];
                $expert_mode = $this->my_feed->using_expert_mode($uid);
                $note_expert_mode = $expert_mode?$this->lang->line('note_expert_mode_notify'):'';
                
                
                $logo = '<img src="'.str_replace('https', 'http', base_url().'/assets/images/main-logo.png').'" alt="feed.biz">';
                $expire_date = $day;
                require_once APPPATH.'libraries/Swift/swift_required.php';
                $arrEmail = array($u_email);
                $preapproved=false;
                $preapprovalProfile = $this->model->getPayment($uid);
                   
                if ($preapprovalProfile) {
                    if ($preapprovalProfile['status_preapproval'] == 'active') {
                        $this->preapprovaldetails->setPreapprovalKey($preapprovalProfile['key_preapproval']);
                        $detailResult = $this->preapprovaldetails->doTransaction();

                        if ($detailResult['Ack'] == 'Success' && $detailResult['Approved'] == 'true' && $detailResult['SenderEmail'] != '' && $detailResult['Status'] == 'ACTIVE') {
                            $preapproved=true;
                        }
                    }
                }
                $f_name = $preapproved?'notify_npay_email_pre.html':'notify_npay_email.html';
                $URL = $preapproved?(base_url().'users/login'):(base_url().'billing/configuration');
                
                
                try {
                    $filePath = str_replace('\\', '/', FCPATH).'assets/template/'.$f_name;
                    $oriEmailTemplate = file_get_contents($filePath);
                    $transport = Swift_SmtpTransport::newInstance();

                    // Create the Mailer using your created Transport
                    $mailer = Swift_Mailer::newInstance($transport);

                    if (!empty($arrEmail)) {
                        foreach ($arrEmail as $email) {
                            $email_template = $oriEmailTemplate;
                            $email_template = str_replace("{#logo}", $logo, $email_template);
                            $email_template = str_replace("{#name}", $u_name, $email_template);
                            $email_template = str_replace("{#expiredate}", $expire_date, $email_template);
                            $email_template = str_replace("{#url}", $URL, $email_template);
                            $email_template = str_replace("{#bill_id}", $bill_id.'', $email_template);
                            $email_template = str_replace("{#billing_detail}", $billing_detail.'', $email_template);
                            $email_template = str_replace("{#refemail}", $email, $email_template);
                            $email_template = str_replace("{#note_expert_mode}", $note_expert_mode, $email_template);

                            $swift_message = Swift_Message::newInstance();
                            $swift_message->setSubject('Your Feed.biz packages will expire in '.$day.' days')
                                    ->setFrom(array($u_email=>$u_name))
                                    ->setTo($email)
                                    ->setBody($email_template, 'text/html');

                            $mailer->send($swift_message);
                        }
                    }

                   // $this->session->set_userdata('invMes',true);
                    //redirect(site_url('users/affiliation/invite_friend'));
                    return;
                } catch (Exception $ex) {
                }
            }
        }
    }
}
