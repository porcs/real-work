<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class help extends CI_Controller
{
    
    public $user_name;
    public $id_user;
    public $iso_code;
    public $id_shop;
    public $id_mode;

    public function __construct()
    {
        
        // load controller parent
        parent::__construct();
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');
        
        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz', array($this->user_name));
        $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));

        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("help", $this->language);
        $this->smarty->assign("label", "help");
        $this->iso_code = mb_substr($this->language, 0, 2);
        
        $this->load->model('help_model');
    }
    
    public function index()
    {
        $data = array();
    
        if (isset($this->iso_code)) {
            $key = array(
                'present'=>'presents',
                'config'=>'configs',
                'amazonmarket'=>'amazonmarkets',
                'ebaymarket'=>'ebaymarkets');
            $message = $this->help_model->get_message(
                array('message_language'=>$this->iso_code));
        
            foreach ($message as $msg) {
                if (!isset($key[$msg['message_type']])) {
                    continue ;
                }

                $data[$key[$msg['message_type']]][] = $msg;
            }
        }
        //echo '<pre>';print_r($data);exit;
        $this->smarty->view('help.tpl', $data);
    }
    
    public function search()
    {
        $data = array();
    
        if (isset($this->iso_code)) {
            $search = null;
            $values = $this->input->post();
            if (isset($values['search_string'])) {
                $search = array(
                    'message_name'=>$values['search_string'],
                    'message_type'=>$values['search_string']);
            }
        
            $key = array('present'=>'presents',
                'config'=>'configs',
                'amazonmarket'=>'amazonmarkets',
                'ebaymarket'=>'ebaymarkets');
            $message = $this->help_model->get_message(
                array('message_language'=>$this->iso_code), null, $search);
        
            foreach ($message as $msg) {
                if (!isset($msg['message_name']) || empty($msg['message_name'])) {
                    continue ;
                }

                if (isset($msg['message_type'])) {
                    if (isset($key[$msg['message_type']])) {
                        $data['search'][] = $msg;
                    }
                } else {
                    $data['search'][] = $msg;
                }
            }
        
            if (empty($data['search'])) {
                $message = $this->help_model->get_message(
                    array('message_language'=>$this->iso_code));
                foreach ($message as $msg) {
                    if (!isset($key[$msg['message_type']])) {
                        continue ;
                    }

                    $data[$key[$msg['message_type']]][] = $msg;
                }
            
                $data['search_empty'] = $values['search_string'];
            }
        }
    
    
        $this->smarty->view('help.tpl', $data);
    }
    
    private function get_msg($type, $url=null)
    {
        $data = array();
        if (!empty($url)) {
            $message_type = '';
            switch ($type) {
                case 'presentation':        $message_type = 'present';    break;
                case 'configuration':        $message_type = 'config';    break;
                case 'amazonmarketplace':        $message_type = 'amazonmarket'; break;
                case 'ebaymarketplace':        $message_type = 'ebaymarket';    break;
            }
            if (isset($this->iso_code)) {
                $url = ucwords(str_replace("-", " ", $url));
                $where = array('message_language' => $this->iso_code,
                    'message_name' => $url,
                    'message_type' => $message_type);
                $presents = $this->help_model->get_message($where);
            }
            if (count($presents)>0) {
                $data['helpdetial']=$presents[0];
            }
        }
        
        $this->smarty->view('help_view.tpl', $data);
    }
    
    public function get_all()
    {
        $where = array('message_language' => $this->iso_code,
            'message_type' => 'info',
            'message_type' => 'marketplace');
        $allmaketplace = $this->help_model->get_message($where);
        $this->output->set_output(json_encode($allmaketplace));
    }

    // notification
    public function flagUnreadNotification($marketplace, $id_shop, $id_country, $identify, $flag=0)
    {
        //flagUnread
        $this->load->library('notifications', array($this->user_name));
        $result = $this->notifications->flagUnread($marketplace, $id_shop, $id_country, $identify, $flag);
        if($result){
            echo json_encode($result);
        }
    }
    
    public function presentation($url=null)
    {
        $this->get_msg(__FUNCTION__, $url);
    }
    
    public function configuration($url=null)
    {
        $this->get_msg(__FUNCTION__, $url);
    }
    
    public function amazonmarketplace($url=null)
    {
        $this->get_msg(__FUNCTION__, $url);
    }
    
    public function ebaymarketplace($url=null)
    {
        $this->get_msg(__FUNCTION__, $url);
    }
}
