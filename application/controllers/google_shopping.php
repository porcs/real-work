<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once(dirname(__FILE__) . '/../libraries/GoogleShopping/classes/google.config.php');
require_once(dirname(__FILE__) . '/component/component.class.php');

class google_shopping extends Component_controller
{
    public function __construct()
    {
        parent::__construct(array('prefix' => _PREFIX_, 'marketplace_name' => _MARKETPLACE_NAME_));
        new GoogleInstall($this->user_name);
    }
    
    public function register_page()
    {
        $this->page = array(
            'parameter'     => 'models/'.$this->id_country,
            'models'        => 'profiles/'.$this->id_country,
            'profiles'      => 'categories/'.$this->id_country,
            'categories'    => 'conditions/'.$this->id_country,
//            'conditions'    => 'offers_options/product/'.$this->id_country.'/'.$this->id_marketplace,
        );
        
        if(isset($this->page[$this->function_call]))
            $this->assigns(array('next' => $this->page[$this->function_call]));
    }

    public function test_view($country = null)
    {
        $this->register_page();
        
        $this->security_check();

        $data = $this->country_data($country, true);

        $restriction = array(
            'id_country' => $data['id_country'],
            'id_shop' => $data['id_shop'],
        );

        $value = $this->component_get_data("google_configurations", $restriction);
        
        $data['form_name'] = 'parameter';
        $data['save_path'] = 'save/parameter/'.$country;
        $data['containers'] = array(
            'settings' => array(
                'title' => "Settings",
                //'info' => "<b>Desction : </b><br/>".nl2br(print_r($value,true))."",
                'fields' => array(
                    'active' => array(
                        'type'=>'input_switcher',
                        'id'=>"active",
                        'name'=>"active",
                        'value'=>isset($value['active']) ? $value['active'] : '',
                        'checked'=>1
                    ),
                    'export_shipping' => array(
                        'type'=>'input_switcher',
                        'id'=>"export_shipping",
                        'name'=>"export_shipping",
                        'value'=>isset($value['export_shipping']) ? $value['export_shipping'] : '',
                        'checked'=>isset($value['export_shipping']) && ($value['export_shipping'] == 1) ? true : false
                    ),
                    'text1' => array(
                        'type'=>'input_text',
                        'id'=>"text1",
                        'name'=>"text1",
                        'value'=>"Test Text",
                        'help_text' => 'help1 help1 help1 help1'
                    ),
                    'select1' => array(
                        'type'=>'select',
                        'id'=>"select1", 
                        'name'=>"select1",
                        'help_text' => 'choose one',
                        'value'=>  array(
                            array('name'=>'option 1', 'value'=>'option_1', 'selected'=>1) ,
                            array('name'=>'option 2', 'value'=>'option_2')
                        )
                    ),
                ),
            ),
            'API' => array(
                'title' => "API",
                'fields' => array(                  
                    'text api' => array(
                        'type'=>'input_text',
                        'id'=>"text1",
                        'name'=>"text1",
                        'value'=>"",
                        'help_text' => 'input your api',
                        'size' => 'sm', // sm=>small, md=>medium, lg=>large
                    ),
                    'select api' => array(
                        'type'=>'select',
                        'id'=>"select1",
                        'name'=>"select1",
                        'size' => 'md',
                        'value'=>  array(
                            array('name'=>'option 1', 'value'=>'option_1', 'selected'=>1) ,
                            array('name'=>'option 2', 'value'=>'option_2')
                        )
                    ),
                ),
            )
        );

        $this->smarty->view("components/template.tpl", $data);
    }
    
    public function parameter($country = null)
    {
        $this->country_data($country);
        $this->register_page();
        $override_post = array();
        if ($this->input->post() && $this->input->post('method') == 'set_parameter') {
            $form_data = $this->post_form_data();
            $google_install = new GoogleInstall();
            $addtional_main_tbl = $google_install->getAdditionalTableField();
          
            if(isset($addtional_main_tbl['google_configurations']['field']) && is_array($addtional_main_tbl['google_configurations']['field'])){
                foreach ($addtional_main_tbl['google_configurations']['field'] as $field => $value){
                    $override_post[$field] = isset($form_data[$field]) ?  $form_data[$field] : null;
                }
            }
        }
    
        $this->component_parameter($override_post);
    
        $this->load->model('my_feed_model');
        $user_code = md5($this->my_feed_model->get_token($this->id_user));
        $user_id = $this->id_user;
        $token = base64_encode("$user_id|$user_code|$this->id_shop|$this->id_country|$this->id_marketplace");
    
        $dir_name = base_url() . 'google_shopping/feed.php?';
        $this->data['url_google_link'] = $dir_name . 'token=' .$token;
    
        $this->smarty->view($this->marketplace_name."/google_shopping_template.tpl", $this->data);
    }

    public function actions($type = null, $country = null) {

        $this->country_data($country);
        $this->register_page();

        switch ($type){
            case 'export':
                $this->component_parameter();
                $this->load->model('my_feed_model');
                $user_code = md5($this->my_feed_model->get_token($this->id_user));
                $user_id = $this->id_user;
                $token = base64_encode("$user_id|$user_code|$this->id_shop|$this->id_country|$this->id_marketplace");
                $dir_name = base_url() . 'google_shopping/feed.php?';
                $this->data['url_google_link'] = $dir_name . 'token=' .$token;
                $this->data['url_google_link_download'] = $dir_name . 'token=' .$token.'&export_file';
                $this->data['no_save_btn']=true; 
                $this->data['function_call'] = implode('_',array($this->function_call,$type));
                $this->smarty->view($this->marketplace_name."/google_shopping_template.tpl", $this->data);
                break;
        }
    }

    public function beautiful_key($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $array_value) {
                unset($data[$key]);
                $data[str_replace('_', ' ', $key)] = $array_value;
            }
        }
        return $data;
    }    

    public function models($country = null)
    {
        require_once(DIR_SERVER . '/application/libraries/GoogleShopping/classes/google.attributes.php');
        $this->country_data($country);
        $this->register_page();
    
        $googleAttribute = new GoogleAttributes($this->user_name);
        $AttributesFields = $googleAttribute::getAttributesFields();
    
        $override_post = array();
        $google_field = array(
            'variation' => 'variation_theme',
            'specific' => 'specific_fields',
            'custom' => 'custom_attributes'
        );
        
        if ($this->input->post() && $this->input->post('method') == 'set_models') {
            $form_data = $this->post_form_data();
        
            $override_post  = $this->models_override_post($AttributesFields, $form_data, $google_field);
        
            foreach ($override_post['model'] as $key_name => $model) {
                if (!empty($key_name)) {
                    $data[$key_name]     = array(
                        'variation_theme'       => isset($model['variation']) ? serialize($model['variation']) : null,
                        'specific_fields'       => isset($model['specific']) ? serialize($model['specific']) : null,
                        'custom_attributes'     => isset($model['custom']) ? serialize($model['custom']) : null,
                    );
                }
            }
            $override_post = $data;
        } else {
            $googleProductUniverse = $googleAttribute->getGoogleProductCategory($this->product_iso_code);            
            $this->data = array_merge($this->data, !empty($AttributesFields) ? $AttributesFields : array());
            $this->data['product_universe'] = !empty($googleProductUniverse) ? $googleProductUniverse : array();
        }
    
        $this->component_models($override_post);
        $this->data['custom_label'] = $AttributesFields['additional_attributes'];

        if (!empty($this->data['models_selected'])) {
            foreach ($this->data['models_selected'] as $selected_key => $selected) {
                foreach ($google_field as $field_key => $field) {
                    if (isset($selected[$field]) && !empty($selected[$field])) {
                        foreach ($selected[$field] as $hidden_key => $g_field) {
                            if (array_key_exists('hidden', $g_field)) {
                                $this->data['models_selected'][$selected_key][$field][$hidden_key]['hidden'] = explode('|', $g_field['hidden']);
                            } else {
                                $this->data['models_selected'][$selected_key][$field][$hidden_key]['hidden'] = array('select');
                            }
                            $json['models_selected'] = $this->data['models_selected'];
                        }
                    }
                }
            }
        }
        $dir_temp = sprintf('%s%s/%s/temp/%s/', USERDATA_PATH, $this->data['user_name'], $this->data['marketplace_name'], $this->data['id_country']);
        $json['attributes_fields'] = !empty($AttributesFields) ? $AttributesFields : '';
        $json['product_universe'] = !empty($googleProductUniverse) ? $googleProductUniverse : '';
        $json['attributes'] = !empty($this->data['attributes']) ? $this->data['attributes'] : '';
        $json['features'] = !empty($this->data['features']) ? $this->data['features'] : '';
        $json['custom_label'] = !empty($this->data['custom_label']) ? $this->data['custom_label'] : '';
        if ( !file_exists($dir_temp) ) {
                $old = umask(0);
                mkdir($dir_temp, 0777, true);
                umask($old);
        }
        file_put_contents($dir_temp.'models.json', json_encode($json));
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function models_selected($country = null, $data_selected = array())
    {   
        $this->country_data($country);
        $dir_temp = sprintf('%s%s/%s/temp/%s/', USERDATA_PATH, $this->data['user_name'], $this->data['marketplace_name'], $this->data['id_country']);
        if ( file_exists($dir_temp.'models.json') ) {
            $data_selected = json_decode(file_get_contents($dir_temp.'models.json'), true);
        }
        $id = isset($_GET['id']) ? $_GET['id'] : 0;
        $this->smarty->assign('variation_theme', !empty($data_selected['attributes_fields']['variation_theme']) ? $data_selected['attributes_fields']['variation_theme'] : '');
        $this->smarty->assign('specific_fields', !empty($data_selected['attributes_fields']['specific_fields']) ? $data_selected['attributes_fields']['specific_fields'] : '');
        $this->smarty->assign('custom_attributes', !empty($data_selected['attributes_fields']['custom_attributes']) ? $data_selected['attributes_fields']['custom_attributes'] : '');
        $this->smarty->assign('attributes', !empty($data_selected['attributes']) ? $data_selected['attributes'] : '');
        $this->smarty->assign('features', !empty($data_selected['features']) ? $data_selected['features'] : '');
        $this->smarty->assign('custom_label', !empty($data_selected['custom_label']) ? $data_selected['custom_label'] : '');
        $this->smarty->assign('data', !empty($data_selected['models_selected'][$id]) ? $data_selected['models_selected'][$id] : array());
        $this->smarty->assign('k', !empty($id) ? $id : 0);
        $html = $this->smarty->fetch('components/model_selected.tpl');
        echo $html;
        exit();
    }

    protected function models_override_post($googleField = null, $override_post = null, $fields = null)
    {
        if (!empty($googleField) && !empty($fields) && !empty($override_post)) {

            foreach ($override_post['model'] as $model_key => $model) {
                if ( empty($model['variation']) && empty($model['specific']) && empty($model['custom'])) {
                    unset($override_post['model'][$model_key]);
                    continue;
                }

                foreach ($fields as $field_key => $field) {
                    foreach ($model[$field_key] as $current_key => $current_theme) {
                        $override_post['model'][$model_key][$field_key][$current_key] = $current_theme;
                        $attr_type = null;

                        if ((isset($googleField[$field]) && !empty($googleField[$field]))
                            && (isset($current_theme['value']) &&  !empty($current_theme['value']))
                            && array_key_exists($current_key, $googleField[$field])) {
                            // to set attr_type : to know that what kind of Attribute Fields
                            $attr_field = explode('_', $current_theme['value']);

                            if ($attr_field[0] == 'Attribute') {
                                $attr_type = 'attribute';
                                $value = isset($attr_field[1]) ? $attr_field[1] : $attr_field[0];

                            } elseif ($attr_field[0] == 'Feature') {
                                $attr_type = 'feature';
                                $value = isset($attr_field[1]) ? $attr_field[1] : $attr_field[0];
                            }


                            if (!empty($current_theme['tag'])) {
                                $override_post['model'][$model_key][$field_key][$current_key]['grouping'][$current_key] = 'on';
                            } else {
                                $override_post['model'][$model_key][$field_key][$current_key]['tag'] = $googleField[$field][$current_key]['tag'];
                            }

                            if (isset($attr_type)) {
                                $override_post['model'][$model_key][$field_key][$current_key]['attr_type'] = $attr_type;
                                $override_post['model'][$model_key][$field_key][$current_key]['value'] = $value;
                            }
                            
                            
                        } else {
                            $override_post['model'][$model_key][$field_key][$current_key] = array();
                        }
                    }
                }
            }
        }

        return $override_post;
    }

    public function productType($universe){
        require_once(DIR_SERVER . '/application/libraries/GoogleShopping/classes/google.attributes.php');
        $html = '';
        $productType = GoogleAttributes::getProductType($this->product_iso_code, $universe);
        foreach ($productType as $type){
            $html .= '<option value="'.$type['id'].'">'.$type['name'].'</option>';
        }
        echo json_encode(array('productType'=>$html)); exit;
    }

    public function profiles($country = null)
    {
        $this->country_data($country);
        $this->register_page();
        $this->component_profiles();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function categories($country = null)
    {
        $this->country_data($country);
        $this->register_page();
        $this->component_categories();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }

    public function conditions($country = null)
    {
        $this->country_data($country);
        $this->register_page();
        $this->component_conditions();
        $this->smarty->view('components/components_template.tpl', $this->data);
    }

    public function logs($country = null)
    {
        $this->country_data($country);
        $this->component_logs();
        $this->data['no_save_btn']=true;
        $this->smarty->view('components/components_template.tpl', $this->data);
    }

    public function reports($country = null)
    {
        $this->country_data($country);
        $this->component_reports();
        $this->data['no_save_btn']=true;
        $this->smarty->view('components/components_template.tpl', $this->data);
    }
    
    public function get_google_types($value = 'text')
    {
        switch ($value['hidden']) {
            case 'select' :
                $type = 'item_field';
            break;
            case 'select|select' :
                $type = 'item_field';
            break;
            case 'input' :
                $type = 'text';
            break;
            case 'select|input' :
                $type = 'text';
            break;
            default :
                $type = 'text';
            break;
        }
        return $type;
    }

    public function get_xml($country = null)
    {
        require_once(dirname(__FILE__) . '/../libraries/GoogleShopping/functions/google.feed.php');
        $this->country_data($country);
        $google_field = array(
            'id'                        => array('tag' => 'g:id'),
            'title'                     => array('tag' => 'title'),
            'description'               => array('tag' => 'description'),
            'google_product_category'   => array('tag' => 'g:google_product_category'),
            'product_type'              => array('tag' => 'g:product_type'),
            'link'                      => array('tag' => 'link'),
            'mobile_link'               => array('tag' => 'g:mobile_link'),
            'image_link'                => array('tag' => 'g:image_link'),
            'additional_image_link'     => array('tag' => 'g:additional_image_link'),
            'condition'                 => array('tag' => 'g:condition'),
            'gtin'                      => array('tag' => 'g:gtin'),
            'mpn'                       => array('tag' => 'g:mpn'),
            'brand'                     => array('tag' => 'g:brand'),
            'identifier_exists'         => array('tag' => 'g:identifier_exists'),
            'availability'              => array('tag' => 'g:availability'),
            'availability_date'         => array('tag' => 'g:availability_date'),
            'price'                     => array('tag' => 'g:price'),
            'sale_price'                => array('tag' => 'g:sale_price'),
            'sale_price_effective_date' => array('tag' => 'g:sale_price_effective_date'),
            'color'                     => array('tag' => 'g:color'),
            'gender'                    => array('tag' => 'g:gender'),
            'age_group'                 => array('tag' => 'g:age_group'),
            'material'                  => array('tag' => 'g:material'),
            'pattern'                   => array('tag' => 'g:pattern'),
            'size'                      => array('tag' => 'g:size'),
            'size_type'                 => array('tag' => 'g:size_type'),
            'size_system'               => array('tag' => 'g:size_system'),
            'custom_label_0'            => array('tag' => 'g:custom_label_0'),
            'custom_label_1'            => array('tag' => 'g:custom_label_1'),
            'custom_label_2'            => array('tag' => 'g:custom_label_2'),
            'custom_label_3'            => array('tag' => 'g:custom_label_3'),
            'custom_label_4'            => array('tag' => 'g:custom_label_4'),
            'multipack'                 => array('tag' => 'g:multipack'),
            'is_bundle'                 => array('tag' => 'g:is_bundle'),
            'adult'                     => array('tag' => 'g:adult'),
            'adwords_redirect'          => array('tag' => 'g:adwords_redirect'),
            'expiration_date'           => array('tag' => 'g:expiration_date'),
            'energy_efficiency_class'   => array('tag' => 'g:energy_efficiency_class'),
            'unit_pricing_measure'      => array('tag' => 'g:unit_pricing_measure'),
            'unit_pricing_base_measure' => array('tag' => 'g:unit_pricing_base_measure'),
            'shipping_weight'           => array('tag' => 'g:shipping_weight'),
            'shipping_label'            => array('tag' => 'g:shipping_label'),
        );
        
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        $this->data['profiles'] = $configuration->getProfiles($this->id_shop, $this->id_country);
        $this->data['models'] = $configuration->getModels($this->id_shop, $this->id_country);
        $this->data['configuration'] = $configuration->getParameters($this->id_shop, $this->id_country);
        $this->data['category'] = $configuration->getSelectedCategories($this->id_shop, $this->id_country);
        $this->data['condition'] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop, $this->id_marketplace);
        
        $item_mapping = $profile = $conditions = array();
        
        if (!empty($this->data['condition'])) {
            foreach ($this->data['condition'] as $condition) {
                $conditions[$condition['txt']] = $condition['condition_value'];
            }
        }
        
        if (!empty($this->data['category'])) {
            foreach ($this->data['category'] as $category) {
                $categories[$category['id_category']] = $category['id_profile'];
            }
        }
        
        if (!empty($this->data['profiles'])) {
            foreach ($this->data['profiles'] as $id_profile => &$profile) {
                if (!empty($profile['id_model']) && !empty($this->data['models']) && array_key_exists($profile['id_model'], $this->data['models'])) {
                    $id_model = $profile['id_model'];
                    $grouping = array();
                    foreach ($this->data['models'][$id_model] as $model_key => &$model) {
                        switch ($model_key) {
                            case 'product_universe' :
                                if (!empty($model)) {
                                    $item_mapping['item_mapping']['google_product_category'] = array(
                                        'type'      => 'text',
                                        'value'     => $model,
                                        'tag'       => 'g:google_product_category',
                                    );
                                }
                            break;
                            case 'product_type' :
                                if (is_array($model) && array_key_exists($model_key, $google_field)) {
                                    $item_mapping['item_mapping']['product_type'] = array(
                                        'type'      => 'array_text',
                                        'value'     => serialize($model),
                                        'tag'       => 'g:product_type',
                                    );
                                }
                            break;
                            case 'variation_theme' :
                            case 'specific_fields' :
                            case 'custom_attributes' :
                                foreach ($model as $item_key => $item) {
                                    if (is_array($item) && array_key_exists($item_key, $google_field) && !empty($item)) {
                                        if (!empty($item['grouping'])) {
                                            $grouping[$item_key] = 'on';
                                        }
                                        $item_mapping['item_mapping'][$item_key] = array(
                                            'type'      => $this->get_google_types($item),
                                            'value'     => $item['value'],
                                            'tag'       => $google_field[$item_key]['tag'],
                                        );
                                    }
                                }
                            break;
                            default :
                                $item_mapping['item_mapping']['title'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'name',
                                    'tag'       => 'title',
                                );
                                $item_mapping['item_mapping']['description'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'description',
                                    'tag'       => 'description',
                                );
                                $item_mapping['item_mapping']['link'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'link',
                                    'tag'       => 'link',
                                );
                                $item_mapping['item_mapping']['link'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'link',
                                    'tag'       => 'link',
                                );
                                $item_mapping['item_mapping']['image_link'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'images',
                                    'tag'       => 'g:image_link',
                                );
                                $item_mapping['item_mapping']['condition'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'condition',
                                    'tag'       => 'g:condition',
                                );
                                $item_mapping['item_mapping']['price'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'price',
                                    'tag'       => 'g:price',
                                );
                                $item_mapping['item_mapping']['availability'] = array(
                                    'type'      => 'item_field',
                                    'value'     => 'quantity',
                                    'tag'       => 'g:availability',
                                );
                            break;
                        }
                    }
                    $profile['data'][$profile['id_model']] = array(
                        'name'      => $profile['name'],
                        'group'     => !empty($grouping) ? 'Yes' : 'No',
                        'shipping'  => !empty($this->data['configuration']['export_shipping']) ? 'Yes': 'No',
                    );
                    if (!empty($grouping)) {
                        $profile['data'][$profile['id_model']]['grouping'] = $grouping;
                    }
                    if (!empty($item_mapping)) {
                        $profile['data'][$profile['id_model']]['item_mapping'] = $item_mapping['item_mapping'];
                    }
                }
            }
        }

        $google_feed = new GoogleFeed(array($this->user_name));
        $google_feed->getXmlFeedByArray($this->user_name, $this->id_shop, $this->id_lang, $this->id_mode, $this->product_iso_code, $this->data['profiles'], $conditions, $categories);
    }
}
