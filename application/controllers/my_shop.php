<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class My_shop extends CI_Controller
{
    
    public $user_name;
    public $id_user;
    public $iso_code;
    public $id_shop;
    public $id_mode;
    const FILE_COL_FILTER_NAME = 'my_product_col_filter.json';
    public function __construct()
    {
        
        // load controller parent
        parent::__construct();
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');
        $this->shop_default = $this->session->userdata('shop_default');
        
        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz', array($this->user_name));
        $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("my_feeds", $this->language);
        $this->smarty->assign("label", "my_feeds");
        $this->iso_code = mb_substr($this->language, 0, 2);
        
        $this->smarty->assign("function", $this->lang->line($this->router->fetch_method()));
        $this->smarty->assign("function_tag", $this->router->fetch_method());
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
    
    public function configuration($popup=false, $message = null)
    {
        $this->session->unset_userdata('dashboard_sum_product');
        $data = array();
        $data['start_feed'] = "false";
                         
        $config_data = $this->my_feed_model->get_general($this->id_user);
        $data['feed_mode']= $this->id_mode;
        $data['feed_biz']=array();
        
        foreach ($config_data as $key => $value) {
            if (strtolower($key) != "feed_mode") {
                $data[strtolower($key)] = unserialize(base64_decode($value['value']));
            }
        }
       
        if ($this->session->userdata('feed')&& isset($data['feed_biz']['verified']) && isset($data['feed_biz']['verified'])=="true") {
            $data['start_feed'] = "true";
            $this->session->unset_userdata('feed');
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
    
        if (!$this->session->userdata('dMsg_DS0') && !$this->session->userdata('dMsg_P0')) {
            if ($message) {
                _get_message_code($message); 
            }
        }
        if ($this->session->userdata('dMsg_DS0')) {
            _get_message_code('11-00002');
            $this->session->unset_userdata('dMsg_DS0');
        }
        if ($this->session->userdata('dMsg_P0')) {
            _get_message_code('11-00005');
            $this->session->unset_userdata('dMsg_P0');
        }
        if (!is_dir(USERDATA_PATH . $this->user_name)) {
            $data['start_feed'] = "false";
            $data['profile'] = urlencode(json_encode(array('username' => $this->user_name, 'id' => $this->id_user)));
        }
        if ($popup!==false) {
            $this->smarty->assign('popup', $popup);
            $this->smarty->assign('allow_progress_in_iframe', $popup);
            $this->smarty->assign('missing_id_shop_flag', true);
            
            $this->session->set_userdata('data_source_popup', $popup);
        
            if ($popup == 0) { // Missing id_shop		
         $this->smarty->assign('popup', 2);
            }
        } else {
            if(empty($message)){
                $this->session->unset_userdata('data_source_popup');
            }
        }
        
        $userdata = $this->authentication->user($this->id_user)->row();
        $data['status'] = $userdata->user_cronj_status;
        $this->load->model('my_feed_model');
        $data['history'] = $this->my_feed_model->get_histories($this->id_user);
        $this->smarty->view('my_shop/configuration.tpl', $data);
    }
    
    public function connect_save($popup=false)
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
            $id_user = $this->id_user;
            $username = $this->user_name;
            
            if (!isset($id_user) || !isset($username) || empty($id_user) || empty($username)) {
                $this->session->set_userdata('error', 'incorrect_user');
                redirect('my_shop/configuration');
            }
            
            $today  = date('Y-m-d H:i:s', time());
            $encrype_data = array();
            $encrype_data['base_url'] = $values['base_url'];
            $encrype_data['username'] =  $username;
            $encrype_data['verified'] = $values['verified'];
            $time_zone = round(isset($values['time_zone'])?$values['time_zone']:0);
            if ($time_zone>12||$time_zone<-12) {
                $time_zone = 0;
            }
            $this->load->model('my_feed_model');
            if($popup){
                $this->my_feed_model->set_time_zone(round($time_zone), $this->id_user);
            }
            if (trim($values['base_url']) !='') {
                $_h = parse_url($values['base_url']);
                if (isset($_h['host'])) {
                    $host = $_h['host'];
                    if (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $host)
                            && preg_match("/^.{1,253}$/", $host)&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $host)) {
                        if (!$this->my_feed_model->check_exist_domain($id_user, base64_encode($host))) {
                            $feedbiz =  array(
                            'id_customer' => $id_user,
                            'name' => 'FEED_BASE_URL_1',
                            'config_date' => $today,
                            'value' => base64_encode($host)
                        );
                            $this->my_feed_model->set_general($feedbiz);
                            $duplicate_domain=false;
                        } else {
                            $encrype_data['base_url'] ='';
                            $duplicate_domain=true;
                        }
                    }
                }
            }
            
            $duplicate_domain=false;
            
            if (isset($values['mode'])) {
                $this->authentication->update($this->id_user, array('user_cronj_status'=>$values['mode']));
            }else{
                $this->authentication->update($this->id_user, array('user_cronj_status'=>1));
            }
            
            $feedbiz =  array(
                'id_customer' => $id_user,
                'name' => 'FEED_BIZ',
                'config_date' => $today,
                'value' => base64_encode(serialize($encrype_data))
            );
            
            if ($duplicate_domain) {
                $this->session->set_userdata('error', 'duplicate_domain_with_other_user');
            } elseif (!$this->my_feed_model->set_general($feedbiz)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('feed', true);
                $this->session->set_userdata('message', 'save_successful');
            }
        }
        if ($this->session->userdata('data_source_popup')) {
            $tmp = $this->session->userdata('data_source_popup');
            $this->session->unset_userdata('data_source_popup');
            redirect("dashboard/iframe_popup_conf/next", 'refresh');
        } else {
            redirect('my_shop/configuration');
        }
    }
    public function connect()
    {
        $this->load->model('my_feed_model');
        $data=array();
        $data['presta_version'] = $this->my_feed_model->checkCurrentModuleVersion(0);
        $this->smarty->view('my_shop/connect.tpl',$data);
    }
    
    public function multichannel_orders()
    {
    
    /*Amazon FBA*/
    require_once(dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.multichannel.php');
    
        $data = array(
            'fba_status_submited' => AmazonMultiChannel::AMAZON_FBA_STATUS_SUBMITED,
            'fba_status_received' => AmazonMultiChannel::AMAZON_FBA_STATUS_RECEIVED,
            'fba_status_invalid' => AmazonMultiChannel::AMAZON_FBA_STATUS_INVALID,
            'fba_status_planning' => AmazonMultiChannel::AMAZON_FBA_STATUS_PLANNING,
            'fba_status_processing' => AmazonMultiChannel::AMAZON_FBA_STATUS_PROCESSING,
            'fba_status_cancelled' => AmazonMultiChannel::AMAZON_FBA_STATUS_CANCELLED,
            'fba_status_complete' => AmazonMultiChannel::AMAZON_FBA_STATUS_COMPLETE,
            'fba_status_completepartialled' => AmazonMultiChannel::AMAZON_FBA_STATUS_COMPLETEPARTIALLED,
            'fba_status_unfulfillable' => AmazonMultiChannel::AMAZON_FBA_STATUS_UNFULFILLABLE,
        );

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('my_shop/multichannel_orders.tpl', $data);
    }
    
    public function orders_multichannel($action, $id_order, $id_country=null)
    {
    
        // Hook
        require_once(BASEPATH.'/../libraries/Hooks.php');
    
        global $result;
    
        $hook = new Hooks();
    
        if ($action == "save_orders") {
            if (!isset($id_country) || empty($id_country) || !$id_country) {
                echo json_encode(array('pass' => false, 'result' => 'missing_id_country'));
                exit;
            }

            $params = array(
                'user_name' => $this->user_name,
                'id_shop' => $this->id_shop,
                'site' => $id_country,
                'id_order' => $id_order,
                'force_send' => true
            );
        } else {
            $params = array(
            'user_name' => $this->user_name,
            'id_shop' => $this->id_shop,
            'id_order' => $id_order
            );
        }
    
        $hook->_call_hook($action, $params);
    
        if (!isset($result)) {
            echo json_encode(array('send' => true));
            exit;
        }
    
        $html = $this->smarty->fetch('my_shop/multichannel_order_detail.tpl', $result);
        
        echo json_encode(array('result' => $html));
        exit;
    }
    
    public function multichannel_orders_data()
    {
        $marketplaces = array();
    
        $values = $this->input->post();
    
	/*$values = array(
	    'draw' =>  1,
	    'length' => 10,
	    'start' => 0,
	    'search' => array(
		  'value' => ''  
	    ),
	    'order' => array(
		'1' => array(
		    'column' => 8,
		    'dir' => 'DESC'
		)
	    )
	);*/

        $this->load->model('marketplace_model');
    
        if (isset($this->session->userdata['menu'])) {
            $menu = $this->session->userdata['menu'];
            $set_id = array();
            foreach ($menu as $marketplace => $marketplace_data) {
                $set_id[(int) $marketplace_data['id']] = (int) $marketplace_data['id'];
            }

            $SubMarketplaceSet = $this->marketplace_model->getSubMarketplaceOfferSet($set_id);
            foreach ($menu as $marketplace => $marketplace_data) {
                $SubMarketplace = $SubMarketplaceSet[(int) $marketplace_data['id']];
                foreach ($SubMarketplace as $SubMarketplace_data) {
                    $marketplaces[$marketplace][$SubMarketplace_data['id_offer_sub_pkg']] = $SubMarketplace_data['domain_offer_sub_pkg'];
                }
            }
        }
    
        if (isset($values) && !empty($values)) {

            require_once(dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.multichannel.php');

            $missing_items = array();
            $amazon_order = new Amazon_Order($this->user_name, true);
            $orders = $amazon_order->get_order_missing_item("AND o.payment_method not like 'Amazon%' AND oi.seller_order_id IS NOT NULL");

            if(!empty($orders)){
                $countries = Marketplaces::getCountries();
            }

            foreach ($orders as $key => $order){
                // check FBA
                $missing_items[$key] =$order;
                $missing_items[$key]['total_paid'] = '';
                $missing_items[$key]['mp_channel_status'] = '';
                $missing_items[$key]['language'] = '';
                $missing_items[$key]['mp_channel'] = '';
                $missing_items[$key]['total_products'] = '';
                $missing_items[$key]['action'] = 'updateOrder';
                $missing_items[$key]['country'] = isset($countries[$order['site']]['title_offer_sub_pkg']) ? strtolower(str_replace(" ", "_", $countries[$order['site']]['title_offer_sub_pkg'])) : '' ;
            }

            $this->load->library('FeedBiz_Orders', array($this->user_name));
            
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];
                                  
            $sort = array();
            $column = null;
            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "o.id_orders"; break;
                    case 1 : $column = "oi.seller_order_id"; break;
                    case 2 : $column = "o.id_marketplace_order_ref"; break;
                    case 3 : $column = "o.sales_channel"; break;
                    case 5 : $column = "mp.mp_channel_status"; break;
                    case 6 : $column = "oi.total_products"; break;
                    case 7 : $column = "o.total_paid"; break;
                    case 8 : $column = "o.order_date"; break;
                }
                $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
            }
        
            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->feedbiz_orders->getMultichannelOrders($this->user_name, $this->id_shop, null, null, $order_by, $search, true, $marketplaces);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = $this->feedbiz_orders->getMultichannelOrders($this->user_name, $this->id_shop, $start, $limit, $order_by, $search, false, $marketplaces);
            $data['data'] = array_merge($missing_items, $data['data']);
            echo json_encode($data);
        }
    }

    public function multichannel_orders_update($seller_order_id){
        $this->load->library('FeedBiz_Orders', array($this->user_name));
        echo json_encode($this->feedbiz_orders->updateOrder($this->user_name, $seller_order_id));
    }

    
    public function my_products()
    {
        
        //$this->load->library('FeedBiz', array($this->user_name));
        $data['shop_condition'] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop);
        $data['cdn_url'] = $this->config->item('base_url');
        $def_json = '[{"p_id":true},{"p_img":true},{"cat_name":true},{"p_status":true},{"p_condition":true},{"p_price":true},{"p_qty":true},{"p_sale_price":false},{"p_sale_from":false},{"p_sale_to":false}]';
        $dir_temp = USERDATA_PATH . $this->user_name . '/json/';
        $file_path = $dir_temp . My_shop::FILE_COL_FILTER_NAME;
        if(file_exists($file_path)){
            $def_json = file_get_contents($file_path);
        }
         $tmp = json_decode($def_json,true);
         foreach($tmp as $k=>$v){
             foreach($v as $f=>$s){
                 $tmp[$f]=$s;
             } 
             unset($tmp[$k]);
         }
         $data['col_fil']=$tmp; 
        $this->smarty->view('my_shop/my_products.tpl', $data);
    }
    
    public function getJsonCategory($type='')
    {
        $this->authentication->validateAjaxToken();
        //$this->load->library('FeedBiz', array($this->user_name));
        echo $this->feedbiz->getJsonAllCategory($this->user_name, $this->id_shop, null, $type);
    }
    public function my_product_data()
    {
        $start = isset($_GET['start']) ? $_GET['start'] : 1;
        $page_size = isset($_GET['length']) ? $_GET['length'] : 10;
        $condition = isset($_GET['condition']) ? $_GET['condition'] : '';
        $category = isset($_GET['category']) ? str_replace("'", '', $_GET['category']) : '';
        $search = isset($_GET['search']['value']) ? $_GET['search']['value'] : '';
        $option = array('start' => $start, 'length' => $page_size, 'condition' => $condition, 'category' => intval($category), 'search' => $search);
        $page = ($start / $page_size) +1;
        //$this->load->library('FeedBiz', array($this->user_name));
        $shop = $this->session->userdata('id_shop');
        $mode = $this->session->userdata('mode_default');
        $this->load->model('currency_model', 'currency');
        $this->load->library('Eucurrency');
        $this->load->config('regex');
        $this->load->library('mydate');
        
        if (is_array($shop) && isset($shop['id_shop'])) {
            $shop = $shop['id_shop'];
        }
        $out = $this->feedbiz->exportProductsForTableList($this->user_name, $shop, $mode, null, $option);
        $currencySignList = $this->config->item('currency_code');
        $all_filter_num = $all_num = 0;
        if (isset($out['all_filter_num'])) {
            $all_filter_num = $out['all_filter_num'];
            unset($out['all_filter_num']);
        }
        if (isset($out['all_num'])) {
            $all_num = $out['all_num'];
            unset($out['all_num']);
        }
        foreach ($out as $k=>$o) {
            $cur = $o['curr'];
            $currencySign = $currencySignList[$cur];
            $out[$k]['price']= $currencySign.''.number_format($out[$k]['price'], 2);
            if(isset($out[$k]['sale_price']) && $out[$k]['sale_price']*1!=0){
                $out[$k]['sale_price']= $currencySign.''.number_format($out[$k]['sale_price'], 2);
            }
            foreach ($o['items']as $i => $d) {
                if(isset($out[$k]['items'][$i]['sale_price']) && $out[$k]['items'][$i]['sale_price']*1!=0){
                    $out[$k]['items'][$i]['sale_price'] = $currencySign.''.number_format($d['sale_price'], 2);
                }
                $out[$k]['items'][$i]['price'] = $currencySign.''.number_format($d['price'], 2);//number_format($this->eucurrency->doDiffConvert($d['price'], $cur, $currency),2);
            }
        }
        echo json_encode(array('data'=>$out, /*'draw'=>$page==0?'1':$page,*/'recordsTotal'=>$all_num, 'recordsFiltered'=>$all_filter_num));
    }

    public function my_product_column_filter_save()
    {
        $dir_temp = USERDATA_PATH . $this->user_name . '/json/';
        if (!file_exists($dir_temp)) {
            $old = umask(0);
            mkdir($dir_temp, 0777, true);
            umask($old);
        }

        $file_path = $dir_temp . My_shop::FILE_COL_FILTER_NAME;
        if($this->input->post()){
            $post=$this->input->post();
            if(isset($post['col_filter'])){
                file_put_contents($file_path, $post['col_filter']);
            }
        }
    }
    
    public function my_product_data_test()
    {
        
        //$this->load->library('FeedBiz', array($this->user_name));
        $shop = $this->session->userdata('id_shop');
        $mode = $this->session->userdata('mode_default');
        $this->load->model('currency_model', 'currency');
        $this->load->library('Eucurrency');
        $this->load->config('regex');
        $this->load->library('mydate');

        if (is_array($shop) && isset($shop['id_shop'])) {
            $shop = $shop['id_shop'];
        }
        echo $shop.' '.$mode.' '.$this->user_name;
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $out = $this->feedbiz->exportProductsForTableList($this->user_name, $shop, $mode);
        print_r($out);
        echo 'xxx';
         
        $currencySignList = $this->config->item('currency_code');
        foreach ($out as $k=>$o) {
            $cur = $o['curr'];
            $currencySign = $currencySignList[$cur];
            foreach ($o['items']as $i => $d) {
                $out[$k]['items'][$i]['price'] = $currencySign.''.number_format($d['price'], 2);//number_format($this->eucurrency->doDiffConvert($d['price'], $cur, $currency),2);
            }
        }
        
        echo json_encode(array('data'=>$out));
    }
    
    public function test_async()
    {
        echo '<pre>';
        echo date('r');
    }
     
    public function get_product_option_by_sku($id_country = null, $marketplace_id = null)
    {
        $value = $this->input->post();
        $result = array();
        if (isset($value['sku'])) {
            $result = $this->feedbiz->marketplace_getProductOptionBySku($value['sku'], $this->id_shop, $id_country, $marketplace_id);
        }
        echo json_encode($result);
    }
    
    public function offers_options($group_by = "product", $id_country = null, $marketplace_id = null)
    {
        $data = array();
        $this->feedbiz->marketplace_product_option($id_country, $marketplace_id);
        //$data['fields'] = $this->feedbiz->marketplace_getProductOptionGetFields($marketplace_id);

        $marketplace = $this->feedbiz->GetMarketplaceById($marketplace_id);
        $this->country_data($id_country, $marketplace);
        
        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }

        if (isset($this->shop_default)) {
            $this->smarty->assign("shop_name", $this->shop_default);
        } else {
            $this->smarty->assign("shop_name", $this->id_shop);
        }
    
        if ($group_by == 'product') {
            $category = $this->feedbiz->marketplace_getCategoryOption($this->id_shop, null, null, null, null, null, $id_country, $marketplace_id);
           
            if (isset($category['category'])) {
                $data['category'] = $this->category_option($category['category']);
            }
        
            switch ($marketplace) {
                case 'Amazon':
                    $this->smarty->view('amazon/Offers_options.tpl', $data); break;
                case 'eBay':
                    $this->smarty->view('ebay/option/Offers_options.tpl', $data); break;
                case 'google_shopping':
                    $this->smarty->view('google_shopping/offers_options.tpl', $data); break;
                default :
                    $this->smarty->view('my_shop/offers_options.tpl', $data);break;
            }
        } elseif ($group_by == 'category') {
            switch ($marketplace) {
                case 'Amazon':
                    $this->smarty->view('amazon/Category_options.tpl', $data); break;
                case 'eBay':
                    $this->smarty->view('ebay/option/Category_options.tpl', $data); break;
                case 'google_shopping':
                    $this->smarty->view('google_shopping/category_options.tpl', $data); break;
                default :
                    $this->smarty->view('my_shop/category_options.tpl', $data); break;
            }
        }
    }
    
    private function country_data($country, $tag)
    {
        $this->smarty->assign("id_country", $country);
    
        if (isset($this->session->userdata['menu'])) {
            $marketplace = $this->session->userdata['menu'];
        }
    
        if (isset($marketplace[$tag]['id'])) {
            $this->smarty->assign("id_marketplace", $marketplace[$tag]['id']);
        }
        if (isset($marketplace[$tag]['submenu'][$country]['country'])) {
            $this->smarty->assign("country", $marketplace[$tag]['submenu'][$country]['country']);
        }
        if (isset($marketplace[$tag]['submenu'][$country]['ext'])) {
            $this->ext = $marketplace[$tag]['submenu'][$country]['ext'];
            $this->smarty->assign("ext", $marketplace[$tag]['submenu'][$country]['ext']);
        }
        
        $currency = $this->feedbiz->getDefaultCurrency($this->id_shop);
        
        if (isset($currency['iso_code'])) {
            $this->currency = $currency['iso_code'];
        } else {
            if (isset($marketplace[$tag]['submenu'][$country]['currency'])) {
                $this->currency = $marketplace[$tag]['submenu'][$country]['currency'];
            }
        }
        
        $this->config->load('regex');
        $currencys = $this->config->item('currency_code');
        
        if (isset($this->currency)) {
            $this->smarty->assign("currency_sign", $currencys[$this->currency]);
        }
        
        /*if (isset($marketplace[$tag]['submenu'][$country]['currency'])) {
            //$this->smarty->assign("currency", $marketplace[$tag]['submenu'][$country]['currency']);  
            $this->config->load('regex');
            $currencys = $this->config->item('currency_code');
            
            
            $this->feedbiz->getDefaultCurrency($this->id_shop);
            
            if(isset($currencys[$marketplace[$tag]['submenu'][$country]['currency']])){
                $this->smarty->assign("currency_sign", $currencys[$marketplace[$tag]['submenu'][$country]['currency']]);     
            }
        }*/
    }
    
    public function category_option($category, $is_child = false, $nbsp = '&nbsp;')
    {
        $select_option = '';
        $icon = ' &#8627; ';
        if (!empty($category)) {
            foreach ($category as $cat) {
                if ($is_child) {
                    $select_option .= '<option value="'.$cat['id_category'].'" class="child">'.$nbsp.$icon.$cat['category'].'</option>';
                } else {
                    if (isset($cat['children']) && !empty($cat['children'])) {
                        $select_option .= '<option value="'.$cat['id_category'].'" class="parent">'.$cat['category'].'</option>';
                    } else {
                        $select_option .= '<option value="'.$cat['id_category'].'" >'.$cat['category'].'</option>';
                    }
                }
                if (isset($cat['children']) && !empty($cat['children'])) {
                    $nbsp = ' &nbsp; ' . $nbsp;
                    $select_option .= $this->category_option($cat['children'], true, $nbsp);
                }
            }
        }
        return $select_option;
    }
    
    public function product_options_page($id_country = null, $marketplace_id = null)
    {
        $values = $this->input->post();
     
//	$values = array(
//	    'draw' =>  1,
//	    'length' => 10,
//	    'start' => 0,
//	    'search' => array(
//		  'value' => ''  
//	    )	    
//	);
//	
//	$values['order'][0]['column'] = 0;
//	$values['order'][0]['dir'] = 'asc';
//	$values['order'][1]['column'] = 1;
//	$values['order'][1]['dir'] = 'asc';
    //$values['search']['value'] = 136;

        
        if (isset($values) && !empty($values)) {
            $search = array();
            
            if (isset($values['columns'])) {
                foreach ($values['columns'] as $key => $columns) {
                    switch ($key) {
            // category search
            case '0':
                if (isset($columns['search']['value']) && !empty($columns['search']['value'])) {
                    $search['id_category'] = $columns['search']['value'];
                }
            break;
            // create date search
            case '1':
                if (isset($columns['search']['value']) && !empty($columns['search']['value'])) {
                    $search['date_created']['date_from'] = $columns['search']['value'];
                }
                break;
            case '2':
                if (isset($columns['search']['value']) && !empty($columns['search']['value'])) {
                    $search['date_created']['date_to'] = $columns['search']['value'];
                }
            break;
            }
                }
            }
            
//	    $search = array(
//		    'date_created' => 
//		      array(
//			'date_from' => '01/04/2013',
//			'date_to' => '30/04/2013',
//			  )
//		); 
            $order_by = $column = '';
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search['all'] = $values['search']['value'];
            
            if (isset($values['order'])) {
                foreach ($values['order'] as $order_sort) {
                    switch ($order_sort['column']) {
                        case 0 : $column = "p.id_product"; break;
                        case 1 : $column = "pa.id_product_attribute"; break;
                    }
                    $sort[] = $column . ' ' . $order_sort['dir'];
                }
                $order_by = implode(', ', $sort);
            }
            
            
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->feedbiz->marketplace_getProductOption($this->id_shop, null, null, $order_by, $search, true, $id_country, $marketplace_id);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $products = $this->feedbiz->marketplace_getProductOption($this->id_shop, $start, $limit, $order_by, $search, false, $id_country, $marketplace_id);
            
            foreach ($products as $product) {
                array_push($data['data'], $product);
            }
            
            echo json_encode($data);
        }
    }
    
    public function category_options_page($id_country = null, $marketplace_id = null)
    {
        $products = $this->feedbiz->marketplace_getCategoryOption($this->id_shop, null, null, null, null, null, $id_country, $marketplace_id);
        $data = array();
        $data['name'] = 'ProductOptionByCategory';
        $data['category'] = $products['category'];
        
        echo json_encode($data);
    }

    public function save_product_options($id_country = null, $marketplace_id = null)
    {
        $result = array();
        $values = $this->input->post();
        if (isset($values['product_options']) && !empty($values['product_options'])) {
            //echo json_encode($values);
           $result = $this->feedbiz->save_marketplace_product_option($values['product_options'], $this->id_shop, $id_country, $marketplace_id);
        }
        echo json_encode($result);
    }
    
    public function save_category_options($id_country = null, $marketplace_id = null)
    {
        $values = $this->input->post();
        //echo json_encode($values); exit;  
        if (isset($values['category_options']) && !empty($values['category_options'])) {
            $result = $this->feedbiz->save_marketplace_category_option($values['category_options'], $this->id_shop, $id_country, $marketplace_id);
        }
        echo json_encode($result);
    }
    
    public function reset_product_options($id_country = null, $marketplace_id = null)
    {
        $values = $this->input->post();
        //echo json_encode($values); exit;  
        $result = array();
        if (isset($values['id_product']) && !empty($values['id_product'])) {
            $result = $this->feedbiz->reset_marketplace_product_option($values['id_product'], $this->id_shop, $id_country, $marketplace_id);
        }
        echo json_encode($result);
    }
    
    public function reset_category_options($id_country = null, $marketplace_id = null)
    {
        $values = $this->input->post();
        //echo json_encode($values); exit;  
        if (isset($values['id_category']) && !empty($values['id_category'])) {
            $result = $this->feedbiz->reset_marketplace_category_option($values['id_category'], $this->id_shop, $id_country, $marketplace_id);
        }
        echo json_encode($result);
    }
    
    public function product_options_download_template($id_country = null, $marketplace_id = null)
    {
        $this->feedbiz->marketplace_ProductOptionDownloadTemplate($id_country, $marketplace_id);
    }
    
    
    public function product_options_download($id_country = null, $marketplace_id = null)
    {
        $values = $this->input->post();
        $this->feedbiz->marketplace_ProductOptionDownload($values, $this->id_shop, $id_country, $marketplace_id);
    }
    
    public function product_options_upload_template($id_country = null, $marketplace_id = null)
    {
        $this->load->helper('form');
        $config['upload_path'] = USERDATA_PATH . $this->user_name .'/process/';
        $config['allowed_types'] = 'txt|csv';
        $config['overwrite'] = true;
        $config['file_name'] = 'offers_options' . (isset($marketplace_id) ? '_'.$marketplace_id : '') . (isset($id_country) ? '_'.$id_country : '');
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file-upload')) {
            echo json_encode(array('error' => $this->upload->display_errors()));
            exit;
        }

        $file_data = $this->upload->data();
        /*$path = $file_data['full_path']; 
        $datalist = $head = array();
        $delimiter = ',';
        $array_header = array();
        $array_header['SKU'] = "sku";
        $array_header['Price_Override'] = "price";
        $array_header['Disable'] = "disable";
        $array_header['Force_in_Stock'] = "force";
        $array_header['Do_not_export_price'] = "no_price_export";
        $array_header['Do_not_export_quantity'] = "no_quantity_export";
        $array_header['Gift_wrap'] = "gift_wrap";
        $array_header['Gift_message'] = "gift_message";
        $array_header['ASIN'] = "asin1";
        $array_header['FBA_value'] = "fba_value";
        $array_header['Latency'] = "latency";
        $array_header['Shipping'] = "shipping";
        $array_header['Shipping_Type'] = "shipping_type";
        $array_header['Condition_note'] = "text";

        if ($path) {
            $fp = fopen($path, 'r');
            $buf = fread($fp, 1000);
            fclose($fp);
        } else {
            return $datalist;
        }

        $count1 = count(explode(',', $buf));
        $count2 = count(explode(';', $buf));
        if ($count2 > $count1){
            $delimiter = ';';
        }

        $fp = fopen($path, 'r');
        $i = 0;

        while ($data = fgetcsv($fp, 0, $delimiter)) {  
            
            if (!is_array($data)){
                return false;
            }
            if($i == 0){
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $vhead = str_replace(" ", "_", $vhead);
                        $head[$khead] = isset($array_header[$vhead]) ? $array_header[$vhead] : $vhead;
                    }
                }
            } else {
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $datalist[$i][$head[$khead]] = trim($vhead);
                    }
                }
                if(!empty($datalist[$i])){
                    
                    if(!isset($datalist[$i]['sku']))continue; 
                    
                    $product = $this->feedbiz->getProductBySKU($this->id_shop, $datalist[$i]['sku']);
                    
                    if(!isset($product['id_product'])) continue;
                    
                    $datalist[$i]['id_product'] = $product['id_product'];
                    $datalist[$i]['id_product_attribute'] = isset($product['id_product_attribute']) ? $product['id_product_attribute'] : null;
                }
            }
            $i++;
        } 
        
        if(!empty($datalist)){
            $details = $this->feedbiz->marketplace_ProductOptionUpload($datalist, $this->id_shop, $id_country, $marketplace_id);
        }*/

        echo json_encode(array('pass' => true));
    }
}
