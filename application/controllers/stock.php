<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/stock.php');

class stock extends CI_Controller
{

    const TAG = 'stock';

    public function __construct()
    {
        parent::__construct();

        $this->load->library('authentication');
        $this->authentication->checkAuthen(); //check logged in and redirect to login page
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');

        if ((!isset($this->user_name) || empty($this->user_name)) &&
            (!isset($this->id_user) || empty($this->id_user))) {
            redirect('users/login');
        }
        //load library

        // id shop
        $this->id_shop = $this->session->userdata('id_shop');
        if (!isset($this->id_shop) || empty($this->id_shop)) {
            $this->load->library('FeedBiz', array($this->user_name));
            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            $this->id_shop = isset($DefaultShop['id_shop']) ?
                $DefaultShop['id_shop'] : '';
        }
        
        $this->load->library('FeedBiz', array($this->user_name));
        
        // id mode
        $this->id_mode = 1;
        
        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->lang->load(strtolower(self::TAG), $this->language);
        $this->smarty->assign("lang", $this->language);
        $this->smarty->assign("label", strtolower(self::TAG));

        // Class name //Function name
        $this->smarty->assign("class", ucwords($this->router->fetch_class()));
        $this->smarty->assign("function",
            $this->lang->line($this->router->fetch_method()));
        
        $this->stock = new StockMovement($this->user_name);
        
        // get document
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
    
    public function report()
    {
        $data = array();
        $this->smarty->view('stock/report.tpl', $data);
    }
    
    public function carts_page()
    {
        $values = $this->input->post();
        
        if (isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];
            /*$page = 1;
            $limit = 10;
            $start = 0;
            $search = null;*/
        
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->stock->getstockCartReport(
                $this->id_shop, null, null, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $products = $this->stock->getstockCartReport(
                $this->id_shop, $start, $limit, $search);
                
            $data['data']=$products;

            echo json_encode($data);
        }
    }
    
    public function carts_download()
    {
        $list = array();
        $result = $this->stock->getstockCartReport($this->id_shop);
    
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['reference'] = "reference";
        $list['header']['order_date'] = "order_date";
        $list['header']['marketplace'] = "marketplace";
        $list['header']['order_ref'] = "order_ref";
        $list['header']['quantity_reserved'] = "quantity_reserved";

        foreach ($result as $key => $data) {
            $list[$key]['id_product'] = (int) $data['id_product'];
            $list[$key]['id_product_attribute'] = (int) $data['id_product_attribute'];
            $list[$key]['reference'] = $data['sku'];
            $list[$key]['order_date'] = $data['order_date'];
            $list[$key]['marketplace'] = $data['marketplace'];
            $list[$key]['order_ref'] = $data['detail'];
            $list[$key]['quantity_reserved'] = $data['quantity'];
        }

        ob_start();
        ob_get_clean();
        $filename = 'feedbiz_carts.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) {
            fputcsv($h, $data, ';');
        }
    }
    
    public function carts()
    {
        $data = array();
        $this->smarty->view('stock/cart_report.tpl', $data);
    }

    public function products()
    {
        $values = $this->input->post();
        
        if (isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];
            //$page = 1;
            //$limit = 10;
            //$start = 0;
            //$search = null;

            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->stock->getstockMovementReport(
                $this->id_shop, null, null, 'smvt.date_add ASC', $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $products = $this->stock->getstockMovementReport(
                $this->id_shop, $start, $limit,
                's.id_product ASC, s.id_product_attribute ASC, smvt.date_add DESC',
                $search);

            foreach ($products as $stock) {
                $product = $this->feedbiz->getProductById(
                    $this->user_name, $stock['id_product'],
                    $this->iso_code);
        
                $d['type'] = $stock['type'];
                $d['date_add'] = $stock['date_add'];
        
                $text = '';
                $text .= ' (' .$stock['id_product'];
                $text .= isset($stock['id_product_attribute']) ?
                    '/' . $stock['id_product_attribute'] .')' : ')';
        
                if (isset($stock['reference'])) {
                    $d['reference'] = 'Reference : ' . $stock['reference'] . $text;
                } elseif (isset($product->reference)) {
                    $d['reference'] = 'Reference : ' . $product->reference . $text ;
                } else {
                    $d['reference'] = $text;
                }
                
                $d['id_product'] = $stock['id_product'];
                $d['id_product_attribute'] = $stock['id_product_attribute'];
                
                $sign = '' ;
                if ($stock['action'] == "decrease") {
                    $sign = '-';
                } elseif ($stock['action'] == "increase") {
                    $sign = '+';
                }

                if ($stock['type'] == StockMovement::update_offer) {
                    $type = $this->lang->line('Feed update offer from') .
                        ' ' . $stock['source'];
                } else {
                    $type = $this->lang->line('Other');
                }

                $d['detail'] = $type;
                $d['quantity'] = $stock['quantity'];
                $d['movement_quantity'] = $sign . $stock['movement_qty'];
                $d['balance_quantity'] = $stock['balance_qty'];
                
                if ($stock['type'] == StockMovement::import_order) {
                    $d['type'] = 'outside_source';
                    $type = sprintf($this->lang->line(
                        'Order from %s (order id:%s)'),
                        $stock['source'], $stock['id_ref']);
                    $d['detail'] = $type;
                    $d['quantity'] = '';
                    $d['movement_quantity'] = '('.$sign . $stock['movement_qty'].')';
                    $d['balance_quantity'] = '';
                } elseif ($stock['type'] != StockMovement::update_offer) {
                    $d['type'] = 'outside_source';
                    $type = sprintf($this->lang->line('%s from %s'),
                        ucfirst(str_replace("_", " ", $stock['type'])),
                        $stock['source']);
                    $d['detail'] = $type;
                    $d['quantity'] = '';
                    $d['movement_quantity'] = '('.$sign . $stock['movement_qty'].')';
                    $d['balance_quantity'] = '';
                }
                
                array_push($data['data'], $d);
            }
            
            echo json_encode($data);
        }
    }
    
    public function products_download()
    {
        $list = array();
    
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['reference'] = "reference";
        $list['header']['date'] = "date";
        $list['header']['details'] = "details";
        $list['header']['quantity'] = "quantity";
        $list['header']['moved'] = "moved";
        $list['header']['balance'] = "balance";
        $products = $this->stock->getstockMovementReport($this->id_shop,
            null, null,
            's.id_product ASC, s.id_product_attribute ASC, smvt.date_add ASC');

        foreach ($products as $key => $stock) {
            $list[$key]['id_product'] = $stock['id_product'];
            $list[$key]['id_product_attribute'] = $stock['id_product_attribute'];

        //$product = $this->feedbiz->getProductById($this->user_name, $stock['id_product'], $this->iso_code);
        $list[$key]['reference'] = '';
            if (isset($stock['reference'])) {
                $list[$key]['reference'] = $stock['reference'];
            } /*else if(isset( $product->reference)) {
        $list[$key]['reference'] = $product->reference;
        } */
        
        $list[$key]['date'] = $stock['date_add'];

            if ($stock['type'] == StockMovement::update_offer) {
                $type = $this->lang->line('Feed update offer from') .
                    ' ' . $stock['source'];
            } else {
                $type = $this->lang->line('Other');
            }

            $list[$key]['detail'] = $type;
            $list[$key]['quantity'] = $stock['quantity'];

            $sign = '' ;
            if ($stock['action'] == "decrease") {
                $sign = '-';
            } elseif ($stock['action'] == "increase") {
                $sign = '+';
            }
            $list[$key]['moved'] = $sign . $stock['movement_qty'];
            $list[$key]['balance'] = $stock['balance_qty'];

            if ($stock['type'] == StockMovement::import_order) {
                $type = sprintf($this->lang->line('Order from %s (order id:%s)'),
                    $stock['source'], $stock['id_ref']);
                $list[$key]['detail'] = $type;
                $list[$key]['quantity'] = '';
                $list[$key]['moved'] = '('.$sign . $stock['movement_qty'].')';
                $list[$key]['balance'] = '';
            } elseif ($stock['type'] != StockMovement::update_offer) {
                $type = sprintf($this->lang->line('%s from %s'),
                    ucfirst(str_replace("_", " ", $stock['type'])), $stock['source']);
                $list[$key]['detail'] = $type;
                $list[$key]['quantity'] = '';
                $list[$key]['moved'] = '('.$sign . $stock['movement_qty'].')';
                $list[$key]['balance'] = '';
            }
        }
    
        ob_start();
        ob_get_clean();
        $filename = 'feedbiz_stock_report.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) {
            fputcsv($h, $data, ';');
        }
    }
    
    public function report_page($id_product, $id_product_attribute)
    {
        $data = array();
        $stocks = $this->stock->getstockMovementReport($id_product,
            $id_product_attribute, $this->id_shop);
        
        foreach ($stocks as $stock) {
            $data[$stock['id_stock_mvt']]['date_add'] = $stock['date_add'];

            if ($stock['action'] == "decrease") {
                $sign = '-';
            } elseif ($stock['action'] == "increase") {
                $sign = '+';
            }

            if ($stock['type'] == StockMovement::import_order) {
                $type = $stock['source'] . ' order id ' . $stock['id_ref'];
            } elseif ($stock['type'] == StockMovement::update_offer) {
                $type = $this->lang->line('Feed update offer from ') .
                    $stock['source'];
            } else {
                $type = $this->lang->line('Other');
            }

            $data[$stock['id_stock_mvt']]['detail'] = $type;
            $data[$stock['id_stock_mvt']]['movement_qty'] = $sign .
                ' ' . $stock['movement_qty'];
            $data[$stock['id_stock_mvt']]['balance_qty'] =
                $stock['balance_qty'];
        }
        
        echo json_encode($data);
    }
}
