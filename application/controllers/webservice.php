<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

//require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/orders.php');
//require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/stock.php');

class webservice extends CI_Controller
{

    const AMAZON = 'Amazon';
    public $host;

    public function __construct()
    {
        parent::__construct();

        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');

        if ((!isset($this->user_name) || empty($this->user_name))
            && (!isset($this->id_user) || empty($this->id_user))) {
            redirect('users/login');
        }

        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("webservice", $this->language);
        $this->smarty->assign("label", "webservice");
        $this->iso_code = mb_substr($this->language, 0, 2);
    }

    public function get_domains()
    {
        require_once dirname(__FILE__) . '/../libraries/UserInfo/configuration.php';
        require_once dirname(__FILE__) . '/../libraries/UserInfo/feedbiz_otp.php';

        $config_data = $this->my_feed_model->get_general($this->id_user);
        $data = unserialize(base64_decode($config_data['FEED_BIZ']['value']));
        $base_url = '';

        if(isset($data['base_url']))
            $base_url = $data['base_url'];

        $id_user = $this->id_user;

        $config = new UserConfiguration();
        $OTP = new FeedbizOTP();

        $token = $config->getUserCode($id_user);
        $token = str_replace(" ", "_", $token);

        // delete otp
        $OTP->del_otp('connection', '', $token);

        // generate_otp
        $generate_otp = $OTP->generate_otp('connection', '', $token);

        $params = array(
            'base_url' => $base_url,
            'url' => $base_url,
            'token' => $token,
            'otp' => $generate_otp,
        );

        // get url
        $url = $config->prepare_argv($params);

        echo 'Token<pre>' . $token . '</pre>';
        echo 'OTP<pre>' . (md5(md5($generate_otp))) . '</pre>';
        echo $url;
        
        exit();
    }

    public function send_orders($id_order, $debug=false)
    {
        require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/stock.php');
        require_once dirname(__FILE__) . '/../libraries/FeedBiz/config/orders.php';
        require_once dirname(__FILE__) . '/../libraries/UserInfo/configuration.php';
        require_once dirname(__FILE__) . '/../libraries/UserInfo/feedbiz_otp.php';
        
        $error = $output = null;
        
        $id_user = $this->id_user;
        $user_name = $this->user_name;
        
        $config = new UserConfiguration();
        $OTP = new FeedbizOTP();

        $userdata = $config->getShopInfo($id_user);

        if (isset($userdata) && !empty($userdata)) {
            $Orders = new Orders($user_name, null, $debug);

            // Get all send orders
            $token = $config->getUserCode($id_user);
            $token = str_replace(" ", "_", $token);

            if($debug){
                // delete otp
                $OTP->del_otp('order', $id_order, $token);
            }

            // generate_otp
            $order_otp = $OTP->generate_otp('order', $id_order, $token);

            $params = array(
                'base_url' => $userdata['base_url'],
                'url' => $userdata['order_url'],
                'token' => $token,
                'id_order' => $id_order,
                'otp' => $order_otp,
            );

            // get url 
            $url = $config->prepare_argv($params);
            
            if ($debug) {
                echo 'token<pre>' . (md5(md5($order_otp))) . '</pre>';
                echo $url;

                echo '<pre>List Error Order</pre>';
                $error_order_list = $Orders->getOrderErrorStatus($this->id_shop);
                foreach ($error_order_list as $error_order) {
                     echo '<pre>'.print_r($error_order['comment'], true).'</pre>';
                }
                exit();
            }
            
            // Check Order Items
            $OrderItems = new OrderItems($user_name);
            $items = $OrderItems->getOrderItem($id_order);
        
            if (empty($items) || $items == '' || !$items) {
                $Orders->updateOrderErrorStatus($id_order, '0', 'missing_order_items');
                $pass = false;
                $error = sprintf($this->lang->line('Send order #%s status : %s'),
                    $id_order, $this->lang->line('Fail').' - '.
                    $this->lang->line('missing_order_items'));
            } else {
                // send order
                 $result = $Orders->send_orders($url, $id_order);
                $pass = $result['pass'];

                if (!$pass) {
                    $error = sprintf(
                        $this->lang->line('Send order #%s status : %s'),
                        $id_order, $this->lang->line('Fail') . ' - ' .
                        $result['output']);
                } else {
                    $order_list = array();
                    $date = date('Y-m-d H:i:s');
                    $stock = new StockMovement($user_name);

                    foreach ($items as $item) {

                    // check stock movement
                    $inStock = $stock->checkOrderInStockMvt(
                    $id_order, $item['id_product'], $item['id_product_attribute']);

                    if (isset($item) && !empty($item) && !$inStock) {
                        $order_list[$id_order]['items'][$item['id_order_items']] = array(
                            'id_product' => $item['id_product'],
                            'id_product_attribute' => $item['id_product_attribute'],
                            'quantity' => $item['quantity'],
                            'quantity_in_stock' => $item['quantity_in_stock'],
                            'reference' => $item['reference'],
                            'product_name' => $item['product_name'],
                            'time' => $date
                            );
                        }
                    }

                    if (isset($order_list) && !empty($order_list)) {
                        $orderInfo = $Orders->getOrderByID($id_order);
                        $Orders->stockMovement(
                            $userdata['url_stockmovement'],
                            $token,
                            $orderInfo['id_shop'],
                            $id_order,
                            $orderInfo['id_marketplace'],
                            $orderInfo['site'],
                            $order_list,
                            $orderInfo['sales_channel']);
                    }
                }

                $output =  sprintf(
                    $this->lang->line('Send order #%s status : %s'),
                    $id_order, $result['output']);
            }
        
            // delete otp
            $OTP->del_otp('order', $id_order, $token);
        }

        echo json_encode(
        array(
            'error' => $error,
            'pass' => $pass,
            'output' => $output,
            'order' => $id_order,
            'invoice' => isset($result['invoice']) ? $result['invoice'] : '',
            'order_number' => isset($result['order_number']) ?
            $result['order_number'] : ''
        )
    );
    }
    
    public function get_orders($id_order)
    {
        if (isset($this->session->userdata['remote']) && $this->session->userdata['remote'] == true) {
            
            require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/stock.php');
            require_once dirname(__FILE__) . '/../libraries/FeedBiz/config/orders.php';

            $orders = new Orders($this->user_name);
            $orders1 = $orders->getOrdersListById($this->user_name, $id_order);
            
            echo '<pre>' . print_r($orders1, true) . '</pre>';
            exit;
        }
    }

    public function view_order($id_order, $html=false, $is_multichannel=false)
    {
        require_once(dirname(__FILE__) . '/../libraries/FeedBiz/config/stock.php');
        require_once dirname(__FILE__) . '/../libraries/FeedBiz/config/orders.php';

        $orders = new Orders($this->user_name);
        $order = $orders->getOrdersListById($this->user_name, $id_order);
        $afn = $mafn = false;
        
        if(isset($order['Order:Multichannel'])){
            if($order['Order:Multichannel'] == 'AFN') {
                $afn = true;
                $order['Order']['Carrier']['ShipmentService'] = null;
                $order['Order']['Carrier']['ForceImport'] = null;
            }
            if($order['Order:Multichannel'] == 'MAFN'){
                $mafn = true;
            }
        }
        
        // exception for multichannel
        if($is_multichannel){
            $mafn = true;
            unset($order['Order:SalesChannel']);
            unset($order['Order:Multichannel']);
            $order['Order:SalesChannel'] = $order['Order']['Payment']['PaymentMethod'];
            $order['Order']['Payment'] = array();
            unset($order['Order']['Payment']);
            $order['Order']['Invoices'] = array();
            unset($order['Order']['Invoices']);

            $order['Order']['Payment']['Amount'] = $order['Order']['Total']['Paid'];
            $order['Order']['Total'] = array();
            unset($order['Order']['Total']);
        }
        
        unset($order['Order:SalesChannelID']);
        unset($order['Order:ShopID']);
        unset($order['Order:LanguageID']);
        unset($order['Order']['References']['ChannelId']);
        unset($order['Order']['References']['MPNumber']);

        $order['Order']['References']['MarketplaceID'] = $order['Order']['References']['MPReference'];
        unset($order['Order']['References']['MPReference']);
        
        unset($order['Order']['Buyer:ID']);
        unset($order['Order']['Shipping:ID']);
        
        if(isset($order['Order']['Shipping']['Address'] )){
            foreach ($order['Order']['Shipping']['Address'] as $key => $item){
                if(!isset($item) || empty($item) || count($item) <=0){
                    $order['Order']['Shipping']['Address'][$key] = null;
                }
            }
        }
        if(!isset($order['Order']['Shipping']['Name']['Company']) || empty($order['Order']['Shipping']['Name']['Company'])){
            $order['Order']['Shipping']['Name']['Company'] = null;
        }
        
        unset($order['Order']['Invoices:ID']);
        if(isset($order['Order']['Items'])){
            foreach ($order['Order']['Items'] as $key => $item){
                unset($order['Order']['Items'][$key]['Quantity']['InStock']);
                unset($order['Order']['Items'][$key]['Price']['PerUnit']);

                if($is_multichannel){
                    unset($order['Order']['Items'][$key]['MarketplaceProductID']);
                    $order['Order']['Items'][$key]['MarketplaceProductID'] = $order['Order']['Items'][$key]['Product']['ID'];
                }
            }
        }
        unset($order['Order']['Info']['IP']);

        if($order['Order']['Info']['Status'] == 1) {
            $order['Order']['Info']['Status'] = 'Sent';
        } elseif($order['Order']['Info']['Status'] == 0) {
            $order['Order']['Info']['Status'] = 'Error';
        } else {
            $order['Order']['Info']['Status'] = 'Send';
        }

        if(isset($order['Order']['Info']['Comment']) && !empty($order['Order']['Info']['Comment'])) {
            $order['Order']['Info']['Error'] = $order['Order']['Info']['Comment'];
        }
        unset($order['Order']['Info']['Comment']);

        if(isset($order['Order']['Info']['Date']) && !empty($order['Order']['Info']['Date'])) {
            $order['Order']['Info']['UpdateDate'] = $order['Order']['Info']['Date'];
        }
        unset($order['Order']['Info']['Date']);

        if(isset($order['Order']['Info']['ForceImport'])) {
            if($order['Order']['Info']['ForceImport'] == 1){
                $order['Order']['Info']['StockMovement'] = 'success';
            } elseif($afn || $mafn) {
                $order['Order']['Info']['StockMovement'] = 'None';
            } elseif($order['Order']['Info']['ForceImport'] == 0){
                $order['Order']['Info']['StockMovement'] = 'Fail';
            } else {
                $order['Order']['Info']['StockMovement'] = 'Waiting';
            }
        }

        if(isset($order['Order']['Info']['StockMovementDate']) && isset($order['Order']['Info']['ForceImport']) && $order['Order']['Info']['ForceImport'] == 1) {
            $order['Order']['Info']['StockMovementOnDate'] = $order['Order']['Info']['StockMovementDate'];
        }

        unset($order['Order']['Info']['StockMovementDate']);
        unset($order['Order']['Info']['ForceImport']);
        
        if($html){
            $data['order'] = $order;
            $html = $this->smarty->fetch('amazon/order_detail.tpl', $data);

            echo json_encode(array('result' => $html));
            exit;
        }else{
            header('Content-Type: application/xml; charset=utf-8');
            $dom =  new DOMDocument();
            $r = $dom->createElement('Response');
            $dom->appendChild($r);

            arrayToXML($order, $r);
            $content = $dom->saveXML();

            echo $content;
            die;
        }
    }

    public function get_order_by_sellerOrderId($seller_order_id=null){

        require_once dirname(__FILE__) . '/../libraries/FeedBiz/config/orders.php';

        $orders = new Orders($this->user_name, null, true);
        //$order = $orders->getOrderBySellerOrderId($seller_order_id);
	$order_import = new SimpleXMLElement(file_get_contents('/var/www/backend/assets/apps/users/u00000000000009/amazon/order-55131.xml'));
	// set to order table
        $order = $orders->importOrders( $order_import );
	//$order = $feedbiz->setOrder($user, $orders, false);
	if(isset($order['pass']) && $order['pass']){
            var_dump('pass-true', $order); exit;
	}

        var_dump('pass-false',$order); exit;
    }

    public function send_marketplace_configurations($debug = false)
    {
        require_once dirname(__FILE__) . '/../libraries/UserInfo/configuration.php';
        require_once dirname(__FILE__) . '/../libraries/UserInfo/feedbiz_otp.php';
        require_once dirname(__FILE__) . '/../libraries/FeedBiz/Marketplaces.php';
        
        $error = $output = null;
        
        $id_user = $this->id_user;
        
        $config = new UserConfiguration();
        $OTP = new FeedbizOTP();

        $userdata = $config->getShopInfo($id_user);

        if (isset($userdata) && !empty($userdata)) {
            $Marketplaces = new Marketplaces($id_user);

            // Get all send orders
            $token = $config->getUserCode($id_user);
            $token = str_replace(" ", "_", $token);

            // generate_otp
            $otp = $OTP->generate_otp('Marketplace', null, $token);
        
            $otp = md5(md5($otp));
            // delete otp
            //$OTP->del_otp('Marketplaces', $id_user, $token);
        }
                
        $json = json_encode(array('error' => $error, '$otp' => $otp));
        echo $json;
    }
}
