<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class My_feeds extends CI_Controller
{
    
    public $user_name;
    public $id_user;
    public $iso_code;
    public $id_shop;
    public $id_mode;

    public function __construct()
    {
        
        // load controller parent
        parent::__construct();
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');
        
        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz', array($this->user_name));
        $this->load->library('Cipher', array('key'=>$this->config->item('encryption_key')));

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("my_feeds", $this->language);
        $this->smarty->assign("label", "my_feeds");
        $this->iso_code = mb_substr($this->language, 0, 2);
           
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
     
    
    public function general($popup=false)
    {
        $data = array();
        $this->smarty->assign("username", $this->user_name);
        $config_data = $this->my_feed_model->get_general($this->id_user);
        
        foreach ($config_data as $key => $value) {
            $data[strtolower($key)] = unserialize(base64_decode($value['value']));
        }

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->load->model('paypal_model', 'paypal');
        $this->load->model('currency_model', 'currency');
        $this->load->library('mydate');
        $this->load->library('eucurrency');
        $currency = $this->currency->get_user_currency($this->id_user);
        $expert_pk = $this->paypal->get_pk_expert_only($this->id_user);
        $total =0 ;

        foreach ($expert_pk as $pk) {
            $total+= $pk['amt_offer_price_pkg'];
        }

        $thisAmount = $this->mydate->moneySymbol($currency).$this->mydate->floatFormat($this->eucurrency->doConvert($total, $currency));
        $txt_total = sprintf($this->lang->line('Expert Mode will be charged xx per month'), $thisAmount);
        if (empty($currency)) {
            $currency= $this->session->userdata('user_currency');
        }
        
        $data['token'] = $this->my_feed_model->get_token($this->id_user);
        $data['txt_ex_total'] = $txt_total;
        
        if($popup)
            $this->smarty->assign("popup", 1);

        $this->smarty->view('my_feeds/configuration/general.tpl', $data);
    }
    
    public function general_save($popup=false)
    {
        if ($this->input->post()) {
            $value = $this->input->post();
            
            $id_user = $this->id_user ;
            $today  = date('Y-m-d H:i:s', time());
           
            $mode =  array(
                'id_customer' => $id_user,
                'name' => 'FEED_MODE',
                'config_date' => $today,
                'value' => base64_encode(serialize($value))
            );
        
            if (!$this->my_feed_model->set_general($mode)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                if(isset($value['token'])){
                    if (!$this->my_feed_model->set_token($value['token'], $this->id_user)) {
                        $this->session->set_userdata('error', 'save_unsuccessful');
                    } else {
                        $this->session->set_userdata('message', 'save_successful');
                    }
                }
            }
        }
        
        helper_redirect('', isset($value[$value['save']]) ? $value[$value['save']] : 'my_feeds/configuration/general/'.$popup);
    }
    
    public function shops()
    {
        $data = array();
        $this->smarty->assign("username", $this->user_name);
        $this->smarty->assign("id_user", $this->id_user);
        
        $shop_data = $this->feedbiz->getShops($this->user_name);
        $shop_data_offer = $this->feedbiz->getOfferShops($this->user_name);
        
        if (isset($shop_data) && !empty($shop_data)) {
            foreach ($shop_data as $product_shop) {
                $data['shop'][$product_shop['id_shop']] = $product_shop;
                
                foreach ($shop_data_offer as $offer_shop) {
                    if ($product_shop['id_shop'] == $offer_shop['id_shop']) {
                        $data['shop'][$product_shop['id_shop']]['offer'] = true;
                        $data['shop'][$product_shop['id_shop']]['offer_link'] = $offer_shop['description'];
                    }
                }
            }
        }
        
        //echo '<pre>' . print_r($data, true) . '</pre>'; exit;
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/configuration/shops.tpl', $data);
    }
    
    public function shops_save()
    {
        if ($this->input->post()) {
            $value = $this->input->post();
            
            $shop = $this->feedbiz->setDefaultShop($this->user_name, $value);
            
            if (!$shop) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('message', $this->lang->line('Change default shop successful'));
                $this->session->set_userdata('id_shop', $value['id_shop']);
                $this->session->set_userdata('shop_name', $shop);
            }
        }
        
        $this->load->library('user_agent');
        if ($this->agent->is_referral()) {
            redirect($this->agent->referrer());
        }
    }

    public function verify_feed()
    {
        $this->authentication->validateAjaxToken();
        sleep(1);
        $id_user = $this->id_user;
         
        $url_feed_setting =  $this->input->post('url');
        if (strpos($url_feed_setting, 'http://') ===false && strpos($url_feed_setting, 'https://') ===false) {
            $url_feed_setting=  ('http://'.$url_feed_setting);
        }
        $_h = parse_url($url_feed_setting);
        if (isset($_h['host'])) {
            $host = $_h['host'];
            if (preg_match("/^(?!\-)(?:[a-zA-Z\d\-\_]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/", $host)&& preg_match("/^.{1,253}$/", $host)&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $host)) {
                if ($this->my_feed_model->check_exist_domain($id_user, base64_encode($host))) {
                    exit;
                }
            } elseif (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $host)&& preg_match("/^.{1,253}$/", $host)&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $host)) {
                if ($this->my_feed_model->check_exist_domain($id_user, base64_encode($host))) {
                    exit;
                }
            } elseif (checkdnsrr($host)) {
                if ($this->my_feed_model->check_exist_domain($id_user, base64_encode($host))) {
                    exit;
                }
            } else {
                exit;
            }
        } else {
            exit;
        }
        
        $token = $this->my_feed_model->get_token($id_user);
        
        $add_url = "&fbtoken=$token&fbuser=".$this->user_name."&fbdomain=".$this->config->item('base_url');
        $add='&';
        if (strpos($url_feed_setting, '?') ===false) {
            $add = '?';
        }
        $add_url = $add."fbtoken=$token&fbuser=".$this->user_name."&fbdomain=".$this->config->item('base_url');
        
//        $file_headers = @get_headers($url_feed_setting.$add_url);
//        if($file_headers[0] == 'HTTP/1.1 404 Not Found') { echo "Not Found"; }

        require_once dirname(__FILE__) . '/../libraries/UserInfo/feedbiz_otp.php';
                    
        $OTP = new FeedbizOTP();
        $OTP->del_otp('connection', '', $token);
        $order_otp = $OTP->generate_otp('connection', '', $token);
        if(!empty($order_otp)){
            $add_url .='&otp='.$order_otp;
        }

        try {
            include_once(dirname(__FILE__). "/../../libraries/feedbiz_override_tools.php");
//            $out = curl_get_contents($url_feed_setting.$add_url);
              $url=$url_feed_setting.$add_url;
              $out = fbiz_get_contents($url,  array('curl_method'=>1,'timeout'=>600));
//
//            $out = @file_get_contents($url_feed_setting.$add_url);


            if (empty($out)) {
                echo $this->lang->line('empty_connector_data');
//                 echo base64_encode(base64_encode(base64_encode($url_feed_setting.$add_url)));
                return;
            }

            $doc = new DOMDocument();
            @$doc->loadXML($out);
            $err = libxml_get_errors($doc);
            if (!empty($err)) {
                file_put_contents(dirname(__FILE__) . '/../../assets/apps/users/'.$this->user_name.'/xml_log/verify_error_'.date('Ymd_His').'.xml', array($url,$out,$err));
                return;
            }
            
            $error_messages = $doc->getElementsByTagName('Error');
            foreach ($error_messages as $error_message) {
                echo $error_message->nodeValue;
                return;
            }
           
            $conn_elm = $doc->getElementsByTagName('Connector');
            $shop = array();
             
            foreach ($conn_elm as $n) {
                $shop['name'] = $n->getAttribute('ShopName');
            }
            $conn_elm = $doc->getElementsByTagName('Software');
            foreach ($conn_elm as $n) {
                $shop['software']=$n->getAttribute('Name');
                $shop['version']=$n->getAttribute('Version');
            }
            $conn_elm = $doc->getElementsByTagName('Urls');
            foreach ($conn_elm as  $node) {
                foreach ($node->childNodes as $child) {
                    $shop['url'][strtolower($child->nodeName)] = $child->nodeValue;
                }
            }
            
            $id_user = $this->id_user;
            $today  = date('Y-m-d H:i:s', time());
            $feedbiz =  array(
                'id_customer' => $id_user,
                'name' => 'FEED_SHOP_INFO',
                'config_date' => $today,
                'value' => base64_encode(serialize($shop))
            );
            
//            echo '<pre>' . print_r($encrype_data, true) . '</pre>'; exit;

            if ($this->my_feed_model->set_general($feedbiz)&& isset($shop['name']) && $shop['name']!='') {
                echo 'verified';
                $this->my_feed_model->delete_general(array('name'=>'FEED_CATEGORY', 'id_customer'=>$this->id_user));
            } else {
            }
        } catch (Exception $ex) {
            //Process the exception
        }
    }
    public function data_feed()
    {
        $data = array();
        
        $config_data = $this->my_feed_model->get_general($this->id_user);
        
        foreach ($config_data as $key => $value) {
            if (strtolower($key) != "feed_mode") {
                $data[strtolower($key)] = unserialize(base64_decode($value['value']));
            }
        }
        $data['feed_token']=$this->my_feed_model->get_token($this->id_user);
        $out = array();
        $out['data']=($data);
        $out['verified'] = $data['feed_biz']['verified'];
        if ($out['verified']!='') {
            $this->my_feed_model->insert_last_import($this->id_user, $this->user_name);
        }
        echo json_encode($out);
    }
    
    public function data_source($popup=false)
    {
        //echo phpinfo();
        //$this->authentication->setMarkRedirect();
        redirect('my_shop/configuration');exit;
        $data = array();
        $data['start_feed'] = "false";
                         
        $config_data = $this->my_feed_model->get_general($this->id_user);
        $data['feed_mode']= $this->id_mode;
        $data['feed_biz']=array();
        foreach ($config_data as $key => $value) {
            if (strtolower($key) != "feed_mode") {
                $data[strtolower($key)] = unserialize(base64_decode($value['value']));
            }
        }
        
        //echo '<pre>' . print_r($data, true) . '</pre>'; exit;
        /*if( $this->session->userdata('feed') )
        {
             $data['start_feed'] = "true";
             $this->session->unset_userdata('feed');
        }*/
        if ($this->session->userdata('feed')&& isset($data['feed_biz']['verified']) && isset($data['feed_biz']['verified'])=="true") {
            $data['start_feed'] = "true";
            $this->session->unset_userdata('feed');
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        if ($this->session->userdata('dMsg')) {
            $data['message'] = ($this->session->userdata('dMsg'));
            $this->session->unset_userdata('dMsg');
        }
        if (!is_dir(USERDATA_PATH . $this->user_name)) {
            $data['start_feed'] = "false";
            $data['profile'] = urlencode(json_encode(array('username' => $this->user_name, 'id' => $this->id_user)));
        }
        if ($popup!==false) {
            $this->smarty->assign('popup', $popup);
            $this->smarty->assign('allow_progress_in_iframe', $popup);
            
            $this->session->set_userdata('data_source_popup', $popup);
        } else {
            $this->session->unset_userdata('data_source_popup');
        }
        
        $userdata = $this->authentication->user($this->id_user)->row();
        $data['status'] = $userdata->user_cronj_status;
        $this->load->model('my_feed_model');
        $data['history'] = $this->my_feed_model->get_histories($this->id_user);
        
        $this->smarty->view('my_feeds/configuration/data_source.tpl', $data);
    }
    
    public function data_source_save()
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
            
            $id_user = $this->id_user;
            $username = $this->user_name;
            
            if (!isset($id_user) || !isset($username) || empty($id_user) || empty($username)) {
                $this->session->set_userdata('error', 'incorrect_user');
                redirect('my_feeds/configuration/data_source');
            }
            
            $today  = date('Y-m-d H:i:s', time());
            
            $encrype_data = array();
            $encrype_data['source'] = $values['source'];
            //$encrype_data['feed_source'] = $values['feed_source'];
            $encrype_data['base_url'] = $values['base_url'];
            $encrype_data['username'] =  $username;
            $encrype_data['verified'] = $values['verified'];
            
            if (trim($values['base_url']) !='') {
                $_h = parse_url($values['base_url']);
                if (isset($_h['host'])) {
                    $host = $_h['host'];
                    if (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $host)&& preg_match("/^.{1,253}$/", $host)&& preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $host)) {
                        if (!$this->my_feed_model->check_exist_domain($id_user, base64_encode($host))) {
                            $feedbiz =  array(
                            'id_customer' => $id_user,
                            'name' => 'FEED_BASE_URL_1',
                            'config_date' => $today,
                            'value' => base64_encode($host)
                        );
                            $this->my_feed_model->set_general($feedbiz);
                            $duplicate_domain=false;
                        } else {
                            $encrype_data['base_url'] ='';
                            $duplicate_domain=true;
                        }
                    }
                }
            }
            $duplicate_domain=false;
            /*if(isset($values['feed_type_product']) && !empty($values['feed_type_product']))
            {
                $encrype_data['product_token'] = $this->get_token();
                //$encrype_data['product']['fileurl'] = $values['product_file_url'];
                $encrype_data['product'] = 'checked';
            }
            
            if(isset($values['feed_type_offer']) && !empty($values['feed_type_offer']))
            {
                $encrype_data['offer_token'] = $this->get_offer_token();
                //$encrype_data['offer']['fileurl'] = $values['offer_file_url'];
                $encrype_data['offer'] = 'checked';
            }*/
            
            if (isset($values['gmerchant_file_url']) && !empty($values['gmerchant_file_url'])) {
                //Product
                $encrype_data['product']['fileurl'] = $values['gmerchant_file_url'];
                $encrype_data['product_token'] = $this->get_token();
                $encrype_data['product']['language'] =  $values['language'];
                $encrype_data['product']['is_default'] =  $values['is_default'];
                $encrype_data['product']['iso_code'] =  $values['iso_code'];
                $encrype_data['product']['curr_name'] =  $values['curr_name'];
                $encrype_data['product']['curr_iso'] =  $values['curr_iso'];
                
                //Offer
                $encrype_data['offer']['fileurl'] = $values['gmerchant_file_url'];
                $encrype_data['offer_token'] = $this->get_offer_token();
                $encrype_data['offer']['language'] =  $values['language'];
                $encrype_data['offer']['is_default'] =  $values['is_default'];
                $encrype_data['offer']['iso_code'] =  $values['iso_code'];
                $encrype_data['offer']['curr_name'] =  $values['curr_name'];
                $encrype_data['offer']['curr_iso'] =  $values['curr_iso'];
            }
            if (isset($values['mode'])) {
                $this->authentication->update($this->id_user, array('user_cronj_status'=>$values['mode']));
            }
            $feedbiz =  array(
                'id_customer' => $id_user,
                'name' => 'FEED_BIZ',
                'config_date' => $today,
                'value' => base64_encode(serialize($encrype_data))
            );
            
//            echo '<pre>' . print_r($encrype_data, true) . '</pre>'; exit;

            if ($duplicate_domain) {
                $this->session->set_userdata('error', 'duplicate_domain_with_other_user');
            } elseif (!$this->my_feed_model->set_general($feedbiz)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('feed', true);
                $this->session->set_userdata('message', 'save_successful');
            }   //echo "true";
        }
        if ($this->session->userdata('data_source_popup')) {
            $tmp = $this->session->userdata('data_source_popup');
            $this->session->unset_userdata('data_source_popup');
            //redirect('my_feeds/configuration/data_source/'.$tmp);
            redirect("dashboard/iframe_popup_conf/next", 'refresh');
        } else {
            redirect('my_feeds/configuration/data_source');
        }
//        if($this->id_mode == 1)
//            redirect('my_feeds/parameters/products/filters');
//        else    
//            redirect('my_feeds/parameters/products/carriers');
    }
    public function import_history()
    {
        $this->load->model('my_feed_model');
        //$data['history'] = $this->my_feed_model->get_histories($this->id_user); 

        $this->smarty->view('my_feeds/logs/import_history.tpl');
    }
    
    public function import_history_page()
    {
        
         //var_dump($type);exit;
        $values = $this->input->post();
         
        $search = array();
        if (isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            
            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "history_table_name"; break;
                    case 1 : $column = "history_action"; break;
                    case 2 : $column = "user_name"; break;
                    case 3 : $column = "history_date_time"; break;
                }
                $sort = $column . ' ' . $order_sort['dir'];
            }
            
            if (isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search = $values['search']['value'];
            }
            
            //$order_by = implode(', ', $sort);
            //$id, $key = 'import_', $limit, $start, $order_by, $keywords, $num_rows
            $logs_rows = $this->my_feed_model->get_histories($this->id_user, 'import_', null, $start, null, $search, true);
            $logs = $this->my_feed_model->get_histories($this->id_user, 'import_', $limit, $start, $sort, $search, false);
            
            $data = $error_log = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $logs_rows;
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = $logs;
            
            echo json_encode($data);
        }
    }
        
    public function carriers()
    {
        $data = array();
        
        $data['carriers'] = $this->feedbiz->getSelectedCarriers($this->user_name, $this->id_shop, $this->id_mode);
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        
        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('my_feeds/parameters/products/carriers.tpl', $data);
    }
    
    public function carriers_save()
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
        // var_dump($values); exit;

            $session_key = time();
            $data_insert = array();

            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        foreach ($value as $k => $val) {
                            if (isset($val) && !empty($val)) {
                                $data_insert[$k]['id_carrier'] = $val;
                                $data_insert[$k]['id_shop'] = $this->id_shop;
                                $data_insert[$k]['id_mode'] = $this->id_mode;
                                $data_insert[$k]['session_key'] = $session_key;
                            }
                        }
                    }
                }
                 
                $result = $this->feedbiz->saveSelectedCarriers($this->user_name, $data_insert, $this->id_shop, $this->id_mode);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        } else {
            if (!$this->feedbiz->truncate_table($this->user_name, 'carrier_selected WHERE id_shop = ' . $this->id_shop . ' AND id_mode = ' . $this->id_mode)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('message', 'save_successful');
            }
        }
        
        helper_redirect('my_feeds/parameters/', isset($values['save'])&&isset($values[$values['save']]) ? $values[$values['save']] : 'carriers');
    }
    
    public function attributes()
    {
        $data = array();

        $data['m_attribute'] = $this->feedbiz->getMappingAttribute($this->user_name, $this->id_shop, $this->id_mode);
        $data['m_row'] =  $data['m_attribute']['row'];
        $attributes = $this->feedbiz->getAttributeValuesExcludeMappingValues($this->user_name, $this->iso_code, $this->id_shop, $this->id_mode);
        
        //echo '<pre>' . print_r($attributes, true) .'</pre>';
        $data['attribute'] = $attributes;

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/parameters/products/attributes.tpl', $data);
    }
    
    public function attributes_save()
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
            
            $data_insert = array();
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        foreach ($value as $k => $val) {
                            if (isset($val) && !empty($val)) {
                                $data_insert[$k][$key] = $val;
                                $data_insert[$k]['id_shop'] = $this->id_shop;
                                $data_insert[$k]['id_mode'] = $this->id_mode;
                            }
                        }
                    }
                }
                $result = $this->feedbiz->saveMappingAttribute($this->user_name, $data_insert, $this->id_shop, $this->id_mode);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        } elseif (!$this->feedbiz->truncate_table($this->user_name, 'mapping_attribute WHERE id_shop = '. $this->id_shop)) {
            $this->session->set_userdata('error', 'save_unsuccessful');
        } else {
            $this->session->set_userdata('message', 'save_successful');
        }
            
        redirect('my_feeds/parameters/products/mapping/attributes');
    }
    
    public function characteristics()
    {
        $data = array();

        $data['characteristics'] = $this->feedbiz->getMappingCharacteristics($this->user_name, $this->id_shop, $this->id_mode);
        $data['characteristics_row'] =  $data['characteristics']['row'];

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/parameters/products/characteristics.tpl', $data);
    }
    
    public function characteristics_save()
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
            $data_insert = array();
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        foreach ($value as $k => $val) {
                            if (isset($val) && !empty($val)) {
                                $data_insert[$k][$key] = $val;
                                $data_insert[$k]['id_shop'] = $this->id_shop;
                                $data_insert[$k]['id_mode'] = $this->id_mode;
                            }
                        }
                    }
                }
                $result = $this->feedbiz->saveMappingCharacteristics($this->user_name, $data_insert, $this->id_shop, $this->id_mode);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        } elseif (!$this->feedbiz->truncate_table($this->user_name, 'mapping_characteristic WHERE id_shop = '. $this->id_shop)) {
            $this->session->set_userdata('error', 'save_unsuccessful');
        } else {
            $this->session->set_userdata('message', 'save_successful');
        }
            
            
        redirect('my_feeds/parameters/products/mapping/characteristics');
    }
    
    public function manufacturers()
    {
        $data = array();
        
        $manufacturers = $this->feedbiz->getManufacturerExcludeMapping($this->user_name, $this->id_shop, $this->id_mode);
        $data['manufacturer'] = $manufacturers;
        $data['m_manufacturer'] = $this->feedbiz->getMappingManufacturer($this->user_name, $this->id_shop, $this->id_mode);
        $data['m_row'] =  $data['m_manufacturer']['row'];
        
        if ($this->session->userdata('error')) {
            $data['error'] =  $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }

        $this->smarty->view('my_feeds/parameters/products/manufacturers.tpl', $data);
    }
    
    public function manufacturers_save()
    {
        if ($this->input->post()) {
            $values = $this->input->post() ;
            $data_insert = array();
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        foreach ($value as $k => $val) {
                            if (isset($val) && !empty($val)) {
                                $data_insert[$k][$key] = $val;
                                $data_insert[$k]['id_shop'] = $this->id_shop;
                                $data_insert[$k]['id_mode'] = $this->id_mode;
                            }
                        }
                    }
                }
                $result = $this->feedbiz->saveMappingManufacturers($this->user_name, $data_insert, $this->id_shop, $this->id_mode);
                if (!$result) {
                    $this->session->set_userdata('error', 'Enter Appropriate manufacture name');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        } elseif (!$this->feedbiz->truncate_table($this->user_name, 'mapping_manufacturer WHERE id_shop = '. $this->id_shop)) {
            $this->session->set_userdata('error', 'save_unsuccessful');
        } else {
            $this->session->set_userdata('message', 'save_successful');
        }
        
        helper_redirect('my_feeds/parameters/', isset($values[$values['save']]) ? $values[$values['save']] : 'mapping/manufacturers');
    }
    
    public function filters()
    {
        $data = array();

        $data['include_manufacturers'] = $this->feedbiz->getFilterManufacturers($this->user_name, $this->id_shop, $this->id_mode);
        $data['exclude_manufacturers'] = $this->feedbiz->getExcludeManufacturers($this->user_name, $this->id_shop, $this->id_mode);
        $data['include_suppliers'] = $this->feedbiz->getFilterSuppliers($this->user_name, $this->id_shop, $this->id_mode);
        $data['exclude_suppliers'] = $this->feedbiz->getExcludeSuppliers($this->user_name, $this->id_shop, $this->id_mode);

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/parameters/products/filters.tpl', $data);
    }
    
    public function filters_save()
    {
        if ($this->input->post()) {
            $inputs = $this->input->post() ;
        
       
            if (!isset($inputs) || empty($inputs)) {
                redirect('my_feeds/parameters/products/filters');
            }
             
            $session_key = date('Y-m-d H:i:s');
            $data_insert = array();
            
            foreach ($inputs as $input_key => $values) {
                if (is_array($values)) {
                    foreach ($values as $key => $value) {
                        $data_insert[$input_key][$key]['id'] = $value;
                        $data_insert[$input_key][$key]['session_key'] = $session_key;

                        $data_insert[$input_key][$key]['id_shop'] = $this->id_shop;
                        $data_insert[$input_key][$key]['id_mode'] = $this->id_mode;
                    }
                }
            }
            $result = $this->feedbiz->savefilters($this->user_name, $data_insert);
            
            if (!$result) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('message', 'save_successful');
            }
        } else {
            if (!$this->feedbiz->truncate_table($this->user_name, 'exclude_manufacturer')) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            }
            
            if (!$this->feedbiz->truncate_table($this->user_name, 'exclude_supplier')) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            }
        }
    
        helper_redirect('my_feeds/parameters/', isset($inputs[$inputs['save']]) ? $inputs[$inputs['save']] : 'filters');
    }
    
    public function price_rules()
    {
        $data = array();

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/parameters/products/price_rules.tpl', $data);
    }
    
    public function get_token()
    {
        $user_data = $this->authentication->user($this->id_user)->row();
        $token = $this->feedbiz->get_token($user_data->user_name);
        
        if (isset($token) && !empty($token)) {
            return ($token);
        }
    }
    
    public function get_offer_token()
    {
        $user_data = $this->authentication->user($this->id_user)->row();
        $token = $this->feedbiz->get_offer_token($user_data->user_name);
        
        if (isset($token) && !empty($token)) {
            return ($token);
        }
    }
    
    //move from validation

    
    
    public function statistics()
    {
        $data = array();
        
        $config_data = $this->my_feed_model->get_general($this->id_user);
        
        if (isset($config_data) && !empty($config_data)) {
            foreach ($config_data as $key => $value) {
                if (strtolower($key) != "feed_mode") {
                    $data[strtolower($key)] = unserialize(base64_decode($value['value']));
                }
            }
        
            if (isset($config_data['FEED_BIZ']) && !empty($config_data['FEED_BIZ'])) {
                $data['feed_biz']['shop']   = $this->feedbiz->getDefaultShop($this->user_name);
                $data['feed_biz']['time']   = $config_data['FEED_BIZ']['config_date'];
            }
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/logs/statistics.tpl', $data);
    }
    
    public function messages_page($type)
    {
        $values = $this->input->post();
        /*$values = array(
            'draw' => 1,
            'length' => 10,
            'start' => 0,
            'search' => array('value'=>'Size L'),
            'order' => array(0 => array('column'=>0,'dir'=>'asc')),
        );*/
        
        $search = array();
        if (isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $sort = array();
            $column = '';
            
            foreach ($values['order'] as $key => $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "id_product"; break;
                    case 1 : $column = "name"; break;
                    case 2 : $column = "shop"; break;
                    case 3 : $column = "type"; break;
                    case 4 : $column = "message"; break;
                    case 5 : $column = "date_add"; break;
                }
                $sort[$key]['field'] = $column;
                $sort[$key]['dir'] = $order_sort['dir'];
            }
            
            if (isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search = $values['search']['value'];
            }
            
            //$order_by = implode(', ', $sort);
            $ignore_case = false;
            if (isset($values['ignore_case']) && ($values['ignore_case']=='true')) {
                $ignore_case = true;
            }
            if ($type == 'product') {
                $logs_rows = $this->feedbiz->get_log_product($this->user_name, $this->id_shop, $search, $sort, null, null, true, false, $ignore_case);
                $logs = $this->feedbiz->get_log_product($this->user_name, $this->id_shop, $search, $sort, $limit, $start, false, false, $ignore_case);
            } elseif ($type == 'offer') {
                //$user, $id_shop, $limit, $start, $order_by, $keywords, $num_rows
                $logs_rows = $this->feedbiz->get_log_offer($this->user_name, $this->id_shop, null, null, $sort, $search, true, false, $ignore_case);
                $logs = $this->feedbiz->get_log_offer($this->user_name, $this->id_shop, $limit, $start, $sort, $search, false, false, $ignore_case);
            }
            
            $data = $error_log = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $logs_rows;
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            foreach ($logs as $log) {
                array_push($data['data'], $log);
            }
            echo json_encode($data);
        }
    }
    
    public function messages()
    {
        $data = array();
        $data['shop'] = $this->feedbiz->getShops($this->user_name);
        $data['type_products'] = 'product';
        $data['type_offers'] = 'offer';
        $get = $this->input->get();

        $data['search'] = !empty($get['batch_id'])?$get['batch_id']:'';
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('my_feeds/logs/messages.tpl', $data);
    }
    
    public function batches()
    {
        $data = array();
//        $data['logs'] = $this->feedbiz->getImportLog($this->user_name);
//                
//        if( $this->session->userdata('error') )
//        {
//             $data['error'] = $this->lang->line($this->session->userdata('error'));
//             $this->session->unset_userdata('error');
//        }
//
//        if( $this->session->userdata('message') )
//        {
//             $data['message'] = $this->lang->line($this->session->userdata('message'));
//             $this->session->unset_userdata('message');
//        }

        $this->smarty->view('my_feeds/logs/batches.tpl', $data);
    }
    
    public function batches_page()
    {
        
         //var_dump($type);exit;
        $values = $this->input->post();
        $column='datetime';
        $search = array();
        if (isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
                    
            foreach ($values['order'] as $order_sort) {
                switch ($order_sort['column']) {
                    case 0 : $column = "batch_id"; break;
                    case 1 : $column = "source"; break;
                    case 2 : $column = "shop"; break;
                    case 3 : $column = "type"; break;
                    case 4 : $column = "no_total"; break;
                    case 5 : $column = "no_success"; break;
                    case 6 : $column = "no_warning"; break;
                    case 7 : $column = "no_error"; break;
                    case 8 : $column = "datetime"; break;
                }
                $sort = $column . ' ' . $order_sort['dir'];
            }
            
            if (isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search = $values['search']['value'];
            }
            
            //$order_by = implode(', ', $sort);
            //$id, $key = 'import_', $limit, $start, $order_by, $keywords, $num_rows
            $logs_rows = $this->feedbiz->getImportLog($this->user_name, null, $start, null, $search, true);
            $logs = $this->feedbiz->getImportLog($this->user_name, $limit, $start, $sort, $search, false);
//            $logs_rows = $this->my_feed_model->get_histories($this->id_user, 'import_', null, $start, null, $search, true);
//            $logs = $this->my_feed_model->get_histories($this->id_user, 'import_', $limit, $start, $sort, $search, false);

            $data = $error_log = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $logs_rows;
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = $logs;
            
            echo json_encode($data);
        }
    }
    
    public function log_last_import($type, $shop)
    {
        $shop= html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($shop)), null, 'UTF-8');
        $logs = $this->feedbiz->get_last_import_log($this->user_name, $type, $shop);
        
        if (isset($logs) && !empty($logs)) {
            echo json_encode($logs);
        } else {
            echo json_encode(array("error" => "true" ));
        }
    }
    
    public function log_download($type, $batch_id)
    {
        // echo '<pre>' . print_r($batch_id, true) . '</pre>'; exit;
        $this->feedbiz->downloadLog($this->user_name, $type, $batch_id);
    }
    
    public function log_download_products($param)
    {
        $data = null;
        
//        echo '<pre>' .print_r($param, true) . '</pre>'; exit;
        $decode= html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($param)), null, 'UTF-8');
        $value = json_decode($decode);
        
        if (isset($value) && !empty($value)) {
            $data = array(
                "batch_id" => $value->batch_id,
                "id_product" => $value->id_product,
                "id_shop" => (int)$value->id_shop,
                "severity" => $value->type,
                "message" => $value->message,
                "date_add" => $value->date_add,
            );
        }
        //echo '<pre>' . print_r($data, true) . '</pre>'; exit;
        $this->feedbiz->downloadLog($this->user_name, 'products', $data);
    }
    
    public function log_download_offers($param)
    {
        $data = null;
        $decode= html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($param)), null, 'UTF-8');
        $value = json_decode($decode);
        
        if (isset($value) && !empty($value)) {
            $data = array(
                "batch_id" => $value->batch_id,
                "id_product" => $value->id_product,
                "id_shop" => (int)$value->id_shop,
                "severity" => $value->type,
                "message" => $value->message,
                "date_add" => $value->date_add,
            );
        }
        
        $this->feedbiz->downloadLog($this->user_name, 'offers', $data) ;
    }
    
    public function condition()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        $this->load->model('marketplace_model', 'mk');
        $id_shop = $this->feedbiz->getDefaultShop($this->user_name);
        $id_shop = $id_shop['id_shop'];
        $this->load->model('paypal_model', 'paypal');
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
        $amz = $ebay = false;
        $amazon_id = 2;
        $ebay_id = 3;
        foreach ($offerUserPackage as $pk) {
            if ($pk['id_offer_pkg']==$amazon_id) {
                $amz = true;
            }
            if ($pk['id_offer_pkg']==$ebay_id) {
                $ebay = true;
            }
        }
        if ($amz) {
            $mk_cond['amazon'] = $this->mk->getMarketplaceCondition($amazon_id);
            $shop_condition['amazon'] = $this->feedbiz->getConditionMapping($this->user_name, $id_shop, $amazon_id);
        }
        if ($ebay) {
            $mk_cond['ebay'] = $this->mk->getMarketplaceCondition($ebay_id);
            $shop_condition['ebay'] = $this->feedbiz->getConditionMapping($this->user_name, $id_shop, $ebay_id);
        }
        if (sizeof($mk_cond)==0) {
            $data['no_market'] = true;
        } else {
            $data['mk_condition'] = $mk_cond;
            $data['shop_condition'] = $shop_condition;
        }
        
        
        
        $this->smarty->view('my_feeds/condition.tpl', $data);
    }
    public function condition_save()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        $this->load->model('marketplace_model', 'mk');
        $id_shop = $this->feedbiz->getDefaultShop($this->user_name);
        $id_shop = $id_shop['id_shop'];
        $post = $this->input->post();
        if (!empty($post)) {
            foreach ($post['cond'] as $key => $val) {
                $cond = $this->mk->getMarketplaceConditionByID($key);
                if (!empty($val)) {
                    $out = $this->feedbiz->setConditionMapping($this->user_name, $id_shop, $cond['id_marketplace'], $cond['condition_value'], $val);
                } else {
                    $out = $this->feedbiz->removeConditionMapping($this->user_name, $id_shop, $cond['id_marketplace'], $cond['condition_value']);
                }
            }
        }
        redirect('my_feeds/parameters/profiles/condition');
    }
    public function my_products()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        
         
        $data['shop_condition'] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop);
         
        $this->smarty->view('my_feeds/my_products.tpl', $data);
    }
    public function getJsonCategory($type='')
    {
        $this->authentication->validateAjaxToken();
         //$this->authentication->validateAjaxToken();
         $this->load->library('FeedBiz', array($this->user_name));
        echo $this->feedbiz->getJsonAllCategory($this->user_name, $this->id_shop, null, $type);
    }
    public function my_product_data()
    {
        //$this->authentication->validateAjaxToken();
        $this->load->library('FeedBiz', array($this->user_name));
        $shop = $this->session->userdata('id_shop');
        $mode = $this->session->userdata('mode_default');
        $this->load->model('currency_model', 'currency');
        $this->load->library('Eucurrency');
        $this->load->library('mydate');
        
        $currency = $this->currency->get_user_currency($this->id_user);
        $currencySign = $this->mydate->moneySymbol($currency);
        
        $out = $this->feedbiz->exportProductsForTableList($this->user_name, $shop['id_shop'], $mode);
        foreach ($out as $k=>$o) {
            $cur = $o['curr'];
            foreach ($o['items']as $i => $d) {
                $out[$k]['items'][$i]['price'] = $currencySign.''.number_format($this->eucurrency->doDiffConvert($d['price'], $cur, $currency), 2);
            }
        }
        
        echo json_encode(array('data'=>$out));
    }
}
