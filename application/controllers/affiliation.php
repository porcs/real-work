<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class affiliation extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        error_reporting(E_ALL);
          
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');

        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('Cipher',
            array('key'=>$this->config->item('encryption_key')));

        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("user", $this->language);
        $this->smarty->assign("label", "user");
        $this->iso_code = mb_substr($this->language, 0, 2);

        $this->smarty->assign('siteurl', site_url());
        _get_message_document($this->router->fetch_class(),
            $this->router->fetch_method());
    }
    
    public function statistic()
    {
        $data['userData'] = (array)$this->authentication->user($this->id_user)->row();
        
        $this->load->model('referrals_model', 'referrals_model');
        $this->load->library('mydate');

        $userID = $data['userData']['id'];
        
        /* Get datetime length */
        $regTimestamp = strtotime($data['userData']['user_created_on']);
        $firstDayRegTimestamp = strtotime(date('Y-m', $regTimestamp).'-1');
        $nowTimestamp = time();
        
        $diff = $this->mydate->getDateDiff($firstDayRegTimestamp, $nowTimestamp);
        
        $monthLength = ceil(doubleval($diff->format('%m.%d')));
        
        if ($monthLength > 6) {
            $monthLength = 6;
        }
        
        $arrMonthLength = $this->mydate->getDateLastMonth($nowTimestamp, $monthLength);
        
        if (!empty($arrMonthLength)) {
            $arrMonth =
                $this->mydate->getDatePeriod($arrMonthLength['start'],
                    $arrMonthLength['end']);
            
            if (!empty($arrMonth)) {
                $arrStat = array();
                foreach ($arrMonth as $val) {
                    // init variable
                    $tmpStartDate = null;
                    $tmpEndDate = null;
                    $tmpQueryReferral = array();
                    $tmpArrValue = array();
                    
                    if (date('m-Y', $val)==date('m-Y')) {
                        $tmpStartDate = $val;
                        $tmpEndDate = time();
                    } elseif (date('m-Y', $val)==date('m-Y', $regTimestamp)) {
                        $tmpStartDate = $regTimestamp;
                        $tmpEndDate = strtotime(date('Y-m-t', $val));
                    } else {
                        $tmpStartDate = $val;
                        $tmpEndDate = strtotime(date('Y-m-t', $val));
                    }
                    $tmpQueryReferral[$userID]['child'] =
                        $this->referrals_model->recurringUsers(
                            $userID,
                            $tmpStartDate,
                            $tmpEndDate);
                    $comm = $this->referrals_model->getUserCommission(
                        $userID,
                        $tmpStartDate,
                        $tmpEndDate);
                   
                    if (!empty($comm)) {
                        //$tmpArrValue = $this->model->getTotalResult($tmpQueryReferral[$userID],$userID);
                        $tmpArrValue = $comm;
                        $tmpArrValue['startDate'] = $tmpStartDate;
                        $tmpArrValue['endDate'] = $tmpEndDate;
                        $tmpArrValue['month'] = date('F y', $tmpStartDate);
                        $tmpArrValue['link']=
                            '/users/affiliation/listReferrals?month='.
                            date('Y-m', $tmpStartDate);
                        $arrStat[] = $tmpArrValue;
                    }
                }
            }
        }
        /* end */
        $graph ='';
      
        foreach ($arrStat as $v) {
            $tol =$v['sale']+$v['tier'];
            $graph .='["'.$v['month'].'",'.$tol.'],';
        }
        $data['graph']=$graph;
        $this->smarty->assign('arrStat', $arrStat);

        $this->smarty->view('affiliation/statistic.tpl', $data);
    }
    
    public function invite_friend()
    {
        $data['userData'] = (array)$this->authentication->user($this->id_user)->row();
        $msg = false;
        if ($this->session->userdata('invMes')) {
            $this->session->unset_userdata('invMes');
            $this->smarty->assign("message",
                $this->lang->line('sent_invite_successful'));
        }
        
        $key = str_replace('+', 'plus',
            $this->cipher->encrypt($data['userData']['user_name']));
        $siteUrl = base_url().'users/login?'.$this->config->item('aff_ref_name').'='.$key;
        $this->smarty->assign('linkUrl', $siteUrl);
        $this->smarty->view('affiliation/invite_friend.tpl', $data);
    }
    
    public function email_friend()
    {
        $data['userData'] = (array)$this->authentication->user($this->id_user)->row();
        if ($this->input->post('ref')) {
            require_once APPPATH.'libraries/Swift/swift_required.php';
        
            $arrRef = $this->input->post('ref');
            $arrEmail = explode(',', $arrRef['remail']);
            $success=false;
            try {
                $filePath =
                    str_replace('\\', '/', FCPATH).'assets/template/recommend_email.html';
                $oriEmailTemplate = file_get_contents($filePath);

                // Create the Transport
                $transport = Swift_SmtpTransport::newInstance();

                // Create the Mailer using your created Transport
                $sys_email = $this->config->item('admin_email');
                $mailer = Swift_Mailer::newInstance($transport);
                $key = str_replace('+', 'plus',
                    $this->cipher->encrypt($data['userData']['user_name']));
                $referralURL = base_url().'users/login?'.
                    $this->config->item('aff_ref_name').'='.$key;
                $logo = '<img src="' . str_replace('https',
                    'http',
                    base_url() . '/assets/images/main-logo.png') .
                    '" alt="Feed.biz" border="0">';
                if (!empty($arrEmail)) {
                    foreach ($arrEmail as $email) {
                        $email_template = $oriEmailTemplate;

                        $email_template = str_replace("{#name}",
                            $this->session->userdata('user_first_name').' '.
                            $this->session->userdata('user_las_name').
                            "({$this->session->userdata('user_email')})",
                            $email_template);
                        $email_template = str_replace("{#url}", $referralURL, $email_template);
                        $email_template = str_replace("{#refemail}", $email, $email_template);
                        $email_template = str_replace("{#logo}", $logo, $email_template);

                        $swift_message = Swift_Message::newInstance();
                        $swift_message->setSubject(
                            $this->lang->line('Let Feed.biz to management your e-commerce business'))
                                ->setFrom(array($sys_email => $sys_email))
                                ->setTo($email)
                                ->setBody($email_template, 'text/html');

                        if ($mailer->send($swift_message)) {
                            $success = true;
                        }
                    }
                }
                if ($success) {
                    $this->session->set_userdata('invMes', true);
                }
                
                
              
                
                redirect(site_url('users/affiliation/invite_friend'));
                return;
            } catch (Exception $ex) {
            }
        }
    }
    
    public function affiliateLists()
    {
        $data = $this->authentication->checkAuthen();
        //echo 'Test';

        $this->load->model('referrals_model', 'referrals_model');
        $this->load->library('mydate');
        $this->load->library('eucurrency');
        $this->load->model('currency_model', 'currency');
         
        $link=array();
        $t_m=$this->input->get('month');
        $m_txt='';
        if (!empty($t_m) && $t_m !='') {
            $month=strtotime($t_m.'-15');
            $link[] = 'month='.$t_m;
            $m_txt = 'in '.date('F Y', $month);
        } else {
            $month = null;
        }
        
        $currency = $this->currency->get_user_currency($this->id_user);
        $data['currency'] = $currency;
        
        $data['arrCurrency'] = $this->currency->get_currency();
//      
        $data['month']=$m_txt;
        
        /* List Referrals */
        
        $queryReferral = $this->referrals_model->getReferralUser($this->id_user, $month);
        $queryAccReferral = $this->referrals_model->getAccReferralUser($this->id_user, $month);
     
        foreach ($queryReferral as $k=>$row) {
            foreach ($row as $key=>$v) {
                if ($key=='sum') {
                    $queryReferral[$k][$key] =
                        $this->mydate->moneySymbol($currency).' '.
                        $this->mydate->floatFormat(
                            $this->eucurrency->doConvert($v, $currency)) ;
                }
            }
        }

        $this->smarty->assign('queryReferral', $queryReferral);
        $this->smarty->assign('queryAccReferral', $queryAccReferral);
        $this->smarty->view('affiliation/list.tpl', $data);
    }
}
