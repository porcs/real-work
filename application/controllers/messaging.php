<?php

require_once(dirname(__FILE__).'/../libraries/FeedBiz/Messagings.php');
require_once(dirname(__FILE__).'/../libraries/FeedBiz/Marketplaces.php');

class Messaging extends CI_Controller {

    private $_calss;
    public $marketplace;
    public $messaging_lib;

    public function __construct() {

        parent::__construct();

        // check login
        $this->load->library('authentication');
        $this->authentication->checkAuthen(); //check logged in and redirect to login page

        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_general = $this->session->userdata('id_general');

        // id shop
        $this->id_shop = $this->session->userdata('id_shop');
        $this->shop_default = $this->session->userdata('shop_default');

        if (!isset($this->id_shop) || empty($this->id_shop) || !($this->shop_default) || empty($this->shop_default)){
            //load FeedBiz library
            $this->load->library('FeedBiz', array($this->user_name));
            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            $this->id_shop = $DefaultShop['id_shop'];
            $this->shop_default = $DefaultShop['name'];
        }

        // id mode
        $this->id_mode = 1;

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("messaging", $this->language);
        $this->smarty->assign("label", "messaging");
        $this->iso_code = mb_substr($this->language, 0, 2);
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
        $this->session->unset_userdata('menu');

        // Class name //Function name
	$this->_calss = $this->router->fetch_class();
	$this->_method = $this->router->fetch_method();
        $this->smarty->assign("class", ucwords($this->_calss));
        $this->smarty->assign("function", ucwords($this->_calss));

        $this->messaging_lib = new Messagings($this->user_name);
        $this->_set_session_message();
    }

    public function mail_invoice($marketplace, $id_country){

        $this->marketplace = $marketplace;
        $this->_country_data($id_country);
        $duplicate_data = $this->get_duplicate_site();
        $data = array();
        $data['not_previous'] =
        $data['not_continue'] = true;
        $data['marketplace_name'] = $marketplace;
        $data['action'] = $this->_method;
        $data['smtp_duplicate'] = isset($duplicate_data['smtp_duplicate']) ? $duplicate_data['smtp_duplicate'] : null;
        $data['mail_maximum'] = isset($duplicate_data['mail_maximum']) ? $duplicate_data['mail_maximum'] : null;
        $data['mail_invoice'] = $this->messaging_lib->get_messaging($this->id_shop, $this->marketplace, $id_country, $this->_method);

        if(isset(Messagings::$order_status['main'])){
            $data['order_status'] = Messagings::$order_status['main'];

            if(isset(Messagings::$order_status[$marketplace]) && is_array(Messagings::$order_status[$marketplace]))
                $data['order_status'] = array_merge ($data['order_status'], Messagings::$order_status[$marketplace]);
        }

        if(isset(Messagings::$template_list[$this->_method])){

            $data['template_list'] = Messagings::$template_list[$this->_method]['main'];
            
            if(isset(Messagings::$template_list[$this->_method][$marketplace]) && is_array(Messagings::$template_list[$this->_method][$marketplace]))
                $data['template_list'] = array_merge ($data['template_list'], Messagings::$template_list[$this->_method][$marketplace]);

            $upload_template = $this->get_file_name($marketplace, $id_country, false, 'html', false);

            $data['customize_template'] = $upload_template;
            
            if(is_array($upload_template))
                $data['template_list'] = array_merge ($data['template_list'], $upload_template);
            
        }

        $file_import = $this->get_file_name($marketplace, $id_country);

        if(!empty($file_import))
            $data['additionnal_file'] = $file_import;

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('messaging/MailInvoice.tpl', $data);

    }

    public function mail_review($marketplace, $id_country){

        $this->marketplace = $marketplace;
        $this->_country_data($id_country);
        $duplicate_data = $this->get_duplicate_site();
        
        $data = array();
        $data['not_previous'] =
        $data['not_continue'] = true;
        $data['marketplace_name'] = $marketplace;
        $data['action'] = $this->_method;
        $data['smtp_duplicate'] = isset($duplicate_data['smtp_duplicate']) ? $duplicate_data['smtp_duplicate'] : null;
        $data['mail_maximum'] = isset($duplicate_data['mail_maximum']) ? $duplicate_data['mail_maximum'] : null;
        $data['mail_review'] = $this->messaging_lib->get_messaging($this->id_shop, $this->marketplace, $id_country, $this->_method);

        if(isset(Messagings::$order_status['main'])){
            $data['order_status'] = Messagings::$order_status['main'];

            if(isset(Messagings::$order_status[$marketplace]) && is_array(Messagings::$order_status[$marketplace]))
                $data['order_status'] = array_merge ($data['order_status'], Messagings::$order_status[$marketplace]);
        }
        
        if(isset(Messagings::$template_list[$this->_method])){

            $data['template_list'] = Messagings::$template_list[$this->_method]['main'];

            if(isset(Messagings::$template_list[$this->_method][$marketplace]) && is_array(Messagings::$template_list[$this->_method][$marketplace]))
                $data['template_list'] = array_merge ($data['template_list'], Messagings::$template_list[$this->_method][$marketplace]);
            
            $upload_template = $this->get_file_name($marketplace, $id_country, false, 'html', false);
            
            $data['customize_template'] = $upload_template;

            if(is_array($upload_template))
                $data['template_list'] = array_merge ($data['template_list'], $upload_template);
        }
        
        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('messaging/MailReview.tpl', $data);

    }

    public function customer_thread($marketplace, $id_country){

        $this->marketplace = $marketplace;
        $this->_country_data($id_country);
        $data = array();
        $data['not_previous'] =
        $data['not_continue'] = true;
        $data['marketplace_name'] = $marketplace;
        $data['action'] = $this->_method;

        $data['customer_thread'] = $this->messaging_lib->get_messaging($this->id_shop, $this->marketplace, $id_country, $this->_method);

        if(isset($this->ext) && !empty($this->ext)) {
            $ext = Marketplaces::extToId($this->ext);
            $data['labels'][$ext] = sprintf('%s-%s', ucfirst($this->marketplace), ucfirst($ext));
        } 

        if(isset(Messagings::$email_providers)){
            $data['email_providers'] = Messagings::$email_providers;
        }

        // lang
        $countries = Marketplaces::getCountries();
        
        if(isset($countries[$id_country]['language_code']) && !empty($countries[$id_country]['language_code'])){
            $language_code = $countries[$id_country]['language_code'];
            $this->smarty->assign("id_lang",$language_code);
        }

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('messaging/CustomerThread.tpl', $data);

    }

    public function reply_template($lang){

        $marketplace = 'main';

        // lang
        $language = $this->feedbiz->checkLanguage($lang, $this->id_shop);
        
        if(isset($language['id_lang']) && !empty($language['id_lang'])){
            $id_lang = $language['id_lang'];
        }
   
        $data = array();
        $data['not_previous'] =
        $data['not_continue'] = true;
        $data['marketplace_name'] = $marketplace;
        $data['action'] = $this->_method;
        $data['reply_template'] = $this->messaging_lib->get_messaging($this->id_shop, null, $id_lang, $this->_method);

        if(isset(Messagings::$template_list[$this->_method])){

            $data['template_list'] = Messagings::$template_list[$this->_method];

            $upload_template = $this->get_file_name($marketplace, $lang, false, 'html', false);
            $data['customize_template'] = $upload_template;

            if(is_array($upload_template))
                $data['template_list'] = array_merge ($data['template_list'], $upload_template);
        }
       
        $this->smarty->assign("reply_template_lang", $lang);
        
        if(isset($language['name']) && !empty($language['name'])){
            $this->smarty->assign("reply_template_language", $language['name']);
        }
        
        $this->smarty->assign("id_lang", $id_lang);
        
        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('messaging/ReplyTemplate.tpl', $data);

    }

    public function get_duplicate_messaging($marketplace, $id_country){
        $return = array();
        $messagings = $this->messaging_lib->get_messaging($this->id_shop, $marketplace, $id_country);
        $smtp_field = array('encryption', 'mail_from', 'password', 'port', 'smtp_server');
        foreach ($messagings as $messaging => $value){
            foreach ($smtp_field as $field){
                $r = strpos($messaging, $field);
                if($r){
                    $return[$field] = $value;
                }
            }
        }
        echo json_encode ($return);
        exit;
    }

    public function get_duplicate_site(){

        $smtp_duplicate = array();
        $mail_maximum = null;
        $messagings = $this->messaging_lib->get_messagings($this->id_shop);
        $countries = Marketplaces::getCountries();
        
        foreach ($messagings as $mk => $messaging1){
            foreach ($messaging1 as $site => $messaging){
                if(isset($messaging[Messagings::$limit_field]))
                    $mail_maximum = $messaging[Messagings::$limit_field];
                $smtp_duplicate[$mk][$site]['id'] = $site;
                $smtp_duplicate[$mk][$site]['name'] = isset($countries[$site]['title_offer_sub_pkg']) ? $countries[$site]['title_offer_sub_pkg'] : $site;
            }
        }
        
        return array('smtp_duplicate'=>$smtp_duplicate, 'mail_maximum'=>$mail_maximum);
    }

    public function set_messaging($action, $marketplace, $id_country){

        $this->marketplace = $marketplace;
        $this->_country_data($id_country);
        
        if ($this->input->post()) {

            $values = $this->input->post();

            if($marketplace == 'main')
                $this->id_marketplace = 0;
            
            $values['id_shop'] = $this->id_shop;
            $values['id_marketplace'] = $this->id_marketplace;
            $values['marketplace'] = $marketplace;
            $values['id_country'] = $id_country;

            if (!$this->messaging_lib->save_messaging($action, $values)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('message', 'save_successful');
            }
        }

        if($marketplace == 'main' && isset($values['lang']))
            redirect('my_shop/'.$this->_calss.'/'.$action.'/' . $values['lang']);
        else
            redirect($marketplace.'/'.$this->_calss.'/'.$action.'/' . $id_country);

    }

    public function upload_file($marketplace, $id_country, $file_type='pdf'){ 

        $error = array();
        $this->marketplace = $marketplace;
        $this->_country_data($id_country);

        $lang = $id_country;

        if(isset($this->ext) && Marketplaces::extToId($this->ext))
            $lang = Marketplaces::extToId($this->ext);

        switch ($file_type){
            case 'pdf' :
                $file_path = $this->messaging_lib->path_pdf . $marketplace . '/' . $lang;
                break;
            case 'html' :
                $file_path = $this->messaging_lib->path_html . $marketplace . '/' . $lang;
                break;
        }     

        // create dir if not already exist in uploads dir
        if (!is_dir($file_path)) {
            if(!mkdir($file_path, 0777, true)){                
                $error[] = "create directory unsuccessful";
                echo json_encode (array('pass'=>false, 'error'=> implode(", ", $error)));
                exit;
            }
        }

        $this->load->helper('form');

        $config = array();
        $config['upload_path'] = $file_path;
        $config['allowed_types'] = 'pdf|html';
        $config['overwrite'] = true;

        $this->load->library('upload');

        $files = $_FILES;

        $cpt = count($_FILES['file-upload']['name']);

        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['file-upload']['name']= $files['file-upload']['name'][$i];
            $_FILES['file-upload']['type']= $files['file-upload']['type'][$i];
            $_FILES['file-upload']['tmp_name']= $files['file-upload']['tmp_name'][$i];
            $_FILES['file-upload']['error']= $files['file-upload']['error'][$i];
            $_FILES['file-upload']['size']= $files['file-upload']['size'][$i];

            //$config['file_name'] = $action ;
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('file-upload')){
                $error[] = $_FILES['file-upload']['name'] .":" . $this->upload->display_errors();
            }
        }

        $this->upload->data();

        if($error) {
            echo json_encode (array('pass'=>false, 'error'=> implode(", ", $error)));
        } else {
            echo json_encode ($this->get_file_name($marketplace, $id_country, false, $file_type, true));
        }
        exit;
    }

    /* $filename include file type : e.g. 'invoice1.pdf' */
    public function delete_file($marketplace, $id_country, $file_type='pdf'){

        $filename = $this->input->post('filename');
        
        $this->marketplace = $marketplace;
        $this->_country_data($id_country);

        $lang = $id_country;

        if(isset($this->ext) && Marketplaces::extToId($this->ext))
            $lang = Marketplaces::extToId($this->ext);
        
        switch ($file_type){
            case 'pdf' :
                $file_path = $this->messaging_lib->path_pdf . $marketplace . '/' . $lang;
                break;
            case 'html' :
                $file_path = $this->messaging_lib->path_html . $marketplace . '/' . $lang;
                break;
        }    

        // check dir 
        if (!is_dir($file_path)) {
            echo json_encode (array('pass'=>true));
            exit;
        }

        echo json_encode (array('pass'=> file_exists($file_path.'/'.$filename) ? (bool)unlink ($file_path.'/'.$filename) : false ));
        exit;
       
    }
    
    public function get_file_name($marketplace, $id_country, $return_json=false, $file_type='pdf', $include_type=true){

        $filename = array();
        $this->marketplace = $marketplace;
        $this->_country_data($id_country);

        $lang = $id_country;

        if(isset($this->ext) && Marketplaces::extToId($this->ext))
            $lang = Marketplaces::extToId($this->ext);
        
        switch ($file_type){
            case 'pdf' :
                $file_path = $this->messaging_lib->path_pdf . $marketplace . '/' . $lang;
                break;
            case 'html' :
                $file_path = $this->messaging_lib->path_html . $marketplace . '/' . $lang;
                break;
        }

        if (!is_dir($file_path)) {
            if($return_json){
                echo json_encode (array('pass'=>false, 'error'=> $this->lang->line('no_file')));
                exit;
            } else {
                return array();
            }
        }

        foreach (glob("$file_path/*.$file_type") as $file){
            $file_name = str_replace("$file_path/", "", $file);

            if(!$include_type)
                $file_name = str_replace("$file_path/", "", str_replace(".$file_type", "", $file));
            
            $filename[] = $file_name;
        }

        if($return_json){
            echo json_encode ($filename);
        } else {
           return $filename;
        }
        
    }

    public function preview_file($marketplace, $id_country, $template, $file_type='html'){

        $this->marketplace = $marketplace;
        $this->_country_data($id_country);

        $lang = $id_country;

        if(isset($this->ext) && Marketplaces::extToId($this->ext))
            $lang = Marketplaces::extToId($this->ext);

        if($marketplace == 'main'){
            $code = $lang;
        } else {
            $countries = Marketplaces::getCountries();
            $code = $this->ext;
            if(isset($countries[$id_country]['language_code']))
                $code = $countries[$id_country]['language_code'];
        }
        
        $file_path = $this->messaging_lib->path_mail . $code;
        $file = $file_path.'/'.$template.'.'.$file_type;

        if (!file_exists($file)) {

            switch ($file_type){
                case 'pdf' :
                    $file_path = $this->messaging_lib->path_pdf . $marketplace . '/' . $lang;
                    $file = $file_path.'/'.$template.'.'.$file_type;
                    break;
                case 'html' :
                    $file_path = $this->messaging_lib->path_html . $marketplace . '/' . $lang;
                    $file = $file_path.'/'.$template.'.'.$file_type;
                    break;
            }
            
            if (!file_exists($file)) {
                header('Content-Type: application/json');
                echo json_encode (array('pass'=>false, 'error'=> $this->lang->line('no_file')));
                exit;
            }
        }
        header('Content-Type: application/json');
        echo json_encode(array('pass'=>true,'output'=> file_get_contents($file)));
        exit;
    }

    private function _country_data($id_country = null) {

        // create table
        if(!$this->messaging_lib->create_table(Messagings::TABLE)){
            $this->smarty->assign("error", $this->lang->line('create_table_unsuccessful'));
        }        
        
	$this->smarty->assign("id_country", $id_country);

	if (isset($this->session->userdata['menu'])){
	    $marketplaces = $this->session->userdata['menu'];
	}
      
        $marketplace = array();
        
        foreach ($marketplaces as $key => $value){
                        
            $marketplace[strtolower($key)] = $value;

            // Mirakl
            foreach ($value as $val){
                if(is_array($val)) {
                    foreach ($val as $v) {
                        if(isset($v['name_marketplace']) && isset($value['id'])){
                            $marketplace[strtolower($v['name_marketplace'])]['id'] = $value['id'];
                            $marketplace[strtolower($v['name_marketplace'])]['submenu'] = $v;
                            if($key == 'General'){
                                 $marketplace[strtolower($v['name_marketplace'])]['general'] = true;
                            }
                        }
                    }
                }
            }
        }
        
	if (isset($marketplace[$this->marketplace]['id'])){
	    $this->id_marketplace = $marketplace[$this->marketplace]['id'];
	    $this->smarty->assign("id_marketplace", $this->id_marketplace);
	} 

	if (isset($marketplace[$this->marketplace]['submenu'][$id_country]['country'])){
	    $this->smarty->assign("country", $marketplace[$this->marketplace]['submenu'][$id_country]['country']);
	}

	if (isset($marketplace[$this->marketplace]['submenu'][$id_country]['ext'])) {
	    $this->smarty->assign("ext", $marketplace[$this->marketplace]['submenu'][$id_country]['ext']);
	    $this->ext = $marketplace[$this->marketplace]['submenu'][$id_country]['ext'];	   
	}

        if(isset($marketplace[$this->marketplace]['general'])){
            $this->smarty->assign("mk_general", true);
        }
    }

    private function _set_session_message(){
        if ($this->session->userdata('error')) {
            $this->smarty->assign("error", $this->lang->line($this->session->userdata('error')));
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $this->smarty->assign("message", $this->lang->line($this->session->userdata('message')));
            $this->session->unset_userdata('message');
        }
    }
}