<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fbmapping extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('auth');
        $this->auth->checkUserManagement();
    }

    public function index() {
	
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'offer_price_packages';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/mapping_color.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/mapping_color.php';
        }

 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/mapping/templates';
       
//        $conf['join'] = array('offer_packages' =>  array('', 'offer_packages', 'offer_price_packages.id_offer_pkg = offer_packages.id_offer_pkg' ),
//            'offer_sub_packages' =>  array('', 'offer_sub_packages', 'offer_sub_packages.id_offer_sub_pkg = offer_price_packages.id_offer_sub_pkg' ) );
//        
        //$warning = ($this->session->userdata('error_message')) ? $this->session->userdata('error_message') : '';   
        //$this->session->unset_userdata('error_message');
       // print_r($this->session->userdata);exit;
       
        $this->load->library('crud', array('table' => 'mapping_color', 'conf' => $conf));
		
		//$this->r($this->crud->process());
		
        //$var['main_content'] = '';
        $var['main_content'] = $this->load->view('admin/mapping/mapping_color', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
    
   
}