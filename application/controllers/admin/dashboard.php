<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function index() {
        if (!session_id()) {
            session_set_cookie_params(86400);
            ini_set('session.gc_maxlifetime', 86400);
            @session_start();
            unset($_SESSION['admin_logedin']);
        }
        $_SESSION['admin_logedin'] = date('c');
        $this->load->model('auth');
        $this->load->model('admin/admin_menu');

        $var = array();
        
        $var['main_menu'] = $this->admin_menu->fetch();
        $var['main_content'] = $this->load->view('admin/common/dashboard',$var,true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/default', $var);
    }

}