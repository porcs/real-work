<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Messages extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
    }   
    
	public function index() {
		$this->load->model ( 'auth' );
		$this->load->model ( 'message_model' );
		$this->load->model ( 'admin/admin_menu' );
		$this->load->model ( 'admin/admin_footer' );
		
		$var = array ();
		$var ['main_menu'] = $this->admin_menu->fetch ( 'tool' );
		$var ['messages'] = $this->message_model->get_messages ();
		$var ['main_content'] = $this->load->view ( 'admin/messages/index', $var, true );
		$var ['main_footer'] = $this->admin_footer->fetch ();
		
// 		$this->load->view ( 'layouts/admin/default', $var );
		$this->load->view ( 'layouts/admin/user/default', $var );
	}
	public function form() {
		$this->load->model ( 'auth' );
		$this->load->model ( 'message_model' );
//		$this->load->model ( 'language_model' );
		$this->load->model ( 'admin/admin_menu' );
		$this->load->model ( 'admin/admin_footer' );
		
		$id = $this->input->get ( 'message' );
		
		$var = array ();
		$var ['main_menu'] = $this->admin_menu->fetch ( 'tool' );
		$var ['types'] = array (
				message_model::TYPE_INFO => 'Information',
				message_model::TYPE_WARNING => 'Warning',
				message_model::TYPE_ERROR => 'Error' 
		);
		$var ['message'] = $this->message_model->get_message ( $id );
		$var ['languages'] = $this->message_model->get_languages ();
		
		$var ['default'] = '';
		foreach ( $var ['message'] as $lang => $ele ) {
			if ($lang == 'en' || empty ( $lang )) {
				$var ['default'] = $lang;
			}
		}
		
		$var ['main_content'] = $this->load->view ( 'admin/messages/form', $var, true );
		$var ['main_footer'] = $this->admin_footer->fetch ();
		$this->load->view ( 'layouts/admin/user/default', $var );
	}
	public function save() {
		$this->load->model ( 'message_model' );
		
		$message_code = $this->input->post ( 'message_code' );
		$message_type = $this->input->post ( 'message_type' );
		$message = $this->input->post ( 'message' );
		$action = $this->input->post ( 'action' );
		
		$var = array (
				'error' => 0,
				'error_message' => 'Save Message Success' 
		);
		if (empty ( $message_code )) {
			$var = array (
					'error' => 1,
					'error_message' => 'Please enter message code' 
			);
		} else if ($action == 'new' && $this->message_model->validate_duplicate ( $message_code )) {
			$var = array (
					'error' => 1,
					'error_message' => 'Please enter message code' 
			);
		}
		
		foreach ( $message as $language => $ele ) {
			$message [$language] ['message_language'] = $language;
			$message [$language] ['message_code'] = $message_code;
			$message [$language] ['message_type'] = $message_type;
		}
		
		$this->message_model->save_message ( $message_code, $message );
		
		echo json_encode ( $var );
	}
	public function delete() {
		$this->load->model ( 'message_model' );
		$message_code = $this->input->post ( 'message_code' );
		$this->message_model->delete_message ( $message_code );
	}
}