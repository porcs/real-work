<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Amazon extends Admin_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('auth');
        $this->auth->checkUserManagement();
    }
    
    public function valid_value() {
        
        if ($this->input->post()) 
        {         
            $data = $this->input->post();
            $product_type = $data['product_type'];//'ProductClothing';//
            $ext = $data['ext'];//'de'; 
            
            $this->load->helper('form');                 
            $config['upload_path'] = dirname(__FILE__) . '/../../libraries/Amazon/valid_value/upload/'. $ext .'/';
            $config['allowed_types'] = 'txt|csv';           
            $config['overwrite'] = true;           
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->upload->set_allowed_types('*');            
            $this->upload->do_upload('file_content');            
            $file_data = $this->upload->data();            
            $path = $file_data['full_path'];//$config['upload_path'] . 'ProductClothing_de.csv';

            require_once dirname(__FILE__) . '/../../libraries/Amazon/classes/amazon.validValues.update.php';
            
            $valid_value = new UpdateValidValues();
            $valid_value->bulk_valid_value($product_type, $ext, $path);
            $var['message'] = 'Success';
            
        }
        
        //$this->load->model('auth');
        $this->load->model('admin/admin_menu');
        $var = array();
        $var['url'] = (@$this->config->item('ssl_protocol') ? 'https://' : 'http://') . 'ndb.feed.dev:3001';        
//        $var['url'] = '/admin';
        $var['main_menu'] = $this->admin_menu->fetch('management');
        $var['main_content'] = $this->load->view('admin/amazon/bulk_valid_value',$var,true);        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();        
        $this->load->view('layouts/admin/default', $var);
    }
    public function dumpValidValue() {
        
        require_once dirname(__FILE__) . '/../../libraries/Amazon/classes/amazon.validValues.update.php';
        $valid_value = new UpdateValidValues();
        $valid_value->dump_valid_value();    
        
    }    
    public function excutefile($type=''){
        $return = '';
        if($type=='validvalue'){
            $node_path = 'https://127.0.0.1:3001/amazon/excutefile/validvalue';
             $return = file_get_contents($node_path);
        }
        echo $return;
    }
    
    public function update($type=''){
        $return = '';
        if($type=='validvalue'){
            $node_path = 'https://127.0.0.1:3001/amazon/update/validvalue';
             $return = file_get_contents($node_path);
        }
        echo $return;
    }
    
    public function download($type=''){
        $return = '';
        if($type=='flatfile'){
            $node_path = 'https://127.0.0.1:3001/amazon/download/flatfile';
             $return = file_get_contents($node_path);
        }
        echo $return;
    } 
    public function error_code_explanations() {
        
        $this->load->model('auth');
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        
        $_GET['table'] = 'amazon_error_resolutions';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/amazon_error_resolutions.php')) {
            exit;
        }else{
            require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/amazon_error_resolutions.php';
        } 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/amazon/templates';       
        $this->load->library('crud', array('table' => 'amazon_error_resolutions', 'conf' => $conf));		
       
        $var['main_content'] = $this->load->view('admin/amazon/error_code_explanations', array('content' => $this->crud->process()), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
        
    }

}