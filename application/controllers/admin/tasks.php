<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Tasks extends MY_Controller {
    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        parent::__construct(); 
        $this->load->library('authentication');

        //helper
        $this->load->helper('form'); 
 
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
    }
    public function store_login($fb_encrypted_data=null){
        require_once dirname(__FILE__).'/../../../libraries/store_lib.php';
        $obj = new StoreInterface();
        $return_output = array('result'=>false,'error'=>'Hack attemp');
        $need_session=false;
        if ($this->input->post() || !empty($fb_encrypted_data)) {
            if(!empty($fb_encrypted_data)){
                $post['fb_encrypted_data']=$fb_encrypted_data;
                $need_session=true;
            }else{
            $post = $this->input->post();
            }
            if(isset($post['fb_encrypted_data'])){  
                $code=$post['fb_encrypted_data'];
                $out = $obj->decrypt($code);
                if($out !== false){
                    $key = array('username','password','ip');
                    $pass=true;
                    foreach($key as $k){
                        if(!isset($out[$k]) || empty($out[$k])){
                            $pass=false;
                        }
                    }
                    $this->load->model('my_feed_model', 'user_model'); 
                    if(!$this->user_model->check_exists_user_email($out['username'])){
                        $return_output['error'] = $this->lang->line('The email address you have entered is not registered');
                    }else if($pass){
                        $userdata = $this->authentication->login($out['username'], $out['password']);
                        if(isset($userdata) && !empty($userdata) && !isset($userdata['error'])) {
                            $id = $userdata['id'];
                            $userdata = $this->authentication->user($id)->row();
                            $language = $this->authentication->getLanguageNameByID($userdata->user_default_language);
                            if($need_session){
                                $this->authentication->logout();
                                $this->session->unset_userdata('id_general');
                                $this->session->unset_userdata('menu');
                                $this->session->unset_userdata('mode_default');
                                $this->session->unset_userdata('amazon_mode_creation');
                                $this->session->unset_userdata('dashboard_sum_product');
                                delete_cookie('id');
                                

                                $session_data = array(
                                    'id' => $userdata->id,
                                    'user_email' => $userdata->user_email,
                                    'user_name' => $userdata->user_name ,
                                    'user_first_name' => $userdata->user_first_name,
                                    'user_las_name' => $userdata->user_las_name,
                                    'language' => !empty($language) ? $language : $this->language,
                                    'logged' => TRUE,
                                    'time' => time(),
                                    'ip' =>  $out['ip'],
                                    'first_login' => TRUE 
                                );

                                $this->session->set_userdata($session_data);
                                $this->authentication->generateNewOTP($userdata->id,true); 
                            }
                            $return_output['result']=true;
                            $return_output['error']=false;
                            $return_output['uid']=$userdata->id;
                            $return_output['uname']=$userdata->user_name;
                        
                        }else{ 
                            $return_output['error'] = $this->lang->line($userdata['error']);
                        }
                    } 
                }
            }
        }
        echo json_encode($return_output); 
    }

}