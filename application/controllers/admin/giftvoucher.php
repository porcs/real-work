<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Giftvoucher extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('auth');
        $this->auth->checkUserManagement();
    }

    public function add()
    {
        $this->load->model('admin/admin_menu');
        $this->load->model('voucher_model','model');
        
        if($this->input->post('gift'))
        {
            $inputGift = $this->input->post('gift');
            
            $arrData = array(
                'name_giftopt' => $inputGift['name'],
                'cmd_giftopt' => $inputGift['cmd'],
                'amt_giftopt' => $inputGift['amt'],
                'currency_giftopt' => $inputGift['currency'],
                'code_qty_giftopt' => $inputGift['code'],
                'priod_giftopt' => $inputGift['priod'],
                'start_date_giftopt' => $inputGift['startDate'],
                'end_date_giftopt' => $inputGift['endDate'],
                'status_giftopt' => $inputGift['status'],
                'created_date_giftopt' => date('Y-m-d H:i:s'),
                'created_date_giftopt' => date('Y-m-d H:i:s'),
            );
            $this->model->setGiftVoucher($arrData);
            
            redirect(site_url('admin/giftvoucher/index'));
            return;
        }
        else
        {
            $var = array();
            $var['main_menu'] = $this->admin_menu->fetch('tool');

            $data = array();

            $var['main_content'] = $this->load->view('admin/giftvoucher/giftvoucher_add',$data,true);

            /* Footer Template */
            $this->load->model('admin/admin_footer');
            $var['main_footer'] = $this->admin_footer->fetch();
            /* End Footer ... */

            $this->load->view('layouts/admin/user/default', $var);
        }
    }
    
    public function edit()
    {
        if($this->input->get('id'))
        {
            $this->load->model('admin/admin_menu');
            $this->load->model('voucher_model','model');
            
            $id = $this->input->get('id');
            
            /* Init Var Main Menu */
            $var = array();
            $var['main_menu'] = $this->admin_menu->fetch('tool');
            
            $data = array();
            $data['queryGiftOption'] = $this->model->getGiftOptionID($id);
            
            if(!empty($data['queryGiftOption']))
            {
                if($this->input->post('gift'))
                {
                    $inputGift = $this->input->post('gift');
                    
                    $arrData = array(
                        'id_giftopt' => $id,
                        'name_giftopt' => $inputGift['name'],
                        'cmd_giftopt' => $inputGift['cmd'],
                        'amt_giftopt' => $inputGift['amt'],
                        'currency_giftopt' => $inputGift['currency'],
                        'code_qty_giftopt' => $inputGift['code'],
                        'priod_giftopt' => $inputGift['priod'],
                        'start_date_giftopt' => $inputGift['startDate'],
                        'end_date_giftopt' => $inputGift['endDate'],
                        'status_giftopt' => $inputGift['status'],
                        'created_date_giftopt' => date('Y-m-d H:i:s'),
                        'created_date_giftopt' => date('Y-m-d H:i:s'),
                    );
                    $this->model->updateGiftOption($arrData);
                    redirect(site_url('admin/giftvoucher/index'));
                    return;
                }
                else
                {
                    $var['main_content'] = $this->load->view('admin/giftvoucher/giftvoucher_edit',$data,true);
        
                    /* Footer Template */
                    $this->load->model('admin/admin_footer');
                    $var['main_footer'] = $this->admin_footer->fetch();
                    /* End Footer ... */

                    $this->load->view('layouts/admin/user/default', $var);
                }
            }
        }
    }
    
    public function detail()
    {
        if($this->input->get('id'))
        {
            $this->load->model('admin/admin_menu');
            $this->load->model('voucher_model','model');
            
            $id = $this->input->get('id');
            
            /* Init Var Main Menu */
            $var = array();
            $var['main_menu'] = $this->admin_menu->fetch('tool');
            
            $data = array();
            $data['queryGiftOption'] = $this->model->getGiftOptionID($id);
            
            if(!empty($data['queryGiftOption']))
            {
                $data['codeList'] = $this->model->getGiftCode($data['queryGiftOption'][0]['id_giftopt']);
                
                $var['main_content'] = $this->load->view('admin/giftvoucher/giftvoucher_detail',$data,true);
        
                /* Footer Template */
                $this->load->model('admin/admin_footer');
                $var['main_footer'] = $this->admin_footer->fetch();
                /* End Footer ... */
                
                $this->load->view('layouts/admin/user/default', $var);
            }
        }
    }
    
    public function generate()
    {
        if(($this->input->post('cmd') && $this->input->post('cmd')=='giftcode') && $this->input->post('id') && $this->input->post('value'))
        {
            $id = $this->input->post('id');
            $value = $this->input->post('value');
            
            $this->load->model('voucher_model','model');
            
            $query = $this->model->getGiftOptionID($id);
            if($query)
            {
                $this->load->library('mydate');
                $remain = $query[0]['code_qty_giftopt'] - $query[0]['used'];
                if($value > $remain)
                    $num = $remain;
                else
                    $num = $value;
                switch($query[0]['priod_giftopt'])
                {
                    case 'month':
                        $startDate = date('Y-m-d');
                        $endDate = date('Y-m-d',$this->mydate->get_x_months_to_the_future());
                        break;
                    case 'year':
                        $startDate = date('Y-m-d');
                        $endDate = date('Y-m-d',$this->mydate->get_x_months_to_the_future(time(),'12'));
                        break;
                    case 'fixed':
                        $startDate = $query[0]['start_date_giftopt'];
                        $endDate = $query[0]['end_date_giftopt'];
                        break;
                }

                for($i=1;$i<=$num;$i++)
                {
                    do{
                        $giftCode = strtoupper($this->mydate->generateRandomString(15));
                    }while($this->model->checkGiftUniqCode($giftCode));
                    
                    $arrData = array(
                        'id_giftcode' => $giftCode,
                        'id_giftopt' => $query[0]['id_giftopt'],
                        'start_date_giftcode' => $startDate,
                        'end_date_giftcode' => $endDate,
                        'status_giftcode' => 'active',
                        'created_date_giftcode' => date('Y-m-d H:i:s'),
                    );
                    $this->model->setGiftCode($arrData);
                }
                
                echo 'true';
                
            }
            else
            {
                echo 'false';
            }
        }
    }
    
    public function deleteCode()
    {
        if($this->input->get('id'))
        {
            $id = $this->input->get('id');
            $this->load->model('voucher_model','model');
            
            $returnURL = $this->model->deleteGiftCode($id);
            redirect(site_url('admin/giftvoucher/detail/?id='.$returnURL));
            return;
        }
    }
    
    public function deleteGiftOpt()
    {
        if($this->input->get('id'))
        {
            $id = $this->input->get('id');
            $this->load->model('voucher_model','model');
            $this->model->deleteGiftOption($id);
            
            redirect(site_url('admin/giftvoucher/index'));
            return;
        }
    }
    
    public function index() {
        $this->load->model('admin/admin_menu');
        $this->load->model('voucher_model','model');
        $var = array();
        $var['main_menu'] = $this->admin_menu->fetch('tool');
        
        $data = array();
        $data['query'] = $this->model->getGiftOption();
        
        $var['main_content'] = $this->load->view('admin/giftvoucher/giftvoucher',$data,true);
        
        /* Footer Template */
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();
        /* End Footer ... */
        
        $this->load->view('layouts/admin/user/default', $var);
    }
}