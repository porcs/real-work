<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Billing extends Admin_Controller
{
    private $fcpath,$dbpath;
    public function __construct(){
        parent::__construct();
        $this->load->model('auth');
        $this->auth->checkUserManagement();
        $this->config->load('paypal_settings');
        $this->config_pay = $this->config->item('pay');
        $this->fcpath = str_replace('\\','/',FCPATH);
        $this->dbpath = str_replace('\\','/',__DATABASE_CONFIG_PATH__); 
    }
    
    public function detail()
    {
        if($this->input->get('id'))
        {
            $id = $this->input->get('id');
            $this->load->model('admin/admin_menu');
            $this->load->model('admin/admin_footer');
            $this->load->model('paypal_model','model');
            $this->load->library('mydate');
            $this->load->library('Cipher',array('key'=>$this->config->item('encryption_key')));
            
            $query = $this->model->getAdminBillingID($id);
            if($query)
            {
                $var = array();
                $data = array();
                
                $var['main_menu'] = $this->admin_menu->fetch('management');
                $var['main_footer'] = $this->admin_footer->fetch();
                
                $data['nav'] = $this->load->view('admin/management/nav',null,true);
                
                $data['query'] = $query;
                $data['queryDetail'] = $this->model->getBillingDetailLists($query['id_billing']);
                if($data['queryDetail'])
                {
                    foreach($data['queryDetail'] as $key => $val)
                    {
                        $data['queryDetail'][$key]['package_detail'] = unserialize($this->cipher->decrypt($val['package_detail']));
                    }
                }
                
                $var['main_content'] = $this->load->view('admin/management/billing_detail', $data, true);
                $this->load->view('layouts/admin/user/default', $var);
            }
        }
    }
    
    public function printBilling()
    {
        if($this->input->get('id'))
        {
            $id = $this->input->get('id');
            
            $this->load->model('paypal_model','model');
            $this->load->library('mydate');
            $this->load->library('Cipher',array('key'=>$this->config->item('encryption_key')));
            
            try
            {
                $fileDir = str_replace('\\', '/', FCPATH);
                $this->load->library('MPDF/mpdf');
                $this->mpdf->mPDF('','A4',10,'garuda',10,10,15,15,9,9,'P');
                $this->mpdf->SetDisplayMode('fullpage');
                
                $query = $this->model->getBilling($id);
//                echo '<pre>';
//                print_r($query);exit;
                if($query)
                {
                    $queryDetail = $this->model->getBillingDetailLists($id);

                    if($queryDetail)
                    {
                        $billDetail = '';
                        $totalAmount = 0;
                        $defaultSign = '';
                        $count = 0;

                        foreach($queryDetail as $key => $val)
                        {
                            $queryDetail[$key]['package_detail'] = unserialize($this->cipher->decrypt($val['package_detail']));
                            switch($val['option_package'])
                            {
                                case 'offer':
                                    $cQuery = $this->model->getOfferCurrency($val['id_package']);
                                    break;
                            }
                            $queryDetail[$key]['currency'] = $cQuery[0]['currency'];

                            $billDetail .= '<div style="margin: 0px 0px 5px 0px;">
                                <div style="float: left;text-align: left;width: 30px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                    '.($key+1).'
                                </div>
                                <div style="float: left;text-align: left;width: 440px;padding: 5px 5px 0px 5px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                    <div>'.$queryDetail[$key]['package_detail']['desc'].'</div>
                                    <div>(Period: '.date('j F Y',strtotime($queryDetail[$key]['start_date_bill_detail'])).' - '.date('j F Y',strtotime($queryDetail[$key]['end_date_bill_detail'])).')</div>
                                </div>
                                <div style="float: left;text-align: left;width: 130px;padding: 0px 5px;line-height: 30px;text-transform: capitalize;font-family: Verdana, sans-serif;background-color: #FFF;">
                                    '.$queryDetail[$key]['option_package'].'
                                </div>
                                <div style="float: left;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                    '.$this->mydate->moneySymbol($queryDetail[$key]['currency']).$queryDetail[$key]['package_detail']['amt'].'
                                </div>
                            </div>';
                            if($key == 0)
                            {
                                $defaultSign = $this->mydate->moneySymbol($queryDetail[$key]['currency']);
                            }
                            $totalAmount += $queryDetail[$key]['package_detail']['amt'];
                            $count = $key;
                        }

                        if(!empty($query['discount_cmd_billing']))
                        {
                            $count += 2;

                            $arrDiscountLog = unserialize($query['discount_log_billing']);
                            $arrDiscountCmd = unserialize($query['discount_cmd_billing']);

                            foreach($arrDiscountCmd as $key => $val)
                            {
                                $dcText = '';
                                switch($val['param'])
                                {
                                    case 'referrals':
                                        $dcText = 'Referrals Discount';
                                        break;
                                    case 'discount_date':
                                            $dcText = 'Until the next payday discount  ';
                                            break;
                                    case 'gift':
                                        $giftQuery = $this->model->getGiftCodeDetailPrint($arrDiscountLog['gift']);

                                        if($giftQuery)
                                        {
                                            $dcText = $giftQuery[0]['name_giftopt'];
                                        }
                                        break;
                                    case 'total':
                                        $totalAmount = $val['total_amt'];
                                        break;
                                }

                                if($dcText != '')
                                {
                                    $billDetail .= '<div style="margin: 0px 0px 5px 0px;">
                                        <div style="float: left;text-align: left;width: 30px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                            '.$count.'
                                        </div>
                                        <div style="float: left;text-align: left;width: 440px;padding: 5px 5px 0px 5px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                            <div>'.$dcText.'</div>
                                        </div>
                                        <div style="float: left;text-align: left;width: 130px;padding: 0px 5px;line-height: 30px;text-transform: capitalize;font-family: Verdana, sans-serif;background-color: #FFF;">
                                            &nbsp;
                                        </div>
                                        <div style="float: left;text-align: right;width: 70px;padding: 0px 5px;line-height: 30px;font-family: Verdana, sans-serif;background-color: #FFF;">
                                            -'.$this->mydate->moneySymbol($this->config_pay['currencycode']).$val['dc_amt'].'
                                        </div>
                                    </div>';
                                }

                                $count += 1;
                            }
                        }
                    }

                    /*
                    * Billing Addr
                    */
                    $billAddr = '<div>'.$query['company_billing'].'</div>';
                    $billAddr .= '<div>'.$query['first_name_billing'].' '.$query['last_name_billing'].'</div>';
                    $billAddr .= '<div>'.$query['address1_billing'];
                    if(!empty($query['address2_billing']))
                        $billAddr .= ', '.$query['address2_billing'];
                    $billAddr .= '</div>';
                    $billAddr .= '<div>'.$query['stateregion_billing'].', '.$query['city_billing'].'</div>';
                    $billAddr .= '<div>'.$query['country_billing'].', '.$query['zipcode_billing'].'</div>';
                    /*
                    * End Billing Addr
                    */

                    $content = file_get_contents($fileDir.'assets/template/billing_template.tpl');

                    $content = str_replace("{#billID}", $query['id_billing'], $content);
                    $content = str_replace("{#billDate}", date('j F Y',strtotime($query['created_date_billing'])), $content);
                    $content = str_replace("{#billAddress}", $billAddr, $content);
                    $content = str_replace("{#billDetail}", $billDetail, $content);
                    $content = str_replace("{#totalAmount}", $defaultSign.number_format($totalAmount,2), $content);

                    $this->mpdf->AddPage();
                    $this->mpdf->WriteHTML($content);
                    $this->mpdf->Output();
                }
            }
            catch(Exception $ex){}
        }
    }

    public function index()
    {
        $this->load->model('admin/admin_menu');
        $this->load->model('admin/admin_footer');
        $this->load->model('paypal_model','model');
        $this->load->library('mydate');
        
        $var = array();
        $data = array();
        
        $var['main_menu'] = $this->admin_menu->fetch('management');
        $var['main_footer'] = $this->admin_footer->fetch();
        
        $data['nav'] = $this->load->view('admin/management/nav',null,true);
        
        $arrParam = array();
        /* billing query */
        $search = '';
        if($this->input->get('keyword'))
            $search = $this->input->get('keyword');
        if($search)
            $arrParam['search'] = 'search='.$search;
        $page = 1;
        if($this->input->get('page'))
            $page = (int)$this->input->get('page');
        if($page)
            $arrParam['page'] = 'page='.$page;
        $order = 'b.created_date_billing';
        if($this->input->get('order'))
            $order = $this->input->get('order');
        if($order)
            $arrParam['order'] = 'order='.$order;
        $sort = 'DESC';
        if($this->input->get('sort'))
            $sort = $this->input->get('sort');
        if($sort)
            $arrParam['sort'] = 'sort='.$sort;
        $limit = 50;
        if($this->input->get('limit'))
            $limit = (int)$this->input->get('limit');
        if($limit)
            $arrParam['limit'] = 'limit='.$limit;
        $data['order'] = $order;
        $data['sort'] = strtoupper($sort);
        $data['query'] = $this->model->getAdminBillings($search,$page,$order,$sort,$limit);
        
        /* pagination */
        $data['totalRow'] = ceil($data['query']['num']/$data['query']['limit']);
        $data['per'] = 10;
        $data['part'] = ceil($data['query']['page']/$data['per']);
        $data['endPage'] = $data['part']*$data['per'];
        $data['startPage'] = $data['endPage']-($data['per']-1);
        if($data['endPage'] > $data['totalRow'])
            $data['endPage'] = $data['totalRow'];
        /* end pagination */
        
        $data['param'] = $arrParam;
        
        /* end billing query */
        
        $var['main_content'] = $this->load->view('admin/management/billing', $data, true);
        
        $this->load->view('layouts/admin/user/default', $var);
    }
}