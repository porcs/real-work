<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Packages extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('auth');
        $this->auth->checkUserManagement();
    }

    public function index() {
	
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'mapping_colors';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/mapping_colors.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/packages.php';
        }

 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/packages/templates';
       
        $conf['join'] = array('offer_packages' =>  array('', 'offer_packages', 'offer_price_packages.id_offer_pkg = offer_packages.id_offer_pkg' ),
            'offer_sub_packages' =>  array('', 'offer_sub_packages', 'offer_sub_packages.id_offer_sub_pkg = offer_price_packages.id_offer_sub_pkg' ) );
        
        //$warning = ($this->session->userdata('error_message')) ? $this->session->userdata('error_message') : '';   
        //$this->session->unset_userdata('error_message');
       // print_r($this->session->userdata);exit;
       
        $this->load->library('crud', array('table' => 'offer_price_packages', 'conf' => $conf));
		
		//$this->r($this->crud->process());
		
        //$var['main_content'] = '';
        $var['main_content'] = $this->load->view('admin/packages/packages', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
    
    public function marketplace() {
	
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'offer_packages';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/marketplace.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/marketplace.php';
        }

 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/packages/templates';
        
       
        $this->load->library('crud', array('table' => 'offer_packages', 'conf' => $conf)); 
        $var['main_content'] = $this->load->view('admin/packages/marketplace', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
    
    public function country() {
	
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'offer_sub_packages';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/marketplace_country.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/marketplace_country.php';
        }

 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/packages/templates';
        
       
        $this->load->library('crud', array('table' => 'offer_sub_packages', 'conf' => $conf)); 
        $var['main_content'] = $this->load->view('admin/packages/country', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
     public function currency() {
	
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'currency';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('management');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/currency.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/currency.php';
        }

 
        
        $conf['theme_path'] = FCPATH . 'application/views/admin/packages/templates';
        
       
        $this->load->library('crud', array('table' => 'currency', 'conf' => $conf)); 
        $var['main_content'] = $this->load->view('admin/packages/currency', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
	
	/*public function test_load_user() {
	
		$sql = "
			SELECT 
				a.*,
				b.company
			FROM  `users` a 
			LEFT JOIN customer b ON a.id = b.id
			WHERE a.id = 1";
		$q = $this->db->query( $sql );

		$res = $q->result();
		
		//$this->r( $res );
		
        $this->load->view('layouts/test');
	}
	
	function r( $arr = array() ) {
		echo '<pre>';
		print_r( $arr );
	}*/
    
    //////// COMPANY ////////////
    public function company() {
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'user_packages';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('user');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/company.php')) {
            exit;
        }else{
            require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/company.php';
        }

        $conf['theme_path'] = FCPATH . 'application/views/admin/user/templates';
        $this->load->library('crud', array('table' => 'user_packages', 'conf' => $conf));
        
        //$warning = ($this->session->userdata('error_message')) ? $this->session->userdata('error_message') : '';   
        //$this->session->unset_userdata('error_message');
         //print_r($this->session->userdata);exit;
         
        $var['main_content'] = $this->load->view('admin/user/company', array('content' => $this->crud->process(), /*'warning' => $warning*/), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
    
    public function group() {
        $this->load->model('admin/admin_menu');
        $this->load->add_package_path(APPPATH . 'third_party/scrud/');
        $_GET['table'] = 'groups';
        $var = array();
        $conf = array();
        $var['main_menu'] = $this->admin_menu->fetch('user');

        if (!file_exists(__DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/groups.php')) {
            exit;
        }else{
        	require __DATABASE_CONFIG_PATH__ . '/' . $this->db->database . '/groups.php';
        }

        $conf['theme_path'] = FCPATH . 'application/views/admin/user/templates';
        $this->load->library('crud', array('table' => 'groups', 'conf' => $conf));
        $var['main_content'] = $this->load->view('admin/user/group', array('content' => $this->crud->process()), true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }

    public function permission() {
        $this->load->model('admin/admin_menu');
        $this->load->library('session');
        
        $var = array();
        $var['main_menu'] = $this->admin_menu->fetch('user');

        $this->db->select('*');
        $this->db->from('components');
        $query = $this->db->get();
        $var['coms'] = $query->result_array();

        $groups = array();
        $query = $this->db->query('SELECT * FROM groups');
        if (!empty($query)) {
            foreach ($query->result_array() as $row) {
                $groups[] = $row;
            }
        }
        $var['groups'] = $groups;

        $query = $this->db->query('SELECT * FROM permissions');
        $pt = array();
        if (!empty($query)) {
            foreach ($query->result_array() as $k => $v) {
                $pt[$v['group_id'] . '_' . $v['com_id'].'_'.$v['permission_type']] = $v['permission_type'];
            }
        }
        $var['pt'] = $pt;

        $var['main_content'] = $this->load->view('admin/user/permission', $var, true);
        
        $this->load->model('admin/admin_footer');
        $var['main_footer'] = $this->admin_footer->fetch();

        $this->load->view('layouts/admin/user/default', $var);
    }
    
    public function user_permission(){
    	$this->load->library('session');
    	$this->load->model('admin/admin_menu');
    	$var = array();
    	$var['main_menu'] = $this->admin_menu->fetch('user');
    	$var['main_content'] = $this->load->view('admin/user/user_permission_browse', $var, true);
    	
    	$this->load->model('admin/admin_footer');
    	$var['main_footer'] = $this->admin_footer->fetch();
    	
    	$this->load->view('layouts/admin/user/default', $var);
    }
    
    public function user_json(){
    	$userDao = new ScrudDao('users', $this->db);
    	 
    	if (!isset($_GET['id'])){
    		$params = array();
    		$params['fields'] = array('id','user_name');
    		$params['conditions'] = array('user_name like ?',array("%".$_GET['q']."%"));
    		$rs = $userDao->find($params);
    		echo $_GET['callback'].'('.json_encode($rs).')';
    	}else{
    		$var = array();
    		
	    	$this->db->select('*');
        	$this->db->from('components');
        	$query = $this->db->get();
        	$var['coms'] = $query->result_array();
    
    		$params = array();
    		$params['fields'] = array('id','user_name','user_manage_flag');
    		$params['conditions'] = array('id = ?',array($_GET['id']));
    		
    		$rs = $userDao->findFirst($params);
    		$var['user'] = $rs;
    
    		$pDao = new ScrudDao('user_permissions', $this->db);
    		$params = array();
    		$params['conditions'] = array('user_id = ?',array($_GET['id']));
    
    		$rs = $pDao->find($params);
    		$pt = array();
    		if (!empty($rs)){
    			foreach($rs as $k => $v){
    				$pt[$v['user_id'].'_'.$v['com_id'].'_'.$v['permission_type']] = $v['permission_type'];
    			}
    		}
    
    		$var['pt'] = $pt;
    		
    		$this->load->view('admin/user/user_permission', $var);
    	}
    }

    public function savePermission() {
    	$this->load->library('session');
    	$groupDao = new ScrudDao('groups', $this->db);
    	$pDao = new ScrudDao('permissions', $this->db);
    	$data = $this->input->post('data');
    	$this->db->query('TRUNCATE TABLE `permissions`');
    	
    	if (count($data) > 0) {
    		foreach ($data as $k => $v) {
    			$group = array();
    			$group['group_manage_flag'] = $v['group_manage_flag'];
    			$group['group_setting_management'] = $v['group_setting_management'];
    			$group['group_global_access'] = $v['group_global_access'];
    			$groupDao->update($group,array('id = ?',array($v['group_id'])));
    			$crudAuth = $this->session->userdata('AUTH');
    			if ($v['group_id'] == $crudAuth['group']['id']){
    				$crudAuth['group']['group_manage_flag'] = $v['group_manage_flag'];
    				$crudAuth['group']['group_setting_management'] = $v['group_setting_management'];
    				$crudAuth['group']['group_global_access'] = $v['group_global_access'];
    			}
    			if (count($v['coms']) > 0){
    				$coms = $v['coms'];
    				foreach ($coms as $k1 => $v1){
    					if (count($v1['permission_type']) > 0){
    						foreach ($v1['permission_type'] as $permission){
    							if ((int)$permission > 0){
    								$p = array();
    								$p['group_id'] = $v['group_id'];
    								$p['com_id'] = $v1['com_id'];
    								$p['permission_type'] = $permission;
    								$pDao->save($p);
    							}
    						}
    					}
    				}
    			}
    		}
    	}
    }
	public  function saveUserPermission(){
		$this->load->library('session');
		$userDao = new ScrudDao('users', $this->db);
		$pDao = new ScrudDao('user_permissions', $this->db);
		$data = $this->input->post('data');
		$this->db->delete('user_permissions', array('user_id' => $data[0]['user_id']));
		
		if (count($data) > 0) {
			foreach ($data as $k => $v) {
				$user = array();
				$user['user_manage_flag'] = $v['user_manage_flag'];
				$user['user_setting_management'] = $v['user_setting_management'];
				$user['user_global_access'] = $v['user_global_access'];
				$userDao->update($user,array('id = ?',array($v['user_id'])));
				$crudAuth = $this->session->userdata('AUTH');
				if ($v['user_id'] == $crudAuth['id']){
					$crudAuth['user_manage_flag'] = $v['user_manage_flag'];
					$crudAuth['user_setting_management'] = $v['user_setting_management'];
					$crudAuth['user_global_access'] = $v['user_global_access'];
				}
				if (count($v['coms']) > 0){
					$coms = $v['coms'];
					foreach ($coms as $k1 => $v1){
						if (count($v1['permission_type']) > 0){
							foreach ($v1['permission_type'] as $permission){
								if ((int)$permission > 0){
									$p = array();
									$p['user_id'] = $v['user_id'];
									$p['com_id'] = $v1['com_id'];
									$p['permission_type'] = $permission;
									$pDao->save($p);
								}
							}
						}
					}
				}
			}
		}
	}
}