<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ebay extends CI_Controller {
    const VERSION = '1.2';
    const DATE_APPROVED = '2016-05-17';
    const MARKETPLACE_NAME = 'eBay';
    const MARKETPLACE_ID = 3;
    const ID_MODE = 1;
    const DEBUG = 9889;
    const TEST_CASE = 1;

    public $user_name;
    public $user_id;
    public $id_shop;
    public $id_lang;
    public $shop_default;
    public $language;
    public $country;
    public $id_country;
    public $ext;
    public $id_region;
    public $currency;
    public $function_call;
    public $class_call;
    public $method;
    public $dir_server;
    public $default_site;
    public $packages;
    public $id_profile;
    public $temp;

    public function __construct() 
    {
        parent::__construct();
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->user_name        = $this->session->userdata('user_name');
        $this->user_id          = $this->session->userdata('id');
        $this->id_shop          = (int)$this->session->userdata('id_shop');
        $this->shop_default     = $this->session->userdata('shop_default');
        $this->language         = $this->session->userdata('language');
        $this->dir_server       = $this->config->item('dir_server');
        $self = $this;
        $init = function($method) use ($self) {
            $self->$method();
        };

        $init_method = array(
            'autoLoader',
            'checkLogin',
            'checkShop',
            'checkLang',
            'getPackages',
            'getIDSite',
            'getConfiguration',
            'getFlag',
        );

        array_map($init, $init_method);

        $this->assign(
            array(
                "lang" => $this->language, 
                "label" => "ebay", 
                "feed_mode" => Ebay::ID_MODE, 
                "packages" => $this->packages
            )
        );

        _get_message_document(
            $this->router->fetch_class(), 
            $this->router->fetch_method()
        );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function autoLoader() 
    {   
        $this->load->model('ebay_model');
        $this->load->model('message_model');
        $this->load->model('my_feed_model');   
        $this->load->library('authentication');
        $this->load->library('Ebay/ebaylib');
        $this->load->library('unit_test');
        $this->load->library('Ebay/ObjectBiz', array($this->user_name));
        $this->config->load('regex');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkShop() 
    {
        if ((!isset($this->id_shop) && empty($this->id_shop))
            || (!isset($this->shop_default) && empty($this->shop_default))
        ) {   
            $default_shop = $this->objectbiz->getDefaultShop($this->user_name);
            if (!empty($default_shop)) {
                $this->id_shop = (int)$default_shop['id_shop'];
                $this->shop_default = $default_shop['name'];
            } else {
                redirect('users/login', 'location');
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkLogin()
    {
        if (empty($this->user_name)) {
            $this->authentication->checkAuthen(false);
            redirect('users/login', 'location');
        }
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkLang() 
    {
        if (!isset($this->language) && empty($this->language)) {
            $this->language = $this->config->item('language');
        }
        
        $self = &$this;
        $lang_default = $this->objectbiz->getLanguageDefault($this->id_shop);
        $get_lang = function($lang) use ($self) {
            if (array_key_exists('id_lang', $lang)) {
                $self->id_lang = $lang['id_lang'];
                $self->product_iso_code = $lang['iso_code'];
            }
        };

        if ( !empty($lang_default) ) {
            array_map($get_lang, $lang_default);
        }

        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->lang->load(
            strtolower(str_replace(' ', '_', Ebay::MARKETPLACE_NAME)), $this->language
        );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function loadObjectbit($params) {
            if (   $this->router->fetch_method() == 'getSelectedAllTemplate' ) :
                    $params['redirect']                     = 1;
                    $this->load->library('Ebay/ObjectBiz_Offer', $params);
                    $this->objectbiz                        = $this->objectbiz_offer;
            else :
                    $this->load->library('Ebay/ObjectBiz', $params);
            endif; 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function validation_ajax($data) {
            if ( !empty($data) && is_array($data) ) :
                    foreach ( $data as $validation) :
                            if ( isset($validation['id_site']) && is_numeric($validation['id_site']) && isset($validation['id_packages']) && is_numeric($validation['id_packages'])) :
                                    $this->user['site']                     = $validation['id_site'];
                                    $this->packages                         = $validation['id_packages'];
                            else :
                                    echo $this->lang->line('hack');
                                    exit();
                            endif;
                    endforeach;
            else :
                    echo $this->lang->line('hack');
                    exit();
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function setShop($shop_default = null) {
            if ( empty($shop_default) ) :
                    $this->authentication->checkAuthen(FALSE);
                    redirect('dashboard', 'location');
            endif;

            if(isset($shop_default) && !empty($shop_default)) :
                    $this->smarty->assign("shop_default" , $shop_default['name']);
                    $this->id_shop                          = (int)$shop_default['id_shop'];
                    $this->shop_default                     = $shop_default['name'];
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getIDSite() {
            if ( !is_numeric($this->packages) ) :
                    return NULL;
            endif;
            $id_site                                        = $this->ebay_model->get_site_from_packages($this->packages, Ebay::MARKETPLACE_ID);
            if ( !empty($id_site) && count($id_site) ) :
                    $this->session->set_userdata('id_site', $id_site['id_site_ebay']);
                    $this->default_site                     = $id_site['id_site_ebay'];
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getConfiguration($ebay_local = array(), $local_query = array(), $ebay_local_query = array()) {
            $local_query                                    = $this->ebay_model->get_configuration($this->user_id);
            $local                                          = !empty($local_query)      ? $local_query      : array();
            $user_refer                                     = $this->set_configuration($local);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !is_numeric($this->packages) ) :
                    if ( isset($user_refer['site']) && is_numeric($user_refer['site']) ) :
                            $ebay_local_query               = $this->ebay_model->get_ebay_configuration($this->user_id, $user_refer['site']);
                    endif;
            else :
                    $ebay_local_query                       = $this->ebay_model->get_ebay_configuration($this->user_id, $this->default_site);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ebay_local                                     = !empty($ebay_local_query) ? $ebay_local_query : array();
            $this->user                                     = $this->set_configuration(array_merge($local, $ebay_local));
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( isset($this->default_site) && is_numeric($this->default_site) ) :
                    $this->user['site']                     = $this->default_site;
            endif;
            $this->assign(array("id_site" => $this->default_site));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getPackages() {
            $this->packages                                 = $this->uri->segment(3);
            if ( is_numeric($this->packages) && $this->packages != Ebay::DEBUG ) :
                    $this->checkPackages();
                    return;
            elseif ( $this->uri->segment(2) == 'mapping' && is_numeric($this->uri->segment(4)) ) :
                    $this->packages                         = $this->uri->segment(4);
                    $this->checkPackages();
                    return;
            elseif ( $this->input->post() 
                    || $this->uri->segment(2) == 'log_processing' 
                    || $this->uri->segment(2) == 'statistic_processing' 
                    || $this->uri->segment(2) == 'error_resolutions_processing'
                    || $this->uri->segment(2) == 'error_resolutions_products_processing'
                    || $this->uri->segment(2) == 'error_resolution_content' 
                    || $this->uri->segment(2) == 'orders_processing'
                    || $this->uri->segment(2) == 'carts_processing'
                    || $this->uri->segment(2) == 'advance_products_processing' 
                    || $this->uri->segment(2) == 'advance_categories_processing' 
                    || $this->uri->segment(2) == 'profile_categories_processing' 
                    || $this->uri->segment(2) == 'synchronization_matching_processing' 
                    || $this->uri->segment(2) == 'advance_select_load' 
                    || $this->uri->segment(2) == 'get_configuration' 
                    || $this->uri->segment(2) == 'ebay_wizard'
                    || $this->uri->segment(3) == Ebay::DEBUG ) :
                    return;
            endif;
            $this->packages                                 = NULL;
            $this->session->unset_userdata('id_site');
            $this->checkPackages();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getFlag() {
            if ( isset($this->packages) && is_numeric($this->packages) && isset($this->user['site']) && is_numeric($this->packages) && !empty($this->session->userdata['menu']['eBay']['submenu'][$this->packages]) ) :
                    $country_site                                   = $this->session->userdata['menu']['eBay']['submenu'];
                    $ext                                            = '';
                    if ( !empty($country_site) && count($country_site) ) :
                            $substr                         = explode('.', $country_site[$this->packages]['ext']);
                            foreach ( $substr as $string ) :
                                    if ( !empty($string) && $ext == '' ) :
                                            $ext            .= $string;
                                    elseif ( !empty($string) && $ext != '' ) :
                                            $ext            .= "_".$string;
                                    endif;
                            endforeach;
                    endif;
                    $this->assign(array("flag_country" => $ext, "name_country" => $country_site[$this->packages]['country']));
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function checkPackages() {
            if ( !is_numeric($this->packages) || !isset($this->session->userdata['menu'][Ebay::MARKETPLACE_NAME]) || (isset($this->packages) && !empty($this->session->userdata['menu']) && !array_key_exists($this->packages, $this->session->userdata['menu'][Ebay::MARKETPLACE_NAME]['submenu'])) ) :
                    redirect('marketplace/configuration', 'location');
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assign() {
            $assigns                                        = func_get_args();
            foreach ( $assigns as $assign) :
                    foreach ( $assign as $key => $value ) :
                            $this->smarty->assign($key, $value);  
                    endforeach;
            endforeach;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function init_packages($data = array()) {
            if ( empty($this->packages) ) :
                    return $data;
            endif;
            $this->objectbiz->checkDB("ebay_tax");
            $this->objectbiz->checkDB("ebay_mapping_category");
            $this->objectbiz->checkDB("ebay_mapping_secondary_category");
            $this->objectbiz->checkDB("ebay_mapping_store_category");
            $this->objectbiz->checkDB("ebay_mapping_secondary_store_category");
            $this->objectbiz->checkDB("ebay_mapping_univers");
            $this->objectbiz->checkDB("mapping_listing_duration");
            $this->objectbiz->checkDB("ebay_mapping_templates");
            $this->objectbiz->checkDB("mapping_features");
            $this->objectbiz->checkDB("ebay_mapping_carrier_domestic");
            $this->objectbiz->checkDB("ebay_mapping_carrier_international");
            $this->objectbiz->checkDB("ebay_mapping_carrier_country_selected");
            $this->objectbiz->checkDB("ebay_mapping_carrier_default");
            $this->objectbiz->checkDB("ebay_mapping_attribute_selected");
            $this->objectbiz->checkDB("ebay_mapping_attribute_value");
            $this->objectbiz->checkDB("ebay_mapping_carrier_rules");
            $this->objectbiz->checkDB("ebay_mapping_condition");
            $this->objectbiz->checkDB("ebay_mapping_condition_description");
            $this->objectbiz->checkDB("ebay_store_category");
            $this->objectbiz->checkDB("ebay_statistics_log");
            $this->objectbiz->checkDB("ebay_statistics");
            $this->objectbiz->checkDB("ebay_statistics_combination");
            $this->objectbiz->checkDB("ebay_product_details");
            $this->objectbiz->checkDB("ebay_product_item_id");
            $this->objectbiz->checkDB("ebay_job_task");
            $this->objectbiz->checkDB("ebay_price_modifier");
            $this->objectbiz->checkDB("ebay_profiles_group");
            $this->objectbiz->checkDB("ebay_profiles");
            $this->objectbiz->checkDB("ebay_profiles_details");
            $this->objectbiz->checkDB("ebay_profiles_payment");
            $this->objectbiz->checkDB("ebay_profiles_return");
            $this->objectbiz->checkDB("ebay_profiles_description");
            $this->objectbiz->checkDB("ebay_profiles_condition");
            $this->objectbiz->checkDB("ebay_profiles_shipping");
            $this->objectbiz->checkDB("ebay_profiles_variation");
            $this->objectbiz->checkDB("ebay_profiles_specific");
            $this->objectbiz->checkDB("ebay_profiles_mapping");
            $this->objectbiz->checkDB("ebay_attribute_univers");
            $this->objectbiz->checkDB("ebay_synchronization_store");
            $this->objectbiz->checkDB("ebay_synchronization_attribute");
            $this->objectbiz->checkDB("ebay_attribute_override");
            $this->objectbiz->checkDB("ebay_api_logs");
            $this->objectbiz->checkDB("ebay_explode_product");

            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function init_pages($data = array()) {
            if ( empty($this->packages) ) :
                    return $data;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $prev['auth_setting']                           = 'authentication';
            $prev['payment_method']                         = 'auth_setting';
            $prev['return_method']                          = 'payment_method';
            $prev['templates']                              = 'return_method';
            $prev['price_modifier']                         = 'templates';
            $prev['mapping/mapping_carriers']               = 'price_modifier';
            $prev['mapping/mapping_carriers_cost']          = 'mapping/mapping_carriers';
            $prev['mapping/mapping_templates']              = 'mapping/mapping_carriers';
            $prev['mapping/univers']                        = 'mapping/mapping_templates';
            $prev['mapping/mapping_categories']             = 'mapping/univers';
            $prev['mapping/mapping_conditions']             = 'mapping/mapping_categories';
            $prev['mapping/variation']                      = 'mapping/mapping_conditions';
            $prev['mapping/variation_values']               = 'mapping/variation';
            $prev['actions_products']                       = 'mapping/variation_values';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $next['authentication']                         = 'auth_setting';
            $next['auth_setting']                           = 'payment_method';
            $next['payment_method']                         = 'return_method';
            $next['return_method']                          = 'templates';
            $next['templates']                              = 'price_modifier';
            $next['price_modifier']                         = 'mapping/mapping_carriers';
            $next['mapping/mapping_carriers']               = 'mapping/mapping_carriers_cost';
            $next['mapping/mapping_carriers_cost']          = 'mapping/mapping_templates';
            $next['mapping/mapping_templates']              = 'mapping/univers';
            $next['mapping/univers']                        = 'mapping/mapping_categories';
            $next['mapping/mapping_categories']             = 'mapping/mapping_conditions';
            $next['mapping/mapping_conditions']             = 'mapping/variation';
            $next['mapping/variation']                      = 'mapping/variation_values';
            $next['mapping/variation_values']               = 'actions_products';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $this->uri->segment(2) == 'mapping') :
                    $data['prev_page']                      = isset($prev['mapping/'.$this->uri->segment(3)]) ? base_url() . 'ebay/' . $prev['mapping/'.$this->uri->segment(3)] . '/' . $this->packages : '';
                    $data['next_page']                      = isset($next['mapping/'.$this->uri->segment(3)]) ? base_url() . 'ebay/' . $next['mapping/'.$this->uri->segment(3)] . '/' . $this->packages : '';
            else :
                    $data['prev_page']                      = isset($prev[$this->uri->segment(2)]) ? base_url() . 'ebay/' . $prev[$this->uri->segment(2)] . '/' . $this->packages : '';
                    $data['next_page']                      = isset($next[$this->uri->segment(2)]) ? base_url() . 'ebay/' . $next[$this->uri->segment(2)] . '/' . $this->packages : '';
            endif;
            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function init_wizard_pages($data = array()) {
            if ( empty($this->packages) ) :
                    return $data;
            endif;
            $this->getFlag();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $prev['wizard_choose']                          = 'wizard_authorization';
            $prev['wizard_shippings']                       = 'wizard_choose';
            $prev['wizard_univers']                         = 'wizard_shippings';
            $prev['wizard_categories']                      = 'wizard_univers';
            $prev['wizard_exports']                         = 'wizard_categories';
            $prev['wizard_finish']                          = 'wizard_exports';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $next['wizard_authorization']                   = 'wizard_choose';
            $next['wizard_choose']                          = 'wizard_shippings';
            $next['wizard_shippings']                       = 'wizard_univers';
            $next['wizard_univers']                         = 'wizard_categories';
            $next['wizard_categories']                      = 'wizard_exports';
            $next['wizard_exports']                         = 'wizard_finish';
            $data['next']                                   = $next;
            $data['prev']                                   = $prev;
            $next['ebay_wizard']                            = 'ebay_wizard';
            $prev['ebay_wizard']                            = 'ebay_wizard';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['prev_page']                              = isset($prev[$this->uri->segment(2)]) && isset($this->user['wizard_start']) && $this->user['wizard_start'] != 1 ? base_url() . 'ebay/' . $prev[$this->uri->segment(2)] : '';
            $data['next_page']                              = isset($next[$this->uri->segment(2)]) && isset($this->user['wizard_start']) && $this->user['wizard_start'] != count($next) ? base_url() . 'ebay/' . $next[$this->uri->segment(2)] : '';
            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function pattern_code() {
            $this->pattern_code                             = $this->config->item('postal_code');
            $this->ext_site                                 = $this->ebay_model->get_string_site($this->packages, Ebay::MARKETPLACE_ID);
            $this->pattern_code_site                        = $this->pattern_code[strtoupper($this->ext_site['iso_code'])];
            $postal_code_validation                         = "pattern='".$this->pattern_code_site."' "."country='".$this->ext_site['name']."'";
            $this->assign(array("postal_code_pattern" => $postal_code_validation));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function general() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'general',
                    'ajax_url'                              => '',
                    'btn_flow'                              => 'close',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function authentication() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-form-user-id"                 => array(
                            'required'                      => $this->lang->line('validation_required_form_user_id'),
                            'minlength'                     => $this->lang->line('validation_minlength_form_user_id'),
                    ),
            );
//                echo "<pre>", print_r($this->unit, true), "</pre>";exit();
            $test = 1 + 1;

$expected_result = 2;

$test_name = 'Adds one plus one';

echo $this->unit->run($test, $expected_result, $test_name);//exit();
//                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                $assign                                         = array(
//                        'current_page'                          => 'authentication',
//                        'ajax_url'                              => '',
//                        'data_tree'                             => 'open',
//                        'btn_flow'                              => 'close',
//                        'hidden_data'                           => array('user-not-available' => $this->lang->line('user_not_available'), 'auth-is-processing' => $this->lang->line('auth_is_processing')),
//                        'validation_data'                       => $validation,
//                        'id_site'                               => isset($this->user["site"])    ? $this->user["site"]      : '',
//                        'appMode'                               => isset($this->user["appMode"]) ? $this->user["appMode"]   : '1',
//                        'userID'                                => isset($this->user["userID"]) ? $this->user["userID"] : '',
//                );
//                $this->assign($assign);
//                $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////      
    public function prepare_configuration($profile_id = 0, $profile_type = 'default') {
            $profile_data                                   = $this->objectbiz->getEbayProfileDetails($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $ebay_listing_duration                          = $this->ebay_model->get_listing_duration();
            if ( !empty($ebay_listing_duration) ) :
                    foreach ( $ebay_listing_duration as $listing ) :
                            if ( isset($profile_data["listing_duration"]) && $listing['duration'] == $profile_data["listing_duration"] ) :
                                    if($listing['duration'] == 'GTC') :
                                            array_unshift($data['listing'], array('duration' => $listing['duration'], 'selected' => 'selected'));
                                    else :
                                            $data['listing'][]      = array('duration' => $listing['duration'], 'selected' => 'selected');
                                    endif;
                            else :
                                    if($listing['duration'] == 'GTC') :
                                            array_unshift($data['listing'], array('duration' => $listing['duration']));
                                    else :
                                            $data['listing'][]      = array('duration' => $listing['duration']);
                                    endif;
                            endif;
                    endforeach;
                    $this->smarty->assign('listing', $data['listing']);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ebay_counter                                   = $this->ebay_model->get_visitor_counter($this->user['site']);
            if ( !empty($ebay_counter) ) :
                    $counter_selected                       = 0;
                    foreach ( $ebay_counter as $counter ) :
                            if ( isset($profile_data["visitor_counter"]) && $counter['counter_value'] == $profile_data["visitor_counter"] ) :
                                    $data['visitor_counter'][]  = array('visitor_value' => $counter['counter_value'], 'visitor_name' => $counter['counter_name'], 'selected' => 'selected');
                                    $counter_selected++;
                            elseif ( empty($profile_data["visitor_counter"]) && $counter['counter_value'] == 'NoHitCounter' ) :
                                    $data['visitor_counter'][]  = array('visitor_value' => $counter['counter_value'], 'visitor_name' => $counter['counter_name'], 'selected' => 'selected');
                            else :
                                    $data['visitor_counter'][]  = array('visitor_value' => $counter['counter_value'], 'visitor_name' => $counter['counter_name']);
                            endif;
                    endforeach;
                    $this->smarty->assign(array('visitor_counter' => $data['visitor_counter']));
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $current_site                                   = $this->ebay_model->get_sites($this->user['site']);
            if ( !empty($current_site) ) :
                    foreach ( $current_site as $site) :
                            $country                        = $site->name;
                            $currency_iso                   = $site->currency;
                            $currency_name                  = $site->currency_name;
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $hidden_data                                    = array(
                    "category-jquery-full-name"             => '',
                    'category-jquery-name'                  => '',
                    'category-jquery-id'                    => '',
                    'id_profile'                            => $profile_id,
                    'profile-type'                          => $profile_type,
                    'form-template-checkbox'                => 1,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $validation                                     = array(
                    "validate-out-of-stock"                 => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                            'minvalue'                      => $this->lang->line('validation_out_of_stock_min'),
                    ),
                    "validate-maximum-quantity"             => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                            'maxvalue'                      => $this->lang->line('validation_maximum_quantity_max'),
                    ),
                    "validate-profile-name"                 => array(
                            'checked'                       => $this->lang->line('validation_profile_name_checked'),
                            'required'                      => $this->lang->line('validation_error_required'),
                    ),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'currency_iso'                          => !empty($currency_iso) ? $currency_iso : '',
                    'currency_name'                         => !empty($currency_name) ? $currency_name : '',
                    'country'                               => !empty($country) ? $country : '',
                    'hidden_data'                           => $hidden_data,
                    'validation_data'                       => $validation,
                    'data_tree'                             => 'open',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
            );
            $this->assign($assign);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function auth_setting() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_configuration(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'auth_setting',
                    'ajax_url'                              => 'advance_profile_configuration_save',
                    'id_site'                               => isset($this->user["site"]) ? $this->user["site"] : '',
                    'paypal_email'                          => isset($this->user['paypal_email']) ? $this->user["paypal_email"] : '',
                    'address'                               => isset($this->user['address']) ? $this->user["address"] : '',
                    'template'                              => isset($this->user['template']) ? $this->user["template"] : '',
                    'no_image'                              => isset($this->user['no_image']) ? $this->user["no_image"] : '',
                    'discount'                              => isset($this->user['discount']) ? $this->user["discount"] : '',
                    'sync'                                  => isset($this->user['sync']) ? $this->user["sync"] : '',
                    'just_orders'                           => isset($this->user['just_orders']) ? $this->user["just_orders"] : '',
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function auth_security_session() {
            if ($this->input->post()) {
                    $data                                                   = $_POST['data'];
                    if ( !empty($data) && count($data) ) :
                            foreach ( $data as $user ) :
                                    $user['appMode']                        = ($user['mode'] == "false") ? array(0) : array(1);
                                    $this->ebaylib->production              = $user['appMode'];
                                    $this->ebaylib->__construct($user['appMode']);
                                    $this->ebaylib->core->_template         = isset($this->user['template']) ? $this->user['template'] : 0;                        
                                    $current_site                           = $this->ebay_model->get_sites($user['id_site']);
                                    $this->ebaylib->GetSessionID($user['id_site'], $this->iso_code);
                                    if ( !empty($this->ebaylib->core->_parser) && $this->ebaylib->core->_parser->Ack == 'Success' ) :
                                            $open_link                      = ($user['appMode'][0] == 0) ? $current_site[0]->link_signin_sandbox : $current_site[0]->link_signin;
                                            $open_link                      .= "?SignIn&runame=".$this->ebaylib->core->_session['runame'];
                                            $open_link                      .= "&SessID=".$this->ebaylib->core->_parser->SessionID;
                                    elseif ( !empty($this->ebaylib->core->_parser) && $this->ebaylib->core->_parser->Ack == 'Failure') :
                                            $res = $this->ebaylib->core->_parser;
                                            if ( !empty($res->Errors) && !empty($res->Errors->ErrorParameters->Value) ) :
                                                    if( !empty($res->Errors->ErrorCode) && $res->Errors->ErrorCode == '10007' ) :
                                                            $failstring['status'] = 'Error';
                                                            $failstring['code'] = strval($res->Errors->ErrorCode);
                                                            $failstring['response'] = $this->lang->line('connection_fail')." : ".strval($res->Errors->ErrorParameters->Value);
                                                            $failstring = json_encode($failstring);
                                                    endif;
                                            endif;
                                    endif;
                            endforeach;
                            echo isset($open_link) ? $open_link : (!empty($failstring) ? $failstring : 'Fail');
                    else :
                            echo 'Fail';
                    endif;
            }
            else {
                    echo 'Fail';
            }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function auth_security_token() {
            if ($this->input->post()) {
                    $data                                           = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    if ( !empty($data) && count($data) ) :
                            foreach ( $data as $user ) :
                                    $sessionID                                      = explode('&SessID=', $user['result']);
                                    $user['appMode']                                = ($user['mode'] == "false") ? array(0) : array(1);
                                    $this->ebaylib->production                      = $user['appMode'];
                                    $this->ebaylib->__construct($user['appMode']);
                                    $this->ebaylib->ConfirmIdentity($user['id_site'], $sessionID[1]);
                                    if ( !empty($this->ebaylib->core->_parser) && $this->ebaylib->core->_parser->Ack == 'Success' ) :
                                            $confirmIdentity                        = $this->ebaylib->core->_parser->UserID->__toString();
                                    endif;
                                    $this->ebaylib->FetchToken($user['id_site'], $sessionID[1]);
                                    if ( !empty($this->ebaylib->core->_parser) && $this->ebaylib->core->_parser->Ack == 'Success' && isset($confirmIdentity) ) :
                                            $token = (array)$this->ebaylib->core->_parser->eBayAuthToken;
                                            $ebay_data                              = array(
                                                    'EBAY_SITE_ID'                  => $user['id_site'],
                                                    'EBAY_TOKEN'                    => $token[0],
                                                    'EBAY_APPMODE'                  => ($user['mode'] == "false") ? 1 : 0,
                                                    'EBAY_USER'                     => isset($confirmIdentity) ? $confirmIdentity : $user['userID'],
                                            );
                                            $result_check                           = $this->ebay_model->get_configuration_name_value($confirmIdentity, $this->user_id);
                                            if ( isset($result_check) && $result_check == 0 ) :
                                                    $result                         = $this->ebay_model->set_configuration($this->user_id, $ebay_data);
                                                    $token                          = 'OK';
                                            else :
                                                    $token                          = 'Duplicated';
                                            endif;
                                    endif;
                            endforeach;
                            echo isset($token) ? $token : 'Fail';
                    else :
                            echo 'Duplicated';
                    endif;
            }
            else {
                    echo 'Duplicated';
            }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function auth_security_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach ( $data as $user ) :
                                    $ebay_data                      = array(
                                            'EBAY_LI_DURATION'      => $user['listing_duration'],
                                            'EBAY_RE_POLICY'        => $user['returns_policy'],
                                            'EBAY_RE_INFORMATION'   => $user['returns_information'],
                                            'EBAY_RE_DAYS'          => $user['returns_within'],
                                            'EBAY_RE_PAYS'          => $user['returns_pays'],
                                            'EBAY_DISPATCH_TIME'    => $user['dispatch_time'],
                                            'EBAY_POSTAL_CODE'      => isset($user['postal']) ? $user['postal'] : '',
                                            'EBAY_ADDRESS'          => isset($user['city']) ? $user['city'] : '',
                                            'EBAY_SITE_ID'          => $user['id_site'],
                                            'PAYER_EMAIL'           => isset($user['email']) ? $user['email'] : '',
                                            'API_TEMPLATE'          => $user['template'],
                                            'EBAY_NO_IMAGE'         => $user['no_image'],
                                            'EBAY_DISCOUNT'         => $user['discount'],
                                            'EBAY_SYNCHRONIZE'      => $user['sync'],
                                            'EBAY_JUST_ORDERS'      => $user['just_orders'],
                                    );  
                            endforeach;
                            $result                                 = $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $user['id_site']);
                            echo ( !$result ) ? "Fail" : "Success";
                    else :
                            echo "Fail";
                    endif; 
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_templates() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';

            if ( file_exists($dir_name) && ($handle = opendir($dir_name)) ) :
                    while (( $file = readdir($handle)) !== false ) :
                            if ( !in_array($file, array('.', '..')) ) :
                                    $data['files'][]         = $file;
                            endif;
                    endwhile;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $mapping_selected                               = $this->objectbiz->getSelectedMappingTemplates($this->user_name, $this->user['site'], $this->id_shop);

            if(isset($mapping_selected) && !empty($mapping_selected)) 
                    $data['ebay_templates_selected']       = $mapping_selected;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_templates',
                    'ajax_url'                              => 'mapping_templates_save',
                    'root_page'                             => 'mapping/',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'select-a-option' => $this->lang->line('select_a_option'), 'default-template' => $this->lang->line('default_template')),
                    'data_tree'                             => 'open',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ////////////////////////////////////////////////////////////////////////
    function mapping_templates_save() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $this->validation_ajax($data);
                            $this->objectbiz->deleteColumn('ebay_mapping_templates', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                            $templates_insert                   = '';
                            foreach ( $data as $template ) :
                                    unset($data_templates);

                                    if ( !empty($template['template_selected']) && $template['template_selected'] != $this->lang->line('default_template') ) :
                                            $data_templates                 = array(
                                                    "id_category"           => $template['category_id'],
                                                    "template_selected"     => $template['template_selected'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "date_add"              => date("Y-m-d H:i:s", strtotime("now")),
                                            );
                                            $templates_insert               .= $this->objectbiz->mappingTableOnly('ebay_mapping_templates', $data_templates);
                                    endif;
                            endforeach;
                            $result_insert                                  = $this->objectbiz->transaction($templates_insert, true);
                            echo "Success";
                    else :
                            echo "Success";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_templates', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Success";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function univers() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $root                                           = $this->ebay_model->get_category_root($this->user['site']);
            $univer_selected                                = $this->objectbiz->getUnivers($this->user['site'], $this->id_shop);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'univers',
                    'ajax_url'                              => 'univers_save',
                    'root_page'                             => 'mapping/',
                    'root'                                  => !empty($root) ? $root : '',
                    'select_root'                           => !empty($univer_selected) ? $univer_selected : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function univers_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    $category_insert                        = '';
                    if ( !empty($data) && count($data) ) :
                            $this->objectbiz->deleteColumn('ebay_mapping_univers', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            foreach ( $data as $category ) :
                                    unset($data_insert);
                                    if ( !empty($category) && $category['category_id'] != 0 ) :
                                            $data_insert                    = array(
                                                    "id_ebay_category"      => $category['category_id'],
                                                    "id_site"               => $category['id_site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "name_ebay_category"    => $category['category_name'],
                                            );
                                            $category_insert                .= $this->objectbiz->mappingTableOnly('ebay_mapping_univers', $data_insert);
                                    endif;
                            endforeach;
                            $result_insert                                  = $this->objectbiz->transaction($category_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_univers', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function create_array_category($data = '', $categories_array = '', $root = 0, $category_f = array()) {
            $this->level_max                                = $this->ebay_model->get_level_sites($this->user['site']);
            $level                                          = 0;
            if ( !empty($data) && count($data) ) :
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    foreach ( $data as $id => $category ) :
                            $level                                  = ( $category->level > $level ) ? $category->level : $level;
                            $result['name']                         = $category->name;
                            $result['id']                           = $category->id_category;
                            $result['level']                        = $category->level;
                            $result['id_parent']                    = $category->id_parent;
                            $result['additionalParameters']         = array("children" => array());
                            $cat[$category->level][$category->id_category]                    = $result;
                    endforeach;
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    for ( $level = $this->level_max['level']; $level > 0; $level-- ) :
                            $rest                                           = array();
                            foreach ( $cat[$level] as $id_current => $current ) :
                                    if ( array_key_exists($current['id_parent'], $cat[$level]) ) :
                                            $cat[$level][$current['id_parent']]['additionalParameters']['children'] = array_replace($cat[$level][$current['id_parent']]['additionalParameters']['children'], array($id_current => $current));
                                            $rest[$id_current]      = $current['id_parent'];
                                            unset($cat[$level][$id_current]);
                                    else :
                                            if ( array_key_exists($current['id_parent'], $rest) ) :
                                                    $cat[$level][$rest[$current['id_parent']]]['additionalParameters']['children'][$current['id_parent']]['additionalParameters']['children'] = array_replace($cat[$level][$rest[$current['id_parent']]]['additionalParameters']['children'][$current['id_parent']]['additionalParameters']['children'], array($id_current => $current));
                                                    unset($cat[$level][$id_current]);
                                            endif;
                                    endif;
                            endforeach;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            foreach ( $cat[$level] as $id => $category ) :
                                    $cat2[$level][$category['id_parent']][$category['id']]   = $category;
                                    if ( $level != 1 ) :
                                            unset($cat[$level][$id]);
                                    endif;
                            endforeach;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( ($level - 1) > 0) :
                                    foreach ( $cat2[$level] as $level_key => $category ) :
                                            if ( array_key_exists($level_key, $cat[$level - 1]) ) :
                                                    $cat[$level - 1][$level_key]['additionalParameters']['children'] = array_replace($cat[$level - 1][$level_key]['additionalParameters']['children'], $category);
                                                    unset($cat2[$level][$level_key]);
                                            else :
                                                    if ( ($level - 2) > 0 ) :
                                                            for ( $i = ($level - 2); $i > 0; $i-- ) :
                                                                    if ( array_key_exists($level_key, $cat[$i]) ) :
                                                                            $cat[$i][$level_key]['additionalParameters']['children'] = array_replace($cat[$i][$level_key]['additionalParameters']['children'], $category);
                                                                            unset($cat2[$level][$level_key]);
                                                                    endif;
                                                            endfor;
                                                    endif;
                                            endif;
                                    endforeach;
                            endif;
                    endfor;
                    $category_f             = $cat[1];
            endif;
            return $category_f;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function dir_remove($dir) {
            if ( !empty($dir) && is_dir($dir)) :
                    $objects = scandir($dir);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    foreach ($objects as $object) :
                          if ($object != "." && $object != "..") :
                                if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                          endif;
                    endforeach;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function get_ebay_categories() {
            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/temp/'.$this->user['site'].'/';
            $time                                           = $this->ebay_model->get_category_max_date();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                if ( file_exists($dir_temp) && !file_exists($dir_temp.strtotime($time['Date']).'.json') && ($handle = opendir($dir_temp)) ) :
                    $this->dir_remove($dir_temp);
//                endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( file_exists($dir_temp.strtotime($time['Date']).'.json') && file_exists($dir_temp.'categories.json') && ($handle = opendir($dir_temp)) ) :
                    $ebay_category                          = json_decode(file_get_contents($dir_temp.'categories.json'), true);
            else :
                    $categories_array2                      = $this->ebay_model->get_category($this->user['site'], FALSE, array('level <' => 8));
                    $ebay_category                          = $this->mapping_category_slice($this->create_array_category($categories_array2));
                    $response['posts']                      = json_encode($ebay_category);
                    if ( !file_exists($dir_temp) ) { 
                                    $old = umask(0); 
                                    mkdir($dir_temp, 0777, true); 
                                    umask($old); 
                    }
                    file_put_contents($dir_temp.'categories.json', $response['posts']);
                    file_put_contents($dir_temp.strtotime($time['Date']).'.json', '');
            endif;
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $univer_selected                                = $this->objectbiz->getUnivers($this->user['site'], $this->id_shop);
            $ebay_category_slice                            = array();
            if ( !empty($univer_selected) ) :
            foreach ($univer_selected as $key => $univer) :
                    foreach ($ebay_category as $key_category => $category) :
                            if ( $category['root'] == $univer['id_ebay_category'] ) :
                                    $ebay_category_slice[]  = $ebay_category[$key_category];
                            endif;
                    endforeach;
            endforeach;
            endif;
            return !empty($ebay_category_slice) ? $ebay_category_slice : array();//$ebay_category;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function get_ebay_store_category() {
            $dir_temp                                       = USERDATA_PATH.$this->user_name.'/ebay/temp_store/'.$this->user['site'].'/';
            $time                                           = $this->objectbiz->getStoreCategoryMaxDate($this->user_name, $this->user['site'], $this->id_shop);
            $time                                           = strtotime($time['date']);
            $this->dir_remove($dir_temp);
            if ( file_exists($dir_temp) && !file_exists($dir_temp.$time.'.json') && ($handle = opendir($dir_temp)) ) :
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( file_exists($dir_temp.$time.'.json') && file_exists($dir_temp.'categories.json') && ($handle = opendir($dir_temp)) ) :
                    $html                                   = json_decode(file_get_contents($dir_temp.'categories.json'), true);
            else :
                    $html                                   = '';
                    $cat                                    = array();
                    $arr_selected_categories = $this->objectbiz->getRootStoreCategory($this->user_name, $this->user['site'], $this->id_shop);
                    if ( !empty($arr_selected_categories) && isset($arr_selected_categories) ) :
                            foreach ($arr_selected_categories as $asc) :
                                    $cat[$asc['id_category']]['additionalParameters']['children'] = $this->getSubStoreTree($asc['id_category']);
                                    $cat[$asc['id_category']]['id']         = $asc['id_category'];
                                    $cat[$asc['id_category']]['name']       = $asc['name_category'];
                            endforeach;
                    endif;
                    $this->smarty->assign('selected', $this->mapping_category_slice($cat));
                    $html .= $this->smarty->fetch('ebay/mapping/storeTemplate.tpl');

                    $response['posts']                      = json_encode($html);
                    if ( !file_exists($dir_temp) ) {
                            $old = umask(0);
                            mkdir($dir_temp, 0777, true);
                            umask($old);
                    }
                    if ( !empty($time) ) : 
                            file_put_contents($dir_temp.'categories.json', $response['posts']);
                            file_put_contents($dir_temp.$time.'.json', '');
                    endif;
            endif;
            return $html;
    }

    function prepare_categories($data = array()) {
            $univer_selected                                = $this->objectbiz->getUnivers($this->user['site'], $this->id_shop);
            $data['have_univer']                            = !empty($univer_selected) ? 1 : 0;

            if( !$data['have_univer'] ):
                    $data['error']                          = $this->lang->line('ebay_univers_not_selected');
            endif;
            $categories_not_leaf                            = $this->ebay_model->get_ebay_categories_not_leaf($this->user['site']);
            $categories_store_not_leaf                      = $this->generate_group('id_site', array('id_parent' => 'id'), $this->objectbiz->getLeafStoreCategory($this->user['site'], $this->user['site'], $this->id_shop));
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ebay_category                                  = $this->get_ebay_categories();
            foreach($ebay_category as $key=>$element) :
                    if(in_array($element['id'], $categories_not_leaf)) :
                            unset($ebay_category[$key]);
                    endif;
            endforeach;

            if(isset($ebay_category) && !empty($ebay_category)) 
                    $data['ebay_categories']                = $ebay_category;

            $ebay_store_category                            = $this->get_ebay_store_category();
            if(isset($ebay_store_category) && !empty($ebay_store_category)) 
                    $data['ebay_store_categories']          = $ebay_store_category;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $mapping_selected                               = $this->objectbiz->getSelectedMappingCategories($this->user_name, $this->user['site'], $this->id_shop);
            $mapping_secondary_selected                     = $this->objectbiz->getSelectedMappingSecondaryCategories($this->user_name, $this->user['site'], $this->id_shop);
            $mapping_store_selected                         = $this->objectbiz->getSelectedMappingStoreCategories($this->user_name, $this->user['site'], $this->id_shop);
            $mapping_secondary_store_selected               = $this->objectbiz->getSelectedMappingSecondaryStoreCategories($this->user_name, $this->user['site'], $this->id_shop);

            $mapping_selected = is_array($mapping_selected) ? $mapping_selected : array();
            $mapping_secondary_selected = is_array($mapping_secondary_selected) ? $mapping_secondary_selected : array();
            $mapping_store_selected = is_array($mapping_store_selected) ? $mapping_store_selected : array();
            $mapping_secondary_store_selected = is_array($mapping_secondary_store_selected) ? $mapping_secondary_store_selected : array();

            foreach($mapping_selected as $key=>$element){
                    if(in_array($element['id_ebay_category'], $categories_not_leaf)){
                            unset($mapping_selected[$key]);
                    }
            }
            foreach($mapping_secondary_selected as $key=>$element){
                    if(in_array($element['id_ebay_category'], $categories_not_leaf)){
                            unset($mapping_secondary_selected[$key]);
                    }
            }
            foreach($mapping_store_selected as $key=>$element){
                    if(!empty($categories_store_not_leaf) && !in_array($element['id_ebay_category'], $categories_store_not_leaf[$this->user['site']]['id_parent'])){
                            unset($mapping_store_selected[$key]);
                    }
            }
            foreach($mapping_secondary_store_selected as $key=>$element){
                    if(!empty($categories_store_not_leaf) && !in_array($element['id_ebay_category'], $categories_store_not_leaf[$this->user['site']]['id_parent'])){
                            unset($mapping_secondary_store_selected[$key]);
                    }
            }
            if(isset($mapping_selected) && !empty($mapping_selected)) 
                    $data['ebay_categories_selected']       = $mapping_selected;

            if(isset($mapping_secondary_selected) && !empty($mapping_secondary_selected)) 
                    $data['ebay_secondary_categories_selected'] = $mapping_secondary_selected;

            if(isset($mapping_store_selected) && !empty($mapping_store_selected)) 
                    $data['ebay_store_selected']            = $mapping_store_selected;

            if(isset($mapping_secondary_store_selected) && !empty($mapping_secondary_store_selected)) 
                    $data['ebay_secondary_store_selected'] = $mapping_secondary_store_selected;

            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_categories() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data                                           = $this->prepare_categories($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_categories',
                    'ajax_url'                              => 'mapping_categories_save',
                    'root_page'                             => 'mapping/',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'choose-ebay-store-categories' => $this->lang->line('choose_ebay_store_categories'),  'select-a-option' => $this->lang->line('select_a_option')),
                    'data_tree'                             => 'open',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonAllCategory() {
            $html = '';                   
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $dir_temp                                       = USERDATA_PATH.$this->user_name.'/ebay/temp/'.$this->user['site'].'/';
                            $time                                           = $this->objectbiz->getCategoryMaxDate($this->user_name, $this->id_shop);
                            $this->temp                                     = isset($data[0]['switch']) ? $data[0]['switch'] : false;
                            $ebay_site                                      = $this->ebay_model->get_sites($this->user['site']);
                            if (isset($this->session->userdata['menu'])){
                                $marketplace = $this->session->userdata['menu'];
                            }

                            if (isset($marketplace[Ebay::MARKETPLACE_NAME]['submenu'][$this->packages])) { 
                                $this->iso_code = $marketplace[Ebay::MARKETPLACE_NAME]['submenu'][$this->packages]['comments'];
                            }

                            $id_lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
                            if ( !empty($id_lang) ) {
                                $this->id_lang = $id_lang['id_lang'];
                            }
//                                 if ( file_exists($dir_temp) && !file_exists($dir_temp.$time['date'].'.json') && ($handle = opendir($dir_temp)) ) :
                                $this->dir_remove($dir_temp);
//                                 endif;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( file_exists($dir_temp.$time['date'].'.json') && file_exists($dir_temp.'categories.json') && ($handle = opendir($dir_temp)) ) :
                                    $html                                   = json_decode(file_get_contents($dir_temp.'categories.json'), true);
                            else :
                                    $roots = $this->objectbiz->getAllRootCategories($this->user_name, $this->id_shop, $this->id_lang, true);
                                    if(!empty($roots)) {
                                            foreach($roots as $root) {
                                                $arr_selected_categories_res = $this->objectbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, Ebay::ID_MODE, $root['id'], false, $this->id_lang);
                                                $name = isset($root['name'][$this->iso_code]) ? $root['name'][$this->iso_code] : '';
                                                if (empty($name)) { continue; }
                                                $arr_selected_categories[] = array('id_category'=>$root['id'],'name'=>$name,'parent'=>0,'type'=>'folder','child'=>sizeof($arr_selected_categories_res));
                                            }
                                    }
                                    $this->having_cat = array();
                                    $this->having_cat = $this->generate_group("date_add", array("id_category"), $this->objectbiz->getFeedbizKeyAllCategories($this->id_shop));
                                    if ( !empty($this->having_cat)  ) :
                                            $this->having_cat = current($this->having_cat);
                                    endif;

                                    foreach ($arr_selected_categories as $asc) :
                                            if ( !empty($this->having_cat) && in_array($asc['id_category'], $this->having_cat['id_category']) ) :
                                                    $sub_html                               = ''.$this->getSubCatTree($asc['id_category']);
                                                    $this->smarty->assign('checked', '');
                                                    $this->smarty->assign('selected', '');
                                                    $this->smarty->assign('switch', ($this->temp == 'mapping_categories') ? 1 : 0);
                                                    $this->smarty->assign('profile', '');
                                                    $this->smarty->assign('type', $asc['type']);
                                                    $this->smarty->assign('id_category', $asc['id_category']);
                                                    $this->smarty->assign('name', $asc['name']);
                                                    $this->smarty->assign('sub_html', $sub_html);
                                                    $html .= $this->smarty->fetch('ebay/mapping/categoryTemplate.tpl');
                                            endif;
                                    endforeach;                                    //
                                    $response['posts']                      = json_encode($html);
                                    if ( !file_exists($dir_temp) ) {
                                            $old = umask(0);
                                            mkdir($dir_temp, 0777, true);
                                            umask($old);
                                    }
                                    file_put_contents($dir_temp.'categories.json', $response['posts']);
                                    file_put_contents($dir_temp.$time['date'].'.json', '');
                            endif;
                            echo $html;
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo $html;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function getSubCatTree($root_cat_id){
            $html = '';
            $arr_selected_categories = $this->objectbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, Ebay::ID_MODE, $root_cat_id, false, $this->id_lang);//
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            foreach ($arr_selected_categories as $asc) {
                    if ( !empty($this->having_cat) && in_array($asc['id_category'], $this->having_cat['id_category']) ) :
                            $sub_html = ''.$this->getSubCatTree($asc['id_category']);                                                                      
                            $this->smarty->assign('checked', '');
                            $this->smarty->assign('selected', '');
                            $this->smarty->assign('switch', ($this->temp == 'mapping_categories') ? 1 : 0);
                            $this->smarty->assign('profile', '');                                          
                            $this->smarty->assign('type', $asc['type']);
                            $this->smarty->assign('id_category', $asc['id_category']);
                            $this->smarty->assign('name', $asc['name']);
                            $this->smarty->assign('sub_html', $sub_html);
                            $html .= $this->smarty->fetch('ebay/mapping/categoryTemplate.tpl');
                    endif;
            }
            return $html;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function getStoreCategory() {
            $html = '';                   
            $cat = array();
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :         
                            $parser = $this->ebaylib->getStore($this->user['site']);
                            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
                            $this->load->library("Ebay/ebaystore", $param);
                            $parser                             = $this->ebaystore->get_store();
                            $arr_selected_categories = $this->objectbiz->getRootStoreCategory($this->user_name, $this->user['site'], $this->id_shop);
                            if ( !empty($arr_selected_categories) && isset($arr_selected_categories) ) :
                                    foreach ($arr_selected_categories as $asc) :
                                            $cat[$asc['id_category']]['additionalParameters']['children'] = $this->getSubStoreTree($asc['id_category']);
                                            $cat[$asc['id_category']]['id']         = $asc['id_category'];
                                            $cat[$asc['id_category']]['name']       = $asc['name_category'];
                                    endforeach;
                            endif;
                            $this->smarty->assign('selected', $this->mapping_category_slice($cat));
                            $html .= $this->smarty->fetch('ebay/mapping/storeTemplate.tpl');
                            echo $html;         
                    endif;
            else :
                    $this->smarty->assign('selected', $this->mapping_category_slice($cat));
                    $html .= $this->smarty->fetch('ebay/mapping/storeTemplate.tpl');
                    echo $html;
            endif;
    }      

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function getSubStoreTree($root_cat_id) {
            $cat = array();
            $arr_selected_categories = $this->objectbiz->getChildStoreCategory($this->user_name, $this->user['site'], $this->id_shop, $root_cat_id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($arr_selected_categories) && isset($arr_selected_categories) ) :
                    foreach ($arr_selected_categories as $asc) :

                            $cat[$asc['id_category']]['additionalParameters']['children']   = $this->getSubStoreTree($asc['id_category']); 
                            $cat[$asc['id_category']]['id']                         = $asc['id_category'];
                            $cat[$asc['id_category']]['name']                       = $asc['name_category'];
                    endforeach;
            endif;
            return $cat;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_categories_save() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax(array(0 => $data[4]));

                    if ( !empty($data) && count($data) ) :
                                                            //Ebay Category Dictionary
                                    $ebay_category_dictionary = array();
                                    $ebay_category = $this->get_ebay_categories();
                                    foreach($ebay_category as $key=>$element){
                                            $element_name = trim(substr($element['name'], strrpos($element['name'], '->') + 2));
                                            $ebay_category_dictionary[$element['id']] = $element_name;
                                            unset($ebay_category[$key]);
                                    }

                                    //Feed.biz Category Dictionary
                                    $feedbiz_category_dictionary = array();
                                    $feedbiz_category = Category::getCategories($this->id_shop, $this->id_lang);
                                    foreach ($feedbiz_category as $key=>$element){
                                            foreach($element['name'] as $element_name){
                                                    $feedbiz_category_dictionary[$key] = $element_name;
                                                    continue;
                                            }
                                    }
                                    if ( !empty($data[0]) ) :
                                            $this->objectbiz->deleteColumn('ebay_mapping_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                    endif;
                                    if ( !empty($data[1]) ) :
                                            $this->objectbiz->deleteColumn('ebay_mapping_secondary_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                    endif;
                                    if ( !empty($data[2]) ) :
                                            $this->objectbiz->deleteColumn('ebay_mapping_store_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                    endif;
                                    if ( !empty($data[3]) ) :
                                            $this->objectbiz->deleteColumn('ebay_mapping_secondary_store_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                    endif;

                            $category_insert                    = '';
                            foreach ( $data as $tab_index => $categories ) :
                                    if($tab_index > 3) :
                                            continue;
                                    endif;
                                    foreach ( $categories as $category ) :
                                            if ( $tab_index == 0 ) :
                                                    $table  = 'ebay_mapping_category';
                                            elseif ( $tab_index == 1  ) :
                                                    $table  = 'ebay_mapping_secondary_category';
                                            elseif ( $tab_index == 2   ) :
                                                    $table  = 'ebay_mapping_store_category';
                                            elseif ( $tab_index == 3   ) :
                                                    $table  = 'ebay_mapping_secondary_store_category';
                                            endif;
                                            if ( $tab_index == 2 || $tab_index == 3 ) :
                                                    $ebay_store_category       = $this->objectbiz->getLeafStoreCategory($this->user_name, $this->user['site'], $this->id_shop);
                                                    foreach ($ebay_store_category as $key => $element) :
                                                            $ebay_category_dictionary[$element['id']] = $element['name'];
                                                    endforeach;
                                            endif;
                                            unset($data_insert);
                                            $val_split = explode(':', $category);                                                
                                            if ( sizeof($val_split) > 0 ) :
                                                    $category_id = $val_split[0];
                                                    $category_ebay_id = $val_split[1];
                                                    if ( !empty($ebay_category_dictionary[$category_ebay_id]) ) {
                                                            $data_insert                    = array(
                                                                    "id_category"           => $category_id,
                                                                    "id_ebay_category"      => $category_ebay_id,
                                                                    "id_site"               => $this->user['site'],
                                                                    "id_shop"               => $this->id_shop,
                                                                    "name_category"         => $feedbiz_category_dictionary[$category_id],//$category['category_name'],
                                                                    "name_ebay_category"    => $ebay_category_dictionary[$category_ebay_id],//$category['category_ebay_name'],
                                                                    "status"                => 1,
                                                            );
                                                            $category_insert                .= $this->objectbiz->mappingTableOnly($table, $data_insert);
                                                    }
                                            endif;
                                    endforeach;
                            endforeach;
                            $result_insert                                  = $this->objectbiz->transaction($category_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_category_slice($categories = array(), $id = '', $string = '', $result = array(), $root = '', $root_name = '') {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( is_array($categories) && !empty($categories) ) :
                    foreach ( $categories as $key => $category ) :
                            $category                                   = (array)$category;
                            if ( empty($string) ) :
                                    $string_new                         = $category['name'];
                                    $root                               = $key; 
                                    $root_name                          = $category['name']; 
                            else :
                                    $string_new                         = $string." -> ".$category['name'];
                            endif;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( isset($category['additionalParameters']) ) :
                                    if ( is_object($category['additionalParameters']) ) :
                                            $category['child']          = $category['additionalParameters']->children;        
                                    else :
                                            $category['child']          = $category['additionalParameters']['children'];        
                                    endif;
                            endif;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( !empty($category['child']) && count($category['child']) ) :
                                    $result                             = $this->mapping_category_slice($category['child'], $category['id'], $string_new, $result, $root, $root_name);  
                            else :
                                    $result[]                           = array('id' => $category['id'], 'name' => $string_new, 'root' => $root, 'root_name' => $root_name);
                            endif;

                    endforeach;
            endif;
            return $result;
    }

    private function prepare_carriers($data = null) {       
            $condition_data                                 = array();
            $json_tab                                       = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $tab_data['type']                               = $type;
            $selected                                       = $this->objectbiz->getRealCarriers($this->id_shop);
            $is_default                                     = $this->generate_key('is_default', $selected);
            $ebay_carriers                                  = $this->ebay_model->get_carrier($this->user['site']);
            $ebay_country                                   = $this->ebay_model->get_carrier_country($this->user['site']);
            $ebay_site                                      = $this->ebay_model->get_sites($this->user['site']);
            $domestic_selected                              = $this->objectbiz->getEbayMappingCarrierDomestic($this->user['site'], $this->id_shop, $this->id_profile);
            $international_selected                         = $this->objectbiz->getEbayMappingCarrierInternational($this->user['site'], $this->id_shop, $this->id_profile);
            $country_selected                               = $this->objectbiz->getEbayMappingCarrierCountrySelected($this->user['site'], $this->id_shop, $this->id_profile);
            $rules                                          = $this->objectbiz->getShippingRules($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $rules_id                                       = $this->objectbiz->getRulesID($this->user['site'], $this->id_shop, $this->id_profile);
            $currentCurrency                                = $this->objectbiz->getCurrentCurrency($this->id_shop);
            $getDefault                                     = $this->objectbiz->getDefaultCarrier($this->user['site'], $this->id_shop, $this->id_profile);
            $getDomestic                                    = $this->objectbiz->geteBayMappingDomestic($this->user['site'], $this->id_shop, $this->id_profile);//echo "<pre>", print_r($getDomestic, true), "</pre>";exit();
            $getInternational                               = $this->objectbiz->geteBayMappingInternational($this->user['site'], $this->id_shop, $this->id_profile);
            $getProfileMapping                              = $this->objectbiz->getEbayProfileShipping($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $getPostalcode                                  = $this->objectbiz->getPostalcodeDispatchtime($this->user_name, $this->user['site'], $this->id_shop);
            $ebay_dispatch_time                             = $this->ebay_model->get_dispatch_time();
            if ( !empty($rules_id) ) :
                    $rules_id                               = current($rules_id);
            endif; 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($getProfileMapping) ) :
                    $data['sp_dispatch_time']               = array();
                    $profile                                = current($getProfileMapping);
            endif;
            if ( !empty($ebay_dispatch_time) ) :
                    foreach ( $ebay_dispatch_time as $dispatch_time ) :
                            if ( isset($getPostalcode[$this->id_profile]['dispatch_time']) && $dispatch_time['dispatch_time'] == $getPostalcode[$this->id_profile]['dispatch_time'] ) :
                                    $data['dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time'], 'selected' => 'selected');
                            else :
                                    $data['dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time']);
                            endif;

                            if ( isset($profile['dispatch_time']) && $dispatch_time['dispatch_time'] == $profile['dispatch_time'] ) :
                                    $data['sp_dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time'], 'selected' => 'selected');
                            else :
                                    $data['sp_dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time']);
                            endif;
                    endforeach;
                    $this->assign(array('sp_dispatch_time' => $data['sp_dispatch_time'], 'sp_postal_code' => isset($profile['postal_code']) ? $profile['postal_code'] : ''));
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if ( !empty($getPostalcode) ) :
                    $this->assign(array('dispatch_time' => $getPostalcode[$this->id_profile]['dispatch_time'], 'postal_code' => isset($getPostalcode[$this->id_profile]['postcode']) ? $getPostalcode[$this->id_profile]['postcode'] : ''));
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($getProfileMapping) ) :
                    $data['sp_dispatch_time']               = array();
                    $profile                                = current($getProfileMapping);
            endif;

            if ( !empty($ebay_dispatch_time) ) :
                    foreach ( $ebay_dispatch_time as $dispatch_time ) :
                            if ( isset($profile['dispatch_time']) && $dispatch_time['dispatch_time'] == $profile['dispatch_time'] ) :
                                    $data['sp_dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time'], 'selected' => 'selected');
                            else :
                                    $data['sp_dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time']);
                            endif;
                    endforeach;
                    $this->assign(array('sp_dispatch_time' => $data['sp_dispatch_time'], 'sp_postal_code' => isset($profile['postal_code']) ? $profile['postal_code'] : ''));
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $international_genarate                         = array();
            $getInternational                               = $this->generate_key('id_ebay_shipping', $getInternational);
            if ( !empty($country_selected) && count($country_selected) ) : 
            foreach ( $country_selected as $country ) :         
                    $international_genarate[$country['country_service']]
                    [$country['id_ebay_shipping']]                          = array(
                            'id_ebay_shipping'                              => $country['id_ebay_shipping'],
                            'ebay_name'                                     => $country['name_ebay_country'],
                    );

                    if ( !empty($international_selected) && count($international_selected) ) :
                            foreach ( $international_selected as $international) :
                                    if ( in_array($country['id_ebay_shipping'], $international) && in_array($country['country_service'], $international)) :
                                            $international_genarate[$country['country_service']][$country['id_ebay_shipping']]['id_shipping']                   = $international['id_shipping'];
                                            $international_genarate[$country['country_service']][$country['id_ebay_shipping']]['name_shipping']                 = $international['name_shipping'];
                                            $international_genarate[$country['country_service']][$country['id_ebay_shipping']]['name_ebay_shipping_service']    = $international['name_ebay_shipping_service'];
                                            $international_genarate[$country['country_service']][$country['id_ebay_shipping']]['name_ebay_shipping']            = $international['name_ebay_shipping'];
                                            $international_genarate[$country['country_service']][$country['id_ebay_shipping']]['id_rule']                       = $international['id_rule'];
                                    endif;
                            endforeach;
                    endif;
            endforeach;
            endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            $this->pattern_code();
            $this->pattern_code                             = $this->config->item('currency_code');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
            $code_real                                      = $this->ebay_model->get_postalcode_real($this->user["site"]);
            $replace_target                                 = array('{1}', '{2}', '{BR}');
            $replace_pattern                                = array($this->ext_site['name'], !empty($code_real['exam_code']) ? $code_real['exam_code'] : '', "<BR>");
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-carrier_rule"                 => array(
                            'required'                      => $this->lang->line('validation_carrier_rule_required'),
                            'select'                        => $this->lang->line('validation_select_required'),
                            'input'                         => $this->lang->line('validation_carrier_rule_required'),
                    ),
                    "validate-form-postal-code"             => array(
                            'required'                      => $this->lang->line('validation_required_form_postal_code'),
                            'regex'                         => str_replace($replace_target, $replace_pattern, $this->lang->line('validation_regex_form_postal_code')),
                            'postal-valid'                  => str_replace($replace_target, $replace_pattern, $this->lang->line('validation_postal_valid_form_postal_code')),
                    ),
                    "validate-price-modifier"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                            'range'                         => $this->lang->line('validation_range_required'),
                            'between'                       => $this->lang->line('validation_between_required'),
                            'range-weight'                  => $this->lang->line('validation_range_weight_required'),
                            'between-weight'                => $this->lang->line('validation_between_weight_required'),
                    ),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['domestic_selected']                      = !empty($domestic_selected) ? $domestic_selected : array(0);
            $data['international_selected']                 = !empty($international_selected) ? $international_selected : array(0);
            $data['country_selected']                       = !empty($country_selected) ? $country_selected : '';
            $data['carrier_conf']                           = !empty($rules_id) ? $rules_id['id_rule'] : 0;
            $data['international_genarate']                 = !empty($international_genarate) ? $international_genarate : array(0);
            $data['rules']                                  = !empty($rules) ? $rules : array(0);
            $data['data_output']                            = !empty($data_output) ? json_encode($data_output) : json_encode(array());
            $data['selected']                               = !empty($selected) ? $selected : '';
            $data['ebay_carriers']                          = !empty($ebay_carriers) ? $ebay_carriers : '';
            $data['is_default']                             = !empty($is_default) ? $is_default : '';
            $data['ebay_country']                           = !empty($ebay_country) ? $ebay_country : '';
            $data['currency']                               = !empty($ebay_site[0]->currency) ? $ebay_site[0]->currency : '';
            $data['current']                                = !empty($currentCurrency['iso_code']) ? $currentCurrency['iso_code'] : '';
            $data['profile_shipping']                       = !empty($getProfileMapping) ? $getProfileMapping : '';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( (!empty($data['profile_shipping'][0]) && $data['profile_shipping'][0]['is_enabled'] == '1') || (!empty($tab_data['type']) && $tab_data['type'] == 'product') ) :
                    $tab_data['tab']                        = 2;
            else :
                    $tab_data['tab']                        = 1;
            endif;
            $json_tab                                       = array(
                'current_active'                            => $tab_data['tab'],
                'tab_data'                                  => array(
                        1                                   => array(
                                'tab_active_help'           => $this->lang->line('Switch to ON will activate this tab and inactivate "Fixed Option"'),
                                'tab_deactive_help'         => $this->lang->line('Switch to OFF will inactivate this tab and activate "Fixed Option"'),
                                'active'                    => $tab_data['tab'] == 1 ? 1 : 0,  
                        ),
                        2                                   => array(
                                'tab_active_help'           => $this->lang->line('Switch to ON will activate this tab and inactivate "Matching Option"'),
                                'tab_deactive_help'         => $this->lang->line('Switch to OFF will inactivate this tab and activate "Matching Option"'),
                                'active'                    => $tab_data['tab'] == 2 ? 1 : 0,  
                        ),
                )

            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array(
                            'id_profile'                    => $this->id_profile, 
                            'form-template-checkbox'        => 1, 
                            'choose-a-country'              => $this->lang->line('choose_a_country'), 
                            'associate-carrier'             => $this->lang->line('associate_carrier'), 
                            'ebay-carrier'                  => $this->lang->line('ebay_carrier'),
                            'skip-url'                  	=> base_url().'ebay/mapping/mapping_templates/'.$this->packages,
                            'carrier-configuration'         => !empty($rules_id) ? $rules_id['id_rule'] : 3,
                    ),
                    'currency_format'                       => 'open',
                    'ng'                                    => 1,
                    'app_data'                              => json_encode($json_tab),
                    'currency_code'                         => json_encode($this->pattern_code),
                    'code'                                  => isset($code_real) ? $code_real : '',
                    'tab_data'                              => isset($tab_data) ? $tab_data : '',
                    'dispatch_time'                         => !empty($data['dispatch_time']) ? $data['dispatch_time'] : '',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'default_selected'                      => !empty($getDefault) ? $getDefault : '',
                    'domestic_selected'                     => !empty($data['domestic_selected']) ? $data['domestic_selected'] : '',
                    'international_selected'                => !empty($data['international_selected']) ? $data['international_selected'] : '',
                    'country_selected'                      => !empty($data['country_selected']) ? $data['country_selected'] : '',
                    'carrier_conf'                          => !empty($data['carrier_conf']) ? $data['carrier_conf'] : '',
                    'international_genarate'                => !empty($data['international_genarate']) ? $data['international_genarate'] : '',
                    'rules'                                 => !empty($data['rules']) ? $data['rules'] : '',
                    'data_output'                           => !empty($data['data_output']) ? $data['data_output'] : '',
                    'selected'                              => !empty($data['selected']) ? $data['selected'] : '',
                    'ebay_carriers'                         => !empty($data['ebay_carriers']) ? $data['ebay_carriers'] : '',
                    'ebay_country'                          => !empty($data['ebay_country']) ? $data['ebay_country'] : '',
                    'currency'                              => !empty($data['currency']) ? $data['currency'] : '',
                    'current'                               => !empty($data['current']) ? $data['current'] : '',
                    'profile_shipping'                      => !empty($data['profile_shipping']) ? $data['profile_shipping'] : '',
            );
            $this->assign($assign);
            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_carriers() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $this->id_profile                               = 0;
            $data                                           = $this->prepare_carriers($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_carriers',
                    'ajax_url'                              => 'mapping_carriers_save',
                    'root_page'                             => 'mapping/',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function prepare_carriers_cost($data = null) {
            $condition_data                                 = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $tab_data['type']                               = $type;
            $selected                                       = $this->objectbiz->getRealCarriers($this->id_shop);
            $ebay_carriers                                  = $this->ebay_model->get_carrier($this->user['site']);
            $ebay_country                                   = $this->ebay_model->get_carrier_country($this->user['site']);
            $ebay_site                                      = $this->ebay_model->get_sites($this->user['site']);
            $domestic_selected                              = $this->objectbiz->getEbayMappingCarrierDomestic($this->user['site'], $this->id_shop, $this->id_profile);
            $international_selected                         = $this->objectbiz->getEbayMappingCarrierInternational($this->user['site'], $this->id_shop, $this->id_profile);
            $country_selected                               = $this->objectbiz->getEbayMappingCarrierCountrySelected($this->user['site'], $this->id_shop, $this->id_profile);
            $postal_time                                    = $this->generate_key('id_profile', $this->objectbiz->getPostalcodeDispatchtime($this->user_name, $this->user['site'], $this->id_shop));
            $rules_id                                       = $this->objectbiz->getRulesID($this->user['site'], $this->id_shop, $this->id_profile);
            $currentCurrency                                = $this->objectbiz->getCurrentCurrency($this->id_shop);
            $getDefault                                     = $this->objectbiz->geteBayMappingDefault($this->user['site'], $this->id_shop, $this->id_profile);
            $getDomestic                                    = $this->objectbiz->geteBayMappingDomestic($this->user['site'], $this->id_shop, $this->id_profile);
            $getInternational                               = $this->objectbiz->geteBayMappingInternational($this->user['site'], $this->id_shop, $this->id_profile);
            $getMerge                                       = array_merge(!empty($getDefault) ? $getDefault : array(), !empty($getDomestic) ? $getDomestic : array());
            $getMerge                                       = array_merge(!empty($getMerge) ? $getMerge : array(), !empty($getInternational) ? $getInternational : array());
            $getDomestic                                    = $this->generate_key('rel', $getMerge);
            if ( !empty($rules_id) ) :
                    $rules_id                               = current($rules_id);
            endif; 
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            $this->pattern_code                             = $this->config->item('currency_code');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
            $code_real                                      = $this->ebay_model->get_postalcode_real($this->user["site"]);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-carrier_rule"                 => array(
                            'required'                      => $this->lang->line('validation_carrier_rule_required'),
                            'select'                        => $this->lang->line('validation_select_required'),
                            'input'                         => $this->lang->line('validation_carrier_rule_required'),
                    ),
                    "validate-price-modifier"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                            'range'                         => $this->lang->line('validation_range_required'),
                            'between'                       => $this->lang->line('validation_between_required'),
                            'range-weight'                  => $this->lang->line('validation_range_weight_required'),
                            'between-weight'                => $this->lang->line('validation_between_weight_required'),
                    ),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////          
            $assign                                         = array(
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array(
                            'id_profile'                    => $this->id_profile, 
                            'form-template-checkbox'        => 1, 
                    ),
                    'currency_format'                       => 'open',
                    'currency_code'                         => json_encode($this->pattern_code),
                    'code'                                  => isset($code_real) ? $code_real : '',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'domestic_selected'                     => !empty($domestic_selected) ? $domestic_selected : '',
                    'international_selected'                => !empty($international_selected) ? $international_selected : '',
                    'country_selected'                      => !empty($country_selected) ? $country_selected : '',
                    'carrier_conf'                          => !empty($rules_id) && $rules_id['id_rule'] != 0 ? $rules_id['id_rule'] : 3,
                    'international_genarate'                => !empty($international_genarate) ? $international_genarate : '',
                    'rules'                                 => !empty($rules) ? $rules : '',
                    'data_output'                           => !empty($data_output) ? json_encode($data_output) : '',
                    'selected'                              => !empty($selected) ? $selected : '',
                    'ebay_carriers'                         => !empty($ebay_carriers) ? $ebay_carriers : '',
                    'ebay_country'                          => !empty($ebay_country) ? $ebay_country : '',
                    'currency'                              => !empty($ebay_site[0]->currency) ? $ebay_site[0]->currency : '',
                    'current'                               => !empty($currentCurrency['iso_code']) ? $currentCurrency['iso_code'] : '',
                    'getDomestic'                           => !empty($getDomestic) ? $getDomestic : '',
            );
            $this->assign($assign);
            return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_carriers_cost() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $this->id_profile                               = 0;
            $data                                           = $this->prepare_carriers_cost($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_carriers_cost',
                    'ajax_url'                              => 'mapping_carriers_cost_save',
                    'root_page'                             => 'mapping/',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ////////////////////////////////////////////////////////////////////////
    function mapping_carriers_save() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $data_carriers                              = (isset($_POST['data_carriers']) ? $_POST['data_carriers'] : '');
                    $data_international_carriers                = (isset($_POST['data_international_carriers']) ? $_POST['data_international_carriers'] : '');
                    $data_country                               = (isset($_POST['data_country']) ? $_POST['data_country'] : '');
                    $data_rules                                 = (isset($_POST['data_rules']) ? $_POST['data_rules'] : '');
                    $data_fixed                                 = (isset($_POST['data_fixed']) ? $_POST['data_fixed'] : '');
                    $data_ref                                   = $this->generate_key('id_carrier', $this->objectbiz->getCarrierRef($this->id_shop));

                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            if ( !empty($data_fixed) && count($data_fixed) ) :
                                    $id_profile = current($data_fixed);
                                    $this->objectbiz->deleteColumn('ebay_profiles_shipping', array('id_profile' => $id_profile['id_profile']));
                                    if ( isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                            unset($data_insert, $data_set);
                                            $data_set                       = array(
                                                    'is_enabled'            => '0',
                                            );
                                            $where_set                      = array(
                                                    'id_profile'            => $id_profile['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                            );
                                            $this->objectbiz->updateColumn('ebay_mapping_carrier_rules', $data_set, $where_set);
                                    endif;
                            else :
                                    if ( !empty($data_carriers) ) :
                                            $id_profile = current($data_carriers);
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_domestic', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_international', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_country_selected', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_default', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            if ( isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                                    unset($data_set, $where_set);
                                                    $data_set                       = array(
                                                            'is_enabled'            => '0',
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $id_profile['id_profile'],
                                                    );
                                                    $this->objectbiz->updateColumn('ebay_profiles_shipping', $data_set, $where_set);
                                            endif;
                                    elseif ( !empty($data_rules) ) :
                                            $id_profile = current($data_rules);
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_domestic', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_international', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_country_selected', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            $this->objectbiz->deleteColumn('ebay_mapping_carrier_default', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                                            if ( isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                                    unset($data_set, $where_set);
                                                    $data_set                       = array(
                                                            'is_enabled'            => '0',
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $id_profile['id_profile'],
                                                    );
                                                    $this->objectbiz->updateColumn('ebay_profiles_shipping', $data_set, $where_set);
                                            endif;
                                    endif;
                            endif;
                            if ( !empty($data_carriers) && count($data_carriers) ) :
                                    foreach ( $data_carriers as $carriers ) :
                                            unset($data_insert);
                                            $data_insert                    = array(
                                                    "id_profile"            => isset($carriers['id_profile']) && is_numeric($carriers['id_profile']) ? $carriers['id_profile'] : 0,
                                                    "id_shipping"           => $carriers['carrier_id'],
                                                    "id_ebay_shipping"      => $carriers['carrier_ebay_id'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "name_shipping"         => $carriers['carrier_name'],
                                                    "name_ebay_shipping_service" => $carriers['carrier_service'],  
                                                    "name_ebay_shipping"    => $this->html_special($carriers['carrier_ebay_name']),
                                                    "id_rule"               => $carriers['carrier_rule_id'],
                                                    "postcode"              => !empty($carriers['postcode']) ? $carriers['postcode'] : 0,
                                                    "dispatch_time"         => isset($carriers['dispatch_time']) && is_numeric($carriers['dispatch_time']) ? $carriers['dispatch_time'] : 3,
                                                    "id_carrier_ref"        => !empty($data_ref[$carriers['carrier_id']]) ? $data_ref[$carriers['carrier_id']]['id_carrier_ref'] : $carriers['carrier_id'],
                                            );
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_mapping_carrier_domestic", $data_insert);
                                    endforeach;
                                    echo ( $result_insert ) ? " Add Domestic shipping. " : "Fail. ";
                            endif;

                            if ( !empty($data_international_carriers) && count($data_international_carriers) ) :
                                    foreach ( $data_international_carriers as $international ) :
                                            unset($data_insert);
                                            $data_insert                    = array(
                                                    "id_profile"            => isset($international['id_profile']) && is_numeric($international['id_profile']) ? $international['id_profile'] : 0,
                                                    "id_shipping"           => $international['carrier_id'],
                                                    "id_ebay_shipping"      => $international['carrier_ebay_id'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "name_shipping"         => $international['carrier_name'],
                                                    "name_ebay_shipping_service" => $international['carrier_service'],  
                                                    "name_ebay_shipping"    => $this->html_special($international['carrier_ebay_name']),
                                                    "id_rule"               => $international['carrier_rule_id'],
                                                    "country_service"       => $international['country_service'],
                                                    "postcode"              => !empty($international['postcode']) ? $international['postcode'] : 0,
                                                    "dispatch_time"         => isset($international['dispatch_time']) && is_numeric($international['dispatch_time']) ? $international['dispatch_time'] : 3,
                                                    "id_carrier_ref"        => !empty($data_ref[$international['carrier_id']]) ? $data_ref[$international['carrier_id']]['id_carrier_ref'] : $international['carrier_id'],
                                            );
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_mapping_carrier_international", $data_insert);
                                    endforeach;
                                    echo ( $result_insert ) ? " Add International Shipping." : "Fail. ";
                            endif;

                            if ( !empty($data_country) && count($data_country) ) :
                                    foreach ( $data_country as $country ) :
                                            unset($data_insert);
                                            $data_insert                    = array(
                                                    "id_profile"            => isset($country['id_profile']) && is_numeric($country['id_profile']) ? $country['id_profile'] : 0,
                                                    "id_ebay_shipping"      => $country['carrier_id'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "country_service"       => $country['country_service'],
                                                    "name_ebay_country"     => $this->html_special($country['country_name']),
                                            );
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_mapping_carrier_country_selected", $data_insert);
                                    endforeach;
                                    echo ( $result_insert ) ? " Add Country Shipping." : "Fail. ";
                            endif;

                            if ( !empty($data_rules) && count($data_rules) ) :
                                    foreach ( $data_rules as $rules ) :
                                            unset($data_insert);
                                            $data_insert                    = array(
                                                    "id_profile"            => isset($rules['id_profile']) && is_numeric($rules['id_profile']) ? $rules['id_profile'] : 0,
                                                    "id_rule"               => $rules['carrier_rule_id'],
                                                    "id_ebay_shipping"      => $rules['carrier_ebay_id'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "name_ebay_shipping_service" => $rules['carrier_service'],  
                                                    "name_ebay_shipping"    => $this->html_special($rules['carrier_ebay_name']),
                                                    "is_enabled"            => !empty($rules['is_default']) ? '1' : '0',
                                                    "postcode"              => !empty($rules['postcode']) ? $rules['postcode'] : 0,
                                                    "dispatch_time"         => isset($rules['dispatch_time']) && is_numeric($rules['dispatch_time']) ? $rules['dispatch_time'] : 3,
                                            );
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_mapping_carrier_default", $data_insert);
                                    endforeach;
                                    echo ( $result_insert ) ? " Add Rules Shipping." : "Fail. ";
                            endif;

                            if ( !empty($data_carriers) ) : 
                                    $id_profile = $data_carriers[0]; 
                                    if ( isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                            unset($data_set, $where_set);
                                            $data_set               = array(
                                                    'is_enabled'    => '1',
                                            );
                                            $where_set              = array(
                                                    'id_profile'    => $id_profile['id_profile'],
                                            );
                                            $this->objectbiz->updateColumn('ebay_mapping_carrier_rules', $data_set, $where_set);
                                    endif;
                            elseif ( !empty($data_rules) ) :
                                    $id_profile = $data_rules[0];
                                    if ( isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                            unset($data_set, $where_set);
                                            $data_set               = array(
                                                    'is_enabled'    => '1',
                                            );
                                            $where_set              = array(
                                                    'id_profile'    => $id_profile['id_profile'],
                                            );
                                            $this->objectbiz->updateColumn('ebay_mapping_carrier_rules', $data_set, $where_set);
                                    endif;
                            endif;

                            if ( !empty($data_fixed) && count($data_fixed) ) :
                                    foreach ( $data_fixed as $fixed ) :
                                            unset($data_insert, $data_detail, $data_mapping);
                                            $data_set = array();
                                            $where_set = array();
                                            if ( isset($fixed['id_profile']) && is_numeric($fixed['id_profile']) ) :
                                                    if ( !empty($fixed['step']) ) :
                                                            $data_set                       = array(
                                                                    'completed'             => round((100 * $fixed['step']) / 9, 2),
                                                            );
                                                            $where_set                      = array(
                                                                    'id_profile'            => $fixed['id_profile'],
                                                                    'id_site'               => $this->user['site'],
                                                                    'id_shop'               => $this->id_shop,
                                                            );
                                                    endif;
                                            endif;
                                            $data_insert                            = array(
                                                    "id_profile"                    => isset($fixed['id_profile']) && is_numeric($fixed['id_profile']) ? $fixed['id_profile'] : 0,
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "id_ebay_shipping"              => $fixed['carrier_ebay_id'],
                                                    "name_ebay_shipping_service"    => !empty($fixed['carrier_service']) ? $fixed['carrier_service'] : 0,
                                                    "name_ebay_shipping"            => !empty($fixed['carrier_ebay_name']) ? $fixed['carrier_ebay_name'] : 0,
                                                    "country_service"               => !empty($fixed['country_service']) ? $fixed['country_service'] : '',
                                                    "name_ebay_country"             => !empty($fixed['country_name']) ? $this->html_special($fixed['country_name']) : null,
                                                    "shipping_cost"                 => !empty($fixed['carrier_cost']) ? $fixed['carrier_cost'] : null,
                                                    "shipping_additionals"          => !empty($fixed['carrier_additionals']) ? $fixed['carrier_additionals'] : 0,
                                                    "postcode"                      => !empty($fixed['postcode']) ? $fixed['postcode'] : 0,
                                                    "dispatch_time"                 => isset($fixed['dispatch_time']) && is_numeric($fixed['dispatch_time']) ? $fixed['dispatch_time'] : 3,
                                                    "is_enabled"                    => isset($fixed['tab']) && $fixed['tab'] == 2 ? '1' : '0',
                                            );
                                            $details_insert                 = $this->objectbiz->updateColumn('ebay_profiles', $data_set, $where_set);
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_profiles_shipping", $data_insert);
                                    endforeach;

                                    if($result_insert) :
                                            echo $fixed['tab'] == 2 ? "Skip Rules." : "Add eBay Shipping.";
                                    else :
                                            echo "Fail.";
                                    endif;
                            endif;
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Fail";
            endif;
    }

    ////////////////////////////////////////////////////////////////////////
    function mapping_carriers_cost_save() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $data_rules                                 = (isset($_POST['data_rules']) ? $_POST['data_rules'] : '');
                    $data_ref                                   = $this->generate_key('id_carrier', $this->objectbiz->getCarrierRef($this->id_shop));
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            if ( !empty($data_rules) ) :
                                    $id_profile = current($data_rules);
                                    $this->objectbiz->deleteColumn('ebay_mapping_carrier_rules', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                            endif;
                            if ( !empty($data_rules) && count($data_rules) ) :
                                    foreach ( $data_rules as $rules ) :
                                            unset($data_insert, $data_set, $where_set);
                                            $data_insert                    = array(
                                                    "id_profile"            => isset($rules['id_profile']) && is_numeric($rules['id_profile']) ? $rules['id_profile'] : 0,
                                                    "id_rule"               => $rules['carrier_rule_id'],
                                                    "id_shipping"           => $rules['carrier_id'],
                                                    "id_ebay_shipping"      => $rules['carrier_ebay_id'],
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "is_international"      => $rules['carrier_is'],
                                                    "country_service"       => !empty($rules['country_service']) ? $rules['country_service'] : 0,
                                                    "shipping_cost"         => !empty($rules['carrier_cost']) ? $rules['carrier_cost'] : 0,
                                                    "shipping_additionals"  => !empty($rules['carrier_additionals']) ? $rules['carrier_additionals'] : 0,
                                                    "min_weight"            => !empty($rules['carrier_min_weight']) ? $rules['carrier_min_weight'] : 0,
                                                    "max_weight"            => !empty($rules['carrier_max_weight']) ? $rules['carrier_max_weight'] : 0,
                                                    "min_price"             => !empty($rules['carrier_min_price']) ? $rules['carrier_min_price'] : 0,
                                                    "max_price"             => !empty($rules['carrier_max_price']) ? $rules['carrier_max_price'] : 0,
                                                    "rel"                   => !empty($rules['carrier_rel']) ? $rules['carrier_rel'] : 0,
                                                    "surcharge"             => !empty($rules['carrier_surcharge']) ? $rules['carrier_surcharge'] : 0,
                                                    "operations"            => !empty($rules['carrier_operation']) ? $rules['carrier_operation'] : 0,
                                                    "is_enabled"            => '1',
                                                    "id_carrier_ref"        => !empty($data_ref[$rules['carrier_id']]) ? $data_ref[$rules['carrier_id']]['id_carrier_ref'] : $rules['carrier_id'],
                                            );
                                            $result_insert                  = $this->objectbiz->mappingTable("ebay_mapping_carrier_rules", $data_insert);
                                            if ( $result_insert && isset($id_profile['id_profile']) && is_numeric($id_profile['id_profile']) ) :
                                                    $data_set               = array(
                                                            'id_rule'       => $rules['carrier_rule_id'],
                                                    );

                                                    $where_set              = array(
                                                            'id_profile'    => $id_profile['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            "id_shop"       => $this->id_shop,
                                                    );
                                                    $this->objectbiz->updateColumn('ebay_mapping_carrier_domestic', $data_set, $where_set);
                                                    $this->objectbiz->updateColumn('ebay_mapping_carrier_international', $data_set, $where_set);
                                                    $this->objectbiz->updateColumn('ebay_mapping_carrier_default', $data_set, $where_set);
                                            endif;
                                    endforeach;
                                    echo ( $result_insert ) ? " Add Rules Shipping." : "Fail. ";
                            endif;
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_mapping_category', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Fail";
            endif;
    }

    function getCategoryEbayParent($result = array()) {
            if ( is_numeric($result) ) :
                    $result = $this->ebay_model->get_category_by_id($result);
                    return $this->getCategoryEbayParent($result);
            elseif ( $result->id_parent != 0 ) :
                    $this->name_category[$result->id_category]      = $result->id_category;
                    $result = $this->ebay_model->get_category_by_id($result->id_parent);
                    return $this->getCategoryEbayParent($result);
            else :
                    $this->name_category[$result->id_category]      = $result->id_category;
                    return $result;
            endif;
    }

    function prepare_attributes($profile_id = 0) {
            $tab_data                                       = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $profile_configuration                          = $this->objectbiz->getEbayProfileConfiguration($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $tab_data['type']                               = $type;
            $category_group['id_ebay_category'][]           = $profile_configuration['id_ebay_category'];
            if ( empty($category_group['id_ebay_category'][0]) ) :
                    $category_group                         = $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->user['site'], $this->id_shop);
                    if ( !empty($category_group) ) :
                            $category_group                 = $this->generate_additions_group('id_ebay_category', 'id_ebay_category', $category_group);
                    else :
                            $category_group['id_ebay_category'][0]  = '';
                    endif;  
            endif;
            $category_group                                 = "'".implode("', '", $category_group['id_ebay_category'])."'";
            $univer_selected                                = $this->objectbiz->getAttributeUnivers($this->user['site'], $this->id_shop);
            $data['have_univer']                            = !empty($univer_selected) ? 1 : 0;

            if( !$data['have_univer'] ):
                    $error                                  = $this->lang->line('ebay_attribute_univers_not_selected');
            else :  $univer_selected                        = $this->generate_key('id_group', $univer_selected);
            endif;

            $attribute_group                                = $this->ebay_model->get_variation_group($category_group, $this->user['site']);
            $attribute_group_value                          = $this->ebay_model->get_variation_value($category_group, $this->user['site']);
            $attribute_main                                 = $this->objectbiz->getAllAttributeNameGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            $attribute_value_main                           = $this->objectbiz->getAllAttributeValueGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            $profile_data                                   = $this->objectbiz->getEbayProfileVariation($this->user_name, $this->user['site'], $this->id_shop, $profile_id, $attribute_group);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( (!empty($profile_data) && $profile_data[0]['is_enabled'] == '1') || (!empty($tab_data['type']) && $tab_data['type'] == 'product') ) : $tab_data['tab'] = 2; else : $tab_data['tab'] = 1; endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//                echo "<pre>", print_r($attribute_main, true), "</pre>";exit();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_group) ) :
                    $attribute_main                        = $this->generate_key('id_attribute_group', $attribute_main);
                    foreach ( $attribute_main as $key => $att_univers ) :
                            if ( !empty($univer_selected) && !array_key_exists($key, $univer_selected) ) :
                                    unset($attribute_main[$key]);
                            endif;
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    $profile_group                          = $this->generate_key('name_attribute', $profile_data);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_value_main) ) :
                    $attribute_value_main                   = $this->generate_group('id_attribute_group', array('name', 'id_attribute', 'attribute_value', 'mode'), $attribute_value_main);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_group_value) ) :
                    $attribute_group_value                  = $this->generate_group('id_group', 'value', $attribute_group_value);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    foreach ( $profile_data as $mapping ) :
                            if ( !empty($attribute_group[$mapping['id_group']]) ) :
                                    $attribute_group[$mapping['id_group']]['selected'][$mapping['value_attribute']]    = $mapping['mode'];
                            endif;
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-variations-ebay"              => array(
                            'required'                      => $this->lang->line('validation_select_required'),
                            'duplicate'                     => $this->lang->line('validation_variation_type'),
                            'include'                       => $this->lang->line('validation_variation_include'),
                    ),
                    "validate-attribute-ebay"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                    ),
            );

                            $error_code = '';
                            if(empty($category_group) || $category_group == "''"){
                                    $error_code = 'mapping_not_found';
                            }
                            else if(empty($attribute_group)){
                                    $error_code = 'no_attributes';
                            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array('id_profile' => $profile_id, 'form-template-checkbox' => 1, 'choose-variation-value' => $this->lang->line('choose_variation_value'), 'choose-variation-type' => $this->lang->line('choose_variation_type'), 'are-you-sure-insert' => $this->lang->line('are_you_sure_insert')),
                    'error_code'                            => $error_code,
                    'attribute_group'                       => !empty($attribute_group) ? $attribute_group : '',
                    'attribute_main'                        => isset($attribute_main) ? $attribute_main : '',
                    'attribute_value_main'                  => isset($attribute_value_main) ? $attribute_value_main : '',
                    'attribute_group_value'                 => isset($attribute_group_value) ? $attribute_group_value : '',
                    'tab_data'                              => isset($tab_data) ? $tab_data : '',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'profile_group'                         => !empty($profile_group) ? $profile_group : '',
                    'error'                                 => !empty($error) ? $error : null,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
    }

    function prepare_values($profile_id = 0) {
            $tab_data                                       = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $profile_configuration                          = $this->objectbiz->getEbayProfileConfiguration($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $tab_data['type']                               = $type;
            $category_group['id_ebay_category'][]           = $profile_configuration['id_ebay_category'];
            if ( empty($category_group['id_ebay_category'][0]) ) :
                    $category_group                         = $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->user['site'], $this->id_shop);
                    if ( !empty($category_group) ) :
                            $category_group                 = $this->generate_additions_group('id_ebay_category', 'id_ebay_category', $category_group);
                    else :
                            $category_group['id_ebay_category'][0]  = '';
                    endif;  
            endif;
            $category_group                                 = "'".implode("', '", $category_group['id_ebay_category'])."'";
            $univer_selected                                = $this->objectbiz->getAttributeUnivers($this->user['site'], $this->id_shop);
            $data['have_univer']                            = !empty($univer_selected) ? 1 : 0;

            if( !$data['have_univer'] ):
                    $error                                  = $this->lang->line('ebay_attribute_univers_not_selected');
            else :  $univer_selected                        = $this->generate_key('id_group', $univer_selected);
            endif;

            $attribute_group                                = $this->ebay_model->get_variation_group($category_group, $this->user['site']);
            $attribute_group_value                          = $this->ebay_model->get_variation_value($category_group, $this->user['site']);
            $attribute_main                                 = $this->objectbiz->getAllAttributeNameGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            $attribute_value_main                           = $this->objectbiz->getAllAttributeValueGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            $profile_data                                   = $this->objectbiz->getEbayProfileVariation($this->user_name, $this->user['site'], $this->id_shop, $profile_id, $attribute_group);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( (!empty($profile_data) && $profile_data[0]['is_enabled'] == '1') || (!empty($tab_data['type']) && $tab_data['type'] == 'product') ) : $tab_data['tab'] = 2; else : $tab_data['tab'] = 1; endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
//                echo "<pre>", print_r($attribute_main, true), "</pre>";exit();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_group) ) :
                    $attribute_main                        = $this->generate_key('id_attribute_group', $attribute_main);
                    foreach ( $attribute_main as $key => $att_univers ) :
                            if ( !empty($univer_selected) && !array_key_exists($key, $univer_selected) ) :
                                    unset($attribute_main[$key]);
                            endif;
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    $profile_group                          = $this->generate_key('name_attribute', $profile_data);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_value_main) ) :
                    $attribute_value_main                   = $this->generate_group('id_attribute_group', array('name', 'id_attribute', 'attribute_value', 'mode'), $attribute_value_main);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_group_value) ) :
                    $attribute_group_value                  = $this->generate_group('id_group', 'value', $attribute_group_value);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    foreach ( $profile_data as $mapping ) :
                            if ( !empty($attribute_group[$mapping['id_group']]) ) :
                                    $attribute_group[$mapping['id_group']]['selected'][$mapping['value_attribute']]    = $mapping['mode'];
                            endif;
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-variations-ebay"              => array(
                            'required'                      => $this->lang->line('validation_select_required'),
                            'duplicate'                     => $this->lang->line('validation_variation_type'),
                            'include'                       => $this->lang->line('validation_variation_include'),
                    ),
                    "validate-attribute-ebay"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                    ),
            );

                            $error_code = '';
                            if(empty($category_group) || $category_group == "''"){
                                    $error_code = 'mapping_not_found';
                            }
                            else if(empty($attribute_group)){
                                    $error_code = 'no_attributes';
                            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array('id_profile' => $profile_id, 'form-template-checkbox' => 1, 'choose-variation-value' => $this->lang->line('choose_variation_value'), 'choose-variation-type' => $this->lang->line('choose_variation_type'), 'are-you-sure-insert' => $this->lang->line('are_you_sure_insert')),
                    'error_code'                            => $error_code,
                    'attribute_group'                       => !empty($attribute_group) ? $attribute_group : '',
                    'attribute_main'                        => isset($attribute_main) ? $attribute_main : '',
                    'attribute_value_main'                  => isset($attribute_value_main) ? $attribute_value_main : '',
                    'attribute_group_value'                 => isset($attribute_group_value) ? $attribute_group_value : '',
                    'tab_data'                              => isset($tab_data) ? $tab_data : '',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'profile_group'                         => !empty($profile_group) ? $profile_group : '',
                    'error'                                 => !empty($error) ? $error : null,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
    }

    ////////////////////////////////////////////////////////////////////////
    function mapping_attributes() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $this->prepare_attributes(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_attributes',
                    'ajax_url'                              => 'attribute_ebay_save',
                    'root_page'                             => 'mapping/',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ////////////////////////////////////////////////////////////////////////

//VARIATION END
    function variation() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $profile_id                                     = 0;
            $this->id_profile                               = 0;
            $attribute_main                                 = $this->objectbiz->getAllAttributeNameGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);

            $attribute_main_json                            = $this->generate_key('id_attribute_group', $attribute_main);  
            $hidden_data                                    = array(
                    'id_profile'                            => $this->id_profile,
                    'update_variation_success'              => $this->lang->line('update_variation_success'),
            );
            $assign                                         = array(
                    'current_page'                          => 'variation',
                    'ajax_url'                              => 'variation_save',
                    'root_page'                             => 'mapping/',
                    'ng'                                    => 1,
                    'app_data'                              => json_encode($attribute_main_json),
                    'hidden_data'				=> $hidden_data
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function variation_save() {
            if ($this->input->post()) :
                    $data                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $id_profile                     = current($data);
                            $id_profile_value               = isset($id_profile['id_profile']) ? intval($id_profile['id_profile']) : 0;
                            $this->objectbiz->deleteColumn('ebay_mapping_attribute_selected', array('id_profile' => 0, 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_lang' => $this->id_lang));
                            $details_insert = '';
                            foreach ( $data as $feedbiz ) :
                                    unset($data_mapping);
                                    if(!empty($feedbiz) && isset($feedbiz['id_attribute_group']) && intval($feedbiz['id_attribute_group']) > 0) :
                                            $data_mapping                   = array(
                                                    "id_profile"            => 0,
                                                    "id_attribute_group"    => $feedbiz['id_attribute_group'],
                                                    "name_attribute"        => htmlspecialchars($feedbiz['name_attribute']),
                                                    "name_attribute_ebay"   => htmlspecialchars($feedbiz['name_attribute_ebay']),
                                                    "id_site"               => $this->user['site'],
                                                    "id_shop"               => $this->id_shop,
                                                    "id_lang"               => $this->id_lang,
                                                    'mode'                  => $feedbiz['mode'],
                                                    'is_enabled'            => 1,
                                            );
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_attribute_selected', $data_mapping);
                                        endif;
                            endforeach;
                    endif;
                    $result_insert                  = empty($details_insert) ? 'Empty' : $this->objectbiz->transaction($details_insert, true);
                    echo "Success";
            endif;
    }
            //VARIATION END

    function variation_values() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $profile_id                                     = 0;
            $this->id_profile                               = 0;
            $attribute_main                                 = array();
            $attribute_main                                 = $this->objectbiz->getAllAttributeNameGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            if( empty($attribute_main) ){
                    redirect(base_url().'ebay/actions_products/'.$this->packages, 'location');
            }

            $attribute_value_main                           = $this->objectbiz->getAllAttributeValueGroup($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $profile_id);
            $attribute_value_main                           = $this->generate_key('id_attribute_group', $attribute_value_main);
            if ( !empty($attribute_main) ) :
                    foreach ( $attribute_main as $main_key => $value_main ) :
                            if ( !empty($attribute_value_main) && array_key_exists($value_main['id_attribute_group'], $attribute_value_main) ) :
                                    $attribute_main[$main_key]['attribute'] = $attribute_value_main[$value_main['id_attribute_group']];
                            endif;
                    endforeach;
            endif;
            $attribute_main_json                            = $this->generate_key('id_attribute_group', $attribute_main);   
            $attribute_main                                 = $this->generate_key('attribute_name', $attribute_main);                
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_profile'                            => $this->id_profile,
                    'add_data'                              => $this->lang->line('add_data'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'variation_values',
                    'ajax_url'                              => 'variation_value_save',
                    'root_page'                             => 'mapping/',
                    'ng'                                    => 1,
                    'app_data'                              => json_encode($attribute_main_json),
                    'hidden_data'                           => $hidden_data,
                    'product_type'                          => !empty($attribute_main) ? $attribute_main : array(),
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }


    function variation_value_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert                                 = '';
                            if ( !empty($data) && count($data) ) :
                                    $id_profile                             = current($data);
                                            $id_profile_value 						= isset($id_profile['id_profile']) ? intval($id_profile['id_profile']) : 0;
                                    $this->objectbiz->deleteColumn('ebay_mapping_attribute_value', array('id_profile' => $id_profile_value, 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_lang' => $this->id_lang));
                                    foreach ( $data as $attributes_value ) :
                                            unset($data_mapping);
                                            if ( isset($attributes_value['id_profile']) && is_numeric($attributes_value['id_profile']) ) :
                                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                    $data_mapping                   = array(
                                                            "id_profile"            => $attributes_value['id_profile'],
                                                            "id_attribute_group"    => $attributes_value['id_attribute_group'],
                                                            "id_attribute"          => $attributes_value['id_attribute'],
                                                            "name_attribute"        => htmlspecialchars($attributes_value['name_attribute']),
                                                            "name_attribute_ebay"   => htmlspecialchars($attributes_value['name_attribute_ebay']),
                                                            "id_site"               => $this->user['site'],
                                                            "id_shop"               => $this->id_shop,
                                                            "id_lang"               => $this->id_lang,
                                                            'mode'                  => $attributes_value['mode'],
                                                            'is_enabled'            => isset($attributes_value['tab']) && $attributes_value['tab'] == 1 ? '1' : '0',
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_attribute_value', $data_mapping);
                                            endif;
                                    endforeach;
                            endif;

                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo "Success";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function attribute_ebay_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $data_attributes                                        = (isset($_POST['data_attribute']) ? $_POST['data_attribute'] : '');
                    $data_attributes_value                                  = (isset($_POST['data_attribute_value']) ? $_POST['data_attribute_value'] : '');
                    $data_fixed                                             = (isset($_POST['data_fixed']) ? $_POST['data_fixed'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert                                 = '';
                            if ( !empty($data_attributes) && count($data_attributes) ) :
                                    $id_profile                             = current($data_attributes);
                                    $this->objectbiz->deleteColumn('ebay_mapping_attribute_selected', array('id_profile' => $id_profile['id_profile'], 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_lang' => $this->id_lang));
                                    foreach ( $data_attributes as $attributes ) :
                                            unset($data_mapping);
                                            if ( !empty($attributes['step']) ) :
                                                    $data_set                       = array(
                                                            'completed'             => round((100 * $attributes['step']) / 9, 2),
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $attributes['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                    );
                                                    $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            endif;
                                            if ( isset($attributes['id_profile']) && is_numeric($attributes['id_profile']) ) :
                                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                    $data_mapping                   = array(
                                                            "id_profile"            => $attributes['id_profile'],
                                                            "id_attribute_group"    => $attributes['id_attribute_group'],
                                                            "name_attribute"        => htmlspecialchars($attributes['name_attribute']),
                                                            "name_attribute_ebay"   => htmlspecialchars($attributes['name_attribute_ebay']),
                                                            "id_site"               => $this->user['site'],
                                                            "id_shop"               => $this->id_shop,
                                                            "id_lang"               => $this->id_lang,
                                                            'mode'                  => $attributes['mode'],
                                                            'is_enabled'            => isset($attributes['tab']) && $attributes['tab'] == 1 ? '1' : '0',
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_attribute_selected', $data_mapping);
                                            endif;
                                    endforeach;
                            endif;
                            if ( !empty($data_attributes_value) && count($data_attributes_value) ) :
                                    $id_profile                             = current($data_attributes_value);
                                    $this->objectbiz->deleteColumn('ebay_mapping_attribute_value', array('id_profile' => $id_profile['id_profile'], 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_lang' => $this->id_lang));
                                    foreach ( $data_attributes_value as $attributes_value ) :
                                            unset($data_mapping);
                                            if ( !empty($attributes_value['step']) ) :
                                                    $data_set                       = array(
                                                            'completed'             => round((100 * $attributes_value['step']) / 9, 2),
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $attributes_value['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                    );
                                                    $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            endif;
                                            if ( isset($attributes_value['id_profile']) && is_numeric($attributes_value['id_profile']) ) :
                                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                    $data_mapping                   = array(
                                                            "id_profile"            => $attributes_value['id_profile'],
                                                            "id_attribute_group"    => $attributes_value['id_attribute_group'],
                                                            "id_attribute"          => $attributes_value['id_attribute'],
                                                            "name_attribute"        => htmlspecialchars($attributes_value['name_attribute']),
                                                            "name_attribute_ebay"   => htmlspecialchars($attributes_value['name_attribute_ebay']),
                                                            "id_site"               => $this->user['site'],
                                                            "id_shop"               => $this->id_shop,
                                                            "id_lang"               => $this->id_lang,
                                                            'mode'                  => $attributes_value['mode'],
                                                            'is_enabled'            => isset($attributes_value['tab']) && $attributes_value['tab'] == 1 ? '1' : '0',
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_attribute_value', $data_mapping);
                                            endif;
                                    endforeach;
                            endif;
                            if ( !empty($data_fixed) && count($data_fixed) ) :
                                    $id_profile                             = current($data_fixed);
                                    $this->objectbiz->deleteColumn('ebay_profiles_variation', array('id_profile' => $id_profile['id_profile']));
                                    foreach ( $data_fixed as $fixed ) :
                                            unset($data_detail, $data_mapping);
                                            $data_set = array();
                                            $where_set = array();
                                            if ( isset($fixed['id_profile']) && is_numeric($fixed['id_profile']) ) :
                                                    if ( !empty($fixed['step']) ) :
                                                            $data_set                       = array(
                                                                    'completed'             => round((100 * $fixed['step']) / 9, 2),
                                                            );
                                                            $where_set                      = array(
                                                                    'id_profile'            => $fixed['id_profile'],
                                                                    'id_site'               => $this->user['site'],
                                                                    'id_shop'               => $this->id_shop,
                                                            );
                                                    endif;
                                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                    $data_detail                    = array(
                                                            'id_profile'            => $fixed['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'id_group'              => isset($fixed['id_group']) ? $fixed['id_group'] : '',
                                                            'name_attribute'        => isset($fixed['name_attribute']) ? htmlspecialchars($fixed['name_attribute']) : '',
                                                            'value_attribute'       => isset($fixed['name_attribute_ebay']) ? htmlspecialchars($fixed['name_attribute_ebay']) : '',
                                                            'mode'                  => $fixed['mode'],
                                                            'is_enabled'            => isset($fixed['tab']) && $fixed['tab'] == 1 ? '0' : '1',
                                                    );
                                                    $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_variation', $data_detail);
                                            endif;
                                    endforeach;
                            endif;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo "Success";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function set_configuration($data = '', $result = array()) {        
            if ( empty($data) ) :
                    return FALSE;
            endif;

            foreach ( $data as $ebay_value ) :
                    if ( $ebay_value->name  == 'EBAY_DEVID' ) 
                        $result['devID']                    = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_USER' ) 
                        $result['userID']                   = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_APPID' ) 
                        $result['appID']                    = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_CERTID' )
                        $result['certID']                   = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_TOKEN' ) 
                        $result['token']                    = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_APPMODE' ) 
                        $result['appMode']                  = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_SITE_ID' )
                        $result['site']                     = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_CURRENCY' )
                        $result['currency']                 = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_DISPATCH_TIME' )
                        $result['dispatch_time']            = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_LI_DURATION' )
                        $result['listing_duration']         = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_POSTAL_CODE' )
                        $result['postal_code']              = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_RE_DAYS' )
                        $result['returns_within']           = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_RE_INFORMATION' )
                        $result['returns_information']      = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_RE_PAYS' )
                        $result['returns_pays']             = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_RE_POLICY' )
                        $result['returns_policy']           = $ebay_value->value;
                    else if ( $ebay_value->name == 'PAYER_EMAIL' )
                        $result['paypal_email']             = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_ADDRESS' )
                        $result['address']                  = $ebay_value->value;
                    else if ( $ebay_value->name == 'FEED_MODE' )
                        $result['feed_mode']                = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_CATEGORY' )
                        $result['category_json']            = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_PAYMENT' )
                        $result['payment_method']           = unserialize($ebay_value->value);
                    else if ( $ebay_value->name == 'API_TEMPLATE' )
                        $result['template']                 = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_NO_IMAGE' )
                        $result['no_image']                 = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_DISCOUNT' )
                        $result['discount']                 = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_SYNCHRONIZE' )
                        $result['sync']                     = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_JUST_ORDERS' )
                        $result['just_orders']              = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_WIZARD' )
                        $result['wizard']                   = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_WIZARD_FINISH' )
                        $result['wizard_finish']            = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_WIZARD_START' )
                        $result['wizard_start']             = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_WIZARD_SUCCESS' )
                        $result['wizard_success']           = $ebay_value->value;
                    else if ( $ebay_value->name == 'EBAY_PROFILE_RULES' )
                        $result['profile_rules']            = $ebay_value->value;
            endforeach;
            return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function statistics_products() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-statistics'                      => $this->lang->line('header_statistics'),
                    'type'                                  => $this->lang->line('type'),
                    'user'                                  => $this->lang->line('user'),
                    'message'                               => $this->lang->line('message'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'export-date'                           => $this->lang->line('export_date'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'statistics',
                    'ajax_url'                              => '',
                    'root_page'                             => 'statistics/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'statistics'                            => !empty($statistics_data) ? $statistics_data : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function statistic_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $batch_id                                       = $this->input->get('batch_id');
            $id_product                                     = $this->input->get('id_product');
            $product_sku                                     = $this->input->get('product_sku');
            $product_ean13                                     = $this->input->get('product_ean13');
            $date_add                                       = $this->input->get('date_add');
            $user                                           = $this->input->get('user');
            $name_product                                   = $this->input->get('name_product');
            $type                                           = $this->input->get('type');
            $response                                       = $this->input->get('response');
            $message                                        = $this->input->get('message');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_name                                      = empty($sort_name) ? $sort_name : 's.'.$sort_name;
            $sort_by                                        = $this->input->get('sort_by');
            $sort_by   										= empty($sort_by) ? $sort_by : $sort_by;
            $all                                            = $this->input->get('all');
            $ignore_case                                    = $this->input->get('ignore_case');
            $ignore_case                                    = !empty($ignore_case)&&$ignore_case=='true'?true:false;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getStatisticByParam(array(
                    's.ebay_user'                             => $this->user['userID'], 
                    's.id_site'                               => $this->user['site'], 
                    's.id_shop'                               => $this->id_shop,
                    's.batch_id'                              => !empty($all) ? $all : $batch_id,
                    's.id_product'                            => !empty($all) ? $all : $id_product,
                    'p.sku'                            		  => !empty($all) ? $all : $product_sku,
                    'p.ean13'                                 => !empty($all) ? $all : $product_ean13,
                    's.date_add'                              => !empty($all) ? $all : $date_add,
                    's.name_product'                          => !empty($all) ? $all : htmlspecialchars($name_product),
                    's.type'                                  => !empty($all) ? $all : $type,
                    's.response'                              => !empty($all) ? $all : $response,
                    's.message'                               => !empty($all) ? $all : htmlspecialchars($message),
                    'p.reference'                             => !empty($all) ? $all : '',
                    'p.sku'                             	  => !empty($all) ? $all : '',
                    'p.ean13'                             	  => !empty($all) ? $all : '',
                    'p.upc'                             	  => !empty($all) ? $all : '',
            ), $limit, $offset, array('s.batch_id as batch_id', 's.id_product as id_product', 'p.sku as product_sku', 'p.ean13 as product_ean13', 's.date_add as date_add', 's.ebay_user as ebay_user', 's.name_product as name_product', 's.response as response', 's.type as type', 's.message as message'), 
                    $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND',$ignore_case);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['count']                                  = $this->objectbiz->getStatisticByParam(array(
                    's.ebay_user'                             => $this->user['userID'], 
                    's.id_site'                               => $this->user['site'], 
                    's.id_shop'                               => $this->id_shop,
                    's.batch_id'                              => !empty($all) ? $all : $batch_id,
                    's.id_product'                            => !empty($all) ? $all : $id_product,
                    'p.sku'                            		  => !empty($all) ? $all : $product_sku,
                    'p.ean13'                            	  => !empty($all) ? $all : $product_ean13,
                    's.date_add'                              => !empty($all) ? $all : $date_add,
                    's.name_product'                          => !empty($all) ? $all : htmlspecialchars($name_product),
                    's.type'                                  => !empty($all) ? $all : $type,
                    's.response'                              => !empty($all) ? $all : $response,
                    's.message'                               => !empty($all) ? $all : htmlspecialchars($message),
                    'p.reference'                             => !empty($all) ? $all : '',
                    'p.sku'                             	  => !empty($all) ? $all : '',
                    'p.ean13'                             	  => !empty($all) ? $all : '',
                    'p.upc'                             	  => !empty($all) ? $all : '',
            ), 0, 0, 'count(*)', $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND',$ignore_case);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = isset($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics']);
            echo json_encode($value);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function log() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-log'                             => $this->lang->line('header_log'),
                    'type'                                  => $this->lang->line('type'),
                    'source'                                => $this->lang->line('source'),
                    'message'                               => $this->lang->line('message'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'export-date'                           => $this->lang->line('export_date'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'log',
                    'ajax_url'                              => '',
                    'root_page'                             => 'statistics/log/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'statistics'                            => !empty($log_data) ? $log_data : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function log_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $batch_id                                       = $this->input->get('batch_id');
            $source                                         = $this->input->get('source');
            $type                                           = $this->input->get('type');
            $total                                          = $this->input->get('total');
            $success                                        = $this->input->get('success');
            $warning                                        = $this->input->get('warning');
            $error                                          = $this->input->get('error');
            $export_date                                    = $this->input->get('date_add');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $all                                            = $this->input->get('all');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getLogByParam(array(
                    'id_site'                               => $this->user['site'], 
                    'id_shop'                               => $this->id_shop,
                    'batch_id'                              => !empty($all) ? $all : $batch_id,
                    'type'                                  => !empty($all) ? $all : $type,
                    'total'                                 => !empty($all) ? $all : $total,
                    'success'                               => !empty($all) ? $all : $success,
                    'warning'                               => !empty($all) ? $all : $warning,
                    'error'                                 => !empty($all) ? $all : $error,
                    'date_add'                              => !empty($export_date) ? date('Y-m-d', strtotime($export_date)) : '',
            ), $limit, $offset, array('batch_id', 'type', 'total', 'success', 'warning', 'error', 'date_add'), $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($data['statistics']) ) :
                    foreach ( $data['statistics'] as $key => $statistic_data ) :
                            $data['statistics'][$key]['source'] = $this->shop_default;
                            $data['statistics'][$key]['download'] = "download";
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['count']                                  = $this->objectbiz->getLogByParam(array(
                    'id_site'                               => $this->user['site'], 
                    'id_shop'                               => $this->id_shop,
                    'batch_id'                              => !empty($all) ? $all : $batch_id,
                    'type'                                  => !empty($all) ? $all : $type,
                    'total'                                 => !empty($all) ? $all : $total,
                    'success'                               => !empty($all) ? $all : $success,
                    'warning'                               => !empty($all) ? $all : $warning,
                    'error'                                 => !empty($all) ? $all : $error,
                    'date_add'                              => !empty($export_date) ? date('Y-m-d', strtotime($export_date)) : '',
            ), 0, 0, 'count(*)', $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = isset($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics']);
            echo json_encode($value);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function log_download($id_packages = null, $id_site = null, $batch_id = null) {
            if ( (!isset($id_packages) && empty($id_packages)) || (!isset($id_site) && empty($id_site)) || (!isset($batch_id) && empty($batch_id)) ) :
                    return FALSE;
            endif;
            $this->load->helper('download');
            $data                                       = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            if ( !empty($data) && count($data) ) :
                    $this->data_statistics              = $this->objectbiz->getStatisticByBatchID($this->user_name, $this->user['site'], $this->id_shop, $batch_id); 
                    $list = array(); 
                    ob_start() ;
                    ob_get_clean() ;
                    $filename = 'ebay_' . date('Ymd_His') . '.csv' ;
                    header("Cache-Control: no-cache, must-revalidate"); 
                    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=$filename");
                    $h = fopen('php://output', 'w') ;

                    if ( !empty($this->data_statistics) ) :
                            foreach($this->data_statistics as $statistics) :
                                    $print      = array($statistics['batch_id'].','.$statistics['ebay_user'].','.$statistics['name_product'].','.$statistics['response'].','.$statistics['type'].','.$statistics['message'].','.$statistics['date_add']);
                                    fputs($h, implode($print, ',')."\n");
                            endforeach;
                    else :
                            fputs($h, "Log details not found.");
                    endif;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function orders() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-orders'                          => $this->lang->line('header_orders'),
                    'country-name'                          => $this->lang->line('country_name'),
                    'id-buyer'                              => $this->lang->line('id_buyer'),
                    'order-date'                            => $this->lang->line('order_date'),
                    'payment-method'                        => $this->lang->line('payment_method'),
                    'order_status'                          => 'Completed',
                    'hidden-search'                         => $this->lang->line('search'),
                    'total-paid'                            => $this->lang->line('total_paid'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'orders',
                    'ajax_url'                              => '',
                    'root_page'                             => 'order/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => array('id_user' => $this->user_id),
                    'statistics'                            => !empty($orders) ? $orders : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function carts() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'carts',
                    'ajax_url'                              => '',
                    'root_page'                             => 'order/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => array('id_user' => $this->user_id),
                    'statistics'                            => !empty($orders) ? $orders : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function orders_shipped() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-orders'                          => $this->lang->line('header_orders'),
                    'country-name'                          => $this->lang->line('country_name'),
                    'id-buyer'                              => $this->lang->line('id_buyer'),
                    'order-date'                            => $this->lang->line('order_date'),
                    'payment-method'                        => $this->lang->line('payment_method'),
                    'order_status'                          => 'Shipped',
                    'hidden-search'                         => $this->lang->line('search'),
                    'total-paid'                            => $this->lang->line('total_paid'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'orders_shipped',
                    'ajax_url'                              => '',
                    'root_page'                             => 'order/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => array('id_user' => $this->user_id),
                    'statistics'                            => !empty($orders) ? $orders : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function orders_cancelled() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-orders'                          => $this->lang->line('header_orders'),
                    'country-name'                          => $this->lang->line('country_name'),
                    'id-buyer'                              => $this->lang->line('id_buyer'),
                    'order-date'                            => $this->lang->line('order_date'),
                    'payment-method'                        => $this->lang->line('payment_method'),
                    'order_status'                          => 'Cancelled',
                    'hidden-search'                         => $this->lang->line('search'),
                    'total-paid'                            => $this->lang->line('total_paid'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'orders_cancelled',
                    'ajax_url'                              => '',
                    'root_page'                             => 'order/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => array('id_user' => $this->user_id),
                    'statistics'                            => !empty($orders) ? $orders : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function orders_awaiting_payment() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-orders'                          => $this->lang->line('header_orders'),
                    'country-name'                          => $this->lang->line('country_name'),
                    'id-buyer'                              => $this->lang->line('id_buyer'),
                    'order-date'                            => $this->lang->line('order_date'),
                    'payment-method'                        => $this->lang->line('payment_method'),
                    'order_status'                          => 'Active',
                    'hidden-search'                         => $this->lang->line('search'),
                    'total-paid'                            => $this->lang->line('total_paid'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'orders_awaiting_payment',
                    'ajax_url'                              => '',
                    'root_page'                             => 'order/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => array('id_user' => $this->user_id),
                    'statistics'                            => !empty($orders) ? $orders : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function orders_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $id_order                                       = $this->input->get('id_orders');
            $id_order_ref                                   = $this->input->get('id_order_ref');
            $id_buyer                                       = $this->input->get('id_buyer');
            $country_name                                   = $this->input->get('country_name');
            $payment_by                                     = $this->input->get('payment_method');
            $paid_amount                                    = $this->input->get('total_paid');
            $order_date                                     = $this->input->get('order_date');
            $id_invoice                                     = $this->input->get('invoice_no');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $sort_status                                    = $this->input->get('sort_status');
            $s_status                                       = 'm.status as status';
            if ( !empty($sort_status) ) :
                    if ( $sort_status == 'Shipped') :
                            $s_status                       = 's.tracking_number as status';
                    elseif ( $sort_status == 'Active') :
                            $s_status                       = 'm.marketplace_order_number as status';
                    elseif ( $sort_status == 'Cancelled') :
                            $s_status                       = 'm.order_type as status';
                    endif;
            endif;
//                $all                                            = $this->input->get('all');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['order']                                  = $this->objectbiz->getOrderByParam($this->user_name, $this->user['site'], $this->id_shop, array(
                    'm.id_orders'                           => $id_order,
                    'm.id_marketplace_order_ref'            => $id_order_ref,
                    'n.id_buyer_ref'                        => $id_buyer,
                    'm.order_status'                        => $sort_status,
                    'm.payment_method'                      => $payment_by,
                    'n.country_name'                        => $country_name,
                    'm.total_paid'                          => $paid_amount,
                    'm.order_date'                          => !empty($order_date) ? date('Y-m-d', strtotime($order_date)) : '',
                    'o.invoice_no'                          => $id_invoice,
            ), $limit, $offset, array('m.id_orders as id_orders'
                , 'm.id_marketplace_order_ref as id_marketplace_order_ref'
                , 'n.id_buyer_ref as id_buyer'
                , "'$sort_status' as order_status"
                , "mp.mp_channel as 'mp_channel'"
                , 'n.country_name as country_name'
                , 'm.payment_method as payment_method'
                , 'm.total_paid as total_paid'
                , 'm.order_date as order_date'
                , 'o.invoice_no as invoice_no'
                , 'm.comment as comment'
                , $s_status), $sort_name, $sort_by);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['count']                                  = $this->objectbiz->getOrderByParam($this->user_name, $this->user['site'], $this->id_shop, array(
                    'm.id_orders'                           => $id_order,
                    'm.id_marketplace_order_ref'            => $id_order_ref,
                    'n.id_buyer_ref'                        => $id_buyer,
                    'm.order_status'                        => $sort_status,
                    'm.payment_method'                      => $payment_by,
                    'n.country_name'                        => $country_name,
                    'm.total_paid'                          => $paid_amount,
                    'm.order_date'                          => !empty($order_date) ? date('Y-m-d', strtotime($order_date)) : '',
                    'o.invoice_no'                          => $id_invoice,
            ), 0, 0, 'count(*)', $sort_name, $sort_by);

            $count = !empty($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['order']);
            echo json_encode($value);
    }     

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function carts_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $id_order                                       = $this->input->get('id_orders');
            $id_order_ref                                   = $this->input->get('id_order_ref');
            $id_buyer                                       = $this->input->get('id_buyer');
            $country_name                                   = $this->input->get('country_name');
            $payment_by                                     = $this->input->get('payment_method');
            $paid_amount                                    = $this->input->get('total_paid');
            $order_date                                     = $this->input->get('order_date');
            $id_invoice                                     = $this->input->get('invoice_no');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $sort_status                                    = $this->input->get('sort_status');
            $s_status                                       = 'm.status as status';


            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['order']                                  = $this->objectbiz->getEbayCarts($this->user['site'], $this->id_shop);
            $count = !empty($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['order']);
            echo json_encode($value);
    }     

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function get_orders_debug() {        	
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
                            $this->load->library("Ebay/ebayorder", $param);
                            $data_from                          = date('Y-m-d', strtotime('-180 days'));
                            $data_to                            = date('Y-m-d', strtotime('+2 days'));
                            $order_status                       = isset($data[0]['order_status']) ? $data[0]['order_status'] : 'Completed';
                            $parser                             = $this->ebayorder->get_orders_debug($data_from, $data_to, 60, $order_status);

                            echo !empty($parser) ? $parser->Ack->__toString() : 'Failured';
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    function get_orders_debug_xml() {
            $orders = array();
            $debug_file_path = USERDATA_PATH.$this->user_name.'/ebay/debug/orders';
            $files = scandir($debug_file_path);
            asort($files);
            foreach($files as $file){
                    if(strpos($file, 'Page') !== FALSE && strpos($file, '.xml') !== FALSE){
                            $ordersDom = simplexml_load_file($debug_file_path.'/'.$file);
                            //echo '<pre>'.print_r($ordersDom, true).'</pre>';
                                    foreach($ordersDom->OrderArray->Order as $orderDom){
                                            $order = array();
                                            $order['OrderID'] = isset($orderDom->OrderID) ? strval($orderDom->OrderID) : '';
                                            $order['OrderStatus'] = isset($orderDom->OrderStatus) ? strval($orderDom->OrderStatus) : '';
                                            $order['AmountPaid'] = isset($orderDom->AmountPaid) ? strval($orderDom->AmountPaid) : '';
                                            $order['CreatedTime'] = isset($orderDom->CreatedTime) ? strval($orderDom->CreatedTime) : '';

                                            foreach($orderDom->TransactionArray->Transaction as $trasaction){
                                                    $item = array();
                                                    $item['ItemID'] = isset($trasaction->Item->ItemID) ? strval($trasaction->Item->ItemID) : '';
                                                    $item['SKU'] = isset($trasaction->Item->SKU) ? strval($trasaction->Item->SKU) : '';
                                                    $item['Product'] = isset($trasaction->Item->Title) ? strval($trasaction->Item->Title) : '';
                                                    $item['Site'] = isset($trasaction->Item->Site) ? strval($trasaction->Item->Site) : '';
                                                    $order['Items'][] = $item;
                                            }

                                            $orders[$order['CreatedTime']] = $order;
                                    }
                    }
            }
            krsort($orders);
            echo json_encode($orders);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function get_orders() {        	
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
                            $this->load->library("Ebay/ebayorder", $param);
                            $data_from                          = date('Y-m-d', strtotime('-90 days'));
                            $data_to                            = date('Y-m-d', strtotime('+2 days'));
                            $order_status                       = isset($data['order_status']) ? $data['order_status'] : 'Completed';
                            $parser                             = $this->ebayorder->get_orders($data_from, $data_to, 60, $order_status);

                            //Stock movement
                            $imported_order = $this->ebayorder->get_imported_orders();  
                                                // LOG
                                                            if ($imported_order) {
                                                                    require_once (DIR_SERVER . '/application/libraries/FeedBiz_Orders.php');
                                                                    require_once DIR_SERVER . '/application/libraries/UserInfo/configuration.php';
                                                                    $config_data = $this->my_feed_model->get_general($this->user_id);

                                                                    $feed = unserialize ( base64_decode ( $config_data ['FEED_SHOP_INFO']['value'] ) );
                                                                    $batch_id = substr ( str_replace ( '.', '', microtime ( true ) ), 0, 13 );
                                                                    $userConfig = new UserConfiguration();
                                                                    $feedBiz_Orders = new FeedBiz_Orders ( array (
                                                                                    $this->user_name 
                                                                    ) );

// 									echo $feed ['url'] ['stockmovement'].'::'.$userConfig->getUserCode($this->user_id).'::'.$this->session->userdata('id_shop').'::'.$this->user['site'];
// 									print_r($imported_order);
                                                                    $feedBiz_Orders->stockMovement ( $this->user_name, $feed ['url'] ['stockmovement'], $userConfig->getUserCode($this->user_id), $this->session->userdata('id_shop'), $batch_id, 3, $this->user['site'], $imported_order, 'eBay' );
                                                            }
                            echo !empty($parser) ? $parser->Ack->__toString() : 'Failured';
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function get_modifier_orders() {        	
//                if ($this->input->post()) :
//                        $data                                       = $_POST['data'];
//                        $this->authentication->validateAjaxToken();
//                        $this->validation_ajax($data);
//                        if ( !empty($data) && count($data) ) :
                            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
                            $this->load->library("Ebay/ebayorder", $param);
                            $parser                             = $this->ebayorder->get_modifier_orders();
                            echo !empty($parser) ? $parser->Ack->__toString() : 'Failured';
//                        else :
//                                echo "Fail";
//                        endif;
//                else :
//                        echo "Fail";
//                endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function templates() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';
            if ( file_exists($dir_name) && ($handle = opendir($dir_name)) ) :
                    while (( $file = readdir($handle)) !== false ) :
                            if ( !in_array($file, array('.', '..')) ) :
                                    $data['file'][]         = $file;
                            endif;
                    endwhile;
            endif;
            $hidden_data                                    = array(
                    'update-templates-add-success'          => $this->lang->line('update_templates_add_success'),
                    'update-templates-add-fail'             => $this->lang->line('update_templates_add_fail'),
                    'update-templates-delete-success'       => $this->lang->line('update_templates_delete_success'),
                    'update-templates-delete-fail'          => $this->lang->line('update_templates_delete_fail'),
                    'update-templates-preview-success'      => $this->lang->line('update_templates_preview_success'),
                    'update-templates-preview-fail'         => $this->lang->line('update_templates_preview_fail'),
                    'are-you-sure'                          => $this->lang->line('are_you_sure'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'templates',
                    'ajax_url'                              => '',
                    'root_page'                             => 'mapping/',
                    'dir_name'                              => $dir_name,
                    'hidden_data'                           => $hidden_data,
                    'jquery_form'                           => 'open',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function templates_download($id_packages = null, $id_site = null, $file_name = null) {
            if ( (!isset($id_packages) && empty($id_packages)) || (!isset($id_site) && empty($id_site)) || (!isset($file_name) && empty($file_name)) ) :
                    return FALSE;
            endif;
            $this->load->helper('download');
            $data                                       = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            if ( !empty($data) && count($data) ) :
                    $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';
                    if ( $file_name == 'default.tpl') {
                            ob_start() ;
                            ob_get_clean() ;
                            header('Content-Description: File Transfer');
                            header("Cache-Control: no-cache, must-revalidate"); 
                            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
                            header("Content-Disposition: attachment; filename=$file_name");
                            $h = fopen('php://output', 'w') ;
                            readfile($this->dir_server. '/assets/apps/ebay/template/Default/default.tpl');
                    }
                    else if ( file_exists($dir_name) ) :
                            ob_start() ;
                            ob_get_clean() ;
                            header('Content-Description: File Transfer');
                            header("Cache-Control: no-cache, must-revalidate"); 
                            header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 
                            header("Content-Disposition: attachment; filename=$file_name");
                            $h = fopen('php://output', 'w') ;
                            readfile($dir_name . $file_name);
                    else :
                            $assign                                         = array(
                                    'error'                                 => $this->lang->line('ebay_not_found'),
                            );
                            $this->assign($assign);
                            $this->templates();
                    endif;
            endif;
    }

    function templates_deleted() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $templates) :
                                    $file                                           = $templates['template_name'];
                                    $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';
                                    if ( file_exists($dir_name) ) :
                                            unlink($dir_name.$file);
                                            $this->objectbiz->deleteColumn('ebay_mapping_templates', array('template_selected' => $file, 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                            echo 'Success';
                                    else :
                                            echo 'Failure';
                                    endif;
                            endforeach;
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Success";
            endif;
    }

    function templates_preview() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $templates) :
                                    $file                                           = $templates['template_name'];
                                    $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';
                                    if ( file_exists($dir_name) ) :
                                            $handle                                 = fopen($dir_name.$file, "r");
                                            $contents                               = fread($handle, filesize($dir_name.$file));
                                            $contents                               = preg_replace_callback('/<script\b(.*?)>(.*?)*<\/script>/i', function() {return null;}, $contents);
                                            $contents                               = preg_replace_callback('/[^\t][\n]/i', function() {return null;}, $contents);
                                            $contents                               = preg_replace_callback('/<script\b(.*?)*>(.*?)<\/script>/i', function() {return null;}, $contents);
                                            $contents                               = preg_replace_callback('/<meta\b(.*?)*>/i', function() {return null;}, $contents);
                                            $contents                               = preg_replace_callback('/<iframe\b(.*?)\/iframe>/i', function() {return null;}, $contents);
                                            fclose($handle);
                                            $json_array                             =  array(
                                                    'Response'                      => 'Success',
                                                    'Result'                        => base64_encode($contents),
                                            );
                                    else :
                                            $json_array                             =  array(
                                                    'Response'                      => 'Failure',
                                            );
                                    endif;
                                    echo json_encode($json_array);
                            endforeach;
                    else :
                            $json_array                             =  array(
                                    'Response'                      => 'Failure',
                            );
                            echo json_encode($json_array);
                    endif;
            else :
                    $json_array                             =  array(
                            'Response'                      => 'Failure',
                    );
                    echo json_encode($json_array);
            endif;
    } 

    function templates_upload() {
            $config['upload_path']                          = USERDATA_PATH.$this->user_name.'/template/';
            $config["allowed_types"]                        = "*"; 
            $config["max_size"]                             = "1000"; 
            $config["max_width"]                            = "2048"; 
            $config["max_height"]                           = "1538"; 
            $config["encrypt_name"]                         = false; 
            $config["remove_space"]                         = true; 
            $config["overwrite"]                            = false; 

            if ( !empty($_FILES['FileInput']) ) :
                    $filename                               = $_FILES['FileInput']['name'];
                    $ext                                    = pathinfo($filename, PATHINFO_EXTENSION);
                    $tmpname                                = $_FILES['FileInput']['tmp_name'];
                    $handle                                 = fopen($tmpname, "r");
                    $contents                               = fread($handle, filesize($tmpname));
                    $contents                               = preg_replace_callback('/<script\b(.*?)>(.*?)*<\/script>/i', function() {return null;}, $contents);
                    $contents                               = preg_replace_callback('/[^\t][\n]/i', function() {return null;}, $contents);
                    $contents                               = preg_replace_callback('/<script\b(.*?)*>(.*?)<\/script>/i', function() {return null;}, $contents);
                    $contents                               = preg_replace_callback('/<meta\b(.*?)*>/i', function() {return null;}, $contents);
                    $contents                               = preg_replace_callback('/<iframe\b(.*?)\/iframe>/i', function() {return null;}, $contents);
                    fclose($handle);
                    $edit_file                              = $this->input->post('form-file');  
                    if ( isset($edit_file) && $edit_file == 'New' ) :
//                                $i = 0;
//                                $check_Iname = 0;
//                                if ( file_exists($config['upload_path']) && ($handle = opendir($config['upload_path'])) ) :
//                                        while (( $file = readdir($handle)) !== false ) :
//                                                if ( !in_array($file, array('.', '..')) && !is_dir($config['upload_path'].$file) ) :
//                                                        $check_name                     = explode(".", $file);
//                                                        $check_Iname                    = ( ($new_name = str_replace($this->user_name."_", "", $check_name[0])) > $check_Iname) ? $new_name : $check_Iname;
//                                                        $i++;
//                                                endif;
//                                        endwhile;
//                                endif;
                            $config["overwrite"]                            = false; 
                            $config["file_name"]                            = $_FILES['FileInput']['name']; 
                    else :
                            $config["overwrite"]                            = true; 
                            $config["file_name"]                            = $edit_file; 
                    endif;

                    if ( ! file_exists($config['upload_path']) ) { 
                            $old = umask(0); 
                            mkdir($config['upload_path'], 0777, true); 
                            umask($old); 
                    } 
            endif;

            $this->load->library('upload', $config);
            $this->upload->initialize($config); 

            if ( ! $this->upload->do_upload('FileInput')) {
                    $error = array('error' => $this->upload->display_errors());
                    $json_array                             =  array(
                            'Response'                      => 'Failure',
                            'Error'                         => $error,
                    );
            }
            else {
                    $data = array('upload_data' => $this->upload->data());
                    $json_array                             =  array(
                            'Response'                      => 'Success',
                            'Data'                          => $data,
                    );
                    $this->load->helper('file');
                    write_file(USERDATA_PATH.$this->user_name.'/template/'.$config["file_name"], $contents, 'w+');
            }
            echo json_encode($json_array);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function test_ebay() {
            $data                                           = $this->init_packages();
            $param = array($this->user_name, $this->user_id, $this->user['site'], null, null);
//                $this->ebaylib->GeteBayOfficialTime(0, $this->user['token']);
//                echo "<pre>", print_r($this->ebaylib->core->_parser, true), "</pre>";
//                $this->load->library('Ebay/ebayexport', $param);
//                $this->ebayexport->export_compress(false);
           $this->load->library('Ebay/ebayofferexport', $param);
           $this->ebayofferexport->export_compress();
//                $this->ebayexport->export_products();

//                $this->ebayexport->export_download();
//                $this->ebayofferexport->export_bulk();
//                 $this->ebayexport->extrack_download('AddFixedPriceItem');
//                 $this->load->library('Ebay/ebaysynchronization', $param);
//                 $this->ebaysynchronization->get_inventory();
//                $this->ebaysynchronization->generate_ebay_product_item_id();
//                $this->load->library('Ebay/ebayspecifics', $param);
//                $this->ebayspecifics->get_specific();
//                $this->ebayspecifics->get_specific_file();
//                $this->ebayspecifics->read_file();
//                echo "<pre>", print_r($this->ebaylib->core->_parser, true), "</pre>";exit();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function test_action($packages = null, $method = null, $func = null, $args_1 = false, $args_2 = false) {
            $data                                           = $this->init_packages();
            $param                                          = array($this->user_name, $this->user_id, $this->user['site'], null, null);
            if ( !isset($packages) && empty($packages) || !isset($method) && empty($method) || !isset($func) && empty($func) ) :
                    exit('Some arguments not found.');
            endif;
            switch ( $method ) :
                    case "ebayexport" : $this->load->library('Ebay/ebayexport', $param);
                                        $this->ebayexport->$func($args_1, $args_2);
                    break;
                    case "ebayexporttest" : $this->load->library('Ebay/ebayexporttest', $param);
                                        $this->ebayexporttest->$func($args_1, $args_2);
                    break;
                    case "ebaysynchronization" : $this->load->library('Ebay/ebaysynchronization', $param);
                                        $this->ebaysynchronization->$func();
                    break;
                    case "ebayofferexport" : $this->load->library('Ebay/ebayofferexport', $param);
                                        $this->ebayofferexport->$func();
                    break;
                    case "ebaywizardexport" : $this->load->library('Ebay/ebayexport', array($this->user_name, $this->user_id, $this->user['site'], null, true));
                                        $this->ebayexport->$func();
                    break;
                    case "ebayshipment" : $this->load->library('Ebay/ebayshipment', $param);
                                        $this->ebayshipment->$func();
                    break;
            endswitch;
//                $this->load->library('Ebay/ebayexport', $param);
//                $this->load->library('Ebay/ebayofferexport', $param);
//                $this->ebayexport->export_products();
//                $this->ebayexport->export_download();
//                $this->ebayofferexport->export_bulk();
//                 $this->ebayexport->extrack_download('AddFixedPriceItem');
//                $this->load->library('Ebay/ebaysynchronization', $param);
//                $this->ebaysynchronization->get_inventory();
//                $this->ebaysynchronization->generate_ebay_product_item_id();
//                $this->load->library('Ebay/ebayspecifics', $param);
//                $this->ebayspecifics->get_specific();
//                $this->ebayspecifics->get_specific_file();
//                $this->ebayspecifics->read_file();
//                echo "<pre>", print_r($this->ebaylib->core->_parser, true), "</pre>";exit();
    }

    function get_product_file(){
            $return = array();
            //Create Compress File.
            $data = $this->init_packages();
            $param = array($this->user_name, $this->user_id, $this->user['site'], null, false);
            $this->load->library('Ebay/ebayexport', $param);
            $this->ebayexport->export_compress();

            $userEbayProductDIR = USERDATA_PATH.$this->user_name."/ebay/product";
            $userEbayProductURL = base_url()."assets/apps/users/".$this->user_name."/ebay/product";
            $files = scandir($userEbayProductDIR);
            foreach($files as $file){
                    if(strpos($file, 'xml') > -1){
                            $return[] = array('file' => $file, 'fullpath' => $userEbayProductURL.'/'.$file);
                    }
            }

            echo json_encode($return);
    }

    function get_offer_file(){
            $return = array();
            //Create Compress File.
            $data = $this->init_packages();
            $param = array($this->user_name, $this->user_id, $this->user['site'], null, false);
            $this->load->library('Ebay/ebayofferexport', $param);
            $this->ebayofferexport->export_compress();

            $userEbayProductDIR = USERDATA_PATH.$this->user_name."/ebay/offer";
            $userEbayProductURL = base_url()."assets/apps/users/".$this->user_name."/ebay/offer";
            $files = scandir($userEbayProductDIR);
            foreach($files as $file){
                    if(strpos($file, 'xml') > -1){
                            $return[] = array('file' => $file, 'fullpath' => $userEbayProductURL.'/'.$file);
                    }
            }

            echo json_encode($return);
    }

    function get_orders_update() {        	
            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
            $this->load->library("Ebay/ebayorder", $param);
            $data_from                      = date('Y-m-d', strtotime('-180 days'));
            $data_to                        = date('Y-m-d', strtotime('+2 days'));
            $order_status					= isset($data['order_status']) ? $data['order_status'] : 'Completed';
            $parser                         = $this->ebayorder->get_orders_update($data_from, $data_to, 60, $order_status);

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function actions() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tab_data                                       = array();
            $tab_data['tab']                                = 1;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                
            $hidden_data                                    = array(
                    'update-actions-delete-success'         => $this->lang->line('update_actions_delete_success'),
                    'update-actions-delete-fail'            => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-export-success'         => $this->lang->line('update_actions_export_success'),
                    'update-actions-export-fail'            => $this->lang->line('update_actions_export_fail'),
                    'update-actions-orders-success'         => $this->lang->line('update_actions_orders_success'),
                    'update-actions-orders-fail'            => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-offers-success'         => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'            => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-send-mail'              => $this->lang->line('update_actions_send_mail'),
                    'update-actions-error'                  => $this->lang->line('update_actions_error'),
                    'update-actions-error-msg'              => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                          => $this->lang->line('starting_get'),
                    'starting-delete'                       => $this->lang->line('starting_delete'),
                    'starting-send'                         => $this->lang->line('starting_send'),

                    'no_online_product'                     => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'             => $this->lang->line('xml_validation_error'),
                    'empty_products'                        => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'actions',
                    'ajax_url'                              => '',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'wizard'                                => !empty($this->user['wizard']) ? $this->user['wizard'] : '',
                    'tab_data'                              => !empty($tab_data) ? $tab_data : '',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function actions_products() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tab_data                                       = array();
            $tab_data['tab']                                = 1;

            $error_messages = $this->verify_configurations();

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                        = array(
                    'update-actions-products-delete-success'    => $this->lang->line('update_actions_delete_success'),
                    'update-actions-products-delete-fail'       => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-products-export-success'    => $this->lang->line('update_actions_export_success'),
                    'update-actions-products-export-fail'       => $this->lang->line('update_actions_export_fail'),
                    'update-actions-products-orders-success'    => $this->lang->line('update_actions_orders_success'),
                    'update-actions-products-orders-fail'       => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-offers-success'             => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'                => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-products-send-mail'         => $this->lang->line('update_actions_send_mail'),
                    'update-actions-products-error'             => $this->lang->line('update_actions_error'),
                    'update-actions-products-error-msg'         => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                              => $this->lang->line('starting_get'),
                    'starting-delete'                           => $this->lang->line('starting_delete'),
                    'starting-send'                             => $this->lang->line('starting_send'),
                    'actions_compress_file_products_success'    => $this->lang->line('actions_compress_file_products_success'),
                    'actions_compress_file_products_fail'       => $this->lang->line('actions_compress_file_products_fail'),

                    'no_online_product'                         => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'                 => $this->lang->line('xml_validation_error'),
                    'empty_products'                            => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                             = array(
                    'current_page'                              => 'actions_products',
                    'ajax_url'                                  => '',
                    'root_page'                                 => 'actions/',
                    'btn_flow'                                  => 'close',
                    'hidden_data'                               => $hidden_data,
                    'wizard'                                    => !empty($this->user['wizard']) ? $this->user['wizard'] : '',
                    'tab_data'                                  => !empty($tab_data) ? $tab_data : '',
                    'error_messages'                            => $error_messages,
                    'remote'                                    => isset($this->session->userdata['remote']) && $this->session->userdata['remote']
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    private function verify_configurations(){
            $error_messages = array();
            $this->id_profile = 0;
            $mapping_carriers = $this->objectbiz->getShippingRules($this->user_name, $this->user['site'], $this->id_shop, 0);
            $getProfileMapping = $this->objectbiz->getEbayProfileShipping($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $mapping_categories = $this->objectbiz->getSelectedMappingCategories($this->user_name, $this->user['site'], $this->id_shop);
            $delete_product = $this->objectbiz->getDeleteProductActiveConfiguration($this->user['userID'], $this->user['site'], $this->id_shop);

            if( empty($this->user['listing_duration'])){
                    $error_messages[] = sprintf($this->lang->line('require_setting'), '<a href="'.base_url().'ebay/auth_setting/'.$this->packages.'">here</a>');
            }
            if(empty($mapping_carriers) && empty($getProfileMapping)){
                    $error_messages[] = sprintf($this->lang->line('require_carrier_mapping'), '<a href="'.base_url().'ebay/mapping/mapping_carriers/'.$this->packages.'">here</a>');
            }
            if(empty($mapping_categories)){
                    $error_messages[] = sprintf($this->lang->line('require_category_mapping'), '<a href="'.base_url().'ebay/mapping/mapping_categories/'.$this->packages.'">here</a>');
            }
            if(!empty($delete_product)){
                    $error_messages[] = $this->lang->line('require_delete_products');
            }
            return $error_messages;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function actions_offers() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tab_data                                       = array();
            $tab_data['tab']                                = 1;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $error_messages = $this->verify_configurations();

            $hidden_data                                    = array(
                    'update-actions-offers-delete-success'  => $this->lang->line('update_actions_delete_success'),
                    'update-actions-offers-delete-fail'     => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-offers-export-success'  => $this->lang->line('update_actions_export_success'),
                    'update-actions-offers-export-fail'     => $this->lang->line('update_actions_export_fail'),
                    'update-actions-offers-orders-success'  => $this->lang->line('update_actions_orders_success'),
                    'update-actions-offers-orders-fail'     => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-offers-success'         => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'            => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-offers-send-mail'       => $this->lang->line('update_actions_send_mail'),
                    'update-actions-offers-error'           => $this->lang->line('update_actions_error'),
                    'update-actions-offers-error-msg'       => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                          => $this->lang->line('starting_get'),
                    'starting-delete'                       => $this->lang->line('starting_delete'),
                    'starting-send'                         => $this->lang->line('starting_send'),
                    'actions_compress_file_products_success' => $this->lang->line('actions_compress_file_products_success'),
                    'actions_compress_file_products_fail'   => $this->lang->line('actions_compress_file_products_fail'),

                    'no_online_product'                     => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'             => $this->lang->line('xml_validation_error'),
                    'empty_products'                        => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'actions_offers',
                    'ajax_url'                              => '',
                    'root_page'                             => 'actions/',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'wizard'                                => !empty($this->user['wizard']) ? $this->user['wizard'] : '',
                    'tab_data'                              => !empty($tab_data) ? $tab_data : '',
                    'error_messages'                        => $error_messages,
                    'remote'                                => isset($this->session->userdata['remote']) && $this->session->userdata['remote']
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function actions_orders() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tab_data                                       = array();
            $tab_data['tab']                                = 1;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'update-actions-orders-delete-success'  => $this->lang->line('update_actions_delete_success'),
                    'update-actions-orders-delete-fail'     => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-orders-export-success'  => $this->lang->line('update_actions_export_success'),
                    'update-actions-orders-export-fail'     => $this->lang->line('update_actions_export_fail'),
                    'update-actions-orders-orders-success'  => $this->lang->line('update_actions_orders_success'),
                    'update-actions-orders-orders-fail'     => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-orders-export-success'  => $this->lang->line('update_actions_order_export_success'),
                    'update-actions-orders-export-fail'     => $this->lang->line('update_actions_order_export_fail'),
                    'update-actions-offers-success'         => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'            => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-orders-send-mail'       => $this->lang->line('update_actions_send_mail'),
                    'update-actions-orders-error'           => $this->lang->line('update_actions_error'),
                    'update-actions-orders-error-msg'       => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                          => $this->lang->line('starting_get'),
                    'starting-delete'                       => $this->lang->line('starting_delete'),
                    'starting-send'                         => $this->lang->line('starting_send'),

                    'hdd-id_order'                         	=> $this->lang->line('id_order'),
                    'hdd-profile_status'                   	=> $this->lang->line('profile_status'),
                    'hdd-created_date'                   	=> $this->lang->line('created_date'),
                    'hdd-item_id_only'                   	=> $this->lang->line('item_id_only'),
                    'hdd-sku'                   		=> $this->lang->line('sku'),
                    'hdd-product'                   	=> $this->lang->line('product'),
                    'hdd-site'                   		=> $this->lang->line('Site'),

                    'no_online_product'                     => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'             => $this->lang->line('xml_validation_error'),
                    'empty_products'                        => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'actions_orders',
                    'ajax_url'                              => '',
                    'root_page'                             => 'actions/',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'wizard'                                => !empty($this->user['wizard']) ? $this->user['wizard'] : '',
                    'tab_data'                              => !empty($tab_data) ? $tab_data : '',
                    'remote'                                    => isset($this->session->userdata['remote']) && $this->session->userdata['remote']
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function actions_deletes() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tab_data                                       = array();
            $tab_data['tab']                                = 1;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'update-actions-orders-delete-success'  => $this->lang->line('update_actions_delete_success'),
                    'update-actions-orders-delete-fail'     => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-orders-export-success'  => $this->lang->line('update_actions_export_success'),
                    'update-actions-orders-export-fail'     => $this->lang->line('update_actions_export_fail'),
                    'update-actions-orders-orders-success'  => $this->lang->line('update_actions_orders_success'),
                    'update-actions-orders-orders-fail'     => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-offers-success'         => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'            => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-orders-send-mail'       => $this->lang->line('update_actions_send_mail'),
                    'update-actions-orders-error'           => $this->lang->line('update_actions_error'),
                    'update-actions-orders-error-msg'       => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                          => $this->lang->line('starting_get'),
                    'starting-delete'                       => $this->lang->line('starting_delete'),
                    'starting-send'                         => $this->lang->line('starting_send'),

                    'no_online_product'                     => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'             => $this->lang->line('xml_validation_error'),
                    'empty_products'                        => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'actions_deletes',
                    'ajax_url'                              => '',
                    'root_page'                             => 'actions/',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'wizard'                                => !empty($this->user['wizard']) ? $this->user['wizard'] : '',
                    'tab_data'                              => !empty($tab_data) ? $tab_data : '',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function get_configuration() {
            $params = array();

            $configuration = $this->ebay_model->get_configuration($this->user_id, 'EBAY_SITE_ID');

            if(!isset($configuration[0]->value)) {
                    echo json_encode(array('error' => "eBay configuration error"));
                    die();
            }

            if(isset($configuration) && !empty($configuration)) {
                    $lang = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                    $site = $this->ebay_model->get_sites($configuration[0]->value);
                    $config_data = $this->my_feed_model->get_general($this->user_id);

                    if(!isset($config_data['FEED_BIZ']['value']))
                    {
                        echo json_encode(array('error' => "URL error"));
                        die();
                    }

                    $feed_biz = unserialize(base64_decode($config_data['FEED_BIZ']['value']));
                    $feed = unserialize(base64_decode($config_data['FEED_SHOP_INFO']['value']));

                     //var_dump($feed);
                    if(!isset($feed['url']['orderimport'])) {
                        echo json_encode(array('error' => "URL error"));
                        die();
                    }

//                $base_url = unserialize(base64_decode($config_data['FEED_BIZ']['value']));
                    $base_url = $feed_biz['base_url'];
                    $url = $feed['url']['orderimport'];

                    $params['base_url'] = $base_url;
                    $params['order_url'] = $url;
                    $params['sales_channal'] = 'eBay';
                    $params['id_shop'] = $this->id_shop;
                    $params['site'] = $site[0]->name;
                    $params['id_lang'] = $lang;
                    $params['username'] = $this->user_name;

                    //var_dump($this->my_feed_model->get_token($this->user_id)); exit;
                    $params['token'] = $this->my_feed_model->get_token($this->user_id);

            }
            echo json_encode($params);
    }

    public function ebay_wizard($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];

            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            if ( !isset($this->user['wizard_start']) || !is_numeric($this->user['site']) ) :
                    $this->user['wizard_start']             = 1;
            endif;
            $data                                           = $this->init_wizard_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (!isset($this->user['appMode']) || !isset($this->user['token']) || !isset($this->user['userID']) || !isset($this->user['wizard_start']) || $this->user['wizard_start'] == 1 || $this->user['wizard_start'] == 0) :      
                    $this->wizard_authorization();
                    unset($data['prev_page']);
            endif;
            if ( (isset($this->user['wizard_finish']) && $this->user['wizard_finish'] == 1) || $this->user['wizard_start'] == 2 ) : 
                    $this->wizard_choose();
            endif;
            if ( $this->user['wizard_start'] == 3 ) :   
                    $this->wizard_shippings();
            endif;
            if ( $this->user['wizard_start'] == 4 ) : 
                    $this->wizard_univers();
            endif;
            if ( $this->user['wizard_start'] == 5 ) :      
                    $this->wizard_categories();
            endif;
            if ( $this->user['wizard_start'] == 6 ) :      
                    $this->wizard_exports();
                    //unset($data['next_page']);
            endif;
            if ( $this->user['wizard_start'] == 7 ) :      
                    $this->wizard_finish();
                    unset($data['next_page']);
            endif;
            $data['popup']                                  = $this->user['wizard_start'];

            $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_authorization($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            if ( !isset($this->user['wizard_start']) || !is_numeric($this->user['site']) ) :
                    $this->user['wizard_start']             = 1;
            endif;
            $data                                           = $this->init_wizard_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_authorization',
                    'ajax_url'                              => '',
                    'btn_flow'                              => 'close',
                    'is_reset'                              => 'open',
                    'info'                                  => 'open',
                    'switch_data'                           => 'open',
                    'hidden_data'                           => array('user-not-available' => $this->lang->line('user_not_available'), 'auth-is-processing' => $this->lang->line('auth_is_processing')),
                    'appMode'                               => isset($this->user["appMode"]) ? $this->user["appMode"]   : '1',
                    'userID'                                => isset($this->user["userID"]) ? $this->user["userID"] : '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            unset($data['prev_page']);
            $ebay_data                                      = array(
                    'EBAY_WIZARD_START'                     => !empty($this->user['wizard_start']) ? $this->user['wizard_start'] : 1,
            );
            $this->user['wizard_start']                     = !empty($this->user['wizard_start']) ? $this->user['wizard_start'] : 1;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_choose($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $country_site                                   = $this->session->userdata['menu']['eBay']['submenu'];
            if ( !empty($country_site) && count($country_site) ) :
                    foreach ( $country_site as $key => $site ) :
                            $ext                            = '';
                            $substr                         = explode('.', $site['ext']);
                            foreach ( $substr as $string ) :
                                    if ( !empty($string) && $ext == '' ) :
                                            $ext            .= $string;
                                    elseif ( !empty($string) && $ext != '' ) :
                                            $ext            .= "_".$string;
                                    endif;
                            endforeach;
                            $country_site[$key]['ext_edit'] = $ext;
                    endforeach;
            endif;
            $replace_target                                 = array('{B}', '{/B}', '{BR}');
            $replace_pattern                                = array('<B>', '</B>', '<BR>');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_choose',
                    'ajax_url'                              => 'wizard_choose_save',
                    'info'                                  => 'open',
                    'info_wizard_choose'                    => str_replace($replace_target, $replace_pattern, $this->lang->line('info_wizard_choose')),
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'country_site'                          => !empty($country_site) ? $country_site : '',
            );
            $this->assign($assign);
            $ebay_data                                      = array(
                    'EBAY_WIZARD_FINISH'                    => 0,
                    'EBAY_WIZARD_START'                     => $this->popup,
            );
            $this->user['wizard_start']                     = $this->popup;
            $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function wizard_choose_save(){ 
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $ebay_data                              = array(
                                    'EBAY_SITE_ID'                  => $this->user['site'],
                            );
                            $result                                 = $this->ebay_model->set_configuration($this->user_id, $ebay_data);
                            echo ( $result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_univers($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $root                                           = $this->ebay_model->get_category_root($this->user['site']);
            $univer_selected                                = $this->objectbiz->getUnivers($this->user['site'], $this->id_shop);
            $message_document = $this->ebay_model->get_ebay_message_document(__CLASS__, __FUNCTION__, $this->iso_code);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_univers',
                    'ajax_url'                              => 'univers_save',
                    'message_document'                      => !empty($message_document) ? $message_document : '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'root'                                  => !empty($root) ? $root : '',
                    'select_root'                           => !empty($univer_selected) ? $univer_selected : '',
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_categories($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $data                                           = $this->prepare_categories($data);
            $message_document = $this->ebay_model->get_ebay_message_document(__CLASS__, __FUNCTION__, $this->iso_code);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_categories',
                    'ajax_url'                              => 'mapping_categories_save',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'choose-ebay-store-categories' => $this->lang->line('choose_ebay_store_categories'), 'select-a-option' => $this->lang->line('select_a_option')),
                    'data_tree'                             => 'open',
                    'message_document'                      => !empty($message_document) ? $message_document : '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'ebay_categories'                       => !empty($data['ebay_categories']) ? $data['ebay_categories'] : '',
                    'ebay_categories_selected'              => !empty($data['ebay_categories_selected']) ? $data['ebay_categories_selected'] : '',
                    'ebay_store_categories'                 => !empty($data['ebay_store_categories']) ? $data['ebay_store_categories'] : '',
                    'ebay_store_selected'                   => !empty($data['ebay_store_selected']) ? $data['ebay_store_selected'] : '',
                    'ebay_secondary_categories_selected'    => !empty($data['ebay_secondary_categories_selected']) ? $data['ebay_secondary_categories_selected'] : '',
                    'ebay_secondary_store_selected'         => !empty($data['ebay_secondary_store_selected']) ? $data['ebay_secondary_categories_selected'] : '',
                    'have_univer'                           => !empty($data['have_univer']) ? $data['have_univer'] : '',
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_tax($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $tax_data                                       = $this->objectbiz->getTaxsLang($this->id_shop, $this->id_lang);
            $tax_selected                                   = $this->objectbiz->getTaxsSelected($this->user['site'], $this->id_shop);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_tax',
                    'ajax_url'                              => 'tax_save',
                    'tax_lang'                              => !empty($tax_data) ? $tax_data : '',
                    'tax_selected'                          => !empty($tax_selected) ? $tax_selected : '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_shippings($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
            $this->pattern_code();
            $this->pattern_code                             = $this->config->item('currency_code');
            $code_real                                      = $this->ebay_model->get_postalcode_real($this->user["site"]);
            $replace_target                                 = array('{1}', '{2}', '{BR}');
            $replace_pattern                                = array($this->ext_site['name'], !empty($code_real['exam_code']) ? $code_real['exam_code'] : '', "<BR>");
            $ebay_dispatch_time                             = $this->ebay_model->get_dispatch_time();           

            if ( !empty($ebay_dispatch_time) ) :
                    foreach ( $ebay_dispatch_time as $dispatch_time ) :
                            if ( isset($this->user['dispatch_time']) && $dispatch_time['dispatch_time'] == $this->user['dispatch_time'] ) :
                                    $data['dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time'], 'selected' => 'selected');
                            else :
                                    $data['dispatch_time'][] = array('dispatch_time' => $dispatch_time['dispatch_time'], 'dispatch_time_name' => "days_".$dispatch_time['dispatch_time']);
                            endif;
                    endforeach;
                    $this->assign(array('dispatch_time' => $data['dispatch_time'], 'postal_code' => isset($this->user['postal_code']) ? $this->user['postal_code'] : ''));
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-form-postal-code"             => array(
                            'required'                      => $this->lang->line('validation_required_form_postal_code'),
                            'regex'                         => str_replace($replace_target, $replace_pattern, $this->lang->line('validation_regex_form_postal_code')),
                            'postal-valid'                  => str_replace($replace_target, $replace_pattern, $this->lang->line('validation_postal_valid_form_postal_code')),
                    ),
            );
            $message_document = $this->ebay_model->get_ebay_message_document(__CLASS__, __FUNCTION__, $this->iso_code);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_shippings',
                    'ajax_url'                              => 'mapping_carriers_save',
                    'message_document'                      => !empty($message_document) ? $message_document : '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'code'                                  => isset($code_real) ? $code_real : '',
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array(
                            'id_profile'                    => $this->id_profile, 
                            'form-template-checkbox'        => 1, 
                            'returns-policy'                => !empty($this->user['returns_policy'])        ? $this->user['returns_policy'] : '',
                            'returns-within'                => !empty($this->user['returns_within'])        ? $this->user['returns_within'] : '',
                            'returns-pays'                  => !empty($this->user['returns_pays'])          ? $this->user['returns_pays'] : '',
                            'returns-information'           => !empty($this->user['returns_information'])   ? $this->user['returns_information'] : '',
                            'listing-duration'              => !empty($this->user['listing_duration'])      ? $this->user['listing_duration'] : '',
                            'no-image'                      => !empty($this->user['no_image'])              ? $this->user['no_image'] : '',
                            'discount'                      => !empty($this->user['discount'])              ? $this->user['discount'] : '',
                            'just-orders'                   => !empty($this->user['just_orders'])           ? $this->user['just_orders'] : '',
                    ),
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_payments($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $this->prepare_payment(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_payments',
                    'ajax_url'                              => 'advance_profile_payment_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_returns($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $this->prepare_return(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_returns',
                    'ajax_url'                              => 'advance_profile_return_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_conditions($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $this->prepare_conditions();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_conditions',
                    'ajax_url'                              => 'advance_profile_condition_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_attributes($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            $this->id_profile                               = 0;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $this->prepare_attributes(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_attributes',
                    'ajax_url'                              => 'attribute_ebay_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_exports($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            $hidden_data                                    = array(
                    'update-actions-products-delete-success'         => $this->lang->line('update_actions_delete_success'),
                    'update-actions-products-delete-fail'            => $this->lang->line('update_actions_delete_fail'),
                    'update-actions-products-export-success'         => $this->lang->line('update_actions_export_success'),
                    'update-actions-products-export-fail'            => $this->lang->line('update_actions_export_fail'),
                    'update-actions-products-orders-success'         => $this->lang->line('update_actions_orders_success'),
                    'update-actions-products-orders-fail'            => $this->lang->line('update_actions_orders_fail'),
                    'update-actions-offers-success'                  => $this->lang->line('update_actions_offers_success'),
                    'update-actions-offers-fail'                     => $this->lang->line('update_actions_offers_fail'),
                    'update-actions-products-send-mail'              => $this->lang->line('update_actions_send_mail'),
                    'update-actions-products-error'                  => $this->lang->line('update_actions_error'),
                    'update-actions-products-error-msg'              => $this->lang->line('update_actions_error_msg'),
                    'starting-get'                          => $this->lang->line('starting_get'),
                    'starting-delete'                       => $this->lang->line('starting_delete'),
                    'starting-send'                         => $this->lang->line('starting_send'),
                    'actions_compress_file_products_success' => $this->lang->line('actions_compress_file_products_success'),
                    'actions_compress_file_products_fail' => $this->lang->line('actions_compress_file_products_fail'),

                    'no_online_product'                     => sprintf($this->lang->line('no_online_product'), $this->user['userID']),
                    'xml_validation_error-send'             => $this->lang->line('xml_validation_error'),
                    'empty_products'                        => $this->lang->line('empty_products'),
            );
             ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search(__FUNCTION__, array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_exports',
                    'ajax_url'                              => '',
                    'hidden_data'                           => $hidden_data,
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'popup_hide'                            => 'open',
                    'tab_data'                              => array('tab' => 2),
                            'hide_next'								=> true
            );
            $this->assign($assign);
//                 unset($data['next_page']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function wizard_finish($debug = null) {
            if ( !isset($this->user['site']) || !is_numeric($this->user['site']) ) :
                    $this->user['site']                      = 0;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->default_site                             = $this->user['site'];
            $data                                           = $this->init_packages();
            $data['packages']                               = $this->ebay_model->get_packages_from_site($this->user['site'], Ebay::MARKETPLACE_ID);
            $data['id_site']                                = $this->user['site'];
            $this->packages                                 = $data['packages'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->getConfiguration();
            $data                                           = $this->init_wizard_pages($data);
            $data['statistics']                             = $this->objectbiz->getLastBatchID($this->user['site'], $this->id_shop);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = count($data['next']) + 1;
            $assign                                         = array(
                    'current_page'                          => 'wizard_finish',
                    'ajax_url'                              => '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 7,
                    'statistics'                            => !empty($data['statistics']) ? $data['statistics'] : '',
                    'hide_previous'                         => true,
            );
            $this->assign($assign);
            unset($data['next_page']);
            $ebay_data                                      = array(
                    'EBAY_WIZARD_FINISH'                    => 1,
                    'EBAY_WIZARD_START'                     => 2,
                    'EBAY_WIZARD_SUCCESS'                   => 0,
            );
            $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( $debug == $this->debug ) :
                    $this->smarty->view('ebay/wizard/main_wizard.tpl', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function ebay_wizard_clear() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $clear) :
                                    $ebay_data                              = array(
                                            'EBAY_WIZARD_FINISH'            => 0,
                                            'EBAY_WIZARD_START'             => $clear['id_popup'] - 1,
                                    );
                                    $result                                 = $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
                            endforeach;
                            echo ( $result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function ebay_wizard_start() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $clear) :
                                    $ebay_data                              = array(
                                            'EBAY_WIZARD_START'             => $clear['id_popup'] + 1,
                                    );
                                    $result                                 = $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
                            endforeach;
                            echo ( $result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function export_from_wizard() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $clear) :
                                    $ebay_data                              = array(
                                            'EBAY_WIZARD_START'             => $clear['id_popup'] + 1,
                                            'EBAY_WIZARD'                   => $clear['from_wizard'],
                                            'EBAY_WIZARD_SUCCESS'           => $clear['from_success'],
                                    );
                                    $result                                 = $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
                            endforeach;
                            echo ( $result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    public function ebay_ajax_click() {
            if ( $this->input->post() ) :
                    $username                               = $this->input->post('username');
                    $userid                                 = $this->input->post('userid');

                    if ( isset($this->session->userdata['ebay_click']) ) :
                            $session                        = unserialize($this->encrypt->decode($this->session->userdata['ebay_click']));
                            if ( $session['user_name'] == $this->user_name && $session['user_id'] == $this->user_id && (strtotime('now') - strtotime($session['click_date'])) > 300 ) :
                                    $this->session->unset_userdata('ebay_click');
                                    return TRUE;
                            else :
                                    return FALSE;
                            endif;
                    else :
                            $ajax_data                      = array(
                                    'user_name'             => $username,
                                    'user_id'               => $userid,
                                    'click_date'            => date('Y-m-d H:i:s', strtotime('now')),
                            );
                            $session                        = $this->encrypt->encode(serialize($ajax_data));
                            $this->session->set_userdata('ebay_click', $session);
                            return TRUE;
                    endif;
            endif;
            return FALSE;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function tax() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $tax_data                                       = $this->objectbiz->getTaxsLang($this->id_shop, $this->id_lang);
            $tax_selected                                   = $this->objectbiz->getTaxsSelected($this->user['site'], $this->id_shop);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'tax',
                    'ajax_url'                              => 'tax_save',
                    'tax_lang'                              => !empty($tax_data) ? $tax_data : '',
                    'tax_selected'                          => !empty($tax_selected) ? $tax_selected : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function tax_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $this->objectbiz->deleteColumn('ebay_tax', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                            $tax_insert                     = '';
                            foreach ( $data as $tax ) :
                                    unset($data_insert);
                                    if ( !empty($tax) && $tax['id_tax'] != 0 ) :
                                            $tax_data               = $this->objectbiz->getTaxs($this->id_shop, $tax['id_tax']);
                                            $data_insert            = array(
                                                    "id_tax"        => $tax['id_tax'],
                                                    "id_site"       => $tax['id_site'],
                                                    "id_shop"       => $this->id_shop,
                                                    "id_lang"       => $this->id_lang,
                                                    "tax_name"      => $tax['tax_name'],
                                                    "tax_rate"      => $tax_data[0]['rate'],
                                                    "tax_type"      => $tax_data[0]['type'],
                                                    "decimal_value" => $tax['tax_decimal'],
                                                    "date_add"      => date("Y-m-d H:i:s", strtotime("now")),
                                            );
                                            $tax_insert             .= $this->objectbiz->mappingTableOnly('ebay_tax', $data_insert);
                                    endif;
                            endforeach;
                            $result_insert                          = $this->objectbiz->transaction($tax_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_tax', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Fail";
            endif;
    }

    function get_postalcode_valid() {
            if ($this->input->post()) :
                    $postal_valid                                    = $_POST['postal_valid'];
                    if ( !empty($postal_valid) && count($postal_valid) ) :
                            $valid_result                            = $this->ebay_model->get_postalcode_valid($postal_valid['postal_code'], $postal_valid['zip']);
                            echo ( $valid_result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    function prepare_conditions() {
            $tab_data                                       = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $profile_configuration                          = $this->objectbiz->getEbayProfileConfiguration($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $profile_data                                   = $this->objectbiz->getEbayProfileCondition($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $mapping_data                                   = $this->objectbiz->getEbayMappingCondition($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $mapping_data_description                       = $this->objectbiz->getEbayMappingConditionDescription($this->user_name, $this->user['site'], $this->id_shop, $this->id_profile);
            $shop_condition                                 = $this->objectbiz->getRealCondition($this->user_name, $this->id_shop);
            $category_group['id_ebay_category'][]           = $profile_configuration['id_ebay_category'];
            if ( empty($category_group['id_ebay_category'][0]) ) :
                    $category_group                         = $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->user['site'], $this->id_shop);
                    if ( !empty($category_group) ) :
                            $category_group                 = $this->generate_additions_group('id_ebay_category', 'id_ebay_category', $category_group);
                    else :
                            $category_group['id_ebay_category'][0]  = '';
                    endif;  
            endif;
            $category_group                                 = "'".implode("', '", $category_group['id_ebay_category'])."'";
            $ebay_condition_values                          = $this->ebay_model->get_condition_values($this->user['site'], (!empty($category_group) && $category_group != "''") ? $category_group : null);

            if ( empty($ebay_condition_values) ) :
                    $assign                                         = array(
                            'condition_values'                      => !empty($ebay_condition_values) ? $ebay_condition_values : '',
                                    'error_code'							=> $category_group == "''" ? 'mapping_not_found' : 'condition_not_found'
                    );
                    $this->assign($assign);
                    return;
            endif;
            $tab_data['type']                               = $type;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( (!empty($profile_data) && $profile_data['is_enabled'] == '1') || (!empty($tab_data['type']) && $tab_data['type'] == 'product') ) :
                    $tab_data['tab']                        = 2;
            else :
                    $tab_data['tab']                        = 1;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    $profile_data['condition_description']  = htmlspecialchars_decode($profile_data['condition_description']);
            endif;
            if ( !empty($mapping_data) ) : 
                    $mapping_data                           = $this->generate_key('condition_value', $mapping_data);
                    foreach ( $shop_condition as $key_shop => $shop_condition_value ) : 
                            foreach ( $mapping_data as $mapping_shop => $mapping_condition_value ) :
                                    if ( !empty($mapping_condition_value) && array_key_exists('condition_data', $mapping_condition_value ) ) :
                                            if ( $shop_condition_value['txt'] == $mapping_condition_value['condition_data'] ) :
                                                    $shop_condition[$key_shop]['condition_value'][] = $mapping_condition_value['condition_value'];
                                                    $shop_condition[$key_shop]['condition_name'][]  = $mapping_condition_value['condition_name'];
                                            endif;
                                    else :
                                            foreach ( $mapping_condition_value as $mapping_shops => $mapping_condition_values ) :
                                                    if ( $shop_condition_value['txt'] == $mapping_condition_values['condition_data'] ) :
                                                            $shop_condition[$key_shop]['condition_value'][] = $mapping_condition_value['condition_value'];
                                                            $shop_condition[$key_shop]['condition_name'][]  = $mapping_condition_value['condition_name'];
                                                    endif;
                                            endforeach;
                                    endif;
                            endforeach;
                    endforeach;
            endif;
            if ( !empty($mapping_data_description) ) :
                    $mapping_data_description               = $this->generate_key('condition_data', $mapping_data_description);
            endif;
            if ( !empty($ebay_condition_values)  ) :
                    $condition_description                  = array();
                    foreach ( $ebay_condition_values as $key_values => $condition_values) :
                            if ( empty($condition_description['new']['condition_description']) && ($condition_values['condition_value'] == '1000' || $condition_values['condition_value'] == '1500' || $condition_values['condition_value'] == '1750') ) :
                                    $condition_description['new']        = array(
                                            'condition_name'        => 'new', 
                                            'condition_description' => !empty($mapping_data_description['new']) ? htmlspecialchars_decode($mapping_data_description['new']['condition_description']) : ''
                                    );
                            endif;
                            if ( empty($condition_description['good']['condition_description']) && ($condition_values['condition_value'] == '4000' || $condition_values['condition_value'] == '5000') ) :
                                    $condition_description['good']    = array(
                                            'condition_name'        => 'good',
                                            'condition_description' => !empty($mapping_data_description['good']) ? htmlspecialchars_decode($mapping_data_description['good']['condition_description']) : ''
                                    );
                            endif;
                            if ( empty($condition_description['used']['condition_description']) && ($condition_values['condition_value'] == '3000') ) :
                                    $condition_description['used']    = array(
                                            'condition_name'        => 'used',
                                            'condition_description' => !empty($mapping_data_description['used']) ? htmlspecialchars_decode($mapping_data_description['used']['condition_description']) : ''
                                    );
                            endif;
                            if ( empty($condition_description['refurbished']['condition_description']) && ($condition_values['condition_value'] == '2000' || $condition_values['condition_value'] == '2500') ) :
                                    $condition_description['refurbished']    = array(
                                            'condition_name'        => 'refurbished',
                                            'condition_description' => !empty($mapping_data_description['refurbished']) ? htmlspecialchars_decode($mapping_data_description['refurbished']['condition_description']) : ''
                                    );
                            endif;
                            if ( empty($condition_description['acceptable']['condition_description']) && ($condition_values['condition_value'] == '6000') ) :
                                    $condition_description['acceptable']    = array(
                                            'condition_name'    => 'acceptable',
                                            'condition_description' => !empty($mapping_data_description['acceptable']) ? htmlspecialchars_decode($mapping_data_description['acceptable']['condition_description']) : ''
                                    );
                            endif;
                    endforeach;
            endif;

            $hidden_data                                    = array(
                    'id_profile'                            => $this->id_profile,
                    'remaining'                             => $this->lang->line('remaining'),
            );
//                echo "<pre>", print_r($ebay_condition_values, true), "</pre>";exit();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'condition_values'                      => !empty($ebay_condition_values) ? $ebay_condition_values : '',
                    'shop_condition'                        => !empty($shop_condition) ? $shop_condition : '',
                    'tab_data'                              => !empty($tab_data) ? $tab_data : '',
                    'condition_description'                 => !empty($condition_description) ? $condition_description : '',
                    'hidden_data'                           => $hidden_data,
                    'tinymce'                               => 'open',
                    'error_code'							=> ''
            );
            $this->assign($assign);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function mapping_conditions(){ 
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $this->id_profile                               = 0;
            $this->prepare_conditions();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'mapping_conditions',
                    'ajax_url'                              => 'advance_profile_condition_save',
                    'root_page'                             => 'mapping/',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data); 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function price_modifier(){ 
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            $this->prepare_price_modifier();
            $this->smarty->view('ebay/main_template.tpl', $data); 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function price_modifier_save() { 
            $data                                           = $this->input->post() ? $_POST['data'] : array();
            if (isset($data[0]) && isset($data[0]['id_profile'])) :

                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $price_insert                   = '';
                            $id_profile                     = current($data);
                            $this->objectbiz->deleteColumn('ebay_price_modifier', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop, 'id_profile' => $id_profile['id_profile']));
                            foreach( $data as $price) : 
                                    unset($data_price);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( !empty($price['step']) ) :
                                            $data_set                       = array(
                                                    'completed'             => round((100 * $price['step']) / 9, 2),
                                            );
                                            $where_set                      = array(
                                                    'id_profile'            => $price['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                            );
                                    endif;

                                    $price_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                    if ( isset($price['price_from']) && is_numeric($price['price_from']) && isset($price['price_value']) && is_numeric($price['price_value']) && isset($price['price_to']) && is_numeric($price['price_to']) ) :
                                            $data_price             = array(
                                                    'id_profile'    => $price['id_profile'],
                                                    'id_site'       => $this->user['site'],
                                                    'id_shop'       => $this->id_shop,
                                                    'price_from'    => $price['price_from'],
                                                    'price_to'      => $price['price_to'],
                                                    'price_value'   => $price['price_value'],
                                                    'price_type'    => $price['price_type'],
                                                    'decimal_value' => $price['rounding'],
                                                    'rel'           => $price['rel'],
                                                    'date_add'      => date('Y-m-d H:i:s', strtotime('now')),
                                            );
                                            $price_insert           .= $this->objectbiz->mappingTableOnly('ebay_price_modifier', $data_price);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($price_insert, true);
                            echo "Success";
                    else :
                            echo "Fail";
                    endif;
            else :
                    $this->objectbiz->deleteColumn('ebay_price_modifier', array('id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                    echo "Success";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonAdvanceAllCategory() {
            $html = '';                   
            $sub_html = '';       
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/advanced/'.$this->user['site'].'/';
                            $time                                           = $this->ebay_model->get_category_max_date();
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    $this->dir_remove($dir_temp);
                            if ( file_exists($dir_temp) && !file_exists($dir_temp.strtotime($time['Date']).'.json') && ($handle = opendir($dir_temp)) ) :
                            endif;
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            foreach ( $data as $category ) :
                                    if ( !isset($category['id_category']) ) :
                                            $category['id_category'] = 0;
                                            $arr_selected_categories        = $this->ebay_model->get_category_array($this->user['site'], true);
                                            $breadcrumbs                    = array(0 => 'All');
                                            $category['id_category']        = 0;
                                    endif;
                                    if ( isset($category['id_category']) && file_exists($dir_temp.strtotime($time['Date']).'.json') && file_exists($dir_temp.$category['id_category'].'.json') && ($handle = opendir($dir_temp)) ) :
                                            $html                                   = json_decode(file_get_contents($dir_temp.$category['id_category'].'.json'), true);
                                    else :
                                            if ( isset($category['id_category']) && is_numeric($category['id_category'])) :
                                                    $arr_selected_categories        = $this->ebay_model->get_category_array($this->user['site'], false, array('id_parent' => $category['id_category']));
                                                    if ( !empty($category['breadCroumbs']) ) :
                                                            foreach ( $category['breadCroumbs'] as $breadcrumbs_data ) :
                                                                    $breadcrumbs[$breadcrumbs_data['id_category']]  = $breadcrumbs_data['name'];
                                                            endforeach;
                                                    endif;
                                                    $selected_categories                    = $this->ebay_model->get_category_array($this->user['site'], false, array('ebay_categories.id_category' => $category['id_category']));
                                            endif;
                                            if ( !empty($arr_selected_categories) ) :
                                                    foreach ($arr_selected_categories as $asc) :
                                                            $sub_html               .= $this->getAdvanceSubCatTree($asc);
                                                    endforeach;
                                                    $this->smarty->assign('checked', '');
                                                    $this->smarty->assign('root', true);
                                                    $this->smarty->assign('profile', '');                
                                                    $this->smarty->assign('breadcrumbs', isset($breadcrumbs) ? $breadcrumbs : '');
                                                    $this->smarty->assign('parent_name', isset($selected_categories[0]['name']) ? $selected_categories[0]['name'] : '');
                                                    $this->smarty->assign('parent_id', isset($selected_categories[0]['id_parent']) ? $selected_categories[0]['id_parent'] : '');
                                                    $this->smarty->assign('sub_html', $sub_html);
                                                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
                                            endif;

                                            $response['posts']                      = json_encode($html);
                                            if ( !file_exists($dir_temp) ) {
                                                    $old = umask(0);
                                                    mkdir($dir_temp, 0777, true);
                                                    umask($old);
                                            }
                                            if ( !empty($response['posts']) ) :
                                                    file_put_contents($dir_temp.$category['id_category'].'.json', $response['posts']);
                                                    file_put_contents($dir_temp.strtotime($time['Date']).'.json', '');
                                            endif;
                                    endif;
                            endforeach;
                            echo $html;
                    endif;  
            else :
                    $this->smarty->assign('checked', '');
                    $this->smarty->assign('root', true);
                    $this->smarty->assign('profile', '');                
                    $this->smarty->assign('breadcrumbs', isset($breadcrumbs) ? $breadcrumbs : '');
                    $this->smarty->assign('parent_name', isset($selected_categories[0]['name']) ? $selected_categories[0]['name'] : '');
                    $this->smarty->assign('parent_id', isset($selected_categories[0]['id_parent']) ? $selected_categories[0]['id_parent'] : '');
                    $this->smarty->assign('sub_html', $sub_html);
                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
                    echo $html;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function getAdvanceSubCatTree($asc){
            $html = '';
            $arr_selected_categories        = $this->ebay_model->get_category_array($this->user['site'], false, array('id_parent' => $asc['id_category']));
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($asc) ) :                                                                 
                    $this->smarty->assign('child', !empty($arr_selected_categories) ? true : false);
                    $this->smarty->assign('selected', '');
                    $this->smarty->assign('profile', true);                                          
                    $this->smarty->assign('type', $asc['level']);
                    $this->smarty->assign('id_category', $asc['id_category']);
                    $this->smarty->assign('name', $asc['name']);
                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
            endif;
            return $html;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonAdvanceAllStoreCategory() {
            $html = '';    
            $sub_html = '';                  
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach ( $data as $category ) :
                                    if ( isset($category['get_store']) && is_numeric($category['get_store']) ) :      
                                            $parser = $this->ebaylib->getStore($this->user['site']);
                                            $param                              = array($this->user_name, $this->user_id, $this->user['site']);
                                            $this->load->library("Ebay/ebaystore", $param);
                                            $parser                             = $this->ebaystore->get_store();
                                            return;
                                            exit();
                                    endif;   
                                    if ( isset($category['id_category']) && is_numeric($category['id_category'])) :
                                            $arr_selected_categories        = $this->objectbiz->getChildStoreCategory($this->user_name, $this->user['site'], $this->id_shop, $category['id_category']);
                                            if ( !empty($category['breadCroumbs']) ) :
                                                    foreach ( $category['breadCroumbs'] as $breadcrumbs_data ) :
                                                            $breadcrumbs[$breadcrumbs_data['id_category']]  = $breadcrumbs_data['name'];
                                                    endforeach;
                                            endif;
                                            $selected_categories            = $this->objectbiz->getSelectedStoreCategory($this->user_name, $this->user['site'], $this->id_shop, $category['id_category']);
                                    else: 
                                            $arr_selected_categories        = $this->objectbiz->getRootStoreCategory($this->user_name, $this->user['site'], $this->id_shop);
                                            $breadcrumbs                    = array(0 => 'All');
                                            $category['id_category']        = 0;
                                            if ( empty($arr_selected_categories) ) :
                                                    $sub_html = $this->lang->line('not_have_store');
                                                    $this->smarty->assign('checked', '');
                                                    $this->smarty->assign('root', true);
                                                    $this->smarty->assign('profile', '');                
                                                    $this->smarty->assign('breadcrumbs', isset($breadcrumbs) ? $breadcrumbs : '');
                                                    $this->smarty->assign('parent_name', isset($selected_categories[0]['name_category']) ? $selected_categories[0]['name_category'] : '');
                                                    $this->smarty->assign('parent_id', isset($selected_categories[0]['parent']) ? $selected_categories[0]['parent'] : '');
                                                    $this->smarty->assign('sub_html', $sub_html);
                                                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
                                            endif;
                                    endif;
                                    if ( !empty($arr_selected_categories) ) :
    //                                        echo "<pre>", print_r($arr_selected_categories, true), "</pre>";exit();
                                            foreach ($arr_selected_categories as $asc) :
                                                    $sub_html               .= $this->getAdvanceSubCatStoreTree($asc);
                                            endforeach;
                                            $this->smarty->assign('checked', '');
                                            $this->smarty->assign('root', true);
                                            $this->smarty->assign('profile', '');                
                                            $this->smarty->assign('breadcrumbs', isset($breadcrumbs) ? $breadcrumbs : '');
                                            $this->smarty->assign('parent_name', isset($selected_categories[0]['name_category']) ? $selected_categories[0]['name_category'] : '');
                                            $this->smarty->assign('parent_id', isset($selected_categories[0]['parent']) ? $selected_categories[0]['parent'] : '');
                                            $this->smarty->assign('sub_html', $sub_html);
                                            $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
                                    endif;  
                            endforeach;
                    endif;
                    echo $html;
            else :
                    $sub_html = $this->lang->line('not_have_store');
                    $this->smarty->assign('checked', '');
                    $this->smarty->assign('root', true);
                    $this->smarty->assign('profile', '');                
                    $this->smarty->assign('error', true);                
                    $this->smarty->assign('breadcrumbs', isset($breadcrumbs) ? $breadcrumbs : '');
                    $this->smarty->assign('parent_name', isset($selected_categories[0]['name_category']) ? $selected_categories[0]['name_category'] : '');
                    $this->smarty->assign('parent_id', isset($selected_categories[0]['parent']) ? $selected_categories[0]['parent'] : '');
                    $this->smarty->assign('sub_html', $sub_html);
                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
                    echo $html;
            endif;
    }      

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function getAdvanceSubCatStoreTree($asc) {
            $html = '';
            $arr_selected_categories        = $this->objectbiz->getChildStoreCategory($this->user_name, $this->user['site'], $this->id_shop, $asc['id_category']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($asc) ) :                                                                 
                    $this->smarty->assign('child', !empty($arr_selected_categories) ? true : false);
                    $this->smarty->assign('selected', '');
                    $this->smarty->assign('profile', true);                                          
                    $this->smarty->assign('type', $asc['order_item']);
                    $this->smarty->assign('id_category', $asc['id_category']);
                    $this->smarty->assign('name', $asc['name_category']);
                    $html .= $this->smarty->fetch('ebay/mapping/categoryeBay.tpl');
            endif;
            return $html;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profiles(){ 
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'advance_profiles',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_main(){ 
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'advance_main',
                    'profile_rules'                         => isset($this->user['profile_rules']) && is_numeric($this->user['profile_rules']) ? $this->user['profile_rules'] : 0
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_main_save() { 
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $set) :
                                    $ebay_data                              = array(
                                            'EBAY_PROFILE_RULES'            => $set['id_profile_rule'],
                                    );
                                    $result                                 = $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
                            endforeach;
                            echo ( $result ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_categories_main() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-profile'                         => $this->lang->line('advance_categories_main_manager'),
                    'update-advance_categories_main-status-success'     => $this->lang->line('update_advance_categories_main_status_success'),
                    'update-advance_categories_main-status-fail'        => $this->lang->line('update_advance_categories_main_status_fail'),
                    'update-advance_categories_main-delete-success'     => $this->lang->line('update_advance_categories_main_delete_success'),
                    'update-advance_categories_main-delete-fail'        => $this->lang->line('update_advance_categories_main_delete_fail'),
                    'update-advance_categories_main-add-success'        => $this->lang->line('update_advance_categories_main_add_success'),
                    'update-advance_categories_main-add-fail'           => $this->lang->line('update_advance_categories_main_add_fail'),
                    'are-you-sure'                          => $this->lang->line('are_you_sure'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'profile-status'                        => $this->lang->line('profile_status'),
                    'profile-create-by'                     => $this->lang->line('profile_create_by'),
                    'profile-completed-percent'             => $this->lang->line('profile_completed_percent'),
                    'created-date'                          => $this->lang->line('created_date'),
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'advance_categories_main',
                    'ajax_url'                              => 'advance_categories_main_save',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'iframe_data'                           => 'open',
                    'statistics'                            => !empty($orders) ? $orders : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_categories_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $profile_name                                   = $this->input->get('profile_name');
            $ebay_user                                      = $this->input->get('ebay_user');
            $date_add                                       = $this->input->get('date_add');
            $completed                                      = $this->input->get('completed');
            $is_enabled                                     = $this->input->get('is_enabled');
            $type                                           = $this->input->get('type');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $all                                            = $this->input->get('all');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getAdvanceCategoriesByParam($this->user_name, array(
                    'm.ebay_user'                           => $this->user['userID'], 
                    'm.id_site'                             => $this->user['site'], 
                    'm.id_shop'                             => $this->id_shop,
                    'm.date_add'                            => !empty($all) ? $all : $date_add,
                    'm.profile_name'                        => !empty($all) ? $all : htmlspecialchars($profile_name),
                    'm.completed'                           => !empty($all) ? $all : $completed,
                    'n.type'                                => !empty($all) ? $all : $type,
                    'm.is_enabled'                          => !empty($all) ? $all : $is_enabled,
            ), $limit, $offset, array(
                'm.id_profile as id_profile', 
                'm.profile_name as profile_name', 
                'm.ebay_user as ebay_user', 
                'm.completed as completed', 
                'm.is_enabled as is_enabled', 
                'm.date_add as date_add'), $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($data['statistics']) ) :
                    foreach ( $data['statistics'] as $key => $statistic_data ) :
                            $data['statistics'][$key]['check_all']      = $data['statistics'][$key]['id_profile'];
                            $data['statistics'][$key]['profile_edit']   = "edit";
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['count']                                  = $this->objectbiz->getAdvanceCategoriesByParam($this->user_name, array(
                    'm.ebay_user'                           => $this->user['userID'], 
                    'm.id_site'                             => $this->user['site'], 
                    'm.id_shop'                             => $this->id_shop,
                    'm.date_add'                            => !empty($all) ? $all : $date_add,
                    'm.profile_name'                        => !empty($all) ? $all : htmlspecialchars($profile_name),
                    'm.completed'                           => !empty($all) ? $all : $completed,
                    'n.type'                                => !empty($all) ? $all : $type,
                    'm.is_enabled'                          => !empty($all) ? $all : $is_enabled,
            ), 0, 0, 'count(*)', $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = isset($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics']);
            echo json_encode($value);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_products_main() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'id_user'                               => $this->user_id,
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-profile'                         => $this->lang->line('advance_categories_main_manager'),
                    'update-advance_categories_main-status-success'     => $this->lang->line('update_advance_categories_main_status_success'),
                    'update-advance_categories_main-status-fail'        => $this->lang->line('update_advance_categories_main_status_fail'),
                    'update-advance_categories_main-delete-success'     => $this->lang->line('update_advance_categories_main_delete_success'),
                    'update-advance_categories_main-delete-fail'        => $this->lang->line('update_advance_categories_main_delete_fail'),
                    'update-advance_categories_main-add-success'        => $this->lang->line('update_advance_categories_main_add_success'),
                    'update-advance_categories_main-add-fail'           => $this->lang->line('update_advance_categories_main_add_fail'),
                    'are-you-sure'                          => $this->lang->line('are_you_sure'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'profile-status'                        => $this->lang->line('profile_status'),
                    'profile-create-by'                     => $this->lang->line('profile_create_by'),
                    'profile-completed-percent'             => $this->lang->line('profile_completed_percent'),
                    'created-date'                          => $this->lang->line('created_date'),
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'advance_products_main',
                    'ajax_url'                              => 'advance_categories_main_save',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'hidden_data'                           => $hidden_data,
                    'iframe_data'                           => 'open',
                    'statistics'                            => !empty($orders) ? $orders : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_products_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $profile_name                                   = $this->input->get('profile_name');
            $ebay_user                                      = $this->input->get('ebay_user');
            $date_add                                       = $this->input->get('date_add');
            $completed                                      = $this->input->get('completed');
            $is_enabled                                     = $this->input->get('is_enabled');
            $type                                           = $this->input->get('type');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $all                                            = $this->input->get('all');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getAdvanceProductsByParam($this->user_name, array(
                    'm.ebay_user'                           => $this->user['userID'], 
                    'm.id_site'                             => $this->user['site'], 
                    'm.id_shop'                             => $this->id_shop,
                    'm.date_add'                            => !empty($all) ? $all : $date_add,
                    'm.profile_name'                        => !empty($all) ? $all : htmlspecialchars($profile_name),
                    'm.completed'                           => !empty($all) ? $all : $completed,
                    'n.type'                                => !empty($all) ? $all : $type,
                    'm.is_enabled'                          => !empty($all) ? $all : $is_enabled,
            ), $limit, $offset, array(
                'm.id_profile as id_profile', 
                'm.profile_name as profile_name', 
                'm.ebay_user as ebay_user', 
                'm.completed as completed', 
                'm.is_enabled as is_enabled', 
                'm.date_add as date_add'), $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($data['statistics']) ) :
                    foreach ( $data['statistics'] as $key => $statistic_data ) :
                            $data['statistics'][$key]['check_all']      = $data['statistics'][$key]['id_profile'];
                            $data['statistics'][$key]['profile_edit']   = "edit";
                    endforeach;
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['count']                                  = $this->objectbiz->getAdvanceProductsByParam($this->user_name, array(
                    'm.ebay_user'                           => $this->user['userID'], 
                    'm.id_site'                             => $this->user['site'], 
                    'm.id_shop'                             => $this->id_shop,
                    'm.date_add'                            => !empty($all) ? $all : $date_add,
                    'm.profile_name'                        => !empty($all) ? $all : htmlspecialchars($profile_name),
                    'm.completed'                           => !empty($all) ? $all : $completed,
                    'n.type'                                => !empty($all) ? $all : $type,
                    'm.is_enabled'                          => !empty($all) ? $all : $is_enabled,
            ), 0, 0, 'count(*)', $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = isset($data['count']) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics']);
            echo json_encode($value);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_categories_main_save() { 
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $data_create                            = isset($_POST['data_create']) ? $_POST['data_create'] : '';
                    $data_delete                            = isset($_POST['data_delete']) ? $_POST['data_delete'] : '';
                    $data_enabled                           = isset($_POST['data_enabled']) ? $_POST['data_enabled'] : '';
                    $data_disabled                          = isset($_POST['data_disabled']) ? $_POST['data_disabled'] : '';
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $enabled_insert                 = '';
                            $disabled_insert                = '';
                            $create_insert                  = '';
                            $delete_insert                  = '';
                            if ( !empty($data_enabled) ) :
                                    foreach( $data_enabled as $enabled) :
                                            unset($data_set, $where_set);
                                            if ( isset($enabled['id_profile']) && is_numeric($enabled['id_profile']) ) :
                                                    $data_set               = array(
                                                            'is_enabled'    => '1',
                                                    );
                                                    $where_set              = array(
                                                            'id_profile'    => $enabled['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            'id_shop'       => $this->id_shop,
                                                    );
                                                    $enabled_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            endif;
                                    endforeach;
                            endif;
                            if ( !empty($data_disabled) ) :
                                    foreach( $data_disabled as $disabled) :
                                            unset($data_set, $where_set);
                                            if ( isset($disabled['id_profile']) && is_numeric($disabled['id_profile']) ) :
                                                    $data_set               = array(
                                                            'is_enabled'    => '0',
                                                    );
                                                    $where_set              = array(
                                                            'id_profile'    => $disabled['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            'id_shop'       => $this->id_shop,
                                                    );
                                                    $disabled_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            endif;
                                    endforeach;
                            endif;
                            if ( !empty($data_delete) ) :
                                    foreach( $data_delete as $delete) :
                                            unset($where_set, $where_set_group);
                                            if ( isset($delete['id_profile']) && is_numeric($delete['id_profile']) ) :
                                                    $where_set              = array(
                                                            'id_profile'    => $delete['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            'id_shop'       => $this->id_shop,
                                                    );
                                                    $delete_insert         .= $this->objectbiz->deleteColumnOnly('ebay_profiles', $where_set);

                                                    $where_set_group        = array(
                                                            'id_profile'    => $delete['id_profile'],
                                                    );
                                                    $delete_insert         .= $this->objectbiz->deleteColumnOnly('ebay_profiles_group', $where_set_group);
                                            endif;
                                    endforeach;
                            endif;
                            $result_insert                  = $this->objectbiz->transaction($enabled_insert.$disabled_insert.$create_insert.$delete_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Success";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_primary_finder() {
            if ($this->input->post()) :
                    $data                                       = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $finder) :
                                    $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/temp/'.$this->user['site'].'/';
                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ( file_exists($dir_temp.'categories.json') && ($handle = opendir($dir_temp)) ) :
                                            $ebay_category                          = json_decode(file_get_contents($dir_temp.'categories.json'), true);
                                            $finder_currect                         = array();
                                            if ( !empty($ebay_category) ) :
                                                    $html                           = '';
                                                    foreach ( $ebay_category as $ky => $category ) :
                                                            if (stripos($category['name'], $finder['key']) !== false) :
                                                                    $list               = explode(' -> ', $category['name']);
                                                                    $name               = $list[count($list) - 1];
                                                                    $assign             = array(
                                                                            'id'        => $category['id'],
                                                                            'name'      => $name,
                                                                            'root_name' => preg_replace("/\w*?".$finder['key']."\w*/i", "<b>$0</b>", $category['name']),
                                                                    );
                                                                    $this->assign($assign);
                                                                    $html           .= $this->smarty->fetch('ebay/categoryFinder.tpl');
                                                                    $finder_currect[] = $ebay_category[$ky];
                                                            endif;
                                                    endforeach;
                                                    if ( empty($html) ) :
                                                            $html = $this->lang->line('search_not_found')." : ".$finder['key'];
                                                    endif;
                                            endif;
                                    else :
                                            $html = $this->lang->line('search_not_found')." : ".$finder['key'];
                                    endif;
                                    echo $html;
                            endforeach;
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_primary_categories() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_primary_categories',
                    'ajax_url'                              => 'advance_primary_categories_save',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'choose-ebay-store-categories' => $this->lang->line('choose_ebay_store_categories'),  'select-a-option' => $this->lang->line('select_a_option')),
                    'data_tree'                             => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            unset($data['prev_page']);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function search_primary_categories() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'search_primary_categories',
                    'ajax_url'                              => '',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'choose-ebay-store-categories' => $this->lang->line('choose_ebay_store_categories'),  'select-a-option' => $this->lang->line('select_a_option')),
                    'info'                                  => 'open',
                    'data_tree'                             => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/configuration_search_primary_categories.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function search_store_categories() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'search_store_categories',
                    'ajax_url'                              => '',
                    'hidden_data'                           => array('choose-ebay-categories' => $this->lang->line('choose_ebay_categories'), 'choose-ebay-store-categories' => $this->lang->line('choose_ebay_store_categories'),  'select-a-option' => $this->lang->line('select_a_option')),
                    'info'                                  => 'open',
                    'data_tree'                             => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/configuration_search_primary_categories.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_configuration($p, $profile_id = '', $type = 'category') {
            if ( $profile_id == 'new' ) :
                    $profile_id                             = $this->objectbiz->getEbayProfile($this->user_name);
                    if ( !empty($profile_id) ) :
                            $profile_id                     = $profile_id + 1;
                    else :
                            $profile_id                     = 1;
                    endif;
            endif;
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_configuration($profile_id, $type);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_configuration',
                    'ajax_url'                              => 'advance_profile_configuration_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'wizard_open'                           => 'open',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }


    function get_profile_name_valid() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            foreach( $data as $profile) :
                                    $postal_valid                                    = $profile['profile_name'];
                                    $id_profile                                      = $profile['id_profile'];
                                    if ( !empty($postal_valid) ) :
                                            $valid_result                            = $this->objectbiz->getProfileNameValid($this->user_name, $postal_valid, $id_profile, $this->user['site'], $this->id_shop);
                                            echo ( $valid_result ) ? "Fail" : "Success";
                                    else :
                                            echo "Fail";
                                    endif;
                            endforeach;
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_configuration_save() { 
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert = '';
                            foreach( $data as $details) :
                                    unset($data_detail, $data_profile, $data_group);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( isset($details['id_profile']) && is_numeric($details['id_profile']) ) :
                                            if ( !empty($details['step']) ) :
                                                    $data_set               = array(
                                                            'profile_name'  => $details['profile_name'],
                                                            'completed'     => round((100 * $details['step']) / 9, 2),
                                                            'date_edit'      => date('Y-m-d H:i:s', strtotime('now')),
                                                    );
                                                    $where_set               = array(
                                                            'id_profile'    => $details['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            'id_shop'       => $this->id_shop,
                                                    );
                                            else :
                                                    $ebay_data                      = array(
                                                            'EBAY_LI_DURATION'      => $details['listing_duration'],
                                                            'EBAY_SITE_ID'          => $details['id_site'],
                                                            'API_TEMPLATE'          => $details['template'],
                                                            'EBAY_NO_IMAGE'         => $details['no_image'],
                                                            'EBAY_DISCOUNT'         => $details['discount'],
                                                            'EBAY_SYNCHRONIZE'      => $details['sync'],
                                                            'EBAY_JUST_ORDERS'      => $details['just_orders'],
                                                    );
                                                    $this->ebay_model->set_ebay_configuration($this->user_id, $ebay_data, $this->user['site']);
                                            endif;
                                            $data_detail                                = array(
                                                    'id_profile'                        => $details['id_profile'],
                                                    'id_site'                           => $this->user['site'],
                                                    'id_shop'                           => $this->id_shop,
                                                    'out_of_stock_min'                  => $details['out_of_stock_min'],
                                                    'maximum_quantity'                  => $details['maximum_quantity'],
                                                    'track_inventory'                   => $details['identifier_with_items'],
                                                    'ebay_category_name'                => $details['categories_name'],
                                                    'id_ebay_category '                 => isset($details['id_categories']) ? $details['id_categories'] : '',
                                                    'secondary_category_name'           => $details['secondary_categories_name'],
                                                    'id_secondary_category'             => $details['id_secondary_categories'],
                                                    'store_category_name'               => $details['store_categories_name'],
                                                    'id_store_category'                 => $details['id_store_categories'],
                                                    'secondary_store_category_name'     => $details['secondary_store_categories_name'],
                                                    'id_secondary_store_category'       => $details['id_secondary_store_categories'],
                                                    'country_name'                      => $details['country_name'],
                                                    'currency'                          => $details['currency'],
                                                    'title_format'                      => $details['title_format'],
                                                    'auto_pay'                          => $details['auto_pay'],
                                                    'gallery_plus'                      => $details['gallery_plus'],
                                                    'listing_duration'                  => $details['listing_duration'],
                                                    'visitor_counter'                   => $details['visitor_counter'],
                                            );
                                            if ( !empty($details['id_profile']) && $this->objectbiz->checkOrderCount("ebay_profiles", array("id_profile" => $details['id_profile'])) == 0 ) :
                                                    $data_profile                       = array(
                                                            'id_profile'                => $details['id_profile'],
                                                            'id_site'                   => $this->user['site'],
                                                            'id_shop'                   => $this->id_shop,
                                                            'ebay_user'                 => $this->user['userID'],
                                                            'profile_name'              => $details['profile_name'],
                                                            'completed'                 => 0,
                                                            'is_enabled'                => 1,
                                                            'date_add'                  => date('Y-m-d H:i:s', strtotime('now')),
                                                            'date_edit'                 => date('Y-m-d H:i:s', strtotime('now')),
                                                    );
                                                    $this->objectbiz->mappingTable('ebay_profiles', $data_profile);

                                                    $data_group                         = array(
                                                            'id_profile'                => $details['id_profile'],
                                                            'type'                      => $details['profile_type'],
                                                    );
                                                    $this->objectbiz->mappingTable('ebay_profiles', $data_profile);
                                                    $this->objectbiz->mappingTable('ebay_profiles_group', $data_group);
                                            endif;
                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_details', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_payment($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_payment($profile_id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_payment',
                    'ajax_url'                              => 'advance_profile_payment_save',
                    'info'                                  => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_payment_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert = '';
                            foreach ( $data as $payment ) :
                                    unset($data_detail);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( isset($payment['id_profile']) && is_numeric($payment['id_profile']) ) :
                                            if ( !empty($payment['step']) ) :
                                                    $data_set                       = array(
                                                            'completed'             => round((100 * $payment['step']) / 9, 2),
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $payment['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                    );
                                            endif;
                                            $data_detail                    = array(
                                                    'id_profile'            => $payment['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                                    'payment_method'        => isset($payment['payment_method']) ? base64_encode(serialize($payment['payment_method'])) : '',
                                                    'payment_instructions'  => $payment['payment_instructions'],
                                            );

                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_payment', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_return($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_return($profile_id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_return',
                    'ajax_url'                              => 'advance_profile_return_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_return_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert = '';
                            foreach ( $data as $return ) :
                                    unset($data_detail);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( isset($return['id_profile']) && is_numeric($return['id_profile']) ) :
                                            if ( !empty($return['step']) ) :
                                                    $data_set                       = array(
                                                            'completed'             => round((100 * $return['step']) / 9, 2),
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $return['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                    );
                                            endif;
                                            $data_detail                    = array(
                                                    'id_profile'            => $return['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                                    'returns_policy'        => $return['returns_policy'],
                                                    'returns_within'        => $return['returns_within'],
                                                    'returns_pays'          => $return['returns_pays'],
                                                    'returns_information'   => $this->html_special($return['returns_information']),
                                                    'holiday_return'        => $return['holiday_return'],
                                            );

                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_return', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_description($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $profile_data                                   = $this->objectbiz->getEbayProfileDescription($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $dir_name                                       = USERDATA_PATH.$this->user_name.'/template/';
            if ( file_exists($dir_name) && ($handle = opendir($dir_name)) ) :
                    while (( $file = readdir($handle)) !== false ) :
                            if ( !in_array($file, array('.', '..')) ) :
                                    $data['files'][]         = $file;
                            endif;
                    endwhile;
            endif;
//                echo $this->encrypt->decode($profile_data['html_description']);exit();
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            $hidden_data                                    = array(
                    'id_profile'                            => $profile_id,
                    'remaining'                             => $this->lang->line('remaining'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_description',
                    'ajax_url'                              => 'advance_profile_description_save',
                    'hidden_data'                           => $hidden_data,
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'data_decode'                           => !empty($profile_data['html_description']) ? htmlspecialchars_decode($profile_data['html_description']) : '',
                    'tinymce'                               => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,  
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_description_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert = '';
                            foreach ( $data as $description ) :
                                    unset($data_detail, $where_set);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( isset($description['id_profile']) && is_numeric($description['id_profile']) ) :
                                            if ( !empty($description['step']) ) :
                                                    $data_set                       = array(
                                                            'completed'             => round((100 * $description['step']) / 9, 2),
                                                    );
                                                    $where_set                      = array(
                                                            'id_profile'            => $description['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                    );
                                            endif;
                                            $data_detail                    = array(
                                                    'id_profile'            => $description['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                                    'description_selected'  => $description['description_selected'],
                                                    'html_description'      => htmlspecialchars($description['html_description'], ENT_COMPAT, 'UTF-8'),
                                            );

                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_description', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function change_keys_assoc($data = null) {
            if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                    array_map('Ebay::change_keys_assoc', $data);
            elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_keys, $data)) :
                    if ( !isset($this->change_data[$data[$this->change_keys]]) ) :
                            $this->change_data[$data[$this->change_keys]]   = $data;
                    endif;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function change_keys($data = null) {
            if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                    array_map('Ebay::change_keys', $data);
            elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_keys, $data)) :
                    if ( !isset($this->change_data[$data[$this->change_keys]]) ) :
                            $this->change_data[$data[$this->change_keys]]   = $data;
                    else :
                            if ( is_array($this->change_data[$data[$this->change_keys]]) && count(current($this->change_data[$data[$this->change_keys]])) > 1 ) :
                                    $this->change_data[$data[$this->change_keys]][]   = $data;
                            else :
                                    $store_data                                     = $this->change_data[$data[$this->change_keys]];
                                    unset($this->change_data[$data[$this->change_keys]]);
                                    $this->change_data[$data[$this->change_keys]][]   = $store_data;
                                    $this->change_data[$data[$this->change_keys]][]   = $data;
                            endif;
                    endif;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function change_group($data = null) {
            if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                    array_map('Ebay::change_group', $data);
            elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_keys, $data)) :
                    if ( is_array($this->change_wanted) ) :
                            unset($add_data);
                            foreach ( $this->change_wanted as $key => $wanted ) :
                                    if ( is_array($wanted) ) :
                                            foreach ( $wanted as $child_key => $child_wanted ) :
                                                    if ( is_array($child_wanted) ) :
//                                                                $add_data[$key]         = $data[$wanted];
                                                    else :
                                                            $val_wanted             = array_key_exists($key, $data) ? $data[$key] : $key;
                                                            $key_wanted             = array_key_exists($child_key, $data) ? $data[$child_key] : (!is_numeric($child_key) ? $child_key : $child_wanted);
//                                                                echo "<pre>", print_r($val_wanted, true), "</pre>";exit();
                                                            $this->change_data[$data[$this->change_keys]][$val_wanted][$key_wanted][]     = ($child_wanted != 'GROUPALL') ? $data[$child_wanted] : $data;
                                                    endif;
                                            endforeach;
                                    else :  
                                            $val_wanted             = array_key_exists($key, $data) ? $data[$key] : (!is_numeric($key) ? $key : $wanted);
                                            $this->change_data[$data[$this->change_keys]][$val_wanted][]     = ($wanted != 'GROUPALL') ? $data[$wanted] : $data;
                                    endif;
                            endforeach;
                    else :
                            $this->change_data[$data[$this->change_keys]][]     = $data[$this->change_wanted];
                    endif;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function additions_group($data = null) {
            if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                    array_map('Ebay::additions_group', $data);
            elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_keys, $data)) :
                    $this->change_data[$this->change_keys][]     = $data[$this->change_wanted];
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_key_assoc($key = null, $data = array()) {
            if ( empty($key) || empty($data) ) :
                    return $data;
            endif;
            $this->change_data                      = array();
            $this->change_keys                      = $key;
            array_map('Ebay::change_keys_assoc', $data);
            return $this->change_data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_key($key = null, $data = array()) {
            if ( empty($key) || empty($data) ) :
                    return $data;
            endif;
            $this->change_data                      = array();
            $this->change_keys                      = $key;
            array_map('Ebay::change_keys', $data);
            return $this->change_data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_group($key = null, $wanted = null, $data = array()) {
            if ( empty($key) || empty($wanted) || empty($data) ) :
                    return $data;
            endif;
            $this->change_data                      = array();
            $this->change_wanted                    = $wanted;
            $this->change_keys                      = $key;
            array_map('Ebay::change_group', $data);
            return $this->change_data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_additions_group($key = null, $wanted = null, $data = array()) {
            if ( empty($key) || empty($wanted) || empty($data) ) :
                    return $data;
            endif;
            $this->change_data                      = array();
            $this->change_wanted                    = $wanted;
            $this->change_keys                      = $key;
            array_map('Ebay::additions_group', $data);
            return $this->change_data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_condition($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_conditions();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_condition',
                    'ajax_url'                              => 'advance_profile_condition_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,  
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_condition_save() {
            if ($this->input->post()) :
                    $data                                   = $_POST['data'];
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert = '';
                            foreach ( $data as $condition ) :
                                    unset($data_detail, $data_mapping);
                                    $data_set = array();
                                    $where_set = array();
                                    if ( isset($condition['id_profile']) && is_numeric($condition['id_profile']) ) :
                                            $this->objectbiz->deleteColumn('ebay_mapping_condition', array('id_profile' => $condition['id_profile'], 'id_site' => $this->user['site'], 'id_shop' => $this->id_shop));
                                            if ( !empty($condition['step']) ) :
                                                    $data_set               = array(
                                                            'completed'     => round((100 * $condition['step']) / 9, 2),
                                                    );
                                                    $where_set              = array(
                                                            'id_profile'    => $condition['id_profile'],
                                                            'id_site'       => $this->user['site'],
                                                            'id_shop'       => $this->id_shop,
                                                    );

                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            $data_detail                    = array(
                                                    'id_profile'            => $condition['id_profile'],
                                                    'id_site'               => isset($this->user['site']) ? $this->user['site'] : '',
                                                    'id_shop'               => isset($this->id_shop) ? $this->id_shop : '',
                                                    'condition_name'        => isset($condition['condition_name']) ? $condition['condition_name'] : '',
                                                    'condition_value'       => isset($condition['condition_value']) ? $condition['condition_value'] : '',
                                                    'condition_description' => htmlspecialchars($condition['condition_description'], ENT_COMPAT, 'UTF-8'),
                                                    'is_enabled'            => isset($condition['tab']) && $condition['tab'] == 1 ? '0' : '1',
                                            );
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['data_mapping']) ) :
                                                    foreach ( $condition['data_mapping'] as $mapping ) :
                                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                            $data_mapping                    = array(
                                                                    'id_profile'            => $condition['id_profile'],
                                                                    'id_site'               => $this->user['site'],
                                                                    'id_shop'               => $this->id_shop,
                                                                    'condition_data'        => $mapping['name'],
                                                                    'condition_name'        => $mapping['mapping_name'],
                                                                    'condition_value'       => $mapping['mapping_value'],
                                                                    'is_enabled'            => isset($condition['tab']) && $condition['tab'] == 1 ? '1' : '0',
                                                            );
                                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition', $data_mapping);
                                                    endforeach;
                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['condition_description_new']) ) :
                                                    unset($data_mapping);
                                                    $data_mapping                    = array(
                                                            'id_profile'            => $condition['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'condition_data'        => 'new',
                                                            'condition_description' => htmlspecialchars($condition['condition_description_new'], ENT_COMPAT, 'UTF-8'),
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition_description', $data_mapping);
                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['condition_description_good']) ) :
                                                    unset($data_mapping);
                                                    $data_mapping                    = array(
                                                            'id_profile'            => $condition['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'condition_data'        => 'good',
                                                            'condition_description' => htmlspecialchars($condition['condition_description_good'], ENT_COMPAT, 'UTF-8'),
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition_description', $data_mapping);
                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['condition_description_used']) ) :
                                                    unset($data_mapping);
                                                    $data_mapping                    = array(
                                                            'id_profile'            => $condition['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'condition_data'        => 'used',
                                                            'condition_description' => htmlspecialchars($condition['condition_description_used'], ENT_COMPAT, 'UTF-8'),
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition_description', $data_mapping);
                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['condition_description_refurbished']) ) :
                                                    unset($data_mapping);
                                                    $data_mapping                    = array(
                                                            'id_profile'            => $condition['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'condition_data'        => 'refurbished',
                                                            'condition_description' => htmlspecialchars($condition['condition_description_refurbished'], ENT_COMPAT, 'UTF-8'),
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition_description', $data_mapping);
                                            endif;
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($condition['condition_description_acceptable']) ) :
                                                    unset($data_mapping);
                                                    $data_mapping                    = array(
                                                            'id_profile'            => $condition['id_profile'],
                                                            'id_site'               => $this->user['site'],
                                                            'id_shop'               => $this->id_shop,
                                                            'condition_data'        => 'acceptable',
                                                            'condition_description' => htmlspecialchars($condition['condition_description_acceptable'], ENT_COMPAT, 'UTF-8'),
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_mapping_condition_description', $data_mapping);
                                            endif;

                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_condition', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_shipping($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data                                           = $this->prepare_carriers($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_shipping',
                    'ajax_url'                              => 'mapping_carriers_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_variation($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            $this->prepare_attributes($profile_id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_variation',
                    'ajax_url'                              => 'attribute_ebay_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'popup_hide'                            => 'open',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function html_special($var) {
            if (is_array($var)) :
                return array_map('Ebay::html_special', $var);
            else :
                return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
            endif;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function html_special_decode($var) {
            if (is_array($var)) :
                return array_map('Ebay::html_special_decode', $var);
            else :
                return htmlspecialchars_decode($var, ENT_QUOTES);
            endif;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_select_load() {
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $profile_id                                     = $this->input->get('id_profile');
            $query                                          = $this->input->get('q');
            $type                                           = $this->input->get('type');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $profile_configuration                          = $this->objectbiz->getEbayProfileConfiguration($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $attribute_group_value                          = $this->ebay_model->get_specific_processing($this->user['site'], $query, $type);
            $attribute_group_value                          = $this->html_special_decode($attribute_group_value);
            echo json_encode(array('results' => $attribute_group_value));
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function advance_profile_specific($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $tab_data                                       = array();
            $type                                           = $this->objectbiz->getEbayProfileType($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $profile_configuration                          = $this->objectbiz->getEbayProfileConfiguration($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $tab_data['type']                               = $type;
            $without                                        = $this->objectbiz->getSpacificWithout($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $hidden_data                                    = array('id_profile' => $profile_id);
            $attribute_group                                = $this->ebay_model->get_specific_group($profile_configuration['id_ebay_category'], $this->user['site'], $without);
            $attribute_group_value                          = $this->ebay_model->get_specific_value($profile_configuration['id_ebay_category'], $this->user['site'], $without);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $profile_data                                   = $this->objectbiz->getEbayProfileSpecific($this->user_name, $this->user['site'], $this->id_shop, $profile_id, $attribute_group);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( (!empty($profile_data) && $profile_data[0]['is_enabled'] == '1') || (!empty($tab_data['type']) && $tab_data['type'] == 'product') ) : $tab_data['tab'] = 2; else : $tab_data['tab'] = 1; endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            if ( !empty($attribute_group) ) :
                    $attribute_group                        = $this->generate_key('id_group', $attribute_group);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    $profile_group                          = $this->generate_key('name_attribute', $profile_data);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($attribute_group_value) ) :
                    $attribute_group_value                  = $this->generate_group('id_group', 'value', $attribute_group_value);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($profile_data) ) :
                    foreach ( $profile_data as $mapping ) :
                            if ( !empty($attribute_group[$mapping['id_group']]) ) :
                                    $attribute_group[$mapping['id_group']]['selected'][$mapping['value_attribute']]    = $mapping['mode'];
                            endif;
                    endforeach;
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $validation                                     = array(
                    "validate-specific-ebay"                => array(
                            'required'                      => $this->lang->line('validation_select_required'),
                            'include'                       => $this->lang->line('validation_specific_include'),
                    ),
                    "validate-attribute-ebay"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                    ),
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_specific',
                    'ajax_url'                              => 'advance_profile_specific_save',
                    'validation_data'                       => $validation,
                    'hidden_data'                           => array('id_profile' => $profile_id, 'form-template-checkbox' => 1, 'choose-specific-value' => $this->lang->line('choose_specific_value'), 'are-you-sure-insert' => $this->lang->line('are_you_sure_insert')),
                    'attribute_group'                       => !empty($attribute_group) ? $attribute_group : '',
                    'attribute_group_value'                 => isset($attribute_group_value) ? $attribute_group_value : '',
                    'tab_data'                              => isset($tab_data) ? $tab_data : '',
                    'data_tree'                             => 'open',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'popup_hide'                            => 'open',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
                    'profile_group'                         => !empty($profile_group) ? $profile_group : '',
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_specific_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert                                 = '';
                            $id_profile                                     = current($data);
                            $this->objectbiz->deleteColumn('ebay_profiles_specific', array('id_profile' => $id_profile['id_profile']));
                            foreach ( $data as $fixed ) :
                                    unset($data_set, $where_set, $data_detail, $data_mapping);
                                    if ( isset($fixed['id_profile']) && is_numeric($fixed['id_profile']) ) :
                                            $data_set                       = array(
                                                    'completed'             => round((100 * $fixed['step']) / 9, 2),
                                            );
                                            $where_set                      = array(
                                                    'id_profile'            => $fixed['id_profile'],
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                            );
                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( isset($fixed['id_group']) && is_numeric($fixed['id_group']) ) :
                                                    $data_detail                    = array(
                                                            'id_profile'            => $fixed['id_profile'],
                                                            'id_group'              => isset($fixed['id_group']) ? $fixed['id_group'] : '',
                                                            'name_attribute'        => isset($fixed['name_attribute']) ? htmlspecialchars($fixed['name_attribute']) : '',
                                                            'value_attribute'       => isset($fixed['name_attribute_ebay']) ? htmlspecialchars($fixed['name_attribute_ebay']) : '',
                                                            'mode'                  => $fixed['mode'],
                                                            'is_enabled'            => isset($fixed['tab']) && $fixed['tab'] == 1 ? '1' : '0',
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_specific', $data_detail);
                                            endif;
                                            $details_insert         .= $this->objectbiz->updateColumnOnly('ebay_profiles', $data_set, $where_set);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    function prepare_price_modifier($profile_id = 0) {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $currentCurrency                                = $this->objectbiz->getCurrentCurrency($this->id_shop);                
            $this->pattern_code                             = $this->config->item('currency_code');       
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $price_modifier                                 = $this->objectbiz->getPriceModifier($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $price_rounding                                 = $this->objectbiz->getRoundingModifier($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $rel_rounding                                   = $this->objectbiz->getPriceModifierRelMax($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $validation                                     = array(
                    "validate-price-modifier"               => array(
                            'required'                      => $this->lang->line('validation_error_required'),
                            'range'                         => $this->lang->line('validation_range_required'),
                            'between'                       => $this->lang->line('validation_between_required'),
                                    'positive'                		=> $this->lang->line('validation_positive_required'),
                    ),
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'price_modifier',
                    'ajax_url'                              => 'price_modifier_save',
                    'currency_symbol'                       => !empty($this->pattern_code[$currentCurrency['iso_code']]) ? $this->pattern_code[$currentCurrency['iso_code']] : '',
                    'price_modifier'                        => !empty($price_modifier) ? $price_modifier : '',
                    'round_selected'                        => !empty($price_rounding) ? $price_rounding : '',
                    'currency_format'                       => 'open',
                    'validation_data'                       => $validation,
                    'rel_round'                             => $rel_rounding,
                    'hidden_data'                           => array('id_profile' => $profile_id, 'currency-symbol' => !empty($this->pattern_code[$currentCurrency['iso_code']]) ? $this->pattern_code[$currentCurrency['iso_code']] : ''),
            );
            $this->assign($assign);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_price_modifier($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            $this->prepare_price_modifier($profile_id);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = array_search($this->uri->segment(2), array_keys($data['next'])) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_price_modifier',
                    'ajax_url'                              => 'price_modifier_save',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data); 
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function advance_profile_finish($p, $profile_id = '') {
            $this->id_profile                               = $profile_id;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->popup                                    = count($data['next']) + 1;
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_finish',
                    'ajax_url'                              => '',
                    'popup'                                 => isset($this->popup) ? $this->popup : 1,
                    'hidden_data'                           => array('id_profile' => $profile_id),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->assign($assign);
            $this->smarty->view('ebay/main_profile.tpl', $data); 
    }

    function advance_profile_categories_mapping() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_advance_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $statistics_data                                = $this->setJsonCategoryTree();
            $statistics_data                                = $this->setJsonCategoryTreeUp();
            $statistics_data                                = $this->setJsonCategoryTreeDown();
            $profile_completed                              = $this->objectbiz->getEbayProfileCompleted($this->user_name, $this->user['site'], $this->id_shop);
            $this->pattern_code                             = $this->config->item('currency_code');
            $hidden_data                                    = array(
                    'chack-all-lang'                        => $this->lang->line('chack_all'),
                    'product-image-lang'                    => $this->lang->line('product_image'),
                    'product-item-lang'                     => $this->lang->line('product_item'),
                    'product-content-lang'                  => $this->lang->line('product_content'),
                    'product-information-lang'              => $this->lang->line('product_information'),
                    'product-detail-lang'                   => $this->lang->line('product_detail'),
                    'product-active-lang'                   => $this->lang->line('active'),
                    'product-manufacturer'                  => $this->lang->line('product_manufacturer'),
                    'product-title'                         => $this->lang->line('product_title'),
                    'product-supplier'                      => $this->lang->line('product_supplier'),
                    'product-quantity'                      => $this->lang->line('product_quantity'),
                    'product-price'                         => $this->lang->line('product_price'),
                    'product-reference'                     => $this->lang->line('product_reference'),
                    'product-sku'                           => $this->lang->line('product_sku'),
                    'product-ean13'                         => $this->lang->line('product_ean13'),
                    'product-upc'                           => $this->lang->line('product_upc'),
                    'category-name'                         => $this->lang->line('category_name'),
                    'condition-name'                        => $this->lang->line('condition_name'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'category-mapping'                      => $this->lang->line('advance_profile_categories_mapping'),
                    'show-all'                              => $this->lang->line('show_all'),
                    'product-profile'                       => $this->lang->line('profile_name'),
                    'are-you-sure'                          => $this->lang->line('are_you_sure_replace_profile'),
                    'replace-profile'                       => $this->lang->line('replace_profile'),
                    'confirm-all'                           => $this->lang->line('confirm_all'),
                    'skip-all'                              => $this->lang->line('skip_all'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'advance_profile_categories_mapping',
                    'ajax_url'                              => 'advance_profile_categories_mapping_save',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'statistics'                            => !empty($statistics_data) ? $statistics_data : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
                    'profile_completed'                     => !empty($profile_completed) ? $profile_completed : '',
                    'currency_code'                         => json_encode($this->pattern_code),
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function advance_profile_categories_mapping_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert                                 = '';
                            foreach ( $data as $fixed ) :
                                    unset($data_set, $data_detail);
                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ( isset($fixed['id_profile']) && $fixed['id_profile'] == 'clear' ) :
                                            $this->objectbiz->deleteColumn('ebay_profiles_mapping', array('id' => $fixed['id_category']));
                                            $result_insert                  = 1;
                                            echo "Success";
                                            return;
                                    elseif ( isset($fixed['id_category']) && is_numeric($fixed['id_category']) ) :
                                            $data_detail                    = array(
                                                    'id_profile'            => isset($fixed['id_profile']) ? $fixed['id_profile'] : '',
                                                    'id_site'               => $this->user['site'],
                                                    'id_shop'               => $this->id_shop,
                                                    'id'                    => isset($fixed['id_category']) ? $fixed['id_category'] : '',
                                                    'selected_name'         => isset($fixed['category_name']) ? htmlspecialchars($fixed['category_name']) : '',
                                                    'type'                  => 'category',
                                                    'is_enabled'            => isset($fixed['is_enabled']) && $fixed['is_enabled'] == 1 ? '1' : '0',
                                            );
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_profiles_mapping', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonCategoryTree() {
            $html = '';                   
            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/catalogs/full/';
            $time                                           = $this->objectbiz->getCategoryMaxDate($this->user_name, $this->id_shop);
            if ( file_exists($dir_temp) && !file_exists($dir_temp.$time['date'].'.json') && ($handle = opendir($dir_temp)) ) :
                     $this->dir_remove($dir_temp);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( file_exists($dir_temp.$time['date'].'.json') && file_exists($dir_temp.'catalogs.json') && ($handle = opendir($dir_temp)) ) :
                    $html                                   = json_decode(file_get_contents($dir_temp.'catalogs.json'), true);
            else :
                    $root = $this->objectbiz->getFeedbizRootCategory($this->user_name, $this->id_shop, $this->id_lang);
                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategories($this->user_name, $this->id_shop, $this->id_lang, $root['id_category']);
                    if ( !empty($arr_selected_categories) ) :
                            foreach ($arr_selected_categories as $asc) :
                                    $html[$asc['id_category']]['id_category']   = $asc['id_category'];                                                                      
                                    $html[$asc['id_category']]['name']          = $asc['category_name'];     
                                    $html[$asc['id_category']]['id_parent']     = $asc['id_parent'];                   
                                    $html[$asc['id_category']]['children']      = $this->setJsonCategorySubTree($asc['id_category']);               
                            endforeach;
                    endif;

                    $response['posts']                      = json_encode($html);
                    if ( !file_exists($dir_temp) ) {
                            $old = umask(0);
                            mkdir($dir_temp, 0777, true);
                            umask($old);
                    }
                    file_put_contents($dir_temp.'catalogs.json', $response['posts']);
                    file_put_contents($dir_temp.$time['date'].'.json', '');
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function setJsonCategorySubTree($root_cat_id = null){
            $html = array();
            if ( isset($root_cat_id) && is_numeric($root_cat_id) ) :
                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategories($this->user_name, $this->id_shop, $this->id_lang, $root_cat_id);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( !empty($arr_selected_categories) ) :
                                    foreach ($arr_selected_categories as $asc) {
                                            $html[$asc['id_category']]['id_category']   = $asc['id_category'];                                                                      
                                            $html[$asc['id_category']]['name']          = $asc['category_name'];  
                                            $html[$asc['id_category']]['id_parent']     = $asc['id_parent'];                   
                                            $html[$asc['id_category']]['children']      = $this->setJsonCategorySubTree($asc['id_category']);                                                                                                                                                
                                    }
                            endif;
                    return $html;
            endif;
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonCategoryTreeUp() {
            $html = array();             
            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/catalogs/up/';
            $time                                           = $this->objectbiz->getCategoryMaxDate($this->user_name, $this->id_shop);
            if ( file_exists($dir_temp) && !file_exists($dir_temp.$time['date'].'.json') && ($handle = opendir($dir_temp)) ) :
                     $this->dir_remove($dir_temp);
            endif;
            $allCategories                                  = $this->objectbiz->getFeedbizAllCategories($this->user_name, $this->id_shop, $this->id_lang);
            if( !empty($allCategories) ) :
                    foreach ( $allCategories as $currentCategory ) :
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( file_exists($dir_temp.$time['date'].'.json') && file_exists($dir_temp.$currentCategory['id_category'].'.json') && ($handle = opendir($dir_temp)) ) :
                                    $html                                   = json_decode(file_get_contents($dir_temp.$currentCategory['id_category'].'.json'), true);
                            else :
                                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategoriesUp($this->user_name, $this->id_shop, $this->id_lang, $currentCategory['id_parent']);
                                    if ( !empty($arr_selected_categories) ) :
                                            $html[$currentCategory['id_category']]['id_category']   = $currentCategory['id_category'];
                                            $html[$currentCategory['id_category']]['name']          = $currentCategory['category_name'];
                                            foreach ($arr_selected_categories as $asc) :
                                                    $html[$currentCategory['id_category']]['id_parent'][$currentCategory['id_parent']]['id_category']    = $asc['id_category'];                                                                      
                                                    $html[$currentCategory['id_category']]['id_parent'][$currentCategory['id_parent']]['name']           = $asc['category_name'];                                                                                                                                                             
                                                    $html[$currentCategory['id_category']]['id_parent'][$currentCategory['id_parent']]['id_parent']      = $this->setJsonCategorySubTreeUp($asc['id_parent']);               
                                            endforeach;
                                    endif;
                                    $response['posts']                      = json_encode($html);
                                    if ( !file_exists($dir_temp) ) {
                                            $old = umask(0);
                                            mkdir($dir_temp, 0777, true);
                                            umask($old);
                                    }
                                    file_put_contents($dir_temp.$currentCategory['id_category'].'.json', $response['posts']);
                            endif;
                            $html = array();
                    endforeach;
                    file_put_contents($dir_temp.$time['date'].'.json', '');
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function setJsonCategorySubTreeUp($root_cat_id = null){
            $sub_html = array();
            if ( isset($root_cat_id) && is_numeric($root_cat_id) ) :
                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategoriesUp($this->user_name, $this->id_shop, $this->id_lang, $root_cat_id);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( !empty($arr_selected_categories) ) :
                                    foreach ($arr_selected_categories as $asc) {
                                            $sub_html[$root_cat_id]['id_category']      = $asc['id_category'];                                                                      
                                            $sub_html[$root_cat_id]['name']             = $asc['category_name'];              
                                            $sub_html[$root_cat_id]['id_parent']        = $this->setJsonCategorySubTreeUp($asc['id_parent']);                                                                                                                                                
                                    }
                            endif;
                    return $sub_html;
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function setJsonCategoryTreeDown() {
            $html = array();             
            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/catalogs/down/';
            $time                                           = $this->objectbiz->getCategoryMaxDate($this->user_name, $this->id_shop);
            if ( file_exists($dir_temp) && !file_exists($dir_temp.$time['date'].'.json') && ($handle = opendir($dir_temp)) ) :
                     $this->dir_remove($dir_temp);
            endif;
            $allCategories                                  = $this->objectbiz->getFeedbizAllCategories($this->user_name, $this->id_shop, $this->id_lang);
            if( !empty($allCategories) ) :
                    foreach ( $allCategories as $currentCategory ) :
                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( file_exists($dir_temp.$time['date'].'.json') && file_exists($dir_temp.$currentCategory['id_category'].'.json') && ($handle = opendir($dir_temp)) ) :
                                    $html                                   = json_decode(file_get_contents($dir_temp.$currentCategory['id_category'].'.json'), true);
                            else :
                                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategoriesDown($this->user_name, $this->id_shop, $this->id_lang, $currentCategory['id_category']);
                                    if ( !empty($arr_selected_categories) ) :
                                            $html[$currentCategory['id_category']]['id_category']   = $currentCategory['id_category'];
                                            $html[$currentCategory['id_category']]['name']          = $currentCategory['category_name'];
                                            $html[$currentCategory['id_category']]['id_parent']     = $currentCategory['id_parent'];
                                            foreach ($arr_selected_categories as $asc) :
                                                    $html[$currentCategory['id_category']]['children'][$asc['id_category']]['id_category']      = $asc['id_category'];                                                                      
                                                    $html[$currentCategory['id_category']]['children'][$asc['id_category']]['name']             = $asc['category_name'];                                                                                                                                                             
                                                    $html[$currentCategory['id_category']]['children'][$asc['id_category']]['id_parent']        = $asc['id_parent'];               
                                                    $html[$currentCategory['id_category']]['children'][$asc['id_category']]['children']         = $this->setJsonCategorySubTreeDown($asc['id_category']);               
                                            endforeach;
                                    endif;
                                    $response['posts']                      = json_encode($html);
                                    if ( !file_exists($dir_temp) ) {
                                            $old = umask(0);
                                            mkdir($dir_temp, 0777, true);
                                            umask($old);
                                    }
                                    file_put_contents($dir_temp.$currentCategory['id_category'].'.json', $response['posts']);
                            endif;
                            $html = array();
                    endforeach;
                    file_put_contents($dir_temp.$time['date'].'.json', '');
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private function setJsonCategorySubTreeDown($root_cat_id = null){
            $sub_html = array();
            if ( isset($root_cat_id) && is_numeric($root_cat_id) ) :
                    $arr_selected_categories = $this->objectbiz->getFeedbizSelectedCategoriesDown($this->user_name, $this->id_shop, $this->id_lang, $root_cat_id);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ( !empty($arr_selected_categories) ) :
                                    foreach ($arr_selected_categories as $asc) {
                                            $sub_html[$asc['id_category']]['id_category']      = $asc['id_category'];                                                                      
                                            $sub_html[$asc['id_category']]['name']             = $asc['category_name'];              
                                            $sub_html[$asc['id_category']]['id_parent']        = $asc['id_parent'];                                                                                                                                                
                                            $sub_html[$asc['id_category']]['children']         = $this->setJsonCategorySubTreeDown($asc['id_category']);                                                                                                                                                
                                    }
                            endif;
                    return $sub_html;
            endif;
    }

    function find_parents($parent_array = null, $id_category = null, $string = '') {
            if ( empty($parent_array) || (!isset($id_category) && empty($id_category)) ) :
                    return $string;
            endif;
            foreach ( $parent_array as $parent ) :
                    if ( empty($string) ) :
                            $string     = $parent['name'];
                    else :
                            $string     = $parent['name']." > ".$string;
                    endif;
                    $string     = $this->find_parents($parent['id_parent'], $parent['id_category'], $string);
            endforeach;
            return $string;
    }

    function delpt_array_setting($data = array(), $string = '') {
            if ( !empty($data['children']) && is_array($data['children']) ) :
                    if ( isset($this->change_delpt) && $this->change_delpt == 0 ) :
                            foreach ( $this->change_wanted as $key_wanted => $wanted) :
                                    if ( $wanted == 'find_category' ) :
                                            $dir_temp                       = $this->dir_server. '/assets/apps/ebay/catalogs/up/';
                                            $html                           = json_decode(file_get_contents($dir_temp.$data['id_category'].'.json'), true);
                                            $string                         = $this->find_parents($data['id_category']);
                                            $this->change_data['find_category'][]   = $string;
                                    else :
                                            $this->change_data[$wanted][]           = $data[$wanted];
                                    endif;
                            endforeach;
                    endif;
                    array_map('Ebay::delpt_array_setting', $data);
            elseif ( is_array($data) && isset($data[$this->change_keys]) ) :
                    foreach ( $this->change_wanted as $key_wanted => $wanted) :
                            if ( $wanted == 'find_category' ) : 
                                    $dir_temp                               = $this->dir_server. '/assets/apps/ebay/catalogs/up/';
                                    $html                                   = json_decode(file_get_contents($dir_temp.$data['id_category'].'.json'), true);
                                    $string                                 = $this->find_parents($html, $data['id_category'], $string);
                                    $this->change_data['find_category'][]   = $string;
                            else :  
                                    $this->change_data[$wanted][]           = $data[$wanted];
                            endif;
                    endforeach;
            elseif ( is_array($data) ) :
                    array_map('Ebay::delpt_array_setting', $data);
            endif;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_delpt_wanted($wanted = null, $key = null, $delpt = null, $data = array()) {
            if ( !isset($key) || empty($wanted) || empty($data) ) :
                    return $data;
            endif;
            $this->change_data                      = array();
            $this->change_wanted                    = $wanted;
            $this->change_keys                      = $key;
            $this->change_delpt                     = $delpt;
            array_map('Ebay::delpt_array_setting', $data);
            return $this->change_data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function profile_categories_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $id_product                                     = $this->input->get('id_product');
            $product_content                                = $this->input->get('product_content');
            $product_information                            = $this->input->get('product_information');
            $product_detail                                 = $this->input->get('product_detail');
            $active                                         = $this->input->get('active');
            $profile_name                                   = $this->input->get('profile_name');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $all                                            = $this->input->get('all');

            $dir_temp                                       = $this->dir_server. '/assets/apps/ebay/catalogs/up/';
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $category_delpt = array();
            if ( (isset($id_product) && is_numeric($id_product)) || (isset($product_content) && !empty($product_content)) || (isset($product_information) && !empty($product_information)) || (isset($product_detail) && !empty($product_detail)) || (isset($active) && is_numeric($active) || !empty($active)) || (isset($all) && !empty($all))  ) :
            else :  $id_category    = array();
                    if ( !empty($profile_name) ) :
                            $id_category['profile_name']                    = $profile_name;
                    endif;
                    $data['statistics_category']            = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $limit, $offset, $id_category);
                    $data['statistics_category_count']      = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, 0, 0, $id_category, 1);
                    $category_delpt                         = $this->generate_delpt_wanted(array('id_category', 'find_category'), 'id_category', 0, $data['statistics_category']);
            endif; 
            if ( !empty($category_delpt['find_category']) ) :
                    foreach ( $category_delpt['find_category'] as $key => $delpt ) :
                            $data['statistics_category'][$key]['reference'] = $delpt;
                    endforeach;
            endif;
            if ( !empty($data['statistics_category']) ) :
                    $data['statistics_category']            = $this->generate_key('id_category', $data['statistics_category']);
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_attribute']                   = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                    'product_content'                       => !empty($all) ? $all : array('name' => htmlspecialchars($product_content)),
                    'product_information'                   => !empty($all) ? $all : array('name' => htmlspecialchars($product_information)),
                    'product_detail'                        => !empty($all) ? $all : array('name' => htmlspecialchars($product_detail)),
                    'id_category'                           => !empty($all) ? $all : (!empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product))),
            ), $limit, $offset, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( empty($data['statistics_category']) && !empty($data['statistics_attribute']) ) :
                    foreach ( $data['statistics_attribute'] as $attributes ) :
                            $attribute['id_product'][$attributes['id_product']] = $attributes['id_product'];
                    endforeach;

                    $data['statistics_attribute']           = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                            'id_product'                    => (!empty($attribute) ? $attribute : array('id_product' => array(0))),
                    ), 0, 0, "*", null, null, 'AND');
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_attribute_offer']             = $this->objectbiz->getAdvanceMappingProductAttributeOfferByParam($this->user_name, $this->id_shop, array(
                    'id_category'                           => !empty($all) ? $all : (!empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product))),
            ), $limit, $offset, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( !empty($data['statistics_attribute']) ) :
                    $data['statistics_attribute']           = $this->generate_group('id_product', array('id_product_attribute' => 'GROUPALL'), $data['statistics_attribute']);
            endif;

            if ( !empty($data['statistics_attribute_offer']) ) :
                    foreach ( $data['statistics_attribute_offer'] as $statistics_attribute_offer ) :
                            if ( !empty($data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']]) ) :
                                        foreach ( $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']] as $attribute_offer_key => $product_offer ) :
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['reference']    = $statistics_attribute_offer['reference'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['sku']          = $statistics_attribute_offer['sku'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['ean13']        = $statistics_attribute_offer['ean13'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['upc']          = $statistics_attribute_offer['upc'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['price']        = $statistics_attribute_offer['price'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['quantity']     = $statistics_attribute_offer['quantity'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['price_type']   = $statistics_attribute_offer['price_type'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['date_add']     = $statistics_attribute_offer['date_add'];
                                        endforeach;
                            endif;
                    endforeach;
            endif;
            $product_delpt                      = $this->generate_delpt_wanted(array('id_product'), 'id_product', 0, $data['statistics_attribute']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getAdvanceMappingCategoriesByParam($this->id_shop, $this->id_lang, array(
                    'id_category_default'                   => !empty($all) ? $all : $id_product,
                    'id_product'                            => !empty($product_delpt) ? $product_delpt : array('id_product' => array($id_product)),
                    'product_content'                       => !empty($all) ? $all : array('name' => htmlspecialchars($product_content)),
                    'product_information'                   => !empty($all) ? $all : array('name' => htmlspecialchars($product_information)),
                    'product_detail'                        => !empty($all) ? $all : array('name' => htmlspecialchars($product_detail)),
                    'id_category'                           => !empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product)),
                    'active'                                => !empty($all) ? $all : $active,
            ), $limit, $offset, array('p.id_product AS id_product'
                , 'p.id_category_default AS id_category_default'
                , 'p.reference AS reference'
                , 'p.sku AS sku'
                , 'p.ean13 AS ean13'
                , 'p.upc AS upc'
                , 'p.quantity AS quantity'
                , 'p.price AS price'
                , 'p.active AS active'
                , 'p.date_add AS date_add'
                , 'p.date_upd AS date_upd'
                , 'c.name AS condition_name'
                , 'cr.iso_code AS iso_code'
                , 'i.image_url AS image_url'
                , 'l.name AS category_name'
                , 'm.name AS manufacturer_name'
                , 's.name AS supplier_name'
                , 'pl.name AS product_name'
                )
                , $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( empty($data['statistics_category']) && empty($data['statistics_attribute']) && !empty($data['statistics']) ) :
                    unset($attribute);
                    foreach ( $data['statistics'] as $attributes ) :
                            $attribute['id_product'][$attributes['id_product']] = $attributes['id_product'];
                    endforeach;

                    $data['statistics_attribute']           = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                            'id_product'                    => (!empty($attribute) ? $attribute : array('id_product' => array(0))),
                    ), 0, 0, "*", null, null, 'AND');

                    if ( !empty($data['statistics_attribute']) ) :
                            $data['statistics_attribute']           = $this->generate_group('id_product', array('id_product_attribute' => 'GROUPALL'), $data['statistics_attribute']);
                    endif;
            endif;

            if ( !empty($data['statistics']) ) :
                    $data['statistics']                     = $this->generate_key('id_product', $data['statistics']);
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_offer']                       = $this->objectbiz->getAdvanceMappingCategoriesOfferByParam($this->user_name, $this->id_shop, $this->id_lang, array(
                    'id_category_default'                   => !empty($all) ? $all : $id_product,
                    'id_product'                            => !empty($all) ? $all : $id_product,
                    'id_category'                           => !empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product)),
            ), $limit, $offset, array('p.id_product AS id_product'
                , 'p.quantity AS quantity'
                , 'p.price AS price'
                , 'p.date_upd AS date_upd'), $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( !empty($data['statistics_offer']) ) :
                    foreach ( $data['statistics_offer'] as $statistics_offer ) :
                            if ( !empty($data['statistics'][$statistics_offer['id_product']]) ) :
                                        $data['statistics'][$statistics_offer['id_product']]['quantity']    = "{B}".$data['statistics'][$statistics_offer['id_product']]['quantity']."{/B} ".$this->lang->line('to')." ".$statistics_offer['quantity'];
                                        $data['statistics'][$statistics_offer['id_product']]['price']       = "{B}".$data['statistics'][$statistics_offer['id_product']]['price']."{/B} ".$this->lang->line('to')." ".$statistics_offer['price'];
                                        $data['statistics'][$statistics_offer['id_product']]['date_upd']    = "{B}".$data['statistics'][$statistics_offer['id_product']]['date_upd']."{/B} ".$this->lang->line('to')." ".$statistics_offer['date_upd'];
                            endif;
                    endforeach;
            endif;
//                echo "<pre>", print_r($data['statistics_offer'], true), "</pre>";exit();
//                echo "<pre>", print_r($data['statistics_category'], true), "</pre>";exit();
//                echo "<pre>", print_r($data['statistics'], true), "</pre>";exit();

            if ( empty($data['statistics_category']) && !empty($data['statistics']) ) :
                    $data['statistics_category_f'] = array();
                    if ( !empty($profile_name) ) :
                            $id_category['profile_name']                    = $profile_name;
                    endif;
                    foreach ( $data['statistics'] as $category ) :
                            $id_category['id_category'][]                   = $category['id_category_default'];
                            if ( file_exists($dir_temp.$category['id_category_default'].'.json') ) :
                                    $html                                   = json_decode(file_get_contents($dir_temp.$category['id_category_default'].'.json'), true);
                                    $category_delpt                         = $this->generate_delpt_wanted(array('id_category', 'find_category'), 'id_category', 0, $html);
                                    $id_category['find_category'][$category['id_category_default']] = $category_delpt['find_category'][0];
                            endif;  
                    endforeach;
                    $data['statistics_category']                    = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $limit, $offset, $id_category, null, (!empty($product_content) ? array('key' => (!empty($product_information) || !empty($product_detail) || !empty($id_product) ? 'AND' : 'OR'), 'value' => $product_content) : (!empty($all) ? array('key' =>'OR', 'value' => $all) : null)));
                    if ( !empty($data['statistics_category']) ) :
                            $data['statistics_category']            = $this->generate_key('id_category', $data['statistics_category']);
                    endif;                        
//                        echo "<pre>", print_r($id_category, true), "</pre>";exit();

                    if ( !empty($data['statistics_category']) ) :
                            foreach ( $data['statistics_category'] as $key => $delpt ) :
                                    if ( !empty($id_category['find_category'][$key]) ) :
                                            $data['statistics_category'][$key]['reference'] = $id_category['find_category'][$key];
                                    else :  unset($category_delpt);
                                            if ( file_exists($dir_temp.$key.'.json') ) :
                                                    $html                                   = json_decode(file_get_contents($dir_temp.$key.'.json'), true);
                                                    $category_delpt                         = $this->generate_delpt_wanted(array('id_category', 'find_category'), 'id_category', 0, $html);
                                                    if ( !empty($category_delpt['find_category'][0]) ) :
                                                            $data['statistics_category'][$key]['reference']     = $category_delpt['find_category'][0];
                                                    endif;
                                            endif;  
                                    endif;
                            endforeach;
                    endif;
                    $data['statistics_category_count']              = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, 0, 0, $id_category, 1, (!empty($product_content) ? array('key' => (!empty($product_information) || !empty($product_detail) || !empty($id_product) ? 'AND' : 'OR'), 'value' => $product_content) : (!empty($all) ? array('key' =>'OR', 'value' => $all) : null)));
            elseif ( empty($data['statistics_category']) && empty($data['statistics']) ) :
                    $id_category                                    = array();
                    if ( !empty($profile_name) ) :
                            $id_category['profile_name']                    = $profile_name;
                    endif;
                    $data['statistics_category']                    = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, $limit, $offset, null, null, (!empty($product_content) ? array('key' =>'AND', 'value' => $product_content) : (!empty($all) ? array('key' =>'AND', 'value' => $all) : null)));
                    if ( !empty($data['statistics_category']) ) :
                            foreach ( $data['statistics_category'] as $key_statistic => $statistic_id ) :
                                    $id                                     = $data['statistics_category'][$key_statistic]['id_category'];
                                    $id_category['id_category'][]                   = $data['statistics_category'][$key_statistic]['id_category'];
                                    if ( file_exists($dir_temp.$id.'.json') ) :
                                            $html                                   = json_decode(file_get_contents($dir_temp.$id.'.json'), true);
                                            $category_delpt                         = $this->generate_delpt_wanted(array('id_category', 'find_category'), 'id_category', 0, $html);
                                            if ( !empty($category_delpt['find_category'][0]) ) :
                                                    $data['statistics_category'][$key_statistic]['reference']     = $category_delpt['find_category'][0];
                                            endif;
                                    endif;  
                            endforeach;
                    endif;
                    if ( !empty($data['statistics_category']) ) :
                            $data['statistics_category']            = $this->generate_key('id_category', $data['statistics_category']);
                    endif;    
                    $data['statistics_category_count']              = $this->objectbiz->getAdvanceMappingProductCategoryByParam($this->user_name, $this->user['site'], $this->id_shop, $this->id_lang, 0, 0, $id_category, 1, (!empty($product_content) ? array('key' =>'AND', 'value' => $product_content) : (!empty($all) ? array('key' =>'AND', 'value' => $all) : null)));
            endif;
            if ( !empty($data['statistics_attribute']) ) :
                    foreach ( $data['statistics_attribute'] as $attribute_key => $statistics_attribute ) :
                            if ( !empty($data['statistics'][$attribute_key]) ) :
                                        $data['statistics'][$attribute_key]['combinations']    = $statistics_attribute;
                            endif;
                    endforeach;
            endif;

            if ( !empty($data['statistics']) ) :
                    $data['statistics']                     = $this->generate_key('id_category_default', $data['statistics']);
                    foreach ( $data['statistics'] as $key => $statistic_value ) :
                            if ( !empty($statistic_value) && array_key_exists('id_product', $statistic_value) ) :
                                    if ( !empty($data['statistics_category'][$statistic_value['id_category_default']]) ) :
                                            $data['statistics_category'][$statistic_value['id_category_default']]['product'][]      = $statistic_value;
                                    endif;
                            else :
                                    $id_category_default                    = current($statistic_value);
                                    if ( !empty($data['statistics_category'][$id_category_default['id_category_default']]) ) :
                                            $data['statistics_category'][$id_category_default['id_category_default']]['product']    = $statistic_value;
                                    endif;
                            endif;
                    endforeach;
            endif;
            if ( !empty($data['statistics_category']) ) :
                    $data['statistics_category'] = array_values($data['statistics_category']);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = !empty($data['statistics_category_count']) ? current($data['statistics_category_count'][0]) : 0;
//echo "<pre>", print_r($data['statistics_category'], true), "</pre>";exit();
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics_category']);
            echo json_encode($value);
    }


    function synchronization() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $popup_next                                     = array(
                    'synchronization_start'                 => 'synchronization_start',
                    'synchronization_detail'                => 'synchronization_detail',
                    'synchronization_matching'              => 'synchronization_matching',
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'synchronization',
                    'ajax_url'                              => 'synchronization_store',
                    'popup_config'                          => 1,
                    'popup_next'                            => $popup_next,
                    'btn_flow'                              => 'close',
                    'data_tree'                             => 'open',
//                        'profile_rules'                         => isset($this->user['profile_rules']) && is_numeric($this->user['profile_rules']) ? $this->user['profile_rules'] : 0
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }


    function synchronization_finish() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $popup_next                                     = array(
                    'synchronization_start'                 => 'synchronization_start',
                    'synchronization_detail'                => 'synchronization_detail',
                    'synchronization_matching'              => 'synchronization_matching',
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'synchronization_finish',
                    'ajax_url'                              => '',
                    'popup_config'                          => 4,
                    'popup_next'                            => $popup_next,
                    'btn_flow'                              => 'close',
                    'data_tree'                             => 'open',
//                        'profile_rules'                         => isset($this->user['profile_rules']) && is_numeric($this->user['profile_rules']) ? $this->user['profile_rules'] : 0
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function synchronization_detail() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $popup_next                                     = array(
                    'synchronization_start'                 => 'synchronization_start',
                    'synchronization_detail'                => 'synchronization_detail',
                    'synchronization_matching'              => 'synchronization_matching',
            );

            $count_product                                  = $this->objectbiz->getFeedbizCountProducts($this->user_name, $this->id_shop);
            $synchronize                                    = $this->objectbiz->getFeedbizCountSynchronize($this->user_name, $this->user['site'], $this->user['userID']);
            if ( !empty($synchronize) ) :
                    foreach ( $synchronize as $syn ) :
                            $syn_data['id_item'][]                  = $syn['id_item'];
                    endforeach;
            endif;
            $syn_include                                    = !empty($syn_data) ? "'".implode("', '", $syn_data['id_item'])."'" : array();
            $count_synchronize                              = !empty($synchronize) ? count($synchronize) : 0;
            $export_count                                   = $this->objectbiz->getFeedbizCountExportID($this->user_name, $this->user['site'], $this->id_shop, $syn_include, $this->user['userID']);
            $unknown                                        = ($count_synchronize > $export_count) ? ($count_synchronize - $export_count) : 0;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'synchronization_detail',
                    'ajax_url'                              => '',
                    'popup_config'                          => 2,
                    'popup_next'                            => $popup_next,
                    'btn_flow'                              => 'close',
                    'data_tree'                             => 'open',
                    'count_product'                         => isset($count_product) && is_numeric($count_product) ? $count_product : 0,
                    'count_synchronize'                     => isset($count_synchronize) && is_numeric($count_synchronize) ? $count_synchronize : 0,
                    'export_count'                          => isset($export_count) && is_numeric($export_count) ? $export_count : 0,
                    'unknown'                               => isset($unknown) && is_numeric($unknown) ? $unknown : 0,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function synchronization_matching() {
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $popup_next                                     = array(
                    'synchronization_start'                 => 'synchronization_start',
                    'synchronization_detail'                => 'synchronization_detail',
                    'synchronization_matching'              => 'synchronization_matching',
            );

            $this->pattern_code                             = $this->config->item('currency_code');

            $hidden_data                                    = array(
                    'chack-all-lang'                        => $this->lang->line('chack_all'),
                    'product-image-lang'                    => $this->lang->line('product_image'),
                    'product-item-lang'                     => $this->lang->line('product_item'),
                    'product-content-lang'                  => $this->lang->line('product_content'),
                    'product-information-lang'              => $this->lang->line('product_information'),
                    'product-detail-lang'                   => $this->lang->line('product_detail'),
                    'product-active-lang'                   => $this->lang->line('active'),
                    'product-manufacturer'                  => $this->lang->line('product_manufacturer'),
                    'product-title'                         => $this->lang->line('db_product_title'),
                    'product-supplier'                      => $this->lang->line('product_supplier'),
                    'product-quantity'                      => $this->lang->line('product_quantity'),
                    'product-price'                         => $this->lang->line('product_price'),
                    'product-reference'                     => $this->lang->line('product_reference'),
                    'product-sku'                           => $this->lang->line('product_sku'),
                    'product-ean13'                         => $this->lang->line('product_ean13'),
                    'product-upc'                           => $this->lang->line('product_upc'),
                    'synchronize-name'                      => $this->lang->line('synchronize_name'),
                    'condition-name'                        => $this->lang->line('condition_name'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'category-mapping'                      => $this->lang->line('synchronization_matching_mapping'),
                    'show-all'                              => $this->lang->line('show_all'),
                    'profile-name'                          => $this->lang->line('profile_name'),
                    'are-you-sure'                          => $this->lang->line('are_you_sure_synchronize_matching'),
                    'confirm-auto-map'                      => $this->lang->line('confirm_auto_map'),
                    'confirm-all'                           => $this->lang->line('yes_to_all'),
                    'skip-all'                              => $this->lang->line('skip_all'),
            );

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'synchronization_matching',
                    'ajax_url'                              => 'synchronization_matching_save',
                    'popup_config'                          => 3,
                    'popup_next'                            => $popup_next,
//                        'btn_flow'                              => 'close',
                    'data_tree'                             => 'open',
                    'data_table'                            => 'open',
                    'hidden_data'                            => !empty($hidden_data) ? $hidden_data : '',
                    'currency_code'                         => json_encode($this->pattern_code),

            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function synchronization_matching_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $id_product                                     = $this->input->get('id_product');
            $product_content                                = $this->input->get('product_content');
            $product_information                            = $this->input->get('product_information');
            $product_detail                                 = $this->input->get('product_detail');
            $active                                         = $this->input->get('active');
            $profile_name                                   = $this->input->get('profile_name');
            $show_list                                      = $this->input->get('show_list');
            $choose_ref                                     = $this->input->get('choose_ref');
            $input_key                                      = $this->input->get('insert_key');
            $reference_key                                  = $this->input->get('reference_key');
            $sort_name                                      = $this->input->get('sort_name');
            $sort_by                                        = $this->input->get('sort_by');
            $all                                            = $this->input->get('all');

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_attribute']                   = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                    'product_content'                       => !empty($all) ? $all : array('name' => htmlspecialchars($product_content)),
                    'product_information'                   => !empty($all) ? $all : array('name' => htmlspecialchars($product_information)),
                    'product_detail'                        => !empty($all) ? $all : array('name' => htmlspecialchars($product_detail)),
            ), $limit, $offset, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');
            if ( empty($data['statistics_category']) && !empty($data['statistics_attribute']) ) :
                    foreach ( $data['statistics_attribute'] as $attributes ) :
                            $attribute['id_product'][$attributes['id_product']] = $attributes['id_product'];
                    endforeach;

                    $data['statistics_attribute']           = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                            'id_product'                    => (!empty($attribute) ? $attribute : array('id_product' => array(0))),
                    ), 0, 0, "*", null, null, 'AND');
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_attribute_offer']             = $this->objectbiz->getAdvanceMappingProductAttributeOfferByParam($this->user_name, $this->id_shop, array(
                    'id_category'                           => !empty($all) ? $all : (!empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product))),
            ), $limit, $offset, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( !empty($data['statistics_attribute']) ) :
                    $data['statistics_attribute']           = $this->generate_group('id_product', array('id_product_attribute' => 'GROUPALL'), $data['statistics_attribute']);
            endif;

            if ( !empty($data['statistics_attribute_offer']) ) :
                    foreach ( $data['statistics_attribute_offer'] as $statistics_attribute_offer ) :
                            if ( !empty($data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']]) ) :
                                        foreach ( $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']] as $attribute_offer_key => $product_offer ) :
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['reference']    = $statistics_attribute_offer['reference'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['sku']          = $statistics_attribute_offer['sku'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['ean13']        = $statistics_attribute_offer['ean13'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['upc']          = $statistics_attribute_offer['upc'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['price']        = $statistics_attribute_offer['price'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['quantity']     = $statistics_attribute_offer['quantity'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['price_type']   = $statistics_attribute_offer['price_type'];
                                                $data['statistics_attribute'][$statistics_attribute_offer['id_product']][$statistics_attribute_offer['id_product_attribute']][$attribute_offer_key]['date_add']     = $statistics_attribute_offer['date_add'];
                                        endforeach;
                            endif;
                    endforeach;
            endif;
            $product_delpt                      = $this->generate_delpt_wanted(array('id_product'), 'id_product', 0, $data['statistics_attribute']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics']                             = $this->objectbiz->getAdvanceMappingCategoriesByParam($this->id_shop, $this->id_lang, array(
                    'id_category_default'                   => !empty($all) ? $all : $id_product,
                    'id_product'                            => !empty($product_delpt) ? $product_delpt : array('id_product' => array($id_product)),
                    'product_content'                       => !empty($all) ? $all : array('name' => htmlspecialchars($product_content)),
                    'product_information'                   => !empty($all) ? $all : array('name' => htmlspecialchars($product_information)),
                    'product_detail'                        => !empty($all) ? $all : array('name' => htmlspecialchars($product_detail)),
                    'id_category'                           => !empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product)),
                    'active'                                => !empty($all) ? $all : $active,
            ), $limit, $offset, array('p.id_product AS id_product'
                , 'p.id_category_default AS id_category_default'
                , 'p.reference AS reference'
                , 'p.sku AS sku'
                , 'p.ean13 AS ean13'
                , 'p.upc AS upc'
                , 'p.quantity AS quantity'
                , 'p.price AS price'
                , 'p.active AS active'
                , 'p.date_add AS date_add'
                , 'p.date_upd AS date_upd'
                , 'c.name AS condition_name'
                , 'cr.iso_code AS iso_code'
                , 'i.image_url AS image_url'
                , 'l.name AS category_name'
                , 'm.name AS manufacturer_name'
                , 's.name AS supplier_name'
                , 'pl.name AS product_name'
                )
                , $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( empty($data['statistics_category']) && empty($data['statistics_attribute']) && !empty($data['statistics']) ) :
                    unset($attribute);
                    foreach ( $data['statistics'] as $attributes ) :
                            $attribute['id_product'][$attributes['id_product']] = $attributes['id_product'];
                    endforeach;

                    $data['statistics_attribute']           = $this->objectbiz->getAdvanceMappingProductAttributeByParam($this->id_shop, array(
                            'id_product'                    => (!empty($attribute) ? $attribute : array('id_product' => array(0))),
                    ), 0, 0, "*", null, null, 'AND');

                    if ( !empty($data['statistics_attribute']) ) :
                            $data['statistics_attribute']           = $this->generate_group('id_product', array('id_product_attribute' => 'GROUPALL'), $data['statistics_attribute']);
                    endif;
            endif;

            if ( !empty($data['statistics']) ) :
                    $data['statistics']                     = $this->generate_key('id_product', $data['statistics']);
            endif;

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $data['statistics_offer']                       = $this->objectbiz->getAdvanceMappingCategoriesOfferByParam($this->user_name, $this->id_shop, $this->id_lang, array(
                    'id_category_default'                   => !empty($all) ? $all : $id_product,
                    'id_product'                            => !empty($all) ? $all : $id_product,
                    'id_category'                           => !empty($category_delpt) ? $category_delpt : array('id_category' => array($id_product)),
            ), $limit, $offset, array('p.id_product AS id_product'
                , 'p.quantity AS quantity'
                , 'p.price AS price'
                , 'p.date_upd AS date_upd'), $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

            if ( !empty($data['statistics_offer']) ) :
                    foreach ( $data['statistics_offer'] as $statistics_offer ) :
                            if ( !empty($data['statistics'][$statistics_offer['id_product']]) ) :
                                        $data['statistics'][$statistics_offer['id_product']]['quantity']    = "{B}".$data['statistics'][$statistics_offer['id_product']]['quantity']."{/B} ".$this->lang->line('to')." ".$statistics_offer['quantity'];
                                        $data['statistics'][$statistics_offer['id_product']]['price']       = "{B}".$data['statistics'][$statistics_offer['id_product']]['price']."{/B} ".$this->lang->line('to')." ".$statistics_offer['price'];
                                        $data['statistics'][$statistics_offer['id_product']]['date_upd']    = "{B}".$data['statistics'][$statistics_offer['id_product']]['date_upd']."{/B} ".$this->lang->line('to')." ".$statistics_offer['date_upd'];
                            endif;
                    endforeach;
            endif;

            if ( !empty($data['statistics_attribute']) ) :
                    foreach ( $data['statistics_attribute'] as $attribute_key => $statistics_attribute ) :
                            if ( !empty($data['statistics'][$attribute_key]) ) :
                                        $data['statistics'][$attribute_key]['combinations']    = $statistics_attribute;
                            endif;
                    endforeach;
            endif;
            if ( !empty($data['statistics']) ) : 
                    $insert_key = array();
                    foreach ( $data['statistics'] as $products ) :
                            $syn_chronize['id_product'][$products['id_product']] = $products['id_product'];

                            if ( !empty($choose_ref) && !empty($input_key) && !empty($reference_key) && $choose_ref == 'custom' ) : 
                                    $insert_key['input_key_string'][]   = str_ireplace('[ProductID]', $products['id_product'], $input_key);
                                    $insert_key['reference_key_string'][]   = str_ireplace('[ProductID]', $products['id_product'], $reference_key);
                            endif;
                    endforeach;

            endif;

            if ( !empty($show_list) ) :
                    $data['statistics_store']               = $this->objectbiz->getSynchronizeStoreByParam($this->user_name, $this->user['site'], $this->id_shop, array(
                            'choose_reference'              => array($choose_ref => !empty($syn_chronize) ? $syn_chronize : array(0)),
                            'input_key'                     => array('name' => !empty($insert_key) ? $insert_key : 0),
                    ), $limit, $offset, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');


                    $data['statistics_store_count']         = $this->objectbiz->getSynchronizeStoreByParam($this->user_name, $this->user['site'], $this->id_shop, array(
                            'choose_reference'              => !empty($all) ? $all : array($choose_ref => !empty($syn_chronize) ? $syn_chronize : array(0)),
                            'input_key'                     => array('name' => !empty($input_key_string) ? $input_key_string : array(0)),
                    ), 0, 0, "*", $sort_name, $sort_by, !empty($all) ? 'OR' : 'AND');

                    if ( !empty($data['statistics_store']) ) :
                            $data['statistics_store']               = $this->generate_key('id_product', $data['statistics_store']);
                    endif;
            endif;

            if ( !empty($data['statistics']) ) :
//                        $data['statistics']                     = $this->generate_key('id_category_default', $data['statistics']);
                    foreach ( $data['statistics'] as $key => $statistic_value ) :
                            if ( !empty($statistic_value) && array_key_exists('id_product', $statistic_value) ) :
                                    if ( !empty($data['statistics_store'][$statistic_value['id_product']]) ) :
                                            $data['statistics_store'][$statistic_value['id_product']]['product'][]      = $statistic_value;
                                    endif;
                            else :
                                    $id_category_default                    = current($statistic_value);
                                    if ( !empty($data['statistics_store'][$id_category_default['id_product']]) ) :
                                            $data['statistics_store'][$id_category_default['id_product']]['product']    = $statistic_value;
                                    endif;
                            endif;
                    endforeach;
            endif;
            if ( !empty($data['statistics_store']) ) :
                    $data['statistics_store'] = array_values($data['statistics_store']);
            endif;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = !empty($data['statistics_store_count']) ? count($data['statistics_store_count']) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => !empty($data['statistics_store']) ? $data['statistics_store'] : array());
            echo json_encode($value);
    }


    function synchronization_matching_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) :
                            $details_insert                                 = '';
                            foreach ( $data as $fixed ) :
                                    unset($data_set, $data_detail);
                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ( !empty($fixed['sku']) && !empty($fixed['id_product']) ) :
                                            $syn_data                       = $this->objectbiz->getSynchronizeStoreBySKU($this->user_name, $this->user['site'], $fixed['sku']);
                                            if ( !empty($syn_data) ) :
                                                    $this->objectbiz->deleteColumn('ebay_synchronization_store', array('SKU' => $fixed['sku'], 'id_site' => $this->user['site']));
                                                    $data_detail                    = array(
                                                            'id_product'            => isset($fixed['id_product']) ? $fixed['id_product'] : '',
                                                            'id_item'               => isset($syn_data[0]['id_item']) ? $syn_data[0]['id_item'] : '',
                                                            'id_site'               => isset($this->user['site']) ? $this->user['site'] : '',
                                                            'id_shop'               => isset($this->id_shop) ? $this->id_shop : '',
                                                            'SKU'                   => isset($fixed['sku']) ? $fixed['sku'] : '',
                                                            'ebay_user'             => isset($syn_data[0]['ebay_user']) ? $syn_data[0]['ebay_user'] : '',
                                                            'name_product'          => isset($syn_data[0]['name_product']) ? $syn_data[0]['name_product'] : '',
                                                            'date_add'              => isset($syn_data[0]['date_add']) ? $syn_data[0]['date_add'] : '',
                                                            'date_end'              => isset($syn_data[0]['date_end']) ? $syn_data[0]['date_end'] : '',
                                                            'is_enabled'            => '1',
                                                    );
                                                    $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_product_item_id', $data_detail);
                                            endif;
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }

    public function prepare_payment($profile_id = 0) {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $profile_data                                   = $this->objectbiz->getEbayProfilePayment($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $payment_method                                 = $this->ebay_model->get_ebay_payment($this->user['site']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ( !empty($payment_method) && !empty($profile_data) ) :
                    $payment_selected                       = unserialize(base64_decode($profile_data['payment_method']));
                    foreach ( $payment_method as $key_pay => $payment) :
                            if ( !empty($payment_selected) && in_array($payment['payment_method'], $payment_selected) ) :
                                    $payment_method[$key_pay]['selected']   = 'selected';
                            endif;
                            $txt = $this->lang->line($payment['payment_name']);
                            $payment_method[$key_pay]['payment_name'] = $txt ===false?$payment['payment_name']:$txt;
                    endforeach;
            endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            $hidden_data                                    = array(
                    'id_profile'                            => $profile_id,
                    'remaining'                             => $this->lang->line('remaining'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'hidden_data'                           => $hidden_data,
                    'payment_method'                        => !empty($payment_method) ? $payment_method : '',
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
            );
            $this->assign($assign);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function payment_method() {
            $this->id_profile                               = 0;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_payment(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'payment_method',
                    'ajax_url'                              => 'advance_profile_payment_save',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    public function prepare_return($profile_id = 0) {
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $profile_data                                   = $this->objectbiz->getEbayProfileReturn($this->user_name, $this->user['site'], $this->id_shop, $profile_id);
            $payment_method                                 = $this->ebay_model->get_ebay_payment($this->user['site']);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ebay_return_within                             = $this->ebay_model->get_return_within();
            if ( !empty($ebay_return_within) ) :
                    foreach ( $ebay_return_within as $return ) :
                            if ( isset($profile_data["returns_within"]) && $return['return'] == $profile_data["returns_within"] ) :
                                    $data['return'][]       = array('within' => $return['return'], 'selected' => 'selected');
                            else :
                                    $data['return'][]       = array('within' => $return['return']);
                            endif;
                    endforeach;
                    $this->smarty->assign(array('returns_within' => $data['return']));
            endif;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
            $hidden_data                                    = array(
                    'id_profile'                            => $profile_id,
                    'remaining'                             => $this->lang->line('remaining'),
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'hidden_data'                           => $hidden_data,
                    'profile_data'                          => !empty($profile_data) ? $profile_data : '',
            );
            $this->assign($assign);
    }


    public function error_resolutions(){

            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $hidden_data                                    = array(
                    'sku'                                   => $this->lang->line('sku'),
                    'varians'                              	=> $this->lang->line('varians'),
                    'price'                              	=> $this->lang->line('price'),
                    'quantity'                              => $this->lang->line('quantity'),
                    'show'                              	=> $this->lang->line('show'),
                    'hide'                              	=> $this->lang->line('hide'),
                    'show-all'                              => $this->lang->line('show_all'),
                    'title-statistics'                      => $this->lang->line('header_statistics'),
                    'type'                                  => $this->lang->line('type'),
                    'user'                                  => $this->lang->line('user'),
                    'message'                               => $this->lang->line('message'),
                    'batch_id'                              => $this->lang->line('batch_id'),
                    'product_name'                          => $this->lang->line('Product Name'),
                    'product_sku'                           => $this->lang->line('product_sku'),
                    'varian_sku'                            => $this->lang->line('varian_sku'),
                    'hidden-search'                         => $this->lang->line('search'),
                    'export-date'                           => $this->lang->line('export_date'),
                    'cause'                                 => $this->lang->line('cause'),
                    'solution'                              => $this->lang->line('solution'),
                    'help'                                  => $this->lang->line('help'),
                    'solved'                                => $this->lang->line('solved')
            );
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'error_resolutions',
                    'ajax_url'                              => '',
                    'root_page'                             => 'error_resolutions/',
                    'data_table'                            => 'open',
                    'btn_flow'                              => 'close',
                    'statistics'                            => !empty($statistics_data) ? $statistics_data : '',
                    'hidden_data'                           => !empty($hidden_data) ? $hidden_data : '',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function error_resolutions_processing() {
            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $search                                         = $this->input->get('search');
            $batch_id                                       = isset($search['value']) ? $search['value'] : '';
//                 $batch_id                                    = empty($batch_id) ? '555052f374139' : $batch_id;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $result											= $this->objectbiz->getStatisticConclusion($batch_id, $limit, $offset);
                $data['statistics']                             = empty($result['data']) ? array() : $result['data'];
                            $data['count']                                  = $result['count'];
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $count = isset($data['count']) && isset($data['count'][0]) ? current($data['count'][0]) : 0;
            $value = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $count, 'recordsFiltered' => $count, 'data' => $data['statistics']);
            echo json_encode($value);
    }

    function error_resolutions_products_processing() {

            $limit                                          = $this->input->get('limit');
            $offset                                         = $this->input->get('offset');
            $id_site                                        = $this->input->get('id_site');
            $id_packages                                    = $this->input->get('id_packages');
            $data                                           = array('id_site' => $id_site, 'id_packages' => $id_packages);
            $this->validation_ajax(array($data));
            $batch_id                                       = $this->input->get('batch_id');
            $errorCode                                       = $this->input->get('code');
            $search                                         = $this->input->get('search');
            $product                                       = $this->input->get('product');
            $resolved                                       = $this->input->get('resolved');
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(!empty($resolved)){
                                    $resolved = $resolved == 'solved' ? 1 : 0;
                                    $result = $this->objectbiz->setStatisticProductSolve($batch_id, $errorCode, $product, $resolved);
                                    echo json_encode(array('batch_id' => $batch_id,
                                                    'error_code' => $errorCode,
                                                    'product' => $product,
                                                    'resolved' => $resolved,
                                                    'resolved_count' => $result['resolved_count']
                                    ));
                            }
                            else{
                    $search_q										 = $search['value'];
                    $search_result				                             = $this->objectbiz->getStatisticProducts($errorCode, $search_q, $limit, $offset);
                    $search = array('draw' => ($offset >= 1 ? $offset + 1 : 1), 'recordsTotal' => $search_result['count'], 'recordsFiltered' => $search_result['count'], 'data' => $search_result['data']);
                    echo json_encode($search);
                            }
    }

    function error_resolution_content(){
            $code = $this->input->get('code');
            $solution = $this->ebay_model->get_error_resolutions($code, $this->iso_code);
            echo json_encode($solution);
    }

    function override_product_attribute(){
                    $products = $this->input->get ( 'products' );
                    $overrided = $this->objectbiz->ebay_attribute_override($this->user_name, $this->id_shop, $this->user ['site'], $this->id_lang, $products);

                    $data = $this->init_packages ();
                    $data = $this->init_pages ( $data );
                    $profile_id = 0;
                    $this->id_profile = 0;
                    $attribute_main = $this->objectbiz->getAllAttributeNameGroup ( $this->user_name, $this->user ['site'], $this->id_shop, $this->id_lang, $profile_id );
                    $profile_configuration = $this->objectbiz->getEbayProfileConfiguration ( $this->user_name, $this->user ['site'], $this->id_shop, $this->id_profile );

                    $category_group ['id_ebay_category'] [] = $profile_configuration ['id_ebay_category'];
                    if (empty ( $category_group ['id_ebay_category'] [0] )) :
                            $category_group = $this->objectbiz->getIDCategoriesMapping ( $this->user_name, $this->user ['site'], $this->id_shop );
                            if (! empty ( $category_group )) :
                                    $category_group = $this->generate_additions_group ( 'id_ebay_category', 'id_ebay_category', $category_group );
                             else :
                                    $category_group ['id_ebay_category'] [0] = '';
                            endif;
                    endif;

                    if (! empty ( $category_group ['id_ebay_category'] )) :
                            foreach ( $category_group ['id_ebay_category'] as $cat_group ) :
                                    $attribute_group [$cat_group] = $this->ebay_model->get_variation_group ( $cat_group, $this->user ['site'] );
                            endforeach
                            ;
                    endif;

                    // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $attribute_group_prepared = array ();
                    $attribute_group_name_sort = array ();
                    foreach ( $attribute_group as $attribute_group_cate ) {
                            if (! empty ( $attribute_group_cate ) && is_array ( $attribute_group_cate )) {
                                    foreach ( $attribute_group_cate as $attribute_group_element ) {
                                            $attribute_group_prepared [$attribute_group_element ['id_group']] = $attribute_group_element;
                                            $attribute_group_name_sort [$attribute_group_element ['id_group']] = $attribute_group_element ['name'];
                                    }
                            }
                    }
                    asort ( $attribute_group_name_sort );
                    $form_data = array('attribute_group'=>$attribute_group,
                                                            'products' => $overrided
                    );

                    $this->smarty->view ( 'ebay/error_resolutions/override_product_attribute.tpl', $form_data );
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function return_method() {
            $this->id_profile                               = 0;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->prepare_return(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'return_method',
                    'ajax_url'                              => 'advance_profile_return_save',
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    public function prepare_explode_products($products = array()) {
            $param                                          = array($this->user_name, $this->user_id, $this->user['site'], null, null);
            $this->load->library('Ebay/ebayexport', $param);
            $this->ebayexport->export_product_array(); 
//                $this->ebayexport->export_product_group(array(59, 242)); 
            $products = !empty($this->ebayexport->product['product']) ? $this->ebayexport->product['product'] : array();
            $country_site = $this->ebay_model->get_sites($this->user['site']);
            if ( !empty($country_site) ) :
                $country_iso = $country_site[0]->iso_code;
            endif;
            if ( !empty($products) ) :
                $explode_selected = $this->generate_key('id_product', $this->objectbiz->getEbayExplodeProduct ( $this->user ['site'], $this->id_shop ));
//                echo "<pre>", print_r($explode_selected, true), "</pre>";exit();
                foreach ( $products as $id => &$product ) :
                    if ( empty($product['combination']) ) {
                        unset($products[$id]);
                        continue;
                    }
                    $product['_id'] = $product['id_product'];
                    $product['name'] = !empty($product['name'][$country_iso]) ? $product['name'][$country_iso] : $product['name'];
                    $product['description'] = !empty($product['description'][$country_iso]) ? $product['description'][$country_iso] : $product['name'];
                    $product['description_short'] = !empty($product['description_short'][$country_iso]) ? $product['description_short'][$country_iso] : $product['name'];
                    $product['isActive'] = !empty($product['active']) ? true : false;
                    $product['isGroup'] = !empty($product['combination']) ? true : false;
                    $product['separateItem'] = !empty($explode_selected) && !empty($explode_selected[$id]) && $explode_selected[$id]['id_product'] == $product['id_product'] ? true : false;
                    $product['notification'] = array(
                        'id' => $product['id_product'],
                        'type' => 'info',
                        'title' => !empty($product['name'][$country_iso]) ? $product['name'][$country_iso] : $product['name'],
                        'msg' => !empty($product['description'][$country_iso]) ? $product['description'][$country_iso] : $product['name'],
                    );
                    unset($product['description'], $product['description_short']);
                endforeach;
            endif;
            return json_encode($products);
//                echo "<pre>", print_r($products, true), "</pre>";exit();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function explode_products() {
            $this->id_profile                               = 0;
            $data                                           = $this->init_packages();
            $data                                           = $this->init_pages($data);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $products = $this->prepare_explode_products(0);
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $assign                                         = array(
                    'current_page'                          => 'explode_products',
                    'ajax_url'                              => 'explode_products_save',
                    'json_data'                             => $products,
                    'ng'                                    => 1,
            );
            $this->assign($assign);
            $this->smarty->view('ebay/main_template.tpl', $data);
    }

    function explode_products_save() {
            if ($this->input->post()) :
                    $data                                                   = (isset($_POST['data']) ? $_POST['data'] : '');
                    $this->authentication->validateAjaxToken();
                    $this->validation_ajax($data);
                    if ( !empty($data) && count($data) ) : 
                            $details_insert                                 = '';
                            $this->objectbiz->deleteColumn('ebay_explode_product', array('id_shop' => $this->id_shop, 'id_site' => $this->user['site']));
                            foreach ( $data[0]['data_object'] as $data_object ) :
                                    unset($data_set, $data_detail);
                                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ( !empty($data_object) && !empty($data_object['separateItem']) && $data_object['separateItem'] == 'true' ) :
                                            $data_detail                    = array(
                                                    'id_product'            => isset($data_object['id_product']) ? $data_object['id_product'] : '',
                                                    'id_site'               => isset($this->user['site']) ? $this->user['site'] : '',
                                                    'id_shop'               => isset($this->id_shop) ? $this->id_shop : '',
                                                    'attribute_field'       => '',
                                                    'date_add'              => date('Y-m-d H:i:s', strtotime('now')),
                                            );
                                            $details_insert         .= $this->objectbiz->mappingTableOnly('ebay_explode_product', $data_detail);
                                    endif;
                            endforeach;
                            $result_insert                  = $this->objectbiz->transaction($details_insert, true);
                            echo ( $result_insert ) ? "Success" : "Fail";
                    else :
                            echo "Fail";
                    endif;
            else :
                    echo "Fail";
            endif;
    }
}
