<?php

class Component_function extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    //-- Load all libraries --//
    
    public function load_libraries() {
        $this->load->library('authentication');
        $this->load->library('FeedBiz', array($this->user_name));
    }
    
    //-- Check has login or not --//

    public function check_login() {
        $this->authentication->checkAuthen();
        if   
            ( (!isset($this->user_name) && empty($this->user_name)) && 
              (!isset($this->id_user) && empty($this->id_user)) )

            redirect('users/login');
    }

    //-- Check have Shop and Shopname or not --//

    public function check_shop() {
        if  
            ( !isset($this->id_shop) || empty($this->id_shop) || 
              !($this->shop_default) || empty($this->shop_default) ) {   

            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            if ( !empty($DefaultShop) ) {
                $this->id_shop = $DefaultShop['id_shop'];
                $this->shop_default = $DefaultShop['name'];
            }
            else {
                redirect('users/login');
            }
        }
    }

    //-- Check has language or not --//

    public function check_lang() {
        if ( !isset($this->language) && empty($this->language) )
            $this->language = $this->config->item('language');

        $lang_default = $this->feedbiz->getLanguageDefault($this->id_shop);
        $this->id_lang = '';
        if ( !empty($lang_default) ) {
            $lang_default = current($lang_default);
            $this->id_lang = $lang_default['id_lang'];
            $this->product_iso_code = $lang_default['iso_code'];
        }
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->lang->load(strtolower(str_replace(' ', '_', $this->marketplace_name)), $this->language);
    }

    /*
     * return array();
     */
    public function country_data($country = null, $rerutn=false) {

        $this->id_country           = isset($country) ? $country : (is_numeric($this->uri->segment(3)) ? $this->uri->segment(3) : $this->uri->segment(4)) ;

        $this->country              = (isset($this->id_country) && isset($this->menu[$this->marketplace_name]['submenu'][$this->id_country]['country'])) ?
                                        $this->menu[$this->marketplace_name]['submenu'][$this->id_country]['country'] : null ;
        $this->ext                  = (isset($this->id_country) && !empty($this->menu[$this->marketplace_name]['submenu'][$this->id_country]['ext'])) ?
                                        $this->menu[$this->marketplace_name]['submenu'][$this->id_country]['ext'] : null ;
        $this->id_region            = (isset($this->id_country) && !empty($this->menu[$this->marketplace_name]['submenu'][$this->id_country]['id_region'])) ?
                                        $this->menu[$this->marketplace_name]['submenu'][$this->id_country]['id_region'] : null ;
        $this->currency             = (isset($this->id_country) && !empty($this->menu[$this->marketplace_name]['submenu'][$this->id_country]['currency'])) ?
                                        $this->menu[$this->marketplace_name]['submenu'][$this->id_country]['currency'] : null ;

        if($rerutn){
            return (array) $this->get_values();
        } else {
            $this->data = $this->get_values();
        }
    }
    
    //-- call script --//

    public function call_script() {
        if ( func_num_args() == 0 ) return null;

        foreach ( func_get_args() as $assign ) {
            $this->smarty->assign($assign, 1);  
        }
    }

    //-- Assign breadcrumbs link --//

    public function assign_breadcrumbs() {
        $data = array(
            'lang'      => $this->language,
            'label'     => $this->marketplace_name,
            'class'     => ucwords($this->class_call),
            'function'  => $this->lang->line($this->function_call) ? $this->lang->line($this->function_call) : ucfirst($this->function_call),
        );
        $this->assigns($data);
    }
    
    //-- Assign all data to templates --//

    public function assigns() {
        if ( func_num_args() == 0 ) return null;

        foreach ( func_get_args() as $assign ) {
            foreach ( $assign as $key => $value ) { 
                $this->smarty->assign($key, $value);  
            }
        }
    }
    
    //-- Check have security data or not --//
    
    protected function security_check() {
        if  ( (!isset($this->id_user) && empty($this->id_user)) || 
              (!isset($this->marketplace_name) && empty($this->marketplace_name)) || 
              (!isset($this->ext) && empty($this->ext))) {
            return false;
        }
    }
           
    //-- Get value -- if not pass variable will send all data. -- can specify key value. //
    
    protected function get_values() {
        $data = array(
            'user_name'         => $this->user_name,
            'id_user'           => $this->id_user,
            'id_shop'           => $this->id_shop,
            'id_mode'           => $this->id_mode,
            'shop_default'      => $this->shop_default,
            'language'          => $this->language,
            'language_name'     => $this->language,
            'menu'              => $this->menu,
            'function_call'     => $this->function_call,
            'class_call'        => $this->class_call,
            'marketplace_prefix'=> strtolower($this->marketplace_prefix),
            'marketplace_name'  => strtolower($this->marketplace_name),
            'is_marketplace'    => _MARKETPLACE_NAME_, 
            'id_marketplace'    => $this->id_marketplace,
            'id_country'        => $this->id_country,
            'country'           => $this->country,
            'countries'         => $this->country,
            'ext'               => $this->ext,
            'id_region'         => $this->id_region,
            'currency'          => $this->currency,
            'id_customer'       => $this->id_user,
            'id_lang'           => $this->id_lang,
            'iso_code'          => $this->iso_code,
            'id_region'         => $this->iso_code,
        );
        
        if ( func_num_args() == 0 ) return $data;
        
        foreach ( func_get_args() as $key ) {
            if (isset($this->$key))
                $specific[$key] = $this->$key;
        }
        return !empty($specific) ? $specific : $data;
    }
    
    //-- Get root categories value -- if have, pass variable to function get_child_category. return html to data table. //
    
    public function get_selected_all_category($configuration = null) {
        $html = '';
        $lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
	// exception for all root category
	$roots = $this->feedbiz->getAllRootCategories($this->user_name, $this->id_shop, $lang['id_lang']);
	if(!empty($roots)){
            foreach($roots as $root){
                $arr_selected_categories_res =$this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $this->id_mode, $root['id'], false, $lang['id_lang']);
                $name = isset($root['name'][$this->iso_code])?$root['name'][$this->iso_code]:(isset($root['name'][$lang['iso_code']])?$root['name'][$lang['iso_code']]:'');
                if(empty($name)){
		    continue;
		    
		}
                $arr_selected_categories[] = array('id_category'=>$root['id'],'name'=>$name,'parent'=>0,'type'=>'folder','child'=>sizeof($arr_selected_categories_res));
            } 
        }
	if(empty($arr_selected_categories)) {
	     echo json_encode($html);
	     exit;
	}
        $profile = $configuration->getProfiles($this->id_shop, $this->id_country);
       
        foreach ($arr_selected_categories as $asc) {
            $html .= $this->get_child_category($lang['id_lang'], $asc, $profile, $configuration);
        }
	
        echo json_encode($html);
        exit;
    }
    
    //-- Get categories value and find child if have, recursive if not have, get html templates from function get_selected_category_template. return html to function get_selected_all_category. //
    
    public function get_child_category($id_lang = null, $category = null, $profile = null, $configuration = null) {
        $html = $subchild = '';
        if (isset($category) && !empty($category)) {
            if ($category['type'] == "folder") {
                if (isset($category['child']) && !empty($category['child'])) {
                    $categories = $this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $this->id_mode, $category['id_category'], false, $id_lang);
                    foreach ($categories as $child) {
                        $subchild .= $this->get_child_category($id_lang, $child, $profile, $configuration);
                    }
                }
            }
        }

        $html .= $this->get_selected_category_template($category, $subchild, $profile, $configuration);
        return $html;
    }
    
    //-- Get html templates and return html to function get_child_category. //
    
    public function get_selected_category_template($selected_category = null, $sub_html = null, $profile = null, $configuration = null) {
        if( !isset($this->list_selected) ) {           
            $this->list_selected = $configuration->getSelectedCategories($this->id_shop, $this->id_country);
        }

        $assing = array(
            'checked'   => false,
            'selected'  => '',
            'sub_html'  => '',
        );
        if ( !empty($this->list_selected[$selected_category['id_category']]) ) {
            $assing['selected'] = $this->list_selected[$selected_category['id_category']]['id_profile'];
            $assing['checked']      = true;
        }
        $assing['profile']      = $profile;
        $assing['sub_html']     = isset($sub_html) && !empty($sub_html) ? $sub_html : '';
        $assing['type']         = $selected_category['type'];
        $assing['id_category']  = $selected_category['id_category'];
        $assing['name']         = is_array($selected_category['name']) ? $selected_category['name'][$this->product_iso_code] : $selected_category['name'];
        $assing['mode']         = $this->id_mode;
        $this->assigns($assing);
        return $this->smarty->fetch('amazon/CategoryGroupTemplate.tpl');
    }
    
    public function post_form_data() {
        $this->values   = isset($_POST['formdata']) ? $_POST['formdata'] : '';
        $searcharray = array();
        if ( !empty($this->values) ) {
            parse_str(urldecode($this->values), $searcharray);
        }
        return $searcharray;
    }
    
    public function merge_form_data() {
        $this->values   = isset($_POST['formdata']) ? $_POST['formdata'] : '';
        $searcharray = array();
        if ( !empty($this->values) ) {
            parse_str(urldecode($this->values), $searcharray);
            if ( !empty($searcharray) && !empty($this->data) ) {
                $this->data     = array_merge($this->data, $searcharray);
            }
        }
    }
}

