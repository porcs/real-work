<?php

require_once(dirname(__FILE__) . '/component.function.check.php');
require_once(dirname(__FILE__) . '/../../libraries/tools.php');
require_once(dirname(__FILE__) . '/../../libraries/Marketplaces/classes/marketplaces.configuration.php');

class Component_controller extends Component_function {
    protected $user_name;
    protected $id_user;
    protected $id_shop;
    protected $id_mode = 1;
    protected $shop_default;
    protected $marketplace_name;
    protected $country;
    protected $id_marketplace;
    protected $id_country;
    protected $ext;
    protected $id_region;
    protected $currency;
    protected $function_call;
    protected $class_call;
    protected $method;
    public $_data = array();
    
    public function __construct($marketplace_name = null) {
        parent::__construct();
        $this->user_name            = $this->session->userdata('user_name');
        $this->id_user              = $this->session->userdata('id');
        $this->id_shop              = $this->session->userdata('id_shop');
        $this->shop_default         = $this->session->userdata('shop_default');
        $this->language             = $this->session->userdata('language');
        $this->function_call        = $this->router->fetch_method();
        $this->class_call           = $this->router->fetch_class();
        $this->marketplace_prefix   = isset($marketplace_name['prefix']) ? nameToKey($marketplace_name['prefix']) : nameToKey($marketplace_name);
        $this->marketplace_name     = isset($marketplace_name['marketplace_name']) ? nameToKey($marketplace_name['marketplace_name']) : nameToKey($marketplace_name);
        $this->menu                 = isset($this->session->userdata['menu']) ? $this->session->userdata['menu'] : '';
        $this->id_marketplace       = isset($this->menu[$this->marketplace_name]) ? $this->menu[$this->marketplace_name]['id'] : $this->check_login();

        $this->load_libraries();
        $this->check_login();
        $this->check_shop();
        $this->check_lang();
        $this->assign_breadcrumbs();
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
        
        load($this->marketplace_name, $this->language);

    }

    /*
     * $fields = see field structor in data.txt
     * 
     */
    public function component_get_data($table, $restriction=array(), $where_operation="AND") {
        $configuration = new MarketplacesConfiguration($this->user_name, strtolower($this->marketplace_prefix));
        return $configuration->get_data($table, $restriction=array(), $where_operation="AND");
    } 
    
    
    public function component_parameter($override_post = null) {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, strtolower($this->marketplace_prefix));
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
            $this->merge_form_data();
        }
        switch ($this->method) {
        case 'set_parameter' :
	    
            if ( !empty($this->data) ) { 
		
		// get iso_code from id_country
		$country = MarketplacesTools::getCountry($this->id_country);
		
                $data                       = array(
                    'id_customer'           => $this->id_user,
                    'user_name'             => $this->user_name,
                    'id_shop'               => $this->id_shop,
                    'id_marketplace'        => $this->id_marketplace,
                    'id_country'            => $this->id_country,
                    'ext'                   => $this->ext, 
                    'countries'             => $this->country,
                    'currency'              => $this->currency,
                    'id_lang'               => $this->id_lang, 
                    'iso_code'              => isset($country['iso_code']) ? $country['iso_code'] : $this->iso_code,
                    'id_region'             => $this->id_region,
                    'active'                => isset($this->data['active']) ? $this->data['active'] : NULL,
                    'api_key'               => isset($this->data['api_key']) ? $this->data['api_key'] : NULL,
                    'token_key'             => isset($this->data['token_key']) ? $this->data['token_key'] : NULL,
                    'secret_key'            => isset($this->data['secret_key']) ? $this->data['secret_key'] : NULL,
                    'merchant_key'          => isset($this->data['merchant_key']) ? $this->data['merchant_key'] : NULL,
                    'merchant_token'        => isset($this->data['merchant_token']) ? $this->data['merchant_token'] : NULL,
                );
                if ( !empty($data) && !empty($override_post) ) {
                    $data = array_merge($data, $override_post);
                }
                $result = $configuration->setParameters($data);
                echo $result ? "Success" : "Failure"; exit;
            }
            echo "Failure"; exit;
            
        default : 
            $result = $configuration->getParameters($this->id_shop, $this->id_country);
            $this->data = array_merge($this->data, !empty($result) ? $result : array());
            $this->data['method'] = 'set_parameter';           
            break;
        }
    }
 
    public function component_models($override_post = null) {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = isset($_POST['method']) ? $_POST['method'] : '';
            $this->merge_form_data();
        }
        switch ($this->method) {
        case 'set_models' :
            if ( !empty($this->data['model']) ) {
                $data = array();
                foreach ( $this->data['model'] as $key_name => $model ) {
                    if ( empty($model['variation']) && empty($model['specific']) && empty($model['custom'])) {
                        continue;
                    }
                    if ( !empty($key_name) ) {
                        $data[$key_name]     = array(
                            'id_country'            => $this->id_country,
                            'id_shop'               => $this->id_shop,
                            'name'                  => isset($key_name) ? $key_name : $model['name'],
                            'product_universe'      => isset($model['product_universe']) ? $model['product_universe'] : 0,
                            'product_type'          => isset($model['product_type']) ? serialize($model['product_type']) : NULL,
                            'variation_theme'       => isset($model['variation']) ? serialize($model['variation']) : NULL,
                            'specific_fields'       => isset($model['specific']) ? serialize($model['specific']) : 0,
                        );
                    }
                    if ( !empty($data) && !empty($override_post) ) {
                        $data[$key_name] = array_merge($data[$key_name], $override_post[$key_name]);
                    }
                    if ( !empty($model['id_model']) ) {
                        $data[$key_name]['id_model'] = $model['id_model'];
                    }
                }
                $result = $configuration->setModels($this->id_shop, $this->id_country, $data);
                echo $result ? "Success" : "Failure"; exit;
            }
            echo "Failure"; exit;
            
        case 'remove_model' :
            $id_model = isset($_POST['id_model']) ? $_POST['id_model'] : 0;
            $result = 0;
            if ( !empty($id_model) ) {
                $result = $configuration->deleteModels($this->id_shop, $this->id_country, $id_model);
            }
            echo $result ? "Success" : "Failure"; exit;
            
        default :
            $this->call_script('jquery_validate');
            $this->data['method'] = 'set_models';
            $this->data['models_selected'] = $configuration->getModels($this->id_shop, $this->id_country);
            $itemAttributes = $this->feedbiz->getAttributes($this->user_name, $this->id_lang, $this->id_shop);
            $itemFeatures   = $this->feedbiz->getFeatures($this->id_shop, $this->id_lang);
            if ( !empty($itemAttributes) ) {
                foreach ( $itemAttributes as $key => $item ) {
                    $this->data['attributes'][$key] = is_array($item['name']) ? $item['name'][$this->product_iso_code] : $item['name'];
                }
            }
            if ( !empty($itemFeatures) ) {
                foreach ( $itemFeatures as $key => $item ) {
                    $this->data['features'][$key] = is_array($item['name']) ? $item['name'][$this->product_iso_code] : $item['name'];
                }
            }
            break;
        }
    }
    
    public function component_profiles($override_post = null) {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = isset($_POST['method']) ? $_POST['method'] : '';
            $this->merge_form_data();
        }
        switch ($this->method) {
        case 'set_profiles' :
            if ( !empty($this->data['profile']) ) {
                $data = array();
                foreach ( $this->data['profile'] as $key_name => $profile ) {
                    if ( !empty($key_name) ) {
                        $data[$key_name]     = array(
                            'id_country'            => $this->id_country,
                            'id_shop'               => $this->id_shop,
                            'id_model'              => isset($profile['id_model']) ? $profile['id_model'] : NULL,
                            'is_default'            => NULL,
                            'name'                  => isset($profile['name']) ? $profile['name'] : NULL,
                            'out_of_stock'          => isset($profile['out_of_stock']) ? $profile['out_of_stock'] : 0,
                            'synchronization_field' => isset($profile['synchronization_field']) ? $profile['synchronization_field'] : 'reference',
                            'title_format'          => isset($profile['title_format']) ? $profile['title_format'] : 1,
                            'html_description'      => isset($profile['html_description']) ? $profile['html_description'] : 0,
                            'description_field'     => isset($profile['description_field']) ? $profile['description_field'] : 1,
                            'price_rules'           => isset($profile['price_rules']) ? serialize($profile['price_rules']) : NULL,
                        );
                    }
                    if ( !empty($data) && !empty($override_post) ) {
                        $data = array_merge($data, $override_post);
                    }
                    if ( !empty($profile['id_profile']) ) {
                        $data[$key_name]['id_profile'] = $profile['id_profile'];
                    }
                }
                $result = $configuration->setProfiles($this->id_shop, $this->id_country, $data);
                echo $result ? "Success" : "Failure"; exit;
            }
            echo "Failure"; exit;
            
        case 'remove_profile' :
            $id_profile = isset($_POST['id_profile']) ? $_POST['id_profile'] : 0;
            $result = 0;
            if ( !empty($id_profile) ) {
                $result = $configuration->deleteProfiles($this->id_shop, $this->id_country, $id_profile);
            }
            echo $result ? "Success" : "Failure"; exit;
            
        default : 
            $this->config->load('regex');
            $currencys = $this->config->item('currency_code');
            $this->call_script('jquery_validate');
            $this->data['currency_sign'] = $currencys[$this->currency];
            $this->data['profiles'] = $configuration->getProfiles($this->id_shop, $this->id_country);
            $this->data['models'] = $configuration->getModels($this->id_shop, $this->id_country);
            $this->data['method'] = 'set_profiles';
            break;
        }
    }
    
    public function component_categories() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
        }
        switch ($this->method) {
        case 'getSelectedAllCategory' :  
            $this->get_selected_all_category($configuration);
            break;
        case 'set_categories' :
            $this->merge_form_data();
            $data = isset($this->data['category']) ? $this->data['category'] : array();
            $result = $configuration->setSelectedCategories($this->id_shop, $this->id_country, $data);
            echo $result ? "Success" : "Failure";
            exit;
        default :
            $this->call_script('data_tree');
            $this->data['no_category'] = _get_message_code('12-00003', 'inner');
            $this->data['method'] = 'set_categories';
            $this->data['profiles'] = $configuration->getProfiles($this->id_shop, $this->id_country);
            break;
        }
    }
    
    public function component_carriers() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
        }
        switch ($this->method) {
        default :
            $this->data['method'] = 'set_carriers';
            break;
        }
    }
    
    public function component_conditions() {
        $this->security_check();
        //$configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        $this->load->model('marketplace_model');
        
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
            $this->merge_form_data();
        }
        switch ($this->method) {
        case 'set_conditions' :  
            if( !empty($this->data['cond']) ) {
                foreach($this->data['cond'] as $key => $val) {
                    $cond = $this->marketplace_model->getMarketplaceConditionByID($key);
                    if( !empty($val) ) { 
                        $this->feedbiz->setConditionMapping($this->user_name, $this->id_shop, $cond['id_marketplace'], $cond['condition_value'], $val); 
                    }
                    else{ 
                        $this->feedbiz->removeConditionMapping($this->user_name, $this->id_shop, $cond['id_marketplace'], $cond['condition_value']); 
                    }
                }
                echo "Success"; exit;
            }
            echo "Failure"; exit;
            
        default :
            $this->marketplace_model->getMarketplaceCondition($this->id_marketplace);
            $this->data['method'] = 'set_conditions';
            $mk_cond[strtolower($this->marketplace_prefix)] = $this->marketplace_model->getMarketplaceCondition($this->id_marketplace);
            $shop_condition[strtolower($this->marketplace_prefix)] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop, $this->id_marketplace);
            
            if ( !empty($shop_condition) && !empty($mk_cond) ) {
                foreach ($shop_condition[strtolower($this->marketplace_prefix)] as $shop_conditions) {
                    if( empty($shop_conditions['condition_value']) ) {
                        foreach ($mk_cond[strtolower($this->marketplace_prefix)] as $conditions) {
                            if( trim($conditions['condition_text']) === trim(ucfirst(strtolower($shop_conditions['txt']))) ) {
                                $val = array(
                                    'id_marketplace' => $conditions['id_marketplace'],
                                    'condition_value' => $conditions['condition_value'],
                                    'name' => $shop_conditions['txt'],
                                );

                                $this->feedbiz->setConditionMapping($this->user_name, $this->id_shop, $val['id_marketplace'], $val['condition_value'], $val['name']);
                            }
                        }
                    }
                }
		
                $this->data['mk_condition'][strtolower($this->marketplace_prefix)] = $this->marketplace_model->getMarketplaceCondition($this->id_marketplace);	    
                $this->data['shop_condition'][strtolower($this->marketplace_prefix)] = $this->feedbiz->getConditionMapping($this->user_name, $this->id_shop, $this->id_marketplace);
            }
            break;
        }
    }
    
    public function component_attributes() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
        }
        switch ($this->method) {
        default :
            $this->data['method'] = 'set_attributes';
            break;
        }
    }
    
    public function component_reports() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
            $data['draw']   = 1;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['data']   = array();
        }
        switch ($this->method) {
        case 'get_reports' :
            $data = array();
                if ($this->input->post()) {

                    $values = $this->input->post();                    
                    $page = $values['draw'];
                    $limit = $values['length'];
                    $start = $values['start'];
                    $search = $values['search']['value'];

                    $sort = array();
                    $column = null;
                    foreach($values['order'] as $order_sort){
                        switch ($order_sort['column']){
                            case 0 : $column = "batch_id"; break;
                            case 5 : $column = "date_add"; break;
                        }
                        $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
                    }
                    $order_by = implode(', ', $sort);

                    $data['draw'] = $page;
                    $data['recordsTotal'] = $configuration->getReports($this->id_shop, $this->id_country, null, null, $order_by, $search, true);
                    $data['recordsFiltered'] = $data['recordsTotal'];

                    $reports= $configuration->getReports($this->id_shop, $this->id_country, $start, $limit, $order_by, $search, false);
                    $list_log = array();
                    foreach ($reports as $key => $report){
                        $list_log[$key] = $report;
                    }
                    $data['data']   = $list_log;
                }

                echo json_encode($data);
            exit;
        default :
            break;
        }
    }
    
    public function component_logs() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);

        $this->method   = isset($_POST['method']) ? $_POST['method'] : null;

        switch ($this->method) {
            case 'get_logs' :
                $data = array();
                if ($this->input->post()) {

                    $values = $this->input->post();
                    $page = $values['draw'];
                    $limit = $values['length'];
                    $start = $values['start'];
                    $search = $values['search']['value'];

                    $sort = array();
                    $column = null;
                    foreach($values['order'] as $order_sort){
                        switch ($order_sort['column']){
                            case 0 : $column = "date_upd"; break;
                        }
                        $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
                    }
                    $order_by = implode(', ', $sort);

                    $data['draw'] = $page;
                    $data['recordsTotal'] = $configuration->getLogs($this->id_shop, $this->id_country, null, null, $order_by, $search, true);
                    $data['recordsFiltered'] = $data['recordsTotal'];

                    $logs= $configuration->getLogs($this->id_shop, $this->id_country, $start, $limit, $order_by, $search, false);
                    $list_log = array();
                    foreach ($logs as $key => $log){
                        $list_log[$key] = $log;
                        $list_log[$key]['action_type'] = l($log['action_type']);
                    }
                    $data['data']   = $list_log;
                }

                echo json_encode($data);
                exit;

            case 'get_logs_details' :
                
                $data = array();
                if ($this->input->post()) {

                    $values = $this->input->post();
                    $page = $values['draw'];
                    $limit = $values['length'];
                    $start = $values['start'];
                    $search = $values['search']['value'];

                    $sort = array();
                    $column = null;
                    foreach($values['order'] as $order_sort){
                        switch ($order_sort['column']){
                            case 0 : $column = "reference"; break;
                        }
                        $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
                    }
                    $order_by = implode(', ', $sort);

                    $data['draw'] = $page;
                    $data['recordsTotal'] = $configuration->getLogProductsSkip($this->id_shop, null, null, $order_by, $search, true);
                    $data['recordsFiltered'] = $data['recordsTotal'];

                    $logs= $configuration->getLogProductsSkip($this->id_shop, $start, $limit, $order_by, $search, false);

                    $list_log = array();
                    foreach ($logs as $key => $log){
                        $list_log[$key]['batch_id'] = $log['batch_id'];
                        $list_log[$key]['reference'] = $log['reference'];
                        $list_log[$key]['message'] = l($log['message']);
                        $list_log[$key]['date_add'] = $log['date_add'];
                        $list_log[$key]['id_shop'] = $log['id_shop'];
                    }

                    $data['data']   = $list_log;
                }

                echo json_encode($data);
                exit;

            default :
                $this->data['method'] = 'get_logs';
                break;
        }
    }
    
    public function component_order_report() {
        $this->security_check();
        $configuration = new MarketplacesConfiguration($this->user_name, $this->marketplace_prefix);
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
            $data['draw']   = 1;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['data']   = array();
        }
        switch ($this->method) {
        case 'get_orders' :
            echo json_encode($data);
            exit;
        default :
            break;
        }
    }
    
    public function component_actions_product() {
        $this->security_check();

        switch ($this->method) {
        default :
            break;
        }
    }
    
    public function component_actions_offer() {
        $this->security_check();

        switch ($this->method) {
        default :
            break;
        }
    }
    
    public function component_actions_order() {
        $this->security_check();

        switch ($this->method) {
        default :
            break;
        }
    }
    
    public function component_actions_delete() {
        $this->security_check();

        switch ($this->method) {
        default :
            break;
        }
    }   
    
    public function component_schedule() {
        $this->security_check();

        if ($this->input->post()) {
            $this->method   = $_POST['method'];
        }
        $this->data['no_data'] = _get_message_code('23-00006', 'inner');
        $this->load->model('cron_model', 'cron');
        $this->load->model('component_model', 'component_model');

        switch ($this->method) {
            case 'ajax_update_cron_status' :
                $ext            = $this->input->post('ext');
                $cron_id        = $this->input->post('cid');
                $status         = $this->input->post('status') == 'true' ? 1 : 0;
                
                $this->cron->updateUserCron($this->id_user, $cron_id, $ext, $status);
            break;
            case 'ajax_get_serv_status' :
                echo json_encode(array('status' => "green", 'timestamp' => "Thu, 12 Nov 2015 04:18:31 +0000"));
            break;
            default :
                $this->data = $this->get_values();
                $list = $this->cron->getUserCronList($this->id_user, strtolower($this->marketplace_prefix), $this->ext, true);

                $countdown= array();
                foreach( $list as $l ) { 
                    $next_time = time();
                    $next_time = $this->cron->getTimeLastCronRun($this->id_user, $l['task_key']);

                    if ( $next_time <= time() )
                        $next_time = time();

                    $this->data['tasks'][]      = $l;
                    $countdown[$l['task_key']]  = $next_time - time();
                    $this->data['countdown']    = $countdown;
                }
                $this->data['allow_cron'] = $this->component_model->check_allow_cron($this->id_user, 'export_amazon.fr_synchronize');
                $this->data['not_menu'] = 1;
                $this->call_script('data_cooldown');
            break;
        }
    }
    
    public function component_error_resolutions() {
        $this->security_check();
        if ($this->input->post()) {
            $this->method   = $_POST['method'];
            $this->type     = $this->input->post('action_type');
            $values         = $this->input->post();
            file_put_contents(dirname(__FILE__)."/../../../assets/error_show.txt", print_r($values, true));
            $this->load->model('component_model', 'component_model');
            require_once dirname(__FILE__) . '/../../libraries/Amazon/classes/amazon.error.resolution.php';  
            $this->amazon_err = new AmazonErrorResolution($this->user_name);
        }

        $this->data = $this->get_values();
        switch ($this->method) {
            case 'test' :
                $sort       = array();
                $error_log  = array();
                $search     = array();
                $order_by   = '';

                $msgs       = $this->component_model->get_error_resolutions($this->marketplace_prefix, null, $this->iso_code);
                $page       = isset($values['draw']) ? $values['draw'] : 1;
                $limit      = isset($values['length']) ? $values['length'] : 0;
                $start      = isset($values['start']) ? $values['start'] : 0;
                if ( !empty($values['order']) ) {
                    foreach( $values['order'] as $order_sort ) {
                        switch ( $order_sort['column'] ) {
                            case 1 : $column = "asrl.result_message_code"; 
                            break;
                        }
                        $sort[] = $column . ' ' . $order_sort['dir'];
                    }

                    if ( isset($values['search']['value']) && !empty($values['search']['value']) ){
                        if( preg_match('/sku=/',$values['search']['value']) ) {
                            $search['sku'] = str_replace('sku=', '', $values['search']['value']);
                        } else {
                            $search['all'] = $values['search']['value'];
                        }
                    }

                    $order_by = implode(', ', $sort);
                    
                    $data['draw'] = $page;
                    $data['recordsTotal'] = $this->amazon_err->get_amazon_error_log($this->id_country, $this->id_shop, $this->type, null, null, $order_by, $search, true);
                    $data['recordsFiltered'] = $data['recordsTotal'];
                    $data['data'] = array();
                    $logs = $this->amazon_err->get_amazon_error_log($this->id_country, $this->id_shop, $this->type, $start, $limit, $order_by, $search);   
                    foreach ( $logs as $log ) {
                        $error_log = $log;                
                        if( isset($msgs[$log['code']]) && !empty($msgs[$log['code']]) ) {
                            $msg = $msgs[$log['code']];
                            $error_log['description'] = $msg['error_details'] ;
                            $error_log['allow_overide'] = (isset($msg['allow_overide']) && $msg['allow_overide'] == 1) ? 1 : 0;
                            $error_log['show_message'] = (isset($msg['show_message']) && $msg['show_message'] ==1) ? 1 : 0;
                            $error_log['resolutions'] = $msg['error_resolution'];
                            $error_log['others'] = $msg['other'];

                            // error type
                            switch ($msg['error_priority']) {
                                case AmazonErrorResolution::_CREATION:  $error_log['creation'] = true;  break;
                                case AmazonErrorResolution::_EDIT_ATTRIBUTE:  $error_log['edit_attribute'] = true;  break;
                                case AmazonErrorResolution::_RECREATE:  $error_log['recreate'] = true;  break;
                                case AmazonErrorResolution::_SOLVED:  $error_log['solved'] = true;  break;
                                case AmazonErrorResolution::_OVERRIDE:  $error_log['override'] = !empty($error_log['others']) ? $error_log['others'] : null;  break;
                            }                    
                        }                
                        array_push($data['data'], $error_log);
                    }
                    echo json_encode($data);
                }
            break;
            default :
                $this->data = $this->get_values();
                $this->data['not_menu'] = 1;
                $this->call_script('data_table');
            break;
        }
    }  
    
}
