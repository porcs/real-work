<?php

if ( !file_exists(dirname(__FILE__).'/component.class.php')) exit('No direct script access allowed');
if ( !file_exists(dirname(__FILE__).'/component.scheduled.class.php')) exit('No direct script access allowed');

if ( class_exists('Components_Controller') == FALSE ) 
    require_once(dirname(__FILE__) . '/component.class.php');
if ( class_exists('Scheduled_Components') == FALSE ) 
    require_once(dirname(__FILE__) . '/component.scheduled.class.php');
