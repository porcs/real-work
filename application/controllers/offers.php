<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class offers extends CI_Controller
{
    
    public $id_user;
    public $user_name;
    public $iso_code;
    public $id_shop;

    public function __construct()
    {
        // load controller parent
        parent::__construct();
        

        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');

        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz_Offers', array($this->user_name));
        
        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("offers", $this->language);
        $this->smarty->assign("label", "offers");
        $this->iso_code = mb_substr($this->language, 0, 2);
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
    
    public function set_default_shop()
    {
        if ($this->input->post()) {
            $value = $this->input->post();
            
            $shop = $this->feedbiz_offers->setDefaultShop($this->id_user, $value);
            
            if (!$shop) {
                $this->session->set_userdata('error', 'save_unsuccessful');
            } else {
                $this->session->set_userdata('message', 'Change default shop successful');
                $this->session->set_userdata('id_shop', $value['id_shop']);
                $this->session->set_userdata('shop_name', $shop);
            }
        }
        
        $this->load->library('user_agent');
        if ($this->agent->is_referral()) {
            redirect($this->agent->referrer());
        }
    }
    
    public function check_shop($id)
    {
        $this->authentication->validateAjaxToken();
        $output = 'false';
        
        if (isset($id) && !empty($id)) {
            if ($this->feedbiz_offers->checkShops($this->user_name, $id)) {
                $output = 'true';
            }
        }
        
       
        echo $output;
    }

//    function general_delete()
//    {
//        $output = '';
//        $table = array(
//            'category_selected',
//            'profile',
//            'profile_item',
//            'profile_price',
//            'rule',
//            'rule_item',
//            'rule_item_price_range',
//        );
//          
//        foreach ($table as $table_name)
//            if(!$this->feedbiz_offers->truncate_table($this->user_name, $table_name . " WHERE id_shop = " . $this->id_shop))
//                $output .= 'delete table - "' . $table_name . '" error!';
//        
//        echo json_encode(array( 'pass'=> !empty($output)? false : true, 'output' => $output ));
//        
//    }
    public function ajax_get_num_price()
    {
        $data = array();
        $price_data = $this->feedbiz_offers->getProfilePrice($this->user_name, $this->id_shop, $this->id_mode);
        
        if (isset($price_data) && !empty($price_data)) {
            echo sizeof($price_data);
        } else {
            echo '0';
        }
        exit;
    }
    public function price($popup = false)
    {
         $data = array();
        $price_data = $this->feedbiz_offers->getProfilePrice($this->user_name, $this->id_shop, $this->id_mode);
        
        if (isset($price_data) && !empty($price_data)) {
            $data['price'] = $price_data;
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        if ($popup) {
            $data['popup'] = $popup;
            $this->smarty->assign('allow_progress_in_iframe', $popup);
        }
        $this->smarty->view('my_feeds/parameters/offers/price.tpl', $data);
    }
    
    public function price_save($popup=false)
    {
        if ($this->input->post()) {
            $values = $this->input->post('price') ;
          
            $session_key = date('Y-m-d H:i:s');
            $data_insert = array();
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if ((isset($value['name']) && !empty($value['name']))  && (isset($value["price_percentage"]) || isset($value["price_value"]))) {
                        if (isset($value['id_profile_price']) && !empty($value['id_profile_price'])) {
                            $data_insert[$key]['id_profile_price'] = $value['id_profile_price'];
                        }
                        
                        $data_insert[$key] = $value;
                        $data_insert[$key]['id_shop'] = $this->id_shop;
                        $data_insert[$key]['id_mode'] = $this->id_mode;
                        $data_insert[$key]['date_add'] = $session_key;
                    }
                }
                $result = $this->feedbiz_offers->saveProfilePrice($this->user_name, $data_insert);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        }
    
        if ($popup) {
            redirect('my_feeds/parameters/profiles/category/'.($popup+1));
        } else {
            redirect('my_feeds/parameters/offers/price');
        }
    }
    
    public function price_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteProfilePrice($this->user_name, $id, $this->id_shop, $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    public function rules()
    {
        $data = array();
        $this->lang->load("my_feeds", $this->language);
        //Rules
        $rule_data = $this->feedbiz_offers->getRules($this->user_name, $this->id_shop, $this->id_mode);

        if (isset($rule_data) && !empty($rule_data)) {
            $data['rules'] = $rule_data;
        }
        
        //Supplier
        $supplier = $this->feedbiz_offers->getSuppliers($this->user_name, $this->id_shop);
        if (isset($supplier) && !empty($supplier)) {
            $data['supplier'] = $supplier;
        }
        
        //Manufacturers
        $manufacturer = $this->feedbiz_offers->getManufacturers($this->user_name, $this->id_shop);
        if (isset($manufacturer) && !empty($manufacturer)) {
            $data['manufacturer'] = $manufacturer;
        }
        
         //Price
        $price = $this->feedbiz_offers->getProfilePrice($this->user_name, $this->id_shop, $this->id_mode);
        if (isset($price) && !empty($price)) {
            $data['price'] = $price;
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        $this->smarty->view('my_feeds/parameters/offers/rules.tpl', $data);
    }
    
    public function rules_save()
    {
        if ($inputs = $this->input->post()) {
            $values = $this->input->post('rule') ;
           
            $session_key = time();
            $data_insert = $values;
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if ((isset($value['name']) && !empty($value['name']))) {
                        $data_insert[$key]['date_add'] = $session_key;
                        $data_insert[$key]['id_shop'] = $this->id_shop;
                        $data_insert[$key]['id_mode'] = $this->id_mode;
                    }
                }
                    
                $result = $this->feedbiz_offers->saveRules($this->user_name, $data_insert);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        }
    
        helper_redirect('my_feeds/parameters/', isset($inputs[$inputs['save']]) ? $inputs[$inputs['save']] : 'rules');
    }
    
    public function rules_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteRules($this->user_name, $id, $this->id_shop, $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    public function rules_item_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteRulesItem($this->user_name, $id, $this->id_shop, $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    public function rules_item_price_range_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteRulesItemPriceRange($this->user_name, $id, $this->id_shop, $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    public function exportOfferList()
    {
        $product = $this->feedbiz_offers->exportOfferList($this->user_name, $this->id_mode, 1);
        echo json_encode($product);
    }
    
    public function get_token()
    {
        $user_data = $this->authentication->user($this->id_user)->row();
        $token = $this->feedbiz_offers->get_token($user_data->user_name);
        
        if (isset($token) && !empty($token)) {
            echo($token);
        }
    }
}
