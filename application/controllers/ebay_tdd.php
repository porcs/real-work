<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/ebay.php';

class Ebay_tdd extends Ebay 
{
    public function __construct() 
    {
        parent::__construct();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function index() {
        $list = array(
            'checkShop' => array(
                'id_shop' => 'INTEGER',
                'shop_name' => 'STRING'
            ),
            'checkLogin' => array(
                'user_name' => 'STRING'
            ),
        );
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function show_source($test, $expected_result) {
        $print = array(
            $test,
            $expected_result
        );
        echo "<pre>", print_r($print, true), "</pre>";
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkShopTest($package, $id_shop = 1, $shop_name = 'Eclairage Design') 
    {
        $test = $this->id_shop;
        $expected_result = $id_shop;
        $test_name = 'Function checkShop $id_shop';
        echo $this->show_source($test, $expected_result).
             $this->unit->run($test, $expected_result, $test_name);
        
        $test = $this->shop_default;
        $expected_result = $shop_name;
        $test_name = 'Function checkShop $shop_name';
        echo $this->show_source($test, $expected_result).
             $this->unit->run($test, $expected_result, $test_name);
        exit();
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkLoginTest($package, $user_name = 'u00000000000342') 
    {
        $test = $this->user_name;
        $expected_result = $user_name;
        $test_name = 'Function checkLogin $user_name';
        
        echo $this->show_source($test, $expected_result).
             $this->unit->run($test, $expected_result, $test_name);
        exit();
    }
}