<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
define('POPUP_STEP_1', '1');
define('POPUP_STEP_2', '2');
define('POPUP_STEP_3', '3');
define('POPUP_STEP_4', '4');
define('POPUP_STEP_5', '5');
define('POPUP_STEP_FIN', 'fin');
define('MAX_PROD', 50000);
class dashboard extends CI_Controller
{

    public function __construct()
    {
             
        // load controller parent
        parent::__construct();
        
        $this->id_user = $this->session->userdata('id');
        $this->user_name = $this->session->userdata('user_name');
        
        if ((!isset($this->user_name) || empty($this->user_name))
            || (!isset($this->id_user) || empty($this->id_user))) {
            redirect('users/login');
        }
        //echo '<pre>';
        //print_r($this->session->userdata('support_employee_info'));exit;

        //1. library
        $this->load->library('form_validation');
        $this->load->library('authentication');
        if ($this->user_name!='') {
            $this->load->library('FeedBiz', array($this->user_name));
        }
        $this->load->library('Cipher',
            array('key'=>$this->config->item('encryption_key')));
        
        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("my_feeds", $this->language);
        $this->lang->load("dashboard", $this->language);
        $this->smarty->assign("label", "my_feeds");
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->db_type = $this->config->item('backend_db');
        //load Module
        $this->load->model('customer_support_model');
        //echo '<pre>';
        //print_r($this->customer_support_model->get_support_employee3());exit;
        $o = $this->session->userdata('dashboard_sum_product');
        if (!empty($o)) {
            $this->sum_products = $o;
        }
    }
        
    public function index($command = null)
    {
        $last_time = 0;
//         function microtime_float($line=0)
//            {
//                global $last_time;
//                list($usec, $sec) = explode(" ", microtime());
//                $dif =  (  (float)$usec + (float)$sec) - $last_time  ;
//                echo $line. '|'.((float)$usec + (float)$sec).'|'.$dif."\n<br>" ;
//                $last_time = (float)$usec + (float)$sec;
//            }
        //echo $this->session->userdata['language'];
        if ($this->user_name=='') {
            $this->id_user = $this->session->userdata('id');
            $this->user_name = $this->session->userdata('user_name');
            
            $this->load->library('FeedBiz', array($this->user_name));
        }
         
        $data = array();
        $this->load->model('my_feed_model', 'my_feed_model');
        $mode_default = $this->my_feed_model->get_default_mode($this->id_user);
        $shop = $this->feedbiz->getDefaultShop($this->user_name);
            
        $offer = $this->my_feed_model->get_next_time_action_multi_key(
            $this->id_user,
            array('import_offer', 'amazon.%_synchronize_offer', 'export_ebay.%_offers'));
        $feed_offer = $offer['import_offer'];
        $amazon_offer = $offer['amazon.%_synchronize_offer'];
        $ebay_offer = $offer['export_ebay.%_offers'];
        
//        $feed_offer = $this->my_feed_model->get_next_time_action($this->id_user,'import_offer');
//        $amazon_offer = $this->my_feed_model->get_next_time_action($this->id_user,'amazon.%_synchronize_offer');
//        $ebay_offer = $this->my_feed_model->get_next_time_action($this->id_user,'export_ebay.%_offers');

        $data['countdown']['fb'] = $feed_offer;
        $data['countdown']['amazon'] = $amazon_offer;
        $data['countdown']['ebay'] =  $ebay_offer;
        $data['countdown']['count'] = 0;
        foreach ($data['countdown'] as $k=>$ch) {
            if ($ch!=-1 && $k!='count') {
                $data['countdown']['count']++;
            }
        }
        if ($data['countdown']['count']!=0) {
            $data['countdown']['count'] = 12/$data['countdown']['count'];
        } else {
            $data['countdown']['count'] = 4;
        }
        if (isset($shop['id_shop'])) {
            $id_shop = $shop['id_shop'];
            $id_mode = isset($mode_default['mode'])?$mode_default['mode']:1;
            $this->id_shop = $id_shop;
            $this->id_mode = $id_mode;
            $category = $this->feedbiz->checkSelectedCategoriesOffer($this->user_name, $id_shop, $id_mode);
           
            if (isset($category) && $category > 0) {
                $data['category'] = $category ;
            }
            
//                $cate = $this->my_feed_model->get_categories($this->id_user); 
//                $index = 'FEED_CATEGORY'; 
//                if(isset($cate[$index])){
//                    $val = $cate[$index]['value'];
//                    $out = unserialize( base64_decode($val));
//                }else{ 

//                    $today  = date('Y-m-d H:i:s', time());
//                    $id_user = $this->id_user;
//                    $mode =  array(
//                        'id_customer' => $id_user,
//                        'name' => $index ,
//                        'config_date' => $today,
//                        'value' => base64_encode(serialize($out))
//                    ); 
//                    $this->my_feed_model->set_general($mode) ;
//                } 
            //best categories

                if (!isset($this->sum_products)) {
                    $out = $this->sum_products =
                        $this->feedbiz->getSubArrCategories($this->user_name,$this->id_shop );
//                   $this->session->set_userdata('dashboard_sum_product',$this->sum_products);
                } else {
                    $out = $this->sum_products;
                }
            $data['best_cat'] = $out;
            unset($data['best_cat']['sum']);
            unset($data['best_cat']['all_sum']);
            $data['active_prod'] =  $out['sum'];
                
            $data['all_prod'] = $out['all_sum']>MAX_PROD?MAX_PROD:$out['all_sum'];
            $data['use_percent'] = number_format(($out['sum']/MAX_PROD)*100, 2);
            $data['degree'] = ceil(-1*(180 - ($data['use_percent']*1.8)));
            $data['use_percent_all'] = number_format(($out['all_sum']/MAX_PROD)*100, 2);
            $data['degree_all'] = ceil(-1*(180 - ($data['use_percent_all']*1.8)));
            $data['max_number_products'] = MAX_PROD;
            //end best categories
            //Activities log
                $data['logs'] = $this->feedbiz->getImportLog($this->user_name, 2);
//              
                $this->load->model('paypal_model', 'paypal');
            $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
            foreach ($offerUserPackage as $pk) {
                if ($pk['id_offer_pkg']==2) {
                    $data['amazon_btn'] = true;
                }
                if ($pk['id_offer_pkg']==3) {
                    $data['ebay_btn'] = true;
                }
            }
            $data['amazon']=$data['ebay']=array();
            if (isset($data['amazon_btn']) && $data['amazon_btn']) {
                //                    $data['amazon'] = $this->feedbiz->get_market_dashboard_log($this->id_shop,'amazon');
            }
            if (isset($data['ebay_btn']) &&  $data['ebay_btn']) {
                //                    $this->load->library('ObjectBiz', array($this->user_name));
//                    $this->objectbiz->checkDB('ebay_statistics_log'); 
//                    $data['ebay'] = $this->feedbiz->get_market_dashboard_log($this->id_shop,'ebay');
            }
                
            $this->load->model('paypal_model', 'paypal_model');
            foreach ($data['logs'] as $d) {
                if ($d['no_total']) {
                    $url = 'my_feeds/batches';
                    $data['activity'][]  =
                        array('id'=>$d['batch_id'],
                            'type'=>$this->lang->line('Import').' '.$this->lang->line($d['type']).' '.$this->lang->line('from').' "'.$d['shop'].'"',
                            'time'=>$this->my_feed_model->dateDiff(strtotime($d['datetime']), time()),
                            'url'=>$url);
                }
            }
            
            foreach ($data['amazon'] as $d) {
                $site_info = $this->paypal_model->get_site_by_id($d['id_country']);
                $url = 'amazon/logs/'.$site_info['id_country'];
//                $site = $site_info['domain'];
                $site='Amazon';
                $data['activity'][]  =
                    array('id'=>$d['batch_id'],
                        'type'=>$this->lang->line('Export to').' '.$site,
                        'time'=>$this->my_feed_model->dateDiff(strtotime($d['date_add']), time()),
                        'url'=>$url);
            }
            foreach ($data['ebay'] as $d) {
                $site_info = $this->paypal_model->get_site_by_id($d['id_site'], true);
                $url = 'ebay/statistics_products/'.$site_info['id_country'];
//                $site = $site_info['domain'];
                $site='eBay';
                $data['activity'][]  =
                    array('id'=>$d['batch_id'],
                        'type'=>$this->lang->line('Export').' '.$this->lang->line($d['type']).' '.$this->lang->line('to').' '.$site,
                        'time'=>$this->my_feed_model->dateDiff(strtotime($d['date_add']), time()),
                        'url'=>$url);
            }
            //end Activities log
            //Error last entries
            $list = $this->feedbiz->get_log_product($this->user_name, null, null, null, null, null, $data['all_prod'], true);
            $index = 'error_prod';
            foreach ($list as $log) {
                $log['message'] = str_replace('Product', 'Products', $log['message']);
                if (!isset($data[$index][$log['message']])
                            && isset($log['percent'])) {
                    $perc =  $log['percent'];
                    $perc = $perc>100?100:$perc;
                    $data[$index][$log['message']]=
                                array('num'=>0,
                                    'percent'=>$perc,
                                    'type'=>($log['severity']==1?'warning':'error'));
                    $data[$index][$log['message']]['num']++;
                }
            }
            $list = $this->feedbiz->get_log_offer($this->user_name, null, null, null, null, null, $data['all_prod'], true);
            $index = 'error_offer';
            foreach ($list as $log) {
                if (!isset($data[$index][$log['message']])
                            && isset($log['percent'])) {
                    $perc =  $log['percent']  ;
                    $perc = $perc>100?100:$perc;
                    $data[$index][$log['message']]=
                                array(
                                    'num'=>0,
                                    'percent'=>$perc,
                                    'type'=>($log['severity']==1?'warning':'error'));
                    $data[$index][$log['message']]['num']++;
                }
            }
            //end Error last entries
            $this->load->library('FeedBiz_Orders', array($this->user_name));
            $market_orders = $this->feedbiz_orders->getNumberOrderInRange($this->user_name);
            if (isset($market_orders['month']['Other'])) {
                $market_orders['label']['Other'] = $this->lang->line('Other');
            }
            $data['orders_items'] = json_encode($market_orders);
            
            $market_orders_graph = $this->feedbiz_orders->getOrdersDataGraph($this->user_name);
            
            $month = json_decode($this->lang->line('month_json'), true);
            $or_gr = array();
            
            foreach ($market_orders_graph as $key => $v) {
                if (isset($v['label']) && !empty($v['label'])) {
                    if (sizeof($v['label'])==1) {
                        $market_orders_graph[$key]['label'][1]
                            =  $market_orders_graph[$key]['label'][0];
                        if (!empty($market_orders_graph[$key]['data'])
                            &&is_array($market_orders_graph[$key]['data'])) {
                            foreach ($market_orders_graph[$key]['data'] as $mk =>$dt) {
                                if (isset($market_orders_graph[$key]['data'][$mk][0])) {
                                    $market_orders_graph[$key]['data'][$mk][1]
                                        = $market_orders_graph[$key]['data'][$mk][0];
                                }
                            }
                        }
                    }
                }
                if (isset($market_orders_graph[$key]['label'])) {
                    foreach ($market_orders_graph[$key]['label'] as $k => $label) {
                        $label = explode(' ', $label);

                        if ($key=='month') {
                            $m = $label[1]*1;
                            $tm = $month[$m-1];
                            $market_orders_graph[$key]['label'][$k]
                                = implode(' ', array($label[0], $tm));
                        } else {
                            $m = $label[0]*1;
                            $tm = $month[$m-1];
                            $market_orders_graph[$key]['label'][$k]
                                = implode(' ', array($tm, $label[1]));
                        }
                    }
                }
            }
            $data['orders_graph'] = json_encode($market_orders_graph);
        }
        
        $prof_data = urlencode(json_encode(array(
            'username' => $this->user_name,
            'id' => $this->id_user)));
        ;
        //echo $data['category'];
        if ($this->db_type=='mysql') {
            if (!is_dir(USERDATA_PATH . $this->user_name)) {
                $data['profile'] = $prof_data;
            } else {
                $db = new Db($this->user_name, 'products', true);
                if (!$db->exist_db) {
                    $data['profile'] = $prof_data;
                } else {
                    if (!$db->db_table_exists('products_product', true)) {
                        $data['profile_product'] =$prof_data;
                        $data['profile']='';
                    }
                    if (!$db->db_table_exists('offers_product', true)) {
                        $data['profile_offer'] =$prof_data;
                        $data['profile']='';
                    }
                    if (!$db->db_table_exists('orders_orders', true)) {
                        $data['profile_order'] =$prof_data;
                        $data['profile']='';
                    }
                }
            }
        } else {
            if (!is_dir(USERDATA_PATH . $this->user_name)) {
                $data['profile'] = $prof_data;
            } else {
                $ext_db = '.db';
                if (defined('DB_VER') && DB_VER=='DB3') {
                    $ext_db = '.db3';
                }
                if (!file_exists(USERDATA_PATH . $this->user_name.'/products'.$ext_db)) {
                    $data['profile_product'] =$prof_data;
                    $data['profile']='';
                }
                if (!file_exists(USERDATA_PATH . $this->user_name.'/offers'.$ext_db)) {
                    $data['profile_offer'] =$prof_data;
                    $data['profile']='';
                }
                if (!file_exists(USERDATA_PATH . $this->user_name.'/orders'.$ext_db)) {
                    $data['profile_order'] =$prof_data;
                    $data['profile']='';
                }
            }
        }
        $f_step = $this->check_first_conf_step();
        if ($f_step!==true) {
            $data['f_step']=$f_step;
            if($f_step!=POPUP_STEP_FIN){
                $this->session->set_userdata('open_wizard',$f_step);
                $this->smarty->assign("open_wizard", $f_step);
            }else{
                $this->session->unset_userdata('open_wizard');
            }
        }
        
        $support_employee_id =
            isset($this->session->userdata['support_employee_id']) ?
            intval($this->session->userdata['support_employee_id']) : null;
        $data['support_employee_info1'] =
            $this->customer_support_model->get_support_employee($support_employee_id);
        $data['support_employee_info3'] =
            $this->customer_support_model->get_support_employee3();
        $data['rate_list'] = $this->customer_support_model->get_support_rate();
        $rated = $this->session->userdata('hitted');
        $data['rated'] = !empty($rated);
        if (isset($command) && !empty($command)) {
            if ($this->session->userdata('dashboard_cmd') != $command) {
                $this->session->set_userdata('dashboard_cmd', $command);
                $data['command'] = $command;
            } else {
            }
        }
        
        //mfa status  
        $mfa = $this->my_feed_model->get_mfa_status_users();
        $email = $this->session->userdata('user_email');
        $this->smarty->assign("user_secure", isset($mfa[$email]));
        $status = $this->my_feed_model->checkCurrentModuleVersion($this->id_user);
        if ($status===true) {
            $this->smarty->assign("user_need_update_module", $status);
        } else {
            $this->smarty->assign("user_need_update_module",
                sprintf($this->lang->line("Please update your Feed.biz module (ver. %s) on your shop, download in"), $status));
        }
        $this->smarty->view('dashboard.tpl', $data);
    }
    public function rateSupporter($score=0)
    {
        if (!is_numeric($score)) {
            echo 'Fail';
        } else {
            echo $this->customer_support_model->set_support_rate($score);
        }
    }
    
    public function support_employee_info_ramdom()
    {
        print_r(json_encode($this->session->userdata('support_employee_info')));
    }

    public function iframe_popup_conf($step = '')
    {
        $message = '11-00005';
        $data = array();
        if ($step == '') {
            $data['close_popup']=true;
        } elseif ($step=='next') {
            $message = '';
            $step = $this->check_first_conf_step();
        }
        
        switch ($step) {
            case POPUP_STEP_1:
                redirect('users/profile/'.$step);
                break;
            case POPUP_STEP_2:
                redirect('my_shop/configuration/'.$step.'/'.$message);
                break;
//            case POPUP_STEP_3:
//                redirect('my_feeds/parameters/offers/price/'.$step);
//                
//                break;
            case POPUP_STEP_3:
                redirect('my_feeds/parameters/profiles/category/'.$step);
                break;
            case POPUP_STEP_4:
                redirect('marketplace/configuration_popup/'.$step);
                break;
            
            default:
                redirect('marketplace/display_conf_link/9');
                break;
        }
    }
    
    public function check_first_conf_step()
    {
        $userdata = $this->authentication->user($this->id_user)->row();
        if ($userdata->user_first_name==''|| $userdata->user_las_name=='') {
            //step1
            return POPUP_STEP_1;
        }
        
        $config_data = $this->my_feed_model->get_general($this->id_user);
        foreach ($config_data as $key => $value) {
            if (strtolower($key) != "feed_mode") {
                $data[strtolower($key)] = unserialize(base64_decode($value['value']));
            }
        }
        if (!(isset($data['feed_biz']['verified'])
            && isset($data['feed_biz']['verified'])=="true")) {
            //step2
//            $this->session->set_userdata('dMsg',$this->lang->line('Please verify your website.')) ;
            return POPUP_STEP_2;
        }
        if (!$this->session->userdata('data_source_popup') && !$this->session->userdata('id_shop')) {
            $this->session->set_userdata('dMsg',
                $this->lang->line('Please import your feed now.')) ;
            return POPUP_STEP_2;
        } elseif (!$this->session->userdata('id_shop')) {
            $this->session->set_userdata('dMsg',
                $this->lang->line('Importing your feed are not successful. Please import feed again.').'(S0)') ;
            return POPUP_STEP_2;
        }
        
        //step3

        if (!isset($this->sum_products)) {
            $out = $this->sum_products = $this->feedbiz->getSubArrCategories($this->user_name);
//            $this->session->set_userdata('dashboard_sum_product',$this->sum_products);
        } else {
            $out = $this->sum_products;
        }
        $mode_default = $this->my_feed_model->get_default_mode($this->id_user);
        $shop = $this->feedbiz->getDefaultShop($this->user_name);
             
        if (isset($shop['id_shop'])) {
            $id_shop = $shop['id_shop'];
            $id_mode = isset($mode_default['mode'])?$mode_default['mode']:1;
            $category = $this->feedbiz->checkSelectedCategoriesOffer($this->user_name, null, $id_mode);
            $price = $this->feedbiz->getProfilePrice($this->user_name, $id_shop, $id_mode);
//            if( isset($price) && sizeof($price)  == 0 && (!isset($category) || $category == 0) ){ 
//                return POPUP_STEP_3;
//            }
        } else {
            $this->session->set_userdata('dMsg_DS0',
                $this->lang->line('Importing your feed are not successful. Please import feed again.').'(DS0)') ;
            return POPUP_STEP_2;
        }
        
        if ($out['sum']==0) {
            $this->session->set_userdata('dMsg_P0',
                $this->lang->line('Importing your feed are not successful. Please import feed again.').'(P0)') ;
            return POPUP_STEP_2;
        }
        
        if ((!isset($category) || $category == 0)) {
            return POPUP_STEP_3;
        }
        
        
        $this->load->model('paypal_model', 'paypal');
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
        if (sizeof($offerUserPackage)==0) {
            return POPUP_STEP_4;
        }
        //done
        return POPUP_STEP_FIN;
    }
}
