<?php

require_once dirname(__FILE__)."/eBay/eBayController.php";

class ebayv1 extends CI_Controller 
{
    use eBayController;

    const MARKETPLACE_NAME = 'eBay';
    const ID_MODE = 2;
    const MARKETPLACE_ID = 3;
    const DEBUG = 9889;
    const DEBUG_MODE = 0;
    const VERSION = '1.2.2';
    const DATE_APPROVED = '2016-05-17';
    const LAST_MODIFIER = '2016-11-29';




    public function __construct() 
    {
        parent::__construct();
        self::setData();
    }




    ////////////////////////////////////////////////////////////////////////////
    private function setData() 
    {
        $self = array(
            'session'     => array_merge(
                $this->session->all_userdata(), 
                $this->config->config
            ),
            'router'      => $this->router->uri->segments,
            'marketplace' => $this->session->userdata('menu')['eBay']['submenu'],
        );
        
        $startup    = $this->startup($self);
        $this->data = $startup();
    }




    ////////////////////////////////////////////////////////////////////////////
    public function __call($name, $args) 
    {
        $post = json_decode(file_get_contents("php://input"), true);
        //debug

        if ( isset($_GET['post'])) {
            $post = array(
                'file'      => isset($_GET['file']) ? $_GET['file'] : '',
                'case'      => isset($_GET['case']) ? $_GET['case'] : '',
                'mode'      => $_GET['mode'],
                'project'   => $_GET['project'],
            );
        }
        
        if ( !empty($post)) {
            ////////////////////////////////////////////////////////////////////
            //function
            $requirePath = function() use ($post)
            {
                if ( empty($post['file'])) {
                    $post['file'] = 'startup';
                }
                
                $name      = $post['file'];
                $pattern   = '%s%s%s%s';
                $ext       = '.php';
                $path      = Configuration::$path['lb_controller'];
                $file_path = sprintf($pattern, DIR_SERVER, $path, $name, $ext);
                $real_path = realpath($file_path);

                if ( file_exists($real_path)) {
                    require_once $real_path;
                }
                
                return $name;
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $getProject = function() use ($post)
            {
                if ( empty($post['project'])) {
                    $post['project'] = $this->data['function_call'];
                }
                
                $this->data['post'] = $post;
                
                return $post['project'];
            };



            $result  = array();
            $debug   = isset($_GET['debug']) ? true : false;
            $startup = $requirePath();
            $project = $getProject($post['project'] ? $post['project'] : null);
            $class   = new $startup($project, $this->data, $debug);
            
            switch( $post['mode']) 
            {
                case 'set_parameter':
                    $result = $class->set();
                    break;
                
                default:
                    $result = $class->get();
                    break;
            }
            
            echo json_encode($result);
            exit();
        }

        $this->smarty->view('ebay/container_templates.tpl', $this->data);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public function mappingFeatures()
    {
        $this->view(); 
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public function mappingFeatureValues()
    {
        $this->view(); 
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public function errorResolutions()
    {
        $this->view(); 
    }
}