<?php

class search extends CI_Controller {

    public function __construct() {
        
        parent::__construct();

        $this->load->library('authentication');
        $this->authentication->checkAuthen(); //check logged in and redirect to login page
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');

        if ((!isset($this->user_name) || empty($this->user_name)) && (!isset($this->id_user) || empty($this->id_user)))
            redirect('users/login');
        
        // id shop
        $this->id_shop = $this->session->userdata('id_shop');
        if (!isset($this->id_shop) || empty($this->id_shop)){
            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            $this->id_shop = $DefaultShop['id_shop'];
        }
        
        // Class name
        $this->class = $this->router->fetch_class();
        
        // id mode
        $this->id_mode = 1;

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->lang->load(strtolower($this->class), $this->language);
        $this->smarty->assign("lang", $this->language);
        $this->smarty->assign("label", strtolower($this->class));

        // Class name // Function name        
        $this->smarty->assign("class", ucwords($this->class));
        $this->smarty->assign("function", $this->lang->line($this->router->fetch_method()));
        
        if(!empty($this->user_name)){
            $this->load->library('FeedBiz', array($this->user_name));
        }

    }
   
    public function results(){
        
        $data = array();
        if($this->input->post()){
            $values = $this->input->post();
            $this->smarty->assign("search_string", trim($values['search_string']));

            // Menu
            if (isset($this->session->userdata['menu'])){
                $this->smarty->assign('menus', rawurlencode(json_encode($this->session->userdata['menu'])));
            }
            $log = $this->feedbiz->get_log_product($this->user_name, $this->id_shop, trim($values['search_string']), 'date_add', 5);
            $data['logs'] = $log;
            
            // Search from History

            // Message Table(MySql)

            // product log


    //        $out = "";
    //
    //        $sql = "show tables";
    //        $rs = $mysqli->query($sql);
    //        if($rs->num_rows > 0){
    //            while($r = $rs->fetch_array()){
    //                $table = $r[0];
    //                $out .= $table.";";
    //                $sql_search = "select * from ".$table." where ";
    //                $sql_search_fields = Array();
    //                $sql2 = "SHOW COLUMNS FROM ".$table;
    //                $rs2 = $mysqli->query($sql2);
    //                if($rs2->num_rows > 0){
    //                    while($r2 = $rs2->fetch_array()){
    //                        $colum = $r2[0];
    //                        $sql_search_fields[] = $colum." like('%".$search."%')";
    //                    }
    //                    $rs2->close();
    //                }
    //                $sql_search .= implode(" OR ", $sql_search_fields);
    //                $rs3 = $mysqli->query($sql_search);
    //                $out .= $rs3->num_rows."\n";
    //                if($rs3->num_rows > 0){
    //                    $rs3->close();
    //                }
    //            }
    //            $rs->close();
    //        }
    //
    //        return $out;
        }
        $this->smarty->view('search.tpl', $data);
    }
}
