<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class profiles extends CI_Controller
{
    
    public $id_user;
    public $user_name;
    public $iso_code;
    public $id_shop;
    public $id_mode;
    
    public function __construct()
    {
        // load controller parent
        parent::__construct();
        
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');

        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz_Offers', array($this->user_name));
        
        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("profiles", $this->language);
        $this->smarty->assign("label", "profiles");
        $this->iso_code = mb_substr($this->language, 0, 2);
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }
    
    public function configuration()
    {
        $data = array();
        
        //Profile
        $profile_data = $this->feedbiz_offers->getProfile(
            $this->user_name,
            $this->id_shop,
            $this->id_mode);
        
        if (isset($profile_data) && !empty($profile_data)) {
            $data['profiles'] = $profile_data;
        }
        
        //Rules
        $rules = $this->feedbiz_offers->getRules($this->user_name,
            $this->id_shop,
            $this->id_mode);
        if (isset($rules) && !empty($rules)) {
            $data['rules'] = $rules;
        }
        
        //Prices
        $prices = $this->feedbiz_offers->getProfilePrice(
            $this->user_name,
            $this->id_shop,
            $this->id_mode);
        if (isset($prices) && !empty($prices)) {
            $data['price'] = $prices;
        }
                
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }

        $this->smarty->view('my_feeds/parameters/profiles/configuration.tpl', $data);
    }
    
    public function configuration_save()
    {
        if ($inputs = $this->input->post()) {
            $values = $this->input->post('profile') ;
            
            $session_key = time();
            $data_insert = $values;
            
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if ((isset($value['name']) && !empty($value['name']))) {
                        $data_insert[$key]['date_add'] = $session_key;
                        $data_insert[$key]['id_shop']  = $this->id_shop;
                        $data_insert[$key]['id_mode']  = $this->id_mode;
                    }
                }
                
                $result = $this->feedbiz_offers->saveProfile($this->user_name, $data_insert);
                if (!$result) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        }
    
        helper_redirect('my_feeds/parameters/',
            isset($inputs[$inputs['save']]) ? $inputs[$inputs['save']] : 'profiles');
    }
    
    public function configuration_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteProfile(
            $this->user_name,
            $id,
            $this->id_shop,
            $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    public function configuration_item_delete($id)
    {
        if (!isset($id) || empty($id)) {
            echo "false";
        }
            
        if (!$this->feedbiz_offers->deleteProfileItem(
            $this->user_name,
            $id,
            $this->id_shop,
            $this->id_mode)) {
            echo "false";
        }
        
        echo "true";
    }
    
    private function _get_profiles()
    {
        $profiles = array();
        
        //Profile
        if (isset($this->id_mode) &&
            !empty($this->id_mode) &&
            $this->id_mode !=1) {
            $profiles = $this->feedbiz_offers->getProfile(
                $this->user_name,
                $this->id_shop,
                $this->id_mode);
        } else {
            $profile = $this->feedbiz_offers->getProfilePrice(
                $this->user_name,
                $this->id_shop,
                $this->id_mode);
            
            if (isset($profile) && !empty($profile)) {
                foreach ($profile as $key => $profile_value) {
                    $profiles[$key]['id_profile'] = $profile_value['id_profile_price'];
                    $profiles[$key]['is_default'] = 0;
                    $profiles[$key]['name'] = $profile_value['name'];
                }
            }
        }
        
        return $profiles;
    }
    private function get_sub_cat_tree($root_cat_id, $profiles)
    {
        $html = '';
        $arr_selected_categories =
            $this->feedbiz_offers->getSelectedCategories(
            $this->user_name,
            $root_cat_id,
            $this->id_shop);
        foreach ($arr_selected_categories as $asc) {
            $sub_html = ''.$this->get_sub_cat_tree($asc['id_category'], $profiles);
            $selected =
                $this->feedbiz_offers->getSelectedCategoriesProfileID(
                $this->user_name,
                $asc['id_category'],
                $this->id_mode,
                $this->id_shop);
            if (isset($selected) && !empty($selected)) {
                $this->smarty->assign('checked', true);
                $this->smarty->assign('selected', $selected['id_profile']);
            } else {
                $this->smarty->assign('checked', '');
                $this->smarty->assign('selected', '');
            }
            $this->smarty->assign('profile', $profiles);
            
            $this->smarty->assign('type', $asc['type']);
            $this->smarty->assign('id_category', $asc['id_category']);
            $this->smarty->assign('name', $asc['name']);
            $this->smarty->assign('sub_html', $sub_html);
            $html .= $this->smarty->fetch('amazon/CategoryGroupTemplate.tpl');
        }
        return $html;
    }
    
    public function category($popup=null)
    {
        $data = array();
        $profiles = $this->_get_profiles();
         
        if (isset($profiles) && !empty($profiles)) {
            $data['profile'] = $profiles;
        }
        
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        if (isset($popup)) {
            $data['popup'] = $popup;
            $this->smarty->assign('allow_progress_in_iframe', $popup);
        }
        
        $data['no_category'] = _get_message_code('12-00004', 'inner');
    
        $this->smarty->view('my_feeds/parameters/profiles/category.tpl', $data);
    }
    
    public function category_save($popup=null)
    {
        if ($this->input->post()) {
            $values = $this->input->post('category') ;
            $session_key = date('Y-m-d H:i:s', time());
            $data_insert = array();
            $this->id_mode = 1;//for all mode
            if (isset($values) && !empty($values)) {
                foreach ($values as $key => $value) {
                    if ((isset($value['id_category']) && !empty($value['id_category']))) {
                        $data_insert[$key] = $value;
                        
                        $data_insert[$key]['date_add'] = $session_key;
                    
                        if (!isset($value['id_profile']) || empty($value['id_profile'])) {
                            $data_insert[$key]['id_profile'] = 0;
                        }

                        $data_insert[$key]['id_shop'] = $this->id_shop;
                        $data_insert[$key]['id_mode'] = $this->id_mode;
                    }
                }
                
                if (isset($data_insert) && !empty($data_insert)) {
                    $result = $this->feedbiz_offers->saveCategory(
                        $this->user_name,
                        $data_insert,
                        $this->id_shop,
                        $this->id_mode);
                } else {
                    if (!$result = $this->feedbiz_offers->truncate_table(
                        $this->user_name,
                        'category_selected WHERE id_shop = '.$this->id_shop.
                        ' AND id_mode = '.$this->id_mode)) {
                        $this->session->set_userdata('error', 'save_unsuccessful');
                        if ($popup) {
                            redirect("dashboard/iframe_popup_conf/next", 'refresh');
                        } else {
                            redirect('my_feeds/parameters/profiles/category');
                        }
                    }
                }
                
                if (!isset($result)) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        } else {
            if (!$this->feedbiz_offers->truncate_table($this->user_name,
                'category_selected WHERE id_shop = ' . $this->id_shop)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                if ($popup) {
                    //redirect('marketplace/display_conf_link/'.($popup+1));
                    redirect("dashboard/iframe_popup_conf/next", 'refresh');
                } else {
                    redirect('my_feeds/parameters/profiles/category');
                }
            }
        }
        if ($popup) {
            redirect("dashboard/iframe_popup_conf/next", 'refresh');
            //redirect('marketplace/display_conf_link/'.($popup+1));
        } else {
            redirect('my_feeds/parameters/category');
        }
    }
    public function getSelectedAllCategory()
    {
        //$this->authentication->validateAjaxToken();
        $data = array();
        $html = '';
        $profiles = $this->_get_profiles();
        $roots = $this->feedbiz_offers->getAllRootCategories($this->user_name,
            $this->id_shop);
        
        $this->id_mode=1;//for all mode
        // var_dump($root); exit;

        
        //make root
        $arr_selected_categories = array();
        if (!empty($roots)) {
            foreach ($roots as $root) {
                $arr_selected_categories_res =
                    $this->feedbiz_offers->getSelectedCategories(
                        $this->user_name,
                        $root['id'],
                        $this->id_shop);
//                if(empty($arr_selected_categories_res)) {
//                    echo json_encode($html);
//                    exit;
//                }
                $arr_selected_categories[] = array(
                    'id_category'=>$root['id'],
                    'name'=>$root['name'],
                    'parent'=>0,
                    'type'=>'folder',
                    'child'=>sizeof($arr_selected_categories_res));
            }
        }
        
        
//            echo '<pre>' . print_r($profiles, true) , '</pre>';
        foreach ($arr_selected_categories as $asc) {
            $sub_html = ''.$this->get_sub_cat_tree($asc['id_category'], $profiles);
            $selected = $this->feedbiz_offers->getSelectedCategoriesProfileID($this->user_name, $asc['id_category'], $this->id_mode, $this->id_shop);
            //echo '<pre>' . print_r($selected, true) , '</pre>';
            if (isset($selected) && !empty($selected)) {
                $this->smarty->assign('checked', true);
                $this->smarty->assign('selected', $selected['id_profile']);
            } else {
                $this->smarty->assign('checked', '');
                $this->smarty->assign('selected', '');
            }
            $this->smarty->assign('profile', $profiles);
            
            $this->smarty->assign('type', $asc['type']);
            $this->smarty->assign('id_category', $asc['id_category']);
            $this->smarty->assign('name', $asc['name']);
            
            $this->smarty->assign('sub_html', $sub_html);
            $html .= $this->smarty->fetch('amazon/CategoryGroupTemplate.tpl');
        }
        
        echo $html;
    }
    
    public function getSelectedCategory($id_parent)
    {
        $html = '';
        $profile = $this->_get_profiles();
        $arr_selected_categories = $this->feedbiz_offers->getSelectedCategories($this->user_name, $id_parent, $this->id_shop);
         
        foreach ($arr_selected_categories as $asc) {
            $selected = $this->feedbiz_offers->getSelectedCategoriesProfileID($this->user_name, $asc['id_category'], $this->id_mode, $this->id_shop);
            if (isset($selected) && !empty($selected)) {
                $this->smarty->assign('checked', true);
                $this->smarty->assign('selected', $selected['id_profile']);
            }
            $this->smarty->assign('profile', $profile);
            
            $this->smarty->assign('type', $asc['type']);
            $this->smarty->assign('id_category', $asc['id_category']);
            $this->smarty->assign('name', $asc['name']);
            
            $html .= $this->smarty->fetch('amazon/CategoryGroupTemplate.tpl');
        }
//        echo $html;
        echo json_encode($html);
    }
    
   /*
    function getSelectedCategories()
    
    {
        $categories = array();
        $arr_categories = $this->feedbiz_offers->getCategories($this->user_name, $this->id_shop, $this->iso_code);
        $arr_selected_categories = $this->feedbiz_offers->getSelectedCategories($this->user_name, $this->id_shop, $this->id_mode);
        
        $selected = $this->addSubSelectedCategory($arr_selected_categories);

        foreach ($arr_categories as $key => $category)
        {
            $categories[$key]['name'] = $category['name'];
            $categories[$key]['id']   = $category['id'];

            if(isset($category['id_profile']) && ($category['id_profile'] != NULL))
                $categories[$key]['additionalParameters']['id_profile']   = $category['id_profile'];

            if(isset($category['child']) && !empty($category['child']))
            {
                $categories[$key]['type'] = 'folder';  
                foreach ($this->addSubCategory($category['child']) as $ckey => $child)
                    $categories[$key]['additionalParameters']['children'] = $this->addSubCategory($category['child']);
            }
            else
                $categories[$key]['type'] = 'item'; 
        }
                
//        echo '<pre>' . print_r(array('category' => $categories, 'selected' => $selected) , true). '</pre>';
        echo json_encode(array('category' => $categories, 'selected' => $selected));
    }
    
    function addSubCategory ($children)
    {
//        echo '<pre>' . print_r($children, true) . '</pre>';
        $childrens = array();
        foreach ($children as $ckey => $child)
        {
            $childrens[$ckey]['additionalParameters']['id'] = $ckey;
            
            if(isset($child['id_profile']) && !empty($child['id_profile']))
                $childrens[$ckey]['additionalParameters']['id_profile'] = (int)$child['id_profile'];
            
            $childrens[$ckey]['id'] = $child['id'];
            $childrens[$ckey]['name'] = $child['name'];
            if(isset($child['child']) && !empty($child['child']) )
            {
                $childrens[$ckey]['type'] = 'folder';
                $childrens[$ckey]['additionalParameters']['children'] = $this->addSubCategory($child['child']);
            }
            else
                $childrens[$ckey]['type'] = 'item';
            
        }
        
        return $childrens;
    }
    
    function addSubSelectedCategory ($datas, $parent = 0, $depth = 0)
    {
        if($depth > 1000) return ''; 
        $tree = array();
       
        if(isset($datas) && !empty($datas))
        
        foreach ($datas as $data)/* for($i=1, $ni=sizeof($datas); $i < $ni; $i++)
        {
            foreach ($data as $key => $value) 
            { 
                if($key != "id_profile")
                {
                    if($value['parent'] == $parent || $parent == 0)
                    {
                        if(isset($value['id']) && !empty($value['id']))
                        {
                            $tree[$value['id']]['name'] = $value['name'];
                            $tree[$value['id']]['type'] = $value['type']; 
                            $tree[$value['id']]['additionalParameters']['id']   = $value['id'];
                            
                            if($value['type'] == "item" && isset($data['id_profile']))
                                $tree[$value['id']]['additionalParameters']['id_profile']   = $data['id_profile'];
                            
                            if($child = $this->addSubSelectedCategory($datas, $value['id'], $depth+1))
                                $tree[$value['id']]['children'] = $child;
                        }
                    }
                }
            }
        }
        return $tree;
    }
    */
}
