<?php 

require_once dirname(__FILE__)."/../../libraries/Ebay/table.config.php";

trait eBayController 
{
    public function startup(&$self, $data = array(), $fn = array())
    {
        ////////////////////////////////////////////////////////////////////////
        //function
        $start = function($value) use (&$fn)
        {
            $fn[$value]();
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getSession'] = function($wanted) use (&$data)
        {
            //function
            return function($value, $key) use ($wanted, &$data)
            {
                if ( !empty($wanted[$value])) {
                    return $data[$key] = $wanted[$value];
                }
                
                $data[$key] = !empty($data[$key]) ? $data[$key] : null;
            };
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getSessions'] = function() use (&$self, $fn)
        {
            $getSession = $fn['getSession']($self['session']);
            array_walk(Configuration::$name_definitions, $getSession);
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getPackages'] = function() use (&$self, &$data)
        {
            $slice = array_slice($self['router'], 0);
            list($class, $function, $packages) = array_pad($slice, 3, null);
            
            $data = array_merge($data, array(
                'class_call'    => $class, 
                'function_call' => $function, 
                'packages'      => $packages
            ));
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getEnvironment'] = function() use (&$data)
        {
            if ( isset($data['language'])) {
                $data['iso_code'] = mb_substr($data['language'], 0, 2);
            }

            $data['id_mode']          = self::ID_MODE;
            $data['label']            = strtolower(self::MARKETPLACE_NAME);
            $data['marketplace_name'] = self::MARKETPLACE_NAME;
            $data['id_marketplace']   = self::MARKETPLACE_ID;
            $data['flag_country']     = 'Red-Cross';
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getCountry'] = function() use (&$self, &$data, $fn)
        {
            if ( isset($self['marketplace'][$data['packages']])) {
                $packages     = $self['marketplace'][$data['packages']];
                $getSession   = $fn['getSession']($packages);
                array_walk(Configuration::$name_definitions, $getSession);
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getSite'] = function() use (&$data, $fn)
        {
            $this->load->model('ebayv1_model', 'eBayModel');
            $this->modelcall = $model = $this->getModel($data);

            if ( !isset($data['id_site'])) {
                if ( $data['packages'] == self::DEBUG || self::DEBUG_MODE) {
                    $data['id_site'] = $model('getSiteFromPackages');
                    
                    if ( !isset($data['id_site'])) {
                        $data['id_site'] = $model('getConfiguration', 
                            'EBAY_SITE_ID'
                        );
                    }
                }

                $packages   = $model('getDataPackagesFromSite');
                $getSession = $fn['getSession']($packages);
                array_walk(Configuration::$name_definitions, $getSession);
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['extension'] = function($ext = '') use (&$data)
        {
            $data['flag_country'] =& $ext;
            
            //function
            return function($value) use (&$ext)
            {
                if ( $ext == '') {
                    return $ext = $value; 
                }

                $ext = sprintf('%s_%s', $ext, $value);
            };
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getFlag'] = function() use (&$data, $fn)
        {
            if ( !empty($data['ext'])) {
                $substr = explode('.', $data['ext']);
                array_walk($substr, $fn['extension']());
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getPages'] = function() use (&$data) 
        {
            $data['prev_page'] = $data['next_page'] = null;
            if (isset(Configuration::$prev_def[$data['function_call']])) {
                $data['prev_page'] = sprintf(
                        Configuration::$prev_def[$data['function_call']], 
                        base_url(), 
                        $data['packages']
                );
            }
            
            if (isset(Configuration::$next_def[$data['function_call']])) {
                $data['next_page'] = sprintf(
                        Configuration::$next_def[$data['function_call']], 
                        base_url(), 
                        $data['packages']
                );
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getDocument'] = function() use (&$data) 
        {
            _get_message_document($data['class_call'], $data['function_call']);
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getDefaultLang'] = function() use (&$data)
        {
            $data['langstore'] = array();
            
            if ( isset(Configuration::${$data['function_call']})) {
                //function
                $this->lang->load(strtolower(
                    str_replace(' ', '_', $data['marketplace_name'])), 
                    $data['language']
                );

                $load = function($lang) use (&$data) {
                    $data['langstore'][$lang] = $lang;

                    if ( $this->lang->line($lang)) {
                        $data['langstore'][$lang] = $this->lang->line($lang);
                    }
                };

                array_walk(Configuration::${$data['function_call']}, $load);
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['getLink'] = function() use (&$data)
        {
            //function
            $link = function(&$url = null) use (&$data)
            {
                if ( !empty($url)) {
                    $pattern = "%s%s/%s/%s";
                    $url = sprintf(
                        $pattern, 
                        $url, 
                        $data['class_call'],
                        $data['function_call'],
                        $data['packages']
                    );
                }
            };
            
            $link($data['base_link']);
            $link($data['cdn_link']);
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['debugMode'] = function() use (&$self)
        {
            if (self::DEBUG_MODE) {
                $self['session'] = array_merge(
                    $self['session'], 
                    Configuration::$usertest_definitions
                );
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['setHeader'] = function() use (&$data)
        {
            if (( self::DEBUG_MODE || $data['packages'] == self::DEBUG)
                && !defined('PHPUNIT_TEST')) {
                header('Access-Control-Allow-Origin: *');
                header('Access-Control-Allow-Headers: Content-Type');
                header('Access-Control-Allow-Methods: GET, POST');
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['checkLogin'] = function() use (&$data)
        {
            //redirect to login page
            if ( empty($data['user_name'])) {
                $this->authentication->checkAuthen();
                redirect('users/login', 'location');
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['checkShop'] = function() use (&$data)
        {
            //redirect to login page
            if ( empty($data['id_shop']) || empty($data['shop_default'])) {
                $this->authentication->checkAuthen();
                redirect('users/login', 'location');
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['checkTable'] = function() use (&$data)
        {
            $this->load->library('Ebay/ObjectBiz', array($data['user_name']));
            
            //function
            $checkDB = function($table)
            {
                $this->objectbiz->checkDB($table);
            };
            
            array_map($checkDB, Configuration::$tb_def);
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['checkPackages'] = function() use (&$self, &$data)
        {
            if ( $data['packages'] == self::DEBUG || self::DEBUG_MODE) {
                return;
            }

            //redirect to marketplace configuration page
            if ( !is_numeric($data['packages']) 
                || empty($self['marketplace'])
                || empty($self['marketplace'][$data['packages']])) {
                    redirect('marketplace/configuration', 'location');
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['checkSites'] = function() use (&$self, &$data)
        {
            if ( $data['packages'] == self::DEBUG || self::DEBUG_MODE) {
                return;
            }

            //redirect to marketplace configuration page
            if ( !isset($data['id_site']) || !is_numeric($data['id_site'])) {
                redirect('marketplace/configuration', 'location');
            }
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['initialJsonData'] = function() use (&$data)
        {
            return array(
                'lang'              => $data['langstore'],
                'base_link'         => $data['base_link'],
                'cdn_link'          => $data['cdn_link'],
                'prev_page'         => $data['prev_page'],
                'next_page'         => $data['next_page'],
                'id_site'           => $data['id_site'],
                'packages'          => $data['packages'],
            );
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $fn['assignValue'] = function() use (&$data, $fn)
        {
            $this->assign(
                array(
                    'current_page'  => $data['function_call'],
                    'lang'          => $data['language'], 
                    'flag_country'  => $data['ext'], 
                    'name_country'  => $data['country'],
                    'json_data'     => json_encode($fn['initialJsonData']())
                )
            );
        };




        ////////////////////////////////////////////////////////////////////////
        //function
        $startup = function() use (&$data, $start)
        {
            array_walk(Configuration::$init_method, $start);          
            return $data;
        };
        
        return $startup;
    }



    ////////////////////////////////////////////////////////////////////////////
    private function getModel(&$data) 
    {
        //function
        return function($method = null, $optional = null) use (&$data)
        {
            switch($method)
            {
                case 'getConfiguration' :
                    return $this->eBayModel->getConfiguration(
                        $data['user_id'],
                        $optional
                    );
                    break;

                case 'geteBayConfiguration' :
                    return $this->eBayModel->geteBayConfiguration(
                        $data['user_id'],
                        $data['id_site']
                    );
                    break;

                case 'getPackagesFromSite' :
                    return $this->eBayModel->getPackagesFromSite(
                        $data['id_marketplace'],
                        $data['id_site']
                    );
                    break;

                case 'getSiteFromPackages' :
                    return $this->eBayModel->getSiteFromPackages(
                        $data['id_marketplace'],
                        $data['packages']
                    );
                    break;

                case 'getDataPackagesFromSite' :
                    return $this->eBayModel->getDataPackagesFromSite(
                        $data['id_marketplace'],
                        $data['user_id'],
                        $data['id_site']
                    );
                    break;

                case 'getCountryFromPackages' :
                    return $this->eBayModel->getCountryFromPackages(
                        $data['packages']
                    );
                    break;
            }
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    public function assign() 
    {
        //function
        $assigns = function($assign)
        {
            foreach ($assign as $key => $value) {
                $this->smarty->assign($key, $value);  
            }
        };
        
        array_walk(func_get_args(), $assigns);
    }
}