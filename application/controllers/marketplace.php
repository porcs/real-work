<?php

class marketplace extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_general = $this->session->userdata('id_general');

        //set language
        $this->load->library('authentication');
        $this->authentication->checkAuthen(); //check logged in and redirect to login page
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("marketplace", $this->language);
        $this->smarty->assign("label", "marketplace");
        $this->iso_code = mb_substr($this->language, 0, 2);
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
        $this->session->unset_userdata('menu');
    }

    public function configuration()
    {
        $data = array();
        $this->load->model('paypal_model', 'paypal');
        $this->load->model('marketplace_model', 'marketplace');
        $this->load->model('currency_model', 'currency');

        $offerPackage = $this->marketplace->getMarketplaceOffer(); // display all marketplace
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true); // display site user select
        $currency = $this->currency->get_user_currency($this->id_user); // display currency
        $data['currency'] = $currency;

        if ($offerPackage) {
            foreach ($offerPackage as $field) {
                $data['marketplace'][$field['id_offer_pkg']] = $field;
                if ($field['submenu_offer_pkg'] == 1) {
                    $region = $this->marketplace->getMarketplaceRegion(); // display region Asia, Australia, Canada, Europe, International, South America, USA
                    foreach ($region as $reg) {
                        $subOfferQuery =
                            $this->marketplace->getSubMarketplaceOffer(
                                $field['id_offer_pkg'], $reg['id_region']);

                        if (isset($subOfferQuery) &&
                            !empty($subOfferQuery) &&
                            $subOfferQuery != '') {
                            if ($field['id_offer_pkg'] != $this->id_general) {
                                $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['name'] = $reg['name_region'];
                            }
                            
                            foreach ($subOfferQuery as $key => $field2) {
                                if ($field['id_offer_pkg'] != $this->id_general) {
                                    $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key] = $field2;
                                } else {
                                    $name_marketplace = $this->marketplace->getSubMarketplaceName($field2['sub_marketplace']);
                                    $data['general_sub_domain'][$name_marketplace]['name'] = $name_marketplace;
                                    $data['general_sub_domain'][$name_marketplace]['sub_marketplace'] = $field2['sub_marketplace'];
                                    $data['general_sub_domain'][$name_marketplace][$reg['id_region']]['name'] = $reg['name_region'];
                                    $data['general_sub_domain'][$name_marketplace][$reg['id_region']]['country'][$key] = $field2;
                                    //$data['general_sub_domain'][$field2['comments']]['name'] = $field2['comments'];   
                                    //$data['general_sub_domain'][$field2['comments']][$reg['id_region']]['name'] = $reg['name_region'];
                                    //$data['general_sub_domain'][$field2['comments']][$reg['id_region']]['country'][$key] = $field2;
                                }

                                foreach ($offerUserPackage as $user_package) {
                                    if ($user_package['id_package'] == $field2['id_offer_price_pkg']) {
                                        if ($field['id_offer_pkg'] == $this->id_general) {
                                            $data['general_sub_domain'][$name_marketplace]['selected'] = true;
                                            $data['general_sub_domain'][$name_marketplace][$reg['id_region']]['country'][$key]['chk'] = true;
                                            //$data['general_sub_domain'][$field2['comments']]['selected'] =  true;     
                                            //$data['general_sub_domain'][$field2['comments']][$reg['id_region']]['country'][$key]['chk'] = true; 
                                        } else {
                                            $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key]['chk'] = true;
                                            $data['marketplace'][$field['id_offer_pkg']]['open'] = true;
                                        }
                                    }

                                    if ($field['id_offer_pkg'] != $this->id_general) {
                                        $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key]['id_offer_price_pkg'] = $field2['id_offer_price_pkg'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //$this->get_id_general();
        $data['arrCurrency'] = $this->currency->get_currency();

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        
        //$this->smarty->assign("General", self::GENERAL);
        //$this->smarty->assign("IDGeneral", $this->id_general);
        $this->smarty->assign("id_general", $this->id_general);
        $this->smarty->view('marketplace/configuration.tpl', $data);
    }

//    public function get_id_general() {
//        $general = $this->marketplace->getMarketplaceOffer(self::GENERAL);
//        $this->id_general = isset($general[0]['id_offer_pkg']) ? $general[0]['id_offer_pkg'] : 0;
//    }

    public function get_general_country($id_marketplace, $sub_marketplace)
    {
        $data = array();
        $this->load->model('paypal_model', 'paypal');
        $this->load->model('marketplace_model', 'marketplace');
        $subOfferQuery = $this->marketplace->getSubMarketplaceOfferGeneral(
            $id_marketplace, $sub_marketplace);
        foreach ($subOfferQuery as $key => $field2) {
            $region = $this->marketplace->getMarketplaceRegion($field2['id_region']);
            $data['domain'][$field2['id_region']]['country'][$key] = $field2;
            $data['domain'][$field2['id_region']]['name'] =
                isset($region[0]['name_region']) ? $region[0]['name_region'] : $field2['id_region'];
        }
        echo $this->smarty->fetch('marketplace/configuration_template_field.tpl', $data);
    }

    public function configuration_popup($popup = false, $default_tab = '')
    {
        $data = array();
        $this->load->model('paypal_model', 'paypal');
        $this->load->model('marketplace_model', 'marketplace');
        $this->load->model('currency_model', 'currency');

        $offerPackage = $this->marketplace->getMarketplaceOffer();
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
        $currency = $this->currency->get_user_currency($this->id_user);
        $data['currency'] = $currency;


        if ($offerPackage) {
            foreach ($offerPackage as $field) {
                $data['marketplace'][$field['id_offer_pkg']] = $field;
                if ($field['submenu_offer_pkg'] == 1) {
                    $region = $this->marketplace->getMarketplaceRegion();

                    foreach ($region as $reg) {
                        $subOfferQuery = $this->marketplace->getSubMarketplaceOffer($field['id_offer_pkg'], $reg['id_region']);

                        if (isset($subOfferQuery) && !empty($subOfferQuery) && $subOfferQuery != '') {
                            $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['name'] = $reg['name_region'];

                            foreach ($subOfferQuery as $key => $field2) {
                                $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key] = $field2;
                                $id_offer_price_pkg = $this->marketplace->getMarketplaceOfferPrice($field['id_offer_pkg'], $field2['id_offer_sub_pkg']);

                                foreach ($offerUserPackage as $user_package) {
                                    if ($user_package['id_offer_pkg'] == $field['id_offer_pkg'] && $user_package['id_offer_sub_pkg'] == $field2['id_offer_sub_pkg']) {
                                        $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key]['chk'] = true;
                                        $data['marketplace'][$field['id_offer_pkg']]['open'] = true;
                                    }
                                }

                                foreach ($id_offer_price_pkg as $field3) {
                                    $data['marketplace'][$field['id_offer_pkg']]['sub_marketplace'][$reg['id_region']]['country'][$key]['id_offer_price_pkg'] = $field3['id_offer_price_pkg'];
                                }
                            }
                        }
                    }
                }
            }
        }

        $data['arrCurrency'] = $this->currency->get_currency();

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        if ($popup) {
            $this->smarty->assign('popup', $popup);
            $this->smarty->assign('allow_progress_in_iframe', $popup);
        } else {
            $popup = 3;
            $this->smarty->assign('popup', $popup);
        }
        if ($default_tab != '') {
            $this->smarty->assign('default_market', $default_tab);
        }
        $this->smarty->view('marketplace/configuration_popup.tpl', $data);
    }

    public function configuration_data($popup = false)
    {
        if ($this->input->post()) {
            $data = array();
            $this->authentication->checkAuthen(); //check logged in and redirect to login page
            $this->load->model('marketplace_model', 'marketplace');
            $this->load->model('currency_model', 'currency');

            $values = $this->input->post();
            $session_key = date('Y-m-d H:i:s');
            if (isset($values['marketplace'])) {
                foreach ($values['marketplace'] as $value) {
                    foreach ($value as $val) {
                        $data['id_users'] = $this->id_user;
                        $data['id_package'] = $val;
                        $data['option_package'] = 'offer';
                        $data['created_date_upkg_detail'] = $session_key;

                        $this->marketplace->setUserPackageDetail($data);
                    }
                }
                $this->session->set_userdata('message', 'save_successful');
            }

            $this->marketplace->deleteUserPackageDetail($this->id_user, $session_key, 'offer');



            if (isset($values['currency'])) {
                $result2 = $this->currency->update_user_currency($this->id_user, $values['currency']);

                if (!$result2) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                }
            }
        }
        $this->session->unset_userdata('menu');

        if ($popup) {
            if (isset($values['force_redirect_wizrd'])) {
                $this->load->model('paypal_model', 'paypal');
                $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
                foreach ($offerUserPackage as $pk) {
                    if ($pk['id_offer_pkg'] == 2) {
                        $allow['amazon'] = true;
                    }
                    if ($pk['id_offer_pkg'] == 3) {
                        $allow['ebay'] = true;
                    }
                }

                if ($values['force_redirect_wizrd'] == 'amazon' &&
                    isset($allow[$values['force_redirect_wizrd']])) {
                    redirect("amazon/platform/1", 'refresh');
                } elseif ($values['force_redirect_wizrd'] == 'ebay' &&
                    isset($allow[$values['force_redirect_wizrd']])) {
                    redirect("ebay/ebay_wizard", 'refresh');
                } else {
                    redirect("/marketplace/configuration_popup/4/" .
                        $values['force_redirect_wizrd'], 'refresh');
                }
            } else {
                redirect("dashboard/iframe_popup_conf/next", 'refresh');
            }
        } else {
            redirect('marketplace/configuration');
        }
    }

    public function display_conf_link($popup = false)
    {
        if ($popup) {
            $this->smarty->assign('popup', $popup);
        } else {
            $popup = 5;
            $this->smarty->assign('popup', $popup);
        }

        $this->load->model('paypal_model', 'paypal');
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
        $data = array();
        $this->authentication->checkAuthen();

        foreach ($offerUserPackage as $pk) {
            if ($pk['id_offer_pkg'] == 2) {
                $data['amazon_btn'] = true;
            }
            if ($pk['id_offer_pkg'] == 3) {
                $data['ebay_btn'] = true;
            }
        }
        $this->session->unset_userdata('dashboard_cmd');
        $this->smarty->view('marketplace/display_conf_link.tpl', $data);
    }

    public function search($array, $key, $value)
    {
        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                return true;
            }

            foreach ($array as $subarray) {
                if ($this->search($subarray, $key, $value)) {
                    return true;
                }
            }
        }
        return false;
    }

    // to test Markeplace Component
    public function test($marketplace = "cdiscount", $id_country = 10)
    {
        require_once dirname(__FILE__) . '/../libraries/Marketplaces/classes/marketplaces.configuration.php';
        require_once dirname(__FILE__) . '/../libraries/Marketplaces/classes/marketplaces.database.php';
        require_once dirname(__FILE__) . '/../libraries/Marketplaces/classes/marketplaces.install.php';

        //load library
        $this->load->library('FeedBiz', array($this->user_name));

        // id shop
        $this->id_shop = $this->session->userdata('id_shop');
        $this->shop_default = $this->session->userdata('shop_default');

        if (!isset($this->id_shop) || empty($this->id_shop) ||
            !($this->shop_default) || empty($this->shop_default)) {
            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            $this->id_shop = $DefaultShop['id_shop'];
            $this->shop_default = $DefaultShop['name'];
        }

        $install = new MarketplacesInstall($this->user_name, $marketplace);

        if ($install) {
            $Marketplace = new MarketplacesConfiguration($this->user_name, $marketplace);
            $return = $Marketplace->getMappingsFeatures($this->id_shop, $id_country);

            echo '<pre>' . print_r($return, true) . '</pre>';
            exit;
        }
    }
}
