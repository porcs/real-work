<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class users extends CI_Controller
{

    public $id_user;
    public $user_name;
    public $iso_code;
    public $mode_default;
    public $site_title = 'Feed.biz';
    private $privatekey = "6LcYi90SAAAAALfyOClp3G3m0KhgEZUpgNysBKvP";
    private $rem_time =2592000;//30days

    public function __construct()
    {
        // load controller parent
        parent::__construct();
//        $time = microtime();
//        $time = explode(' ', $time);
//        $this->start =  $time[1] + $time[0]; 
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->mode_default = $this->session->userdata('mode_default');
        
        //1. library
        $this->load->library('form_validation');
        $this->load->library('authentication');

        //helper
        $this->load->helper('form');

        //database
        $this->db_table = 'users';
        $this->load->database();

        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');

        $this->smarty->assign("lang", $this->language);
        $this->lang->load("user", $this->language);
        $this->smarty->assign("label", "user");
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->email_templates = 'email/' . $this->language . '/';
        $this->email_after_activate = 'after_activate.tpl';

        $this->load->model('email_sender_model', 'email_sender');
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }

    public function index()
    {
        $data['content_data'] = "User ID " . $this->id_user;
        $this->load->view('default', $data);
    }
    
    public function encrypt_decrypt($action, $string)
    {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '4da3240a0fb7a734d46f75f6c1307009';
        $secret_iv = '$Forum.Feed.Biz\=+*-/>/$Client.Feed.biz';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        
        if (version_compare(PHP_VERSION, '5.3.8', '>')) {
            echo 'Server has PHP 5.3.8 or above!';
            if ($action == 'encrypt') {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            } elseif ($action == 'decrypt') {
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }
        }
        return $output;
    }
    public function ajax_check_mfa_login()
    {
        $isAjax=isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        if (!$isAjax) {
            die('Access denied - not an AJAX request...');
        }
        $ref_host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
        if ($ref_host!=$_SERVER["HTTP_HOST"]) {
            die('Access denied - host are not correct');
        }
        $this->load->model('my_feed_model', 'user_model');
        $mfa = $this->user_model->get_mfa_status_users();
        $username = '';
        if ($this->input->post()) {
            $username = trim($this->input->post('email'));
            
            if (isset($mfa[$username])) {
                echo json_encode(array('result'=>true));
                return;
            }
        }
        echo json_encode(array('result'=>false ));
    }
    public function login()
    {
        //        $time = microtime();
//$time = explode(' ', $time);
//$time = $time[1] + $time[0];
//$finish = $time;
//$total_time = round(($finish - $this->start ), 4);
//echo 'Page generated in '.$total_time.' seconds.'; exit;
        $this->id_user = $this->session->userdata('id');
        if (!empty($this->id_user)) {
            redirect('dashboard', 'refresh');
        }
        $data = array();
//        include DIR_SERVER.'/libraries/ipb_forum_connect.php'; 

        if ($this->input->post()) {
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->session->userdata('new_register_email')) {
                $this->session->unset_userdata('new_register_email');
            }
            if ($this->form_validation->run() == true) {
                $username = trim($this->input->post('email'));
                $password = $this->db->escape_str($this->input->post('password'));//mysqli_real_escape_string($this->input->post('password'));
                $userdata = $this->authentication->login($username, $password);
                if (!empty($userdata)) {
                    $this->load->model('my_feed_model', 'user_model');
                    $mfa = $this->user_model->get_mfa_status_users();
                    if (isset($mfa[$username])) {
                        include(dirname(__FILE__).'/../libraries/mfa/config.php');
                        $otp = trim($this->input->post('otp_login'));
                        if (empty($otp)) {
                            $data['error'] =$this->lang->line('Please try again. Your OTP is wrong.');
                            unset($userdata);
                        } else {
                            if (!TokenAuth6238::verify(base64_decode($mfa[$username]), $otp,10)) {
                                $data['error'] =$this->lang->line('Please try again. Your OTP is wrong.');
                                unset($userdata);
                            }
                        }
                    }
                }
            } else {
                $data['error'] = $this->lang->line('invalid_username');
            }
        } else {
            
//            if (!session_id()) {
//                session_name('ips4_IPSSessionFront');
//                session_set_cookie_params(0, '/', '.feed.biz');
//                session_start();
//            }
//            $session_id = session_id();
//            $session_forum = get_forum_session($session_id);

//            if(isset($session_forum['ips4_member_key']) && !empty($session_forum['ips4_member_key'])){
//                $username = $this->encrypt_decrypt('decrypt',$session_forum['ips4_member_key']);
////                echo $session_forum['ips4_member_key'].' '.$username;exit;
//                $password = null;
//                $login_as_forum = true;
//                $userdata = $this->authentication->login($username, $password, $login_as_forum);
//            } 
        }
        
        if (isset($userdata) && !empty($userdata) && !isset($userdata['error'])) {
            if (isset($userdata['user_default_language']) && !empty($userdata['user_default_language'])) {
                $language = $this->authentication->getLanguageNameByID($userdata['user_default_language']);
            }
            if(isset($userdata['manager_status'])&&$userdata['manager_status']==true){
                $session_data = array(
                    'm_id' => $userdata['id'],
                    'user_email' => $this->input->post('email'),
                    'user_name' => $userdata['user_name'],
                    'user_first_name' => $userdata['user_first_name'],
                    'user_las_name' => $userdata['user_las_name'],
                    'language' => !empty($language) ? $language : $this->language,
                    'logged' => true,
                    'time' => time(),
                    'ip' => $this->input->ip_address()
                );
                $this->session->set_userdata($session_data);
                redirect('manager', 'refresh');
                return;
            }
            
            $session_data = array(
                'id' => $userdata['id'],
                'user_email' => $this->input->post('email'),
                'user_name' => $userdata['user_name'],
                'user_first_name' => $userdata['user_first_name'],
                'user_las_name' => $userdata['user_las_name'],
                'language' => !empty($language) ? $language : $this->language,
                'logged' => true,
                'time' => time(),
                'ip' => $this->input->ip_address()
            );
            $this->load->model('cron_model', 'cron_model');
            $this->cron_model->setLastHost($userdata['id'], 0);
                
            $remember = $this->input->post('remember_me');
            if ($remember) {
                $time = isset($this->config->item['sess_expiration_remember_me'])?$this->config->item['sess_expiration_remember_me']:$this->rem_time;
                $uptime = isset($this->config->item['sess_time_to_update_remember_me'])?$this->config->item['sess_time_to_update_remember_me']:$this->rem_time;
                $session_data['new_expiration'] = $time;//30 days 
                $session_data['new_sess_time_to_update'] = $uptime;//30 days 
                $this->session->sess_expiration = $session_data['new_expiration'];
                $this->session->sess_time_to_update = $session_data['new_sess_time_to_update'];
            }
            $this->session->set_userdata($session_data);
            
            if (isset($userdata['user_name']) && !is_dir(USERDATA_PATH . $userdata['user_name'])) {
                redirect('dashboard', 'refresh');
            } else {
                if (isset($userdata['id'])) {

                    //set cookie for forum
//                    $dataencode = $this->encrypt_decrypt('encrypt', $this->input->post('email'));
//                    $_SESSION['ips4_member_key'] = $dataencode;
                    //include DIR_SERVER.'/libraries/ipb_forum_connect.php';
//                    if(!empty($_COOKIE['ips4_IPSSessionFront']))
//                    ipb_session_write_db($_COOKIE['ips4_IPSSessionFront'],$dataencode);
                    $this->session->unset_userdata('id_general');
                    $this->session->unset_userdata('menu');
                    $this->session->unset_userdata('mode_default');
                    $this->session->unset_userdata('amazon_mode_creation');
                    $this->session->unset_userdata('dashboard_sum_product');
                    $redirect = $this->session->userdata('original_redirect') . '';
                    if (!empty($redirect)) {
                        $this->session->unset_userdata('original_redirect');
                        redirect($redirect, 'refresh');
                    } else {
                        redirect('dashboard', 'refresh');
                    }
                } else {
                    $data['error'] = $this->lang->line($userdata['error']);
                }
            }
        } elseif (isset($userdata['error'])) {
            $data['error'] = $this->lang->line($userdata['error']);
        }
                    
        //Login with oAuth
        //Comprobate if the user request a strategy
        if ($this->uri->segment(3) == '') {
            $ci_config = $this->config->item('opauth_config');
            $opauth['strategies'] = array_keys($ci_config['Strategy']);
            $opauth['path'] = $ci_config['path'];

            foreach ($opauth['strategies'] as $strategies) {
                $strategies_path = $opauth['path'] . strtolower($strategies);
                $this->smarty->assign(strtolower($strategies) . "_path", $strategies_path);
                $this->smarty->assign(strtolower($strategies), $strategies);
            }
        } else {
            //Run login
            $config_data = $this->config->item('opauth_config');
            //print_r($config_data);
            $this->load->library('Opauth/Opauth', $config_data, true);
            //$this->opauth->run();
        }

        if ($this->session->userdata('error')) {
            $data['error'] = $this->session->userdata('error');
            if ($this->session->userdata('error') == 'session_error') {
                $data['error'] = $this->lang->line('Session Timeout Please Login again.');
            }
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
        if ($this->session->userdata('new_register_email')) {
            $data['new_register_email'] = $this->session->userdata('new_register_email');
        }
        if (!session_id()) {
            @session_start();
            unset($_SESSION['last_sig']);
        }
//        $this->smarty->assign('cdn_url', base_url('/'));
        $this->smarty->assign("cdn_url", (function_exists('cdn_url')?cdn_url():base_url()));
        $this->smarty->view('users/login.tpl', $data);
    }

    public function authenticate()
    {
        $is_first_time = false;

        //echo '<pre>' . print_r($this->input->post('opauth'), true) . '</pre>';
        if ($this->input->post('opauth')) {
            $profile_data = $this->authentication->unserialize_authen($this->input->post('opauth'));

            //echo '<pre>' . print_r($profile_data, true) . '</pre>';exit;
            if (!isset($profile_data) || empty($profile_data)) {
                $sys_email = $this->config->item('admin_email');
                $f_name = 'notify_default_email.html';
                $filePath = str_replace('\\', '/', FCPATH) . 'assets/template/' . $f_name;
                $reason = '<pre>' . print_r(unserialize(base64_decode($this->input->post('opauth'))), true) . '</pre>';
                //$logo = '<img src="'.base_url().'/nblk/images/feedbiz_logo.png" alt="Feed.biz">';
                $logo = '<img src="' . str_replace('http', 'https', base_url() . '/nblk/images/feedbiz_logob.png') . '" alt="feed.biz">';
                $msg = '@ ' . date('r') . '<br>reason : ' . $reason;
                $email = '';
                $email_template = file_get_contents($filePath);
                $email_template = str_replace("{#logo}", $logo, $email_template);
                $email_template = str_replace("{#msg}", $msg, $email_template);
                $email_template = str_replace("{#refemail}", $email, $email_template);
                $subject = '[Sys]Feed.biz oAuth Fail!';
                $this->email_sender->sendMail($sys_email, $subject, $email_template);


                $this->session->set_userdata('error', 'authenticate_error');
                redirect('users/login', 'refresh');
            }

            $id = $this->authentication->get_opauth_id($profile_data['user_auth_id']);

            if (!$id) {
                $is_first_time = true;
                $id = $this->authentication->save_opauth_data($profile_data);
                $this->session->set_userdata('first_login', true);
            }

            $user_data = $this->authentication->user($id)->row();
            
            if (isset($user_data->user_default_language) && !empty($user_data->user_default_language)) {
                $language = $this->authentication->getLanguageNameByID($user_data->user_default_language);
            }
                    
            $session_data = array(
                'id' => $id,
                'user_name' => $user_data->user_name,
                'user_provider' => $profile_data['user_provider'],
                'user_first_name' => (isset($profile_data['user_first_name'])) ? $profile_data['user_first_name'] : '',
                'user_las_name' => (isset($profile_data['user_las_name'])) ? $profile_data['user_las_name'] : '',
                'language' => !empty($language) ? $language : $this->language,
                'logged' => true,
                'time' => time(),
                'ip' => $this->input->ip_address()
            );
            $email = $user_data->user_email;

            $this->authentication->update_last_login($id);
            $this->session->set_userdata($session_data);
            $this->authentication->generateNewOTP($id);
            if ($is_first_time) {
                $this->load->model('customer_support_model', 'support');
                $this->support->inc_count_contact();
                $this->support->send_email2supporter();
                $support_email = $this->support->support_email;

                $b_url = base_url(); //'sismiracles.com';// 
                $im = ($b_url . '/nblk/images/feedbiz_logob.png');
                $logo = '<img src="' . ($im) . '" alt="feed.biz" />';
                $im = ($b_url . $this->session->userdata['support_employee_image']);
                $support_image = '<img src="' . ($im) . '" alt="feed.biz Supporter" width="80"/>';
                $im = ($b_url . '/nblk/images/signature_scan.gif');
                $support_signature = '<img src="' . ($im) . '" alt="feed.biz Supporter Signature" width="200" style="border: none" />';

//                $im = file_get_contents(getcwd().'/nblk/images/feedbiz_logob.png'); 
//                $logo = '<img src="data:image/png;base64,' . base64_encode($im) . '" alt="feed.biz" />';
//                $im = file_get_contents(getcwd().$this->session->userdata['support_employee_info']['image']); 
//                $support_image = '<img src="data:image/png;base64,' . base64_encode($im) . '" alt="feed.biz Supporter" width="80"/>';
                /*$email_data = array(
                    'sitename' => $this->site_title,
                    'identity' => $email,
                    'id' => $id,
                    'backend_link' => 'dashboard',
                    'logo' => $logo,
                    'support_name' => $this->session->userdata('support_employee_name'),
                    'support_image' => $support_image,
                    'support_signature' => $support_signature,
                );*/
                
                $this->smarty->assign('sitename', sprintf($this->site_title));
                $this->smarty->assign('identity', $email);
                $this->smarty->assign('logo', $logo);
                $this->smarty->assign('backend_link',  site_url('dashboard'));
                $this->smarty->assign('id',  $id);
                $this->smarty->assign('support_name',  $this->session->userdata('support_employee_name'));
                $this->smarty->assign('support_image',  $support_image);
                $this->smarty->assign('support_signature',  $support_signature);
                $message = $this->smarty->fetch($this->email_templates . $this->email_after_activate);

                //$message = $this->load->view($this->email_templates . $this->email_after_activate, $email_data, true);
                $this->email_sender->setSender($support_email);
                $this->email_sender->sendMail($email, $this->site_title . ' - ' . $this->lang->line('email_welcome_subject'), $message);

                redirect('dashboard', 'refresh');
                //redirect('users/profile','refresh');
                /* $data['profile'] = urlencode(json_encode(array('username' => $user_data->user_name,'id' => $user_data->id)));
                  //$this->smarty->view('users/authenticate.tpl', $data);
                  $this->smarty->view('dashboard.tpl', $data); */
            } else {
                //                if(!isset($profile_data['user_las_name']) || ($profile_data['user_las_name'])=='' || !isset($profile_data['user_first_name']) || ($profile_data['user_first_name'])=='' ){
//                    $this->session->set_userdata('first_login',true); 
//                    redirect('users/profile','refresh');
//                    exit;
//                }

                if (!is_dir(USERDATA_PATH. $user_data->user_name)) {
                    //                    $data['profile'] = urlencode(json_encode(array('username' => $user_data->user_name,'id' => $user_data->id)));
//                    //$this->smarty->view('users/authenticate.tpl', $data);  
//                    $this->smarty->view('dashboard.tpl', $data);  
                    redirect('dashboard', 'refresh');
                } else {
                    $redirect = $this->session->userdata('original_redirect') . '';
                    if (!empty($redirect)) {
                        $this->session->unset_userdata('original_redirect');
                        redirect($redirect, 'refresh');
                    } else {
                        redirect('dashboard', 'refresh');
                    }
                }
            }
        } else {
            $this->session->set_userdata('error', $this->lang->line('authenticate_error_1'));
            redirect('users/login', 'refresh');
        }
    }

    public function authenticate_delete_user()
    {
        //        $this->session->sess_destroy();
//        delete_cookie('id');  
        $id = $this->session->userdata('id');
        $user_data = $this->authentication->user($id)->row();
        $u = $user_data->user_name;
        if (!isset($id) || empty($id)) {
            echo "false";
        }

//        if($this->authentication->delete_user($id))
//        {
//            echo "true";
//        }
//        else
//        {
//            echo "false";
//        } 

        require_once APPPATH . 'libraries/Swift/swift_required.php';
        $sys_email = 'palmp.itriva@gmail.com';
        $arrEmail = array($sys_email);
        $f_name = 'notify_default_email.html';
        $user = $u;
        $reason = $this->input->get('reason');
        try {
            $filePath = str_replace('\\', '/', FCPATH) . 'assets/template/' . $f_name;
            $oriEmailTemplate = file_get_contents($filePath);
            $transport = Swift_SmtpTransport::newInstance();
            $b_url = base_url(); //'sismiracles.com';//
            $logo = '<img src="' . str_replace('http', 'https', $b_url . '/nblk/images/feedbiz_logob.png') . '" alt="feed.biz">';

            // Create the Mailer using your created Transport
            $mailer = Swift_Mailer::newInstance($transport);
            $msg = '@ ' . date('r') . '<br>user: ' . $user . '<br><br>email:' . $this->session->userdata('user_email') . '<br><br>reason: ' . $reason;
            if (!empty($arrEmail)) {
                foreach ($arrEmail as $email) {
                    $email_template = $oriEmailTemplate;
                    $email_template = str_replace("{#logo}", $logo, $email_template);
                    $email_template = str_replace("{#msg}", $msg, $email_template);
                    $email_template = str_replace("{#refemail}", $email, $email_template);

                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject('[Sys]Feed.biz Registration Error Report!')
                            //->setFrom(array($u_email=>$u_name))
                            ->setFrom(array($sys_email => $sys_email))
                            ->setTo($email)
                            ->setBody($email_template, 'text/html');

                    $mailer->send($swift_message);
                }
            }
        } catch (Exception $ex) {
        }

        exit;
    }

    public function forgotten_password()
    {
        $data = array();
        if ($this->input->post()) {
            $data_return = array();

            $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required');

            if ($this->form_validation->run() == true) {
                $data_return = $this->authentication->forgotten_password($this->input->post('email'));

                if (isset($data_return['message'])) {
                    //if there were no errors
                    $this->session->set_userdata('message', $this->lang->line($data_return['message']));
                    redirect("users/login", 'refresh'); //we should display a confirmation page here instead of the login page
                }
            } else {
                $data_return['error'] = 'email_require';
            }
        }

        // print_r($data_return['error']); exit;
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        } elseif (isset($data_return['error'])) {
            $data['error'] = $this->lang->line($data_return['error']);
            $this->session->unset_userdata('error');
        }




        $this->smarty->view('users/forgot_password.tpl', $data);
    }

    //reset password - final step for forgotten password
    public function reset_password($code = '')
    {
        if (!$code) {
            show_404();
        }

        $data = array();

        $user = $this->authentication->forgotten_password_check($code);

        if (isset($user->id)) {
            $this->smarty->assign('id', $user->id);
        }

        if (isset($user->user_email)) {
            $this->smarty->assign('email', $user->user_email);
        }

        if (!isset($user->id)) {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_userdata('error', $user['error']);
            redirect("users/forgotten_password", 'refresh');
        } else {
            if ($this->input->post()) {

                //if the code is valid then display the password reset form
                $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required');
                $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

                if ($this->form_validation->run() == false) {
                    //set the flash data error message if there is one
                    $data['error'] = 'form_validation_error';
                } else {
                    // do we have a valid request?
                    if ($user->id != $this->input->post('id') || $user->user_email != $this->input->post('email')) {
                        //something fishy might be up
                        $this->authentication->clear_forgotten_password_code($code);
                        $data['error'] = 'reset_password_validation_error';
                    } else {
                        //var_dump($this->input->post('id')); exit;
                        // finally change the password
                        $identity = $user->user_email;
                        $change = $this->authentication->reset_password($identity, $this->input->post('new'));
                        
                        //#Start change password forum.feed.biz
                        include DIR_SERVER.'/libraries/ipb_forum_connect.php';
                        forgetpassword($identity, $this->input->post('new'));
                        //#End change password forum.feed.biz

                        if (isset($change['message'])) {
                            //if the password was successfully changed
                            $this->session->set_userdata('message', $this->lang->line($change['message']));
                            redirect('users/login/', 'refresh');
                            //$this->logout();
                        }
                    }
                }
            }
        }

        if (isset($change['error'])) {
            $data['error'] = $this->lang->line($change['error']);
        }
        $this->smarty->assign('code', $code);
        $this->smarty->view('users/reset_password.tpl', $data);
    }

    public function logout()
    {
        if (!session_id()) {
            @session_start();
        }
        unset($_SESSION);
        session_destroy();
        $this->authentication->logout();
        $this->session->sess_destroy();
        delete_cookie('id');
        delete_cookie('ci_session');
        
        //#Start Clear Session login forum.feed.biz
            include DIR_SERVER.'/libraries/ipb_forum_connect.php';
        if (isset($_COOKIE['ips4_IPSSessionFront'])) {
            ipb_clear_session($_COOKIE['ips4_IPSSessionFront']);
        }
            //** logout forum.feed.biz
            //header('Location: https://forum.feed.biz/logout.php?url=client');
        //#End Clear Session login forum.feed.biz
        $domain = '.feed.biz';
        setcookie('ips4_IPSSessionFront', '', time()-60*60*24*7, '/', $domain);
        setcookie('ips4_member_id', '', time()-60*60*24*7, '/', $domain);
        setcookie('ips4_pass_hash', '', time()-60*60*24*7, '/', $domain);
        unset($_COOKIE['ips4_IPSSessionFront']);
        unset($_COOKIE['ips4_member_id']);
        unset($_COOKIE['ips4_pass_hash']);
        redirect('users/login', 'refresh');
    }
    
    public function language($language='english')
    {
        $lang = $this->authentication->getLanguageIDByName($language);
        if(empty($lang)){
            $language = 'english';
            $lang = $this->authentication->getLanguageIDByName($language);
            $this->session->set_userdata('language', $language);
        }
        $update_data = array('user_default_language' => $lang);
        if(!empty($this->id_user)){
            $this->authentication->update($this->id_user, $update_data);
        }
        $this->session->set_userdata('language', $language);
        //redirect('/');
    }

    public function profile($popup = false)
    {
        //get profile from users table
        //$data = array();
        $data = $this->authentication->checkAuthen(); //check logged in and redirect to login page
        //$this->authentication->setMarkRedirect();
        $user_data = $this->authentication->user($this->id_user)->row();

        //check user from database
        if (!isset($user_data) || empty($user_data)) {
            $this->session->set_userdata('error', 'session_error');
            redirect('users/login', 'refresh');
        }

        $csrf = $this->_get_csrf_nonce();

        foreach ($csrf as $key => $value) {
            $data['csrf']['key'] = $key;
            $data['csrf']['value'] = $value;
        }

        $user_data = (array)$user_data;

        //Account
        $this->db->where(array('id' => $this->id_user));
        $account = $this->db->get('customer')->row();
        $account_data = (array)$account;

        if (empty($account)) {
            $account_data = array_merge(array('id' => $this->id_user), $account_data);
        }

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        if ($popup !== false) {
            $this->smarty->assign('popup', $popup);
            $this->session->set_userdata('profile_popup', $popup);
            $user_data['first_login'] = true;
            //$data['popup'] = $popup;
        }
        $profile_image_path = '/assets/images/profile/100/';
        $user_data['image'] = file_exists(DIR_SERVER.$profile_image_path.$this->id_user.'.jpg') ? base_url().$profile_image_path.$this->id_user.'.jpg' : base_url() . '/assets/images/profile/empty-profile.png';
        
        $this->smarty->assign('profile', $user_data);
        $this->smarty->assign('account', $account_data);
        $this->smarty->view('users/profile.tpl', $data);
    }
    public function profile_image($popup = false)
    {
        //        $this->authentication->validateAjaxToken();
            //Image upload 
            $validate_dir = null;
        if (isset($_FILES['userfile']['name']) && !empty($_FILES['userfile']['name'])) {
            $path_img_profile = DIR_SERVER.'/assets/images/profile/';
            $path_img_profile_original = $path_img_profile.'original';
            $image_sizes = array(35, 50, 80, 100, 250);
            $folder_img_profile_all = array_merge(array('original'), $image_sizes);
                
            foreach ($folder_img_profile_all as $folder_img_profile_ele) {
                if (!file_exists($path_img_profile.$folder_img_profile_ele)) {
                    if (!is_writable($path_img_profile)) {
                        $validate_dir = $this->lang->line('Directory is not writable');
                        break;
                    } else {
                        mkdir($path_img_profile.$folder_img_profile_ele);
                    }
                }
            }
                
            if (empty($validate_dir)) {
                $config = array();
                $config['upload_path'] = $path_img_profile.'original';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = 1024 * 512;
                $config['encrypt_name'] = true;
                $this->load->library('upload', $config);
                $this->load->library('image_lib', $config);
                if ($this->upload->do_upload()) {
                    $data = $this->upload->data();
                    $config = array();
                    $config['image_library'] = 'gd2';
                    $config['source_image']    = $data['full_path'];
                    $config['create_thumb'] = true;
                    $config['maintain_ratio'] = true;
                    $config['create_thumb'] = false;
                        
                    foreach ($image_sizes as $image_size) {
                        $config['new_image'] = DIR_SERVER.'/assets/images/profile/'.$image_size.'/'.$this->input->post('id').'.jpg';
                        $config['width']    = $image_size;
                        $config['height']    = $image_size;
                        $this->image_lib->initialize($config);
                        $this->image_lib->resize();
                    }
                    $img_path = base_url() .'/assets/images/profile/250/'.$this->input->post('id').'.jpg';
                                //#Start set image,name forum.feed.biz
                                include DIR_SERVER.'/libraries/ipb_forum_connect.php';
                    ipb_update_image($this->input->post('email'), $img_path);
                    ipb_update_name($this->input->post('email'), $this->input->post('firstname'));
                                //#End set image forum.feed.biz
                        unlink($data['full_path']);
                    echo "<img src='".$img_path."'  class='icon-big-circle'>";
                } else {
                    $img_path = DIR_SERVER .'/assets/images/profile/250/'.$this->input->post('id').'.jpg';
                    if (file_exists($img_path)) {
                        $img_path = base_url() .'/assets/images/profile/250/'.$this->input->post('id').'.jpg';
                        echo "<img src='".$img_path."'  class='icon-big-circle'>";
                    } else {
                        $img_path = base_url() . '/assets/images/profile/empty-profile.png';
                        echo "<img src='".$img_path."'  class='icon-big-circle'>";
                    }
                }
            }
        }
    }

    public function profile_edit($popup = false)
    {
        if ($this->input->post()) {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === false || !$this->input->post('id')) {
                $this->session->set_userdata('error', $this->lang->line('error_csrf'));
                redirect('users/profile', 'refresh');
            }

            $this->form_validation->set_rules('firstname', $this->lang->line('edit_user_validation_fname_label'), 'required|xss_clean');
            $this->form_validation->set_rules('lastname', $this->lang->line('edit_user_validation_lname_label'), 'required|xss_clean');

            $ip_address = $this->input->ip_address();

            $update_data = array(
                'user_email' => ($this->input->post('email')) ? $this->input->post('email') : null,
                'user_first_name' => ($this->input->post('firstname')) ? $this->input->post('firstname') : null,
                'user_las_name' => ($this->input->post('lastname')) ? $this->input->post('lastname') : null,
                'user_password' => ($this->input->post('new')) ? $this->input->post('new') : null,
                'user_newsletter' => ($this->input->post('newsletter')) ? 1 : 0,
                'user_birthday' => ($this->input->post('birthday')) ? $this->input->post('birthday') : null,
                'user_ip_address' => $ip_address,
                'current_password' => ($this->input->post('current')) ? $this->input->post('current') : null,
                'token_assist' =>($this->input->post('token_assist')) ? $this->input->post('token_assist') : null,
            );
            
            //#Start change password forum.feed.biz
            if ($this->input->post('new')) {
                include DIR_SERVER.'/libraries/ipb_forum_connect.php';
                changepassword($this->input->post('email'), $this->input->post('current'), $this->input->post('new'));
            }
            //#End change password forum.feed.biz

            if ($this->input->post('id')) {
                $update = $this->authentication->update($this->input->post('id'), $update_data);

                if (isset($update['error'])) {
                    $this->session->set_userdata('error', $update['error']);
                } elseif (!empty($validate_dir)) {
                    $this->session->set_userdata('error', $validate_dir);
                } else {
                    $session_data = array(
                        'id' => $this->input->post('id'),
                        'user_email' => $this->input->post('email'),
                        'user_first_name' => $update_data['user_first_name'],
                        'user_las_name' => $update_data['user_las_name'],
                        'logged' => true,
                        'time' => time(),
                        'ip' => $this->input->ip_address(),
                    );
                    $this->session->set_userdata($session_data);
                    $this->session->set_userdata('message', $update['message']);
                }
            }
            if ($popup) {
                //redirect("users/billing_information", 'refresh');
                $step = $this->session->userdata('profile_popup') + 1;
                $this->session->unset_userdata('profile_popup');
                //redirect("my_feeds/configuration/data_source/".$step, 'refresh');
                redirect("dashboard/iframe_popup_conf/next", 'refresh');
            } else {
                redirect("users/profile", 'refresh');
            }
        }
    }

    public function billing_information()
    {
        $data = array();
        $data = $this->authentication->checkAuthen(); //check logged in and redirect to login page
        $csrf = $this->_get_csrf_nonce();
        $this->load->model('paypal_model', 'model');
        $this->load->model('country_model', 'country');

        foreach ($csrf as $key => $value) {
            $data['csrf']['key'] = $key;
            $data['csrf']['value'] = $value;
        }

        //get account from customer table
        if ($this->id_user) {
            //$this->db->where(array('id' => $this->id_user) );
            //$account = $this->db->get('customer')->row();
            $account_data = array();

            //check customer from database
//            if(isset($account) && !empty($account))
//                $account_data = (array)$account;                    
//            if( empty($account) )
//            {
//                $user_data = $this->authentication->user($this->id_user)->row();
//                if(isset($user_data) && !empty($user_data))
//                {
//                    $account_data['first_name'] = $user_data->user_first_name;
//                    $account_data['last_name']   = $user_data->user_las_name;
//                    $account_data['id']   = $this->id_user;
//                }
//            }
//            else 
//            {
//                $account_data = array_merge( array('id' => $this->id_user), $account_data);
//            }

            $tmpInfo = $this->model->getUserPackage($this->id_user);
            if ($tmpInfo) {
                $account_data = array(
                    'id' => $this->id_user,
                    'first_name' => $tmpInfo[0]['firstname_upkg'],
                    'last_name' => $tmpInfo[0]['lastname_upkg'],
                    'company' => $tmpInfo[0]['company_upkg'],
                    //'complement' => $tmpInfo[0]['company_upkg'],
                    'address1' => $tmpInfo[0]['address1_upkg'],
                    'address2' => $tmpInfo[0]['address2_upkg'],
                    'stateregion' => $tmpInfo[0]['state_upkg'],
                    'city' => $tmpInfo[0]['city_upkg'],
                    'country' => $tmpInfo[0]['country_upkg'],
                    'zipcode' => $tmpInfo[0]['zip_upkg'],
                );
            } else {
                $user_data = $this->authentication->user($this->id_user)->row();
                if (isset($user_data) && !empty($user_data)) {
                    $account_data['first_name'] = $user_data->user_first_name;
                    $account_data['last_name'] = $user_data->user_las_name;
                    $account_data['id'] = $this->id_user;
                }
            }
            if (isset($tmpInfo[0]['country_upkg']) && $tmpInfo[0]['country_upkg'] != '') {
                $ctry = $tmpInfo[0]['country_upkg'];
            } else {
                $ctry = '';
            }
            $country_option = $this->country->getOptionCountry($ctry);

            if ($this->session->userdata('first_login')) {
                $account_data['first_login'] = true;
            }
        }

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->assign('siteurl', site_url());
        $this->smarty->assign('account', $account_data);
        $this->smarty->assign('country_option', $country_option);
        $this->smarty->view('users/billing_information.tpl', $data);
    }

    public function billing_information_edit()
    {
        if ($this->input->post()) {
            // do we have a valid request?
            if ($this->_valid_csrf_nonce() === false || !$this->input->post('id')) {
                $this->session->set_userdata('error', $this->lang->line('error_csrf'));
                redirect('users/account', 'refresh');
            }

            if ($this->input->post('id')) {
                if (($this->input->post('id') !== $this->id_user)) {
                    $this->session->set_userdata('error', $this->lang->line('error_id_mismat'));
                    redirect('users/account', 'refresh');
                }

                //get account from customer table
                $infoArr = (array) $this->input->post();

                $this->load->model('paypal_model', 'model');

                $arrUserPkg = array(
                    'id_users' => $this->id_user,
                    'company_upkg' => $infoArr['company'],
                    'firstname_upkg' => $infoArr['firstname'],
                    'lastname_upkg' => $infoArr['lastname'],
                    'address1_upkg' => $infoArr['address1'],
                    'address2_upkg' => $infoArr['address2'],
                    'state_upkg' => $infoArr['stateregion'],
                    'city_upkg' => $infoArr['city'],
                    'country_upkg' => $infoArr['country'],
                    'zip_upkg' => $infoArr['zipcode'],
                    'status_upkg' => 'active',
                    'created_date_upkg' => date('Y-m-d H:i:s'),
                );
                $result = $this->model->setUserPackage($arrUserPkg);

//                $this->db->where( array('id' => (int)$this->input->post('id')) );
//                $this->db->get('customer');
//
//                if( $this->db->affected_rows() == 1)
//                {    
//
//                    $update_data = array(
//                        'company'       => ($this->input->post('company'))      ? $this->input->post('company')     : '',
//                        'first_name'    => ($this->input->post('firstname'))    ? $this->input->post('firstname')   : '',
//                        'last_name'     => ($this->input->post('lastname'))     ? $this->input->post('lastname')    : '',
//                        'complement'    => ($this->input->post('complement'))   ? $this->input->post('complement')  : '',
//                        'address1'      => ($this->input->post('address1'))     ? $this->input->post('address1')    : '',
//                        'address2'      => ($this->input->post('address2'))     ? $this->input->post('address2')    : '',
//                        'stateregion'   => ($this->input->post('stateregion'))  ? $this->input->post('stateregion') : '',
//                        'zipcode'       => ($this->input->post('zipcode'))      ? $this->input->post('zipcode')     : '',
//                        'city'          => ($this->input->post('city'))         ? $this->input->post('city')        : '',
//                        'country'       => ($this->input->post('country'))      ? $this->input->post('country')     : '',
//                    );
//
//                    $result = $this->db->update('customer', $update_data, array('id' => (int)$this->input->post('id')));
//
//                }
//                else
//                {
//                    $insert_data = array(
//                        'id'            => ($this->input->post('id'))           ? (int)$this->input->post('id')    : '',
//                        'company'       => ($this->input->post('company'))      ? $this->input->post('company')     : '',
//                        'first_name'    => ($this->input->post('firstname'))    ? $this->input->post('firstname')   : '',
//                        'last_name'     => ($this->input->post('lastname'))     ? $this->input->post('lastname')    : '',
//                        'complement'    => ($this->input->post('complement'))   ? $this->input->post('complement')  : '',
//                        'address1'      => ($this->input->post('address1'))     ? $this->input->post('address1')    : '',
//                        'address2'      => ($this->input->post('address2'))     ? $this->input->post('address2')    : '',
//                        'stateregion'   => ($this->input->post('stateregion'))  ? $this->input->post('stateregion') : '',
//                        'zipcode'       => ($this->input->post('zipcode'))      ? $this->input->post('zipcode')     : '',
//                        'city'          => ($this->input->post('city'))         ? $this->input->post('city')        : '',
//                        'country'       => ($this->input->post('country'))      ? $this->input->post('country')     : '',
//                    );
//
//                    $result = $this->db->insert('customer', $insert_data);
//
//                }

                if ($result) {
                    $this->session->set_userdata('message', 'account_save_successful');
                } else {
                    $this->session->set_userdata('error', 'account_save_unsuccessful');
                }
            }
//            if ($this->session->userdata('first_login')) {
//                $this->session->unset_userdata('first_login');
//                redirect("my_feeds/configuration/data_source", 'refresh');
//            } else {
                redirect("users/billing_information", 'refresh');
//            }
        }
    }

    public function register($lang=null)
    {
        //translation language
        if(isset($lang)){
            $this->language($lang);
            redirect('/users/register');
        }
        
        $this->load->model('customer_support_model', 'support');
        $data = array();
//        $se_info = $this->session->userdata('support_employee_info');
        $greeting_txt = $this->support->support_greeting_txt;
        $data['support_image'] = $this->session->userdata('sem_image');
        $data['support_txt'] = $greeting_txt;
        $data['support_name'] = $this->session->userdata('sem_name');//$se_info['name'];
        $data['user_referral'] = $this->session->userdata('referral') . '';

        if ($this->uri->segment(3) == '') {
            $ci_config = $this->config->item('opauth_config');
            $opauth['strategies'] = array_keys($ci_config['Strategy']);
            $opauth['path'] = $ci_config['path'];

            foreach ($opauth['strategies'] as $strategies) {
                $strategies_path = $opauth['path'] . strtolower($strategies);
                $this->smarty->assign(strtolower($strategies) . "_path", $strategies_path);
                $this->smarty->assign(strtolower($strategies), $strategies);
            }
        } else {
            //Run login
            $config_data = $this->config->item('opauth_config');
            //print_r($config_data);
            $this->load->library('Opauth/Opauth', $config_data, true);
            //$this->opauth->run();
        }

        $this->smarty->view('users/register.tpl', $data);
    }

    public function register_save()
    {
        $data = array();

        if ($this->input->post()) {
            $values = $this->input->post();
            //print_r($values);exit;
            $email = $values['email'];
            //$username           = $values['username'] ;
            $username = uniqid('u'); //temp for more secure
            $password = $values['password'];
            $referral = $values['referral'];
            $additional_data = array(/* 'user_first_name' => $values['firstname'], 'user_las_name' => $values['lastname'] */);

            $id = $this->authentication->register($email, $password, $additional_data, $username, $referral);
            
            //#Start forum.feed.biz
            include DIR_SERVER.'/libraries/ipb_forum_connect.php';
            register($username, $email, $password);
            
            $this->load->model('my_feed_model', 'user_model');
            $this->user_model->update_store_user_info();
            //#End forum.feed.biz

            if (!isset($id['id']) || !$id['id']) {
                $this->session->set_userdata('error', $this->lang->line($id['error']));
                redirect('users/login', 'refresh');
            } elseif (isset($id['error'])) {
                $this->session->set_userdata('error', $this->lang->line($id['error']));
                redirect('users/login', 'refresh');
            } else {
                if (isset($id['activation']) && $id['activation'] == 1) {
                    $this->session->set_userdata("message", $this->lang->line('please_activate_account_by_email_click'));
                    $this->session->set_userdata("new_register_email", $values['email']);
                    redirect('users/login', 'refresh');
                }

                $user_data = $this->authentication->user($id['id'])->row();
                $data['profile'] = urlencode(json_encode(array('username' => $user_data->user_name, 'id' => $user_data->id)));

                if (isset($data['profile'])) {
                    $session_data = array(
                        'id' => $id['id'],
                        'user_email' => $values['email'],
                        'user_name' => $user_data->user_name,
//                        'user_first_name'   => $values['firstname'] ,
//                        'user_las_name'     => $values['lastname'] ,
                        'logged' => true,
                        'time' => time(),
                        'ip' => $this->input->ip_address(),
                        'first_login' => true,
                    );

                    $this->session->set_userdata($session_data);
                }
                //$this->smarty->view('users/authenticate.tpl', $data);
                redirect('dashboard', 'refresh');
                //redirect('users/profile','refresh');
            }
        } else {
            $this->session->set_userdata('error', 'create_user_error');
            redirect('users/login', 'refresh');
        }
    }

    public function test_remote()
    {
        $time = time();
        $time_en = md5($time);
        $tf = substr($time, -1, 1);
        $ta = substr($time, -2, 1);
        $tb = substr($time, -3, 1);
        $tg = substr($time, -4, 1);
        $pf = substr($time_en, $tf + $tg, 1);
        $pb = substr($time_en, $ta + $tb, 1);
        $time_end = $pb . $time_en . $pf;
        $input = array('k' => bin2hex(gzcompress($time_end)), 'a' => base64_encode(time()));
        echo $time . ' ' . $time_en . ' ' . $tf . ' ' . $ta . ' ' . $tb . ' ' . $pf . ' ' . $pb . ' ' . $time_end . ' ' . gzcompress($time_end);
        print_r($input);
        $f = mt_rand(0, 99999);
        $b = mt_rand(0, 99999);
        $st = ($f . '!' . base64_encode(json_encode($input)) . '!' . $b);
        echo '<br>' . $st . '<br>';
    }

    public function remote($code = '', $id = 0)
    {
        $expire_time_min = 15;
        if (!function_exists('hex2bin')) {
            function hex2bin($str)
            {
                $sbin = "";
                $len = strlen($str);
                for ($i = 0; $i < $len; $i += 2) {
                    $sbin .= pack("H*", substr($str, $i, 2));
                }

                return $sbin;
            }
        }
        $res = false;
        $out = base64_decode($code);
        if (strpos($out, "!") !== false) {
            $out = explode('!', $out);
            if (isset($out[1])) {
                $cur_time = time();
                $out = base64_decode($out[1]);
                $data = json_decode($out, true);
                if (!empty($data)) {
                    $key = gzuncompress(hex2bin(($data['k'])));
                    $link_time = base64_decode($data['a']);
                    $time = $link_time;
                    $time_en = md5($time);
                    $tf = substr($time, -1, 1);
                    $ta = substr($time, -2, 1);
                    $tb = substr($time, -3, 1);
                    $tg = substr($time, -4, 1);
                    $pf = substr($time_en, $tf + $tg, 1);
                    $pb = substr($time_en, $ta + $tb, 1);
                    $time_end = $pb . $time_en . $pf;
                    $dif = round(abs($cur_time - $link_time) / 60, 2);
                    if ($dif <= $expire_time_min && $key == $time_end) {
                        $this->authentication->logout();
                        $this->session->unset_userdata('id_general');
                        $this->session->unset_userdata('menu');
                        $this->session->unset_userdata('mode_default');
                        $this->session->unset_userdata('amazon_mode_creation');
                        $this->session->unset_userdata('dashboard_sum_product');
                        delete_cookie('id');

                        $userdata = $this->authentication->user($id)->row();
                        $language = $this->authentication->getLanguageNameByID($userdata->user_default_language);
                
                        $session_data = array(
                            'id' => $userdata->id,
                            'user_email' => $userdata->user_email,
                            'user_name' => $userdata->user_name ,
                            'user_first_name' => $userdata->user_first_name,
                            'user_las_name' => $userdata->user_las_name,
                            'language' => !empty($language) ? $language : $this->language,
                            'logged' => true,
                            'time' => time(),
                            'ip' => $this->input->ip_address(),
                            'first_login' => true,
                            'remote' => true
                        );

                        $this->session->set_userdata($session_data);
                        $this->authentication->generateNewOTP($userdata->id, true);
                        $redir = 'dashboard';
                        redirect($redir, 'refresh');
                    }
                }
            }
        }
        redirect('users/logout', 'refresh');
    }

    //activate the user
    public function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->authentication->activate($id, $code);
        }

        if (!isset($activation['error'])) {
            $this->load->model('my_feed_model', 'user_model');
            $this->user_model->update_store_user_info();
            
            $userdata = $this->authentication->user($id)->row();
            $session_data = array(
                'id' => $userdata->id,
                'user_name' => $userdata->user_name,
                'user_email' => $userdata->user_email,
                'user_first_name' => $userdata->user_first_name,
                'user_las_name' => $userdata->user_las_name,
                'language' => $this->language,
                'logged' => true,
                'time' => time(),
                'ip' => $this->input->ip_address(),
                'first_login' => true
            );
            
            //#Start Session login forum.feed.biz
            include DIR_SERVER.'/libraries/ipb_forum_connect.php';
            ipb_session_login_activate($userdata->user_email);
            //#End Session login forum.feed.biz

            $this->session->set_userdata($session_data);
            $redir = 'dashboard';

            $this->load->model('customer_support_model', 'support');
            $this->support->inc_count_contact();
            $this->support->send_email2supporter();
            $support_email = $this->support->support_email;
            $b_url = base_url(); //'sismiracles.com';// base_url();
            $im = ($b_url . '/nblk/images/feedbiz_logob.png');
            $logo = '<img src="' . ($im) . '" alt="feed.biz" />';
            $im = ($b_url . $this->session->userdata['sem_image']);
            $support_image = '<img src="' . ($im) . '" alt="feed.biz Supporter" width="80"/>';
            $im = ($b_url . '/nblk/images/signature_scan.gif');
            $support_signature = '<img src="' . ($im) . '" alt="feed.biz Supporter Signature" width="200" style="border: none" />';
            /*$email_data = array(
                'sitename' => $this->site_title,
                'identity' => $userdata->user_email,
                'id' => $userdata->id,
                'backend_link' => 'dashboard',
                'logo' => $logo,
                'support_name' => $this->session->userdata('support_employee_name'),
                'support_image' => $support_image,
                'support_signature' => $support_signature,
            );*/
            
            $this->smarty->assign('sitename', sprintf($this->site_title));
            $this->smarty->assign('identity', $userdata->user_email);
            $this->smarty->assign('id',  $userdata->id);
            $this->smarty->assign('backend_link',  site_url('dashboard'));
            $this->smarty->assign('logo', $logo);
            $this->smarty->assign('support_name',  $this->session->userdata('support_employee_name'));
            $this->smarty->assign('support_image',  $support_image);
            $this->smarty->assign('support_signature',  $support_signature);
            $message = $this->smarty->fetch($this->email_templates . $this->email_after_activate);
            //$message = $this->load->view($this->email_templates . $this->email_after_activate, $email_data, true);

            $email = $userdata->user_email;
            $this->email_sender->setSender($support_email);
            $this->email_sender->sendMail($email, $this->site_title . ' - ' . $this->lang->line('email_welcome_subject'), $message);
            redirect($redir, 'refresh');
        } else {
            //redirect them to the forgot password page
            $this->session->set_userdata('error', $this->lang->line($activation['error']));
            redirect("users/login", 'refresh');
        }
    }

    public function check_register_username($username)
    {
        $q = $this->db->where('user_name', $username)->get('users');

        if ($q->num_rows > 0) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_register_email($email = '')
    {
        if ($email=='') {
            $post = $this->input->post();
            if (!isset($post['email'])) {
                echo "false";
            } else {
                $email = $post['email'];
            }
        }
        $q = $this->db->where('user_email', $email)->get('users');
        if ($q->num_rows > 0) {
            echo "false";
        } else {
            echo "true";
        }
    }

    public function check_email()
    {
        $q = $this->db->where('user_email', strtolower($this->input->post('email')))->get('users');
        if ($q->num_rows > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function captcha()
    {
        require_once('application/libraries/recaptchalib.php');

        $resp = recaptcha_check_answer($this->privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        if (!$resp->is_valid) {
            echo "false";
        } else {
            $this->session->set_flashdata('captcha', true);
        }
        echo "true";
        die;
    }

    public function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_userdata('csrfkey', $key);
        $this->session->set_userdata('csrfvalue', $value);

        return array($key => $value);
    }

    public function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->userdata('csrfkey')) !== false &&
                $this->input->post($this->session->userdata('csrfkey')) == $this->session->userdata('csrfvalue')) {
            return true;
        } else {
            return false;
        }
    }

    public function get_default_shop()
    {
        echo $this->session->userdata('id_shop');
        @header_remove();
        exit;
    }

    public function test_data()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        $shop = $this->session->userdata('id_shop');
        $mode = $this->session->userdata('mode_default');
        $out = $this->feedbiz->exportProductListArr($this->user_name, $shop['id_shop'], $mode);
        echo json_encode(array('data' => $out));
    }

    

    public function test_get_sub_arr()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        $this->iso_code = mb_substr($this->language, 0, 2);
        $out = $this->feedbiz->getSubArrCategories(1);
    }

    public function test_get_num_cat()
    {
        $this->load->library('FeedBiz', array($this->user_name));
        $this->iso_code = mb_substr($this->language, 0, 2);
        $out = $this->feedbiz->getBigCategories($this->user_name, 1);
        echo '<pre>';
        print_r($out);
        $out2 = $out;
        $res = array();
        foreach ($out as $ox) {
            $max = 0;
            $max_id = 0;
            foreach ($out2 as $k => $o) {
                if ($max > $o['num']) {
                    $tmp = $max;
                } else {
                    $max = $o['num'];
                    $max_id = $k;
                }
            }
            $res[] = $out[$max_id];
            unset($out2[$max_id]);
        }
        print_r($res);
    }

    public function test_email()
    {
        require_once APPPATH . 'libraries/Swift/swift_required.php';
        $subject = 'test email by swift';
        $message = 'test message by swift';
        try {
            $transport = Swift_SmtpTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $swift_message = Swift_Message::newInstance();
            $swift_message->setSubject($subject)
                            ->setFrom(array($this->config->item('admin_email') => $this->site_title))
                            ->setTo('palmp.jobs@gmail.com')
                            ->setBody($message, 'text/html');
            $out = $mailer->send($swift_message) ;
            print_r($out);
        } catch (Exception $ex) {
            print_r($ex);
        }
    }
    public function test_img()
    {
        $im = file_get_contents(getcwd() . '/nblk/images/feedbiz_logob.png');
        $logo = '<img src="data:image/png;base64,' . base64_encode($im) . '" alt="feed.biz" />';
        echo $logo;
    }

    public function check_sig()
    {
        $hide_sig = $this->session->userdata('hide_sig');
        echo '<pre>';
        print_r(json_decode($hide_sig, true));

        echo '<br>--------------------------------------------------------<br>';
        if (!session_id()) {
            @session_start();
        }
        print_r($_SESSION);
    }
    public function clear_sig()
    {
        if (!session_id()) {
            @session_start();
        }
        unset($_SESSION);
        $this->session->set_userdata('hide_sig', array());
    }
    public function ajax_get_sig_process($action = '')
    {
        $force_terminate = false;
        $win = false;
        $action = str_replace('%20', ' ', $action);
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $force_terminate = false;
            $win = true;
        }
        $win = true;
//        $isAjax=isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
//        if(!$isAjax) { 
//            die('Access denied - not an AJAX request...');
//        }

        if ($action == '') {
            die('The action is empty.');
        }
        if ($win) {
            if (!session_id()) {
                session_start();
            }
        }
        $out_sig = array();
        $hide_sig = $this->session->userdata('hide_sig');
        if (!empty($hide_sig)) {
            $hide_sig = json_decode($hide_sig, true);
        }
        $new_action='';
        $sig=array();
        if(is_array($hide_sig)){
            foreach($hide_sig as $k =>$v){
                if(isset($v['real_time_running']) && $v['real_time_running']==true && preg_match("/{$action}.*/", $k, $sig)){
                    $new_action = $k;
                    break;
                }
            }
        }
        if (!empty($hide_sig) && !isset($hide_sig[$action]) && !empty($new_action) && isset($hide_sig[$new_action])) {
            $action = $new_action;
        }

        if (!empty($hide_sig) && isset($hide_sig[$action])) {
            $data = $hide_sig[$action];
            $out_sig['found'] = true;
            $out_sig['data']['batch_id'] = $data['bid'];
            $out_sig['data']['msg'] = $data['proc_msg'];
            $out_sig['data']['percent'] = $data['progress'];
            $out_sig['data']['err_msg'] = $data['err_msg'];
            $out_sig['data']['status'] = $data['status'];
            $out_sig['data']['comments'] = $data['comments'];
            $txt = '';
            switch ($data['status']) {
                case 0: $txt = 'not started';
                    break;
                case 1: $txt = 'running';
                    break;
                case 4: $txt = 'error';
                    break;
                case 9: $txt = 'finished';
                    break;
            }
            $out_sig['data']['status_txt'] = $txt;
        } else {
            $out_sig['error_msg'] = 'Not found your action (' . $action . ').'.print_r($hide_sig, true);
            $out_sig['found'] = false;
            if (isset($hide_sig)) {
                $out_sig['all_pk'] = $hide_sig;
            }
        }
        $out_sig['delay'] = 2000;
        $out_sig['timeout'] = 6000;
        if (sizeof($out_sig) != 0) {
            echo json_encode($out_sig);
        }

        if ($win) {
            $data = serialize($out_sig);
            if (isset($_SESSION['last_req'])) {
                if (strcmp($_SESSION['last_req'], $data) == 0) {
                    if (!isset($_SESSION['count_get_same'])) {
                        $_SESSION['count_get_same'] = 0;
                    }

                    $_SESSION['count_get_same'] = $_SESSION['count_get_same'] + 1;
                    if ($_SESSION['count_get_same'] > 100) {
                        unset($_SESSION['last_req']);
                        $_SESSION['count_get_same'] = 0;
                        if (isset($this->user_name)) {
                            $user = $this->user_name;
                        }
                        try {
                            //                            @file_get_contents (($this->config->item('ssl_protocol')?'https://':'http://')."127.0.0.1:3001/clear_process?$user");
                        } catch (Exception $ex) {
                            die('can not clear process');
                        }
                    }
                }
            } else {
                $_SESSION['count_get_same'] = 0;
            }
            $_SESSION['last_req'] = $data;
        }

//        @header_remove();
        exit;
    }

    public function ajax_switch_popup($status = '1')
    {
        if (!session_id()) {
            session_start();
        }
        $_SESSION['display_all_popup'] = $status;
        exit;
    }

    public function ajax_update_process()
    {
        @header_remove();
        $force_terminate = false;
        $win = false;

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $force_terminate = false;
            $win = true;
        }
        $win = true;

        $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
//        if(!$isAjax) { 
//            die('Access denied - not an AJAX request...');
//        }

        if ($force_terminate || !($this->authentication->logged_in() && isset($this->session->userdata['id']))) {
            // not login
            $add = '3';
            if (!$this->authentication->logged_in()) {
                $add = '1';
            }
            if (!isset($this->session->userdata['id'])) {
                $add = '2';
            }
            die('Session timeout ' . $add);
        }
        $this->session->set_userdata('aLive');

        $user = $this->user_name;

        $clear_proc = $this->input->get('clear_process');
        if (!empty($clear_proc)) {
            if (!session_id()) {
                session_start();
            }
            if (isset($_SESSION['last_sig']['proc'][$this->input->get('clear_process')])) {
                unset($_SESSION['last_sig']['proc'][$this->input->get('clear_process')]);
            }
            die();
        }
        if ($this->input->get('clear_all_process') == 'all') {
            if (!session_id()) {
                session_start();
            }
            unset($_SESSION['last_sig']);
            header_remove();
            die();
        }

        try {
            $data = @file_get_contents(($this->config->item('ssl_protocol') ? 'https://' : 'http://') . "127.0.0.1:3001/get_process?$user");
        } catch (Exception $ex) {
            die('can not get process');
        }
        if (!session_id()) {
            session_start();
            if ($win) {
                if (!isset($_SESSION['last_sig'])) {
                    try {
                        @file_get_contents(($this->config->item('ssl_protocol') ? 'https://' : 'http://') . "127.0.0.1:3001/clear_process?$user");
                        unset($_SESSION['hide_sig']);
                    } catch (Exception $ex) {
                        die('can not clear process');
                    }
                }
            } else {
                if (!isset($_SESSION['last_sig'])) {
                    unset($_SESSION['hide_sig']);
                }
            }
        }
        if ($win) {
            $same = false;
            if (isset($_SESSION['last_data'])) {
                if (strcmp($_SESSION['last_data'], $data) == 0) {
                    $t = json_decode($data, true);
                    if (!empty($t) && isset($t['proc'])) {
                        $same = true;
                        $_SESSION['count_same'] = $_SESSION['count_same'] + 1;
                    }
                }
            } else {
                $_SESSION['count_same'] = 0;
            }
            if (isset($data)) {
                $_SESSION['last_data'] = $data;
            }

            if ($same && $_SESSION['count_same'] > 100) {
                try {
                    //                    @file_get_contents (($this->config->item('ssl_protocol')?'https://':'http://')."127.0.0.1:3001/clear_process?$user");
//                    if(isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig'] ) > 0){
//                        foreach($_SESSION['hide_sig'] as $k=>$v){
//                            if($v['popup_display']){
//                                unset($_SESSION['hide_sig'][$k]);
//                            }
//                        }
//                     }
//                    unset($_SESSION['last_data']);
                } catch (Exception $ex) {
                    die('can not clear process');
                }
            }
        }

        if (!isset($_SESSION['count_empty'])) {
            $_SESSION['count_empty'] = 0;
        }
        if (isset($data) && !empty($data)) {
            $ex = json_decode($data, true);
            if (!isset($ex['proc'])) {
                $_SESSION['count_empty'] = $_SESSION['count_empty'] + 1;
            } else {
                $_SESSION['count_empty'] = 0;
            }
        }
        if ($_SESSION['count_empty'] > 10) {
            $_SESSION['count_empty'] = 0;
            if (isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig']) > 0) {
                foreach ($_SESSION['hide_sig'] as $k => $v) {
                    if ($v['status'] == 4 || $v['status'] == 9) {
                        unset($_SESSION['hide_sig'][$k]);
                    }
                }
            }
        }



        if (isset($data) && !empty($data)) {
            $ex = json_decode($data, true);
            if (isset($ex['proc'])) {
                foreach ($ex['proc'] as $k => $v) {
                    if ($k == '') {
                        //if($k==''|| $v['bid']==null){
                        unset($ex['proc'][$k]);
                        continue;
                    }
                    $v['action'] = str_replace('%20', ' ', $v['action']);
                    if (!$ex['proc'][$k]['popup_display']) {
                        $_SESSION['hide_sig'][$v['action']] = $v;
                        if (isset($_SESSION['display_all_popup']) && $_SESSION['display_all_popup'] == '1') {
                        } else {
                            unset($ex['proc'][$k]);
                        }
                    } else {
                        $_SESSION['hide_sig'][str_replace(' ', '_', $v['process'])] = $v;
                    }
                }
            }
            if (isset($_SESSION['hide_sig']) && sizeof($_SESSION['hide_sig']) > 0) {
                $this->session->set_userdata('hide_sig', json_encode($_SESSION['hide_sig']));
            } else {
                $this->session->unset_userdata('hide_sig');
            }
        } else {
            $ex = array('error_reason' => 'Can not connect server node');
            die('Can not connect server node.');
        }

        if (isset($_SESSION['last_sig'])) {
            $l_sig = $_SESSION['last_sig'];
        } else {
            $l_sig = $ex;
        }
        $out_sig = $ex;

        if (isset($l_sig['proc'])) {
            foreach ($l_sig['proc'] as $k => $v) {
                if (!isset($ex['proc'][$k])) { //something wrong
                    $out_sig['proc'][$k] = $l_sig['proc'][$k];
                    if ($l_sig['proc'][$k]['progress'] == 100 || $l_sig['proc'][$k]['status'] == 9) {
                        // process done
                        $out_sig['proc'][$k]['s_status'] = 'complete';
                    } elseif ($l_sig['proc'][$k]['progress'] == 0 || $l_sig['proc'][$k]['status'] == 0) {// process not start
                    } elseif ($l_sig['proc'][$k]['status'] == 4 || $l_sig['proc'][$k]['status'] == 8) {
                        // process not start
                        $out_sig['proc'][$k]['s_status'] = 'error';
                    } elseif ($l_sig['proc'][$k]['progress'] != 100 && $l_sig['proc'][$k]['status'] != 9) {
                        // process terminate with out done
                        if ($l_sig['proc'][$k]['progress'] > 50) {
                            $out_sig['proc'][$k]['s_status'] = 'complete';
                        } else {
                            $out_sig['proc'][$k]['s_status'] = 'error';
                        }
                    } else {
                        // process terminate with out done
                        $out_sig['proc'][$k]['s_status'] = 'error';
                    }
                } else {
                    // process running
                    if ($l_sig['proc'][$k]['status'] == 9 || $l_sig['proc'][$k]['progress'] == 100) {
                        $out_sig['proc'][$k]['s_status'] = 'complete';
                    } elseif ($l_sig['proc'][$k]['status'] == 4 || $l_sig['proc'][$k]['status'] == 8) {
                        // process not start
                        $out_sig['proc'][$k]['s_status'] = 'error';
                    } else {
                        $out_sig['proc'][$k]['s_status'] = 'running';
                    }
                }
            }
        }

        if (!isset($ex['delay']) || !isset($ex['timeout'])) {
            $out_sig['delay'] = 2000;
            $out_sig['timeout'] = 7000;
        }

        if (isset($data) && strpos($data, 'throw') === false) {
            if (sizeof($out_sig) != 0) {
                echo json_encode($out_sig);
            }
        }

        $_SESSION['last_sig'] = $out_sig;
        @header_remove();
        session_write_close();

        exit;
    }
                
    public function security($action='')
    {
        $this->authentication->checkAuthen();
        
        include(dirname(__FILE__).'/../libraries/mfa/config.php');
        $this->load->model('my_feed_model', 'user_model');
        
        if ($this->input->post()) {
            $mfa_info = $this->user_model->get_mfa_info($this->id_user);
            if (!empty($mfa_info['mfa_secret_key'])) {
                $mfa_secret_key = $mfa_info['mfa_secret_key'];
            } else {
                $word = $this->lang->line('Please scan your QR code again. The key is wrong.');
                $this->session->set_userdata('error', $word);
                redirect('/users/security', 'refresh');
                return;
            }
            $post = $this->input->post();
            if (!empty($post['otp_reverify'])) {
                $currentcode=$post['otp_reverify'];
                if (TokenAuth6238::verify($mfa_secret_key, $currentcode,10)) {
                    $status_case=1;
                    $this->user_model->update_mfa_info($this->id_user, null, '0');
                    $word = $this->lang->line('Successful!, the login security was deactivated.');
                    $this->session->set_userdata('message', $word);
                    $this->user_model->update_mfa_status_users();
                } else {
                    $word = $this->lang->line('Please try again. Your OTP is wrong.');
                    $this->session->set_userdata('error', $word);
                }
            } elseif (!empty($post['otp_verify'])) {
                $currentcode=$post['otp_verify'];
                if (TokenAuth6238::verify($mfa_secret_key, $currentcode,10)) {
                    $this->user_model->update_mfa_info($this->id_user, null, 1);
                    $word = $this->lang->line('Successful!, the login security was activated on your account.');
                    $this->session->set_userdata('message', $word);
                    $this->user_model->update_mfa_status_users();
                } else {
                    $word = $this->lang->line('Please try again. Your OTP is wrong.');
                    $this->session->set_userdata('error', $word);
                }
            } else {
                $word = $this->lang->line('Please try again. Your OTP is wrong.');
                $this->session->set_userdata('error', $word);
            }
            $this->user_model->update_store_user_info();
            redirect('/users/security', 'refresh');
            return;
        }
        
        if (!empty($action)&&$action=='reset_otp') {
            $g = new GoogleAuthenticator();
            $mfa_secret_key =$g->generateSecret();
            $status_case = 0;
            $this->user_model->update_mfa_info($this->id_user, $mfa_secret_key, 0, $status_case);
            $word = $this->lang->line('Successful!, Your QR code was reset.');
            $this->session->set_userdata('message', $word);
            redirect('/users/security', 'refresh');
            return;
        }
        $mfa_info = $this->user_model->get_mfa_info($this->id_user);
        if (!empty($mfa_info['mfa_secret_key'])) {
            $mfa_secret_key = $mfa_info['mfa_secret_key'];
        } else {
            $g = new GoogleAuthenticator();
            $mfa_secret_key =$g->generateSecret();
            $this->user_model->update_mfa_info($this->id_user, $mfa_secret_key);
        }
        
        $site_name = $this->config->item('site_name');
        if (!empty($site_name)) {
            $title = $site_name;
        } else {
            $title = $this->config->item('base_url');
        }
        
        $data['mfa_enable'] = !empty($mfa_info['mfa_enable'])&&$mfa_info['mfa_enable']==1?true:false;
        if (!$data['mfa_enable']) {
            $data['qr_code'] = TokenAuth6238::get_qr_code($this->session->userdata('user_email'), $mfa_secret_key, $title);
        } else {
            $data['qr_code']='';
        }
        $data['mfa_first_time']  = empty($mfa_info['mfa_enable']) || (!empty($mfa_info['mfa_enable']) && empty($mfa_info['mfa_enable'])==0)?true:false;
        if ($this->session->userdata('error')) {
            $data['error'] = $this->session->userdata('error');
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
        $this->smarty->assign('cdn_url', base_url('/'));
        $this->smarty->view('users/security.tpl', $data);
    }
    public function update_mfa_json($status = 1, $user_id=14)
    {
        $this->load->model('my_feed_model', 'user_model');
        $this->user_model->update_mfa_status($user_id, $status);
        $this->user_model->update_mfa_status_users();
    }
    
    public function update_user_info_json()
    {
        $this->load->model('my_feed_model', 'user_model');
        $this->user_model->update_store_user_info();
    }

    public function clr_history()
    {
        $this->authentication->checkAuthen();
        if (isset($this->id_user) && !empty($this->id_user)) {
            $sql = "delete from histories where user_id = '" . $this->id_user . "' ORDER BY history_date_time DESC LIMIT 1";
            $this->db->query($sql);
            echo 'Clear your history ID:' . $this->id_user;
        } else {
            echo 'You are not logged in';
        }
    }
    
    public function notifications()
    {
        $data = array();
        $this->smarty->view('users/notifications.tpl', $data);
    }
    public function ajax_get_utk_ref()
    {
        $key = $this->authentication->getGeneratedOTP();
        echo json_encode(array('utk'=>$key));
    }
    public function see_session()
    {
        session_start();
        echo '<pre>';
        print_r($_SESSION);
    }
    public function test_marketplace_create_db()
    {
        require_once dirname(__FILE__) . '/../libraries/Marketplaces/classes/marketplaces.install.php';
        $mkp = new MarketplacesInstall('palmpcss', 'cdiscount');
    }
    public function clr_original_redirect()
    {
        $this->session->unset_userdata('original_redirect');
    }
    public function get_user_data()
    {
        echo '<pre>';
        print_r($this->session->userdata);
        echo  '</pre>';
    }
}
