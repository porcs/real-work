<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

        public function __construct() {
            
            // load controller parent
            parent::__construct();
            
            $this->language = $this->config->item('language');
            
        }
	public function init()
        {
            $this->smarty->assign("label", "welcome");
            $this->smarty->assign("lang", $this->language);
            
            if ( $this->session->userdata('logged') ) {
                $this->smarty->assign("logged", "true");
            }
            
            if ( $this->session->userdata('user_first_name') ) {
                $this->smarty->assign("user_first_name", $this->session->userdata('user_first_name'));
            }

            if ( $this->session->userdata('user_las_name')) {
                $this->smarty->assign("user_las_name", $this->session->userdata('user_las_name'));
            }
        }
        
	public function index()
	{            
            $this->init();
            $data['uri'] = $this->uri->segment(1);
            $this->smarty->view('index.tpl', $data);
	}
        
        function phpinfo()
        {
            echo phpinfo();
        }
        
        public function test()
        {

            
            $query = $this->db->query("SELECT * from configuration where name = 'FEED_SHOP_INFO' "); 
            echo '<pre>';
             print_r ($query->result_array());
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */