<?php
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.config.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.form.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.xml.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.xml.productData.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.xml.validator.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.webservice.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.database.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.create.database.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.order.php';
require_once dirname(__FILE__) . '/../libraries/Amazon/controllers/amazon.displayform.php';
require_once dirname(__FILE__) . '/../libraries/errorHandler.php';

class Amazon extends CI_Controller {

    const TAG = 'Amazon';
    const SYNC_MODE = 1;
    const CREATE_MODE = 2;
    public $ext;
    private $api_settings;
    private $exceptionWarningAPIMethod = array('parameters' => 1, 'actions' => 1, 'features' => 1);    
    
    public function __construct() {
   
        parent::__construct();
	
        //check logged in and redirect to login page
        $this->load->library('authentication');
        $this->authentication->checkAuthen(); 
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');

        if ((!isset($this->user_name) || empty($this->user_name)) && (!isset($this->id_user) || empty($this->id_user)))
            redirect('users/login');
        
        //load library
        $this->load->library('FeedBiz', array($this->user_name));
        
        // id shop
        $this->id_shop = $this->session->userdata('id_shop');
        $this->shop_default = $this->session->userdata('shop_default');
        
        if (!isset($this->id_shop) || empty($this->id_shop) || !($this->shop_default) || empty($this->shop_default)){
            $DefaultShop = $this->feedbiz->getDefaultShop($this->user_name);
            $this->id_shop = $DefaultShop['id_shop'];
            $this->shop_default = $DefaultShop['name'];
        }
        
        // id mode
        $this->id_mode = 1;
        
        //set language
        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->iso_code = mb_substr($this->language, 0, 2);
        $this->lang->load(strtolower(self::TAG), $this->language);
        $this->smarty->assign("lang", $this->language);
        $this->smarty->assign("label", strtolower(self::TAG));

        // amazon model
        $this->load->model("amazon_model", "amazon_model_user");
	
	// amazon database
        $this->amazon_model = new AmazonDatabase($this->user_name); 

        // Class name //Function name
	$_calss = $this->router->fetch_class();
	$_function = $this->router->fetch_method();
        $this->smarty->assign("class", ucwords($_calss));
        $this->smarty->assign("function", ($this->lang->line($_function)) ? $this->lang->line($_function) : ucfirst($_function));
        $this->smarty->assign("function_tag", ucfirst($_function));
              
	// check table exists
        $this->_check_table_exists(); 
	
	// initialize
        AmazonXSD::initialize();
        
	// get document
        _get_message_document($this->router->fetch_class(), $this->router->fetch_method());
    }

    public function debugs($country, $action_type = null/*, $option = null, $type = null*/) {

        require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.feeds.php';

        $this->_country_data($country);
        $user_data = $this->api_settings;
        $params = new stdClass();
        foreach ($user_data as $key => $value) {
            if (is_array($value)){
                $params->$key = (object)$value;
            } else {
                $params->$key = $value;
            }
        }        
       
        $params->language = $this->language;
       
        $params->id_marketplace = defined('_ID_MARKETPLACE_AMZ_') ? _ID_MARKETPLACE_AMZ_ : 2 ;
        
	$value = $this->input->post(); 
	
	if(isset($value) && !empty($value)) {
	    echo 'start - ' . date('H:i:s');
	} else {
	    $this->smarty->assign("amazon_create", AmazonDatabase::CREATE);
	    $this->smarty->assign("amazon_sync", AmazonDatabase::SYNC);
	    $this->smarty->assign("amazon_reports", 'reports');
	    $this->smarty->assign("amazon_shipping_group", 'groups');
	    $this->smarty->assign("amazon_update_order", 'update_order');
	    $this->smarty->assign("amazon_delete", AmazonDatabase::DELETE);
	    $this->smarty->assign("amazon_get_orders", 'get_orders');
	    
	    $this->smarty->assign("amazon_delete_out_of_stock", AmazonDatabase::DELETE_OUT_OF_STOCK);
	    $this->smarty->assign("amazon_delete_all_sent", AmazonDatabase::DELETE_ALL_SENT);
	    $this->smarty->assign("amazon_delete_inactive", AmazonDatabase::DELETE_INACTIVE);	  
	    $this->smarty->assign("amazon_delete_disabled", AmazonDatabase::DELETE_DISABLED);	  
	    $this->smarty->assign("amazon_repricing", AmazonDatabase::REPRICING);	  
	    $this->smarty->assign("amazon_repricing_export", AmazonDatabase::REPRICING_EXPORT);	  
	    $this->smarty->assign("amazon_repricing_analyst", AmazonDatabase::REPRICING_ANALYSIS);
	    
	    $this->smarty->assign("yesterday", date('d-m-Y',strtotime('-1 day')));	  
	    $this->smarty->assign("today", date('d-m-Y'));	
	    
	    $this->smarty->view('amazon/Debugs.tpl');
	}
	
        switch ($action_type) {
	    
            case AmazonDatabase::CREATE :
		$parameter = $value;
                $option = rawurlencode((json_encode($parameter/*array('limiter' => , 'id_product' => $value['id_product'])*/)));
                $params->mode = $option;
                $amazon_feeds = new AmazonFeeds($params, AmazonDatabase::CREATE, true, true);
                $amazon_feeds->FeedsData();
                break;
            case AmazonDatabase::SYNC :
                $params->creation = false;
                $params->mode = AmazonDatabase::SYNC;  
		
		if(isset($value['update_type']))
		    $params->update_type = $value['update_type'];
		
                $cron = false;                
		if(isset($value['cron']))
                    $cron = true;
		
                $amazon_feeds = new AmazonFeeds($params, AmazonDatabase::SYNC, true, $cron);
                $amazon_feeds->FeedsData();
                break;           
            case 'reports' :
                require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.reports.php';
		$params->mode = $value['report_mode']; // creation or synchronize
                $amazon_reports = new AmazonReports($params, true);
		
		if($value['report_mode'] == 1)
		    $amazon_reports->getReport();
		else if($value['report_mode'] == 2)
		{
		    $datefrom = $value['datefrom'];
		    $dateto = $value['dateto'];
		    $amazon_reports->getReportFulfillmentFees($datefrom, $dateto);
		}
                break;
	    case 'groups' :	
		require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.reports.php';
		$amazon_reports = new AmazonReports($params, true);
		$amazon_reports->getShippingGroups();
		break;	    
            case 'update_order' :
                require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.status.orders.php';
                $amazonOrders = new AmazonStatusOrders($params, true, AmazonStatusOrders::AmazonOrderFulfillment);
                $amazonOrders->confirmOrder();          
                break;
            case 'delete' :
                $params->creation = false;
                $params->mode = rawurlencode((json_encode(array('delete' => $value['delete']))));
                $amazon_feeds = new AmazonFeeds($params, AmazonDatabase::DELETE, true);
                $amazon_feeds->FeedsData();         
                break;            
            case 'get_orders' :
                require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.import.orders.php';
                $params->creation = false;
                $datefrom = $value['datefrom'];
                $dateto = $value['dateto'];
		$type = $value['type'];
                $params->option = rawurlencode((json_encode(array('datefrom' => $datefrom, 'dateto' => $dateto, 'status' => $type)))) ;
               
                $amazon_get_order = new AmazonImportOrders($params, true);
                $amazon_get_order->Dispatch();
                break; 	  
	    case AmazonDatabase::REPRICING :
                require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/functions/amazon.repricing.automaton.php');
                
                $params = $this->_setParams();                
                $debug = true;
                $cron = true;
                $params->asin = $value['asin'];
                $type = $value['type'];
                $object = json_decode(json_encode($params), FALSE);
                
                $amazon_repricing = new AmazonRepricingAutomaton($object, $debug, $cron);
                $amazon_repricing->Dispatch($type);
                break;
	    case 'PrefixTable' :
		require_once(dirname(__FILE__) . '/../../application/libraries/Amazon/classes/amazon.prefix.table.php');
		$old_prefix = ( isset($value['old_prefix']) && !empty($value['old_prefix']) ) ? $value['old_prefix'] : '' ;
		$new_prefix = ( isset($value['new_prefix']) && !empty($value['new_prefix']) ) ? $value['new_prefix'] : '' ;
		$amazon_prefix = new AmazonPrefixTable();
                $amazon_prefix->changePrefix($this->user_name, $old_prefix, $new_prefix);
		break;
            case 'deletedProducts' :
                require_once(dirname(__FILE__) .'/../../libraries/Hooks.php');
                //application\libraries\Amazon\hook\ProductManagement.php
                $hook = new Hooks();
                $hook->_call_hook('import_products', array(
                    'user_name' => $this->user_name,
                    'id_shop' => $this->id_shop,
                    'debug' => true
                ));
                exit;
            case 'trigger_reprice' :
                $skus = $value['sku'];
                $amazon_database = new AmazonDatabase($this->user_name, true);
                foreach ($skus as $sku){
                    $amazon_database->update_repricing_product_update_log($this->id_shop, $country, $sku);
                }
		break;
            case 'updateOrder' :
                $amazon_order = new Amazon_Order($this->user_name, true);
                $orders = $amazon_order->get_order_missing_item();
                foreach ($orders as $order){
                    if(isset($order['seller_order_id'])) {
                        $pos = strpos($order['payment_method'], 'Amazon');
                        echo '<br/>-------------------------------------------------<br/>';
                        if ($pos === false) {
                            echo '<pre><b>https://client.feed.biz/webservice/WsFeedBiz/api/Order/'
                            . 'updateOrder?token=1ad8fec1e88dcca6393349c5b3b86ae3&seller_order_id='.$order['seller_order_id'].'</b></pre>';
                        }
                        echo '<pre>'.print_r($order, true).'</pre>';
                    }
                }
                exit;
                break;
        }   
	
	if(isset($value) && !empty($value))
	    echo '<br/>end - ' . date('H:i:s');
    }

    /*Advertising*/
    public function advertising($action, $id_country, $sub_action=null, $return_json=false){

        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.advertising.class.php';

        $_function = 'advertising';
        $host = $this->config->item('base_url');
        $this->_country_data($id_country);
        $this->_set_session_message();
        $data = array();
        
        $ad_api = $this->_get_api('advertising'); // get config data
        $ad_config = isset($ad_api[$this->id_region]) ? $ad_api[$this->id_region] : null ;
        $ad_config['sandbox'] = true ;
        
        if($values = $this->input->post()) {
            $ad_config['accessToken'] = isset($values['token_access']) ? rawurldecode($values['token_access']) : null ;
        }

        // get advertising data from db
        $advertising_data = $this->amazon_model->get_advertising($this->id_shop, $id_country);

        $profile_id = isset($advertising_data['profile_profileId']) ? $advertising_data['profile_profileId'] : null ;
        if(isset($profile_id) && !empty($profile_id)) {
            $ad_config['profileId'] = $profile_id;
        }
       
        $refresh_access = isset($advertising_data['refresh_token']) ? $advertising_data['refresh_token'] : null ;
        if(isset($refresh_access) && !empty($refresh_access)) {
            $ad_config['refreshToken'] = $refresh_access;
        }

        # we use refresh_token because the token will expire in 30 minute
        //check expire token
        if(isset($advertising_data['access_token'])){
            $access_token = $this->amazon_model->get_advertising_access_token($this->id_shop, $id_country, 'access_token');
            $ad_config['accessToken'] = $access_token;
        }
        
        $advertise = new AmazonAdvertising($ad_config);

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        switch ($action){

            case 'remove_authentication' :
                $return = (bool)$this->amazon_model->remove_advertising($this->id_shop, $id_country);
                echo json_encode($return); exit;
                break ;
	    case 'authentication' :
                
                $profile_profileId = $profile_status = $profile_status_details = null ;
                
                // if have profile id & refresh token just show check status button
                if(isset($profile_id) && !empty($profile_id)){

                    //1. show profile id & check status button
                    $sub_action = 4;
                    $profile_profileId = $advertising_data['profile_profileId'];
                    $profile_status = $advertising_data['profile_status'];
                    $profile_status_details = $advertising_data['profile_status_details'];

                // if don't have profile || refresh token, user must login with Amazon
                } else {

                     // 2. Confirm Register Advertising //redirect from amazon_handle after login with Amazon
                    if(isset($sub_action) && $sub_action == 2){

                        if(isset($this->session->userdata['ad_authen_code'])) {
                            $code = isset($this->session->userdata['ad_authen_code']) ? $this->session->userdata['ad_authen_code'] : null ;
                            $advertise->requestAccessToken($code, $host); // return access_token, refresh_token, token_type, expires_in
                            if(isset($advertise->config['accessToken'])){
                                $data['token_access'] = $advertise->config['accessToken'];
                                
                                // save access_token, refresh_token, token_type, expires_in
                                $save_data = array();
                                $save_data['access_token'] = $advertise->config['accessToken'];
                                $save_data['refresh_token'] = $advertise->config['refreshToken'];
                                $save_data['token_type'] = $advertise->config['tokenType'];
                                $save_data['expires_in'] = $advertise->config['expiresIin'];
                                
                                $this->amazon_model->save_advertising($this->id_shop, $id_country, $save_data);

                                // show confirm register button
                            } else {
                                // throw
                                $this->session->set_userdata('error', 'Request access token fail, please try again');
                                helper_redirect('amazon/advertising/authentication/',$id_country);
                            }
                            $this->session->unset_userdata('ad_authen_code');
                        } else {
                            // throw
                            $this->session->set_userdata('error', 'Login fail, please try again');
                            helper_redirect('amazon/advertising/authentication/',$id_country);
                        }
                    }
                    
                    // 3. Register Profile to Advertising // after user click confirm registerProfile
                    else if(isset($sub_action) && $sub_action == 3){
                                
                        // profile id already in DB
                        if(isset($advertising_data['profile_profileId']) &&
                            isset($advertising_data['register_profile_status']) && $advertising_data['register_profile_status'] == "SUCCESS"){
                            /* PROFILE ID */
                            $profile_profileId = $advertising_data['profile_profileId'];
                            $profile_status = $advertising_data['profile_status'];
                            $profile_status_details = $advertising_data['profile_status_details'];

                        // Regis new profile
                        } else if(isset($advertise->config['accessToken']) || isset($advertise->config['refreshToken'])) { // need access token || refresh token

                            $countryCode = strtoupper(Amazon_Tools::domainToId($this->ext));
                            $registerProfile = $advertise->registerProfile(array("countryCode" => $countryCode));

                            //success
                            if(isset($registerProfile['success']) && $registerProfile['success'] && isset($registerProfile['response'])){
                                
                                $registerProfileResponse = $registerProfile['response'];

                                //Merchant is already registered // this will return profile_id
                                if(isset($registerProfileResponse['profileId']) &&
                                    isset($registerProfileResponse['status']) && $registerProfileResponse['status'] == "SUCCESS"){
                                    /* PROFILE ID */
                                    $profile_profileId = $registerProfileResponse['profileId'];
                                    $profile_status = $registerProfileResponse['status'];
                                    $profile_status_details =  $registerProfileResponse['statusDetails'];

                                    // save profile data
                                    $save_data = array();
                                    $save_data['profile_profileId'] = $profile_profileId;
                                    $save_data['profile_status'] = $profile_status;
                                    $save_data['profile_status_details'] = $profile_status_details;
                                    $this->amazon_model->save_advertising($this->id_shop, $id_country, $save_data);

                                } else {
                                    if(isset($registerProfileResponse['registerProfileId'])) {
                                            // save register data
                                            $save_data = array();
                                            $save_data['register_profile_id'] = $registerProfileResponse['registerProfileId'];
                                            $save_data['register_profile_status'] = $registerProfileResponse['status'];
                                            $save_data['register_profile_details'] = $registerProfileResponse['statusDetails'];
                                            $this->amazon_model->save_advertising($this->id_shop, $id_country, $save_data);
                                            $registerProfileId = ($registerProfileResponse['registerProfileId']);
                                    } else {
                                        // throw
                                        var_dump('error', '#321'); exit;
                                    }
                                }
                                
                            // unsuccess
                            } else {
                                // throw
                                var_dump('error', '#321', $registerProfile); exit;
                            }
                        }

                        // get profile status to return detail
                        if(isset($registerProfileId)) {
                            $profile = $advertise->registerProfileStatus($registerProfileId);
                             //success
                            if(isset($profile['success']) && $profile['success'] && isset($profile['response'])){
                                $profileResponse = $profile['response'];
                                if(isset($profileResponse['profileId'])) {
                                    $profile_profileId = $profileResponse['profileId'] ;
                                    $profile_status = $profileResponse['status'] ;
                                    $profile_status_details = $profileResponse['statusDetails'] ;

                                    // save profile data
                                    $save_data = array();
                                    $save_data['profile_profileId'] = $profile_profileId ;
                                    $save_data['profile_status'] = $profile_status ;
                                    $save_data['profile_status_details'] = $profile_status_details ;
                                    $this->amazon_model->save_advertising($this->id_shop, $id_country, $save_data);
                                } else {
                                    // throw
                                    var_dump('error', '#344', $profileResponse); exit;
                                }
                            // unsuccess
                            } else {
                                // throw
                                var_dump('error', '#321', $registerProfile); exit;
                            }
                        }                        
                    } 
                    // 1. Show "login with Amazon" button
                    $data['ad_client_id'] = isset($ad_config['clientId']) ? $ad_config['clientId'] : null ;
                }

                // set profile to variable
                $data['profile_profileId'] = $profile_profileId;
                $data['profile_status'] = $profile_status;
                $data['profile_status_details'] = $profile_status_details;
                $data['sub_action'] = $sub_action;
                $data['next_page'] = $_function . '/profiles';
                $this->smarty->view('amazon/AdvertisingLogin.tpl', $data);
                break;

            case 'profiles' :
                //$data['no_data'] = _get_message_code('23-00011', 'inner');
                $data['previous_page'] = $_function . '/login';
                $data['next_page'] = $_function . '/campaigns';
                $this->smarty->view('amazon/AdvertisingProfile.tpl', $data);
                break;

            case 'get_profiles' :
                $values = $this->input->post();
                $profiles = $advertise->getProfiles();
                $data['data'] = isset($profiles['success']) && $profiles['success'] && isset($profiles['response']) ? $profiles['response'] : array();
                $data['draw'] = $values['draw'];
                $data['recordsTotal'] = count($data['data']);
                $data['recordsFiltered'] = $data['recordsTotal'];
                echo json_encode($data); exit;
                break;

            case 'get_profile' :              
                if ($values = $this->input->post()) {
                    $profileId = $values['profileId'];
                    $profile = $advertise->getProfile($profileId);
                    $data['data'] = isset($profile['success']) && $profile['success'] && isset($profile['response']) ? $profile['response'] : array();
                }                
                echo json_encode($data); exit;
                break;

            case 'save_profile' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {                    
                    $updateData = array($values);
                    $updateProfiles = $advertise->updateProfiles($updateData);
                    $result['pass'] = isset($updateProfiles['success']) ? $updateProfiles['success'] : false ;
                    if(isset($updateProfiles['response'])){
                        if($result['pass']){
                            foreach ($updateProfiles['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $updateProfiles['response'];
                        }
                    } 
                }
                echo json_encode($result); exit;
                break;
                
            case 'campaigns' :
                //$data['no_data'] = _get_message_code('23-00012', 'inner');
                $data['message_campaign_name'] = $this->lang->line("message_campaign_name");
                $data['message_daily_budget'] = $this->lang->line("message_daily_budget");
                $data['message_campaign_targeting'] = $this->lang->line("message_campaign_targeting");
                $data['message_campaign'] = $this->lang->line("message_campaign");
                $data['message_campaign_status'] = $this->lang->line("message_campaign_status");
                //$data['message_bid'] = $this->lang->line("message_bid");
                //$data['message_default_bid'] = $this->lang->line("message_default_bid");
                
                $data['previous_page'] = $_function . '/profiles';
                $data['next_page'] = $_function . '/groups';
                $this->smarty->assign("add_data", $this->lang->line('Add a new campaign'));
                $this->smarty->view('amazon/AdvertisingCampaign.tpl', $data);
                break;

            case 'get_campaigns' :
                //startIndex : Optional. 0-indexed record offset for the result set. Defaults to 0.
                //count : Optional. Number of records to include in the paged response. Defaults to max page size.
                //campaignType : Optional. Restricts results to campaigns of a single campaign type. Must be sponsoredProducts
                //stateFilter : Optional. Restricts results to campaigns with state within the specified comma-separated list.
                //              Must be one of enabled, paused, archived. Default behavior is to include all
                //name: Optional. Restricts results to campaigns with the specified name
                //campaignIdFilter: Optional. Restricts results to campaigns specified in comma-separated list.
                $values = $this->input->post();
                $criteria1 = array(
                    'startIndex' => $values['start'],
                    'count' => $values['length'],
                );
                $criteria2 = array();       
                $campaigns = $advertise->listCampaigns($criteria1);     
                $total_campaigns = $advertise->listCampaigns($criteria2);
                $data['data'] = isset($campaigns['success']) && $campaigns['success'] && isset($campaigns['response']) ? $campaigns['response'] : array();
                $data['draw'] = $values['draw'];
                $data['recordsTotal'] = isset($total_campaigns['success']) && $total_campaigns['success'] && isset($total_campaigns['response']) ? count($total_campaigns) : 0 ;
                $data['recordsFiltered'] = $data['recordsTotal'];
                echo json_encode($data); exit;
                break;
            
            case 'get_campaign' :
                if ($values = $this->input->post()) {
                    $campaignId = $values['campaignId'];
                    $campaign = $advertise->getCampaign($campaignId);
                    $data['data'] = isset($campaign['success']) && $campaign['success'] && isset($campaign['response']) ? $campaign['response'] : array();;
                }
                echo json_encode($data); exit;
                break;

            case 'save_campaign' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $values['campaignId'] = (int) $values['campaignId'];
                    $updateData = array($values);
                    $updateResults = $advertise->updateCampaigns($updateData);
                    $result['pass'] = isset($updateResults['success']) ? $updateResults['success'] : false ;
                    if(isset($updateResults['response'])){
                        if($result['pass']){
                            foreach ($updateResults['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $updateResults['response'];
                        }
                    }
                }
                echo json_encode($result); exit;
                break;
                
            case 'archive_campaign' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $campaignId = $values['campaignId']; 
                    $archiveCampaign = $advertise->archiveCampaign($campaignId);
                    $result['pass'] = isset($archiveCampaign['success']) ? $archiveCampaign['success'] : false ;
                    if(isset($archiveCampaign['response'])){
//                        if($result['pass']){
//                            foreach ($archiveCampaign['response'] as $updateResult){
//                                $result = $updateResult;
//                            }
//                        } else {
                            $result = $archiveCampaign['response'];
//                        }
                    }
                }
                echo json_encode($result); exit;
                break;
                
            case 'create_campaign' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $createData = array($values);
                    $createCampaigns = $advertise->createCampaigns($createData);
                    $result['pass'] = isset($createCampaigns['success']) ? $createCampaigns['success'] : false ;
                    if(isset($createCampaigns['response'])){
                        if($result['pass']){
                            foreach ($createCampaigns['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $createCampaigns['response'];
                        }
                    }                    
                }
                echo json_encode($result); exit;
                break;

            case 'groups' :
                //$data['no_data'] = _get_message_code('23-00013', 'inner');
                $data['message_adGroup_name'] = $this->lang->line('message_adGroup_name');
                $data['message_default_bid'] = $this->lang->line('message_default_bid');
                $data['message_adGroup_status'] = $this->lang->line('message_adGroup_status');
                $data['message_adGroups'] = $this->lang->line('message_adGroups');
                $data['previous_page'] = $_function . '/campaigns';
                $data['next_page'] = $_function . '/keywords';
                //$data['next_sub_action'] = $_function . '/biddable';
                //get campaign list // only enabled 
                $campaigns = $advertise->listCampaigns(/*array("stateFilter" => "enabled")*/);
                $this->smarty->assign("campaigns", isset($campaigns['response']) ? $campaigns['response'] : array());
                $this->smarty->assign("add_data", $this->lang->line('Add a new groups'));
                $this->smarty->view('amazon/AdvertisingAdGroups.tpl', $data);
                break;
            
            case 'get_groups' :
                //startIndex : Optional. 0-indexed record offset for the result set. Defaults to 0.
                //count : Optional. Number of records to include in the paged response. Defaults to max page size.
                //campaignType : Optional. Restricts results to campaigns of a single campaign type. Must be sponsoredProducts
                //stateFilter : Optional. Restricts results to campaigns with state within the specified comma-separated list.
                //              Must be one of enabled, paused, archived. Default behavior is to include all
                //name: Optional. Restricts results to campaigns with the specified name
                //campaignIdFilter: Optional. Restricts results to campaigns specified in comma-separated list.
                //adGroupIdFilter : Optional. Restricts results to ad groups specified in comma-separated list.
                $values = $this->input->post();
                if(isset($values['start']) || isset($values['length'])) {
                    $criteria1 = array(
                        'startIndex' => $values['start'],
                        'count' => $values['length'],
                    );
                    $criteria2 = array();
                    $adGroups = $advertise->listAdGroups($criteria1);
                    $total_adGroups = $advertise->listAdGroups($criteria2);
                    $campaigns = $advertise->listCampaigns(/*array("stateFilter" => "enabled")*/);

                    $list_campaigns = array();
                    if(isset($campaigns['response']))
                    foreach ($campaigns['response'] as $campaign){
                        $list_campaigns[(int)$campaign['campaignId']] = $campaign;
                    }
                    if(isset($adGroups['response']))
                    foreach ($adGroups['response'] as $adKey => $adGroup){
                       if(isset($list_campaigns[$adGroup['campaignId']])){
                           $adGroups['response'][$adKey]['campaignName'] = $list_campaigns[$adGroup['campaignId']]['name'];
                       }
                    }
                    $data['data'] = isset($adGroups['response']) ? $adGroups['response'] : array();
                    $data['draw'] = $values['draw'];
                    $data['recordsTotal'] = isset($total_adGroups['response']) ? count($total_adGroups['response']) : 0;
                    $data['recordsFiltered'] = $data['recordsTotal'];

                    echo json_encode($data); exit;

                } else {
                    $adGroups = $advertise->listAdGroups();
                    if($return_json){
                        echo json_encode(isset($adGroups['response']) ? $adGroups['response'] : array()); exit;
                    }
                    return isset($adGroups['response']) ? $adGroups['response'] : array();
                }
                break;

            case 'get_group' :
                if ($values = $this->input->post()) {
                    $adGroupId = $values['adGroupId'];
                    $campaign = $advertise->getAdGroup($adGroupId);
                    $data['data'] = isset($campaign['response']) ? $campaign['response'] : array();
                }
                echo json_encode($data); exit;
                break;
                
            case 'save_group' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $values['adGroupId'] = (int) $values['adGroupId'];
                    $values['campaignId'] = (int) $values['campaignId'];
                    $updateData = array($values);
                    $updateAdGroups = $advertise->updateAdGroups($updateData);
                    
                    $result['pass'] = isset($updateAdGroups['success']) ? $updateAdGroups['success'] : false ;
                    if(isset($updateAdGroups['response'])){
                        if($result['pass']){
                            foreach ($updateAdGroups['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $updateAdGroups['response'];
                        }
                    }
                }              

                echo json_encode($result); exit;
                break;
                
            case 'archive_group' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $adGroupId = $values['adGroupId'];
                    $archiveResults = $advertise->archiveAdGroup($adGroupId);
                    $result = isset($archiveResults['response']) ? $archiveResults['response'] : array();
                }
                echo json_encode($result); exit;
                break;
                
            case 'create_group' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $values['campaignId'] = (int) $values['campaignId'];
                    $createData = array($values);
                    $createAdGroups = $advertise->createAdGroups($createData);
                    $result['pass'] = isset($createAdGroups['success']) ? $createAdGroups['success'] : false ;
                    if(isset($createAdGroups['response'])){
                        if($result['pass']){
                            foreach ($createAdGroups['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $createAdGroups['response'];
                        }
                    }
                }
                echo json_encode($result); exit;
                break;

            case 'keywords' :
                $data['sub_action'] = $sub_action;
                $data['previous_page'] = $_function . '/groups';
                $data['next_page'] = $_function . '/product_ad';
                //get AdGroups list 
                $adGroups = $advertise->listAdGroups();                
                $this->smarty->assign("adGroups", isset($adGroups['response']) ? $adGroups['response'] : array() );
                $add_data = array('biddable'=>$this->lang->line('Add a new keyword'), 'negative'=>$this->lang->line('Add a new negative keyword'));
                $this->smarty->assign("add_data", $add_data);
                $this->smarty->view('amazon/AdvertisingKeywords.tpl', $data);
                break;

            case 'list_ads' :
                
                $values = $this->input->post();
                $list_campaigns = $list_biddables = array();

                //get campaign list
                $campaigns = $advertise->listCampaigns();
                if(isset($campaigns['response']))
                foreach ($campaigns['response'] as $campaign){
                    $list_campaigns[(string)$campaign['campaignId']] = $campaign;
                }

                $criteria1 = array(
                    'startIndex' => $values['start'],
                    'count' => $values['length'],
                );
                $criteria2 = array( );

                 //get AdGroups list
                $adGroups = $advertise->listAdGroups($criteria1);
                $total_adGroups = $advertise->listAdGroups($criteria2);
                if(isset($adGroups['response']))
                foreach ($adGroups['response'] as $adKey => $adGroup){
                    $adGroups['response'][$adKey] = $adGroup;
                    $adGroups['response'][$adKey]['adGroupName'] = $adGroup['name'];
                    if(isset($list_campaigns[$adGroup['campaignId']])){
                        $adGroups['response'][$adKey]['campaignName'] = $list_campaigns[$adGroup['campaignId']]['name'];
                    }
                }                    

                $data['data'] =  isset($adGroups['response']) ? $adGroups['response'] : array();
                $data['draw'] = $values['draw'];
                $data['recordsTotal'] = isset($total_adGroups['response']) ? count($total_adGroups['response']) : 0;
                $data['recordsFiltered'] = $data['recordsTotal'];

                echo json_encode($data); exit;
                break;
                        
            case 'get_keywords' :
                if ($values = $this->input->post()) {
                    if(isset($values['adGroupId'])){
                        $adGroupId = (int)$values['adGroupId'];
                    }
                    $criteria1 = array(
                        'startIndex' => $values['start'],
                        'count' => $values['length'],
                        'adGroupIdFilter' => $adGroupId,
                    );
                    $criteria2 = array('adGroupIdFilter' => $adGroupId);
                    switch ($sub_action){
                        case 'biddable':
                            $biddables = $advertise->listBiddableKeywords($criteria1);
                            $total_biddables = $advertise->listBiddableKeywords($criteria2);
                            $data['data'] =  isset($biddables['response']) ? $biddables['response'] : array();
                            $data['draw'] = $values['draw'];
                            $data['recordsTotal'] = isset($total_biddables['response']) ? count($total_biddables['response']) : 0;
                            $data['recordsFiltered'] = $data['recordsTotal'];
                            echo json_encode($data); exit;
                            break;
                        case 'negative':
                            $negatives = $advertise->listNegativeKeywords($criteria1);
                            $total_negatives = $advertise->listNegativeKeywords($criteria2);
                            $data['data'] =  isset($negatives['response']) ? $negatives['response'] : array();
                            $data['draw'] = $values['draw'];
                            $data['recordsTotal'] = isset($total_negatives['response']) ? count($total_negatives['response']) : 0;
                            $data['recordsFiltered'] = $data['recordsTotal'];
                            echo json_encode($data); exit;
                            break;
                    }
                }
                break;

            case 'get_keyword' :
                if ($values = $this->input->post()) {
                    $keywordId = $values['keywordId'];
                    switch ($sub_action){
                        case 'biddable':
                            $keyword = $advertise->getBiddableKeyword($keywordId);
                            break;
                        case 'negative':
                            $keyword = $advertise->getNegativeKeyword($keywordId);
                            break;
                    }
                    $data['data'] = isset($keyword['response']) ? $keyword['response'] : array();
                }
                echo json_encode($data); exit;
                break;

            case 'save_keyword' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $values['keywordId'] = (int) $values['keywordId'];
                    $updateData = array($values);
                    switch ($sub_action){
                        case 'biddable':
                            $updateKeyword = $advertise->updateBiddableKeywords($updateData);
                            break;
                        case 'negative':
                            $updateKeyword = $advertise->updateNegativeKeywords($updateData);
                            break;
                    }
                    $result['pass'] = isset($updateKeyword['success']) ? $updateKeyword['success'] : false ;
                    if(isset($updateKeyword['response'])){
                        if($result['pass']){
                            foreach ($updateKeyword['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $updateKeyword['response'];
                        }
                    }
                }

                echo json_encode($result); exit;
                break;

            case 'archive_keyword' :
                $result = array();
                $result['pass'] = false;
                if ($values = $this->input->post()) {
                    $keywordId = $values['keywordId'];
                    switch ($sub_action){
                        case 'biddable':
                            $archiveResults = $advertise->archiveBiddableKeyword($keywordId);
                            break;
                        case 'negative':
                            $archiveResults = $advertise->archiveNegativeKeyword($keywordId);
                            break;
                    }
                    $result = isset($archiveResults['response']) ? $archiveResults['response'] : array();
                }
                echo json_encode($result); exit;
                break;
                
            case 'create_keyword' :
                $result = array();
                $result['pass'] = false;                 
                if ($values = $this->input->post()) {
                    $createData = array();
                    if(isset($values['keywordText']) && is_array($values['keywordText'])){
                        foreach ($values['keywordText'] as $keyText => $keywordText){
                            $createData[$keyText]['adGroupId'] =  (int) $values['adGroupId'];
                            $createData[$keyText]['campaignId'] =  (int) $values['campaignId'];
                            $createData[$keyText]['keywordText'] = $keywordText;
                            $createData[$keyText]['matchType'] = $values['matchType'];
                            $createData[$keyText]['state'] = $values['state'];
                        }
                    }
                    switch ($sub_action){
                        case 'biddable':
                            $createKeywords = $advertise->createBiddableKeywords($createData);
                            break;
                        case 'negative':
                            $createKeywords = $advertise->createNegativeKeywords($createData);
                            break;
                    }
                    $result['pass'] = isset($createKeywords['success']) ? $createKeywords['success'] : false ;
                    if(isset($createKeywords['response'])){
                        if($result['pass']){
                            foreach ($createKeywords['response'] as $updateResult){
                                $result = $updateResult;
                            }
                        } else {
                            $result = $createKeywords['response'];
                        }
                    }
                }
                echo json_encode($result); exit;
                break;

            case 'product_ad' :
                $data['previous_page'] = $_function . '/keywords';
                $data['next_page'] = $_function . '/keywords';

                /*$category = $this->feedbiz->marketplace_getCategoryOption($this->id_shop, null, null, null, null, null, $id_country, _ID_MARKETPLACE_AMZ_);
                if(isset($category['category'])){
                    $data['category'] = $this->category_option($category['category']);
                }*/

                //getProductsAdvertising($id_shop, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null)
                $this->smarty->view('amazon/AdvertisingProductAd.tpl', $data);
                break;

            case 'get_product_ad' :
                $values = $this->input->post();
                $criteria1 = array(
                    'startIndex' => $values['start'],
                    'count' => $values['length'],
                );
                if(isset($values['adGroupId'])){
                    $adGroupId = $values['adGroupId'];
                    $criteria1['adGroupId'] = $adGroupId;
                    $criteria2 = array('adGroupId' => $adGroupId);
                }
                $listProductAds = $advertise->listProductAds($criteria1);
                $total_productAds = $advertise->listProductAds($criteria2);
                $data['data'] =  isset($listProductAds['response']) ? $listProductAds['response'] : array();
                $data['draw'] = $values['draw'];
                $data['recordsTotal'] = isset($total_productAds['response']) ? count($total_productAds['response']) : 0;
                $data['recordsFiltered'] = $data['recordsTotal'];
                echo json_encode($data); exit;
            
                /*if(isset($values) && !empty($values)) {

                    $search = array();

                    foreach ($values['columns'] as $columns){
                        if(isset($columns['search']['value']) && !empty($columns['search']['value'])){
                            $search['id_category'] = $columns['search']['value'];
                        }
                    }

                    $order_by = $column = '';
                    $page = $values['draw'];
                    $limit = $values['length'];
                    $start = $values['start'];
                    $search['all'] = $values['search']['value'];

                    if(isset($values['order'])) {
                        foreach($values['order'] as $order_sort){
                            switch ($order_sort['column']){
                                case 0 : $column = "p.reference"; break;
                                case 1 : $column = "p.reference"; break;
                                case 2 : $column = "pa.reference"; break;
                            }
                            $sort[] = $column . ' ' . $order_sort['dir'];
                        }
                        $order_by = implode(', ', $sort);
                    }

                    $ls_sku = $ls_productAds = array();
                    
                    $product_list = $this->amazon_model->getProductsAdvertising($this->id_shop, $id_country, $start, $limit, $order_by, $search);

                    foreach ($product_list as $product){
                        $ls_sku[] =  $product['reference'];
                    }

                    $criteria1 = array(
                        'sku' => '"'. implode('","', $ls_sku) .'"',
                    );

                    // get advertise from sku
                    $listProductAds = $advertise->listProductAds($criteria1);

                    if(isset($listProductAds['response']))
                    foreach ($listProductAds['response'] as $productAd){
                        $ls_productAds[$productAd['sku']] =  $productAd;
                    }
                    
                    $data = array();
                    $data['draw'] = $page;
                    $data['recordsTotal'] = $this->amazon_model->getProductsAdvertising($this->id_shop, $id_country, null, null, $order_by, $search, true);
                    $data['recordsFiltered'] = $data['recordsTotal'];
                    $data['data'] = array();

                    foreach ($product_list as $product) {
                        $sku = $product['reference'];
                        $product['sku'] = $sku ;
                        $product['adId'] = isset($ls_productAds[$sku]['adId']) ? $ls_productAds[$sku]['adId'] : null ;
                        $product['adGroupId'] = isset($ls_productAds[$sku]['adGroupId']) ? $ls_productAds[$sku]['adGroupId'] : null ;
                        $product['campaignId'] = isset($ls_productAds[$sku]['campaignId']) ? $ls_productAds[$sku]['campaignId'] : null ;
                        $product['state'] = isset($ls_productAds[$sku]['state']) ? $ls_productAds[$sku]['state'] : null ;
                        array_push($data['data'], $product);
                    }
                    echo json_encode($data);
                }*/
                break;
        }
        
        // Save new access token
        /*if(isset($advertise->config["accessToken"]) && !empty($advertise->config["accessToken"])){
            // save profile data
            $save_data = array();
            $save_data['access_token'] = $advertise->config["accessToken"] ;
            $this->amazon_model->save_advertising($this->id_shop, $id_country, $save_data);
        }*/
        
    }
    
    /*Shipping Templates*/
    public function get_shipping_group_name($id_country, $return_json=false){
	
	$shipping_groupnames = $this->amazon_model->get_shipping_group_names($this->id_shop, $id_country);
	
	if(!$return_json)
	    return $shipping_groupnames;
	    
	echo json_encode ($shipping_groupnames) ; 
	    
    }

    /* FBA */    
    public function fba($action, $id_country){

        $this->_country_data($id_country);
        $this->_set_session_message();
        $data = array();        

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        //ALTER TABLE `amazon_fba` ADD COLUMN `shipment_details`  text NULL AFTER `fba_master_platform`;

	switch ($action){	    
	   
	    case 'parameters' :
                $shipping_groupnames = $this->amazon_model->get_shipping_group_names($this->id_shop, $id_country);
                if(!isset($shipping_groupnames) || empty($shipping_groupnames)) {
                    $data['no_shipping_groups'] = _get_message_code('22-00015', 'inner');
                } else {
                    $data['shipping_groups'] = $shipping_groupnames;
                }                
		$data['fba'] = $this->amazon_model_user->get_fba($this->id_user, $this->ext, $this->id_shop);
		$data['master_platform'] = $this->amazon_model_user->get_fba_master($this->id_user, $this->id_shop, $this->region);
		$this->smarty->view('amazon/FbaParameters.tpl', $data); break;
	    
	    case 'carriers' : 
		require_once dirname(__FILE__) . '/../libraries/Amazon/controllers/amazon.display.carrier.php';
		$params = $this->_setParams();        
		$DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params); 
		$data['mapping_carrier'] = $DisplayForm->get_mapping_carrier($id_country);		
		$data['carriers'] = $this->feedbiz->getCarriers($this->user_name, $this->id_shop);
		$data['amazon_carriers'] = Amazon_Carrier::$carrier_fba;		
		$this->smarty->view('amazon/FbaCarriersMapping.tpl', $data); break;

            case 'shipment_details' :
		//$data['no_data'] = _get_message_code('23-00015', 'inner');
		$data['fba'] = $this->amazon_model_user->get_fba($this->id_user, $this->ext, $this->id_shop);
                $this->smarty->assign("not_continue", true);
		$this->smarty->view('amazon/FbaShipmentDetails.tpl', $data); break;

	    case 'save_fba' :	
		if ($values = $this->input->post()) {
                    $values['id_customer'] = $this->id_user;
                    $values['user_name'] = $this->user_name;
                    $values['id_shop'] = $this->id_shop;
                    $values['id_country'] = $id_country;
                    $values['ext'] = $this->ext;
                    $values['region'] = $this->region;
                    $values['date_add'] = date('Y-m-d H:i:s');
                    if(isset($values['shipment_details']) && !empty($values['shipment_details'])){ // 2016-10-06
                        $values['shipment_details'] = serialize($values['shipment_details']);
                        $redirect = 'shipment_details/'.$id_country;
                        $continue_redirect = '';
                    } else {
                        $values['fba_formula'] = isset($values['fba_formula']) && $values['fba_formula'] ? $values['fba_formula'] : null;
                        $values['fba_estimated_fees'] = isset($values['fba_estimated_fees']) && $values['fba_estimated_fees'] ? $values['fba_estimated_fees'] : null;
                        $values['fba_fulfillment_fees'] = isset($values['fba_fulfillment_fees']) && $values['fba_fulfillment_fees'] ? $values['fba_fulfillment_fees'] : null;
                        $values['default_fba_fees'] = (isset($values['default_fba_fees']) && !empty($values['default_fba_fees'])) ? $values['default_fba_fees'] : null;
                        $values['default_shipping_template'] = isset($values['default_shipping_template']) ? $values['default_shipping_template'] : null;
                        $values['fba_multichannel'] = isset($values['fba_multichannel']) && $values['fba_multichannel'] ? $values['fba_multichannel'] : 0;
                        $values['fba_multichannel_auto'] = isset($values['fba_multichannel_auto']) && $values['fba_multichannel_auto'] ? $values['fba_multichannel_auto'] : 0;
                        $values['fba_decrease_stock'] = isset($values['fba_decrease_stock']) && $values['fba_decrease_stock'] ? $values['fba_decrease_stock'] : 0;
                        $values['fba_stock_behaviour'] = isset($values['fba_stock_behaviour']) && $values['fba_stock_behaviour'] ? $values['fba_stock_behaviour'] : null;
                        $redirect = 'parameters/'.$id_country;
                        $continue_redirect = 'carriers/'.$id_country;
                    }
                    if (!$this->amazon_model_user->set_fba($values)) {
                        $this->session->set_userdata('error', 'save_unsuccessful');
                    } else {
                        $this->session->set_userdata('message', 'save_successful');
                    }
                    helper_redirect('amazon/fba/', (isset($values['save']) && ($values['save'] == 'continue') && strlen($continue_redirect) > 0) ? $continue_redirect : $redirect);
		}
		helper_redirect('amazon/fba/', $redirect);
		break;
	    
	    case 'set_master_platform' :
		if ($value = $this->input->post()) {
		    
		    $values = array();
		    if(isset($value['fba_master_platform']) && $value['fba_master_platform']){
			$fba_master_platform = $value['fba_master_platform'];
			$values['fba_master_platform'] = 0 ;
		    }
		    
		    $values['user_name'] = $this->user_name;
		    $values['id_customer'] = $this->id_user;
		    $values['id_shop'] = $this->id_shop;
		    $values['date_add'] = date('Y-m-d H:i:s');
		    
		    if ($this->amazon_model_user->set_fba($values, $this->region) ) {
		    
			$values['region'] = $this->region;
			$values['fba_master_platform'] = isset($fba_master_platform) ? (int) $fba_master_platform : 0 ;
			$values['id_country'] = $id_country;
			$values['ext'] = $this->ext;
			
			if (!$this->amazon_model_user->set_fba($values/*, $this->region*/)) {
			    echo false;
			} else {
			    echo true;
			}
			
		    }
		}
		echo false;
		break;
	    case 'save_fba_carriers' :	
		
		if ($this->input->post()) {	
		    
		    $values = $this->input->post();
		    
		    if (!$this->amazon_model->save_mapping_carrier($values, $this->id_shop, $this->id_mode, $values['id_country'])) {
			$this->session->set_userdata('error', 'save_unsuccessful');
		    } else {
			$this->session->set_userdata('message', 'save_successful');
		    }		    
		}
		
		helper_redirect('amazon/fba/', (isset($values['save']) && ($values['save']=='continue')) ? 'shipment_details/'.$id_country : 'carriers/'.$id_country);
		break;
	}
    }   
    /** End FBA **/
    
    /** Start Repricing **/
    public function repricing($page = null, $country = null, $option = null, $json_code = false, $debug = false) {        
        
        if (!isset($country))
            redirect('/marketplace/configuration');
      
        $this->_country_data($country);
        $this->_set_session_message();
	
	$ext = Amazon_Tools::iso_codeToId($this->ext);
        $data = array();

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
        
        switch ($page){
            case 'parameters': 
                
                $api_key = $this->amazon_model_user->get_repricing_parameters($this->id_user, $this->id_shop);
                if(isset($api_key[$country]))
                    $data = array_merge ($data, $api_key[$country]);
                $this->smarty->assign("api_key", $api_key);
                if($json_code){
                    echo json_encode($data);
                } else {
                    $this->smarty->view('amazon/RepricingParameters.tpl', $data); 
                }
                break;
            
             case 'check':
                 
                require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.repricing.php';
                $pass = false;
                $message = '';
                $value = $this->input->post();
                
                if($debug){
                    $value['aws_key_id'] = 'AKIAJSZMJLF3AGZIBOQQ';
                    $value['aws_secret_key'] = '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';
                }
                
                if (!empty($value)) {                                       
                    $params = $this->_setParams();                 
                    $repricing = new AmazonRepricing($params, $debug);
                    $result = $repricing->listQueues($value['aws_key_id'], $value['aws_secret_key']);
                    $message = $result;                    
                    if(is_array($message)){
                        foreach($message as $k => $v){
                            if(is_bool($v) || is_numeric($v) || is_object($v) || is_array($v)){continue;}
                            if($this->lang->line($v) == false){continue;}
                            $message[$k] = $this->lang->line($v);
                        }
                    }
                    if( isset($result['RequestId']) ){
                        $pass = true;                        
                    }
                    
                } 
                
                echo json_encode(array('pass' => $pass, 'result'=> $message));
                break;
            
            case 'service_settings' :
                
                require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.repricing.automaton.php';
                $value = $this->input->post();
                
                if(isset($value['action'])) {
		    
                    $options = null;
                    $params = $this->_setParams();  
		    $params->language = $this->language;
		    
                    $repricing = new AmazonRepricingAutomaton($params, $debug);
                    
                    if(isset($value['purge_queue']) && !empty($value['purge_queue'])) {
                        $options = $value['purge_queue'];
                    }
                    $repricing->Dispatch($value['action'], $options);
                }else{
		    
		    if(isset($option)){
			
			$params = $this->_setParams();   
			$params->language = $this->language;
			
			$repricing = new AmazonRepricingAutomaton($params, $debug);

			$repricing->Dispatch($option);
		    }
		}
                
                $this->smarty->assign("Repricing_SUBSCRIBE", AmazonRepricingAutomaton::SUBSCRIBE);
                $this->smarty->assign("Repricing_CANCEL", AmazonRepricingAutomaton::CANCEL);
                $this->smarty->assign("Repricing_CHECK", AmazonRepricingAutomaton::CHECK);
                $this->smarty->assign("Repricing_PURGE", AmazonRepricingAutomaton::PURGE);                    
                $this->smarty->view('amazon/RepricingServiceSettings.tpl', $data);
                break; 
               
            case 'strategies' :
                
                $repricing_strategies = $this->get_repricing_strategies($country);
                $data['strategies'] = $repricing_strategies['strategies'];
        
                if(isset($data['strategies']) && !empty($data['strategies'])){
                    $this->smarty->assign("size_data", sizeof($data['strategies']));
                }
                
                $this->smarty->assign("not_continue", true);
                $this->smarty->assign("behavior", $repricing_strategies['behavior']);
                $this->smarty->assign("add_data", $this->lang->line('Add a new repricing strategy'));
                $this->smarty->view('amazon/RepricingStrategies.tpl', $data);
                break;
            
            case 'product_strategies' :
                
                if(isset($this->shop_default))
                    $this->smarty->assign("shop_name", $this->shop_default);
                else
                    $this->smarty->assign("shop_name", $this->id_shop);
                
                if($option == 'products'){ 
                    $category = $this->feedbiz->marketplace_getCategoryOption($this->id_shop, null, null, null, null, null, $country, _ID_MARKETPLACE_AMZ_);           
                    if(isset($category['category'])){
                        $data['category'] = $this->category_option($category['category']);
                    }
                    $this->smarty->view('amazon/RepricingProductStrategies.tpl', $data);
                } else if($option == "categories") {
                    $this->smarty->view('amazon/RepricingCategoryStrategies.tpl', $data);
                }
                
                break;
		
	    case 'repricing_trigger' :
		
		// TO DO TRIGGER a reprice
		$result = $this->amazon_model->update_repricing_product_update_log($this->id_shop, $country);
			
		echo json_encode(array('pass' => $result));
		break;
	    
	    case 'logs' :
		$data = array();
		$this->smarty->assign("ext_key", $ext);
		$this->smarty->view('amazon/RepricingLogs.tpl', $data);
		break;
	    
	    case 'logs_page' :
		
		$values = $this->input->post();

		if(isset($values) && !empty($values)) {

		    $page = $values['draw'];
		    $limit = $values['length'];
		    $start = $values['start'];
		    $search = $values['search']['value'];

		    $sort = array();
		    $column = null;
		    foreach($values['order'] as $order_sort){
			switch ($order_sort['column']){
			    case 0 : $column = "sku"; break;
			    case 1 : $column = "date_upd"; break;
			    case 2 : $column = "reprice"; break;
			}
			$sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
		    }
		    $order_by = implode(', ', $sort);
		  
		    $data['draw'] = $page;
		    $data['recordsTotal'] = $this->amazon_model->get_repricing_logs($this->id_shop, $country, null, null, $order_by, $search, true);
		    $data['recordsFiltered'] = $data['recordsTotal'];
		    $data['data'] = array();
		    $logs = $this->amazon_model->get_repricing_logs($this->id_shop, $country, $start, $limit, $order_by, $search);

		    foreach ($logs as $log){
			array_push($data['data'], $log);
		    }
		}
		
		echo json_encode($data); 
		
		break;
        }
        
    }
    
    public function category_strategies_page($id_country = null){
        
        $this->_country_data($id_country);
        $id_lang = isset($this->api_settings['id_lang']) ? $this->api_settings['id_lang'] : null;
        $products = $this->amazon_model->getCategories($this->id_shop, $id_country, $id_lang);
        $data = array();
        $data['name'] = 'ProductOptionByCategory';
        $data['category'] = $products['category'];
        
        echo json_encode($data);        
    }
    
    public function save_category_strategies($id_country = null){
        $values = $this->input->post();
        if(isset($values['category_strategies']) && !empty($values['category_strategies'])){            
            $result = $this->amazon_model->saveCategoriesStrategies($values['category_strategies'], $this->id_shop, $id_country);           
        }        
        echo json_encode($result);
    }
    
    public function category_option($category, $is_child = false, $nbsp = '&nbsp;'){
        $select_option = '';
        $icon = ' &#8627; ';
        if(!empty($category)){
            foreach ($category as $cat){
                if($is_child){
                    $select_option .= '<option value="'.$cat['id_category'].'" class="child">'.$nbsp.$icon.$cat['category'].'</option>';
                } else {
                    if(isset($cat['children']) && !empty($cat['children'])){
                        $select_option .= '<option value="'.$cat['id_category'].'" class="parent">'.$cat['category'].'</option>';
                    } else {
                        $select_option .= '<option value="'.$cat['id_category'].'" >'.$cat['category'].'</option>';
                    }
                }
                if(isset($cat['children']) && !empty($cat['children'])){
                    $nbsp = ' &nbsp; ' . $nbsp;
                    $select_option .= $this->category_option($cat['children'], true, $nbsp);
                } 
                
            }
        }
        return $select_option;
    }
         
    public function product_strategies_download_template(){
        $this->amazon_model->productStrategiesDownloadTemplate();
    }    
    
    public function product_strategies_download($id_country = null){
        $values = $this->input->post();
        $this->amazon_model->productStrategiesDownload($values,  $this->id_shop, $id_country);
    }
    
    public function product_strategies_upload_template($id_country = null){        
         
        $this->load->helper('form');                 
        $config['upload_path'] = USERDATA_PATH  . $this->user_name .'/process/';
        $config['allowed_types'] = 'txt|csv';       
        $config['overwrite'] = true;      
        $config['file_name'] = 'product_strategies';      
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('file-upload')){
            echo json_encode(array('error' => $this->upload->display_errors())); 
            exit;
        }
        if(!$this->upload->data()){
            echo json_encode(array('error' => $this->upload->display_errors())); 
            exit;
        }
       
        echo json_encode( array( 'pass' => true ));
    }
    
    public function product_strategies_page($id_country = null){
        
        $values = $this->input->post();
        if(isset($values) && !empty($values)) {
             
            $search = array();
            
            foreach ($values['columns'] as $columns){
                if(isset($columns['search']['value']) && !empty($columns['search']['value'])){
                    $search['id_category'] = $columns['search']['value'];
                }
            }
            
            $order_by = $column = '';
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search['all'] = $values['search']['value'];
            
            if(isset($values['order'])) {
                foreach($values['order'] as $order_sort){
                    switch ($order_sort['column']){
                        case 0 : $column = "p.reference"; break;
                        case 1 : $column = "p.reference"; break;
                        case 2 : $column = "pa.reference"; break;
                    }
                    $sort[] = $column . ' ' . $order_sort['dir'];
                }
                $order_by = implode(', ', $sort);
            }
            
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_model->getProductStrategies($this->id_shop, $id_country, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $products = $this->amazon_model->getProductStrategies($this->id_shop, $id_country, $start, $limit, $order_by, $search);
            
            foreach ($products as $product) {
                array_push($data['data'], $product);
            }
            
            echo json_encode($data); 
        }
    }
    
    public function get_product_strategy_by_sku($id_country = null){
        
        $value = $this->input->post();
        $result = array();
        if(isset($value['sku'])){
            $result = $this->amazon_model->getProductStrategyBySku($value['sku'], $this->id_shop, $id_country);
        }
        echo json_encode($result);
        
    }
    
    public function save_product_strategies($id_country = null){

        //$api_key = $this->amazon_model_user->get_repricing_parameters($this->id_user, $this->id_shop);
        //$country = array_keys($api_key);
        $result = array();
        $values = $this->input->post();
        
        if(isset($values['product_strategies']) && !empty($values['product_strategies'])){
            $result = $this->amazon_model->saveProductStrategy($values['product_strategies'], $this->id_shop, $id_country);
        }        

        echo json_encode($result);
            /*if(isset($values['apply_all']) && $values['apply_all']) {
                $apply_all = (bool)$values['apply_all'];

            }*/
        /*$values['product_strategies']['0583215011196']['child']['0583215011196']['sku']='0583215011196';
        $values['product_strategies']['0583215011196']['child']['0583215011196']['id_product']='1088';
        $values['product_strategies']['0583215011196']['child']['0583215011196']['id_product_attribute']='67819';
        $values['product_strategies']['0583215011196']['child']['0583215011196']['sku']='0583215011196';
        $values['product_strategies']['0583215011196']['child']['0583215011196']['parent_sku']='0583215011196';
        $values['apply_all'] = 1;*/
    }
    
    public function reset_product_strategies($id_country = null) {
        $result = array();
        $values = $this->input->post();
        if(isset($values['id_product']) && !empty($values['id_product'])){            
            $result = $this->amazon_model->resetProductStrategy($values['id_product'], $this->id_shop, $id_country);            
        }       
        echo json_encode($result);
    }
    
    public function reset_category_strategies($id_country = null) {
        $result = array();
        $values = $this->input->post();
        if(isset($values['id_category']) && !empty($values['id_category'])){            
            $result = $this->amazon_model->resetCategoryStrategy($values['id_category'], $this->id_shop, $id_country);            
        }       
        echo json_encode($result);
    }
    
    public function get_repricing_strategies($id_country){
        
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.strategies.php';
        
        $result = array();

        $strategies = new AmazonStrategy($this->user_name);
        $result['strategies'] = $strategies->get_strategies($this->id_shop, $id_country);
        
        $result['behavior']['Inactive'] = AmazonStrategy::_behaviorInactive;
        $result['behavior']['Automatic'] = AmazonStrategy::_behaviorAutomatic;
        $result['behavior']['Manual'] = AmazonStrategy::_behaviorManual;
        
        return $result;
        
    }

    public function delete_repricing_strategies($id_strategy, $id_shop, $id_country) {
        
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.strategies.php';
        
        $pass = true;
        $strategies = new AmazonStrategy($this->user_name);
        if (!$strategies->delete($id_strategy, $id_shop, $id_country)) {
             $pass = false;      
        }

        echo json_encode(array('pass' => $pass)); die();
    } 
    
    public function save_repricing_strategies($id_country) {
        
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.strategies.php';
        
        if($this->input->post()){
            $strategies = new AmazonStrategy($this->user_name);
            $value = $this->input->post();
            
            if (!$strategies->save($value)) {
                $this->session->set_userdata('error', 'save_unsuccessful');                
            }
            
            $this->session->set_userdata('message', 'save_successful'); 
            redirect('amazon/repricing/strategies/' . $id_country);
        }
    } 
    
    public function save_repricing_parameters() {
        
        if ($this->input->post()) {
            
            $values = $this->input->post();            
            $id_country = $values['id_country'];
            
            if (isset($this->session->userdata['menu']))
                $marketplace = $this->session->userdata['menu'];
            if (isset($marketplace[self::TAG]['submenu'][$values['id_country']])) {
                $ext = $marketplace[self::TAG]['submenu'][$values['id_country']]['ext'];
            }
            
            $repricing_api_field =  array('aws_key_id','aws_secret_key','defaut_override_shipping');
            $value = array();
            foreach ($repricing_api_field as $key => $name){
                $value[$key]['id_customer'] = $this->id_user;
                $value[$key]['user_name'] = $this->user_name;
                $value[$key]['id_shop'] = $this->id_shop;
                $value[$key]['id_country'] = $id_country;
                $value[$key]['ext'] = $ext;
                $value[$key]['name'] = $name;
                $value[$key]['value'] = $values[$name];
                $value[$key]['date_upd'] = date('Y-m-d H:i:s');
            }

            if (!$this->amazon_model_user->set_repricing_parameters($value)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/repricing/parameters/' . $id_country);
            }
            
            $this->session->set_userdata('message', 'save_successful'); 
            
            if (isset($values['save'])) {
                if ($values['save'] == "continue") {
                    redirect('amazon/repricing/service_settings/' . $id_country);
                } else {
                    redirect('amazon/repricing/parameters/' . $id_country);
                }
            } else {
                redirect('amazon/repricing/parameters/' . $id_country);
            }
        }
    }
    /** End Repricing **/
    
    /** Error Resoluation **/    
    public function error_resolution_construct(){        
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.validValues.php';  
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.error.resolution.php';        
        $this->attribute_name_exeption = AmazonErrorResolution::$err_attribute_exeption;
        $this->amazon_err = new AmazonErrorResolution($this->user_name);                
    }
    
    public function error_resolutions($error_type = null, $country = null){
        $this->error_resolution_construct();
        $data = array();
        $data['products'] = ucwords(AmazonDatabase::CREATE);
        $data['offers'] = ucwords(AmazonDatabase::SYNC);
        $data['recreate'] = AmazonErrorResolution::_RECREATE;
        $data['creation'] = AmazonErrorResolution::_CREATION;
        $data['error_type'] = $error_type; // offers or products
        $data['error_type_lang'] = $this->lang->line($error_type); // offers or products
        $this->_country_data($country);
        $this->amazon_model->update_error_notification($this->id_shop, $country);
        $this->smarty->view('amazon/ErrorResolutions.tpl', $data);
    }
        
    public function error_data($type, $country, $action_type, $start=null, $debug=false){
        
        $this->error_resolution_construct();  
        $this->_country_data($country); 
       
        $search = array();

        if($debug){
            $values = array(
                'draw' => 1,
                'length' => 5,
                'start' => isset($start)?$start:0,
                'order' => array(0=>array('column'=>1,'dir'=>'desc')),
            );
        } else {
            $values = $this->input->post();
        }
        
        if(isset($values) && !empty($values)) {            

            $priority = array();
            $msgs = $this->amazon_model_user->get_error_resolutions($type, null, mb_substr($this->language, 0, 2));

            foreach ($msgs as $key => $message){
                if(isset($message['error_priority']) && $message['error_priority']){
                    $priority[$key] = $message;
                }
            }

            $page = $values['draw'];
            $page_real = ($values['start']==0) ? 1 : ($values['start']/$values['length'])+1;
            $limit = $values['length'];
            $start = $values['start'];
            $sort = $prior_sort = array();
            
            foreach($values['order'] as $order_sort){
                switch ($order_sort['column']){
                    case 1 : $column = "asrl.result_message_code"; break;
                }
                $sort[] = $column . '*1 asc' ;
            }
            
            if(isset($values['search']['value']) && !empty($values['search']['value'])){
                if(preg_match('/sku=/',$values['search']['value'])){
                    $search['sku'] = str_replace('sku=', '', $values['search']['value']);
                } else {
                    $search['all'] = $values['search']['value'];
                }
            }
            
            $order_by = implode(', ', $sort);
            $data = $error_log = $history = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_err->get_amazon_error_log($country, $this->id_shop, $action_type, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
           
            $where = null;
            $priority_logs_totals = 0;
            
            if($debug){
                var_dump('Page : ' . $page_real);
                var_dump('Type : ' . $type);
                var_dump('Action Type : ' . $action_type);
            }

            // Prior Error
            if(count($priority)){
                
                if($debug){
                    var_dump( '------ priority_logs -----', 'count priority : ' . (count($priority)), 'start : ' . $start, 'limit : ' . $limit, '------ priority_logs -----');
                }

                $where = 'asrl.result_message_code IN (\''.implode("','", array_keys($priority)).'\')';
                $priority_logs_totals = $this->amazon_err->get_amazon_error_log($country, $this->id_shop, $action_type, null, null, $order_by, $search, true, $where);
                $priority_logs = $this->amazon_err->get_amazon_error_log($country, $this->id_shop, $action_type, $start, $limit, $order_by, $search, false, $where);
            }
            
            $error_key = 0;

            // Prior Error
            if(isset($priority_logs)) {
                foreach ($priority_logs as $log){
                    if(isset($msgs[$log['code']]) && !empty($msgs[$log['code']]) && isset($msgs[$log['code']]['error_priority'])) {

                        $error_key = isset($history['key']) ? ($history['key']+1) : $error_key;
                        $history['key'] = $error_key;

                        $msg = $msgs[$log['code']];
                        $error_log[$error_key]['code'] = $log['code'];
                        $error_log[$error_key]['description'] = $msg['error_details'] ;
                        $error_log[$error_key]['allow_overide'] = (isset($msg['allow_overide']) && $msg['allow_overide'] == 1) ? 1 : 0;
                        $error_log[$error_key]['show_message'] = (isset($msg['show_message']) && $msg['show_message'] ==1) ? 1 : 0;
                        $error_log[$error_key]['resolutions'] = $msg['error_resolution'];
                        $error_log[$error_key]['others'] = $msg['other'];
                        $error_log[$error_key]['important'] = true;
                        $error_log[$error_key]['rows'] = $log['rows'];

                        if(isset($log['sku']) && !empty($log['sku'])) {
                            $error_log[$error_key]['sku'] = $log['sku'];
                        }

                        // error type
                        switch ($msg['error_solved_type']) {
                            case AmazonErrorResolution::_CREATION:  $error_log[$error_key]['creation'] = true;  break;
                            case AmazonErrorResolution::_EDIT_ATTRIBUTE:  $error_log[$error_key]['edit_attribute'] = true;  break;
                            case AmazonErrorResolution::_RECREATE:  $error_log[$error_key]['recreate'] = true;  break;
                            case AmazonErrorResolution::_SOLVED:  $error_log[$error_key]['solved'] = true;  break;
                            case AmazonErrorResolution::_OVERRIDE:  $error_log[$error_key]['override'] = !empty($error_log[$error_key]['others']) ? $error_log[$error_key]['others']:null;
                                break;
                        }
                        if($debug){
                            var_dump('error_key : ' . $error_key);
                        }
                    }
                }                
            }

            if(($error_key != ($limit-1)) || empty($error_log)){ //if prior not full in the table
                
                if(isset($error_log)){
                    if($priority_logs_totals >= $limit){
                        $start = $limit-(((($page_real-1)*$limit)+1)-count($error_log)/**(-1)*/); // prior more than limit
                    } else {
                        $start = ($limit-count($error_log))*($page_real-1); // prior less than limit = 10
                    }
                    $limit = $limit-count($error_log);
                }

                if(empty($error_log)){
                    $start = (($page_real-1)*$limit) - $priority_logs_totals;
                } 

                if($debug){
                    var_dump( '------ logs -----','real page : '.$page_real,'count priority totals : '.($priority_logs_totals),'start: '.$start, 'limit : '.$limit,'------ logs -----');
                }

                // Normal error
                $where = 'asrl.result_message_code NOT IN (\''.implode("','", array_keys($priority)).'\')';
               
                $logs = $this->amazon_err->get_amazon_error_log($country, $this->id_shop, $action_type, $start, $limit, $order_by, $search, false, $where);

                if(count($error_log) > 0){
                    $key = ($error_key+1);
                } else {
                    $key = ($error_key);
                }
                
                // Normal error
                foreach ($logs as $log){
                    if(isset($msgs[$log['code']]) && !empty($msgs[$log['code']])) {
                        $msg = $msgs[$log['code']];
                        $error_log[$key]['code'] = $log['code'];
                        $error_log[$key]['description'] = $msg['error_details'] ;
                        $error_log[$key]['allow_overide'] = (isset($msg['allow_overide']) && $msg['allow_overide'] == 1) ? 1 : 0;
                        $error_log[$key]['show_message'] = (isset($msg['show_message']) && $msg['show_message'] ==1) ? 1 : 0;
                        $error_log[$key]['resolutions'] = $msg['error_resolution'];
                        $error_log[$key]['others'] = $msg['other'];
                        $error_log[$key]['rows'] = $log['rows'];

                        if(isset($log['sku']) && !empty($log['sku'])) {
                            $error_log[$key]['sku'] = $log['sku'];
                        }
                        
                        // error type
                        switch ($msg['error_solved_type']) {
                            case AmazonErrorResolution::_CREATION:  $error_log[$key]['creation'] = true;  break;
                            case AmazonErrorResolution::_EDIT_ATTRIBUTE:  $error_log[$key]['edit_attribute'] = true;  break;
                            case AmazonErrorResolution::_RECREATE:  $error_log[$key]['recreate'] = true;  break;
                            case AmazonErrorResolution::_SOLVED:  $error_log[$key]['solved'] = true;  break;
                            case AmazonErrorResolution::_OVERRIDE:  $error_log[$key]['override'] = !empty($error_log[$key]['others']) ? $error_log[$key]['others']:null;
                                break;
                        }
                    } else {
                        $error_log[$key] = $log;
                    }
                    if($debug){
                        var_dump('key : ' . $key);
                    }
                    $key++;
                }
            }
            
            ksort($error_log);
            if($debug){
                var_dump($error_log);exit;
            }
            $data['data'] = $error_log;
            echo json_encode($data); 
        }
    }   
    
    public function error_resolution_details($country, $error_code, $action_type, $allow_overide = null, $sku = null){
        
        $this->error_resolution_construct();
        $this->_country_data($country);
        
        $values = $this->input->post();
        $search = '';   
        
        /*$values = array(
            'draw' => 1,
            'length' => 10,
            'start' => 0,
            'order' => array(0=>array('column'=>1,'dir'=>'asc')),
        );*/
        
        if(isset($sku) && !empty($sku)){
            $search = $sku;
        }
        
        if(isset($values) && !empty($values)) {
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $sort = array();
            foreach($values['order'] as $order_sort){
                switch ($order_sort['column']){
                    case 1 : $column = "asrl.result_message_code"; break;
                }
                $sort[] = $column . ' ' . $order_sort['dir'];
            }

            $order_by = implode(', ', $sort);
        }
            
        $data = array();
        $data['draw'] = $page;                
        $data['recordsTotal'] = $this->amazon_err->get_amazon_error_log_details($this->api_settings, $error_code, $action_type,$allow_overide,null,null,$order_by,$search, true);
        $data['recordsFiltered'] = $data['recordsTotal'];
        $data['data'] = array();
        $logs = $this->amazon_err->get_amazon_error_log_details($this->api_settings, $error_code, $action_type, $allow_overide, $start, $limit, $order_by, $search);
	
        foreach ($logs as $log){
            array_push($data['data'], $log);
        }

        echo json_encode($data);         
    }
   
    public function error_edit_data($type, $country, $error_code, $action_type, $override = null){
        
        $this->_country_data($country);
        $this->error_resolution_construct();
        $valid_value = new ValidValue($this->ext);
        $data = array();        
        $skus = $this->input->post();

        if(!empty($skus['sku'])) {
	
	    //$amazon_err_models = $this->amazon_err->getModels($country, $this->id_shop, $skus['sku']);
	    $amazon_err_valid_value = $valid_value->getAttributesForProductType();	
	    $amazon_err_resolutions = $this->amazon_model_user->get_error_resolutions($type, null, mb_substr($this->language, 0, 2));
	    $amazon_err_result_description = $this->amazon_err->get_amazon_error_result_description($this->api_settings, $error_code, $skus['sku'], false, true);
	    
	    foreach($skus['sku'] as $key => $sku_item){
                if(!empty($sku_item)){  
                    
                    $amazon_attribute = '';
                    $data['sku'][$key]['name'] = trim(str_replace("&", "", $sku_item));
                    $data['sku'][$key]['key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $data['sku'][$key]['name']);
                    
                    //$models = isset($amazon_err_models[$sku_item]) ? $amazon_err_models[$sku_item] : null;
		    $models = $this->amazon_err->getModels($country, $this->id_shop, $sku_item);

                    if(isset($models) && isset($models['id_model'])){
                        $data['sku'][$key]['id_model'] = $models['id_model'];
                        $data['sku'][$key]['id_product'] = $models['id_product'];
                        $data['sku'][$key]['id_product_attribute'] = $models['id_product_attribute'];
                        if(isset($models['universe']) && !empty($models['universe'])){                        
                            $amazon_attribute = isset($amazon_err_valid_value['main']) ? $amazon_err_valid_value['main'] : '';
                            $amazon_attribute = isset($amazon_err_valid_value[$models['universe']]) ? $amazon_err_valid_value[$models['universe']] : $amazon_attribute;
                            $data['sku'][$key]['amazon_attribute'] = $amazon_attribute;     
                        }
                    } else {
                        $data['sku'][$key]['missing_model'] = 'No Models defined to "'.$data['sku'][$key]['name'].'". Please set up Models, products universe in the Models and then map your products universe with Amazon in Categories tab.';
                    }
                    
                    // attribute field
                    $attribute = array();
                    if(isset($override)){
                        $attribute[0]['attribute_field'] = $override;   
                        if(isset($this->attribute_name_exeption[$attribute[0]['attribute_field']])){
                            $attribute[0]['attribute_field'] = $this->attribute_name_exeption[$attribute[0]['attribute_field']];
                        }
                        $attribute[0]['Amazon_value'] = '';
                    } else{
                        
                        // Get Message
                        $msg = $amazon_err_resolutions[$error_code];
                        $msg_pattern = $msg_name = null;
                        
                        if(!empty($msg['message_pattern'])){
                            $message_pattern = unserialize($msg['message_pattern']);
                            $msg_pattern = $message_pattern['msg_pattern'];
                            $msg_name = $message_pattern['msg_name'];
                        }                        
                        
                        $message = $amazon_err_result_description[$error_code][$data['sku'][$key]['name']];  
                        $word = $this->amazon_err->message_pattern($message['message'], $msg_pattern, $msg_name);
                        $attribute = $word; 
                    }
                    
                    $data['sku'][$key]['attribute'] = $attribute ;
                    //$data['sku'][$key][]['Amazon_value'] = !empty($word['Amazon_value']) ? $word['Amazon_value'] : '';
                }
            }
        } else {
            echo json_encode(false); exit;
        }
        
        $this->smarty->assign("popup", true);
        $this->smarty->assign("not_continue", true);
        $this->smarty->assign("not_previous", true);
        $this->smarty->assign("action_type", $action_type);
        $this->smarty->assign("id_shop", $this->id_shop);
        $this->smarty->assign("error_code", $error_code);
        $this->smarty->view('amazon/EditAttribute.tpl', $data);
    }     
    
    public function solved_error($id_country, $error_code, $action_type, $actions = null){
        
        $this->_country_data($id_country);
        $this->error_resolution_construct();

        if ($this->input->post()) {  
            
            $datails = array();
            $values = $this->input->post();      

            if(isset($values['sku']) && !empty($values['sku'])){
                foreach($values['sku'] as $sku){
                    
                    $id_shop = (int)$this->id_shop;
                    // Save Attribute Overide
                    if(!$this->amazon_err->update_result_code($id_shop, $id_country, $sku, $error_code, $action_type, $actions, false, $this->ext)){
                        $datails['error'][$sku] = true;
                        continue;
                    } else {
                        $datails['success'][$sku] = true;
                    }
                }
            } 
            echo json_encode(array('pass' => true, 'detail' => $datails)); exit;
        }        
        echo json_encode(array('pass' => false, 'detail' => array('empty_data' => true)));  exit;        
    }
    
    public function confirm_asin($id_country, $error_code, $action_type){ 
        
	$values = $this->input->post();  
	$datails = array();
	
	$this->_country_data($id_country);
	$this->error_resolution_construct();
        
	//$action = null;
	//if($action_type == 'Create')
	$action = 'recreate';       
		    
        if (isset($values['asin']) && isset($values['sku'])) {    
            
	    foreach ($values['asin'] as $key => $asin){
		if(isset($values['sku'][$key])){
		    
		    $models = $this->amazon_err->getModelBySKU($values['sku'][$key], $id_country, $this->id_shop);
		    $product = $this->amazon_model->getProductBySKU($this->id_shop, $values['sku'][$key]);

		    if(!$product['id_product']){
			$datails['error'][$values['sku'][$key]] = true;
			continue;
		    }

		    $data['ext'] = $this->ext;
		    $data['sku'] = $values['sku'][$key];
		    $data['id_product'] = $product['id_product'];
		    
		    if(isset($product['id_product_attribute']))
			$data['id_product_attribute'] = $product['id_product_attribute'];
		    
		    $data['id_shop'] = (int)$this->id_shop;
		    $data['id_country'] = (int)$id_country;
		    $data['id_model'] = isset($models[0]['id_model']) ? $models[0]['id_model'] : 0;
		    $data['attribute_field'] = 'ASIN';
		    $data['override_value'] = $asin;
		    $data['date_upd'] = date('Y-m-d H:i:s');		    
		    
		    // Save Attribute Overide
		    if(!$this->amazon_err->save_amazon_attribute_override($data, $error_code, $action_type, $action, true)){
			$datails['error'][$key] = true;
			continue;
		    } else {				   
			$datails['success'][$key] = true;
                        // update action to error to send ASIN
                        $this->amazon_model->updateMatchProductToSync($data['sku'], $id_country, $this->id_shop, 'error');
		    }
		}
	    }
	    
	    echo json_encode(array('pass' => true, 'detail' => $datails)); exit;		
            
        }        
       
	echo json_encode(array('pass' => false, 'detail' => array('empty_data' => true)));  exit;        
    }
    
    public function save_error_edit_data(){ 
        $this->error_resolution_construct();
        if ($this->input->post()) {    
            $datails = array();
            $values = $this->input->post();             
            if(isset($values['attribute_overide']) && !empty($values['attribute_overide'])){
                foreach($values['attribute_overide'] as $attributes){   
                    $data = array();                    
                    if(isset($attributes['attribute']) && !empty($attributes['attribute'])){
                        foreach ($attributes['attribute'] as $attribute){
                            if(isset($attribute['attribute_field']) && !empty($attribute['attribute_field']) 
                                    && isset($attribute['override_value']) && !empty($attribute['override_value'])){
                                $data['sku'] = $attributes['sku'];
                                $data['id_product'] = (int)$attributes['id_product'];
                                $data['id_product_attribute'] = isset($attributes['id_product_attribute']) ? (int)$attributes['id_product_attribute'] : 0;
                                $data['id_shop'] = (int)$values['id_shop'];
                                $data['id_country'] = (int)$values['id_country'];
                                $data['id_model'] = (int)$attributes['id_model'];
                                $data['attribute_field'] = $attribute['attribute_field'];
                                $data['override_value'] = $attribute['override_value'];
                                $data['date_upd'] = date('Y-m-d H:i:s');
                                
                                // Save Attribute Overide
                                if(!$this->amazon_err->save_amazon_attribute_override($data, $values['error_code'], $values['action_type'])){
                                    $datails['error'][$data['sku']] = true;
                                    continue;
                                } else {
                                    //$datails['success'][$data['sku']] = true;
                                    $datails['success'][$data['sku']][$data['attribute_field']] = $data['override_value'];
                                }
                            }
                        }
                    }
                }
            }            
            echo json_encode(array('pass' => true, 'detail' => $datails));
            exit;            
        }        
        echo json_encode(array('pass' => false, 'detail' => array('empty_data' => true))); exit;
    }
    
    public function error_data_upload_template($id_country, $error_code, $action_type){        
         
        $this->_country_data($id_country);
        $this->error_resolution_construct();
        
        $this->load->helper('form');
        $config['upload_path'] = USERDATA_PATH. $this->user_name .'/amazon/';
        $config['allowed_types'] = 'txt|csv';
        $config['allowed_override'] = true;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->upload->set_allowed_types('*');

        if(!$this->upload->do_upload('file-upload')){
            echo json_encode(array('error' => $this->upload->display_errors())); exit;
        }

        $file_data = $this->upload->data();
        $path = $file_data['full_path'];
        $datalist = $head = array();
        $delimiter = ',';

        if ($path) {
            $fp = fopen($path, 'r');
            $buf = fread($fp, 1000);
            fclose($fp);
        } else {
            return $datalist;
        }

        $count1 = count(explode(',', $buf));
        $count2 = count(explode(';', $buf));
        if ($count2 > $count1){
            $delimiter = ';';
        }

        $fp = fopen($path, 'r');
        $i = 0;

        while ($data = fgetcsv($fp, 0, $delimiter)) {               
            if (!is_array($data)){
                return false;
            }
            if($i == 0){
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $head[$khead] = $vhead;
                    }
                }
            } else {
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $datalist[$i][$head[$khead]] = trim($vhead);
                    }
                }
            }
            $i++;
        } 

        $datails = array();

        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.validValues.php';
        $valid_value = new ValidValue($this->ext);        
        foreach ($datalist as $attribute){
            if(isset($attribute['attribute_field']) && !empty($attribute['attribute_field']) /*&& isset($attribute['override_value']) && !empty($attribute['override_value'])*/){
                // attribute field
                $attribute_fields = ucwords(str_replace(array(" ","_"), "", $attribute['attribute_field']));
                if(isset($this->attribute_name_exeption[$attribute_fields])){
                    $attribute_fields = $this->attribute_name_exeption[$attribute_fields];
                }                    
                $models = $this->amazon_err->getModelBySKU($attribute['sku'], $id_country, $this->id_shop);
                $amazon_attribute = array();
                if(isset($models[0]['universe']) && !empty($models[0]['universe'])){                        
                    $amazon_attribute = $valid_value->getAttributesForProductType($models[0]['universe']);
                }
                if(isset($amazon_attribute[$attribute_fields])){   
                    $data['id_model'] = $models[0]['id_model'];
                    $data['id_product'] = $models[0]['id_product'];
                    $data['id_product_attribute'] = $models[0]['id_product_attribute'];
                    $data['sku'] = $attribute['sku'];
                    $data['id_shop'] = (int)$this->id_shop;
                    $data['id_country'] = (int)$id_country;
                    $data['attribute_field'] =  !empty($attribute_fields) ?  $attribute_fields : '';                    
                    $data['date_upd'] = date('Y-m-d H:i:s');
                    if(!isset($attribute['override_value']) || empty($attribute['override_value'])){
                        $this->amazon_err->remove_amazon_attribute_override($data['sku'], $data['id_shop'], $data['id_country'], $error_code, $action_type);
                        $datails['success'][$data['sku']][$data['attribute_field']] = $this->lang->line('removed');
                    } else {
                        $data['override_value'] = $attribute['override_value'];
                         // Save Attribute Overide
                        if(!$this->amazon_err->save_amazon_attribute_override($data, $error_code, $action_type)){
                            $datails['error'][$data['sku']] = true;
                            continue;
                        } else {
                            //$datails['success'][$data['sku']] = true;
                            $datails['success'][$data['sku']][$data['attribute_field']] = $data['override_value'];
                        }
                    }
                } else {
                    $datails['error'][$attribute['sku']] = true;
                    continue;
                }
            }
       }
       echo json_encode(array('pass' => true, 'detail' => $datails));
    }
    
    public function error_data_download_template($country, $error_code, $action_type){
        $this->error_resolution_construct();
        $this->_country_data($country);
        ob_start();
        ob_get_clean();
        $filename = 'amazon_attribute_override_template.csv';
        $list = array();
        $list['header']['sku'] = "sku";
        $list['header']['attribute_field'] = "attribute_field";
        $list['header']['override_value'] = "override_value";
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        $type = (isset($action_type) && $action_type == ucwords(AmazonDatabase::CREATE)) ? 'products' : 'offers';
        $amazon_err_resolutions = $this->amazon_model_user->get_error_resolutions($type, null, mb_substr($this->language, 0, 2));
        $logs = $this->amazon_err->get_amazon_error_log_details($this->api_settings, $error_code, $action_type, 1, null, null, ' asrl.sku ASC ', null, false, null, true);
        $key=0;
        foreach ($logs as $log){
            // Get Message
            $msg = $amazon_err_resolutions[$error_code];
            $msg_pattern = $msg_name = null;
            if(!empty($msg['message_pattern'])){
                $message_pattern = unserialize($msg['message_pattern']);
                $msg_pattern = $message_pattern['msg_pattern'];
                $msg_name = $message_pattern['msg_name'];

                $msp_val = $this->amazon_err->message_pattern($log['message'], $msg_pattern, $msg_name);
            } else {
                $msp_val = null;
            }
            if(!empty($msp_val)){
                foreach ($msp_val as $msp_v){
                    $list[$key]['sku'] = (string)$log['sku'];
                    $list[$key]['attribute_field'] = isset($msp_v['attribute_field']) ? $msp_v['attribute_field'] : '';
                    $list[$key]['override_value'] = isset($msp_v['Amazon_value']) ? $msp_v['Amazon_value'] : '';
                     $key++;
                }
            } else {
                $list[$key]['sku'] = (string)$log['sku'];
                $list[$key]['attribute_field'] = '';
                $list[$key]['override_value'] = '';
                $key++;
            }
        }
        foreach ($list as $data) {
            fputcsv($h, $data, ';');
        }
    }      
    /** Error Resoluation **/
    
    public function features($country = null){        
        $data = array();
        $this->_country_data($country);
        $this->_set_session_message();       
        $data['no_data'] = _get_message_code('23-00008', 'inner');
        $this->smarty->view('amazon/Features.tpl', $data);
    }
    
    public function save_configuration_features(){
        if ($this->input->post()) {
            $values = $this->input->post();            
            $id_region = $country = '';
            if (isset($this->session->userdata['menu'])){
                $marketplace = $this->session->userdata['menu'];
            }
            if (isset($marketplace[self::TAG]['submenu'][$values['id_country']])) {
                $id_region = $marketplace[self::TAG]['submenu'][$values['id_country']]['id_region'];
                $country = $marketplace[self::TAG]['submenu'][$values['id_country']]['country'];
            }
            $values['id_customer'] = $this->id_user;
            $values['user_name'] = $this->user_name;
            $values['countries'] = $country;
            $values['id_shop'] = $this->id_shop;
            $values['creation'] = isset($values['creation']) ? $values['creation'] : 0;
	    $values['create_out_of_stock'] = isset($values['create_out_of_stock']) ? $values['create_out_of_stock'] : 0;
	    $values['allow_send_order_cacelled'] = isset($values['allow_send_order_cacelled']) ? $values['allow_send_order_cacelled'] : 0;
            // exception beginner mode
            if(isset($this->session->userdata['mode_default']) && $this->session->userdata['mode_default'] == 1){
                $exclude_feature_list = array('delete_products', 'second_hand', 'code_examption', 'synchronization_field', 'sku_as_supplier_reference', 'sku_as_supplier_reference', 'repricing', 'shipping_override', 'fba', 'messaging', 'advertising', 'allow_send_order_cacelled', 'orders_cancelation');
                foreach ($values as $field=> $value){
                    if(in_array($field, $exclude_feature_list)){
                        unset($values[$field]);
                    }
                }
            }
            if (!$this->amazon_model_user->set_parameters($values)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/features/' . $values['id_country']);
            }
            if (!$this->amazon_model->save_configuration_features($values, $this->id_shop, $values['id_country'])) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/features/' . $values['id_country']);
            }
        }
        // set data to session data
        $this->session->unset_userdata('amazon_mode_creation');
        if (isset($values['save'])) {
            if ($values['save'] == "continue") {
                $this->session->set_userdata('message', 'save_successful');               
                redirect('amazon/parameters/' . $values['id_country']);
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/features/' . $values['id_country']);
            }
        } else {
            $this->session->set_userdata('message', 'save_successful');
            redirect('amazon/features/' . $values['id_country']);
        }
    }

    public function ajax_get_next_time(){
        $post = $this->input->post();
        if(isset($post['process_key']) && !empty($post['process_key'])){
            $this->load->model('cron_model', 'cron');
            $next_time = $this->cron->getTimeLastCronRun($this->id_user,$post['process_key']);
            if($next_time<=time())$next_time=time();
            echo $next_time-time();
            return;
        }
        echo '20';
    }
    
    public function scheduled_tasks($country = null,$get=''){
        if($get=='test'){
            define('ADMIN_TEST_ONLY','palm@common-services.com');
        }
        $data = array();
        $marketplace = $this->session->userdata['menu']; 
        $this->_country_data($country);
        $data['no_data'] = _get_message_code('23-00006', 'inner');
        $this->load->model('cron_model', 'cron');
        $ext = $marketplace[self::TAG]['submenu'][$country]['ext']; 
        $site_id = $marketplace[self::TAG]['submenu'][$country]['id']; 
        $market= strtolower(self::TAG);
        $list = $this->cron->getUserCronList($this->id_user,$market,$ext);
        $fba_repricing_filter = $this->cron->get_amazon_log_display_filter();
        $data['ext'] =$ext;
        $data['market'] = $market;                   
        $data['tasks'] = array();
        $countdown= array();
        foreach($list as $l){ 
            if($l['cron_type']=='fba' || $l['cron_type']=='repricing'){
                if(empty($fba_repricing_filter[$site_id][$l['cron_type']])){
                    continue;
                }
            }
            $master_fba_site='';
            if($l['cron_type']=='fba' && empty($master_fba_site)  ){
                require_once dirname(__FILE__) . '/../libraries/UserInfo/configuration.php';
                $obj = new UserConfiguration();
                $master_fba_site = $obj->get_amazon_fba_update_site($this->id_user,array($this->id_shop));
                $master_fba_site = (isset($master_fba_site[0]['ext']) && !empty($master_fba_site[0]['ext']))?$master_fba_site[0]['ext']:'';
            }
            $task = array('task_name'=> $l['cron_type'],
                'task_display_name'=>  ucfirst($l['cron_marketplace'].$ext).' - '.$this->lang->line($l['cron_action'].' '.$l['cron_type']),
                'task_lastupdate'=>'',
                'task_id'=>$l['id_cron'],
                'task_status'=>'',
                'task_running'=>$l['status']||$l['status'] == '' ? true : false,
                'task_file'=>$l['cron_file'],
                'task_detail'=>$l['cron_description'],
                );  
            $next_time = time();
            if($l['cron_key']==''){
                $task['task_key'] = $l['cron_marketplace'].$ext.'_'.$l['cron_action'].'_'.$l['cron_type']; 
            }else{
                if(!empty($master_fba_site) && in_array($l['cron_action'],array('fba_manager','fba_order_status')) ){
                    $task['task_key'] = str_replace('{ext}',$master_fba_site,$l['cron_key']);
                }else{
                    $task['task_key'] = str_replace('{ext}',$ext,$l['cron_key']);
                }
                $key = str_replace('{site_id}',$site_id,$task['task_key']); 
                $task['task_key'] = $key;
                $next_time = $this->cron->getTimeLastCronRun($this->id_user,$key);
                if($next_time<=time())$next_time=time();
            }
            $data['tasks'][] = $task;
            $countdown[$task['task_key']] = $next_time-time();
            $data['countdown'] = $countdown;
        }
        $data['allow_cron'] = $this->amazon_model_user->check_allow_cron($this->id_user);
        $this->smarty->assign("not_continue", true);
        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }
        if ($this->session->userdata('remote') && $this->session->userdata('remote')===true) {
            $data['display_file']=true;
        }
        $this->smarty->view('amazon/ScheduledTasks.tpl', $data);
    }
    
    public function ajax_update_cron_status(){
        if ($this->input->post()) {
           $user_id = $this->id_user;
           $this->load->model('cron_model', 'cron');
           $cron_id = $this->input->post('cid');
           $status =  $this->input->post('status')=='true'?1:0;
           $ext = $this->input->post('ext');
           $this->cron->updateUserCron($user_id,$cron_id,$ext,$status);
        }
    }
   
    public function ajax_get_serv_status(){
        if ($this->input->post()) {            
            $type = $this->input->post('task');
            $id_country = $this->input->post('id_country');            
            $API = $version = '';
            $output = array();            
            $this->_country_data($id_country);            
            if(isset($this->api_settings) && !empty($this->api_settings)){
                if( $type == 'product' || $type == 'offer' ){
                    $API = 'Products';               
                    $version= '2011-10-01';
                } else if(strpos($type, 'repricing') !== false){
                    $API =  'Subscriptions';
                    $version= '2013-07-01';
                } else if($type = 'order'){
                    $API = 'Orders';
                    $version= '2013-09-01';
                }  
                $amazonApi = $this->connect_to_amazon_webservice($id_country,$this->api_settings['merchant_id'], $this->api_settings['aws_id'], $this->api_settings['secret_key']);
                if($amazonApi){
                    $result = $amazonApi->ServiceStatus($API, true, $version);
                    if(isset($result->GetServiceStatusResult) && isset($result->GetServiceStatusResult->Status) ){
                        $output['status'] = strtolower($result->GetServiceStatusResult->Status->__toString());
                    }
                    if(isset($result->GetServiceStatusResult) && isset($result->GetServiceStatusResult->Timestamp) ){
                        $output['timestamp'] = strtotime($result->GetServiceStatusResult->Timestamp->__toString());
                        $output['timestamp'] = date('r',$output['timestamp']);
                    }
                }                 
            }
        }
         echo json_encode($output);
    }
    
    public function save_status(){
        $values = $this->input->post();
        $values['id_customer'] = $this->id_user;
        $values['id_mode'] = $this->id_mode;
        $values['id_shop'] = $this->id_shop;
        $values['cron_create_products'] = isset($values['cron_create_products']) ? $values['cron_create_products'] : 0;
        $values['cron_delete_products'] = isset($values['cron_delete_products']) ? $values['cron_delete_products'] : 0;
        $values['cron_update_offers'] = isset($values['cron_update_offers']) ? $values['cron_update_offers'] : 0;
        $values['cron_send_orders'] = isset($values['cron_send_orders']) ? $values['cron_send_orders'] : 0;
        $values['cron_update_order_status'] = isset($values['cron_update_order_status']) ? $values['cron_update_order_status'] : 0;
        if (!$this->amazon_model_user->set_parameters($values)) {
            $this->session->set_userdata('error', 'save_unsuccessful');
        }
        $this->session->set_userdata('message', 'save_successful');
        redirect('amazon/scheduled_tasks/' . $values['id_country']);
    }

    public function creation($country = null) {
        $data = array();
        $this->_country_data($country);
        $user_data = $this->api_settings;
        if (isset($user_data['active']) && !$user_data['active'])
            $this->smarty->assign("debug", true);
        $data['action'] = AmazonDatabase::CREATE;
        $this->smarty->assign("id_country", $country);
        $this->smarty->assign("popup", 4);
        $this->smarty->view('amazon/Creation.tpl', $data);
    }

    public function matching($country = null, $mode = 1) {
        $data = array();
        $this->_country_data($country);
        $user_data = $this->api_settings;
        if (isset($user_data['active']) && !$user_data['active'])
            $this->smarty->assign("debug", true);
        $data['mode_match'] = AmazonDatabase::MODE_MATCH;
        $data['mode_sync'] = AmazonDatabase::MODE_SYNC;
        $data['mode_sync_match'] = AmazonDatabase::MODE_SYNC_MATCH;
        $data['action'] = AmazonDatabase::SYNC;
        $this->smarty->assign("id_country", $country);
        $this->smarty->assign("popup", 4);
        $this->smarty->assign("mode", $mode);
        $this->smarty->view('amazon/Matching.tpl', $data);
    }

    public function matching_products($country, $mode = null, $page = 0, $debug = false) {
        if (!isset($country) || empty($country)) {
            echo json_encode(array('error' => 'Missing country.'));  die();
        }
        $this->_country_data($country);
        $user_data = $this->api_settings;
        if (!isset($user_data) || empty($user_data)) {
            echo json_encode(array('error' => 'Missing user data.'));  die();
        }
        require_once getcwd() . '/assets/apps/amazon_matching_product.php';
        $matching = new Matching($user_data, $debug);
        $products = $matching->products($mode, $page);
        $json = json_encode(array('products' => $products));
        echo $json; exit;
    }

    public function confirm_products($country) {
        if ($this->input->post()) {
            $product_list = $this->input->post();
            if (!isset($country) || empty($country)) {
                echo json_encode(array('error' => 'Missing country.')); die();
            }
            $this->_country_data($country);
            $user_data = $this->api_settings;
            if (!isset($user_data) || empty($user_data)) {
                echo json_encode(array('error' => 'Missing user data.')); die();
            }
            require_once getcwd() . '/assets/apps/amazon_matching_product.php';
            $matching = new Matching($user_data);
            $products = $matching->confirm_products($product_list['matched']);
            $json = json_encode(array('products' => $products));
            echo $json; exit;
        }
    }

    /* mode: synchronize or create */
    public function platform($mode = 1) {
        if (isset($this->session->userdata['menu'])){
            $marketplace = $this->session->userdata['menu'];
        }
        $data['submenu'] = $marketplace[self::TAG]['submenu'];
        $data['mode'] = $mode;
        $this->smarty->assign("popup", 1);
        $this->smarty->view('amazon/Platform.tpl', $data);
    }

    /* Amazon valid value */
    public function get_valid_value($name, $element, $ext, $product_type, $attribute_field, $product_sub_type = null, $select_value = null) {
        
        require_once dirname(__FILE__) . '/../libraries/Amazon/classes/amazon.validValues.php';
        $valid_value = new ValidValue($ext);
        $value = $valid_value->get_valid_value($product_type, $attribute_field, $product_sub_type);
        $select_id = 'model['.$name.']['.$element.']['.$attribute_field.'][CustomValue]';
        $select_rel = $attribute_field;
        $options = array();
        
        if(isset($value) && !empty($value)) {
            $cssClass = ' class="custom_value search-select"'; 
            $this->smarty->assign('select_id', $select_id);
            $this->smarty->assign('select_cssClass', $cssClass);
            $this->smarty->assign('select_rel',$select_rel);
            $this->smarty->assign('isCustomValue',true);
            foreach($value as $c){ 
                $option = array();
                $option["value"] = $c['value'];
                $option["desc"] = $c['desc'];
                $options[] = $option;
            }
            $options[] = array("value" => 'CustomValue', "desc" => 'Default Value');
            $this->smarty->assign('select_options', $options);
            $html = $this->smarty->fetch('amazon/inputSelectHTML.tpl');
        }  else  {
            $cssClass = ' class="custom_value form-control"';
            $this->smarty->assign('custom_free_text', true);
            $this->smarty->assign('text_id', $select_id);
            $this->smarty->assign('text_cssClass', $cssClass);
            $this->smarty->assign('text_rel',$select_rel);
            $this->smarty->assign('text_value',$select_value);
            $this->smarty->assign('text_additional_attributes','');
            $this->smarty->assign('text_disabled','');
            $html = $this->smarty->fetch('amazon/inputTextHTML.tpl');
        }
        echo json_encode($html);
    }  

    /* Displays correspongind view for the current action */
    public function models($country = null, $steps = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $this->_country_data($country);
        $this->_set_session_message();
        
        global $amzn_attr_values;
        $data = $amzn_attr_values = $m_name = array();

        $params = $this->_setParams();
        //$DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params);
        
        $model_list = array();
        $models = $this->amazon_model->get_amazon_model($country, $this->id_mode, $this->id_shop);
        	
        if(empty($models) && !isset($steps) && !empty($this->api_settings)){
            $this->smarty->assign("no_data", _get_message_code('23-00002', 'inner'));
        }
        $i=0;
        foreach ($models as $models_key => $models_value) {
            $key = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace(" ", "_", $models_value['name']));
            if (isset($m_name[$models_value['name']]) && $m_name[$models_value['name']] == true)
                $key = $key . '_' . $i++;

            $model_list[$models_key]['name'] = $models_value['name'];
            $model_list[$models_key]['key'] = $key;

            if(isset($models_value['universe']) && !empty($models_value['universe'])){
                $model_list[$models_key]['selected_universe'] = AmazonForm::getName($models_value['universe'], 'universe') ;
            }
            if(isset($models_value['product_type']) && !empty($models_value['product_type'])){
                $model_list[$models_key]['selected_product_type'] = AmazonForm::getName($models_value['product_type'], 'product_types') ;
            }
            if(isset($models_value['sub_product_type']) && !empty($models_value['sub_product_type'])){
                $model_list[$models_key]['selected_product_subtype'] = AmazonForm::getName($models_value['sub_product_type']) ;
            }
            $model_list[$models_key]['id_model'] = $models_value['id_model'];
            $m_name[$models_value['name']] = true;
        }        
        if (isset($model_list) && !empty($model_list)){
            $this->smarty->assign("models", $model_list);            
        }       
        
        if(isset($model_list) && !empty($model_list)){
            $data['model'] = $model_list;
            $this->smarty->assign("size_data", sizeof($model_list));
        }
        $this->smarty->assign("add_data", $this->lang->line('Add a model'));
        $this->smarty->assign('product_type_options', AmazonForm::getProductsUniverse(false));

        //Step
        if (isset($steps) && !empty($steps) && strlen($steps)) {
            $this->smarty->assign("popup_more", $steps);
            $this->smarty->assign("mode", 2);
            $this->smarty->assign("popup", 2);
            $this->smarty->assign("no_data", _get_message_code('22-00009', 'inner'));
        }
        // Warning unable save amazon_status to update offers
        if ($this->session->userdata('status_warning') && $this->session->userdata('status_warning') == 'status_fail') {
            _get_message_code('22-00012');
        }      
        $this->smarty->view('amazon/Models.tpl', $data);
    }

    public function get_model($country, $id_model){
        $models = $this->amazon_model->get_amazon_model($country, null, $this->id_shop, $id_model);
        $this->_country_data($country);
        $params = $this->_setParams();
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params);
        $form = $DisplayForm->displayModelsForm($models, $country, $this->id_shop);
        $html = '';
        foreach ($form['data']['model'] as $k => $models){
            $this->smarty->assign("k", $k);
            $this->smarty->assign("data", $models);
            $html .= $this->smarty->fetch('amazon/Models_tmp.tpl');
        }
        echo $html; exit;
    }

    public function get_duplicate_model($id_model, $country) {
        
	$this->_country_data($country);
        $params = $this->_setParams(); 
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params);
        $form = $DisplayForm->duplicateForm($id_model);
        echo json_encode($form);
    }

    public function actions($type = null, $country = null) {        
        
        if (!isset($country) || empty($country) || $country == null || $type == null)
            redirect('/marketplace/configuration');

        $data = array();
        $this->_country_data($country);

        //find carrier 
        $params = $this->_setParams();        
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params); 
        $carrier = $DisplayForm->get_mapping_carrier($country);
        
        //22-00005 Mapping Carrier
        if (!isset($carrier['mapping_carrier_flag']) || empty($carrier['mapping_carrier_flag']))
            $data['message_code_inner']['carrier'] = _get_message_code('22-00005', 'inner'); //$this->smarty->assign("carrier", 'true');

        $data['api_settings'] = $this->api_settings;
        if (!isset($data['api_settings']['active']) || !$data['api_settings']['active'])
            $this->smarty->assign("debug", true);

        //Creation
        if (isset($data['api_settings']['creation']) && !empty($data['api_settings']['creation']))
            $this->smarty->assign("creation", $data['api_settings']['creation']);
        
        //22-00002 Quantity and Price
        if ( (!isset($data['api_settings']['synchronize_quantity']) || empty($data['api_settings']['synchronize_quantity']) ) 
            && ( !isset($data['api_settings']['synchronize_price']) || empty($data['api_settings']['synchronize_price'])) ){
            $data['message_code_inner']['sync'] = _get_message_code('22-00002', 'inner');
        //22-00003 Quantity
        }else if( (!isset($data['api_settings']['synchronize_quantity']) || empty($data['api_settings']['synchronize_quantity']) ) 
            && ( isset($data['api_settings']['synchronize_price']) && !empty($data['api_settings']['synchronize_price']))){
            $data['message_code_inner']['sync'] = _get_message_code('22-00003', 'inner');
        //22-00004 Price
        } else if ( (isset($data['api_settings']['synchronize_quantity']) && !empty($data['api_settings']['synchronize_quantity']) ) 
            && ( !isset($data['api_settings']['synchronize_price']) || empty($data['api_settings']['synchronize_price']))){
            $data['message_code_inner']['sync'] = _get_message_code('22-00004', 'inner');
        }
        
        $data['yesterday'] = date('d/m/Y',strtotime('-1 day'));
        $data['today'] = date('d/m/Y');
        $this->smarty->assign("sync", AmazonDatabase::SYNC);
        $this->smarty->assign("sync_mode", self::SYNC_MODE);
        $this->smarty->assign("create_mode", self::CREATE_MODE);
        $this->smarty->assign("out_of_stock", AmazonDatabase::DELETE_OUT_OF_STOCK);
        $this->smarty->assign("all_sent", AmazonDatabase::DELETE_ALL_SENT);
        $this->smarty->assign("disabled", AmazonDatabase::DELETE_DISABLED);
        $this->smarty->assign("id_country", $country);
        $this->smarty->assign("ext", $this->ext);
        $this->smarty->assign("id_user", $this->id_user);
        $this->smarty->assign("id_shop", $this->id_shop);
        
        switch ($type){
            case 'offers':  $this->smarty->view('amazon/ActionsOffers.tpl', $data); break;
            case 'products':  $this->smarty->view('amazon/ActionsProducts.tpl', $data); break;
            case 'orders':  $this->smarty->view('amazon/ActionsOrders.tpl', $data); break;
            case 'relations':  $this->smarty->view('amazon/ActionsRelations.tpl', $data); break;
            case 'images':  $this->smarty->view('amazon/ActionsImages.tpl', $data); break;
            case 'delete':  $this->smarty->view('amazon/ActionsDelete.tpl', $data); break;
            case 'repricing':  
                $this->smarty->assign("repricing_analysis", AmazonDatabase::REPRICING_ANALYSIS);
                $this->smarty->assign("repricing_export", AmazonDatabase::REPRICING_EXPORT);                
		$this->smarty->assign("repricing_trigger", 'repricing_trigger');

                $this->smarty->view('amazon/ActionsRepricing.tpl', $data); break;
            case 'fba':  
		require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.fba.stock.php';
		//FBA Manager
		
		$fba = $this->amazon_model_user->get_fba($this->id_user, $this->ext, $this->id_shop);
		
		switch ($fba['fba_stock_behaviour']){		    
		    case AmazonFBAStock::FBA_STOCK_SWITCH : 
			$this->smarty->assign("behaviour", AmazonFBAStock::FBA_STOCK_SWITCH); break;
		    case AmazonFBAStock::FBA_STOCK_SYNCH :
			$this->smarty->assign("behaviour", AmazonFBAStock::FBA_STOCK_SYNCH); break;
		}
		$this->smarty->assign('fba_master_platform', (int) $fba['fba_master_platform']);
		$this->smarty->assign("FBA_STOCK_SWITCH", AmazonFBAStock::FBA_STOCK_SWITCH);
		$this->smarty->assign("FBA_STOCK_SYNCH", AmazonFBAStock::FBA_STOCK_SYNCH);
                $this->smarty->view('amazon/ActionsFBA.tpl', $data); 
		break;
	    case 'shipping' :
		$data['shipping_group_names'] = $this->get_shipping_group_name($country);		
		$this->smarty->view('amazon/ActionsShipping.tpl', $data); 
		break;
        }
    }

    public function parameters($country = null, $popup = null, $mode = 1) {        
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $data = array();
        $this->_country_data($country);
        $this->_set_session_message();
        $api = $this->_get_api();      
        if(isset($api)){
            $this->smarty->assign("developer_account_number", $api[$this->id_region]['develop_account_number']);
        }
        $data['api_settings'] = $this->api_settings;
        if(empty($data['api_settings']) && !isset($popup)){
            _get_message_code('23-00001');
        }

        $this->smarty->assign("get_access_url_ext", $this->session->userdata['menu'][self::TAG]['submenu'][$country]['domain']);

        if (isset($popup) && !empty($popup)) {
            $this->smarty->assign("mode", $mode);
            $this->smarty->assign("popup", 2);
            if ($mode == Amazon::SYNC_MODE){
                $this->smarty->assign("action", AmazonDatabase::SYNC);
            }elseif ($mode == Amazon::CREATE_MODE){
                $this->smarty->assign("action", AmazonDatabase::CREATE);
            }
        }
        $this->smarty->view('amazon/Parameters.tpl', $data);
    }

    public function update_parameters_status($id_country, $status) {
        $values = array();
        $values['id_customer'] = $this->id_user;
        $values['id_country'] = $id_country;
        $values['id_shop'] = $this->id_shop;
        $values['active'] = $status;

        if (!$this->amazon_model_user->set_parameters($values)) {
            echo json_encode(false);
        }
        echo json_encode(true);
    }

    public function save_parameters() {
        
        if ($this->input->post()) {
            $values = $this->input->post();
            $this->_country_data($values['id_country']);            
            $api = $this->_get_api();
            if(isset($api)){
                $awsKeyId = $api[$this->id_region]['aws_id'];
                $awsSecretKey = $api[$this->id_region]['secret_key'];
            }            
            
            $lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
            
            if (isset($lang['iso_code'])) {
                $isocode = $lang['iso_code'];
            } else {
                $lang_default = $this->feedbiz->getLanguageDefault($this->id_shop);
                if (isset($lang_default) && !empty($lang_default))
                    foreach ($lang_default as $ld)
                        $isocode = $ld['iso_code'];
                else {
                    if (isset($this->iso_code)) {
                        $isocode = $this->iso_code;
                    } else {
                        $isocode = 'en';
                    }
                }
            }
            
            $values['aws_id'] = $awsKeyId;
            $values['secret_key'] = $awsSecretKey;
            $values['id_customer'] = $this->id_user;
            $values['synchronize_quantity'] = isset($values['synchronize_quantity']) ? $values['synchronize_quantity'] : 0;
            $values['synchronize_price'] = isset($values['synchronize_price']) ? $values['synchronize_price'] : 0;
            $values['user_name'] = $this->user_name;
            $values['ext'] = $this->ext;
            $values['countries'] = $this->country;
            $values['currency'] = $this->currency;
            $values['id_mode'] = $this->id_mode;
            $values['id_shop'] = $this->id_shop;
            $values['id_lang'] = isset($lang['id_lang']) ? $lang['id_lang'] : '';
            $values['iso_code'] = $isocode;
            $values['id_region'] = $this->id_region;
            $values['active'] = isset($values['active']) ? $values['active'] : 0;

            if (!$this->amazon_model_user->set_parameters($values)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                echo json_encode(array('pass' => false, 'next_page' => 'parameters'));
            }

            $api_settings = $this->amazon_model_user->get_parameters($this->id_user, $this->ext, $this->id_shop);

            if (isset($values['save'])) {
                if ($values['save'] == "next") {                  
                    if ($values['mode'] == 1) {
                        echo json_encode(array('pass' => true, 'next_page' => 'category', 'step' => 2, 'mode' => $values['mode']));                        
                    } else {                       
                        if (!$this->_check_step_more($values['id_country'], 'models', null, true)){
                            echo json_encode(array('pass' => true, 'next_page' => 'category', 'step' => 2, 'mode' => $values['mode']));                            
                        }
                    }
                } else if ($values['save'] == "continue") {
                    $this->session->set_userdata('message', 'save_successful');
                    if($api_settings['creation']){
                        echo json_encode(array('pass' => true, 'next_page' => 'models'));
                    }else {
                        echo json_encode(array('pass' => true, 'next_page' => 'profiles'));
                    }
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                    echo json_encode(array('pass' => true, 'next_page' => 'parameters'));
                }
            } else {
                $this->session->set_userdata('message', 'save_successful');
                echo json_encode(array('pass' => true, 'next_page' => 'parameters'));
            }
        }
    }

    //Send.tpl
    public function get_submission_list($country, $submission_id) {
        $logs = $this->amazon_model->get_amazon_submission_result_log($country, $this->id_shop, $submission_id);

        if (isset($logs['error']))
            echo json_encode($logs['error']);
    }

    //Report.tpl
    public function get_submission_list_log($country, $json_view = false) {
        
        $data = array();
        $logs = $this->amazon_model->get_submission_list_log($country, $this->id_shop);
        $data['logs']  = $logs;
        $data['last_batch_id'] = isset($logs[0]['batch_id']) ? $logs[0]['batch_id'] : '';

        if ($json_view) echo json_encode($data['logs']);
        else return $data;
    }

    public function report($country = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $data = $last_report = array();
        $this->_country_data($country);
        $this->smarty->assign("id_country", $country);        
        $this->smarty->view('amazon/Report.tpl', $data);
    }  
    
    public function report_error_log($country) {        
        
        $submission_id = null;
        $this->_country_data($country);
        $data = $search = array();
        $data['data'] = array();  
        $values = $this->input->post();
        
        if(isset($values) && !empty($values)) {
            
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            
            if(isset($values['search']['value']) && !empty($values['search']['value'])) {
                
                $submission_id = $values['search']['value'];
                $sort = array();
                
                foreach($values['order'] as $order_sort){
                    switch ($order_sort['column']){
                        case 0 : $column = "sku"; break;
                        case 1 : $column = "result_message_code"; break;
                    }
                    $sort[] = $column . ' ' . $order_sort['dir'];
                }
                $order_by = implode(', ', $sort);
                $data['draw'] = $page;
                $data['recordsTotal'] = $this->amazon_model->get_amazon_submission_result_log($country, $this->id_shop, $submission_id, null, null, null, $order_by, $search, true);
                $data['recordsFiltered'] = $data['recordsTotal'];

                $error_logs = $this->amazon_model->get_amazon_submission_result_log($country, $this->id_shop, $submission_id, null, $start, $limit, $order_by, $search);   
                if(isset($error_logs['error']) && !empty($error_logs['error'])) {
                    foreach ($error_logs['error'] as $log){
                        array_push($data['data'], $log);
                    }
                }
            }
        } 
        
        echo json_encode($data); 
    }
    
    public function report_page($country = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $values = $this->input->post();
        $search = array();
        
        if(isset($values) && !empty($values)) {
            
            $this->_country_data($country);
           
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            
            if(isset($values['columns'])) {
                foreach ($values['columns'] as $columns) {
                    if(!empty($columns['search']['value'])) {
                        $search[$columns['data']] = $columns['search']['value'];
                    }
                }
            }
            
            if(isset($values['search']['value']) && !empty($values['search']['value'])) {
                $search['all'] = $values['search']['value'];
            }

            $sort = array();
            foreach($values['order'] as $order_sort){
                switch ($order_sort['column']){
                    case 0 : $column = "batch_id"; break;
                    case 1 : $column = "action_type"; break;
                    case 2 : $column = "feed_type"; break;
                    case 7 : $column = "date_add"; break;
                }
                $sort[] = $column . ' ' . $order_sort['dir'];
            }
            
            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_model->get_submission_list_log($country, $this->id_shop, null, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $logs = $this->amazon_model->get_submission_list_log($country, $this->id_shop, null, $start, $limit, $order_by, $search);

            foreach ($logs as $log){
                array_push($data['data'], $log);
            }

            echo json_encode($data); 
        }
    }

    public function send($country = null, $mode = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $data = $last_report = array();
        
        $this->_country_data($country);
        $this->_set_session_message();
        $this->smarty->assign("id_country", $country);

        if (isset($mode)) {
            $this->smarty->assign("popup", 5);
            $this->smarty->assign("mode", $mode);
            if ($mode == Amazon::SYNC_MODE)
                $this->smarty->assign("action", AmazonDatabase::SYNC);
            elseif ($mode == Amazon::CREATE_MODE)
                $this->smarty->assign("action", AmazonDatabase::CREATE);
        }
        $this->smarty->view('amazon/Send.tpl', $data);
    }

    public function logs($country = null, $popup = null, $action_process = null, $action_type = null, $batch_id = null, $products = null) {
  
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $data = array();
        $this->_country_data($country);
	
	// filter data
	$data['filter'] = $this->amazon_model->get_log_type($country, $this->id_shop);	
		
        if (isset($action_process) && !empty($action_process)) {
            $this->load->model("my_feed_model", "my_feed_model");
            $this->my_feed_model->update_history($this->id_user, strtolower(self::TAG) . $this->ext, $action_process);
        }
        
        $last_normal_update = $this->amazon_model->get_log($country, $this->id_shop, true, false);
        $data['last_normal_update'] = !empty($last_normal_update['0'])? $last_normal_update['0'] : array('date_upd' => 'Never') ;
        
        $last_cron_update = $this->amazon_model->get_log($country, $this->id_shop, true, true);
        $data['last_cron_update'] = !empty($last_cron_update['0'])? $last_cron_update['0'] : array('date_upd' => 'Never') ;
        
        $log = $this->get_log($country, 0, true);
        
        $data['history'] = $log;
        
        if(isset($log[0]['batch_id']) && !empty($log[0]['batch_id']) && !isset($popup)){ //If its not popup
            $batch_id = $log[0]['batch_id'];
            $this->smarty->assign("batch_id", $batch_id);
        }

        if (isset($popup) && !empty($popup) && $popup != false) {
	    
	    //$id_country,$id_shop,$action_process,$action_type,$batch_id=null,$start=null,$limit=null,$num_rows=false,$search=array(),$exclude_display_id_product=false
            $logs = $this->amazon_model->get_validation_log($country, $this->id_shop, $action_process, $action_type, $batch_id, null, null, false, array(), false);
            
            foreach ($logs as $key => $log) {
                $data['logs'][$key]['batch_id'] = $log['batch_id'];
                $data['logs'][$key]['action_process'] = ($log['action_process'] == AmazonDatabase::CREATE) ? 
                        AmazonDatabase::CREATION : ( ($log['action_process'] == AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE : $log['action_process']);
                $data['logs'][$key]['action_type'] = $log['action_type'];
                $data['logs'][$key]['datetime'] = $log['date_add'];
                $data['logs'][$key]['messages'] = str_replace(array("<", ">", "'", '"'), '', $log['message']);
            }
            
            if (isset($data['logs'])){
                echo json_encode($data['logs']);
            }
        } else {
            
            $this->_set_session_message();           
            $this->smarty->view('amazon/Logs.tpl', $data);
        }
    }
    
    public function get_log($country, $page = null, $return_array = false){
        
        $option = array();
        $value = $this->input->post();
        if(!empty($value)){
            if(!empty($value['date_from']))
                $option['date_from'] = date('Y-m-d', strtotime($value['date_from']));
            if(!empty($value['date_to']))
                $option['date_to'] = date('Y-m-d', strtotime($value['date_to']));
            if(!empty($value['action_type']))
                $option['action_type'] = trim($value['action_type']);
        }
        $log = $this->amazon_model->get_log($country, $this->id_shop, false, null, null, $page, $option);
        if(!$return_array)
            echo json_encode($log);

        return $log;
    }

    public function validation_log($country, $batch_id=null) {
  
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $values = $this->input->post();
        
//	$values = array(
//	    'draw' =>7,
//	    'order' =>array(
//	       '0' => array(
//		   'column' => 4,
//		   'dir' => 'desc'
//		   )
//		),
//	    'start' => 0,
//	    'length' => 10,
//	    'search' => array('value' =>'56b42082f1bf3')
//	);
	
        if(isset($values) && !empty($values)) {
            
            $this->_country_data($country);
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            
            if(isset($values['search']['value']) && !empty($values['search']['value'])) {
                $batch_id = $values['search']['value'];
            }
            $ignore_case = false;
            if(isset($values['ignore_case']) && $values['ignore_case']=='true'){
                $ignore_case = true;    
            }
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_model->get_validation_log($country, $this->id_shop, null, null, $batch_id, null, null, true,array(),false,$ignore_case);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            
            $logs = $this->amazon_model->get_validation_log($country, $this->id_shop, null, null, $batch_id, $start, $limit, false,array(),false,$ignore_case);
            foreach ($logs as $key => $log) {
                $data['data'][$key]['batch_id'] = $log['batch_id'];
                $data['data'][$key]['action_process'] = $log['action_process'];
                $data['data'][$key]['action_type'] = $log['action_type'];
                $data['data'][$key]['datetime'] = $log['date_add'];
                $data['data'][$key]['message'] = $log['message'];                
            }

            echo json_encode($data); 
        }
    }
    
    public function carts($country = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
        
        $data = array();
        $this->_country_data($country);
	
        $this->smarty->view('amazon/Carts.tpl', $data);
    }

    public function carts_page($country = null) {
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
        
        $values = $this->input->post();

        if(isset($values) && !empty($values)) {
            
            $this->_country_data($country);
            $this->amazon_order = new Amazon_Order($this->user_name, true);
             
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];
                        
            $sort = array();
	    $column = null;
            foreach($values['order'] as $order_sort){
                switch ($order_sort['column']){
                    case 0 : $column = "mp_order_id"; break;
                    case 1 : $column = "reference"; break;
                    case 2 : $column = "quantity"; break;
                    case 3 : $column = "timestamp"; break;                        
                    case 4 : $column = "date_upd"; break;                        
                }
                $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
            }
            $order_by = implode(', ', $sort);
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_order->getRemoteCartLists($this->id_shop, $country, null, null, $order_by, $search, true);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $orders = $this->amazon_order->getRemoteCartLists($this->id_shop, $country, $start, $limit, $order_by, $search, false);
	    
            foreach ($orders as $order){
                array_push($data['data'], $order);
            }

            echo json_encode($data); 
        }
    }   
    
    public function orders($country = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
        
        $data = array();
        $this->_country_data($country);
        
        $data['no_data'] = _get_message_code('23-00007', 'inner');         
        $this->smarty->assign("id_country", $country);
        $this->smarty->assign("ext", $this->ext);
        $this->smarty->assign("id_shop", $this->id_shop);
        $this->smarty->assign("id_user", $this->id_user);
        $this->smarty->assign("tag", self::TAG);
        $this->smarty->assign("id_marketplace", $this->id_marketplace);
        $this->smarty->assign("cron_send_orders", (isset($this->api_settings['cron_send_orders']) && $this->api_settings['cron_send_orders'] == 1) ? 1 : 0);
        
        $data['yesterday'] = date('d/m/Y',strtotime('-1 day'));
        $data['today'] = date('d/m/Y');   
        
        $this->smarty->view('amazon/Orders.tpl', $data);
    }

    public function orders_page($country = null) {

        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
        
        $values = $this->input->post();
//        $values = array(
//            'draw' => 1,
//            'length' => 10,
//            'start' => 0,
//            'search' => array('value'=>''),
//            'order' => array(0=>array('column'=>6,'dir'=>'desc')),
//        );

        if(isset($values) && !empty($values)) {
            
            $this->_country_data($country);
            $this->amazon_order = new Amazon_Order($this->user_name, true);
             
            $page = $values['draw'];
            $limit = $values['length'];
            $start = $values['start'];
            $search = $values['search']['value'];
            
            //Status
            $status = null;
            if(isset($values['columns'][11]['search']['value'])) {
                $search_status = $values['columns'][11]['search']['value'];
                if($search_status != 'all'){
                    $status = $search_status;
                }
            }
            //order
            $sort = array();
	    $column = null;
            foreach($values['order'] as $order_sort){
                switch ($order_sort['column']){
                    case 0 : $column = "o.id_orders"; break;
                    case 1 : $column = "o.id_marketplace_order_ref"; break;
                    case 2 : $column = "ob.address_name"; break;
                    case 3 : $column = "o.total_paid"; break;
                    case 4 : $column = "o.order_status"; break;
                    case 5 : $column = "mp.mp_channel"; break;
                    case 6 : $column = "o.order_date"; break;
                    case 7 : $column = "o.shipping_date"; break;
                    case 8 : $column = "os.tracking_number"; break;
                    case 9 : $column = "oi.seller_order_id"; break;
                    case 10 : $column = "oi.invoice_no"; break;
                    case 11 : $column = "o.status"; break;
                }
                $sort[] = isset($column) ? $column . ' ' . $order_sort['dir'] : '';
            }
            $order_by = implode(', ', $sort);

            //filter
            $key=0;
            foreach($values['columns'] as $columns => $order_columns){
                if(isset($order_columns['search']['value']) && !empty($order_columns['search']['value'])){
                    switch ($columns){
                        case 0 : $filter_table = 'orders'; $filter_column = "o.id_orders"; break;
                        case 1 : $filter_table = 'orders'; $filter_column = "o.id_marketplace_order_ref"; break;
                        case 2 : $filter_table = 'order_buyer'; $filter_column = "ob.address_name"; break;
                        case 3 : $filter_table = 'orders'; $filter_column = "o.total_paid"; break;
                        case 4 : $filter_table = 'orders'; $filter_column = "o.order_status"; break;
                        case 5 : $filter_table = 'mp_multichannel'; $filter_column = "mp.mp_channel"; break;
                        case 6 : $filter_table = 'orders'; $filter_column = "o.order_date"; break;
                        case 7 : $filter_table = 'orders'; $filter_column = "o.shipping_date"; break;
                        case 8 : $filter_table = 'order_shipping'; $filter_column = "os.tracking_number"; break;
                        case 9 : $filter_table = 'order_invoice'; $filter_column = "oi.seller_order_id"; break;
                        case 10 : $filter_table = 'order_invoice';$filter_column = "oi.invoice_no"; break;
                    }
                    if(isset($filter_table) && isset($filter_column)){
                        $search[$key]['value'] = $order_columns['search']['value'];
                        $search[$key]['table'] = $filter_table;
                        $search[$key]['column'] = $filter_column;
                    }
                    $key++;
                }
            }
            
            $data = array();
            $data['draw'] = $page;
            $data['recordsTotal'] = $this->amazon_order->getOrderLists($this->id_shop, $country, null, null, $order_by, $search, true, $status);
            $data['recordsFiltered'] = $data['recordsTotal'];
            $data['data'] = array();
            $orders = $this->amazon_order->getOrderLists($this->id_shop, $country, $start, $limit, $order_by, $search, false, $status);

            foreach ($orders as $order){
                array_push($data['data'], $order);
            }

            echo json_encode($data); 
        }
    }    
    
    public function orders_acknowledgement($country, $id_order, $order_number) {
	require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.status.orders.php';
	$this->_country_data($country);
	$params = $this->api_settings;
	$amazon_order = new Amazon_Order($this->user_name, true);
	$order_data = $amazon_order->feedbiz_order->getOrdersByID($this->user_name, $id_order);
	$orders = array();
	$orders[$id_order]['AmazonOrderID'] = $order_data['id_marketplace_order_ref'] ; 
	$orders[$id_order]['MerchantOrderID'] = $order_number ;		
	$amazonOrders = new AmazonStatusOrders((object)$params, false, AmazonStatusOrders::AmazonOrderAcknowledgement);
	$amazonOrders->updateAcknowledgeOrder($orders);  
    }
        
    public function changeOrderAddressName($id_orders){        
        $post = $this->input->post();
        if(isset($post['name']) && !empty($post['name'])) {            
            $this->amazon_order = new Amazon_Order($this->user_name, true);
            if($this->amazon_order->feedbiz_order->changeOrderAddressName($this->user_name, $id_orders, $post['name'])) {
                echo json_encode(array('success' => true)); exit;
            }
        }
        echo json_encode(array('success' => false)); exit;
    }
        
    public function save_orders_detail(){
        $post = $this->input->post();
        if(isset($post)) {
            $id_order = $post['id_orders'];
            unset($post['id_orders']);
            $data = $post;
            $this->amazon_order = new Amazon_Order($this->user_name, true);
            if($this->amazon_order->feedbiz_order->changeOrderAddress($this->user_name, $id_order, $data)){
                if($this->amazon_order->feedbiz_order->updateOrderErrorStatus($this->user_name, $id_order, '', '')){
                    echo json_encode(array('success' => true)); exit;
                }
            }
        }
        echo json_encode(array('success' => false)); exit;
    }

    public function get_orders_detail($id_order) {
        $this->amazon_order = new Amazon_Order($this->user_name, true);
        $orders = $this->amazon_order->getOrdersToCreateFBA($this->id_shop, $id_order, 'orders_detail') ;
        echo json_encode($orders);
    }  
    
    public function get_key_pairs($ext) {
        echo json_encode($this->amazon_model_user->get_key_pairs($this->id_user, $ext));
    }

    public function checkWS($actions=null) {
        
        if ($this->input->post()) {
            $message = array();
            $values = $this->input->post();
            $id_country = $values['id_country'];
            $merchantId = $values['merchant_id'];
            $authToken = $values['auth_token'];
            $this->_country_data($values['id_country']);
            $api = $this->_get_api();
            if(isset($api)){
                $awsKeyId = $api[$this->id_region]['aws_id'];
                $awsSecretKey = $api[$this->id_region]['secret_key'];
            }

            if(!empty($merchantId) && !empty($awsKeyId) && !empty($awsSecretKey))
            {
                $get_access_message = '';
                if(isset($actions) && !empty($actions)){
                    $get_access_message = $this->lang->line("Please click 'Get Access Data' to establish an authenticated connection between Feed.biz and Amazon");
                }
                
                $amazonApi = $this->connect_to_amazon_webservice($id_country, $merchantId, $awsKeyId, $awsSecretKey, $authToken);

                if($amazonApi){
                    $result = $amazonApi->ListMarketplaceParticipations();                   
                } else {                    
                    $message['error'][] = $this->lang->line('Unable to login');

                    if(isset($actions) && !empty($actions)){
                        $message['error'][] = $get_access_message;
                    }
                }                
                if (isset($result->ListMarketplaceParticipationsResult->ListParticipations)){
                    $message['success'] = $this->lang->line('Connection to Amazon successful');
                }
                elseif (isset($result) && is_object($result) && isset($result->Error)) {
                    $message['error'][] = strval($result->Error->Message);
                    if(isset($actions) && !empty($actions)){
                        $message['error'][] = $get_access_message;
                    }
                } else {
                    $message['error'][] = strval($result);
                    if(isset($actions) && !empty($actions)){
                        $message['error'][] = $get_access_message;
                    }
                }
            } else {
                $message['code'] = _get_message_code('22-00007', 'inner');
            }
            
            echo json_encode($message);
        }
    }
    
    public function connect_to_amazon_webservice($id_country, $merchantId, $awsKeyId, $awsSecretKey, $authToken = null){
        
        $amazonApi = null;
        $currency = $ext = '';
        
        ob_start();

        if (isset($this->session->userdata['menu']))
            $marketplace = $this->session->userdata['menu'];
        if (isset($marketplace[self::TAG]['submenu'][$id_country]['currency'])) {
            $currency = $marketplace[self::TAG]['submenu'][$id_country]['currency'];
            $ext = $marketplace[self::TAG]['submenu'][$id_country]['ext'];
        }
        
        $marketPlaceRegion = Amazon_Tools::iso_codeToId($ext);
        $marketPlaceCurrency = $currency;

        $auth = array(
            'MerchantID' => trim($merchantId),
            'AWSAccessKeyID' => trim($awsKeyId),
            'SecretKey' => trim($awsSecretKey)
        );

        if(isset($authToken) && !empty($authToken)){
            $auth['auth_token'] = trim($authToken);
        }
        
        $marketPlace = array();
        $marketPlace['Currency'] = $marketPlaceCurrency;
        $marketPlace['Country'] = $marketPlaceRegion;

        if (!$amazonApi = new Amazon_WebService($auth, $marketPlace, null, false))
            return false;            

        ob_get_clean();
        
        return $amazonApi;
    }

    public function mappings($country = null, $steps = null, $mode = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        require_once dirname(__FILE__) . '/../libraries/tools.php';
        
        $data = $fields = array();        
        $this->_country_data($country);
        $this->_set_session_message();
        
        $params = $this->_setParams();  	
	
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params); 	
        $mappings = array_remove_empty( $DisplayForm->displayMappingForm($country) );       

	foreach ($mappings as $mapping) {
	    foreach ($mapping as $mapp){
		foreach ($mapp as $map){
		    if(isset($map['fields']))
			$fields[] = true;
		}
	    }
	}
	
        if (empty($fields)) {
            if (isset($mode) && $mode == 2) {
                redirect('amazon/creation/' . $country);
            }
        }
	
	if(isset($mappings['attribute']))
	    $data['mappings'] = $mappings['attribute'];
        
        //Attribute
        if(isset($data['mapping_attribute']['mapping_attribute_flag']))
            $this->smarty->assign("mapping_attribute_flag", $data['mapping_attribute']['mapping_attribute_flag']);

        //Feature
        if(isset($data['mapping_feature']['mapping_feature_flag']))
            $this->smarty->assign("mapping_feature_flag", $data['mapping_feature']['mapping_feature_flag']);

        //Popup Step More        
        if (isset($steps) && !empty($steps)) {
            $this->smarty->assign("popup_more", true);
            $this->smarty->assign("mode", 2);
            $this->smarty->assign("popup", 3);
            $this->smarty->assign("no_data", _get_message_code('22-00011', 'inner'));
        }
        
        // Warning unable save amazon_status to update offers 
        if ($this->session->userdata('status_warning') && $this->session->userdata('status_warning') == 'status_fail') {
            _get_message_code('22-00012');
        }        
            
        // Check for save&continue
        if($this->_not_continue($this->ext, 'import_orders')){
            $this->_not_continue($this->ext, 'second_hand');            
        }  
        
	//no field to map
	if (empty($fields)) {
	    $this->smarty->assign("no_mapping_info", _get_message_code('23-00009', 'inner'));
	}
	$this->smarty->assign("no_mapping", _get_message_code('23-00010', 'inner'));
	
	// avoid duplicate header language
	if(isset($params->iso_code) && $params->iso_code == 'en')
	    $this->smarty->assign("no_translate", true);
	    
        $this->smarty->view('amazon/Mappings.tpl', $data);
    }
    
    public function mappings_custom_value($country){
        $data = array();
        $this->_country_data($country);
       
        $params = $this->_setParams();        
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params); 
        $data['mappings'] = array_remove_empty( $DisplayForm->displayMappingCustomValueForm($country) );

        $this->smarty->assign("popup", true);
        $this->smarty->view('amazon/MappingsCustomValue.tpl', $data);
    }

    public function carriers($country = null, $id = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');

        $data = array();
        $this->_country_data($country);
        $this->_set_session_message();
        
        //Amazon Carrier
        require_once dirname(__FILE__) . '/../libraries/Amazon/controllers/amazon.display.carrier.php';
        $data['amazon_carrier_incoming'] = Amazon_Carrier::display();
        $data['amazon_carrier_outgoing'] = Amazon_Carrier::$carrier_codes;
        $data['amazon_shipping_override'][Amazon_Carrier::SHIPPING_STANDARD] = Amazon_Carrier::getShippingMethods(Amazon_Carrier::SHIPPING_STANDARD);
        $data['amazon_shipping_override'][Amazon_Carrier::SHIPPING_EXPRESS] = Amazon_Carrier::getShippingMethods(Amazon_Carrier::SHIPPING_EXPRESS);
        $data['shipping_standard'] = Amazon_Carrier::SHIPPING_STANDARD;
        $data['shipping_express'] = Amazon_Carrier::SHIPPING_EXPRESS;
        
        if($id)
        {
            $cssClass = ' class="form-control m--t10"';
            $this->smarty->assign('text_id', 'carrier[outgoing]['.$id.'][other]');
            $this->smarty->assign('text_cssClass', $cssClass);
            $this->smarty->assign('text_rel', 'outgoing-carrier-other');
            $this->smarty->assign('text_value','');
            $this->smarty->assign('text_additional_attributes','');
            $this->smarty->assign('text_disabled','');
            $html = $this->smarty->fetch('amazon/inputTextHTML.tpl');
             
            echo json_encode($html);
            exit;
        }
        //Carrier 
        $params = $this->_setParams();        
        $DisplayForm = new AmazonDisplayForm($this->amazon_model, $this->feedbiz, $params); 
        $data['mapping_carrier'] = $DisplayForm->get_mapping_carrier($country);
        $data['carriers'] = $this->feedbiz->getCarriers($this->user_name, $this->id_shop); // $this->displayForm->get_mapping_carrier($country);
        
        $data['shipping_override'] = $this->amazon_model->get_mapping_shipping_override($country, $this->id_shop); // $this->displayForm->get_mapping_carrier($country);
        $this->smarty->assign("mapping_carrier_flag", $data['mapping_carrier']['mapping_carrier_flag']);
        $this->smarty->assign("carrier_expand", 'true');       

        // Check for save&continue
        $this->_not_continue($this->ext, 'second_hand'); 
        
        $this->smarty->view('amazon/Carriers.tpl', $data);
    }
    
    public function save_mappings_custom_value() {
        $return = array();
        if ($this->input->post()) {
            $values = $this->input->post();
            if(isset($values['ext']) && !empty($values['ext'])){
                 $return = $this->amazon_model->save_mapping_valid_value_custom($values, $values['ext']);
            }
        }  
        echo json_encode($return);
    }
    
    public function save_mappings() {
        
        if ($this->input->post()) {
            $values = $this->input->post();
            $id_country = $values['id_country'];
	    
            if (!$this->amazon_model->save_mapping_attribute($values, $this->id_shop, $this->id_mode, $id_country)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/mappings/' . $id_country);
            }
            if (!$this->amazon_model->save_mapping_feature($values, $this->id_shop, $this->id_mode, $id_country)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/mappings/' . $id_country);
            }
            
            // To update all product when user set new mapping
            if(!$this->amazon_model->update_amazon_status($id_country, $this->id_shop, date('Y-m-s H:i:s'), 'mappings')) {
                $this->session->set_userdata('status_warning', 'status_fail');
            } else {
                $this->session->set_userdata('status_warning', false);
            }
        }
        if (isset($values['save'])) {
            if ($values['save'] == "next") {
                if (!$this->_check_step_more($values['id_country'], 'profiles'))
                    if ($values['mode'] == 1) {
                        redirect('amazon/matching/' . $id_country);
                    } else if ($values['mode'] == 2) {
                        redirect('amazon/creation/' . $id_country);
                    }
            }
            else if ($values['save'] == "continue") {
                
                $this->session->set_userdata('message', 'save_successful');
               
                // If import_orders
                if(!$this->_not_continue($values['ext'], 'import_orders')){
                    redirect('amazon/carriers/' . $id_country);

                // If import_orders
                } else if(!$this->_not_continue($values['ext'], 'second_hand')){
                    redirect('amazon/conditions/' . $id_country);
                }
                
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/mappings/' . $id_country);
            }
        } else {
            $this->session->set_userdata('message', 'save_successful');
            redirect('amazon/mappings/' . $$id_country);
        }
    }
    
    public function save_carriers() {
        if ($this->input->post()) {
            $values = $this->input->post();
            $id_country = $values['id_country'];

            if (!$this->amazon_model->save_mapping_carrier($values, $this->id_shop, $this->id_mode, $id_country)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/carriers/' . $id_country);
            }	    
            if (isset($values['save'])) {
                if ($values['save'] == "continue") {
                    $this->session->set_userdata('message', 'save_successful');
                    redirect('amazon/conditions/' . $id_country);
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                    redirect('amazon/carriers/' . $id_country);
                }
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/carriers/' . $$id_country);
            }
        }
    }
    
    public function profiles($country = null, $steps = null) {
        
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
                
        $data = array();
        
        $this->_country_data($country);   
        $this->_set_session_message();
        $display_features = $this->amazon_model->get_configuration_features($country, $this->id_shop);

        //Model
        $model_list = array();
        $models = $this->amazon_model->get_amazon_model($country, $this->id_mode, $this->id_shop);
        foreach ($models as $models_key => $models_value) {
            $model_list[$models_key]['name'] = $models_value['name'];
            $model_list[$models_key]['id_model'] = $models_value['id_model'];
        }
        
        if (isset($model_list) && !empty($model_list)){
            $this->smarty->assign("models", $model_list);
        } else {
            $data['no_models'] = _get_message_code('22-00008', 'inner');
        }
        
        // Product Feature for product key specific
        $lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
        $features = $this->feedbiz->getFeatures($this->id_shop, $lang['id_lang']);
	
	// remove empty feature value ; 2/3/2016
	if(isset($features))
	    foreach ($features as $fkey => $feature)
	    {
		if(!isset($feature['value']) || empty($feature['value']))
		    unset($features[$fkey]);
	    }
        
        if (isset($features) && !empty($features))
            $this->smarty->assign("features", $features);
        
        //Mode
        if (isset($this->id_mode) && !empty($this->id_mode)) {
            $data['id_mode'] = $this->id_mode;
        }
        // Creation
        $parameters = $this->api_settings;
        if (isset($parameters['creation']) && !empty($parameters['creation'])) {
            $this->smarty->assign("creation", $parameters['creation']);
        }
        //Profile
        $profiles = $this->amazon_model->get_amazon_profile($country, $this->id_mode, $this->id_shop);

        if(empty($profiles) && !empty($this->api_settings)){
            $this->smarty->assign("no_data", _get_message_code('23-00002', 'inner'));
        } else {
            $data['profile'] = $profiles;  
        }
        $this->smarty->assign("add_data", $this->lang->line('Add a profile to the list'));
        if(isset($profiles) && !empty($profiles)){
            $this->smarty->assign("size_data", sizeof($profiles));
        }
        foreach ($profiles as $pk => $pf) {
            while(strpos($pf['name'],"''")!==false){
                $pf['name'] = str_replace("''","'",$pf['name']); 
            }
            $data['profile'][$pk]['name']=$pf['name'];
            if (isset($pf['code_exemption']['chk']) && $pf['code_exemption']['chk'] == 1)
                if (isset($pf['code_exemption']['manufacturer']))
                    foreach ($pf['code_exemption']['manufacturer'] as $mkey => $manufacturer)
                        $data['profile'][$pk]['code_exemption']['manufacturer'][$mkey] = $this->get_manufacturers($pk, $manufacturer, ($mkey == 0) ? $mkey : null);
        }        
        // Warning unable save amazon_status to update offers 
        if ($this->session->userdata('status_warning') && $this->session->userdata('status_warning') == 'status_fail') {
            _get_message_code('22-00012');
        }
        // Repricing
        if(isset($display_features['repricing']) && $display_features['repricing']) {
            $repricing_strategies = $this->get_repricing_strategies($country);
            if(!isset($repricing_strategies['strategies']) || empty($repricing_strategies['strategies'])) {
                $data['no_repricing_strategies'] = _get_message_code('22-00014', 'inner');
            } else {
                $data['repricing_strategies'] = $repricing_strategies['strategies'];
            }
        }
        if(isset($display_features['shipping_override']) && $display_features['shipping_override']) {
            $shipping_groupnames = $this->amazon_model->get_shipping_group_names($this->id_shop, $country);
            if(!isset($shipping_groupnames) || empty($shipping_groupnames)) {
                $data['no_shipping_groups'] = _get_message_code('22-00015', 'inner');
            } else {
                $data['shipping_groups'] = $shipping_groupnames;
            }
        }
        //Advertising
        if(isset($display_features['advertising']) && $display_features['advertising']) {
            $advertising_groups = $this->advertising('get_groups', $country);
            if(!isset($advertising_groups) || empty($advertising_groups)) {
                $data['no_advertising_groups'] = _get_message_code('22-00016', 'inner');
            } else {
                $data['advertising_groups'] = $advertising_groups;
            }
        }
        
        if (isset($steps) && !empty($steps)) {
            $decode_steps = unserialize(base64_decode($steps));
            $this->smarty->assign("popup_more", $decode_steps);
            $this->smarty->assign("mode", 2);
            $this->smarty->assign("popup", 2);
            // has profile but don't have model
            if(isset($decode_steps['no_model']) && $decode_steps['no_model']){
                $this->smarty->assign("no_data", _get_message_code('22-00013', 'inner'));           
            } else{
                $this->smarty->assign("no_data", _get_message_code('22-00010', 'inner')); 
            }
        }        
        $this->smarty->view('amazon/Profiles.tpl', $data);
    }

    public function save_profiles() {        
        if ($this->input->post()) {
            $values = $this->input->post();

            $id_country = $values['id_country'];
            $display_features = $this->amazon_model->get_configuration_features($id_country, $this->id_shop);
            $advertising_groups = null;

            //Advertising
            if(isset($display_features['advertising']) && $display_features['advertising']) {
                $advertising_groups = $this->advertising('get_groups', $id_country);
            }

            $save_profile = $this->amazon_model->save_profiles($values, $this->id_shop, $this->id_mode, $id_country, $advertising_groups);
            
            if ($save_profile == false) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/mappings/' . $id_country);
            } else if ($save_profile === 'status_fail') {
                $this->session->set_userdata('status_warning', 'status_fail');
            } else {
                $this->session->set_userdata('status_warning', false);
            }
        }
        //redirect
        if (isset($values['save'])) {
            if ($values['save'] == "next") {
                redirect('amazon/category/' . $id_country . '/3/2');
            } else if ($values['save'] == "continue") {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/category/' . $id_country);
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/profiles/' . $id_country);
            }
        } else {
            $this->session->set_userdata('message', 'save_successful');
            redirect('amazon/profiles/' . $$id_country);
        }
    }

    public function category($country = null, $popup = null, $mode = 1) {        
                
        if (!isset($country) || empty($country) || $country == null)
            redirect('/marketplace/configuration');
        
        if($mode == 2){
            $this->_check_step_more($country, 'models');
        }
        
        $this->_country_data($country);
        $this->_set_session_message();
        
        $data = $profile = array();
        $data['mode'] = $mode;
        $data['prifile_mapping'] = true;
        $data['feed_mode'] = $this->id_mode;
        $this->smarty->assign("id_country", $country);
		
        //Profile
        if (isset($popup) && !empty($popup)) {
            
            if ($popup == 4){
                $this->smarty->assign("popup_state", 4);
            }else{
                $this->smarty->assign("popup_state", 3);
            }

            $this->smarty->assign("popup", 3);

            if($mode == 1) {
            	$data['prifile_mapping'] = false;
            }
            if ($mode == 2){
                $profile = $this->amazon_model->get_amazon_profile($country, $this->id_mode, $this->id_shop);
            }
        } else {
            $profile = $this->amazon_model->get_amazon_profile($country, $this->id_mode, $this->id_shop);
        }

        if (isset($profile) && !empty($profile)){
            $data['profile'] = $profile;
        } else {
            if(!isset($popup)){
                _get_message_code('22-00006');
            }
        }
        
        // Check for save & continue
        if($this->_not_continue($this->ext, 'creation')){
            if($this->_not_continue($this->ext, 'import_orders')){
                $this->_not_continue($this->ext, 'second_hand');
            }
        }       
        
	$data['no_category'] = _get_message_code('12-00003', 'inner');
        $this->smarty->view('amazon/Category.tpl', $data);
    }

    public function getSelectedAllCategory($id_country, $mode = null) {
        $html = '';
        
        $this->_country_data($id_country);
        $lang = $this->feedbiz->checkLanguage($this->iso_code, $this->id_shop);
                
        $roots = $this->feedbiz->getAllRootCategories($this->user_name, $this->id_shop,$lang,true);
        $this->id_mode = $id_mode = 1;//for all mode
                    
        $arr_selected_categories = array();
        if(!empty($roots)){
            foreach($roots as $root){
                $arr_selected_categories_res =$this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $id_mode, $root['id'], false, $lang['id_lang']);
                $name = isset($root['name'][$this->iso_code])?$root['name'][$this->iso_code]:(isset($root['name'][$lang['iso_code']])?$root['name'][$lang['iso_code']]:'');
                if(empty($name)){continue;}
                $arr_selected_categories[] = array('id_category'=>$root['id'],'name'=>$name,'parent'=>0,'type'=>'folder','child'=>sizeof($arr_selected_categories_res));
            } 
        } 
        
        $profile = $this->amazon_model->get_amazon_profile($id_country, $this->id_mode, $this->id_shop);    
        foreach ($arr_selected_categories as $asc)
            $html .= $this->getChildCategory($lang['id_lang'], $asc, $id_country, $mode, $profile);
	
        echo json_encode($html);

    }

    public function getChildCategory($id_lang, $category, $id_country, $mode = null, $profile = null) {
        $html = $subchild = '';

        if ($mode == "null" || $mode == "undefined" || $mode == "")
            $mode = null;

        if (isset($mode) && !empty($mode)) {
            $this->smarty->assign("popup", 3);
            $this->smarty->assign("mode", $mode);
        }

        if (isset($category) && !empty($category)) {
            if ($category['type'] == "folder") {
                if (isset($category['child']) && !empty($category['child'])) {
                    $id_mode = 1;//for all mode
                    $categories = $this->feedbiz->getSelectedCategoriesOffer($this->user_name, $this->id_shop, $id_mode, $category['id_category'], false, $id_lang);
                    foreach ($categories as $child) {
                        $subchild .= $this->getChildCategory($id_lang, $child, $id_country, $mode, $profile);
                    }
                }
            }
        }
        
        $html .= $this->getSelectedCategoryTemplate($category, $id_country, $subchild, $mode, $profile);
        return $html;
    }

    public function getSelectedCategoryTemplate($selected_category, $id_country, $sub_html = null, $mode = null, $profile = null) {
            
        if(!isset($this->list_selected)){           
            $this->list_selected = $this->amazon_model->get_profile_selected($id_country,$this->id_mode,$this->id_shop,(isset($mode)?$mode:false));
        }
        $checked=false;
	
	if(isset($profile))
	    $this->smarty->assign('profile', $profile);
	
        if (isset($this->list_selected[$selected_category['id_category']]) && !empty($this->list_selected[$selected_category['id_category']])) {
            $this->smarty->assign('checked', true);
            $this->smarty->assign('selected', $this->list_selected[$selected_category['id_category']]['id_profile']);
            $checked=true;
        } else {
            $this->smarty->assign('checked', false);
            $this->smarty->assign('selected', '');
        }
        
        if (isset($sub_html) && !empty($sub_html)) {
            $this->smarty->assign('sub_html', $sub_html);
        } else{ 
            $this->smarty->assign('sub_html', '');
        }

        $this->smarty->assign('type', $selected_category['type']);
        $this->smarty->assign('id_category', $selected_category['id_category']);
        $this->smarty->assign('name', $selected_category['name']);
        $this->smarty->assign('mode', $mode);

        return $this->smarty->fetch('amazon/CategoryGroupTemplate.tpl');
    }
    public function duplicate_category() {
        if ($this->input->post()) {
            $values = $this->input->post();
            $id_country = $values['id_country']; 
            $this->amazon_model->duplicate_category_selected($values, $this->id_shop, $id_country);
             echo json_encode(array('pass'=>true)); exit;
        }

        echo json_encode(array('pass'=>false)); exit;
    }
    public function save_category($popup=null) {
        
        if ($this->input->post()) {
            $values = $this->input->post();            
            $id_country = $values['id_country'];           
            $this->id_mode = 1; //for all mode
            if (!$this->amazon_model->save_category_selected($values, $this->id_shop, $this->id_mode, $id_country, $popup)) {
                $this->session->set_userdata('error', 'save_unsuccessful');
                redirect('amazon/mappings/' . $id_country);
            }
            //redirect
            if (isset($values['save'])) {
                if ($values['save'] == "next") {
                    if ($values['mode'] == 1) {
                        redirect('amazon/matching/' . $id_country);
                    } else if ($values['mode'] == 2) {                        
                        redirect('amazon/mappings/' . $id_country . '/4/2');
                    }
                } else if ($values['save'] == "continue") {
                    
                    $this->session->set_userdata('message', 'save_successful');
                    
                    // If creation mode 
                    if(!$this->_not_continue($values['ext'], 'creation')){
                        redirect('amazon/mappings/' . $id_country);
                        
                    // If import_orders
                    } else if(!$this->_not_continue($values['ext'], 'import_orders')){
                        redirect('amazon/carriers/' . $id_country);
                    
                    // If import_orders
                    } else if(!$this->_not_continue($values['ext'], 'second_hand')){
                        redirect('amazon/conditions/' . $id_country);
                    }
                    
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                    redirect('amazon/category/' . $id_country);
                }
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/category/' . $id_country);
            }

            $this->session->set_userdata('message', 'save_successful');
            redirect('amazon/category/' . $id_country);
        }
    }

    public function get_manufacturers($element, $selected = null, $key = null) {
        $manufacturers = $this->feedbiz->getManufacturers($this->user_name, $this->id_shop);
        $attr_options = array();

        foreach ($manufacturers as $k => $opt) {
            $option = array();
            $option["value"] = $k;
            $option["desc"] = $opt;
            if (isset($selected))
                $option["selected"] = ($selected == $k) ? ' selected' : '';
            $attr_options[] = $option;
        }
       
        $this->smarty->assign('select_id', 'profile[' . $element . '][code_exemption][manufacturer][]');
        $this->smarty->assign('select_cssClass', 'class="custom_value search-select"');
        $this->smarty->assign('select_data_content', $element);
        $this->smarty->assign('select_rel', '[code_exemption][manufacturer][]');
        $this->smarty->assign('select_options', $attr_options);
        $input_html = $this->smarty->fetch('amazon/inputSelectHTML.tpl');

        if (isset($selected) && isset($key)) {
            $this->smarty->assign('add_option', true);
            $this->smarty->assign('displayName', 'Manufacturers');
            $this->smarty->assign('removable_option', false);
        } else {
            if (!isset($selected) && !isset($key)) {
                $this->smarty->assign('removable_option', false);
                $this->smarty->assign('displayName', 'Manufacturers');
                $this->smarty->assign('add_option', true);
            } else {
                $this->smarty->assign('add_option', false);
                $this->smarty->assign('displayName', '');
                $this->smarty->assign('removable_option', true);                
            }
        }
        $this->smarty->assign('withIcon', false);
        $this->smarty->assign('additionalStyle', 'margin-bottom: 10px;');
        $this->smarty->assign('includeLabel', true);
        $this->smarty->assign('input_html', $input_html);
        $element = $this->smarty->fetch('amazon/FieldGroupHTML.tpl');

        if (isset($selected)) return $element;
        else echo json_encode($element);
    }

    public function form_data($country=null) {
        
        if(isset($country)) {
            $this->_country_data($country);
            global $params;
            $params = $this->_setParams();
            $params->id_country = $country;
        }
        
        require_once dirname(__FILE__) . '/../libraries/Amazon/functions/amazon.form.data.php';
    }
    
    public function get_report_inventory($country) {
        
        $amazon_product = $this->amazon_model->get_report_inventory($country, $this->id_shop);
        $data['amazon_no_product'] = count($amazon_product);        
        $data['feed_no_product'] = $this->feedbiz->getNumberOfProduct($this->user_name, $this->id_shop);
        $data['product_to_created'] = $this->amazon_model->getNumProducts($country, $this->id_mode, $this->id_shop);
        
        $user = new stdClass();
        $user->id_shop = $this->id_shop;
        $user->id_country = $country;
        $data['product_to_sync'] = sizeof($this->amazon_model->exportProductListByMode($user, AmazonDatabase::MODE_SYNC));
        
        echo json_encode($data);
    }

    //Save form Data
    public function save_data() {
        if ($this->input->post()) {
            
            $values = $this->input->post();
            
            $id_country = $values['id_country'];
                    
            if (isset($values['model']) && !empty($values['model'])) {
                $save_data = $this->amazon_model->save_model($values['model'], $this->id_shop, $id_country, $this->id_mode);
                if (!$save_data) {
                    $this->session->set_userdata('error', 'save_unsuccessful');
                    redirect('amazon/models/' . $id_country);
                } else if ($save_data === 'status_fail') {
                    $this->session->set_userdata('status_warning', 'status_fail');
                } else {
                    $this->session->set_userdata('status_warning', false);
                }
            } else {
                $this->session->set_userdata('error', 'No data to save');
                if(isset($values['popup_more'])) {
                    redirect('amazon/models/' . $id_country . '/' . $values['popup_more']);
                } else {
                    redirect('amazon/models/' . $id_country);
                }
            }

            //redirect
            if (isset($values['save'])) {
                if ($values['save'] == "next") {
                    if (!$this->_check_step_more($values['id_country'], 'profiles', 'models'))
                        redirect('amazon/category/' . $values['id_country'] . '/2/' . $values['mode']);
                }
                else if ($values['save'] == "continue") {
                    $this->session->set_userdata('message', 'save_successful');
                    redirect('amazon/profiles/' . $id_country);
                } else {
                    $this->session->set_userdata('message', 'save_successful');
                    redirect('amazon/models/' . $id_country);
                }
            } else {
                $this->session->set_userdata('message', 'save_successful');
                redirect('amazon/models/' . $$id_country);
            }
        } else {
            $this->session->set_userdata('error', 'save_unsuccessful');
            redirect('amazon/models/' . $id_country);
        }
    }

    public function delete_data() {
        if ($this->input->post()) {
            $data = array();
            $values = $this->input->post();
            $data['id_model'] = $values['id_model'];
            $data['id_country'] = $values['id_country'];
            $data['id_shop'] = $this->id_shop;
            $data['id_mode'] = $this->id_mode;

            if (!$this->amazon_model->delete_model($data)) echo "false";
            else echo "true";
        }
    }

    public function delete_profile() {
        if ($this->input->post()) {
            $data = array();
            $values = $this->input->post();
            $data['id_profile'] = $values['id_profile'];
            $data['id_country'] = $values['id_country'];
            $data['id_shop'] = $this->id_shop;
            $data['id_mode'] = $this->id_mode;

            if (!$this->amazon_model->delete_profile($data)) echo "false";
            else echo "true";
        }
    }

    public function downloadLog($id_country, $id_shop, $batch_id = null, $id_submission = null) {
        $data = array();
        $data['batch_id'] = $batch_id;
        $data['id_submission_feed'] = $id_submission;
        $data['id_country'] = $id_country;
        $data['id_shop'] = $id_shop;
        $this->amazon_model->downloadLog($data);
    }    

    public function downloadOrderLog($id_country, $id_shop, $batch_id) {        
        $this->amazon_order = new Amazon_Order($this->user_name);
        $data = array();
        $data['batch_id'] = $batch_id;
        $data['id_country'] = $id_country;
        $data['id_shop'] = $id_shop;
        $this->amazon_order->downloadLog($data);
    }
    
    public function conditions($id_country = null){ 
        $this->_country_data($id_country);
        $this->load->model('marketplace_model','mk');        
        $this->load->model('paypal_model','paypal');  
        $offerUserPackage = $this->paypal->getUserPackageID($this->id_user, true);
        $amz = $ebay = false;
        $amazon_id = $this->id_marketplace;

        if (!isset($this->session->userdata['mode_default']) || $this->session->userdata['mode_default'] == 1){
            $data['expert_mode'] = true;
        }
            
        foreach($offerUserPackage as $pk){
            if($pk['id_offer_pkg']==$amazon_id){ $amz = true;}
        }
        if($amz){
            $mk_cond[strtolower(self::TAG)] = $this->mk->getMarketplaceCondition($amazon_id);
            $shop_condition[strtolower(self::TAG)] = $this->feedbiz->getConditionMapping($this->user_name,$this->id_shop,$amazon_id);
        }        
        
        if(!isset($mk_cond) || sizeof($mk_cond) == 0){
            $data['no_market'] = true;
        }else{ 
            foreach ($shop_condition[strtolower(self::TAG)] as $shop_conditions){
                if(empty($shop_conditions['condition_value'])){
                    foreach ($mk_cond[strtolower(self::TAG)] as $amazon_conditions){
                        if(trim($amazon_conditions['condition_text']) === trim(ucfirst(strtolower($shop_conditions['txt']))) ){
                            $val = array(
                                'id_marketplace' => $amazon_conditions['id_marketplace'],
                                'condition_value' => $amazon_conditions['condition_value'],
                                'name' => $shop_conditions['txt'],
                            );
                            $this->save_conditions($val);
                        }
                    }
                }
            }            
            
            $mk_cond[strtolower(self::TAG)] = $this->mk->getMarketplaceCondition($amazon_id);
            $shop_condition[strtolower(self::TAG)] = $this->feedbiz->getConditionMapping($this->user_name,$this->id_shop,$amazon_id);
            
            $data['mk_condition'] = $mk_cond; 
            $data['shop_condition'] = $shop_condition;  
        }        
        $this->smarty->assign("not_continue", true);
        $this->smarty->view('amazon/Conditions.tpl', $data);         
    }
    
    public function save_conditions($val = null){ 
        
        $shop = $this->feedbiz->getDefaultShop($this->user_name);
        $id_shop = $shop['id_shop'];
        
        if(isset($val) && !empty($val)){
            $out = $this->feedbiz->setConditionMapping($this->user_name, $id_shop, $val['id_marketplace'], $val['condition_value'], $val['name']);
        }else {
            $this->load->model('marketplace_model','mk');
            $post = $this->input->post();  
           
            if(!empty($post)){
                foreach($post['cond'] as $key => $val){
                     $cond = $this->mk->getMarketplaceConditionByID($key); 
                    if(!empty($val)){ 
                        $out = $this->feedbiz->setConditionMapping($this->user_name, $id_shop, $cond['id_marketplace'], $cond['condition_value'], $val); 
                    }else{ 
                        $out = $this->feedbiz->removeConditionMapping($this->user_name, $id_shop, $cond['id_marketplace'], $cond['condition_value']); 
                    }
                }
            }
            redirect('amazon/conditions/'.$post['id_country']);
        }
    }

    private function _get_api($type=null){
        include dirname(__FILE__) . '/../libraries/Amazon/parameters/api.settings.php';
        $api = array();
        switch ($type){
            case "advertising" :
                $api = isset($amazon_advertising_api) ? $amazon_advertising_api : array();
                break;
            default :
                if(in_array(strtolower($this->config->item('site_name')), array('ndb.feed.dev','dev.feed.biz'))){
                    $api = isset($amazon_dev_api) ? $amazon_dev_api : array();
                } else {
                    $api = isset($amazon_api) ? $amazon_api : array();
                }
        }
        return $api;
    }
    
    private function _not_continue($ext, $lable){       
        $amazon_model = $this->amazon_model_user->get_mode($this->id_user, $this->user_name);
        if(isset($amazon_model[$this->id_shop]['fields'][$ext][$lable]) && $amazon_model[$this->id_shop]['fields'][$ext][$lable] == true){
            $this->smarty->assign("not_continue", false);
            return false;
        } else {
            $this->smarty->assign("not_continue", true);
            return true;
        }
    }
    
    private function _setParams(){        
        $params = new stdClass();
        $params->iso_code = $this->iso_code;
        $params->id_mode = $this->id_mode;
        $params->id_shop = $this->id_shop;
        $params->id_user = $this->id_user;
        $params->ext     = $this->ext;
        $params->user_name = $this->user_name;        
        $params->shop_name = $this->shop_default;  
        return $params;
    }
    
    private function _set_session_message(){
        if ($this->session->userdata('error')) {
            $this->smarty->assign("error", $this->lang->line($this->session->userdata('error')));
            $this->session->unset_userdata('error');
        }
        if ($this->session->userdata('message')) {
            $this->smarty->assign("message", $this->lang->line($this->session->userdata('message')));
            $this->session->unset_userdata('message');
        }
    }
    
    private function _check_table_exists() {       
        $amazon_table = new AmazonCreateDatabase($this->amazon_model->db->product, $this->user_name);
        $amazon_table->create_table();
    }

    private function _country_data($country = null) {
	    
	$this->smarty->assign("id_country", $country);

	if (isset($this->session->userdata['menu'])){
	    $marketplace = $this->session->userdata['menu'];
	}
	if (isset($marketplace[self::TAG]['id'])){
	    $this->id_marketplace = $marketplace[self::TAG]['id'];
	    $this->smarty->assign("id_marketplace", $this->id_marketplace);
	}
	if (isset($marketplace[self::TAG]['submenu'][$country]['country'])){
	    $this->country = $marketplace[self::TAG]['submenu'][$country]['country'];
	    $this->smarty->assign("country", $marketplace[self::TAG]['submenu'][$country]['country']);
	}
	if (isset($marketplace[self::TAG]['submenu'][$country]['ext'])) {
	    $this->smarty->assign("ext", $marketplace[self::TAG]['submenu'][$country]['ext']);
	    $this->ext = $marketplace[self::TAG]['submenu'][$country]['ext'];
	    if(isset($marketplace[self::TAG]['submenu'][$country]['comments'])){
		$this->iso_code = $marketplace[self::TAG]['submenu'][$country]['comments'];
	    }else{
		$this->iso_code = $marketplace[self::TAG]['submenu'][$country]['ext'];
	    }
	}
        if (isset($marketplace[self::TAG]['submenu'][$country]['currency'])) {
            $default_curency = $marketplace[self::TAG]['submenu'][$country]['currency'];
            $this->currency = $default_curency;
        }
	$currency = $this->feedbiz->getDefaultCurrency($this->id_shop);
	if (isset($currency['iso_code'])) {
	    $default_curency = $currency['iso_code'];
	} 
	$this->config->load('regex');
	$currencys = $this->config->item('currency_code');  
	if(isset($default_curency)) {
	    $this->smarty->assign("currency_sign", $currencys[$default_curency]);
        }
	if (isset($marketplace[self::TAG]['submenu'][$country]['id_region'])){
	    $id_region = $marketplace[self::TAG]['submenu'][$country]['id_region'];
	    $this->id_region = $id_region;
	    $this->smarty->assign($id_region, true);
	    if ( $id_region == "as" ){
		if(isset($marketplace[self::TAG]['submenu'][$country]['ext']) && $marketplace[self::TAG]['submenu'][$country]['ext'] == '.jp'){
		    $this->smarty->assign(strtolower($marketplace[self::TAG]['submenu'][$country]['country']), true);
		}
	    } 
	}
	if (isset($marketplace[self::TAG]['submenu'][$country]['region'])){
	    $this->region = $marketplace[self::TAG]['submenu'][$country]['region'];
	    $this->smarty->assign('region', $marketplace[self::TAG]['submenu'][$country]['region']);
	}
	$this->api_settings = $this->amazon_model_user->get_parameters($this->id_user, $this->ext, $this->id_shop);
	if(empty($this->api_settings) && !isset($this->exceptionWarningAPIMethod[$this->router->fetch_method()])) {
	    $this->smarty->assign('disabled_add', true);
	    _get_message_code('22-00001');
	}
    }
    
    private function _check_step_more($country, $step, $last_step = null, $json = false) {
        $steps = array();
        $k = 0;
        //Check Model 
        $models = $this->amazon_model->get_amazon_model($country, $this->id_mode, $this->id_shop);
        if (!isset($models) || empty($models) || $models == null) {
            $steps['active'] = ($step == "models") ? true : false;
            $steps['step'] = ($k + 1);
            $steps['title'] = 'Models';
            $k++;
        } else {
            //Check Profile
            $profiles = $this->amazon_model->get_amazon_profile($country, $this->id_mode, $this->id_shop);
            if (!isset($profiles) || empty($profiles) || $profiles == null) {            
                $steps['active'] = ($step == "profiles") ? true : false;
                $steps['step'] = ($k + 1);
                $steps['title'] = 'Profiles';
            } else {
                if($last_step == "models"){
                    $steps['active'] = ($step == "profiles") ? true : false;
                    $steps['step'] = ($k + 1);
                    $steps['title'] = 'Profiles';
                    $steps['no_model'] = true;
                }
            }
        }
        if (isset($steps) && !empty($steps)) {
            if($json){
                if ($steps['active'] == true) {
                    $encode_steps = base64_encode(serialize($steps));
                    echo json_encode(array('pass' => true, 'next_page' => $steps['title'], 'step' => $encode_steps));
                    exit;
                }
            } else {
                if ($steps['active'] == true) {
                    $encode_steps = base64_encode(serialize($steps));
                    redirect('amazon/' . strtolower($steps['title']) . '/' . $country . '/' . $encode_steps);
                }
            }
        } else {
            return false;
        }
    }

}