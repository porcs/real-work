<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class tasks extends CI_Controller {

    // controller for control , manange and run import/export feed on cron jobs
    public $id_shop;
    public $user_name;
    public $user_id;
    public $run = true;
    public $aync = false;

    /* @FREETAIL = free trial month */

    const FREETAIL = 0;

    public function __construct() {

        // load controller parent
        parent::__construct();
        ini_set('display_errors', 1);
        date_default_timezone_set('UTC');

        $this->language = $this->session->userdata('language') ? $this->session->userdata('language') : $this->config->item('language');
        $this->lang->load("my_feeds", $this->language);
        $func = $this->uri->segment(2);
        if ($func == 'display_popup_template') {
            return;
        }


        // 1. library
        $this->load->model('cron_model', 'cron_model');
        $this->user_id = $this->session->userdata('id');
        $ip = $this->cron_model->getBestBackendHost($this->user_id);
        $def = (@$this->config->item('ssl_protocol') ? 'https://' : 'http://').'127.0.0.1:3001';
        $target_host = empty($ip) ? $def : $ip.':3001';
        if (!empty($ip)) {
            $this->session->set_userdata('bh_url', $ip);
            $host_id = $this->cron_model->getIdByTargetIp($ip);
            $this->cron_model->setLastHost($this->user_id, $host_id);
        }
        $this->target_host = $target_host;
        $this->load->model('my_feed_model', 'my_feed_model');
        $this->import_feed_url = $target_host.'/import_feed/';
        $this->import_product_url = $target_host.'/import_product/';
        $this->import_offer_url = $target_host.'/import_offer/';
        $this->import_shop_order_url = $target_host.'/import_shop_order/';

        $this->crond_import_feed_url = "import_feed_cron.php";
        $this->crond_import_product_url = "import_product_cron.php";
        $this->crond_import_offer_url = "import_offer_cron.php";
        $this->crond_import_order_url = "import_shop_order_cron.php";
        $this->crond_update_offer_amazon_url = "amazon_synchronize_cron.php";
        $this->crond_update_offer_ebay_url = "ebay_export_offer_cron.php";

        $this->cron_import_feed_url = base_url()."tasks/run_import_by_node/{uid}/feed/system";
        $this->cron_import_product_url = base_url()."tasks/run_import_by_node/{uid}/product/system";
        $this->cron_import_offer_url = base_url()."tasks/run_import_by_node/{uid}/offer/system";
        $this->cron_import_order_url = base_url()."tasks/run_import_order_from_site/{uid}";

        // choose set in config//
        $this->export_amazon_create_url = $target_host.'/amazon/create/';
        $this->export_amazon_synchronize_url = $target_host.'/amazon/synchronize/';
        $this->export_amazon_report_url = $target_host.'/amazon/get/report/';
        $this->export_amazon_offers_url = $target_host.'/amazon/send/offers/';
        $this->export_amazon_orders_url = $target_host.'/amazon/get/orders/';
        $this->export_amazon_update_url = $target_host.'/amazon/update/orders/';
        $this->export_amazon_delete_url = $target_host.'/amazon/delete/products/';
        $this->export_amazon_repricing_url = $target_host.'/amazon/repricing/'; // 21/07/2015
        $this->export_amazon_fba_estimated_url = $target_host.'/amazon/fba/estimated/'; // 16/11/2015
        $this->export_amazon_fba_manager_url = $target_host.'/amazon/fba/manager/'; // 16/11/2015
        $this->export_amazon_fba_orders_status_url = $target_host.'/amazon/fba/order_status/'; // 16/11/2015
        $this->export_amazon_shipping_group_url = $target_host.'/amazon/shipping/group_names/'; // 21/01/2016

        $this->export_ebay_product_url = $target_host.'/ebay/export/';

        $this->export_mirakl_offers_url = $target_host.'/mirakl/send/offers/';
        $this->export_mirakl_orders_url = $target_host.'/mirakl/get/orders/';
        $this->export_mirakl_import_order_url = $target_host.'/mirakl/import/order/';
        $this->export_mirakl_accept_order_url = $target_host.'/mirakl/accept/order/';
        $this->export_mirakl_products_url = $target_host.'/mirakl/send/products/';
        $this->export_mirakl_delete_products_url = $target_host.'/mirakl/delete/products/';

        $this->create_profiles_product_url = $target_host.'/createuser/product/';
        $this->create_profiles_offer_url = $target_host.'/createuser/offer/';
        $this->create_profiles_order_url = $target_host.'/createuser/order/';

        /* Send order manual */
        $this->send_orders_url = $target_host.'/send/orders/';

        $this->threshold_feed = '3'; // mins
        $this->threshold_product = '3'; // mins
        $this->threshold_offer = '3'; // mins
        $this->threshold_min = '1'; // mins

        $this->file_crontab = USERDATA_PATH.'/cron/user_crontab.cron';
        $this->frequency_feed_feed = '1441'; // mins = 1 day
        $this->frequency_feed_offer = '91'; // mins
        $this->frequency_feed_order = '120'; // mins
        $this->frequency_update_offer_amazon = '91';
        $this->frequency_update_offer_ebay = '91';
        $this->frequency_update_offer_mirakl = '91';
        $this->frequency_feed_product = 0;
        $this->frequency_send_orders = '3'; //mins
        $this->list_tasks = array();

        $this->status_val ['enable'] = true;
        $this->status_val ['disable'] = false;
        $this->status_val ['default'] = true;

        $this->threshold_amazon_report = '0'; // mins
        $this->threshold_amazon_orders = '5'; // mins
        $this->threshold_amazon_create = $this->threshold_amazon_synchronize = $this->threshold_amazon_offers = $this->threshold_amazon_delete = $this->threshold_amazon_update = '20'; // mins
        $this->threshold_amazon_repricing_reprice = '2'; // mins // 21/07/2015
        $this->threshold_amazon_repricing_export = '30'; // mins // 21/07/2015
        $this->threshold_amazon_fba_estimated = '30'; // mins // 16/11/2015
        $this->threshold_amazon_fba_manager = '30'; // mins // 16/11/2015
        $this->threshold_amazon_fba_order_status = '30'; // mins // 16/11/2015

        $this->threshold_amazon_shipping_override = // mins // 16/11/2015
                $this->threshold_amazon_relations = // mins // 16/11/2015
                $this->threshold_amazon_images = '2'; // mins // 16/11/2015

        $this->threshold_ebay = '7'; // mins
        $this->threshold_play = '7'; // mins        
        $this->threshold_send_orders = '5'; // mins

        $this->threshold_mirakl_offers = '20'; // mins
        $this->threshold_mirakl_orders = '20'; // mins
        $this->threshold_mirakl_import_order = '20';
        $this->threshold_mirakl_accept_order = '0';
        $this->threshold_mirakl_products = '0';
        $this->threshold_mirakl_delete_products = '20';


        $this->load->model('paypal_model', 'paypal_model');
        $this->load->model("amazon_model", "amazon_model");
        $this->load->model("mirakl_model");

        $this->message_code_often = _get_icon_help('12-00001');
        $this->message_code_please_wait = _get_icon_help('12-00002');
        $this->message_code_access_denied = _get_icon_help('11-00001');
        $this->message_code_shop_not_found = _get_icon_help('11-00002');

        $this->spread = 4;
        $this->cron_user_list = array();
        $this->cron_user_id_list = array();
        $this->shop_name = array();

        $this->load->library('authentication');
        $func = $this->uri->segment(2);

//        $skip_all = array('test_read_xml_ebay','test_session');
//        if (!in_array($func, $skip_all)){    
//                $this->authentication->validateAjaxToken();   
//        }
        $this->id_user = $this->session->userdata('id');
        $user_data = $this->authentication->user($this->id_user)->row();
        $this->user_name = !empty($user_data->user_name) ? $user_data->user_name : '';
        $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
        $post = !empty($_POST) ? $_POST : '';
        $arr = array('time' => date('Y-m-d H:i:s'), 'uid' => $this->id_user, 'uname' => $this->user_name, 'uri' => $uri, 'post' => print_r($post, true), 'ref' => '');
        if (isset($_SERVER["HTTP_REFERER"])) {
            $arr['ref'] = $_SERVER['HTTP_REFERER'];
        }
        if (!empty($this->user_name)) {
            if (file_exists(USERDATA_PATH.$this->user_name.'/xml_log')) {
                //            file_put_contents(USERDATA_PATH.$this->user_name.'/xml_log/call_task.log',print_r($arr,true),FILE_APPEND);
                if (is_writable(USERDATA_PATH.$this->user_name.'/xml_log/call_tasks.csv')) {
                    $fp = fopen(USERDATA_PATH.$this->user_name.'/xml_log/call_tasks.csv', 'a');
                } else {
                    $fp = fopen(USERDATA_PATH.$this->user_name.'/call_tasks.csv', 'a');
                }
                fputcsv($fp, $arr);
                fclose($fp);
            }
        }
    }

    public function ajax_run_upload_product_options($id_shop, $shop_name, $id_country = null, $marketplace_id = null) {
        $id_user = $this->session->userdata('id');
        $user_data = $this->authentication->user($id_user)->row();

        if (!isset($user_data) || empty($user_data)) {
            echo json_encode(array('error_configuration' => true));
            die();
        }
        $data = array(
            'user_name' => $user_data->user_name,
            'id_shop' => $id_shop,
            'shopname' => $shop_name,
        );
        if (isset($id_country) && !empty($id_country)) {
            $data['id_country'] = $id_country;
        }

        if (isset($marketplace_id) && !empty($marketplace_id)) {
            $data['marketplace_id'] = $marketplace_id;
        }

        $feed_data = rawurlencode(json_encode($data));
        $url = $this->target_host.'/import/offers_options/'.$feed_data;
        if ($this->run) {
            return $this->do_post_request($url);
        }
        return;
    }

    public function ajax_run_upload_product_strategies($id_shop, $shop_name, $id_country) {
        $id_user = $this->session->userdata('id');
        $user_data = $this->authentication->user($id_user)->row();

        if (!isset($user_data) || empty($user_data)) {
            echo json_encode(array('error_configuration' => true));
            die();
        }
        $data = array(
            'user_name' => $user_data->user_name,
            'id_shop' => $id_shop,
            'shopname' => $shop_name,
            'id_country' => $id_country
        );

        $feed_data = rawurlencode(json_encode($data));
        $url = $this->target_host.'/import/product_strategies/'.$feed_data;
        if ($this->run) {
            return $this->do_post_request($url);
        }
        return;
    }

    public function ajax_get_message_code($code = '', $type = 'inner') {
        echo _get_message_code($code, $type);
    }

    public function ajax_get_icon_help($code = '', $type = 'inner') {
        echo _get_icon_help($code);
    }

    public function ajax_create_profiles($type = '') {
        $list = array(
            'product',
            'offer',
            'order'
        );
        if (!in_array($type, $list)) {
            echo 'Fail';
            die();
            return;
        }
        $this->id_user = $this->session->userdata('id');
        $user_data = $this->authentication->user($this->id_user)->row();
        $this->user_name = $user_data->user_name;
        $feed_data = urlencode(json_encode(array(
            'username' => $this->user_name,
            'id' => $this->id_user
        )));
        $url = 'create_profiles_'.$type.'_url';

        if ($this->run) {
            echo $this->do_post_request($this->$url.$feed_data);
        }
    }

    public function ajax_run_data_feed($type = 'feed') {
        $this->id_user = $this->session->userdata('id');
        $thres_hold_type = "threshold_".$type;
        if (empty($this->id_user)) {
            return;
        }
        $id = $this->id_user;
        $verified = $this->my_feed_model->check_verified_import($id);
        $avail_last_time = $this->my_feed_model->check_last_import($id, 'import_', $this->threshold_min);
        $avail_feed_time = $this->my_feed_model->check_last_import($id, 'import_'.$type, $this->$thres_hold_type);
        $json = array(
            'status' => false,
            'txt' => '',
            'result' => ''
        );
        if ($verified && $avail_last_time === true && $avail_feed_time === true) {
            $out = $this->run_import_by_node($id, $type, 'user');
            $json ['status'] = true;
            $url = 'import_'.$type.'_url';
            $url = $this->$url;
            $json ['result'] = $out;
            $json ['url'] = $url;
            $json ['txt'] = $this->lang->line('your_'.$type.'_are_importing');
        } elseif (!$verified) {
            $json ['txt'] = $this->lang->line('your_website_has_not_verified');
        } elseif ($avail_last_time !== true) {
            $min = ceil(abs($avail_last_time) / 60);
//             $json ['txt'] = str_replace('xxx', $min, $this->lang->line('you_have_update_too_often_wait_for_xxx_mins') . $this->message_code_often);
            $json ['txt'] = $this->lang->line('you_have_just_updated_your_feed_please_wait_for_a_few_minutes');
        } elseif ($avail_feed_time !== true) {
            $min = ceil(abs($avail_feed_time) / 60);
//             $json ['txt'] = str_replace('xxx', $min, $this->lang->line('you_have_just_update_' . $type . '_please_wait_for_xxx_mins') . $this->message_code_please_wait);
            $json ['txt'] = $this->lang->line('you_have_just_updated_your_feed_please_wait_for_a_few_minutes');
        }
        echo json_encode($json);

        exit();
    }

    public function get_list_avail_pk($id) {
        $out = $this->paypal_model->get_pk_not_expire($id);
        echo '<pre>';
        print_r($out [$id]);
    }

    public function check_running_process($market_type = 'amazon', $ext = '.com', $id_shop = null, $feed_type = 'products') {
        $json = array(
            'status' => false,
            'txt' => '',
            'result' => '',
            'url' => ''
        );

        $id_user = $this->session->userdata('id');
        $market_type = strtolower($market_type);
        $thres_hold_type = "threshold_".$market_type.'_'.$feed_type;

        if (!isset($id_user) || empty($id_user)) {
            //$json ['txt'] = sprintf("%s/s%s Access Denied.", basename(__FILE__), __LINE__); //'Please do not hack our website.';
            $json ['txt'] = $this->lang->line('Access Denied.').$this->message_code_access_denied; //'Please do not hack our website.';
            echo json_encode($json);
            exit();
        }

        $user_data = $this->authentication->user($id_user)->row();

        if ($id_shop === null) {
            $this->load->library('FeedBiz', array(
                $user_data->user_name
            ));
            $id_shop = $this->feedbiz->getDefaultShop();
            $id_shop = $id_shop ['id_shop'];
        }

        if (empty($id_shop)) {
            //$json ['txt'] = sprintf("%s/s%s Access Denied.", basename(__FILE__), __LINE__); 
            $json ['txt'] = $this->lang->line('Not found your shop.').$this->message_code_shop_not_found; //'Please do not hack our website.';
            //$json ['txt'] = 'Not found default shop';
            echo json_encode($json);
            exit();
        }

        $pk = $this->paypal_model->get_pk_by_site($market_type, $ext);
        $expire = $this->paypal_model->get_pk_not_expire($id_user, $market_type, $ext);

        if (empty($expire)) {
            $other = $this->check_free_trial($user_data, $market_type, $ext);
        } else {
            $other = array(
                'site_url' => $pk ['url']
            );
        }

        $other = array(
            'site_url' => $pk ['url']
        );
        $avail_last_time = $this->my_feed_model->check_last_import($id_user, 'export_', $this->threshold_min);
        $avail_feed_time = $this->my_feed_model->check_last_import($id_user, 'export_'.$other ['site_url'].'_'.$feed_type, $this->$thres_hold_type);

        if ($this->run && $avail_feed_time === true) {
            $json ['status'] = true;
        } elseif ($avail_last_time !== true) {
            $min = ceil(abs($avail_last_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have update to often. Please wait for %s mins.').$this->message_code_often, $min);
        } elseif ($avail_feed_time !== true) {
            $min = ceil(abs($avail_feed_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have just update your %s. Please wait for %s mins.').$this->message_code_please_wait, $this->lang->line($feed_type), $min);
        }

        echo json_encode($json);
        exit();
    }

    private function check_free_trial($user_data, $market_type, $ext) {
        if (self::FREETAIL > 0) {
            $created = strtotime($user_data->user_created_on);
            $expired = strtotime(date('Y-m-d', strtotime(self::FREETAIL.' month ago')));
            $diff = $created - $expired;

            if ($diff <= 0) {
                $json ['txt'] = sprintf("%s/s%s Please pay for this service.", basename(__FILE__), __LINE__);
                //$json ['txt'] = 'Please pay for this service.';
                $json ['url'] = 'service/package_payment/'.$market_type.'/'.$ext;
                echo json_encode($json);
                exit();
            }
        }

        $other = array();
        $other ['site_url'] = $market_type.$ext;
        return $other;
    }

    public function ajax_run_popup_market($market_type = 'amazon', $ext = '.com', $id_shop = null, $feed_type = 'products', $mode = null) {
        $json = array(
            'status' => false,
            'txt' => '',
            'result' => '',
            'url' => ''
        );
        $id_user = $this->session->userdata('id');

        $market_type = strtolower($market_type);
        //$thres_hold_type = "threshold_" . $market_type . '_' . $feed_type;

        if (!isset($id_user) || empty($id_user)) {
            $json ['txt'] = $this->lang->line('Access Denied.').$this->message_code_access_denied;
            echo json_encode($json);
            exit();
        }

        $user_data = $this->authentication->user($id_user)->row();

        if ($id_shop === null) {
            $this->load->library('FeedBiz', array(
                $user_data->user_name
            ));
            $shop = $this->feedbiz->getDefaultShop();
            $id_shop = $shop ['id_shop'];
        }

        if (empty($id_shop)) {
            $json ['txt'] = $this->lang->line('Not found your shop.').$this->message_code_shop_not_found;
            echo json_encode($json);
            exit();
        }

        $pk = $this->paypal_model->get_pk_by_site($market_type, $ext);
        $expire = $this->paypal_model->get_pk_not_expire($id_user, $market_type, $ext);

        if (empty($expire)) {
            $other = $this->check_free_trial($user_data, $market_type, $ext);
        } else {
            $other = array(
                'site_url' => $pk ['url']
            );
        }
        $freq = "frequency_update_offer_".$market_type;
        $min = $this->$freq;
        $next = array();
        $next['next_time'] = strtotime("+{$min} minutes");

        // $avail_feed_time = $this->my_feed_model->check_last_import($id_user, 'export_'.$other['site_url']. '_' . $feed_type, $this->$thres_hold_type);

        if ($this->run) {
            switch ($market_type) {
                case 'amazon' :
                    $this->my_feed_model->insert_last_export($id_user, $other ['site_url'], $feed_type, 'user', $next);
                    return $this->run_amazon_by_node($id_user, $ext, $id_shop, $feed_type, $mode);
                    break;
                case 'ebay' :
                    return $this->run_export_ebay_by_node();
                    break;
                default :
                    break;
            }
            $json ['status'] = true;
        }

        echo json_encode($json);
        exit();
    }

    public function ajax_run_send_orders($market_type, $id_marketplace, $ext, $site, $id_shop, $shop_name, $id_user, $cron_send_orders = 1) {
        $json = array(
            'status' => false,
            'txt' => '',
            'result' => '',
            'url' => ''
        );

        if (!isset($id_user) || empty($id_user)) {
            $id_user = $this->session->userdata('id');
        }

        if (empty($id_user)) {
            $json ['txt'] = 'User incorrect.';
            echo json_encode($json);
            exit();
        }

        $user_data = $this->authentication->user($id_user)->row();

        if (empty($user_data)) {
            $json ['txt'] = $this->lang->line('Access Denied.').$this->message_code_access_denied;
            echo json_encode($json);
            exit();
        }

        $market_type = strtolower($market_type);
        $thres_hold_type = "threshold_send_orders";

        if ($id_shop === null) {
            $this->load->library('FeedBiz', array(
                $user_data->user_name
            ));
            $shop = $this->feedbiz->getDefaultShop();
            $id_shop = $shop['id_shop'];
        }

        if (empty($id_shop)) {
            $json ['txt'] = $this->lang->line('Not found your shop.').$this->message_code_shop_not_found;
            echo json_encode($json);
            exit();
        }

        $pk = $this->paypal_model->get_pk_not_expire($id_user, $market_type, $ext);

        if (empty($pk)) {
            $other = $this->check_free_trial($user_data, $market_type, $ext);
        } else {
            $other = array();
            $other ['type'] = $pk ['offer_pkg_type'];
            $other ['pk_ext'] = $pk ['ext_offer_sub_pkg'];
            $other ['site'] = $pk ['name_offer_pkg'];
            $other ['site_url'] = $pk ['domain_offer_sub_pkg'];
            $other ['bill_info'] = $this->get_bill_info($pk);
        }

        $freq = "frequency_send_orders";
        $min = $this->$freq;
        $other['next_time'] = strtotime("+{$min} minutes");

        $verified = true;
        $cron_status = $this->my_feed_model->check_export_status($id_user, $market_type, $ext);
        $avail_last_time = $this->my_feed_model->check_last_import($id_user, 'send_orders_', $this->threshold_min);
        $avail_feed_time = $this->my_feed_model->check_last_import($id_user, 'send_orders_'.$market_type.$ext, $this->$thres_hold_type);

        if ($this->run && $verified && $avail_feed_time === true && $this->status_val[$cron_status]) {
            $data = array();
            $data['id_user'] = $id_user;
            $data['id_shop'] = $id_shop;
            $data['site'] = $site;
            $data['id_marketplace'] = $id_marketplace;
            $data['user_name'] = $user_data->user_name;
            $data['shop_name'] = $shop_name;
            $data['cron_send_orders'] = $cron_send_orders;

            $this->my_feed_model->insert_last_export($id_user, $other['site_url'], 'orders', 'user', $other);
            return $this->run_send_orders($data);
        } elseif (!$verified) {
            $json ['txt'] = $this->lang->line('your_website_has_not_verified');
        } elseif ($avail_last_time !== true) {
            $min = ceil(abs($avail_last_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have update to often. Please wait for %s mins.').$this->message_code_often, $min);
        } elseif ($avail_feed_time !== true) {
            $min = ceil(abs($avail_feed_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have just update your %s. Please wait for %s mins.').$this->message_code_please_wait, 'orders', $min);
        }

        echo json_encode($json);
        exit();
    }

    public function run_send_orders($user_data) {
        if (!isset($user_data) || empty($user_data)) {
            echo json_encode(array(
                'error_configuration' => true
            ));
            die();
        }
        $feed_data = rawurlencode(json_encode($user_data));
        $url = 'send_orders_url';

        if ($this->run) {
            return $this->do_post_request($this->$url.$feed_data);
        }

        return;
    }

    public function ajax_run_export($market_type = 'amazon', $ext = '.com', $id_shop = null, $feed_type = 'products', $id_user = null, $mode = null) {
        $json = array(
            'status' => false,
            'txt' => '',
            'result' => '',
            'url' => ''
        );

        if (!isset($id_user) || empty($id_user)) {
            $id_user = $this->session->userdata('id');
        }

        if (empty($id_user)) {
            $json ['txt'] = 'User incorrect.';
            echo json_encode($json);
            exit();
        }

        $user_data = $this->authentication->user($id_user)->row();

        if (empty($user_data)) {
            $json ['txt'] = $this->lang->line('Access Denied.').$this->message_code_access_denied;
            echo json_encode($json);
            exit();
        }

        $market_type = strtolower($market_type);
        $thres_hold_type = "threshold_".$market_type.'_'.$feed_type;

        if ($id_shop === null) {
            $this->load->library('FeedBiz', array(
                $user_data->user_name
            ));
            $shop = $this->feedbiz->getDefaultShop();
            $id_shop = $shop['id_shop'];
        }

        if (empty($id_shop)) {
            //$json ['txt'] = sprintf("%s/s%s Not found your shop.", basename(__FILE__), __LINE__);
            $json ['txt'] = $this->lang->line('Not found your shop.').$this->message_code_shop_not_found;
            echo json_encode($json);
            exit();
        }

        $pk = $this->paypal_model->get_pk_not_expire($id_user, $market_type, $ext);

        if (empty($pk)) {
            $other = $this->check_free_trial($user_data, $market_type, $ext);
        } else {
            $other = array();
            $other ['type'] = $pk ['offer_pkg_type'];
            $other ['pk_ext'] = $pk ['ext_offer_sub_pkg'];
            $other ['site'] = $pk ['name_offer_pkg'];
            $other ['site_url'] = $pk ['domain_offer_sub_pkg'];
            $other ['bill_info'] = $this->get_bill_info($pk);
        }

        $freq = "frequency_update_offer_".$market_type;
        $min = $this->$freq;

        $other['next_time'] = strtotime("+{$min} minutes");

        //$verified = $this->my_feed_model->check_verified_import($id_user);
        $verified = true;
        $cron_status = $this->my_feed_model->check_export_status($id_user, $market_type, $ext); // default  

        $avail_last_time = $this->my_feed_model->check_last_import($id_user, '%', $this->threshold_min);

        if ($feed_type == 'synchronize' && $market_type == 'amazon') {
            $avail_feed_time = $this->my_feed_model->check_last_import($id_user, '%'.$market_type.'%'.'_'.$feed_type, $this->$thres_hold_type);
        } else if ($market_type == 'mirakl') {

            require_once dirname(__FILE__).'/../libraries/Mirakl/classes/mirakl.history.log.php';
            $rawmode = json_decode(rawurldecode($mode)); // decode

            if ($feed_type == 'products') {
                $action_type = MiraklHistoryLog::ACTION_TYPE_EXPORT_PRODUCT;
            } else if ($feed_type == 'offers') {
                $action_type = MiraklHistoryLog::ACTION_TYPE_EXPORT_OFFER;
            } else if ($feed_type == 'delete_products') {
                $action_type = MiraklHistoryLog::ACTION_TYPE_DELETE_PRODUCT;
            } else if ($feed_type == 'accept_order') {
                $action_type = MiraklHistoryLog::ACTION_TYPE_ACCEPT_ORDER;
            } else if ($feed_type == 'import_order') {
                $action_type = MiraklHistoryLog::ACTION_TYPE_IMPORT_ORDER;
            } else {
                $action_type = '';
            }

            $other_action = $action_type.'_mirakl_'.$rawmode->sub_marketplace.'_'.$ext;  // import_orders_mirakl_13_.com
            $avail_feed_time = $this->my_feed_model->check_last_import($id_user, $other_action, $this->$thres_hold_type, 'user');
            //print_r($action_type); exit;
        } else {
            $avail_feed_time = $this->my_feed_model->check_last_import($id_user, '%'.$other ['site_url'].'_'.$feed_type, $this->$thres_hold_type);
        }

        if ($this->run && $verified && $avail_feed_time === true && $this->status_val [$cron_status]) {
            //if (1) {

            if ($market_type != 'mirakl') { // This function mirakl not need ins into histories, But ins into histories in the node exec. (MiraklHistoryLog::set($info))
                $this->my_feed_model->insert_last_export($id_user, $other ['site_url'], $feed_type, 'user', $other);
            }

            switch ($market_type) {
                case 'amazon' :
                    return $this->run_amazon_by_node($id_user, $ext, $id_shop, $feed_type, $mode);
                    break;
                case 'ebay' :
                    return $this->run_export_ebay_by_node($id_user, $ext, $id_shop, $feed_type, 'user');
                    break;
                case 'mirakl' :
                    //print_r('test'); exit;
                    return $this->run_mirakl_by_node($id_user, $ext, $id_shop, $feed_type, $rawmode);
                    break;
                default :
                    break;
            }
            $json ['status'] = true;
        } elseif (!$verified) {
            $json ['txt'] = $this->lang->line('your_website_has_not_verified');
        } elseif ($avail_last_time !== true) {
            $min = ceil(abs($avail_last_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have update to often. Please wait for %s mins.').$this->message_code_often, $min);
        } elseif ($avail_feed_time !== true) {
            $min = ceil(abs($avail_feed_time) / 60);
            $json ['txt'] = sprintf($this->lang->line('You have just update your %s. Please wait for %s mins.').$this->message_code_please_wait, $feed_type, $min);
        }

        echo json_encode($json);
        exit();
    }

    public function get_bill_info($pk) {
        $other = array();
        $other ['id_billing'] = $pk ['id_billing'];
        $other ['id_bill_detail'] = $pk ['id_bill_detail'];
        $other ['id_package'] = $pk ['id_package'];
        $other ['start_date_bill'] = $pk ['start_date_bill_detail'];
        $other ['end_date_bill'] = $pk ['end_date_bill_detail'];
        $other ['payment_method_billing'] = $pk ['payment_method_billing'];
        return $other;
    }

    public function run_amazon_by_node($id, $ext, $id_shop, $feed_type, $mode = null) {
        $market_data = $this->amazon_model->get_parameters($id, $ext, $id_shop);

        if (!isset($market_data) || empty($market_data)) {
            echo json_encode(array('error_configuration' => true));
            die();
        }

        if (isset($mode) && $mode == "update_offer") {
            $market_data['update_type'] = 'offer';
            $market_data['mode'] = 'sync';
        } else {
            $market_data['mode'] = $mode;
        }

        $market_data['shop_name'] = $this->session->userdata('shop_default') ? $this->session->userdata('shop_default') : '';

        if ($market_data ['active'] == 0) {
            $this->my_feed_model->update_history($id, 'amazon'.$ext, $feed_type, json_encode(array('active_status' => $market_data ['active'])));
            echo json_encode(array('active_status' => 'status inactive.'));
            die();
        }

        $feed_data = rawurlencode(json_encode($market_data));
        $url = 'export_amazon_'.$feed_type.'_url';

        if ($this->run) {
            return $this->do_post_request($this->$url.$feed_data);
        }

        return;
    }

    public function run_import_by_node($id, $type = 'feed', $name = 'system', $reqout = 0, $other = null) {
        if (is_numeric($id) && $id != 0) {
            $param = $this->my_feed_model->get_json_for_import($id);
            //echo '<pre>' . print_r($param, true) . '</pre>';
            $encode = urlencode($param);

            $url = 'import_'.$type.'_url';
            if ($name == 'system') {
                $thres_hold_type = "threshold_".$type;
                $avail_last_time = $this->my_feed_model->check_last_import($id, 'import_', $this->threshold_min);
                $avail_feed_time = $this->my_feed_model->check_last_import($id, 'import_'.$type, $this->$thres_hold_type);
                if (!($avail_last_time === true && $avail_feed_time === true)) {
                    return false;
                }
            }
            if (empty($other)) {
                $freq = "frequency_feed_".$type;
                $min = $this->$freq;
                if ($min) {
                    $other = array(
                        'next_time' => strtotime("+{$min} minutes")
                    );
                }
            }
            if ($this->run) {
                if ($name == 'system') {
                    $r = rand(300, 999);
                    usleep($r);
                }
                $this->my_feed_model->insert_last_import($id, $type, $name, $other);
                if ($reqout) {
                    echo $this->$url.$encode;
                }

                $out = $this->do_post_request($this->$url.$encode, null);
                $output = json_decode($out, true);
                if (!is_array($output)) {
                    $output = json_decode($output, true);
                }
                if (!empty($output) && is_array($output)) {
                    foreach ($output as $o) {
                        if ($o ['error'] == "Access Denied.") {
                            $this->my_feed_model->unverify_import($id);
                        }
                    }
                }
                if ($reqout) {
                    echo $out.'<br>';
                    print_r($output);
                }

                return $out;
            }
        }
        return false;
    }

    public function run_mirakl_by_node($id_user, $ext, $id_shop, $feed_type, $rawmode = '') {
        
        if (empty($rawmode)) {
            echo json_encode(array('status' => 0, 'txt' => 'No input mode.'));
            return;
        }

        if (!isset($rawmode->sub_marketplace) || empty($rawmode->sub_marketplace)) {
            echo json_encode(array('status' => 0, 'txt' => 'No input sub_marketplace.'));
            return;
        }

        if (!isset($rawmode->id_country) || empty($rawmode->id_country)) {
            echo json_encode(array('status' => 0, 'txt' => 'No input id_country.'));
            return;
        }

        $market_data = array();
        $market_data['id_user'] = $id_user;
        $market_data['id_shop'] = $id_shop;
        $market_data['sub_marketplace'] = $rawmode->sub_marketplace;
        $market_data['id_country'] = $rawmode->id_country;

        if ($feed_type === 'offers') {
            $market_data['export_all'] = $rawmode->export_all;
            $market_data['purge'] = $rawmode->purge;
            $market_data['send_mirakl'] = $rawmode->send_mirakl;
        } elseif ($feed_type === 'import_order') {
            $market_data['date_start'] = $rawmode->date_start;
            $market_data['import_status'] = $rawmode->import_status;
        } elseif ($feed_type === 'products') {
            $market_data['in_stock'] = $rawmode->in_stock;
            $market_data['send_mirakl'] = $rawmode->send_mirakl;
        } elseif ($feed_type === 'accept_order') {
            $market_data['order_id'] = $rawmode->order_id;
        } else {
            // no parameter
        }
        
        //echo '<pre>'; print_r($market_data); exit;

        $feed_data = rawurlencode(json_encode($market_data));
        $url = 'export_mirakl_'.$feed_type.'_url';

        //echo '<pre>'; print_r($this->$url . $feed_data); exit;

        if ($this->run) {
            print_r($this->do_post_request($this->$url.$feed_data));
        }
    }

    private function do_post_request($url, $data = array(), $optional_headers = null, $getresponse = false) {
        if ($this->aync) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
            curl_exec($ch);
            curl_close($ch);
            // $params = array('http' => array(
            // 'method' => 'POST',
            // 'content' => $data
            // ));
            // if ($optional_headers !== null) {
            // $params['http']['header'] = $optional_headers;
            // }
            // $ctx = stream_context_create($params);
            // $fp = fopen($url, 'rb', false, $ctx);
            // if (!$fp) {
            // return false;
            // }
            // if ($getresponse){
            //
                    // $response = stream_get_contents($fp);
            // return $response;
            // }
            return true;
        } else {
            //$start_post = microtime();
            //echo 'start :'.$start_post."<br>\n\n";
            //echo $url."<br>\n\n";
            ob_start();
            $ctx = stream_context_create(array(
                'http' => array(
                    'timeout' => 600
                )
                    )
            );
            $start_time = date('Y-m-d H:i:s');
            echo file_get_contents($url, 0, $ctx);

            // var_dump(file_get_contents($url)); exit;
            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL,$url);
            // //curl_setopt($ch, CURLOPT_POST, 1);
            // //curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($data)?$data:array() );
            // //curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            // curl_exec ($ch);
            // curl_close ($ch);
            $output = ob_get_contents();
            ob_end_clean();

            $uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
            $post = !empty($_POST) ? $_POST : '';
            $arr = array('time' => date('Y-m-d H:i:s'), 'uid' => $this->id_user, 'uri' => $uri, 'post' => print_r($post, true), 'ref' => '', 'node' => $url, 'start_time' => $start_time, 'output' => $output);
            if (isset($_SERVER["HTTP_REFERER"])) {
                $arr['ref'] = $_SERVER['HTTP_REFERER'];
            }
            if (!empty($this->user_name)) {
                if (file_exists(USERDATA_PATH.$this->user_name.'/xml_log')) {
                    //            file_put_contents(USERDATA_PATH.$this->user_name.'/xml_log/call_task.log',print_r($arr,true),FILE_APPEND);
                    if (is_writable(USERDATA_PATH.$this->user_name.'/xml_log/call_tasks2node.csv')) {
                        $fp = fopen(USERDATA_PATH.$this->user_name.'/xml_log/call_tasks.csv', 'a');
                    } else {
                        $fp = fopen(USERDATA_PATH.$this->user_name.'/call_tasks2node.csv', 'a');
                    }
                    fputcsv($fp, $arr);
                    fclose($fp);
                }
            }
            // echo 'end :'. (microtime()-$start_post) . ' ' .microtime()."<br>\n\n";
            return $output;
        }
    }

    // process popup template//
    public function display_popup_template($marketplace, $str = '') {
        if (empty($str))
            return;
        $encode_message = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($str)), null, 'UTF-8');
        $message = json_decode($encode_message, true);
        $data['message'] = $message;

        switch ($marketplace) {
            case "amazon" :
                $this->smarty->view('amazon/PopupProcess.tpl', $data);
                break;
            default :
                $this->smarty->view('PopupProcess.tpl', $data);
                break;
        }
    }

    public function test_task($id = 0) {
        $params = array(
            'username' => 'u00000000000003',
            'userid' => 3,
            'siteid' => 0,
            'method' => 'features',
            'link' => 'products'
        );
        echo urlencode(json_encode($params));
    }

    public function run_export_ebay_by_node() {
        if (!isset($_POST ['siteid']) || !isset($_POST ['method']) || !isset($_POST ['link'])) {
            die('fail');
        }

        $params = array(
            'username' => $this->session->userdata('user_name'),
            'userid' => $this->session->userdata('id'),
            'siteid' => $_POST ['siteid'],
            'method' => $_POST ['method'],
            'link' => $_POST ['link']
        );

        $freq = "frequency_update_offer_ebay";
        $min = $this->$freq;
        $next = array();
        $next['next_time'] = strtotime("+{$min} minutes");
        $res = $this->my_feed_model->getDomainEbayByIDSite($_POST['siteid']);
        if (isset($res[0]['domain'])) {
            $site_url = $res[0]['domain'];
            $this->my_feed_model->insert_last_export($this->session->userdata('id'), $site_url, $_POST ['link'], 'user', $next);
        }

        $url = $this->export_ebay_product_url.urlencode(json_encode($params));
        echo $this->do_post_request($url, $params);
    }

    public function run_import_order_from_site($id_user, $name = 'system', $other = null) {
        //INITIAL
        $user_data = $this->authentication->user($id_user)->row();
        if (empty($user_data)) {
            return;
        }

        $user_name = $user_data->user_name;

//        require_once DIR_SERVER . '/application/libraries/FeedBiz_Orders.php';
//        require_once DIR_SERVER . '/application/libraries/Ebay/ObjectBiz.php';
//        require_once DIR_SERVER . '/application/libraries/Ebay/ebayshipment.php';        
//        require_once DIR_SERVER . '/application/libraries/Amazon/functions/amazon.status.orders.php';
//
//        $ebayObjectBiz = new ObjectBiz(array($user_name));
//        $feedBizOrder = new FeedBiz_Orders(array($user_name));
//        $ebayCarriers = $ebayObjectBiz->getCarriers();
//        //INITIAL END
//        
//        //retrieve setting + connector
//        $orders = array();
//        $order_ids = array();
        $config_data = $this->my_feed_model->get_general($id_user);
        if (isset($config_data ['FEED_SHOP_INFO'])) {
            $feed = unserialize(base64_decode($config_data ['FEED_SHOP_INFO'] ['value']));
            $fbtoken = $this->my_feed_model->get_token($id_user);

            if (isset($feed ['url'] ['shippedorders'])) {
                if (empty($other)) {
                    $freq = "frequency_feed_order";
                    $min = $this->$freq;
                    $other = array(
                        'next_time' => strtotime("+{$min} minutes")
                    );
                }
                if ($name == 'system') {
                    $r = rand(300, 999);
                    usleep($r);
                }
                $this->my_feed_model->insert_last_import($id_user, 'order', $name, $other);
                $shippedorder_url = $feed ['url'] ['shippedorders'];
                $conj = strpos($shippedorder_url, '?') !== false ? '&' : '?';
                $encode = urlencode(json_encode(array('user_id' => $id_user, 'user_name' => $user_name, 'xml_url' => $shippedorder_url.$conj.'fbtoken='.$fbtoken)));
                $url = "import_shop_order_url";
                $out = $this->do_post_request($this->$url.$encode, null);
//                $xml_string = file_get_contents($shippedorder_url . $conj . 'fbtoken=' . $fbtoken);
//
//
//                // validate xml
//                libxml_use_internal_errors(true);
//                $xml_doc = simplexml_load_string($xml_string);
//
//                // load dom
//                if ($xml_doc) {
//                    $xml_dom = new SimpleXMLElement($xml_string);
//                    foreach ($xml_dom->Orders->Order as $orderDOM) {
//                        $orders [strval($orderDOM->MPOrderID)] = array(
//                            'MPOrderID' => strval($orderDOM->MPOrderID),
//                            'CarrierID' => strval($orderDOM->CarrierID),
//                            'CarrierName' => strval($orderDOM->CarrierName),
//                            'ShippingNumber' => strval($orderDOM->ShippingNumber),
//                            'ShippingDate' => strval($orderDOM->ShippingDate)
//                        );
//                        $order_ids [] = strval($orderDOM->MPOrderID);
//                    }                    
//                    
//                    if (!empty($orders)) {               
//                        $ordersNoTracking = $feedBizOrder->getOrdersNoTracking($user_name, $order_ids);
//                        
//                        // SQLITE
//                        $feedBizOrder->updateOrderTracking($user_name, $orders);
//                        // LOG
//                        $current = new DateTime();
//                        $logs = array();
//                        foreach($ordersNoTracking as $oid=>$orderNoTracking){
//                        	$logs[] = array('batch_id'=>$orderNoTracking['id_marketplace_order_ref'],
//		                        			'request_id'=>$orderNoTracking['id_orders'],
//		                        			'error_code'=>0,
//		                        			'message'=>'Update tracking number: '.$orders[$oid]['ShippingNumber'].' Shipped Date: '.$orders[$oid]['ShippingDate'],
//		                        			'date_marketplace'=>$orders[$oid]['ShippingDate'],
//		                        			'date_add'=>$current->format('Y-m-d H:i:s')
//                        	);
//                        }
//                        $feedBizOrder->insertLog($user_name, $logs);
//                        // LOG END
//                        //SQLITE END
//                    }
//                }
            }
        }



//        //MP
//        $current = new DateTime();
//        $raworders = $feedBizOrder->getOrdersForShip($user_name);
//        $logs = array();
//        $done = array();
//        $orderMPMapping = array();
//        $shipments = array();
//        foreach ($raworders as $channel=>$sites) {
//        	//EBAY
//        	if (strpos(strtolower($channel), 'ebay') !== FALSE) {
//        		foreach($sites as $site_id => $site_orders){
//        			foreach($site_orders as $order){
//        				$id_shop = $order['id_shop'];
//        				$id_carrier = $order['id_carrier'];
//        				$shipments[] = array('OrderID' => $order['id_marketplace_order_ref'],
//        				// 	                        					'OrderLineItemID' => $order['id_marketplace_order_ref'],
//        						'ShipmentTrackingNumber' => $order['tracking_number'],
//        						'ShippedTime' => $order['shipping_date'],
//        						'ShippingCarrierUsed' => isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz'
//        				);
//        				$orderMPMapping[$order['id_marketplace_order_ref']] = $order;
//        			}
//        			 
//        			//API
//        			if($shipments){
//	        			$ebayshipment = new ebayshipment(array($user_name, $id_user, $site_id));
//	        			$ebayshipment->export_shipment($shipments);
//	        			$ebayshipment->export_bulk();
//	        			$rs = $ebayshipment->export_download();
//	        			 
//	        			//Response
//	        			if($rs){
//	        				foreach($rs as $orderResponse){
//	        					$Ack = strval($orderResponse->Ack);
//	        					$OrderID = strval($orderResponse->OrderID);
//	        					$raworder = isset($orderMPMapping[$OrderID]) ? $orderMPMapping[$OrderID] : null;
//	        					$id_shop = $raworder['id_shop'];
//	        					$id_carrier = $raworder['id_carrier'];
//	        					$carrier_name = isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz';
//	        					
//	        					if($raworder){
//	        						if(strtolower($Ack) == 'success'){
//	        							$logs[] = array('batch_id'=>$OrderID,
//	        									'request_id'=>intval($orderMPMapping[$OrderID]['id_orders']),
//	        									'error_code'=>0,
//	        									'message'=>'Ebay SetShipmentTrackingInfo: '.$raworder['tracking_number'].' Shipped Date: '.$raworder['shipping_date'],
//	        									'date_marketplace'=>$raworder['shipping_date'],
//	        									'date_add'=>$current->format('Y-m-d H:i:s')
//	        							);
//	        							$done[] = intval($orderMPMapping[$OrderID]['id_orders']);
//	        						}
//	        						else{
//	        							$error_messages = array();
//	        							foreach($orderResponse->Errors as $errDOM)
//	        							{
//	        								$error_messages[] = strval($errDOM->ErrorCode).': '.strval($errDOM->LongMessage);
//	        							}
//	        							$logs[] = array('batch_id'=>$OrderID,
//	        									'request_id'=>intval($orderMPMapping[$OrderID]['id_orders']),
//	        									'error_code'=>1,
//	        									'message'=>'Ebay SetShipmentTrackingInfo: '.implode('<br/>', $error_messages),
//	        									'date_marketplace'=>$raworder['shipping_date'],
//	        									'date_add'=>$current->format('Y-m-d H:i:s')
//	        							);
//	        						}
//	        					}
//	        				}
//	        			}
//        			}
//        		}
//        	}
//        }
//        
//        //SQLITE
//        $feedBizOrder->insertLog($user_name, $logs);
//        $feedBizOrder->updateShippedStatus($user_name, $done);
//        //SQLITE END
//        //MP END
    }

    public function shipped_order() {
        $this->run_import_order_from_site(35);
        exit;
        $users = $this->my_feed_model->get_user_conj_feed_active();
        foreach ($users as $u) {
            $this->run_import_order_from_site($u ['id']);
        }
    }

    public function get_message_help($code) {
        echo json_encode(array('info' => _get_message_help($code)));
    }

    public function shipped_order_ebay_upload() {
        $id_user = 35;
        $name = 'system';

        $user_data = $this->authentication->user($id_user)->row();
        if (empty($user_data)) {
            return;
        }

        require_once DIR_SERVER.'/application/libraries/FeedBiz_Orders.php';
        require_once DIR_SERVER.'/application/libraries/Ebay/ObjectBiz.php';
        require_once DIR_SERVER.'/application/libraries/Ebay/ebayshippings.php';
        require_once DIR_SERVER.'/application/libraries/Ebay/ebayshipment.php';
        require_once DIR_SERVER.'/application/libraries/Amazon/functions/amazon.status.orders.php';

        $user_name = $user_data->user_name;
        $orders = array();
        $order_ids = array();
        $shipments = array();
        $config_data = $this->my_feed_model->get_general($id_user);
        if (isset($config_data ['FEED_SHOP_INFO'])) {
            //$feed_biz = unserialize ( base64_decode ( $config_data ['FEED_BIZ'] ['value'] ) );
            $feed = unserialize(base64_decode($config_data ['FEED_SHOP_INFO'] ['value']));
            $fbtoken = $this->my_feed_model->get_token($id_user);

            if (isset($feed ['url'] ['shippedorders'])) {
                if (empty($other)) {
                    $freq = "frequency_feed_order";
                    $min = $this->$freq;
                    $other = array(
                        'next_time' => strtotime("+{$min} minutes")
                    );
                }
                if ($name == 'system') {
                    $r = rand(300, 999);
                    usleep($r);
                }
                $this->my_feed_model->insert_last_import($id_user, 'order', $name, $other);
                $shippedorder_url = $feed ['url'] ['shippedorders'];
                $conj = strpos($shippedorder_url, '?') !== false ? '&' : '?';
                $xml_string = file_get_contents($shippedorder_url.$conj.'fbtoken='.$fbtoken);

                // validate xml
                libxml_use_internal_errors(true);
                $xml_doc = simplexml_load_string($xml_string);

                // load dom
                if ($xml_doc) {
                    $xml_dom = new SimpleXMLElement($xml_string);
                    foreach ($xml_dom->Orders->Order as $orderDOM) {
                        $orders [strval($orderDOM->MPOrderID)] = array(
                            'MPOrderID' => strval($orderDOM->MPOrderID),
                            'CarrierID' => strval($orderDOM->CarrierID),
                            'CarrierName' => strval($orderDOM->CarrierName),
                            'ShippingNumber' => strval($orderDOM->ShippingNumber),
                            'ShippingDate' => strval($orderDOM->ShippingDate)
                        );
                        $order_ids [] = strval($orderDOM->MPOrderID);
                    }
                }

                // DEBUG
//                 $orders = array(
//                     '1' => array(
//                         'MPOrderID' => '1',
//                         'CarrierID' => '1',
//                         'CarrierName' => 'Test Carrier',
//                         'ShippingNumber' => 'ABC20150115164052',
//                         'ShippingDate' => '2015-01-15 16:40:52'
//                     ),
//                     '3' => array(
//                         'MPOrderID' => '3',
//                         'CarrierID' => '1',
//                         'CarrierName' => 'Test Carrier',
//                         'ShippingNumber' => 'ABC20150115164052',
//                         'ShippingDate' => '2015-01-15 16:40:52'
//                     )
//                 );
//                 $order_ids = array(
//                     '1', '2', '3'
//                 );
                //ENDDEBUG

                if (!empty($orders)) {
                    $ebayObjectBiz = new ObjectBiz(array($user_name));
                    $feedBizOrderModel = new FeedBiz_Orders(array($user_name));
                    $raw_orders = $feedBizOrderModel->getOrdersNoTracking($user_name, $order_ids); //***RETREIVE BEFORE UPDATE BASE
                    print_r($raw_orders);
                    exit;
                    // update order tracking in SQLITE
//     				$ebayObjectBiz->shipOrder($user_name, $orders);
                    foreach ($orders as $order) {
                        $datetime = new DateTime($order['ShippingDate']);
                        $oid = $order['MPOrderID'];

                        if (isset($raw_orders[$oid])) {
                            $data_insert = false;
                            $raw_order = $raw_orders[$oid];

                            // update ebay
                            if (strpos(strtolower($raw_order['sales_channel']), 'ebay') !== false) {
                                $shipments[$raw_order['site']][] = array('OrderID' => $raw_order['id_marketplace_order_ref'],
                                    'OrderLineItemID' => $raw_order['id_marketplace_order_item_ref'],
                                    'ShipmentTrackingNumber' => $order['ShippingNumber'],
                                    'ShippedTime' => $datetime->format('c'),
                                    'ShippingCarrierUsed' => $order['CarrierName']
                                );
                            }
                        }
                    }
                }
            }
        }

        foreach ($shipments as $site_id => $shipments_list) {
            $shipments = array();
            $ebayshipment = new ebayshipment(array($user_name, $id_user, $site_id));
            $ebayshipment->export_shipment($shipments_list);
            $ebayshipment->export_bulk();
            $ebayshipment->export_download();
        }
        echo 'done';
    }

    public function ajax_get_next_time() {
        $post = $this->input->post();
        if (isset($post['process_key']) && !empty($post['process_key'])) {
            $this->load->model('cron_model', 'cron');
            $next_time = $this->cron->getTimeLastCronRun($this->id_user, $post['process_key']);
            if ($next_time <= time()) {
                $next_time = time();
            }
            echo $next_time - time();
            return;
        }
        echo '20';
    }

}
