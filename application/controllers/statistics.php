<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class statistics extends CI_Controller
{
    
    public $id_user;
    public $user_name;
    public $iso_code;
    public $id_mode;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->user_name = $this->session->userdata('user_name');
        $this->id_user = $this->session->userdata('id');
        $this->id_shop = $this->session->userdata('id_shop');
        $this->id_mode = $this->session->userdata('mode_default');
        
//        if( (!isset($this->user_name) || empty($this->user_name )) &&
//        (!isset($this->id_user) || empty($this->id_user)) )
//            redirect('users/login');

        //1. library
        $this->load->library('authentication');
        $this->authentication->checkAuthen();//check logged in and redirect to login page
        $this->load->library('form_validation');
        $this->load->library('FeedBiz', array($this->user_name));
        //$this->load->library('FeedBiz_Offers', array($this->user_name));

        //load database
        $this->load->database();
        
        //load helper
        $this->load->helper('form');
        $this->load->helper('html');
                                
        //set language
        $this->language = $this->session->userdata('language') ?
            $this->session->userdata('language') : $this->config->item('language');
        $this->smarty->assign("lang", $this->language);
        $this->lang->load("my_feeds", $this->language);
        $this->smarty->assign("label", "my_feeds");
        $this->iso_code = mb_substr($this->language, 0, 2);
    }
       
    public function get_product_qty()
    {
        $date = date('Ymd_His');
      
        $list = array();
        $result = $this->feedbiz->getProductList($this->id_shop);
        
        //var_dump($result); exit;
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['reference'] = "reference";
        $list['header']['quantity'] = "quantity";
        //$list['header']['price'] = "price";

        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $data) {
                $list[$key]['id_product'] = $data['id_product'];
                $list[$key]['id_product_attribute'] = 
                    isset($data['id_product_attribute']) ?
                    $data['id_product_attribute'] : null;
                $list[$key]['reference'] = $data['reference'];
                $list[$key]['quantity'] = $data['quantity'];
                //$list[$key]['price'] = $data['price'];               
            }
        }

        ob_start();
        ob_get_clean();
        $filename = 'feedbiz_offer_' . $date . '.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) {
            fputcsv($h, $data, ';');
        }
    }

    
    public function manufacturers()
    {
        if ($this->authentication->logged_in() && isset($this->session->userdata['id'])) {
            if ($this->session->userdata('error')) {
                $data['error'] = $this->lang->line($this->session->userdata('error'));
                $this->session->unset_userdata('error');
            }

            if ($this->session->userdata('message')) {
                $data['message'] = $this->lang->line($this->session->userdata('message'));
                $this->session->unset_userdata('message');
            }
            
            $this->smarty->view('statistics/manufacturers.tpl', $data);
        } else {
            redirect('users/login');
        }
    }
    

     
    
    ///////////////////////
    public function ebay_getRootCategories()
    {
        $root = array();
        
        $this->db->from('ebay_categories');
        $this->db->where('is_root_category', '1');
        $query = $this->db->get();
        
        $result = $query->result();
        
        foreach ($result as $value) {
            $root[] =  $value->id_category;
        }
        
        return $root;
    }
     

    public function ebay_getSubCategories($id_parent, $stie)
    {
        $categories = array();
        
        $this->db->select('*');
        $this->db->from('ebay_categories');
        $this->db->join('ebay_categories_sites', 'ebay_categories.id_category = ebay_categories_sites.id_category');
        $this->db->where('id_parent', $id_parent);
        $this->db->where('id_site', $stie);
        $query = $this->db->get();
        $result = $query->result();
        
        foreach ($result as $category) {
            //            echo '<pre>' . print_r($category->id_category, true) . '<pre>';
            $categories[$category->id_category]['id'] = $category->id_category;
            $categories[$category->id_category]['name'] = $category->name;
            $categories[$category->id_category]['child'] = $this->ebay_getSubCategories($category->id_category, $stie);
        }
          
        return $categories;
    }
    ///////////////////
    
    public function categories()
    {
        $data['uri'] = $this->uri->segment(1);
        $data['page_name'] = $this->router->method;

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('statistics/categories.tpl', $data);
    }
    
    public function suppliers()
    {
        $data['uri'] = $this->uri->segment(1);
        $data['page_name'] = $this->router->method;

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('statistics/suppliers.tpl', $data);
    }
    
    public function carriers()
    {
        $data['uri'] = $this->uri->segment(1);
        $data['page_name'] = $this->router->method;

        if ($this->session->userdata('error')) {
            $data['error'] = $this->lang->line($this->session->userdata('error'));
            $this->session->unset_userdata('error');
        }

        if ($this->session->userdata('message')) {
            $data['message'] = $this->lang->line($this->session->userdata('message'));
            $this->session->unset_userdata('message');
        }

        $this->smarty->view('statistics/carriers.tpl', $data);
    }
     
}
