<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class DoDirectPayment
{
    private $paypalConfig;
    private $PayPal;
    private $DPFields, $CCDetails, $PayerInfo, $PayerName, $BillingAddress, $ShippingAddress, $PaymentDetails;
    private $isShipping, $taxrate, $subtotal, $tax, $total;
    private $Product = array();
    private $PaypalResult;
    
    function __construct($config_pay)
    {        
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
        
        // init Shipping
        $this->isShipping = false;
        
        // Set Taxrate
        if($config_pay['taxrate']>0) $this->taxrate = $config_pay['taxrate'];
        else $this->taxrate = 0;
        
        $this->DPFields = array(
            'paymentaction' => 'Sale',
            'ipaddress' => $config_pay['device_ip_address'],
            'returnfmfdetails' => '1',
        );
        
        $this->CCDetails = array(
            'creditcardtype' => '',
            'acct' => '',
            'expdate' => '',
            'cvv2' => '',
            'startdate' => '',
            'issuenumber' => '',
        );

        $this->PayerInfo = array(
            'email' => '',
            'payerid' => '',
            'payerstatus' => '',
            'business' => '',
        );

        $this->PayerName = array(
            'salutation' => '',
            'firstname' => '',
            'middlename' => '',
            'lastname' => '',
            'suffix' => '',
        );

        $this->BillingAddress = array(
            'street' => '',
            'street2' => '',
            'city' => '',
            'state' => '',
            'countrycode' => '',
            'zip' => '',
            'phonenum' => '',
        );

        $this->ShippingAddress = array(
            'shiptoname' => '',
            'shiptostreet' => '',
            'shiptostreet2' => '',
            'shiptocity' => '',
            'shiptostate' => '',
            'shiptozip' => '',
            'shiptocountrycode' => '',
            'shiptophonenum' => '',
        );
        
        $this->PaymentDetails = array(
            'amt' => '',
            'currencycode' => $config_pay['currencycode'],
            'itemamt' => '',
            'shippingamt' => '',
            'handlingamt' => '',
            'taxamt' => '',
            'desc' => '',
            'custom' => '',
            'invnum' => '',
            'buttonsource' => $config_pay['buttonsource'],
            'notifyurl' => '',
        );
    }
    
    public function setDPFields($DPFields)
    {
        if(isset($DPFields['paymentaction'])) $this->DPFields['paymentaction'] = $DPFields['paymentaction'];
        if(isset($DPFields['ipaddress'])) $this->DPFields['ipaddress'] = $DPFields['ipaddress'];
        if(isset($DPFields['returnfmfdetails'])) $this->DPFields['returnfmfdetails'] = $DPFields['returnfmfdetails'];
    }
    
    public function setProduct($arrProduct)
    {
        $product = array(
            'l_name' => $arrProduct['name'],
            'l_desc' => $arrProduct['desc'],
            'l_amt' => $arrProduct['amt'],
            'l_number' => $arrProduct['number'],
            'l_qty' => $arrProduct['qty'],
            'l_taxamt' => $arrProduct['taxamt'],
        );
        $this->Product[] = $product;
    }
    
    public function setPayment($paymentData)
    {        
        // Set CreditCard Detail
        if(isset($paymentData['cctype'])) $this->CCDetails['creditcardtype'] = $paymentData['cctype'];
        if(isset($paymentData['ccnum'])) $this->CCDetails['acct'] = $paymentData['ccnum'];
        if(isset($paymentData['ccexp'])) $this->CCDetails['expdate'] = $paymentData['ccexp'];
        if(isset($paymentData['cvv2'])) $this->CCDetails['cvv2'] = $paymentData['cvv2'];
        if(isset($paymentData['startdate'])) $this->CCDetails['startdate'] = $paymentData['startdate'];
        if(isset($paymentData['issuenumber'])) $this->CCDetails['issuenumber'] = $paymentData['issuenumber'];
        
        // Set Payer Info
        if(isset($paymentData['email'])) $this->PayerInfo['email'] = $paymentData['email'];
        if(isset($paymentData['payerid'])) $this->PayerInfo['payerid'] = $paymentData['payerid'];
        if(isset($paymentData['payerstatus'])) $this->PayerInfo['payerstatus'] = $paymentData['payerstatus'];
        if(isset($paymentData['business'])) $this->PayerInfo['business'] = $paymentData['business'];
        
        // Set Payer Name
        if(isset($paymentData['salutation'])) $this->PayerName['salutation'] = $paymentData['salutation'];
        if(isset($paymentData['firstname'])) $this->PayerName['firstname'] = $paymentData['firstname'];
        if(isset($paymentData['middlename'])) $this->PayerName['middlename'] = $paymentData['middlename'];
        if(isset($paymentData['lastname'])) $this->PayerName['lastname'] = $paymentData['lastname'];
        if(isset($paymentData['suffix'])) $this->PayerName['suffix'] = $paymentData['suffix'];
        
        // Set Billing Address
        if(isset($paymentData['street'])) $this->BillingAddress['street'] = $paymentData['street'];
        if(isset($paymentData['street2'])) $this->BillingAddress['street2'] = $paymentData['street2'];
        if(isset($paymentData['city'])) $this->BillingAddress['city'] = $paymentData['city'];
        if(isset($paymentData['state'])) $this->BillingAddress['state'] = $paymentData['state'];
        if(isset($paymentData['countrycode'])) $this->BillingAddress['countrycode'] = $paymentData['countrycode'];
        if(isset($paymentData['zip'])) $this->BillingAddress['zip'] = $paymentData['zip'];
        if(isset($paymentData['phonenum'])) $this->BillingAddress['phonenum'] = $paymentData['phonenum'];  
        
        // Set Payment Detail
        if(isset($paymentData['currencycode'])) $this->PaymentDetails['currencycode'] = $paymentData['currencycode'];
        if(isset($paymentData['shippingamt'])) $this->PaymentDetails['shippingamt'] = $paymentData['shippingamt'];
        if(isset($paymentData['handlingamt'])) $this->PaymentDetails['handlingamt'] = $paymentData['handlingamt'];
        if(isset($paymentData['desc'])) $this->PaymentDetails['desc'] = $paymentData['desc'];
        if(isset($paymentData['custom'])) $this->PaymentDetails['custom'] = $paymentData['custom'];
        if(isset($paymentData['invnum'])) $this->PaymentDetails['invnum'] = $paymentData['invnum'];
        if(isset($paymentData['notifyurl'])) $this->PaymentDetails['notifyurl'] = $paymentData['notifyurl'];
    }
    
    public function setShippingStatus($isShipping)
    {
        $this->isShipping = $isShipping;
    }
    
    public function setShipping($shippingData)
    {
        if(isset($shippingData['shiptoname'])) $this->ShippingAddress['shiptoname'] = $shippingData['shiptoname'];
        if(isset($shippingData['shiptostreet'])) $this->ShippingAddress['shiptostreet'] = $shippingData['shiptostreet'];
        if(isset($shippingData['shiptostreet2'])) $this->ShippingAddress['shiptostreet2'] = $shippingData['shiptostreet2'];
        if(isset($shippingData['shiptocity'])) $this->ShippingAddress['shiptocity'] = $shippingData['shiptocity'];
        if(isset($shippingData['shiptostate'])) $this->ShippingAddress['shiptostate'] = $shippingData['shiptostate'];
        if(isset($shippingData['shiptozip'])) $this->ShippingAddress['shiptozip'] = $shippingData['shiptozip'];
        if(isset($shippingData['shiptocountrycode'])) $this->ShippingAddress['shiptocountrycode'] = $shippingData['shiptocountrycode'];
        if(isset($shippingData['shiptophonenum'])) $this->ShippingAddress['shiptophonenum'] = $shippingData['shiptophonenum'];
    }
    
    public function doTransaction()
    {
        // Summary Subtotal
        $this->subtotal = 0;
        if(sizeof($this->Product)<=0) return;
        foreach($this->Product as $val)
        {
            $this->subtotal += $val['l_amt']*$val['l_qty'];
        }
        
        // Set Subtotal Tax
        $this->tax = 0;
        if($this->taxrate > 0) $this->tax = $this->subtotal*$this->taxrate/100;
        
        // Set Total Amount
        $this->total = $this->subtotal;
        if($this->tax > 0) $this->total += $this->tax;
        if(isset($this->PaymentDetails['shippingamt'])) $this->total += $this->PaymentDetails['shippingamt'];
        if(isset($this->PaymentDetails['handlingamt'])) $this->total += $this->PaymentDetails['handlingamt'];
        
        // Set PaymentDetails subtotal, total, tax
        $this->PaymentDetails['amt'] = number_format($this->total,2);
        $this->PaymentDetails['itemamt'] = number_format($this->subtotal,2);
        $this->PaymentDetails['taxamt'] = number_format($this->tax,2);
        
        $PayPalRequestData = array(
            'DPFields' => $this->DPFields, 
            'CCDetails' => $this->CCDetails,
            'PayerInfo' => $this->PayerInfo,
            'PayerName' => $this->PayerName, 
            'BillingAddress' => $this->BillingAddress, 
            'PaymentDetails' => $this->PaymentDetails, 
            'OrderItems' => $this->Product,
        );
        if($this->isShipping) $PayPalRequestData['ShippingAddress'] = $this->ShippingAddress;
        $this->PaypalResult = $this->PayPal->DoDirectPayment($PayPalRequestData);
        return $this->PaypalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PaypalResult;
    }
    
    public function getAmount()
    {
        return array(
            'taxrate' => $this->taxrate,
            'subtotal' => $this->subtotal,
            'tax' => $this->tax,
            'total' => $this->total,
        );
    }
}