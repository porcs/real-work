<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class DoCapture
{
    private $paypalConfig;
    private $PayPal;
    private $DCFields;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        // Set Paypal Config
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIVersion' => $config_pay['api_version'],
            'APISubject' => $config_pay['api_subject'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
        
        $this->DCFields = array(
            'AUTHORIZATIONID' => '',
            'AMT' => '',
            'COMPLETETYPE' => 'Complete',
        );
    }
    
    public function setDCFields($DCFields)
    {
        if(isset($DCFields['AUTHORIZATIONID'])) $this->DCFields['AUTHORIZATIONID'] = $DCFields['AUTHORIZATIONID'];
        if(isset($DCFields['AMT'])) $this->DCFields['AMT'] = $DCFields['AMT'];
        if(isset($DCFields['COMPLETETYPE'])) $this->DCFields['COMPLETETYPE'] = $DCFields['COMPLETETYPE'];
    }
    
    public function doTransaction()
    {
       $DataArray = array(
           'DCFields' => $this->DCFields,
       );
       $this->PayPalResult = $this->PayPal->DoCapture($DataArray);
       return $this->PayPalResult;
    }
    
    public function getPayPalResult()
    {
        return $this->PayPalResult;
    }
}