<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';
require_once 'paypal-classic-api-php/paypal.adaptive.class.php';

class CancelPreapproval
{
    private $paypalConfig;
    private $PayPal;
    private $key;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'ApplicationID' => $config_pay['application_id'],
            'DeveloperAccountEmail' => $config_pay['developer_account_email'],
            'IPAddress' => $config_pay['device_ip_address'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APISubject' => $config_pay['api_subject'],
        );
        $this->PayPal = new PayPal_Adaptive($this->paypalConfig);
    }
    
    public function setPreapprovalKey($key)
    {
        $this->key = $key;
    }
    
    public function doTransaction()
    {
        $DataArray = array(
            'CancelPreapprovalFields' => array(
                'PreapprovalKey' => $this->key,
            ),
        );
        $this->PayPalResult = $this->PayPal->CancelPreapproval($DataArray);
        return $this->PayPalResult;
    }
    
    public function getPayPalResult()
    {
        return $this->PayPalResult;
    }
}