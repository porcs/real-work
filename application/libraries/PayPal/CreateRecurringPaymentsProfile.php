<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class CreateRecurringPaymentsProfile
{
    private $paypalConfig;
    private $PayPal;
    private $CRPPFields, $ProfileDetails, $ScheduleDetails, $BillingPeriod, $CCDetails, $PayerInfo, $PayerName, $BillingAddress, $ShippingAddress;
    private $Products = array();
    private $isShipping;
    private $taxrate, $subtotal, $tax, $total;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
        
        // Set Taxrate
        if($config_pay['taxrate']>0) $this->taxrate = $config_pay['taxrate'];
        else $this->taxrate = 0;
        
        // Initial shipping status
        $this->isShipping = false;
        
        // Prepare CRPPFields
        $this->CRPPFields = array(
            'token' => '',            
        );
        
        // Prepare Profile Details
        $this->ProfileDetails = array(
            'subscribername' => '',                                             // Full name of the person receiving the product or service paid for by the recurring payment.  32 char max.
            'profilestartdate' => '',                                           // Required.  The date when the billing for this profiile begins.  Must be a valid date in UTC/GMT format.
            'profilereference' => '',                                           // The merchant's own unique invoice number or reference ID.  127 char max.
        );
        
        // Prepare Schedule Details
        $this->ScheduleDetails = array(
            'desc' => '',                                                       // Required.  Description of the recurring payment.  This field must match the corresponding billing agreement description included in SetExpressCheckout.
            'maxfailedpayments' => '',                                          // The number of scheduled payment periods that can fail before the profile is automatically suspended.  
            'autobillamt' => 'AddToNextBilling',                                // This field indiciates whether you would like PayPal to automatically bill the outstanding balance amount in the next billing cycle.  Values can be: NoAutoBill or AddToNextBilling
        );
        
        // Prepare Billing Period
        $this->BillingPeriod = array(
            'trialbillingperiod' => '', 
            'trialbillingfrequency' => '', 
            'trialtotalbillingcycles' => '', 
            'trialamt' => '', 
            'billingperiod' => 'Month', 					// Required.  Unit for billing during this subscription period.  One of the following: Day, Week, SemiMonth, Month, Year
            'billingfrequency' => '1',                                          // Required.  Number of billing periods that make up one billing cycle.  The combination of billing freq. and billing period must be less than or equal to one year. 
            'totalbillingcycles' => '0',                                        // the number of billing cycles for the payment period (regular or trial).  For trial period it must be greater than 0.  For regular payments 0 means indefinite...until canceled.  
            'amt' => '', 							// Required.  Billing amount for each billing cycle during the payment period.  This does not include shipping and tax. 
            'currencycode' => $config_pay['currencycode'],                      // Required.  Three-letter currency code.
            'shippingamt' => '',                                                // Shipping amount for each billing cycle during the payment period.
            'taxamt' => '' 							// Tax amount for each billing cycle during the payment period.
        );
        
        // Prepare CCDetails
        $this->CCDetails = array(
            'creditcardtype' => '',                                             // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
            'acct' => '',                                                       // Required.  Credit card number.  No spaces or punctuation.  
            'expdate' => '', 							// Required.  Credit card expiration date.  Format is MMYYYY
            'cvv2' => '', 							// Requirements determined by your PayPal account settings.  Security digits for credit card.
            'startdate' => '',                                                  // Month and year that Maestro or Solo card was issued.  MMYYYY
            'issuenumber' => '',                                                // Issue number of Maestro or Solo card.  Two numeric digits max.
        );
        
        // Prepare Payer Info
        $this->PayerInfo = array(
            'email' => '',                                                      // Email address of payer.
            'payerid' => '',                                                    // Unique PayPal customer ID for payer.
            'payerstatus' => '',                                                // Status of payer.  Values are verified or unverified
            'countrycode' => '',                                                // Payer's country of residence in the form of the two letter code.
            'business' => '',                                                   // Payer's business name.
        );
        
        // Prepare Payer Name
        $this->PayerName = array(
            'salutation' => '',                                                 // Payer's salutation.  20 char max.
            'firstname' => '', 							// Payer's first name.  25 char max.
            'middlename' => '',                                                 // Payer's middle name.  25 char max.
            'lastname' => '', 							// Payer's last name.  25 char max.
            'suffix' => '',							// Payer's suffix.  12 char max.
        );
        
        // Prepare Billing Address
        $this->BillingAddress = array(
            'street' => '',                                                     // Required.  First street address.
            'street2' => '',                                                    // Second street address.
            'city' => '', 							// Required.  Name of City.
            'state' => '',                                                      // Required. Name of State or Province.
            'countrycode' => '',                                                // Required.  Country code.
            'zip' => '',                                                        // Required.  Postal code of payer.
            'phonenum' => '',                                                   // Phone Number of payer.  20 char max.
        );
        
        // Prepare Shipping Address
        $this->ShippingAddress = array(
            'shiptoname' => '',                                                 // Required if shipping is included.  Person's name associated with this address.  32 char max.
            'shiptostreet' => '',                                               // Required if shipping is included.  First street address.  100 char max.
            'shiptostreet2' => '',                                              // Second street address.  100 char max.
            'shiptocity' => '',                                                 // Required if shipping is included.  Name of city.  40 char max.
            'shiptostate' => '',                                                // Required if shipping is included.  Name of state or province.  40 char max.
            'shiptozip' => '',                                                  // Required if shipping is included.  Postal code of shipping address.  20 char max.
            'shiptocountrycode' => '',                                          // Required if shipping is included.  Country code of shipping address.  2 char max.
            'shiptophonenum' => ''                                              // Phone number for shipping address.  20 char max.
        );
    }
    
    public function setShippingStatus($isShipping)
    {
        $this->isShipping = $isShipping;
    }
    
    public function setCRPPFields($CRPPFields)
    {
        if(isset($CRPPFields['token'])) $this->CRPPFields['token'] = $CRPPFields['token'];        
    }
    
    public function setProfileDetails($ProfileDetails)
    {
        if(isset($ProfileDetails['subscribername'])) $this->ProfileDetails['subscribername'] = $ProfileDetails['subscribername'];
        if(isset($ProfileDetails['profilestartdate'])) $this->ProfileDetails['profilestartdate'] = $ProfileDetails['profilestartdate'];
        if(isset($ProfileDetails['profilereference'])) $this->ProfileDetails['profilereference'] = $ProfileDetails['profilereference'];
    }
    
    public function setScheduleDetails($ScheduleDetails)
    {
        if(isset($ScheduleDetails['desc'])) $this->ScheduleDetails['desc'] = $ScheduleDetails['desc'];
        if(isset($ScheduleDetails['maxfailedpayments'])) $this->ScheduleDetails['maxfailedpayments'] = $ScheduleDetails['maxfailedpayments'];
        if(isset($ScheduleDetails['autobillamt'])) $this->ScheduleDetails['autobillamt'] = $ScheduleDetails['autobillamt'];
    }
    
    public function setBillingPeriod($BillingPeriod)
    {
        if(isset($BillingPeriod['trialbillingperiod'])) $this->BillingPeriod['trialbillingperiod'] = $BillingPeriod['trialbillingperiod'];
        if(isset($BillingPeriod['trialbillingfrequency'])) $this->BillingPeriod['trialbillingfrequency'] = $BillingPeriod['trialbillingfrequency'];
        if(isset($BillingPeriod['trialtotalbillingcycles'])) $this->BillingPeriod['trialtotalbillingcycles'] = $BillingPeriod['trialtotalbillingcycles'];
        if(isset($BillingPeriod['trialamt'])) $this->BillingPeriod['trialamt'] = $BillingPeriod['trialamt'];
        if(isset($BillingPeriod['billingperiod'])) $this->BillingPeriod['billingperiod'] = $BillingPeriod['billingperiod'];
        if(isset($BillingPeriod['billingfrequency'])) $this->BillingPeriod['billingfrequency'] = $BillingPeriod['billingfrequency'];
        if(isset($BillingPeriod['totalbillingcycles'])) $this->BillingPeriod['totalbillingcycles'] = $BillingPeriod['totalbillingcycles'];
        if(isset($BillingPeriod['amt'])) $this->BillingPeriod['amt'] = $BillingPeriod['amt'];
        if(isset($BillingPeriod['currencycode'])) $this->BillingPeriod['currencycode'] = $BillingPeriod['currencycode'];
        if(isset($BillingPeriod['shippingamt'])) $this->BillingPeriod['shippingamt'] = $BillingPeriod['shippingamt'];
        if(isset($BillingPeriod['taxamt'])) $this->BillingPeriod['taxamt'] = $BillingPeriod['taxamt'];
    }
    
    public function setCCDetails($CCDetails)
    {
        if(isset($CCDetails['creditcardtype'])) $this->CCDetails['creditcardtype'] = $CCDetails['creditcardtype'];
        if(isset($CCDetails['acct'])) $this->CCDetails['acct'] = $CCDetails['acct'];
        if(isset($CCDetails['expdate'])) $this->CCDetails['expdate'] = $CCDetails['expdate'];
        if(isset($CCDetails['cvv2'])) $this->CCDetails['cvv2'] = $CCDetails['cvv2'];
        if(isset($CCDetails['startdate'])) $this->CCDetails['startdate'] = $CCDetails['startdate'];
        if(isset($CCDetails['issuenumber'])) $this->CCDetails['issuenumber'] = $CCDetails['issuenumber'];
    }
    
    public function setPayerInfo($PayerInfo)
    {
        if(isset($PayerInfo['email'])) $this->PayerInfo['email'] = $PayerInfo['email'];
        if(isset($PayerInfo['payerid'])) $this->PayerInfo['payerid'] = $PayerInfo['payerid'];
        if(isset($PayerInfo['payerstatus'])) $this->PayerInfo['payerstatus'] = $PayerInfo['payerstatus'];
        if(isset($PayerInfo['countrycode'])) $this->PayerInfo['countrycode'] = $PayerInfo['countrycode'];
        if(isset($PayerInfo['business'])) $this->PayerInfo['business'] = $PayerInfo['business'];
    }
    
    public function setPayerName($PayerName)
    {
        if(isset($PayerName['salutation'])) $this->PayerName['salutation'] = $PayerName['salutation'];
        if(isset($PayerName['firstname'])) $this->PayerName['firstname'] = $PayerName['firstname'];
        if(isset($PayerName['middlename'])) $this->PayerName['middlename'] = $PayerName['middlename'];
        if(isset($PayerName['lastname'])) $this->PayerName['lastname'] = $PayerName['lastname'];
        if(isset($PayerName['suffix'])) $this->PayerName['suffix'] = $PayerName['suffix'];
    }
    
    public function setBillingAddress($BillingAddress)
    {
        if(isset($BillingAddress['street'])) $this->BillingAddress['street'] = $BillingAddress['street'];
        if(isset($BillingAddress['street2'])) $this->BillingAddress['street2'] = $BillingAddress['street2'];
        if(isset($BillingAddress['city'])) $this->BillingAddress['city'] = $BillingAddress['city'];
        if(isset($BillingAddress['state'])) $this->BillingAddress['state'] = $BillingAddress['state'];
        if(isset($BillingAddress['countrycode'])) $this->BillingAddress['countrycode'] = $BillingAddress['countrycode'];
        if(isset($BillingAddress['zip'])) $this->BillingAddress['zip'] = $BillingAddress['zip'];
        if(isset($BillingAddress['phonenum'])) $this->BillingAddress['phonenum'] = $BillingAddress['phonenum'];
    }
    
    public function setShippingAddress($ShippingAddress)
    {
        if(isset($ShippingAddress['shiptoname'])) $this->ShippingAddress['shiptoname'] = $ShippingAddress['shiptoname'];
        if(isset($ShippingAddress['shiptostreet'])) $this->ShippingAddress['shiptostreet'] = $ShippingAddress['shiptostreet'];
        if(isset($ShippingAddress['shiptostreet2'])) $this->ShippingAddress['shiptostreet2'] = $ShippingAddress['shiptostreet2'];
        if(isset($ShippingAddress['shiptocity'])) $this->ShippingAddress['shiptocity'] = $ShippingAddress['shiptocity'];
        if(isset($ShippingAddress['shiptostate'])) $this->ShippingAddress['shiptostate'] = $ShippingAddress['shiptostate'];
        if(isset($ShippingAddress['shiptozip'])) $this->ShippingAddress['shiptozip'] = $ShippingAddress['shiptozip'];
        if(isset($ShippingAddress['shiptocountrycode'])) $this->ShippingAddress['shiptocountrycode'] = $ShippingAddress['shiptocountrycode'];
        if(isset($ShippingAddress['shiptophonenum'])) $this->ShippingAddress['shiptophonenum'] = $ShippingAddress['shiptophonenum'];
    }
    
    public function setProduct($arrProduct)
    {
        $product = array(
            'l_name' => isset($arrProduct['name'])?$arrProduct['name']:'',              // Item Name.  127 char max.
            'l_desc' => isset($arrProduct['desc'])?$arrProduct['desc']:'', 		// Item description.  127 char max.
            'l_amt' => isset($arrProduct['amt'])?$arrProduct['amt']:'',                 // Cost of individual item.
            'l_number' => isset($arrProduct['number'])?$arrProduct['number']:'', 	// Item Number.  127 char max.
            'l_qty' => isset($arrProduct['qty'])?$arrProduct['qty']:'', 		// Item quantity.  Must be any positive integer.  
            'l_taxamt' => isset($arrProduct['taxamt'])?$arrProduct['taxamt']:'',        // Item's sales tax amount.    
        );
        $this->Products[] = $product;
    }
    
    public function doTransaction()
    {
        // Summary Subtotal
        $this->subtotal = 0;
        if(sizeof($this->Products)>0)
        {
            foreach($this->Products as $val)
            {
                $this->subtotal += $val['l_amt']*$val['l_qty'];
            }
        }        
        
        // Set Subtotal Tax
        $this->tax = 0;
        if($this->taxrate > 0) $this->tax = $this->subtotal*$this->taxrate/100;
        
        // Set Total Amount
        $this->total = $this->subtotal;
        if($this->tax > 0) $this->total += $this->tax;
        if(isset($this->BillingPeriod['shippingamt']) && strlen($this->BillingPeriod['shippingamt'])) $this->total += $this->BillingPeriod['shippingamt'];
        
        // Set PaymentDetails subtotal, total, tax
        if($this->total>0) $this->BillingPeriod['amt'] = number_format($this->total,2);        
        if($this->tax>0) $this->BillingPeriod['taxamt'] = number_format($this->tax,2);
        
        $PayPalRequestData = array(
            'CRPPFields' => $this->CRPPFields,
            'ProfileDetails' => $this->ProfileDetails, 
            'ScheduleDetails' => $this->ScheduleDetails, 
            'BillingPeriod' => $this->BillingPeriod, 
            'CCDetails' => $this->CCDetails, 
            'PayerInfo' => $this->PayerInfo, 
            'PayerName' => $this->PayerName, 
            'BillingAddress' => $this->BillingAddress,
            'OrderItems' => $this->Products,
        );
        if($this->isShipping) $PayPalRequestData['ShippingAddress'] = $this->ShippingAddress;
        
        $this->PayPalResult = $this->PayPal->CreateRecurringPaymentsProfile($PayPalRequestData);
        return $this->PayPalResult;
    }
    
    public function getPayPalResult()
    {
        return $this->PayPalResult;
    }
}