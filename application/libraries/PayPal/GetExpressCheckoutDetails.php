<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class GetExpressCheckoutDetails
{
    private $paypalConfig;
    private $PayPal;
    private $token, $PaypalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'], 
            'APIVersion' => $config_pay['api_version'], 
            'APISubject' => $config_pay['api_subject'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
    }
    
    public function setToken($token)
    {
        $this->token = $token;
    }
    
    public function doTransaction()
    {
        $this->PaypalResult = $this->PayPal->GetExpressCheckoutDetails($this->token);
        return $this->PaypalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PaypalResult;
    }
}