<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';
require_once 'paypal-classic-api-php/paypal.adaptive.class.php';

class PayPreapproval
{
    private $paypalConfig,$config_pay;
    private $PayPal;
    private $isFunding, $PayRequestFields, $ClientDetailsFields, $FundingTypes,$SenderIdentifierFields, $AccountIdentifierFields;
    private $Receivers;
    private $PayPalResult;
    
    function __construct($config_pay){
        $this->config_pay = $config_pay;
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'ApplicationID' => $config_pay['application_id'],
            'DeveloperAccountEmail' => $config_pay['developer_account_email'],
            'IPAddress' => $config_pay['device_ip_address'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APISubject' => $config_pay['api_subject'],
        );
        $this->PayPal = new PayPal_Adaptive($this->paypalConfig);
        
        // Prepare Funding 
        $this->isFunding = false;
        
        // Prepare Pay Request Fields
        $this->PayRequestFields = array(
            'ActionType' => 'PAY', 						// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
            'CancelURL' => $config_pay['domain'], 				// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
            'CurrencyCode' => $config_pay['currencycode'], 			// Required.  3 character currency code.
            'FeesPayer' => '', 							// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
            'IPNNotificationURL' => '', 					// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
            'Memo' => '', 							// A note associated with the payment (text, not HTML).  1000 char max
            'Pin' => '',                                                        // The sener's personal id number, which was specified when the sender signed up for the preapproval
            'PreapprovalKey' => '', 						// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
            'ReturnURL' => $config_pay['domain'],                               // Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
            'ReverseAllParallelPaymentsOnError' => '',                          // Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
            'SenderEmail' => '', 						// Sender's email address.  127 char max.
            'TrackingID' => '',							// Unique ID that you specify to track the payment.  127 char max.
        );
        
        // Prepare Client Detail Fields
        $this->ClientDetailsFields = array(
            'CustomerID' => '', 						// Your ID for the sender  127 char max.
            'CustomerType' => '', 						// Your ID of the type of customer.  127 char max.
            'GeoLocation' => '', 						// Sender's geographic location
            'Model' => '', 							// A sub-identification of the application.  127 char max.
            'PartnerName' => '',						// Your organization's name or ID
        );
        
        // Prepare Funding Types
        $this->FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');
        
        // Prepare Sender Identifier Fields
        $this->SenderIdentifierFields = array(
            'UseCredentials' => '',						// If TRUE, use credentials to identify the sender.  Default is false.
        );
        
        // Prepare Account Identifier Fields
        $this->AccountIdentifierFields = array(
            'Email' => '',                                                                      // Sender's email address.  127 char max.
            'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''),	// Sender's phone number.  Numbers only.
        );
    }
    
    public function setPayRequestFields($PayRequestFields)
    {
        if(isset($PayRequestFields['ActionType']) && strlen($PayRequestFields['ActionType'])) $this->PayRequestFields['ActionType'] = $PayRequestFields['ActionType'];
        if(isset($PayRequestFields['CancelURL']) && strlen($PayRequestFields['CancelURL'])) $this->PayRequestFields['CancelURL'] = $PayRequestFields['CancelURL'];
        if(isset($PayRequestFields['CurrencyCode']) && strlen($PayRequestFields['CurrencyCode'])) $this->PayRequestFields['CurrencyCode'] = $PayRequestFields['CurrencyCode'];
        if(isset($PayRequestFields['FeesPayer']) && strlen($PayRequestFields['FeesPayer'])) $this->PayRequestFields['FeesPayer'] = $PayRequestFields['FeesPayer'];
        if(isset($PayRequestFields['IPNNotificationURL']) && strlen($PayRequestFields['IPNNotificationURL'])) $this->PayRequestFields['IPNNotificationURL'] = $PayRequestFields['IPNNotificationURL'];
        if(isset($PayRequestFields['Memo']) && strlen($PayRequestFields['Memo'])) $this->PayRequestFields['Memo'] = $PayRequestFields['Memo'];
        if(isset($PayRequestFields['Pin']) && strlen($PayRequestFields['Pin'])) $this->PayRequestFields['Pin'] = $PayRequestFields['Pin'];
        if(isset($PayRequestFields['PreapprovalKey']) && strlen($PayRequestFields['PreapprovalKey'])) $this->PayRequestFields['PreapprovalKey'] = $PayRequestFields['PreapprovalKey'];
        if(isset($PayRequestFields['ReturnURL']) && strlen($PayRequestFields['ReturnURL'])) $this->PayRequestFields['ReturnURL'] = $PayRequestFields['ReturnURL'];
        if(isset($PayRequestFields['ReverseAllParallelPaymentsOnError']) && strlen($PayRequestFields['ReverseAllParallelPaymentsOnError'])) $this->PayRequestFields['ReverseAllParallelPaymentsOnError'] = $PayRequestFields['ReverseAllParallelPaymentsOnError'];
        if(isset($PayRequestFields['SenderEmail']) && strlen($PayRequestFields['SenderEmail'])) $this->PayRequestFields['SenderEmail'] = $PayRequestFields['SenderEmail'];
        if(isset($PayRequestFields['TrackingID']) && strlen($PayRequestFields['TrackingID'])) $this->PayRequestFields['TrackingID'] = $PayRequestFields['TrackingID'];
    }
    
    public function setClientDetailsFields($ClientDetailsFields)
    {
        if(isset($ClientDetailsFields['CustomerID']) && strlen($ClientDetailsFields['CustomerID'])) $this->ClientDetailsFields['CustomerID'] = $ClientDetailsFields['CustomerID'];
        if(isset($ClientDetailsFields['CustomerType']) && strlen($ClientDetailsFields['CustomerType'])) $this->ClientDetailsFields['CustomerType'] = $ClientDetailsFields['CustomerType'];
        if(isset($ClientDetailsFields['GeoLocation']) && strlen($ClientDetailsFields['GeoLocation'])) $this->ClientDetailsFields['GeoLocation'] = $ClientDetailsFields['GeoLocation'];
        if(isset($ClientDetailsFields['Model']) && strlen($ClientDetailsFields['Model'])) $this->ClientDetailsFields['Model'] = $ClientDetailsFields['Model'];
        if(isset($ClientDetailsFields['PartnerName']) && strlen($ClientDetailsFields['PartnerName'])) $this->ClientDetailsFields['PartnerName'] = $ClientDetailsFields['PartnerName'];
    }
    
    public function setReceiver($arrReceiver){
        $config_pay = $this->config_pay;
        
        $Receiver = array(
            'Amount' => isset($arrReceiver['Amount'])?$arrReceiver['Amount']:'', 						// Required.  Amount to be paid to the receiver.
            'Email' => isset($arrReceiver['Email'])?$arrReceiver['Email']:$config_pay['api_subject'], 							// Receiver's email address. 127 char max.
            'InvoiceID' => isset($arrReceiver['InvoiceID'])?$arrReceiver['InvoiceID']:'', 					// The invoice number for the payment.  127 char max.
            'PaymentType' => isset($arrReceiver['PaymentType'])?$arrReceiver['PaymentType']:'', 				// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
            'PaymentSubType' => isset($arrReceiver['PaymentSubType'])?$arrReceiver['PaymentSubType']:'', 			// The transaction subtype for the payment.
            'Phone' => isset($arrReceiver['Phone'])?$arrReceiver['Phone']:array('CountryCode'=>'','PhoneNumber'=>'','Extension'=>''),       // Receiver's phone number.   Numbers only.
            'Primary' => isset($arrReceiver['Primary'])?$arrReceiver['Primary']:'',						// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
        );
        $this->Receivers[] = $Receiver;
    }
    
    public function setSenderIdentifierFields($SenderIdentifierFields)
    {
        if(isset($SenderIdentifierFields['UseCredentials']) && strlen($SenderIdentifierFields['UseCredentials'])) $this->SenderIdentifierFields['UseCredentials'] = $SenderIdentifierFields['UseCredentials'];
    }
    
    public function setAccountIdentifierFields($AccountIdentifierFields)
    {
        if(isset($AccountIdentifierFields['Email']) && strlen($AccountIdentifierFields['Email'])) $this->AccountIdentifierFields['Email'] = $AccountIdentifierFields['Email'];
        if(isset($AccountIdentifierFields['Phone']) && strlen($AccountIdentifierFields['Phone'])) $this->AccountIdentifierFields['Phone'] = $AccountIdentifierFields['Phone'];
    }
    
    public function setIsFunding($isFunding)
    {
        $this->isFunding = $isFunding;
    }
    
    public function doTransaction()
    {
        $PayPalRequestData = array(
            'PayRequestFields' => $this->PayRequestFields,
            'ClientDetailsFields' => $this->ClientDetailsFields,
            'Receivers' => $this->Receivers,
            'SenderIdentifierFields' => $this->SenderIdentifierFields,
            'AccountIdentifierFields' => $this->AccountIdentifierFields,
        );
        if($this->isFunding) $PayPalRequestData['FundingTypes'] = $this->FundingTypes;
        $this->PayPalResult = $this->PayPal->Pay($PayPalRequestData);
        return $this->PayPalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PayPalResult;
    }
}