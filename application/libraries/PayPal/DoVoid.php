<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class DoVoid
{
    private $paypalConfig;
    private $PayPal;
    private $TransactionID;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        // Set Paypal Config
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIVersion' => $config_pay['api_version'],
            'APISubject' => $config_pay['api_subject'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
    } 
    
    public function setTransactionID($TransactionID)
    {
        $this->TransactionID = $TransactionID;
    }
    
    public function doTransaction()
    {
       $DataArray = array(
           'DVFields' => array(
               'AUTHORIZATIONID' => $this->TransactionID,
           ),
       );
       $this->PayPalResult = $this->PayPal->DoVoid($DataArray);
       return $this->PayPalResult;
    }
    
    public function getPayPalResult()
    {
        return $this->PayPalResult;
    }
}