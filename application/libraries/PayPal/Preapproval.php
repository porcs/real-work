<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';
require_once 'paypal-classic-api-php/paypal.adaptive.class.php';

class Preapproval
{
    private $paypalConfig;
    private $PayPal;
    private $PreapprovalFields, $ClientDetailsFields;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'ApplicationID' => $config_pay['application_id'],
            'DeveloperAccountEmail' => $config_pay['developer_account_email'],
            'IPAddress' => $config_pay['device_ip_address'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],            
        );
        $this->PayPal = new PayPal_Adaptive($this->paypalConfig);
        
        // Preapproval Fields
        $this->PreapprovalFields = array(
            'ReturnURL' => $config_pay['domain'], 				// URL to return the sender to after approving at PayPal.
            'CancelURL' => $config_pay['domain'],  				// Required.  URL to send the browser to after the user cancels.
            'CurrencyCode' => $config_pay['currencycode'], 			// Required.  Currency Code.
            'DateOfMonth' => '', 						// The day of the month on which a monthly payment is to be made.  0 - 31.  Specifying 0 indiciates that payment can be made on any day of the month.
            'DayOfWeek' => '', 							// The day of the week that a weekly payment should be made.  Allowable values: NO_DAY_SPECIFIED, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY    
            'IPNNotificationURL' => '', 					// The URL for IPN notifications.
            'MaxAmountPerPayment' => '', 					// The preapproved maximum amount per payment.  Cannot exceed the preapproved max total amount of all payments.
            'MaxNumberOfPayments' => '', 					// The preapproved maximum number of payments.  Cannot exceed the preapproved max total number of all payments. 
            'MaxTotalAmountOfPaymentsPerPeriod' => '',                          // The preapproved maximum number of all payments per period.
            'MaxTotalAmountOfAllPayments' => '',                                // The preapproved maximum total amount of all payments.  Cannot exceed $2,000 USD or the equivalent in other currencies.
            'Memo' => '', 							// A note about the preapproval.
            'PaymentPeriod' => '', 						// The pament period.  One of the following:  NO_PERIOD_SPECIFIED, DAILY, WEEKLY, BIWEEKLY, SEMIMONTHLY, MONTHLY, ANNUALLY
            'PinType' => '', 							// Whether a personal identification number is required.  It is one of the following:  NOT_REQUIRED, REQUIRED            
            'SenderEmail' => '', 						// Sender's email address.  If not specified, the email address of the sender who logs on to approve is used.
            'StartingDate' => '', 						// Required.  First date for which the preapproval is valid.  Cannot be before today's date or after the ending date.
            'EndingDate' => '', 						// Required.  The last date for which the preapproval is valid.  It cannot be later than one year from the starting date.
            'FeesPayer' => '', 							// The payer of the PayPal fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
            'DisplayMaxTotalAmount' => '',					// Whether to display the max total amount of this preapproval.  Values are:  true/false
        );

        // Client Detail Fields
        $this->ClientDetailsFields = array(
            'CustomerID' => '', 						// Your ID for the sender.
            'CustomerType' => '', 						// Your ID of the type of customer.
            'GeoLocation' => '', 						// Sender's geographic location.
            'Model' => '', 							// A sub-id of the application
            'PartnerName' => '',						// Your organization's name or ID.        
        );
    }
    
    public function setPreapprovalFields($arrPreapprovalFields)
    {
        if(isset($arrPreapprovalFields['ReturnURL']) && strlen($arrPreapprovalFields['ReturnURL'])) $this->PreapprovalFields['ReturnURL'] = $arrPreapprovalFields['ReturnURL'];
        if(isset($arrPreapprovalFields['CancelURL']) && strlen($arrPreapprovalFields['CancelURL'])) $this->PreapprovalFields['CancelURL'] = $arrPreapprovalFields['CancelURL'];
        if(isset($arrPreapprovalFields['CurrencyCode']) && strlen($arrPreapprovalFields['CurrencyCode'])) $this->PreapprovalFields['CurrencyCode'] = $arrPreapprovalFields['CurrencyCode'];
        if(isset($arrPreapprovalFields['DateOfMonth']) && strlen($arrPreapprovalFields['DateOfMonth'])) $this->PreapprovalFields['DateOfMonth'] = $arrPreapprovalFields['DateOfMonth'];
        if(isset($arrPreapprovalFields['DayOfWeek']) && strlen($arrPreapprovalFields['DayOfWeek'])) $this->PreapprovalFields['DayOfWeek'] = $arrPreapprovalFields['DayOfWeek'];
        if(isset($arrPreapprovalFields['IPNNotificationURL']) && strlen($arrPreapprovalFields['IPNNotificationURL'])) $this->PreapprovalFields['IPNNotificationURL'] = $arrPreapprovalFields['IPNNotificationURL'];
        if(isset($arrPreapprovalFields['MaxAmountPerPayment']) && strlen($arrPreapprovalFields['MaxAmountPerPayment'])) $this->PreapprovalFields['MaxAmountPerPayment'] = $arrPreapprovalFields['MaxAmountPerPayment'];
        if(isset($arrPreapprovalFields['MaxNumberOfPayments']) && strlen($arrPreapprovalFields['MaxNumberOfPayments'])) $this->PreapprovalFields['MaxNumberOfPayments'] = $arrPreapprovalFields['MaxNumberOfPayments'];
        if(isset($arrPreapprovalFields['MaxTotalAmountOfPaymentsPerPeriod']) && strlen($arrPreapprovalFields['MaxTotalAmountOfPaymentsPerPeriod'])) $this->PreapprovalFields['MaxTotalAmountOfPaymentsPerPeriod'] = $arrPreapprovalFields['MaxTotalAmountOfPaymentsPerPeriod'];
        if(isset($arrPreapprovalFields['MaxTotalAmountOfAllPayments']) && strlen($arrPreapprovalFields['MaxTotalAmountOfAllPayments'])) $this->PreapprovalFields['MaxTotalAmountOfAllPayments'] = $arrPreapprovalFields['MaxTotalAmountOfAllPayments'];
        if(isset($arrPreapprovalFields['Memo']) && strlen($arrPreapprovalFields['Memo'])) $this->PreapprovalFields['Memo'] = $arrPreapprovalFields['Memo'];
        if(isset($arrPreapprovalFields['PaymentPeriod']) && strlen($arrPreapprovalFields['PaymentPeriod'])) $this->PreapprovalFields['PaymentPeriod'] = $arrPreapprovalFields['PaymentPeriod'];
        if(isset($arrPreapprovalFields['PinType']) && strlen($arrPreapprovalFields['PinType'])) $this->PreapprovalFields['PinType'] = $arrPreapprovalFields['PinType'];
        if(isset($arrPreapprovalFields['SenderEmail']) && strlen($arrPreapprovalFields['SenderEmail'])) $this->PreapprovalFields['SenderEmail'] = $arrPreapprovalFields['SenderEmail'];
        if(isset($arrPreapprovalFields['StartingDate']) && strlen($arrPreapprovalFields['StartingDate'])) $this->PreapprovalFields['StartingDate'] = $arrPreapprovalFields['StartingDate'];
        if(isset($arrPreapprovalFields['EndingDate']) && strlen($arrPreapprovalFields['EndingDate'])) $this->PreapprovalFields['EndingDate'] = $arrPreapprovalFields['EndingDate'];
        if(isset($arrPreapprovalFields['FeesPayer']) && strlen($arrPreapprovalFields['FeesPayer'])) $this->PreapprovalFields['FeesPayer'] = $arrPreapprovalFields['FeesPayer'];
        if(isset($arrPreapprovalFields['DisplayMaxTotalAmount']) && strlen($arrPreapprovalFields['DisplayMaxTotalAmount'])) $this->PreapprovalFields['DisplayMaxTotalAmount'] = $arrPreapprovalFields['DisplayMaxTotalAmount'];        
    }
    
    public function setClientDetailsFields($arrClientDetailsFields)
    {
        if(isset($arrClientDetailsFields['CustomerID']) && strlen($arrClientDetailsFields['CustomerID'])) $this->ClientDetailsFields['CustomerID'] = $arrClientDetailsFields['CustomerID'];
        if(isset($arrClientDetailsFields['CustomerType']) && strlen($arrClientDetailsFields['CustomerType'])) $this->ClientDetailsFields['CustomerType'] = $arrClientDetailsFields['CustomerType'];
        if(isset($arrClientDetailsFields['GeoLocation']) && strlen($arrClientDetailsFields['GeoLocation'])) $this->ClientDetailsFields['GeoLocation'] = $arrClientDetailsFields['GeoLocation'];
        if(isset($arrClientDetailsFields['Model']) && strlen($arrClientDetailsFields['Model'])) $this->ClientDetailsFields['Model'] = $arrClientDetailsFields['Model'];
        if(isset($arrClientDetailsFields['PartnerName']) && strlen($arrClientDetailsFields['PartnerName'])) $this->ClientDetailsFields['PartnerName'] = $arrClientDetailsFields['PartnerName'];
    }
    
    public function doTransaction()
    {
        $PayPalRequestData = array(
            'PreapprovalFields' => $this->PreapprovalFields, 
            'ClientDetailsFields' => $this->ClientDetailsFields,
        );
        $this->PayPalResult = $this->PayPal->Preapproval($PayPalRequestData);
        return $this->PayPalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PayPalResult;
    }
}