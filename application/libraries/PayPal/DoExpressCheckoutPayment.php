<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class DoExpressCheckoutPayment
{
    private $paypalConfig, $PayPal, $GECDResult, $DECPFields, $PaypalResult, $Payments;
    private $paymentAction;
    function __construct($config_pay)
    {        
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'], 
            'APIVersion' => $config_pay['api_version'], 
            'APISubject' => $config_pay['api_subject'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
        
        $this->DECPFields = array(
            'token' => '',
            'payerid' => '',            
            'returnfmfdetails' => '1',
            'giftmessage' => '',
            'giftreceiptenable' => '',
            'giftwrapname' => '',
            'giftwrapamount' => '',
            'buyermarketingemail' => '',
            'surveyquestion' => '',
            'surveychoiceselected' => '',
            'allowedpaymentmethod' => '',
            'buttonsource' => $config_pay['buttonsource'],
        );
        
        $this->paymentAction = 'Sale';
    }
    
    public function setPaymentAction($paymentAction)
    {
        $this->paymentAction = $paymentAction;
    }
    
    public function setDECPFields($GECDResult)
    {
        $this->GECDResult = $GECDResult;
        
        if(isset($this->GECDResult['TOKEN']) && strlen($this->GECDResult['TOKEN'])) $this->DECPFields['token'] = $this->GECDResult['TOKEN'];
        if(isset($this->GECDResult['PAYERID']) && strlen($this->GECDResult['PAYERID'])) $this->DECPFields['payerid'] = $this->GECDResult['PAYERID'];        
        
        $this->Payments = array();
        for($i=0;$i<sizeof($this->GECDResult['PAYMENTS']);$i++)
        {
            $payment = array();
            foreach($this->GECDResult['PAYMENTS'][$i] as $key => $val)
            {
                if(!is_array($val)) $val = trim($val);
                $payment[strtolower($key)] = $val;
            }
            if($payment['orderitems'])
            {
                $orderItem = array();
                for($j=0;$j<sizeof($payment['orderitems']);$j++)
                {
                    $item = array();
                    foreach($payment['orderitems'][$j] as $key => $val)
                    {                
                        $item[strtolower($key)] = trim($val);
                    }
                    $orderItem[] = $item;
                }
                unset($payment['orderitems']);
                $payment['order_items'] = $orderItem;
            }
            $payment['paymentaction'] = $this->paymentAction;            
            $this->Payments[] = $payment;
        }
    }
    
    public function doTransaction()
    {
        $PayPalRequest = array(
            'DECPFields' => $this->DECPFields, 
            'Payments' => $this->Payments,
        );
        $this->PaypalResult = $this->PayPal->DoExpressCheckoutPayment($PayPalRequest);
        return $this->PaypalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PaypalResult;
    }
}