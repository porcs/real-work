<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';
require_once 'paypal-classic-api-php/paypal.adaptive.class.php';

class PreapprovalDetails
{
    private $paypalConfig;
    private $PayPal;
    private $PreapprovalKey,$PreapprovalDetailsFields;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'ApplicationID' => $config_pay['application_id'],
            'DeveloperAccountEmail' => $config_pay['developer_account_email'],
            'IPAddress' => $config_pay['device_ip_address'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],            
        );
        $this->PayPal = new PayPal_Adaptive($this->paypalConfig);                
    }        
    
    public function setPreapprovalKey($PreapprovalKey)
    {
        $this->PreapprovalKey = $PreapprovalKey;
        
        // Prepare request arrays
        $this->PreapprovalDetailsFields = array(
            'GetBillingAddress' => '', 						// Opion to get the billing address in the response.  true or false.  Only available with Advanced permissions levels.
            'PreapprovalKey' => $this->PreapprovalKey, 				// Required.  A preapproval key that identifies the preapproval for which you want to retrieve details.  Returned in the PreapprovalResponse
        );
    }
    
    public function doTransaction()
    {
        $PayPalRequestData = array(
            'PreapprovalDetailsFields' => $this->PreapprovalDetailsFields,             
        );
        $this->PayPalResult = $this->PayPal->PreapprovalDetails($PayPalRequestData);
        return $this->PayPalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PayPalResult;
    }
}