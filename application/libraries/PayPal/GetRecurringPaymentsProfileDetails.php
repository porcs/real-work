<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class GetRecurringPaymentsProfileDetails
{
    private $paypalConfig;
    private $PayPal;
    private $ProfileID;
    private $PayPalResult;
    
    function __construct($config_pay)
    {
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
    }
    
    public function setProfileID($ProfileID)
    {
        $this->ProfileID = $ProfileID;
    }
    
    public function doTransaction()
    {
        $GRPPDFields = array(
            'profileid' => $this->ProfileID,
        );
        
        $PayPalRequestData = array(
            'GRPPDFields' => $GRPPDFields,
        );
        
        $this->PayPalResult = $this->PayPal->GetRecurringPaymentsProfileDetails($PayPalRequestData);
        return $this->PayPalResult;
    }
    
    public function getPayPalResult()
    {
        return $this->PayPalResult;
    }
}