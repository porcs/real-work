<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once 'paypal-classic-api-php/paypal.class.php';

class SetExpressCheckout
{
    private $paypalConfig;
    private $PayPal;
    private $SECFields, $SurveyChoices, $BillingAgreement, $Payment;
    private $taxrate, $subtotal, $tax, $total;
    private $Product = array();
    private $PaypalResult, $isBillingAgreement;
    
    function __construct($config_pay)
    {        
        // Set Paypal Config
        $this->paypalConfig = array(
            'Sandbox' => $config_pay['sandbox'],
            'APIVersion' => $config_pay['api_version'],
            'APIUsername' => $config_pay['api_username'],
            'APIPassword' => $config_pay['api_password'],
            'APISignature' => $config_pay['api_signature'],
            'APIVersion' => $config_pay['api_version'],
            'APISubject' => $config_pay['api_subject'],
            'APIButtonSource' => $config_pay['buttonsource'],
        );
        $this->PayPal = new PayPal($this->paypalConfig);
        
        // Set Taxrate
        if($config_pay['taxrate']>0) $this->taxrate = $config_pay['taxrate'];
        else $this->taxrate = 0;
        
        // Set BillingAgreement true/false, initial = false
        $this->isBillingAgreement = false;
        
        // Set Paypal Options
        $this->SECFields = array(
            'token' => '', 								// A timestamped token, the value of which was returned by a previous SetExpressCheckout call.
            'maxamt' => '', 						// The expected maximum total amount the order will be, including S&H and sales tax.
            'returnurl' => '', 							// Required.  URL to which the customer will be returned after returning from PayPal.  2048 char max.
            'cancelurl' => '', 							// Required.  URL to which the customer will be returned if they cancel payment on PayPal's site.
            'callback' => '', 							// URL to which the callback request from PayPal is sent.  Must start with https:// for production.
            'callbacktimeout' => '', 					// An override for you to request more or less time to be able to process the callback request and response.  Acceptable range for override is 1-6 seconds.  If you specify greater than 6 PayPal will use default value of 3 seconds.
            'callbackversion' => '', 					// The version of the Instant Update API you're using.  The default is the current version.							
            'reqconfirmshipping' => '0', 				// The value 1 indicates that you require that the customer's shipping address is Confirmed with PayPal.  This overrides anything in the account profile.  Possible values are 1 or 0.
            'noshipping' => '0', 						// The value 1 indiciates that on the PayPal pages, no shipping address fields should be displayed.  Maybe 1 or 0.
            'allownote' => '1', 							// The value 1 indiciates that the customer may enter a note to the merchant on the PayPal page during checkout.  The note is returned in the GetExpresscheckoutDetails response and the DoExpressCheckoutPayment response.  Must be 1 or 0.
            'addroverride' => '', 						// The value 1 indiciates that the PayPal pages should display the shipping address set by you in the SetExpressCheckout request, not the shipping address on file with PayPal.  This does not allow the customer to edit the address here.  Must be 1 or 0.
            'localecode' => '', 						// Locale of pages displayed by PayPal during checkout.  Should be a 2 character country code.  You can retrive the country code by passing the country name into the class' GetCountryCode() function.
            'pagestyle' => '', 							// Sets the Custom Payment Page Style for payment pages associated with this button/link.  
            'hdrimg' => '', 							// URL for the image displayed as the header during checkout.  Max size of 750x90.  Should be stored on an https:// server or you'll get a warning message in the browser.
            'hdrbordercolor' => '', 					// Sets the border color around the header of the payment page.  The border is a 2-pixel permiter around the header space.  Default is black.  
            'hdrbackcolor' => '', 						// Sets the background color for the header of the payment page.  Default is white.  
            'payflowcolor' => '', 						// Sets the background color for the payment page.  Default is white.
            'skipdetails' => '', 						// This is a custom field not included in the PayPal documentation.  It's used to specify whether you want to skip the GetExpressCheckoutDetails part of checkout or not.  See PayPal docs for more info.
            'email' => '', 								// Email address of the buyer as entered during checkout.  PayPal uses this value to pre-fill the PayPal sign-in page.  127 char max.
            'solutiontype' => 'Sole', 						// Type of checkout flow.  Must be Sole (express checkout for auctions) or Mark (normal express checkout)
            'landingpage' => 'Login', 						// Type of PayPal page to display.  Can be Billing or Login.  If billing it shows a full credit card form.  If Login it just shows the login screen.
            'channeltype' => '', 						// Type of channel.  Must be Merchant (non-auction seller) or eBayItem (eBay auction)
            'giropaysuccessurl' => '', 					// The URL on the merchant site to redirect to after a successful giropay payment.  Only use this field if you are using giropay or bank transfer payment methods in Germany.
            'giropaycancelurl' => '', 					// The URL on the merchant site to redirect to after a canceled giropay payment.  Only use this field if you are using giropay or bank transfer methods in Germany.
            'banktxnpendingurl' => '',  				// The URL on the merchant site to transfer to after a bank transfter payment.  Use this field only if you are using giropay or bank transfer methods in Germany.
            'brandname' => '', 							// A label that overrides the business name in the PayPal account on the PayPal hosted checkout pages.  127 char max.
            'customerservicenumber' => '', 				// Merchant Customer Service number displayed on the PayPal Review page. 16 char max.
            'giftmessageenable' => '0', 					// Enable gift message widget on the PayPal Review page. Allowable values are 0 and 1
            'giftreceiptenable' => '0', 					// Enable gift receipt widget on the PayPal Review page. Allowable values are 0 and 1
            'giftwrapenable' => '0', 					// Enable gift wrap widget on the PayPal Review page.  Allowable values are 0 and 1.
            'giftwrapname' => '', 						// Label for the gift wrap option such as "Box with ribbon".  25 char max.
            'giftwrapamount' => '', 					// Amount charged for gift-wrap service.
            'buyeremailoptionenable' => '1', 			// Enable buyer email opt-in on the PayPal Review page. Allowable values are 0 and 1
            'surveyquestion' => '', 					// Text for the survey question on the PayPal Review page. If the survey question is present, at least 2 survey answer options need to be present.  50 char max.
            'surveyenable' => '1', 						// Enable survey functionality. Allowable values are 0 and 1
            'buyerid' => '', 							// The unique identifier provided by eBay for this buyer. The value may or may not be the same as the username. In the case of eBay, it is different. 255 char max.
            'buyerusername' => '', 						// The user name of the user at the marketplaces site.
            'buyerregistrationdate' => date('Y-m-d').'T00:00:00Z',  			// Date when the user registered with the marketplace.
            'allowpushfunding' => ''					// Whether the merchant can accept push funding.  0 = Merchant can accept push funding : 1 = Merchant cannot accept push funding.			
        );
        
        // Basic array of survey choices.  Nothing but the values should go in here.  
        $this->SurveyChoices = array('Yes', 'No');
        
        // Initial Payment
        $this->Payment = array(
            'amt' => '', 							// Required.  The total cost of the transaction to the customer.  If shipping cost and tax charges are known, include them in this value.  If not, this value should be the current sub-total of the order.
            'currencycode' => $config_pay['currencycode'], 					// A three-character currency code.  Default is USD.
            'itemamt' => '', 						// Required if you specify itemized L_AMT fields. Sum of cost of all items in this order.  
            'shippingamt' => '', 					// Total shipping costs for this order.  If you specify SHIPPINGAMT you mut also specify a value for ITEMAMT.
            'insuranceoptionoffered' => '', 		// If true, the insurance drop-down on the PayPal review page displays the string 'Yes' and the insurance amount.  If true, the total shipping insurance for this order must be a positive number.
            'handlingamt' => '', 					// Total handling costs for this order.  If you specify HANDLINGAMT you mut also specify a value for ITEMAMT.
            'taxamt' => '', 						// Required if you specify itemized L_TAXAMT fields.  Sum of all tax items in this order. 
            'desc' => '', 							// Description of items on the order.  127 char max.
            'custom' => '', 						// Free-form field for your own use.  256 char max.
            'invnum' => '', 						// Your own invoice or tracking number.  127 char max.
            'notifyurl' => '',  						// URL for receiving Instant Payment Notifications
            'shiptoname' => '', 					// Required if shipping is included.  Person's name associated with this address.  32 char max.
            'shiptostreet' => '', 					// Required if shipping is included.  First street address.  100 char max.
            'shiptostreet2' => '', 					// Second street address.  100 char max.
            'shiptocity' => '', 					// Required if shipping is included.  Name of city.  40 char max.
            'shiptostate' => '', 					// Required if shipping is included.  Name of state or province.  40 char max.
            'shiptozip' => '', 						// Required if shipping is included.  Postal code of shipping address.  20 char max.
            'shiptocountry' => '', 					// Required if shipping is included.  Country code of shipping address.  2 char max.
            'shiptophonenum' => '',  				// Phone number for shipping address.  20 char max.
            'notetext' => '', 						// Note to the merchant.  255 char max.  
            'allowedpaymentmethod' => '', 			// The payment method type.  Specify the value InstantPaymentOnly.
            'paymentaction' => 'Sale', 					// How you want to obtain the payment.  When implementing parallel payments, this field is required and must be set to Order. 
            'paymentrequestid' => '',  				// A unique identifier of the specific payment request, which is required for parallel payments. 
            'sellerpaypalaccountid' => ''			// A unique identifier for the merchant.  For parallel payments, this field is required and must contain the Payer ID or the email address of the merchant.
        );
        
        // For Billing Agreement
        $this->BillingAgreement = array(
            'l_billingtype' => 'MerchantInitiatedBilling', 							// Required.  Type of billing agreement.  For recurring payments it must be RecurringPayments.  You can specify up to ten billing agreements.  For reference transactions, this field must be either:  MerchantInitiatedBilling, or MerchantInitiatedBillingSingleSource
            'l_billingagreementdescription' => 'Billing Agreement', 			// Required for recurring payments.  Description of goods or services associated with the billing agreement.  
            'l_paymenttype' => 'Any', 							// Specifies the type of PayPal payment you require for the billing agreement.  Any or IntantOnly
            'l_billingagreementcustom' => ''					// Custom annotation field for your own use.  256 char max.
        );
    }
    
    public function setIsBillingAgreement($isBillingAgreement)
    {
        $this->isBillingAgreement = $isBillingAgreement;
    }
    
    public function setProduct($arrProduct)
    {
        $product = array(
            'name' => isset($arrProduct['name'])?$arrProduct['name']:'', 							// Item name. 127 char max.
            'desc' => isset($arrProduct['desc'])?$arrProduct['desc']:'', 							// Item description. 127 char max.
            'amt' => isset($arrProduct['amt'])?$arrProduct['amt']:'', 								// Cost of item.
            'number' => isset($arrProduct['number'])?$arrProduct['number']:'', 							// Item number.  127 char max.
            'qty' => isset($arrProduct['qty'])?$arrProduct['qty']:'', 								// Item qty on order.  Any positive integer.
            'taxamt' => isset($arrProduct['taxamt'])?$arrProduct['taxamt']:'', 							// Item sales tax
            'itemurl' => isset($arrProduct['itemurl'])?$arrProduct['itemurl']:'', 							// URL for the item.
            'itemcategory' => isset($arrProduct['itemcategory'])?$arrProduct['itemcategory']:'', 				// One of the following values:  Digital, Physical
            'itemweightvalue' => isset($arrProduct['itemweightvalue'])?$arrProduct['itemweightvalue']:'', 					// The weight value of the item.
            'itemweightunit' => isset($arrProduct['itemweightunit'])?$arrProduct['itemweightunit']:'', 					// The weight unit of the item.
            'itemheightvalue' => isset($arrProduct['itemheightvalue'])?$arrProduct['itemheightvalue']:'', 					// The height value of the item.
            'itemheightunit' => isset($arrProduct['itemheightunit'])?$arrProduct['itemheightunit']:'', 					// The height unit of the item.
            'itemwidthvalue' => isset($arrProduct['itemwidthvalue'])?$arrProduct['itemwidthvalue']:'', 					// The width value of the item.
            'itemwidthunit' => isset($arrProduct['itemwidthunit'])?$arrProduct['itemwidthunit']:'', 					// The width unit of the item.
            'itemlengthvalue' => isset($arrProduct['itemlengthvalue'])?$arrProduct['itemlengthvalue']:'', 					// The length value of the item.
            'itemlengthunit' => isset($arrProduct['itemlengthunit'])?$arrProduct['itemlengthunit']:'',
        );
        $this->Product[] = $product;
    }
    
    public function setFields($arrFields)
    {
        if(isset($arrFields['maxamt'])) $this->SECFields['maxamt'] = $arrFields['maxamt'];
        if(isset($arrFields['returnurl'])) $this->SECFields['returnurl'] = $arrFields['returnurl'];
        if(isset($arrFields['cancelurl'])) $this->SECFields['cancelurl'] = $arrFields['cancelurl'];
        if(isset($arrFields['callback'])) $this->SECFields['callback'] = $arrFields['callback'];
        if(isset($arrFields['callbacktimeout'])) $this->SECFields['callbacktimeout'] = $arrFields['callbacktimeout'];
        if(isset($arrFields['callbackversion'])) $this->SECFields['callbackversion'] = $arrFields['callbackversion'];
        if(isset($arrFields['reqconfirmshipping'])) $this->SECFields['reqconfirmshipping'] = $arrFields['reqconfirmshipping'];
        if(isset($arrFields['noshipping'])) $this->SECFields['noshipping'] = $arrFields['noshipping'];
        if(isset($arrFields['allownote'])) $this->SECFields['allownote'] = $arrFields['allownote'];
        if(isset($arrFields['addroverride'])) $this->SECFields['addroverride'] = $arrFields['addroverride'];
        if(isset($arrFields['localecode'])) $this->SECFields['localecode'] = $arrFields['localecode'];
        if(isset($arrFields['pagestyle'])) $this->SECFields['pagestyle'] = $arrFields['pagestyle'];
        if(isset($arrFields['hdrimg'])) $this->SECFields['hdrimg'] = $arrFields['hdrimg'];
        if(isset($arrFields['hdrbordercolor'])) $this->SECFields['hdrbordercolor'] = $arrFields['hdrbordercolor'];
        if(isset($arrFields['hdrbackcolor'])) $this->SECFields['hdrbackcolor'] = $arrFields['hdrbackcolor'];
        if(isset($arrFields['payflowcolor'])) $this->SECFields['payflowcolor'] = $arrFields['payflowcolor'];
        if(isset($arrFields['skipdetails'])) $this->SECFields['skipdetails'] = $arrFields['skipdetails'];
        if(isset($arrFields['email'])) $this->SECFields['email'] = $arrFields['email'];
        if(isset($arrFields['solutiontype'])) $this->SECFields['solutiontype'] = $arrFields['solutiontype'];
        if(isset($arrFields['landingpage'])) $this->SECFields['landingpage'] = $arrFields['landingpage'];
        if(isset($arrFields['channeltype'])) $this->SECFields['channeltype'] = $arrFields['channeltype'];
        if(isset($arrFields['giropaysuccessurl'])) $this->SECFields['giropaysuccessurl'] = $arrFields['giropaysuccessurl'];
        if(isset($arrFields['giropaycancelurl'])) $this->SECFields['giropaycancelurl'] = $arrFields['giropaycancelurl'];
        if(isset($arrFields['banktxnpendingurl'])) $this->SECFields['banktxnpendingurl'] = $arrFields['banktxnpendingurl'];
        if(isset($arrFields['brandname'])) $this->SECFields['brandname'] = $arrFields['brandname'];
        if(isset($arrFields['customerservicenumber'])) $this->SECFields['customerservicenumber'] = $arrFields['customerservicenumber'];
        if(isset($arrFields['giftmessageenable'])) $this->SECFields['giftmessageenable'] = $arrFields['giftmessageenable'];
        if(isset($arrFields['giftreceiptenable'])) $this->SECFields['giftreceiptenable'] = $arrFields['giftreceiptenable'];
        if(isset($arrFields['giftwrapenable'])) $this->SECFields['giftwrapenable'] = $arrFields['giftwrapenable'];
        if(isset($arrFields['giftwrapname'])) $this->SECFields['giftwrapname'] = $arrFields['giftwrapname'];
        if(isset($arrFields['giftwrapamount'])) $this->SECFields['giftwrapamount'] = $arrFields['giftwrapamount'];
        if(isset($arrFields['buyeremailoptionenable'])) $this->SECFields['buyeremailoptionenable'] = $arrFields['buyeremailoptionenable'];
        if(isset($arrFields['surveyquestion'])) $this->SECFields['surveyquestion'] = $arrFields['surveyquestion'];
        if(isset($arrFields['surveyenable'])) $this->SECFields['surveyenable'] = $arrFields['surveyenable'];
        if(isset($arrFields['buyerid'])) $this->SECFields['buyerid'] = $arrFields['buyerid'];
        if(isset($arrFields['buyerusername'])) $this->SECFields['buyerusername'] = $arrFields['buyerusername'];
        if(isset($arrFields['buyerregistrationdate'])) $this->SECFields['buyerregistrationdate'] = $arrFields['buyerregistrationdate'];
        if(isset($arrFields['allowpushfunding'])) $this->SECFields['allowpushfunding'] = $arrFields['allowpushfunding'];
    }
    
    public function setPayment($arrPayment)
    {
        if(isset($arrPayment['amt'])) $this->Payment['amt'] = $arrPayment['amt'];
        if(isset($arrPayment['currencycode'])) $this->Payment['currencycode'] = $arrPayment['currencycode'];
        if(isset($arrPayment['itemamt'])) $this->Payment['itemamt'] = $arrPayment['itemamt'];
        if(isset($arrPayment['shippingamt'])) $this->Payment['shippingamt'] = $arrPayment['shippingamt'];
        if(isset($arrPayment['insuranceoptionoffered'])) $this->Payment['insuranceoptionoffered'] = $arrPayment['insuranceoptionoffered'];
        if(isset($arrPayment['handlingamt'])) $this->Payment['handlingamt'] = $arrPayment['handlingamt'];
        if(isset($arrPayment['taxamt'])) $this->Payment['taxamt'] = $arrPayment['taxamt'];
        if(isset($arrPayment['desc'])) $this->Payment['desc'] = $arrPayment['desc'];
        if(isset($arrPayment['custom'])) $this->Payment['custom'] = $arrPayment['custom'];
        if(isset($arrPayment['invnum'])) $this->Payment['invnum'] = $arrPayment['invnum'];
        if(isset($arrPayment['notifyurl'])) $this->Payment['notifyurl'] = $arrPayment['notifyurl'];
        if(isset($arrPayment['shiptoname'])) $this->Payment['shiptoname'] = $arrPayment['shiptoname'];
        if(isset($arrPayment['shiptostreet'])) $this->Payment['shiptostreet'] = $arrPayment['shiptostreet'];
        if(isset($arrPayment['shiptostreet2'])) $this->Payment['shiptostreet2'] = $arrPayment['shiptostreet2'];
        if(isset($arrPayment['shiptocity'])) $this->Payment['shiptocity'] = $arrPayment['shiptocity'];
        if(isset($arrPayment['shiptostate'])) $this->Payment['shiptostate'] = $arrPayment['shiptostate'];
        if(isset($arrPayment['shiptozip'])) $this->Payment['shiptozip'] = $arrPayment['shiptozip'];
        if(isset($arrPayment['shiptocountry'])) $this->Payment['shiptocountry'] = $arrPayment['shiptocountry'];
        if(isset($arrPayment['shiptophonenum'])) $this->Payment['shiptophonenum'] = $arrPayment['shiptophonenum'];
        if(isset($arrPayment['notetext'])) $this->Payment['notetext'] = $arrPayment['notetext'];
        if(isset($arrPayment['allowedpaymentmethod'])) $this->Payment['allowedpaymentmethod'] = $arrPayment['allowedpaymentmethod'];
        if(isset($arrPayment['paymentaction'])) $this->Payment['paymentaction'] = $arrPayment['paymentaction'];
        if(isset($arrPayment['paymentrequestid'])) $this->Payment['paymentrequestid'] = $arrPayment['paymentrequestid'];
        if(isset($arrPayment['sellerpaypalaccountid'])) $this->Payment['sellerpaypalaccountid'] = $arrPayment['sellerpaypalaccountid'];
        $this->Payment['order_items'] = $this->Product;
    }
    
    public function setBillingAgreement($arrBilling)
    {
        if(isset($arrBilling['l_billingtype']) && strlen($arrBilling['l_billingtype'])) $this->BillingAgreement['l_billingtype'] = $arrBilling['l_billingtype'];
        if(isset($arrBilling['l_billingagreementdescription']) && strlen($arrBilling['l_billingagreementdescription'])) $this->BillingAgreement['l_billingagreementdescription'] = $arrBilling['l_billingagreementdescription'];
        if(isset($arrBilling['l_paymenttype']) && strlen($arrBilling['l_paymenttype'])) $this->BillingAgreement['l_paymenttype'] = $arrBilling['l_paymenttype'];
        if(isset($arrBilling['l_billingagreementcustom']) && strlen($arrBilling['l_billingagreementcustom'])) $this->BillingAgreement['l_billingagreementcustom'] = $arrBilling['l_billingagreementcustom'];        
    }
    
    public function doTransaction()
    {
        $this->subtotal = 0;
        if(sizeof($this->Product)>0)
        {
            foreach($this->Product as $val)
            {
                $this->subtotal += $val['amt']*$val['qty'];
            }
        }        
        
        // Set Subtotal Tax
        $this->tax = 0;
        if($this->taxrate > 0) $this->tax = $this->subtotal*$this->taxrate/100;
        
        // Set Total Amount
        $this->total = $this->subtotal;
        if($this->tax > 0) $this->total += $this->tax;
        if(isset($this->Payment['shippingamt']) && strlen($this->Payment['shippingamt'])) $this->total += $this->Payment['shippingamt'];
        if(isset($this->Payment['handlingamt']) && strlen($this->Payment['handlingamt'])) $this->total += $this->Payment['handlingamt'];
        
        // Set PaymentDetails subtotal, total, tax
        if($this->total>0) $this->Payment['amt'] = number_format($this->total,2);
        if($this->subtotal>0) $this->Payment['itemamt'] = number_format($this->subtotal,2);
        if($this->tax>0) $this->Payment['taxamt'] = number_format($this->tax,2);
        
        $PayPalRequest = array(
            'SECFields' => $this->SECFields, 
            'SurveyChoices' => $this->SurveyChoices,             
            'Payments' => array($this->Payment),        
        );
        if($this->isBillingAgreement) $PayPalRequest['BillingAgreements'] = array($this->BillingAgreement);
        $this->PaypalResult = $this->PayPal->SetExpressCheckout($PayPalRequest);
        return $this->PaypalResult;
    }
    
    public function getPaypalResult()
    {
        return $this->PaypalResult;
    }
    
    public function getAmount()
    {
        return array(
            'taxrate' => $this->taxrate,
            'subtotal' => $this->subtotal,
            'tax' => $this->tax,
            'total' => $this->total,
        );
    }
}