<?php   if ( !file_exists(dirname(__FILE__).'/ObjectBiz.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../eucurrency.php')) exit('No direct script access allowed');
        echo header('Content-Type: text/html; charset=utf-8');
        if ( class_exists('FeedBiz')    == FALSE ) require dirname(dirname(__FILE__)).'/FeedBiz.php';
        if ( class_exists('ObjectBiz')  == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        if ( class_exists('ebaylib')    == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')    == FALSE ) require dirname(__FILE__).'/connect.php';
        if ( class_exists('Eucurrency') == FALSE ) require dirname(__FILE__).'/../eucurrency.php';
        require dirname(__FILE__).'/../tools.php'; 
        
        class ebaystore extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $language;
                protected $site;
                protected $id_shop;
                protected $objectbiz;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_JUST_ORDERS' )     $result['just_orders']          = $value;
                        endwhile;
                        return $result;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_data($parent_category_id = null, $childCategory = null) {
                        $query = '';
                        unset($data_store);
                        if ( !empty($childCategory) && isset($childCategory) ) :
                                foreach ($childCategory as $child) :
                                        $id_category                            = $child->CategoryID->__toString();
                                        if ( !empty($child->ChildCategory) ) :
                                                $query .= $this->set_data($id_category, $child->ChildCategory);           
                                        endif;
                                        $data_store                             = array(
                                                'id_category'                   => isset($id_category) ? $id_category : '',
                                                'id_site'                       => $this->site,
                                                'id_shop'                       => $this->id_shop,
                                                'ebay_user'                     => $this->ebayUser,
                                                'name_category'                 => isset($child->Name) ? $child->Name->__toString() : '',
                                                'parent'                        => isset($parent_category_id) ? $parent_category_id : '',
                                                'order_item'                    => isset($child->Order) ? $child->Order->__toString() : 0,
                                                "date_add"                      => date("Y-m-d H:i:s", strtotime("now")),
                                        );
                                        $query                                  .= $this->objectbiz->mappingTableOnly('ebay_store_category', $data_store);
                                endforeach;
                        endif;
                        return $query;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_store_db($parser = '') {
                        $query                                                  = '';
                        $parser                                                 = $this->ebaylib->core->_parser;
                        $this->objectbiz->checkDB("ebay_store_category");
                        $this->objectbiz->deleteColumn('ebay_store_category', array('id_site' => $this->site, 'id_shop' => $this->id_shop));
//                      echo "<pre>", print_r($parser, true), '</pre>';exit();
                        if ( !empty($parser) && $parser->Ack->__toString() == 'Success' && isset($parser->Store) ) :
                                foreach ( $parser->Store->CustomCategories->CustomCategory as $customCategory ) :
                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        $id_category                            = !empty($customCategory->CategoryID) ? $customCategory->CategoryID->__toString() : Null;
                                        $name                                   = !empty($customCategory->Name) ? $customCategory->Name->__toString() : Null;
                                        $order                                  = !empty($customCategory->Order) ? $customCategory->Order->__toString() : Null;
                                        $childCategory                          = !empty($customCategory->ChildCategory) ? $customCategory->ChildCategory : '';   
                                        if ( !empty($childCategory) ) :
                                                $query                          .= $this->set_data($id_category, $childCategory);           
                                        endif;
                                        $data_store                             = array(
                                                'id_category'                   => isset($id_category) ? $id_category : '',
                                                'id_site'                       => $this->site,
                                                'id_shop'                       => $this->id_shop,
                                                'ebay_user'                     => $this->ebayUser,
                                                'name_category'                 => isset($name) ? $name : '',
                                                'parent'                        => NULL,
                                                'order_item'                    => isset($order) ? $order : 0,
                                                "date_add"                      => date("Y-m-d H:i:s", strtotime("now")),
                                        );
                                        $query                                  .= $this->objectbiz->mappingTableOnly('ebay_store_category', $data_store);
                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                endforeach;
                                $this->objectbiz->transaction($query);
                        endif;
                } 

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_store() {
                        $this->ebaylib->GetStore($this->site);
                        $this->set_store_db();
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                             = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->objectbiz                        = new ObjectBiz(array($this->user_name));
                        $this->connect                          = new connect();
                        
                        $shop_default                           = $this->objectbiz->getDefaultShop($this->user_name);
                        if ( !empty($shop_default) && count($shop_default) ) :
                                $this->id_shop                  = (int)$shop_default['id_shop'];
                        endif;
                  
                        $this->user_conf                        = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                             = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        $this->site                             = isset($this->user['site']) ? $this->user['site'] : 0;
                        $this->ebayUser                         = $this->user_conf['userID'];
                        $this->discount                         = !empty($this->user['discount']) ? $this->user['discount'] : 0;
                        $this->ebaylib                          = new ebaylib( (!empty($this->user_conf['appMode']) && $this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        $this->id_lang                          = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                        $this->language                         = $this->objectbiz->getDefaultISO($this->user_name, $this->id_shop);
                        if ( empty($this->user_conf['token']) ) :
                                $this->_result['msg']['empty_token']    = 1;
                                return $this->_result;
                        endif;
                        $this->ebaylib->token                   = $this->user_conf['token'];
                        $this->ebaylib->core->_template         = !empty($this->user['template']) ? $this->user['template'] : 1;
                        $this->site_object                      = $this->connect->fetch($this->connect->get_ebay_site_by_id($this->site));
                        $this->currency                         = $this->site_object['currency'];
                        $this->country                          = strtoupper($this->site_object['iso_code']);
                        list($this->id_offer_packages)          = $this->connect->fetch($this->connect->get_id_offers_packages('eBay'));
                }
        }
