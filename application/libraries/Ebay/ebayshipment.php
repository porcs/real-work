<?php   echo header('Content-Type: text/html; charset=utf-8');
        if ( !file_exists(dirname(__FILE__).'/ObjectBiz.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( class_exists('ObjectBiz')      == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        if ( class_exists('ebaylib')        == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')        == FALSE ) require dirname(__FILE__).'/connect.php';
    
        class ebayshipment extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $language;
                protected $site;
                protected $id_shop;
                protected $objectbiz;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;
                protected $dir_server;
                protected $_bulk_error = 0;
        
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_SYNCHRONIZE' )     $result['synchronization']      = $value;
                        endwhile;
                        return $result;
                }
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics($parser = '', $type = '', $name = '', $result_insert = '') {
                        if ( empty($parser) ) :
                                return;
                        endif;
            
                        $this->total_response_log                           = !empty($this->total_response_log)     ? $this->total_response_log     : 0;
                        $this->success_response_log                         = !empty($this->success_response_log)   ? $this->success_response_log   : 0;
                        $this->warning_response_log                         = !empty($this->warning_response_log)   ? $this->warning_response_log   : 0;
                        $this->error_response_log                           = !empty($this->error_response_log)     ? $this->error_response_log     : 0;
            
                        $ack                                                = $parser->Ack->__toString();
                        if ( isset($parser->Errors) ) :
                                $error_message_show = array();
                                foreach ( $parser->Errors as $error_message) :
                                        if ( $error_message->ErrorCode->__toString() == '1036' ) :
                                                $this->error_response_log++;
                                                $this->total_response_log++;
                                                return;
                                        endif;
                                        if ( $error_message->ErrorCode->__toString() != '731'
                                            && $error_message->ErrorCode->__toString() != '21919444' ) :
                                                $error_message_show[$error_message->ErrorCode->__toString()] = $error_message->LongMessage->__toString();
                                        endif;
                                endforeach;
                        endif;
                        $orderArray = $this->objectbiz->getOrdersByRef($this->user_name, !empty($parser->OrderID) ? strval($parser->OrderID) : '');
            
                        if ( ($ack == 'Success' || $ack == 'Warning') ) :
                                if ($ack == 'Success') :
                                        $this->success_response_log++;
                                else :
                                        $error_check = 0;
                                        foreach ( $parser->Errors as $error_message) :
                                                if ( $error_message->ErrorCode->__toString() == '21919444' ) :
                                                        $error_check++;
                                                endif;
                                        endforeach;
                                        if ( $error_check > 0 ) :
                                                $this->success_response_log++;
                                                $ack = 'Success';
                                        else :
                                                $this->warning_response_log++;
                                        endif;
                                endif;
                                if ( !empty($orderArray) ) :
                                        $result_insert                                      .= $this->objectbiz->updateOrdersStatusOnly($orderArray);
                                endif;
                                $this->total_response_log++;
                        elseif ( $ack == 'Failure' ) :
                                $this->error_response_log++;
                                $this->total_response_log++;
                        endif;

                        if ( !empty($orderArray) ) :
                                $error_message_show = empty($error_message_show) ? array (''=>'') : $error_message_show;
                        
                                foreach($error_message_show as $ecode=>$emessage) {
                                        $data_insert                                        = array(
                                                "batch_id"                                  => $this->batch_id,
                                                "id_product"                                => isset($orderArray) ? $orderArray[0]['id_orders'] : 0,
                                                "id_site"                                   => $this->site,
                                                "id_shop"                                   => $this->id_shop,
                                                "ebay_user"                                 => $this->ebayUser,
                                                "sku_export"                                => 'Update tracking number',
                                                "name_product"                              => 'Update tracking number',
                                                "response"                                  => $ack,
                                                "type"                                      => $type,
                                                "code"                                      => $ecode,
                                                "message"                                   => html_special($emessage),
                                                "combination"                               => 0, 
                                                "date_add"                                  => date("Y-m-d H:i:s", strtotime("now")),
                                        );
                                        $result_insert                                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                }
                        endif;
                        return $result_insert;
                }
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics_log() {
                        unset($data_insert);
                        $data_insert                                = array(
                                "batch_id"                          => $this->batch_id,
                                "id_site"                           => $this->site,
                                "id_shop"                           => $this->id_shop,
                                "type"                              => 'Shipment',
                                "total"                             => $this->success_response_log + $this->warning_response_log + $this->error_response_log,
                                "success"                           => $this->success_response_log,
                                "warning"                           => $this->warning_response_log,
                                "error"                             => $this->error_response_log + (!empty($error_statistic['error']) ? $error_statistic['error'] : 0),
                                "date_add"                          => date("Y-m-d H:i:s", strtotime("now")),
                        );
                        $this->error_response_log                   = 0;
                        $data['data_insert'][]                      = $data_insert;
                        $result_insert                              = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getJobsCheck() {
                        $this->ebaylib->getJobs($this->site, 'Failed');
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getJobsCheck2() {
                        $this->ebaylib->getJobs($this->site, 'Created');
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function abortJobCheck() {
                        $this->objectbiz->checkDB("ebay_job_task");
                        $jobStatus = array('Failed', 'Created');
                        foreach ($jobStatus as $status) :
                                unset($abort_jobID, $result_insert, $data_set, $where_set);
                                $abort_jobID = $this->getJobsCheck($status);
                                if ( !empty($abort_jobID) && !empty($abort_jobID->jobProfile) && count($abort_jobID->jobProfile) ) :
                                        foreach ( $abort_jobID->jobProfile as $jobID ) :
                                                if ( empty($jobID->completionTime) ) :
                                                        $this->ebaylib->abortJob($this->site, $jobID->jobId->__toString());
                                                        $data_set                       = array(
                                                                'jobStatus'             => 'Aborted',
                                                                'percentComplete'       => '100%',
                                                        );
                                                        $where_set                      = array(
                                                                'jobId'                 => $jobID->jobId->__toString(),
                                                                'fileReferenceId'       => $jobID->inputFileReferenceId->__toString(),
                                                                'ebay_user'             => $this->ebayUser,
                                                        );
                                                        $result_insert                  = $this->objectbiz->updateColumn('ebay_job_task', $data_set, $where_set);
                                                endif;
                                        endforeach;
                                endif;
                        endforeach;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_method() {
                        $methods                                = array('AddFixedPriceItem', 'RelistFixedPriceItem', 'ReviseFixedPriceItem', 'EndFixedPriceItem', 'VerifyAddFixedPriceItem', 'SetShipmentTrackingInfo');
                        foreach ( $methods as $method ) :
                                if ( file_exists($this->dir_name."/".$method.".xml.gz") ) unlink($this->dir_name."/".$method.".xml.gz");
                                if ( file_exists($this->dir_name."/".$method.".zip") ) unlink($this->dir_name."/".$method.".zip");        
                                $this->_result[$method]         = 'No Used';
                        endforeach;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_upload_method() {
                        $methods                                = array('AddFixedPriceItem', 'RelistFixedPriceItem', 'ReviseFixedPriceItem', 'EndFixedPriceItem', 'VerifyAddFixedPriceItem', 'SetShipmentTrackingInfo');
                        $this->_result                          = array();
                        foreach ( $methods as $method ) :    
                                $this->_result[$method]['createJob']    = 'No Used';
                                $this->_result[$method]['jobID']        = 'No Used';
                                $this->_result[$method]['upload']       = 'No Used';
                                $this->_result[$method]['startUpload']  = 'No Used';
                        endforeach;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_download_method() {
                        $methods                                = array('AddFixedPriceItem', 'RelistFixedPriceItem', 'ReviseFixedPriceItem', 'EndFixedPriceItem', 'VerifyAddFixedPriceItem', 'SetShipmentTrackingInfo');
                        $this->_result                          = array();
                        foreach ( $methods as $method ) :    
                                $this->_result[$method]         = 'No Used';
                        endforeach;
                }
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_shipment($shipments) {
                        $this->abortJobCheck();
                        $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/order";
                        $this->export_method();
                        $this->_result['SetShipmentTrackingInfo']   = $this->ebaylib->SetShipmentTrackingInfo($this->site, $shipments, "SetShipmentTrackingInfo", "order", $this->user_name);

                        $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                        if ( $data_table_statistics_log ) :
                                unset($data_insert);
                                $data_insert                        = array(
                                        "batch_id"                  => $this->batch_id,
                                        "id_site"                   => $this->site,
                                        "id_shop"                   => $this->id_shop,
                                        "type"                      => 'Shipment',
                                        "total"                     => 0,
                                        "success"                   => 0,
                                        "warning"                   => 0,
                                        "error"                     => 0,
                                        "date_add"                  => date("Y-m-d H:i:s", strtotime("now")),
                                );
                                $data['data_insert'][]              = $data_insert;
                                $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                        endif;
                        return $this->_result;
                }

  
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_bulk() {
                        $this->dir_name                                     = USERDATA_PATH.$this->user_name."/ebay/order";
                        $files                                              = array(
                                'SetShipmentTrackingInfo'                   => $this->dir_name.'/SetShipmentTrackingInfo.xml.gz',
                        );

                        $this->export_upload_method();

                        foreach ( $files as $type => $file ) :
                        if (file_exists($file)) {
                                if ( strtotime(date("Y-m-d", filemtime($file))) == strtotime(date("Y-m-d", time())) ) :
                                        $this->ebaylib->createUploadJob($this->site, $type);
                                        $this->response                         = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                        		$this->_bulk_error              		= 0;
                                        		$this->_result[$type]['createJob']      = $this->response->ack->__toString();
                                                $this->ebaylib->_jobID                  = $this->response->jobId->__toString();
                                                $this->ebaylib->_fileID                 = $this->response->fileReferenceId->__toString();
                                                $this->fileID                           = $this->response->fileReferenceId->__toString();
                                                $this->_result[$type]['jobID']          = $this->response->jobId->__toString();
                                                createUploadFile :
                                                $this->ebaylib->createUploadFile($this->site, $type, "order", $this->user_name);
                                                $this->response                         = $this->ebaylib->core->_parser;
                                                if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                                        $this->_bulk_error              = 0;
                                                        $this->_result[$type]['upload'] = $this->response->ack->__toString();
                                                        startUploadJob :
                                                        $this->ebaylib->startUploadJob($this->site);
                                                        $this->response                	= $this->ebaylib->core->_parser;
                                                        if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                                                $this->_bulk_error      = 0;
                                                                $this->_result[$type]['startUpload'] = $this->response->ack->__toString();
                                                                $this->objectbiz->checkDB("ebay_job_task");
                                                                $this->ebaylib->getJobStatus($this->site);
                                                                $this->response                     = $this->ebaylib->core->_parser;
                                                                if ( !empty($this->response) &&
                                                                     !empty($this->response->jobProfile->jobId) ) :
                                                                        $data_insert                        = array(
                                                                                'jobId'                 	=> $this->response->jobProfile->jobId->__toString(),
                                                                                'fileReferenceId'           => $this->fileID,
                                                                                'batch_id'                  => $this->batch_id,
                                                                                'ebay_user'                 => $this->ebayUser,
                                                                                'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                                                'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                                                'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                                                'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                                                'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                                        );
                                                                        $data['data_insert'][]              = $data_insert;
                                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                                endif;
                                                        else :
                                                                if ( $this->_bulk_error < 3 ) :
                                                                        $this->_bulk_error++;
                                                                        goto startUploadJob;
                                                                else :
                                                                        $this->_result['msg']['bulk_startupload']     = 1;
                                                                        return $this->_result;
                                                                endif;
                                                        endif;
                                                else :
                                                        if ( $this->_bulk_error < 3 ) :
                                                                $this->_bulk_error++;
                                                     			goto createUploadFile;
                                                        else :
                                                                $this->_result['msg']['bulk_createfile']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                endif;
                                        else :
                                                if ( $this->_bulk_error < 3 ) :
                                                        $this->_bulk_error++;
                                                        $this->abortJobCheck();
                                                        $this->export_bulk();
                                                else :
                                                        $this->_result['msg']['bulk_createjob']     = 1;
                                                        return $this->_result;
                                                endif;
                                        endif;
                                endif;
                        }
                        endforeach;
                        return $this->_result;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_download($percent = '', $date = "now") {
                        $this->objectbiz->checkDB("ebay_job_task");
                        $this->dir_name                                     = USERDATA_PATH.$this->user_name."/ebay/order";
                        $job_task                                           = $this->objectbiz->getJobTask($this->user_name, 'products', $this->ebayUser, 'now');
                        $this->export_download_method();
                        
                        if (!empty($job_task)) :
                        foreach ( $job_task as $task ) :
                                $this->ebaylib->_jobID                      = $task['jobId'];  
                                while ( $task['jobId'] ) :
                                        $this->ebaylib->getJobStatus($this->site);
                                        $this->response                     = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) ) :
                                                $data_insert                        = array(
                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                        'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                        'batch_id'                  => $this->batch_id,
                                                        'ebay_user'                 => $this->ebayUser,
                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                );
                                                $data['data_insert'][]              = $data_insert;
                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                $percentComplete                    = $this->response->jobProfile->percentComplete->__toString();
                                                $jobStatus                          = $this->response->jobProfile->jobStatus->__toString();
                                        endif;
                                        if ( !empty($percent) ) :
                                                $percent->set_running_task($percentComplete);
                                        endif;
                                        if ( (!empty($percentComplete) && $percentComplete == '100.0') || (!empty($jobStatus) && in_array($jobStatus, array('Failed', 'Aborted')))) :
                                        		$task['jobId']      = 0;
                                                $data_insert                        = array(
                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                        'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                        'batch_id'                  => $this->batch_id,
                                                        'ebay_user'                 => $this->ebayUser,
                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                );
                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                startDownloadFile :
                                                $this->ebaylib->startDownloadFile($this->site, $task['jobType'], "order", $this->user_name);
                                                $this->response             = $this->ebaylib->core->_parser;
                                                if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure') )) :
                                                        $this->_bulk_error              = 0;
                                                        $this->_result[$task['jobType']] = $this->response->ack->__toString();
                                                        $this->ebaylib->getDownloadFile($task['jobType'], "order", $this->user_name);
                                                        $this->objectbiz->checkDB("ebay_statistics");
                                                        $this->objectbiz->checkDB("ebay_statistics_log");

                                                        $result_insert = '';
                                                        if ( !empty($this->ebaylib->core->_parser) ) :
                                                                foreach ( $this->ebaylib->core->_parser as $parse ) :
                                                                        $result_insert .= $this->export_statistics($parse, $task['jobType']);
                                                                endforeach;
                                                                $this->objectbiz->transaction($result_insert, true);
                                                                $this->export_statistics_log();
                                                        endif;
                                                else :
                                                        if ( $this->_bulk_error < 3 ) :
                                                                $this->_bulk_error++;
                                                                goto startDownloadFile;
                                    			else :
                                                                $this->_result['msg']['bulk_download']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                endif;
                                                
                                                if(in_array($jobStatus, array('Failed', 'Aborted'))) :
                                                	$this->_result['msg']['job_failure'] = 1;
                                                endif;
                                        else :  sleep(5);
                                        endif;
                                endwhile;
                        endforeach;
                        endif;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getTrackingFromStore() {
                        require_once  DIR_SERVER . '/application/libraries/UserInfo/configuration.php';
                        require_once  DIR_SERVER . '/application/libraries/FeedBiz_Orders.php';
                        
                        $feedBizOrder   = new FeedBiz_Orders(array($this->user_name));
                        $uinfo          = new UserConfiguration(); 
                        $input          = $uinfo->check_site_verify($this->user_id); 
                        $shop           = $input['feed_shop_info']; 
                        
                        if( empty($input['feed_biz']['verified']) || $input['feed_biz']['verified'] != 'verified' ) :
                                $this->_result['msg']['no_verified']     = 1;
                                return $this->_result;
                        elseif ( !isset($shop['url']['shippedorders']) ) :
                                $this->_result['msg']['no_shippedorders_link']     = 1;
                                return $this->_result;
                        endif;

                        $shippedorder_url = $shop['url']['shippedorders'];
                        $conj = strpos($shippedorder_url, '?') !== FALSE ? '&' : '?'; 
                        $input['xml_url'] = $shippedorder_url . $conj . 'fbtoken=' . $input['feed_token'];

                        if(!isset($input['xml_url']) || empty($input['xml_url']) || !isset($input['user_name']) || empty($input['user_name'])){
                                $this->_result['msg']['no_data']     = 1;
                                return $this->_result;
                        }
                        
                        $ebayCarriers = $this->objectbiz->getAllCarriers();
                        
                        $orders = array();
                        $order_ids = array();

                        $xml_string = file_get_contents($input['xml_url']); 

                        // validate xml
                        libxml_use_internal_errors(true);
                        $xml_doc = simplexml_load_string($xml_string);
                        $shop_id_list = array();
                        if(empty($xml_doc)){
                                $xml_string = file_get_contents($input['xml_url']);  
                                $xml_doc = simplexml_load_string($xml_string);
                        }

                        if ($xml_doc) {

                                $xml_dom = new SimpleXMLElement($xml_string);

                                foreach ($xml_dom->Orders->Order as $orderDOM) {
                                        $orders [strval($orderDOM->MPOrderID)] = array(
                                                'MPOrderID' => strval($orderDOM->MPOrderID),
                                                'CarrierID' => strval($orderDOM->CarrierID),
                                                'CarrierName' => strval($orderDOM->CarrierName),
                                                'ShippingNumber' => strval($orderDOM->ShippingNumber),
                                                'ShippingDate' => strval($orderDOM->ShippingDate)
                                        );
                                        $order_ids [] = strval($orderDOM->MPOrderID);
                                }    
                        }
                        
                        if (!empty($orders)) {    
                                $feedBizOrder->updateOrderTracking($this->user_name, $orders);
                        }
                        $row = $uinfo->check_ebay_config($this->user_id);
                        $raworders = $feedBizOrder->getOrdersForShip($this->user_name);
                        if ( $row == 2 ) {
                                $current = new DateTime();
                                $logs = array();
                                $done = array();
                                $orderMPMapping = array();
                                $shipments = array();

                                foreach ($raworders as $channel => $sites) {
                                    if (strpos(strtolower($channel), 'ebay') !== FALSE) {
                                        foreach ($sites as $site_id => $site_orders) {
                                            if ( $site_id == $this->site) {
                                                foreach ($site_orders as $order) {
                                                    $id_shop = $order['id_shop'];
                                                    $id_carrier = $order['id_carrier'];
                                                    $shipments[] = array(
                                                        'OrderID'                   => $order['id_marketplace_order_ref'],
                                                        'ShipmentTrackingNumber'    => $order['tracking_number'],
                                                        'ShippedTime'               => $order['shipping_date'],
                                                        'ShippingCarrierUsed'       => isset($ebayCarriers[$id_shop]) && isset($ebayCarriers[$id_shop][$id_carrier]) ? $ebayCarriers[$id_shop][$id_carrier]['name'] : 'Feedbiz'
                                                    );
                                                    $orderMPMapping[$order['id_marketplace_order_ref']] = $order;
                                                }
                                                //API
                                                if ( !empty($shipments)) {
                                                    $this->_result = $this->export_shipment($shipments);                      
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                        return $this->_result;
                }
        
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        $this->user_name                = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                  = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                     = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        parent::__construct();
                        $this->dir_server               = DIR_SERVER;
                        $this->objectbiz                = new ObjectBiz(array($this->user_name));
                        $this->connect                  = new connect();
                        $shop_default                   = $this->objectbiz->getDefaultShop($this->user_name);
                        $this->id_shop                  = (int)$shop_default['id_shop'];
                        $this->user_conf                = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                     = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        list($this->user_email)         = $this->connect->fetch($this->connect->get_user_mail($this->user_id));
                        $this->site                     = isset($this->user['site']) ? $this->user['site'] : 0;
                        $site_result                    = $this->connect->fetch($this->connect->get_ebay_site_by_id($this->site));
                        $this->site_name                = !empty($site_result) ? $site_result['name'] : '';
                        $this->id_lang                  = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                        $this->language                 = $this->objectbiz->getDefaultISO($this->user_name, $this->id_shop);
                        $this->currency                 = $site_result['currency'];
                        $this->country                  = strtoupper($site_result['iso_code']);
                        $this->ebayUser                 = $this->user_conf['userID'];
                        $this->discount                 = !empty($this->user['discount']) ? $this->user['discount'] : 0;
                        $this->ebaylib                  = new ebaylib( (!empty($this->user_conf['appMode']) && $this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        if ( empty($this->user_conf['token']) ) :
                                $this->_result['msg']['empty_token']    = 1;
                                return $this->_result;
                        endif;
                        $this->ebaylib->token           = $this->user_conf['token'];
                        $this->ebaylib->core->_template = !empty($this->user['template']) ? $this->user['template'] : 1;
                        $this->batch_id                 = uniqid();
                        $this->mode                     = unserialize(base64_decode($this->user_conf['feed_mode']));
                        $result_log                     = $this->objectbiz->checkBatchID($this->batch_id);
                        while( !empty($result_log) ) :
                                $this->batch_id         = uniqid();
                                $result_log             = $this->objectbiz->checkBatchID($this->batch_id);
                        endwhile;
                }
        }
