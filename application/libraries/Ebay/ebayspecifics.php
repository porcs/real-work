<?php   echo header('Content-Type: text/html; charset=utf-8');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( class_exists('ebaylib')   == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')   == FALSE ) require dirname(__FILE__).'/connect.php';
        require dirname(__FILE__).'/../tools.php';
        
        class ebayspecifics extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                public $name = array(0 => array('added'=>true));
                public $value = array(0 => array('added'=>true));
                public $mode = array(0 => array('added'=>true));
                public $insert = array();
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name == 'EBAY_USER' )                 $result['userID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                        endwhile;
                        return $result;
                }
                
                function in_arrayi($needle, $haystack) {
                        return in_array(strtolower($needle), array_map('strtolower', $haystack));
                }

                function parse_specific() {
                        $this->response                                     = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) && $this->response->Ack->__toString() == 'Success' && count($this->response->Recommendations)) :
                                foreach ( $this->response->Recommendations as $recommendations) :
                                        if ( array_key_exists('NameRecommendation', (array)$recommendations) ) :
                                                $id_category                = $recommendations->CategoryID->__toString();
                                                for ( $i = 0; $i < count($recommendations->NameRecommendation); $i++ ) :
                                                        $name               = $recommendations->NameRecommendation[$i]->Name->__toString();
                                                        list($this->group_id)     = $this->connect->fetch($this->connect->get_attributes_group(html_special($name)));
                                                        if ( empty($this->group_id) && !is_numeric($this->group_id) ) :
                                                                unset($data_name);
                                                                $data_name  = array(
                                                                        'name'      => html_special($name),
                                                                );
                                                                $this->connect->add_db('ebay_attributes_group1', $data_name);
                                                                $this->group_id     = $this->connect->insert_id();
                                                        endif;
                                                        
                                                        $validationRules    = $recommendations->NameRecommendation[$i]->ValidationRules;
                                                        $selectionMode      = $validationRules->SelectionMode->__toString();
                                                        
                                                        list($this->mode)         = $this->connect->fetch($this->connect->get_selection_mode($selectionMode));
                                                        if ( empty($this->mode) && !is_numeric($this->mode) ) :
                                                                unset($data_mode);
                                                                $data_mode  = array(
                                                                        'selection_mode'     => $selectionMode,
                                                                );
                                                                $this->connect->add_db('ebay_attributes_mode1', $data_mode);
                                                                $this->mode     = $this->connect->insert_id();
                                                        endif;
                                                        
                                                        unset($data_insert);
                                                        $data_insert = array(
                                                                'id_category'       => $id_category,
                                                                'id_group'          => !empty($this->group_id) ? $this->group_id : 'Error', 
                                                                'id_site'           => $this->id_site, 
                                                                'id_selection_mode' => !empty($this->mode) ? $this->mode : 'Error', 
                                                                'date_add'          => date('Y-m-d H:i:s', strtotime("now")),
                                                        );
                                                        $this->connect->replace_db('ebay_attributes1', $data_insert);
                                                        
                                                        if ( isset($recommendations->NameRecommendation[$i]->ValueRecommendation) && count($recommendations->NameRecommendation[$i]->ValueRecommendation) ) :
                                                                foreach ( $recommendations->NameRecommendation[$i]->ValueRecommendation as $valueRecommendation ) :
                                                                        $id_attribute='';
                                                                        $value              = $valueRecommendation->Value->__toString();
                                                                         
                                                                                list($id_attribute)         = $this->connect->fetch($this->connect->get_attributes_value($this->group_id, html_special($value)));
                                                                              
                                                                                if ( empty($id_attribute) && !is_numeric($id_attribute) ) :
                                                                                        unset($data_value);
                                                                                        $data_value = array(
                                                                                                'id_group'  => !empty($this->group_id) ? $this->group_id : 'Error', 
                                                                                                'value'     => html_special($value),
                                                                                        );
                                                                                        $this->connect->replace_db('ebay_attributes_value1', $data_value);
                                                                                endif;
                                                                        
                                                                endforeach;
                                                        endif;
                                                endfor;
                                        endif;
                                endforeach;
//                                $file = USERDATA_PATH.$this->user_name.'/ebay/temp/0/attribute.txt';
//                                file_put_contents($file, print_r($this->name, true));
                        endif;
                }
                
//                function parse_specific_manual($recommendations,$id_site) {
//                     
//                                        if ( array_key_exists('NameRecommendation', (array)$recommendations) ) :
//                                                $id_category                = $recommendations->CategoryID->__toString();
//                                                for ( $i = 0; $i < count($recommendations->NameRecommendation); $i++ ) :
//                                                        $name               = $recommendations->NameRecommendation[$i]->Name->__toString();
//                                                        list($this->group_id)     = $this->connect->fetch($this->connect->get_attributes_group(html_special($name)));
//                                                        if ( !isset($this->group_arr[html_special($name)]) ) :
//                                                                unset($data_name);
//                                                                $data_name  = array(
//                                                                        'name'      => html_special($name),
//                                                                );
//                                                                $this->connect->add_db('ebay_attributes_group1', $data_name);
//                                                                $this->group_id     = $this->connect->insert_id();
//                                                        endif;
//                                                        
//                                                        $validationRules    = $recommendations->NameRecommendation[$i]->ValidationRules;
//                                                        $selectionMode      = $validationRules->SelectionMode->__toString();
//                                                        
//                                                        list($this->mode)         = $this->connect->fetch($this->connect->get_selection_mode($selectionMode));
//                                                        if ( empty($this->mode) && !is_numeric($this->mode) ) :
//                                                                unset($data_mode);
//                                                                $data_mode  = array(
//                                                                        'selection_mode'     => $selectionMode,
//                                                                );
//                                                                $this->connect->add_db('ebay_attributes_mode1', $data_mode);
//                                                                $this->mode     = $this->connect->insert_id();
//                                                        endif;
//                                                        
//                                                        unset($data_insert);
//                                                        $data_insert = array(
//                                                                'id_category'       => $id_category,
//                                                                'id_group'          => !empty($this->group_id) ? $this->group_id : 'Error', 
//                                                                'id_site'           => $id_site, 
//                                                                'id_selection_mode' => !empty($this->mode) ? $this->mode : 'Error', 
//                                                                'date_add'          => date('Y-m-d H:i:s', strtotime("now")),
//                                                        );
//                                                        $this->connect->replace_db('ebay_attributes1', $data_insert);
//                                                        
//                                                        if ( isset($recommendations->NameRecommendation[$i]->ValueRecommendation) && count($recommendations->NameRecommendation[$i]->ValueRecommendation) ) :
//                                                                foreach ( $recommendations->NameRecommendation[$i]->ValueRecommendation as $valueRecommendation ) :
//                                                                        $id_attribute='';
//                                                                        $value              = $valueRecommendation->Value->__toString();
//                                                                         
//                                                                                list($id_attribute)         = $this->connect->fetch($this->connect->get_attributes_value($this->group_id, html_special($value)));
//                                                                              
//                                                                                if ( empty($id_attribute) && !is_numeric($id_attribute) ) :
//                                                                                        unset($data_value);
//                                                                                        $data_value = array(
//                                                                                                'id_group'  => !empty($this->group_id) ? $this->group_id : 'Error', 
//                                                                                                'value'     => html_special($value),
//                                                                                        );
//                                                                                        $this->connect->replace_db('ebay_attributes_value1', $data_value);
//                                                                                endif;
//                                                                        
//                                                                endforeach;
//                                                        endif;
//                                                endfor;
//                                        endif;
//                                
//                }
                
                
                function specific_manual_init($clear=false){
                    if($clear){
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_group1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_value1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_mode1');
                        $this->connect->create_table("ebay_attributes");
                        $this->connect->create_table("ebay_attributes_group");
                        $this->connect->create_table("ebay_attributes_value");
                        $this->connect->create_table("ebay_attributes_mode");
                    }
                    $q = $this->connect->get_attributes_all_groups();
                    while($g = $this->connect->fetch($q)){
                        $data_name = $g['name'];
                        $enc = md5(html_special(strtolower($data_name)));
                        if($enc!='d41d8cd98f00b204e9800998ecf8427e'){
                            $data_name_key = $enc;
                        }else{
                            $data_name_key = md5(html_special(($data_name)));
                        }  
                        $this->name[$data_name_key] =array('id'=> $g['id_group'],'id_site'=>$g['id_site'],'name'=>$g['name'],'added'=>true); 
                    }
                    
                    $q = $this->connect->get_attributes_all_modes();
                    while($g = $this->connect->fetch($q)){
                        $this->mode[($g['selection_mode'])] =  array('id'=>$g['id_selection_mode'],'added'=>true) ;
                    }
                     
                    $q = $this->connect->get_attributes_all_values();
                    while($g = $this->connect->fetch($q)){
                        $gen_value          = implode('_#_', array( $g['value'], $g['id_group']));
                        
                        $data_name = $gen_value;
                        $enc = md5(html_special(strtolower($data_name)));
                        if($enc!='d41d8cd98f00b204e9800998ecf8427e'){
                            $data_name_key = $enc;
                        }else{
                            $data_name_key = md5(html_special(($data_name)));
                        } 
                        $key = $data_name_key; 
                        $this->value[$key] =  array('val'=>$gen_value,'id'=>$g['id_attribute'],'id_site'=>$g['id_site'],'added'=>true) ;
                    } 
                    
                }
                function parse_specific_manual($recommendations,$id_site) 
                {
                   if ( array_key_exists('NameRecommendation', (array)$recommendations) ) :
                        $id_category                    = $recommendations->CategoryID->__toString();
                        for ( $i = 0; $i < count($recommendations->NameRecommendation); $i++ ) :
                                $data_name              = $recommendations->NameRecommendation[$i]->Name->__toString();
                                if(md5(html_special(strtolower($data_name)))!='d41d8cd98f00b204e9800998ecf8427e'){
                                    $data_name_key = md5(html_special(strtolower($data_name)));
                                }else{
                                    $data_name_key = md5(html_special(($data_name)));
                                } 
//                                $this->c++;
//                                echo $this->c.' '.$data_name_key.' '.html_special($data_name).' 1'.html_special(($data_name)).' 2'.(strtolower($data_name)).'<br>';
//                                if (!$this->in_arrayi($data_name, $this->name)){
                                    if(!isset($this->name[$data_name_key])){
                                        $size = sizeof($this->name);
                                        $this->name[$data_name_key]   =  array('id'=>$size,'id_site'=>$id_site,'name'=>html_special($data_name),'added'=>false);
                                    }
//                                }

                                $validationRules        = $recommendations->NameRecommendation[$i]->ValidationRules;
                                $selectionMode          = $validationRules->SelectionMode->__toString();
                                $enable = 1;
                                if(isset($validationRules->VariationSpecifics) && $validationRules->VariationSpecifics->__toString() == 'Disabled'){
                                    $enable=0;
                                }
//                                if (!in_array($selectionMode, $this->mode))
                                    if(!isset($this->mode[$selectionMode])){
                                        $size = sizeof($this->mode);
                                        $this->mode[$selectionMode]   =  array('id'=>$size,'added'=>false) ; 
                                    }
                                $id_group = $this->name[$data_name_key]['id'];
                                $this->insert[] = array(
                                        'id_category'   => $id_category,
                                        'id_site'        => $id_site,
                                        'id_group'      => $id_group, 
                                        'id_selection_mode' => $this->mode[$selectionMode]['id'],
                                        'date_add'          => date('Y-m-d H:i:s', strtotime("now")),
                                        'enable_validation'        => $enable,
                                );
                                if ( isset($recommendations->NameRecommendation[$i]->ValueRecommendation) && count($recommendations->NameRecommendation[$i]->ValueRecommendation) ) :
                                        foreach ( $recommendations->NameRecommendation[$i]->ValueRecommendation as $valueRecommendation ) :
                                                $gen_value          = implode('_#_', array($valueRecommendation->Value->__toString(), $id_group));
                                                $data_name = $gen_value;
                                                $enc = md5(html_special(strtolower($data_name)));
                                                if($enc!='d41d8cd98f00b204e9800998ecf8427e'){
                                                    $data_name_key = $enc;
                                                }else{
                                                    $data_name_key = md5(html_special(($data_name)));
                                                } 
                                                $key = $data_name_key; 
                                                if(!isset($this->value[$key])){
                                                    $size = sizeof($this->value);
                                                    $this->value[$key]    = array('val'=>$gen_value,'id'=>$size,'id_site'=>$id_site,'added'=>false) ; 
                                                }
//                                                if (!in_array($gen_value, $this->value))
//                                                        $this->value[]    = $gen_value;

                                        endforeach;
                                endif;
                        endfor;
                    endif;
                }
                
                function get_specific_manual($fp,$wr=false){
//                        $this->query                                        = $this->connect->truncate_table('ebay_attributes1');
//                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_group1');
//                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_value1');
//                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_mode1');
//                        $this->connect->create_table("ebay_attributes");
//                        $this->connect->create_table("ebay_attributes_group");
//                        $this->connect->create_table("ebay_attributes_value");
//                        $this->connect->create_table("ebay_attributes_mode");
//                        unset($this->name[0], $this->value[0], $this->mode[0]);
//                        print_r($this->insert);
                        $transactions = '';
//                        $this->connect->select_query('START TRANSACTION');
                        foreach ( $this->name as $key => $name ) :
                                if($name['added'])continue;
                                $this->name[$key]['added']=true;
                                unset($data_name);
                                $data_name = array(
                                        'id_group'  => $name['id'],
                                        'name'      => ($name['name']),
                                        'id_site'  => $name['id_site'],
                                );
                                
                                $sql = $this->connect->replace_db('ebay_attributes_group1', $data_name,$wr);
                                if($wr) fwrite($fp,$sql.";\n");
                                 
                        endforeach;
//                        print_r($this->name);
//                        print_r($data_name);
//                        exit;
                        
                        foreach ( $this->value as $key => $values ) :
                                if($values['added'])continue;
                                $this->value[$key]['added']=true;
                                unset($data_value);
                                list($value, $group)    = explode('_#_', $values['val']);
                                $data_value = array(
                                        'id_attribute' => $values['id'],
                                        'id_group'  => $group,
                                        'value'     => html_special($value),
                                        'id_site'  => $values['id_site'],
                                );
                                $sql = $this->connect->replace_db('ebay_attributes_value1', $data_value,$wr);
                                if($wr) fwrite($fp,$sql.";\n");
                        endforeach;
                        
                        foreach ( $this->mode as $key => $modes ) :
                                if($modes['added'])continue;
                                $this->mode[$key]['added']=true;
                                unset($data_mode);
                                $data_mode = array(
                                        'selection_mode'     => $key,
                                );
                                $sql = $this->connect->replace_db('ebay_attributes_mode1', $data_mode,$wr);
                                if($wr) fwrite($fp,$sql.";\n");
                        endforeach;
                        
                        foreach ( $this->insert as $key => $data_insert ) :
                                $sql = $this->connect->replace_db('ebay_attributes1', $data_insert,$wr);
                                if($wr) fwrite($fp,$sql.";\n");
                        endforeach;
                         unset($this->insert);
                         $this->insert=array();
//                        $this->connect->select_query('COMMIT');
                }
                
                function get_specific($skip=false) {
                    
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_group1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_value1');
                        $this->query                                        = $this->connect->truncate_table('ebay_attributes_mode1');
                        $this->connect->create_table("ebay_attributes");
                        $this->connect->create_table("ebay_attributes_group");
                        $this->connect->create_table("ebay_attributes_value");
                        $this->connect->create_table("ebay_attributes_mode");
                         
                    if($skip){return;}
                        while ( list($this->id_site)                        = $this->connect->fetch($this->site_configuration) ) :
                                unset($category);
                                $query                                      = $this->connect->get_categories_ebay_by_site($this->id_site);
                                while ( list($this->id_category)            = $this->connect->fetch($query) ) :
                                        $category[]                         = $this->id_category;
                                endwhile;
                                
                                if ( isset($category) && count($category) ) :
                                        $categories                         = array_chunk($category, 250, true);
                                        foreach ( $categories as $category_chunk ) :
                                                unset($category);
                                                $category['_Repeat']        = $category_chunk;
                                                if ( isset($category) ) :
                                                        $this->ebaylib->GetCategorySpecifics($this->id_site, $category);
                                                        $this->parse_specific();
                                                endif;     
                                        endforeach;
                                endif;   
                        endwhile;

                        return $this->result['group'] = '';
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->dir_server                       = DIR_SERVER;
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->connect                          = new connect();
                        $this->user_configuration               = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->site                             = isset($this->user_configuration['site']) ? $this->user_configuration['site'] : 0;
                        $this->ebayUser                         = $this->user_configuration['userID'];
                        $this->ebaylib                          = new ebaylib( ($this->user_configuration['appMode'] == 1) ? array(0) : array(1) );
                        $this->ebaylib->token                   = $this->user_configuration['token'];
                        $this->site_configuration               = $this->connect->get_ebay_site();
                        $this->ebaylib->core->_template         = 1;
                }
        }
