<?php   

require_once dirname(__FILE__).'/../../db.php';
require_once dirname(__FILE__).'/../table.config.php';
require_once dirname(__FILE__).'/../../../../libraries/ci_connector.php';

trait patchModel
{
    public function get()
    {
        return $this->getTasks();
    }
    
    public function set()
    {
        return $this->setTasks();
    }
}