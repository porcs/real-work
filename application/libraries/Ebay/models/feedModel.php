<?php   

require_once dirname(__FILE__).'/patchModel.php';

abstract class feedModel 
{
    private $data;
    private $project;
    
    abstract public function get();
    abstract public function set();
    
    public function __construct($project, &$data, $debug)
    {
        $this->data     =& $data;
        $this->project  = $project;
        $this->debug    = $debug;
    }



    ////////////////////////////////////////////////////////////////////////////
    public function __call($function, $parameter)
    {
        if ( $function == 'getTasks' || $function == 'setTasks') {
            return call_user_func(
                array($this, 'services'), 
                $this->getProject()
            );
        }
    }



    ////////////////////////////////////////////////////////////////////////////
    private function getData()
    {
        return $this->data;
    }



    ////////////////////////////////////////////////////////////////////////////
    private function getProject()
    {
        return $this->project;
    }



    ////////////////////////////////////////////////////////////////////////////
    private function showlog($callback)
    {
        $debug = function($value) 
        {
            echo '<BR>   Root:: '.$value[1];
            echo '<BR>  Child:: '.$value[2];
            echo '<BR> Detail:: '.$value[3];
            echo '<BR> Expect:: '.print_r($value[0], true);
            echo '<BR>----------------------------------------<BR><BR><BR>';
        };

        array_walk($callback, $debug);
    }



    ////////////////////////////////////////////////////////////////////////////
    private function getConfig($common = null)
    {
        //function
        return function($name) use ($common)
        {
            ////////////////////////////////////////////////////////////////////
            //function
            $getPath = function($name = null, $ext = '.ini') use ($common)
            {
                $path      = Configuration::$path[$common];
                $pattern   = '%s%s%s%s';
                $file_path = sprintf($pattern, DIR_SERVER, $path, $name, $ext);
                $real_path = realpath($file_path);

                return file_exists($real_path) ? $real_path : null;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $getFile = function($file = null)
            {                
                return file_exists($file) ? parse_ini_file($file, true) : null;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $valid = function($config, $xsd = array())
            {
                //function
                $validate = function($value, $key) use (&$config) 
                {
                    if ( empty($config[$value])) {
                        $config = null;
                    }
                };

                array_walk(array_flip($xsd), $validate);

                return $config;
            };



            ////////////////////////////////////////////////////////////////////
            $conf = function() use ($common, $name, $getPath, $valid, $getFile)
            {
                if ( !empty($name)) {
                    $file = $getPath($name);

                    switch ($common) 
                    {
                        case 'services' :
                            $xsd = array('services'  => 'array');
                            return $valid($getFile($file), $xsd);

                        case 'tasks' :
                            $xsd = array('config'  => 'array');
                            $prepare = $valid($getFile($file), $xsd);
                            return $prepare['config'];
                    }

                }
                
                return array();
            };



            if ($this->debug) {
                $this->showlog(array(
                    array(
                        $getPath($name),
                        'getConfig',
                        'getPath',
                        'get path from directory.'
                    ),
                    array(
                        $conf(),
                        'getConfig',
                        'conf',
                        'last result from config file.'
                    )
                ));
            }
            
            return $conf();
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function getCallback($callback = array())
    {
        $callback['image'] = $this->images($this->getConfig('tasks'));
        $callback['task']  = $this->task();
        
        return $callback;
    }



    ////////////////////////////////////////////////////////////////////////////
    private function services($name = null)
    {
        $config  = $this->getConfig('services');
        $data    = $config($name); 
        $service = $this->service();
        $filter  = $this->filters($data['services']);

        if ( isset($data['services'])) {
            //function
            $services = function(&$service, $service_name) use ($data)
            {
                if ( array_key_exists($service_name, $data)) {
                    $service = $data[$service_name];
                }
            };

            array_walk($data['services'], $services);
            array_walk($data['services'], $service);
            
            $filter($data);
        }

        return $data['services'];
    }



    ////////////////////////////////////////////////////////////////////////////
    private function service($db = array())
    {
        $data        = $this->getData();
        $connector   = $this->dbConnector($data, $db);
        $callback    = $this->getCallback();
        $supervisors = $this->supervisors($db, $data, $callback);

        //function
        return function(&$build, $key) use ($supervisors)
        {
            $supervisor = $supervisors($build);
            $build = $supervisor();
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function filters($services)
    {
        //function
        return function(&$data) use ($services)
        {
            $filter = function($service, $key) use ($services, &$data)
            {
                if ( !$services[$key]) {
                    unset($data['services'][$key]);
                }
            };
            
            $loop = $data['services'];
            array_walk($loop, $filter);
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function supervisors(&$db, $data, $callback)
    {
        return function($build, $fn = array()) use (&$db, $data, $callback)
        {
            ////////////////////////////////////////////////////////////////////
            if ( !empty($build['environments'])) {
                $name = $build['name'];
                //function
                $environments = function($env, $key) use ($name, &$db, &$data)
                {
                    if ( !empty($env) && is_numeric($key)) {
                        list($in, $env) = array_pad(
                            explode('=', $env), 2, ''
                        );
                        
                        $db[$name]['enviroments'][$key][] = $env;
                        $data[trim($in)][] = ltrim($env);

                    } else if ( isset($env)) {
                        $data[$key] = &$db[$name]['enviroments'][$key];
                        $data[$key] = $env;
                    }
                };

                array_walk($build['environments'], $environments);
            }



            ////////////////////////////////////////////////////////////////////
            if ( !empty($build['depends_on'])) {
                //function
                $depends_on = function($name) use ($db, &$data)
                {
                    if ( !empty($db[$name]['enviroments'])) {
                        $env  = $db[$name]['enviroments'];
                        $data = array_merge($data, $env);
                    }
                };

                array_walk($build['depends_on'], $depends_on);
            }



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['implodes'] = function($args) use (&$data)
            {   
                //function
                return function($value) use (&$data, $args)
                {
                    if ( $args['mode'] == 'IN' && isset($data[$value])) {
                        $pattern      = "('%s')";
                        $string       = implode("', '", $data[$value]);
                        $data[$value] = sprintf($pattern, $string);
                    }
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['iterators'] = function($args, &$callback) use (&$data)
            {
                //function
                return function($value, $key) use (&$data, $args, &$callback) 
                {
                    $data[$key][] = $value;
                    $callback[$key] = $key;

                    if ( $args['mode'] == 'LOOP') {
                        $supervisor =& $data['supervisor'];
                        $supervisor['count'] = count($data[$key]);
                        $supervisor['data'][$key] = 0;
                    }
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['recursives'] = function($args, &$callback = '') use ($fn)
            {
                $iterator = $fn['iterators']($args, $callback);
                //function
                return function($value, $key) use ($iterator)
                {
                    if ( isset($value) && !is_array($value)) {
                        return $iterator($value, $key);
                    }

                    array_walk($value, $iterator);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['leaf'] = function($value, $key) use (&$data)
            {
                return $data = array_merge($data, array($key => $value));
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['from'] = function($values, $mode) use ($fn)
            {
                $args = array(
                    'mode'  => strtoupper($mode),
                    'count' => count(current($values)),
                );

                switch ( strtoupper($mode))
                {
                    case 'LOOP' :
                        $recursive = $fn['recursives']($args);
                        array_walk_recursive($values, $recursive);
                        break;

                    case 'IN' :
                        $callback  = array();
                        $recursive = $fn['recursives']($args, $callback);
                        $implode   = $fn['implodes']($args);

                        array_walk_recursive($values, $recursive);
                        array_walk($callback, $implode);
                        break;

                    default :
                        array_walk_recursive($values, $fn['leaf']);
                        break;
                }
            };



            ////////////////////////////////////////////////////////////////////
            if ( !empty($build['values_from'])) {
                //function
                $values_from = function($name, $key) use ($db, $fn)
                {
                    if ( !empty($db[$name]['values'])) {
                        $fn['from']($db[$name]['values'], $key);
                    }
                };

                array_walk($build['values_from'], $values_from);
            }



            ////////////////////////////////////////////////////////////////////
            if ( !empty($data['post']['expect'])) {
                //function
                $expect = $data['post']['expect'];
                $data = array_merge($expect, $data);
            }



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['env'] = function($save) use ($build, &$db)
            {
                if ( !empty($save)) {
                    //function
                    $environments = function($value, $key) use ($build, &$db)
                    {
                        if ( !empty($value)) {
                            $db[$build['name']]['values'][$key] = $value;
                        }
                    };

                    array_walk($save, $environments);
                }

                return $save;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['database'] = function() use ($build, $db)
            {
                if ( isset($build['database'])) {
                    switch ( strtolower($build['database']))
                    {
                        case 'config' :
                            return $db['ci_connector'];

                        case 'user' :
                            return $db['connector'];
                    }
                }

                return false;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['query'] = function($sql = null) use ($build, $fn)
            {
                if ($db = $fn['database']()) {
                    // transaction or normal query
                    switch ( strtolower($build['build']))
                    {
                        case 'transaction' :
                            return $db->db_exec($sql,false,true,true,true,false);
                            
                        case 'tracking' :
                            $db->db_query_result_str($sql);
                            return $db->db_changes() == -1 ? false : true;

                        default :
                            return $db->db_query_string_fetch($sql);
                    }
                }

                return array();
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['option'] = function($res = array()) use ($build)
            {
                if ( !empty($res) ) {
                    // get the current array result 
                    switch ( strtolower($build['build']))
                    {
                        case 'row' :
                            return current($res);

                        case 'field' :
                            return current(array_chunk(current($res), 1, true));

                        case 'left' :
                            return current(array_chunk(current($res), 1));
                    }
                }

                return $res;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['builds'] = function($sqlQuery) use ($build, $fn)
            {
                if ( !empty($build['build'])) {
                    //function
                    $run = function(&$entry) use ($fn)
                    {
                        $entry = $fn['option']($fn['query']($entry));
                    };

                    array_walk($sqlQuery, $run);
                }

                return $fn['env']($sqlQuery);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['override'] = function($res = array()) use ($build, &$data)
            {
                $name = $build['name'];

                if ( !empty($data['post']['services'][$name]['image'])) {
                    $image = $data['post']['services'][$name]['image'];
                    //function
                    $filter = function($value, $key) use (&$res)
                    {
                        switch (strtolower($key))
                        {
                            case 'where' : case 'orderby' : 
                            case 'limit' : case 'offset' :
                                $res[strtolower($key)] = $value;
                                break;
                        }
                    };

                    array_walk($image, $filter);
                }

                return $res;
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['get_values'] = function() use ($build, &$data)
            {
                $results = array(array());
                
                if ( !empty($data['post']['services'])) {
                    $services = $data['post']['services'];
                    $name = $build['name'];

                    if (!empty($services[$name]['values']['data']) &&
                        !empty($services[$name]['values']['format']) &&
                        $services[$name]['values']['format'] == 'update' ) {
                        $results = $services[$name]['values']['data'];
                    }
                }
                
                return $results;
            };





            ////////////////////////////////////////////////////////////////////
            //function
            $fn['sql'] = function($conf) use ($build, &$data, $callback, $fn)
            {
                $values = $fn['get_values']();
                
                //function
                $sql = function(&$r) use ($conf, $build, &$data, $callback, $fn)
                {
                    $combine = array_merge($data, $r);
                    $supervisors = $callback['task'](
                        $conf, 
                        $fn['database'](), 
                        $combine, 
                        $build
                    );
                    
                    $r = $supervisors();
                };

                array_walk($values, $sql);
                
                return $values;
            };
            



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['image'] = function(&$rep) use ($build, &$data, $callback, $fn)
            {
                $name = $build['image'];
                $rep = array_merge($data, $rep);
                $rep = $callback['image']($name, $fn);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['images'] = function($repos, $res = array()) use ($build, $fn)
            {
                if ( isset($build['image'])) {
                    $res =& $repos;
                    array_walk($repos, $fn['image']);
                }

                return $res;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['dispatch'] = function(&$repo) use (&$data)
            {
                //function
                $assign = function(&$value, $key) use (&$data, &$repo)
                {
                    if ( isset($data[$key][$value])) {
                        $repo[$key] = $data[$key][$value];
                        $value++;
                    }
                };

                array_walk($data['supervisor']['data'], $assign);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['dispatches'] = function($repos) use (&$data, $fn)
            {
                if ( isset($data['supervisor'])) {
                    array_walk($repos, $fn['dispatch']);
                }

                return $repos;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['prepare'] = function($repos = array()) use (&$data, $build)
            {
                if ( isset($data['supervisor'])) {
                    if ( isset($build['fill'])) {
                        return array_fill_keys($data[$build['fill']], 
                            array()
                        );
                    }
                    
                    return array_pad($repos, $data['supervisor']['count'], 
                        array()
                    );
                } 

                return array($data);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['arrange'] = function($pass, &$scope) use ($build)
            {
                //function
                return function($value, $key) use ($build, $pass, &$scope)
                {
                    if ( !empty($build['output'][$key])) {
                        $pass = $build['output'][$key];
                    }
                        
                    switch ($pass)
                    {
                        case 'group' :
                            return $scope[$key][] = $value;

                        case 'group-key' :
                            if ( empty($scope[$key][$value])) {
                                return $scope[$key][$value] = 1;
                            } 

                            return $scope[$key][$value]++;
                    } 
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['arranges'] = function(&$scope) use ($build, $fn)
            {
                //function
                return function($res, $key) use ($build, &$scope, $fn)
                {
                    $pass = false;
                    
                    if ( array_key_exists('*', $build['output'])) {
                        $pass = $build['output']['*'];
                    }

                    switch ($pass)
                    {
                        case 'group' : case 'group-key' :
                            $output = $fn['arrange']($pass, $scope);
                            array_walk_recursive($res, $output);
                            $res = array($res);
                            break;
                        
                        case 'list' :
                            $scope = $res;
                            break;

                        default :
                            $scope[$key] = $res;
                            break;
                        
                    }
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['crop'] = function($result, $scope = array()) use ($build, $fn)
            {
                if ( isset($build['output']) && !empty($build['build'])) {
                    $output = $fn['arranges']($scope);
                    array_walk($result, $output);
                    return $scope;
                }

                return $result;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['combine'] = function(&$scope)
            {
                //function
                return function($res) use (&$scope)
                {
                    //function
                    $list = function($value) use (&$scope)
                    {
                        $scope[] = $value;
                    };

                    array_walk($res, $list);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['combines'] = function($res, $scope = array()) use ($build, $fn)
            {
                if ( !empty($build['combine']) && !empty($build['build'])) {
                    $output = $fn['combine']($scope);
                    array_walk($res, $output);
                    $res = array($scope);
                } 

                return $res;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['sizeof'] = function($res) use ($build)
            {
                if ( !empty($build['sizeof']) && !empty($build['build'])) {
                    $res = !empty($res) ? sizeof($res) : 0;
                } 

                return $res;
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $supervisor = function() use ($fn)
            {
                $repos   = $fn['prepare']();
                $repos   = $fn['dispatches']($repos);
                $query   = $fn['images']($repos);
                $result  = $fn['builds']($query);
                $result  = $fn['combines']($result);
                $result  = $fn['crop']($result);
                $result  = $fn['sizeof']($result);
                
                if ($this->debug) {
                    $this->showlog(array(
                        array(
                            $fn['prepare'](),
                            'supervisors',
                            'prepare',
                            'prepare the data.'
                        ),
                        array(
                            $query,
                            'supervisors',
                            'images',
                            'get sql query.'
                        ),
                        array(
                            $fn['builds']($query),
                            'supervisors',
                            'builds',
                            'response from query.'
                        ),
                        array(
                            $fn['combines']($fn['builds']($query)),
                            'supervisors',
                            'combines',
                            'combines the data.'
                        ),
                        array(
                            $fn['crop']($fn['combines']($fn['builds']($query))),
                            'supervisors',
                            'supervisor',
                            'last result from call service.'
                        ),
                    ));
                }
                
                return $result;
            };

            return $supervisor;
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function task()
    {
        //function
        return function($config, $db, $data, $build, $sql = null)
        {
            $syntax = array(
                'statement', 'column', 'from',
                'where', 'groupby', 'orderby',
                'limit', 'offset', 'values',
                'table', 'set'
            );

            $fields = array_flip($syntax);



            ////////////////////////////////////////////////////////////////////
            //function
            $getData = function($method) use ($config)
            {
                //function
                return function($res = array()) use ($config, $method) 
                {
                    if ( !empty($config[$method])) {
                        $res = $config[$method];
                    }

                    return $res;
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['statement'] = function() use ($getData)
            {
                $function = $getData('statement');
                $override = $function();
                //function
                return function($res = null) use ($override)
                {
                    if ( !empty($override)) {
                        return Configuration::$sql_templates[$override];
                    }

                    return $res;
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['column'] = function() use ($getData)
            {
                $function = $getData('column');
                $override = $function();
                //function
                return function($res = '*') use ($override)
                {
                    if ( !empty($override)) {
                        //function
                        $combine = function(&$value, $key) 
                        {
                            if ( !is_numeric($key)) {
                               $value = sprintf('%s AS %s', $value, $key);
                            }
                        };

                        array_walk($override, $combine);
                        $res = implode(', ', $override);
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['from'] = function() use ($getData)
            {
                $function = $getData('from');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        //function
                        $combine = function(&$value) use (&$res) 
                        {
                            $value = sprintf('%s %s', 
                                $res = empty($res) ? 'FROM' : ',',
                                $value
                            );
                        };

                        array_walk($override, $combine);
                        $res = implode('', $override);
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['table'] = function() use ($getData)
            {
                $function = $getData('table');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        return $override;
                    }

                    return $res;
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['where'] = function() use ($getData)
            {
                $function = $getData('where');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        //function
                        $combine = function(&$value, $key) use (&$res) 
                        {
                            $query = '';
                            $patterns = '%s %s %s "%s" ';
                            //separate the current array to variables
                            //set the current key to variables
                            //set the current assign operator to variables 
                            //as (LIKE, IN <, >, <=, >=, =, !=)
                            //set the current logic operator to variables 
                            //as (OR, AND)
                            list($key, $assign, $logic) = array_pad(
                                explode(' ', $key), 3, '');

                            //set where logic as (OR, AND)
                            switch (strtoupper($logic))
                            {
                                case 'OR' : 
                                    $logic = strtoupper($logic);
                                    break;

                                default : 
                                    $logic = 'AND';
                            }

                            $assign = strtoupper($assign);
                            //set where option as (LIKE, IN <, >, <=, >=, =, !=)
                            switch ($assign)
                            {
                                case '<'  : case '>'  : case '='  : 
                                case '<>' : case '<=' : case '>=' : 
                                case '!=' : case 'LIKE' :
                                    $assign = $assign;
                                    break;

                                case 'IN' : case 'NOT_IN' :
                                    $assign = str_replace('_', ' ', $assign);
                                    $patterns = '%s %s %s %s ';
                                    break;

                                case 'FIELD' :
                                    $assign = '=';
                                    $patterns = '%s %s %s %s ';
                                    break;

                                default : 
                                    $assign = '=';
                            }

                            if ( !empty($key) && !is_numeric($key)) {
                                $query = sprintf($patterns, 
                                    $res = empty($res) ? 'WHERE' : $logic,
                                    $key,
                                    $assign,
                                    $value
                                );
                            }

                            $value = $query;
                        };

                        array_walk($override, $combine);
                        $res = implode('', $override);
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['groupby'] = function() use ($getData)
            {
                $function = $getData('groupby');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        //function
                        $combine = function(&$value) use (&$res) 
                        {
                            $value = sprintf('%s %s', 
                                $res = empty($res) ? 'GROUP BY' : ',',
                                $value
                            );
                        };

                        array_walk($override, $combine);
                        $res = implode('', $override);
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['orderby'] = function() use ($getData)
            {
                $function = $getData('orderby');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        //function
                        $combine = function(&$value, $key) use (&$res) 
                        {
                            $value = sprintf('%s %s %s', 
                                $res = empty($res) ? 'ORDER BY' : ',',
                                $key,
                                $value
                            );
                        };

                        array_walk($override, $combine);
                        $res = implode('', $override);
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['limit'] = function() use ($getData)
            {
                $function = $getData('limit');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        $res = sprintf('%s %s', 
                            $res = 'LIMIT',
                            $override
                        );
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['offset'] = function() use ($getData)
            {
                $function = $getData('offset');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                    if ( !empty($override)) {
                        $res = sprintf('%s %s', 
                            $res = 'OFFSET',
                            $override
                        );
                    }

                    return trim($res);
                };
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['set'] = function() use ($getData)
            {
                $function = $getData('set');
                $override = $function();
                //function
                return function($res = '') use ($override)
                {
                     if ( !empty($override)) {
                        //function
                        $combine = function(&$value, $key) use (&$res) 
                        {
                            $value = sprintf('%s %s = "%s"', 
                                $res = empty($res) ? 'SET' : ',',
                                $key,
                                $value
                            );
                        };

                        array_walk($override, $combine);
                        $res = implode('', $override);
                    }

                    return trim($res);
                };
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['get_values'] = function($results = array()) use ($data, $build) 
            {
                if ( !empty($data['post']['services'])) {
                    $services = $data['post']['services'];
                    $name = $build['name'];

                    if (!empty($services[$name]['values']['data']) &&
                        !empty($services[$name]['values']['format']) &&
                        $services[$name]['values']['format'] == 'insert' ) {
                        $results = $services[$name]['values']['data'];
                    }
                }

                return $results;
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['set_values'] = function($data, &$sql) use ($db)
            {
                //function
                return function($value) use ($data, &$sql, $db)
                {
                    $key = strtolower(str_replace(":", '', $value));
                    $key = str_replace("%", '', $key);
                    $value = str_replace("%", '', $value);

                    if ( array_key_exists($key, $data)) {
                        $sql = str_replace(
                            $value, 
                            $db->escape_str($data[$key]),
                            $sql
                        );
                    }
                };
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['set_where'] = function($data) use ($db)
            {
                //function
                return function(&$value) use ($data, $db)
                {
                    $key = strtolower(str_replace(":", '', $value));
                    $key = str_replace("%", '', $key);
                    
                    if ( array_key_exists($key, $data)) {
                        $value = $db->escape_str($data[$key]);
                    }
                };
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['value'] = function($override) use ($data, $fn)
            {
                //function
                return function(&$value) use ($override, $data, $fn) 
                {
                    $combine    = array_merge($data, $value);
                    $sql        = '"'.implode('", "', $override).'"';
                    $setting    = $fn['set_values']($combine, $sql);
                    array_walk($override, $setting);
                    $value      = sprintf('(%s)', $sql);
                };
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['values'] = function() use ($getData, $fn)
            {
                $function = $getData('values');
                $override = $function();
                $values   = $fn['get_values']();
                $setting  = $fn['value']($override);
                //function
                return function($res = '') use ($override, $values, $setting)
                {
                    if ( !empty($override) && !empty($values)) {
                        //function
                        $combine = function(&$value) use (&$res) 
                        {
                            $value = sprintf('%s %s', 
                                $res = empty($res) ? 'VALUES' : ',',
                                $value
                            );
                        };

                        array_walk($values, $setting);
                        array_walk($values, $combine);

                        $res = implode('', $values);
                    }

                    return trim($res);
                };
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['combineWhere'] = function() use ($fn, $config, $data, &$sql)
            {
                if ( !empty($config['where'])) {
                    //function
                    $setting = $fn['set_values']($data, $sql);
                    array_map($setting, $config['where']);
                }

                return $sql;
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['combineString'] = function() use ($fn, $config, $data, &$sql)
            {
                preg_match('/::[A-Z0-9]*::/', $sql, $matches);
                
                if ( !empty($matches)) {
                    //function
                    $setting = $fn['set_values']($data, $sql);
                    array_map($setting, $matches);
                }

                return $sql;
            };




            ////////////////////////////////////////////////////////////////////
            //function
            $fn['combineSQL'] = function() use ($fn, &$fields, $syntax, &$sql)
            {
                if ( $sql = $fields['statement']) {
                    //function
                    $setting = function($name) use (&$sql, $fields)
                    {
                        $replace = null;
                        
                        if ( array_key_exists($name, $fields)) {
                            $replace = $fields[$name];
                        }

                        $sql = str_replace($name, $replace, $sql);
                    };

                    array_map($setting, $syntax);
                }

                return trim($sql);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $fn['startup'] = function() use ($fn, &$fields)
            {
                //function
                $init = function(&$value, $field) use ($fn)
                {
                    $callback = $fn[$field]();
                    $value    = $callback();
                };

                array_walk($fields, $init);
            };



            ////////////////////////////////////////////////////////////////////
            //function
            $supervisors = function() use ($fn, &$fields, &$sql)
            {
                $fn['startup']();
                $fn['combineSQL']();
                $fn['combineWhere']();
                $fn['combineString']();

                if ($this->debug) {
                    $statement = $fn['statement']();
                    $column = $fn['column']();
                    $from = $fn['from']();
                    $where = $fn['where']();
                    $groupby = $fn['groupby']();
                    $orderby = $fn['orderby']();
                    $limit = $fn['limit']();
                    $offset = $fn['offset']();
                    
                    $this->showlog(array(
                        array(
                            $statement(),
                            'task',
                            'statement',
                            'call statement .'
                        ),
                        array(
                            $column(),
                            'task',
                            'column',
                            'call column.'
                        ),
                        array(
                            $from(),
                            'task',
                            'from',
                            'call from.'
                        ),
                        array(
                            $where(),
                            'task',
                            'where',
                            'call where.'
                        ),
                        array(
                            $groupby(),
                            'task',
                            'groupby',
                            'call groupby.'
                        ),
                        array(
                            $orderby(),
                            'task',
                            'orderby',
                            'call orderby.'
                        ),
                        array(
                            $limit(),
                            'task',
                            'limit',
                            'call limit.'
                        ),
                        array(
                            $offset(),
                            'task',
                            'offset',
                            'call offset.'
                        ),
                    ));
                }
                
                return $sql;
            };

            return $supervisors;
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function images($getFile)
    {
        //function
        return function($name, $fn) use ($getFile)
        {
            $common = $getFile($name);
            $config = array_merge($common, $fn['override']());
            $query  = $fn['sql']($config);
            return implode(';', $query);
        };
    }



    ////////////////////////////////////////////////////////////////////////////
    private function dbConnector($data, &$db) 
    {
        // create a database connector if it's first times
        if ( empty($db['connector']) && !empty($data['user_name'])) {
            $db['connector'] = new Db($data['user_name']);
        }

        // create a database connector if it's first times
        if ( empty($db['ci_connector'])) {
            $db['ci_connector'] = new ci_connector();
        }

        return $db;
    }
}
       
