<?php   if ( !file_exists(dirname(__FILE__).'/ObjectBiz.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../eucurrency.php')) exit('No direct script access allowed');
        echo header('Content-Type: text/html; charset=utf-8');
        if ( class_exists('FeedBiz')    == FALSE ) require dirname(dirname(__FILE__)).'/FeedBiz.php';
        if ( class_exists('ObjectBiz')  == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        if ( class_exists('ebaylib')    == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')    == FALSE ) require dirname(__FILE__).'/connect.php';
        if ( class_exists('Eucurrency') == FALSE ) require dirname(__FILE__).'/../eucurrency.php';
        require dirname(__FILE__).'/../tools.php'; 
         
        class ebayorder extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $language;
                protected $site;
                protected $id_shop;
                protected $objectbiz;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                protected $product;
                protected $product_relist;
                protected $product_revise;
                protected $product_add;
                protected $dir_server;
                protected $_result;
                protected $_debug;
                protected $_debug_path;
                protected $imported_orders = array();

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_JUST_ORDERS' )     $result['just_orders']          = $value;
                                else if ( $name == 'EBAY_MOD_ORDERS' )      $result['mod_orders']           = $value;
                        endwhile;
                        return $result;
                }
                
                function convertPrice($amount, $rate_to, $rate_from) {
                        if ( !isset($amount) || empty($rate_from) && empty($rate_to) ) :
                                return $amount;
                        endif;
                        $rate_temp = (!empty($rate_from) && !is_numeric($rate_from)) || ($rate_from * 1 == 0) ? $amount : ($amount / $rate_from);
                        return isset($rate_to) && !is_numeric($rate_to) ? $rate_temp : ($rate_temp * $rate_to);
                }
                
                function dir_remove($dir) {
                        if ( !empty($dir) && is_dir($dir)) :
                                $objects = scandir($dir);
                                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                foreach ($objects as $object) :
                                      if ($object != "." && $object != "..") :
                                            if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                                      endif;
                                endforeach;
                        endif;
                }
                
                function putContent($parser = null, $page = null) {
                        if ( !isset($parser) && empty($parser) || !isset($page) && empty($page) ) :
                                return Null;
                        endif;
                        
                        if ( $this->_debug ) :
//                                if ( file_exists($this->_debug_path) && !file_exists($this->_debug_path.'page'.$page.'.txt') && ($handle = opendir($this->_debug_path)) ) :
                                        $this->dir_remove($this->_debug_path);
//                                else :
                                        if ( !file_exists($this->_debug_path) ) { 
                                                $old = umask(0); 
                                                mkdir($dir_temp, 0777, true); 
                                                umask($old); 
                                        }
                                        file_put_contents($this->_debug_path.'page'.$page.'.txt', $parser);
//                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                private function set_orders_last_update($page = null, $parser = '') {
                        $parser                                 = $this->ebaylib->core->_parser;
//                        echo '<pre>'.print_r($parser, true).'</pre>';
                        if ( !empty($parser) && $parser->Ack->__toString() == 'Success' && isset($parser->OrderArray) ) :
                                $this->putContent($parser, $page);
                                foreach ( $parser->OrderArray->Order as $transaction ) :                                		
                                        foreach ( $transaction->TransactionArray->Transaction as $transactionItem ) :
                                                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                $transaction_orderID                            = !empty($transaction->OrderID) ? $transaction->OrderID->__toString() : '';
                                                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                
                                                if ( !empty($transaction_orderID) ) :
                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                        $marketplace_order_number               = !empty($transaction->ShippingDetails->SellingManagerSalesRecordNumber) ? $transaction->ShippingDetails->SellingManagerSalesRecordNumber->__toString() : $transactionItem->ShippingDetails->SellingManagerSalesRecordNumber->__toString();
                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                        unset($data_order);
                                                        if (!empty($marketplace_order_number) ) :
                                                                $data_order                             = array(
                                                                        'marketplace_order_number'      => $marketplace_order_number
                                                                );
                                                                $this->objectbiz->callPath($this->user_name, 'orders');
                                                                $result_query                           = $this->objectbiz->OrderTable("orders", $data_order, array('id_marketplace_order_ref' => $transaction_orderID), true);
                                                                //echo $transaction_orderID.'=>'.$marketplace_order_number.'<br/>';
                                                        endif;
                                                endif;
                                        endforeach;
                                endforeach;
                        endif;
                } 
                
                function get_orders_update($from = '', $to = '', $day = 30, $order_status = 'Completed', $update = false) {
                	if ( empty($from) || empty($to) ) :
                	return;
                	endif;
                	$page = 1;
                	$this->skipItem = array();
                	do {
                		$this->ebaylib->GetMyeBaySelling($this->site, $page, 'SoldList', $day);
                		$resultSelling              = $this->ebaylib->core->_parser;
                		$site                       = $this->site;
                		$page++;
                		if (empty($resultSelling->SoldList)) :
                		break;
                		endif;
                	} while ( $page <= $resultSelling->SoldList->PaginationResult->TotalNumberOfPages );

                	$page = 1;
                	do {
                		$this->ebaylib->GetOrders($this->site, $page, $from, $to, $order_status);
                		$resultOrders                          = $this->ebaylib->core->_parser;
                		$this->set_orders_last_update($page);
                		$page++;
                		if (empty($resultOrders->OrderArray)) :
                		break;
                		endif;
                	} while ( $page <= $resultOrders->PaginationResult->TotalNumberOfPages );
                
                	return $this->ebaylib->core->_parser;
                }
                
                private function save_order($transaction = null, $transactionItem = null) {
                        if ( empty($transaction) || empty($transactionItem) ) :
                                return;
                        endif;
                        $sku                                    = $this->transaction_SKU;
                        $currency_query                         = $this->connect->get_ebay_currency($this->transaction_buyerSite);
                        list($currency_used)                    = $this->connect->fetch($currency_query);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $rate_to                = !empty($this->rate_to[$this->transaction_itemID]) ? $this->rate_to[$this->transaction_itemID] : 0;
                        $rate_from              = !empty($this->rate_from[$this->transaction_itemID]) ? $this->rate_from[$this->transaction_itemID] : 0;
                        $this->tax_rate         = !empty($this->tax_rates[$this->transaction_itemID]) ? $this->tax_rates[$this->transaction_itemID] : 0;
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $title_object           = "";
                        $this->id_product       = 0;
                        $this->product_title    = "";
                        $this->quantity         = 0;
                        $this->startprice       = 0;
                        $this->reference        = "";
                        $this->id_currency      = 0;
                        $this->id_condition     = 0;
                        $this->id_product_attribute = 0;

                        if ( !empty($this->id_shop) && !empty($this->lang) && !empty($sku) ) :
                                unset($product_title);
                                $this->id_tax                   = $this->objectbiz->getTaxIDDetails($this->user_name, $this->id_shop, $this->lang, $this->tax_rate);
                                $this->tax_name                 = $this->objectbiz->getTaxNameDetails($this->user_name, $this->id_shop, $this->lang, $this->tax_rate);
                                $product_title                  = $this->objectbiz->findProductFromSKU($this->id_shop, $this->lang, $sku);
                                if ( !empty($product_title) ) :
                                        $title_object           = current($product_title);
                                        $this->id_product       = isset($title_object['id_product'])    ? $title_object['id_product']   : NULL;
                                        $this->product_title    = isset($title_object['name'])          ? $title_object['name']         : NULL;
                                        $this->quantity         = isset($title_object['quantity'])      ? $title_object['quantity']     : NULL;
                                        $this->startprice       = isset($title_object['price'])         ? $title_object['price']        : NULL;
                                        $this->reference        = isset($title_object['reference'])     ? $title_object['reference']    : NULL;
                                        $this->id_currency      = isset($title_object['id_currency'])   ? $title_object['id_currency']  : NULL;
                                        $this->id_condition     = isset($title_object['id_condition'])  ? $title_object['id_condition'] : NULL;
                                        $this->id_product_attribute = isset($title_object['id_product_attribute']) ? $title_object['id_product_attribute'] : NULL;
                                endif;
                        endif;

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($transactionItem->Variation->VariationSpecifics) ) :
                                $sku                            = $transactionItem->Variation->SKU->__toString();
                                $new_price                      = $this->objectbiz->findProductAttibuteFromSKU($this->id_shop, $this->id_product, $sku);
                                if ( !empty($new_price) ) :
                                        $new_price              = current($new_price);
                                        $this->startprice       = isset($new_price['price'])            ? $new_price['price']           : NULL;
                                        $this->quantity         = isset($new_price['quantity'])         ? $new_price['quantity']        : NULL;
                                        $this->reference        = isset($new_price['reference'])        ? $new_price['reference']       : NULL;
                                        $this->id_product_attribute = isset($new_price['id_product_attribute']) ? $new_price['id_product_attribute'] : NULL;
                                endif;
                        else :
                                $new_price                      = $this->objectbiz->findProductAttibuteFromSKU($this->id_shop, $this->id_product, $sku);
                                if ( !empty($new_price) ) :
                                        $new_price              = current($new_price);
                                        $this->id_product_attribute = isset($new_price['id_product_attribute']) ? $new_price['id_product_attribute'] : NULL;
                                endif;
                        endif;

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                       
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $this->transaction_shippingService      = !empty($transaction->ShippingServiceSelected->ShippingService) ? $transaction->ShippingServiceSelected->ShippingService->__toString() : '';
                        $this->transaction_opt_shippingService  = !empty($transaction->ShippingDetails->ShippingServiceOptions) ? $transaction->ShippingDetails->ShippingServiceOptions[0]->ShippingService->__toString() : (!empty($transaction->ShippingDetails->InternationalShippingServiceOption) ? $transaction->ShippingDetails->InternationalShippingServiceOption[0]->ShippingService->__toString() : '');
                        if ( !empty($this->transaction_shippingService) && !empty($this->transaction_opt_shippingService) && $this->transaction_shippingService == 'NotSelected' ) :
                                $this->transaction_shippingService = $this->transaction_opt_shippingService;
                        endif;

                        if ( isset($this->id_product) ) :
                                $carrier                                = $this->objectbiz->findProductCarrier($this->id_shop, $this->transaction_shippingService);
                                $carrier                                = !empty($carrier) ? $carrier : $this->objectbiz->findReturnCarrierFromDefault($this->id_shop, $this->transaction_id_site['id_site']);
                                $carrier                                = !empty($carrier) ? $carrier : $this->objectbiz->findProductCarrierFromIDProduct($this->id_shop, $this->id_product);
                                $carrier                                = !empty($carrier) ? $carrier : $this->objectbiz->findCarrierFromDB($this->id_shop);
                                if ( !empty($carrier) ) :
                                        $carrier                        = current($carrier);
                                        $carrier                        = $carrier['id_carrier'];
                                else :  $carrier                        =  NULL;
                                endif;
                        endif;
                        
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $this->transaction_platform             = !empty($transactionItem->Platform) ? $transactionItem->Platform->__toString() : '';
                        $this->transaction_orderStatus          = !empty($transaction->OrderStatus) ? $transaction->OrderStatus->__toString() : '';
                        $this->transaction_paymentMethod        = !empty($transaction->CheckoutStatus->PaymentMethod) ? $transaction->CheckoutStatus->PaymentMethod->__toString() : '';
                        $this->transaction_amountSaved          = !empty($transaction->AmountSaved) ? $transaction->AmountSaved->__toString() : '';
                        $this->transaction_iso_code             = !empty($transaction->AmountSaved) ? $transaction->AmountSaved->attributes()->__toString() : '';
                        $this->transaction_subtotal             = !empty($transaction->Subtotal) ? $transaction->Subtotal->__toString() : '';
                        $this->transaction_total_discount       = !empty($this->transaction_amountSaved) ? number_format($this->convertPrice($this->transaction_amountSaved, $rate_to, $rate_from), 4, '.', '') : '';
                        $this->transaction_total_amount         = !empty($this->transaction_subtotal) ? number_format($this->convertPrice($this->transaction_subtotal, $rate_to, $rate_from), 4, '.', '') : '';
                        $this->transaction_total_amount_tax_excl= !empty($this->transaction_subtotal) ? (number_format($this->convertPrice($this->transaction_subtotal, $rate_to, $rate_from), 4, '.', '') / (100 + $this->tax_rate)) * 100 : '';
                        $this->transaction_total                = !empty($transaction->Total) ? $transaction->Total->__toString() : '';
                        $this->transaction_total_paid           = !empty($this->transaction_total) ? number_format($this->convertPrice($this->transaction_total, $rate_to, $rate_from), 4, '.', '') : '';
                        $this->transaction_total_paid_tax_excl  = !empty($this->transaction_total) ? (number_format($this->convertPrice($this->transaction_total, $rate_to, $rate_from), 4, '.', '') / (100 + $this->tax_rate)) * 100 : '';
                        $this->transaction_createdTime          = !empty($transaction->CreatedTime) ? $transaction->CreatedTime->__toString() : '';
                        $this->transaction_order_date           = !empty($this->transaction_createdTime) ? date('Y-m-d H:i:s', strtotime($this->transaction_createdTime)) : '';
                        $this->transaction_shippedTime          = !empty($transaction->ShippedTime) ? $transaction->ShippedTime->__toString() : '';
                        $this->transaction_shipping_date        = !empty($this->transaction_shippedTime) ? date('Y-m-d H:i:s', strtotime($this->transaction_shippedTime)) : $this->transaction_shippedTime;
                        $this->transaction_paidTime             = !empty($transaction->PaidTime) ? $transaction->PaidTime->__toString() : '';
                        $this->transaction_purchase_date        = !empty($this->transaction_paidTime) ? date('Y-m-d H:i:s', strtotime($this->transaction_paidTime)) : '';
                        $this->transaction_last_modified        = !empty($transaction->CheckoutStatus->LastModifiedTime) ? $transaction->CheckoutStatus->LastModifiedTime->__toString() : '';
                        $this->transaction_last_modified_date   = !empty($this->transaction_last_modified) ? date('Y-m-d H:i:s', strtotime($this->transaction_last_modified)) : '';
                        $this->transaction_comment              = !empty($transaction->BuyerCheckoutMessage) ? html_special($transaction->BuyerCheckoutMessage->__toString()) : '';

                        $this->transaction_buyerUserID          = !empty($transaction->BuyerUserID) ? $transaction->BuyerUserID->__toString() : '';
                        $this->transaction_email                = !empty($transactionItem->Buyer->Email) ? $transactionItem->Buyer->Email->__toString() : '';
                        $this->transaction_staticAlias          = !empty($transactionItem->Buyer->StaticAlias) ? $transactionItem->Buyer->StaticAlias->__toString() : '';
                        $this->transaction_name                 = !empty($transaction->ShippingAddress->Name) ? $transaction->ShippingAddress->Name->__toString() : '';
                        $this->transaction_addressID            = !empty($transaction->ShippingAddress->AddressID) ? $transaction->ShippingAddress->AddressID->__toString() : '';
                        $this->transaction_address1             = !empty($transaction->ShippingAddress->Street1) ? $transaction->ShippingAddress->Street1->__toString() : '';
                        $this->transaction_address2             = !empty($transaction->ShippingAddress->Street2) ? $transaction->ShippingAddress->Street2->__toString() : '';
                        $this->transaction_city                 = !empty($transaction->ShippingAddress->CityName) ? $transaction->ShippingAddress->CityName->__toString() : '';
                        $this->transaction_stateOrProvince      = !empty($transaction->ShippingAddress->StateOrProvince) ? $transaction->ShippingAddress->StateOrProvince->__toString() : '';
                        $this->transaction_country              = !empty($transaction->ShippingAddress->Country) ? $transaction->ShippingAddress->Country->__toString() : '';
                        $this->transaction_country_name         = !empty($transaction->ShippingAddress->CountryName) ? $transaction->ShippingAddress->CountryName->__toString() : '';
                        $this->transaction_postal_code          = !empty($transaction->ShippingAddress->PostalCode) ? $transaction->ShippingAddress->PostalCode->__toString() : '';
                        $this->transaction_phone                = !empty($transaction->ShippingAddress->Phone) ? $transaction->ShippingAddress->Phone->__toString() : '';
                        $this->transaction_security_code_token  = !empty($transaction->EIASToken) ? $transaction->EIASToken->__toString() : '';

                        $this->transaction_sellerUserID         = !empty($transaction->SellerUserID) ? $transaction->SellerUserID->__toString() : '';
                        $this->transaction_user_id              = !empty($this->user_id) ? $this->user_id : '';
                        $this->transaction_sellerEmail          = !empty($transaction->SellerEmail) ? $transaction->SellerEmail->__toString() : '';
                        $this->transaction_sellerEToken         = !empty($transaction->SellerEIASToken) ? $transaction->SellerEIASToken->__toString() : '';

                        $this->transaction_weightMajor          = !empty($transaction->ShippingDetails->CalculatedShippingRate->WeightMajor) ? $transaction->ShippingDetails->CalculatedShippingRate->WeightMajor->__toString() : '';
                        $this->transaction_WeightMinor          = !empty($transaction->ShippingDetails->CalculatedShippingRate->WeightMinor) ? $transaction->ShippingDetails->CalculatedShippingRate->WeightMinor->__toString() : '';
                        $this->transaction_weight               = !empty($this->transaction_weightMajor) ? number_format($this->transaction_weightMajor.(!empty($this->transaction_WeightMinor) ? '.'.$this->transaction_WeightMinor : '.0000'), 4, '.', '') : number_format(0, 4, '.', '');
                        $this->transaction_trackingDetails      = !empty($transactionItem->ShippingDetails->ShipmentTrackingDetails) ? $transactionItem->ShippingDetails->ShipmentTrackingDetails->ShipmentTrackingNumber->__toString() : '';
                        $this->transaction_shippingServiceCost  = !empty($transaction->ShippingServiceSelected->ShippingServiceCost) ? $transaction->ShippingServiceSelected->ShippingServiceCost->__toString() : '';
                        $this->transaction_services_cost        = !empty($this->transaction_shippingServiceCost) ? number_format($this->convertPrice($this->transaction_shippingServiceCost, $rate_to, $rate_from), 4, '.', '') : 0;
                        $this->transaction_id_carrier           = !empty($carrier) ? $carrier : '';

                        $this->transaction_quantityPurchased    = !empty($transactionItem->QuantityPurchased) ? $transactionItem->QuantityPurchased->__toString() : 0;
                        $this->transaction_actualShippingCost   = !empty($transactionItem->ActualShippingCost) ? $transactionItem->ActualShippingCost->__toString() : NULL;
                        $this->transaction_tax_incl             = !empty($this->transaction_actualShippingCost) ? number_format($this->convertPrice($this->transaction_actualShippingCost, $rate_to, $rate_from), 4, '.', '')  : number_format(0, 4, '.', '');
                        $this->transaction_tax_excl             = !empty($this->transaction_actualShippingCost) ? number_format($this->convertPrice($this->transaction_actualShippingCost, $rate_to, $rate_from), 4, '.', '')  : number_format(0, 4, '.', '');
                        $this->transaction_shipping_price       = !empty($this->transaction_tax_excl) ? number_format($this->transaction_tax_excl / $this->transaction_quantityPurchased, 4, '.', '') : number_format(0, 4, '.', '');
                        $this->transaction_price                = !empty($transactionItem->TransactionPrice) ? $transactionItem->TransactionPrice->__toString() : NULL;
                        $this->transaction_price_tax_incl       = !empty($this->transaction_price) ? number_format($this->convertPrice($this->transaction_price, $rate_to, $rate_from), 4, '.', '')  : Null;
                        $this->transaction_price_tax_excl       = !empty($this->transaction_price) ? number_format($this->convertPrice($this->transaction_price, $rate_to, $rate_from) / (100 + $this->tax_rate) * 100 , 4, '.', '') : Null;
                        $this->transaction_total_price_tax_incl = !empty($this->transaction_price) ? number_format($this->convertPrice($this->transaction_price, $rate_to, $rate_from) * $this->transaction_quantityPurchased, 4, '.', '') : NULL;
                        $this->transaction_total_price_tax_excl = !empty($this->transaction_price) ? number_format($this->convertPrice($this->transaction_price, $rate_to, $rate_from) / (100 + $this->tax_rate) * 100 * $this->transaction_quantityPurchased, 4, '.', '') : NULL;

                        $this->transaction_taxOnSubtotalAmount  = !empty($transactionItem->Taxes->TaxDetails[0]->TaxOnSubtotalAmount) ? $transactionItem->Taxes->TaxDetails[0]->TaxOnSubtotalAmount->__toString() : 0;
                        $this->transaction_sub_price_tax_incl   = !empty($this->transaction_taxOnSubtotalAmount) ? number_format($this->convertPrice($this->transaction_taxOnSubtotalAmount, $rate_to, $rate_from), 4, '.', '') : 0;
                        $this->transaction_totalTaxAmount       = !empty($transactionItem->Taxes->TotalTaxAmount) ? $transactionItem->Taxes->TotalTaxAmount->__toString() : 0;
                        $this->transaction_totalamount_tax_incl = !empty($this->transaction_totalTaxAmount) ? number_format($this->convertPrice($this->transaction_totalTaxAmount, $rate_to, $rate_from), 4, '.', '') : 0;
                        $this->transaction_taxOnShippingAmount  = !empty($transactionItem->Taxes->TaxDetails[0]->TaxOnShippingAmount) ? $transactionItem->Taxes->TaxDetails[0]->TaxOnShippingAmount->__toString() : 0;
                        $this->transaction_shipping_tax_incl    = !empty($this->transaction_taxOnShippingAmount) ? number_format($this->convertPrice($this->transaction_taxOnShippingAmount, $rate_to, $rate_from), 4, '.', '') : 0;                                                                                                  
                        $marketplace_order_number               = !empty($transaction->ShippingDetails->SellingManagerSalesRecordNumber) ? $transaction->ShippingDetails->SellingManagerSalesRecordNumber->__toString() : $transactionItem->ShippingDetails->SellingManagerSalesRecordNumber->__toString();
                        $this->transaction_unpaidItem           = !empty($transactionItem->UnpaidItem->Status) ? $transactionItem->UnpaidItem->Status->__toString() : '';
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($this->transaction_orderStatus) && !empty($this->transaction_shippedTime) && $this->transaction_orderStatus == 'Completed' ) :
                                $this->transaction_orderStatus  = 'Shipped';
                        endif;
                        if ( !empty($this->transaction_orderStatus) && empty($this->transaction_paidTime) && $this->transaction_orderStatus == 'Completed' ) :
                                $this->transaction_orderStatus  = 'Active';
                        endif;
                        if ( !empty($this->transaction_orderStatus) && !empty($this->transaction_paymentMethod) && !empty($this->transaction_paidTime) && ($this->transaction_paymentMethod != 'None' || $this->transaction_paymentMethod == 'PayPal') && $this->transaction_orderStatus == 'Active' ) :
                                $this->ebaylib->GetItemTransactions($this->site, $this->transaction_itemID, $this->transaction_ID);
                                $itemOrders                             = $this->ebaylib->core->_parser;
                                $buyer_shipping_address                 = array();
                                if ( !empty($itemOrders) ) :
                                        $buyer_address                  = $itemOrders->TransactionArray->Transaction->Buyer->BuyerInfo->ShippingAddress;
                                        if ( !empty($buyer_address) ) :
                                                foreach ( $buyer_address[0] as $key_name => $address ) :
                                                        $buyer_shipping_address[$key_name]  = strval($address);
                                                endforeach;
                                        endif;
                                endif;

                                if ( !empty($buyer_shipping_address) ) :
                                        $resultOrders                   = $this->ebaylib->ReviseCheckoutStatus($this->site, $marketplace_order_number, $this->transaction_buyerUserID, $this->transaction_orderStatus, $buyer_shipping_address);
                                endif;
                        endif;
                        
                        unset($data_order);
                        $data_order                             = array(
                                'id_marketplace_order_ref'      => $this->transaction_orderID,
                                'marketplace_order_number'      => $marketplace_order_number,
                                'sales_channel'                 => $this->transaction_platform,
                                'id_shop'                       => $this->id_shop,
                                'id_marketplace'                => $this->id_offer_packages,
                                'site'                          => $this->transaction_id_site['id_site'],
                                'id_lang'                       => isset($this->lang) ? $this->lang : 0,
                                'order_type'                    => empty($this->transaction_unpaidItem) ? 'Standard' : $this->transaction_unpaidItem,
                                'order_status'                  => $this->transaction_orderStatus,
                                'payment_method'                => $this->transaction_paymentMethod,
                                'id_currency'                   => !empty($this->transaction_iso_code) ? $this->transaction_iso_code : (isset($this->id_currency) ? $this->id_currency : ''),
                                'total_discount'                => $this->transaction_total_discount,
                                'total_amount'                  => $this->transaction_total_amount,
                                'total_paid'                    => $this->transaction_total_paid,
                                'total_shipping'                => number_format($this->transaction_services_cost, 4, '.', ''),
                                'order_date'                    => $this->transaction_order_date,
                                'shipping_date'                 => $this->transaction_shipping_date,
                                'delivery_date'                 => $this->transaction_shipping_date,
                                'purchase_date'                 => $this->transaction_purchase_date,
                                'affiliate_id'                  => '',
                                'commission'                    => '',
                                'ip'                            => '',
                                'gift_message'                  => '',
                                'gift_amount'                   => '',
                                'comment'                       => html_special($this->transaction_comment),
                                'date_add'                      => date('Y-m-d H:i:s', strtotime('now')),
                        );

                        $having                                 = $this->objectbiz->checkOrderCount("orders_orders", array("id_marketplace_order_ref" => $this->transaction_orderID));
                        $result_query                           = $this->objectbiz->OrderTable("orders_orders", $data_order, array('id_marketplace_order_ref' => $this->transaction_orderID), $having);
                        $result_orders                          = $this->objectbiz->getOrdersByRef($this->user_name, $this->transaction_orderID);

                        if ( !empty($result_orders) ) :
                                $id_orders                      = current($result_orders);
                                $id_orders                      = $id_orders['id_orders'];
                        endif;

//                        require_once(BASEPATH.'/../libraries/Hooks.php');
//     
//                        $hook = new Hooks();
//                        $hook->_call_hook('save_orders', array(
//                            'user_name'     => $this->user_name,
//                            'id_shop'       => $this->id_shop,
//                            'site'          => $this->transaction_id_site['id_site'], 
//                            'id_order'      => $id_orders
//                        ));
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        
                        if (mb_detect_encoding(strval($this->transaction_city)) != 'UTF-8') {
                            $this->transaction_city = iconv("ISO-8859-15", "UTF-8//TRANSLIT", $this->transaction_city);
                        }
                        if (mb_detect_encoding(strval($this->transaction_postal_code)) != 'UTF-8') {
                            $this->transaction_postal_code = iconv("ISO-8859-15", "UTF-8//TRANSLIT", $this->transaction_postal_code);
                        }
                        
                        unset($data_buyer);
                        $data_buyer                             = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : '',
                                'id_buyer_ref'                  => $this->transaction_buyerUserID,
                                'site'                          => $this->transaction_buyer_site['id_site'],
                                'email'                         => ($this->transaction_email != 'Invalid Request') ? $this->transaction_email : $this->transaction_staticAlias,
                                'name'                          => html_special($this->transaction_name),
                                'address_name'                  => html_special($this->transaction_name),
                                'id_address_ref'                => html_special($this->transaction_addressID),
                                'address1'                      => html_special($this->transaction_address1),
                                'address2'                      => html_special($this->transaction_address2),
                                'city'                          => html_special($this->transaction_city),
                                'district'                      => '',
                                'state_region'                  => html_special($this->transaction_stateOrProvince),
                                'country_code'                  => $this->transaction_country,
                                'country_name'                  => html_special($this->transaction_country_name),
                                'postal_code'                   => $this->transaction_postal_code,
                                'phone'                         => $this->transaction_phone,
                                'security_code_token'           => $this->transaction_security_code_token,
                        );

                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_buyer", array("id_orders" => $id_orders));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_buyer", $data_buyer, array("id_orders" => $id_orders), $having);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_seller);
                        $data_seller                            = array(
                                'id_seller'                     => $this->transaction_sellerUserID,
                                'id_user'                       => $this->transaction_user_id,
                                'site'                          => $this->transaction_siteID,
                                'email'                         => $this->transaction_sellerEmail,
                                'name'                          => '',
                                'status'                        => '',
                                'token'                         => $this->transaction_sellerEToken,
                        );
                        $having_1                               = $this->objectbiz->checkOrderCount("orders_order_seller", array("email" => $this->transaction_sellerEmail));
                        $having_2                               = $this->objectbiz->checkOrderCount("orders_order_seller", array("site" => $this->transaction_siteID));
                        $having                                 = (($having_1 == 0) && ($having_2 == 0)) ? 0 : 1;
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_seller", $data_seller, array('id_user' => $this->user_id, "email" => $this->transaction_sellerEmail, "site" => $this->transaction_siteID), $having);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_shipping);
                        $data_shipping                          = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : '',
                                'shipment_service'              => !empty($this->transaction_shippingService) ? $this->transaction_shippingService : $this->transaction_opt_shipping_cost,
                                'weight'                        => $this->transaction_weight,
                                'tracking_number'               => $this->transaction_trackingDetails,
                                'shipping_services_cost'        => !empty($this->transaction_services_cost) ? number_format($this->transaction_services_cost, 4, '.', '') : "0.0000",
                                'shipping_services_level'       => '',
                                'id_carrier'                    => $this->transaction_id_carrier,
                        );
                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_shipping", array("id_orders" => $id_orders));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_shipping", $data_shipping, array("id_orders" => $id_orders), $having);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_status);
                        $data_status                            = array(
                                'name'                          => $this->transaction_orderStatus,
                                'date_add'                      => $this->transaction_order_date,
                        );

                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_status", array("name" => $this->transaction_orderStatus));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_status", $data_status, array("name" => $this->transaction_orderStatus), $having);

                        $status_id                              = $this->objectbiz->getOrdersStatusID($this->user_name, $this->transaction_orderStatus);
                        $id_status                              = 0;
                        if ( !empty($status_id) ) :
                                $status_id                      = current($status_id);
                                $id_status                      = $status_id['id_status'];
                        endif;



                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_item);
                        $data_item                              = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : Null,
                                'id_marketplace_order_item_ref' => $this->transaction_ID,
                                'id_shop'                       => $this->id_shop,
                                'id_product'                    => isset($this->id_product) ? $this->id_product : '',
                                'reference'                     => isset($this->reference) ? $this->reference : '',
                                'id_marketplace_product'        => $this->transaction_itemID,
                                'id_product_attribute'          => isset($this->id_product_attribute) ? $this->id_product_attribute : '',
                                'product_name'                  => isset($this->product_title) ? html_special($this->product_title) : '',
                                'quantity'                      => $this->transaction_quantityPurchased,
                                'quantity_in_stock'             => isset($this->quantity) ? $this->quantity : '',
                                'quantity_refunded'             => Null,
                                'product_weight'                => $this->transaction_weight,
                                'product_price'                 => isset($this->startprice) ? number_format($this->startprice, 4, '.', '') : 0,
                                'shipping_price'                => $this->transaction_shipping_price,
                                'tax_rate'                      => $this->tax_rate,
                                'unit_price_tax_incl'           => $this->transaction_price_tax_incl,
                                'unit_price_tax_excl'           => $this->transaction_price_tax_excl,
                                'total_price_tax_incl'          => $this->transaction_total_price_tax_incl,
                                'total_price_tax_excl'          => $this->transaction_total_price_tax_excl,
                                'total_shipping_price_tax_incl' => $this->transaction_tax_incl,
                                'total_shipping_price_tax_excl' => $this->transaction_tax_excl,
                                'id_condition'                  => isset($this->id_condition) ? $this->id_condition : '',
                                'promotion'                     => Null,
                                'message'                       => html_special($this->transaction_comment),
                                'id_status'                     => !empty($id_status) ? $id_status : Null,
                                'id_tax'                        => isset($this->id_tax) ? $this->id_tax : 0,
                        );

                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_items", array("id_marketplace_order_item_ref" => $this->transaction_ID));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_items", $data_item, array("id_marketplace_order_item_ref" => $this->transaction_ID), $having);
                        $order_items                            = $this->objectbiz->getOrdersItemByID($this->user_name, $id_orders);

                        $data_item['time'] = isset($this->transaction_order_date) && !empty($this->transaction_order_date) ? $this->transaction_order_date : date('Y-m-d H:i:s');
                        if ( $this->transaction_orderStatus == 'Completed' ) :
                                $this->imported_orders[$id_orders]['items'][] = $data_item;
                        endif;

                        if ( !empty($order_items) ) :
                                $id_order_items                 = current($order_items);
                                $id_order_items                 = $id_order_items['id_order_items'];
                        endif;

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($transactionItem->Variation->VariationSpecifics) ) :
                                $attributes                     = $this->objectbiz->findProductAttributeFromSKU($this->id_shop, $this->id_product, $sku);
                                if(!empty($attributes)) :
                                        foreach ( $attributes as $attribute ) :
                                                unset($data_variation);

                                                $data_variation                 = array(
                                                        'id_order_items'        => isset($id_order_items) ? $id_order_items : '',
                                                        'id_attribute_group'    => !empty($attribute['id_attribute_group']) ? $attribute['id_attribute_group'] : '',
                                                        'id_attribute'          => !empty($attribute['id_attribute']) ? $attribute['id_attribute'] : '',
                                                        'id_shop'               => $this->id_shop,
                                                );
                                                $having                 = $this->objectbiz->checkOrderCount("orders_order_items_attribute", array("id_order_items" => isset($id_order_items) ? $id_order_items : '', 'id_attribute_group' => !empty($attribute['id_attribute_group']) ? $attribute['id_attribute_group'] : '', 'id_attribute' => !empty($attribute['id_attribute']) ? $attribute['id_attribute'] : ''));
                                                $result_query           = $this->objectbiz->OrderTable("orders_order_items_attribute", $data_variation, array("id_order_items" => isset($id_order_items) ? $id_order_items : '', 'id_attribute_group' => !empty($attribute['id_attribute_group']) ? $attribute['id_attribute_group'] : '', 'id_attribute' => !empty($attribute['id_attribute']) ? $attribute['id_attribute'] : ''), $having);
                                        endforeach;
                                endif;
                        endif;

                        $this->transaction_checkoutStatus       = !empty($transaction->CheckoutStatus->Status) ? $transaction->CheckoutStatus->Status->__toString() : '';
                        $this->transaction_externalTransactionID= !empty($transaction->ExternalTransaction[0]->ExternalTransactionID) ? $transaction->ExternalTransaction[0]->ExternalTransactionID->__toString() : '';

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        unset($data_payment);
                        $data_payment                           = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : '',
                                'id_order_items'                => isset($id_order_items) ? $id_order_items : '',
                                'payment_method'                => $this->transaction_paymentMethod,
                                'payment_status'                => $this->transaction_checkoutStatus,
                                'id_currency'                   => isset($this->id_currency) ? $this->id_currency : '',
                                'Amount'                        => $this->transaction_total_paid,
                                'external_transaction_id'       => $this->transaction_externalTransactionID,
                        );
                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_payment", array("id_orders" => isset($id_orders) ? $id_orders : ''));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_payment", $data_payment, array("id_orders" => isset($id_orders) ? $id_orders : ''), $having);


                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_invoice);
                        $data_invoice                           = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : '',
                                'total_discount_tax_excl'       => $this->transaction_total_discount,
                                'total_discount_tax_incl'       => $this->transaction_total_discount,
                                'total_paid_tax_excl'           => number_format($this->transaction_total_amount_tax_excl, 4, '.', ''),
                                'total_paid_tax_incl'           => number_format($this->transaction_total_amount, 4, '.', ''),
                                'total_products'                => number_format(($this->transaction_total_paid + $this->transaction_total_discount), 4, '.', ''),
                                'total_shipping_tax_excl'       => number_format(($this->transaction_total_paid_tax_excl - $this->transaction_total_amount_tax_excl), 4, '.', ''),
                                'total_shipping_tax_incl'       => number_format(($this->transaction_total_paid - $this->transaction_total_amount), 4, '.', ''),
                                'note'                          => '',
                                'date_add'                      => !empty($this->transaction_last_modified_date) ? $this->transaction_last_modified_date : '',
                        );
                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_invoice", array("id_orders" => isset($id_orders) ? $id_orders : ''));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_invoice", $data_invoice, array("id_orders" => isset($id_orders) ? $id_orders : ''), $having);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_tax);
                        $data_tax                               = array(
                                'id_orders'                     => isset($id_orders) ? $id_orders : '',
                                'tax_description'               => !empty($this->tax_name) ? html_special($this->tax_name) : '',
                                'imposition'                    => 'SalesTax',
                                'tax_amount'                    => !empty($this->tax_rate) ? $this->tax_rate : 0,
                                'tax_on_sub_total_amount'       => !empty($this->tax_rate) ? $this->tax_rate : 0,
                                'tax_on_shipping_amount'        => 0,
                        );
                        $having                                 = $this->objectbiz->checkOrderCount("orders_order_taxes", array("id_orders" => isset($id_orders) ? $id_orders : ''));
                        $result_query                           = $this->objectbiz->OrderTable("orders_order_taxes", $data_tax, array("id_orders" => isset($id_orders) ? $id_orders : ''), $having);
                }
                
                private function preparing_transaction($transaction = null, $transactionItem = null) {
                        if ( empty($transaction) || empty($transactionItem) ) :
                                return;
                        endif;
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($this->transaction_ID, $this->transaction_SKU, $this->transaction_buyerSite, $this->transaction_siteID, $this->transaction_itemID, $this->transaction_orderID);
                        $this->transaction_ID                           = !empty($transactionItem->TransactionID) ? $transactionItem->TransactionID->__toString() : Null;
                        $this->transaction_SKU                          = !empty($transactionItem->Item->SKU) ? $transactionItem->Item->SKU->__toString() : Null;
                        $this->transaction_buyerSite                    = !empty($transactionItem->Item->Site) ? $transactionItem->Item->Site->__toString() : Null;
                        $this->transaction_siteID                       = !empty($transactionItem->TransactionSiteID) ? $transactionItem->TransactionSiteID->__toString() : '';
                        $this->transaction_itemID                       = !empty($transactionItem->Item->ItemID) ? $transactionItem->Item->ItemID->__toString() : Null;
                        $this->transaction_orderID                      = !empty($transaction->OrderID) ? $transaction->OrderID->__toString() : '';
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($this->transaction_SKU) ) :
                                unset($sku_list);
                                if ( !empty($transactionItem->Variation->SKU) ) :
                                        $sku_list[]                     =  strval($transactionItem->Variation->SKU);
                                        $sku_implode                    = '"'.implode('", "', $sku_list).'"';
                                        $this->transaction_SKU          = $this->objectbiz->synchronizeGetSkuByVariation($sku_implode);
                                else :  return;
                                endif;
                        endif;
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($transactionItem->TransactionID) ) :
                                return;
                        endif;

                        $this->transaction_buyer_site                   = $this->connect->fetch($this->connect->get_ebay_site_by_name($this->transaction_siteID));
                        $this->transaction_id_site                      = $this->connect->fetch($this->connect->get_ebay_site_by_name($this->transaction_buyerSite));
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($sku, $this->id_product_attribute, $this->startprice, $this->quantity, $this->id_tax, $this->tax_name, $this->id_condition, $this->id_currency, $this->reference, $this->product_title, $this->id_product);
                }

                private function get_orders_db() {
                        $this->skipOrders                       = array();
                        $skipOrders                             = $this->objectbiz->getOrdersFromDB($this->user_name, $this->site, $this->id_shop, $this->id_offer_packages);
                        if ( !empty($skipOrders) ) :
                        foreach ( $skipOrders as $transaction_orders ) :
                        	$this->skipOrders[]     = $transaction_orders['transaction_orders'];
                        endforeach;
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                private function set_orders_db($parser = '') {
                        $parser                                 = $this->ebaylib->core->_parser;
                        $this->get_orders_db();
                        $justOrders = isset($this->user['just_orders']) ? $this->user['just_orders'] : 1;
                        if ( !empty($parser) && $parser->Ack->__toString() == 'Success' && isset($parser->OrderArray) ) :
                                foreach ( $parser->OrderArray->Order as $transaction ) :
                                        //validate existing order
                                        if(in_array($transaction->OrderID, $this->skipOrders))
                                                continue;
                                        
//                                	if ( $transaction->OrderID != '122134520208-1709068968002') {
//                                            continue;
//                                        }
//                      echo "<pre>", print_r($parser, true), '</pre>';exit();
                                        
                                        foreach ( $transaction->TransactionArray->Transaction as $transactionItem ) :
                                                $this->preparing_transaction($transaction, $transactionItem);
                                                if ( 
                                                        isset($this->transaction_id_site['id_site']) && //check siteID
                                                        in_array($this->transaction_ID, $this->skipItem) && //check deleted transaction
                                                        !in_array($this->transaction_orderID, $this->skipOrders) && //not imported order
                                                        !empty($this->transaction_SKU) &&//check item ref
                                                        !empty($this->transaction_itemID) && //check product existing
                                                        ($justOrders == 1 || (!empty($this->product) && array_key_exists($this->transaction_itemID, $this->product))) ) :
                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                        $this->save_order($transaction, $transactionItem);
                                                endif;
                                        endforeach;
                                endforeach;
                        endif;
                } 
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                private function set_modifier_orders_db($parser = '') {
                        $parser                                 = $this->ebaylib->core->_parser;
//                      echo "<pre>", print_r($parser, true), '</pre>';exit();
                        $this->get_orders_db();
                        if ( !empty($parser) && $parser->Ack->__toString() == 'Success' && isset($parser->OrderArray) ) :
                                foreach ( $parser->OrderArray->Order as $transaction ) :
                                        //validate existing order
                                        if(in_array($transaction->OrderID, $this->skipOrders)) :	
                                        foreach ( $transaction->TransactionArray->Transaction as $transactionItem ) :
                                                $this->preparing_transaction($transaction, $transactionItem);
                                                if ( 
                                                        isset($this->transaction_id_site['id_site']) && //check siteID
                                                        in_array($this->transaction_orderID, $this->skipOrders) && //imported order
                                                        !empty($this->transaction_SKU) &&//check item ref
                                                        !empty($this->transaction_itemID) ) :

                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                        $this->save_order($transaction, $transactionItem);
                                                endif;
                                        endforeach;
                                        endif;
                                endforeach;
                        endif;
                } 

                function get_imported_orders(){
                	return $this->imported_orders;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_modifier_orders($from = '', $to = '') {
                        set_time_limit(0);
                        $old_add = $this->objectbiz->getOldOrderAdd($this->site, $this->id_shop);
                        $from       = !empty($this->user['mod_orders']) ? $this->user['mod_orders'] : $old_add;
                        $date_from  = new DateTime($from);
                        $date_to    = new DateTime($from);
                        $now        = date("Y-m-d", strtotime("+1 day"));

                        while ( $to != $now ) :
                                $date_to->modify('+10 days');
                                $to = $date_to->format("Y-m-d");
                                $from = $date_from->format("Y-m-d");
                                if ( strtotime($now) <= $date_to->getTimestamp() ) :
                                        $to = $now;
                                endif;
                                $page = 1;
                                do {
                                        $this->ebaylib->GetModifierOrders($this->site, $page, $from."T06:00:00.000Z", $to."T06:00:00.000Z");
                                        $resultOrders                          = $this->ebaylib->core->_parser;
                                        $this->set_modifier_orders_db();
                                        $page++;
                                        if (empty($resultOrders->OrderArray)) :
                                                break;
                                        endif;
                                } while ( $page <= $resultOrders->PaginationResult->TotalNumberOfPages );

                                $date_from->modify('+10 days');
                                $date_now    = new DateTime($to);
                                $date_now->modify('-2 days');
                                $this->connect->set_last_modifier_order($this->user_id, $this->site, $date_now->format("Y-m-d"));
                        endwhile;

                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_orders($from = '', $to = '', $day = 30, $order_status = 'All') {
                        if ( empty($from) || empty($to) ) :
                                return;
                        endif;
                        $page = 1;
                        set_time_limit(0);
                        $this->skipItem = array();
                        do {
                                $this->ebaylib->GetMyeBaySelling($this->site, $page, 'SoldList', $day);
                                $resultSelling              = $this->ebaylib->core->_parser;
                                $site                       = $this->site;
                                $page++;
                                //ignore order transaction which is deleted
                                if ( !empty($resultSelling->SoldList) && $resultSelling->Ack->__toString() == 'Success' ) :
                                        foreach ( $resultSelling->SoldList->OrderTransactionArray->OrderTransaction as $itemOrdering ) :
                                    
                                                //order containing single product
                                                if ( isset($itemOrdering->Transaction->TransactionID) && !empty($itemOrdering->Transaction->TransactionID) ) :
                                                        $this->skipItem[]       = $itemOrdering->Transaction->TransactionID->__toString();
                                                endif;
                                                
                                                //order containing multiple products
                                                if (isset($itemOrdering->Order->TransactionArray) && !empty($itemOrdering->Order->TransactionArray)) :
                                                    foreach($itemOrdering->Order->TransactionArray->Transaction as $transactionElement) :
                                                        $this->skipItem[] = $transactionElement->TransactionID->__toString();
                                                    endforeach;
                                                endif;
                                        endforeach;
                                endif;
                                if (empty($resultSelling->SoldList)) :
                                        break;
                                endif;
                        } while ( $page <= $resultSelling->SoldList->PaginationResult->TotalNumberOfPages );

                        $this->obj_sku                          = $this->objectbiz->getExportProduct($this->site, $this->id_shop, $this->ebayUser);
                        if ( !empty($this->obj_sku) ) :
                                foreach ( $this->obj_sku as $sku ) :
                                        $this->product[$sku['id_item']] = array(
                                                'id_shop'               => $sku['id_shop'],
                                                'id_product'            => $sku['id_product'],
                                                'id_item'               => $sku['id_item'],
                                                'sku'                   => $sku['SKU'],
                                        );
                                endforeach;
                        endif;
                        $page = 1;
                        $last_add = $this->objectbiz->getLastOrderAdd($this->site, $this->id_shop, $order_status == 'All' ? 'Completed' : $order_status);
                        $last_add = !empty($last_add) ? $last_add : $from;
                        $date_from  = new DateTime($last_add);
                        $date_from->modify('-10 days');
                        do {
                                $this->ebaylib->GetOrders($this->site, $page, $date_from->format("Y-m-d"), $to, $order_status);
                                $resultOrders                          = $this->ebaylib->core->_parser;
                                $this->set_orders_db();
                                $page++;
                                if (empty($resultOrders->OrderArray)) :
                                        break;
                                endif;
                        } while ( $page <= $resultOrders->PaginationResult->TotalNumberOfPages );
                        
                        return $this->ebaylib->core->_parser;
                }
                
                private function debug($data = null, $name = 'log_call.txt', $append = true) {
                        $this->dir_name                     = USERDATA_PATH.$this->user_name."/ebay/debug/orders/";
                        $this->dir_file                     = $this->dir_name.$name."";
                        if(!is_dir($this->dir_name)) :
                                $old = umask(0); 
                                mkdir($this->dir_name, 0777, true);
                                umask($old); 
                        endif;
                        $current = $data;
                        if ( file_exists($this->dir_file) ) :
                        		if($append):
                                	$current .= file_get_contents($this->dir_file);
                        		else :
                        			unlink($this->dir_file);
                        		endif;
                        endif;
                        file_put_contents($this->dir_file, $current);
                        $old = umask(0); 
                        chmod($this->dir_file, 0755);
                        umask($old); 
                }
                
                function get_orders_debug_selling_writer($csv = '', $item = null, $selling_count = 1) {
                        if ( empty($item) && isset($item->Item) ) {
                                return $csv;
                        }
                        $csv .= $selling_count."\t";
                        $csv .= (isset($item->Item->ItemID) && !empty($item->Item->ItemID) ? strval($item->Item->ItemID) : '')."\t";
                        $csv .= (isset($item->Item->SKU) && 
                        		!empty($item->Item->SKU) ? 
                        		strval($item->Item->SKU) : 
                        			(isset($item->Item->Variations->Variation->SKU) && 
                        			!empty($item->Item->Variations->Variation->SKU) ? 
                        			strval($item->Item->Variations->Variation->SKU) : ''))."\t";
                        $csv .= (isset($item->TransactionID) && !empty($item->TransactionID) ? strval($item->TransactionID) : '')."\t";
                        $csv .= (isset($item->Item->Title) && !empty($item->Item->Title) ? strval($item->Item->Title) : '')."\t";
                        $csv .= (isset($item->Item->Quantity) && !empty($item->Item->Quantity) ? strval($item->Item->Quantity) : '')."\t";
                        $csv .= (isset($item->Item->ListingDetails->StartTime) && !empty($item->Item->ListingDetails->StartTime) ? strval($item->Item->ListingDetails->StartTime) : '')."\t";
                        $csv .= (isset($item->Item->ListingDetails->ViewItemURL) && !empty($item->Item->ListingDetails->ViewItemURL) ? strval($item->Item->ListingDetails->ViewItemURL) : '')."\r\n";
                        return $csv;
                }
                
                function get_orders_debug_ordering_writer($csv = '', $item = null, $selling_count = 1) {
                        if ( empty($item) ) {
                                return $csv;
                        }
                        $csv .= $selling_count."\t";
                        $csv .= (isset($item->Item->ItemID) && !empty($item->Item->ItemID) ? strval($item->Item->ItemID) : '')."\t";
                        $csv .= (isset($item->Item->SKU) && !empty($item->Item->SKU) ? strval($item->Item->SKU) : isset($item->Variation->SKU) && !empty($item->Variation->SKU) ? strval($item->Variation->SKU) : '')."\t";
                        $csv .= (isset($item->TransactionID) && !empty($item->TransactionID) ? strval($item->TransactionID) : '')."\t";
                        $csv .= (isset($item->Item->Title) && !empty($item->Item->Title) ? strval($item->Item->Title) : '')."\t";
                        $csv .= (isset($item->QuantityPurchased) && !empty($item->QuantityPurchased) ? strval($item->QuantityPurchased) : '')."\r\n";
                        return $csv;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_orders_debug($from = '', $to = '', $day = 30, $order_status = 'Completed') {
                        if ( empty($from) || empty($to) ) :
                                return;
                        endif;

                        $page = 1;
                        //Debug product soldlist in ebay account//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $selling_count = 1;
                        $this->skipItem = array();
                        $csv = "\n\n\n\n".date("Y-m-d H:i:s", strtotime('now'))."\r\n\n";
                        $csv .= "ID COUNT\tID ITEM\tSKU\tTRANSACTION ID\tNAME PRODUCTS\tQUANTITY AVAILABLE\tSTART TIME\tLINK\r\n";
                        do {
                                $this->ebaylib->GetMyeBaySelling($this->site, $page, 'SoldList', $day);
                                $resultSelling              = $this->ebaylib->core->_parser;
                                $site                       = $this->site;
                                $page++;
                                //ignore order transaction which is deleted
                                if ( !empty($resultSelling->SoldList) && $resultSelling->Ack->__toString() == 'Success' ) :
                                        foreach ( $resultSelling->SoldList->OrderTransactionArray->OrderTransaction as $itemOrdering ) : 
                                                if ( !empty($itemOrdering->Order) ) :
                                                        foreach ( $itemOrdering->Order->TransactionArray->Transaction as $itemTransaction ) : 
                                                                $csv = $this->get_orders_debug_selling_writer($csv, $itemTransaction, $selling_count++);
                                                        endforeach;
                                                else : $csv = $this->get_orders_debug_selling_writer($csv, $itemOrdering->Transaction, $selling_count++);
                                                endif;
                                        endforeach;
                                endif;
                                if (empty($resultSelling->SoldList)) :
                                        break;
                                endif;
                        } while ( $page <= $resultSelling->SoldList->PaginationResult->TotalNumberOfPages );
                        
                        $this->debug($csv, 'soldlist.csv');
                        
                        //Debug export product list//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $user_lists = $this->objectbiz->getUserExport($this->site, $this->id_shop);
                        if ( !empty($user_lists) ) :
                                $user_list = array();
                                foreach ( $user_lists as $user_export ) :
                                        $user_list[] = $user_export['ebay_user'];
                                endforeach;
                                $selling_count = 1;
                                $csv = "\n\n\n\n".date("Y-m-d H:i:s", strtotime('now'))."\r\n\n";
                                $csv .= "Debug for user : ".$this->ebayUser."\r\n";
                                if ( !empty($user_list) ) :
                                        $csv .= "Database have users : ".implode(", ", $user_list)."\r\n";
                                endif;
                                $csv .= "\n\n";
                                $csv .= "ID COUNT\tID ITEM\tSKU\tID PRODUCT\tNAME PRODUCTS\tSTART TIME\r\n";
                                $this->obj_sku                          = $this->objectbiz->getExportProduct($this->site, $this->id_shop, $this->ebayUser);
                                if ( !empty($this->obj_sku) ) :
                                        foreach ( $this->obj_sku as $sku ) :
                                                $ebay_product_item = array($selling_count++, $sku['id_item'], $sku['SKU'], $sku['id_product'], $sku['name_product'], $sku['date_add']);
                                                $csv .= implode("\t", $ebay_product_item)."\r\n";
                                        endforeach;
                                endif;
                                $this->debug($csv, 'exportList.csv');
                        endif;
                        //Debug import xml//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $page = 1;
                        $selling_count = 1;
                        $dom = new DOMDocument;
                        $dom->preserveWhiteSpace = FALSE;
                        $dom->formatOutput = TRUE;
                        $csv = "\n\n\n\n".date("Y-m-d H:i:s", strtotime('now'))."\r\n\n";
                        $csv .= "ID COUNT\tID ITEM\tSKU\tTRANSACTION ID\tNAME PRODUCTS\tQUANTITY PURCHASED\r\n";
                        do {
                                $this->ebaylib->GetOrders($this->site, $page, $from, $to, $order_status);
                                $resultOrders                          = $this->ebaylib->core->_parser;
                                if ( !empty($resultOrders->OrderArray) && $resultOrders->Ack->__toString() == 'Success' ) :
                                        foreach ( $resultOrders->OrderArray->Order->TransactionArray->Transaction as $itemOrdering ) : 
                                                $csv = $this->get_orders_debug_ordering_writer($csv, $itemOrdering, $selling_count++);
                                        endforeach;
                                endif;
                                $dom->loadXML($resultOrders->asXML());
                                $this->debug($dom->saveXML(), "Page".$page."-".date("Y-m-d", strtotime('now')).".xml", false);
                                $page++;
                                if (empty($resultOrders->OrderArray)) :
                                        break;
                                endif;
                        } while ( $page <= $resultOrders->PaginationResult->TotalNumberOfPages );
                        $this->debug($csv, 'orderList.csv');
                        
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                             = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->dir_server                       = DIR_SERVER;
                        $this->objectbiz                        = new ObjectBiz(array($this->user_name));
                        $this->connect                          = new connect();
                        $this->eucurrency                       = new Eucurrency();
                        
                        $shop_default                           = $this->objectbiz->getDefaultShop($this->user_name);
                        if ( !empty($shop_default) && count($shop_default) ) :
                                $this->id_shop                  = (int)$shop_default['id_shop'];
                        endif;
                        $this->_debug_path                      = USERDATA_PATH.$this->user_name.'/debug/';
                        $this->user_conf                        = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                             = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        $this->site                             = isset($this->user['site']) ? $this->user['site'] : 0;
                        $this->ebayUser                         = $this->user_conf['userID'];
                        $this->ebaylib                          = new ebaylib( ($this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        $this->id_lang                          = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                        $this->language                         = $this->objectbiz->getDefaultISO($this->user_name, $this->id_shop);
                        $this->ebaylib->token                   = $this->user_conf['token'];
                        $this->ebaylib->core->_template         = 1;
                        $this->site_object                      = $this->connect->fetch($this->connect->get_ebay_site_by_id($this->site));
                        $this->currency                         = $this->site_object['currency'];
                        $this->country                          = strtoupper($this->site_object['iso_code']);
                        list($this->id_offer_packages)          = $this->connect->fetch($this->connect->get_id_offers_packages('eBay'));
                        $tax_rate                               = $this->objectbiz->getDetailTax($this->user_name, $this->site, $this->id_shop);
                        $rate_from                              = $this->objectbiz->getDetailRateFrom($this->user_name, $this->site, $this->id_shop);
                        $rate_to                                = $this->objectbiz->getDetailRateTo($this->user_name, $this->site, $this->id_shop);
                        $this->lang                             = $this->id_lang;
                         $this->currency_iso                    = $this->objectbiz->getCurrentCurrency($this->id_shop);
                        if ( !empty($this->currency_iso) ) :
                                $this->currency_iso             = $this->currency_iso['iso_code'];
                        else :  $this->currency_iso             = NULL;
                        endif;
                        $this->objectbiz->genarateMappingLastest($this->user_name, $this->site, $this->id_shop);
                        $this->tax_rates                        = array();
                        $this->rate_from                        = array();
                        $this->rate_to                          = array();

                        if ( !empty($tax_rate) ) :
                                foreach ( $tax_rate as $id => $rate ) :
                                        if ( !empty($rate['id_item']) ) :
                                                $this->tax_rates[$rate['id_item']] = $rate['tax_rate'];
                                        endif;
                                endforeach;
                        endif;

                        if ( !empty($rate_from) ) :
                                foreach ( $rate_from as $id => $rate ) :
                                        if ( !empty($rate['id_item']) ) :
                                                $this->rate_from[$rate['id_item']] = $rate['currency_rate_from'];
                                        endif;
                                endforeach;
                        endif;
                        
                        if ( !empty($rate_to) ) :
                                foreach ( $rate_to as $id => $rate ) :
                                        if ( !empty($rate['id_item']) ) :
                                                $this->rate_to[$rate['id_item']] = $rate['currency_rate_to'];
                                        endif;
                                endforeach;
                        endif;
                }
        }
