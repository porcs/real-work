<?php   if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed'); 
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        echo header('Content-Type: text/html; charset=utf-8');
        if ( class_exists('ebaylib')   == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')   == FALSE ) require dirname(__FILE__).'/connect.php';
        
        class ebayshippings extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name == 'EBAY_USER' )                 $result['userID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                        endwhile;
                        return $result;
                }

                function parse_shippings_location() {
                        $this->data_0                                       = $this->connect->select_query("SHOW TABLES LIKE 'ebay_shipping_country'");
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->select_query("CREATE TABLE ebay_shipping_country(id_site int(11) NOT NULL, service varchar(255) NOT NULL, name varchar(255) NOT NULL, date_add datetime NOT NULL, PRIMARY KEY (id_site, name, service))");
                        endif;
                        $this->response                                     = $this->ebaylib->core->_parser;
                        if ( $this->response->Ack->__toString() == 'Success' && count($this->response->ShippingLocationDetails) ) :
                                foreach ( $this->response->ShippingLocationDetails as $shipping ) :
                                        $shipping = (array)$shipping;
                                        unset($dataShipping);
                                        $dataShipping               = array(
                                                'id_site'           => $this->id_site,
                                                'service'           => $shipping['ShippingLocation'],
                                                'name'              => $shipping['Description'],
                                                'date_add'          => date('Y-m-d H:i:s', strtotime("now")),
                                        );
                                        $this->connect->replace_db('ebay_shipping_country', $dataShipping);
                                endforeach;
                        endif;
                }
                
                function parse_shippings_service() {
                        $this->data_0                                       = $this->connect->select_query("SHOW TABLES LIKE 'ebay_shipping'");
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->select_query("CREATE TABLE ebay_shipping(id_shipping int(11) NOT NULL, id_site int(11) NOT NULL, service varchar(255) NOT NULL, name varchar(255) CHARACTER SET utf8 NOT NULL, is_international enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '0', is_flat enum('0','1') CHARACTER SET utf8 NOT NULL, is_calculated enum('0','1') CHARACTER SET utf8 NOT NULL, is_valid enum('0','1') CHARACTER SET utf8 NOT NULL, date_add datetime NOT NULL, PRIMARY KEY (id_shipping, id_site, service))");
                        endif;
                        $this->response                                     = $this->ebaylib->core->_parser;
                        if ( $this->response->Ack->__toString() == 'Success' && count($this->response->ShippingServiceDetails) ) :
                                foreach ( $this->response->ShippingServiceDetails as $shipping ) :
                                        $shipping = (array)$shipping;
                                        unset($dataShipping);
                                        $dataShipping               = array(
                                                'id_shipping'       => $shipping['ShippingServiceID'],
                                                'id_site'           => $this->id_site,
                                                'service'           => $shipping['ShippingService'],
                                                'name'              => $shipping['Description'],
                                                'is_international'  => isset($shipping['InternationalService']) ? 1 : 0,
                                                'is_flat'           => (!empty($shipping['ServiceType']) && is_array($shipping["ServiceType"])) ? (in_array("Flat", $shipping["ServiceType"]) ? 1 : 0) : (!empty($shipping['ServiceType']) && ($shipping["ServiceType"] == "Flat") ? 1 : 0),
                                                'is_calculated'     => (!empty($shipping['ServiceType']) && is_array($shipping["ServiceType"])) ? (in_array("Calculated", $shipping["ServiceType"]) ? 1 : 0) : (!empty($shipping['ServiceType']) && ($shipping["ServiceType"] == "Calculated") ? 1 : 0),
                                                'is_valid'          => (isset($shipping['ValidForSellingFlow']) && $shipping['ValidForSellingFlow'] == "true") ? 1 : 0,
                                                'date_add'          => date('Y-m-d H:i:s', strtotime("now")),
                                        );
                                        $this->connect->replace_db('ebay_shipping', $dataShipping);
                                endforeach;
                        endif;
                }
                
                function get_shippings() {
                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $this->ebaylib->GeteBayDetails($this->id_site, 'GetShippings', 'ShippingServiceDetails');
                                $this->parse_shippings_service();
                        endwhile;
                        $this->get_country();
                }
                
                function get_country() {
                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $this->ebaylib->GeteBayDetails($this->id_site, 'GetShippings', 'ShippingLocationDetails');
                                $this->parse_shippings_location();
                        endwhile;
                }
                
                function get_shippings_by_site() {
                        $this->id_site = $this->site;
                        $this->ebaylib->GeteBayDetails($this->id_site, 'GetShippings', 'ShippingServiceDetails');
                        $this->parse_shippings_service();
                        $this->get_country_by_site();
                }
                
                function get_country_by_site() {
                        $this->id_site = $this->site;
                        $this->ebaylib->GeteBayDetails($this->id_site, 'GetShippings', 'ShippingLocationDetails');
                        $this->parse_shippings_location();
                }


                function CompleteSale($siteID,
                		$ItemID,
                		$OrderID,
                		$OrderLineItemID,
                		$ShipmentTrackingNumber,
                		$ShippedTime,
                		$ShippingCarrierUsed) {
                	 
                	if ( !isset($siteID) ) :
                	return FALSE;
                	endif;
                	
                	$result = FALSE;
                	$parameter = array('ItemID' => $ItemID,
                			'OrderID' => $OrderID,
                			'OrderLineItemID' => $OrderLineItemID,
                			'ShipmentTrackingNumber' => $ShipmentTrackingNumber,
                			'ShippedTime' => $ShippedTime,
                			'ShippingCarrierUsed' => $ShippingCarrierUsed
                	);
                	
                	if ( !empty($parameter) ) :
                	$this->core->_session['siteID'] = $siteID;
                	$this->core->_session['method'] = "CompleteSale";
                	$this->core->_session['token'] = $this->ebaylib->token;
                	$this->core->_session['endPoint'] = $this->core->_session['serverUrl'];
                	$this->core->_template = 1;
                	$this->core->call("CompleteSale", "buildMessages", $parameter);
                	$this->core->parser();
                	$result = $this->core->_parser;
                	endif;
                	return $result;
                }
                
                public function get_ebay_info(){
                	return array('ebayUser' => $this->ebayUser);
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                            = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                              = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                                 = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->connect                              = new connect();
                        $this->user_configuration                   = $this->set_configuration($this->connect->select_query("SELECT * FROM configuration Where id_customer = '$this->user_id'"));
                        $this->ebayUser                             = $this->user_configuration['userID'];
                        $this->ebaylib                              = new ebaylib( ($this->user_configuration['appMode'] == 1) ? array(0) : array(1) );
                        $this->ebaylib->token                       = $this->user_configuration['token'];
                        $this->site_configuration                   = $this->connect->select_query("SELECT id_site FROM ebay_sites ORDER BY id_site ASC");
                        $this->ebaylib->core->_template             = 1;
                }
        }
