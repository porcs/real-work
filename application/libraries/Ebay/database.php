<?php 
    class database { 
            var $host = 'localhost'; 
            var $database; 
            var $connect_db; 
            var $selectdb; 
            var $db; 
            var $sql; 
            var $table; 
            var $where;  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            public function connectdb( $db_name = "database", $user = "username", $pwd = "password" ){ 
                    $this->database                             = $db_name; 
                    $this->username                             = $user; 
                    $this->password                             = $pwd; 
                    $this->connect_db                           = mysqli_connect($this->host, $this->username, $this->password ) or $this->_error(); 
                    $this->db                                   = mysqli_select_db($this->connect_db,$this->database) or $this->_error(); 
                    mysqli_query($this->connect_db,"SET NAMES UTF8");  
                    mysqli_query($this->connect_db,"SET character_set_results=utf8"); 
                    if ($this->db): return true;  
                    else :  return false; 
                    endif; 
            } 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function select_query( $sql = "sql" ) {  
                    if ( $result = mysqli_query($this->connect_db, $sql) ) : return $result;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function fetch( $sql = "sql" ){  
                    if ( $result = mysqli_fetch_array($sql) ) :  return $result;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function add_db( $table = "table", $data = "data", $rSql = false) { 
                foreach($data as $key => $value) :
                            if (empty($add)) : $add             = "(";  
                            else : $add                         = $add.",";  
                            endif;  

                            if (empty($val)) : $val             = "(";  
                            else : $val                         = $val.",";  
                            endif;  

                            $add                                = $add.$key;  
                            $val                                = $val."'".$value."'";  
                    endforeach; 
                    $add                                        = $add.")";  
                    $val                                        = $val.")";  
                    $sql                                        = "INSERT INTO ".$table." ".$add." VALUES ".$val;  
                  
                    if ($rSql) return $sql;
                    if (mysqli_query($this->connect_db,$sql)) : return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            } 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function replace_db( $table = "table", $data = "data", $rSql = false) { 
                    foreach($data as $key => $value) :
                            if (empty($add)) : $add             = "(";  
                            else : $add                         = $add.",";  
                            endif;  

                            if (empty($val)) : $val             = "(";  
                            else : $val                         = $val.",";  
                            endif;  

                            $add                                = $add.$key;  
                            $val                                = $val."'".$value."'";  
                    endforeach; 
                    $add                                        = $add.")";  
                    $val                                        = $val.")";  
                    $sql                                        = "REPLACE INTO ".$table." ".$add." VALUES ".$val;  
                    
                    if ($rSql) return $sql;
                    if (mysqli_query($this->connect_db,$sql)) : return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            } 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function insert_id( ){  
                    if ($result = mysqli_insert_id($this->connect_db)) : return $result;  
                    else :  $this->_error();  
                            return false;  
                    endif;    
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function count_rows( $sql = "sql" ){  
                    if ($result = mysqli_num_rows($sql)) : return $result;  
                    else :  $this->_error();  
                            return false;  
                    endif;    
            }  

            function num_rows( $table = "table", $field = "field", $where = "where" ) {  
                    if ($where=="") : $where                    = "";  
                    else : $where                               = " WHERE ".$where;  
                    endif;  

                    $sql                                        = "SELECT ".$field." FROM ".$table.$where;  
                    if (mysqli_query($this->connect_db,$sql)) : return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function update( $table = "table", $set = "set", $where = "where"){  
                    $sql                                        = "UPDATE ".$table." SET ".$set." WHERE ".$where; 
                    if (mysqli_query($this->connect_db,$sql)) :
                            return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            } 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function update_db($table="table",$data="data",$where="where"){  
                    $set                                        = "";  
                    foreach( $data as $key => $value ) :
                            if (!empty($set)) : $set            = $set.",";  
                            endif;   
                            $set                                = $set.$key."='".$value."'";  
                    endforeach; 

                    $sql="UPDATE ".$table." SET ".$set." WHERE ".$where;  
                    if (mysqli_query($this->connect_db,$sql)) : return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function del( $table = "table", $where = "where" ){  
                    $sql                                        = "DELETE FROM ".$table." WHERE ".$where;  
                    if (mysqli_query($this->connect_db,$sql)) : return true;  
                    else :  $this->_error();  
                            return false;  
                    endif;  
            } 

//            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            function sql_count_field( $table="table" ){ 
//                    $i                                          = 0; 
//                    $list                                       = mysql_list_fields($this->database, $table); 
//                    while( $i < mysql_num_fields($list) ) :
//                            $count_field[]                      = mysql_field_name($list, $i); 
//                            $i++; 
//                    endwhile;
//                    return $count_field; 
//            } 
//
//            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            function sql_list_table(){ 
//                    $result                                     = mysql_list_tables($this->database); 
//                    if (!$result) :
//                            print "DB Error, could not list tables\n"; 
//                            $this->_error(); 
//                            exit; 
//                    endif; 
//                    while ( $row = mysqli_fetch_row($result) ) : 
//                            $table[]                            = $row[0];    
//                    endwhile; 
//                    return  $table; 
//            } 

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function _error(){  
                    $this->error[]                              = mysqli_error($this->connect_db); 
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_configuration($id_user = null){
                    if ( !isset($id_user) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT * FROM configuration Where id_customer = '".$id_user."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_ebay_configuration($id_user = null, $id_site = null){
                    if ( !isset($id_user) || !isset($id_site) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT * FROM ebay_configuration Where id_customer = '$id_user' AND id_site = '".$id_site."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_ebay_site_by_id($id_site = null){
                    if ( !isset($id_site) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT * FROM ebay_sites Where id_site = '".$id_site."'";
                    return $this->select_query($sql);
            }  
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_ebay_site_by_name($name = null){
                    if ( !isset($name) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_site FROM ebay_sites Where name = '".$name."'";
                    return $this->select_query($sql);
            }  
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_ebay_site(){
                    $sql                                        = "SELECT id_site FROM ebay_sites ORDER BY id_site ASC";
                    return $this->select_query($sql);
            }  
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_ebay_currency($currency = null){
                    if ( !isset($currency) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT currency FROM ebay_sites Where name = '".$currency."'";
                    return $this->select_query($sql);
            }  
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_id_offers_packages($name_offer_pkg = null){
                    if ( !isset($name_offer_pkg) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_offer_pkg FROM offer_packages Where name_offer_pkg = '".$name_offer_pkg."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_user_mail($id_user = null){
                    if ( !isset($id_user) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT user_email FROM users WHERE id = '".$id_user."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_categories_ebay_site(){
                    $sql                                        = "SELECT id_category, id_site FROM ebay_categories_sites where id_category > '0' group by id_category, id_site";
//                    $sql                                        = "SELECT id_category, id_site FROM ebay_categories_sites c1 WHERE  NOT EXISTS (SELECT id_category FROM ebay_categories_sites1 c2 WHERE c1.id_category = c2.id_category) AND id_category > '0' group by id_category, id_site";
//                      $sql                                        = "select `c1`.`id_category` AS `id_category`,`c1`.`id_site` AS `id_site` from `ebay_categories_sites1` `c1` where ((not(exists(select `c2`.`id_category`,`c2`.`id_site` from `ebay_categories_sites` `c2` where ((`c1`.`id_category` = `c2`.`id_category`) and (`c1`.`id_site` = `c2`.`id_site`))))) and (not(exists(select `c3`.`id_category`,`c3`.`id_site` from `ebay_categories_features` `c3` where ((`c1`.`id_category` = `c3`.`id_category`) and (`c1`.`id_site` = `c3`.`id_site`))))) and (`c1`.`id_category` > '0')) group by `c1`.`id_category`,`c1`.`id_site`";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_categories_ebay_by_site($id_site = null){
                    if ( !isset($id_site) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_category FROM ebay_categories_sites where id_category > '0' AND id_site = '".$id_site."' group by id_category";
                    return $this->select_query($sql);
            } 
            
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_attributes_all_values(){
                    
                    $sql                                        = "SELECT * FROM ebay_attributes_value1 where 1";
                    return $this->select_query($sql);
            }
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_attributes_all_modes(){
                    
                    $sql                                        = "SELECT * FROM ebay_attributes_mode1 where 1";
                    return $this->select_query($sql);
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_attributes_all_groups(){
                    
                    $sql                                        = "SELECT * FROM ebay_attributes_group1 where 1";
                    return $this->select_query($sql);
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_attributes_group($name = null){
                    if ( !isset($name) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_group FROM ebay_attributes_group1 where `name` = '".$name."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_selection_mode($mode = null){
                    if ( !isset($mode) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_selection_mode FROM ebay_attributes_mode1 where `selection_mode` = '".$mode."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function get_attributes_value($id_group = null, $value = null){
                    if ( !isset($id_group) || !isset($value) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SELECT id_attribute FROM ebay_attributes_value1 WHERE `id_group` = '".$id_group."' AND `value` = '".$value."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function show_table($table = null){
                    if ( !isset($table) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "SHOW TABLES LIKE '".$table."'";
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function truncate_table($table = null){
                    if ( !isset($table) ) :
                            return FALSE;
                    endif;
                    $sql                                        = "TRUNCATE TABLE ".$table;
                    return $this->select_query($sql);
            }  

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            function create_table($table = null){
                    if ( !isset($table) ) :
                            return FALSE;
                    endif;
                    $sql['ebay_categories_features']            = "CREATE TABLE ebay_categories_features(id_category int(11) NOT NULL, id_site int(11) NOT NULL, is_enabled enum('0','1') NOT NULL DEFAULT '0', date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site))";
                    $sql['ebay_attributes']                     = "CREATE TABLE ebay_attributes1(id_category int(11), id_group int(11), id_site int(11), id_selection_mode int(11), date_add datetime NOT NULL,enable_validation tinyint default '1', PRIMARY KEY (id_category, id_group, id_site))";
                    $sql['ebay_attributes_group']               = "CREATE TABLE ebay_attributes_group1(id_group int(11) NOT NULL AUTO_INCREMENT, name varchar(255) NOT NULL, id_site int(11) NOT NULL, PRIMARY KEY (id_group))";
                    $sql['ebay_attributes_value']               = "CREATE TABLE ebay_attributes_value1(id_attribute int(11) NOT NULL AUTO_INCREMENT, id_site int(11) NOT NULL, id_group int(11) NOT NULL, value varchar(255) NOT NULL, PRIMARY KEY (id_attribute))";
                    $sql['ebay_attributes_mode']                = "CREATE TABLE ebay_attributes_mode1(id_selection_mode int(11) NOT NULL AUTO_INCREMENT, selection_mode varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'FreeText, Prefilled, SelectionOnly', PRIMARY KEY (id_selection_mode))";
                    $sql['ebay_listing_durations']              = "CREATE TABLE ebay_listing_durations(id int(11) NOT NULL AUTO_INCREMENT, listing_type varchar(30) NOT NULL, id_site int(11) NOT NULL, duration_set_id int(11), PRIMARY KEY (id, listing_type, id_site))";
                    $sql['ebay_listing_durations_set']          = "CREATE TABLE ebay_listing_durations_set(duration_set_id int(11) NOT NULL, duration varchar(20) NOT NULL, PRIMARY KEY (duration_set_id, duration))";
                    $sql['ebay_payment_method']                 = "CREATE TABLE ebay_payment_method(id int(11) NOT NULL AUTO_INCREMENT, id_site int(11) NOT NULL, payment_method varchar(30) NOT NULL, PRIMARY KEY (id, id_site))";
                    $sql['ebay_condition_values']               = "CREATE TABLE ebay_condition_values(id int(11) NOT NULL AUTO_INCREMENT, id_category int(11) NOT NULL, id_site int(11) NOT NULL, condition_value int(11) NOT NULL, condition_name varchar(50) NOT NULL, is_enabled enum('0','1') NOT NULL DEFAULT '1', date_add datetime NOT NULL, PRIMARY KEY (id, id_site))";
                    $sql['ebay_categories']                     = "CREATE TABLE ebay_categories1(id_category int(11) NOT NULL, id_site int(11) NOT NULL, id_parent int(11) NOT NULL, is_root_category enum('0','1') NOT NULL DEFAULT '0', date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site))";
                    $sql['ebay_categories_sites']               = "CREATE TABLE ebay_categories_sites1(id_category int(11) NOT NULL, id_site int(11) NOT NULL, level int(11) NOT NULL, name varchar(255) NOT NULL, date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site, level))";
                    
                    return $this->select_query($sql[$table]);
            }              
    } 
?>