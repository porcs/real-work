<?php   if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        echo header('Content-Type: text/html; charset=utf-8');
        if ( class_exists('ebaylib')   == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')   == FALSE ) require dirname(__FILE__).'/connect.php';
        if ( class_exists('ObjectBiz')      == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        require dirname(__FILE__).'/../tools.php';
        
        class ebaysynchronization extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                public $inventory;  
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_SYNCHRONIZE' )     $result['synchronization']      = $value;
                                else if ( $name == 'EBAY_VERIFY_PRODUCT' )  $result['verify_product']       = $value;
                                else if ( $name == 'EBAY_ENABLED_MANUAL' )  $result['enabled_manual']       = $value;
                                else if ( $name == 'EBAY_PROFILE_RULES' )   $result['profile_rules']        = $value;
                        endwhile;
                        return $result;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parse_features($itemSelling = null, $result_query = '') {
                        if ( empty($itemSelling) ) :
                                return $result_query;
                        endif;
                        
                        $duration                       = 2592000;
                        if (!empty($itemSelling->ListingDuration) && strval($itemSelling->ListingDuration) != "GTC") :
                                $duration                       = str_replace("Days_", "", strval($itemSelling->ListingDuration));
                                if ( is_numeric($duration) ) :
                                        $duration   = $duration * 86400;
                                endif;
                        endif;
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        unset($data_item, $exitst_data);
                        
                        $date_add                               = !empty($itemSelling->ListingDetails->StartTime) ? date('Y-m-d H:i:s', strtotime($itemSelling->ListingDetails->StartTime)) : '';
                        $sku                                    = !empty($itemSelling->SKU) ? strval($itemSelling->SKU) : '';

                        if ( !empty($itemSelling->Variations) ) :
                                foreach ( $itemSelling->Variations->Variation as $variation ) :
                                        $sku_list[]     =  strval($variation->SKU);
                                        foreach ( $variation->VariationSpecifics->NameValueList as $specific_variation ) :
                                                $data_item                      = array(
                                                        'id_item'               => strval($itemSelling->ItemID),
                                                        'id_site'               => $this->site,
                                                        'SKU'                   => !empty($variation->SKU) ? strval($variation->SKU) : '',
                                                        'start_price'           => !empty($variation->StartPrice) ? strval($variation->StartPrice) : '',
                                                        'quantity'              => !empty($variation->Quantity) ? strval($variation->Quantity) : 0,
                                                        'variation_name'        => !empty($specific_variation->Name) ? strval($specific_variation->Name) : '',
                                                        'variation_value'       => !empty($specific_variation->Value) ? strval($specific_variation->Value) : '',
                                                        'name_product'          => Db::escape_string(strval($itemSelling->Title)),
                                                        'ebay_user'             => !empty($this->ebayUser) ? $this->ebayUser : '',
                                                        'date_add'              => $date_add,
                                                        'date_end'              => !empty($duration) ? date('Y-m-d H:i:s', strtotime($itemSelling->ListingDetails->StartTime) + $duration) : '',
                                                );
                                                $result_query                   .= $this->objectbiz->mappingTableOnly("ebay_synchronization_attribute", $data_item);
                                        endforeach;
                                endforeach;
                                $sku_implode            = '"'.implode('", "', $sku_list).'"';
                                $sku                    = $this->objectbiz->synchronizeGetSkuByVariation($sku_implode);
                        endif;

                        unset($data_item);
                        $data_item                      = array(
                                'id_item'               => strval($itemSelling->ItemID),
                                'id_site'               => $this->site,
                                'SKU'                   => !empty($sku) ? $sku : '',
                                'name_product'          => Db::escape_string(strval($itemSelling->Title)),
                                'quantity'              => !empty($itemSelling->Quantity) ? strval($itemSelling->Quantity) : 0,
                                'ebay_user'             => !empty($this->ebayUser) ? $this->ebayUser : '',
                                'date_add'              => $date_add,
                                'date_end'              => !empty($duration) ? date('Y-m-d H:i:s', strtotime($itemSelling->ListingDetails->StartTime) + $duration) : '',
                        );
                        $result_query                   .= $this->objectbiz->mappingTableOnly("ebay_synchronization_store", $data_item);
                        //endif;
                        return $result_query;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_inventory($percent = '') {
                        $page = 1;
                        $total_page = 0;
                        $result_query = '';
                        $this->inventory = array();
                        $this->percent_count = 0;
                        $this->percent = $percent;
                        if ( !empty($this->percent) ) : $this->percent->set_running_task($this->percent_count); endif;
                        do {
                                $this->ebaylib->GetMyeBaySelling($this->site, $page);
                                $resultSelling              = $this->ebaylib->core->_parser;
                                $site                       = $this->site;
                                if ( !empty($resultSelling) && !empty($resultSelling->ActiveList) && !empty($resultSelling->ActiveList) ) :
                                        $this->percent_count = !empty($this->percent) ? (($page * 100) / strval($resultSelling->ActiveList->PaginationResult->TotalNumberOfPages)) : 0;
                                        if ( !empty($resultSelling->ActiveList->PaginationResult->TotalNumberOfPages) && empty($total_page) ) :
                                                $total_page = $resultSelling->ActiveList->PaginationResult->TotalNumberOfPages;
                                        endif;
                                        foreach ( $resultSelling->ActiveList->ItemArray->Item as $itemSelling ) :
                                                $result_query = $this->parse_features($itemSelling, $result_query);
                                        endforeach;
                                        if ( !empty($this->percent) ) : $this->percent->set_running_task($this->percent_count); endif;
                                endif;
                                
                                if ( empty($resultSelling->ActiveList) ) :
                                        if ( $page == 1 && !empty($resultSelling->Ack) && strval($resultSelling->Ack) == 'Success' ) :
                                                $this->objectbiz->deleteColumn('ebay_product_item_id', array('id_site' => $this->site, 'id_shop' => $this->id_shop, 'ebay_user' => $this->ebayUser, 'is_enabled' => '1'));
                                                $this->objectbiz->deleteColumn('ebay_synchronization_store', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                                $this->objectbiz->deleteColumn('ebay_synchronization_attribute', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                        endif;
                                        break;
                                endif;
                                $page++;
                        } while ( !empty($resultSelling->ActiveList) && $page <= $resultSelling->ActiveList->PaginationResult->TotalNumberOfPages );

                        if ( $total_page == ($page - 1) ) :
                                if ( !empty($result_query) ) :
                                        $this->objectbiz->deleteColumn('ebay_product_item_id', array('id_site' => $this->site, 'id_shop' => $this->id_shop, 'ebay_user' => $this->ebayUser, 'is_enabled' => '1'));
                                        $this->objectbiz->deleteColumn('ebay_synchronization_store', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                        $this->objectbiz->deleteColumn('ebay_synchronization_attribute', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                endif;
                                $this->objectbiz->transaction($result_query, true);
                        endif;
                        $this->_result['AddFixedPriceItem']                 = 'No Used';
                        $this->_result['RelistFixedPriceItem']              = 'No Used';
                        $this->_result['ReviseFixedPriceItem']              = 'No Used';
                        $this->_result['EndFixedPriceItem']                 = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']           = 'No Used';
                        if ( !empty($this->percent) ) : $this->percent->set_running_task(100); endif;
                        return $this->_result;
                }
                
                function generate_ebay_product_item_id(){
                	$results = $this->objectbiz->generate_ebay_product_item_id($this->user_name, $this->site, $this->id_shop);
                	if(!empty($results)){
	                	foreach($results as $row)
	                	{
	                		if(isset($delete_products[$row['id_item']])){
	                			$this->objectbiz->deleteColumn('ebay_product_item_id', array('id_item' => intval($row['id_item']), 'id_product' => intval($row['id_product'])));
	                		}
	                		else{//skip 1st dupplicate product
	                			$delete_products[$row['id_item']] = 1;
	                		}
	                	}
                	}
                }
                
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_percent_inventory($percent = '') {
                        $page = 1;
                        $total_page = 0;
                        $result_query = '';
                        $this->inventory = array();
                        $this->percent_count = 0;
                        $this->percent = $percent;
                        if ( !empty($this->percent) ) : $this->percent->set_running_task($this->percent_count); endif;
                        do {
                                $this->ebaylib->GetMyeBaySelling($this->site, $page);
                                $resultSelling              = $this->ebaylib->core->_parser;
                                $site                       = $this->site;
                                if ( !empty($resultSelling) && !empty($resultSelling->ActiveList) && !empty($resultSelling->ActiveList) ) :
                                        $this->percent_count = !empty($this->percent) ? (($page * 100) / strval($resultSelling->ActiveList->PaginationResult->TotalNumberOfPages)) : 0;
                                        if ( !empty($resultSelling->ActiveList->PaginationResult->TotalNumberOfPages) && empty($total_page) ) :
                                                $total_page = $resultSelling->ActiveList->PaginationResult->TotalNumberOfPages;
                                        endif;
                                        foreach ( $resultSelling->ActiveList->ItemArray->Item as $itemSelling ) :
                                                $result_query = $this->parse_features($itemSelling, $result_query);
                                        endforeach;
                                        if ( !empty($this->percent) ) : $this->percent->set_running_task($this->percent_count); endif;
                                endif;
                                
                                if ( empty($resultSelling->ActiveList) ) :
                                        if ( $page == 1 && !empty($resultSelling->Ack) && strval($resultSelling->Ack) == 'Success' ) :
                                                $this->objectbiz->deleteColumn('ebay_product_item_id', array('id_site' => $this->site, 'id_shop' => $this->id_shop, 'ebay_user' => $this->ebayUser, 'is_enabled' => '1'));
                                                $this->objectbiz->deleteColumn('ebay_synchronization_store', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                                $this->objectbiz->deleteColumn('ebay_synchronization_attribute', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                        endif;
                                        break;
                                endif;
                                $page++;
                        } while ( !empty($resultSelling->ActiveList) && $page <= $resultSelling->ActiveList->PaginationResult->TotalNumberOfPages );
                        
                        if ( $total_page == ($page - 1) ) :
                                if ( !empty($result_query) ) :
                                        $this->objectbiz->deleteColumn('ebay_product_item_id', array('id_site' => $this->site, 'id_shop' => $this->id_shop, 'ebay_user' => $this->ebayUser, 'is_enabled' => '1'));
                                        $this->objectbiz->deleteColumn('ebay_synchronization_store', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                        $this->objectbiz->deleteColumn('ebay_synchronization_attribute', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                endif;
                                $this->objectbiz->transaction($result_query, true);
                        endif;
                        $this->_result['AddFixedPriceItem']                 = 'No Used';
                        $this->_result['RelistFixedPriceItem']              = 'No Used';
                        $this->_result['ReviseFixedPriceItem']              = 'No Used';
                        $this->_result['EndFixedPriceItem']                 = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']           = 'No Used';
                        if ( !empty($this->percent) ) : $this->percent->set_running_task(100); endif;
                        return $this->_result;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                             = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->connect                          = new connect();
                        $this->objectbiz                        = new ObjectBiz(array($this->user_name));
                        $shop_default                           = $this->objectbiz->getDefaultShop($this->user_name);
                        $this->id_shop                          = (int)$shop_default['id_shop'];
                        $this->user_conf                        = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                             = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        $this->ebayUser                         = $this->user_conf['userID'];
                        $this->site                             = isset($this->user['site']) ? $this->user['site'] : 0;
                        $this->ebaylib                          = new ebaylib( (!empty($this->user_conf['appMode']) && $this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        if ( empty($this->user_conf['token']) ) :
                                $this->_result['msg']['empty_token']    = 1;
                                return $this->_result;
                        endif;
                        $this->ebaylib->token                   = $this->user_conf['token'];
                        $this->site_configuration               = $this->connect->get_ebay_site();
                        $this->ebaylib->core->_template         = 1;
                        $this->batch_id                         = uniqid();
                        $this->objectbiz->checkDB("ebay_synchronization_store");
                }
        }
