<?php   if ( !file_exists(dirname(__FILE__).'/key.php'))  exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/core.php')) exit('No direct script access allowed');
        if ( class_exists('key')  == FALSE ) require dirname(__FILE__).'/key.php';
        if ( class_exists('core') == FALSE ) require dirname(__FILE__).'/core.php';

        class ebaylib {
                public $key;
                public $core;
                protected $token;
                protected $_jobID;
                protected $_fileID;
                public static $_elements = array();
                public $version = 903;

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($production = array(0)) {
                        $this->dir_server                                   = DIR_SERVER;
                        if ( file_exists(dirname(__FILE__)."/key.php") ) :
                                $this->key                                  = new key($production);
                                $this->core                                 = new core($this->key->_key);
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ConfirmIdentity($id = '', $session = '') {
                        if ( !isset($id) || empty($session) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'SessionID'                                 => $session,
                        );
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "ConfirmIdentity";
                                $this->core->call("ConfirmIdentity", "buildMessages", $parameter);
                                $this->core->parser();
                        endif; 
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetCategories($id = '', $parameter = array()) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'DetailLevel'                               => 'ReturnAll',
                                'ViewAllNodes'                              => 1,
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetCategories";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetCategories", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetItem($id = '', $where = null, $parameter = array()) {
                        if ( !isset($id) && !empty($where) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                key($where)                                 => current($where),
                                'DetailLevel'                               => 'ReturnAll',
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetCategorySpecifics($id = '', $categoryID = '', $parameter = array()) {
                        if ( !isset($id) || !isset($categoryID) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'CategoryID'                                => $categoryID,
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetCategorySpecifics";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetCategorySpecifics", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetCategorySpecificsFile($id = '', $parameter = array()) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'CategorySpecificsFileInfo'                 => 1,
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetCategorySpecifics";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetCategorySpecifics", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetCategoryFeatures($id = '', $categoryID = '', $featureID = 'VariationsEnabled', $parameter = array()) {
                        if ( !isset($id) || !isset($categoryID) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'CategoryID'                                => $categoryID,
                                'FeatureID'                                 => $featureID,
                                'ViewAllNodes'                              => 1,
                                'AllFeaturesForCategory'                    => 1,
                                'DetailLevel'                               => 'ReturnAll',
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetCategoryFeatures";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetCategoryFeatures", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetSessionID($id = '', $lang = '') {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'RuName'                                    => $this->core->_session['runame'],
                                'lang'                                      => !empty($lang) ? $lang : '',
                        );
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetSessionID";
                                $this->core->call("GetSessionID", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GeteBayOfficialTime($id = '', $token = null) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'DetailName'                                => '1',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GeteBayOfficialTime";
                                $this->core->_session['token']              = $token;
                                $this->core->call('GeteBayOfficialTime', "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GeteBayDetails($id = '', $call = '', $details_service = 'ShippingServiceDetails', $parameter = array()) {
                        if ( !isset($id) || empty($call) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'DetailName'                                => $details_service,
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GeteBayDetails";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call($call, "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetMyeBaySelling($id = '', $page = null, $call = 'ActiveList', $day = 30, $parameter = array()) {
                        if ( !isset($id) || empty($call) || empty($page) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'ErrorLanguage'                             => 'en_US',
                                'PageNumber'                                => $page,
                        );

                        if ( !empty($call) && count($call) && $call == 'ActiveList' ) :
                                $parameter['ActiveList']                    = array('Sort' => 'TimeLeft');
                        elseif ( !empty($call) && count($call) && $call == 'DeletedFromSoldList' ) :
                                $parameter['DeletedFromSoldList']           = array(
                                        'Sort'                              => 'TimeLeft',
                                        'OrderStatusFilter'                 => 'All',
                                        'DurationInDays'                    => $day,
                                );
                        elseif ( !empty($call) && count($call) && $call == 'SoldList' ) :
                                $parameter['SoldList']                      = array(
                                        'Sort'                              => 'TimeLeft',
                                        'OrderStatusFilter'                 => 'All',
                                        'DurationInDays'                    => $day,
                                );
                        endif;

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetMyeBaySelling";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetMyeBaySelling", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetOrders($id = '', $page = 1, $from = '', $to = '', $status = '', $parameter = array()) {
                        if ( !isset($id) || empty($from) || empty($to) || empty($status) || empty($page)  ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'CreateTimeFrom'                            => $from,
                                'CreateTimeTo'                              => $to,
                                'OrderStatus'                               => $status,
                                'PageNumber'                                => $page,
                                'ErrorLanguage'                             => 'en_US',
                                'DetailLevel'                               => 'ReturnAll',
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetOrders";
                                $this->core->_session['compatability']      = "879";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetOrders", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetModifierOrders($id = '', $page = 1, $from = '', $to = '', $parameter = array()) {
                        if ( !isset($id) || empty($from) || empty($to) || empty($page)  ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'ModTimeFrom'                               => $from,
                                'ModTimeTo'                                 => $to,
                                'PageNumber'                                => $page,
                                'ErrorLanguage'                             => 'en_US',
                                'DetailLevel'                               => 'ReturnAll',
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetOrders";
                                $this->core->_session['compatability']      = "879";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetOrders", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetSellerTransactions($id = '', $sku = '', $parameter = array()) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'IncludeCodiceFiscale'                      => 1,
                                'IncludeFinalValueFee'                      => 1,
                                'IncludeContainingOrder'                    => 1,
                                'InventoryTrackingMethod'                   => 'SKU',
                                'ErrorLanguage'                             => 'en_US',
                                'DetailLevel'                               => 'ReturnAll',
                        );
                        
                        if ( !empty($sku) && count($sku) ) :
                                $parameter['SKUArray']                      = $sku;
                        endif;

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetSellerTransactions";
                                $this->core->_session['compatability']      = "879";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetSellerTransactions", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetItemTransactions($id = '', $id_item = null, $transaction_id = null, $parameter = array()) {
                        if ( !isset($id) || (!isset($id_item) && empty($id_item)) || (!isset($transaction_id) && empty($transaction_id)) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'ItemID'                                    => $id_item,
                                'TransactionID'                             => $transaction_id,
                                'ErrorLanguage'                             => 'en_US',
                                'DetailLevel'                               => 'ReturnAll',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetItemTransactions";
                                $this->core->_session['compatability']      = "879";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetItemTransactions", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ReviseCheckoutStatus($id = '', $id_order = null, $buyer_id = null, $payment_method = null, $shipping_address = null, $parameter = array()) {
                        if ( !isset($id) || (!isset($id_order) && empty($id_order)) || (!isset($buyer_id) && empty($buyer_id))|| (!isset($payment_method) && empty($payment_method)) || (!isset($shipping_address) && empty($shipping_address)) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'OrderID'                                   => $id_order,
                                'PaymentMethodUsed'                         => $payment_method,
                                'BuyerID'                                   => $buyer_id,
                                'ShippingAddress'                           => $shipping_address,
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "ReviseCheckoutStatus";
                                $this->core->_session['compatability']      = "879";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("ReviseCheckoutStatus", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function FetchToken($id = '', $session = '') {
                        if ( !isset($id) || empty($session) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'SessionID'                                 => $session,
                        );
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "FetchToken";
                                $this->core->call("FetchToken", "buildMessages", $parameter);
                                $this->core->parser();
                        endif; 
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function SetUserPreferences($id = '') {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'ErrorLanguage'                             => 'en_US',
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "SetUserPreferences";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("SetUserPreferences", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetUserPreferences($id = '') {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetUserPreferences";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetUserPreferences", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function GetStore($id = '') {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "GetStore";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("GetStore", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ConditionDefinitions($cond = '') {
                        if ( strtolower($cond) == "new" )                               : $conditionValue = 1000;
                                elseif ( strtolower($cond) == "new other" )             : $conditionValue = 1500;
                                elseif ( strtolower($cond) == "refurbished" )           : $conditionValue = 2500;
                                elseif ( strtolower($cond) == "seller refurbished" )    : $conditionValue = 2500;
                                elseif ( strtolower($cond) == "used" )                  : $conditionValue = 3000;
                                elseif ( strtolower($cond) == "used like new" )         : $conditionValue = 4000;
                                elseif ( strtolower($cond) == "very good" )             : $conditionValue = 4000;
                                elseif ( strtolower($cond) == "used very good" )        : $conditionValue = 4000;
                                elseif ( strtolower($cond) == "good" )                  : $conditionValue = 5000;
                                elseif ( strtolower($cond) == "used good" )             : $conditionValue = 5000;
                                elseif ( strtolower($cond) == "acceptable" )            : $conditionValue = 6000;
                                elseif ( strtolower($cond) == "used acceptable" )       : $conditionValue = 6000;
                                else                                        : $conditionValue = 1000;
                        endif;
                        return $conditionValue;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ItemSpecifics($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        $priority_spec                                      = 1;
                        if ( isset($product['spec']) ) :
                                foreach ( $product['spec'] as $key => $spec ) :
                                        $priority_value                     = 1;
                                        foreach ( $spec as $spec_key => $spec_value ) :
                                                if ( $spec_key != 'Color' && $spec_key != 'Disk space' ) :
                                                        $parameter['ItemSpecifics']['NameValueList']
                                                        ['_Repeat'][$priority_spec] = array(
                                                                'Name'              => $spec_key,
                                                        );
                                                        $parameter['ItemSpecifics']['NameValueList']
                                                                ['_Repeat'][$priority_spec]['Value']
                                                                ['_Repeat'][1] = '';
                                                        foreach ( $spec_value as $value ) :
                                                                $parameter['ItemSpecifics']['NameValueList']
                                                                ['_Repeat'][$priority_spec]['Value']
                                                                ['_Repeat'][1] .= $value." ";
                                                                $priority_value++ ;
                                                        endforeach;
                                                endif;
                                        endforeach;
                                        $priority_spec++ ;
                                endforeach;
                        endif;
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ProductListingDetails($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;
                        if ( (!empty($product['ean13']) && count($product['ean13'])) ) :
                                $parameter['ProductListingDetails']['EAN']         = $product['ean13'];
                        elseif ( (!empty($product['upc']) && count($product['upc'])) ) :
                                $parameter['ProductListingDetails']['UPC']         = $product['upc'];
                        endif;
                        if ( (!empty($product['manufacturer']) && count($product['manufacturer'])) ) :
                                $m_name = current($product['manufacturer']);
                                if ( !empty($product['supplier']) ) {
                                    $key_name = current($product['supplier']);
                                }
                                $parameter['ProductListingDetails']['Brand']       = current($product['manufacturer']);
                                $parameter['ProductListingDetails']['MPN']         = !empty($key_name['supplier_reference']) ? $key_name['supplier_reference'] : $product['reference'];
                        endif;
                        return $parameter;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function PrimaryCategory($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        if ( isset($product['category']['mapping_id']) ) :
                                $parameter['PrimaryCategory']               = array('CategoryID' => $product['category']['mapping_id']);
                        endif;
                        return $parameter;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function SecondaryCategory($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        if ( isset($product['secondary_category']['mapping_id']) ) :
                                $parameter['SecondaryCategory']             = array('CategoryID' => $product['secondary_category']['mapping_id']);
                        endif;
                        return $parameter;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function StoreCategory($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        if ( isset($product['store_category']['mapping_id']) ) :
                                $parameter['Storefront']                    = array(
                                            'StoreCategoryID'               => $product['store_category']['mapping_id'],
                                            'StoreCategoryName'             => $product['store_category']['mapping'],
                                );
                                if ( isset($product['secondary_store_category']['mapping_id']) ) :
                                        $parameter['Storefront']['StoreCategory2ID']    = $product['secondary_store_category']['mapping_id'];
                                        $parameter['Storefront']['StoreCategory2Name']  = $product['secondary_store_category']['mapping'];
                                endif;
                        endif;
                        return $parameter;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function PaymentMethod($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        $parameter['PaymentMethods']                        = array('_Repeat' => array());
                        if ( !empty($product['payment_method']) ) :
                                foreach ( $product['payment_method'] as $payment) :
                                        $parameter['PaymentMethods']['_Repeat'] = array_merge($parameter['PaymentMethods']['_Repeat'], array($payment));
                                endforeach;
                        endif;
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ReturnPolicy($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;
                        //$product['return']['name'] = 'ReturnsNotAccepted';
                        $parameter['ReturnPolicy']['ReturnsAcceptedOption'] = !empty($product['return']['name']) ? $product['return']['name'] : 'ReturnsNotAccepted';
                        if ( isset($product['return']) && isset($product['return']['name']) && $product['return']['name'] == 'ReturnsAccepted' ) :
                                $parameter['ReturnPolicy']                  = array(
                                        'ReturnsAcceptedOption'             => 'ReturnsAccepted',
                                        'ReturnsWithinOption'               => isset($product['return']['days']) ? $product['return']['days'] : 'Days_30',
                                        'ShippingCostPaidByOption'          => isset($product['return']['pays']) ? $product['return']['pays'] : 'Buyer',
                                		'Description'						=> !empty($product['return']['description']) ? $product['return']['description'] : "No return product"
                                );
                        endif;
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ShippingDetails($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;

                        $priority_local                                     = 1;
                        $priority_international                             = 1;
                        if ( isset($product['carrier_domestic']) ) :
                        foreach ( $product['carrier_domestic'] as $carrier_id => $carriers ) :
                                foreach ( $carriers as $id_priority => $carrier ) :
                                        if ( isset($carrier['name']['mapping']) && $priority_local < 5 ) :
                                                $parameter['ShippingDetails']
                                                ['ShippingServiceOptions']
                                                ['_Repeat'][$priority_local]            = array(
                                                        'ShippingService'               => isset($carrier['name']['mapping']) ? $carrier['name']['mapping'] : $carrier['name']['default'],
                                                        'ShippingServicePriority'       => $priority_local,
                                                        'ShippingServiceAdditionalCost' => !empty($carrier['additionals']) ? $carrier['additionals'] : 0,
                                                        'ShippingServiceCost'           => !empty($product['shipping_override']) ? $product['shipping_override'] : (!empty($carrier['cost']) ? $carrier['cost'] : 0),
                                                );

                                                if ( (empty($parameter['ShippingDetails']['ShippingServiceOptions']['_Repeat'][$priority_local]['ShippingServiceCost']) && $priority_local == 1) 
                                                  || ($parameter['ShippingDetails']['ShippingServiceOptions']['_Repeat'][$priority_local]['ShippingServiceCost'] == 0 && $priority_local == 1) 
                                                   ) :
                                                        $parameter['ShippingDetails']['ShippingServiceOptions']['_Repeat'][$priority_local]['FreeShipping']   = true;
                                                endif;
                                                $priority_local++ ;
                                        endif;
                               endforeach;
                       endforeach;
                       endif;
                       
                       if ( isset($product['carrier_international']) ) :
                                foreach ( $product['carrier_international'] as $carriers_id => $carriers ) :
                                        foreach ( $carriers as $id_priority => $carrier ) :
                                                if ( $priority_international < 5 ) :
                                                        $parameter['ShippingDetails']
                                                        ['InternationalShippingServiceOption']
                                                        ['_Repeat'][$priority_international]    = array(
                                                                'ShippingService'               => isset($carrier['name']['mapping']) ? $carrier['name']['mapping'] : '',
                                                                'ShippingServicePriority'       => $priority_international,
                                                                'ShippingServiceCost'           => !empty($product['shipping_override']) ? $product['shipping_override'] : (!empty($carrier['cost']) ? $carrier['cost'] : 0),
                                                                'ShippingServiceAdditionalCost' => !empty($carrier['additionals']) ? $carrier['additionals'] : 0,
                                                                'ShipToLocation'                => array('_Repeat' => array($carrier['country'])),
                                                        );
//                                                        if ( isset($carrier['country']) ) :
//                                                                foreach ( $carrier['country'] as $country_location ) :
//                                                                        $parameter['ShippingDetails']
//                                                                        ['InternationalShippingServiceOption']
//                                                                        ['_Repeat'][$priority_international]
//                                                                        ['ShipToLocation']['_Repeat'][]       = $country_location;
//                                                                endforeach;
//                                                        endif;
                                                        $priority_international++ ;
                                                endif;
                                        endforeach;
                                endforeach;
                        endif;
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function Variations($parameter = array(), $product = array()) {
                        if ( empty($product) ) :
                                return $parameter;
                        endif;
                        $variation                                                  = 1;
                        $variationSpecificsSet                                      = 1;
                        $picture                                                    = 1;
                        $array_picture_node                                         = array();
                        $array_name_count                                           = array();
                        if ( isset($product['spec_mapping']) ) :
                                foreach ( $product['spec_mapping'] as $spec_id => $specs ) :
                                        $variationSpecificsSet_node                 = 1;
                                        foreach ( $specs as $attr_key => $spec_value ) :
                                                if ( count($product['spec_mapping'][$spec_id][$attr_key]) > count($array_name_count) ) :
                                                        $array_name_count           = $product['spec_mapping'][$spec_id][$attr_key];
                                                        $array_key_count            = $attr_key;
                                                endif;
                                                $parameter['Variations']['VariationSpecificsSet']['NameValueList']
                                                ['_Repeat'][$variationSpecificsSet] = array(
                                                        'Name'                      => $attr_key,
                                                );
                                                foreach ( $spec_value as $s_val) :
                                                        $parameter['Variations']['VariationSpecificsSet']['NameValueList']
                                                        ['_Repeat'][$variationSpecificsSet]['Value']
                                                        ['_Repeat'][$variationSpecificsSet_node]    = $s_val;
                                                        $variationSpecificsSet_node++;
                                                endforeach;
                                        endforeach;
                                        $variationSpecificsSet++;
                                endforeach;

                                if ( isset($product['combinations']) && is_array($product['combinations']) && count($product['combinations']) ) :
                                        $attr_val                           = array();
                                        $picture_node                       = 1;
                                        foreach ( $product['combinations'] as $combination_id => $combination ) :
                                                $picture++;
                                                $variation_node                             = 1;
                                                        if ( isset($combination['quantity']) && !empty($combination['price']) && isset($combination['attribute_mapping_name'])) :
                                                                $parameter['Variations']['Variation']
                                                                ['_Repeat'][$variation]             = array(
                                                                        'SKU'                       => $combination['sku']/*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/,
                                                                        'EAN'                       => !empty($combination['ean13']) ? $combination['ean13'] : ''/*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/,
                                                                        'UPC'                       => !empty($combination['upc']) ? $combination['upc'] : '' /*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/,
                                                                        'StartPrice'                => !empty($combination['price_discount']) ? $combination['price_discount'] : $combination['price'],
                                                                        'Quantity'                  => (isset($combination['quantity']) && $combination['quantity'] > 0) ? $combination['quantity'] : 0,
                                                                );
                                                                foreach ( $combination['attribute_mapping_name'] as $attributes_id => $attributes_name ) :
                                                                        $parameter['Variations']['Variation']
                                                                        ['_Repeat'][$variation]['VariationSpecifics']['NameValueList']
                                                                        ['_Repeat'][$variation_node] = array(
                                                                                'Name'      => $attributes_name['Name'],
                                                                                'Value'     => current($combination['attribute_mapping_name'][$attributes_id]['Value']),
                                                                        );
                                                                        $variation_node++;
                                                                endforeach;
                                                        endif;
                                                $variation++;
                                        endforeach;
                                endif;
                        endif;
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ItemType($product = array(), $parameter = array()) {
                        $parameter                                          = array(
                                'Title'                                     => isset($product['name']) ? $product['name'] : '',
                                'Description'                               => isset($product['description']) ? $product['description'] : '',
                                'Currency'                                  => isset($product['currency']) ? $product['currency'] : 'EUR',
                                'Country'                                   => isset($product['country']) ? $product['country'] : '',
                                'PostalCode'                                => isset($product['postal_code']) ? $product['postal_code'] : '',
                                'Location'                                  => isset($product['address']) ? $product['address'] : '',
                                'StartPrice'                                => isset($product['price']) ? $product['price'] : '0.00',
                                'DispatchTimeMax'                           => isset($product['dispatch_time']) ? $product['dispatch_time'] : '3',
                                'ListingDuration'                           => isset($product['listing_duration']) ? $product['listing_duration'] : 'Days_3',
                                'CategoryMappingAllowed'                    => TRUE,
                                'PaymentMethods'                            => 'PayPal',
                                'PayPalEmailAddress'                        => isset($product['paypal_email']) ? $product['paypal_email'] : '',
                                'ConditionID'                               => $this->ConditionDefinitions($product['condition']),
                                'ConditionDescription'                      => isset($product['condition_description']) ? $product['condition_description'] : '',
                                'ConditionDisplayName'                      => ucfirst(strtolower($product['condition'])),
                                'ShippingTermsInDescription'                => TRUE,
                                'Quantity'                                  => isset($product['quantity']) ? 1 : 0,
                                'PictureDetails'                            => array('GalleryType' => 'Gallery', 'GalleryURL' => isset($product['images']) ? $product['images'] : '', 'PictureURL' => isset($product['images']) ? $product['images'] : ''),
//                                'PictureDetails'                            => array('GalleryType' => !empty($product['gallery_plus']) ? 'Plus' : 'Gallery', 'GalleryURL' => isset($product['images']) ? $product['images'] : '', 'PictureURL' => isset($product['images']) ? $product['images'] : ''),
                        );
                        $parameter                                          = $this->ReturnPolicy($parameter, $product);
                        $parameter                                          = $this->ShippingDetails($parameter, $product);
                        $parameter                                          = $this->PrimaryCategory($parameter, $product);
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function OfferFixedItemType($products = array(), $parameter = array()) {
                        $parameter['ErrorLanguage']                         = 'en_US';
                        $parameter['Version']                               = $this->version;
                        $messaged                                           = array();
                        foreach ( $products as $id_product => $product ) : 
                                if ( isset($product['combinations']) && is_array($product['combinations']) && count($product['combinations']) ) :
                                        foreach ( $product['combinations'] as $combination_id => $combination ) :
                                                if ( isset($combination['quantity']) && !empty($combination['price']) ) :
                                                        $parameter['InventoryStatus']
                                                        ['_Repeat'][]               = array(
                                                                'ItemID'            => !empty($product['relist']) ? $product['relist'] : '',
                                                                'SKU'               => $combination['sku']/*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/,
                                                                'StartPrice'        => $combination['price'],
                                                                'Quantity'          => $combination['quantity'],
                                                                'id_product'        => $product['id_product'],
                                                                'name'              => $product['name'],
                                                                'batch_id'          => $product['batch_id'],
                                                        );
                                                        $messaged[]                 = $combination['sku']; 
                                                endif;
                                        endforeach;
                                else :
                                        $parameter['InventoryStatus']['_Repeat'][]          = array(
                                                'ItemID'                                    => !empty($product['relist']) ? $product['relist'] : '',
                                                'Quantity'                                  => $product['quantity'],
                                                'SKU'                                       => !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : '')/*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/,
                                                'StartPrice'                                => !empty($product['price']) ? $product['price'] : '0.00',
                                                'id_product'                                => $product['id_product'],
                                                'name'                                      => $product['name'],
                                                'batch_id'                                  => $product['batch_id'],
                                        );
                                        $messaged[]                         = !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : '');
                                endif;
                        endforeach;
                        $parameter['MessageID']                             = implode('-#-#-', $messaged);
                        return $parameter;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function FixedItemType($product = array(), $parameter = array()) {
                        $sku                                                = !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : '')/*."_#_".((isset($product['site']) && is_numeric($product['site'])) ? $product['site'] : '')*/;
                        $parameter['ErrorLanguage']                         = 'en_US';
                        $parameter['DetailLevel']                           = 'ReturnAll';
                        $parameter['Version']                               = $this->version;
                        $parameter['MessageID']                             = !empty($product['name']) ? $product['name']."-#-#-".$product['id_product']."-#-#-".$product['batch_id']."-#-#-".$sku : '';
                        $parameter['Item']                                  = array(
                                'Title'                                     => !empty($product['name']) ? mb_substr($product['name'], 0, 80,'UTF-8') : '',
                                'Description'                               => !empty($product['description']) ? $product['description'] : $product['name'],
                                'Description_short'                         => !empty($product['description_short']) ? $product['description_short'] : $product['name'],
                                'Currency'                                  => !empty($product['currency']) ? $product['currency'] : 'EUR',
                                'Country'                                   => !empty($product['country']) ? $product['country'] : '',
                                'PostalCode'                                => !empty($product['postal_code']) ? $product['postal_code'] : '',
                                'Location'                                  => !empty($product['address']) ? mb_substr($product['address'], 0, 45,'UTF-8') : '',
                                'DispatchTimeMax'                           => isset($product['dispatch_time']) ? $product['dispatch_time'] : '5',
                                'ListingDuration'                           => !empty($product['listing_duration']) ? $product['listing_duration'] : 'Days_3',
                                'CategoryMappingAllowed'                    => TRUE,
                                'PayPalEmailAddress'                        => !empty($product['paypal_email']) ? $product['paypal_email'] : '',
                                'ConditionID'                               => is_numeric($product['condition'])?$product['condition']:$this->ConditionDefinitions($product['condition']),//$this->ConditionDefinitions($product['condition']),
                                'ConditionDescription'                      => isset($product['condition_description']) ? $product['condition_description'] : '',
                                'ShippingTermsInDescription'                => TRUE,
                                'ApplicationData'                           => $product['id_product']."-#-#-".$sku,
                                'SalesTaxPercent'                           => !empty($product['salesTax']) ? $product['salesTax'] : '',
                                'Manufacturer'                              => !empty($product['manufacturer']) ? current($product['manufacturer']) : '',
                                'HitCounter'                                => !empty($product['visitor_counter']) ? $product['visitor_counter'] : 'NoHitCounter',
                                'AutoPay'                                   => !empty($product['auto_pay']) ? 1 : 0,
                                'SKU'                                       => $sku,
                                'InventoryTrackingMethod'                   => 'ItemID',
                                'Delete_variation'                          => !empty($product['variation_old']) ? $product['variation_old'] : '',
                                'PaymentInstructions'                       => !empty($product['PaymentInstructions']) ? $product['PaymentInstructions'] : '',
                        );

                        if ( isset($product['relist']) ) :
                                $parameter['Item']['ItemID']                = isset($product['relist']) ? $product['relist'] : '';
                        endif;
                        $parameter['Item']                                  = $this->PaymentMethod($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->ReturnPolicy($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->ShippingDetails($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->PrimaryCategory($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->SecondaryCategory($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->StoreCategory($parameter['Item'], $product);
                        $parameter['Item']                                  = $this->ProductListingDetails($parameter['Item'], $product);

                        if ( isset($product['template_description']) ) :
                                $parameter['Item']['template_description']  = $product['template_description'];
                        endif;
                        if ( isset($product['images']) && is_array($product['images']) ) :
                                $product_image                              = current($product['images']);
                        endif;
                        if ( isset($product['spec_mapping']) ) :
                                $parameter['Item']                          = $this->Variations($parameter['Item'], $product);
                                if ( !isset($parameter['Item']['Variations']['Pictures']) ):
                                            if ( isset($product['images']) && is_array($product['images']) ) :
                                                    $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][0]    = '0';
                                                    $parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][0]    = 0;
                                                    foreach ( $product['images'] as $images ) :
                                                            $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][]               = isset($images['url']) ? $images['url'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
                                                            $parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][]               = isset($images['url']) ? 1 : 0;
                                                    endforeach;
                                                    for ( $i = 1; $i <= 5; $i++) :
                                                            if ( empty($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i]) ) :
                                                                    $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i] = 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
                                                    				$parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][$i] = 0;
                                                            endif;
                                                    endfor;
                                                    unset($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][0]);
                                                    unset($parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][0]);
                                            
                                            else :
                                                    for ( $i = 1; $i <= 5; $i++) :
	                                                    if ( empty($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i]) ) :
		                                                    $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i] = 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
		                                                    $parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][$i] = 0;
	                                                    endif;
                                                    endfor;
                                            endif;
                                endif;
                        else :
                                $parameter['Item']['Quantity']              = (isset($product['quantity']) && $product['quantity'] > 0) ? $product['quantity'] : 0;
                                $parameter['Item']['StartPrice']            = isset($product['price']) ? $product['price'] : '0.00';
                                $parameter['Item']['PictureDetails']        = array('GalleryType' => 'Gallery', 'GalleryURL' => !empty($product['images']) ? $product['images'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif');
//                                $parameter['Item']['PictureDetails']        = array('GalleryType' => !empty($product['gallery_plus']) ? 'Plus' : 'Gallery', 'GalleryURL' => !empty($product['images']) ? $product['images'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif');
                                if ( isset($product['images']) && is_array($product['images']) ) :
                                        $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][0]    = '0';
                                        $parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][0]    = '0';
                                        foreach ( $product['images'] as $images ) :
                                                $parameter['Item']['PictureDetails']['PictureURL']
                                                    ['_Repeat'][]               = isset($images['url']) ? $images['url'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
                                        		$parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][] = isset($images['url']) ? 1 : 0;
                                        endforeach;
                                        for ( $i = 1; $i <= 5; $i++) :
                                                if ( empty($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i]) ) :
                                                        $parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i] = 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
                                        				$parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][$i] = 0;
                                                endif;
                                        endfor;
                                        unset($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][0]);
                                        unset($parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][0]);
                               else :
                              	for ( $i = 1; $i <= 5; $i++) :
	                              	if ( empty($parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i]) ) :
		                              	$parameter['Item']['PictureDetails']['PictureURL']['_Repeat'][$i] = 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
		                              	$parameter['Item']['PictureDetails']['PictureURL']['_HasPic'][$i] = 0;
	                              	endif;
                              	endfor;
                              endif;
                        endif;
                        if ( !empty($product['wizard'])/* && $product['listing_duration'] == 'GTC'*/ ) :
//                                $parameter['Item']['OutOfStockControl']     = 1;
                        elseif ($product['listing_duration'] == 'GTC' ):
                                $parameter['Item']['OutOfStockControl']     = 1;                        
                        endif;
                        return $parameter;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function AddItem($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;
                        $parameter['ErrorLanguage']                         = 'en_US';
                        $parameter['DetailLevel']                           = 'ReturnAll';
                        $parameter['Item']                                  = $this->ItemType($product);

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "AddItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("AddItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function AddItems($id = '', $products = array(), $parameter = array()) {
                        if ( !isset($id) || empty($products) ) :
                                return FALSE;
                        endif;
                        
                        $messageID                                          = 1;
                        $parameter['ErrorLanguage']                         = 'en_US';
                        $parameter['DetailLevel']                           = 'ReturnAll';
                        foreach ( $products['product'] as $product_id => $product ) :
                                $parameter['AddItemRequestContainer']
                                ['_Repeat'][$messageID]                     = array(
                                        'MessageID'                         => $messageID,
                                        'Item'                              => $this->ItemType($product),
                                );
                                $messageID++ ;
                        endforeach;

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "AddItems";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("AddItems", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function AddFixedPriceItem($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = $this->FixedItemType($product);

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "AddFixedPriceItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("AddFixedPriceItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function EndFixedPriceItem($id = '', $product_sku = array(), $parameter = array()) {
                        if ( !isset($id) || !isset($product_sku) ) :
                                return FALSE;
                        endif;

                        $parameter                                          = array(
                                'SKU'                                       => $product_sku,
                                'EndingReason'                              => 'NotAvailable',
                                'ErrorLanguage'                             => 'en_US',
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "EndFixedPriceItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("EndFixedPriceItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function RelistFixedPriceItem($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = $this->FixedItemType($product);

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "RelistFixedPriceItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("RelistFixedPriceItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ReviseFixedPriceItem($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = $this->FixedItemType($product);

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "ReviseFixedPriceItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("ReviseFixedPriceItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function beforeReviseInventoryStatus($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;
                        
                        $this->core->_output = $this->OfferFixedItemType($product);
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function ReviseInventoryStatus($id = '', $parameter = array()) {
                        if ( !isset($id) || empty($parameter) ) :
                                return FALSE;
                        endif;

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "ReviseInventoryStatus";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("ReviseInventoryStatus", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function reviseItem($id = '', $product = array(), $parameter = array()) {
                        if ( !isset($id) || empty($product) ) :
                                return FALSE;
                        endif;

                        $parameter['ErrorLanguage']                         = 'en_US';
                        $parameter['DetailLevel']                           = 'ReturnAll';
                        $parameter['Item']                                  = array(
                                'Title'                                     => isset($product['name']) ? $product['name'] : '',
                                'Description'                               => isset($product['description']) ? $product['description'] : '',
                                'ItemID'                                    => isset($product['relist']) ? $product['relist'] : '',
                                'Currency'                                  => isset($product['currency']) ? $product['currency'] : 'EUR',
                                'Country'                                   => isset($product['country']) ? $product['country'] : '',
                                'PostalCode'                                => isset($product['postal_code']) ? $product['postal_code'] : '',
                                'Location'                                  => isset($product['address']) ? $product['address'] : '',
                                'DispatchTimeMax'                           => isset($product['dispatch_time']) ? $product['dispatch_time'] : '3',
                                'ListingDuration'                           => isset($product['listing_duration']) ? $product['listing_duration'] : 'Days_3',
                                'CategoryMappingAllowed'                    => TRUE,
                                'PaymentMethods'                            => 'PayPal',
                                'PayPalEmailAddress'                        => isset($product['paypal_email']) ? $product['paypal_email'] : '',
                                'ConditionID'                               => $this->ConditionDefinitions($product['condition']),
                                'ConditionDisplayName'                      => ucfirst(strtolower($product['condition'])),
                                'ShippingTermsInDescription'                => TRUE,
                                'Quantity'                                  => isset($product['quantity']) ? 1 : 0,
                        );
                        $parameter                                          = $this->ReturnPolicy($parameter, $product);
                        $parameter                                          = $this->ShippingDetails($parameter, $product);
                        $parameter                                          = $this->PrimaryCategory($parameter, $product);
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "ReviseItem";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("ReviseItem", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function Bulk($id = '', $products = array(), $method = 'VerifyAddFixedPriceItem', $type = '', $userName = '', $parameter = array()) {
                        if ( !isset($id) || empty($products) || empty($userName) || empty($type) ) :
                                return FALSE;
                        endif;
                        
                        $fix                                                = 1;
                        if ( !empty($type) && $type == 'product' ) :
                                foreach ( $products['product'] as $product_id => $product ) :
                                        $parameter['BulkDataExchange'][$method]
                                        ['_Repeat'][$fix]                           = ( $type == 'product' ) ? $this->FixedItemType($product) : '';
                                        $fix++ ;
//                                        if($fix > 500) { break; } 
                                endforeach;
                        elseif ( !empty($type) && $type == 'offer' ) :
                                $parameter['BulkDataExchange'][$method]
                                        ['_Repeat'][$fix]                           = $this->OfferFixedItemType($products['product']);
                                        $fix++ ;
                        else :
                                return FALSE;
                        endif;
                        $parameter['Version']                               = $this->version;
                        $parameter['SiteID']                                = $id;

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "BulkDataExchange";
                                $this->core->_session['token']              = $this->token;
                                $this->core->call("BulkDataExchange", "BulkMessage", $parameter);
                                $this->dir_name                             = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                                if(!is_dir($this->dir_name)) :
                                        $old = umask(0); 
                                        mkdir($this->dir_name, 0777, true);
                                        umask($old); 
                                endif;
                                
                                $this->core->_dom->save($this->dir_name."/".$method.".xml");
                                $gzfile                                     = $this->dir_name."/".$method.".xml.gz";
                                $fp                                         = gzopen ($gzfile, "w9");
                                gzwrite ($fp, file_get_contents($this->dir_name."/".$method.".xml"));
                                gzclose($fp);
                                
                                if ( file_exists($this->dir_name."/AddFixedPriceItem.xml") ) unlink($this->dir_name."/AddFixedPriceItem.xml");
                                if ( file_exists($this->dir_name."/RelistFixedPriceItem.xml") ) unlink($this->dir_name."/RelistFixedPriceItem.xml");
                                if ( file_exists($this->dir_name."/ReviseFixedPriceItem.xml") ) unlink($this->dir_name."/ReviseFixedPriceItem.xml");
                                if ( file_exists($this->dir_name."/VerifyAddFixedPriceItem.xml") ) unlink($this->dir_name."/VerifyAddFixedPriceItem.xml");
                                if ( file_exists($this->dir_name."/ReviseInventoryStatus.xml") ) unlink($this->dir_name."/ReviseInventoryStatus.xml");
                                return "Success";
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function BulkEnd($id = '', $products = array(), $method = 'EndFixedPriceItem', $type = '', $userName = '', $parameter = array()) {
                        if ( !isset($id) || empty($products) ) :
                                return FALSE;
                        endif;

                        $fix                                                = 1;
                        
                        foreach ( $products as $product ) :
                                $sku                                        = !empty($product['sku']) ? $product['sku'] : '';
                                $parameter['BulkDataExchange'][$method]
                                ['_Repeat'][$fix]                           = array(
//                                        'SKU'                               => $product['sku'],
                                        'ItemID'                            => $product['id_item'],
                                        'EndingReason'                      => 'Incorrect',
                                        'ErrorLanguage'                     => 'en_US',
                                        'Version'                           => '583',
                                        'MessageID'                         => !empty($product['name']) ? $product['name']."-#-#-".$product['id_product']."-#-#-".$product['batch_id']."-#-#-".$sku : '',
                                );
                                $fix++ ;
                        endforeach;
                        $parameter['Version']                               = 583;
                        $parameter['SiteID']                                = $id;
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "BulkDataExchange";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['compatability']      = 583;
                                $this->core->call("BulkDataExchange", "BulkMessage", $parameter);
                                $this->dir_name                             = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                                if(!is_dir($this->dir_name)) :
                                        $old = umask(0); 
                                        mkdir($this->dir_name, 0777, true);
                                        umask($old); 
                                endif;
                                
                                $this->core->_dom->save($this->dir_name."/".$method.".xml");
                                $gzfile                                     = $this->dir_name."/".$method.".xml.gz";
                                $fp                                         = gzopen ($gzfile, "w9");
                                gzwrite ($fp, file_get_contents($this->dir_name."/".$method.".xml"));
                                gzclose($fp);
                                
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml") ) unlink($this->dir_name."/EndFixedPriceItem.xml");
                                return "Success";
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function SetShipmentTrackingInfo($id = '', $shipments = array(), $method = 'SetShipmentTrackingInfo', $type = 'order', $userName = '', $parameter = array()) {
                        if ( !isset($id) || empty($shipments) ) :
                                return FALSE;
                        endif;

                        $fix                                                = 1;
                        foreach ( $shipments as $shipment ) :
                                $parameter['BulkDataExchange'][$method]
                                ['_Repeat'][$fix]                           = array(
                                        'IsPaid'                            => 1,
                                        'IsShipped'                         => 1,
                                        'OrderID'                           => $shipment['OrderID'],
                                        'OrderLineItemID'                   => isset($shipment['OrderLineItemID']) ? $shipment['OrderLineItemID'] : '',
                                        'ShipmentTrackingNumber'            => $shipment['ShipmentTrackingNumber'],
                                        'ShippedTime'                       => $shipment['ShippedTime'],
                                        'ShippingCarrierUsed'               => $shipment['ShippingCarrierUsed'],
                                );
                                $fix++ ;
                        endforeach;
                        $parameter['Version']                               = 583;
                        $parameter['SiteID']                                = $id;
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "BulkDataExchange";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['compatability']      = 583;
                                $this->core->call("BulkDataExchange", "BulkMessage", $parameter);
                                $this->dir_name                             = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                                if(!is_dir($this->dir_name)) :
                                        $old = umask(0); 
                                        mkdir($this->dir_name, 0777, true);
                                        umask($old); 
                                endif;
                                
                                $this->core->_dom->save($this->dir_name."/".$method.".xml");
                                $gzfile                                     = $this->dir_name."/".$method.".xml.gz";
                                $fp                                         = gzopen ($gzfile, "w9");
                                gzwrite ($fp, file_get_contents($this->dir_name."/".$method.".xml"));
                                gzclose($fp);
                                
                                if ( file_exists($this->dir_name."/".$method.".xml") ) unlink($this->dir_name."/".$method.".xml");
                                return "Success";
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function abortJob($id = '', $jobID = '', $parameter = array()) {
                        if ( !isset($id) || empty($jobID) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'jobId'                                     => $jobID,
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "abortJob";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['BulkendPoint'];
                                $this->core->call("abortJob", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getJobs($id = '', $jobStatus = '', $parameter = array()) {
                        if ( !isset($id) || empty($jobStatus) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'jobStatus'                                 => $jobStatus,
                        );

                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "getJobs";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['BulkendPoint'];
                                $this->core->call("getJobs", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function createUploadJob($id = '', $uploadJobType = 'VerifyAddFixedPriceItem') {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'uploadJobType'                             => $uploadJobType,
                                'UUID'                                      => uniqid('', true),
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "createUploadJob";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['BulkendPoint'];
                                $this->core->call("createUploadJob", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function createUploadFile($id = '', $uploadJobType = 'VerifyAddFixedPriceItem', $type = '', $userName = '', $parameter = array()) {
                        if ( !isset($id) || empty($userName) || empty($type) ) :
                                return FALSE;
                        endif;
                        
                        $this->dir_name                                     = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                        if ( file_exists($this->dir_name."/".$uploadJobType.".xml.gz") ) :
                                $file                                       = $this->dir_name."/".$uploadJobType.".xml.gz";
                                $handle                                     = fopen($file, 'r');
                                $fileData                                   = fread( $handle, filesize($file) );
                                fclose($handle);
                                $fileSize                                   = strlen($fileData);
                        else :
                                return FALSE;
                        endif;
                    
                        $parameter                                          = array(
                                'taskReferenceId'                           => $this->_jobID,
                                'fileReferenceId'                           => $this->_fileID,
                                'fileFormat'                                => 'gzip',
                                'fileAttachment'                            => '<Size>'.$fileSize.'</Size><Data><xop:Include xmlns:xop="http://www.w3.org/2004/08/xop/include" href="cid:urn:uuid:4A51606E0C0D1" /></Data>',
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "uploadFile";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['FileEndPoint'];
                                $this->core->call("uploadFile", "buildMessages", $parameter);
                                $requestPart                                = $this->build($this->core->_xmlBody, $fileData);
                                if ( !empty($requestPart) ) :
                                        $obj                                = $this->core->_lastObject->sendHttpRequest($requestPart);
                                        if ( !empty($obj) ) :
                                                $this->core->_parser        = new SimpleXMLElement($obj);
                                        else :
                                                $this->core->_parser        = '';
                                        endif;
                                else :
                                        $this->core->_parser                = '';
                                endif;
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function startDownloadFile($id = '', $downloadJobType = 'VerifyAddFixedPriceItem', $type = '', $userName = '', $parameter = array()) {
                        if ( !isset($id) || empty($userName) || empty($type) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'taskReferenceId'                           => $this->_jobID,
                                'fileReferenceId'                           => $this->_fileID,
                        );
                        
                        $this->dir_name                                     = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "downloadFile";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['FileEndPoint'];
                                $this->core->_parser                        = '';
                                $this->core->call("downloadFile", "buildMessages", $parameter);
                                if ( !empty(core::$_element['downloadFile']) ) :
                                        $parse                                      = $this->parseForResponseXML(core::$_element['downloadFile']);
                                        $this->core->_parser                        = new SimpleXMLElement($parse);
                                        $dom                                        = new DomDocument();
                                        $dom->preserveWhitespace                    = false;
                                        $dom->loadXML($parse);
                                        $dom->formatOutput                          = true;
                                        if($uuid = $this->parseForXopIncludeUUID($dom)) :
                                                $fileBytes                                  = $this->parseForFileBytes($uuid, core::$_element['downloadFile']);
                                                if ( file_exists($this->dir_name."/".$downloadJobType.".zip") ) unlink($this->dir_name."/".$downloadJobType.".zip");
                                                file_put_contents($this->dir_name."/".$downloadJobType.".zip", '');
                                                if ( file_exists($this->dir_name."/".$downloadJobType.".zip") ) :
                                                        $handler                                    = fopen($this->dir_name."/".$downloadJobType.".zip", 'wb') or die("Failed. Cannot Open AddFixedPriceItem to Write!</b></p>");
                                                        fwrite($handler, $fileBytes);
                                                        fclose($handler);
                                                endif;
                                        endif;
                                else :
                                        $this->core->_parser                = '';
                                endif;
                        endif;
                }
                
                function getDownloadFile($id = '', $downloadJobType = 'VerifyAddFixedPriceItem', $type = '', $userName = '') {
                        if ( empty($userName) || empty($type) ) :
                                return FALSE;
                        endif;
                        
                        $this->dir_name                                     = USERDATA_PATH.$userName."/ebay/".$type."/".$id;
                        $zip                                                = zip_open($this->dir_name."/".$downloadJobType.".zip");
                        if ($zip && $zip != 11) :
                                while ($zip_entry = zip_read($zip)) :
                                        if (zip_entry_open($zip, $zip_entry, "r")) :
                                                $buf                        = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                                                zip_entry_close($zip_entry);
                                        endif;
                                endwhile;
                                zip_close($zip);
                        endif;
                        if ( !empty($buf) ) :
                                $this->core->_parser                                = new SimpleXMLElement($buf);
                        endif;
                }
                
                function parseForFileBytes($uuid, $response) {
                        $contentId                                          = 'Content-ID: <' . $uuid . '>';
                        $mimeBoundaryPart                                   = strpos($response,'--MIMEBoundaryurn_uuid_');
                        $beginFile                                          = strpos($response, $contentId, $mimeBoundaryPart);
                        $beginFile                                          += strlen($contentId);
                        $beginFile                                          += 4;
                        $endFile                                            = strpos($response,'--MIMEBoundaryurn_uuid_',$beginFile);
                        $endFile                                            -= 2;
                        $fileBytes                                          = substr($response, $beginFile, $endFile - $beginFile);
                        return $fileBytes;
                }
                
                function parseForXopIncludeUUID($responseDOM) {
                        $xopInclude                                         = $responseDOM->getElementsByTagName('Include')->item(0);
                        $uuid = null;
                        if($xopInclude){
	                        $uuid                                               = $xopInclude->getAttributeNode('href')->nodeValue;
	                        $uuid                                               = substr($uuid, strpos($uuid,'urn:uuid:'));
                        }
                        return $uuid;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parseForResponseXML($response) {
                        $beginResponseXML                                   = strpos($response, '<?xml');
                        $endResponseXML                                     = strpos($response, '</downloadFileResponse>', $beginResponseXML);
                        $endResponseXML                                     += strlen('</downloadFileResponse>');
                        return substr($response, $beginResponseXML,
                                $endResponseXML - $beginResponseXML);
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function startUploadJob($id = '', $parameter = array()) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'jobId'                                     => $this->_jobID,
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "startUploadJob";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['BulkendPoint'];
                                $this->core->call("startUploadJob", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                function getJobStatus($id = '', $parameter = array()) {
                        if ( !isset($id) ) :
                                return FALSE;
                        endif;
                        
                        $parameter                                          = array(
                                'jobId'                                     => $this->_jobID,
                        );
                        
                        if ( !empty($parameter) ) :
                                $this->core->_session['siteID']             = $id;
                                $this->core->_session['method']             = "getJobStatus";
                                $this->core->_session['token']              = $this->token;
                                $this->core->_session['endPoint']           = $this->core->_session['BulkendPoint'];
                                $this->core->call("getJobStatus", "buildMessages", $parameter);
                                $this->core->parser();
                        endif;
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function build($request, $file) {
                        $requestPart   = '';
                        $requestPart  .= "--MIME_boundary" . "\r\n";
                        $requestPart  .= 'Content-Type: application/xop+xml; charset=UTF-8; type="text/xml; charset=UTF-8"' . "\r\n";
                        $requestPart  .= 'Content-Transfer-Encoding: binary' . "\r\n";
                        $requestPart  .= 'Content-ID: <0.urn:uuid:4A515E9048570>' . "\r\n" . "\r\n";
                        $requestPart  .= $request . "\r\n";

                        $binaryPart = '';
                        $binaryPart .= "--MIME_boundary" . "\r\n";
                        $binaryPart .= 'Content-Type: application/octet-stream' . "\r\n";
                        $binaryPart .= 'Content-Transfer-Encoding: binary' . "\r\n";
                        $binaryPart .= 'Content-ID: <urn:uuid:4A51606E0C0D1>' . "\r\n" . "\r\n";
                        $binaryPart .= $file . "\r\n";
                        $binaryPart .= "--MIME_boundary" . "--";

                       return $requestPart . $binaryPart;
                }
        }