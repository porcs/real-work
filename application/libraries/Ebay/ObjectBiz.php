<?php   if ( class_exists('FeedBiz')      == FALSE ) require dirname(__FILE__).'/../FeedBiz.php';
        if ( class_exists('ImportLog')    == FALSE ) require_once(dirname(__FILE__) . '/../ImportLog.php');

        class ObjectBiz extends FeedBiz {
                private $_path;
                private $_user;
                private $_connector;
                private $dir_server;
                private $_databasename;
                private $_databaseuser;
                private $_engine = " ENGINE=InnoDB DEFAULT CHARSET=utf8";
                
                public static $sql = array(
                        'ebay_store_category'                       => "CREATE TABLE `ebay_store_category` (`id_category` bigint(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `ebay_user` text, `name_category` text, `parent` int(11) DEFAULT NULL, `order_item` int(11) DEFAULT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_category'                     => "CREATE TABLE `ebay_mapping_category` (`id_category` bigint(11) NOT NULL DEFAULT '0', `id_ebay_category` bigint(11) DEFAULT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_category` text, `name_ebay_category` text, `status` int(11) NOT NULL DEFAULT '1',  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_secondary_category'           => "CREATE TABLE `ebay_mapping_secondary_category` (`id_category` bigint(11) NOT NULL DEFAULT '0', `id_ebay_category` bigint(11) DEFAULT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_category` text, `name_ebay_category` text, `status` int(11) NOT NULL DEFAULT '1',  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_store_category'               => "CREATE TABLE `ebay_mapping_store_category` (`id_category` bigint(11) NOT NULL DEFAULT '0', `id_ebay_category` bigint(11) DEFAULT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_category` text, `name_ebay_category` text, `status` int(11) NOT NULL DEFAULT '1',  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_secondary_store_category'     => "CREATE TABLE `ebay_mapping_secondary_store_category` (`id_category` bigint(11) NOT NULL DEFAULT '0', `id_ebay_category` bigint(11) DEFAULT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_category` text, `name_ebay_category` text, `status` int(11) NOT NULL DEFAULT '1',  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_univers'                      => "CREATE TABLE `ebay_mapping_univers` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_ebay_category` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_ebay_category` text,  PRIMARY KEY (`id_profile`,`id_ebay_category`,`id_site`,`id_shop`))",
                        'ebay_tax'                                  => "CREATE TABLE `ebay_tax` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_tax` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_lang` int(11) DEFAULT NULL, `tax_name` text, `tax_rate` int(11) DEFAULT NULL, `tax_type` text, `decimal_value` int(11) DEFAULT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_tax`,`id_site`,`id_shop`))",
                        'ebay_price_modifier'                       => "CREATE TABLE `ebay_price_modifier` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `price_from` float(11,2) NOT NULL, `price_to` float(11,2) NOT NULL, `price_value` float(11,2) NOT NULL, `price_type` varchar(20) DEFAULT NULL, `decimal_value` int(11) DEFAULT NULL, `rel` int(11) NOT NULL DEFAULT '0', `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`,`rel`))",
                        'ebay_mapping_templates'                    => "CREATE TABLE `ebay_mapping_templates` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_category` int(11) NOT NULL DEFAULT '0', `template_selected` text, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`id_category`,`id_site`,`id_shop`))",
                        'ebay_mapping_carrier_rules'                => "CREATE TABLE `ebay_mapping_carrier_rules` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_rule` int(11) NOT NULL DEFAULT '0', `id_shipping` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL, `is_international` int(11) DEFAULT NULL, `country_service` varchar(100) NOT NULL DEFAULT '', `shipping_cost` float(11,2) NOT NULL, `shipping_additionals` float(11,2) DEFAULT NULL, `min_weight` int(11) DEFAULT NULL, `max_weight` int(11) DEFAULT NULL, `min_price` int(11) DEFAULT NULL, `max_price` int(11) DEFAULT NULL, `surcharge` int(11) DEFAULT NULL, `rel` int(11) NOT NULL DEFAULT '0', `operations` varchar(100) NOT NULL DEFAULT '', `is_enabled` enum('0','1') NOT NULL DEFAULT '0', `id_carrier_ref` int(11) DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_rule`,`id_ebay_shipping`,`id_shop`,`id_site`,`id_shipping`,`country_service`,`operations`,`rel`))",
                        'ebay_mapping_carrier_domestic'             => "CREATE TABLE `ebay_mapping_carrier_domestic` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_shipping` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_shipping` text, `name_ebay_shipping_service` text, `name_ebay_shipping` text, `id_rule` int(11) NOT NULL, `postcode` varchar(20) DEFAULT '0', `dispatch_time` int(11) DEFAULT '3', `id_carrier_ref` int(11) DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_shipping`,`id_ebay_shipping`,`id_site`,`id_shop`))",
                        'ebay_mapping_carrier_international'        => "CREATE TABLE `ebay_mapping_carrier_international` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_shipping` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_shipping` text, `name_ebay_shipping_service` text, `name_ebay_shipping` text, `id_rule` int(11) NOT NULL, `country_service` varchar(100) NOT NULL DEFAULT '', `postcode` varchar(20) DEFAULT '0', `dispatch_time` int(11) DEFAULT '3', `id_carrier_ref` int(11) DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_shipping`,`id_ebay_shipping`,`id_site`,`id_shop`,`country_service`))",
                        'ebay_mapping_carrier_country_selected'     => "CREATE TABLE `ebay_mapping_carrier_country_selected` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `country_service` varchar(100) NOT NULL DEFAULT '', `name_ebay_country` text,  PRIMARY KEY (`id_profile`,`id_ebay_shipping`,`id_site`,`id_shop`,`country_service`))",
                        'ebay_mapping_carrier_default'              => "CREATE TABLE `ebay_mapping_carrier_default` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `name_ebay_shipping_service` text, `name_ebay_shipping` text, `id_rule` int(11) NOT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1', `postcode` varchar(20) DEFAULT '0', `dispatch_time` int(11) DEFAULT '3',  PRIMARY KEY (`id_profile`,`id_ebay_shipping`,`id_site`,`id_shop`))",
                        'ebay_mapping_carrier_return_default'       => "CREATE TABLE `ebay_mapping_carrier_return_default` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_shipping` int(11) NOT NULL DEFAULT '0', `name_shipping` text, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_profile`,`id_shipping`,`id_site`,`id_shop`))",
                        'ebay_product_item_id'                      => "CREATE TABLE `ebay_product_item_id` (`id_product` int(11) NOT NULL, `id_item` varchar(30) NOT NULL DEFAULT '', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `SKU` varchar(100) NOT NULL DEFAULT '', `ebay_user` varchar(50) NOT NULL DEFAULT '', `name_product` text, `date_add` datetime DEFAULT NULL, `date_end` datetime DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_product`,`id_site`,`id_item`,`id_shop`,`SKU`,`ebay_user`))",
                        'ebay_product_details'                      => "CREATE TABLE `ebay_product_details` (`batch_id` varchar(30) NOT NULL DEFAULT '', `id_product` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `SKU` varchar(100) NOT NULL DEFAULT '', `ebay_user` varchar(30) NOT NULL DEFAULT '', `name_product` text, `description_product` text, `id_primary_category` int(11) DEFAULT NULL, `id_secondary_category` int(11) DEFAULT NULL, `id_store_category` int(11) DEFAULT NULL, `id_secondary_store_category` int(11) DEFAULT NULL, `condition_value` int(11) DEFAULT NULL, `price` float(11,2) NOT NULL DEFAULT '0.00', `combination_count` int(11) DEFAULT NULL, `quantity` int(11) NOT NULL DEFAULT '0', `dispatch_time` int(11) DEFAULT NULL, `postal_code` varchar(20) DEFAULT NULL, `listing_duration` varchar(10) DEFAULT NULL, `price_modifier_rate` float(11,2) DEFAULT NULL, `price_modifier_type` text, `tax_rate` float(11,2) DEFAULT NULL, `currency_rate_from` float(11,2) DEFAULT NULL, `currency_rate_to` float(11,2) DEFAULT NULL, `date_add` datetime DEFAULT NULL, `date_end` datetime DEFAULT NULL, `id_item` text, `export_pass` enum('0','1') NOT NULL DEFAULT '0', `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`batch_id`,`id_product`,`id_site`,`id_shop`,`SKU`,`ebay_user`))",
                        'ebay_statistics'                           => "CREATE TABLE `ebay_statistics` (`batch_id` varchar(30) NOT NULL DEFAULT '', `id_product` int(11) NOT NULL DEFAULT '0', `id_combination` int(11) NOT NULL DEFAULT '0', `sku` text, `sku_export` text, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `combination` int(11) DEFAULT NULL, `ebay_user` varchar(30) NOT NULL DEFAULT '', `name_product` text NOT NULL, `response` text, `type` text, `message` text, `code` varchar(10) NOT NULL DEFAULT '', `resolved` text NOT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`batch_id`,`id_product`,`id_combination`,`id_site`,`id_shop`,`ebay_user`,`code`))",
                        'ebay_statistics_combination'               => "CREATE TABLE `ebay_statistics_combination` (`batch_id` varchar(30) NOT NULL DEFAULT '', `id_product` int(11) NOT NULL DEFAULT '0', `sku` text, `sku_export` text, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_combination` int(11) NOT NULL DEFAULT '0', `response` text, `type` text, `message` text, `code` varchar(10) NOT NULL DEFAULT '', `resolved` text NOT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`batch_id`,`id_product`,`id_site`,`id_shop`,`id_combination`,`code`))",
                        'ebay_statistics_log'                       => "CREATE TABLE `ebay_statistics_log` (`batch_id` varchar(30) NOT NULL DEFAULT '', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `type` text, `method` text, `total` int(11) DEFAULT NULL, `success` int(11) DEFAULT NULL, `warning` int(11) DEFAULT NULL, `error` int(11) DEFAULT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`batch_id`,`id_site`,`id_shop`))",
                        'ebay_job_task'                             => "CREATE TABLE `ebay_job_task` (`jobId` bigint(11) NOT NULL, `fileReferenceId` text, `batch_id` text, `ebay_user` text NOT NULL, `jobType` varchar(30) NOT NULL DEFAULT '', `jobStatus` text, `percentComplete` text, `creationTime` datetime DEFAULT NULL, `startTime` datetime DEFAULT NULL,  PRIMARY KEY (`jobId`,`jobType`))",
                        'ebay_api_logs'                             => "CREATE TABLE `ebay_api_logs` (`id_log` int(11) NOT NULL AUTO_INCREMENT, `batch_id` text NOT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `ebay_user` text NOT NULL, `error_id` int(11) DEFAULT NULL, `error_message` text, `severity` text, `type` text, `api_call_type` text, `time` datetime DEFAULT NULL,  PRIMARY KEY (`id_log`))",
                        'ebay_mapping_attribute_selected'           => "CREATE TABLE `ebay_mapping_attribute_selected` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_attribute_group` int(11) NOT NULL, `name_attribute` text NOT NULL, `name_attribute_ebay` text NOT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_lang` int(11) NOT NULL DEFAULT '0', `mode` int(11) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_profile`,`id_attribute_group`,`id_site`,`id_shop`,`id_lang`))",
                        'ebay_mapping_attribute_value'              => "CREATE TABLE `ebay_mapping_attribute_value` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_attribute_group` int(11) NOT NULL, `id_attribute` int(11) NOT NULL, `name_attribute` text NOT NULL, `name_attribute_ebay` text NOT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_lang` int(11) NOT NULL DEFAULT '0', `mode` int(11) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_profile`,`id_attribute_group`,`id_attribute`,`id_site`,`id_shop`,`id_lang`))",
                        'ebay_mapping_specific_selected'            => "CREATE TABLE `ebay_mapping_specific_selected` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_feature` int(11) NOT NULL, `name_feature` text NOT NULL, `name_feature_ebay` text NOT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_lang` int(11) NOT NULL DEFAULT '0', `mode` int(11) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_profile`,`id_feature`,`id_site`,`id_shop`,`id_lang`))",
                        'ebay_mapping_specific_value'               => "CREATE TABLE `ebay_mapping_specific_value` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_feature` int(11) NOT NULL, `id_feature_value` int(11) NOT NULL, `name_feature` text NOT NULL, `name_feature_ebay` text NOT NULL, `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_lang` int(11) NOT NULL DEFAULT '0', `mode` int(11) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1',  PRIMARY KEY (`id_profile`,`id_feature`,`id_feature_value`,`id_site`,`id_shop`,`id_lang`))",
                        'ebay_mapping_condition'                    => "CREATE TABLE `ebay_mapping_condition` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL, `condition_data` text NOT NULL, `condition_value` varchar(30) NOT NULL DEFAULT '', `condition_name` text NOT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`,`condition_value`))",
                        'ebay_mapping_condition_description'        => "CREATE TABLE `ebay_mapping_condition_description` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL, `condition_data` varchar(30) NOT NULL DEFAULT '', `condition_description` text,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`,`condition_data`))",
                        'ebay_profiles_group'                       => "CREATE TABLE `ebay_profiles_group` (`id_profile` int(11) NOT NULL DEFAULT '0', `type` text,  PRIMARY KEY (`id_profile`))",
                        'ebay_profiles'                             => "CREATE TABLE `ebay_profiles` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `profile_name` text, `ebay_user` text, `completed` int(11) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '1', `date_add` datetime DEFAULT NULL, `date_edit` datetime DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_shop`,`id_site`))",
                        'ebay_profiles_details'                     => "CREATE TABLE `ebay_profiles_details` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_category` int(11) NOT NULL DEFAULT '0', `category_name` text, `id_ebay_category` int(11) NOT NULL, `ebay_category_name` text NOT NULL, `id_secondary_category` int(11) DEFAULT NULL, `secondary_category_name` text, `id_store_category` int(11) DEFAULT NULL, `store_category_name` text, `id_secondary_store_category` int(11) DEFAULT NULL, `secondary_store_category_name` text, `id_product` int(11) DEFAULT NULL, `out_of_stock_min` int(11) DEFAULT NULL, `out_of_stock_created` int(11) DEFAULT NULL, `maximum_quantity` int(11) DEFAULT NULL, `track_inventory` varchar(10) DEFAULT NULL, `title_format` int(11) DEFAULT NULL, `visitor_counter` varchar(20) DEFAULT NULL, `gallery_plus` enum('0','1') NOT NULL DEFAULT '0', `auto_pay` enum('0','1') NOT NULL DEFAULT '0', `country_name` varchar(10) DEFAULT NULL, `currency` varchar(10) DEFAULT NULL, `listing_duration` varchar(10) DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`))",
                        'ebay_profiles_payment'                     => "CREATE TABLE `ebay_profiles_payment` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `payment_method` text, `payment_instructions` text,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`))",
                        'ebay_profiles_return'                      => "CREATE TABLE `ebay_profiles_return` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `returns_policy` text, `returns_within` text, `returns_pays` varchar(30) DEFAULT NULL, `returns_information` text, `holiday_return` tinyint(4) DEFAULT NULL,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`))",
                        'ebay_profiles_description'                 => "CREATE TABLE `ebay_profiles_description` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `description_selected` tinyint(4) DEFAULT NULL, `html_description` text,  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`))",
                        'ebay_profiles_condition'                   => "CREATE TABLE `ebay_profiles_condition` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `condition_value` text NOT NULL, `condition_name` text NOT NULL, `condition_description` text, `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`))",
                        'ebay_profiles_shipping'                    => "CREATE TABLE `ebay_profiles_shipping` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_ebay_shipping` int(11) NOT NULL DEFAULT '0', `name_ebay_shipping_service` varchar(50) NOT NULL DEFAULT '', `name_ebay_shipping` text, `country_service` varchar(50) NOT NULL DEFAULT '', `name_ebay_country` text, `shipping_cost` float(11,2) NOT NULL, `shipping_additionals` float(11,2) DEFAULT NULL, `postcode` varchar(20) DEFAULT '0', `dispatch_time` int(11) DEFAULT '3', `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`,`id_ebay_shipping`,`country_service`,`name_ebay_shipping_service`))",
                        'ebay_profiles_variation'                   => "CREATE TABLE `ebay_profiles_variation` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_group` int(11) NOT NULL DEFAULT '0', `name_attribute` varchar(100) NOT NULL DEFAULT '', `value_attribute` varchar(50) NOT NULL DEFAULT '', `mode` tinyint(4) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id_profile`,`id_group`,`name_attribute`,`value_attribute`,`id_site`,`id_shop`))",
                        'ebay_profiles_specific'                    => "CREATE TABLE `ebay_profiles_specific` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_group` int(11) NOT NULL DEFAULT '0', `name_attribute` varchar(100) NOT NULL DEFAULT '', `value_attribute` varchar(50) NOT NULL DEFAULT '', `mode` tinyint(4) DEFAULT NULL, `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id_profile`,`id_site`,`id_shop`,`id_group`,`name_attribute`,`value_attribute`))",
                        'ebay_profiles_mapping'                     => "CREATE TABLE `ebay_profiles_mapping` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id` int(11) NOT NULL DEFAULT '0', `selected_name` varchar(30) NOT NULL DEFAULT '', `type` varchar(30) NOT NULL DEFAULT '', `is_enabled` enum('0','1') NOT NULL DEFAULT '0',  PRIMARY KEY (`id`,`id_site`,`id_shop`,`selected_name`,`type`))",
                        'ebay_attribute_univers'                    => "CREATE TABLE `ebay_attribute_univers` (`id_profile` int(11) NOT NULL DEFAULT '0', `id_site` int(11) NOT NULL DEFAULT '0', `id_shop` int(11) NOT NULL DEFAULT '0', `id_group` int(11) NOT NULL DEFAULT '0', `name` text,  PRIMARY KEY (`id_group`,`id_site`,`id_shop`))",
                        'ebay_synchronization_store'                => "CREATE TABLE `ebay_synchronization_store` (`id_item` varchar(30) NOT NULL DEFAULT '', `id_site` int(11) NOT NULL DEFAULT '0', `SKU` text NOT NULL, `ebay_user` varchar(30) NOT NULL DEFAULT '', `name_product` text, `quantity` int(11) NOT NULL DEFAULT '0', `date_add` datetime DEFAULT NULL, `date_end` datetime DEFAULT NULL,  PRIMARY KEY (`id_site`,`id_item`,`ebay_user`))",
                        'ebay_synchronization_attribute'            => "CREATE TABLE `ebay_synchronization_attribute` (`id_item` varchar(30) NOT NULL DEFAULT '', `id_site` int(11) NOT NULL DEFAULT '0', `SKU` varchar(100) NOT NULL DEFAULT '', `start_price` int(11) NOT NULL DEFAULT '0', `quantity` int(11) NOT NULL DEFAULT '0', `variation_name` varchar(100) NOT NULL DEFAULT '', `variation_value` varchar(100) NOT NULL DEFAULT '', `ebay_user` varchar(30) NOT NULL DEFAULT '', `name_product` text, `date_add` datetime DEFAULT NULL, `date_end` datetime DEFAULT NULL,  PRIMARY KEY (`id_site`,`id_item`,`SKU`,`variation_name`,`variation_value`,`ebay_user`))",
                        'ebay_attribute_override'                   => "CREATE TABLE `ebay_attribute_override` (`sku` text NOT NULL, `id_product` int(11) DEFAULT NULL, `id_product_attribute` int(11) DEFAULT NULL, `id_shop` int(11) DEFAULT NULL, `id_site` int(11) DEFAULT NULL, `attribute_field` text NOT NULL, `override_value` text NOT NULL, `date_upd` datetime DEFAULT NULL)",
                        'ebay_explode_product'                      => "CREATE TABLE `ebay_explode_product` (`id_product` int(11) DEFAULT NULL, `id_shop` int(11) DEFAULT NULL, `id_site` int(11) DEFAULT NULL, `attribute_field` text NOT NULL, `date_add` datetime DEFAULT NULL,  PRIMARY KEY (`id_product`,`id_site`,`id_shop`))",
                );

                
                public function __construct( $user, $database = 'products' ) {
                        parent::__construct( $user );
                        $this->dir_server                           = DIR_SERVER;
                        $this->_user                                = $user[0];
                        $this->_connector                           = new Db($user[0], $database);
                        $this->_databasename                        = $database;
                }
                
                
                public function callPath($user, $database) {
                        if ( empty($user) ) {
                                return FALSE;
                        }
                        $this->_databasename                        = $database;
                        $this->_user                                = $user;
                }

                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
                public function checkDBOnly($field = '') {
                        if ( $this->_connector->db_status && !empty($field) ) {
                                $result                             = $this->_connector->db_table_exists($field, true);
                                
                                if ( !empty($result) ) :
                                        return TRUE;
                                endif;
                        }
                        return false;
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function checkDB($field = '') {
                        if ( $this->_connector->db_status && !empty($field) && isset(self::$sql[$field]) ) {
                                $query_change                       = self::$sql[$field].$this->_engine;
                                $field_change                       = $field."_backup";

                                if ( !empty($query_change) && is_array($query_change) ) {
                                    
                                        $field_change               = $field;
                                        $field                      = $query_change['TABLE_NAME'];
                                        $query_change               = $query_change['SQL'].$this->_engine;
                                }

                                $result                             = $this->_connector->db_table_exists($field, true);

                                if ( !empty($result) && $field_change == $field."_backup" ) {
                                        $result                     = $this->_connector->db_query_string_fetch("SHOW CREATE TABLE ".$field);
                                        if ( !empty($result) ) :
                                                foreach ( $result as $table ) {
                                                        $onlyconsonants     = str_replace(array("\r", "\n"), "", $table["Create Table"]);
                                                        $onlyconsonants     = str_replace("(  `", "(`", $onlyconsonants);
                                                        $onlyconsonants     = str_replace(",  `", ", `", $onlyconsonants);

                                                        if ( isset($query_change) && $query_change == $onlyconsonants ) {
                                                                return TRUE;
                                                        } 

                                                        else {
                                                                $this->backupTable($field, $onlyconsonants, $field_change, $query_change);
                                                                return TRUE;
                                                        }
                                                }
                                        endif;
                                } 
                                elseif ( !empty($result) && stripos($field_change, '_backup') == 0 ) {
                                        $result             = $this->_connector->db_query_string_fetch("SHOW CREATE TABLE ".$field);

                                        foreach ( $result as $table ) {
                                                $onlyconsonants     = str_replace(array("\r", "\n"), "", $table["Create Table"]);
                                                $onlyconsonants     = str_replace("(  `", "(`", $onlyconsonants);
                                                $onlyconsonants     = str_replace(",  `", ", `", $onlyconsonants);

                                                $this->backupTable($field_change, $onlyconsonants, $field, $query_change);
                                                return TRUE;
                                        }
                                } 
                                else {
                                        return $this->createTable($field);
                                }
                        }
                        return FALSE;
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function createTable($string = '') {
                        $_db                                = $this->_connector;
                        if ( isset(self::$sql[$string]) && is_array(self::$sql[$string]) ) : 
                                $result                     = $_db->db_query_result_str(self::$sql[$string]['SQL'].$this->_engine);
                        else :
                                $result                     = $_db->db_query_result_str(self::$sql[$string].$this->_engine);
                        endif;

                        return TRUE;
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function backupTable($field1 = null, $sql1 = null, $field2 = null, $sql2 = null) {
                        if ( empty($field1) || empty($field2) || empty($sql1) || empty($sql2) ) :
                                return FALSE;
                        endif;

                        $insert_field                       = '';
                        $select_field                       = '';
                        $_db                                = $this->_connector;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = str_replace($field1, $field2, $sql1).";";
                                $_db->db_query_result_str($sql);
                                
                        $sql = "INSERT INTO ".$field2." SELECT * FROM ".$field1.";";
                                $_db->db_query_result_str($sql);

                        $sql = "DROP TABLE ".$field1.";";
                                $_db->db_query_result_str($sql);
                                
                        $sql = $sql2.";";
                                $_db->db_query_result_str($sql);
                                
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
                        $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$field1."'";
                        $result1 = $_db->db_query_string_fetch($sql);
                        $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '".$field2."'";
                        $result2 = $_db->db_query_string_fetch($sql);

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($result1) && !empty($result2) && count($result1) < count($result2) ) :
                                $result = $result1;
                        elseif ( !empty($result1) && !empty($result2) && count($result1) >= count($result2) ) :
                                $result = $result2;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($result) ) :
                                foreach ( $result as $field ) :
                                        if ( empty($insert_field) && !empty($field) ) :
                                                $insert_field               .= '('. $field['COLUMN_NAME'];
                                                $select_field               .= $field['COLUMN_NAME'];
                                        else :
                                                $insert_field               .= ', '. $field['COLUMN_NAME'];
                                                $select_field               .= ', '. $field['COLUMN_NAME'];
                                        endif;
                                endforeach;
                                $insert_field                       .= ')';
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "INSERT INTO ".$field1." ".$insert_field." SELECT ".$select_field." FROM ".$field2.";";
                                $_db->db_query_result_str($sql);
                        $sql = "DROP TABLE ".$field2.";";
                                $_db->db_query_result_str($sql);
                        
                        return TRUE;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function sqlQuerySwap($sql = null, $database = null, $current = false, $key = false) {
                        if ( empty($sql) || (!isset($database) && empty($database) ) ) :
                                return Null;
                        endif;
                        $_db                        = $this->_connector;
                        $sql                        = str_replace("::PREFIX::", $database, $sql);
                        $result                     = $_db->db_query_string_fetch($sql);
                        
                        if ( !empty($result) ) {
                                if ( $current == true ) {
                                        return ($key == true) ? current($result[0]) : current($result);
                                }
                                return $result;
                        }
                        return FALSE;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function sqlQueryAffected($sql = null, $database = null, $current = false, $key = false) {
                        if ( empty($sql) || (!isset($database) && empty($database) ) ) :
                                return Null;
                        endif;
                        $_db = $this->_connector;
                        $_db->db_query_result_str($sql);
                        $affected = $_db->db_changes();

                        if ( !empty($affected) ) {
                                return TRUE;
                        }
                        return FALSE;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function checkCount($table = '', $where = '') {
                        if ( empty($table) ) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        if ( !empty($where)) {
                            
                                foreach ( $where as $key => $field) {
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                }
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "SELECT "
                                . "COUNT(*) "
                                . "FROM ".$table." ".$column;

                        $result = $this->sqlQuerySwap($sql, 'products', true, true);

                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        return 0;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function checkOrderCount($table = '', $where = '') {
                        if ( empty($table) ) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        if ( !empty($where)) {
                                foreach ( $where as $key => $field) {
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                }
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "SELECT "
                                . "COUNT(*) "
                                . "FROM ".$table." ".$column;
                        
                        $result = $this->sqlQuerySwap($sql, 'orders', true, true);

                        if ( !empty($result) ) :
                                return $result;
                        endif;
                        return 0;
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function deleteColumn($table = '', $where = '') {
                        if ( empty($table) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        if ( !empty($where)) :
                                foreach ( $where as $key => $field) :
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "DELETE "
                                . "FROM ".$table." ".$column;
                        
                        return $this->sqlQueryAffected($sql, 'products');
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function deleteColumnOnly($table = '', $where = '') {
                        if ( empty($table) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        if ( !empty($where)) :
                                foreach ( $where as $key => $field) :
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "DELETE "
                                . "FROM ".$table." ".$column;
                        
                        if ( !empty($sql) ) :
                                return $sql.";";
                        endif;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function deleteOrderTable($table = '', $where = '') {
                        if ( empty($table) ) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        if ( !empty($where)) {
                                foreach ( $where as $key => $field) {
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                }
                        }
                        $sql = "DELETE "
                                . "FROM ".$table." ".$column;
                        
                        return $this->sqlQueryAffected($sql, 'orders');
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function updateColumn($table = '', $data = '', $where) {
                        if ( empty($table) || empty($data) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        $set = '';
                        if ( !empty($data)) :
                                foreach ( $data as $key => $field) :
                                        $set                .= empty($set) ? 'SET ' : ' , ';  
                                        $set                .= $key;  
                                        $set                .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($where)) :
                                foreach ( $where as $key => $field) :
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "UPDATE "
                                .$table." ".$set." ".$column; 

                        return $this->sqlQueryAffected($sql, 'products');
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function updateColumnOnly($table = '', $data = '', $where) {
                        if ( empty($table) || empty($data) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = '';
                        $set = '';
                        if ( !empty($data)) :
                                foreach ( $data as $key => $field) :
                                        $set                .= empty($set) ? 'SET ' : ' , ';  
                                        $set                .= $key;  
                                        $set                .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($where)) :
                                foreach ( $where as $key => $field) :
                                        $column             .= empty($column) ? 'where ' : ' AND ';  
                                        $column             .= $key;  
                                        $column             .= ' = "'.$field.'"';  
                                endforeach;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $sql = "UPDATE "
                                .$table." ".$set." ".$column; 
                        
                        if ( !empty($sql) ) :
                                return $sql.";";
                        endif;
                }
                

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function mappingTable($table = '', $resultArray = array()) {
                        if ( empty($table) || empty($resultArray) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = ''; 
                        $value  = '';
                        if ( !empty($resultArray) ) :
                                foreach ( $resultArray as $key => $field) :
                                        $column             .= empty($column) ? '(' : ',';  
                                        $value              .= empty($value)  ? '(' : ',';  
                                        $column             .= $key;  
                                        $value              .= ' "'.$field.'"';  
                                endforeach;

                                $column                     .= ')';  
                                $value                      .= ')';  
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($column) && !empty($value) ) :
                                $sql                        = "REPLACE INTO ".$table." ".$column." VALUES ".$value; 
                                return $this->sqlQueryAffected($sql, 'products');
                        endif;
                        
                        return FALSE;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function mappingTableOnly($table = '', $resultArray = array()) {
                        if ( empty($table) || empty($resultArray) ) :
                                return FALSE;
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column                             = ''; 
                        $value                              = '';
                        if ( !empty($resultArray) ) :
                                foreach ( $resultArray as $key => $field) :
                                        $column             .= empty($column) ? '(' : ',';  
                                        $value              .= empty($value)  ? '(' : ',';  
                                        $column             .= $key;  
                                        $value              .= ' "'.$this->_connector->escape_str($field).'" ';  
                                endforeach;
                                $column                     .= ')';  
                                $value                      .= ')';  
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($column) && !empty($value) ) :
                                $sql                        = "REPLACE INTO ".$table." ".$column." VALUES ".$value; 
                                return $sql.";";
                        endif;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function OrderTable($table = '', $resultArray = array(), $where = '', $having = false) {
                        if ( empty($table) || empty($resultArray) ) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $column = ''; 
                        $value = '';
                        $set = '';
                        $where_column = '';
                        
                        if ( !empty($resultArray) ) :
                                foreach ( $resultArray as $key => $field) {
                                        if ( $having ) {
                                                if (!empty($set)) {
                                                        $set        = $set.", ";  
                                                }
                                                $set                = $set.$key." = '".$field."'";  
                                        }
                                        
                                        else {
                                                $column             .= empty($column) ? '(' : ',';  
                                                $value              .= empty($value)  ? '(' : ',';  
                                                $column             .= $key;  
                                                $value              .= ' "'.$field.'"';  
                                        }
                                }
                                if ( !empty($where)) {
                                        foreach ( $where as $key => $field) {
                                                $where_column       .= empty($where_column) ? 'where ' : ' AND ';  
                                                $where_column       .= $key;  
                                                $where_column       .= ' = "'.$field.'"';  
                                        }
                                }
                                if ( $having ) {
                                        $sql                        = "UPDATE ".$table." SET ".$set." ".$where_column; 
                                }
                                else {
                                        $column                     .= ')';  
                                        $value                      .= ')';  
                                        $sql                        = "REPLACE INTO ".$table." ".$column." VALUES ".$value; 
                                }
                        endif;
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( $this->_connector->db_status && !empty($sql) ) {
                        	return $this->sqlQueryAffected($sql, 'orders');
                        }

                        return FALSE;
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function selectQuery($sql = '') {
                        if ( empty($sql)) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $_db = $this->_connector;
                        if ( $_db->db_status && !empty($sql) ) {
                                $result = $_db->db_query_string_fetch($sql);
                                return $result;
                        }
                }
      
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function transaction($query = null, $direct_query = false, $asyn = false) {
                    if ( empty($query) ) :
                            return;
                    endif;
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $_db = $this->_connector;
                    if ( $_db->db_status && !empty($query) ) {
                            if ( $direct_query ) :
                                    $_db->db_exec($query, false, true, true, true, $asyn);
                            else :    
                                    $_db->db_exec($query);
                            endif;
                            return TRUE;
                    }
                    return FALSE;
                }

                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelected($table = '', $where = array()) {
                        if ( empty($table) ) {
                                return FALSE;
                        }
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $_db                                = $this->_connector;
                        (!empty($where)) ? $_db->where($where) : '';
                        $_db->from($table);
                        $result                             = $_db->db_array_query($_db->query);
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if(!$result)
                            return FALSE;
                        return $result;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDefaultLanguages($user = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ){
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "id_lang "
                                . "FROM ::PREFIX::_language "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND is_default = '1'";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDefaultISO($user = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "iso_code "
                                . "FROM ::PREFIX::_language "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND is_default = '1'";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function checkBatchID($user = null, $batch_id = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($batch_id) && empty($batch_id)) ){
                                return FALSE;
                        }
                        $sql = "SELECT "
                                . "count(*) "
                                . "FROM ebay_statistics_log "
                                . "WHERE batch_id = '".$batch_id."'";
                        
                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                
                public function getOrders($user = null, $id_shop = null, $ebay_site = null) {
                        if ( empty($user) || (!isset($id_shop) && empty($id_shop)) || (!isset($ebay_site) && empty($ebay_site)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "m.id_orders as id_order, "
                                . "m.id_marketplace_order_ref as id_marketplace_order_ref, "
                                . "m.site as site, "
                                . "m.payment_method as payment_method, "
                                . "m.total_paid as total_paid, "
                                . "m.order_date as order_date, "
                                . "n.id_buyer as id_buyer, "
                                . "n.email as email, "
                                . "m.status as status, "
                                . "o.id_invoice as id_invoice, "
                                . "o.invoice_no as invoice_no "
                                . "FROM ::PREFIX::_orders m LEFT JOIN ::PREFIX::_order_buyer n ON (m.id_orders = n.id_orders) "
                                . "LEFT JOIN ::PREFIX::_order_invoice o ON (m.id_orders = o.id_orders) "
                                . "WHERE m.id_shop = " . $id_shop . " "
                                . "AND m.site = '" . $ebay_site . "' ";    
                        
                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                public function getOrdersByRef($user = null, $ref = null) {
                        if ( empty($user) || empty($ref) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "id_orders "
                                . "FROM ::PREFIX::_orders "
                                . "WHERE id_marketplace_order_ref = '" . $ref . "'";

                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                public function getOrdersStatusID($user = null, $name = null) {
                        if ( empty($user) || empty($name) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "id_status "
                                . "FROM ::PREFIX::_order_status "
                                . "WHERE name = '" . $name . "'";
                       
                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                public function getOrdersFromDB($user = null, $id_site = null, $id_shop = null, $id_marketplace = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_marketplace) && empty($id_marketplace)) || (!isset($user) && empty($user)) ) {
                                return Null;
                        }

                        $sql = "SELECT "
                                . "o.id_marketplace_order_ref AS transaction_orders "
                                . "FROM "
                                . "::PREFIX::_orders o "
                                . "WHERE "
                                . "o.id_shop = '".$id_shop."' "
                                . "AND o.site = '".$id_site."' "
                                . "AND o.id_marketplace = '".$id_marketplace."'";

                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                public function getOrdersItemByID($user = null, $id = null) {
                        if ( empty($user) || (empty($id) && !isset($id)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "id_order_items "
                                . "FROM ::PREFIX::_order_items "
                                . "WHERE id_orders = '" . $id . "' "
                                . "ORDER BY id_order_items DESC LIMIT 1 ";
                        
                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                public function updateOrderStatus($id_order , $status, $id_invoice){
                        require_once(dirname(__FILE__) . '/../FeedBiz/config/orders.php');
                        $order = new Orders(ObjectModel::$user);
                        return $order->updateOrderStatus($id_order, $status, $id_invoice);
                }
                
                public function geteBayMappingInternational($id_site = null, $id_shop = null, $id_profile = 0) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "i.id_shipping as id_shipping, "
                                . "i.id_ebay_shipping as id_ebay_shipping, "
                                . "i.name_shipping as name_shipping, "
                                . "i.name_ebay_shipping as name_ebay_shipping, "
                                . "i.name_ebay_shipping_service as name_ebay_shipping_service, "
                                . "i.country_service as country_service, "
                                . "c.name_ebay_country as name_ebay_country, "
                                . "i.id_rule as id_rule, "
                                . "1 as is_international, "
                                . "r.shipping_cost as shipping_cost, "
                                . "r.shipping_additionals as shipping_additionals, "
                                . "r.min_weight as min_weight, "
                                . "r.max_weight as max_weight, "
                                . "r.min_price as min_price, "
                                . "r.max_price as max_price, "
                                . "r.surcharge as surcharge, "
                                . "Case "
                                . "WHEN r.rel IS NULL THEN 1 "
                                . "ELSE rel "
                                . "END as rel, "
                                . "r.operations as operations "
                                . "FROM ebay_mapping_carrier_international i "
                                . "LEFT JOIN ebay_mapping_carrier_country_selected c ON (i.country_service = c.country_service AND i.id_site = c.id_site AND i.id_ebay_shipping = c.id_ebay_shipping) "
                                . "LEFT JOIN ebay_mapping_carrier_rules r ON ((i.id_shipping = r.id_shipping) AND (i.country_service = r.country_service) AND (i.id_site = r.id_site) AND (i.id_ebay_shipping = r.id_ebay_shipping)) "
                                . "WHERE i.id_shop = '".$id_shop."' "
                                . "AND i.id_site = '".$id_site."' "
                                . "AND i.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function geteBayMappingDefault($id_site = null, $id_shop = null, $id_profile = 0) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "-1 as id_shipping, "
                                . "i.id_ebay_shipping as id_ebay_shipping, "
                                . "'Carrier default' AS name_shipping, "
                                . "i.name_ebay_shipping as name_ebay_shipping, "
                                . "i.name_ebay_shipping_service as name_ebay_shipping_service, "
                                . "i.id_rule as id_rule, "
                                . "r.is_international as is_international, "
                                . "r.country_service as country_service, "
                                . "r.shipping_cost as shipping_cost, "
                                . "r.shipping_additionals as shipping_additionals, "
                                . "r.min_weight as min_weight, "
                                . "r.max_weight as max_weight, "
                                . "r.min_price as min_price, "
                                . "r.max_price as max_price, "
                                . "r.surcharge as surcharge, "
                                . "Case "
                                . "WHEN r.rel IS NULL THEN 1 "
                                . "ELSE rel "
                                . "END as rel, "
                                . "r.operations as operations "
                                . "FROM ebay_mapping_carrier_default i "
                                . "LEFT JOIN ebay_mapping_carrier_rules r ON ((r.id_shipping = -1) AND (i.id_site = r.id_site) AND (i.id_ebay_shipping = r.id_ebay_shipping)) "
                                . "WHERE i.id_shop = '".$id_shop."' "
                                . "AND i.id_site = '".$id_site."' "
                                . "AND i.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function geteBayMappingDomestic($id_site = null, $id_shop = null, $id_profile = 0) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "i.id_shipping as id_shipping, "
                                . "i.id_ebay_shipping as id_ebay_shipping, "
                                . "i.name_shipping as name_shipping, "
                                . "i.name_ebay_shipping as name_ebay_shipping, "
                                . "i.name_ebay_shipping_service as name_ebay_shipping_service, "
                                . "i.id_rule as id_rule, "
                                . "r.is_international as is_international, "
                                . "r.country_service as country_service, "
                                . "r.shipping_cost as shipping_cost, "
                                . "r.shipping_additionals as shipping_additionals, "
                                . "r.min_weight as min_weight, "
                                . "r.max_weight as max_weight, "
                                . "r.min_price as min_price, "
                                . "r.max_price as max_price, "
                                . "r.surcharge as surcharge, "
                                . "Case "
                                . "WHEN r.rel IS NULL THEN 1 "
                                . "ELSE rel "
                                . "END as rel, "
                                . "r.operations as operations "
                                . "FROM ebay_mapping_carrier_domestic i "
                                . "LEFT JOIN ebay_mapping_carrier_rules r ON ((i.id_shipping = r.id_shipping) AND (i.id_site = r.id_site) AND (i.id_ebay_shipping = r.id_ebay_shipping)) "
                                . "WHERE i.id_shop = '".$id_shop."' "
                                . "AND i.id_site = '".$id_site."' "
                                . "AND i.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function geteBayMappingSpecificDefault($id_site = null, $id_shop = null ) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT
                                        r.id_profile AS id_profile,
                                        'mapping' AS table_key,
                                        -1 AS id_shipping,
                                        i.id_ebay_shipping AS id_ebay_shipping,
                                        'Carrier default' AS name_shipping,
                                        i.name_ebay_shipping AS name_ebay_shipping,
                                        i.name_ebay_shipping_service AS name_ebay_shipping_service,
                                        NULL AS country_service,
                                        NULL AS name_ebay_country,
                                        i.id_rule AS id_rule,
                                        i.is_enabled AS is_default,
                                        r.is_international AS is_international,
                                        r.shipping_cost AS shipping_cost,
                                        r.shipping_additionals AS shipping_additionals,
                                        r.min_weight AS min_weight,
                                        r.max_weight AS max_weight,
                                        r.min_price AS min_price,
                                        r.max_price AS max_price,
                                        r.surcharge AS surcharge,
                                        r.operations AS operations,
                                        r.is_enabled AS is_enabled
                                FROM
                                        ebay_mapping_carrier_default i
                                LEFT JOIN ebay_mapping_carrier_rules r ON(
                                        (i.id_rule = r.id_rule)
                                        AND(
                                                r.id_shipping = -1
                                        )
                                        AND(
                                                i.id_site = r.id_site
                                        )
                                        AND(
                                                i.id_shop = r.id_shop
                                        )
                                )
                                WHERE
                                        i.id_shop = '".$id_shop."'
                                AND i.id_site = '".$id_site."'
                                AND r.is_international = '0'
                                AND r.is_enabled = '1'
                                UNION
                                SELECT 
                                        id_profile AS id_profile,
                                        'specific' AS table_key,
                                        id_ebay_shipping as id_shipping,
                                        id_ebay_shipping AS id_ebay_shipping,
                                        NULL AS name_shipping,
                                        name_ebay_shipping AS name_ebay_shipping,
                                        name_ebay_shipping_service AS name_ebay_shipping_service,
                                        country_service AS country_service,
                                        name_ebay_country AS name_ebay_country,
                                        3 AS id_rule,
                                        0 AS is_default,
                                        0 AS is_international,
                                        shipping_cost AS shipping_cost,
                                        shipping_additionals AS shipping_additionals,
                                        NULL AS min_weight,
                                        NULL AS max_weight,
                                        NULL AS min_price,
                                        NULL AS max_price,
                                        NULL AS surcharge,
                                        NULL AS operations,
                                        is_enabled AS is_enabled
                                FROM
                                ebay_profiles_shipping
                                WHERE id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND country_service = ''
                                AND name_ebay_country = ''
                                AND is_enabled = '1'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function geteBayMappingSpecificDomestic($id_site = null, $id_shop = null ) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT
                                        r.id_profile AS id_profile,
                                        'mapping' AS table_key,
                                        i.id_shipping AS id_shipping,
                                        i.id_ebay_shipping AS id_ebay_shipping,
                                        i.name_shipping AS name_shipping,
                                        i.name_ebay_shipping AS name_ebay_shipping,
                                        i.name_ebay_shipping_service AS name_ebay_shipping_service,
                                        NULL AS country_service,
                                        NULL AS name_ebay_country,
                                        i.id_rule AS id_rule,
                                        r.is_international AS is_international,
                                        r.shipping_cost AS shipping_cost,
                                        r.shipping_additionals AS shipping_additionals,
                                        r.min_weight AS min_weight,
                                        r.max_weight AS max_weight,
                                        r.min_price AS min_price,
                                        r.max_price AS max_price,
                                        r.surcharge AS surcharge,
                                        r.operations AS operations,
                                        r.is_enabled AS is_enabled
                                FROM
                                        ebay_mapping_carrier_domestic i
                                LEFT JOIN ebay_mapping_carrier_rules r ON(
                                        (i.id_rule = r.id_rule)
                                        AND(
                                                i.id_shipping = r.id_shipping
                                        )
                                        AND(
                                                i.id_site = r.id_site
                                        )
                                        AND(
                                                i.id_shop = r.id_shop
                                        )
                                )
                                WHERE
                                        i.id_shop = '".$id_shop."'
                                AND i.id_site = '".$id_site."'
                                AND r.is_international = '0'
                                AND r.is_enabled = '1'
                                UNION
                                SELECT 
                                        id_profile AS id_profile,
                                        'specific' AS table_key,
                                        id_ebay_shipping as id_shipping,
                                        id_ebay_shipping AS id_ebay_shipping,
                                        NULL AS name_shipping,
                                        name_ebay_shipping AS name_ebay_shipping,
                                        name_ebay_shipping_service AS name_ebay_shipping_service,
                                        country_service AS country_service,
                                        name_ebay_country AS name_ebay_country,
                                        3 AS id_rule,
                                        0 AS is_international,
                                        shipping_cost AS shipping_cost,
                                        shipping_additionals AS shipping_additionals,
                                        NULL AS min_weight,
                                        NULL AS max_weight,
                                        NULL AS min_price,
                                        NULL AS max_price,
                                        NULL AS surcharge,
                                        NULL AS operations,
                                        is_enabled AS is_enabled
                                FROM
                                ebay_profiles_shipping
                                WHERE id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND country_service = ''
                                AND name_ebay_country = ''
                                AND is_enabled = '1'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                
                public function getCarrierDefault($id_site = null, $id_shop = null ) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT
                                d.id_ebay_shipping AS id_ebay_shipping,
                                d.name_shipping AS name_shipping,
                                d.name_ebay_shipping_service AS name_ebay_shipping_service,
                                r.shipping_cost AS shipping_cost, 
                                r.shipping_additionals AS shipping_additionals
                                FROM
                                ebay_mapping_carrier_domestic d
                                LEFT JOIN ebay_mapping_carrier_rules r ON 
                                (d.id_shipping = r.id_shipping AND d.id_ebay_shipping = r.id_ebay_shipping AND d.id_site = r.id_site AND d.id_shop = r.id_shop)
                                WHERE d.id_shipping IN (
                                SELECT
                                id_carrier
                                FROM
                                ::PREFIX::_carrier
                                WHERE id_shop = ".$id_shop."
                                AND is_default = 1)
                                AND d.id_site = ".$id_site."
                                AND d.id_shop = ".$id_shop."
                                ";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }
                
                public function geteBayMappingSpecificInternational($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }

                        $sql = "SELECT
                                        r.id_profile AS id_profile,
                                        'mapping' AS table_key,
                                        i.id_shipping AS id_shipping,
                                        i.id_ebay_shipping AS id_ebay_shipping,
                                        i.name_shipping AS name_shipping,
                                        i.name_ebay_shipping AS name_ebay_shipping,
                                        i.name_ebay_shipping_service AS name_ebay_shipping_service,
                                        i.country_service AS country_service,
                                        c.name_ebay_country AS name_ebay_country,
                                        i.id_rule AS id_rule,
                                        r.is_international AS is_international,
                                        r.shipping_cost AS shipping_cost,
                                        r.shipping_additionals AS shipping_additionals,
                                        r.min_weight AS min_weight,
                                        r.max_weight AS max_weight,
                                        r.min_price AS min_price,
                                        r.max_price AS max_price,
                                        r.surcharge AS surcharge,
                                        r.operations AS operations,
                                        r.is_enabled AS is_enabled
                                FROM
                                        ebay_mapping_carrier_international i
                                LEFT JOIN ebay_mapping_carrier_country_selected c ON(
                                        i.country_service = c.country_service
                                )
                                AND i.id_site = c.id_site
                                LEFT JOIN ebay_mapping_carrier_rules r ON(
                                        (i.id_rule = r.id_rule)
                                        AND(
                                                i.country_service = r.country_service
                                        )
                                        AND(
                                                i.id_shipping = r.id_shipping
                                        )
                                        AND(
                                                i.id_site = r.id_site
                                        )
                                        AND(
                                                i.id_shop = r.id_shop
                                        )
                                )
                                WHERE
                                        i.id_shop = '".$id_shop."'
                                AND i.id_site = '".$id_site."'
                                AND r.is_international = '1'
                                AND r.is_enabled = '1'
                                UNION
                                SELECT 
                                        id_profile AS id_profile,
                                        'specific' AS table_key,
                                        id_ebay_shipping as id_shipping,
                                        id_ebay_shipping AS id_ebay_shipping,
                                        NULL AS name_shipping,
                                        name_ebay_shipping AS name_ebay_shipping,
                                        name_ebay_shipping_service AS name_ebay_shipping_service,
                                        country_service AS country_service,
                                        name_ebay_country AS name_ebay_country,
                                        3 AS id_rule,
                                        0 AS is_international,
                                        shipping_cost AS shipping_cost,
                                        shipping_additionals AS shipping_additionals,
                                        NULL AS min_weight,
                                        NULL AS max_weight,
                                        NULL AS min_price,
                                        NULL AS max_price,
                                        NULL AS surcharge,
                                        NULL AS operations,
                                        is_enabled AS is_enabled
                                FROM
                                ebay_profiles_shipping
                                WHERE id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND country_service <> ''
                                AND name_ebay_country <> ''
                                AND is_enabled = '1'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getCurrentCurrency($id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "c.iso_code as iso_code "
                                . "FROM ::PREFIX::_product p "
                                . "LEFT JOIN ::PREFIX::_currency c on ((p.id_currency = c.id_currency) AND (p.id_shop = c.id_shop)) "
                                . "WHERE p.id_shop = '".$id_shop."' "
                                . "LIMIT 0, 1";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }
                
                public function getRulesID($id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "id_rule "
                                . "FROM ebay_mapping_carrier_rules "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND id_profile = '".$id_profile."' "
                                . "LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getProductTaxId($id_product = null) {
                        if (empty($id_product)){
                                return Null;
                        }
                        
                        $sql = "SELECT b.id_tax as id_tax
                                FROM ebay_product_item_id a 
                                JOIN ::PREFIX::_product b ON a.id_shop = b.id_shop and a.id_product = b.id_product 
                                WHERE a.id_item = '".$id_product."' 
                                LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getBatchLogByOffset($id_site = null, $id_shop = null, $limit = 10, $offset = 0) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT *"
                                . "FROM "
                                . "ebay_statistics_log "
                                . "WHERE "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."'"
                                . "ORDER BY date_add DESC "
                                . "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getBatchLogByBatchID($user = null, $id_site = null, $id_shop = null, $batch_id = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($batch_id) && empty($batch_id)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "batch_id, "
                                . "total, "
                                . "success, "
                                . "warning, "
                                . "error, "
                                . "date_add "
                                . "FROM "
                                . "ebay_statistics_log "
                                . "WHERE "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."'"
                                . "AND batch_id = '".$batch_id."'";
                                                
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function updateEbayStatisticsLog($user = null, $batch_id = null){
                	if ( (!isset($user) && empty($user)) || (!isset($batch_id) && empty($batch_id)) ){
                		return Null;
                	}
                	$sql = "UPDATE ebay_statistics_log SET 
                                success = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response = 'Success'),
                                warning = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response = 'Warning'),
                                error   = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response in ('Error', 'Failure'))
                                WHERE batch_id = '::BATCHID::';";
                        $sql = str_replace('::BATCHID::', $batch_id, $sql);
                        $this->sqlQueryAffected($sql);
                        
                        $sql = "UPDATE ebay_statistics_log SET total = success + warning + error WHERE batch_id = '::BATCHID::';";
                        $sql = str_replace('::BATCHID::', $batch_id, $sql);
                        $this->sqlQueryAffected($sql);
                }
                
                public function updateEbayStatisticsLogSql($user = null, $batch_id = null){
                	if ( (!isset($user) && empty($user)) || (!isset($batch_id) && empty($batch_id)) ){
                		return Null;
                	}
                	$sql = "UPDATE ebay_statistics_log SET 
                                success = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response = 'Success'),
                                warning = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response = 'Warning'),
                                error   = (SELECT COUNT(*) as cnt FROM ebay_statistics WHERE batch_id = '::BATCHID::' AND response in ('Error', 'Failure'))
                                WHERE batch_id = '::BATCHID::';
                                UPDATE ebay_statistics_log SET total = success + warning + error WHERE batch_id = '::BATCHID::';";
                        $sql = str_replace('::BATCHID::', $batch_id, $sql);
                        return $sql;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getLogByParam($where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        
                        if ( !empty($where) ) {
                                foreach ( $where as $key => $field ) {
                                        if ( !empty($field) || is_numeric($field) ) {
                                                if ( empty($sqlWhere) ) {
                                                        if ( $key == 'id_site' || $key == 'id_shop' ) {
                                                                $sqlWhere   .= "WHERE ".$key ." = '". $field."' ";
                                                        }
                                                        else{
                                                                $sqlWhere   .= "WHERE ".$key ." LIKE '%". $field."%' ";
                                                        }
                                                }
                                                else if ( $operation == 'OR' && ($key != 'id_site' && $key != 'id_shop') ){
                                                        if ( empty($sqlOparation) ) {
                                                                $sqlOparation .= "AND ( ".$key ." LIKE '%". $field."%' ";
                                                        }
                                                        else{
                                                                $sqlOparation .= "OR ".$key ." LIKE '%". $field."%' ";
                                                        }
                                                }
                                                else{
                                                        if ( $key == 'id_site' || $key == 'id_shop' ) {
                                                                $sqlWhere   .= "AND ".$key ." = '". $field."' ";
                                                        }
                                                        else{
                                                                $sqlWhere   .= "AND ".$key ." LIKE '%". $field."%' ";
                                                        }
                                                }
                                        }
                                }
                        }
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;

                        
                        $sql = "SELECT ".$count." "
                                . "FROM ebay_statistics_log "
                                . $sqlWhere;
                                if ( !empty($sqlOparation) ) {
                        $sql    .= $sqlOparation.") ";
                                }
                                if ( !empty($order_name) && !empty($order_by) ) {
                        $sql    .= "ORDER BY ".$order_name." ".$order_by." ";
                                }else{
                         $sql   .= "ORDER BY date_add DESC ";
                                }
                                if ( $limit !== 0 && $offset !== 0 ) {
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                }

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getStatisticByOffset($ebay_user = null, $id_site = null, $id_shop = null, $limit = 10, $offset = 0) {
                        if ( empty($ebay_user) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) {
                                return Null;
                        }

                        $sql = "SELECT * "
                                . "FROM ebay_statistics "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getStatisticByParam($where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND',$ignore_case=false) {
                		$order_name_filter = array('s.product_ean13' => 'p.ean13', 's.product_sku' => 'p.sku');
                        $sqlWhere = '';
                        $sqlOparation = '';
                        
                        if ( !empty($where) ) {
                                foreach ( $where as $key => $field ){
                                        if ( !empty($field) || is_numeric($field) ){
                                                if ( empty($sqlWhere) ) {
                                                        if ( $key == 's.id_site' || $key == 's.id_shop' ) 
                                                                $sqlWhere   .= "WHERE ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "WHERE ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else if ( $operation == 'OR' && ($key != 's.id_site' && $key != 's.id_shop') ){
                                                        if ( empty($sqlOparation) ) 
                                                                $sqlOparation .= "AND ( ".$key ." LIKE '%". $field."%' ";
                                                        else
                                                                $sqlOparation .= "OR ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else{
                                                        if ( $key == 's.id_site' || $key == 's.id_shop' ) 
                                                                $sqlWhere   .= "AND ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "AND ".$key ." LIKE '%". $field."%' ";
                                                }
                                        }
                                }
                        }
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        $order_name = isset($order_name_filter[$order_name]) ? $order_name_filter[$order_name] : $order_name;
                        if($ignore_case){
                            $sqlWhere .= " AND code not in ('FB0015','FB0021') ";
                        }
                        $sql = "SELECT ".$count." "
                                . "FROM ebay_statistics s "
                                . "JOIN ::PREFIX::_product p ON p.id_product = s.id_product AND p.id_shop = s.id_shop "
                                . $sqlWhere;
                                if ( !empty($sqlOparation) ){
                        $sql    .= $sqlOparation.") ";
                                }
                                if ( !empty($order_name) && !empty($order_by) ){
                        $sql    .= "ORDER BY ".$order_name." ".$order_by." ";
                                }else {
                         $sql   .= "ORDER BY s.date_add DESC ";
                                }
                                if ( $limit !== 0 && $offset !== 0 ) {
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                }
                                
                        return $this->sqlQuerySwap($sql, 'products');
                }

                public function getStatisticProducts($errorCode, $search, $limit = 10, $offset = 0) {
                	
                	$sql = "SELECT ::QUERY::
								FROM ebay_statistics s
								WHERE s.resolved <> '1' AND s.code = '".htmlspecialchars($errorCode)."' ";
                	if(!empty($search)){
                		$sql .= " AND (s.sku_export like '%".$search."%' OR s.name_product like '%".$search."%' OR p.id_product = '".$search."' OR p.id_combination = '".$search."')";
                	}
                	
                	$sqlDataFields = "s.id_product as ProductID, s.name_product as ProductName, s.sku_export as ProductSKU, s.sku_export as CombinationSKU, s.id_combination as CombinationID, s.message as Message, s.resolved as Resolved";
                	
                	$sqlData = str_replace('::QUERY::', $sqlDataFields, $sql);
                	$sqlData .= "GROUP BY s.id_product, id_combination ORDER BY s.id_product LIMIT ".intval($limit)." OFFSET ".intval($offset);
                	$sqlCount = str_replace('::QUERY::', 'count(*) as count', $sql);          	
                	$return = $this->sqlQuerySwap($sqlData, 'products');
                	$read = array();
                	foreach($return as $return_key=>$return_element){
                		$ProductID = $return_element['ProductID'];
                		if(isset($read[$ProductID]) && $return_element['CombinationID'] > 0){
                			$return[$return_key]['ProductName'] = '';
                			$return[$return_key]['ProductSKU'] = '';
                		}
                		$read[$ProductID] = true;
                	}      	
                	
                	$rawCount = $this->sqlQuerySwap($sqlCount, 'products');
                	$count = 0;
                	foreach($rawCount as $row){
                		$count = $row['count'];
                	}
                	
                	return array('data'=>$return, 'count'=>$count);
                }
                
                public function setStatisticProductSolve($batchID, $errorCode, $productID, $resolved){
                	$db = new Db($this->_user, 'products');
                	$db->db_exec("UPDATE ebay_statistics SET resolved = '".intval($resolved)."' WHERE batch_id = '".htmlspecialchars($batchID)."' AND code = '".htmlspecialchars($errorCode)."' AND id_product = '".intval($productID)."'");
                	
                	$rawCount = $this->sqlQuerySwap("SELECT COUNT(*) as count FROM ebay_statistics WHERE batch_id='".$batchID."' AND code='".$errorCode."' AND resolved = '1'", 'products');
                	$count = 0;
                	foreach($rawCount as $row){
                		$count = $row['count'];
                	}
                	
                	return array('resolved_count'=>$count);
                }
                
                public function getOrderByParam($user, $id_site = null, $id_shop = null, $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        $sqlOparation                       = '';
                        if ( !empty($where) ){
                                foreach ( $where as $key => $field ){
                                        if ( !empty($field) || is_numeric($field) ) {
                                                if ( $operation == 'OR' ) {
                                                        $sqlOparation   .= "OR ".$key ." LIKE '%". $field."%' ";
                                                } 
                                                else {
                                                        if ( $key == "m.order_status" && $field == "Active") :
                                                                $sqlOparation   .= "AND ".$key ." LIKE '%". $field."%' AND m.order_type LIKE '%Standard%' ";
                                                        elseif ( $key == "m.order_status" && $field == "Cancelled") :
                                                                $sqlOparation   .= "AND (".$key ." LIKE '%". $field."%' OR m.order_status IN ('Active', 'Completed', 'Shipped') AND m.order_type NOT LIKE '%Standard%') ";
                                                        else :
                                                                $sqlOparation   .= "AND ".$key ." LIKE '%". $field."%' ";
                                                        endif;
                                                }
                                        }
                                }
                        }
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." "
                                . "FROM ::PREFIX::_orders m LEFT JOIN ::PREFIX::_order_buyer n ON (m.id_orders = n.id_orders) "
                                . "LEFT JOIN ::PREFIX::_order_invoice o ON (m.id_orders = o.id_orders) "
                                . "LEFT JOIN ::PREFIX::_order_shipping s ON (m.id_orders = s.id_orders) "
                                . "LEFT JOIN mp_multichannel mp ON mp.id_orders = o.id_orders AND mp.id_shop = m.id_shop "
                                . "WHERE m.site = '".$id_site."' AND m.id_shop = '".$id_shop."' ";
                                if ( !empty($sqlOparation) ) {
                        $sql    .= $sqlOparation." ";
                                }
                                if ( !empty($order_name) && !empty($order_by) ) {
                        $sql    .= "ORDER BY m.".$order_name." ".$order_by." ";
                                }else {
                        $sql   .= "ORDER BY m.id_orders DESC ";
                                }
                                if ( $limit !== 0 && $offset !== 0 ) {
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                }

                        return $this->sqlQuerySwap($sql, 'orders');
                }
                
                
                public function getAdvanceCategoriesByParam($user, $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        if ( !empty($where) ) {
                                foreach ( $where as $key => $field ) {
                                        if ( !empty($field) || is_numeric($field) ) {
                                                if ( empty($sqlWhere) ) {
                                                        if ( $key == 'm.site' || $key == 'm.id_shop' ) 
                                                                $sqlWhere   .= "WHERE ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "WHERE ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else if ( $operation == 'OR' && ($key != 'm.site' && $key != 'm.id_shop') ) {
                                                        if ( empty($sqlOparation) ) 
                                                                $sqlOparation .= "AND ( ".$key ." LIKE '%". $field."%' ";
                                                        else
                                                                $sqlOparation .= "OR ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else {
                                                        if ( $key == 'm.site' || $key == 'm.id_shop' ) 
                                                                $sqlWhere   .= "AND ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "AND ".$key ." LIKE '%". $field."%' ";
                                                }
                                        }
                                }
                        }
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." "
                                . "FROM ebay_profiles m LEFT JOIN ebay_profiles_group n ON (m.id_profile = n.id_profile AND n.type = 'category') "
                                . $sqlWhere;
                                if ( !empty($sqlOparation) ) {
                        $sql    .= $sqlOparation.") ";
                                }
                                if ( !empty($order_name) && !empty($order_by) ) {
                        $sql    .= "ORDER BY m.".$order_name." ".$order_by." ";
                                }else {
                        $sql   .= "ORDER BY m.date_add DESC ";
                                }
                                if ( $limit !== 0 && $offset !== 0 ) {
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                }

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAdvanceProductsByParam($user, $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        if ( !empty($where) ) {
                                foreach ( $where as $key => $field ) {
                                        if ( !empty($field) || is_numeric($field) ) {
                                                if ( empty($sqlWhere) ) {
                                                        if ( $key == 'm.site' || $key == 'm.id_shop' ) 
                                                                $sqlWhere   .= "WHERE ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "WHERE ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else if ( $operation == 'OR' && ($key != 'm.site' && $key != 'm.id_shop') ) {
                                                        if ( empty($sqlOparation) ) 
                                                                $sqlOparation .= "AND ( ".$key ." LIKE '%". $field."%' ";
                                                        else
                                                                $sqlOparation .= "OR ".$key ." LIKE '%". $field."%' ";
                                                }
                                                else {
                                                        if ( $key == 'm.site' || $key == 'm.id_shop' ) 
                                                                $sqlWhere   .= "AND ".$key ." = '". $field."' ";
                                                        else
                                                                $sqlWhere   .= "AND ".$key ." LIKE '%". $field."%' ";
                                                }
                                        }
                                }
                        }
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." "
                                . "FROM ebay_profiles m LEFT JOIN ebay_profiles_group n ON (m.id_profile = n.id_profile AND n.type = 'product') "
                                . $sqlWhere;
                                if ( !empty($sqlOparation) ) {
                        $sql    .= $sqlOparation.") ";
                                }
                                if ( !empty($order_name) && !empty($order_by) ) {
                        $sql    .= "ORDER BY m.".$order_name." ".$order_by." ";
                                }else {
                        $sql   .= "ORDER BY m.date_add DESC ";
                                }
                                if ( $limit !== 0 && $offset !== 0 ) {
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                }

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAdvanceMappingCategoriesByParam($id_shop = null, $id_lang = '', $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        if ( (empty($id_lang) && !isset($id_lang)) || (empty($id_shop) && !isset($id_shop)) ) :
                                return FALSE;
                        endif;

                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        $sqlIdCategory                      = '';
                        $sqlIdProduct                       = '';
                        $sqlIdCategoryDefault               = '';
                        $sqlContent                         = '';
                        $sqlInformation                     = '';
                        $sqlDetail                          = '';
                        if ( !empty($where) ) :
                                foreach ( $where as $key => $field ) :
                                        if ( !empty($field) || is_numeric($field) ) :
                                                if ( $operation == 'OR' ) :
                                                        if ( empty($sqlOparation) ) :
                                                                $sqlOparation   .= " ( pl.name LIKE '%". $field."%' OR l.name LIKE '%". $field."%' OR m.name LIKE '%". $field."%' OR s.name LIKE '%". $field."%' )";
                                                                $sqlOparation   .= " OR ( p.sku LIKE '%". $field."%' OR p.ean13 LIKE '%". $field."%' OR p.upc LIKE '%". $field."%' OR p.reference LIKE '%". $field."%' )";
                                                                $sqlOparation   .= " OR ( p.quantity LIKE '%". $field."%' OR p.price LIKE '%". $field."%' OR c.name LIKE '%". $field."%' )";
                                                        endif;
                                                        if ( $key == 'id_product' ) :
                                                                if ( !empty($field['id_product']) ) : 
                                                                        foreach ( $field['id_product'] as $id_product ) :
                                                                                if ( isset($id_product) && is_numeric($id_product)  ) :
                                                                                        if ( empty($sqlIdProduct) ) :
                                                                                                $sqlIdProduct   .= "p.".$key ." = '". $id_product."' ";
                                                                                        else :
                                                                                                $sqlIdProduct   .= "OR p.".$key ." = '". $id_product."' ";
                                                                                        endif;
                                                                                endif;
                                                                        endforeach;
                                                                        if ( !empty($sqlIdProduct) ) :
                                                                                $sqlIdProduct   = " OR (".$sqlIdProduct.") ";
                                                                        endif;
                                                                endif;
                                                        endif;
                                                else :
                                                        if ( $key == 'id_category' ) :
                                                                if ( !empty($field['id_category']) ) :  
                                                                        foreach ( $field['id_category'] as $id_category ) :
                                                                                if ( isset($id_category) && is_numeric($id_category)  ) :
                                                                                        if ( empty($sqlIdCategory) ) :
                                                                                                $sqlIdCategory   .= "cy.".$key ." = '". $id_category."' ";
                                                                                        else :
                                                                                                $sqlIdCategory   .= "OR cy.".$key ." = '". $id_category."' ";
                                                                                        endif;
                                                                                endif;
                                                                        endforeach;
                                                                        if ( !empty($sqlIdCategory) ) :
                                                                                $sqlIdCategory   = " (".$sqlIdCategory.") ";
                                                                        endif;
                                                                endif;
                                                                if ( empty($sqlOparation) ) :
                                                                        $sqlOparation   = $sqlIdCategory;
                                                                elseif ( !empty($sqlOparation) && !empty($sqlIdCategory) ) : 
                                                                        $sqlOparation   .= " AND ".$sqlIdCategory;
                                                                endif;
                                                        endif; 
                                                        if ( $key == 'id_product' ) :
                                                                if ( !empty($field['id_product']) ) : 
                                                                        foreach ( $field['id_product'] as $id_product ) :
                                                                                if ( isset($id_product) && is_numeric($id_product)  ) :
                                                                                        if ( empty($sqlIdProduct) ) :
                                                                                                $sqlIdProduct   .= "p.".$key ." = '". $id_product."' ";
                                                                                        else :
                                                                                                $sqlIdProduct   .= "OR p.".$key ." = '". $id_product."' ";
                                                                                        endif;
                                                                                endif;
                                                                        endforeach;
                                                                        if ( !empty($sqlIdProduct) ) :
                                                                                $sqlIdProduct   = " OR (".$sqlIdProduct.") ";
                                                                        endif;
                                                                endif;
                                                        endif;
                                                        if ( $key == 'product_content' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( pl.name LIKE '%". $field['name']."%' OR l.name LIKE '%". $field['name']."%' OR m.name LIKE '%". $field['name']."%' OR s.name LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( pl.name LIKE '%". $field['name']."%' OR l.name LIKE '%". $field['name']."%' OR m.name LIKE '%". $field['name']."%' OR s.name LIKE '%". $field['name']."%' )";
                                                                endif;
                                                                
                                                        endif;
                                                        if ( $key == 'product_information' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( p.sku LIKE '%". $field['name']."%' OR p.ean13 LIKE '%". $field['name']."%' OR p.upc LIKE '%". $field['name']."%' OR p.reference LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( p.sku LIKE '%". $field['name']."%' OR p.ean13 LIKE '%". $field['name']."%' OR p.upc LIKE '%". $field['name']."%' OR p.reference LIKE '%". $field['name']."%' )";
                                                                endif;
                                                                
                                                        endif;
                                                        if ( $key == 'product_detail' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( p.quantity = '". $field['name']."' OR p.price = '". $field['name']."' OR c.name LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( p.quantity = '". $field['name']."' OR p.price = '". $field['name']."' OR c.name LIKE '%". $field['name']."%' )";
                                                                endif;
                                                                
                                                        endif;
                                                        if ( $key == 'choose_ref' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( p.quantity = '". $field['name']."' OR p.price = '". $field['name']."' OR c.name LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( p.quantity = '". $field['name']."' OR p.price = '". $field['name']."' OR c.name LIKE '%". $field['name']."%' )";
                                                                endif;
                                                                
                                                        endif;
                                                        if ( $key == 'active' ) : 
                                                                $sqlWhere   .= " AND ( p.active = '". $field. "' )";
                                                        endif;
                                                endif;
                                        endif;
                                endforeach;
                        endif;

                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." "
                                . "FROM "
                                . "::PREFIX::_product p "
                                . "LEFT JOIN ::PREFIX::_category_lang l ON (p.id_category_default = l.id_category AND l.id_shop = '".$id_shop."' AND l.id_lang = '".$id_lang."') "
                                . "LEFT JOIN ::PREFIX::_manufacturer m ON (p.id_manufacturer = m.id_manufacturer AND m.id_shop = '".$id_shop."')  "
                                . "LEFT JOIN ::PREFIX::_conditions c ON (p.id_condition = c.id_condition AND c.id_shop = '".$id_shop."') "
                                . "LEFT JOIN ::PREFIX::_supplier s ON (p.id_supplier = s.id_supplier AND s.id_shop = '".$id_shop."') "
                                . "LEFT JOIN ::PREFIX::_currency cr ON (p.id_currency = cr.id_currency AND cr.id_shop = '".$id_shop."') "
                                . "LEFT JOIN ::PREFIX::_product_image i ON (p.id_product = i.id_product AND i.image_type = 'default' AND i.id_shop = '".$id_shop."') "
                                . "LEFT JOIN ::PREFIX::_product_lang pl ON (p.id_product = pl.id_product AND pl.id_shop = '".$id_shop."' AND pl.id_lang = '".$id_lang."') "
                                . "LEFT JOIN ::PREFIX::_category cy ON(cy.id_category = p.id_category_default AND cy.id_shop = '".$id_shop."') "
                                . "WHERE (p.id_shop = '".$id_shop."' "
                                . "AND l.id_shop = '".$id_shop."' "
                                . "AND m.id_shop = '".$id_shop."' "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "AND s.id_shop = '".$id_shop."' "
                                . "AND cr.id_shop = '".$id_shop."' "
                                . "AND cy.id_shop = '".$id_shop."' "
                                . "AND i.id_shop = '".$id_shop."' "
                                . "AND pl.id_shop = '".$id_shop."' "
                                . "AND l.id_lang = '".$id_lang."' "
                                . "AND pl.id_lang = '".$id_lang."' ";
                                if ( !empty($sqlOparation) ) :
                        $sql    .= "AND (".$sqlOparation.") ";
                                endif;

                        $sql    .= $sqlIdProduct;
                        $sql    .= ') ';
                        $sql    .= $sqlWhere;
                        $sql    .= "GROUP BY p.id_product ";
                                if ( !empty($order_name) && !empty($order_by) ) :
                        $sql    .= "ORDER BY p.".$order_name." ".$order_by." ";
                                else :
                        $sql    .= "ORDER BY p.id_product ASC ";
                                endif;

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                public function getAdvanceMappingCategoriesOfferByParam($user = null, $id_shop = null, $id_lang = '', $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        if ( (empty($id_lang) && !isset($id_lang)) || (empty($id_shop) && !isset($id_shop)) || (empty($user) && !isset($user)) ) :
                                return FALSE;
                        endif;

                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        $sqlIdCategory                      = '';
                        $sqlIdProduct                       = '';
                        $sqlIdCategoryDefault               = '';
                        if ( !empty($where) ) :
                                foreach ( $where as $key => $field ) :
                                        if ( !empty($field) || is_numeric($field) ) :
                                                if ( $key == 'id_category' ) :
                                                        if ( !empty($field['id_category']) ) : 
                                                                foreach ( $field['id_category'] as $id_category ) :
                                                                        if ( isset($id_category) && is_numeric($id_category)  ) :
                                                                                if ( empty($sqlIdCategory) ) :
                                                                                        $sqlIdCategory   .= "cy.".$key ." = '". $id_category."' ";
                                                                                else :
                                                                                        $sqlIdCategory   .= "OR cy.".$key ." = '". $id_category."' ";
                                                                                endif;
                                                                        endif;
                                                                endforeach;
                                                                if ( !empty($sqlIdCategory) ) :
                                                                        $sqlIdCategory   = " (".$sqlIdCategory.") ";
                                                                endif;
                                                        endif;
                                                        if ( empty($sqlOparation) ) :
                                                                $sqlOparation   = $sqlIdCategory;
                                                        elseif ( !empty($sqlOparation) && !empty($sqlIdCategory) ) : 
                                                                $sqlOparation   .= " AND ".$sqlIdCategory;
                                                        endif;
                                                endif;
                                        endif;
                                endforeach;
                        endif;
                        
                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." "
                                . "FROM "
                                . "::PREFIX::_product p "
                                . "LEFT JOIN ::PREFIX::_category cy ON(cy.id_category = p.id_category_default) "
                                . "WHERE p.id_shop = '".$id_shop."' "
                                . "AND cy.id_shop = '".$id_shop."' ";
                                if ( !empty($sqlOparation) ) :
                        $sql    .= "AND (".$sqlOparation.") ";
                                endif;
                        $sql    .= "GROUP BY p.id_product ";
                                if ( !empty($order_name) && !empty($order_by) ) :
                        $sql    .= "ORDER BY p.".$order_name." ".$order_by." ";
                                else :
                         $sql   .= "ORDER BY p.id_product ASC ";
                                endif;

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                                
                public function getAdvanceMappingProductAttributeByParam($id_shop = null, $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        if ( (empty($id_shop) && !isset($id_shop)) ) :
                                return FALSE;
                        endif;
                        $_db                                = $this->_connector;
                        $sqlWhere                           = '';
                        $sqlOparation                       = '';
                        $sqlIdCategory                      = '';
                        $sqlIdProduct                       = '';
                        $sqlIdCategoryDefault               = '';
                        $sqlContent                         = '';
                        $sqlInformation                     = '';
                        $sqlDetail                          = '';
                        if ( !empty($where) ) :
                                foreach ( $where as $key => $field ) :
                                        if ( !empty($field) || is_numeric($field) ) :
                                                if ( $operation == 'OR' ) :
                                                        if ( empty($sqlOparation) ) :
                                                                $sqlOparation   .= " ( al.name LIKE '%". $field."%' OR l.name LIKE '%". $field."%' )";
                                                                $sqlOparation   .= " OR ( a.sku LIKE '%". $field."%' OR a.ean13 LIKE '%". $field."%' OR a.upc LIKE '%". $field."%' OR a.reference LIKE '%". $field."%' )";
                                                                $sqlOparation   .= " OR ( a.quantity LIKE '%". $field."%' OR a.price LIKE '%". $field."%' )";
                                                        endif;
                                                else :
                                                        if ( $key == 'id_category' ) :
                                                                if ( !empty($field['id_category']) ) :
                                                                        foreach ( $field['id_category'] as $id_category ) :
                                                                                if ( isset($id_category) && is_numeric($id_category)  ) :
                                                                                        if ( empty($sqlIdCategory) ) :
                                                                                                $sqlIdCategory   .= "cy.".$key ." = '". $id_category."' ";
                                                                                        else :
                                                                                                $sqlIdCategory   .= "OR cy.".$key ." = '". $id_category."' ";
                                                                                        endif;
                                                                                endif;
                                                                        endforeach;
                                                                        if ( !empty($sqlIdCategory) ) :
                                                                                $sqlIdCategory   = " AND (".$sqlIdCategory.") ";
                                                                        endif;
                                                                endif;
                                                        endif;
                                                        if ( $key == 'id_product' ) :
                                                                if ( !empty($field['id_product']) ) :
                                                                        foreach ( $field['id_product'] as $id_product ) :
                                                                                if ( isset($id_product) && is_numeric($id_product)  ) :
                                                                                        if ( empty($sqlIdProduct) ) :
                                                                                                $sqlIdProduct   .= "a.".$key ." = '". $id_product."' ";
                                                                                        else :
                                                                                                $sqlIdProduct   .= "OR a.".$key ." = '". $id_product."' ";
                                                                                        endif;
                                                                                endif;
                                                                        endforeach;
                                                                        if ( !empty($sqlIdProduct) ) :
                                                                                $sqlIdProduct   = " AND (".$sqlIdProduct.") ";
                                                                        endif;
                                                                endif;
                                                        endif;
                                                        if ( $key == 'product_content' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( al.name LIKE '%". $field['name']."%' OR l.name LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( al.name LIKE '%". $field['name']."%' OR l.name LIKE '%". $field['name']."%' )";
                                                                endif;
                                                        endif;
                                                        if ( $key == 'product_information' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( a.sku LIKE '%". $field['name']."%' OR a.ean13 LIKE '%". $field['name']."%' OR a.upc LIKE '%". $field['name']."%' OR a.reference LIKE '%". $field['name']."%' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( a.sku LIKE '%". $field['name']."%' OR a.ean13 LIKE '%". $field['name']."%' OR a.upc LIKE '%". $field['name']."%' OR a.reference LIKE '%". $field['name']."%' )";
                                                                endif;
                                                                
                                                        endif;
                                                        if ( $key == 'product_detail' ) :
                                                                if ( !empty($field['name']) ) :
                                                                        if ( empty($sqlOparation) ) 
                                                                                $sqlOparation   .= "( a.quantity = '". $field['name']."' OR a.price = '". $field['name']."' )";
                                                                        else
                                                                                $sqlOparation   .= "AND ( a.quantity = '". $field['name']."' OR a.price = '". $field['name']."' )";
                                                                endif;
                                                                
                                                        endif;
                                                endif;
                                        endif;
                                endforeach;
                        endif;

                        $sql = "SELECT "
                                . "a.id_product AS id_product, "
                                . "a.id_product_attribute AS id_product_attribute, "
                                . "ac.id_attribute AS id_attribute, "
                                . "ac.id_attribute_group AS id_attribute_group, "
                                . "a.reference AS reference, "
                                . "a.sku AS sku, "
                                . "a.ean13 AS ean13, "
                                . "a.upc AS upc, "
                                . "a.price AS price, "
                                . "a.quantity AS quantity, "
                                . "a.price_type AS price_type, "
                                . "a.date_add AS date_add, "
                                . "al.name AS attribute_name, "
                                . "l.name AS attribute_value "
                                . "FROM "
                                . "::PREFIX::_product_attribute a "
                                . "LEFT JOIN ::PREFIX::_product_attribute_combination ac ON ( "
                                . "a.id_product_attribute = ac.id_product_attribute AND ac.id_shop = '".$id_shop."' "
                                . ") "
                                . "LEFT JOIN ::PREFIX::_attribute_group_lang al ON ( "
                                . "ac.id_attribute_group = al.id_attribute_group AND al.id_shop = '".$id_shop."' "
                                . ") "
                                . "LEFT JOIN ::PREFIX::_attribute_lang l ON ( "
                                . "ac.id_attribute_group = l.id_attribute_group AND ac.id_attribute = l.id_attribute AND l.id_shop = '".$id_shop."' AND l.id_lang = '".$id_shop."' "
                                . ") "
                                . "WHERE a.id_shop = '".$id_shop."' "
                                . "AND ac.id_shop = '".$id_shop."' "
                                . "AND al.id_shop = '".$id_shop."' "
                                . "AND l.id_shop = '".$id_shop."' "
                                . "AND al.id_lang = '".$id_shop."' "
                                . "AND l.id_lang = '".$id_shop."' "
                                . "AND a.id_product IN (SELECT  "
                                . "p.id_product AS id_product "
                                . "FROM "
                                . "::PREFIX::_product p "
                                . "LEFT JOIN ::PREFIX::_category cy ON(cy.id_category = p.id_category_default AND cy.id_shop = '".$id_shop."') "
                                . "WHERE p.id_shop = '".$id_shop."' "
                                . "AND cy.id_shop = '".$id_shop."' "
                                . $sqlIdCategory." "
                                . "ORDER BY p.id_product ASC ";

                        $sql    .= ") ";
                                if ( !empty($sqlOparation) ) :
                        $sql    .= "AND (".$sqlOparation.") ";
                                endif;
                        $sql    .= $sqlIdProduct. " ";
                        $sql    .= "GROUP BY a.id_product_attribute, l.id_attribute_group ";
                                if ( !empty($order_name) && !empty($order_by) ) :
                        $sql    .= "ORDER BY ".$order_name." ".$order_by." ";
                                else :
                        $sql    .= "ORDER BY a.id_product ASC ";
                                endif;

                        return $this->sqlQuerySwap($sql, 'products');
                }
                                
                public function getAdvanceMappingProductAttributeOfferByParam($user = null, $id_shop = null, $where = '', $limit = 10, $offset = 0, $count = '*', $order_name = '', $order_by = '', $operation = 'AND') {
                        if ( (empty($user) && !isset($user)) || (empty($id_shop) && !isset($id_shop)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT "
                                . "a.id_product AS id_product, "
                                . "a.id_product_attribute AS id_product_attribute, "
                                . "a.reference AS reference, "
                                . "a.sku AS sku, "
                                . "a.ean13 AS ean13, "
                                . "a.upc AS upc, "
                                . "a.price AS price, "
                                . "a.quantity AS quantity, "
                                . "a.price_type AS price_type, "
                                . "a.date_add AS date_add "
                                . "FROM "
                                . "::PREFIX::_product_attribute a "
                                . "WHERE a.id_shop = '".$id_shop."' ";
                                if ( !empty($sqlOparation) ) :
                        $sql    .= $sqlOparation.") ";
                                endif;
                                if ( !empty($order_name) && !empty($order_by) ) :
                        $sql    .= "ORDER BY ".$order_name." ".$order_by." ";
                                else :
                        $sql    .= "ORDER BY a.id_product ASC ";
                                endif;

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                                
                public function getAdvanceMappingProductCategoryByParam($user = null, $id_site = null, $id_shop = null, $id_lang = '', $limit = 10, $offset = 0, $id_category = null, $count = null, $string = null) {
                        if ( (empty($user) && !isset($user)) || (empty($id_site) && !isset($id_site)) || (empty($id_shop) && !isset($id_shop)) || (empty($id_lang) && !isset($id_lang)) ) :
                                return FALSE;
                        endif;

                        $sqlIdCategory                      = '';
                        $sqlProfileName                     = '';
                        if ( !empty($id_category) ) :
                                foreach ( $id_category as $key => $field ) :
                                        if ( !empty($field) || is_numeric($field) ) :
                                                if ( $key == 'id_category' ) : 
                                                        if ( !empty($field) ) :
                                                                foreach ( $field as $id_category ) :
                                                                        if ( isset($id_category) && is_numeric($id_category)  ) :
                                                                                if ( empty($sqlIdCategory) ) :
                                                                                        $sqlIdCategory   .= "c.".$key ." = '". $id_category."' ";
                                                                                else :
                                                                                        $sqlIdCategory   .= "OR c.".$key ." = '". $id_category."' ";
                                                                                endif;
                                                                        endif;
                                                                endforeach;

                                                                if ( !empty($sqlIdCategory) ) :
                                                                        $sqlIdCategory   = " (".$sqlIdCategory.") ";
                                                                endif;
                                                        endif;
                                                elseif ( $key == 'profile_name' ) :
                                                        if ( empty($sqlProfileName) ) :
                                                                $sqlProfileName   .= "AND p.".$key ." LIKE '%". $field."%' ";
                                                        endif;
                                                endif;
                                        endif;
                                endforeach;
                        endif;

                        $sql = "SELECT DISTINCT ";
                                if ( $count == 1 ) :
                        $sql    .= "count(*) ";
                                else :
                        $sql    .= "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name, "
                                . "c.date_add AS date_add, "
                                . "c.id_category AS id_product, "
                                . "c.id_category AS image_url, "
                                . "c.id_category AS id_category_default, "
                                . "c.id_category AS reference, "
                                . "c.id_category AS price, "
                                . "c.id_category AS date_upd, "
                                . "c.id_category AS product_name, "
                                . "p.profile_name AS profile_name, "
                                . "pm.is_enabled AS active ";
                                endif;
                        $sql    .= "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category) AND cl.id_shop = '".$id_shop."' AND cl.id_lang = '".$id_lang."' "
                                . "LEFT JOIN ebay_profiles_mapping pm ON(c.id_category = pm.id AND pm.id_shop = '".$id_shop."' AND pm.id_site = '".$id_site."') "
                                . "LEFT JOIN ebay_profiles p ON (pm.id_profile = p.id_profile AND p.id_shop = '".$id_shop."' AND p.id_site = '".$id_site."') "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' "
                                . "AND c.id_parent <> '0' ";

                                if ( !empty($string) ) :
                        $sql    .= "AND cl.name LIKE '%".$string['value']."%' ";
                                endif;
                                if ( !empty($sqlIdCategory) && !empty($string) ) :
                        $sql    .= $string['key']." ".$sqlIdCategory." ";
                                elseif ( !empty($sqlIdCategory) )  :
                        $sql    .= "AND ".$sqlIdCategory." ";
                                endif;
                        $sql    .= $sqlProfileName." ";
                                if ( !empty($order_name) && !empty($order_by) && $order_name == 'id_category' ) :
                        $sql    .= "ORDER BY c.id_category ".$order_by." ";
                                else :
                        $sql    .= "ORDER BY c.id_category ASC ";
                                endif;
                                if ( $limit !== 0 && $offset !== 0 ) :
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                endif;

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAttributeMappingGroup($id_category = '', $id_shop = null) {
                        if ( (!isset($id_category) && empty($id_category)) || (!isset($id_shop) && empty($id_shop)) ) {
                                return Null;
                        }
                        
                        $sql = "SELECT DISTINCT "
                                . "l.id_attribute_group as id_attribute_group, "
                                . "l.name as name "
                                . "FROM ::PREFIX::_product_category c "
                                . "LEFT JOIN ::PREFIX::_product p ON c.id_category = p.id_category_default "
                                . "LEFT JOIN ::PREFIX::_product_attribute a ON p.id_product = a.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute_combination ac ON a.id_product_attribute = ac.id_product_attribute "
                                . "LEFT JOIN ::PREFIX::_attribute_group_lang l ON ac.id_attribute_group = l.id_attribute_group "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND c.id_category = '".$id_category."' "
                                . "AND p.has_attribute > 0 "
                                . "AND c.id_product = p.id_product";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                public function getAllAttributeNameGroup($user = '', $id_site = null, $id_shop = null, $id_lang = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return Null;
                        endif;
                        
                        $sql = "SELECT DISTINCT "
                                . "ac.id_attribute_group AS id_attribute_group, "
                                . "l.`name` AS name, "
                                . "s.name_attribute_ebay AS attribute_name, "
                                . "s.`mode` AS mode "
                                . "FROM ::PREFIX::_product_category c "
                                . "LEFT JOIN ::PREFIX::_product p ON c.id_product = p.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute a ON p.id_product = a.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute_combination ac ON a.id_product_attribute = ac.id_product_attribute "
                                . "LEFT JOIN ::PREFIX::_attribute_group_lang l ON ac.id_attribute_group = l.id_attribute_group "
                                . "LEFT JOIN ebay_mapping_attribute_selected s ON ac.id_attribute_group = s.id_attribute_group "
                                . "AND s.id_profile = '".$id_profile."' AND s.id_site = '".$id_site."' AND s.id_shop = '".$id_shop."' AND s.id_lang = '".$id_lang."' "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND p.has_attribute > 0 "
                                . "AND l.id_lang = '".$id_lang."' "
                                . "GROUP BY l.name";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                public function getAllAttributeValueGroup($user = '', $id_site = null, $id_shop = null, $id_lang = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return Null;
                        endif;
                        
                        $sql = "SELECT DISTINCT "
                                . "s2.id_attribute_group AS id_attribute_group, "
                                . "s2.id_attribute AS id_attribute, "
                                . "s2.name AS name, "
                                . "v.name_attribute_ebay AS attribute_value, "
                                . "v.mode AS mode "
                                . "FROM ( "
                                . "SELECT DISTINCT "
                                . "ac.id_attribute_group AS id_attribute_group, "
                                . "l.name AS name "
                                . "FROM ::PREFIX::_product_category c "
                                . "LEFT JOIN ::PREFIX::_product p ON c.id_product = p.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute a ON p.id_product = a.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute_combination ac ON a.id_product_attribute = ac.id_product_attribute "
                                . "LEFT JOIN ::PREFIX::_attribute_group_lang l ON ac.id_attribute_group = l.id_attribute_group "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND p.has_attribute > 0 "
                                . "AND l.id_lang = '".$id_lang."' "
                                . "GROUP BY l.name"
                                . ") s1 "
                                . "LEFT JOIN ::PREFIX::_attribute_lang s2 ON s1.id_attribute_group = s2.id_attribute_group "
                                . "LEFT JOIN ebay_mapping_attribute_value v ON s2.id_attribute_group = v.id_attribute_group AND s2.id_attribute = v.id_attribute "
                                . "AND v.id_profile = '".$id_profile."' AND v.id_site = '".$id_site."' AND v.id_shop = '".$id_shop."' AND v.id_lang = '".$id_lang."' "
                                . "WHERE s2.id_shop = '".$id_shop."' "
                                . "AND s2.id_lang = '".$id_lang."' "
                                . "ORDER BY s2.id_attribute_group ASC";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                public function getAttributeGroupLang($user = '', $id_shop = null, $id_lang = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ) :
                                return Null;
                        endif;
                        
                        $sql = "SELECT "
                                . "id_attribute_group, "
                                . "name "
                                . "FROM ::PREFIX::_attribute_group_lang "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_lang > '".$id_lang."' "
                                . "ORDER BY name ASC";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAttributeValueMappingGroup($id_site = null, $id_shop = null, $id_lang = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT DISTINCT "
                                . "s.id_attribute_group as id_attribute_group, "
                                . "s.name_attribute as name_attribute, "
                                . "s.name_attribute_ebay as name_attribute_ebay, "
                                . "v.name_attribute as value_mapping_name, "
                                . "v.name_attribute_ebay as value_name_ebay, "
                                . "l.id_attribute as id_attribute, "
                                . "l.name as value_name "
                                . "FROM ebay_mapping_attribute_selected s "
                                . "LEFT JOIN ::PREFIX::_attribute_lang l ON s.id_attribute_group = l.id_attribute_group "
                                . "LEFT JOIN ebay_mapping_attribute_value v ON s.id_attribute_group = v.id_attribute_group AND v.id_attribute = l.id_attribute "
                                . "WHERE s.id_shop = '".$id_shop."' "
                                . "AND s.id_site = '".$id_site."' "
                                . "AND s.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function errorInsertProduct($id_site = null, $id_shop = null, $sku = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "p.id_product AS id_product "
                                . "FROM "
                                . "::PREFIX::_product AS p "
                                . "LEFT JOIN ::PREFIX::_product_attribute AS a ON p.id_product = a.id_product "
                                . "LEFT JOIN ::PREFIX::_product_lang AS l "
                                . "WHERE "
                                . "(p.sku = '".$sku."' "
                                . "OR p.ean13 = '".$sku."' "
                                . "OR p.upc = '".$sku."' "
                                . "OR p.reference = '".$sku."') "
                                . "OR (a.sku = '".$sku."' "
                                . "OR a.ean13 = '".$sku."' "
                                . "OR a.upc = '".$sku."' "
                                . "OR a.reference = '".$sku."')"
                                . "AND s.id_shop = '".$id_shop."' "
                                . "AND s.id_site = '".$id_site."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getLastBatchID($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT * "
                                . "FROM "
                                . "ebay_statistics_log "
                                . "WHERE "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getIDItemByLast($user = null, $id_site = null, $id_shop = null, $ebay_user = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($ebay_user) && empty($ebay_user)) ){
                                return Null;
                        }

                        $sql = "SELECT
                                id_item,
                                id_product,
                                id_site,
                                id_shop,
                                SKU,
                                ebay_user,
                                name_product,
                                date_add,
                                date_end,
                                is_enabled
                                FROM
                                ebay_product_item_id
                                WHERE
                                id_shop = '".$id_shop."'
                                AND id_site = '".$id_site."'
                                AND ebay_user = '".$ebay_user."'
                                AND id_item IN (SELECT
                                        max(id_item) AS id_item
                                FROM
                                        ebay_product_item_id
                                WHERE
                                        id_shop = '".$id_shop."'
                                AND id_site = '".$id_site."'
                                AND ebay_user = '".$ebay_user."'
                                AND is_enabled = '1'
                                GROUP BY SKU)";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getUnivers($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) {
                                return Null;
                        }
                        
                        $sql = "SELECT "
                                . "id_ebay_category, "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_univers "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAttributeUnivers($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) {
                                return Null;
                        }
                        
                        $sql = "SELECT "
                                . "id_group, "
                                . "name "
                                . "FROM ebay_attribute_univers "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getTaxsLang($id_shop = null, $id_lang = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "id_tax, "
                                . "name "
                                . "FROM ::PREFIX::_tax_lang "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_lang = '".$id_lang."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getTaxs($id_shop = null, $id_tax = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_tax) && empty($id_tax)) ) {
                                return Null;
                        }

                        $sql = "SELECT "
                                . "rate, "
                                . "type "
                                . "FROM ::PREFIX::_tax "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_tax = '".$id_tax."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getTaxsSelected($id_site = null, $id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT * "
                                . "FROM ebay_tax "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getConditionMap($id_site = null, $id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT 
                                c.id_profile AS id_profile,
                                'mapping' AS table_key,
                                c.condition_data AS condition_data,
                                c.condition_value AS condition_value,
                                d.condition_description AS condition_description
                                FROM ebay_mapping_condition c
                                LEFT JOIN ebay_mapping_condition_description d ON 
                                (
                                        (c.id_profile = d.id_profile)
                                        AND (
                                                c.id_site = d.id_site
                                        )
                                        AND (
                                                c.id_shop = d.id_shop
                                        )
                                        AND (
                                                c.condition_data = d.condition_data
                                        )
                                )
                                WHERE c.id_site = '".$id_site."'
                                AND c.id_shop = '".$id_shop."'
                                AND c.is_enabled = '1'
                                UNION
                                SELECT 
                                id_profile,
                                'specific' AS table_key,
                                NULL AS condition_data,
                                condition_value,
                                condition_description
                                FROM
                                ebay_profiles_condition
                                WHERE 
                                id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND is_enabled = '1'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getRealCarriers($id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) ) {
                                return Null;
                        }
                        
                        $sql = "SELECT "
                                . "c.id_carrier AS id_carrier, "
                                . "c.is_default AS is_default, "
                                . "c.name AS name "
                                . "FROM "
                                . "::PREFIX::_carrier c "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "GROUP BY c.id_carrier "
                                . "ORDER BY c.id_carrier";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDefaultCarrier($id_site = null, $id_shop = null, $id_profile = 0) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return Null;
                        }
                        
                        $sql = "SELECT "
                                . "c.id_ebay_shipping AS id_ebay_shipping, "
                                . "c.name_ebay_shipping AS name_ebay_shipping, "
                                . "c.name_ebay_shipping_service AS name_ebay_shipping_service, "
                                . "c.is_enabled AS is_enabled "
                                . "FROM "
                                . "ebay_mapping_carrier_default c "
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND c.id_site = '".$id_site."' "
                                . "AND c.id_profile = '".$id_profile."' ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getRealCondition($user = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "c.name AS txt "
                                . "FROM "
                                . "::PREFIX::_product p "
                                . "LEFT JOIN ::PREFIX::_conditions c ON p.id_condition = c.id_condition "
                                . "WHERE p.id_shop = '".$id_shop."' "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "GROUP BY p.id_condition "
                                . "ORDER BY c.id_condition ASC ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getAllCarriers() {                        
                        $sql = "SELECT * "
                                . "FROM ::PREFIX::_carrier";
                        
                        $prepared = array();
                        $result = $this->sqlQuerySwap($sql, 'products');
                        foreach ($result as $ele){
                        	$prepared[$ele['id_shop']][$ele['id_carrier']] = $ele;
                        }
                        
                        return $prepared;
                }
                
                public function getUserExport($id_site = null, $id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return Null;
                        }

                        $sql = "SELECT DISTINCT "
                                . "ebay_user "
                                . "FROM "
                                . "ebay_product_item_id "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getExportProduct($id_site = null, $id_shop = null, $ebay_user = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($ebay_user) && empty($ebay_user)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "id_product, "
                                . "id_item, "
                                . "id_site, "
                                . "id_shop, "
                                . "name_product, "
                                . "date_add, "
                                . "SKU "
                                . "FROM "
                                . "ebay_product_item_id "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND ebay_user = '".$ebay_user."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findProductFromSKU($id_shop = null, $id_lang = null, $sku = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($sku) && empty($sku)) ){
                                return Null;
                        }

                        $sql = "SELECT "
                                . "m.id_product AS id_product, "
                                . "m.name AS name, "
                                . "n.quantity AS quantity, "
                                . "n.price AS price, "
                                . "n.id_currency AS id_currency, "
                                . "n.id_condition AS id_condition, "
                                . "n.id_tax AS id_tax, "
                                . "n.reference AS reference "
                                . "FROM "
                                . "::PREFIX::_product_lang m "
                                . "LEFT JOIN ::PREFIX::_product n ON (m.id_product = n.id_product) "
                                . "WHERE "
                                . "( "
                                . "n.sku = '".$sku."' "
                                . "OR n.ean13 = '".$sku."' "
                                . "OR n.upc = '".$sku."' "
                                . "OR n.reference = '".$sku."' "
                                . ") "
                                . "AND m.id_shop = '".$id_shop."' "
                                . "AND m.id_lang = '".$id_lang."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findProductAttibuteFromSKU($id_shop = null, $id_product = null, $sku = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_product) && empty($id_product)) || (!isset($sku) && empty($sku)) ) {
                                return Null;
                        }

                        $sql = "SELECT "
                                . "price, "
                                . "quantity, "
                                . "id_product_attribute, "
                                . "reference "
                                . "FROM "
                                . "::PREFIX::_product_attribute "
                                . "WHERE "
                                . "id_product = '".$id_product."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND ( "
                                . "sku = '".$sku."' "
                                . "OR ean13 = '".$sku."' "
                                . "OR upc = '".$sku."' "
                                . "OR reference = '".$sku."' )";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findReturnCarrierFromDefault($id_shop = null, $id_site = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return Null;
                        }
                        
                        $count = $this->_connector->db_table_exists('ebay_mapping_carrier_return_default', true);

                        if ( $count) {
                            $sql = "SELECT id_shipping as id_carrier
                                    FROM 
                                    ebay_mapping_carrier_return_default
                                    WHERE id_shop = '".$id_shop."'
                                    AND id_site = '".$id_site."'
                                    AND is_enabled = '1';";

                            return $this->sqlQuerySwap($sql, 'products');
                        }
                        
                        return false;
                }
                
                public function findProductCarrier($id_shop = null, $id_service = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_service) && empty($id_service)) ){
                                return Null;
                        }

                        $sql = "SELECT CASE
                                WHEN id_carrier IS NOT NULL THEN id_carrier
                                ELSE (select id_carrier from ::PREFIX::_carrier where is_default = 1 AND id_shop = '".$id_shop."')
                                END id_carrier
                                FROM 
                                ::PREFIX::_carrier  
                                WHERE  id_shop = '".$id_shop."'
                                AND id_carrier in (
                                SELECT id_shipping FROM ebay_mapping_carrier_domestic WHERE name_ebay_shipping_service = '".$id_service."' AND id_shop = '".$id_shop."')
                                OR id_carrier in (
                                SELECT id_shipping FROM ebay_mapping_carrier_international WHERE name_ebay_shipping_service = '".$id_service."' AND id_shop = '".$id_shop."')
                                GROUP BY id_carrier";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findProductCarrierFromIDProduct($id_shop = null, $id_product = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_product) && empty($id_product)) ){
                                return Null;
                        }

                        $sql = "SELECT id_carrier 
                                FROM ::PREFIX::_product_carrier 
                                WHERE id_carrier = (SELECT max(id_carrier) FROM ::PREFIX::_product_carrier WHERE id_shop = '".$id_shop."' AND id_product = '".$id_product."') 
                                AND id_shop = '".$id_shop."' 
                                AND id_product = '".$id_product."' 
                                GROUP BY id_carrier";
                        

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findCarrierFromDB($id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }

                        $sql = "SELECT id_carrier 
                                FROM ::PREFIX::_carrier 
                                WHERE id_carrier = (SELECT max(id_carrier) FROM ::PREFIX::_carrier WHERE id_shop = '".$id_shop."') 
                                AND id_shop = '".$id_shop."' 
                                GROUP BY id_carrier";
                        

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function findProductAttributeFromSKU($id_shop = null, $id_product = null, $sku = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) || (!isset($id_product) && empty($id_product)) || (!isset($sku) && empty($sku)) ){
                                return Null;
                        }

                        $sql = "SELECT DISTINCT "
                                . "o.id_attribute AS id_attribute, "
                                . "o.id_attribute_group AS id_attribute_group "
                                . "FROM "
                                . "::PREFIX::_product m "
                                . "LEFT JOIN ::PREFIX::_product_attribute n ON m.id_product = n.id_product "
                                . "LEFT JOIN ::PREFIX::_product_attribute_combination o ON n.id_product_attribute = o.id_product_attribute "
                                . "WHERE "
                                . "m.id_product = '".$id_product."' "
                                . "AND m.id_shop = '".$id_shop."' "
                                . "AND n.id_shop = '".$id_shop."' "
                                . "AND o.id_shop = '".$id_shop."' "
                                . "AND ( "
                                . "n.sku = '".$sku."' "
                                . "OR n.ean13 = '".$sku."' "
                                . "OR n.upc = '".$sku."' "
                                . "OR n.reference = '".$sku."' )";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingCategories($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT DISTINCT * "
                                . "FROM ebay_mapping_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getMappingCategory($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ){
                                return Null;
                        }
                        
                        $sql = "SELECT "
                                . "id_category, "
                                . "id_ebay_category, "
                                . "name_category, "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop ='".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingSecondaryCategories($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "id_category, "
                                . "id_ebay_category, "
                                . "name_category, "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_secondary_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingStoreCategories($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "id_category, "
                                . "id_ebay_category, "
                                . "name_category, "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingSecondaryStoreCategories($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "id_category, "
                                . "id_ebay_category, "
                                . "name_category, "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_secondary_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getRootStoreCategory($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }                        
                        $sql = "SELECT * "
                                . "FROM ebay_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND parent = '' "
                                . "AND id_category <> '1'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getLeafStoreCategory($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }                        
                        $sql = "SELECT id_category AS id,"
                                . "name_category AS name, "
                                . "id_site "
                                . "FROM ebay_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_category <> '1' "
                                . "AND id_category NOT IN ( "
                                . "SELECT parent "
                                . "FROM ebay_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."') ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getCategoryChildDefault($user = null, $id_parent = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_parent) && empty($id_parent)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "id_parent, "
                                . "id_category "
                                . "FROM ::PREFIX::_category "
                                . "WHERE id_category = '".$id_parent."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getChildStoreCategory($user = null, $id_site = null, $id_shop = null, $id_category) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_category) && empty($id_category)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT DISTINCT * "
                                . "FROM ebay_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'"
                                . "AND parent = '".$id_category."'";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedStoreCategory($user = null, $id_site = null, $id_shop = null, $id_category) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_category) && empty($id_category)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT DISTINCT * "
                                . "FROM ebay_store_category "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'"
                                . "AND id_category = '".$id_category."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingTemplates($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT DISTINCT "
                                . "id_category, "
                                . "template_selected, "
                                . "date_add "
                                . "FROM ebay_mapping_templates "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingAttributesGroup($user = null, $id_site = null, $id_shop = null, $id_lang = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_lang) && empty($id_lang)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT DISTINCT
                                m.id_profile AS id_profile,
                                'mapping' AS table_key,
                                m.id_attribute_group As id_attribute_group, 
                                m.name_attribute As attribute_name, 
                                m.name_attribute_ebay As ebay_attribute_name, 
                                n.name_attribute As attribute_value, 
                                n.name_attribute_ebay As ebay_attribute_value, 
                                n.id_attribute As id_attribute
                                FROM
                                        ebay_mapping_attribute_selected m
                                LEFT JOIN ebay_mapping_attribute_value n ON 
                                (
                                        (
                                                m.id_attribute_group = n.id_attribute_group AND
                                                m.id_site	= n.id_site AND
                                                m.id_shop	= n.id_shop AND
                                                m.id_lang = n.id_lang AND
                                                m.is_enabled = n.is_enabled AND 
                                                m.id_profile = n.id_profile
                                        )
                                )
                                WHERE
                                        m.id_site = '".$id_site."'
                                AND m.id_shop = '".$id_shop."'
                                AND m.id_lang = '".$id_lang."'
                                AND m.is_enabled = '1'
                                UNION
                                SELECT
                                id_profile,
                                'specific' AS table_key,
                                NULL AS id_attribute_group,
                                NULL AS attribute_name,
                                name_attribute AS ebay_attribute_name,
                                NULL AS attribute_value,
                                value_attribute AS ebay_attribute_value,
                                NULL AS id_attribute
                                FROM
                                ebay_profiles_variation
                                WHERE
                                id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND is_enabled = '1' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSelectedMappingAttributes($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ){
                                return FALSE;
                        }
                        
                        $sql = "SELECT DISTINCT "
                                . "id_attribute_group, "
                                . "name_attribute, "
                                . "name_attribute_ebay, "
                                . "mode AS id_group "
                                . "FROM ebay_mapping_attribute_selected "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getStatisticByBatchID($user = null, $id_site = null, $id_shop = null, $batch_id = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($batch_id) && empty($batch_id)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT * "
                                . "FROM ebay_statistics "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND batch_id = '".$batch_id."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getStatisticByBatchIDStatus($user = null, $id_site = null, $id_shop = null, $batch_id = null, $status = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($batch_id) && empty($batch_id)) || (!isset($status) && empty($status)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT name_product,"
                                . "message,"
                                . "date_add "
                                . "FROM ebay_statistics "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND batch_id = '".$batch_id."' "
                                . "AND response = '".$status."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailTaxByItemID($user = null, $id_site = null, $id_shop = null, $id_item = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_item) && empty($id_item)) ){
                                return FALSE;
                        }

                        $sql = "SELECT tax_rate "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_item = '".$id_item."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                        
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailRateFromByItemID($user = null, $id_site = null, $id_shop = null, $id_item = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_item) && empty($id_item)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT currency_rate_from "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_item = '".$id_item."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailRateToByItemID($user = null, $id_site = null, $id_shop = null, $id_item = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_item) && empty($id_item)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT currency_rate_to "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_item = '".$id_item."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailTax($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ){
                                return FALSE;
                        }

                        $sql = "SELECT tax_rate, id_item "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";

                        return $this->sqlQuerySwap($sql, 'products');
                        
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailRateFrom($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT currency_rate_from, id_item "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getDetailRateTo($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT currency_rate_to, id_item "
                                . "FROM ebay_product_details "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "ORDER BY date_add DESC "
                                . "LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getTaxIDDetails($user = null, $id_shop = null, $id_lang = null, $rate = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($rate) && empty($rate)) || (!isset($rate['tax_rate']) && empty($rate['tax_rate'])) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "l.id_tax As id_tax "
                                . "FROM "
                                . "::PREFIX::_tax x "
                                . "LEFT JOIN ::PREFIX::_tax_lang l ON x.id_tax = l.id_tax AND x.id_shop = l.id_shop "
                                . "WHERE  "
                                . "x.rate = '".$rate."' "
                                . "AND x.id_shop = '".$id_shop."' "
                                . "AND l.id_shop = '".$id_shop."' "
                                . "AND l.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getTaxNameDetails($user = null, $id_shop = null, $id_lang = null, $rate = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($rate) && empty($rate)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "l.name As name "
                                . "FROM "
                                . "::PREFIX::_tax x "
                                . "LEFT JOIN ::PREFIX::_tax_lang l ON x.id_tax = l.id_tax AND x.id_shop = l.id_shop "
                                . "WHERE  "
                                . "x.rate = '".$rate."' "
                                . "AND x.id_shop = '".$id_shop."' "
                                . "AND l.id_shop = '".$id_shop."' "
                                . "AND l.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getShippingRules($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "id_profile, "
                                . "id_rule, "
                                . "id_shipping, "
                                . "id_ebay_shipping, "
                                . "is_international, "
                                . "country_service, "
                                . "shipping_cost, "
                                . "shipping_additionals, "
                                . "min_weight, "
                                . "max_weight, "
                                . "min_price, "
                                . "max_price, "
                                . "surcharge, "
                                . "rel, "
                                . "operations, "
                                . "is_enabled "
                                . "FROM "
                                . "ebay_mapping_carrier_rules "
                                . "WHERE  "
                                . "id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND id_profile = '".$id_profile."' ";

                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfile($user = null) {
                        if ( (!isset($user) && empty($user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT id_profile "
                                . "FROM ebay_profiles "
                                . "ORDER BY id_profile DESC "
                                . "LIMIT 0, 1";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileCompleted($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT id_profile, profile_name "
                                . "FROM ebay_profiles "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' "
                                . "AND is_enabled = '1' "
                                . "ORDER BY id_profile ASC ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileType($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "g.type As type "
                                . "FROM "
                                . "ebay_profiles_group g "
                                . "WHERE "
                                . "g.id_profile = '".$id_profile."'";

                        $result = $this->sqlQuerySwap($sql, 'products');
                        if ( !empty($result) ) :
                                return current($result[0]);
                        endif;
                                
                       	return 'default';
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileConfiguration($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT * "
                                . "FROM "
                                . "ebay_profiles_details "
                                . "WHERE "
                                . "id_profile = '".$id_profile."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."'";
                        
                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileDetails($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "d.id_profile As id_profile, "
                                . "d.out_of_stock_min As out_of_stock_min, "
                                . "d.out_of_stock_created As out_of_stock_created, "
                                . "d.maximum_quantity As maximum_quantity, "
                                . "d.track_inventory As track_inventory, "
                                . "d.ebay_category_name As ebay_category_name, "
                                . "d.id_ebay_category As id_ebay_category, "
                                . "d.secondary_category_name As secondary_category_name, "
                                . "d.id_secondary_category As id_secondary_category, "
                                . "d.store_category_name As store_category_name, "
                                . "d.id_store_category As id_store_category, "
                                . "d.secondary_store_category_name As secondary_store_category_name, "
                                . "d.id_secondary_store_category As id_secondary_store_category, "
                                . "d.country_name As country_name, "
                                . "d.currency As currency, "
                                . "d.title_format As title_format, "
                                . "d.auto_pay As auto_pay, "
                                . "d.gallery_plus As gallery_plus, "
                                . "d.listing_duration As listing_duration, "
                                . "d.visitor_counter As visitor_counter ";
                                if ( $id_profile != 0 ) :
                        $sql    .= ", p.profile_name As profile_name, "
                                . "p.is_enabled As is_enabled, "
                                . "p.date_add As date_add, "
                                . "p.date_edit As date_edit, "
                                . "p.completed As completed, "
                                . "p.ebay_user As ebay_user ";
                                endif;
                        $sql    .= "FROM "
                                . "ebay_profiles_details d ";
                                if ( $id_profile != 0 ) :
                        $sql    .= "LEFT JOIN ebay_profiles p ON p.id_profile = d.id_profile ";
                                endif;
                        $sql    .= "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfilePayment($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "d.id_profile As id_profile, "
                                . "d.payment_method As payment_method, "
                                . "d.payment_instructions As payment_instructions "
                                . "FROM "
                                . "ebay_profiles_payment d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";


                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileReturn($user = null, $id_site = null, $id_shop = null, $id_profile = 'all') {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "d.id_profile As id_profile, "
                                . "d.returns_policy As returns_policy, "
                                . "d.returns_within As returns_within, "
                                . "d.returns_pays As returns_pays, "
                                . "d.returns_information As returns_information, "
                                . "d.holiday_return As holiday_return "
                                . "FROM "
                                . "ebay_profiles_return d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' ";
                                if ( $id_profile != 'all' || $id_profile == '0' ) : 
                        $sql   .= "AND id_profile = '".$id_profile."' ";
                                endif;
                        $sql   .= "AND d.id_site = '".$id_site."' ";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayAllReturn($user = null, $id_site = null, $id_shop = null, $id_profile = 'all') {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "d.id_profile As id_profile, "
                                . "d.returns_policy As returns_policy, "
                                . "d.returns_within As returns_within, "
                                . "d.returns_pays As returns_pays, "
                                . "d.returns_information As returns_information, "
                                . "d.holiday_return As holiday_return "
                                . "FROM "
                                . "ebay_profiles_return d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' ";
                                if ( $id_profile != 'all' || $id_profile == '0' ) : 
                        $sql   .= "AND id_profile = '".$id_profile."' ";
                                endif;
                        $sql   .= "AND d.id_site = '".$id_site."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileDescription($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT "
                                . "d.description_selected As description_selected, "
                                . "d.html_description As html_description "
                                . "FROM "
                                . "ebay_profiles_description d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";


                        return $this->sqlQuerySwap($sql, 'products', true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileCondition($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "d.condition_value As condition_value, "
                                . "d.condition_name As condition_name, "
                                . "d.condition_description As condition_description, "
                                . "d.is_enabled As is_enabled "
                                . "FROM "
                                . "ebay_profiles_condition d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";
                        
                        return $this->sqlQuerySwap($sql, 'products', true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getIDCategoriesMapping($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT "
                                . "id_ebay_category "
                                . "FROM "
                                . "ebay_mapping_category "
                                . "WHERE "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "GROUP BY id_ebay_category ";
                        

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getCategoriesMappingByID($user = null, $id_site = null, $id_shop = null, $id = null) {
                        if ( empty($user) || (empty($id) && !isset($id)) || (empty($id_site) && !isset($id_site)) || (empty($id_shop) && !isset($id_shop)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "name_ebay_category "
                                . "FROM ebay_mapping_category "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND id_ebay_category = '".$id."' "
                                . "ORDER BY name_ebay_category DESC LIMIT 1 ";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileVariation($user = null, $id_site = null, $id_shop = null, $id_profile = null, $within = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) || (!isset($within) && empty($within)) ) :
                                return FALSE;
                        endif;
                        $where                              = '';
                        if ( !empty($within) ) :
                                foreach ($within as $forin ) :
                                        if ( empty($where) ) :
                                                $where              .= "AND (d.name_attribute = '".htmlspecialchars($forin['name'])."' ";
                                        else :
                                                $where              .= "OR d.name_attribute = '".htmlspecialchars($forin['name'])."' ";
                                        endif;
                                endforeach;
                        endif;
                        if ( !empty($where) ) :
                                $where                              .= ') ';
                        endif;

                        $sql = "SELECT "
                                . "d.id_group As id_group, "
                                . "d.name_attribute As name_attribute, "
                                . "d.value_attribute As value_attribute, "
                                . "d.mode As mode, "
                                . "d.is_enabled As is_enabled "
                                . "FROM "
                                . "ebay_profiles_variation d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . $where. " "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileSpecific($user = null, $id_site = null, $id_shop = null, $id_profile = null, $within = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) || (!isset($within) && empty($within)) ) :
                                return FALSE;
                        endif;

                        $where                              = '';
                        foreach ($within as $forin ) :
                                if ( empty($where) ) :
                                        $where              .= "AND (d.name_attribute = '".htmlspecialchars($forin['name'])."' ";
                                else :
                                        $where              .= "OR d.name_attribute = '".htmlspecialchars($forin['name'])."' ";
                                endif;
                        endforeach;
                        $where                              .= ') ';
                        
                        $sql = "SELECT "
                                . "d.id_group As id_group, "
                                . "d.name_attribute As name_attribute, "
                                . "d.value_attribute As value_attribute, "
                                . "d.mode As mode, "
                                . "d.is_enabled As is_enabled "
                                . "FROM "
                                . "ebay_profiles_specific d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . $where. " "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";

                        
                        return $this->sqlQuerySwap($sql, 'products');
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayProfileShipping($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT "
                                . "d.id_ebay_shipping As id_ebay_shipping, "
                                . "d.name_ebay_shipping_service As name_ebay_shipping_service, "
                                . "d.name_ebay_shipping As name_ebay_shipping, "
                                . "d.country_service As country_service, "
                                . "d.name_ebay_country As name_ebay_country, "
                                . "d.shipping_cost As shipping_cost, "
                                . "d.shipping_additionals As shipping_additionals, "
                                . "d.postcode As postal_code, "
                                . "d.dispatch_time As dispatch_time, "
                                . "d.is_enabled As is_enabled "
                                . "FROM "
                                . "ebay_profiles_shipping d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getProfileNameValid($user = null, $profile_name = null, $id_profile = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($profile_name) && empty($profile_name)) || (!isset($id_profile) && empty($id_profile)) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "count(profile_name) "
                                . "FROM "
                                . "ebay_profiles "
                                . "WHERE "
                                . "profile_name = '".$profile_name."' "
                                . "AND id_profile <> '".$id_profile."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND id_shop = '".$id_shop."' ";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getProfileForExport($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT 
                                p.id_profile AS id_profile,
                                m.id AS id,
                                g.type AS type
                                FROM
                                ebay_profiles p
                                LEFT JOIN ebay_profiles_group g ON (p.id_profile = g.id_profile AND p.id_site = '".$id_site."' AND p.id_shop = '".$id_shop."')
                                LEFT JOIN ebay_profiles_mapping m ON (p.id_profile = m.id_profile AND p.id_site = m.id_site AND p.id_shop = m.id_shop AND m.type = g.type)
                                WHERE p.is_enabled = '1'
                                AND p.completed = 100
                                AND m.is_enabled = '1'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getPriceModifier($user = null, $id_site = null, $id_shop = null, $id_profile = 'all') {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT * "
                                . "FROM "
                                . "ebay_price_modifier "
                                . "WHERE  "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' ";
                                if ( $id_profile != 'all' || $id_profile == '0' ) : 
                        $sql   .= "AND id_profile = '".$id_profile."' ";
                                endif;
                        $sql   .= "ORDER BY rel ASC";
                        
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getPriceModifierRelMax($user = null, $id_site = null, $id_shop = null, $id_profile = 'all') {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT Max(rel) "
                                . "FROM "
                                . "ebay_price_modifier "
                                . "WHERE  "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' ";
                                if ( $id_profile != 'all' || $id_profile == '0' ) : 
                        $sql   .= "AND id_profile = '".$id_profile."' ";
                                endif;
                        $sql   .= "GROUP BY id_site "
                                . "Having Max(rel) > 0 ";
                        $sql   .= "ORDER BY rel ASC";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getRoundingModifier($user = null, $id_site = null, $id_shop = null, $id_profile = 0) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT decimal_value AS `decimal` "
                                . "FROM "
                                . "ebay_price_modifier "
                                . "WHERE  "
                                . "id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' "
                                . "AND id_profile = '".$id_profile."' "
                                . "ORDER BY rel ASC Limit 1";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getExportProductOption($id_site = null, $id_shop = null, $id_lang = 0) {
                        if ( (!isset($id_lang) && empty($id_lang)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }
                        
                        $count = $this->_connector->db_table_exists('ebay_product_option', true);
                        
                        if ( $count ) {                   
                                $sql = "SELECT 
                                        `id_product`,
                                        `id_product_attribute`,
                                        `sku`,
                                        `c_force` as `force`,
                                        `c_disable` as `disable`,
                                        `price`,
                                        `shipping`
                                        FROM
                                        ebay_product_option
                                        WHERE
                                        id_shop = '".$id_shop."'
                                        AND id_country = '".$id_site."'";

                                $result = $this->sqlQuerySwap($sql, 'products');
                                $count = $this->_connector->db_table_exists('marketplace_product_option', true);
                                if ( empty($result) && $count ) :
                                        $sql = "SELECT 
                                        `id_product`,
                                        `id_product_attribute`,
                                        `sku`,
                                        `c_force` as `force`,
                                        `c_disable` as `disable`,
                                        `price`,
                                        `shipping`
                                        FROM
                                        marketplace_product_option
                                        WHERE
                                        id_shop = '".$id_shop."'";
                                        return $this->sqlQuerySwap($sql, 'products');
                                endif;
                                return $result;
                        }
                        return NULL;
                }
                
                public function getCategoryMaxDate($user = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "max( date_add ) as date  "
                                . "FROM ::PREFIX::_category_selected "
                                . "WHERE id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'offers', true);
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getStoreCategoryMaxDate($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "max( date_add ) as date  "
                                . "FROM ebay_store_category "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND id_site = '".$id_site."' ";

                        return $this->sqlQuerySwap($sql, 'products', true);
                }
                
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayMappingCondition($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) {
                                return FALSE;
                        }

                        $sql = "SELECT "
                                . "d.condition_data As condition_data, "
                                . "d.condition_value As condition_value, "
                                . "d.condition_name As condition_name, "
                                . "d.is_enabled As is_enabled "
                                . "FROM "
                                . "ebay_mapping_condition d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getEbayMappingConditionDescription($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT "
                                . "d.condition_data As condition_data, "
                                . "d.condition_description As condition_description "
                                . "FROM "
                                . "ebay_mapping_condition_description d "
                                . "WHERE "
                                . "d.id_shop = '".$id_shop."' "
                                . "AND d.id_site = '".$id_site."' "
                                . "AND d.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getJobStatus($user = null, $ebay_user = null, $date = 'now') {
                        if ( (!isset($user) && empty($user)) || (!isset($ebay_user) && empty($ebay_user)) ) {
                                return FALSE;
                        }
                        
                        $sql = "SELECT * "
                                . "FROM ebay_job_task "
                                . "WHERE date(creationTime) = '".date("Y-m-d", strtotime($date))."' "
                                . "AND (jobStatus = 'InProcess' "
                                . "OR jobStatus = 'Scheduled' "
                                . "OR jobStatus = 'Created')  "
                                . "AND ebay_user = '".$ebay_user."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function getSpacificWithout($user = null, $id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;

                        
                        $sql = "SELECT DISTINCT "
                                . "name_attribute_ebay AS value_attribute "
                                . "FROM  "
                                . "ebay_mapping_attribute_selected s "
                                . "WHERE s.is_enabled = '1' "
                                . "AND s.mode <> -1 "
                                . "AND s.id_site = '".$id_site."' "
                                . "AND s.id_shop = '".$id_shop."' "
                                . "AND s.id_profile = '".$id_profile."' "
                                . "UNION "
                                . "SELECT DISTINCT "
                                . "name_attribute "
                                . "FROM  "
                                . "ebay_profiles_variation v "
                                . "WHERE  "
                                . "v.is_enabled = '1' "
                                . "AND v.id_profile = '".$id_profile."' ";

                        return $this->sqlQuerySwap($sql, 'products');
               }
                
                public function shipOrder($username, $orders){
                	$sql_list = array();
                	foreach($orders as $order_element){
                		$sql_list[] = "UPDATE orders_orders SET \"shipping_date\" = '".$order_element['ShippingDate']."' WHERE \"id_orders\" IN (SELECT \"id_orders\" FROM orders_order_shipping WHERE \"id_orders\" = '".$order_element['MPOrderID']."' AND \"tracking_number\" = '');";
                		$sql_list[] = "UPDATE orders_order_shipping SET \"tracking_number\" = '".$order_element['ShippingNumber']."' WHERE \"id_orders\" = '".$order_element['MPOrderID']."';";
                	}
                	
                	$sql = implode(' ', $sql_list);
                 	$db = $this->_connector;
	                if ( $_db->db_status && !empty($sql) ) {
                            $_db->db_exec($sql);
                            return TRUE;
                    }
                    return FALSE;
                }
                
                public function insertEbayStatistic($username, $logs){
                	$ebay_log = '';
                	foreach($logs as $log){
                		$ebay_log .= $this->mappingTableOnly("ebay_statistics", $log);
                	}
                	
                	if($ebay_log)
                		$this->transaction($ebay_log);
                	return true;                	
                }
                
                public function getFeedbizRootCategory($user = null, $id_shop = null, $id_lang = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name "
                                . "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category)"
                                . "WHERE c.is_root_category = 1 "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'offers', true);
                }
                
                public function getFeedbizAllCategories($user = null, $id_shop = null, $id_lang = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name "
                                . "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category)"
                                . "WHERE c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                public function getFeedbizKeyAllCategories($id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "id_category, "
                                . "date(date_add) AS date_add "
                                . "FROM "
                                . "::PREFIX::_category "
                                . "WHERE id_shop = '".$id_shop."' "
                                . "AND date(date_add) = (SELECT Date(Max(date_add)) FROM ::PREFIX::_category WHERE id_shop = '".$id_shop."')";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getFeedbizSelectedCategories($user = null, $id_shop = null, $id_lang = null, $id_category = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($id_category) && empty($id_category)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name "
                                . "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category) "
                                . "WHERE c.is_root_category = 0 "
                                . "AND c.id_parent = '".$id_category."' "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                public function getFeedbizSelectedCategoriesUp($user = null, $id_shop = null, $id_lang = null, $id_category = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($id_category) && empty($id_category)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name "
                                . "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category) "
                                . "WHERE c.id_category = '".$id_category."' "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                public function getFeedbizSelectedCategoriesDown($user = null, $id_shop = null, $id_lang = null, $id_category = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_lang) && empty($id_lang)) || (!isset($id_category) && empty($id_category)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT "
                                . "c.id_parent AS id_parent, "
                                . "c.id_category AS id_category, "
                                . "cl.name AS category_name "
                                . "FROM  "
                                . "::PREFIX::_category c "
                                . "LEFT JOIN ::PREFIX::_category_lang cl ON (c.id_category = cl.id_category) "
                                . "WHERE c.id_parent = '".$id_category."' "
                                . "AND c.id_shop = '".$id_shop."' "
                                . "AND cl.id_shop = '".$id_shop."' "
                                . "AND cl.id_lang = '".$id_lang."' ";

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                public function getFeedbizCountProducts($user = null, $id_shop = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "count(id_product) "
                                . "FROM  "
                                . "::PREFIX::_product "
                                . "WHERE id_shop = '".$id_shop."' ";

                        $result = $this->sqlQuerySwap($sql, 'products');
                        if ( !empty($result) ) :
                                return current($result[0]);
                        endif;

                        return 0;
                }
                
                public function getFeedbizCountSynchronize($user = null, $id_site = null, $ebay_user = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) || (!isset($ebay_user) && empty($ebay_user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "id_item "
                                . "FROM  "
                                . "ebay_synchronization_store "
                                . "WHERE id_site = '".$id_site."' "
                                . "AND ebay_user = '".$ebay_user."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getFeedbizCountExportID($user = null, $id_site = null, $id_shop = null, $include = null, $ebay_user = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($ebay_user) && empty($ebay_user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT "
                                . "count(id_product) "
                                . "FROM  "
                                . "ebay_product_item_id "
                                . "WHERE "
                                . "id_shop = '".$id_shop."' "
                                . "AND ebay_user = '".$ebay_user."' ";
                                if ( !empty($include) ) :
                        $sql  .= "AND id_item IN (".$include.") ";
                                else :
                        $sql  .= "AND id_item IN ('<>') ";
                                endif;

                        $result = $this->sqlQuerySwap($sql, 'products');
                        if ( !empty($result) ) :
                                return current($result[0]);
                        endif;

                        return 0;
                }

                public function getEbayAvailSite($user){
                        $db = $this->_connector;
                        if ( !$db->db_table_exists('ebay_product_details', true) )
                            return array();

                        $sql = "SELECT id_site from ebay_product_details , ::PREFIX::_shop as shop where ebay_product_details.id_shop = shop.id_shop and shop.is_default = '1' and  ebay_product_details.is_enabled ='1' and  ebay_product_details.export_pass = '1' group by ebay_product_details.id_site";
                        $out=  $this->sqlQuerySwap($sql, 'products'); 
                        $ret = array();
                        if ( !empty($out) ) :
                                foreach($out as $o){
                                    $ret[] = $o['id_site'];
                                }
                        endif;
                        $db->db_close();
                        return $ret;
                }                   
                
                public function getEbayFlagUpdate($user = null, $id_site = null, $id_shop = null, $id_marketplace = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_site) && empty($id_site)) || (!isset($id_marketplace) && empty($id_marketplace)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT id_product FROM ::PREFIX::_flag_update WHERE id_product IN (SELECT id_product FROM ebay_product_item_id WHERE is_enabled = '1' AND id_shop = '".$id_shop."' AND id_site = '".$id_site."' ORDER BY id_product ASC) 
                                AND (flag IS NULL OR flag = 1) AND id_marketplace = '".$id_marketplace."' AND id_site = '".$id_site."'";

                        return $this->sqlQuerySwap($sql, 'offers');
                }
                
                
                public function getJobTask($user = null, $type = null, $ebay_user = null, $date = 'now') {
                        if ( (!isset($ebay_user) && empty($ebay_user)) || (!isset($user) && empty($user)) || (!isset($type) && empty($type)) ) :
                                return FALSE;
                        endif;
                        $sql = "SELECT * FROM ebay_job_task WHERE date(creationTime) = '".date("Y-m-d", strtotime($date))."' AND (jobStatus = 'InProcess' OR jobStatus = 'Scheduled' OR jobStatus = 'Created') AND ebay_user = '".$ebay_user."'";

                        return $this->sqlQuerySwap($sql, $type);
                }
                
                
                public function getSynchronizeStoreByParam($user = null, $id_site = null, $id_shop = null, $where = '', $limit = 10, $offset = 0, $count = "*", $order_name = '', $order_by = '', $operation = 'AND') {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                        endif;
                        $sqlSku                             = '';
                        $sqlKey                             = '';

                        if ( !empty($where) ) :
                                foreach ( $where as $key => $field ) :
                                        if ( !empty($field) || is_numeric($field) ) :
                                                if ( $key == 'input_key' ) :
                                                        if ( !empty($field['name']) && !empty($field['name']['reference_key_string']) ) :
                                                                foreach ( $field['name']['reference_key_string'] as $ref_key => $name ) :
                                                                        if ( empty($sqlKey) ) :
                                                                                $sqlKey .= " ((p.sku = '".$name."' AND s.SKU = '".$field['name']['input_key_string'][$ref_key]."' ) OR (p.reference = '".$name."' AND s.SKU = '".$field['name']['input_key_string'][$ref_key]."' )) ";
                                                                        else :
                                                                                $sqlKey .= " OR ((p.sku = '".$name."' AND s.SKU = '".$field['name']['input_key_string'][$ref_key]."' ) OR (p.reference = '".$name."' AND s.SKU = '".$field['name']['input_key_string'][$ref_key]."' )) ";
                                                                        endif;
                                                                endforeach;
                                                                if ( !empty($sqlKey) ) :
                                                                        $sqlKey = " AND (".$sqlKey.")";
                                                                endif;
                                                        endif;
                                                elseif ( $key == 'choose_reference' ) : 
                                                        $sqlSku                             = " AND s.SKU IN(SELECT sku FROM product WHERE id_shop = '".$id_shop."' AND id_product = 0) ";
                                                        if ( !empty($field['sku']) && is_array($field['sku']) && !empty($field['sku']['id_product']) ) : 
                                                                $sqlSku   = " AND s.SKU IN(SELECT sku FROM product WHERE id_shop = '".$id_shop."' AND id_product IN ('".implode("', '", $field['sku']['id_product'])."')) ";
                                                        elseif ( !empty($field['reference']) && is_array($field['reference']) && !empty($field['reference']['id_product']) ) : 
                                                                $sqlSku   = " AND s.SKU IN(SELECT reference FROM product WHERE id_shop = '".$id_shop."' AND id_product IN ('".implode("', '", $field['reference']['id_product'])."')) ";
                                                        endif;
                                                endif;
                                        endif;
                                endforeach;
                        endif;
                        
                        if ( !empty($sqlKey)  ) :
                                $sqlSku                             = '';
                        else :
                                $sqlKey                             = '';
                        endif;

                        $count = ( is_array($count) ) ? implode(',', $count) : $count;
                        
                        $sql = "SELECT ".$count." FROM (SELECT p.id_category_default AS id_category,
                                    s.name_product AS category_name,
                                    s.date_add AS date_add,
                                    p.id_product AS id_product,
                                    s.id_item AS image_url,
                                    p.id_product AS id_category_default,
                                    s.SKU AS reference,
                                    p.id_product AS price,
                                    s.date_end AS date_upd,
                                    s.name_product AS name_product,
                                    s.SKU AS profile_name,
                                    p.active AS active "
                                . "FROM
                                ebay_synchronization_store s ";
                                if ( !empty($sqlKey) ) :
                        $sql    .= " LEFT JOIN ::PREFIX::_product p ON ( s.SKU <> '' ) ";
                                else :        
                        $sql    .= " LEFT JOIN ::PREFIX::_product p ON (s.SKU = p.sku OR s.SKU = p.reference)";
                                endif;
                                
                        $sql    .= " WHERE
                                s.id_site = '".$id_site."' 
                                AND s.SKU <> '' ";
                        
                                if ( !empty($sqlSku) ) :
                        $sql    .= $sqlSku." ";
                                endif;
                                if ( !empty($sqlKey) ) :
                        $sql    .= $sqlKey." ";
                                endif;
                        $sql    .= "ORDER BY s.date_add DESC ";
                        $sql    .= ") ssi WHERE image_url NOT IN (SELECT id_item FROM ebay_product_item_id GROUP BY id_item) GROUP BY reference ";
                                if ( !empty($order_name) && !empty($order_by) ) :
                        $sql    .= "ORDER BY ".$order_name." ".$order_by." ";
                                else :
                        $sql    .= "ORDER BY id_product DESC ";
                                endif;
                                if ( $limit !== 0 && $offset !== 0 ) :
                        $sql    .= "LIMIT ".$limit." "
                                . "OFFSET ".$offset." ";
                                endif;
                                
                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getSynchronizeStoreBySKU($user = null, $id_site = null, $sku = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($sku) && empty($sku)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT *
                                FROM  
                                ebay_synchronization_store s 
                                WHERE id_site = '".$id_site."'
                                AND s.SKU = '".$sku."'
                                AND s.date_add IN (SELECT MAX(date_add) FROM ebay_synchronization_store WHERE id_site = '".$id_site."' AND SKU = '".$sku."')";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function synchronizeGetSkuByVariation($sku = null) {
                        if ( (!isset($sku) && empty($sku)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT CASE 
                                WHEN sku IS NOT NULL THEN sku
                                WHEN reference IS NOT NULL THEN reference
                                END AS sku
                                FROM ::PREFIX::_product 
                                WHERE id_product IN (
                                SELECT 
                                id_product
                                FROM 
                                ::PREFIX::_product_attribute
                                WHERE sku IN (".$sku.")
                                OR reference IN (".$sku.") 
                                OR ean13 IN (".$sku.") 
                                OR upc IN (".$sku.") 
                                GROUP BY id_product)";

                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                public function getNewSynchronizeData($user = null, $id_site = null, $sku = null, $date = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($sku) && empty($sku)) || (!isset($user) && empty($user)) || (!isset($date) && empty($date)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT date_add
                                FROM  
                                ebay_synchronization_store s 
                                WHERE id_site = '".$id_site."'
                                AND s.SKU = '".$sku."'
                                AND s.date_add > '".$date."' ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getLastOrderAdd($id_site = null, $id_shop = null, $order_status = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($order_status) && empty($order_status)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT date(max(order_date)) FROM ::PREFIX::_orders
                                WHERE site = '".$id_site."'
                                AND order_status = '".$order_status."' 
                                AND id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'orders', true, true);
                }
                
                public function getOldOrderAdd($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT date(min(order_date)) FROM ::PREFIX::_orders
                                WHERE site = '".$id_site."'
                                AND id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'orders', true, true);
                }
                
                public function getPostalcodeDispatchtime($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                        endif;
                        
                        $result = array();
                        
                        $sql = "SELECT
                                id_profile,
                                postcode,
                                dispatch_time
                                FROM
                                ebay_mapping_carrier_default
                                WHERE
                                id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                GROUP BY postcode";
                                
                        $result = $this->sqlQuerySwap($sql, 'products');
                        
                        if ( empty($result) ) :
                                $sql = "SELECT
                                        id_profile,
                                        postcode,
                                        dispatch_time
                                        FROM
                                        ebay_mapping_carrier_domestic
                                        WHERE
                                        id_site = '".$id_site."'
                                        AND id_shop = '".$id_shop."'
                                        GROUP BY postcode
                                        UNION
                                        SELECT
                                        id_profile,
                                        postcode,
                                        dispatch_time
                                        FROM
                                        ebay_mapping_carrier_international
                                        WHERE
                                        id_site = '".$id_site."'
                                        AND id_shop = '".$id_shop."'
                                        GROUP BY postcode";

                                return $this->sqlQuerySwap($sql, 'products');
                        endif;
                        
                        return $result;
                }
                
                public function getExportProfileConfiguration($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT 
                                id_profile,
                                out_of_stock_min,
                                out_of_stock_created,
                                maximum_quantity,
                                track_inventory,
                                auto_pay,
                                listing_duration,
                                title_format,
                                country_name,
                                currency,
                                gallery_plus,
                                visitor_counter
                                FROM
                                ebay_profiles_details
                                WHERE
                                id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDeleteZeroActiveConfiguration($user = null, $ebay_user = null) {
                        if ( (!isset($user) && empty($user)) || (!isset($ebay_user) && empty($ebay_user)) ) :
                                return FALSE;
                        endif;
                        
                        $sql = "SELECT  
                                i.id_product AS id_product,
                                i.id_item AS id_item,
                                i.sku AS SKU,
                                i.name_product AS name_product
                                FROM ::PREFIX::_product p
                                JOIN ebay_product_item_id i ON (p.id_product = i.id_product AND p.id_shop = i.id_shop)
                                WHERE p.active = 0
                                AND i.is_enabled = '1'
                                AND i.ebay_user = '".$ebay_user."' 
                                ORDER BY i.id_product ASC
                                ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getSynchronizationAttribute($user = null, $id_site = null ) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT 
                                id_item,
                                SKU,
                                quantity,
                                start_price,
                                variation_name,
                                variation_value
                                FROM ebay_synchronization_attribute
                                WHERE id_site = '".$id_site."' 
                                AND ebay_user = '".$user."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDeleteProductActiveConfiguration($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT 
                                id_item AS id_item,
                                sku AS SKU,
                                name_product AS name_product
                                FROM ebay_synchronization_store
                                WHERE id_site = '".$id_site."' 
                                AND ebay_user = '".$user."' 
                                AND SKU NOT IN (SELECT reference AS SKU
                                FROM ::PREFIX::_product
                                WHERE id_shop = ".$id_shop.")
                                AND SKU NOT IN (SELECT reference AS SKU
                                FROM ::PREFIX::_product_attribute
                                WHERE id_shop = ".$id_shop.")";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDeleteQuantityZeroSync($user = null, $id_site = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT 
                                id_item AS id_item,
                                sku AS SKU,
                                name_product AS name_product
                                FROM ebay_synchronization_store
                                WHERE id_site = '".$id_site."' 
                                AND ebay_user = '".$user."' 
                                AND quantity = '0'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDeleteOptionDisable($user = null, $id_site = null, $id_shop = null, $ebay_user = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($ebay_user) && empty($ebay_user)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT 
                                id_product,
                                id_item,
                                sku AS SKU,
                                date_add AS date_add,
                                name_product
                                FROM ebay_product_item_id
                                WHERE 
                                id_product IN (
                                    SELECT id_product
                                    FROM
                                    ebay_product_option
                                    WHERE
                                    c_disable = 1
                                    AND id_shop = '".$id_shop."'
                                )
                                AND is_enabled = '1'
                                AND id_site = '".$id_site."'
                                AND ebay_user = '".$ebay_user."' 
                                UNION
                                SELECT 
                                id_product,
                                id_item,
                                sku AS SKU,
                                date_add AS date_add,
                                name_product
                                FROM ebay_product_item_id
                                WHERE 
                                id_product IN (
                                    SELECT id_product
                                    FROM
                                    marketplace_product_option
                                    WHERE
                                    c_disable = 1
                                    AND id_shop = '".$id_shop."'
                                )
                                AND is_enabled = '1'
                                AND id_site = '".$id_site."'
                                AND ebay_user = '".$ebay_user."' 
                                ";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getRemoveCustoms($user = null, $id_site = null, $id_shop = null, $ebay_user = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($ebay_user) && empty($ebay_user)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT 
                                p.id_product AS id_product,
                                i.id_item AS id_item,
                                i.SKU AS SKU,
                                i.date_add AS date_add,
                                i.name_product AS name_product
                                FROM
                                (products_product p,
                                ebay_product_item_id i)
                                WHERE
                                p.id_product = i.id_product
                                AND p.id_shop = i.id_shop
                                AND i.ebay_user = '".$ebay_user."'
                                AND i.id_site = '".$id_site."'
                                AND i.id_shop = '".$id_shop."'
                                AND i.is_enabled = '1'
                                AND p.id_manufacturer = 5";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getDeleteProductOutOfStock($user = null, $id_site = null, $id_shop = null, $ebay_user = null, $batch_id = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) || (!isset($ebay_user) && empty($ebay_user)) || (!isset($batch_id) && empty($batch_id)) ) :
                                return FALSE;
                  	endif;
                        $result = array();
                        
                        $sql = "SELECT
                                id_product,
                                id_item,
                                sku AS SKU,
                                date_add AS date_add,
                                name_product
                                FROM (SELECT
                                id_product,
                                id_item,
                                sku AS SKU,
                                date_add AS date_add,
                                name_product
                                FROM
                                ebay_product_item_id
                                WHERE
                                id_site = '".$id_site."'
                                AND id_shop = '".$id_shop."'
                                AND ebay_user = '".$ebay_user."' 
                                AND is_enabled = '1'
                                AND id_product IN (
                                SELECT
                                id_product
                                FROM
                                ::PREFIX::_product
                                WHERE
                                id_shop = '".$id_shop."'
                                AND quantity = 0
                                )
                                ORDER BY
                                date_add DESC) a
                                ORDER BY date_add DESC
                                ";

                        $product_result = $this->sqlQuerySwap($sql, 'products');
                        $batch_id = !empty($batch_id) ? $batch_id : uniqid();
                        
                        if ( !empty($product_result) ) :
                                foreach ( $product_result as $key => $endItem ) :
                                        
                                        $include_id_product[]               = $endItem['id_product'];
                                        if ( !empty($endItem) && array_key_exists('id_product', $endItem) ) :
                                                $result[$endItem['id_item']] = array(
                                                        'id_product'                => $endItem['id_product'],
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        $result[$Item['id_item']] = array(
                                                                'id_product'                => $Item['id_product'],
                                                                'id_item'                   => $Item['id_item'],
                                                                'sku'                       => $Item['SKU'],
                                                                'name'                      => $Item['name_product'],
                                                                'batch_id'                  => $batch_id,
                                                        );
                                                endforeach;
                                        endif;
                                endforeach;
                        endif;
                        
                        if ( !empty($include_id_product) ) :                         
                                $sql = "SELECT id_product FROM ::PREFIX::_product WHERE id_product in ('".implode("', '", $include_id_product)."') AND quantity > 0 AND id_shop = '".$id_shop."'";
                                $offer_result = $this->sqlQuerySwap($sql, 'offers');
                                if ( !empty($offer_result) ) :
                                        foreach ( $offer_result as $key_array => $offer_item ) :
                                                unset($result[$offer_item['id_product']]);
                                        endforeach;
                                endif;
                        endif;

                        return $result;
                }
                
                function generate_ebay_product_item_id($user = null, $id_site = null, $id_shop = null) {
                  	if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                  	endif;
                	//generate
                	$sql = "REPLACE INTO ebay_product_item_id (id_product, id_item, id_site, id_shop, SKU, ebay_user, name_product, date_add, date_end, is_enabled)
                                SELECT 
                                p.id_product as id_product, 
                                sys.id_item as id_item, 
                                sys.id_site as id_site, 
                                p.id_shop as id_shop, 
                                CASE 
                                WHEN p.sku IS NOT NULL THEN p.sku
                                WHEN p.reference IS NOT NULL THEN p.reference
                                END as SKU,
                                sys.ebay_user as ebay_user, 
                                sys.name_product as name_product, 
                                sys.date_add as date_add, 
                                sys.date_end as date_end, 
                                sys.is_enabled as is_enabled
                                FROM 
                                (SELECT id_item, id_site, SKU, ebay_user, name_product, date_add, date_end, '1' as is_enabled
                                FROM ebay_synchronization_store
                                WHERE id_site = '".$id_site."') sys JOIN 
                                (SELECT a.id_product as id_product, 
                                a.id_shop as id_shop,
                                a.sku as sku,
				a.reference as reference,
                                a.ean13 as ean13,
                                a.upc as upc 
                                FROM products_product_attribute a
                                WHERE (a.id_shop = '".$id_shop."')) p
                                ON (sys.SKU = p.sku OR sys.SKU = p.reference OR sys.SKU = p.ean13);";

                	$this->_connector->db_query_result_str($sql);
                        
                	$sql = "SELECT 
                                p.id_product as id_product, 
                                sys.id_item as id_item, 
                                sys.id_site as id_site, 
                                p.id_shop as id_shop, 
                                CASE 
                                WHEN p.sku IS NOT NULL THEN p.sku
                                WHEN p.reference IS NOT NULL THEN p.reference
                                END as SKU,
                                sys.ebay_user as ebay_user, 
                                sys.name_product as name_product, 
                                sys.date_add as date_add, 
                                sys.date_end as date_end, 
                                sys.is_enabled as is_enabled
                                FROM 
                                (SELECT id_item, id_site, SKU, ebay_user, name_product, date_add, date_end, '1' as is_enabled
                                FROM ebay_synchronization_store
                                WHERE id_site = '".$id_site."') sys JOIN 
                                (SELECT a.id_product as id_product, 
                                a.id_shop as id_shop,
                                a.sku as sku,
				a.reference as reference,
                                a.ean13 as ean13,
                                a.upc as upc 
                                FROM products_product_attribute a
                                WHERE (a.id_shop = '".$id_shop."')) p
                                ON (sys.SKU = p.sku OR sys.SKU = p.reference OR sys.SKU = p.ean13) GROUP BY id_product;";

                	$product_result = $this->sqlQuerySwap($sql, 'products');
                        $exclude = array();
                        if ( !empty($product_result) ) :
                                foreach ( $product_result as $product ) :
                                        $exclude[] = $product['id_product'];
                                endforeach;
                        endif;
                        $exclude = !empty($exclude) ? "'".implode("', '", $exclude)."'" : "''";
                        $sql = "REPLACE INTO ebay_product_item_id (id_product, id_item, id_site, id_shop, SKU, ebay_user, name_product, date_add, date_end, is_enabled)
                                SELECT 
                                p.id_product as id_product, 
                                sys.id_item as id_item, 
                                sys.id_site as id_site, 
                                p.id_shop as id_shop, 
                                CASE 
                                WHEN p.sku IS NOT NULL THEN p.sku
                                WHEN p.reference IS NOT NULL THEN p.reference
                                END as SKU,
                                sys.ebay_user as ebay_user, 
                                sys.name_product as name_product, 
                                sys.date_add as date_add, 
                                sys.date_end as date_end, 
                                sys.is_enabled as is_enabled
                                FROM 
                                (SELECT id_item, id_site, SKU, ebay_user, name_product, date_add, date_end, '1' as is_enabled
                                FROM ebay_synchronization_store
                                WHERE id_site = '".$id_site."') sys JOIN 
                                (SELECT id_product, id_shop, sku, reference, ean13, upc FROM products_product WHERE id_shop = '".$id_shop."') p
                                ON (sys.SKU = p.sku OR sys.SKU = p.reference OR sys.SKU = p.ean13)";
//                                WHERE p.id_product NOT IN (".$exclude.");

                	$this->_connector->db_query_result_str($sql);

                	//return duplicate
                	$sql = "SELECT e.id_item as id_item, p.active as active, p.quantity as quantity, p.id_product as id_product
                                FROM ebay_product_item_id e
                                join ::PREFIX::_product p on p.id_product = e.id_product
                                WHERE e.id_site = '".$id_site."' AND e.id_item in (SELECT id_item
                                FROM ebay_product_item_id
                                WHERE is_enabled = '1' AND id_site = '".$id_site."'
                                GROUP BY id_item
                                HAVING count(*)  > 1)
                                ORDER BY id_item, p.active DESC, p.quantity DESC, p.id_product DESC;";
                	
                	return $this->sqlQuerySwap($sql, 'products');
                }
                
                public function getCarrierRef($id_shop = null) {
                        if ( (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                  	endif;
                        
                        $sql = "SELECT id_carrier, id_carrier_ref FROM ::PREFIX::_carrier WHERE id_shop = '".$id_shop."'";
                	
                	return $this->sqlQuerySwap($sql, 'products');
                }
                
                
                public function genarateMappingLastest($user = null, $id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_site) && empty($id_site)) ) :
                                return FALSE;
                  	endif;
                        $sql = "SELECT max(c.id_carrier) AS id_shipping, c.name AS name_shipping, ec.id_shipping AS current_shipping, c.id_carrier_ref AS id_carrier_ref FROM ebay_mapping_carrier_domestic ec,
                                (SELECT id_carrier, id_carrier_ref, `name` FROM ::PREFIX::_carrier WHERE id_shop = '".$id_shop."') c
                                WHERE ec.id_carrier_ref = c.id_carrier_ref AND ec.id_site = '".$id_site."' AND ec.id_shop = '".$id_shop."'
                                GROUP BY c.id_carrier_ref HAVING max(c.id_carrier) AND max(c.id_carrier) != ec.id_shipping";

                        $last_domestics = $this->sqlQuerySwap($sql, 'products');
                        
                        $sql = "SELECT max(c.id_carrier) AS id_shipping, c.name AS name_shipping, ec.id_shipping AS current_shipping, c.id_carrier_ref AS id_carrier_ref FROM ebay_mapping_carrier_international ec,
                                (SELECT id_carrier, id_carrier_ref, `name` FROM ::PREFIX::_carrier WHERE id_shop = '".$id_shop."') c
                                WHERE ec.id_carrier_ref = c.id_carrier_ref AND ec.id_site = '".$id_site."' AND ec.id_shop = '".$id_shop."'
                                GROUP BY c.id_carrier_ref HAVING max(c.id_carrier) AND max(c.id_carrier) != ec.id_shipping";

                        $last_international = $this->sqlQuerySwap($sql, 'products');
                        
                        $sql = "SELECT max(c.id_carrier) AS id_shipping, c.name AS name_shipping, ec.id_shipping AS current_shipping, c.id_carrier_ref AS id_carrier_ref FROM ebay_mapping_carrier_rules ec,
                                (SELECT id_carrier, id_carrier_ref, `name` FROM ::PREFIX::_carrier WHERE id_shop = '".$id_shop."') c
                                WHERE ec.id_carrier_ref = c.id_carrier_ref AND ec.id_site = '".$id_site."' AND ec.id_shop = '".$id_shop."'
                                GROUP BY c.id_carrier_ref HAVING max(c.id_carrier) AND max(c.id_carrier) != ec.id_shipping";

                        $last_rule = $this->sqlQuerySwap($sql, 'products');
                        
                        $table_array = array('ebay_mapping_carrier_domestic' => $last_domestics, 'ebay_mapping_carrier_international' => $last_international, 'ebay_mapping_carrier_rules' => $last_rule);
                        $sql_update = '';

                        if ( !empty($table_array) ) :
                                foreach ( $table_array as $table_name => $table ) :
                                        if ( !empty($table) ) :
                                                foreach ( $table as $last_generate ) :
                                                        if ( $table_name == 'ebay_mapping_carrier_rules' ) :
                                                                $data_update = array(
                                                                        'id_shipping'       => $last_generate['id_shipping'],
                                                                        'id_carrier_ref'    => $last_generate['id_carrier_ref'],
                                                                );
                                                        else :  $data_update = array(
                                                                        'id_shipping'       => $last_generate['id_shipping'],
                                                                        'name_shipping'     => $last_generate['name_shipping'],
                                                                        'id_carrier_ref'    => $last_generate['id_carrier_ref'],
                                                                );
                                                        endif;
                                            
                                                        $data_where = array(
                                                                'id_shop'           => $id_shop,
                                                                'id_site'           => $id_site,
                                                                'id_shipping'       => $last_generate['current_shipping'],
                                                        );
                                                        
                                                        $sql_update .= $this->updateColumnOnly($table_name, $data_update, $data_where);
                                                endforeach;
                                        endif;
                                endforeach;
                                
                                if ( !empty($sql_update) ) :
                                        $this->transaction($sql_update);
                                endif;
                        endif;

                }
                
                function getProductReferences($username, $id_shop){
                	$return                                 = array('sku' => array(), 'reference' => array());
                	$sql                                    = "select sku, ean13, upc, reference from ::PREFIX::_product where id_shop = '".$id_shop."'";
                	$result                                 = $this->sqlQuerySwap($sql, 'products');
                        if ( !empty($result) ) :
                                foreach ($result as $row){
                                        $fields = array('sku', 'reference');
                                        foreach($fields as $field){
                                                $val = $row[$field];
                                                if(!empty($val)){
                                                        if(isset($return[$field][$val])){
                                                                $return[$field][$val]++;
                                                        }
                                                        else{
                                                                $return[$field][$val] = 1;
                                                        }
                                                }
                                        }
                                }

                                return $return;
                        endif;
                        return FALSE;
                }
                
                function getCombinationProductReferences($username, $id_shop){
                	$return                                 = array('sku' => array(), 'reference' => array());
                	$sql                                    = "select sku, ean13, upc, reference from ::PREFIX::_product_attribute where id_shop = '".$id_shop."'";
                	$result                                 = $this->sqlQuerySwap($sql, 'products');
                        if ( !empty($result) ) :
                                foreach ($result as $row){
                                        $fields = array('sku', 'reference');
                                        foreach($fields as $field){
                                                $val = $row[$field];
                                                if(!empty($val)){
                                                        if(isset($return[$field][$val])){
                                                                $return[$field][$val]++;
                                                        }
                                                        else{
                                                                $return[$field][$val] = 1;
                                                        }
                                                }
                                        }
                                }

                                return $return;
                        endif;
                        return FALSE;
                }
                 function getProductLatestUploadSummary($user = null, $id_site = null, $id_shop = null){
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($user) && empty($user)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                  	endif;
                        $this->_databasename = "new"; 
                        $this->_user = $user;
                	$return = array();
                	//get all upload group

	                $sql = "SELECT DISTINCT el.batch_id as batch_id, date(el.date_add) as date_add, el.id_site as id_site, el.id_shop as id_shop, el.type as type, es.ebay_user as ebay_user 
	                		FROM ebay_statistics_log el JOIN ebay_statistics es ON el.batch_id = es.batch_id
	                		WHERE el.type IN ('Product', 'Offer') AND el.id_site = '".$id_site."' AND el.id_shop = '".$id_shop."' AND es.type IN ('ReviseFixedPriceItem', 'ReviseInventoryStatus') AND CURDATE() - date(el.date_add) IN (1)";
                        
                	$result = $this->sqlQuerySwap($sql, 'products');

                        if ( !empty($result) ) :
                                foreach ( $result as $row ) :
                                        $return[$row['type']]['id_site']        = $row['id_site'];
                                        $return[$row['type']]['id_shop']        = $row['id_shop'];
                                        $return[$row['type']]['type']           = $row['type'];
                                        $return[$row['type']]['ebay_user']      = $row['ebay_user'];
                                        $return[$row['type']]['date_add']       = $row['date_add'];
                                        $return[$row['type']]['batch_id'][]     = $row['batch_id'];
                                endforeach;
                        else : 
                                return null;
                        endif;
                        
                        $sql = "SELECT count(*) FROM ::PREFIX::_orders WHERE order_date >= DATE(NOW() - INTERVAL 1 DAY) AND site = '".$id_site."' AND id_shop = '".$id_shop."' AND sales_channel = 'eBay'";
                	$result = $this->sqlQuerySwap($sql, 'orders', true, true);
                        $return["Order"]        = $result;
                        
	                //Product Total
                        $check_count = array();

	                foreach($return as $return_key => $return_element){
	                	//SHOP
                                if ( $return_key == 'Product' || $return_key == 'Offer') {
                                        if ( !empty($check_count[$return_element['type']][$return_element['id_shop']]) ) :
                                                $return[$return_key]['ShopTotal']                   = $check_count[$return_element['type']][$return_element['id_shop']]['ShopTotal'];
                                                $return[$return_key]['ShopOutOfStockTotal']         = $check_count[$return_element['type']][$return_element['id_shop']]['ShopOutOfStockTotal'];
                                                $return[$return_key]['EbayTotal']                   = $check_count[$return_element['type']][$return_element['id_shop']]['EbayTotal'];
                                        else :
                                                $sql                                                = "SELECT count(*) as Count FROM ::PREFIX::_product WHERE id_shop = '".$return_element['id_shop']."'";
                                                $result                                             = $this->sqlQuerySwap($sql, strtolower($return_element['type']).'s', true, true);
                                                $return[$return_key]['ShopTotal']                   = $result;
                                                $check_count[$return_element['type']]
                                                [$return_element['id_shop']]['ShopTotal']           = $result;

                                                //SHOP OUT OF STOCK
                                                $sql = "SELECT count(*) as Count FROM ::PREFIX::_product WHERE (quantity = 0 OR quantity IS NULL) AND id_shop = '".$return_element['id_shop']."'";

                                                $result                                             = $this->sqlQuerySwap($sql, strtolower($return_element['type']).'s', true, true);
                                                $return[$return_key]['ShopOutOfStockTotal']         = $result;
                                                $check_count[$return_element['type']]
                                                [$return_element['id_shop']]['ShopOutOfStockTotal'] = $result;

                                                //EBAY
                                                $sql = "SELECT count(*) as Count FROM ebay_product_item_id
                                                        WHERE id_site = '".$return_element['id_site']."' AND id_shop = '".$return_element['id_shop']."' AND ebay_user = '".$return_element['ebay_user']."' AND is_enabled = '1'";
                                                $result                                             = $this->sqlQuerySwap($sql, 'products', true, true);
                                                $return[$return_key]['EbayTotal']                   = $result;
                                                $check_count[$return_element['type']]
                                                [$return_element['id_shop']]['EbayTotal']           = $result;
                                        endif;
                                }
	                }
	                
	                //get summary
	                foreach($return as $return_key => $return_element) {
                                if ( $return_key == 'Product' || $return_key == 'Offer') {
                                        $sql2 = "SELECT CASE WHEN type = 'Compressed' THEN 'Feedbiz' ELSE 'Ebay' END AS MessageFrom,
                                                                                        response as Level,
                                                                                        message as Message,
                                                                                        count(*) as Count
                                                                        FROM ebay_statistics
                                                                        WHERE batch_id IN ('".implode("', '", $return_element['batch_id'])."') 
                                                                        GROUP BY MessageFrom, response, message
                                                                        ORDER BY Count DESC";
                                        
                                        $result = $this->sqlQuerySwap($sql2, 'products');                  
                                        $return[$return_key]['messages'] = $result;
                                        
                                        if ( !empty($result) ) :
                                                $error = 0;
                                                $total = 0;
                                                $success = 0;
                                                $warning = 0;
                                                foreach ( $result as $res ) :
                                                        if (strtolower($res['Level']) == 'error') :
                                                                $error += $res['Count'];
                                                        endif;
                                                        if (strtolower($res['Level']) == 'success') :
                                                                $success += $res['Count'];
                                                        endif;
                                                        if (strtolower($res['Level']) == 'warning') :
                                                                $warning += $res['Count'];
                                                        endif;
                                                        $total += $res['Count'];
                                                endforeach;
                                                $return[$return_key]['Error']       = $error;
                                                $return[$return_key]['Success']     = $success + $warning;
                                                $return[$return_key]['Total']       = $total;
                                        endif;
                                }
	                }	

                	return $return;
                }
		
                function getInventory( $id_site, $id_shop=null){
                        
                	//get all sync sku
	                $sql = "SELECT count(*) as row FROM ebay_product_item_id WHERE id_site = ".(int)$id_site." AND is_enabled = '1';";			
                	return $this->sqlQuerySwap($sql, 'products', true, true);
                }

                function getNumberOfErrorCreateProduct( ){
                        $yesterday = date('Y-m-d',strtotime('yesterday')); 
	                $sql = "SELECT  (error),id_site  FROM ebay_statistics_log WHERE date_add >= '$yesterday' AND type = 'Product' and method = 'Created' group by id_site order by date_add desc  ";
                	$out = $this->sqlQuerySwap($sql, 'products' );
                        $count=0;
                        if(is_array($out))
                        foreach($out as $r){
                            $count+= $r['error'];
                        }
                        return $count;
                }
                
                function getProductLatestBatchIDUploadSummary($batch_id){
                	$return = array();
                	//get all upload group

	                $sql = "SELECT DISTINCT el.batch_id as batch_id, date(el.date_add) as date_add, el.id_site as id_site, el.id_shop as id_shop, el.type as type, es.ebay_user as ebay_user 
	                		FROM ebay_statistics_log el JOIN ebay_statistics es ON el.batch_id = es.batch_id
	                		WHERE el.type IN ('Product', 'Offer') AND el.batch_id = '".$batch_id."'";
                	$result = $this->sqlQuerySwap($sql, 'products');

                        if ( !empty($result) ) :
                                foreach ( $result as $row ) :
                                        $return[$row['type']]['id_site']        = $row['id_site'];
                                        $return[$row['type']]['id_shop']        = $row['id_shop'];
                                        $return[$row['type']]['type']           = $row['type'];
                                        $return[$row['type']]['ebay_user']      = $row['ebay_user'];
                                        $return[$row['type']]['batch_id'][]     = $row['batch_id'];
                                        $return[$row['type']]['date_add']       = $row['date_add'];
                                endforeach;
                        else : 
                                return null;
                        endif;

	                //Product Total
                        $check_count = array();
	                foreach($return as $return_key=>$return_element){
	                	//SHOP
                                if ( !empty($check_count[$return_element['type']][$return_element['id_shop']]) ) :
                                        $return[$return_key]['ShopTotal']                   = $check_count[$return_element['type']][$return_element['id_shop']]['ShopTotal'];
                                        $return[$return_key]['ShopOutOfStockTotal']         = $check_count[$return_element['type']][$return_element['id_shop']]['ShopOutOfStockTotal'];
                                        $return[$return_key]['EbayTotal']                   = $check_count[$return_element['type']][$return_element['id_shop']]['EbayTotal'];
                                else :
                                        $sql                                                = "SELECT count(*) as Count FROM products_product WHERE id_shop = '".$return_element['id_shop']."'";
                                        $result                                             = $this->sqlQuerySwap($sql, strtolower($return_element['type']).'s', true, true);
                                        $return[$return_key]['ShopTotal']                   = $result;
                                        $check_count[$return_element['type']]
                                        [$return_element['id_shop']]['ShopTotal']           = $result;

                                        //SHOP OUT OF STOCK
                                        $sql = "SELECT count(*) as Count
                                                FROM products_product p
                                                LEFT JOIN (SELECT id_product, SUM(quantity) as quantity
                                                FROM products_product_attribute
                                                GROUP BY id_product) c ON p.id_product = c.id_product
                                                WHERE (p.quantity = 0 AND c.quantity IS NULL) OR (c.quantity = 0) AND p.id_shop = '".$return_element['id_shop']."'";

                                        $result                                             = $this->sqlQuerySwap($sql, strtolower($return_element['type']).'s', true, true);
                                        $return[$return_key]['ShopOutOfStockTotal']         = $result;
                                        $check_count[$return_element['type']]
                                        [$return_element['id_shop']]['ShopOutOfStockTotal'] = $result;

                                        //EBAY
                                        $sql = "SELECT count(*) as Count FROM ebay_product_item_id
                                                WHERE id_site = '".$return_element['id_site']."' AND id_shop = '".$return_element['id_shop']."' AND ebay_user = '".$return_element['ebay_user']."' AND is_enabled = '1'";
                                        $result                                             = $this->sqlQuerySwap($sql, 'products', true, true);
                                        $return[$return_key]['EbayTotal']                   = $result;
                                        $check_count[$return_element['type']]
                                        [$return_element['id_shop']]['EbayTotal']           = $result;
                                endif;
	                }
	                
	                //get summary
	                foreach($return as $return_key=>$return_element){
	                	$sql2 = "SELECT CASE WHEN type = 'Compressed' THEN 'Feedbiz' ELSE 'Ebay' END AS MessageFrom,
										response as Level,
										message as Message,
										count(*) as Count
								FROM ebay_statistics
								WHERE batch_id IN ('".implode("', '", $return_element['batch_id'])."') 
								GROUP BY MessageFrom, response, message
								ORDER BY Count DESC";
	                	$return[$return_key]['messages'] = $this->sqlQuerySwap($sql2, 'products');
	                }	                
                	return $return;
                }
                
                public function ebay_attribute_override($username, $shop, $site, $lang, $products = array()){
                	if(empty($products) || !is_array($products)){
                		return;
                	}
                	//Get Product
                	$sql = "SELECT pl.name as product,
                					CASE WHEN p.sku IS NOT NULL THEN p.sku
                					WHEN p.reference IS NOT NULL THEN p.reference
                					END
                					 as sku_product,
                					'' as sku,
		                			p.id_product as id_product,
		                			0 as id_product_attribute,
		                			p.id_shop as id_shop
                			FROM ::PREFIX::_product p
                			JOIN product_lang pl ON pl.id_product = p.id_product AND pl.id_shop = p.id_shop
                			WHERE p.id_shop = '".$shop."' AND pl.id_lang = '".$lang."'";
                	
                	$wheres = array();                	 
                	foreach($products as $product){
                		if($product['combination'] == 0){
                			$wheres[] = "p.id_product = '".intval($product['product'])."'";
                		}
                	}

                	$product_result = array();
                	if(!empty($wheres)){
	                	$sql .= "AND (".implode(" OR ", $wheres).")";
	                	$product_result = $this->sqlQuerySwap($sql, 'products');
                	}
                	
                	//Get Combination                	
                	$sql = "SELECT pl.name as product,
                					CASE WHEN p.sku IS NOT NULL THEN p.sku
                					WHEN p.reference IS NOT NULL THEN p.reference
                					END
                					 as sku_product,
                					CASE  WHEN a.sku IS NOT NULL THEN a.sku
                					WHEN a.reference IS NOT NULL THEN a.reference
                					END
                					 as sku,
		                			p.id_product as id_product,
		                			a.id_product_attribute as id_product_attribute,
		                			p.id_shop as id_shop
                			FROM ::PREFIX::_product p
                			JOIN ::PREFIX::_product_lang pl ON pl.id_product = p.id_product AND pl.id_shop = p.id_shop
                			LEFT JOIN ::PREFIX::_product_attribute a ON a.id_product = p.id_product AND a.id_shop = p.id_shop
                			WHERE p.id_shop = '".$shop."' AND pl.id_lang = '".$lang."'";
                	$wheres = array();
                	
                	foreach($products as $product){
                		if($product['combination'] > 0){
                			$wheres[] = "(p.id_product = '".intval($product['product'])."' AND a.id_product_attribute = '".intval($product['combination'])."')";
                		}
                	}
                	
                	$combination_result = array();
                	if(!empty($wheres)){
	                	$sql .= "AND (".implode(" OR ", $wheres).")";
	                	$combination_result = $this->sqlQuerySwap($sql, 'products');
                	}

                	$merge_result = array_merge($product_result, $combination_result);
                	$override_result = array();
                	$sql = "SELECT id_site, id_product, id_product_attribute, attribute_field, override_value, date_upd FROM ebay_attribute_override WHERE id_shop = '".$shop."' AND id_site = '".$site."'";
                	$wheres = array();
                	foreach($merge_result as $merge_result_element){
                		$id_product = $merge_result_element['id_product'];
                		$id_product_attribute = $merge_result_element['id_product_attribute'];
                		$wheres[] = "(id_product = '".intval($id_product)."' AND id_product_attribute = '".intval($id_product_attribute)."')";
                	}
                	if(!empty($wheres)){
	                	$sql .= "AND (".implode(" OR ", $wheres).")";
	                	$override_result = $this->sqlQuerySwap($sql, 'products');
                	}

                	$return = array();
                	foreach($merge_result as $merge_result_element){
                		$id_product = $merge_result_element['id_product'];
                		$id_product_attribute = $merge_result_element['id_product_attribute'];
                		$return[$id_product][$id_product_attribute] = $merge_result_element;
                	}
                	
                	foreach($override_result as $override_result_element){
                		$id_product = $override_result_element['id_product'];
                		$id_product_attribute = $override_result_element['id_product_attribute'];
                		$return[$id_product][$id_product_attribute]['override'][] = $override_result_element;
                	}

                	return $return;
                }
                
                function getEbayMappingCarrierDomestic($id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;
                        $sql = "SELECT 
                                *
                                FROM ebay_mapping_carrier_domestic
                                WHERE id_site = '".$id_site."' 
                                AND id_shop = '".$id_shop."' 
                                AND id_profile = '".$id_profile."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                function getEbayMappingCarrierInternational($id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT 
                                *
                                FROM ebay_mapping_carrier_international
                                WHERE id_site = '".$id_site."' 
                                AND id_shop = '".$id_shop."' 
                                AND id_profile = '".$id_profile."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }

                function getEbayMappingCarrierCountrySelected($id_site = null, $id_shop = null, $id_profile = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) || (!isset($id_profile) && empty($id_profile)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT 
                                *
                                FROM ebay_mapping_carrier_country_selected
                                WHERE id_site = '".$id_site."' 
                                AND id_shop = '".$id_shop."' 
                                AND id_profile = '".$id_profile."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                
                public function getIDProductByItemID($id_item = null) {
                        if (empty($id_item)){
                                return Null;
                        }
                        
                        $sql = "SELECT id_product
                                FROM ebay_product_item_id
                                WHERE id_item = '".$id_item."' 
                                LIMIT 0, 1";
                        
                        return $this->sqlQuerySwap($sql, 'products', true, true);
                }
                
                public function updateOrdersStatus($id_orders = null) {
                        if (empty($id_orders)){
                                return Null;
                        }
                        
                        $sql = "UPDATE ::PREFIX::_orders SET shipping_status = '1' WHERE id_orders IN ('".implode("', '", $id_orders)."')";
                        
                        return $this->sqlQueryAffected($sql, 'orders');
                }
                
                public function updateOrdersStatusOnly($id_orders = null) {
                        if (empty($id_orders)){
                                return Null;
                        }
                        
                        $sql = "UPDATE orders_orders SET shipping_status = '1' WHERE id_orders IN ('".$id_orders[0]['id_orders']."');";
                        
                        return $sql;
                }
                
                function getEbayExplodeProduct($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT 
                                *
                                FROM ebay_explode_product
                                WHERE id_site = '".$id_site."' 
                                AND id_shop = '".$id_shop."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                function getEbayCarts($id_site = null, $id_shop = null) {
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_shop) && empty($id_shop)) ) :
                                return FALSE;
                        endif;

                        $sql = "SELECT 
                                o.id_shop AS id_shop,
                                o.site AS id_country,
                                o.id_marketplace_order_ref AS mp_order_id,
                                'Pending' AS `status`,
                                i.reference AS reference,
                                i.quantity AS quantity,
                                o.order_date AS `timestamp`,
                                o.date_add AS date_upd
                                FROM
                                orders_orders o
                                LEFT JOIN orders_order_items i ON (o.id_orders = i.id_orders AND o.id_shop = i.id_shop)
                                WHERE
                                o.sales_channel = 'eBay'
                                AND o.order_status = 'Active'
                                AND (o.order_type NOT LIKE 'Close%' AND o.order_type NOT LIKE 'Open')
                                AND o.payment_method NOT LIKE 'None'
                                AND o.id_shop = '".$id_shop."'
                                AND o.site = '".$id_site."'";

                        return $this->sqlQuerySwap($sql, 'products');
                }
                
                function getErrorResolutionsRoots($config = array('limit' => 10, 'offset' => 0)) {
                        if ( (!isset($config['id_site']) && empty($config['id_site'])) || (!isset($config['id_shop']) && empty($config['id_shop'])) ) :
                                return FALSE;
                        endif;

                        $return = array();
                	$sql = "SELECT ::QUERY::
                                FROM ebay_statistics ::WHERE::
                                GROUP BY `code` ::ORDERBY:: 
                                ";
                        
                        $sql_data_batch = "SELECT batch_id
                                FROM ebay_statistics_log
                                WHERE date_add = (::MAX_DATE::)
                                AND type = 'Product' AND method = 'Created'";
                        
                        $sql_data_max_date = "SELECT max(date_add) AS date_add
                                FROM ebay_statistics_log 
                                WHERE type = 'Product' AND method = 'Created'";
                        
                	$sql_data_fields = "`code` AS id, `code`, `message`, count(`code`) AS total";
                	$sql_data_where = "WHERE resolved <> '1' && batch_id = (::BATCH_ID::) AND id_site = ".$config['id_site']." AND id_shop = ".$config['id_shop']." AND `code` != '' AND `code` LIKE 'FB%'";
                        
                	if (!empty($config['sku'])) {
                		$sql_data_where .= " AND sku_export LIKE '%:Q%'";
                		$sql_data_where = str_replace(":Q", $config['sku'], $sql_data_where);
                	}
                	if (!empty($config['message'])) {
                		$sql_data_where .= " AND message LIKE '%:Q%'";
                		$sql_data_where = str_replace(":Q", $config['message'], $sql_data_where);
                	}
                	$sql_data  = str_replace('::QUERY::', $sql_data_fields, $sql);
                	$sql_data  = str_replace('::WHERE::', $sql_data_where, $sql_data);
                	$sql_batch = str_replace('::MAX_DATE::', $sql_data_max_date, $sql_data_batch);
                	$sql_data  = str_replace('::BATCH_ID::', $sql_batch, $sql_data);

                	$sql_data_count = $sql_data = str_replace('::ORDERBY::', 'ORDER BY total DESC ', $sql_data);
                	$sql_data .= "LIMIT ".$config['limit']." ";
                        $sql_data .= "OFFSET ".$config['offset']." ";
                	$return = $this->sqlQuerySwap($sql_data, 'products');
                	$result = $this->sqlQuerySwap($sql_data_count, 'products');
                	$batch_id = $this->sqlQuerySwap($sql_batch, 'products', true, true);
                	$count = !empty($result) || !is_array($result) ? sizeof($result) : 0;
                	return array('data' => $return, 'count' => $count, 'batch_id' => $batch_id);
                }
                
                
                function getErrorResolutionsChild($config = array('limit' => 10, 'offset' => 0)) {
                        if ( (!isset($config['id_site']) && empty($config['id_site'])) || (!isset($config['id_shop']) && empty($config['id_shop'])) ) :
                                return FALSE;
                        endif;

                        switch($config['code']) {
                            case 'FB0001':
                                $sql_data_fields = "`sku_export` AS reference, `id_product`, `name_product`, `message`";
                                $fields = array(
                                    array(
                                        'id' => 'id_product',
                                        'text' => !empty($config['lang']['Product ID']) ? $config['lang']['Product ID'] : 'Product ID',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'name_product',
                                        'text' => !empty($config['lang']['Product name']) ? $config['lang']['Product name'] : 'Product name',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'message',
                                        'text' => !empty($config['lang']['Messages']) ? $config['lang']['Messages'] : 'Messages',
                                        'style' => 'text-left'
                                    )
                                );
                            break;
                            case 'FB0016':
                                $sql_data_fields = "`sku_export` AS reference, `id_product`, id_combination, `sku_export` AS sku, `name_product`, `message`";
                                $fields = array(
                                    array(
                                        'id' => 'sku',
                                        'text' => !empty($config['lang']['SKU']) ? $config['lang']['SKU'] : 'SKU',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'id_combination',
                                        'text' => !empty($config['lang']['Combination ID']) ? $config['lang']['Combination ID'] : 'Combination ID',
                                        'style' => 'text-center'
                                    ),
                                    array(
                                        'id' => 'name_product',
                                        'text' => !empty($config['lang']['Product name']) ? $config['lang']['Product name'] : 'Product name',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'message',
                                        'text' => !empty($config['lang']['Messages']) ? $config['lang']['Messages'] : 'Messages',
                                        'style' => 'text-left'
                                    )
                                );
                            break;
                            case 'FB0002':
                            case 'FB0003':
                            case 'FB0004':
                            case 'FB0005':
                            case 'FB0006':
                            case 'FB0007':
                            case 'FB0008':
                            case 'FB0009':
                            case 'FB0010':
                            case 'FB0011':
                            case 'FB0012':
                            case 'FB0013':
                            case 'FB0014':
                            case 'FB0015':
                            case 'FB0017':
                            case 'FB0018':
                            case 'FB0019':
                            case 'FB0020':
                            case 'FB0021':
                            case 'FB0022':
                                $sql_data_fields = "`sku_export` AS reference, `id_product`, `sku_export` AS sku, `name_product`, `message`";
                                $fields = array(
                                    array(
                                        'id' => 'sku',
                                        'text' => !empty($config['lang']['SKU']) ? $config['lang']['SKU'] : 'SKU',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'name_product',
                                        'text' => !empty($config['lang']['Product name']) ? $config['lang']['Product name'] : 'Product name',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'message',
                                        'text' => !empty($config['lang']['Messages']) ? $config['lang']['Messages'] : 'Messages',
                                        'style' => 'text-left'
                                    )
                                );
                            break;
                            default:
                                $sql_data_fields = "`code` AS id, `code`, `message`, count(`code`) AS total";
                                $fields = array(
                                    array(
                                        'id' => 'code',
                                        'text' => 'Code',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'message',
                                        'text' => 'Message',
                                        'style' => 'text-left'
                                    ),
                                    array(
                                        'id' => 'total',
                                        'text' => 'Total',
                                        'style' => 'text-left'
                                    )
                                );
                            break;
                        }

                        $return = array();
                	$sql = "SELECT ::QUERY::
                                FROM ebay_statistics ::WHERE:: ::ORDERBY::";
                        
                        $sql_data_batch = "SELECT batch_id
                                FROM ebay_statistics_log
                                WHERE date_add = (::MAX_DATE::)
                                AND type = 'Product' AND method = 'Created'";
                        
                        $sql_data_max_date = "SELECT max(date_add) AS date_add
                                FROM ebay_statistics_log 
                                WHERE type = 'Product' AND method = 'Created'";
                        
                	
                	$sql_data_where = "WHERE resolved <> '1' && batch_id = (::BATCH_ID::) AND id_site = ".$config['id_site']." AND id_shop = ".$config['id_shop'];
                        
                	if (!empty($config['code'])) {
                		$sql_data_where .= " AND `code` = ':Q'";
                		$sql_data_where = str_replace(":Q", $config['code'], $sql_data_where);
                	}
                	if (!empty($config['sku'])) {
                		$sql_data_where .= " AND sku_export LIKE '%:Q%'";
                		$sql_data_where = str_replace(":Q", $config['sku'], $sql_data_where);
                	}
                	if (!empty($config['message'])) {
                		$sql_data_where .= " AND message LIKE '%:Q%'";
                		$sql_data_where = str_replace(":Q", $config['message'], $sql_data_where);
                	}
                	$sql_data = str_replace('::QUERY::', $sql_data_fields, $sql);
                	$sql_data = str_replace('::WHERE::', $sql_data_where, $sql_data);
                	$sql_data = str_replace('::BATCH_ID::', $sql_data_batch, $sql_data);
                	$sql_data = str_replace('::MAX_DATE::', $sql_data_max_date, $sql_data);
                	$sql_data_count = $sql_data = str_replace('::ORDERBY::', 'ORDER BY id_product DESC ', $sql_data);
                	$sql_data .= "LIMIT ".$config['limit']." ";
                        $sql_data .= "OFFSET ".$config['offset']." ";
                	$return = $this->sqlQuerySwap($sql_data, 'products');
                	$result = $this->sqlQuerySwap($sql_data_count, 'products');
                	$count = !empty($result) || !is_array($result) ? sizeof($result) : 0;
                	return array('data' => $return, 'count' => $count, 'fields' => $fields);
                }
                
                public function saveSolvedErrorResolutions($config = array()) {
                    if ( (!isset($config['id_site']) && empty($config['id_site'])) || (!isset($config['id_shop']) && empty($config['id_shop'])) ) :
                        return false;
                    endif;
                    
                    $sql = '';
                    if (!empty($config['filters'])) {
                        foreach( $config['filters'] as $filter ) {
                            $data = array(
                                'resolved' => 1
                            );
                            
                            $where = array(
                                'batch_id' => $config['batch_id'],
                                'sku_export' => $filter['reference'],
                                'id_product' => $filter['id_product'],
                            );
                            
                            $sql .= $this->updateColumnOnly('ebay_statistics', $data, $where);
                        }
                    }

                    if ( !empty($sql) ) :
                        $this->transaction($sql);
                        return true;
                    endif;
                    return false;
                }
        }
       
