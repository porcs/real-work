<?php   echo header('Content-Type: text/html; charset=utf-8');
        if ( !file_exists(dirname(__FILE__).'/ObjectBiz.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../Swift/swift_required.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../eucurrency.php')) exit('No direct script access allowed');
        if ( class_exists('ObjectBiz') == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        if ( class_exists('ebaylib')         == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')         == FALSE ) require dirname(__FILE__).'/connect.php';
        if ( class_exists('Swift')           == FALSE ) require dirname(__FILE__).'/../Swift/swift_required.php';
        if ( class_exists('Eucurrency')      == FALSE ) require dirname(__FILE__).'/../eucurrency.php';
        require dirname(__FILE__).'/../tools.php';
        
        class ebayofferexport extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $language;
                protected $site;
                protected $id_shop;
                protected $objectbiz;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                public $product;
                protected $product_relist;
                protected $product_revise;
                protected $product_add;
                protected $product_references;
                protected $_result;
                protected $_email = 'support@feed.biz';
                protected $dir_server;
                protected $flag_sql = '';
                protected $_bulk_error = 0;
                protected $id_marketplace = 3;
                protected $comb_count = array();
                protected $compress_config = 'export_product_array';
                protected $out_of_stock_min;
                protected $out_of_stock_created = 0;
                protected $maximum_quantity;

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function slice_product($product = null, $result_insert = '') {
                        if ( empty($product) ) : 
                                return;
                        endif;
                        $this->error_request_log                        = !empty($this->error_request_log)     ? $this->error_request_log     : 0;
                        $id                                             = !empty($product['id_product_reference']) ? $product['id_product_reference'] : $product['id_product'];
                        $messages                                       = array();
                        //SKU//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['sku']) && empty($product['reference']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //SKU Unique///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product['sku']) && count($product['sku']) ) :
                                $sku_count                              = isset($this->product_references['sku'][$product['sku']]) ? $this->product_references['sku'][$product['sku']] : 0;
                                if ( !empty($sku_count) && $sku_count > 1 ) :
                                	$messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_UNIQUE, $this->language);
                                endif;
                        endif;
                        if ( !empty($product['reference']) && count($product['reference']) ) :
                                $reference_count                        = isset($this->product_references['reference'][$product['reference']]) ? $this->product_references['reference'][$product['reference']] : 0;
                                if ( !empty($reference_count) && $reference_count > 1 ) :
                                        $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_REFERENCE_ID_MUST_BE_UNIQUE, $this->language);
                                endif;
                        endif;
                        if ( !empty($product['disable']) && $product['disable'] == 1 ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_WAS_DISABLED, $this->language);
                        endif;
                        //Quantity, Price, Image////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product['combination']) ) :
                                $check_quantity                         = 0;
                                $check_price                            = 0;
                                foreach ( $product['combination'] as $combination_id => $combination_product ) :
                                        $combination_price              = 0;
                                        $check_quantity                 += $combination_product['quantity'];
                                        $combination_price              += (isset($product['price']) ? $product['price'] : 0) + (isset($combination_product['price']) ? $combination_product['price'] : 0);
                                        $check_price                    += $combination_price;
                                        if ( empty($combination_product['sku']) && empty($combination_product['reference']) ) :
                                                $comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_PROVIDED, $this->language);
                                        endif;
                                        if ( !empty($combination_product['disable']) && $combination_product['disable'] == 1 ) :
                                		$comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_WAS_DISABLED, $this->language);
                                        endif;
                                        if ( $combination_price < 1 ) :
                                		$comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PRICE_IS_NOT_VALID, $this->language);
                                        endif;
                                        //SKU, EAN13, UPC, REFERENCE UNIQUE////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        if ( !empty($combination_product['sku']) && count($combination_product['sku']) ) :
                                                $sku_count                              = isset($this->c_product_references['sku'][$combination_product['sku']]) ? $this->c_product_references['sku'][$combination_product['sku']] : 0;
                                                if ( !empty($sku_count) && $sku_count > 1 ) :
                                                        $comb_messages[$id][$combination_id][]  = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_UNIQUE, $this->language);
                                                endif;
                                        endif;
                                        
                                        if ( !empty($combination_product['reference']) && count($combination_product['reference']) ) :
                                                $reference_count                        = isset($this->c_product_references['reference'][$combination_product['reference']]) ? $this->c_product_references['reference'][$combination_product['reference']] : 0;
                                                if ( !empty($reference_count) && $reference_count > 1 ) :
                                                        $comb_messages[$id][$combination_id][]  = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_REFERENCE_ID_MUST_BE_UNIQUE, $this->language);
                                                endif;
                                        endif;
                                        //SET SQL////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        if ( !empty($comb_messages[$id][$combination_id]) ) :
                                                unset($this->product['product'][$id]['combination'][$combination_id], $data_combination, $this->product['product'][$id]['combinations'][$combination_id]);
                                                foreach($comb_messages[$id][$combination_id] as $message_object):
	                                                $data_combination           = array(
	                                                        "batch_id"          => $this->batch_id,
	                                                        "id_product"        => isset($id) ? $id : 0,
	                                                        "id_site"           => $this->site,
	                                                        "id_shop"           => $this->id_shop,
	                                                        "combination"       => 1,
                                                                "ebay_user"         => $this->ebayUser,
                                                                "sku_export"        => !empty($combination_product['sku']) ? $combination_product['sku'] : (!empty($combination_product['reference']) ? $combination_product['reference'] : ''),
	                                                        "response"          => 'Error',
	                                                        "type"              => 'Compressed',
                                                                "code"              => $message_object['error_code'],
                                                                "name_product"      => html_special($product['name']),
	                                                        "message"           => html_special(strip_tags($message_object['error_details'])),
	                                                        "id_combination"    => isset($combination_id) ? $combination_id : 0,
	                                                        "date_add"          => date("Y-m-d H:i:s", strtotime("now")), 
	                                                );
	                                                $result_insert              .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_combination);
                                                endforeach;
                                        endif;
                                endforeach;
                                
                                if ( empty($this->product['product'][$id]['combination']) ) :
                                	$messages[$id][]            = $this->connect->ebay_error_resolution(connect::$ERR_THE_COMBINATIONS_ARE_NOT_VALID, $this->language);
                                endif;
                        else :  
                                if ( $product['price'] < 1 ) :
                                        if ( $product['price'] < 1 ) :
                                                $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PRICE_IS_NOT_VALID, $this->language);
                                        endif;
                                endif;
                        endif;

                        if ( !empty($messages) && count($messages) ) : 
                                unset($data_insert, $data_combination);
                                foreach ( $messages as $messages_id => $message) :
                                    $this->error_request_log++;
                                	foreach($message as $message_object):
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_product"                => isset($messages_id) ? $messages_id : 0,
                                                "id_combination"            => 0,
                                                "combination"               => 0,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "ebay_user"                 => $this->ebayUser,
                                                "sku_export"                => !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : ''),
                                                "name_product"              => html_special($product['name']),
                                                "response"                  => 'Error',
                                                "type"                      => 'Compressed',
                                                "code"                      => $message_object['error_code'],
                                                "message"                   => html_special(strip_tags($message_object['error_details'])),  
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $result_insert                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                    endforeach;
                                    unset($this->product['product'][$messages_id]);
                                endforeach;
                        endif;//echo "<pre>", print_r($messages, true), "</pre>";
                        return $result_insert;
                }   

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_configuration($product = null, $configs = '', $detail = null) {
                        if ( empty( $product ) ) : 
                                return;
                        endif;
                        
                        //Configs//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($configs) ) :
                                foreach ( $configs as $key => $config ) :
                                        if ( $key == 'listing_duration'  ) :
                                                $this->product['product'][$product['id_product']][$key]                 = $config;
                                        endif;
                                endforeach;
                                $this->product['product'][$product['id_product']]['name']                               = !empty($this->product['product'][$product['id_product']]['name'][$this->language]) ? $this->product['product'][$product['id_product']]['name'][$this->language] : '';
                                $this->product['product'][$product['id_product']]['description']                        = !empty($this->product['product'][$product['id_product']]['description'][$this->language]) ? $this->product['product'][$product['id_product']]['description'][$this->language] : '';
                                $this->product['product'][$product['id_product']]['currency']                           = $this->currency;
                                $this->product['product'][$product['id_product']]['batch_id']                           = $this->batch_id;
                                if ( !empty($detail) ) :
                                        foreach ( $detail as $conf ) :
                                                $this->maximum_quantity                                                = $conf['maximum_quantity'] ? $conf['maximum_quantity'] : 1000;
                                                $this->out_of_stock_min                                                = $conf['out_of_stock_min'] ? $conf['out_of_stock_min'] : 0;
                                                $this->out_of_stock_created                                            = $conf['out_of_stock_created'] ? $conf['out_of_stock_created'] : 0;
                                                $this->product['product'][$product['id_product']]['quantity']          = (!empty($conf['out_of_stock_min']) && $this->product['product'][$product['id_product']]['quantity'] <= $conf['out_of_stock_min']) ? 0 : $this->product['product'][$product['id_product']]['quantity'];
                                                $this->product['product'][$product['id_product']]['quantity']          = (!empty($this->maximum_quantity) && $this->product['product'][$product['id_product']]['quantity'] > $this->maximum_quantity) ? $this->maximum_quantity : $this->product['product'][$product['id_product']]['quantity'];
                                        endforeach;
                                 endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_mapping_relist($product = null) {
                        if ( empty($product) || empty($this->itemID) ) {
                                return;
                        }
                        $id                                         = $product['id_product'];
                        $un_delete                                  = 1;
                        $curent_sku                                                     = !empty($product['reference']) ? $product['reference'] : (!empty($product['sku']) ? $product['sku'] : '')/*."_#_".$this->site*/;
                        if ( !empty($this->itemID[$id]) && array_key_exists('id_product', $this->itemID[$id]) ) :
                                if ( $this->ebayUser == $this->itemID[$id]['ebay_user'] && $this->itemID[$id]['is_enabled'] == 1  && ($curent_sku == $this->itemID[$id]['SKU'] || $id == $this->itemID[$id]['id_product'])) :
                                        $this->product['product'][$id]['relist']        = $this->itemID[$id]['id_item'];
                                        $this->product['product'][$id]['relist_date']   = $this->itemID[$id]['date_end'];
                                        $this->product['product'][$id]['sku']           = !empty($curent_sku) ? $curent_sku : $this->itemID[$id]['SKU'];
                                        $un_delete                                      = 0;
                                        $this->total_request_log++;
                                endif;
                                if ( $un_delete ) :
                                        unset($this->product['product'][$id]);
                                endif;
                        elseif ( !empty($this->itemID[$id]) && is_array($this->itemID[$id]) && !empty($product['category']['mapping_id']) && isset($this->features[$product['category']['mapping_id']]) && ($this->features[$product['category']['mapping_id']] == 0 || !empty($this->expolde_products[$product['id_product']])) ) :
                                foreach ( $product['combination'] as $key => $combination ) :
                                        $combinations[$combination['reference']] = $combination;
//                                        $curent_sku[$combination['reference']] = !empty($combination['reference']) ? $combination['reference'] : (!empty($combination['sku']) ? $combination['sku'] : '')/*."_#_".$this->site*/;
                                endforeach;
                                foreach ( $this->itemID[$id] as $itemsID ) :

                                        if ( !empty($itemsID) && array_key_exists('id_product', $itemsID) && !empty($combinations) ) :
                                                if ( $this->ebayUser == $itemsID['ebay_user'] && $itemsID['is_enabled'] == 1  && array_key_exists($itemsID['SKU'], $combinations)) :
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['relist']        = $itemsID['id_item'];
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['relist_date']   = $itemsID['date_end'];
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['sku']           = !empty($itemsID['SKU']) ? $itemsID['SKU'] : $combination['reference'];
                                                        $this->total_request_log++;
                                                else :
                                                        unset($this->product['product'][$id."_".$itemsID['SKU']]);
                                                endif;
                                        endif;
                                endforeach;
                        else :
                            if ( $un_delete ) :
                                unset($this->product['product'][$id]);
                            endif;
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_mapping_category($product = null, $mappings = '', $field = null) {
                        if ( empty( $product ) ) : 
                                return;
                        endif;
                        
                        if ( empty($mappings) && (empty($field) || ($field == 'category')) ) : 
                                $this->_result['msg']['mapping_category'] = 1;
                                return;
                        endif;
                        
                        if ( !empty($mappings) ) :
                                if ( !empty($product['id_category_default']) && array_key_exists($product['id_category_default'], $mappings) ) :
                                        $this->product['product']
                                        [$product['id_product']][$field]    = array("mapping" => $mappings[$product['id_category_default']]["name_ebay_category"], "id" => $mappings[$product['id_category_default']]["id_category"], "mapping_id" => $mappings[$product['id_category_default']]['id_ebay_category']);
                                        return;
                                else :
                                        if ( !empty($product['id_category_default']) ) :
                                                $results                    = $this->objectbiz->getCategoryChildDefault($this->user_name, $product['id_category_default'], $this->id_shop);
                                                if ( !empty($results) ) :
                                                        foreach ( $results as $res ) :
                                                                if ( !empty($mappings[$res["id_parent"]]) ) :
                                                                        $this->product['product'][$product['id_product']][$field]                  = array("mapping" => $mappings[$res["id_parent"]]["name_ebay_category"], "id" => $mappings[$res["id_parent"]]["id_category"], "mapping_id" => $mappings[$res["id_parent"]]['id_ebay_category']);
                                                                        $this->product['product'][$product['id_product']]['id_category_default']   = $mappings[$res["id_parent"]]["id_category"];
                                                                        return;
                                                                endif;
                                                        endforeach;
                                                endif;
                                        endif;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function extract_combination($product = null, $combination = array()) {
                        if ( empty($product) ) {
                                return;
                        }
                        if ( is_array($product['combination'])) :
                                $id     = $product['id_product'];
                                $this->export_mapping_relist($product);
                                foreach ( $product['combination'] as $comb_id => $comb ) :
                                        unset($data_insert, $data_new);
                                        $data_insert['sku']                     = !empty($comb['reference']) ? $comb['reference'] : (!empty($comb['sku']) ? $comb['sku'] : '');
                                        $data_insert['ean13']                   = isset($comb['ean13']) ? $comb['ean13'] : '';
                                        $data_insert['upc']                     = isset($comb['upc']) ? $comb['upc'] : '';
                                        $data_insert['name']                    = isset($this->product['product'][$id]['name']) ? $this->product['product'][$id]['name'] : '';
                                        $data_insert['quantity']                = isset($comb['quantity']) ? $comb['quantity'] : 0;
                                        $data_insert['price']                   = isset($comb['price']) ? ($comb['price'] + (isset($product['price']) ? $product['price'] : 0)) : (isset($product['price']) ? $product['price'] : 0);
                                        $data_new['id_product']                 = isset($product['id_product']) ? $product['id_product'] : '';
                                        $data_new['description']                = isset($product['description']) ? $product['description'] : '';
                                        $data_new['description']                = isset($product['description_short']) ? $product['description_short'] : '';
                                        $data_new['ean13']                      = isset($comb['ean13']) ? $comb['ean13'] : '';
                                        $data_new['upc']                        = isset($comb['upc']) ? $comb['upc'] : '';
                                        $data_new['reference']                  = isset($comb['reference']) ? $comb['reference'] : '';
                                        $data_new['condition']                  = isset($product['condition']) ? $product['condition'] : '';
                                        $data_new['carrier']                    = isset($product['carrier']) ? $product['carrier'] : '';
                                        $data_new['currency']                   = isset($product['currency']) ? $product['currency'] : '';
                                        $data_new['category']                   = isset($product['category']) ? $product['category'] : '';
                                        $data_new['id_category_default']        = isset($product['id_category_default']) ? $product['id_category_default'] : '';
                                        $data_new['combination']                = array();
                                        $data_new['listing_duration']           = isset($product['listing_duration']) ? $product['listing_duration'] : '';
                                        $data_new['return']                     = isset($product['return']) ? $product['return'] : '';
                                        $data_new['postal_code']                = isset($product['postal_code']) ? $product['postal_code'] : '';
                                        $data_new['address']                    = isset($product['address']) ? $product['address'] : '';
                                        $data_new['site']                       = isset($product['site']) ? $product['site'] : '';
                                        $data_new['paypal_email']               = isset($product['paypal_email']) ? $product['paypal_email'] : '';
                                        $data_new['payment_method']             = isset($product['payment_method']) ? $product['payment_method'] : '';
                                        $data_new['dispatch_time']              = isset($product['dispatch_time']) ? $product['dispatch_time'] : '';
                                        $data_new['country']                    = isset($product['country']) ? $product['country'] : '';
                                        $data_new['batch_id']                   = isset($product['batch_id']) ? $product['batch_id'] : '';
                                        $data_new['active']                     = isset($product['active']) ? $product['active'] : false;
                                        $data_new['disable']                    = isset($comb['disable']) ? $comb['disable'] : '';
                                        $data_insert['quantity']                = (!empty($this->out_of_stock_min) && $data_insert['quantity'] <= $this->out_of_stock_min) ? 0 : $data_insert['quantity'];
                                        $data_insert['quantity']                = (!empty($this->maximum_quantity) && $data_insert['quantity'] > $this->maximum_quantity) ? $this->maximum_quantity : $data_insert['quantity'];

                                        $combination[$comb_id]      = $data_insert;
                                        $data_new                   = array_merge($data_new, $data_insert);
                                        $this->product['product'][$id]['combinations']  = !empty($combination) ? $combination : array();
                                        if ( empty($this->product['product'][$id]['combinations']) ) :
                                                unset($this->product['product'][$id]['combinations']);
                                        endif;
                                        if ( !empty($product['category']['mapping_id']) && is_numeric($product['category']['mapping_id']) && ((isset($this->features[$product['category']['mapping_id']]) && $this->features[$product['category']['mapping_id']] == 0) ||
                                             (isset($this->expolde_products[$product['id_product']])))
                                           ) :
                                                if ( !empty($data_new['reference']) ) {
                                                    $this->product['product'][$id."_".$data_new['reference']]      = !empty($this->product['product'][$id."_".$data_new['reference']]) ? array_merge($this->product['product'][$id."_".$data_new['reference']], $data_new) : $data_new;
                                                    $this->export_mapping_relist($this->product['product'][$id."_".$data_new['reference']]);
                                                    $this->product['product'][$id."_".$data_new['reference']]['id_product_reference'] = $id."_".$data_new['reference'];
                                                }
                                                else {
                                                    unset($this->product['product'][$id]);
                                                    continue;
                                                }
                                                $combination_id_group[] = $id."_".$data_new['reference'];
                                        endif;
                                endforeach;
                                if ( !empty($product['category']['mapping_id']) && is_numeric($product['category']['mapping_id']) && ((isset($this->features[$product['category']['mapping_id']]) && $this->features[$product['category']['mapping_id']] == 0) ||
                                     (isset($this->expolde_products[$product['id_product']])))
                                   ) :
                                        $this->total_request_log--;
                                        unset($this->product['product'][$id], $combination, $comb, $data_insert, $product);
                                        return !empty($combination_id_group) ? $combination_id_group : array();
                                endif;
                        else :
                                $id                                             = $product['id_product'];
                                $this->export_mapping_relist($product);
                        endif;
                        unset($combination, $comb, $data_insert, $data_new, $product);
                        return array($id);
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_discount_price($product = null) {
                        if ( !isset($product) && empty($product) ) :
                                return null;
                        endif;
                        $id                                             = $product['id_product'];
                        if ( is_array($product['combination'])) :
                                foreach ( $product['combination'] as $comb_id => $comb ) :
                                        if ( isset($product['sale']) && !empty($product['sale'][$comb['id_product_attribute']]) ) :
                                                $current_price              = isset($comb['price']) ? ($comb['price'] + (isset($product['price']) ? $product['price'] : 0)) : (isset($product['price']) ? $product['price'] : 0);
                                                $saleArray                  = $product['sale'][$comb['id_product_attribute']];
                                                if ( (strtotime('now') > strtotime($saleArray['date_from']) && strtotime('now') < strtotime($saleArray['date_to'])) || '1970-01-01' == $saleArray['date_to']) :
                                                        if ( $saleArray['reduction_type'] == 'percentage' ) :
                                                                $current_price = round(($current_price - (($current_price * $saleArray['reduction']) / 100)), 2);
                                                        endif;
                                                        if ( $saleArray['reduction_type'] == 'Price' || $saleArray['reduction_type'] == 'amount') :
                                                                $current_price  = $current_price - $saleArray['reduction'];
                                                        endif;
                                                endif;
                                                $this->product['product'][$id]['combination'][$comb_id]['price'] = $current_price;
                                        endif;
                                endforeach;
                        else :  if ( isset($product['sale']) && !empty($product['sale'][0]) ) :
                                        $current_price              = (isset($product['price']) ? $product['price'] : 0);
                                        $saleArray                  = $product['sale'][0];
                                        if ( (strtotime('now') > strtotime($saleArray['date_from']) && strtotime('now') < strtotime($saleArray['date_to'])) || '1970-01-01' == $saleArray['date_to']) :
                                                if ( $saleArray['reduction_type'] == 'percentage' ) :
                                                        $current_price = round(($current_price - (($current_price * $saleArray['reduction']) / 100)), 2);
                                                endif;
                                                if ( $saleArray['reduction_type'] == 'Price' || $saleArray['reduction_type'] == 'amount') :
                                                        $current_price  = $current_price - $saleArray['reduction'];
                                                endif;
                                        endif;
                                        $this->product['product'][$id]['price'] = $current_price;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_price_modifier($product = null) {
                        if ( !empty($product) && !empty($this->_price_modifier) ) :
                                foreach ( $this->_price_modifier as $price_modifier ) :
                                        if (   (!empty($price_modifier["decimal_value"]) || is_numeric($price_modifier["decimal_value"]))
                                            && (!empty($price_modifier["price_from"]) || is_numeric($price_modifier["price_from"]))
                                            && (!empty($price_modifier["price_to"]) || is_numeric($price_modifier["price_to"]))
                                            && (!empty($price_modifier["price_value"]) || is_numeric($price_modifier["price_value"]))
                                            && (!empty($price_modifier["price_type"])) 
                                            //&& ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"]) 
                                           ) : 
                                                if ( $price_modifier["price_type"] == 'percent' ) : 
                                                        if ( !empty($product['combination']) ) :
                                                                foreach( $product['combination'] as $comb_key => $comb ) :
                                                                        if ($price_modifier["price_from"] <= $comb['price'] && $comb['price'] <= $price_modifier["price_to"])  :
                                                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] + number_format(($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] * $price_modifier["price_value"]/100), $price_modifier["decimal_value"], '.', '');
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                            if ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"])  :
                                                                    $this->product['product'][$product['id_product']]['price']     = $this->product['product'][$product['id_product']]['price'] + number_format(($this->product['product'][$product['id_product']]['price'] * $price_modifier["price_value"]/100), $price_modifier["decimal_value"], '.', '');
                                                            endif;
                                                        endif;
                                                elseif ( $price_modifier["price_type"] == 'value' ) :
                                                        if ( !empty($product['combination']) ) :
                                                                foreach( $product['combination'] as $comb_key => $comb ) :
                                                                        if ($price_modifier["price_from"] <= $comb['price'] && $comb['price'] <= $price_modifier["price_to"])  :
                                                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] + number_format($price_modifier["price_value"], $price_modifier["decimal_value"], '.', '');
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                            if ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"])  :
                                                                    $this->product['product'][$product['id_product']]['price']     = $this->product['product'][$product['id_product']]['price'] + number_format($price_modifier["price_value"], $price_modifier["decimal_value"], '.', '');
                                                            endif;
                                                        endif;
                                                endif;
                                                $this->price_modifier_rate = floatval($price_modifier["price_value"]);
                                                $this->price_modifier_type = $price_modifier["price_type"];
                                        endif;
                                endforeach;
                        endif;

                        if ( !empty($product) ) : 
                                $this->export_product_option($this->product['product'][$product['id_product']]);
                                $this->product['product'][$product['id_product']]['price']                             = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['price'], $this->current_currency['iso_code'], $this->currency);
                                if ( !empty($product['combination']) ) : 
                                        foreach( $product['combination'] as $comb_key => $comb ) :
                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'], $this->current_currency['iso_code'], $this->currency);
                                                $this->comb_count[count($comb['attributes'])]   = !empty($this->comb_count[count($comb['attributes'])]) ? ++$this->comb_count[count($comb['attributes'])] : 1;
                                        endforeach;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_product_option($product = '') {
                        if ( !empty($product) && !empty($this->product_option) && array_key_exists($product['id_product'], $this->product_option)  ) :
                                $id = $product['id_product'];
                                if ( array_key_exists("id_product", $this->product_option[$id]) ) :
                                        if ( empty($product['combination']) ) : 
                                                $this->product['product'][$id]['price']              = (isset($this->product_option[$id]['price']) && (int)$this->product_option[$id]['price'] != 0) ? $this->product_option[$id]['price'] : $product['price'];
                                                $this->product['product'][$id]['quantity']           = isset($this->product_option[$id]['force']) ? $this->product_option[$id]['force'] : $product['quantity'];
                                                $this->product['product'][$id]['disable']            = !empty($this->product_option[$id]['disable']) ? 1 : (!is_numeric($this->product_option[$id]['disable']) && !empty($product['disable']) ? 1 : 0);
                                        else :
                                                if ( !empty($this->product_option[$id]['id_product_attribute']) && array_key_exists($this->product_option[$id]['id_product_attribute'], $product['combination']) ) :
                                                        $id_combination                              = $this->product_option[$id]['id_product_attribute'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['price']    = (isset($this->product_option[$id]['price']) && (int)$this->product_option[$id]['price'] != 0) ? $this->product_option[$id]['price'] : $product['combination'][$id_combination]['price'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['quantity'] = isset($this->product_option[$id]['force']) ? $this->product_option[$id]['force'] : $product['combination'][$id_combination]['quantity'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['disable']  = !empty($this->product_option[$id]['disable']) ? 1 : (!is_numeric($this->product_option[$id]['disable']) && !empty($product['combination'][$id_combination]['disable']) ? 1 : 0);
                                                endif;
                                        endif;

                                else :
                                        if ( empty($product['combination']) ) : 
                                                $this->product['product'][$id]['disable']            = !empty($product['disable']) ? 1 : 0;
                                        else :  
                                                foreach ( $this->product_option[$id] as $product_option_attribute ) :
                                                        if ( !empty($product_option_attribute['id_product_attribute']) && array_key_exists($product_option_attribute['id_product_attribute'], $product['combination']) ) :
                                                                $id_combination                              = $product_option_attribute['id_product_attribute'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['price']    = (isset($product_option_attribute['price']) && (int)$product_option_attribute['price'] != 0) ? $product_option_attribute['price'] : $product['combination'][$id_combination]['price'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['quantity'] = isset($product_option_attribute['force']) ? $product_option_attribute['force'] : $product['combination'][$id_combination]['quantity'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['disable']  = !empty($product_option_attribute['disable']) ? 1 : (!is_numeric($product_option_attribute['disable']) && !empty($product['combination'][$id_combination]['disable']) ? 1 : 0);
                                                        endif;
                                                endforeach;
                                        endif;
                                endif;
                        else :
                                $id = $product['id_product'];
                                if ( empty($product['combination']) ) : 
                                        $this->product['product'][$id]['disable']            = !empty($product['disable']) ? 1 : 0;
                                else :
                                        foreach ( $product['combination'] as $id_combination => $product_option_attribute ) :
                                                $this->product['product'][$id]
                                                ['combination'][$id_combination]['disable']  = !empty($product_option_attribute['disable']) ? 1 : 0;
                                        endforeach;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_revise_relists($product = '', $product_id = '') {
                        if ( empty($product) || empty($product_id) ) :
                                return;
                        endif;

                        $this->product_revise['product'][$product_id]   = $product;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_SYNCHRONIZE' )     $result['synchronization']      = $value;
                                else if ( $name == 'EBAY_VERIFY_PRODUCT' )  $result['verify_product']       = $value;
                                else if ( $name == 'EBAY_ENABLED_MANUAL' )  $result['enabled_manual']       = $value;
                                else if ( $name == 'EBAY_PROFILE_RULES' )   $result['profile_rules']        = $value;
                        endwhile;
                        return $result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics($parser = '', $type = '', $name = '', $result_insert = '') {
                        if ( empty($parser) ) :
                                return;
                        endif;
                        
                        $this->total_response_log                           = !empty($this->total_response_log)     ? $this->total_response_log     : 0;
                        $this->success_response_log                         = !empty($this->success_response_log)   ? $this->success_response_log   : 0;
                        $this->warning_response_log                         = !empty($this->warning_response_log)   ? $this->warning_response_log   : 0;
                        $this->error_response_log                           = !empty($this->error_response_log)     ? $this->error_response_log     : 0;
                        $ack                                                = $parser->Ack->__toString();
                        
                        if ( !empty($parser->CorrelationID) && count($parser->CorrelationID) ) :
                                list($productName, $productID, $idItem, $sku, $this->batch_id)= @explode("-#-#-", $parser->CorrelationID->__toString());
                        elseif ( isset($name) ) : 
                                list($productName, $productID, $idItem, $sku, $this->batch_id)= @explode("-#-#-", $name);
                        endif;
                        if ( isset($parser->Errors) ) :
                                $error_message_show = array();
                                foreach ( $parser->Errors as $error_message) :
                                        if ( $error_message->ErrorCode->__toString() == '21916333' ) :
                                                unset($data_insert, $data_where);
                                                $data_insert                                = array(
                                                        "is_enabled"                        => 0,
                                                );

                                                $data_where                                 = array(
                                                        "id_item"                           => isset($idItem) ? $idItem : 0,
                                                        "id_site"                           => $this->site,
                                                        "id_shop"                           => $this->id_shop,
                                                        "ebay_user"                         => $this->ebayUser,
                                                );
                                                $result_insert                              .= $this->objectbiz->updateColumnOnly("ebay_product_item_id", $data_insert, $data_where);
                                        endif;
                                        $error_message_show[$error_message->ErrorCode->__toString()] = $error_message->LongMessage->__toString();
                                endforeach;
                        endif;

                        $error_message_show = empty($error_message_show) ? array(''=>'') : $error_message_show;
                        
                        if ( $ack == 'Success' || $ack == 'Warning' ) :
                                foreach ( $parser->InventoryStatus as $Inventory ) :
                                        if ($ack == 'Success') :
                                                $this->success_response_log++;
                                        else :
                                                $this->warning_response_log++;
                                        endif;
                                        
                                        $this->flag_sql                                     .= ImportLog::update_flag_offers(3, $this->site, $this->id_shop, $productID, 0);
                                        unset($data_insert);
                                        foreach($error_message_show as $ecode=>$emessage) {
                                                if ( $ecode == '21917091' ) :
                                                        $this->success_response_log++;
                                                        $this->warning_response_log--;
                                                        $data_insert                                        = array(
                                                                "batch_id"                                  => $this->batch_id,
                                                                "id_product"                                => isset($productID) ? $productID : 0,
                                                                "id_site"                                   => $this->site,
                                                                "id_shop"                                   => $this->id_shop,
                                                                "sku_export"                                => $sku,
                                                                "ebay_user"                                 => $this->ebayUser,
                                                                "name_product"                              => isset($productName) ? html_special($productName) : 0,
                                                                "response"                                  => 'Success',
                                                                "type"                                      => $type,
                                                                "code"                                      => '',
                                                                "id_combination"                            => 0, 
                                                                "combination"                               => 0, 
                                                                "message"                                   => '',  
                                                                "date_add"                                  => date("Y-m-d H:i:s", strtotime("now")), 
                                                        );
                                                        $result_insert                                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                                else :  
                                                        $data_insert                                        = array(
                                                                "batch_id"                                  => $this->batch_id,
                                                                "id_product"                                => isset($productID) ? $productID : 0,
                                                                "id_site"                                   => $this->site,
                                                                "id_shop"                                   => $this->id_shop,
                                                                "sku_export"                                => $sku,
                                                                "ebay_user"                                 => $this->ebayUser,
                                                                "name_product"                              => isset($productName) ? html_special($productName) : 0,
                                                                "response"                                  => $ack,
                                                                "type"                                      => $type,
                                                                "code"                                      => $ecode,
                                                                "id_combination"                            => 0, 
                                                                "combination"                               => 0, 
                                                                "message"                                   => html_special($emessage),  
                                                                "date_add"                                  => date("Y-m-d H:i:s", strtotime("now")), 
                                                        );
                                                        $result_insert                                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                                endif;
                                        }
                                        $this->total_response_log++;
                                endforeach;
                                
                        elseif ( $ack == 'Failure' ) : 
                                unset($data_insert);
                                foreach($error_message_show as $ecode=>$emessage) {
                                        $data_insert                                        = array(
                                                "batch_id"                                  => $this->batch_id,
                                                "id_product"                                => isset($productID) ? $productID : 0,
                                                "id_site"                                   => $this->site,
                                                "id_shop"                                   => $this->id_shop,
                                                "sku_export"                                => $sku,
                                                "ebay_user"                                 => $this->ebayUser,
                                                "name_product"                              => isset($productName) ? html_special($productName) : 0,
                                                "response"                                  => 'Failure',
                                                "type"                                      => $type,
                                                "code"                                      => $ecode,
                                                "id_combination"                            => 0, 
                                                "combination"                               => 0, 
                                                "message"                                   => html_special($emessage),    
                                                "date_add"                                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $result_insert                                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                }
                                $this->error_response_log++;
                                $this->total_response_log++;
                        endif;
                        return $result_insert;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics_log() {
                        $this->objectbiz->updateEbayStatisticsLog($this->user_name, $this->batch_id);
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics_sql() {
                        return $this->objectbiz->updateEbayStatisticsLogSql($this->user_name, $this->batch_id);
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getJobsCheck($status) {
                        $this->ebaylib->getJobs($this->site, $status);
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function abortJobCheck() {
                        $this->objectbiz->checkDB("ebay_job_task");
                        $jobStatus = array('Failed', 'Created');
                        foreach ($jobStatus as $status) :
                                unset($abort_jobID, $result_insert);
                                $abort_jobID = $this->getJobsCheck($status);
                                if ( !empty($abort_jobID) && !empty($abort_jobID->jobProfile) && count($abort_jobID->jobProfile) ) :
                                        foreach ( $abort_jobID->jobProfile as $jobID ) :
                                                if ( empty($jobID->completionTime) ) :
                                                        $this->ebaylib->abortJob($this->site, $jobID->jobId->__toString());
                                                        $data_insert                        = array(
                                                                'jobId'                     => $jobID->jobId->__toString(),
                                                                'fileReferenceId'           => $jobID->inputFileReferenceId->__toString(),
                                                                'batch_id'                  => $this->batch_id,
                                                                'ebay_user'                 => $this->ebayUser,
                                                                'jobType'                   => $jobID->jobType->__toString(),
                                                                'jobStatus'                 => 'Aborted',
                                                                'percentComplete'           => '100%',
                                                                'creationTime'              => date('Y-m-d H:i:s', strtotime('now')),
                                                                'startTime'                 => date('Y-m-d H:i:s', strtotime('now')),
                                                        );
                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert, 'offers', $this->user_name);
                                                endif;
                                        endforeach;
                                endif;
                        endforeach;
                }
                
                function object_product_list($product = array(), $data = array()) {
                        if ( empty($product) ) :
                                return NULL;
                        endif;
                        $count = 0;
                        if( isset($product) && !empty($product) && isset($product->id_product) ) {
                                    $data['id_product']        = $product->id_product;
                                    $data['name']              = $product->name;
                                    $data['description']       = $product->description;
                                    $data['description_short'] = isset($product->description_short) ? $product->description_short : '' ;
                                    $data['sku']               = isset($product->sku) ? $product->sku : '' ;
                                    $data['upc']               = isset($product->upc) ? $product->upc : '' ;
                                    $data['ean13']             = isset($product->ean13) ? $product->ean13 : '' ;
                                    $data['reference']         = isset($product->reference) ? $product->reference : '' ;
                                    $data['quantity']          = isset($product->quantity) && $product->quantity > -1 ? ($product->active == 0 ? 0 : $product->quantity) : 0 ;
                                    $data['condition']         = isset($product->condition) ? $product->condition : '' ;
                                    $data['weight']            = isset($product->weight) ? $product->weight : '';                
                                    $data['price']             = isset($product->price) ? $product->price : '';
                                    $data['manufacturer']      = isset($product->manufacturer) ? $product->manufacturer : '';
                                    $data['supplier']          = isset($product->supplier) ? $product->supplier : '';
                                    $data['width']             = isset($product->width) ? $product->width : '';
                                    $data['height']            = isset($product->height) ? $product->height : '';
                                    $data['depth']             = isset($product->depth) ? $product->depth : '';
                                    $data['weight']            = isset($product->weight) ? $product->weight : '';
                                    $data['active']            = isset($product->active) ? $product->active : '';
                                    $data['currency']          = isset($product->currency) ? $product->currency : '';
                                    $data['feature']           = isset($product->feature) ? $product->feature : '';

                                    if(isset($product->on_sale) && $product->on_sale == 1)
                                            $data['sale'] = $product->sale;

                                    $data['carrier']       = isset($product->carrier) ? $product->carrier : '';

                                    if(isset($product->currency) && !empty($product->currency)) {
                                            foreach ($product->currency as $currency)
                                                    if(isset($currency['name']) || !empty($currency['name']))
                                                            $data['currency'] = strtolower($currency['name']);
                                    }
                                    else {
                                            $data['currency'] = '';
                                    }

                                    if (isset($product->id_category_default)) 
                                            $data['id_category_default'] = $product->id_category_default;
                                    else
                                            $data['category'] = '';

                                    if(isset($product->image) && !empty($product->image)) {
                                            foreach ($product->image as $key_image => $image) {
                                                    if(isset($image['image_url']) || !empty($image['image_url'])) {
                                                            $data['images'][$key_image]['url']  = $image['image_url'];
                                                            $data['images'][$key_image]['type']  = $image['image_type'];
                                                    }
                                            }
                                    }

                                    $data['combination']   = isset($product->combination) ? $product->combination : '';
                        }
                        return $data;
                }
                
                function export_product_list() {
                        $flag_update                                = $this->objectbiz->getEbayFlagUpdate($this->user_name, $this->site, $this->id_shop, 3);
                        if ( !empty($flag_update) ) :
                                foreach ($flag_update as $key => $product) :
                                        $products                           = new Product($this->user_name, $product['id_product'], $this->id_lang, $this->mode['mode'], $this->id_shop);
                                        $this->product['product'][$product['id_product']]   = $this->object_product_list($products);
                                endforeach;
                        endif;
                }
                
                function export_get_flag() {
                        $flag_update                                = $this->generate_array_key("ebayofferexport::addition_array_group", "id_product", "id_product", $this->objectbiz->getEbayFlagUpdate($this->user_name, $this->site, $this->id_shop, 3));
                        if ( !empty($flag_update) ) : 
                                $this->export_product_group($flag_update);
                        endif;
                }
                
                function export_product_group($product_list = array()) {
                        if ( !isset($product_list) && empty($product_list) ) :
                                return null;
                        endif;
                        unset($data);
                        $page = 1;
                        $data                   = $this->objectbiz->exportProductsArray($this->user_name, $this->id_shop, $this->mode['mode'], mb_substr($this->language, 0, 2), $page, $product_list['id_product']);
                        $product['product']     = empty($product['product']) ? (empty($data) ? array() : $data['product']) : ($product['product'] + (empty($data) ? array() : $data['product']));
                        $this->product          = $product;
                }
                
                function export_product_item($id_product = null) {
                        if ( !isset($id_product) && empty($id_product) ) :
                                return null;
                        endif;
                        $products                                   = new Product($this->user_name, $id_product, $this->id_lang, $this->mode['mode'], $this->id_shop);
                        $this->product['product'][$id_product]      = $this->object_product_list($products);
                }
                
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function addition_array_group($data = null) {
                        if ( !empty($data) && is_array($data) && !array_key_exists($this->change_wanted, $data) ) :
                                array_map('ebayexport::addition_array_group', $data);
                        elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_wanted, $data)) :
                                $this->change_data[$this->change_keys][]     = $data[$this->change_wanted];
                        endif;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function change_array_keys($data = null) {
                        if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                                array_map('ebayexport::change_array_keys', $data);
                        elseif ( !empty($data) && is_array($data) && array_key_exists($this->change_keys, $data)) : 
                                if ( !isset($this->change_data[$data[$this->change_keys]]) ) : 
                                        $this->change_data[$data[$this->change_keys]]   = $data;
                                else : 
                                        if ( is_array($this->change_data[$data[$this->change_keys]]) && count(current($this->change_data[$data[$this->change_keys]])) > 1 ) :
                                                $this->change_data[$data[$this->change_keys]][]   = $data;
                                        else :
                                                $store_data                                     = $this->change_data[$data[$this->change_keys]];
                                                unset($this->change_data[$data[$this->change_keys]]);
                                                $this->change_data[$data[$this->change_keys]][]   = $store_data;
                                                $this->change_data[$data[$this->change_keys]][]   = $data;
                                        endif;
                                endif;
                        endif;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function generate_array_key($function = null, $key = null, $wanted = null, $data = array()) {
                        if ( empty($function) || empty($key) || empty($data) ) :
                                return $data;
                        endif;
                        $this->change_data                      = array();
                        $this->change_wanted                    = $wanted;
                        $this->change_keys                      = $key;
                        array_map($function, $data);
                        return $this->change_data;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_method() {
                        $methods                                = array('ReviseInventoryStatus');
                        foreach ( $methods as $method ) :
                                if ( file_exists($this->dir_name."/".$method.".xml.gz") ) unlink($this->dir_name."/".$method.".xml.gz");
                                if ( file_exists($this->dir_name."/".$method.".zip") ) unlink($this->dir_name."/".$method.".zip");        
                                $this->_result[$method]         = 'No Used';
                        endforeach;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function preparing_export($result_insert = '') {
//                        $this->ebayUser = 'deguizeo';
//                        $this->abortJobCheck();
                        $this->export_get_flag();
//                        $this->export_product_list();
//                        $this->export_product_item(16431);
//                        $this->export_product_group(array("id_product" => array(16431)));
                        $this->export_method();
                        $this->total_request_log = 0;
                        $this->error_request_log = 0;
                        list($id_packages)          = $this->connect->fetch($this->connect->get_packages_from_site($this->site, $this->id_marketplace));
                        $category_mappings          = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_category', '', $this->objectbiz->getMappingCategory($this->site, $this->id_shop));
                        $this->itemID               = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_product', '', $this->objectbiz->getIDItemByLast($this->user_name, $this->site, $this->id_shop, $this->ebayUser));
                        $this->_price_modifier      = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_profile', '', $this->objectbiz->getPriceModifier($this->user_name, $this->site, $this->id_shop));
                        $getConfiguration           = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_profile', '', $this->objectbiz->getExportProfileConfiguration($this->user_name, $this->site, $this->id_shop));
                        $category_group             = $this->generate_array_key('ebayofferexport::addition_array_group', 'id_category', 'id_ebay_category', $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->site, $this->id_shop));
                        $this->product_option       = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_product', '', $this->objectbiz->getExportProductOption($id_packages, $this->id_shop, $this->id_lang));
                        $this->expolde_products     = $this->generate_array_key('ebayofferexport::change_array_keys', 'id_product', '', $this->objectbiz->getEbayExplodeProduct($this->site, $this->id_shop));
                        $this->current_currency     = $this->objectbiz->getCurrentCurrency($this->user_name, $this->id_shop);
                        $this->c_product_references = $this->objectbiz->getCombinationProductReferences($this->user_name, $this->id_shop);
                        $this->product_references   = $this->objectbiz->getProductReferences($this->user_name, $this->id_shop);
                        $category_group             = !empty($category_group['id_category']) ? @implode("', '", $category_group['id_category']) : '';
                        $query_data                 = $this->connect->get_categories_features($this->site, $category_group);
                        if ( !empty($query_data) ) :
                                while ( list($id_category, $is_enabled) = $this->connect->fetch($query_data) ) :
                                        $this->features[$id_category]  = $is_enabled;
                                endwhile;
                        endif;
                        if ( empty($this->itemID) ) :
                                $this->_result['msg']['no_export']     = 1;
                                return $result_insert;
                        endif;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if(!empty($this->product['product']) && is_array($this->product['product'])) :
                                foreach ( $this->product['product'] as $id => $product ) :
	                                if ( $product['id_product'] == 0 ) :
	                                        continue;
	                                endif;
                                        if ( array_key_exists($id, $this->itemID)) {
                                                $_getConfiguration          = isset($getConfiguration[0]) ? (array_key_exists('id_profile', $getConfiguration[0]) ? array($getConfiguration[0]) : $getConfiguration[0]) : '';                                
                                                $this->export_discount_price($this->product['product'][$id]);
                                                $this->export_price_modifier($this->product['product'][$id]);
                                                $this->export_mapping_category($this->product['product'][$id], $category_mappings, 'category');
                                                //export_mapping_category unset product that is under un-mapping category
                                                if ( empty($this->product['product'][$id]) ) {
                                                        continue;
                                                }
                                                $this->export_configuration($this->product['product'][$id], $this->user, $_getConfiguration);
                                                $combination_id = $this->extract_combination($this->product['product'][$id]);
                                                if ( !empty($combination_id) && is_array($combination_id) ) {
                                                    foreach ( $combination_id as $id ) :
                                                            if ( empty($this->product['product'][$id]) ) {
                                                                    continue;
                                                            }
                                                            $result_insert  = $this->slice_product($this->product['product'][$id], $result_insert);
                                                            //slice_product unset product that is quantity = 0, price < 1 not sku ean13 upc referrance
                                                            if ( empty($this->product['product'][$id]) ) {
                                                                    continue;
                                                            }
                                                            if ( isset($this->product['product'][$id]['relist']) ) :
                                                                      $this->export_revise_relists($this->product['product'][$id], $id);
                                                            endif;

                                                    endforeach;
                                                }
                                        }
                                        else {  
                                            unset($this->product['product'][$id]);
                                        }
	                        endforeach;
                        endif;

                        if ( empty($this->product['product'] ) ) :
                                if ( !empty($this->error_request_log) && $this->error_request_log > 0 ) :
                                        unset($data_insert);
                                        $data_insert                                = array(
                                                "batch_id"                          => $this->batch_id,
                                                "id_site"                           => $this->site,
                                                "id_shop"                           => $this->id_shop,
                                                "type"                              => 'Offer',
                                                "method"                            => 'ReviseInventory',
                                                "total"                             => $this->total_request_log,
                                                "success"                           => 0,
                                                "warning"                           => 0,
                                                "error"                             => isset($this->error_request_log) ? $this->error_request_log : 0,
                                                "date_add"                          => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]                      = $data_insert;
                                        $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert, 'Offer', $this->user_name);
                                endif;
                                $this->_result['msg']['empty_products']         = 1;
                                return $result_insert;
                        endif;

                        if ( isset($this->error_request_log) ) :
                                unset($data_insert);
                                $data_insert                                    = array(
                                        "batch_id"                              => $this->batch_id,
                                        "id_site"                               => $this->site,
                                        "id_shop"                               => $this->id_shop,
                                        "type"                                  => 'Offer',
                                        "method"                            	=> 'ReviseInventory',
                                        "total"                                 => $this->total_request_log,
                                        "success"                               => 0,
                                        "warning"                               => 0,
                                        "error"                                 => isset($this->error_request_log) ? $this->error_request_log : 0,
                                        "date_add"                              => date("Y-m-d H:i:s", strtotime("now")), 
                                );
                                $result_insert                                  .= $this->objectbiz->mappingTableOnly("ebay_statistics_log", $data_insert);
                        endif;
                        return $result_insert;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_products($common = 'add', $products = array()) {
                        $result_insert = $this->preparing_export();
                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        $result_insert = '';
                        if ( !empty($this->product_revise) ) : 
                                foreach ($this->product_revise['product'] as $product_id => $product_current ) :
                                        unset($product);
                                        $product['product'][$product_id]        = $product_current;
                                        $this->ebaylib->beforeReviseInventoryStatus($this->site, $product['product']);
                                        $parameters = $this->ebaylib->core->_output;
                                        if ( !empty($parameters) ) :
                                                foreach ( $parameters['InventoryStatus']['_Repeat'] as $parameter ) :
                                                        $parameter['ErrorLanguage'] = $parameters['ErrorLanguage'];
                                                        $parameter['Version']       = $parameters['Version'];
                                                        $this->ebaylib->ReviseInventoryStatus($this->site, $parameter);
//                                                        echo "<pre>", print_r($this->ebaylib->core->_parser, true), "</pre>";
                                                        $result_insert .= $this->export_statistics($this->ebaylib->core->_parser, 'ReviseInventoryStatus', $product_current['name'].'-#-#-'.$product_current['id_product'].'-#-#-'.$product_current['relist'].'-#-#-'.(!empty($product_current['sku']) ? $product_current['sku'] : (!empty($product_current['reference']) ? $product_current['reference'] : '')).'-#-#-'.$this->batch_id);
                                                endforeach;
                                        endif;
                                endforeach;
                        endif;
                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        $this->export_statistics_log();
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_compress() { 
                        $result_insert = $this->preparing_export();
                        if ( !empty($this->_result['msg']['empty_products']) || !empty($this->_result['msg']['no_export']) ) :
                                !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                                return $this->_result;
                        endif;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($this->product_revise) ) {
                                $this->_result['ReviseInventoryStatus']     = $this->ebaylib->Bulk($this->site, $this->product_revise, "ReviseInventoryStatus", "offer", $this->user_name);
                                if ( !empty($this->ebaylib->core->_xmlError) ) :
                                        $validation['ReviseInventoryStatus'] = array(
                                                'error'                     => $this->ebaylib->core->_xmlError,
                                                'product'                   => $this->product_revise['product'],
                                        );
                                        $this->_result['msg']['validation_error']     = 1;
                                        $this->_result['ReviseInventoryStatus']      = 'Failed';
                                endif;
                        }
                        else {
                                $this->_result['msg']['no_export']     = 1;
                                $this->_result['ReviseInventoryStatus']      = 'Failed';
                                return $this->_result;
                        }

                        if ( !empty($validation) && count($validation) ) :
                                $result_insert  = '';
                                foreach ( $validation as $valid_key => $validate ) :
                                        foreach ($validate['product'] as $product_key => $product ) :
                                                $this->error_request_log++;
                                                unset($data_insert);
                                                $data_insert                    = array(
                                                        "batch_id"              => $this->batch_id,
                                                        "id_product"            => isset($product_key) ? $product_key : 0,
                                                        "ebay_user"             => $this->ebayUser,
                                                        "name_product"          => isset($product['name']) ? html_special($product['name']) : 0,
                                                        "response"              => 'Error',
                                                        "type"                  => 'Validation',
                                                        "combination"           => 0, 
                                                        "message"               => $validation[$valid_key]['error'],  
                                                        "date_add"              => date("Y-m-d H:i:s", strtotime("now")), 
                                                );
                                                $result_insert                  .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                        endforeach;
                                endforeach;
                                $this->objectbiz->transaction($this->user_name, true, true);
                        endif;
                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        unset($this->product, $this->product_revise);
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_bulk() {
                        $this->dir_name                                     = USERDATA_PATH.$this->user_name."/ebay/offer/$this->site";
                        $files                                              = array(
                                'AddFixedPriceItem'                         => $this->dir_name.'/AddFixedPriceItem.xml.gz', 
                                'RelistFixedPriceItem'                      => $this->dir_name.'/RelistFixedPriceItem.xml.gz', 
                                'ReviseFixedPriceItem'                      => $this->dir_name.'/ReviseFixedPriceItem.xml.gz',
                                'EndFixedPriceItem'                         => $this->dir_name.'/EndFixedPriceItem.xml.gz', 
                                'VerifyAddFixedPriceItem'                   => $this->dir_name.'/VerifyAddFixedPriceItem.xml.gz', 
                                'ReviseInventoryStatus'                     => $this->dir_name.'/ReviseInventoryStatus.xml.gz',
                        );
                        
                        $this->abortJobCheck();
                        $this->_result = array();
                        $this->_result['AddFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['AddFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['AddFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['AddFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['RelistFixedPriceItem']['createJob'] = 'No Used';
                        $this->_result['RelistFixedPriceItem']['jobID']     = 'No Used';
                        $this->_result['RelistFixedPriceItem']['upload']    = 'No Used';
                        $this->_result['RelistFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['createJob'] = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['jobID']     = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['upload']    = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['EndFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['EndFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['EndFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['EndFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['ReviseInventoryStatus']['createJob'] = 'No Used';
                        $this->_result['ReviseInventoryStatus']['jobID']     = 'No Used';
                        $this->_result['ReviseInventoryStatus']['upload']    = 'No Used';
                        $this->_result['ReviseInventoryStatus']['startUpload']  = 'No Used';
                        foreach ( $files as $type => $file ) :
                        if (file_exists($file)) {
                                if ( strtotime(date("Y-m-d", filemtime($file))) == strtotime(date("Y-m-d", time())) ) :
                                        createUploadJob :
                                        $this->ebaylib->createUploadJob($this->site, $type);
                                        $this->response                         = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure'))) :
                                                if ( $this->response->ack->__toString() == 'Failure') :
                                                        $data_insert                        = array(
                                                                'batch_id'                  => $this->batch_id,
                                                                'id_site'                   => $this->site,
                                                                'id_shop'                   => $this->id_shop,
                                                                'ebay_user'                 => $this->ebayUser,
                                                                'error_id'                  => $this->response->errorMessage->error->errorId->__toString(),
                                                                'error_message'             => html_special($this->response->errorMessage->error->message->__toString()),
                                                                'severity'                  => $this->response->errorMessage->error->severity->__toString(),
                                                                'type'                      => $this->response->errorMessage->error->domain->__toString(),
                                                                'api_call_type'             => $this->response->errorMessage->error->subdomain->__toString(),
                                                                'time'                      => date('Y-m-d H:i:s', strtotime($this->response->timestamp->__toString())),
                                                        );
                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_api_logs", $data_insert, 'offers', $this->user_name);
                                                        if ( $this->_bulk_error < 10 ) :
                                                                $this->_bulk_error++;
                                                                $this->abortJobCheck();
                                                                goto createUploadJob;
                                                        else :
                                                                $this->_result['msg']['bulk_createjob']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                elseif ( $this->response->ack->__toString() == 'Success' ) :
                                                        $data_insert                        = array(
                                                                'jobId'                     => $this->response->jobId->__toString(),
                                                                'fileReferenceId'           => $this->response->fileReferenceId->__toString(),
                                                                'batch_id'                  => $this->batch_id,
                                                                'ebay_user'                 => $this->ebayUser,
                                                                'jobType'                   => $type,
                                                                'jobStatus'                 => 'Created',
                                                                'percentComplete'           => '',
                                                                'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->timestamp->__toString())),
                                                                'startTime'                 => '',
                                                        );
                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert, 'offers', $this->user_name);
                                                        $this->_bulk_error                      = 0;
                                                        $this->_result[$type]['createJob']      = $this->response->ack->__toString();
                                                        $this->ebaylib->_jobID                  = $this->response->jobId->__toString();
                                                        $this->ebaylib->_fileID                 = $this->response->fileReferenceId->__toString();
                                                        $this->fileID                           = $this->response->fileReferenceId->__toString();
                                                        $this->_result[$type]['jobID']          = $this->response->jobId->__toString();
                                                        createUploadFile :
                                                        $this->ebaylib->createUploadFile($this->site, $type, "offer", $this->user_name);
                                                        $this->response                         = $this->ebaylib->core->_parser;
                                                        if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure'))) :
                                                                if ( $this->response->ack->__toString() == 'Failure') :
                                                                        $data_insert                        = array(
                                                                                'batch_id'                  => $this->batch_id,
                                                                                'id_site'                   => $this->site,
                                                                                'id_shop'                   => $this->id_shop,
                                                                                'ebay_user'                 => $this->ebayUser,
                                                                                'error_id'                  => $this->response->errorMessage->error->errorId->__toString(),
                                                                                'error_message'             => html_special($this->response->errorMessage->error->message->__toString()),
                                                                                'severity'                  => $this->response->errorMessage->error->severity->__toString(),
                                                                                'type'                      => $this->response->errorMessage->error->domain->__toString(),
                                                                                'api_call_type'             => $this->response->errorMessage->error->subdomain->__toString(),
                                                                                'time'                      => date('Y-m-d H:i:s', strtotime($this->response->timestamp->__toString())),
                                                                        );
                                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_api_logs", $data_insert, 'offers', $this->user_name);
                                                                        if ( $this->_bulk_error < 10 ) :
                                                                                $this->_bulk_error++;
                                                                                goto createUploadFile;
                                                                        else :
                                                                                $this->_result['msg']['bulk_createfile']     = 1;
                                                                                return $this->_result;
                                                                        endif;
                                                                elseif ( $this->response->ack->__toString() == 'Success' ) :
                                                                        $this->_bulk_error              = 0;
                                                                        $this->_result[$type]['upload'] = $this->response->ack->__toString();
                                                                        startUploadJob :
                                                                        $this->ebaylib->startUploadJob($this->site);
                                                                        $this->response                 = $this->ebaylib->core->_parser;
                                                                        if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure'))) :
                                                                                if ( $this->response->ack->__toString() == 'Failure') :
                                                                                        $data_insert                        = array(
                                                                                                'batch_id'                  => $this->batch_id,
                                                                                                'id_site'                   => $this->site,
                                                                                                'id_shop'                   => $this->id_shop,
                                                                                                'ebay_user'                 => $this->ebayUser,
                                                                                                'error_id'                  => $this->response->errorMessage->error->errorId->__toString(),
                                                                                                'error_message'             => html_special($this->response->errorMessage->error->message->__toString()),
                                                                                                'severity'                  => $this->response->errorMessage->error->severity->__toString(),
                                                                                                'type'                      => $this->response->errorMessage->error->domain->__toString(),
                                                                                                'api_call_type'             => $this->response->errorMessage->error->subdomain->__toString(),
                                                                                                'time'                      => date('Y-m-d H:i:s', strtotime($this->response->timestamp->__toString())),
                                                                                        );
                                                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_api_logs", $data_insert, 'offers', $this->user_name);
                                                                                        if ( $this->_bulk_error < 10 ) :
                                                                                                $this->_bulk_error++;
                                                                                                goto startUploadJob;
                                                                                        else :
                                                                                                $this->_result['msg']['bulk_startupload']     = 1;
                                                                                                return $this->_result;
                                                                                        endif;
                                                                                elseif ( $this->response->ack->__toString() == 'Success' ) :
                                                                                        $this->_bulk_error      = 0;
                                                                                        $this->_result[$type]['startUpload'] = $this->response->ack->__toString();
                                                                                        $this->ebaylib->getJobStatus($this->site);
                                                                                        $this->response                     = $this->ebaylib->core->_parser;
                                                                                        if ( !empty($this->response) && !empty($this->response->jobProfile->jobId) ) :
                                                                                                $data_insert                        = array(
                                                                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                                                                        'fileReferenceId'           => $this->fileID,
                                                                                                        'batch_id'                  => $this->batch_id,
                                                                                                        'ebay_user'                 => $this->ebayUser,
                                                                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                                                                );
                                                                                                $data['data_insert'][]              = $data_insert;
                                                                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert, 'offers', $this->user_name);
                                                                                        endif;
                                                                                endif;
                                                                        endif;
                                                                endif;
                                                        endif;
                                                endif;
                                        endif;
                                endif;
                        }
                        endforeach;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_download($percent = '', $date = "now") {
                        $job_task                                           = $this->objectbiz->getJobTask($this->user_name, 'offers', $this->ebayUser, 'now');
                        $this->_result                                      = array();
                        $this->_result['AddFixedPriceItem']                 = 'No Used';
                        $this->_result['RelistFixedPriceItem']              = 'No Used';
                        $this->_result['ReviseFixedPriceItem']              = 'No Used';
                        $this->_result['EndFixedPriceItem']                 = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']           = 'No Used';
                        $this->_result['ReviseInventoryStatus']             = 'No Used';
                        if (!empty($job_task)) :
                        foreach ( $job_task as $task ) :
                                $this->ebaylib->_jobID                      = $task['jobId'];
                                while ( $task['jobId'] ) :
                                        getJobStatus :
                                        $this->ebaylib->getJobStatus($this->site);
                                        $this->response                     = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure'))) :
                                                if ( $this->response->ack->__toString() == 'Failure') :
                                                        $data_insert                        = array(
                                                                'batch_id'                  => $this->batch_id,
                                                                'id_site'                   => $this->site,
                                                                'id_shop'                   => $this->id_shop,
                                                                'ebay_user'                 => $this->ebayUser,
                                                                'error_id'                  => $this->response->errorMessage->error->errorId->__toString(),
                                                                'error_message'             => html_special($this->response->errorMessage->error->message->__toString()),
                                                                'severity'                  => $this->response->errorMessage->error->severity->__toString(),
                                                                'type'                      => $this->response->errorMessage->error->domain->__toString(),
                                                                'api_call_type'             => $this->response->errorMessage->error->subdomain->__toString(),
                                                                'time'                      => date('Y-m-d H:i:s', strtotime($this->response->timestamp->__toString())),
                                                        );
                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_api_logs", $data_insert, 'offers', $this->user_name);
                                                        if ( $this->_bulk_error < 10 ) :
                                                                $this->_bulk_error++;
                                                                goto getJobStatus;
                                                        else :
                                                                return $this->_result;
                                                        endif;
                                                elseif ( $this->response->ack->__toString() == 'Success' ) :
                                                        $this->_bulk_error              = 0;
                                                        $data_insert                        = array(
                                                                'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                                'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                                'batch_id'                  => $this->batch_id,
                                                                'ebay_user'                 => $this->ebayUser,
                                                                'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                                'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                                'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                                'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                                'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                        );
                                                        $data['data_insert'][]              = $data_insert;
                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert, 'offers', $this->user_name);
                                                        $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                        $percentComplete                    = $this->response->jobProfile->percentComplete->__toString();
                                                        $jobStatus                          = $this->response->jobProfile->jobStatus->__toString();
                                                        if ( !empty($percent) ) :
                                                                $percent->set_running_task($percentComplete);
                                                        endif;
                                                endif;
                                        endif;
                                        if ( (!empty($percentComplete) && $percentComplete == '100.0') || (!empty($jobStatus) && in_array($jobStatus, array('Failed', 'Aborted')))) :
                                                $task['jobId']                      = 0;
                                                $data_insert                        = array(
                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                        'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                        'batch_id'                  => $this->batch_id,
                                                        'ebay_user'                 => $this->ebayUser,
                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                );
                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert, 'offers', $this->user_name);
                                                $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                startDownloadFile :
                                                $this->ebaylib->startDownloadFile($this->site, $task['jobType'], "offer", $this->user_name);
                                                $this->response                     = $this->ebaylib->core->_parser;
                                                if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure') )) :
                                                        $this->_bulk_error              = 0;
                                                        $this->_result[$task['jobType']] = $this->response->ack->__toString();
                                                        if ( $this->response->ack->__toString() == 'Success' ) :
                                                                $this->ebaylib->getDownloadFile($this->site, $task['jobType'], "offer", $this->user_name);
                                                                $result_insert = '';
//                                                                $this->batch_id         = '54fe79e2e296d';
                                                                if ( !empty($this->ebaylib->core->_parser) ) :
                                                                        foreach ( $this->ebaylib->core->_parser as $parse ) :
                                                                                $result_insert .= $this->export_statistics($parse, $task['jobType']);
                                                                        endforeach;
                                                                        $logs = $this->export_statistics_sql();
//                                                                        file_put_contents($this->dir_name.'/sql.txt', print_r($result_insert.$this->flag_sql.$logs));
                                                                        $this->objectbiz->transaction($result_insert.$this->flag_sql.$logs, true, true);
                                                                endif;
                                                        endif;
                                                else :
                                                        if ( $this->_bulk_error < 3 ) :
                                                                $this->_bulk_error++;
                                                                goto startDownloadFile;
                                    			else :
                                                                $this->_result['msg']['bulk_download']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                endif;
                                                
                                                if(in_array($jobStatus, array('Failed', 'Aborted'))) :
                                                	$this->_result['msg']['job_failure'] = 1;
                                                endif;
                                        else :  sleep(5);
                                        endif;
                                endwhile;
                        endforeach;
                        endif;
                        return $this->_result;
                }
                
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function extrack_download($task = 'ReviseInventoryStatus') {
                        $this->ebaylib->getDownloadFile($this->site, $task, "offer", $this->user_name);
                        $result_insert = '';
                        if ( !empty($this->ebaylib->core->_parser) ) :
                                foreach ( $this->ebaylib->core->_parser as $parse ) :
                                        $result_insert .= $this->export_statistics($parse, $task);
                                endforeach;
                                $logs = $this->export_statistics_sql();
                                $this->objectbiz->transaction($result_insert.$this->flag_sql.$logs, true, true);
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        $this->user_name                = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                  = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                     = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->cron                     = empty($userdata) ? FALSE : TRUE;
                        parent::__construct();
                        $this->dir_server               = DIR_SERVER;
                        $this->objectbiz                = new ObjectBiz(array($this->user_name));
                        $this->connect                  = new connect();
                        $this->eucurrency               = new Eucurrency();
                        $shop_default                   = $this->objectbiz->getDefaultShop($this->user_name);
                        if ( empty($shop_default) ) :
                                $this->_result['msg']['empty_products']         = 1;
                                return $this->_result;
                        endif;
                        $this->id_shop                  = (int)$shop_default['id_shop'];
                        $this->user_conf                = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                     = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        list($this->user_email)         = $this->connect->fetch($this->connect->get_user_mail($this->user_id));
                        $this->site                     = isset($this->user['site']) ? $this->user['site'] : 0;
                        $site_result                    = $this->connect->fetch($this->connect->get_ebay_site_by_id($this->site));
                        $this->id_lang                  = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                        $this->language                 = $this->objectbiz->getDefaultISO($this->user_name, $this->id_shop);
                        $this->currency                 = $site_result['currency'];
                        $this->country                  = strtoupper($site_result['iso_code']);
                        $this->ebayUser                 = $this->user_conf['userID'];
                        $this->discount                 = !empty($this->user['discount']) ? $this->user['discount'] : 0;
                        $this->ebaylib                  = new ebaylib( (!empty($this->user_conf['appMode']) && $this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        if ( empty($this->user_conf['token']) ) :
                                $this->_result['msg']['empty_token']    = 1;
                                return $this->_result;
                        endif;
                        $this->ebaylib->token           = $this->user_conf['token'];
                        $this->ebaylib->core->_template = !empty($this->user['template']) ? $this->user['template'] : 1;
                        $this->batch_id                 = uniqid();
                        $this->mode                     = unserialize(base64_decode($this->user_conf['feed_mode']));
                        $result_log                     = $this->objectbiz->checkBatchID($this->batch_id);
                        while( !empty($result_log) ) :
                                $this->batch_id         = uniqid();
                                $result_log             = $this->objectbiz->checkBatchID($this->batch_id);
                        endwhile;
                        $this->objectbiz->checkDB("ebay_job_task");
                        $this->objectbiz->checkDB("ebay_api_logs");
                        $this->dir_name                 = USERDATA_PATH.$this->user_name."/ebay/offer/$this->site";
                }
                
        }
//                $a = new ebayOfferExport(array('goohara', '108', 0));
//                $a->export_compress();
