<?php   if ( !file_exists(dirname(__FILE__).'/database.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect_config.php')) exit('No direct script access allowed');
//         if ( class_exists('database') == FALSE ) require dirname(__FILE__).'/database.php';
        if ( class_exists('ci_db_connect') == FALSE ) require dirname(__FILE__).'/../../../libraries/ci_db_connect.php';
        require dirname(__FILE__).'/connect_config.php';
        
        class connect extends ci_db_connect {
		    	private $ERROR_MESSAGES = array();
		    	public static $ERR_SKU_MUST_BE_PROVIDED = "FB0001";
		    	public static $ERR_SKU_MUST_BE_UNIQUE = "FB0002";
		    	public static $ERR_EAN13_MUST_BE_UNIQUE = "FB0003";
		    	public static $ERR_UPC_MUST_BE_UNIQUE = "FB0004";
		    	public static $ERR_PRODUCT_REFERENCE_ID_MUST_BE_UNIQUE = "FB0005";
		    	public static $ERR_THIS_CATEGORY_CAN_NOT_EXPORT = "FB0006";
		    	public static $ERR_DISPATCH_TIME_MUST_BE_PROVIDED = "FB0007";
		    	public static $ERR_LISTING_DURATION_MUST_BE_PROVIDED = "FB0008";
		    	public static $ERR_POSTAL_CODE_MUST_BE_PROVIDED = "FB0009";
		    	public static $ERR_COUNTRY_MUST_BE_PROVIDED = "FB0010";
		    	public static $ERR_NO_VALID_SITE_ID_SPECIFIED_IN_THE_REQUEST = "FB0011";
		    	public static $ERR_CONDITION_MUST_BE_PROVIDED = "FB0012";
		    	public static $ERR_CONDITION_S_MUST_BE_MAPPING = "FB0013";
		    	public static $ERR_CARRIER_NOT_FOUND_FOR_PRODUCT_NAME_S = "FB0014";
// 		    	public static $ERR_SKU_MUST_BE_PROVIDED_FOR_COMBINATION_ID_S = "FB0015";
// 		    	public static $ERR_PRODUCT_IS_OUT_OF_STOCK_FOR_COMBINATION_ID_S = "FB0016";
// 		    	public static $ERR_PRICE_IS_NOT_VALID_FOR_COMBINATION_ID_S = "FB0017";
// 		    	public static $ERR_COMBINATION_SKU_MUST_BE_UNIQUE_S = "FB0018";
// 		    	public static $ERR_COMBINATION_EAN13_MUST_BE_UNIQUE = "FB0019";
// 		    	public static $ERR_COMBINATION_UPC_MUST_BE_UNIQUE = "FB0020";
// 		    	public static $ERR_COMBINATION_REFERENCE_ID_MUST_BE_UNIQUE = "FB0021";
		    	public static $ERR_PRODUCT_IS_OUT_OF_STOCK = "FB0015";
		    	public static $ERR_ATTRIBUTE_CONFIGURATION_CONFLICT_FOR_COMBINATION_ID_S = "FB0016";
// 		    	public static $ERR_IMAGE_IS_REQUIRED_FOR_COMBINATION_ID_S = "FB0017";
		    	public static $ERR_THE_COMBINATIONS_ARE_NOT_VALID = "FB0017";
		    	public static $ERR_PRICE_IS_NOT_VALID = "FB0018";
		    	public static $ERR_PICTURE_IMAGE_MUST_BE_PROVIDED = "FB0019";
		    	public static $ERR_QUANTITY_IS_NOT_VALID = "FB0020";
		    	public static $ERR_PRODUCT_WAS_DISABLED = "FB0021";
		    	public static $ERR_TITLE_MUST_BE_PROVIDED = "FB0022";
        		
        	
                public function __construct() {
                        $this->host                             = DATABASEHOST;
                        $this->connectdb(DBNAME_DB, USERNAME_DB, PASSWORD_DB, $this->host);
                }
        
                public function get_configuration($id_user = null) {
                    if (!isset($id_user)) :
                        return FALSE;
                    endif;
                    $sql = "SELECT * FROM configuration Where id_customer = '" . $id_user . "'";
                    return $this->select_query($sql);
                }


                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_configuration($id_user = null, $id_site = null){
                        if ( !isset($id_user) || !isset($id_site) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT * FROM ebay_configuration Where id_customer = '$id_user' AND id_site = '".$id_site."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_site_by_id($id_site = null){
                        if ( !isset($id_site) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT * FROM ebay_sites Where id_site = '".$id_site."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_site_by_name($name = null){
                        if ( !isset($name) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_site FROM ebay_sites Where name = '".$name."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_site(){
                        $sql                                        = "SELECT id_site FROM ebay_sites ORDER BY id_site ASC";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_currency($currency = null){
                        if ( !isset($currency) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT currency FROM ebay_sites Where name = '".$currency."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_id_offers_packages($name_offer_pkg = null){
                        if ( !isset($name_offer_pkg) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_offer_pkg FROM offer_packages Where name_offer_pkg = '".$name_offer_pkg."'";
                        return $this->select_query($sql);
                }  
                
                 
                function get_packages_from_site($id_site = null, $marketplace_id = null) {
                        if ( (!isset($id_site) && empty($id_site)) ||  (!isset($marketplace_id) && empty($marketplace_id)) ) : 
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT `id_offer_sub_pkg` as id_packages FROM (`offer_price_packages`) WHERE `id_site_ebay` = '".$id_site."' AND `id_offer_pkg` = '".$marketplace_id."' AND `offer_pkg_type` IN ('0', '1')";
                        return $this->select_query($sql);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_user_mail($id_user = null){
                        if ( !isset($id_user) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT user_email FROM users WHERE id = '".$id_user."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_full_name($id_user = null){
                        if ( !isset($id_user) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT user_first_name, user_las_name FROM users WHERE id = '".$id_user."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_categories_ebay_site($id_site = null) {
                        if ( !isset($id_site) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_category, id_site FROM ebay_categories_sites where id_category > '0' and id_site = ".$id_site." group by id_category, id_site";
    //                    $sql                                        = "SELECT id_category, id_site FROM ebay_categories_sites c1 WHERE  NOT EXISTS (SELECT id_category FROM ebay_categories_sites1 c2 WHERE c1.id_category = c2.id_category) AND id_category > '0' group by id_category, id_site";
//                          $sql                                        = "select `c1`.`id_category` AS `id_category`,`c1`.`id_site` AS `id_site` from `ebay_categories_sites1` `c1` where ((not(exists(select `c2`.`id_category`,`c2`.`id_site` from `ebay_categories_sites` `c2` where ((`c1`.`id_category` = `c2`.`id_category`) and (`c1`.`id_site` = `c2`.`id_site`))))) and (not(exists(select `c3`.`id_category`,`c3`.`id_site` from `ebay_categories_features` `c3` where ((`c1`.`id_category` = `c3`.`id_category`) and (`c1`.`id_site` = `c3`.`id_site`))))) and (`c1`.`id_category` > '0')) group by `c1`.`id_category`,`c1`.`id_site`";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_categories_ebay_by_site($id_site = null){
                        if ( !isset($id_site) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_category FROM ebay_categories_sites where id_category > '0' AND id_site = '".$id_site."' group by id_category";
                        return $this->select_query($sql);
                } 


                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_attributes_all_values(){

                        $sql                                        = "SELECT * FROM ebay_attributes_value1 where 1";
                        return $this->select_query($sql);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_attributes_all_modes(){

                        $sql                                        = "SELECT * FROM ebay_attributes_mode1 where 1";
                        return $this->select_query($sql);
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_attributes_all_groups(){

                        $sql                                        = "SELECT * FROM ebay_attributes_group1 where 1";
                        return $this->select_query($sql);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_attributes_group($name = null){
                        if ( !isset($name) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_group FROM ebay_attributes_group1 where `name` = '".$name."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_selection_mode($mode = null){
                        if ( !isset($mode) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_selection_mode FROM ebay_attributes_mode1 where `selection_mode` = '".$mode."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_shipping_default($id_site = null){
                        if ( (!isset($id_site) && empty($id_site)) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_shipping, service, shipping_name FROM ebay_shipping_default where `id_site` = '".$id_site."' and is_enabled = '1'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_attributes_value($id_group = null, $value = null){
                        if ( !isset($id_group) || !isset($value) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_attribute FROM ebay_attributes_value1 WHERE `id_group` = '".$id_group."' AND `value` = '".$value."'";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_categories_features($id_site = null, $id_categories = null){
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_categories) && empty($id_categories)) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_category, is_enabled FROM ebay_categories_features WHERE `id_site` = '".$id_site."' AND `id_category` IN ('".$id_categories."')";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_conditions_values($id_site = null, $id_categories = null){
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_categories) && empty($id_categories)) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "SELECT id_category, condition_value FROM ebay_condition_values WHERE `id_site` = '".$id_site."' AND `id_category` IN (".$id_categories.")";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_last_modifier_order($id_customer = null, $id_site = null, $date = null){
                        if ( (!isset($id_site) && empty($id_site)) || (!isset($id_customer) && empty($id_customer)) || (!isset($date) && empty($date)) ) :
                                return FALSE;
                        endif;
                        $sql                                        = "REPLACE INTO ebay_configuration (id_customer, name, value, id_site, config_date) VALUES ('$id_customer', 'EBAY_MOD_ORDERS', '$date', '$id_site', '".date("Y-m-d H:i:s", strtotime("now"))."')";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_ebay_authorization($id_customer = null) {
                        $where = '';
                        if ( (!isset($id_customer) && empty($id_customer)) ) :
                                $where = "AND id_customer = '".$id_customer."'";
                        endif;
                        $sql                                        = "SELECT * FROM ebay_configuration Where `name` = 'EBAY_SITE_ID' $where";
                        return $this->select_query($sql);
                }  

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function create_table($table = null){
                        if ( !isset($table) ) :
                                return FALSE;
                        endif;
                        $sql['ebay_categories_features']            = "CREATE TABLE ebay_categories_features(id_category int(11) NOT NULL, id_site int(11) NOT NULL, is_enabled enum('0','1') NOT NULL DEFAULT '0', date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site))";
                        $sql['ebay_attributes']                     = "CREATE TABLE ebay_attributes1(id_category int(11), id_group int(11), id_site int(11), id_selection_mode int(11), date_add datetime NOT NULL,enable_validation tinyint default '1', PRIMARY KEY (id_category, id_group, id_site))";
                        $sql['ebay_attributes_group']               = "CREATE TABLE ebay_attributes_group1(id_group int(11) NOT NULL AUTO_INCREMENT, name varchar(255) NOT NULL, id_site int(11) NOT NULL, PRIMARY KEY (id_group))";
                        $sql['ebay_attributes_value']               = "CREATE TABLE ebay_attributes_value1(id_attribute int(11) NOT NULL AUTO_INCREMENT, id_site int(11) NOT NULL, id_group int(11) NOT NULL, value varchar(255) NOT NULL, PRIMARY KEY (id_attribute))";
                        $sql['ebay_attributes_mode']                = "CREATE TABLE ebay_attributes_mode1(id_selection_mode int(11) NOT NULL AUTO_INCREMENT, selection_mode varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'FreeText, Prefilled, SelectionOnly', PRIMARY KEY (id_selection_mode))";
                        $sql['ebay_listing_durations']              = "CREATE TABLE ebay_listing_durations(id int(11) NOT NULL AUTO_INCREMENT, listing_type varchar(30) NOT NULL, id_site int(11) NOT NULL, duration_set_id int(11), PRIMARY KEY (id, listing_type, id_site))";
                        $sql['ebay_listing_durations_set']          = "CREATE TABLE ebay_listing_durations_set(duration_set_id int(11) NOT NULL, duration varchar(20) NOT NULL, PRIMARY KEY (duration_set_id, duration))";
                        $sql['ebay_payment_method']                 = "CREATE TABLE ebay_payment_method(id int(11) NOT NULL AUTO_INCREMENT, id_site int(11) NOT NULL, payment_method varchar(30) NOT NULL, PRIMARY KEY (id, id_site))";
                        $sql['ebay_condition_values']               = "CREATE TABLE ebay_condition_values(id_category int(11) NOT NULL, id_site int(11) NOT NULL, condition_value int(11) NOT NULL, condition_name varchar(50) NOT NULL, is_enabled enum('0','1') NOT NULL DEFAULT '1', date_add datetime NOT NULL, PRIMARY KEY (id, id_site))";
                        $sql['ebay_categories']                     = "CREATE TABLE ebay_categories1(id_category int(11) NOT NULL, id_site int(11) NOT NULL, id_parent int(11) NOT NULL, is_root_category enum('0','1') NOT NULL DEFAULT '0', date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site))";
                        $sql['ebay_categories_sites']               = "CREATE TABLE ebay_categories_sites1(id_category int(11) NOT NULL, id_site int(11) NOT NULL, level int(11) NOT NULL, name varchar(255) NOT NULL, date_add datetime NOT NULL, PRIMARY KEY (id_category, id_site, level))";

                        return $this->select_query($sql[$table]);
                }  
                
                public function ebay_error_resolution($error_code, $lang){
                	if(!isset($this->ERROR_MESSAGES[$lang])){
                		$table = 'ebay_error_resolutions';
			                		$sql = "SELECT *
			                		FROM (`$table`) WHERE error_lang = '".$lang."'";
                		$query = $this->select_query($sql);
                		while ($row = $this->fetch($query)){
                			$this->ERROR_MESSAGES[$row['error_lang']][$row['error_code']] = $row;
                		}
                	}
                	
                	return isset($this->ERROR_MESSAGES[$lang]) && isset($this->ERROR_MESSAGES[$lang][$error_code]) ? $this->ERROR_MESSAGES[$lang][$error_code] : array('error_code' => $error_code, 'error_details' => 'EMPTY MESSAGE.');
                }
        }

