<?php   echo header('Content-Type: text/html; charset=utf-8');
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        
//        set_error_handler("ErrorHandler");
//
//        function ErrorHandler($errno, $errstr, $errfile, $errline) {
//                $STDERR = fopen('php://stderr', 'w+');
//                fwrite($STDERR, "$errstr in $errfile on $errline\n"); 
//        }
        
        if ( !file_exists(dirname(__FILE__).'/ObjectBiz.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/ebaysynchronization.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../Swift/swift_required.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../eucurrency.php')) exit('No direct script access allowed');
        if ( class_exists('ObjectBiz')      == FALSE ) require dirname(__FILE__).'/ObjectBiz.php';
        if ( class_exists('ebaylib')        == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')        == FALSE ) require dirname(__FILE__).'/connect.php';
        if ( class_exists('ebaysynchronization')        == FALSE ) require dirname(__FILE__).'/ebaysynchronization.php';
        if ( class_exists('Swift')          == FALSE ) require dirname(__FILE__).'/../Swift/swift_required.php';
        if ( class_exists('Eucurrency')     == FALSE ) require dirname(__FILE__).'/../eucurrency.php';
        require dirname(__FILE__).'/../tools.php';
        
        class ebayexport extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $language;
                protected $site;
                protected $id_shop;
                protected $objectbiz;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                public $product;
                protected $product_relist;
                protected $product_revise;
                protected $product_add;
                protected $product_references;
                protected $_result;
                protected $_email = 'support@feed.biz';
                protected $dir_server;
                protected $flag_sql = '';
                protected $_bulk_error = 0;
                protected $id_marketplace = 3;
                protected $comb_count = array();
                protected $compress_config = 'export_product_array';
                protected $out_of_stock_min;
                protected $out_of_stock_created = 0;
                protected $maximum_quantity;

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function slice_product($product = null, $result_insert = '') {
                        if ( empty($product) ) : 
                                return;
                        endif;
                        $this->total_request_log                        = !empty($this->total_request_log)     ? $this->total_request_log     : 0;
                        $this->error_request_log                        = !empty($this->error_request_log)     ? $this->error_request_log     : 0;
                        $id                                             = !empty($product['id_product_reference']) ? $product['id_product_reference'] : $product['id_product'];
                        $this->total_request_log++;
                        //SKU//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['sku']) && empty($product['reference']) ) :
                                $messages[$id][]                    = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_PROVIDED, $this->language);
                        else :
                                unset($product_sku);
                                $product_sku                            = !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : '');
                        endif;
                        //SKU Unique///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product['sku']) && count($product['sku']) ) :
                                $sku_count                              = isset($this->product_references['sku'][$product['sku']]) ? $this->product_references['sku'][$product['sku']] : 0;
                                if ( !empty($sku_count) && $sku_count > 1 ) :
                                	$messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_UNIQUE, $this->language);
                                endif;
                        endif;
                        if ( !empty($product['reference']) && count($product['reference']) ) :
                                $reference_count                        = isset($this->product_references['reference'][$product['reference']]) ? $this->product_references['reference'][$product['reference']] : 0;
                                if ( !empty($reference_count) && $reference_count > 1 ) :
                                        $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_REFERENCE_ID_MUST_BE_UNIQUE, $this->language);
                                endif;
                        endif;
                        //Category/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['id_category_default']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_THIS_CATEGORY_CAN_NOT_EXPORT, $this->language);
                        endif;
                        //Dispatch Time/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['name']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_TITLE_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //Dispatch Time/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !isset($product['dispatch_time']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_DISPATCH_TIME_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //Listing Duration//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['listing_duration']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_LISTING_DURATION_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //Postal Code///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['postal_code']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_POSTAL_CODE_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //Country///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['country']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_COUNTRY_MUST_BE_PROVIDED, $this->language);
                        endif;
                        //Ebay Site/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['site']) && !is_numeric($product['site']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_NO_VALID_SITE_ID_SPECIFIED_IN_THE_REQUEST, $this->language);
                        endif;
                        //Ebay Site/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['condition']) ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_CONDITION_MUST_BE_PROVIDED, $this->language);
                        endif;
                        if ( empty($product['active']) && $this->common == 'add' ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_WAS_DISABLED, $this->language);
                        endif;
                        if ( !empty($product['condition_error_mapping']) ) :
                                foreach ( $product['condition_error_mapping'] as $condition_error) :
                                        $tmpError                       = $this->connect->ebay_error_resolution(connect::$ERR_CONDITION_S_MUST_BE_MAPPING, $this->language);
                                        $tmpError['error_details']      = sprintf($tmpError['error_details'], $condition_error);
                                        $messages[$id][]                = $tmpError;
                                endforeach;
                        endif;
                        //Ebay Site/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($product['carrier_domestic']) ) :
                                $tmpError                               = $this->connect->ebay_error_resolution(connect::$ERR_CARRIER_NOT_FOUND_FOR_PRODUCT_NAME_S, $this->language);
                                $tmpError['error_details']              = sprintf($tmpError['error_details'], $product['name']);
                                $messages[$id][]                        = $tmpError;
                        endif;
                        if ( !empty($product['disable']) && $product['disable'] == 1 ) :
                                $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_WAS_DISABLED, $this->language);
                        endif;
                        //Quantity, Price, Image////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product['combination']) ) :
                                $check_quantity                         = 0;
                                $check_price                            = 0;
                                $check_image                            = 0;
                                $check_name_attr                        = 0;                    
                                $check_value_attr                       = 0;                    
                                $main_attribute                         = array();
                                $comb_messages                          = array();
                                foreach ( $product['combination'] as $combination_id => $combination_product ) :
                                        $combination_price              = 0;
                                        $check_quantity                 += $combination_product['quantity'];
                                        $combination_price              += (isset($product['price']) ? $product['price'] : 0) + (isset($combination_product['price']) ? $combination_product['price'] : 0);
                                        $check_price                    += $combination_price;
                                        if ( empty($combination_product['sku']) && empty($combination_product['reference']) ) :
                                                $comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_PROVIDED, $this->language);
                                        else :
                                                unset($combination_sku);
                                                $combination_sku        = !empty($combination_product['sku']) ? $combination_product['sku'] : (!empty($combination_product['reference']) ? $combination_product['reference'] : '');
                                        endif;
                                        if ( $this->common == 'add' && $combination_product['quantity'] <= 0 && !$this->out_of_stock_created ) :
                                		$comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_IS_OUT_OF_STOCK, $this->language);
                                        endif;
                                        if ( !empty($combination_product['disable']) && $combination_product['disable'] == 1 ) :
                                		$comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_WAS_DISABLED, $this->language);
                                        endif;
                                        if ( $combination_price < 1 ) :
                                		$comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PRICE_IS_NOT_VALID, $this->language);
                                        endif;
                                        //SKU, EAN13, UPC, REFERENCE UNIQUE////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        if ( !empty($combination_product['sku']) && count($combination_product['sku']) ) :
                                                $sku_count                              = isset($this->c_product_references['sku'][$combination_product['sku']]) ? $this->c_product_references['sku'][$combination_product['sku']] : 0;
                                                if ( !empty($sku_count) && $sku_count > 1 ) :
                                                        $comb_messages[$id][$combination_id][]  = $this->connect->ebay_error_resolution(connect::$ERR_SKU_MUST_BE_UNIQUE, $this->language);
                                                endif;
                                        endif;
                                        
                                        if ( !empty($combination_product['reference']) && count($combination_product['reference']) ) :
                                                $reference_count                        = isset($this->c_product_references['reference'][$combination_product['reference']]) ? $this->c_product_references['reference'][$combination_product['reference']] : 0;
                                                if ( !empty($reference_count) && $reference_count > 1 ) :
                                                        $comb_messages[$id][$combination_id][]  = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_REFERENCE_ID_MUST_BE_UNIQUE, $this->language);
                                                endif;
                                        endif;
                                        if ( !empty($this->comb_count) && !empty($combination_product['attributes']) && count($combination_product['attributes']) != max(array_flip($this->comb_count)) ) :
                                                $tmpError                                       = $this->connect->ebay_error_resolution(connect::$ERR_ATTRIBUTE_CONFIGURATION_CONFLICT_FOR_COMBINATION_ID_S, $this->language);
                                                $tmpError['error_details']                      = sprintf($tmpError['error_details'], $combination_id);
                                                $comb_messages[$id][$combination_id][]          = $tmpError;
                                        endif;
                                        if ( !$this->user['no_image'] ) :
                                                if ( empty($product['images']) && empty($combination_product['images']) ) :
                                                        $comb_messages[$id][$combination_id][]          = $this->connect->ebay_error_resolution(connect::$ERR_PICTURE_IMAGE_MUST_BE_PROVIDED, $this->language);
                                                else :
                                                        $check_image++;
                                                endif;
                                        endif;
                                        if ( !empty($comb_messages[$id][$combination_id]) ) :
                                                unset($this->product['product'][$id]['combination'][$combination_id], $data_combination, $this->product['product'][$id]['combinations'][$combination_id]);
                                                foreach($comb_messages[$id][$combination_id] as $message_object):
	                                                $data_combination           = array(
	                                                        "batch_id"          => $this->batch_id,
	                                                        "id_product"        => isset($id) ? $id : 0,
	                                                        "id_site"           => $this->site,
	                                                        "id_shop"           => $this->id_shop,
	                                                        "combination"       => 1,
                                                                "ebay_user"         => $this->ebayUser,
                                                                "sku_export"        => !empty($combination_product['sku']) ? $combination_product['sku'] : (!empty($combination_product['reference']) ? $combination_product['reference'] : ''),
	                                                        "response"          => 'Error',
	                                                        "type"              => 'Compressed',
                                                                "code"              => $message_object['error_code'],
                                                                "name_product"      => html_special($product['name']),
	                                                        "message"           => html_special(strip_tags($message_object['error_details'])),
	                                                        "id_combination"    => isset($combination_id) ? $combination_id : 0,
	                                                        "date_add"          => date("Y-m-d H:i:s", strtotime("now")), 
	                                                );
	                                                $result_insert              .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_combination);
                                                endforeach;
                                        endif;
                                endforeach;
                                
                                if ( empty($this->product['product'][$id]['combination']) || empty($this->product['product'][$id]['combinations']) ) :
                                        $messages[$id][]                        = $this->connect->ebay_error_resolution(connect::$ERR_THE_COMBINATIONS_ARE_NOT_VALID, $this->language);
                                endif;
                        else :
                                if ( $product['quantity'] <= 0 || $product['price'] < 1 || (!$this->user['no_image'] && empty($product['images'])) ) :
                                        if ( $this->common == 'add' && $product['quantity'] <= 0 && !$this->out_of_stock_created) :
                                                $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PRODUCT_IS_OUT_OF_STOCK, $this->language);
                                        endif;
                                        if ( $product['price'] < 1 ) :
                                                $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PRICE_IS_NOT_VALID, $this->language);
                                        endif;
                                        if ( !$this->user['no_image'] && empty($product['images']) ) :
                                                $messages[$id][]                = $this->connect->ebay_error_resolution(connect::$ERR_PICTURE_IMAGE_MUST_BE_PROVIDED, $this->language);
                                        endif;
                                endif;
                        endif;

                        if ( !empty($messages) && count($messages) ) : 
                                unset($data_insert, $data_combination);
                                foreach ( $messages as $messages_id => $message) :
                                    $this->error_request_log++;
                                	foreach($message as $message_object):
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_product"                => isset($messages_id) ? $messages_id : 0,
                                                "id_combination"            => 0,
                                                "combination"               => 0,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "ebay_user"                 => $this->ebayUser,
                                                "sku_export"                => !empty($product['sku']) ? $product['sku'] : (!empty($product['reference']) ? $product['reference'] : ''),
                                                "name_product"              => html_special($product['name']),
                                                "response"                  => 'Error',
                                                "type"                      => 'Compressed',
                                                "code"                      => $message_object['error_code'],
                                                "message"                   => html_special(strip_tags($message_object['error_details'])),  
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $result_insert                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                    endforeach;
                                    unset($this->product['product'][$messages_id]);
                                endforeach;
                        endif;//echo "<pre>", print_r($messages, true), "</pre>";
                        return $result_insert;
                }   

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_configuration($product = null, $configs = '', $templates = '', $domestic_shippings = '', $international_shippings = '', $condition = '', $postalcode = null, $configuration = null, $return_policy = null, $profile_payment = null) {
                        if ( empty( $product ) ) : 
                                return;
                        endif;
                        $dir_name                                                                                       = USERDATA_PATH.$this->user_name."/template/";
                        //Configs//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($configs) ) :
                                foreach ( $configs as $key => $config ) :
                                        if ( $key == 'site' || $key == 'address' || $key == 'listing_duration' ) :
                                                $this->product['product'][$product['id_product']][$key]                 = $config;
                                        endif;
                                endforeach;
                                $this->product['product'][$product['id_product']]['name']                               = !empty($this->product['product'][$product['id_product']]['name'][$this->language]) ? $this->product['product'][$product['id_product']]['name'][$this->language] : '';
                                $this->product['product'][$product['id_product']]['description']                        = !empty($this->product['product'][$product['id_product']]['description'][$this->language]) ? $this->product['product'][$product['id_product']]['description'][$this->language] : '';
                                $this->product['product'][$product['id_product']]['description_short']                  = !empty($this->product['product'][$product['id_product']]['description_short'][$this->language]) ? $this->product['product'][$product['id_product']]['description_short'][$this->language] : '';
                                $this->product['product'][$product['id_product']]['currency']                           = $this->currency;
                                $this->product['product'][$product['id_product']]['country']                            = $this->country;
                                $this->product['product'][$product['id_product']]['batch_id']                           = $this->batch_id;
                                $this->product['product'][$product['id_product']]['description']                        = preg_replace_callback('/<script\b(.*?)>(.*?)*<\/script>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description']);
                                $this->product['product'][$product['id_product']]['description']                        = preg_replace_callback('/<script\b(.*?)*>(.*?)<\/script>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description']);
                                $this->product['product'][$product['id_product']]['description']                        = preg_replace_callback('/<meta\b(.*?)*>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description']);
                                $this->product['product'][$product['id_product']]['description']                        = preg_replace_callback('/<iframe\b(.*?)\/iframe>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description']);
                                $this->product['product'][$product['id_product']]['description_short']                  = preg_replace_callback('/<script\b(.*?)>(.*?)*<\/script>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description_short']);
                                $this->product['product'][$product['id_product']]['description_short']                  = preg_replace_callback('/<script\b(.*?)*>(.*?)<\/script>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description_short']);
                                $this->product['product'][$product['id_product']]['description_short']                  = preg_replace_callback('/<meta\b(.*?)*>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description_short']);
                                $this->product['product'][$product['id_product']]['description_short']                  = preg_replace_callback('/<iframe\b(.*?)\/iframe>/i', function() {return null;}, $this->product['product'][$product['id_product']]['description_short']);
                        endif;
                        //Return///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($return_policy) ) :
                                foreach ( $return_policy as $return ) :
                                        $this->product['product'][$product['id_product']]['return']['name']             = $return['returns_policy'];
                                        $this->product['product'][$product['id_product']]['return']['days']             = $return['returns_within'];
                                        $this->product['product'][$product['id_product']]['return']['description']      = htmlspecialchars_decode($return['returns_information'], ENT_QUOTES);
                                        $this->product['product'][$product['id_product']]['return']['pays']             = $return['returns_pays'];
                                endforeach;
                        elseif ( $this->wizard ) :
                                        $this->product['product'][$product['id_product']]['return']['name']             = !empty($this->wizard) ? $this->user['returns_policy'] : 'ReturnsAccepted';
                                        $this->product['product'][$product['id_product']]['return']['days']             = !empty($this->wizard) ? $this->user['returns_within'] : 'Days_14';
                                        $this->product['product'][$product['id_product']]['return']['description']      = !empty($this->wizard) ? htmlspecialchars_decode($this->user['returns_information'], ENT_QUOTES) : 'Return within 14 Days';
                                        $this->product['product'][$product['id_product']]['return']['pays']             = !empty($this->wizard) ? $this->user['returns_pays'] : 'Buyer';
                        endif;
                        //Postalcode Dispatch Time///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($postalcode) ) :
                                foreach ( $postalcode as $postcode ) :
                                        $this->product['product'][$product['id_product']]['postal_code']                = $postcode['postcode'];
                                        $this->product['product'][$product['id_product']]['dispatch_time']              = isset($postcode['dispatch_time']) && is_numeric($postcode['dispatch_time']) ? $postcode['dispatch_time'] : 5;
                                endforeach;
                        elseif ( $this->wizard ) :
                                        $this->product['product'][$product['id_product']]['postal_code']                = !empty($this->wizard) ? $this->user['postal_code'] : '';
                                        $this->product['product'][$product['id_product']]['dispatch_time']              = !empty($this->wizard) ? $this->user['dispatch_time'] : 5;
                        endif;
                        //Peyment Dispatch Time///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($profile_payment) && !empty($profile_payment['payment_method']) ) :
                                        $this->product['product'][$product['id_product']]['payment_method']             = unserialize(base64_decode($profile_payment['payment_method']));
                        elseif ( $this->wizard && !empty($profile_payment) ) :
                                        $this->product['product'][$product['id_product']]['payment_method']             = !empty($this->wizard) ? unserialize(base64_decode($profile_payment['payment_method'])) : '';
                        endif;
                        //Configuration///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($configuration) ) :
                                foreach ( $configuration as $conf ) :
                                        $this->maximum_quantity                                                         = $conf['maximum_quantity'] ? $conf['maximum_quantity'] : 1000;
                                        $this->out_of_stock_min                                                         = $conf['out_of_stock_min'] ? $conf['out_of_stock_min'] : 0;
                                        $this->out_of_stock_created                                                     = $conf['out_of_stock_created'] ? $conf['out_of_stock_created'] : 0;
                                        $this->product['product'][$product['id_product']]['listing_duration']           = $conf['listing_duration'];
                                        $this->product['product'][$product['id_product']]['track_inventory']            = $conf['track_inventory'];
                                        $this->product['product'][$product['id_product']]['auto_pay']                   = $conf['auto_pay'];
                                        $this->product['product'][$product['id_product']]['country_name']               = $conf['country_name'];
                                        $this->product['product'][$product['id_product']]['currency']                   = $conf['currency'];
                                        $this->product['product'][$product['id_product']]['gallery_plus']               = $conf['gallery_plus'];
                                        $this->product['product'][$product['id_product']]['visitor_counter']            = $conf['visitor_counter'];
                                        $this->product['product'][$product['id_product']]['quantity']                   = (!empty($conf['out_of_stock_min']) && $this->product['product'][$product['id_product']]['quantity'] <= $conf['out_of_stock_min']) ? 0 : $this->product['product'][$product['id_product']]['quantity'];
                                        $this->product['product'][$product['id_product']]['quantity']                   = (!empty($this->maximum_quantity) && $this->product['product'][$product['id_product']]['quantity'] > $this->maximum_quantity) ? $this->maximum_quantity : $this->product['product'][$product['id_product']]['quantity'];
                                endforeach;
                        elseif ( $this->wizard ) :
                                        $this->product['product'][$product['id_product']]['listing_duration']           = !empty($this->wizard) ? $this->user['listing_duration'] : 'GTC';
                        endif;
                        //Templates///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($templates) ) :
                                foreach ( $templates as $template ) :
                                        if ( !empty($product['id_category_default']) && !empty($template["id_category"]) && ($product['id_category_default'] == $template["id_category"]) && ($template['template_selected'] != 'Default') ) :
                                                $this->product['product']
                                                [$product['id_product']]['template_description']                        = $dir_name.$template['template_selected'];
                                        endif;
                                endforeach;
                        else : 
                                $this->product['product'][$product['id_product']]['template_description']               = $this->dir_server. "/assets/apps/ebay/template/Default/ebay_product_template.html";
                        endif;
                        //Shipping///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($this->wizard) ) :
                                list($id_shipping, $service, $shipping_name)    = $this->connect->fetch($this->connect->get_shipping_default($this->site));
                                if ( !empty($id_shipping) ) : 
                                        $this->product['product']
                                        [$product['id_product']]['carrier_domestic'][$id_shipping][0]['name']           = array("default" => "default", "mapping" => $service);
                                        $this->product['product']
                                        [$product['id_product']]['carrier_domestic'][$id_shipping][0]['mapping_id']     = $id_shipping;
                                        $this->product['product']
                                        [$product['id_product']]['carrier_domestic'][$id_shipping][0]['cost']           = 0;
                                        $this->product['product']
                                        [$product['id_product']]['carrier_domestic'][$id_shipping][0]['additionals']    = 0;
                                        unset($this->product['product'][$product['id_product']]['carrier']);
                                endif;
                        elseif ( !empty($domestic_shippings) || !empty($this->_getDefault) ) :
                                $domestic_shippings         = $this->generate_array_key('ebayexport::change_array_keys', 'id_shipping', '', $domestic_shippings);
                                $international_shippings    = $this->generate_array_key('ebayexport::change_array_keys', 'id_shipping', '', $international_shippings);
                                $default_shippings          = $this->generate_array_key('ebayexport::change_array_keys', 'id_shipping', '', $this->_getDefault);
                                

                                if ( !empty($default_shippings) ) :
                                        foreach ( $default_shippings as $default_shipping) :
                                                if ( !empty($default_shipping['is_default'])) : 
                                                        $this->product['product']
                                                        [$product['id_product']]['carrier_domestic'][0][0]['name']           = array("default" => $default_shipping['name_shipping'], "mapping" => $default_shipping["name_ebay_shipping_service"]);
                                                        $this->product['product']
                                                        [$product['id_product']]['carrier_domestic'][0][0]['mapping_id']     = $default_shipping["id_ebay_shipping"];
                                                        $this->product['product']
                                                        [$product['id_product']]['carrier_domestic'][0][0]['cost']           = $this->eucurrency->doDiffConvert($default_shipping["shipping_cost"], $this->current_currency['iso_code'], $this->currency);
                                                        $this->product['product']
                                                        [$product['id_product']]['carrier_domestic'][0][0]['additionals']    = $this->eucurrency->doDiffConvert($default_shipping["shipping_additionals"], $this->current_currency['iso_code'], $this->currency);
                                                endif;
                                        endforeach;
                                endif;    

                                if ( !empty($product['carrier']) ) {
                                    foreach ( $product['carrier'] as $key => $shipping ) : 
                                            //Domestic Shipping///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($domestic_shippings[$key]) && array_key_exists('table_key', $domestic_shippings[$key]) && $domestic_shippings[$key]['table_key'] == 'mapping' ) :
                                                    $carrier_id = $domestic_shippings[$key]["id_shipping"];
                                                    unset($result);
                                                    $shipping_count[$carrier_id]    = isset($shipping_count[$carrier_id]) ? ++$shipping_count[$carrier_id] : 0;
                                                    $result = $this->set_carriersConfiguration($product['id_product'], $carrier_id, $domestic_shippings[$key]['name_shipping'], $domestic_shippings[$key], $shipping_count);
                                                    if ( $result ) :
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['name']           = array("default" => $domestic_shippings[$key]['name_shipping'], "mapping" => $domestic_shippings[$key]["name_ebay_shipping_service"]);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['mapping_id']     = $domestic_shippings[$key]["id_ebay_shipping"];
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']           = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost'], $this->current_currency['iso_code'], $this->currency);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']    = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals'], $this->current_currency['iso_code'], $this->currency);
                                                    endif;
                                            else :  if ( !empty($domestic_shippings[$key]) && $domestic_shippings[$key]['table_key'] != 'specific' ) :
                                                            foreach ( $domestic_shippings[$key] as $sub_key => $domestic ) :
                                                                    if(!isset($domestic['id_shipping']) && array_key_exists('table_key', $domestic[$key]) && $domestic[$key]['table_key'] == 'mapping' ){
                                                                            continue;
                                                                    }

                                                                    $carrier_id = $domestic['id_shipping'];
                                                                    $shipping_count[$carrier_id]    = isset($shipping_count[$carrier_id]) ? ++$shipping_count[$carrier_id] : 0;
                                                                    unset($result);
                                                                    $result = $this->set_carriersConfiguration($product['id_product'], $carrier_id, $domestic['name_shipping'], $domestic, $shipping_count);
                                                                    if ( $result ) :
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['name']                 = array("default" => $domestic['name_shipping'], "mapping" => $domestic["name_ebay_shipping_service"]);
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['mapping_id']           = $domestic["id_ebay_shipping"];
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['country']              = $domestic["country_service"];
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                 = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost'], $this->current_currency['iso_code'], $this->currency);
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']          = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals'], $this->current_currency['iso_code'], $this->currency);
                                                                    endif;
                                                            endforeach;
                                                    endif;
                                            endif;
                                            if ( !empty($domestic_shippings[$key]) && array_key_exists('table_key', $domestic_shippings[$key]) && $domestic_shippings[$key]['table_key'] == 'specific' ) :
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$key][0]['name']                  = array("default" => $domestic_shippings[$key]['name_ebay_shipping'], "mapping" => $domestic_shippings[$key]["name_ebay_shipping_service"]);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$key][0]['mapping_id']            = $domestic_shippings[$key]["id_ebay_shipping"];
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$key][0]['cost']                  = $this->eucurrency->doDiffConvert($domestic_shippings[$key]['shipping_cost'], $this->current_currency['iso_code'], $this->currency);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_domestic'][$key][0]['additionals']           = $this->eucurrency->doDiffConvert($domestic_shippings[$key]['shipping_additionals'], $this->current_currency['iso_code'], $this->currency);
                                            endif;
                                            //International Shipping//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                            if ( !empty($international_shippings[$key]) && array_key_exists('table_key', $international_shippings[$key]) && $international_shippings[$key]['table_key'] == 'mapping' ) :
                                                    $carrier_id = $international_shippings[$key]['id_shipping'];
                                                    $shipping_count[$carrier_id]    = isset($shipping_count[$carrier_id]) ? ++$shipping_count[$carrier_id] : 0;
                                                    unset($result);
                                                    $result = $this->set_carriersConfiguration($product['id_product'], $carrier_id, $international_shippings[$key]['name_shipping'], $international_shippings[$key], $shipping_count);
                                                    if ( $result ) :
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['name']        = array("default" => $international_shippings[$key]['name_shipping'], "mapping" => $international_shippings[$key]["name_ebay_shipping_service"]);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['mapping_id']  = $international_shippings[$key]["id_ebay_shipping"];
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['country']     = $international_shippings[$key]["country_service"];
                                                            $this->product['product'][$product['id_product']]
                                                            ['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                                = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost'], $this->current_currency['iso_code'], $this->currency);
                                                            $this->product['product'][$product['id_product']]
                                                            ['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']                         = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_international'][$international_shippings[$key]["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals'], $this->current_currency['iso_code'], $this->currency);
                                                    endif;
                                            else :  if ( !empty($international_shippings[$key]) ) :
                                                            foreach ( $international_shippings[$key] as $international ) :
                                                                    if(!isset($international['id_shipping'])){
                                                                            continue;
                                                                    }

                                                                    $carrier_id = $international['id_shipping'];
                                                                    $shipping_count[$carrier_id]    = isset($shipping_count[$carrier_id]) ? ++$shipping_count[$carrier_id] : 0;
                                                                    unset($result);
                                                                    $result = $this->set_carriersConfiguration($product['id_product'], $carrier_id, $international['name_shipping'], $international, $shipping_count);
                                                                    if ( $result ) :
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['name']                 = array("default" => $international['name_shipping'], "mapping" => $international["name_ebay_shipping_service"]);
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['mapping_id']           = $international["id_ebay_shipping"];
                                                                            $this->product['product']
                                                                            [$product['id_product']]['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['country']              = $international["country_service"];
                                                                            $this->product['product'][$product['id_product']]
                                                                            ['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                                         = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost'], $this->current_currency['iso_code'], $this->currency);
                                                                            $this->product['product'][$product['id_product']]
                                                                            ['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']                                  = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_international'][$international["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals'], $this->current_currency['iso_code'], $this->currency);
                                                                    endif;
                                                            endforeach;
                                                    endif;
                                            endif;
                                            if ( !empty($international_shippings[$key]) && array_key_exists('table_key', $international_shippings[$key]) && $international_shippings[$key]['table_key'] == 'specific' ) :
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$key][$key+1]['name']                                     = array("default" => $international_shippings[$key]['name_ebay_shipping'], "mapping" => $international_shippings[$key]["name_ebay_shipping_service"]);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$key][$key+1]['mapping_id']                               = $international_shippings[$key]["id_ebay_shipping"];
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$key][$key+1]['country']                                  = $international_shippings[$key]["country_service"];
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$key][$key+1]['cost']                                     = $this->eucurrency->doDiffConvert($international_shippings[$key]['shipping_cost'], $this->current_currency['iso_code'], $this->currency);
                                                            $this->product['product']
                                                            [$product['id_product']]['carrier_international'][$key][$key+1]['additionals']                              = $this->eucurrency->doDiffConvert($international_shippings[$key]['shipping_additionals'], $this->current_currency['iso_code'], $this->currency);
                                            endif;
                                    endforeach;
                                }
                                
                                if ( empty($this->product['product'][$product['id_product']]['carrier_domestic']) ) :
                                        if ( !empty($this->carrier_default)) : 
                                                $this->product['product']
                                                [$product['id_product']]['carrier_domestic'][0]['name']           = array("default" => $this->carrier_default['name_shipping'], "mapping" => $this->carrier_default["name_ebay_shipping_service"]);
                                                $this->product['product']
                                                [$product['id_product']]['carrier_domestic'][0]['mapping_id']     = $this->carrier_default["id_ebay_shipping"];
                                                $this->product['product']
                                                [$product['id_product']]['carrier_domestic'][0]['cost']           = $this->eucurrency->doDiffConvert($this->carrier_default["shipping_cost"], $this->current_currency['iso_code'], $this->currency);
                                                $this->product['product']
                                                [$product['id_product']]['carrier_domestic'][0]['additionals']    = $this->eucurrency->doDiffConvert($this->carrier_default["shipping_additionals"], $this->current_currency['iso_code'], $this->currency);
                                        else : 
                                                $domestic_shipping = current($domestic_shippings);
                                                $domestic_key = key($domestic_shippings);
                                                if ( array_key_exists('table_key', $domestic_shipping) && $domestic_shipping['table_key'] == 'mapping' ) {
                                                    $this->product['product']
                                                    [$product['id_product']]['carrier_domestic'][0]['name']           = array("default" => $domestic_shipping['name_shipping'], "mapping" => $domestic_shipping["name_ebay_shipping_service"]);
                                                    $this->product['product']
                                                    [$product['id_product']]['carrier_domestic'][0]['mapping_id']     = $domestic_shipping["id_ebay_shipping"];
                                                    $this->product['product']
                                                    [$product['id_product']]['carrier_domestic'][0]['cost']           = $this->eucurrency->doDiffConvert($domestic_shipping["shipping_cost"], $this->current_currency['iso_code'], $this->currency);
                                                    $this->product['product']
                                                    [$product['id_product']]['carrier_domestic'][0]['additionals']    = $this->eucurrency->doDiffConvert($domestic_shipping["shipping_additionals"], $this->current_currency['iso_code'], $this->currency);
                                                }
                                                else {
                                                    $shipping_count = array();
                                                    foreach ( $domestic_shipping as $sub_key => $domestic ) :
                                                        if(!isset($domestic['id_shipping']) && array_key_exists('table_key', $domestic[$key]) && $domestic[$key]['table_key'] == 'mapping' ){
                                                                continue;
                                                        }

                                                        $carrier_id = $domestic['id_shipping'];
                                                        $shipping_count[$carrier_id]    = isset($shipping_count[$carrier_id]) ? ++$shipping_count[$carrier_id] : 0;
                                                        unset($result);
                                                        $result = $this->set_carriersConfiguration($product['id_product'], $carrier_id, $domestic['name_shipping'], $domestic, $shipping_count);
                                                        if ( $result ) :
                                                                $this->product['product']
                                                                [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['name']                 = array("default" => $domestic['name_shipping'], "mapping" => $domestic["name_ebay_shipping_service"]);
                                                                $this->product['product']
                                                                [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['mapping_id']           = $domestic["id_ebay_shipping"];
                                                                $this->product['product']
                                                                [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['country']              = $domestic["country_service"];
                                                                $this->product['product']
                                                                [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                 = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost'], $this->current_currency['iso_code'], $this->currency);
                                                                $this->product['product']
                                                                [$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']          = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['carrier_domestic'][$domestic["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals'], $this->current_currency['iso_code'], $this->currency);
                                                        endif;
                                                endforeach;}
                                        endif;
                                endif;
                                unset($this->product['product'][$product['id_product']]['carrier']);
                        endif;

                        //Tax not used///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product) && !empty($tax) ) : 
                                if ( strtolower($tax[0]['tax_type']) == 'percent' ) :
                                        $this->product['product'][$product['id_product']]['price']                     = round($this->product['product'][$product['id_product']]['price'] + ($this->product['product'][$product['id_product']]['price'] * ($tax[0]['tax_rate']/100)), $tax[0]['decimal_value']);
                                endif;

                                if ( !empty($product['combination']) ) :
                                        foreach( $product['combination'] as $comb_key => $comb ) :
                                                if ( strtolower($tax[0]['tax_type']) == 'percent' ) :
                                                        $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = round($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] + ($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] * ($tax[0]['tax_rate']/100)), $tax[0]['decimal_value']);
                                                endif;
                                        endforeach;
                                endif;
                        endif;
                        //Conditions///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $condition_groups   = $this->generate_array_key('ebayexport::change_array_group', 'condition_data', array('condition_value', 'condition_description'), $condition);
                        if ( !empty($product) && !empty($condition_groups) ) : 
                                $category_id                = !empty($this->product['product'][$product['id_product']]['category']) ? $this->product['product'][$product['id_product']]['category']['mapping_id'] : '';
                                $static_condition           = $this->product['product'][$product['id_product']]['condition'];
                                if ( !empty($category_id) && !empty($condition_groups[$static_condition]) ) : 
                                        foreach ( $condition_groups[$static_condition]['condition_value'] as $key => $condition_value ) :
                                                if ( !empty($this->condition_list[$category_id]) && in_array($condition_value, $this->condition_list[$category_id]) ) :
                                                        $this->product['product'][$product['id_product']]['condition']                  = $condition_groups[$static_condition]['condition_value'][$key];
                                                        $this->product['product'][$product['id_product']]['condition_description']      = $condition_groups[$static_condition]['condition_description'][$key];
                                                        break;
                                                endif;
                                                if ( $static_condition == $this->product['product'][$product['id_product']]['condition'] ) :
                                                        $this->product['product'][$product['id_product']]['condition_error_mapping'][]  = $static_condition;
                                                endif;
                                        endforeach;
                                else :  
                                        $this->product['product'][$product['id_product']]['condition_error_mapping'][]  = $static_condition;
                                endif;
                        endif;

                        //Paypal///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($product) && !empty($this->paypalEmail) ) : 
                                $this->product['product'][$product['id_product']]['paypal_email']                     = $this->paypalEmail; 
                        endif;
                }
                
                function set_carriersConfiguration($id = NULL, $carrier_id = NULL, $name = NULL, $shipping_zone = NULL, $shipping_count = NULL) {
                        if ( empty( $this->product['product'] ) ) : 
                                $this->_result['msg']['empty_products'] = 1;
                                return;
                        endif;
                        
                        if ( (!isset($id) && empty($id)) || (!isset($carrier_id) && empty($carrier_id)) || empty($name) || empty($shipping_zone) ) : 
                                return;
                        endif;

                        if ( $shipping_zone['id_rule'] == 3 && $shipping_zone['is_international'] == 0 ) :
                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                = $shipping_zone["shipping_cost"];
                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']         = $shipping_zone["shipping_additionals"];
                                return true;
                        elseif ( $shipping_zone['id_rule'] == 3 && $shipping_zone['is_international'] == 1 ) :
                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']           = $shipping_zone["shipping_cost"];
                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']    = $shipping_zone["shipping_additionals"];
                                return true;
                        elseif ( $shipping_zone['id_rule'] == 2 ) :
                                $w = $this->product['product'][$id]['weight'];
                                if ( $shipping_zone['operations'] == 'is between' && !empty($w['value']) && $w['value'] >= $shipping_zone['min_weight'] && $w['value'] <= $shipping_zone['max_weight'] ) : 
                                        if ( $shipping_zone['is_international'] == 0 ) :
                                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                = $shipping_zone["shipping_cost"];
                                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']         = $shipping_zone["shipping_additionals"];
                                        else :
                                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']           = $shipping_zone["shipping_cost"];
                                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']    = $shipping_zone["shipping_additionals"];
                                        endif; 
                                        return true;
                                endif;
                        elseif ( $shipping_zone['id_rule'] == 4 ) : 
                                $price = 0;
                                if ( !empty($this->product['product'][$id]['combination']) ) :
                                        foreach ( $this->product['product'][$id]['combination'] as $comb_key => $comb ) :
                                                $price              = ($comb['price'] > $price) ? $comb['price'] : $price;
                                        endforeach;
                                else :
                                        $price                      = $this->product['product'][$id]['price'];
                                endif;

                                if ( $shipping_zone['operations'] == 'is between' && !empty($price) && $price >= $this->eucurrency->doDiffConvert($shipping_zone['min_price'], $this->current_currency['iso_code'], $this->currency) && $price <= $this->eucurrency->doDiffConvert($shipping_zone['max_price'], $this->current_currency['iso_code'], $this->currency) ) : 
                                        if ( $shipping_zone['is_international'] == 0 ) :
                                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']                = $shipping_zone["shipping_cost"];
                                                $this->product['product'][$id]['carrier_domestic'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']         = $shipping_zone["shipping_additionals"];
                                        else :
                                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['cost']           = $shipping_zone["shipping_cost"];
                                                $this->product['product'][$id]['carrier_international'][$shipping_zone["id_ebay_shipping"]][$shipping_count[$carrier_id]]['additionals']    = $shipping_zone["shipping_additionals"];
                                        endif;
                                        return true;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_mapping_relist($product = null) {
                        if ( empty($product) || empty($this->itemID) ) {
                                return;
                        }
                        $id                                                             = $product['id_product'];
                        $curent_sku                                                     = !empty($product['reference']) ? $product['reference'] : (!empty($product['sku']) ? $product['sku'] : '')/*."_#_".$this->site*/;
                        if ( !empty($this->itemID[$id]) && array_key_exists('id_product', $this->itemID[$id]) ) :
                                if ( $this->ebayUser == $this->itemID[$id]['ebay_user'] && $this->itemID[$id]['is_enabled'] == 1  && ($curent_sku == $this->itemID[$id]['SKU'] || $id == $this->itemID[$id]['id_product'])) :
                                        $this->product['product'][$id]['relist']        = $this->itemID[$id]['id_item'];
                                        $this->product['product'][$id]['relist_date']   = $this->itemID[$id]['date_end'];
                                        $this->product['product'][$id]['sku']           = !empty($curent_sku) ? $curent_sku : $this->itemID[$id]['SKU'];
                                endif;
                        elseif ( !empty($this->itemID[$id]) && is_array($this->itemID[$id]) && !empty($product['category']['mapping_id']) && isset($this->features[$product['category']['mapping_id']]) && ($this->features[$product['category']['mapping_id']] == 0 || !empty($this->expolde_products[$product['id_product']])) ) :
                                foreach ( $product['combination'] as $key => $combination ) :
                                        $combinations[$combination['reference']] = $combination;
//                                        $curent_sku[$combination['reference']] = !empty($combination['reference']) ? $combination['reference'] : (!empty($combination['sku']) ? $combination['sku'] : '')/*."_#_".$this->site*/;
                                endforeach;
                                foreach ( $this->itemID[$id] as $itemsID ) :
                                        if ( !empty($itemsID) && array_key_exists('id_product', $itemsID) && !empty($combinations) ) :
                                                if ( $this->ebayUser == $itemsID['ebay_user'] && $itemsID['is_enabled'] == 1  && array_key_exists($itemsID['SKU'], $combinations)) :
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['relist']        = $itemsID['id_item'];
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['relist_date']   = $itemsID['date_end'];
                                                        $this->product['product'][$id."_".$itemsID['SKU']]['sku']           = !empty($itemsID['SKU']) ? $itemsID['SKU'] : $combination['reference'];
                                                endif;
                                        endif;
                                endforeach;
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_mapping_category($product = null, $mappings = '', $field = null) {
                        if ( empty( $product ) ) : 
                                return;
                        endif;
                        
                        if ( empty($mappings) && (empty($field) || ($field == 'category')) ) : 
                                $this->_result['msg']['mapping_category'] = 1;
                                return;
                        endif;
                        
                        if ( !empty($mappings) ) :
                                if ( !empty($product['id_category_default']) && array_key_exists($product['id_category_default'], $mappings) ) :
                                        $this->product['product']
                                        [$product['id_product']][$field]    = array("mapping" => $mappings[$product['id_category_default']]["name_ebay_category"], "id" => $mappings[$product['id_category_default']]["id_category"], "mapping_id" => $mappings[$product['id_category_default']]['id_ebay_category']);
                                        return;
                                else :
                                        if ( !empty($product['id_category_default']) ) :
                                                $results                    = $this->objectbiz->getCategoryChildDefault($this->user_name, $product['id_category_default'], $this->id_shop);
                                                if ( !empty($results) ) :
                                                        foreach ( $results as $res ) :
                                                                if ( !empty($mappings[$res["id_parent"]]) ) :
                                                                        $this->product['product'][$product['id_product']][$field]                  = array("mapping" => $mappings[$res["id_parent"]]["name_ebay_category"], "id" => $mappings[$res["id_parent"]]["id_category"], "mapping_id" => $mappings[$res["id_parent"]]['id_ebay_category']);
                                                                        $this->product['product'][$product['id_product']]['id_category_default']   = $mappings[$res["id_parent"]]["id_category"];
                                                                        return;
                                                                endif;
                                                        endforeach;
                                                endif;
                                        endif;
                                endif;
                                 if ($field == 'category') :
                                        unset($this->product['product'][$product['id_product']]);
                                 endif;
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function clear_old_attribute($product = null) {
                        if ( empty( $product ) ) : 
                                return;
                        endif;
                        if ( !empty($product['relist']) && !empty($this->variation_attributes) && array_key_exists($product['relist'], $this->variation_attributes) ) :
                                $id = $product['id_product'];
                                if ( array_key_exists("SKU", $this->variation_attributes[$product['relist']]) ) : 
                                        $this->product['product'][$id]['variation_old'][] = array(
                                                'SKU'               => $this->variation_attributes[$product['relist']]['SKU'],
                                                'quantity'          => $this->variation_attributes[$product['relist']]['quantity'],
                                                'start_price'       => $this->variation_attributes[$product['relist']]['start_price'],
                                                'variation'         => array(0 => array(
                                                'variation_name'    => $this->variation_attributes[$product['relist']]['variation_name'],
                                                'variation_value'   => $this->variation_attributes[$product['relist']]['variation_value'],
                                                ))
                                        );
                                else : 
                                        $sku_variation = $this->generate_array_key("ebayexport::change_array_keys", "SKU", "", $this->variation_attributes[$product['relist']]);
                                        foreach ( $sku_variation as $sku_root => $item_variations ) :
                                                if ( array_key_exists("SKU", $item_variations) ) : 
                                                        $this->product['product'][$id]['variation_old'][] = array(
                                                                'SKU'               => $item_variations['SKU'],
                                                                'quantity'          => $item_variations['quantity'],
                                                                'start_price'       => $item_variations['start_price'],
                                                                'variation'         => array(0 => array(
                                                                'variation_name'    => $item_variations['variation_name'],
                                                                'variation_value'   => $item_variations['variation_value'],
                                                                ))
                                                        );
                                                else :
                                                        unset($sku, $quantity, $start_price, $variation);
                                                        foreach ( $item_variations as $item_variation ) :
                                                                $sku            = $item_variation['SKU'];
                                                                $quantity       = $item_variation['quantity'];
                                                                $start_price    = $item_variation['start_price'];
                                                                $variation[]    = array(
                                                                        'variation_name'    => $item_variation['variation_name'],
                                                                        'variation_value'   => $item_variation['variation_value'],
                                                                );
                                                        endforeach;
                                                        $this->product['product'][$id]['variation_old'][] = array(
                                                                'SKU'               => $sku,
                                                                'quantity'          => $quantity,
                                                                'start_price'       => $start_price,
                                                                'variation'         => $variation,
                                                        );
                                                endif;
                                        endforeach;
                                endif;
                        endif;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function extract_combination($product = null, $combination = array()) {
                        if ( empty($product) ) {
                                return;
                        }
                        if ( is_array($product['combination'])) :
                                $id     = $product['id_product'];
                                $this->export_mapping_relist($product);
                                foreach ( $product['combination'] as $comb_id => $comb ) :
                                        if ( !isset($product['category']['mapping_id']) && ((empty($product['carrier_domestic']) && empty($product['carrier_international'])) || (empty($product['sku']) && empty($product['upc']) && empty($product['ean13'])) || empty($product['category']) || empty($product['payment_method']) || empty($product['postal_code']) || empty($product['return'])/* || empty($product['paypal_email'])*/ ) ) :
                                                break;
                                        endif;
                                        $data_store_price[$comb_id]             = isset($comb['price']) ? ($comb['price'] + (isset($product['price']) ? $product['price'] : 0)) : (isset($product['price']) ? $product['price'] : 0);
                                endforeach;
                                if ( !empty($data_store_price) ) :
                                    $min = min($data_store_price);
                                endif;
                                foreach ( $product['combination'] as $comb_id => $comb ) : 
                                        if ( !isset($product['category']['mapping_id']) && ((empty($product['carrier_domestic']) && empty($product['carrier_international'])) || (empty($product['sku']) && empty($product['upc']) && empty($product['ean13'])) || empty($product['category']) || empty($product['payment_method']) || empty($product['postal_code']) || empty($product['return'])/* || empty($product['paypal_email'])*/ ) ) :
                                                break;
                                        endif;
                                        unset($data_insert, $data_new);
                                        $data_insert['sku']                     = !empty($comb['reference']) ? $comb['reference'] : (!empty($comb['sku']) ? $comb['sku'] : '');
                                        $data_insert['ean13']                   = isset($comb['ean13']) ? $comb['ean13'] : '';
                                        $data_insert['upc']                     = isset($comb['upc']) ? $comb['upc'] : '';
                                        $data_insert['name']                    = isset($this->product['product'][$id]['name']) ? $this->product['product'][$id]['name'] : '';
                                        $data_insert['quantity']                = isset($comb['quantity']) ? $comb['quantity'] : 0;
                                        $data_insert['price']                   = isset($comb['price']) ? ($comb['price'] + (isset($product['price']) ? $product['price'] : 0)) : (isset($product['price']) ? $product['price'] : 0);
                                        $data_new['id_product']                 = isset($product['id_product']) ? $product['id_product'] : '';
                                        $data_new['description']                = isset($product['description']) ? $product['description'] : '';
                                        $data_new['description']                = isset($product['description_short']) ? $product['description_short'] : '';
                                        $data_new['ean13']                      = isset($comb['ean13']) ? $comb['ean13'] : '';
                                        $data_new['upc']                        = isset($comb['upc']) ? $comb['upc'] : '';
                                        $data_new['reference']                  = isset($comb['reference']) ? $comb['reference'] : '';
                                        $data_new['condition']                  = isset($product['condition']) ? $product['condition'] : '';
                                        $data_new['condition_description']      = isset($product['condition_description']) ? $product['condition_description'] : '';
                                        $data_new['manufacturer']               = isset($product['manufacturer']) ? $product['manufacturer'] : '';
                                        $data_new['supplier']                   = isset($product['supplier']) ? $product['supplier'] : '';
                                        $data_new['currency']                   = isset($product['currency']) ? $product['currency'] : '';
                                        $data_new['category']                   = isset($product['category']) ? $product['category'] : '';
                                        $data_new['id_category_default']        = isset($product['id_category_default']) ? $product['id_category_default'] : '';
                                        $data_new['template_description']       = isset($product['template_description']) ? $product['template_description'] : '';
                                        $data_new['visitor_counter']            = isset($product['visitor_counter']) ? $product['visitor_counter'] : '';
                                        $data_new['gallery_plus']               = isset($product['gallery_plus']) ? $product['gallery_plus'] : '';
                                        $data_new['country_name']               = isset($product['country_name']) ? $product['country_name'] : '';
                                        $data_new['auto_pay']                   = isset($product['auto_pay']) ? $product['auto_pay'] : '';
                                        $data_new['track_inventory']            = isset($product['track_inventory']) ? $product['track_inventory'] : '';
                                        $data_new['PaymentInstructions']        = isset($product['PaymentInstructions']) ? $product['PaymentInstructions'] : '';
                                        $data_new['combination']                = array();
                                        $data_new['listing_duration']           = isset($product['listing_duration']) ? $product['listing_duration'] : '';
                                        $data_new['return']                     = isset($product['return']) ? $product['return'] : '';
                                        $data_new['postal_code']                = isset($product['postal_code']) ? $product['postal_code'] : '';
                                        $data_new['address']                    = isset($product['address']) ? $product['address'] : '';
                                        $data_new['site']                       = isset($product['site']) ? $product['site'] : '';
                                        $data_new['paypal_email']               = isset($product['paypal_email']) ? $product['paypal_email'] : '';
                                        $data_new['payment_method']             = isset($product['payment_method']) ? $product['payment_method'] : '';
                                        $data_new['dispatch_time']              = isset($product['dispatch_time']) ? $product['dispatch_time'] : '';
                                        $data_new['country']                    = isset($product['country']) ? $product['country'] : '';
                                        $data_new['batch_id']                   = isset($product['batch_id']) ? $product['batch_id'] : '';
                                        $data_new['wizard']                     = isset($product['wizard']) ? $product['wizard'] : false;
                                        $data_new['active']                     = isset($product['active']) ? $product['active'] : false;
                                        $data_new['link']                       = isset($product['link']) ? $product['link'] : false;
                                        $data_new['shipping_override']          = isset($product['shipping_override']) ? $product['shipping_override'] : 0;
                                        $data_new['disable']                    = isset($comb['disable']) ? $comb['disable'] : '';
                                        $data_insert['quantity']                = (!empty($this->out_of_stock_min) && $data_insert['quantity'] <= $this->out_of_stock_min) ? 0 : $data_insert['quantity'];
                                        $data_insert['quantity']                = (!empty($this->maximum_quantity) && $data_insert['quantity'] > $this->maximum_quantity) ? $this->maximum_quantity : $data_insert['quantity'];
                                        
                                        if ( isset($min) && $min != 0 && empty($this->expolde_products[$product['id_product']]) ) :
                                            if ( $data_insert['price'] > ($min * 4) ) :
                                                unset($this->product['product'][$id]['combination'][$comb_id]);
                                                continue;
                                            endif;
                                        endif;
                                        
                                        if ( isset($product['carrier_international']) ) :
                                                $data_new['carrier_international'] = $product['carrier_international'];
                                        endif;

                                        if ( isset($product['carrier_domestic']) ) :
                                                $data_new['carrier_domestic']   = $product['carrier_domestic'];
                                        endif;

                                        if ( isset($product['images']) ) :
                                                $product_image      = current($product['images']);
                                        endif;
                                        if (!empty($comb['images']) && count($comb['images'])) :
                                                foreach ( $comb['images'] as $i => $img ) :
                                                        $data_insert['images'][$i]['url']  = isset($img['url']) ? $img['url'] : (isset($product_image) ? $product_image['url'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif');
                                                endforeach;
                                        else : 
                                                $data_insert['images'][0]['url'] =  isset($product_image) ? $product_image['url'] : 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif';
                                        endif;
                                        
                                        if ( !empty($this->attribute_value[$this->id_profile]) ) :
                                                $this->attribute_value_count = $this->generate_array_key('ebayexport::change_array_keys', 'ebay_attribute_value', '', $this->attribute_value[$this->id_profile]);
                                        endif;
                                        

                                        $value_attribute = array(); 
                                        if ( !empty($this->attribute_value[$this->id_profile]) && count($this->attribute_value[$this->id_profile]) ) : 
                                                if ( array_key_exists("table_key", $this->attribute_value[$this->id_profile]) ) :
                                                        $combination_attribute_value = $this->attribute_value[$this->id_profile];
                                                        $value_attribute['name'][isset($combination_attribute_value['id_attribute_group']) ? $combination_attribute_value['id_attribute_group'] : 0]    = isset($combination_attribute_value['ebay_attribute_name']) ? $combination_attribute_value['ebay_attribute_name'] : '';
                                                        if ( !empty($combination_attribute_value['id_attribute'])) :
                                                                $value_attribute['value']
                                                                [isset($combination_attribute_value['id_attribute_group']) ? $combination_attribute_value['id_attribute_group'] : 0]
                                                                [isset($combination_attribute_value['id_attribute']) ? $combination_attribute_value['id_attribute'] : 0]       
                                                                = isset($combination_attribute_value['ebay_attribute_value']) ? $combination_attribute_value['ebay_attribute_value'] : '';
                                                        endif;
                                                else :
                                                        foreach ( $this->attribute_value[$this->id_profile] as $combination_attribute_value ) :
                                                                $value_attribute['name'][isset($combination_attribute_value['id_attribute_group']) ? $combination_attribute_value['id_attribute_group'] : 0]    = isset($combination_attribute_value['ebay_attribute_name']) ? $combination_attribute_value['ebay_attribute_name'] : '';
                                                                if ( !empty($combination_attribute_value['id_attribute'])) :
                                                                        $value_attribute['value']
                                                                        [isset($combination_attribute_value['id_attribute_group']) ? $combination_attribute_value['id_attribute_group'] : 0]
                                                                        [isset($combination_attribute_value['id_attribute']) ? $combination_attribute_value['id_attribute'] : 0]       
                                                                        = isset($combination_attribute_value['ebay_attribute_value']) ? $combination_attribute_value['ebay_attribute_value'] : '';
                                                                endif;
                                                        endforeach;
                                                endif;
                                        endif;
                                        if ( isset($comb['attributes']) ) :
                                                $name = '';
                                                foreach ( $comb['attributes'] as $attribute_id => $attribute ) :
                                                        $attribute_name_mapping                                                             = (!empty($value_attribute['name']) && array_key_exists($attribute_id, $value_attribute['name'])) ? $value_attribute['name'][$attribute_id] : (empty($value_attribute['name']) ? $attribute['name'][$this->language] : (!empty($attribute['name'][$this->language]) ? $attribute['name'][$this->language] : ''));

                                                        if ( !empty($attribute_name_mapping) ) :
                                                                $data_insert['attribute_mapping_name'][$attribute_id]['Name']               = $attribute_name_mapping;
                                                                foreach ( $attribute['value'] as $attribute_val_id => $value ) :
                                                                        $last_value = !empty($value_attribute['value'][$attribute_id][$attribute_val_id]) ? $value_attribute['value'][$attribute_id][$attribute_val_id] : '';
                                                                        $attribute_value_mapping                                            = 
                                                                        !empty($value_attribute['value'][$attribute_id]) && array_key_exists($attribute_val_id, $value_attribute['value'][$attribute_id]) && !empty($this->attribute_value_count) && !empty($this->attribute_value_count[$last_value]) && array_key_exists("table_key", $this->attribute_value_count[$last_value])
                                                                        ? $last_value : (empty($value_attribute['value'][$attribute_id]) 
                                                                        ? $value['name'][$this->language] : (empty($value_attribute['value'][$attribute_id][$attribute_val_id]) 
                                                                        ? $value['name'][$this->language] : (!empty($this->attribute_value_count) && !empty($this->attribute_value_count[$last_value]) && !array_key_exists("table_key", $this->attribute_value_count[$last_value])) ? $value['name'][$this->language] : ''));
                                                                        if ( !empty($attribute_value_mapping) ) :
                                                                                $data_insert['attribute_mapping_name'][$attribute_id]['Value'][$attribute_val_id]       = $attribute_value_mapping;
                                                                                $data_spec['spec_mapping'][$attribute_id][$attribute_name_mapping][$attribute_val_id]   = $attribute_value_mapping;
                                                                        endif;

                                                                        $name  .= isset($value['name']) ? " ".$value['name'][$this->language] : '';
                                                                        $data_spec['spec'][$attribute_id][$attribute['name'][$this->language]][$attribute_val_id]   = (isset($value['name']) ? $value['name'][$this->language] : '');
                                                                endforeach;
                                                        endif;
                                                endforeach;
                                                if ( isset($data_spec['spec']) ) :
                                                        $data_insert['name']    .= " - [".$name." ]";
                                                        $combination[$comb_id]  = $data_insert;
                                                        $data_new               = array_merge($data_new, $data_insert);
                                                endif;
                                        endif;

                                        if ( isset($data_spec['spec']) ) :
                                                foreach ( $data_spec['spec'] as $att_id => $spec_value ) :
                                                        foreach ( $spec_value as $att => $spec ) :
                                                                $data_spec['spec'][$att_id][$att] = array_unique($spec);
                                                        endforeach;
                                                endforeach;
                                        endif;
                                        if ( isset($data_spec['spec_mapping']) ) :
                                                foreach ( $data_spec['spec_mapping'] as $att_id => $spec_value ) :
                                                        foreach ( $spec_value as $att => $spec ) :
                                                                $data_spec['spec_mapping'][$att_id][$att] = array_unique($spec);
                                                        endforeach;
                                                endforeach;
                                        endif;
                                        $this->product['product'][$id]['combinations']  = !empty($combination) ? $combination : array();
                                        if ( empty($this->product['product'][$id]['combinations']) ) :
                                                unset($this->product['product'][$id]['combinations']);
                                        endif;
                                        if ( !empty($product['category']['mapping_id']) && is_numeric($product['category']['mapping_id']) && ((isset($this->features[$product['category']['mapping_id']]) && $this->features[$product['category']['mapping_id']] == 0) ||
                                             (isset($this->expolde_products[$product['id_product']])))
                                           ) :
                                                if ( !empty($data_new['reference']) ) {
                                                    $this->total_request_log = !empty($this->total_request_log) ? $this->total_request_log + 1 : 0;
                                                    $this->product['product'][$id."_".$data_new['reference']]      = !empty($this->product['product'][$id."_".$data_new['reference']]) ? array_merge($this->product['product'][$id."_".$data_new['reference']], $data_new) : $data_new;
                                                    $this->export_mapping_relist($this->product['product'][$id."_".$data_new['reference']]);
                                                    $this->product['product'][$id."_".$data_new['reference']]['id_product_reference'] = $id."_".$data_new['reference'];
                                                }
                                                else {
                                                    unset($this->product['product'][$id]);
                                                    continue;
                                                }
                                                $combination_id_group[] = $id."_".$data_new['reference'];
                                        endif;
                                endforeach;
                                if ( isset($data_spec['spec']) ) :
                                        $this->product['product'][$id]['spec']   = $data_spec['spec'];
                                endif;
                                if ( isset($data_spec['spec_mapping']) ) :
                                        $this->product['product'][$id]['spec_mapping']   = $data_spec['spec_mapping'];
                                endif;
                                if ( !empty($product['category']['mapping_id']) && is_numeric($product['category']['mapping_id']) && ((isset($this->features[$product['category']['mapping_id']]) && $this->features[$product['category']['mapping_id']] == 0) ||
                                     (isset($this->expolde_products[$product['id_product']])))
                                   ) :
                                        $this->total_request_log--;
                                        unset($this->product['product'][$id], $combination, $data_spec, $product_image, $comb, $value_attribute, $data_insert, $current_price, $product, $data_store_price);
                                        return !empty($combination_id_group) ? $combination_id_group : array();
                                endif;
                        else :
                                $id                                             = $product['id_product'];
                                $this->export_mapping_relist($product);
                        endif;
                        unset($combination, $data_spec, $product_image, $comb, $value_attribute, $data_insert, $data_new, $current_price, $product, $data_store_price);
                        return array($id);
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_discount_price($product = null) {
                        if ( !isset($product) && empty($product) ) :
                                return null;
                        endif;
                        $id                                             = $product['id_product'];
                        if ( is_array($product['combination'])) :
                                foreach ( $product['combination'] as $comb_id => $comb ) :
                                        if ( isset($product['sale']) && !empty($product['sale'][$comb['id_product_attribute']]) ) :
                                                $current_price              = isset($comb['price']) ? ($comb['price'] + (isset($product['price']) ? $product['price'] : 0)) : (isset($product['price']) ? $product['price'] : 0);
                                                $saleArray                  = $product['sale'][$comb['id_product_attribute']];
                                                if ( (strtotime('now') > strtotime($saleArray['date_from']) && strtotime('now') < strtotime($saleArray['date_to'])) || '1970-01-01' == $saleArray['date_to']) :
                                                        if ( $saleArray['reduction_type'] == 'percentage' ) :
                                                                $current_price = round(($current_price - (($current_price * $saleArray['reduction']) / 100)), 2);
                                                        endif;
                                                        if ( $saleArray['reduction_type'] == 'Price' || $saleArray['reduction_type'] == 'amount') :
                                                                $current_price  = $current_price - $saleArray['reduction'];
                                                        endif;
                                                endif;
                                                $this->product['product'][$id]['combination'][$comb_id]['price'] = $current_price;
                                        endif;
                                endforeach;
                        else :  if ( isset($product['sale']) && !empty($product['sale'][0]) ) :
                                        $current_price              = (isset($product['price']) ? $product['price'] : 0);
                                        $saleArray                  = $product['sale'][0];
                                        if ( (strtotime('now') > strtotime($saleArray['date_from']) && strtotime('now') < strtotime($saleArray['date_to'])) || '1970-01-01' == $saleArray['date_to']) :
                                                if ( $saleArray['reduction_type'] == 'percentage' ) :
                                                        $current_price = round(($current_price - (($current_price * $saleArray['reduction']) / 100)), 2);
                                                endif;
                                                if ( $saleArray['reduction_type'] == 'Price' || $saleArray['reduction_type'] == 'amount') :
                                                        $current_price  = $current_price - $saleArray['reduction'];
                                                endif;
                                        endif;
                                        $this->product['product'][$id]['price'] = $current_price;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_price_modifier($product = null) {
                        if ( !empty($product) && !empty($this->_price_modifier) ) :
                                foreach ( $this->_price_modifier as $price_modifier ) :
                                        if (   (!empty($price_modifier["decimal_value"]) || is_numeric($price_modifier["decimal_value"]))
                                            && (!empty($price_modifier["price_from"]) || is_numeric($price_modifier["price_from"]))
                                            && (!empty($price_modifier["price_to"]) || is_numeric($price_modifier["price_to"]))
                                            && (!empty($price_modifier["price_value"]) || is_numeric($price_modifier["price_value"]))
                                            && (!empty($price_modifier["price_type"])) 
                                            //&& ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"]) 
                                           ) : 
                                                if ( $price_modifier["price_type"] == 'percent' ) : 
                                                        if ( !empty($product['combination']) ) :
                                                                foreach( $product['combination'] as $comb_key => $comb ) :
                                                                        if ($price_modifier["price_from"] <= $comb['price'] && $comb['price'] <= $price_modifier["price_to"])  :
                                                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] + number_format(($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] * $price_modifier["price_value"]/100), $price_modifier["decimal_value"], '.', '');
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                            if ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"])  :
                                                                    $this->product['product'][$product['id_product']]['price']     = $this->product['product'][$product['id_product']]['price'] + number_format(($this->product['product'][$product['id_product']]['price'] * $price_modifier["price_value"]/100), $price_modifier["decimal_value"], '.', '');
                                                            endif;
                                                        endif;
                                                elseif ( $price_modifier["price_type"] == 'value' ) :
                                                        if ( !empty($product['combination']) ) :
                                                                foreach( $product['combination'] as $comb_key => $comb ) :
                                                                        if ($price_modifier["price_from"] <= $comb['price'] && $comb['price'] <= $price_modifier["price_to"])  :
                                                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->product['product'][$product['id_product']]['combination'][$comb_key]['price'] + number_format($price_modifier["price_value"], $price_modifier["decimal_value"], '.', '');
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                            if ($price_modifier["price_from"] <= $product['price'] && $product['price'] <= $price_modifier["price_to"])  :
                                                                    $this->product['product'][$product['id_product']]['price']     = $this->product['product'][$product['id_product']]['price'] + number_format($price_modifier["price_value"], $price_modifier["decimal_value"], '.', '');
                                                            endif;
                                                        endif;
                                                endif;
                                                $this->price_modifier_rate = floatval($price_modifier["price_value"]);
                                                $this->price_modifier_type = $price_modifier["price_type"];
                                        endif;
                                endforeach;
                        endif;

                        if ( !empty($product) ) : 
                                $this->export_product_option($this->product['product'][$product['id_product']]);
                                $this->product['product'][$product['id_product']]['price']                             = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['price'], $this->current_currency['iso_code'], $this->currency);
                                if ( !empty($product['combination']) ) : 
                                        foreach( $product['combination'] as $comb_key => $comb ) :
                                                $this->product['product'][$product['id_product']]['combination'][$comb_key]['price']   = $this->eucurrency->doDiffConvert($this->product['product'][$product['id_product']]['combination'][$comb_key]['price'], $this->current_currency['iso_code'], $this->currency);
                                                $this->comb_count[count($comb['attributes'])]   = !empty($this->comb_count[count($comb['attributes'])]) ? ++$this->comb_count[count($comb['attributes'])] : 1;
                                        endforeach;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_product_option($product = '') {
                        if ( !empty($product) && !empty($this->product_option) && array_key_exists($product['id_product'], $this->product_option)  ) :
                                $id = $product['id_product'];
                                if ( array_key_exists("id_product", $this->product_option[$id]) ) :
                                        if ( empty($product['combination']) ) : 
                                                $this->product['product'][$id]['price']              = (isset($this->product_option[$id]['price']) && (int)$this->product_option[$id]['price'] != 0) ? $this->product_option[$id]['price'] : $product['price'];
                                                $this->product['product'][$id]['quantity']           = isset($this->product_option[$id]['force']) ? $this->product_option[$id]['force'] : $product['quantity'];
                                                $this->product['product'][$id]['disable']            = !empty($this->product_option[$id]['disable']) ? 1 : (!is_numeric($this->product_option[$id]['disable']) && !empty($product['disable']) ? 1 : 0);
                                                $this->product['product'][$id]['shipping_override']  = !empty($this->product_option[$id]['shipping']) ? $this->product_option[$id]['shipping'] : 0;
                                        else :  
                                                if ( !empty($this->product_option[$id]['id_product_attribute']) && array_key_exists($this->product_option[$id]['id_product_attribute'], $product['combination']) ) :
                                                        $id_combination                              = $this->product_option[$id]['id_product_attribute'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['price']    = (isset($this->product_option[$id]['price']) && (int)$this->product_option[$id]['price'] != 0) ? $this->product_option[$id]['price'] : $product['combination'][$id_combination]['price'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['quantity'] = isset($this->product_option[$id]['force']) ? $this->product_option[$id]['force'] : $product['combination'][$id_combination]['quantity'];
                                                        $this->product['product'][$id]
                                                        ['combination'][$id_combination]['disable']  = !empty($this->product_option[$id]['disable']) ? 1 : (!is_numeric($this->product_option[$id]['disable']) && !empty($product['combination'][$id_combination]['disable']) ? 1 : 0);
                                                        $this->product['product'][$id]['shipping_override']  = !empty($this->product_option[$id]['shipping']) ? $this->product_option[$id]['shipping'] : 0;
                                                endif;
                                        endif;

                                else :
                                        if ( empty($product['combination']) ) : 
                                                $this->product['product'][$id]['disable']            = !empty($product['disable']) ? 1 : 0;
                                        else :  
                                                foreach ( $this->product_option[$id] as $product_option_attribute ) :
                                                        if ( !empty($product_option_attribute['id_product_attribute']) && array_key_exists($product_option_attribute['id_product_attribute'], $product['combination']) ) :
                                                                $id_combination                              = $product_option_attribute['id_product_attribute'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['price']    = (isset($product_option_attribute['price']) && (int)$product_option_attribute['price'] != 0) ? $product_option_attribute['price'] : $product['combination'][$id_combination]['price'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['quantity'] = isset($product_option_attribute['force']) ? $product_option_attribute['force'] : $product['combination'][$id_combination]['quantity'];
                                                                $this->product['product'][$id]
                                                                ['combination'][$id_combination]['disable']  = !empty($product_option_attribute['disable']) ? 1 : (!is_numeric($product_option_attribute['disable']) && !empty($product['combination'][$id_combination]['disable']) ? 1 : 0);
                                                                $this->product['product'][$id]['shipping_override']  = !empty($product_option_attribute['shipping']) ? $product_option_attribute['shipping'] : 0;
                                                        endif;
                                                endforeach;
                                        endif;
                                endif;
                        else :
                                $id = $product['id_product'];
                                if ( empty($product['combination']) ) : 
                                        $this->product['product'][$id]['disable']            = !empty($product['disable']) ? 1 : 0;
                                else :
                                        foreach ( $product['combination'] as $id_combination => $product_option_attribute ) :
                                                $this->product['product'][$id]
                                                ['combination'][$id_combination]['disable']  = !empty($product_option_attribute['disable']) ? 1 : 0;
                                        endforeach;
                                endif;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_revise_relists($product = '', $product_id = '') {
                        if ( empty($product) || empty($product_id) ) :
                                return;
                        endif;

                        $time_relist                = strtotime($product['relist_date']) - strtotime('now');
                        if ( $time_relist < 0 && $product['listing_duration'] != 'GTC' ) :
                                $this->product_relist['product'][$product_id]   = $product;
                        else :
                                $this->product_revise['product'][$product_id]   = $product;
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name  == 'EBAY_DEVID' )               $result['devID']                = $value;
                                else if ( $name == 'EBAY_USER' )            $result['userID']               = $value;
                                else if ( $name == 'EBAY_APPID' )           $result['appID']                = $value;
                                else if ( $name == 'EBAY_CERTID' )          $result['certID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'EBAY_SITE_ID' )         $result['site']                 = $value;
                                else if ( $name == 'EBAY_CURRENCY' )        $result['currency']             = $value;
                                else if ( $name == 'EBAY_DISPATCH_TIME' )   $result['dispatch_time']        = $value;
                                else if ( $name == 'EBAY_LI_DURATION' )     $result['listing_duration']     = $value;
                                else if ( $name == 'EBAY_POSTAL_CODE' )     $result['postal_code']          = $value;
                                else if ( $name == 'EBAY_RE_DAYS' )         $result['returns_within']       = $value;
                                else if ( $name == 'EBAY_RE_INFORMATION' )  $result['returns_information']  = $value;
                                else if ( $name == 'EBAY_RE_PAYS' )         $result['returns_pays']         = $value;
                                else if ( $name == 'EBAY_RE_POLICY' )       $result['returns_policy']       = $value;
                                else if ( $name == 'PAYER_EMAIL' )          $result['paypal_email']         = $value;
                                else if ( $name == 'EBAY_ADDRESS' )         $result['address']              = $value;
                                else if ( $name == 'FEED_MODE' )            $result['feed_mode']            = $value;
                                else if ( $name == 'EBAY_CATEGORY' )        $result['category_json']        = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                                else if ( $name == 'EBAY_PAYMENT' )         $result['payment_method']       = unserialize($value);
                                else if ( $name == 'EBAY_NO_IMAGE' )        $result['no_image']             = $value;
                                else if ( $name == 'EBAY_DISCOUNT' )        $result['discount']             = $value;
                                else if ( $name == 'EBAY_SYNCHRONIZE' )     $result['synchronization']      = $value;
                                else if ( $name == 'EBAY_VERIFY_PRODUCT' )  $result['verify_product']       = $value;
                                else if ( $name == 'EBAY_ENABLED_MANUAL' )  $result['enabled_manual']       = $value;
                                else if ( $name == 'EBAY_PROFILE_RULES' )   $result['profile_rules']        = $value;
                        endwhile;
                        return $result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics($parser = '', $type = '', $name = '', $result_insert = '') {
                        if ( empty($parser) ) :
                                return;
                        endif;
                        
                        $this->total_response_log                           = !empty($this->total_response_log)     ? $this->total_response_log     : 0;
                        $this->success_response_log                         = !empty($this->success_response_log)   ? $this->success_response_log   : 0;
                        $this->warning_response_log                         = !empty($this->warning_response_log)   ? $this->warning_response_log   : 0;
                        $this->error_response_log                           = !empty($this->error_response_log)     ? $this->error_response_log     : 0;
                        $this->delete_already                               = 0;
                        $ack                                                = $parser->Ack->__toString();
                        
                        if ( !empty($parser->CorrelationID) && count($parser->CorrelationID) ) :
                                $correlation_id_array = @explode("-#-#-", $parser->CorrelationID->__toString());
                                if(sizeof($correlation_id_array) != 4) {
                                        return;
                                }
                                list($productName, $productID, $this->batch_id, $sku)= $correlation_id_array;
                        elseif ( isset($name) ) :
                                $name_array = @explode("-#-#-", $name);
                                if(sizeof($name_array) != 4){
                                        return;
                                }
                                list($productName, $productID, $this->batch_id, $sku)= $name_array;
                        endif;
                        
                        if ( isset($parser->Errors) ) :
                                $error_message_show = array();
                                foreach ( $parser->Errors as $error_message) :
                                        if ( $error_message->ErrorCode->__toString() == '1036' ) :
                                                $this->error_response_log++;
                                                $this->total_response_log++;
                                                return $result_insert;
                                        endif;
                                        if ( $error_message->ErrorCode->__toString() == '1047' && $type == 'EndFixedPriceItem' ) :
                                                $this->delete_already       = 1;
                                        endif;
                                        if ( $error_message->ErrorCode->__toString() != '731' 
                                          && $error_message->ErrorCode->__toString() != '732' 
                                          && $error_message->ErrorCode->__toString() != '11012' 
                                          && $error_message->ErrorCode->__toString() != '11015' 
                                          && $error_message->ErrorCode->__toString() != '21919158'
                                          && $error_message->ErrorCode->__toString() != '21917108'
                                          && $error_message->ErrorCode->__toString() != '21915465'
                                          && $error_message->ErrorCode->__toString() != '21919403'
                                          && $error_message->ErrorCode->__toString() != '21919258'
                                          && $error_message->ErrorCode->__toString() != '21917236'
                                          && $error_message->ErrorCode->__toString() != '21919401' ) :
                                                    $error_message_show[$error_message->ErrorCode->__toString()] = $error_message->LongMessage->__toString();
                                        endif;
                                endforeach;
                        endif;
                        
                        if ( ($ack == 'Success' || $ack == 'Warning') && $type != 'EndFixedPriceItem' ) :
                                if ($ack == 'Success') :
                                        $this->success_response_log++;
                                else :
                                        $error_check = 0;
                                        foreach ( $parser->Errors as $error_message) :
                                                if ( $error_message->ErrorCode->__toString() == '11012' || $error_message->ErrorCode->__toString() == '21919258' || $error_message->ErrorCode->__toString() == '21919158' || $error_message->ErrorCode->__toString() == '21917108' || $error_message->ErrorCode->__toString() == '21915465' || $error_message->ErrorCode->__toString() == '21919401' || $error_message->ErrorCode->__toString() == '21919403' || $error_message->ErrorCode->__toString() == '21917236' ) :
                                                        $error_check++;
                                                endif;
                                        endforeach;
                                        if ( $error_check > 0/*== count($parser->Errors)*/ ) :
                                                $this->success_response_log++;
                                                $ack = 'Success';
                                        else :
                                                $this->warning_response_log++;
                                        endif;
                                endif;
                                $this->flag_sql                                     .= ImportLog::update_flag_offers(3, $this->site, $this->id_shop, $productID, 0);
                                $this->total_response_log++;
                                unset($data_insert);
                                $data_insert                                = array(
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_item"                           => $parser->ItemID->__toString(),
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                        "name_product"                      => isset($productName) ? html_special($productName) : 0,
                                        "date_add"                          => date("Y-m-d H:i:s", strtotime("now")), 
                                        "date_end"                          => date("Y-m-d H:i:s", strtotime($parser->EndTime->__toString())),
                                        "is_enabled"                        => 1,
                                );
                                $result_insert                              .= $this->objectbiz->mappingTableOnly("ebay_product_item_id", $data_insert);
                                
                                unset($data_insert, $data_where);
                                $data_insert                                = array(
                                        "id_item"                           => $parser->ItemID->__toString(),
                                        "export_pass"                       => 1,
                                        "is_enabled"                        => 1,
                                );
                                
                                $data_where                                 = array(
                                        "batch_id"                          => $this->batch_id,
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                );
                                $result_insert                              .= $this->objectbiz->updateColumnOnly("ebay_product_details", $data_insert, $data_where);
                                
                        elseif ( ($ack == 'Success' || $ack == 'Warning') && $type == 'EndFixedPriceItem' ) :
                                unset($data_insert, $data_where);
                                $data_insert                                = array(
                                        "is_enabled"                        => 0,
                                );

                                $data_where                                = array(
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                );
                                $result_insert                              .= $this->objectbiz->updateColumnOnly('ebay_product_item_id', $data_insert, $data_where);
                                
                                unset($data_insert, $data_where);
                                $data_insert                                = array(
                                        "id_item"                           => $parser->ItemID->__toString(),
                                        "export_pass"                       => 1,
                                        "is_enabled"                        => 0,
                                );
                                
                                $data_where                                 = array(
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                );
                                $result_insert                              .= $this->objectbiz->updateColumnOnly('ebay_product_details', $data_insert, $data_where);
                                ($ack == 'Success')   ? $this->success_response_log++ : $this->warning_response_log++;
                        elseif ( $ack == 'Failure' && $type == 'EndFixedPriceItem' && $this->delete_already == 1 ) :
                                unset($data_insert, $data_where);
                                $data_insert                                = array(
                                        "is_enabled"                        => 0,
                                );

                                $data_where                                = array(
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                );
                                $result_insert                              .= $this->objectbiz->updateColumnOnly('ebay_product_item_id', $data_insert, $data_where);

                                unset($data_insert, $data_where);
                                $data_insert                                = array(
                                        "id_item"                           => $parser->ItemID->__toString(),
                                        "export_pass"                       => 1,
                                        "is_enabled"                        => 1,
                                );

                                $data_where                                 = array(
                                        "id_product"                        => isset($productID) ? $productID : 0,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "SKU"                               => $sku,
                                        "ebay_user"                         => $this->ebayUser,
                                );
                                $result_insert                              .= $this->objectbiz->updateColumnOnly('ebay_product_details', $data_insert, $data_where);
                                $ack                                        = 'Success';
                                $this->success_response_log++;
                                $this->total_response_log++;
                        elseif ( $ack == 'Failure' ) :
                                $this->error_response_log++;
                                $this->total_response_log++;
                        endif;

                        unset($data_insert);
                        
                        $error_message_show = empty($error_message_show) ? array (''=>'') : $error_message_show;
                        
                        foreach($error_message_show as $ecode=>$emessage){
                                $data_insert                                        = array(
                                        "batch_id"                                  => $this->batch_id,
                                        "id_product"                                => isset($productID) ? $productID : 0,
                                        "id_site"                                   => $this->site,
                                        "id_shop"                                   => $this->id_shop,
                                        "ebay_user"                                 => $this->ebayUser,
                                        "sku_export"                                => $sku,
                                        "name_product"                              => isset($productName) ? html_special($productName) : 0,
                                        "response"                                  => $ack,
                                        "type"                                      => $type,
                                        "code"                                      => $ecode,
                                        "message"                                   => html_special($emessage),
                                        "combination"                               => 0, 
                                        "date_add"                                  => date("Y-m-d H:i:s", strtotime("now")),
                                );
                                $result_insert                                      .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                        }
                        
                        return $result_insert;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics_log() {
                        $this->objectbiz->updateEbayStatisticsLog($this->user_name, $this->batch_id);
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_statistics_sql() {
                        return $this->objectbiz->updateEbayStatisticsLogSql($this->user_name, $this->batch_id);
                }
                
                function export_mail($from, $to) {
                        $transport = Swift_SmtpTransport::newInstance();
                        $mailer = Swift_Mailer::newInstance($transport); 

                        $action = '';
                        $config['base_url'] = DIR_DOMAIN;
                        if (!empty($config['base_url'])) {
                                $base_url = isset($config['base_url']) && !empty($config['base_url']) ? $config['base_url'] : $this->dir_server;
                        } elseif ( !empty($this->dir_server) ) {
                                $base_url = $this->dir_server;
                        } else {
                                $base_url = base_url();
                        }
                        $logo                   = '<img src="' . $base_url . '/nblk/images/feedbiz_logob.png" alt="feed.biz" style="border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;" />';
                        $ebaylogo               = '<img height=40 src="' . $base_url . '/assets/images/EBay_logo.svg-Custom.png" alt="eBay" >';
                        $this->dir_name         = USERDATA_PATH.$this->user_name."/ebay/product";
                        
                        list($id_packages)      = $this->connect->fetch($this->connect->get_packages_from_site($this->site, $this->id_marketplace));
                        $feed = array();
                        $feed['iso']            = $this->country;
                        $feed['country']        = $this->site_name;
                        $feed['market_place']   = "eBay";
                        $result                 = $this->objectbiz->getProductLatestBatchIDUploadSummary($this->batch_id);

                        $assign                 = array(
                                'logo'          => $logo, 
                                'ebaylogo'      => $ebaylogo, 
                                'fname'         => $this->first_name, 
                                'lname'         => $this->last_name,
                                'feed'          => $feed,
                                'results'       => $result,
                                'lang'       => $this->language,
                        );
                        $this->core->_smarty->assign($assign);
                        
                        if ( !empty($result) ) :
                                $templates  = $this->core->_smarty->fetch(dirname(__FILE__) . '/../../views/templates/email_template/ebay_feedsubmissionresult.tpl');
                                $swift_message = Swift_Message::newInstance();
                                $swift_message->setSubject('Feed.biz : eBay '.$this->country." - ".$action." result");
                                $swift_message->setFrom(array($from => 'Feed.biz'));
                                $swift_message->setBody($templates, 'text/html');
                                $swift_message->setTo($to);
                                $mailer->send($swift_message);
                        endif;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function getJobsCheck($status) {
                        $this->ebaylib->getJobs($this->site, $status);
                        return $this->ebaylib->core->_parser;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function abortJobCheck() {
                        $this->objectbiz->checkDB("ebay_job_task");
                        $jobStatus = array('Failed', 'Created');
                        foreach ($jobStatus as $status) :
                                unset($abort_jobID, $result_insert, $data_set, $where_set);
                                $abort_jobID = $this->getJobsCheck($status);
                                if ( !empty($abort_jobID) && !empty($abort_jobID->jobProfile) && count($abort_jobID->jobProfile) ) :
                                        foreach ( $abort_jobID->jobProfile as $jobID ) :
                                                if ( empty($jobID->completionTime) ) :
                                                        $this->ebaylib->abortJob($this->site, $jobID->jobId->__toString());
                                                        $data_set                       = array(
                                                                'jobStatus'             => 'Aborted',
                                                                'percentComplete'       => '100%',
                                                        );
                                                        $where_set                      = array(
                                                                'jobId'                 => $jobID->jobId->__toString(),
                                                                'fileReferenceId'       => $jobID->inputFileReferenceId->__toString(),
                                                                'ebay_user'             => $this->ebayUser,
                                                        );
                                                        $result_insert                  = $this->objectbiz->updateColumn('ebay_job_task', $data_set, $where_set);
                                                endif;
                                        endforeach;
                                endif;
                        endforeach;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_end_compress() {   
                        $product                                            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                        $product->get_inventory();
                        $product->generate_ebay_product_item_id();
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getIDItemByLast($this->user_name, $this->site, $this->id_shop, $this->ebayUser));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_product', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => $endItem['id_product'],
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        $endProduct[$Item['id_item']]                   = array(
                                                                'id_product'        => $Item['id_product'],
                                                                'id_item'           => $Item['id_item'],
                                                                'sku'               => $Item['SKU'],
                                                                'name'              => $Item['name_product'],
                                                                'batch_id'          => $this->batch_id,
                                                        );
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_zero_active_end_compress() {
                        $product                                            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                        $product->get_inventory();
                        $product->generate_ebay_product_item_id();
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getDeleteZeroActiveConfiguration($this->user_name, $this->ebayUser));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_product', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => $endItem['id_product'],
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        if ( !empty($Item) && array_key_exists('id_product', $Item) ) :
                                                                $endProduct[$Item['id_item']]                   = array(
                                                                        'id_product'        => $Item['id_product'],
                                                                        'id_item'           => $Item['id_item'],
                                                                        'sku'               => $Item['SKU'],
                                                                        'name'              => $Item['name_product'],
                                                                        'batch_id'          => $this->batch_id,
                                                                );
                                                        endif;
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_end_option_disable_compress() {
                        $product                                            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                        $product->get_inventory();
                        $product->generate_ebay_product_item_id();
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getDeleteOptionDisable($this->user_name, $this->site, $this->id_shop, $this->ebayUser));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_product', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => $endItem['id_product'],
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        if ( !empty($Item) && array_key_exists('id_product', $Item) ) :
                                                                $endProduct[$Item['id_item']]                   = array(
                                                                        'id_product'        => $Item['id_product'],
                                                                        'id_item'           => $Item['id_item'],
                                                                        'sku'               => $Item['SKU'],
                                                                        'name'              => $Item['name_product'],
                                                                        'batch_id'          => $this->batch_id,
                                                                );
                                                        endif;
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_out_of_stock_end_compress() {
                        $this->abortJobCheck();
                        $endProduct                 = $this->objectbiz->getDeleteProductOutOfStock($this->user_name, $this->site, $this->id_shop, $this->ebayUser, $this->batch_id);
                        if ( !empty($endProduct) ) :
                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_remove_custom() {
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getRemoveCustoms($this->user_name, $this->site, $this->id_shop, $this->ebayUser));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_product', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => $endItem['id_product'],
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        if ( !empty($Item) && array_key_exists('id_product', $Item) ) :
                                                                $endProduct[$Item['id_item']]                   = array(
                                                                        'id_product'        => $Item['id_product'],
                                                                        'id_item'           => $Item['id_item'],
                                                                        'sku'               => $Item['SKU'],
                                                                        'name'              => $Item['name_product'],
                                                                        'batch_id'          => $this->batch_id,
                                                                );
                                                        endif;
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }

                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_sync_zero_end_compress($percent = null) {
                        $product                                            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                        $product->get_percent_inventory(!empty($percent) ? $percent : '');
                        $product->generate_ebay_product_item_id();
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_item', '', $this->objectbiz->getDeleteQuantityZeroSync($this->ebayUser, $this->site));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_item', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => '9999',
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        if ( !empty($Item) && array_key_exists('id_item', $Item) ) :
                                                                $endProduct[$Item['id_item']] = array(
                                                                        'id_product'        => '9999',
                                                                        'id_item'           => $Item['id_item'],
                                                                        'sku'               => $Item['SKU'],
                                                                        'name'              => $Item['name_product'],
                                                                        'batch_id'          => $this->batch_id,
                                                                );
                                                        endif;
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_product_active_end_compress($percent = null) {
                        $product                                            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                        $product->get_percent_inventory(!empty($percent) ? $percent : '');
                        $product->generate_ebay_product_item_id();
                        $this->abortJobCheck();
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_item', '', $this->objectbiz->getDeleteProductActiveConfiguration($this->ebayUser, $this->site, $this->id_shop));
                        if ( !empty($this->itemID) && count($this->itemID) ) :
                                foreach ( $this->itemID as $key => $endItem ) :
                                        if ( !empty($endItem) && array_key_exists('id_item', $endItem) ) :
                                                $endProduct[$key]                   = array(
                                                        'id_product'                => '9999',
                                                        'id_item'                   => $endItem['id_item'],
                                                        'sku'                       => $endItem['SKU'],
                                                        'name'                      => $endItem['name_product'],
                                                        'batch_id'                  => $this->batch_id,
                                                );
                                        else :
                                                foreach ( $endItem as $Item) :
                                                        if ( !empty($Item) && array_key_exists('id_item', $Item) ) :
                                                                $endProduct[$Item['id_item']] = array(
                                                                        'id_product'        => '9999',
                                                                        'id_item'           => $Item['id_item'],
                                                                        'sku'               => $Item['SKU'],
                                                                        'name'              => $Item['name_product'],
                                                                        'batch_id'          => $this->batch_id,
                                                                );
                                                        endif;
                                                endforeach;
                                        endif;
                                endforeach;

                                $this->_result['AddFixedPriceItem']         = 'No Used';
                                $this->_result['RelistFixedPriceItem']      = 'No Used';
                                $this->_result['ReviseFixedPriceItem']      = 'No Used';
                                $this->_result['EndFixedPriceItem']         = 'No Used';
                                
                                $this->dir_name                             = USERDATA_PATH.$this->user_name."/ebay/delete/$this->site";
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.xml.gz") ) unlink($this->dir_name."/EndFixedPriceItem.xml.gz");
                                if ( file_exists($this->dir_name."/EndFixedPriceItem.zip") ) unlink($this->dir_name."/EndFixedPriceItem.zip");
                                $this->_result['EndFixedPriceItem']         = $this->ebaylib->BulkEnd($this->site, $endProduct, "EndFixedPriceItem", "product", $this->user_name);
                                
                                $data_table_statistics_log                  = $this->objectbiz->checkDB("ebay_statistics_log");
                                if ( $data_table_statistics_log ) :
                                        unset($data_insert);
                                        $data_insert                        = array(
                                                "batch_id"                  => $this->batch_id,
                                                "id_site"                   => $this->site,
                                                "id_shop"                   => $this->id_shop,
                                                "type"                      => 'Product',
                                                "method"                    => 'Remove',
                                                "total"                     => count($endProduct),
                                                "success"                   => 0,
                                                "warning"                   => 0,
                                                "error"                     => 0,
                                                "date_add"                  => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]              = $data_insert;
                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                        else :
                                $this->_result['msg']['empty_products'] = 1;
                        endif;
                        return $this->_result;
                }
                
                function export_product_item($id_product = null) {
                        if ( !isset($id_product) && empty($id_product) ) :
                                return null;
                        endif;
                        $products                                   = new Product($this->user_name, $id_product, $this->id_lang, $this->mode['mode'], $this->id_shop);
                        $this->product['product'][$id_product]      = $this->object_product_list($products);
                }
                
                function export_product_group($product_list = array()) {
                        if ( !isset($product_list) && empty($product_list) ) :
                                return null;
                        endif;
                        unset($data);
                        $page = 0;
                        $data                   = $this->objectbiz->exportProductsArray($this->user_name, $this->id_shop, $this->mode['mode'], mb_substr($this->language, 0, 2), $page, $product_list);
                        $product['product']     = empty($product['product']) ? (empty($data) ? array() : $data['product']) : ($product['product'] + (empty($data) ? array() : $data['product']));
                        $this->product          = $product;
                }
                
                function export_product_set($id_products = array()) {
                        if ( !isset($id_products) && empty($id_products) ) :
                                return null;
                        endif;
                        foreach ( $id_products as $id_product ) :
                                $products                                   = new Product($this->user_name, $id_product, $this->id_lang, $this->mode['mode'], $this->id_shop);
                                $this->product['product'][$id_product]      = $this->object_product_list($products);
                        endforeach;
                }
                
                function export_product_list() {
                        $page = 1;
                        $product['product'] = array();
                        do {
                                unset($data);
                                $data                   = $this->objectbiz->exportProductList($this->user_name, $this->id_shop, $this->mode['mode'], mb_substr($this->language, 0, 2), $page);
                                $product['product']     = empty($product['product']) ? (empty($data) ? array() : $data['product']) : ($product['product'] + (empty($data) ? array() : $data['product']));
                                $page++;
                        } while( !empty($data) && count($data) );
                        $this->product = $product;
                        $this->_result['shop'] = $this->id_shop;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function addition_array_group($data = null) {
                        if ( !empty($data) && is_array($data) && !array_key_exists($this->change_wanted, $data) ) :
                                array_map('ebayexport::addition_array_group', $data);
                        elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_wanted, $data)) :
                                $this->change_data[$this->change_keys][]     = $data[$this->change_wanted];
                        endif;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function change_array_keys($data = null) {
                        if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                                array_map('ebayexport::change_array_keys', $data);
                        elseif ( !empty($data) && is_array($data) && array_key_exists($this->change_keys, $data)) : 
                                if ( !isset($this->change_data[$data[$this->change_keys]]) ) : 
                                        $this->change_data[$data[$this->change_keys]]   = $data;
                                else : 
                                        if ( is_array($this->change_data[$data[$this->change_keys]]) && count(current($this->change_data[$data[$this->change_keys]])) > 1 ) :
                                                $this->change_data[$data[$this->change_keys]][]   = $data;
                                        else :
                                                $store_data                                     = $this->change_data[$data[$this->change_keys]];
                                                unset($this->change_data[$data[$this->change_keys]]);
                                                $this->change_data[$data[$this->change_keys]][]   = $store_data;
                                                $this->change_data[$data[$this->change_keys]][]   = $data;
                                        endif;
                                endif;
                        endif;
                }
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function generate_array_key($function = null, $key = null, $wanted = null, $data = array()) {
                        if ( empty($function) || empty($key) || empty($data) ) :
                                return $data;
                        endif;
                        $this->change_data                      = array();
                        $this->change_wanted                    = $wanted;
                        $this->change_keys                      = $key;
                        array_map($function, $data);
                        return $this->change_data;
                }
                
                function export_method() {
                        $methods                                = array('AddFixedPriceItem', 'RelistFixedPriceItem', 'ReviseFixedPriceItem', 'EndFixedPriceItem', 'VerifyAddFixedPriceItem');
                        foreach ( $methods as $method ) :
                                if ( file_exists($this->dir_name."/".$method.".xml.gz") ) unlink($this->dir_name."/".$method.".xml.gz");
                                if ( file_exists($this->dir_name."/".$method.".zip") ) unlink($this->dir_name."/".$method.".zip");        
                                $this->_result[$method]         = 'No Used';
                        endforeach;
                }
                
                
                function object_product_list($product = array(), $data = array()) {
                        if ( empty($product) ) :
                                return NULL;
                        endif;
                        $count = 0;
                        if( isset($product) && !empty($product) && isset($product->id_product) ) {
                                    $data['id_product']        = $product->id_product;
                                    $data['name']              = $product->name;
                                    $data['description']       = $product->description;
                                    $data['description_short'] = isset($product->description_short) ? $product->description_short : '' ;
                                    $data['sku']               = isset($product->sku) ? $product->sku : '' ;
                                    $data['upc']               = isset($product->upc) ? $product->upc : '' ;
                                    $data['ean13']             = isset($product->ean13) ? $product->ean13 : '' ;
                                    $data['reference']         = isset($product->reference) ? $product->reference : '' ;
                                    $data['quantity']          = isset($product->quantity) && $product->quantity > -1 ? ($product->active == 0 ? 0 : $product->quantity) : 0 ;
                                    $data['condition']         = isset($product->condition) ? $product->condition : '' ;
                                    $data['weight']            = isset($product->weight) ? $product->weight : '';                
                                    $data['price']             = isset($product->price) ? $product->price : '';
                                    $data['manufacturer']      = isset($product->manufacturer) ? $product->manufacturer : '';
                                    $data['supplier']          = isset($product->supplier) ? $product->supplier : '';
                                    $data['width']             = isset($product->width) ? $product->width : '';
                                    $data['height']            = isset($product->height) ? $product->height : '';
                                    $data['depth']             = isset($product->depth) ? $product->depth : '';
                                    $data['weight']            = isset($product->weight) ? $product->weight : '';
                                    $data['active']            = isset($product->active) ? $product->active : '';
                                    $data['currency']          = isset($product->currency) ? $product->currency : '';
                                    $data['feature']           = isset($product->feature) ? $product->feature : '';

                                    if(isset($product->on_sale) && $product->on_sale == 1)
                                            $data['sale'] = $product->sale;

                                    $data['carrier']       = isset($product->carrier) ? $product->carrier : '';

                                    if(isset($product->currency) && !empty($product->currency)) {
                                            foreach ($product->currency as $currency)
                                                    if(isset($currency['name']) || !empty($currency['name']))
                                                            $data['currency'] = strtolower($currency['name']);
                                    }
                                    else {
                                            $data['currency'] = '';
                                    }

                                    if (isset($product->id_category_default)) 
                                            $data['id_category_default'] = $product->id_category_default;
                                    else
                                            $data['category'] = '';

                                    if(isset($product->image) && !empty($product->image)) {
                                            foreach ($product->image as $key_image => $image) {
                                                    if(isset($image['image_url']) || !empty($image['image_url'])) {
                                                            $data['images'][$key_image]['url']  = $image['image_url'];
                                                            $data['images'][$key_image]['type']  = $image['image_type'];
                                                    }
                                            }
                                    }

                                    $data['combination']   = isset($product->combination) ? $product->combination : '';
                        }
                        return $data;
                }
                
                
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function change_array_group($data = null) {
                        if ( !empty($data) && is_array($data) && !array_key_exists($this->change_keys, $data) ) :
                                array_map('Ebay::change_array_group', $data);
                        elseif ( !empty($data) && is_array($data)  && array_key_exists($this->change_keys, $data)) :
                                if ( is_array($this->change_wanted) ) :
                                        unset($add_data);
                                        foreach ( $this->change_wanted as $key => $wanted ) :
                                                if ( is_array($wanted) ) :
                                                        foreach ( $wanted as $child_key => $child_wanted ) :
                                                                if ( is_array($child_wanted) ) :
        //                                                                $add_data[$key]         = $data[$wanted];
                                                                else :
                                                                        $val_wanted             = array_key_exists($key, $data) ? $data[$key] : $key;
                                                                        $key_wanted             = array_key_exists($child_key, $data) ? $data[$child_key] : (!is_numeric($child_key) ? $child_key : $child_wanted);
        //                                                                echo "<pre>", print_r($val_wanted, true), "</pre>";exit();
                                                                        $this->change_data[$data[$this->change_keys]][$val_wanted][$key_wanted][]     = ($child_wanted != 'GROUPALL') ? $data[$child_wanted] : $data;
                                                                endif;
                                                        endforeach;
                                                else :  
                                                        $val_wanted             = array_key_exists($key, $data) ? $data[$key] : (!is_numeric($key) ? $key : $wanted);
                                                        $this->change_data[$data[$this->change_keys]][$val_wanted][]     = ($wanted != 'GROUPALL') ? $data[$wanted] : $data;
                                                endif;
                                        endforeach;
                                else :
                                        $this->change_data[$data[$this->change_keys]][]     = $data[$this->change_wanted];
                                endif;
                        endif;
                }
                
                function percent_count($page = null, $product_count = 0) {
                        if ( empty($product_count) || empty($page) ) :
                                return 0;
                        endif;
                        $percent_counts = intval(($page * 30) / $product_count);
                        if ( !empty($this->percent) ) : $this->percent->set_running_task($percent_counts + $this->percent_count); endif;
                }
                
                function compress_config($config = null) {
                        if ( empty($config) ) :
                                return;
                        endif;
                        $this->compress_config = $config;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function preparing_export($synchronize = false, $result_insert = '') {
                        if ( !empty($this->percent) ) : $this->percent->set_running_task(2); endif;
                        if( $synchronize ) :
                                $product            = new ebaysynchronization(array($this->user_name, $this->user_id, $this->site));
                                $product->get_inventory();
                                $product->generate_ebay_product_item_id();    
                                if ( !empty($this->percent) ) : $this->percent->set_running_task($this->percent_count); endif;
                        endif;
                        $this->variation_attributes      = $this->generate_array_key('ebayexport::change_array_keys', 'id_item', '', $this->objectbiz->getSynchronizationAttribute($this->ebayUser, $this->site));
                        $this->abortJobCheck();
                        $this->ebaylib->SetUserPreferences($this->site);
                        $this->ebaylib->GetUserPreferences($this->site);
                        $getPreferrences    = $this->ebaylib->core->_parser;
                        if ( !empty($getPreferrences) && !empty($getPreferrences->SellerPaymentPreferences) && !empty($getPreferrences->SellerPaymentPreferences->DefaultPayPalEmailAddress) ) :
                                $this->paypalEmail  = $getPreferrences->SellerPaymentPreferences->DefaultPayPalEmailAddress->__toString();
                        endif;
                        $this->percent_count = 20; 
//                        $this->compress_config = 'export_product_group';
                        switch ( $this->compress_config ) :
                                case "export_product_array"     : $this->export_product_array();
                                        break;
                                case "export_product_group"     : $this->export_product_group(array(1201));
                                        break;
                                case "export_product_db"        : $this->export_product_db();
                                        break;
                                case "export_product_query"     : $this->export_product_query();
                                        break;
                                case "export_product_item"     : $this->export_product_item(1802);
                                        break;
                                default : $this->export_product_array();
                                        break;
                        endswitch;

                        if ( empty( $this->product['product'] ) ) : 
                                $this->_result['msg']['empty_products'] = 1;
                                return;
                        endif;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $profile_data               = $this->objectbiz->getProfileForExport($this->user_name, $this->site, $this->id_shop);
                        $profile_group['default']   = array(0);
                        if ( !empty($profile_data) ) :
                                foreach ( $profile_data as $profile_type ) :
                                        $profile_group[$profile_type['type']][$profile_type['id']]  = $profile_type['id_profile'];
                                endforeach;
                        endif;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $profile_payment = $this->objectbiz->getEbayProfilePayment($this->user_name, $this->site, $this->id_shop, 0);
						
                        if ( $this->profile_rule == 0 )      unset($profile_group['category'], $profile_group['product']);
                        elseif ( $this->profile_rule == 1 )  unset($profile_group['default'], $profile_group['product']);
                        elseif ( $this->profile_rule == 2 )  unset($profile_group['default'], $profile_group['category']);
                        elseif ( $this->profile_rule == 3 )  unset($profile_group['product']);
                        elseif ( $this->profile_rule == 4 )  unset($profile_group['category']);
                        
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($profile_group) ) : $this->_result['msg']['empty_products_in_profile'] = 1;
                                return;
                        endif;
                        
                        list($id_packages)          = $this->connect->fetch($this->connect->get_packages_from_site($this->site, $this->id_marketplace));
                        $categories_used            = $this->generate_array_key('ebayexport::addition_array_group', 'ebay_category', 'id_ebay_category', $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->site, $this->id_shop));
                        $category_group             = !empty($categories_used['ebay_category']) ? "'".implode("', '", $categories_used['ebay_category'])."'" : "''";
                        $query                      = $this->connect->get_conditions_values($this->site, $category_group);
                        while ( list($temp_category, $temp_value)   = $this->connect->fetch($query) ) :
                                $this->condition_list[$temp_category][]        = $temp_value;
                        endwhile;
                        $this->carrier_default      = $this->objectbiz->getCarrierDefault($this->site, $this->id_shop);
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $category_mappings          = $this->generate_array_key('ebayexport::change_array_keys', 'id_category', '', $this->objectbiz->getMappingCategory($this->site, $this->id_shop));
                        $secondary_mappings         = $this->generate_array_key('ebayexport::change_array_keys', 'id_category', '', $this->objectbiz->getSelectedMappingSecondaryCategories($this->user_name, $this->site, $this->id_shop));
                        $store_mappings             = $this->generate_array_key('ebayexport::change_array_keys', 'id_category', '', $this->objectbiz->getSelectedMappingStoreCategories($this->user_name, $this->site, $this->id_shop));
                        $store_secondary_mappings   = $this->generate_array_key('ebayexport::change_array_keys', 'id_category', '', $this->objectbiz->getSelectedMappingSecondaryStoreCategories($this->user_name, $this->site, $this->id_shop));
                        $price_modifier             = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getPriceModifier($this->user_name, $this->site, $this->id_shop));
                        $return_policy              = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getEbayAllReturn($this->user_name, $this->site, $this->id_shop));
                        $getDefault                 = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->geteBayMappingSpecificDefault($this->site, $this->id_shop));
                        $getDomestic                = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->geteBayMappingSpecificDomestic($this->site, $this->id_shop));
                        $getInternational           = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->geteBayMappingSpecificInternational($this->site, $this->id_shop));
                        $getPostalcode              = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getPostalcodeDispatchtime($this->user_name, $this->site, $this->id_shop));
                        $getConfiguration           = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getExportProfileConfiguration($this->user_name, $this->site, $this->id_shop));
                        $this->itemID               = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getIDItemByLast($this->user_name, $this->site, $this->id_shop, $this->ebayUser));
                        $tax_selected               = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getTaxsSelected($this->site, $this->id_shop));
                        $condition_mapping          = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getConditionMap($this->site, $this->id_shop));
                        $this->attribute_value      = $this->generate_array_key('ebayexport::change_array_keys', 'id_profile', '', $this->objectbiz->getSelectedMappingAttributesGroup($this->user_name, $this->site, $this->id_shop, $this->id_lang));
                        $category_group             = $this->generate_array_key('ebayexport::addition_array_group', 'id_category', 'id_ebay_category', $this->objectbiz->getIDCategoriesMapping($this->user_name, $this->site, $this->id_shop));
                        $this->product_option       = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getExportProductOption($id_packages, $this->id_shop, $this->id_lang));
                        $this->expolde_products     = $this->generate_array_key('ebayexport::change_array_keys', 'id_product', '', $this->objectbiz->getEbayExplodeProduct($this->site, $this->id_shop));
                        $templates_description      = $this->objectbiz->getSelectedMappingTemplates($this->user_name, $this->site, $this->id_shop);
                        $this->product_references   = $this->objectbiz->getProductReferences($this->user_name, $this->id_shop);
                        $this->c_product_references = $this->objectbiz->getCombinationProductReferences($this->user_name, $this->id_shop);
                        $category_group             = !empty($category_group['id_category']) ? @implode("', '", $category_group['id_category']) : '';
                        $query_data                 = $this->connect->get_categories_features($this->site, $category_group);
                        $this->current_currency     = $this->objectbiz->getCurrentCurrency($this->id_shop);
                        $fixedShippings = $this->objectbiz->getEbayProfileShipping($this->user_name, $this->site, $this->id_shop, 0);
                        $isFixedShippings = !empty($fixedShippings) && is_array($fixedShippings) && $fixedShippings[0]['is_enabled'] == 1;
                        $fixedShippingsPostCode = $fixedShippings[0]['postal_code'];
                        $fixedShippingsDispatchTime = $fixedShippings[0]['dispatch_time'];
                        if ( !empty($query_data) ) :
                                while ( list($id_category, $is_enabled) = $this->connect->fetch($query_data) ) :
                                        $this->features[$id_category]  = $is_enabled;
                                endwhile;
                        endif;
                        $this->export_method();
                        $this->percent_count = 55;
                        $num = 1;
                        $time = 1;
                        $array_count_product = count($this->product['product']);
                        $last_pid='';
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        foreach ( $this->product['product'] as $id => $product ) :
                                if(!empty($this->product[$last_pid])):
                                    
                                    $this->product[$last_pid]='';
                                    $this->product[$last_pid]=null;
                                endif;
                                $last_pid = $id;
                                if ( intval($num / $time) == intval($array_count_product * 0.25) ) :
                                        $this->percent_count($num, $array_count_product);
                                        $time++;
                                endif;
                                $num++;
                                if ( !empty($profile_group['product']) && array_key_exists($id, $profile_group['product']) ) :
                                        $this->id_profile   = $profile_group['product'][$id];
                                elseif ( !empty($profile_group['category']) && !empty($product['id_category_default']) && array_key_exists($product['id_category_default'], $profile_group['category']) ) :
                                        $this->id_profile   = $profile_group['category'][$product['id_category_default']];
                                elseif ( !empty($profile_group['default']) ) :
                                        $this->id_profile   = 0;
                                else :
//                                        unset($this->product['product'][$id]);
                                        continue;
                                endif;
                                
                                $this->_getDefault          = isset($getDefault[$this->id_profile]) ? (array_key_exists('id_profile', $getDefault[$this->id_profile]) ? array($getDefault[$this->id_profile]) : $getDefault[$this->id_profile]) : '';
                                $_getDomestic               = isset($getDomestic[$this->id_profile]) ? (array_key_exists('id_profile', $getDomestic[$this->id_profile]) ? array($getDomestic[$this->id_profile]) : $getDomestic[$this->id_profile]) : '';
                                $_getInternational          = isset($getInternational[$this->id_profile]) ? (array_key_exists('id_profile', $getInternational[$this->id_profile]) ? array($getInternational[$this->id_profile]) : $getInternational[$this->id_profile]) : '';
                                $_tax_selected              = isset($tax_selected[$this->id_profile]) ? (array_key_exists('id_profile', $tax_selected[$this->id_profile]) ? array($tax_selected[$this->id_profile]) : $tax_selected[$this->id_profile]) : '';
                                $_condition_mapping         = isset($condition_mapping[$this->id_profile]) ? (array_key_exists('id_profile', $condition_mapping[$this->id_profile]) ? array($condition_mapping[$this->id_profile]) : $condition_mapping[$this->id_profile]) : '';
                                $_price_modifier            = isset($price_modifier[$this->id_profile]) ? (array_key_exists('id_profile', $price_modifier[$this->id_profile]) ? array($price_modifier[$this->id_profile]) : $price_modifier[$this->id_profile]) : '';
                                $_getPostalcode             = isset($getPostalcode[$this->id_profile]) ? (array_key_exists('id_profile', $getPostalcode[$this->id_profile]) ? array($getPostalcode[$this->id_profile]) : $getPostalcode[$this->id_profile]) : '';
                                $_getConfiguration          = isset($getConfiguration[$this->id_profile]) ? (array_key_exists('id_profile', $getConfiguration[$this->id_profile]) ? array($getConfiguration[$this->id_profile]) : $getConfiguration[$this->id_profile]) : '';                                
                                $_return_policy             = isset($return_policy[$this->id_profile]) ? (array_key_exists('id_profile', $return_policy[$this->id_profile]) ? array($return_policy[$this->id_profile]) : $return_policy[$this->id_profile]) : '';                                
                                $this->_price_modifier      = $_price_modifier;
                                $this->comb_count 			= array();
                                $this->export_discount_price($this->product['product'][$id]);
                                $this->export_price_modifier($this->product['product'][$id]);
                                $this->export_mapping_category($this->product['product'][$id], $category_mappings, 'category');
                                
//                              export_mapping_category unset product that is under un-mapping category
                                if ( empty($this->product['product'][$id]) ) {
                                	continue;
                                }
                                
                                $this->product['product'][$id]['PaymentInstructions'] = isset($profile_payment) && isset($profile_payment['payment_instructions']) && !empty($profile_payment['payment_instructions']) ? $profile_payment['payment_instructions'] : '';
                                
                                //in case Fixed Shipping, override carrier and postal code
                                if($isFixedShippings){
                                	$_getPostalcode[0]['postcode'] = $fixedShippingsPostCode;
                                	$_getPostalcode[0]['dispatch_time'] = $fixedShippingsDispatchTime;
                                	
                                	$this->product['product'][$id]['carrier'] = array();
                                	foreach ($fixedShippings as $key=>$shipping){
                                		$this->product['product'][$id]['carrier'][$shipping['id_ebay_shipping']] = $shipping;
                                	}
                                }
                                
                                $this->export_mapping_category($this->product['product'][$id], $secondary_mappings, 'secondary_category');             
                                $this->export_mapping_category($this->product['product'][$id], $store_mappings, 'store_category');             
                                $this->export_mapping_category($this->product['product'][$id], $store_secondary_mappings, 'secondary_store_category');
                                $this->export_configuration($this->product['product'][$id], $this->user, $templates_description, $_getDomestic, $_getInternational, $_condition_mapping, $_getPostalcode, $_getConfiguration, $_return_policy, $profile_payment);    
                                $this->product['product'][$id]['wizard'] = !empty($this->wizard) ? 1 : 0;
                                $combination_id = $this->extract_combination($this->product['product'][$id]);
//                              extract_combination unset product that is not featured.

                                if ( !empty($combination_id) && is_array($combination_id) ) {
                                        foreach ( $combination_id as $id ) :
                                                if ( empty($this->product['product'][$id]) ) {
                                                        continue;
                                                }
                                                $result_insert              = $this->slice_product($this->product['product'][$id], $result_insert);
                //                              slice_product unset product that is quantity = 0, price < 1 not sku ean13 upc referrance
                                                if ( empty($this->product['product'][$id]) ) {
                                                        continue;
                                                }
                                                $this->clear_old_attribute($this->product['product'][$id]);
                                                $this->product['product'][$id]['PaymentInstructions'] = isset($profile_payment) && isset($profile_payment['payment_instructions']) && !empty($profile_payment['payment_instructions']) ? $profile_payment['payment_instructions'] : '';

                                                if ( isset($this->product['product'][$id]['relist']) ) : //echo $id."<BR>";
                                                          $this->export_revise_relists($this->product['product'][$id], $id);
                                                else :
                                                        $this->product_add['product'][$id]      = $this->product['product'][$id];
                                                endif;

                                                unset($data_details);
                                                if ( $this->common == 'add' ) :
                                                    $data_details                                = array(
                                                            "batch_id"                          => $this->batch_id,
                                                            "id_product"                        => !empty($this->product['product'][$id]['id_product']) ? $this->product['product'][$id]['id_product'] : '',
                                                            "id_site"                           => $this->site,
                                                            "id_shop"                           => $this->id_shop,
                                                            "SKU"                               => !empty($this->product['product'][$id]['reference']) ? $this->product['product'][$id]['reference'] : (!empty($this->product['product'][$id]['sku']) ? $this->product['product'][$id]['sku'] : ''),
                                                            "ebay_user"                         => $this->ebayUser,
                                                            "name_product"                      => !empty($this->product['product'][$id]['name']) ? html_special($this->product['product'][$id]['name']) : '',
                                                            "description_product"               => '',
                                                            "id_primary_category"               => !empty($this->product['product'][$id]['category']['mapping_id']) ? $this->product['product'][$id]['category']['mapping_id'] : '',
                                                            "id_secondary_category"             => !empty($this->product['product'][$id]['secondary_category']['mapping_id']) ? $this->product['product'][$id]['secondary_category']['mapping_id'] : '',
                                                            "id_store_category"                 => !empty($this->product['product'][$id]['store_category']['mapping_id']) ? $this->product['product'][$id]['store_category']['mapping_id'] : '',
                                                            "id_secondary_store_category"       => !empty($this->product['product'][$id]['secondary_store_category']['mapping_id']) ? $this->product['product'][$id]['secondary_store_category']['mapping_id'] : '',
                                                            "condition_value"                   => !empty($this->product['product'][$id]['condition']) ? $this->product['product'][$id]['condition'] : '',
                                                            "dispatch_time"                     => !empty($this->product['product'][$id]['dispatch_time']) ? $this->product['product'][$id]['dispatch_time'] : '',
                                                            "postal_code"                       => !empty($this->product['product'][$id]['postal_code']) ? $this->product['product'][$id]['postal_code'] : '',
                                                            "listing_duration"                  => !empty($this->product['product'][$id]['listing_duration']) ? $this->product['product'][$id]['listing_duration'] : '',
                                                            "tax_rate"                          => !empty($tax_selected) ? $tax_selected[0]['tax_rate'] : '',
                                                            "price"                             => !empty($this->product['product'][$id]['price']) ? $this->product['product'][$id]['price'] : 0,
                                                            "quantity"                          => !empty($this->product['product'][$id]['quantity']) ? $this->product['product'][$id]['quantity'] : 0,
                                                            "combination_count"                 => !empty($this->product['product'][$id]['combinations']) && is_array($this->product['product'][$id]['combinations']) ? count($this->product['product'][$id]['combinations']) : 0,
                                                            "price_modifier_rate"               => !empty($this->price_modifier_rate) ? $this->price_modifier_rate : '',
                                                            "price_modifier_type"               => !empty($this->price_modifier_type) ? $this->price_modifier_type : '',
                                                            "currency_rate_from"                => !empty($this->eucurrency->eurCurrency) && !empty($this->current_currency['iso_code']) && ($this->current_currency['iso_code'] != 'EUR') ? floatval($this->eucurrency->eurCurrency[$this->current_currency['iso_code']]) : 1,
                                                            "currency_rate_to"                  => !empty($this->eucurrency->eurCurrency) && !empty($this->currency) && ($this->currency != 'EUR') ? floatval($this->eucurrency->eurCurrency[$this->currency]) : 1,
                                                            "date_add"                          => date("Y-m-d H:i:s", strtotime("now")), 
                                                            "date_end"                          => '',
                                                            "export_pass"                       => 0,
                                                            "is_enabled"                        => 1,
                                                    );
                                                    $result_insert                             .= $this->objectbiz->mappingTableOnly("ebay_product_details", $data_details);
                                                endif;
                                        endforeach;
                                }
                        endforeach;//echo "<pre>", print_r($this->product, true), "</pre>";

                        //If empty set log//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( empty($this->product['product'] ) ) :
                                if ( !empty($this->error_request_log) && $this->error_request_log > 0 ) :
                                        unset($data_insert);
                                        $data_insert                                = array(
                                                "batch_id"                          => $this->batch_id,
                                                "id_site"                           => $this->site,
                                                "id_shop"                           => $this->id_shop,
                                                "type"                              => 'Product',
                                                "method"                            => $this->common == 'add' ? 'Created' : 'Revise',
                                                "total"                             => $this->total_request_log,
                                                "success"                           => 0,
                                                "warning"                           => 0,
                                                "error"                             => isset($this->error_request_log) ? $this->error_request_log : 0,
                                                "date_add"                          => date("Y-m-d H:i:s", strtotime("now")), 
                                        );
                                        $data['data_insert'][]                      = $data_insert;
                                        $this->objectbiz->mappingTable("ebay_statistics_log", $data_insert);
                                endif;
                                $this->_result['msg']['empty_products']         = 1;
                                return $result_insert;
                        endif;
                        
                        if ( $this->common == 'add' ) :
                                $this->total_request_log                    = $this->total_request_log - (!empty($this->product_revise['product']) ? count($this->product_revise['product']) : 0);
                        elseif ( $this->common == 'revise' ) :
                                $this->total_request_log                    = $this->total_request_log - (!empty($this->product_add['product']) ? count($this->product_add['product']) : 0);
                        endif;

                        if ( isset($this->error_request_log) ) :
                                unset($data_insert);
                                $data_insert                                = array(
                                        "batch_id"                          => $this->batch_id,
                                        "id_site"                           => $this->site,
                                        "id_shop"                           => $this->id_shop,
                                        "type"                              => 'Product',
                                        "method"                            => $this->common == 'add' ? 'Created' : 'Revise',
                                        "total"                             => $this->total_request_log,
                                        "success"                           => 0,
                                        "warning"                           => 0,
                                        "error"                             => isset($this->error_request_log) ? $this->error_request_log : 0,
                                        "date_add"                          => date("Y-m-d H:i:s", strtotime("now")), 
                                );
                                $data['data_insert'][]                      = $data_insert;
                                $result_insert                             .= $this->objectbiz->mappingTableOnly("ebay_statistics_log", $data_insert);
                        endif;
                        
                        return $result_insert;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_products($synchronize = false, $common = 'add', $products = array()) {
                        $this->common           = $common;
                        $result_insert          = $this->preparing_export($synchronize);
                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        $result_insert = '';
                        if ( !empty($this->product_add) && $common != 'verify' && $common != 'revise'  ) :
                                //$this->product_add$this->product_relist$this->product_revise
                                foreach ($this->product_add['product'] as $product_id => $product_current ) :
                                        $this->ebaylib->AddFixedPriceItem($this->site, $product_current);
                                        $result_insert .= $this->export_statistics($this->ebaylib->core->_parser, 'AddFixedPriceItem', $product_current['name'].'-#-#-'.$product_current['id_product'].'-#-#-'.$this->batch_id.'-#-#-'.(!empty($product_current['reference']) ? $product_current['reference'] : (!empty($product_current['sku']) ? $product_current['sku'] : '')));
                                endforeach;
                        endif;
                        if ( !empty($this->product_revise) && $common == 'revise' ) : 
                                foreach ($this->product_revise['product'] as $product_id => $product_current ) :
                                        $this->ebaylib->ReviseFixedPriceItem($this->site, $product_current);
                                        $result_insert .= $this->export_statistics($this->ebaylib->core->_parser, 'ReviseFixedPriceItem', $product_current['name'].'-#-#-'.$product_current['id_product'].'-#-#-'.$this->batch_id.'-#-#-'.(!empty($product_current['reference']) ? $product_current['reference'] : (!empty($product_current['sku']) ? $product_current['sku'] : '')));
                                endforeach;
                        endif;
                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        $this->export_statistics_log();
                        unset($this->product, $this->product_add, $this->product_revise);
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_compress($synchronize = false, $common = 'add', $percent = '') {
                        $this->percent_count    = 0;
                        $this->common           = $common;
                        if ( !empty($percent) ) : 
                                $this->percent = $percent;
                                $this->percent->set_running_task($this->percent_count); 
                        endif;
                        $result_insert = $this->preparing_export($synchronize);
                        if ( !empty($this->_result['msg']['empty_products']) ) :
                                !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                                if ( !empty($percent) ) : $this->percent->set_running_task(100); endif;
                                return $this->_result;
                        endif;
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($this->product_add) && $common != 'verify' && $common != 'revise'  ) {
                                $this->_result['AddFixedPriceItem']     = $this->ebaylib->Bulk($this->site, $this->product_add, "AddFixedPriceItem", "product", $this->user_name);
                                if ( !empty($this->ebaylib->core->_xmlError) ) :
                                        $validation['AddFixedPriceItem'] = array(
                                                'error'                     => $this->ebaylib->core->_xmlError,
                                                'product'                   => $this->product_add['product'],
                                        );
                                        $this->_result['msg']['validation_error'] = 1;
                                endif;
                        }
                        elseif ( empty($this->product_add) && $common != 'verify' && $common != 'revise' ) {
                                $this->_result['msg']['empty_products']         = 1;
                                return $this->_result;
                        }
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( $common == 'verify' ) {
                                $this->_result['VerifyAddFixedPriceItem']   = $this->ebaylib->Bulk($this->site, $this->product, "VerifyAddFixedPriceItem", "product", $this->user_name);
                                if ( !empty($this->ebaylib->core->_xmlError) ) :
                                        $validation['AddFixedPriceItem'] = array(
                                                'error'                     => $this->ebaylib->core->_xmlError,
                                                'product'                   => $this->product_add['product'],
                                        );
                                        $this->_result['msg']['validation_error'] = 1;
                                endif;
                        }
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( $common == 'revise' && !empty($this->product_revise) && empty($this->user['verify_product']) ) {
                                $this->_result['ReviseFixedPriceItem']      = $this->ebaylib->Bulk($this->site, $this->product_revise, "ReviseFixedPriceItem", "product", $this->user_name);
                                if ( !empty($this->ebaylib->core->_xmlError) ) :
                                        $validation['ReviseFixedPriceItem'] = array(
                                                'error'                     => $this->ebaylib->core->_xmlError,
                                                'product'                   => $this->product_revise['product'],
                                        );
                                        $this->_result['msg']['validation_error'] = 1;
                                endif;
                        }
                        elseif ( $common == 'revise' && empty($this->product_revise)  ) {
                                $this->_result['msg']['empty_products']         = 1;
                                return $this->_result;
                        }
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if ( !empty($percent) ) : $this->percent->set_running_task(90); endif;
                        if ( !empty($validation) && count($validation) ) :
                                foreach ( $validation as $valid_key => $validate ) :
                                        foreach ($validate['product'] as $product_key => $product ) :
                                                $this->error_request_log++;
                                                unset($data_insert);
                                                $data_insert                = array(
                                                        "batch_id"          => $this->batch_id,
                                                        "id_product"        => isset($product_key) ? $product_key : 0,
                                                        "id_site"           => $this->site,
                                                        "id_shop"           => $this->id_shop,
                                                        "ebay_user"         => $this->ebayUser,
                                                        "sku_export"        => !empty($product['reference']) ? $product['reference'] : (!empty($product['sku']) ? $product['sku'] : ''),
                                                        "name_product"      => isset($product['name']) ? $product['name'] : 0,
                                                        "response"          => 'Error',
                                                        "type"              => 'Validation',
                                                        "message"           => $validation[$valid_key]['error'],  
                                                        "combination"       => 0, 
                                                        "date_add"          => date("Y-m-d H:i:s", strtotime("now")), 
                                                );
                                                $data['data_insert'][]      = $data_insert;
                                                $result_insert              .= $this->objectbiz->mappingTableOnly("ebay_statistics", $data_insert);
                                        endforeach;
                                endforeach;
                        endif;

                        !empty($result_insert) ? $this->objectbiz->transaction($result_insert, true, true) : '';
                        if ( !empty($percent) ) : $this->percent->set_running_task(100); endif;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_bulk() {
                        $this->dir_name                                     = USERDATA_PATH.$this->user_name."/ebay/product/$this->site";
                        $files                                              = array(
                                'AddFixedPriceItem'                         => $this->dir_name.'/AddFixedPriceItem.xml.gz', 
                                'RelistFixedPriceItem'                      => $this->dir_name.'/RelistFixedPriceItem.xml.gz', 
                                'ReviseFixedPriceItem'                      => $this->dir_name.'/ReviseFixedPriceItem.xml.gz',
                                'EndFixedPriceItem'                         => $this->dir_name.'/EndFixedPriceItem.xml.gz', 
                                'VerifyAddFixedPriceItem'                   => $this->dir_name.'/VerifyAddFixedPriceItem.xml.gz', 
                        );
                        
                        $this->_result                                      = array();
                        $this->_result['AddFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['AddFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['AddFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['AddFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['RelistFixedPriceItem']['createJob'] = 'No Used';
                        $this->_result['RelistFixedPriceItem']['jobID']     = 'No Used';
                        $this->_result['RelistFixedPriceItem']['upload']    = 'No Used';
                        $this->_result['RelistFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['createJob'] = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['jobID']     = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['upload']    = 'No Used';
                        $this->_result['ReviseFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['EndFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['EndFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['EndFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['EndFixedPriceItem']['startUpload']  = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['createJob']    = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['jobID']        = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['upload']       = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']['startUpload']  = 'No Used';
                        foreach ( $files as $type => $file ) :
                        if (file_exists($file)) {
                                if ( strtotime(date("Y-m-d", filemtime($file))) == strtotime(date("Y-m-d", time())) ) :
                                        $this->ebaylib->createUploadJob($this->site, $type);
                                        $this->response                         = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                        		$this->_bulk_error              		= 0;
                                        		$this->_result[$type]['createJob']      = $this->response->ack->__toString();
                                                $this->ebaylib->_jobID                  = $this->response->jobId->__toString();
                                                $this->ebaylib->_fileID                 = $this->response->fileReferenceId->__toString();
                                                $this->fileID                           = $this->response->fileReferenceId->__toString();
                                                $this->_result[$type]['jobID']          = $this->response->jobId->__toString();
                                                createUploadFile :
                                                $this->ebaylib->createUploadFile($this->site, $type, "product", $this->user_name);
                                                $this->response                         = $this->ebaylib->core->_parser;
                                                if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                                        $this->_bulk_error              = 0;
                                                        $this->_result[$type]['upload'] = $this->response->ack->__toString();
                                                        startUploadJob :
                                                        $this->ebaylib->startUploadJob($this->site);
                                                        $this->response                	= $this->ebaylib->core->_parser;
                                                        if ( !empty($this->response) && $this->response->ack->__toString() == 'Success' ) :
                                                                $this->_bulk_error      = 0;
                                                                $this->_result[$type]['startUpload'] = $this->response->ack->__toString();
                                                                $this->objectbiz->checkDB("ebay_job_task");
                                                                $this->ebaylib->getJobStatus($this->site);
                                                                $this->response                     = $this->ebaylib->core->_parser;
                                                                if ( !empty($this->response) &&
                                                                     !empty($this->response->jobProfile->jobId) ) :
                                                                        $data_insert                        = array(
                                                                                'jobId'                 	=> $this->response->jobProfile->jobId->__toString(),
                                                                                'fileReferenceId'           => $this->fileID,
                                                                                'batch_id'                  => $this->batch_id,
                                                                                'ebay_user'                 => $this->ebayUser,
                                                                                'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                                                'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                                                'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                                                'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                                                'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                                        );
                                                                        $data['data_insert'][]              = $data_insert;
                                                                        $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                                endif;
                                                        else :
                                                                if ( $this->_bulk_error < 3 ) :
                                                                        $this->_bulk_error++;
                                                                        goto startUploadJob;
                                                                else :
                                                                        $this->_result['msg']['bulk_startupload']     = 1;
                                                                        return $this->_result;
                                                                endif;
                                                        endif;
                                                else :
                                                        if ( $this->_bulk_error < 3 ) :
                                                                $this->_bulk_error++;
                                                     			goto createUploadFile;
                                                        else :
                                                                $this->_result['msg']['bulk_createfile']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                endif;
                                        else :
                                                if ( $this->_bulk_error < 3 ) :
                                                        $this->_bulk_error++;
                                                        $this->abortJobCheck();
                                                        $this->export_bulk();
                                                else :
                                                        $this->_result['msg']['bulk_createjob']     = 1;
                                                        return $this->_result;
                                                endif;
                                        endif;
                                endif;
                        }
                        endforeach;
                        return $this->_result;
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function export_download($percent = '', $date = "now") {
                        $this->objectbiz->checkDB("ebay_job_task");
                        $job_task                                           = $this->objectbiz->getJobTask($this->user_name, 'products', $this->ebayUser, 'now');
                        $this->_result                                      = array();
                        $this->_result['AddFixedPriceItem']                 = 'No Used';
                        $this->_result['RelistFixedPriceItem']              = 'No Used';
                        $this->_result['ReviseFixedPriceItem']              = 'No Used';
                        $this->_result['EndFixedPriceItem']                 = 'No Used';
                        $this->_result['VerifyAddFixedPriceItem']           = 'No Used';
                        if (!empty($job_task)) :
                        foreach ( $job_task as $task ) :
                                $this->ebaylib->_jobID                      = $task['jobId'];
                                while ( $task['jobId'] ) :
                                        $this->ebaylib->getJobStatus($this->site);
                                        $this->response                     = $this->ebaylib->core->_parser;
                                        if ( !empty($this->response) ) :
                                                $data_insert                        = array(
                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                        'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                        'batch_id'                  => $this->batch_id,
                                                        'ebay_user'                 => $this->ebayUser,
                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                );
                                                $data['data_insert'][]              = $data_insert;
                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                $percentComplete                    = $this->response->jobProfile->percentComplete->__toString();
                                                $jobStatus                          = $this->response->jobProfile->jobStatus->__toString();
                                        endif;
                                        if ( !empty($percent) ) :
                                                $percent->set_running_task($percentComplete);
                                        endif;
                                        if ( (!empty($percentComplete) && $percentComplete == '100.0') || (!empty($jobStatus) && in_array($jobStatus, array('Failed', 'Aborted')))) :
                                                $task['jobId']                      = 0;
                                                $data_insert                        = array(
                                                        'jobId'                     => $this->response->jobProfile->jobId->__toString(),
                                                        'fileReferenceId'           => $this->response->jobProfile->fileReferenceId->__toString(),
                                                        'batch_id'                  => $this->batch_id,
                                                        'ebay_user'                 => $this->ebayUser,
                                                        'jobType'                   => $this->response->jobProfile->jobType->__toString(),
                                                        'jobStatus'                 => $this->response->jobProfile->jobStatus->__toString(),
                                                        'percentComplete'           => $this->response->jobProfile->percentComplete->__toString(),
                                                        'creationTime'              => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->creationTime->__toString())),
                                                        'startTime'                 => date('Y-m-d H:i:s', strtotime($this->response->jobProfile->startTime->__toString())),
                                                );
                                                $result_insert                      = $this->objectbiz->mappingTable("ebay_job_task", $data_insert);
                                                $this->ebaylib->_fileID             = $this->response->jobProfile->fileReferenceId->__toString();
                                                startDownloadFile :
                                                $this->ebaylib->startDownloadFile($this->site, $task['jobType'], "product", $this->user_name);
                                                $this->response                     = $this->ebaylib->core->_parser;
                                                if ( !empty($this->response) && in_array($this->response->ack->__toString(), array('Success', 'Failure') )) :
                                                        $this->_bulk_error              = 0;
                                                        $this->_result[$task['jobType']] = $this->response->ack->__toString();
                                                        $this->ebaylib->getDownloadFile($this->site, $task['jobType'], "product", $this->user_name);
                                                        $this->objectbiz->checkDB("ebay_statistics");
                                                        $this->objectbiz->checkDB("ebay_statistics_log");
                                                        $this->objectbiz->checkDB("ebay_product_item_id");
                                                        $result_insert = '';
                                                        if ( !empty($this->ebaylib->core->_parser) ) : 
                                                                foreach ( $this->ebaylib->core->_parser as $parse ) :
                                                                        $result_insert .= $this->export_statistics($parse, $task['jobType']);
                                                                endforeach;
                                                                $logs = $this->export_statistics_sql();
                                                                $this->objectbiz->transaction($result_insert.$this->flag_sql.$logs, true);
                                                                if('EndFixedPriceItem' == $task['jobType']){
                                                                        $this->objectbiz->deleteColumn('ebay_synchronization_store', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                                                        $this->objectbiz->deleteColumn('ebay_synchronization_attribute', array('id_site' => $this->site, 'ebay_user' => $this->ebayUser));
                                                                }
                                                                
                                                                if ( !empty($this->user_email)) :
                                                                        $this->export_mail($this->_email, $this->user_email);
                                                                endif;
                                                        endif;
                                                else :
                                                        if ( $this->_bulk_error < 3 ) :
                                                                $this->_bulk_error++;
                                                                goto startDownloadFile;
                                    			else :
                                                                $this->_result['msg']['bulk_download']     = 1;
                                                                return $this->_result;
                                                        endif;
                                                endif;
                                                
                                                if(in_array($jobStatus, array('Failed', 'Aborted'))) :
                                                	$this->_result['msg']['job_failure'] = 1;
                                                endif;
                                        else :  sleep(5);
                                        endif;
                                endwhile;
                        endforeach;
                        endif;
                        return $this->_result;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function extrack_download($task = 'AddFixedPriceItem') {
                        $this->ebaylib->getDownloadFile($this->site, $task, "product", $this->user_name);
                        $result_insert = '';
                        if ( !empty($this->ebaylib->core->_parser) ) :
                                foreach ( $this->ebaylib->core->_parser as $parse ) :
                                        $result_insert .= $this->export_statistics($parse, $task);
                                endforeach;
                                $logs = $this->export_statistics_sql();
                                $this->objectbiz->transaction($result_insert.$this->flag_sql.$logs, true, true);
                                if ( !empty($this->user_email)) :
                                        $this->export_mail($this->_email, $this->user_email);
                                endif;
                        endif;
                }

//                public function export_product_array() {
//                	$page = 1;
//                        $product_count                  = round($this->objectbiz->checkCount("products_product", array('id_shop' => $this->id_shop)) / 200);
//                	$product['product'] = array();
//                	do {    
//                		unset($data);
//                		$data                   = $this->objectbiz->exportProductsArray($this->user_name, $this->id_shop, $this->mode['mode'], mb_substr($this->language, 0, 2), $page);
//                		$product['product']     = empty($product['product']) ? (empty($data) ? array() : $data['product']) : ($product['product'] + (empty($data) ? array() : $data['product']));
//                                $this->percent_count($page, $product_count);
//                		$page++;
//                	} while( !empty($data) && count($data) );
//                	$this->product = $product;
//                	$this->_result['shop'] = $this->id_shop;
//                }
                
                public function export_product_array() {
                                
                	$this->product =   $this->objectbiz->exportProductsArray($this->user_name, $this->id_shop, $this->mode['mode'], mb_substr($this->language, 0, 2));
                        
                        $this->percent_count(1, 1);
                	$this->_result['shop'] = $this->id_shop;
                }
                
                public function products_monitoring($username){
                	$result = array();
                	$result = $this->objectbiz->getProductLatestUploadSummary($username, $this->site, $this->id_shop);
                	return $result;
                }
                
		public function products_monitoring_user_report($id_site, $id_shop){
                	return  $this->objectbiz->getInventory($id_site, $id_shop);
                }

                public function products_monitoring_create_error_report(){
                	return  $this->objectbiz->getNumberOfErrorCreateProduct();
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        $this->user_name                = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                  = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                     = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->cond                     = empty($userdata[3]) ? false : $userdata[3];
                        $this->wizard                   = empty($userdata[4]) ? false : $userdata[4];
                        parent::__construct();
                        $this->dir_server               = DIR_SERVER;
                        $this->objectbiz                = new ObjectBiz(array($this->user_name));
                        $this->connect                  = new connect();
                        $this->eucurrency               = new Eucurrency();
                        $shop_default                   = $this->objectbiz->getDefaultShop($this->user_name);
                        if ( empty($shop_default) ) :
                                $this->_result['msg']['empty_products']         = 1;
                                return $this->_result;
                        endif;
                        $this->id_shop                  = (int)$shop_default['id_shop'];
                        $this->user_conf                = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->user                     = $this->set_configuration($this->connect->get_ebay_configuration($this->user_id, $this->site));
                        list($this->user_email)         = $this->connect->fetch($this->connect->get_user_mail($this->user_id));
                        list($this->first_name
                           , $this->last_name)          = $this->connect->fetch($this->connect->get_full_name($this->user_id));
                        $this->site                     = isset($this->user['site']) ? $this->user['site'] : 0;
                        $site_result                    = $this->connect->fetch($this->connect->get_ebay_site_by_id($this->site));
                        $this->site_name                = !empty($site_result) ? $site_result['name'] : '';
                        $this->id_lang                  = $this->objectbiz->getDefaultLanguages($this->user_name, $this->id_shop);
                        $this->language                 = $this->objectbiz->getDefaultISO($this->user_name, $this->id_shop);
                        $this->currency                 = $site_result['currency'];
                        $this->country                  = strtoupper($site_result['iso_code']);
                        $this->ebayUser                 = $this->user_conf['userID'];
                        $this->discount                 = !empty($this->user['discount']) ? $this->user['discount'] : 0;
                        $this->ebaylib                  = new ebaylib( (!empty($this->user_conf['appMode']) && $this->user_conf['appMode'] == 1) ? array(0) : array(1) );
                        $this->profile_rule             = empty($this->user['profile_rules']) ? 0 : $this->user['profile_rules'];
                        if ( empty($this->user_conf['token']) ) :
                                $this->_result['msg']['empty_token']    = 1;
                                return $this->_result;
                        endif;
                        $this->ebaylib->token           = $this->user_conf['token'];
                        $this->ebaylib->core->_template = !empty($this->user['template']) ? $this->user['template'] : 1;
                        $this->batch_id                 = uniqid();
                        $this->mode                     = unserialize(base64_decode($this->user_conf['feed_mode']));
                        $result_log                     = $this->objectbiz->checkBatchID($this->batch_id);
                        while( !empty($result_log) ) :
                                $this->batch_id         = uniqid();
                                $result_log             = $this->objectbiz->checkBatchID($this->batch_id);
                        endwhile;
                        $this->enabled_manual           = !empty($this->user['enabled_manual']) ? $this->user['enabled_manual'] : 0;
                        
                        $this->objectbiz->checkDB("ebay_tax");
                        $this->objectbiz->checkDB("ebay_mapping_category");
                        $this->objectbiz->checkDB("ebay_mapping_univers");
                        $this->objectbiz->checkDB("mapping_listing_duration");
                        $this->objectbiz->checkDB("ebay_mapping_templates");
                        $this->objectbiz->checkDB("mapping_features");
                        $this->objectbiz->checkDB("ebay_mapping_carrier_domestic");
                        $this->objectbiz->checkDB("ebay_mapping_carrier_international");
                        $this->objectbiz->checkDB("ebay_mapping_carrier_country_selected");
                        $this->objectbiz->checkDB("ebay_mapping_carrier_default");
                        $this->objectbiz->checkDB("ebay_mapping_attribute_selected");
                        $this->objectbiz->checkDB("ebay_mapping_attribute_value");
                        $this->objectbiz->checkDB("ebay_mapping_carrier_rules");
                        $this->objectbiz->checkDB("ebay_mapping_condition");
                        $this->objectbiz->checkDB("ebay_statistics_log");
                        $this->objectbiz->checkDB("ebay_statistics");
                        $this->objectbiz->checkDB("ebay_statistics_combination");
                        $this->objectbiz->checkDB("ebay_product_details");
                        $this->objectbiz->checkDB("ebay_product_item_id");
                        $this->objectbiz->checkDB("ebay_job_task");
                        $this->objectbiz->checkDB("ebay_profiles_group");
                        $this->objectbiz->checkDB("ebay_profiles");
                        $this->objectbiz->checkDB("ebay_profiles_details");
                        $this->genemap                  = $this->objectbiz->genarateMappingLastest($this->user_name, $this->site, $this->id_shop);
                        $this->dir_name                 = USERDATA_PATH.$this->user_name."/ebay/product/$this->site";
                }
        }
