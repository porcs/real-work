<?php   if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        echo header('Content-Type: text/html; charset=utf-8');
        if ( class_exists('ebaylib')   == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')   == FALSE ) require dirname(__FILE__).'/connect.php';
        
        class ebayfeatures extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name == 'EBAY_USER' )                 $result['userID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                        endwhile;
                        return $result;
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parse_features() {
                        $this->data_0                               = $this->connect->show_table('ebay_categories_features');
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->create_table('ebay_categories_features');
                        endif;
                        $this->response                             = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) ) :
                                $enabled                            = 'false';
                                if ( $this->response->Ack->__toString() == 'Success' && count($this->response->Category) ) :
                                        $enabled                    = $this->response->Category->VariationsEnabled->__toString();
                                endif;
                                $dataFeatures                       = array(
                                        'id_category'               => $this->id_category,
                                        'id_site'                   => $this->id_site,
                                        'is_enabled'                => ($enabled == 'true' || ($enabled == 'True')) ? '1' : '0',
                                        'date_add'                  => date('Y-m-d H:i:s', strtotime("now")),
                                );

                                $this->connect->replace_db('ebay_categories_features', $dataFeatures);
                        endif;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_features() {
//                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $query                              = $this->connect->get_categories_ebay_site($this->site);
                                while ( list($this->id_category, $this->id_site)    = $this->connect->fetch($query) ) :
                                        $this->ebaylib->GetCategoryFeatures($this->id_site, $this->id_category);
                                        $this->parse_features();
                                endwhile;
//                        endwhile;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parse_durations_features() {
                        $this->data_0                               = $this->connect->show_table('ebay_listing_durations');
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->create_table('ebay_listing_durations');
                                $this->connect->create_table('ebay_listing_durations_set');
                        endif;
                        $this->response                             = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) ) :
                                if ( $this->response->Ack->__toString() == 'Success' && count($this->response->Category) ) :
                                        unset($dataFeatures);
                                        foreach ( $this->response->Category->ListingDuration as $listingType ) :
                                                $dataFeatures                       = array(
                                                        'listing_type'              => !empty($listingType) ? $listingType->attributes()->__toString() : '',
                                                        'id_site'                   => $this->id_site,
                                                        'duration_set_id'           => !empty($listingType) ? $listingType->__toString() : '',
                                                );
                                                $this->connect->replace_db('ebay_listing_durations', $dataFeatures);
                                        endforeach;
                                        unset($dataFeatures);
                                        foreach ( $this->response->FeatureDefinitions->ListingDurations->ListingDuration as $listingDuration ) :
                                                foreach ( $listingDuration as $duration ) :
                                                        $dataFeatures                       = array(
                                                                'duration_set_id'           => !empty($listingDuration) ? $listingDuration->attributes()->__toString() : '',
                                                                'duration'                  => !empty($duration) ? $duration->__toString() : '',
                                                        );
                                                      $this->connect->replace_db('ebay_listing_durations_set', $dataFeatures);
                                                endforeach;
                                        endforeach;
                                endif;
                        endif;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_features_listing_durations() {
                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $query                              = $this->connect->get_categories_ebay_by_site($this->id_site);
                                while ( list($this->id_category)    = $this->connect->fetch($query) ) :
                                        $this->ebaylib->GetCategoryFeatures($this->id_site, $this->id_category, 'ListingDurations');
                                        $this->parse_durations_features();
//                                        echo "<pre>", print_r($this->ebaylib->core->_parser, true), "</pre>";
                                        break;
                                endwhile;
                        endwhile;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parse_payment_methods_features() {
                        $this->data_0                               = $this->connect->show_table('ebay_payment_method');
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->create_table('ebay_payment_method');
                        endif;
                        $this->response                             = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) ) :
                                if ( $this->response->Ack->__toString() == 'Success' && count($this->response->SiteDefaults) ) :
                                        unset($dataFeatures);
                                        foreach ( $this->response->SiteDefaults->PaymentMethod as $paymentMethod ) :
                                                $dataFeatures                       = array(
                                                        'payment_method'            => !empty($paymentMethod) ? $paymentMethod->__toString() : '',
                                                        'id_site'                   => $this->id_site,
                                                );
                                                $this->connect->replace_db('ebay_payment_method', $dataFeatures);
                                        endforeach;
                                endif;
                        endif;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_features_payment_methods() {
                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $query                              = $this->connect->get_categories_ebay_by_site($this->id_site);
                                while ( list($this->id_category)    = $this->connect->fetch($query) ) :
                                        $this->ebaylib->GetCategoryFeatures($this->id_site, $this->id_category, 'PaymentMethods');
                                        $this->parse_payment_methods_features();
                                        break;
                                endwhile;
                        endwhile;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parse_condition_values_features($id_site = 0) {
                        $this->data_0                               = $this->connect->show_table('ebay_condition_values');
                        if ( !$this->connect->count_rows($this->data_0) ) :
                                $this->connect->create_table('ebay_condition_values');
                        endif;
                        $this->response                             = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) ) :
                                if ( ($this->response->Ack->__toString() == 'Success' || $this->response->Ack->__toString() == 'Warning') && !empty($this->response->Category->ConditionValues) && count($this->response->Category) ) :
                                        unset($dataFeatures);
                                        foreach ( $this->response->Category->ConditionValues->Condition as $conditionValues ) :
                                                $dataFeatures                       = array(
                                                        'id_category'               => $this->id_category,
                                                        'id_site'                   => isset($id_site) ? $id_site : 0,
                                                        'condition_value'           => !empty($conditionValues) ? $conditionValues->ID->__toString() : '',
                                                        'condition_name'            => !empty($conditionValues) ? $conditionValues->DisplayName->__toString() : '',
                                                        'is_enabled'                => '1',
                                                        'date_add'                  => date('Y-m-d H:i:s', strtotime("now")),
                                                );
                                                $this->connect->replace_db('ebay_condition_values', $dataFeatures);
                                        endforeach;
                                endif;
                        endif;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_features_condition_values() {
                        while ( list($this->id_site)                = $this->connect->fetch($this->site_configuration) ) :
                                $query                              = $this->connect->get_categories_ebay_by_site($this->id_site);
                                while ( list($this->id_category)    = $this->connect->fetch($query) ) :
                                        $this->ebaylib->GetCategoryFeatures($this->id_site, $this->id_category, 'ConditionValues');
                                        $this->parse_condition_values_features($this->id_site);
                                endwhile;
                        endwhile;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_features_site_condition_values() {
                        $query                              = $this->connect->get_categories_ebay_by_site($this->site);
                        while ( list($this->id_category)    = $this->connect->fetch($query) ) :
                                $this->ebaylib->GetCategoryFeatures($this->site, $this->id_category, 'ConditionValues');
                                $this->parse_condition_values_features($this->site);
                        endwhile;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function get_current_features($id = '', $categoryId = '') {
                        if ( !isset($id) || (empty($categoryId) && !is_numeric($categoryId)) ) :
                                return;
                        endif;
                        $this->ebaylib->GetCategoryFeatures($id, $categoryId, 'ListingDurations');            
                        $this->response                     = $this->ebaylib->core->_parser;
                        $enabled                            = 'false';
                        if ( $this->response->Ack->__toString() == 'Success' && count($this->response->Category) ) :
                                $enabled                    = $this->response->Category->VariationsEnabled->__toString();
                        endif;
                        $dataFeatures                       = array(
                                'id_category'               => $categoryId,
                                'id_site'                   => $id,
                                'is_enabled'                => ($enabled == 'true') ? '1' : '0',
                                'date_add'                  => date('Y-m-d H:i:s', strtotime("now")),
                        );
                        return $this->ebaylib->core->_parser;
                }
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->site                             = empty($userdata) ? exit('No direct script access allowed') : $userdata[2];
                        $this->connect                          = new connect();
                        $this->user_configuration               = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->ebayUser                         = $this->user_configuration['userID'];
                        $this->ebaylib                          = new ebaylib( ($this->user_configuration['appMode'] == 1) ? array(0) : array(1) );
                        $this->ebaylib->token                   = $this->user_configuration['token'];
                        $this->site_configuration               = $this->connect->get_ebay_site();
                        $this->ebaylib->core->_template         = 1;
                }
        }
