<?php
    class key {
            public $production;
            public $_key                        = array();
            public $mode                        = array(
                    "production"                => array(
//                            "devID"             => 'dd01a8e8-2bd5-41bc-9299-105936dbf8c8',
//                            "appID"             => 'Melon0135-7009-47f1-ae31-368cd4d8748',
//                            "certID"            => '64a57206-683a-48ed-96ed-1100fddee7fd',
//                            "runame"            => 'Melon-Melon0135-7009--ezzhdrc',
//                            "devID"             => '7fe21cb6-888a-4d0b-a4a9-828aff1142b0',
//                            "appID"             => 'commonse-ecf9-413d-b667-95ddf72f7bfa',
//                            "certID"            => '7eb8dcbd-d869-4228-a65d-c5fb75a7577f',
//                            "runame"            => 'common_service-commonse-ecf9-4-mrfozqjx',
                            "devID"             => '30898859-2c3c-4563-adc6-b90b4362cf53',
                            "appID"             => 'Feedbizd4-3d70-4f9d-a151-f6162edb154',
                            "certID"            => 'adcad2a3-fae2-49d2-8d1d-f63d6a2f876c',
                            "runame"            => 'Feedbiz-Feedbizd4-3d70--tfvoj',
                            "token"             => '',
                            "method"            => '',
                            "serverUrl"         => 'https://api.ebay.com/ws/api.dll',
                            "BulkendPoint"      => 'https://webservices.ebay.com/BulkDataExchangeService',
                            "FileEndPoint"      => 'https://storage.ebay.com/FileTransferService',
                            "compatability"     => 903,
                    ),
                    "sanbox"                    => array(
                            "devID"             => '30898859-2c3c-4563-adc6-b90b4362cf53',
                            "appID"             => 'Feedbiz1c-e6b8-4b72-bd3e-2d958e2c5a9',
                            "certID"            => '446a2772-d299-4970-a5f2-60f5edda71ad',
                            "runame"            => 'Feedbiz-Feedbiz1c-e6b8--keegmcu',
                            "token"             => '',
                            "method"            => '',
                            "serverUrl"         => 'https://api.sandbox.ebay.com/ws/api.dll',
                            "BulkendPoint"      => 'https://webservices.sandbox.ebay.com/BulkDataExchangeService',
                            "FileEndPoint"      => 'https://storage.sandbox.ebay.com/FileTransferService',
                            "compatability"     => 903,
                    ),
            );

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            public function __construct( $productions = array() ) {
                    foreach ( $productions as $production) :
                            if ($production) :
                                    $this->_key = $this->mode['production'];
                            else :    
                                    $this->_key = $this->mode['sanbox'];
                            endif;
                    endforeach;   
            }
    }
