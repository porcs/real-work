<?xml version="1.0" encoding="utf-8"?>
<GetUserPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <ShowSellerPaymentPreferences>1</ShowSellerPaymentPreferences>
</GetUserPreferencesRequest>