<?xml version="1.0" encoding="utf-8"?>
<GetCategorySpecificsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        {if isset($ErrorLanguage)}
                <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
        {/if}
	<WarningLevel><![CDATA[High]]></WarningLevel>
        {if isset($CategoryID)}
                {foreach from = $CategoryID['_Repeat'] item = CID}
                        <CategoryID><![CDATA[{$CID}]]></CategoryID>
                {/foreach}
        {/if}
        {if isset($CategorySpecific)}
                <CategorySpecific>
                        <CategoryID><![CDATA[{$CategorySpecific['_Repeat'][0]}]]></CategoryID>
                </CategorySpecific>
        {/if}
        {if isset($CategorySpecificsFileInfo)}
                <CategorySpecificsFileInfo><![CDATA[{$CategorySpecificsFileInfo}]]></CategorySpecificsFileInfo>
        {/if}
	<RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
	</RequesterCredentials>
</GetCategorySpecificsRequest>