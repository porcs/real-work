<?xml version="1.0" encoding="utf-8"?>
<GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    {if isset($RuName)}
        <RuName><![CDATA[{$RuName}]]></RuName>
    {/if}
    {if isset($lang) && strtolower($lang) == 'fr' }
        <ErrorLanguage>fr_FR</ErrorLanguage>
    {elseif isset($lang) && strtolower($lang) == 'en' }
        <ErrorLanguage>en_US</ErrorLanguage>
    {/if}
</GetSessionIDRequest>