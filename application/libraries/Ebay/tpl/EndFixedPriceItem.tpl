<?xml version="1.0" encoding="utf-8"?>
<EndFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
	</RequesterCredentials>
        <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
        {if isset($EndingReason) }
        <EndingReason><![CDATA[{$EndingReason}]]></EndingReason>
        {/if}
        {if isset($ItemID) }
        <ItemID><![CDATA[{$ItemID}]]></ItemID>
        {/if}
        {if isset($SKU) }
        <SKU><![CDATA[{$SKU}]]></SKU>
        {/if}
</EndFixedPriceItemRequest>