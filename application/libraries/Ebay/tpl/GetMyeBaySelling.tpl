<?xml version="1.0" encoding="utf-8"?>
<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    {if isset($ActiveList)}
    <ActiveList>
    <Sort><![CDATA[TimeLeft]]></Sort>
    <Pagination>
      <EntriesPerPage>200</EntriesPerPage>
      <PageNumber>{$PageNumber}</PageNumber>
    </Pagination>
    </ActiveList>
    {/if}
    {if isset($DeletedFromSoldList)}
    <DeletedFromSoldList>
    <Sort><![CDATA[CurrentPrice]]></Sort>
    {if isset($DeletedFromSoldList['OrderStatusFilter'])}
    <OrderStatusFilter>{$DeletedFromSoldList['OrderStatusFilter']}</OrderStatusFilter >
    {/if}
    {if isset($DeletedFromSoldList['DurationInDays'])}
    <DurationInDays>{$DeletedFromSoldList['DurationInDays']}</DurationInDays>
    {/if}
    <Pagination>
      <EntriesPerPage>200</EntriesPerPage>
      <PageNumber>{$PageNumber}</PageNumber>
    </Pagination>
    </DeletedFromSoldList>
    {/if}
    {if isset($SoldList)}
    <SoldList>
    <Sort><![CDATA[CurrentPrice]]></Sort>
    {if isset($SoldList['OrderStatusFilter'])}
    <OrderStatusFilter>{$SoldList['OrderStatusFilter']}</OrderStatusFilter >
    {/if}
    {if isset($SoldList['DurationInDays'])}
    <DurationInDays>{$SoldList['DurationInDays']}</DurationInDays>
    {/if}
    <Pagination>
      <EntriesPerPage>200</EntriesPerPage>
      <PageNumber>{$PageNumber}</PageNumber>
    </Pagination>
    </SoldList>
    {/if}
    {if isset($OutputSelector)}
    <OutputSelector><![CDATA[{$OutputSelector}]]></OutputSelector>
    {/if}
</GetMyeBaySellingRequest>