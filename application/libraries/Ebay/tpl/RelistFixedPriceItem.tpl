<?xml version="1.0" encoding="utf-8"?>
<RelistFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        <RequesterCredentials>
		<eBayAuthToken>{$token}</eBayAuthToken>
	</RequesterCredentials>
        <ErrorLanguage>{$ErrorLanguage}</ErrorLanguage>
        <Version>{$Version}</Version>
        <Item>
                <SKU><![CDATA[{$Item['SKU']}]]></SKU>
                <Title><![CDATA[{$Item['Title']}]]></Title>
                <Description><![CDATA[{$Item['Description']}]]></Description>
                <PrimaryCategory>
                        <CategoryID><![CDATA[{$Item['PrimaryCategory']['CategoryID']}]]></CategoryID>
                </PrimaryCategory>
                <ConditionID>{$Item['ConditionID']}</ConditionID>
                <CategoryMappingAllowed>{$Item['CategoryMappingAllowed']}</CategoryMappingAllowed>
                <DispatchTimeMax><![CDATA[{$Item['DispatchTimeMax']}]]></DispatchTimeMax>
                <ListingDuration><![CDATA[{$Item['ListingDuration']}]]></ListingDuration>
                <Country><![CDATA[{$Item['Country']}]]></Country>
                <Currency><![CDATA[{$Item['Currency']}]]></Currency>
                <Location><![CDATA[{$Item['Location']}]]></Location>
                {if isset($Item['Quantity']) }
                <Quantity>{$Item['Quantity']}</Quantity>
                {/if}
                {if !empty($Item['StartPrice']) }
                <StartPrice>{$Item['StartPrice']}</StartPrice>
                {/if}
                {if !empty($Item['PictureDetails']) }
                <PictureDetails>
                        <GalleryType><![CDATA[Gallery]]></GalleryType>
                        {if !empty($Item['PictureDetails']['PictureURL'])}
                                {if is_array($Item['PictureDetails']['PictureURL'])}
                                {foreach from = $Item['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                <GalleryURL><![CDATA[{$PictureURL}]]></GalleryURL>
                                {/foreach}
                                {/if}
                        {else}  
                                <GalleryURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></GalleryURL>
                        {/if}
                        {if !empty($Item['PictureDetails']['PictureURL'])}
                                {if is_array($Item['PictureDetails']['PictureURL'])}
                                {foreach from = $Item['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                {/foreach}
                                {/if}
                        {else}  
                                <PictureURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></PictureURL>
                        {/if}
                </PictureDetails>
                {/if}
                <PostalCode><![CDATA[{$Item['PostalCode']}]]></PostalCode>
                {foreach from = $Item['PaymentMethods']['_Repeat'] item = Payment }
                <PaymentMethods><![CDATA[{$Payment}]]></PaymentMethods>
                {/foreach}
                <PayPalEmailAddress><![CDATA[{$Item['PayPalEmailAddress']}]]></PayPalEmailAddress>
                <InventoryTrackingMethod><![CDATA[{$Item['InventoryTrackingMethod']}]]></InventoryTrackingMethod>
                <ReturnPolicy>
                        <ReturnsAcceptedOption><![CDATA[{$Item['ReturnPolicy']['ReturnsAcceptedOption']}]]></ReturnsAcceptedOption>
                        {if $Item['ReturnPolicy']['ReturnsAcceptedOption'] == 'ReturnsAccepted'}
                        <ReturnsWithinOption><![CDATA[{$Item['ReturnPolicy']['ReturnsWithinOption']}]]></ReturnsWithinOption>
                        <ShippingCostPaidByOption><![CDATA[{$Item['ReturnPolicy']['ShippingCostPaidByOption']}]]></ShippingCostPaidByOption>
                        <Description><![CDATA[{$Item['ReturnPolicy']['Description']}]]></Description>
                        {/if}
                </ReturnPolicy>
                <ShippingDetails>
                        {if !empty($Item['ShippingDetails']['ShippingServiceOptions']) }
                        {foreach from = $Item['ShippingDetails']['ShippingServiceOptions']['_Repeat'] item = ShippingServiceOptions }
                        <ShippingServiceOptions>
                                <ShippingService><![CDATA[{$ShippingServiceOptions['ShippingService']}]]></ShippingService>
                                <ShippingServiceAdditionalCost>{$ShippingServiceOptions['ShippingServiceAdditionalCost']}</ShippingServiceAdditionalCost>
                                <ShippingServiceCost>{$ShippingServiceOptions['ShippingServiceCost']}</ShippingServiceCost>
                                <ShippingServicePriority>{$ShippingServiceOptions['ShippingServicePriority']}</ShippingServicePriority>
                                {if !empty($ShippingServiceOptions['FreeShipping']) }
                                <FreeShipping>{$ShippingServiceOptions['FreeShipping']}</FreeShipping>
                                {/if}
                        </ShippingServiceOptions>
                        {/foreach}
                        {/if}
                        {if !empty($Item['ShippingDetails']['InternationalShippingServiceOption']) }
                        {foreach from = $Item['ShippingDetails']['InternationalShippingServiceOption']['_Repeat'] item = InternationalShippingServiceOption }
                        <InternationalShippingServiceOption>
                                <ShippingService><![CDATA[{$InternationalShippingServiceOption['ShippingService']}]]></ShippingService>
                                <ShippingServiceCost>{$InternationalShippingServiceOption['ShippingServiceCost']}</ShippingServiceCost>
                                <ShippingServicePriority>{$InternationalShippingServiceOption['ShippingServicePriority']}</ShippingServicePriority>
                                {if !empty($InternationalShippingServiceOption['ShipToLocation']) }
                                {foreach from = $InternationalShippingServiceOption['ShipToLocation']['_Repeat'] item = ShipToLocation }
                                <ShipToLocation><![CDATA[{$ShipToLocation}]]></ShipToLocation>
                                {/foreach}
                                {/if}
                        </InternationalShippingServiceOption>
                        {/foreach}
                        {/if}
                </ShippingDetails>
                {if !empty($Item['Variations']) && count($Item['Variations']) }
                <Variations>
                        <VariationSpecificsSet>
                                {if !empty($Item['Variations']['VariationSpecificsSet']['NameValueList']) }
                                {foreach from = $Item['Variations']['VariationSpecificsSet']['NameValueList']['_Repeat'] item = NameValueList }
                                <NameValueList>
                                        <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                        {foreach from = $NameValueList['Value']['_Repeat'] item = Value }
                                        <Value><![CDATA[{$Value}]]></Value>
                                        {/foreach}
                                </NameValueList>
                                {/foreach}
                                {/if}
                        </VariationSpecificsSet>
                        {if !empty($Item['Variations']['Variation']) }
                        {foreach from = $Item['Variations']['Variation']['_Repeat'] item = Variation }
                        <Variation>
                                <SKU><![CDATA[{$Variation['SKU']}]]></SKU>
                                <StartPrice>{$Variation['StartPrice']}</StartPrice>
                                <Quantity>{$Variation['Quantity']}</Quantity>
                                <VariationSpecifics>
                                        {if !empty($Variation['VariationSpecifics']['NameValueList']) }
                                        {foreach from = $Variation['VariationSpecifics']['NameValueList']['_Repeat'] item = NameValueList }
                                        <NameValueList>
                                                <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                                <Value><![CDATA[{$NameValueList['Value']}]]></Value>
                                        </NameValueList>
                                        {/foreach}
                                        {/if}
                                </VariationSpecifics>
                        </Variation>
                        {/foreach}
                        {/if}
                        {if !empty($Item['Variations']['Pictures']) }
                        <Pictures>
                                {if !empty($Item['Variations']['Pictures']['VariationSpecificName']) }
                                <VariationSpecificName><![CDATA[{$Item['Variations']['Pictures']['VariationSpecificName']}]]></VariationSpecificName>
                                {foreach from = $Item['Variations']['Pictures']['VariationSpecificPictureSet']['_Repeat'] item = VariationSpecificPictureSet }
                                <VariationSpecificPictureSet>
                                        <VariationSpecificValue><![CDATA[{$VariationSpecificPictureSet['VariationSpecificValue']}]]></VariationSpecificValue>
                                        {foreach from = $VariationSpecificPictureSet['PictureURL']['_Repeat'] item = PictureURL }
                                        <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                        {/foreach}
                                </VariationSpecificPictureSet>
                                {/foreach}
                                {/if}
                        </Pictures>
                        {/if}
                </Variations>
                {/if}
        </Item>
</RelistFixedPriceItemRequest>