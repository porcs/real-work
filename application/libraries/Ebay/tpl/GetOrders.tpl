<?xml version="1.0" encoding="utf-8"?>
<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
            <eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    {if !empty($CreateTimeFrom)}
    <CreateTimeFrom><![CDATA[{$CreateTimeFrom}]]></CreateTimeFrom>
    {/if}
    {if !empty($CreateTimeTo)}
    <CreateTimeTo><![CDATA[{$CreateTimeTo}]]></CreateTimeTo>
    {/if}
    {if !empty($ModTimeFrom)}
    <ModTimeFrom><![CDATA[{$ModTimeFrom}]]></ModTimeFrom>
    {/if}
    {if !empty($ModTimeTo)}
    <ModTimeTo><![CDATA[{$ModTimeTo}]]></ModTimeTo>
    {/if}
    {if !empty($OrderRole)}
    <OrderRole><![CDATA[{$OrderRole}]]></OrderRole>
    {/if}
    {if !empty($OrderStatus)}
    <OrderStatus><![CDATA[{$OrderStatus}]]></OrderStatus>
    {/if}
    <Pagination>
      <EntriesPerPage>100</EntriesPerPage>
      <PageNumber>{$PageNumber}</PageNumber>
    </Pagination>
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    <DetailLevel><![CDATA[{$DetailLevel}]]></DetailLevel>
    <Version>879</Version>
</GetOrdersRequest>