<?xml version="1.0" encoding="utf-8"?>
<ReviseCheckoutStatusRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
            <eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    {if !empty($OrderID) && count($OrderID) }
    <OrderID>{$OrderID}</OrderID>
    {/if}
    <CheckoutStatus>Complete</CheckoutStatus>
    {if !empty($PaymentMethodUsed) && count($PaymentMethodUsed) }
    <PaymentMethodUsed>{$PaymentMethodUsed}</PaymentMethodUsed>
    {/if}
    {if !empty($BuyerID) && count($BuyerID) }
    <BuyerID>{$BuyerID}</BuyerID>
    {/if}
    {if !empty($ShippingAddress) && count($ShippingAddress) }
    <ShippingAddress>
        {foreach from = $ShippingAddress key = key item = address }
        {if !empty($key) && count($key) && !empty($address) && count($address) && ($key != "AddressID" && $key != "AddressOwner" && $key != "AddressUsage") }
        <{$key}>{$address}</{$key}>
        {/if}
        {/foreach}
    </ShippingAddress>
    {/if}
    {if !empty($ErrorLanguage) && count($ErrorLanguage) }
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    {/if}
</ReviseCheckoutStatusRequest>