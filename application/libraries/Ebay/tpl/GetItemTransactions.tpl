<?xml version="1.0" encoding="utf-8"?>
<GetItemTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
            <eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    {if !empty($ItemID) && count($ItemID) }
    <ItemID>{$ItemID}</ItemID>
    {/if}
    {if !empty($TransactionID) && count($TransactionID) }
    <TransactionID>{$TransactionID}</TransactionID>
    {/if}
    {if !empty($DetailLevel) && count($DetailLevel) }
    <DetailLevel><![CDATA[{$DetailLevel}]]></DetailLevel>
    {/if}
    {if !empty($ErrorLanguage) && count($ErrorLanguage) }
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    {/if}
</GetItemTransactionsRequest>