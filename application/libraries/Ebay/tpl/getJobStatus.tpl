<?xml version="1.0" encoding="utf-8"?>
<getJobStatusRequest xmlns:sct="http://www.ebay.com/soaframework/common/types" xmlns="http://www.ebay.com/marketplace/services">
    {if !empty($jobId)}<jobId><![CDATA[{$jobId}]]></jobId>{/if}
</getJobStatusRequest>