<?xml version="1.0" encoding="utf-8"?>
<GetStoreRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <CategoryStructureOnly>1</CategoryStructureOnly>
</GetStoreRequest>