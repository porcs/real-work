<?xml version="1.0" encoding="utf-8"?>
<GeteBayOfficialTimeRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
</GeteBayOfficialTimeRequest>