<?xml version="1.0" encoding="utf-8"?>
<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        {if isset($Item)}
                {if isset($Item['Title'])}
                {assign var="Title" value=$Item['Title']}
                {/if}
                {if isset($Item['Description'])}
                {assign var="Description" value=$Item['Description']}
                {/if}
                {if isset($Item['PrimaryCategory']['CategoryID'])}
                {assign var="CategoryID" value=$Item['PrimaryCategory']['CategoryID']}
                {/if}
                {if isset($Item['SKU'])}
                {assign var="SKU" value=$Item['SKU']}
                {/if}
                {if isset($Item['DispatchTimeMax'])}
                {assign var="DispatchTimeMax" value=$Item['DispatchTimeMax']}
                {/if}
                {if isset($Item['ListingDuration'])}
                {assign var="ListingDuration" value=$Item['ListingDuration']}
                {/if}
                {if isset($Item['Country'])}
                {assign var="Country" value=$Item['Country']}
                {/if}
                {if !empty($Item['Location'])}
                {assign var="Location" value=$Item['Location']}
                {/if}
                {if isset($Item['StartPrice'])}
                {assign var="StartPrice" value=$Item['StartPrice']}
                {else}
                {assign var="StartPrice" value="*"}
                {/if}
                {if isset($Item['PostalCode'])}
                {assign var="PostalCode" value=$Item['PostalCode']}
                {/if}
                {if isset($Item['Currency'])}
                {assign var="Currency" value=$Item['Currency']}
                {/if}
                {if isset($Item['PayPalEmailAddress'])}
                {assign var="PayPalEmailAddress" value=$Item['PayPalEmailAddress']}
                {/if}
                {if isset($Item['Manufacturer'])}
                {assign var="Manufacturer" value=$Item['Manufacturer']}
                {/if}
                {if !empty($Item['PictureDetails']['PictureURL'])}
                        {if is_array($Item['PictureDetails']['PictureURL'])}
                        {foreach from = $Item['PictureDetails']['PictureURL']['_Repeat'] key = PictureKey item = PictureURL }
                        {assign var="Image{$PictureKey}" value=$PictureURL}
                        {assign var="HasImage{$PictureKey}" value=$Item['PictureDetails']['PictureURL']['_HasPic'][$PictureKey]}
                        {/foreach}
                        {/if}
                {/if}
                {if empty($Image1)}
                    {assign var="Image1" value=''}
                {/if}
                {if empty($Image2)}
                    {assign var="Image2" value=''}
                {/if}
                {if empty($Image3)}
                    {assign var="Image3" value=''}
                {/if}
                {assign var="vMinPrice" value = 0}
                {assign var="vMaxPrice" value = 0}
                {if !empty($Item['Variations']['Variation']) }
                    {foreach from = $Item['Variations']['Variation']['_Repeat'] item = Variation }
                        {if empty($vMinPrice) && empty($vMaxPrice) && isset($Variation['StartPrice']) } 
                            {assign var="vMinPrice" value = $Variation['StartPrice']}
                            {assign var="vMaxPrice" value = $Variation['StartPrice']}
                        {elseif isset($vMaxPrice) && isset($Variation['StartPrice']) && $Variation['StartPrice'] > $vMaxPrice}
                            {assign var="vMaxPrice" value = $Variation['StartPrice']}
                        {elseif isset($vMinPrice) && isset($Variation['StartPrice']) && $Variation['StartPrice'] < $vMinPrice}
                            {assign var="vMinPrice" value = $Variation['StartPrice']}
                        {/if}
                    {/foreach}
                    {if isset($vMinPrice) && isset($vMaxPrice) && ($vMinPrice == $vMaxPrice)} 
                            {assign var="StartPrice" value = $vMinPrice}
                    {elseif isset($vMinPrice) && isset($vMaxPrice) && ($vMinPrice != $vMaxPrice)}
                            {assign var="StartPrice" value = $vMinPrice|cat:'" - "'|cat:$vMaxPrice}
                    {/if}
                {/if}
        {/if}
        <RequesterCredentials>
		<eBayAuthToken>{$token}</eBayAuthToken>
	</RequesterCredentials>
        <ErrorLanguage>{$ErrorLanguage}</ErrorLanguage>
        <Version>{$Version}</Version>
        {if !empty($Item['Variations']) && count($Item['Variations']) > 0 }
        <DeletedField><![CDATA[Item.SKU]]></DeletedField>
        {/if}
        {if isset($Item)}
        <Item>
                {if empty($Item['Variations']) || count($Item['Variations']) == 0 }
                        <SKU><![CDATA[{$Item['SKU']}]]></SKU>
                {/if}
                {if !empty($Item['AutoPay']) }
                <AutoPay>{$Item['AutoPay']}</AutoPay>
                {/if}
                {if isset($Item['Title']) }
                <Title><![CDATA[{$Item['Title']}]]></Title>
                {/if}
                {if isset($Item['Description']) }
                <Description>
                    {if isset($Item['template_description']) }
                        <![CDATA[{include file=$Item['template_description']}]]> 
                    {else}
                        <![CDATA[{$Item['Description']}]]>
                    {/if}
                </Description>
                {/if}
                {if isset($Item['PrimaryCategory']) }
                <PrimaryCategory>
                        <CategoryID><![CDATA[{$Item['PrimaryCategory']['CategoryID']}]]></CategoryID>
                </PrimaryCategory>
                {/if}
                {if isset($Item['SecondaryCategory']) }
                <SecondaryCategory>
                        <CategoryID><![CDATA[{$Item['SecondaryCategory']['CategoryID']}]]></CategoryID>
                </SecondaryCategory>
                {/if}
                {if isset($Item['Storefront']) }
                <Storefront>
                    {if isset($Item['Storefront']['StoreCategory2ID']) }
                    <StoreCategory2ID><![CDATA[{$Item['Storefront']['StoreCategory2ID']}]]></StoreCategory2ID>
                    {/if}
                    {if isset($Item['Storefront']['StoreCategory2Name']) }
                    <StoreCategory2Name><![CDATA[{$Item['Storefront']['StoreCategory2Name']}]]></StoreCategory2Name>
                    {/if}
                    {if isset($Item['Storefront']['StoreCategoryID']) }
                    <StoreCategoryID><![CDATA[{$Item['Storefront']['StoreCategoryID']}]]></StoreCategoryID>
                    {/if}
                    {if isset($Item['Storefront']['StoreCategoryName']) }
                    <StoreCategoryName><![CDATA[{$Item['Storefront']['StoreCategoryName']}]]></StoreCategoryName>
                    {/if}
                </Storefront>
                {/if}
                {if isset($Item['ConditionID']) }
                <ConditionID>{$Item['ConditionID']}</ConditionID>
                {/if}
                {if isset($Item['ConditionDescription']) && isset($Item['ConditionID']) && $Item['ConditionID'] != '1000'  }
                <ConditionDescription>{$Item['ConditionDescription']}</ConditionDescription>
                {/if}
                {if isset($Item['CategoryMappingAllowed']) }
                <CategoryMappingAllowed>{$Item['CategoryMappingAllowed']}</CategoryMappingAllowed>
                {/if}
                {if isset($Item['DispatchTimeMax']) }
                <DispatchTimeMax><![CDATA[{$Item['DispatchTimeMax']}]]></DispatchTimeMax>
                {/if}
                {if isset($Item['ListingDuration']) }
                <ListingDuration><![CDATA[{$Item['ListingDuration']}]]></ListingDuration>
                {/if}
                {if isset($Item['HitCounter']) }
                <HitCounter><![CDATA[{$Item['HitCounter']}]]></HitCounter>
                {/if}
                {if isset($Item['Country']) }
                <Country><![CDATA[{$Item['Country']}]]></Country>
                {/if}
                {if isset($Item['Currency']) }
                <Currency><![CDATA[{$Item['Currency']}]]></Currency>
                {/if}
                {if !empty($Item['Location']) }
                <Location><![CDATA[{$Item['Location']}]]></Location>
                {/if}
                {if isset($Item['OutOfStockControl']) }
                <OutOfStockControl>{$Item['OutOfStockControl']}</OutOfStockControl>
                {/if}
                {if isset($Item['Quantity']) && (isset($Item['OutOfStockControl']) || $Item['Quantity'] > 0 ) }
                <Quantity>{$Item['Quantity']}</Quantity>
                {/if}
                {if isset($Item['ItemID']) }
                <ItemID>{$Item['ItemID']}</ItemID>
                {/if}
                {if isset($Item['StartPrice']) && (isset($Item['OutOfStockControl']) || $Item['StartPrice'] > 0 ) }
                <StartPrice>{$Item['StartPrice']}</StartPrice>
                {/if}
                {if !empty($Item['PictureDetails']) }
                <PictureDetails>
                        <GalleryType><![CDATA[Gallery]]></GalleryType>
                        {if !empty($Item['PictureDetails']['PictureURL'])}
                                {if is_array($Item['PictureDetails']['PictureURL'])}
                                {foreach from = $Item['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                {if $PictureURL != 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}
                                <GalleryURL><![CDATA[{$PictureURL}]]></GalleryURL>
                                {/if}
                                {/foreach}
                                {/if}
                        {else}  
                                <GalleryURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></GalleryURL>
                        {/if}
                        {if !empty($Item['PictureDetails']['PictureURL'])}
                                {if is_array($Item['PictureDetails']['PictureURL'])}
                                {foreach from = $Item['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                {if $PictureURL != 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}
                                <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                {/if}
                                {/foreach}
                                {/if}
                        {else}  
                                <PictureURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></PictureURL>
                        {/if}
                </PictureDetails>
                {/if}
                {if !empty($Item['PostalCode']) }
                <PostalCode><![CDATA[{$Item['PostalCode']}]]></PostalCode>
                {/if}
                {if !empty($Item['ProductListingDetails']) }
                <ProductListingDetails>
                    {if !empty($Item['ProductListingDetails']['EAN']) }
                        <EAN><![CDATA[{$Item['ProductListingDetails']['EAN']}]]></EAN>
                    {/if}
                    <ListIfNoProduct>1</ListIfNoProduct>
                </ProductListingDetails>
                {/if}
                {if !empty($Item['PaymentMethods']['_Repeat']) }
                {foreach from = $Item['PaymentMethods']['_Repeat'] item = Payment }
                {*<BuyerRequirementDetails>
                    <LinkedPayPalAccount>FALSE</LinkedPayPalAccount>
                </BuyerRequirementDetails>*}
                        <PaymentMethods><![CDATA[{$Payment}]]></PaymentMethods>
                {/foreach}
                {/if}
                {if !empty($Item['PayPalEmailAddress']) }
                        <PaymentMethods><![CDATA[PayPal]]></PaymentMethods>
                        <PayPalEmailAddress><![CDATA[{$Item['PayPalEmailAddress']}]]></PayPalEmailAddress>
                {/if}
                {if !empty($Item['InventoryTrackingMethod']) }
                <InventoryTrackingMethod><![CDATA[{$Item['InventoryTrackingMethod']}]]></InventoryTrackingMethod>
                {/if}
                {if !empty($Item['ReturnPolicy']) }
                <ReturnPolicy>
                        <ReturnsAcceptedOption><![CDATA[{$Item['ReturnPolicy']['ReturnsAcceptedOption']}]]></ReturnsAcceptedOption>
                        {if !empty($Item['ReturnPolicy']['Description']) }
                        <Description><![CDATA[{$Item['ReturnPolicy']['Description']}]]></Description>
                        {/if}
                        {if $Item['ReturnPolicy']['ReturnsAcceptedOption'] == 'ReturnsAccepted'}
                        <ReturnsWithinOption><![CDATA[{$Item['ReturnPolicy']['ReturnsWithinOption']}]]></ReturnsWithinOption>
                        <ShippingCostPaidByOption><![CDATA[{$Item['ReturnPolicy']['ShippingCostPaidByOption']}]]></ShippingCostPaidByOption>
                        {/if}
                </ReturnPolicy>
                {/if}
                {if !empty($Item['ShippingDetails']) }
                <ShippingDetails>
                        {if !empty($Item['ShippingDetails']['ShippingServiceOptions']) }
                        {foreach from = $Item['ShippingDetails']['ShippingServiceOptions']['_Repeat'] item = ShippingServiceOptions }
                        <ShippingServiceOptions>
                                <ShippingService><![CDATA[{$ShippingServiceOptions['ShippingService']}]]></ShippingService>
                                <ShippingServiceAdditionalCost>{$ShippingServiceOptions['ShippingServiceAdditionalCost']}</ShippingServiceAdditionalCost>
                                <ShippingServiceCost>{$ShippingServiceOptions['ShippingServiceCost']}</ShippingServiceCost>
                                <ShippingServicePriority>{$ShippingServiceOptions['ShippingServicePriority']}</ShippingServicePriority>
                                {if !empty($ShippingServiceOptions['FreeShipping']) }
                                <FreeShipping>{$ShippingServiceOptions['FreeShipping']}</FreeShipping>
                                {/if}
                        </ShippingServiceOptions>
                        {/foreach}
                        {/if}
                        {if !empty($Item['ShippingDetails']['InternationalShippingServiceOption']) }
                        {foreach from = $Item['ShippingDetails']['InternationalShippingServiceOption']['_Repeat'] item = InternationalShippingServiceOption }
                        <InternationalShippingServiceOption>
                                <ShippingService><![CDATA[{$InternationalShippingServiceOption['ShippingService']}]]></ShippingService>
                                <ShippingServiceAdditionalCost>{$InternationalShippingServiceOption['ShippingServiceAdditionalCost']}</ShippingServiceAdditionalCost>
                                <ShippingServiceCost>{$InternationalShippingServiceOption['ShippingServiceCost']}</ShippingServiceCost>
                                <ShippingServicePriority>{$InternationalShippingServiceOption['ShippingServicePriority']}</ShippingServicePriority>
                                {if !empty($InternationalShippingServiceOption['ShipToLocation']) }
                                {foreach from = $InternationalShippingServiceOption['ShipToLocation']['_Repeat'] item = ShipToLocation }
                                <ShipToLocation><![CDATA[{$ShipToLocation}]]></ShipToLocation>
                                {/foreach}
                                {/if}
                        </InternationalShippingServiceOption>
                        {/foreach}
                        {/if}
                </ShippingDetails>
                {/if}
                {if !empty($Item['Variations']) && count($Item['Variations']) }
                <Variations>
                        {if !empty($Item['Variations']['VariationSpecificsSet']) }
                        <VariationSpecificsSet>
                                {if !empty($Item['Variations']['VariationSpecificsSet']['NameValueList']) }
                                {foreach from = $Item['Variations']['VariationSpecificsSet']['NameValueList']['_Repeat'] item = NameValueList }
                                <NameValueList>
                                        <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                        {foreach from = $NameValueList['Value']['_Repeat'] item = Value }
                                        <Value><![CDATA[{$Value}]]></Value>
                                        {/foreach}
                                </NameValueList>
                                {/foreach}
                                {/if}
                        </VariationSpecificsSet>
                        {/if}
                        {if !empty($Item['Variations']['Variation']) }
                        {foreach from = $Item['Variations']['Variation']['_Repeat'] item = Variation }
                        <Variation>
                                <SKU><![CDATA[{$Variation['SKU']}]]></SKU>
                                <StartPrice>{$Variation['StartPrice']}</StartPrice>
                                <Quantity>{$Variation['Quantity']}</Quantity>
                                {if !empty($Variation['VariationSpecifics']) }
                                <VariationSpecifics>
                                        {if !empty($Variation['VariationSpecifics']['NameValueList']) }
                                        {foreach from = $Variation['VariationSpecifics']['NameValueList']['_Repeat'] item = NameValueList }
                                        <NameValueList>
                                                <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                                <Value><![CDATA[{$NameValueList['Value']}]]></Value>
                                        </NameValueList>
                                        {/foreach}
                                        {/if}
                                </VariationSpecifics>
                                {/if}
                                {if !empty($Variation['DiscountPriceInfo']) }
                                <DiscountPriceInfo>
                                        {if !empty($Variation['DiscountPriceInfo']['OriginalRetailPrice']) }
                                        <OriginalRetailPrice>{$Variation['DiscountPriceInfo']['OriginalRetailPrice']}</OriginalRetailPrice>
                                        {/if}
                                        {if !empty($Variation['DiscountPriceInfo']['SoldOneBay']) }
                                        <SoldOneBay>{$Variation['DiscountPriceInfo']['SoldOneBay']}</SoldOneBay>
                                        {/if}
                                        {if !empty($Variation['DiscountPriceInfo']['PricingTreatment']) }
                                        <PricingTreatment>{$Variation['DiscountPriceInfo']['PricingTreatment']}</PricingTreatment>
                                        {/if}
                                        {if !empty($Variation['DiscountPriceInfo']['MinimumAdvertisedPriceExposure']) }
                                        <MinimumAdvertisedPriceExposure>{$Variation['DiscountPriceInfo']['MinimumAdvertisedPriceExposure']}</MinimumAdvertisedPriceExposure>
                                        {/if}
                                        {if !empty($Variation['DiscountPriceInfo']['MinimumAdvertisedPrice']) }
                                        <MinimumAdvertisedPrice>{$Variation['DiscountPriceInfo']['MinimumAdvertisedPrice']}</MinimumAdvertisedPrice>
                                        {/if}
                                        {if !empty($Variation['DiscountPriceInfo']['MadeForOutletComparisonPrice']) }
                                        <MadeForOutletComparisonPrice>{$Variation['DiscountPriceInfo']['MadeForOutletComparisonPrice']}</MadeForOutletComparisonPrice>
                                        {/if}
                                </DiscountPriceInfo>
                                {/if}
                        </Variation>
                        {/foreach}
                        {/if}
                        {if !empty($Item['Variations']['Pictures']) }
                        <Pictures>
                                {if !empty($Item['Variations']['Pictures']['VariationSpecificName']) }
                                <VariationSpecificName><![CDATA[{$Item['Variations']['Pictures']['VariationSpecificName']}]]></VariationSpecificName>
                                {foreach from = $Item['Variations']['Pictures']['VariationSpecificPictureSet']['_Repeat'] item = VariationSpecificPictureSet }
                                <VariationSpecificPictureSet>
                                        <VariationSpecificValue><![CDATA[{$VariationSpecificPictureSet['VariationSpecificValue']}]]></VariationSpecificValue>
                                        {foreach from = $VariationSpecificPictureSet['PictureURL']['_Repeat'] item = PictureURL }
                                        <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                        {/foreach}
                                </VariationSpecificPictureSet>
                                {/foreach}
                                {/if}
                        </Pictures>
                        {/if}
                </Variations>
                {/if}
        </Item>
        {/if}
</ReviseFixedPriceItemRequest>