<?xml version="1.0" encoding="utf-8"?>
<GetCategoryFeaturesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
            <eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <CategoryID><![CDATA[{$CategoryID}]]></CategoryID>
    <ViewAllNodes><![CDATA[{$ViewAllNodes}]]></ViewAllNodes>
    <FeatureID><![CDATA[{$FeatureID}]]></FeatureID>
    <AllFeaturesForCategory><![CDATA[{$AllFeaturesForCategory}]]></AllFeaturesForCategory>
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    <DetailLevel><![CDATA[{$DetailLevel}]]></DetailLevel>
</GetCategoryFeaturesRequest>