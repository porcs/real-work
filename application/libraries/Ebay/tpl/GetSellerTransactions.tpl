<?xml version="1.0" encoding="utf-8"?>
<GetSellerTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
            <eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    {if !empty($SKUArray) && count($SKUArray) }
    <SKUArray>
            {foreach $SKUArray['_Repeat'] as $sku_selected }
                    {if !empty($sku_selected) }
                            <SKU><![CDATA[{$sku_selected}]]></SKU>
                    {/if}
            {/foreach}
    </SKUArray>
    {/if}
    {if !empty($IncludeCodiceFiscale) && count($IncludeCodiceFiscale) }
    <IncludeCodiceFiscale>{$IncludeCodiceFiscale}</IncludeCodiceFiscale>
    {/if}
    {if !empty($IncludeFinalValueFee) && count($IncludeFinalValueFee) }
    <IncludeFinalValueFee>{$IncludeFinalValueFee}</IncludeFinalValueFee>
    {/if}
    {if !empty($IncludeContainingOrder) && count($IncludeContainingOrder) }
    <IncludeContainingOrder>{$IncludeContainingOrder}</IncludeContainingOrder>
    {/if}
    {if !empty($InventoryTrackingMethod) && count($InventoryTrackingMethod) }
    <InventoryTrackingMethod>{$InventoryTrackingMethod}</InventoryTrackingMethod>
    {/if}
    {if !empty($DetailLevel) && count($DetailLevel) }
    <DetailLevel><![CDATA[{$DetailLevel}]]></DetailLevel>
    {/if}
    {if !empty($ErrorLanguage) && count($ErrorLanguage) }
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    {/if}
</GetSellerTransactionsRequest>