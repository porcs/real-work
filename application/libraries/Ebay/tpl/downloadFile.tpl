<?xml version="1.0" encoding="utf-8"?>
<downloadFileRequest xmlns:sct="http://www.ebay.com/soaframework/common/types" xmlns="http://www.ebay.com/marketplace/services">
    {if !empty($taskReferenceId)}<taskReferenceId>{$taskReferenceId}</taskReferenceId>{/if}
    {if !empty($fileReferenceId)}<fileReferenceId>{$fileReferenceId}</fileReferenceId>{/if}
</downloadFileRequest>