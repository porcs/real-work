<?xml version="1.0" encoding="utf-8"?>
{strip}
<BulkDataExchangeRequests>
        <Header>
		<SiteID>{if isset($SiteID)}{$SiteID}{else}{"0"}{/if}</SiteID>
		<Version>{if isset($Version)}{$Version}{else}{"663"}{/if}</Version>
	</Header>
        {if isset($BulkDataExchange)}
                {foreach from = $BulkDataExchange key = method item = Bulk}
                {if isset($Bulk)}
                        {foreach from = $BulkDataExchange[$method]['_Repeat'] item = AddFixedPriceItem}
                                {if isset($AddFixedPriceItem['Item']['Title'])}
                                {assign var="Title" value=$AddFixedPriceItem['Item']['Title']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['Description'])}
                                {assign var="Description" value=$AddFixedPriceItem['Item']['Description']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['Description_short'])}
                                {assign var="Description_short" value=$AddFixedPriceItem['Item']['Description_short']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['PrimaryCategory']['CategoryID'])}
                                {assign var="CategoryID" value=$AddFixedPriceItem['Item']['PrimaryCategory']['CategoryID']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['SKU'])}
                                {assign var="SKU" value=$AddFixedPriceItem['Item']['SKU']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['DispatchTimeMax'])}
                                {assign var="DispatchTimeMax" value=$AddFixedPriceItem['Item']['DispatchTimeMax']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['ListingDuration'])}
                                {assign var="ListingDuration" value=$AddFixedPriceItem['Item']['ListingDuration']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['Country'])}
                                {assign var="Country" value=$AddFixedPriceItem['Item']['Country']}
                                {/if}
                                {if !empty($AddFixedPriceItem['Item']['Location'])}
                                {assign var="Location" value=$AddFixedPriceItem['Item']['Location']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['StartPrice'])}
                                {assign var="StartPrice" value=$AddFixedPriceItem['Item']['StartPrice']}
                                {else}
                                {assign var="StartPrice" value="*"}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['PostalCode'])}
                                {assign var="PostalCode" value=$AddFixedPriceItem['Item']['PostalCode']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['Currency'])}
                                {assign var="Currency" value=$AddFixedPriceItem['Item']['Currency']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['PayPalEmailAddress'])}
                                {assign var="PayPalEmailAddress" value=$AddFixedPriceItem['Item']['PayPalEmailAddress']}
                                {/if}
                                {if isset($AddFixedPriceItem['Item']['Manufacturer'])}
                                {assign var="Manufacturer" value=$AddFixedPriceItem['Item']['Manufacturer']}
                                {/if}
                                {if !empty($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                        {if is_array($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                        {foreach from = $AddFixedPriceItem['Item']['PictureDetails']['PictureURL']['_Repeat'] key = PictureKey item = PictureURL }
                                        {assign var="Image{$PictureKey}" value=$PictureURL}
                                        {assign var="HasImage{$PictureKey}" value=$AddFixedPriceItem['Item']['PictureDetails']['PictureURL']['_HasPic'][$PictureKey]}
                                        {/foreach}
                                        {/if}
                                {/if}
                                {if empty($Image1)}
                                    {assign var="Image1" value=''}
                                {/if}
                                {if empty($Image2)}
                                    {assign var="Image2" value=''}
                                {/if}
                                {if empty($Image3)}
                                    {assign var="Image3" value=''}
                                {/if}
                                {assign var="vMinPrice" value = 0}
                                {assign var="vMaxPrice" value = 0}
                                {if !empty($AddFixedPriceItem['Item']['Variations']['Variation']) }
                                    {foreach from = $AddFixedPriceItem['Item']['Variations']['Variation']['_Repeat'] item = Variation }
                                        {if empty($vMinPrice) && empty($vMaxPrice) && isset($Variation['StartPrice']) } 
                                            {assign var="vMinPrice" value = $Variation['StartPrice']}
                                            {assign var="vMaxPrice" value = $Variation['StartPrice']}
                                        {elseif isset($vMaxPrice) && isset($Variation['StartPrice']) && $Variation['StartPrice'] > $vMaxPrice}
                                            {assign var="vMaxPrice" value = $Variation['StartPrice']}
                                        {elseif isset($vMinPrice) && isset($Variation['StartPrice']) && $Variation['StartPrice'] < $vMinPrice}
                                            {assign var="vMinPrice" value = $Variation['StartPrice']}
                                        {/if}
                                    {/foreach}
                                    {if isset($vMinPrice) && isset($vMaxPrice) && ($vMinPrice == $vMaxPrice)} 
                                            {assign var="StartPrice" value = $vMinPrice}
                                    {elseif isset($vMinPrice) && isset($vMaxPrice) && ($vMinPrice != $vMaxPrice)}
                                            {assign var="StartPrice" value = $vMinPrice|cat:'" - "'|cat:$vMaxPrice}
                                    {/if}
                                {/if}
                                {if isset($AddFixedPriceItem) && ($method == 'AddFixedPriceItem' || $method == 'RelistFixedPriceItem' || $method == 'ReviseFixedPriceItem' || $method == 'VerifyAddFixedPriceItem' || $method == 'AddItem' ) }
                                <{$method}Request xmlns="urn:ebay:apis:eBLBaseComponents">
                                        <ErrorLanguage>{$AddFixedPriceItem['ErrorLanguage']}</ErrorLanguage>
                                        <Version>{$AddFixedPriceItem['Version']}</Version>
                                        <MessageID><![CDATA[{$AddFixedPriceItem['MessageID']}]]></MessageID>
                                        {if $method == 'ReviseFixedPriceItem' && !empty($AddFixedPriceItem['Item']['Variations']) && count($AddFixedPriceItem['Item']['Variations']) > 0 }
                                        <DeletedField><![CDATA[Item.SKU]]></DeletedField>
                                        {/if}
                                        <Item>
                                        	{if empty($AddFixedPriceItem['Item']['Variations']) || count($AddFixedPriceItem['Item']['Variations']) == 0 }
                                                	<SKU><![CDATA[{$AddFixedPriceItem['Item']['SKU']}]]></SKU>
                                                {/if}
                                                <AutoPay>{$AddFixedPriceItem['Item']['AutoPay']}</AutoPay>
                                                {*if !empty($AddFixedPriceItem['Item']['ApplicationData']) }
                                                <ApplicationData>{$AddFixedPriceItem['Item']['ApplicationData']}</ApplicationData>
                                                {/if*}
                                                {if isset($AddFixedPriceItem['Item']['Title']) }
                                                <Title><![CDATA[{$AddFixedPriceItem['Item']['Title']}]]></Title>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['Description']) }
                                                <Description>
                                                    {if isset($AddFixedPriceItem['Item']['template_description']) }
                                                        <![CDATA[{include file=$AddFixedPriceItem['Item']['template_description']}]]> 
                                                    {else}
                                                        <![CDATA[{$AddFixedPriceItem['Item']['Description']}]]>
                                                    {/if}
                                                </Description>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['PrimaryCategory']) }
                                                <PrimaryCategory>
                                                        <CategoryID><![CDATA[{$AddFixedPriceItem['Item']['PrimaryCategory']['CategoryID']}]]></CategoryID>
                                                </PrimaryCategory>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['SecondaryCategory']) }
                                                <SecondaryCategory>
                                                        <CategoryID><![CDATA[{$AddFixedPriceItem['Item']['SecondaryCategory']['CategoryID']}]]></CategoryID>
                                                </SecondaryCategory>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['Storefront']) }
                                                <Storefront>
                                                    {if isset($AddFixedPriceItem['Item']['Storefront']['StoreCategory2ID']) }
                                                    <StoreCategory2ID><![CDATA[{$AddFixedPriceItem['Item']['Storefront']['StoreCategory2ID']}]]></StoreCategory2ID>
                                                    {/if}
                                                    {if isset($AddFixedPriceItem['Item']['Storefront']['StoreCategory2Name']) }
                                                    <StoreCategory2Name><![CDATA[{$AddFixedPriceItem['Item']['Storefront']['StoreCategory2Name']}]]></StoreCategory2Name>
                                                    {/if}
                                                    {if isset($AddFixedPriceItem['Item']['Storefront']['StoreCategoryID']) }
                                                    <StoreCategoryID><![CDATA[{$AddFixedPriceItem['Item']['Storefront']['StoreCategoryID']}]]></StoreCategoryID>
                                                    {/if}
                                                    {if isset($AddFixedPriceItem['Item']['Storefront']['StoreCategoryName']) }
                                                    <StoreCategoryName><![CDATA[{$AddFixedPriceItem['Item']['Storefront']['StoreCategoryName']}]]></StoreCategoryName>
                                                    {/if}
                                                </Storefront>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['ConditionID']) }
                                                <ConditionID>{$AddFixedPriceItem['Item']['ConditionID']}</ConditionID>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['ConditionDescription']) && isset($AddFixedPriceItem['Item']['ConditionID']) && $AddFixedPriceItem['Item']['ConditionID'] != '1000'  }
                                                <ConditionDescription>{$AddFixedPriceItem['Item']['ConditionDescription']}</ConditionDescription>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['CategoryMappingAllowed']) }
                                                <CategoryMappingAllowed>{$AddFixedPriceItem['Item']['CategoryMappingAllowed']}</CategoryMappingAllowed>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['DispatchTimeMax']) }
                                                <DispatchTimeMax><![CDATA[{$AddFixedPriceItem['Item']['DispatchTimeMax']}]]></DispatchTimeMax>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['ListingDuration']) && $method != 'ReviseFixedPriceItem' }
                                                <ListingDuration><![CDATA[{$AddFixedPriceItem['Item']['ListingDuration']}]]></ListingDuration>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['HitCounter']) }
                                                <HitCounter><![CDATA[{$AddFixedPriceItem['Item']['HitCounter']}]]></HitCounter>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['Country']) }
                                                <Country><![CDATA[{$AddFixedPriceItem['Item']['Country']}]]></Country>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['Currency']) }
                                                <Currency><![CDATA[{$AddFixedPriceItem['Item']['Currency']}]]></Currency>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['Location']) }
                                                <Location><![CDATA[{$AddFixedPriceItem['Item']['Location']}]]></Location>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['OutOfStockControl']) }
                                                <OutOfStockControl>{$AddFixedPriceItem['Item']['OutOfStockControl']}</OutOfStockControl>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['Quantity']) && (isset($AddFixedPriceItem['Item']['OutOfStockControl']) || $AddFixedPriceItem['Item']['Quantity'] > 0 ) }
                                                <Quantity>{$AddFixedPriceItem['Item']['Quantity']}</Quantity>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['ItemID']) }
                                                <ItemID>{$AddFixedPriceItem['Item']['ItemID']}</ItemID>
                                                {/if}
                                                {if isset($AddFixedPriceItem['Item']['StartPrice']) && (isset($AddFixedPriceItem['Item']['OutOfStockControl']) || $AddFixedPriceItem['Item']['StartPrice'] > 0 ) }
                                                <StartPrice>{$AddFixedPriceItem['Item']['StartPrice']}</StartPrice>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['PictureDetails']) }
                                                <PictureDetails>
                                                        <GalleryType><![CDATA[Gallery]]></GalleryType>
                                                        {if !empty($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                                                {if is_array($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                                                {foreach from = $AddFixedPriceItem['Item']['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                                                {if $PictureURL != 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}
                                                                <GalleryURL><![CDATA[{$PictureURL}]]></GalleryURL>
                                                                {/if}
                                                                {/foreach}
                                                                {/if}
                                                        {else}  
                                                                <GalleryURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></GalleryURL>
                                                        {/if}
                                                        {if !empty($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                                                {if is_array($AddFixedPriceItem['Item']['PictureDetails']['PictureURL'])}
                                                                {foreach from = $AddFixedPriceItem['Item']['PictureDetails']['PictureURL']['_Repeat'] item = PictureURL }
                                                                {if $PictureURL != 'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}
                                                                <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                                                {/if}
                                                                {/foreach}
                                                                {/if}
                                                        {else}  
                                                                <PictureURL><![CDATA[{'http://p.ebaystatic.com/aw/pics/cmp/icn/iconImgNA_96x96.gif'}]]></PictureURL>
                                                        {/if}
                                                </PictureDetails>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['PostalCode']) }
                                                <PostalCode><![CDATA[{$AddFixedPriceItem['Item']['PostalCode']}]]></PostalCode>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']) }
                                                <ProductListingDetails>
                                                    {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']['Brand']) }
                                                        <BrandMPN>
                                                            <Brand><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['Brand']}]]></Brand>
                                                            <MPN><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['MPN']}]]></MPN>
                                                        </BrandMPN>
                                                    {/if}
                                                    {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']['EAN']) }
                                                        <EAN><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['EAN']}]]></EAN>
                                                    {/if}
                                                    {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']['UPC']) }
                                                        <UPC><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['UPC']}]]></UPC>
                                                    {/if}
                                                    <UseFirstProduct>true</UseFirstProduct>
                                                    <ReturnSearchResultOnDuplicates>true</ReturnSearchResultOnDuplicates>
                                                </ProductListingDetails>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']) }
                                                <ItemSpecifics>
                                                    {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']['Brand']) }
                                                        <NameValueList>
                                                            <Name>Brand</Name>
                                                            <Value><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['Brand']}]]></Value>
                                                        </NameValueList>
                                                    {/if}
                                                    {if !empty($AddFixedPriceItem['Item']['ProductListingDetails']['MPN']) }
                                                        <NameValueList>
                                                            <Name>MPN</Name>
                                                            <Value><![CDATA[{$AddFixedPriceItem['Item']['ProductListingDetails']['MPN']}]]></Value>
                                                        </NameValueList>
                                                    {/if}
                                                </ItemSpecifics>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['PaymentMethods']['_Repeat']) }
                                                {foreach from = $AddFixedPriceItem['Item']['PaymentMethods']['_Repeat'] item = Payment }
                                                {*<BuyerRequirementDetails>
                                                    <LinkedPayPalAccount>FALSE</LinkedPayPalAccount>
                                                </BuyerRequirementDetails>*}
                                                        <PaymentMethods><![CDATA[{$Payment}]]></PaymentMethods>
                                                {/foreach}
                                                {/if}
                                                
                                                {if !empty($AddFixedPriceItem['Item']['PayPalEmailAddress']) }
                                                        <PaymentMethods><![CDATA[PayPal]]></PaymentMethods>
                                                        <PayPalEmailAddress><![CDATA[{$AddFixedPriceItem['Item']['PayPalEmailAddress']}]]></PayPalEmailAddress>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['InventoryTrackingMethod']) }
                                                <InventoryTrackingMethod><![CDATA[{$AddFixedPriceItem['Item']['InventoryTrackingMethod']}]]></InventoryTrackingMethod>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['ReturnPolicy']) }
                                                <ReturnPolicy>
                                                        <ReturnsAcceptedOption><![CDATA[{$AddFixedPriceItem['Item']['ReturnPolicy']['ReturnsAcceptedOption']}]]></ReturnsAcceptedOption>
                                                        {if isset($AddFixedPriceItem['Item']['ReturnPolicy']['Description']) }
                                                        <Description><![CDATA[{$AddFixedPriceItem['Item']['ReturnPolicy']['Description']}]]></Description>
                                                        {/if}
                                                        {if $AddFixedPriceItem['Item']['ReturnPolicy']['ReturnsAcceptedOption'] == 'ReturnsAccepted'}
                                                        <ReturnsWithinOption><![CDATA[{$AddFixedPriceItem['Item']['ReturnPolicy']['ReturnsWithinOption']}]]></ReturnsWithinOption>
                                                        <ShippingCostPaidByOption><![CDATA[{$AddFixedPriceItem['Item']['ReturnPolicy']['ShippingCostPaidByOption']}]]></ShippingCostPaidByOption>
                                                        {/if}
                                                </ReturnPolicy>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['ShippingDetails']) }
                                                <ShippingDetails>
		                                                {if isset($AddFixedPriceItem['Item']['PaymentInstructions']) }
		                                                	<PaymentInstructions>{$AddFixedPriceItem['Item']['PaymentInstructions']}</PaymentInstructions>
		                                                {/if}
                                                
                                                        {if !empty($AddFixedPriceItem['Item']['ShippingDetails']['ShippingServiceOptions']) }
                                                        {foreach from = $AddFixedPriceItem['Item']['ShippingDetails']['ShippingServiceOptions']['_Repeat'] item = ShippingServiceOptions }
                                                        <ShippingServiceOptions>
                                                                <ShippingService><![CDATA[{$ShippingServiceOptions['ShippingService']}]]></ShippingService>
                                                                <ShippingServiceAdditionalCost>{$ShippingServiceOptions['ShippingServiceAdditionalCost']}</ShippingServiceAdditionalCost>
                                                                <ShippingServiceCost>{$ShippingServiceOptions['ShippingServiceCost']}</ShippingServiceCost>
                                                                <ShippingServicePriority>{$ShippingServiceOptions['ShippingServicePriority']}</ShippingServicePriority>
                                                                {if !empty($ShippingServiceOptions['FreeShipping']) }
                                                                <FreeShipping>{$ShippingServiceOptions['FreeShipping']}</FreeShipping>
                                                                {/if}
                                                        </ShippingServiceOptions>
                                                        {/foreach}
                                                        {/if}
                                                        {if !empty($AddFixedPriceItem['Item']['ShippingDetails']['InternationalShippingServiceOption']) }
                                                        {foreach from = $AddFixedPriceItem['Item']['ShippingDetails']['InternationalShippingServiceOption']['_Repeat'] item = InternationalShippingServiceOption }
                                                        <InternationalShippingServiceOption>
                                                                <ShippingService><![CDATA[{$InternationalShippingServiceOption['ShippingService']}]]></ShippingService>
                                                                <ShippingServiceAdditionalCost>{$InternationalShippingServiceOption['ShippingServiceAdditionalCost']}</ShippingServiceAdditionalCost>
                                                                <ShippingServiceCost>{$InternationalShippingServiceOption['ShippingServiceCost']}</ShippingServiceCost>
                                                                <ShippingServicePriority>{$InternationalShippingServiceOption['ShippingServicePriority']}</ShippingServicePriority>
                                                                {if !empty($InternationalShippingServiceOption['ShipToLocation']) }
                                                                {foreach from = $InternationalShippingServiceOption['ShipToLocation']['_Repeat'] item = ShipToLocation }
                                                                <ShipToLocation><![CDATA[{$ShipToLocation}]]></ShipToLocation>
                                                                {/foreach}
                                                                {/if}
                                                        </InternationalShippingServiceOption>
                                                        {/foreach}
                                                        {/if}
                                                </ShippingDetails>
                                                {/if}
                                                {if !empty($AddFixedPriceItem['Item']['Variations']) && count($AddFixedPriceItem['Item']['Variations']) }
                                                <Variations>
                                                        {if !empty($AddFixedPriceItem['Item']['Variations']['VariationSpecificsSet']) }
                                                        <VariationSpecificsSet>
                                                                {if !empty($AddFixedPriceItem['Item']['Variations']['VariationSpecificsSet']['NameValueList']) }
                                                                {foreach from = $AddFixedPriceItem['Item']['Variations']['VariationSpecificsSet']['NameValueList']['_Repeat'] item = NameValueList }
                                                                <NameValueList>
                                                                        <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                                                        {foreach from = $NameValueList['Value']['_Repeat'] item = Value }
                                                                        <Value><![CDATA[{$Value}]]></Value>
                                                                        {/foreach}
                                                                </NameValueList>
                                                                {/foreach}
                                                                {/if}
                                                        </VariationSpecificsSet>
                                                        {/if}
                                                        {if !empty($AddFixedPriceItem['Item']['Delete_variation']) && $method == 'ReviseFixedPriceItem' }
                                                        {foreach from = $AddFixedPriceItem['Item']['Delete_variation'] item = delete_variation }
                                                        <Variation>
                                                                <Delete>True</Delete>
                                                                <SKU><![CDATA[{$delete_variation['SKU']}]]></SKU>
                                                                <StartPrice>{$delete_variation['start_price']}</StartPrice>
                                                                <Quantity>{$delete_variation['quantity']}</Quantity>
                                                                <VariationSpecifics>
                                                                        {foreach from = $delete_variation['variation'] item = spacific_variation }
                                                                        {if !empty($spacific_variation['variation_name']) }
                                                                        <NameValueList>
                                                                                <Name><![CDATA[{$spacific_variation['variation_name']}]]></Name>
                                                                                <Value><![CDATA[{$spacific_variation['variation_value']}]]></Value>
                                                                        </NameValueList>
                                                                        {/if}
                                                                        {/foreach}
                                                                </VariationSpecifics>
                                                        </Variation>
                                                        {/foreach}
                                                        {/if}
                                                        {if !empty($AddFixedPriceItem['Item']['Variations']['Variation']) }
                                                        {foreach from = $AddFixedPriceItem['Item']['Variations']['Variation']['_Repeat'] item = Variation }
                                                        <Variation>
                                                                <SKU><![CDATA[{$Variation['SKU']}]]></SKU>
                                                                <StartPrice>{$Variation['StartPrice']}</StartPrice>
                                                                <Quantity>{$Variation['Quantity']}</Quantity>
                                                                {if !empty($Variation['EAN']) || !empty($Variation['UPC']) }
                                                                <VariationProductListingDetails>
                                                                    {if !empty($Variation['EAN']) }
                                                                        <EAN>{$Variation['EAN']}</EAN>
                                                                    {/if}
                                                                    {if !empty($Variation['UPC']) }
                                                                        <UPC>{$Variation['UPC']}</UPC>
                                                                    {/if}
                                                                </VariationProductListingDetails>
                                                                {/if}
                                                                {if !empty($Variation['VariationSpecifics']) }
                                                                <VariationSpecifics>
                                                                        {if !empty($Variation['VariationSpecifics']['NameValueList']) }
                                                                        {foreach from = $Variation['VariationSpecifics']['NameValueList']['_Repeat'] item = NameValueList }
                                                                        <NameValueList>
                                                                                <Name><![CDATA[{$NameValueList['Name']}]]></Name>
                                                                                <Value><![CDATA[{$NameValueList['Value']}]]></Value>
                                                                        </NameValueList>
                                                                        {/foreach}
                                                                        {/if}
                                                                </VariationSpecifics>
                                                                {/if}
                                                                {if !empty($Variation['DiscountPriceInfo']) }
                                                                <DiscountPriceInfo>
                                                                        {if !empty($Variation['DiscountPriceInfo']['OriginalRetailPrice']) }
                                                                        <OriginalRetailPrice>{$Variation['DiscountPriceInfo']['OriginalRetailPrice']}</OriginalRetailPrice>
                                                                        {/if}
                                                                        {if !empty($Variation['DiscountPriceInfo']['SoldOneBay']) }
                                                                        <SoldOneBay>{$Variation['DiscountPriceInfo']['SoldOneBay']}</SoldOneBay>
                                                                        {/if}
                                                                        {if !empty($Variation['DiscountPriceInfo']['PricingTreatment']) }
                                                                        <PricingTreatment>{$Variation['DiscountPriceInfo']['PricingTreatment']}</PricingTreatment>
                                                                        {/if}
                                                                        {if !empty($Variation['DiscountPriceInfo']['MinimumAdvertisedPriceExposure']) }
                                                                        <MinimumAdvertisedPriceExposure>{$Variation['DiscountPriceInfo']['MinimumAdvertisedPriceExposure']}</MinimumAdvertisedPriceExposure>
                                                                        {/if}
                                                                        {if !empty($Variation['DiscountPriceInfo']['MinimumAdvertisedPrice']) }
                                                                        <MinimumAdvertisedPrice>{$Variation['DiscountPriceInfo']['MinimumAdvertisedPrice']}</MinimumAdvertisedPrice>
                                                                        {/if}
                                                                        {if !empty($Variation['DiscountPriceInfo']['MadeForOutletComparisonPrice']) }
                                                                        <MadeForOutletComparisonPrice>{$Variation['DiscountPriceInfo']['MadeForOutletComparisonPrice']}</MadeForOutletComparisonPrice>
                                                                        {/if}
                                                                </DiscountPriceInfo>
                                                                {/if}
                                                        </Variation>
                                                        {/foreach}
                                                        {/if}
                                                        {if !empty($AddFixedPriceItem['Item']['Variations']['Pictures']) }
                                                        <Pictures>
                                                                {if !empty($AddFixedPriceItem['Item']['Variations']['Pictures']['VariationSpecificName']) }
                                                                <VariationSpecificName><![CDATA[{$AddFixedPriceItem['Item']['Variations']['Pictures']['VariationSpecificName']}]]></VariationSpecificName>
                                                                {foreach from = $AddFixedPriceItem['Item']['Variations']['Pictures']['VariationSpecificPictureSet']['_Repeat'] item = VariationSpecificPictureSet }
                                                                <VariationSpecificPictureSet>
                                                                        <VariationSpecificValue><![CDATA[{$VariationSpecificPictureSet['VariationSpecificValue']}]]></VariationSpecificValue>
                                                                        {foreach from = $VariationSpecificPictureSet['PictureURL']['_Repeat'] item = PictureURL }
                                                                        <PictureURL><![CDATA[{$PictureURL}]]></PictureURL>
                                                                        {/foreach}
                                                                </VariationSpecificPictureSet>
                                                                {/foreach}
                                                                {/if}
                                                        </Pictures>
                                                        {/if}
                                                </Variations>
                                                {/if}
                                        </Item>
                                </{$method}Request>
                                {elseif isset($AddFixedPriceItem) && $method == 'EndFixedPriceItem' }
                                <{$method}Request xmlns="urn:ebay:apis:eBLBaseComponents">
                                        <MessageID><![CDATA[{$AddFixedPriceItem['MessageID']}]]></MessageID>
                                        {if isset($AddFixedPriceItem['ErrorLanguage']) }
                                        <ErrorLanguage>{$AddFixedPriceItem['ErrorLanguage']}</ErrorLanguage>
                                        {/if}
                                        {if isset($AddFixedPriceItem['EndingReason']) }
                                        <EndingReason>{$AddFixedPriceItem['EndingReason']}</EndingReason>
                                        {/if}
                                        {if isset($AddFixedPriceItem['ItemID']) }
                                        <ItemID>{$AddFixedPriceItem['ItemID']}</ItemID>
                                        {/if}
                                        {if isset($AddFixedPriceItem['SKU']) }
                                        <SKU>{$AddFixedPriceItem['SKU']}</SKU>
                                        {/if}
                                        {if isset($AddFixedPriceItem['Version']) }
                                        <Version>{$AddFixedPriceItem['Version']}</Version>
                                        {/if}
                                </{$method}Request>
                                {elseif isset($AddFixedPriceItem) && $method == 'SetShipmentTrackingInfo' }
                                <{$method}Request xmlns="urn:ebay:apis:eBLBaseComponents">
                                        {if isset($AddFixedPriceItem['IsPaid']) }
                                        <IsPaid>{$AddFixedPriceItem['IsPaid']}</IsPaid>
                                        {/if}
                                        {if isset($AddFixedPriceItem['IsShipped']) }
                                        <IsShipped>{$AddFixedPriceItem['IsShipped']}</IsShipped>
                                        {/if}
                                        {if isset($AddFixedPriceItem['OrderID']) }
                                        <OrderID>{$AddFixedPriceItem['OrderID']}</OrderID>
                                        {/if}
                                        {if isset($AddFixedPriceItem['OrderLineItemID']) }
                                        <OrderLineItemID>{$AddFixedPriceItem['OrderLineItemID']}</OrderLineItemID>
                                        {/if}
                                        <Shipment>
                                                {if isset($AddFixedPriceItem['ShipmentTrackingNumber']) }
                                                <ShipmentTrackingNumber>{$AddFixedPriceItem['ShipmentTrackingNumber']}</ShipmentTrackingNumber>
                                                {/if}
                                                {if isset($AddFixedPriceItem['ShippedTime']) }
                                                <ShippedTime>{$AddFixedPriceItem['ShippedTime']}</ShippedTime>
                                                {/if}
                                                {if isset($AddFixedPriceItem['ShippingCarrierUsed']) }
                                                <ShippingCarrierUsed>Other</ShippingCarrierUsed>
                                                {/if}
                                        </Shipment>
                                </{$method}Request>
                                {elseif isset($AddFixedPriceItem) && $method == 'ReviseInventoryStatus' }
                                {foreach from = $AddFixedPriceItem['InventoryStatus']['_Repeat'] key = method_key item = InventoryStatus}
                                <{$method}Request xmlns="urn:ebay:apis:eBLBaseComponents">
                                        <ErrorLanguage>{$AddFixedPriceItem['ErrorLanguage']}</ErrorLanguage>
                                        <Version>{$AddFixedPriceItem['Version']}</Version>
                                        <MessageID><![CDATA[{$InventoryStatus['name']|cat:'-#-#-'|cat:$InventoryStatus['id_product']|cat:'-#-#-'|cat:$InventoryStatus['ItemID']|cat:'-#-#-'|cat:$InventoryStatus['SKU']|cat:'-#-#-'|cat:$InventoryStatus['batch_id']}]]></MessageID>
                                                <InventoryStatus>
                                                {if isset($InventoryStatus['ItemID']) }
                                                <ItemID>{$InventoryStatus['ItemID']}</ItemID>
                                                {/if}
                                                {if isset($InventoryStatus['SKU']) }
                                                <SKU>{$InventoryStatus['SKU']}</SKU>
                                                {/if}
                                                {if isset($InventoryStatus['Quantity']) && $InventoryStatus['Quantity'] != "0" }
                                                    <Quantity>{$InventoryStatus['Quantity']}</Quantity>
                                                    {if isset($InventoryStatus['StartPrice']) }
                                                    <StartPrice>{$InventoryStatus['StartPrice']}</StartPrice>
                                                    {/if}
                                                {else if isset($InventoryStatus['Quantity']) && $InventoryStatus['Quantity'] == "0" }
                                                    <Quantity>{$InventoryStatus['Quantity']}</Quantity>
                                                    {if isset($InventoryStatus['StartPrice']) }
                                                    <StartPrice>{$InventoryStatus['StartPrice']}</StartPrice>
                                                    {/if}
                                                {/if}
                                                </InventoryStatus>
                                </{$method}Request>
                                {/foreach}
                                {/if}
                        {/foreach}
                {/if}
                {/foreach}
        {/if}
</BulkDataExchangeRequests>
{/strip}