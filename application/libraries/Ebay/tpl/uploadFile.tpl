<?xml version="1.0" encoding="utf-8"?>
<uploadFileRequest xmlns:sct="http://www.ebay.com/soaframework/common/types" xmlns="http://www.ebay.com/marketplace/services">
    {if !empty($taskReferenceId)}<taskReferenceId>{$taskReferenceId}</taskReferenceId>{/if}
    {if !empty($fileReferenceId)}<fileReferenceId>{$fileReferenceId}</fileReferenceId>{/if}
    {if !empty($fileFormat)}<fileFormat>{$fileFormat}</fileFormat>{/if}
    {if !empty($fileAttachment)}<fileAttachment>{$fileAttachment}</fileAttachment>{/if}
</uploadFileRequest>