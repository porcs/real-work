<?xml version="1.0" encoding="utf-8"?>
<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
    {if isset($ItemID)}
    <ItemID>{$ItemID}</ItemID>
    {/if}
    {if isset($SKU)}
    <SKU>{$SKU}</SKU>
    {/if}
    {if isset($TransactionID)}
    <TransactionID>{$TransactionID}</TransactionID>
    {/if}
    {if isset($VariationSKU)}
    <VariationSKU>{$VariationSKU}</VariationSKU>
    {/if}
    {if isset($DetailLevel)}
    <DetailLevel>{$DetailLevel}</DetailLevel>
    {/if}
    {if isset($ErrorLanguage)}
    <ErrorLanguage>{$ErrorLanguage}</ErrorLanguage>
    {/if}
    {if isset($OutputSelector)}
    <OutputSelector>{$OutputSelector}</OutputSelector>
    {/if}
</GetItemRequest>