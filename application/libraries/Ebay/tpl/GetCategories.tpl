<?xml version="1.0" encoding="utf-8"?>
<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        {if isset($ErrorLanguage)}
                <ErrorLanguage><![CDATA[{$ErrorLanguage}]]></ErrorLanguage>
        {/if}
        {if isset($DetailLevel)}
                <DetailLevel><![CDATA[{$DetailLevel}]]></DetailLevel>
        {/if}
        {if isset($ViewAllNodes)}
                <ViewAllNodes><![CDATA[{$ViewAllNodes}]]></ViewAllNodes>
        {/if}
	<RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
	</RequesterCredentials>
</GetCategoriesRequest>