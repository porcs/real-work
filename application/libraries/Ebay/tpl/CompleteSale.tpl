<?xml version="1.0" encoding="utf-8"?>
<CompleteSaleRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  <RequesterCredentials>
    <eBayAuthToken>{$token}</eBayAuthToken>
  </RequesterCredentials>
  <WarningLevel>High</WarningLevel>
  <FeedbackInfo>
    <CommentText>Automatically update shipping information by Feed.biz.</CommentText>
    <CommentType>Positive</CommentType>
  </FeedbackInfo>
  <ItemID>{$ItemID}</ItemID>
  <OrderID>{$OrderID}</OrderID>
  <OrderLineItemID>{$OrderLineItemID}</OrderLineItemID>
  <Paid>true</Paid>
  <Shipment>
    <Notes>Automatically update shipping information by Feed.biz.</Notes>
	<ShipmentTrackingDetails>
      <ShipmentTrackingNumber>{$ShipmentTrackingNumber}</ShipmentTrackingNumber>
{*	  <ShippingCarrierUsed>{$ShippingCarrierUsed}</ShippingCarrierUsed>*}
	  <ShippingCarrierUsed>Other</ShippingCarrierUsed>
	</ShipmentTrackingDetails>
	<ShippedTime>{$ShippedTime}</ShippedTime>
  </Shipment>
  <Shipped>true</Shipped>
</CompleteSaleRequest>