<?xml version="1.0" encoding="utf-8"?>
<createUploadJobRequest xmlns:sct="http://www.ebay.com/soaframework/common/types" xmlns="http://www.ebay.com/marketplace/services">
    {if !empty($fileType)}<fileType><![CDATA[{$fileType}]]></fileType>{/if}
    {if !empty($uploadJobType)}<uploadJobType><![CDATA[{$uploadJobType}]]></uploadJobType>{/if}
    {if !empty($UUID)}<UUID><![CDATA[{$UUID}]]></UUID>{/if}
</createUploadJobRequest>