<?xml version="1.0" encoding="utf-8"?>
<ReviseInventoryStatusRequest xmlns="urn:ebay:apis:eBLBaseComponents">
        <RequesterCredentials>
		<eBayAuthToken>{$token}</eBayAuthToken>
	</RequesterCredentials>
        <ErrorLanguage>{$ErrorLanguage}</ErrorLanguage>
        <Version>{$Version}</Version>
        <InventoryStatus>
        {if isset($ItemID) }
        <ItemID>{$ItemID}</ItemID>
        {/if}
        {if isset($SKU) }
        <SKU>{$SKU}</SKU>
        {/if}
        {if isset($Quantity) }
        <Quantity>{$Quantity}</Quantity>
        {/if}
        {if isset($StartPrice) }
        <StartPrice>{$StartPrice}</StartPrice>
        {/if}
        </InventoryStatus>
</ReviseInventoryStatusRequest>