<?xml version="1.0" encoding="utf-8"?>
<SetUserPreferencesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
    </RequesterCredentials>
    <SellerPaymentPreferences>
        <PayPalPreferred>1</PayPalPreferred>
        <PayPalAlwaysOn>1</PayPalAlwaysOn>
    </SellerPaymentPreferences>
</SetUserPreferencesRequest>