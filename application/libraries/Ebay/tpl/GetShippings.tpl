<?xml version="1.0" encoding="utf-8"?>
<GeteBayDetailsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
	<RequesterCredentials>
		<eBayAuthToken><![CDATA[{$token}]]></eBayAuthToken>
	</RequesterCredentials>
        {if isset($DetailName)}
                <DetailName><![CDATA[{$DetailName}]]></DetailName>
        {/if}
</GeteBayDetailsRequest>