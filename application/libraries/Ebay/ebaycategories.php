<?php   echo header('Content-Type: text/html; charset=utf-8');
        if ( !file_exists(dirname(__FILE__).'/ebaylib.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/connect.php')) exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/../tools.php')) exit('No direct script access allowed');
        if ( class_exists('ebaylib')   == FALSE ) require dirname(__FILE__).'/ebaylib.php';
        if ( class_exists('connect')   == FALSE ) require dirname(__FILE__).'/connect.php';
        require dirname(__FILE__).'/../tools.php';
        
        class ebaycategories extends ebaylib {
                protected $user_name;
                protected $user_id;
                protected $connect;
                protected $ebaylib;
                protected $ebayUser;  
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function set_configuration($data = '', $result = array()) {        
                        while( list($id_customer, $name, $value, $config_date) = $this->connect->fetch($data) ) :
                                if ( $name == 'EBAY_USER' )                 $result['userID']               = $value;
                                else if ( $name == 'EBAY_TOKEN' )           $result['token']                = $value;
                                else if ( $name == 'EBAY_APPMODE' )         $result['appMode']              = $value;
                                else if ( $name == 'API_TEMPLATE' )         $result['template']             = $value;
                        endwhile;
                        return $result;
                }

                function parse_categories() {
                        $this->data_0                                       = $this->connect->show_table('ebay_categories1');
                        $this->data_1                                       = $this->connect->show_table('ebay_categories_sites1');
                        if ( !$this->connect->count_rows($this->data_0) || !$this->connect->count_rows($this->data_1) ) :
                                $this->connect->create_table('ebay_categories');
                                $this->connect->create_table('ebay_categories_sites');
                        endif;
                        $this->response                                     = $this->ebaylib->core->_parser;
                        if ( !empty($this->response) && $this->response->Ack->__toString() == 'Success' && count($this->response->CategoryArray->Category) ) :
                                foreach ( $this->response->CategoryArray->Category as $category) :
                                        $category = (array)$category;
                                        $dataCategory                       = array(
                                                'id_category'               => $category['CategoryID'],
                                                'id_site'                   => $this->id_site,
                                                'id_parent'                 => ($category['CategoryLevel'] == 1) ? 0 : $category['CategoryParentID'],
                                                'is_root_category'          => ($category['CategoryLevel'] == 1) ? '1' : '0',
                                                'date_add'                  => date('Y-m-d H:i:s', strtotime("now")),
                                        );
                                        
                                        $dataSite                           = array(
                                                'id_category'               => $category['CategoryID'],
                                                'id_site'                   => $this->id_site,
                                                'level'                     => $category['CategoryLevel'],
                                                'name'                      => html_special($category['CategoryName']),
                                                'date_add'                  => date('Y-m-d H:i:s', strtotime("now")),
                                        );
                                        $this->connect->replace_db('ebay_categories1', $dataCategory);
                                        $this->connect->replace_db('ebay_categories_sites1', $dataSite);
                                endforeach;
                        endif;
                }
                
                function get_categories() {
                        while ( list($this->id_site)          = $this->connect->fetch($this->site_configuration) ) :
                                $this->ebaylib->GetCategories($this->id_site);
                                $this->parse_categories();
                        endwhile;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($userdata = array()) {
                        parent::__construct();
                        $this->user_name                        = empty($userdata) ? exit('No direct script access allowed') : $userdata[0];
                        $this->user_id                          = empty($userdata) ? exit('No direct script access allowed') : $userdata[1];
                        $this->connect                          = new connect();
                        $this->user_configuration               = $this->set_configuration($this->connect->get_configuration($this->user_id));
                        $this->ebayUser                         = $this->user_configuration['userID'];
                        $this->ebaylib                          = new ebaylib( ($this->user_configuration['appMode'] == 1) ? array(0) : array(1) );
                        $this->ebaylib->token                   = $this->user_configuration['token'];
                        $this->site_configuration               = $this->connect->get_ebay_site();
                        $this->ebaylib->core->_template         = 1;
                }
        }
