<?php   //echo header('Content-Type: text/html; charset=utf-8');
        if ( !file_exists(dirname(__FILE__).'/config.php'))  exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/classes/ebay.xsd.php'))  exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__).'/classes/ebay.xml.validator.php'))  exit('No direct script access allowed');
        if ( !file_exists(dirname(__FILE__)."/../Smarty/Smarty.class.php"))  exit('No direct script access allowed');
        require dirname(__FILE__).'/config.php';
        require dirname(__FILE__).'/classes/ebay.xsd.php';
        require dirname(__FILE__).'/classes/ebay.xml.validator.php';

        class core {
                public static $_element = array();
                public $_output;
                public $_lastMethod;
                public $_lastObject;
                public $_session;
                public $_parser;
                public $_xmlBody;
                public $_xmlError;
                public $_xmlErrorLog    = array();
                public $_template = 0;
                public $_smarty;
                public $_dom;
                private $_xsd;
                private $_ebayXmlValidator;
                private $_errorCount = 0;
                private $_request;
                private $_paremeter;

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($session = '') {
                        if ( !empty($session)) :
                                $this->_session             = $session;
                        endif;
                        $this->_smarty                      = new Smarty();
                        $this->_xsd                         = new EBayXSD();
                        $this->_dom                         = new DOMDocument();
                        $this->_ebayXmlValidator            = new EBayXmlValidator();
                        $this->_smarty->compile_dir         = dirname(__FILE__) . "/../../views/templates_c";
                        $this->_smarty->template_dir        = dirname(__FILE__) . "/../../views/templates";
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function call($method = '', $request = '', $paremeter = NULL) {
                        if ( empty($method) || empty($request) || !class_exists($method) ) :
                                return;
                        endif;
                        $this->_request                     = $request;
                        $this->_paremeter                   = $paremeter;
                        $ObjMethod                          = $this->lastMethod($method);
                        $log_core                           = array(
                                'Method'                    => $method,
                                'Request'                   => $request,
                                'serverUrl'                 => (!empty($this->_session['endPoint']) && !empty($this->_session['endPoint']) != '') ? $this->_session['endPoint'] : $this->_session['serverUrl'],
                        );

                        if ( $method == 'BulkDataExchange' ) :
//                                $this->_xsd->initialize();
                        endif;
                        if ( empty($this->_template) && count($this->_template) && $this->_template == 0 ) :
                                $ObjMethod->$request($paremeter);
                                $this->_xmlBody             = $ObjMethod->requestXmlBody;
                                self::$_element[$method]    = $ObjMethod->sendHttpRequest($this->_xmlBody);
                        else :
                                $paremeter['token']         = $this->_session['token'];
                                $this->_smarty->assign($paremeter);
                                $this->_xmlBody             = $this->_smarty->fetch(dirname(__FILE__).'/tpl/'.$method.'.tpl');
                                if ( $method == 'BulkDataExchange' ) :
                                        $contents           = preg_replace_callback('/<script\b(.*?)>(.*?)*<\/script>/i', function() {return null;}, $this->_xmlBody);
                                        $contents           = preg_replace_callback('/<script\b(.*?)*>(.*?)<\/script>/i', function() {return null;}, $contents);
                                        $contents           = preg_replace_callback('/<meta\b(.*?)*>/i', function() {return null;}, $contents);
                                        $contents           = preg_replace_callback('/<iframe\b(.*?)\/iframe>/i', function() {return null;}, $contents);
                                        $contents           = preg_replace_callback('/<style\b(.*?)>(.*?)*<\/style>/i', function() {return null;}, $contents);
                                        $this->_dom->loadXML($contents);
                                        $this->_dom->preserveWhitespace = false;
                                        $this->_dom->formatOutput = true;
//                                        $this->_ebayXmlValidator->validateRequest($this->_dom);
                                else :
                                        $this->_output = self::$_element[$method]    = $ObjMethod->sendHttpRequest($this->_xmlBody);
                                endif;
                        endif;
                }
                
                private function debug() {
                        $this->dir_name                     = DIR_SERVER . "/assets/apps/ebay/debug/";
                        $this->dir_file                     = DIR_SERVER . "/assets/apps/ebay/debug/log_call.txt";
                        if(!is_dir($this->dir_name)) :
                                $old = umask(0); 
                                mkdir($this->dir_name, 0777, true);
                                umask($old); 
                        endif;
                        $current = file_get_contents($this->dir_file);
                        $current .= "\n".implode("\t", $log_core);
                        $old = umask(0); 
                        chmod($this->dir_file, 0755);
                        file_put_contents($this->dir_file, $current);
                        umask($old); 
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function parser($node = 'Ack') {
                        libxml_use_internal_errors(true); 
                        if ( empty($this->_xmlError) ) :
                                if ( empty(self::$_element[$this->_lastMethod]) && $this->_errorCount <= 3 ) :
                                        $this->_errorCount++;
                                        $this->call($this->_lastMethod, $this->_request, $this->_paremeter);
                                        $this->parser();
                                endif;

                                try {
                                        $this->_parser                    = new SimpleXMLElement(self::$_element[$this->_lastMethod]);
                                        return $this->_parser->$node;
                                } catch (Exception $e) {
                                        if (!$this->_parser) {
                                                $xml = @explode("\n", self::$_element[$this->_lastMethod]);
                                                $errors = libxml_get_errors();
                                                foreach ($errors as $error) :
                                                        $this->display_xml_error($error, $xml);
                                                endforeach;

                                                libxml_clear_errors();
                                        }
                                        return array();
                                }
                        else :  $this->_parser              = array();
                                return array();
                        endif;
                }
                
                function display_xml_error($error, $xml) {
                        switch ($error->level) :
                                case LIBXML_ERR_WARNING:
                                        $type = "Warning";
                                        break;
                                 case LIBXML_ERR_ERROR:
                                        $type = "Error";
                                        break;
                                case LIBXML_ERR_FATAL:
                                        $type = "Fatal Error";
                                        break;
                        endswitch;
                        
                        $data                   = array(
                                'level'         => $error->level,
                                'code'          => $error->code,
                                'line'          => $error->line,
                                'column'        => $error->column,
                                'message'       => $error->message,
                                'file'          => !empty($error->file) ? $error->file : '',
                                'type'          => !empty($type) ? $type : '',
                                'data'          => date('Y-m-d H:i:s', strtotime('now')),
                        );
                        $this->_xmlErrorLog[]   = $data;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function lastMethod($method = 'GetUser') {
                        $ObjMethod                          = new $method($this->_session);
                        $this->_lastMethod                  = $method;
                        $this->_lastObject                  = $ObjMethod;
                        return $ObjMethod;
                }
        }