<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class ReviseCheckoutStatus extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ShippingAddress']       = array(
                                'Name'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Street1'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Street2'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'CityName'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StateOrProvince'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Country'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'CountryName'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PostalCode'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Phone'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'OrderID'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'CheckoutStatus'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PaymentMethodUsed'                 => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'BuyerID'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShippingAddress'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                }
        }

