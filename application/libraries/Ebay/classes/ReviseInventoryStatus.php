<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class ReviseInventoryStatus extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        parent::$_elements['InventoryStatus']       = array(
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'Quantity'                          => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'InventoryStatus'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

