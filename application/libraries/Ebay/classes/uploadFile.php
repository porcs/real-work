<?php   if ( !file_exists(dirname(__FILE__).'/bulkMessage.php')) exit('No direct script access allowed');
        if ( class_exists('bulkMessage') == FALSE ) require dirname(__FILE__).'/bulkMessage.php';
    
        class uploadFile extends bulkMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'fileAttachment'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'fileFormat'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'fileReferenceId'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'taskReferenceId'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
                
                protected function getHeaders() {	
                        $headers                                    = LoadSession::buildEbayHeaders();
                        $contentType = 'multipart/related; boundary=MIME_boundary;type="application/xop+xml";start="<0.urn:uuid:4A515E9048570>";start-info="text/xml"';
                        array_push($headers, 'Content-Type: ' . $contentType)	;
                        array_push($headers, 'X-EBAY-SOA-SERVICE-NAME: FileTransferService');
                        return $headers;
                }
        }

