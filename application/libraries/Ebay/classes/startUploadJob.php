<?php   if ( !file_exists(dirname(__FILE__).'/bulkMessage.php')) exit('No direct script access allowed');
        if ( class_exists('bulkMessage') == FALSE ) require dirname(__FILE__).'/bulkMessage.php';
    
        class startUploadJob extends bulkMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'jobId'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
                
                protected function getHeaders() {	
                        $headers                                    = LoadSession::buildEbayHeaders();
                        array_push($headers, 'X-EBAY-SOA-SERVICE-NAME: BulkDataExchangeService');
                        return $headers;
                }
        }

