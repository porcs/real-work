<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetMyeBaySelling extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ActiveList']            = array(
                                'Sort'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'ActiveList'                        => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'OutputSelector'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

