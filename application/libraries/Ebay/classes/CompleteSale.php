<?php   if ( !file_exists(dirname(__FILE__).'/getJobStatus.php')) exit('No direct script access allowed');
        if ( class_exists('bulkMessage') == FALSE ) require dirname(__FILE__).'/bulkMessage.php';
    
        class CompleteSale extends bulkMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'ItemID'                             => array(
								'required'                  => true,
								'type'                      => 'string',
								'array'                     => false,
							),
                                'OrderID'                   => array(
								'required'                  => true,
								'type'                      => 'string',
								'array'                     => false,
							),
                                'OrderLineItemID'           => array(
								'required'                  => true,
								'type'                      => 'string',
								'array'                     => false,
							),
                                'ShipmentTrackingNumber'    => array(
								'required'                  => true,
								'type'                      => 'string',
								'array'                     => false,
							),
                                'ShippedTime'               => array(
								'required'                  => true,
								'type'                      => 'string',
								'array'                     => false,
							)
                        );
                }
                
                protected function getHeaders() {
                        $headers                                    = LoadSession::buildEbayHeaders();
                        array_push($headers, 'X-EBAY-API-COMPATIBILITY-LEVEL:903');
                        array_push($headers, 'X-EBAY-API-DEV-NAME:'.$this->devID);
                        array_push($headers, 'X-EBAY-API-APP-NAME:'.$this->appID);
                        array_push($headers, 'X-EBAY-API-CERT-NAME:'.$this->certID);
                        array_push($headers, 'X-EBAY-API-SITEID:'.$this->siteID);
                        array_push($headers, 'X-EBAY-API-CALL-NAME:CompleteSale');
                        return $headers;
                }
        }

