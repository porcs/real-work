<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetOrders extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'CreateTimeFrom'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'CreateTimeTo'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'OrderRole'                         => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'OrderStatus'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

