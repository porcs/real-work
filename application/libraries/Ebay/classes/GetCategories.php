<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetCategories extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'LevelLimit'                        => array(
                                        'required'                  => false,
                                        'type'                      => 'int',
                                        'array'                     => false,
                                ),
                                'CategoryParent'                    => array(
                                        'required'                  => false,
                                        'type'                      => 'string',
                                        'array'                     => false,
                                ),
                                'CategorySiteID'                    => array(
                                        'required'                  => false,
                                        'type'                      => 'string',
                                        'array'                     => false,
                                ),
                                'ViewAllNodes'                      => array(
                                        'required'                  => false,
                                        'type'                      => 'boolean',
                                        'array'                     => false,
                                ),
                                'DetailLevel'                       => array(
                                        'required'                  => true,
                                        'type'                      => 'string',
                                        'array'                     => false,
                                ),
                                'OutputSelector'                    => array(
                                        'required'                  => false,
                                        'type'                      => 'string',
                                        'array'                     => false,
                                ),
                        );
                }
        }

