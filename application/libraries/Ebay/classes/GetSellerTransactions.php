<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetSellerTransactions extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['SKUArray']              = array(
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'SKUArray'                          => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'InventoryTrackingMethod'           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'IncludeCodiceFiscale'              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'IncludeFinalValueFee'              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'IncludeContainingOrder'            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

