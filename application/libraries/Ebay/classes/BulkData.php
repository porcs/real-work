<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';

        class BulkData extends buildMessage {
                public $requestXmlBody      = '';
                public static $_elements = array();

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function BulkMessage($request = '') {
                        $this->requestXmlBody               .= '<?xml version="1.0" encoding="utf-8"?>';
                        $this->requestXmlBody               .= '<'.$this->method.'Requests>'."\r\n";
                        $this->requestXmlBody               .= "\t".'<Header>'."\r\n\t\t".'<Version>'.$this->compatability.'</Version>'."\r\n\t\t".'<SiteID>'.$this->siteID.'</SiteID>'."\r\n\t".'</Header>';
                        foreach ( $request['BulkDataExchange'] as $BulkKey => $BulkRepeat ) :
                                foreach ( $BulkRepeat['_Repeat'] as $key => $product ) :
                                        $this->requestSubXmlBody            = '';
                                        $this->requestXmlBody               .= "\r\n\t".'<'.$BulkKey.'Request xmlns="urn:ebay:apis:eBLBaseComponents">'."\r\n";
                                        $this->requestXmlBody               .= $this->buildSubMessage($product, 'main');
                                        $this->requestXmlBody               .= "\t".'</'.$BulkKey.'Request>'."\r\n";
                                endforeach;                 
                        endforeach;
                        $this->requestXmlBody               .= '</'.$this->method.'Requests>';
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($session = '') {
                        parent::$_elements                  = self::$_elements;
                        parent::__construct($session);
                }
        }

