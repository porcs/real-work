<?php

/**
 * Deals with general XSD properties/methods used
 */
class EBayXSD {
    
    private static $initialized = false;
    //const NS_PREFFIX = "xs:";
    const NS_PREFFIX = "xs:";
    /**************************************************************************
                               DOM XML Schemas
     **************************************************************************/
    private static $mainDom;
    private static $mainXPath;
    
    /**************************************************************************
           Initialize static variables setting respective DOM and DomXPath
     **************************************************************************/
    public static function initialize(){
        if(self::$initialized)
            return;
        //Location of XSD file used for validation
        $xsd_file_content = file_get_contents(realpath(dirname(__FILE__)).'/../xsd/merchantdataservice.xsd');
        
        // Gets rid of all namespace references
        //$xsd_file_content = preg_replace('/\"ns:/', '"', $xsd_file_content);
        //$xsd_file_content = preg_replace('/\"xs:/', '"', $xsd_file_content);
        
        //Load Main XSD
        self::$mainDom = new DOMDocument();
        self::$mainDom->loadXML($xsd_file_content);
        self::$mainDom->formatOutput = true;
        self::$mainXPath = new DOMXPath(self::$mainDom);
        
        //remove annotations to reduce processing and memory used on this elements
        $annotations = self::$mainXPath->query('//' . self::NS_PREFFIX . 'annotation');
        foreach($annotations as $annotation){
            $annotation->parentNode->removeChild($annotation);
        }
        
        self::$initialized = true;
    }
    
    /**************************************************************************
               Functions for accessing Base DOM Documents / DOM Xpaths
     **************************************************************************/
    public static function getMainDom(){
        self::initialize();
        return self::$mainDom;
    }
    public static function getMainXPath(){
        self::initialize();
        return self::$mainXPath;
    }
    
    /**
     * Obtains XML structure of current element
     * @param DOMNode $element
     * @return type
     */
    public static function getReferencedStructure(DOMNode &$element, DOMDocument &$elementDom){
        
        if(!($element instanceof DOMElement)){
            return $element;
        }
        $elementXPath = new DOMXPath($elementDom);
        
        $ref = $element->getAttribute('ref');
        $min = $element->getAttribute('minOccurs') != "" ? $element->getAttribute('minOccurs') : "";
        $max = $element->getAttribute('maxOccurs') != "" ? $element->getAttribute('maxOccurs') : "";
        
        if($ref){ 
            $query = '//' . self::NS_PREFFIX . 'element[@name="' . $ref . '"]';        
            $xpath = array('prod'=>$elementXPath);

            foreach($xpath as $x){
                $result = $x->query($query);
                if($result->length > 0){
                    foreach ($result as $node){
                        
                        $element = $elementDom->importNode($node->cloneNode(true),true);
                        
                        if($min != ""){
                            $element->setAttribute('minOccurs',$min);
                        }
                        if($max != ""){
                            $element->setAttribute('maxOccurs',$max);
                        }
                        $query2 = './/' . self::NS_PREFFIX . 'element[@ref]'; 
                        $children_ref = $elementXPath->query($query2, $element);
                        if($children_ref->length > 0){
                            foreach($children_ref as $child_ref){
                                $children_ref = self::getReferencedStructure($child_ref, $elementDom);
                            }
                        }
                        return $element;
                    }
                }
            }
            
        }else{
            return $element;
        }
    }
    
    /**
     * Searches in XSD files related (Base or Product) for the XSD structure of current element
     * @param DOMElement $element
     */
    public static function getElementStructure(DOMElement &$element, DOMDocument &$elementDom){
        
        $productXPath = new DOMXPath($elementDom);
        
        $xpath = array( 'element'=>$productXPath);  
        //obtain referenced structure of current element
        $element = self::getReferencedStructure($element, $elementDom);
        //Get Structure from its Type definition
        $type = $element->getAttribute('type');
        
        if($type){
            $type = str_replace("ns:","", $type);
            
            $break = false;
            $arrTags = array('' . self::NS_PREFFIX . 'simpleType','' . self::NS_PREFFIX . 'complexType');            
            foreach($arrTags as $tag){
                $query = $tag . '[@name="' . $type . '"]';  
                foreach($xpath as $key=>$x){ 
                    $result = $x->query($query);
                    if($result->length > 0){
                        foreach ($result as $node){
                            $typeStructure = $elementDom->importNode($node->cloneNode(true),true);
                            $typeStructure->removeAttribute('name');
                            $element->appendChild($typeStructure);
                            $element->removeAttribute('type');
                            $break = true;
        
                            break;
                        }
                    }
                }
                if($break)
                    break;
            }
        }
        
        //get current attributes and save them for later
        $attributesList = $element->getElementsByTagName('attribute');
        $attributes = array();
        foreach($attributesList as $attribute){
            $attributes[] = $attribute->cloneNode(true);
        }
        
        $extension = $element->getElementsByTagName('extension')->length > 0 ? 
                     $element->getElementsByTagName('extension')->item(0) : 
                    false;
        
        //If element has en extension, its data is obtained and attributes are added
        if($extension && $extension->getAttribute('base') != null){
            $break = false;
            $arrTags = array('' . self::NS_PREFFIX . 'simpleType','' . self::NS_PREFFIX . 'complexType');            
            foreach($arrTags as $tag){
                $query = $tag . '[@name="' . str_replace("ns:","",$extension->getAttribute('base')) . '"]';        
                foreach($xpath as $x){
                    $result = $x->query($query);
                    if($result->length > 0){
                        foreach ($result as $node){
                            $extensionStructure = $elementDom->importNode($node->cloneNode(true),true);
                            $extensionStructure->removeAttribute('name');
                            
                            /*****************************************************************************************
                             * if base element is an extension, its attributes are appended and the "base" is updated
                             *****************************************************************************************/
                            if($extensionStructure->getElementsByTagName("extension")->length > 0){ 
                                $new_extension = $extensionStructure->getElementsByTagName("extension")->item(0);
                                $cur_extension = $element->getElementsByTagName("extension")->item(0);
                                $cur_extension->setAttribute("base", $new_extension->getAttribute("base"));
                                foreach($new_extension->getElementsByTagName("attribute") as $attr){
                                    $cur_extension->appendChild($attr);
                                }
                                
                            }
                            /*****************************************************************************************
                             * if base element is restriction, and its extension is complextype, then restriciont 
                             * is included in simple content
                             *****************************************************************************************/
                            elseif($extensionStructure->getElementsByTagName("restriction")->length > 0 &&
                                   $element->getElementsByTagName("extension")->length > 0){ 
                                $new_restriction = $extensionStructure->getElementsByTagName("restriction")->item(0);
                                $cur_extension = $element->getElementsByTagName("extension")->item(0);
                                foreach($cur_extension->getElementsByTagName("attribute") as $attr){
                                    $new_restriction->appendChild($attr);
                                }
                                $cur_extension->parentNode->replaceChild($new_restriction, $cur_extension);
                                
                                
                            }else{
                                //base restriction
                                $restrictionToAdd = $extensionStructure->getElementsByTagName('restriction');
                                if($restrictionToAdd->length > 0){ 
                                    //add respective attributes
                                    foreach($attributes as $attribute){
                                        $restrictionToAdd->item(0)->appendChild($attribute);
                                    }
                                    //remove Extension node
                                    $extensionToRemove = $element->getElementsByTagName('extension')->item(0);
                                    $parentNode = $extensionToRemove->parentNode;
                                    $parentNode->removeChild($extensionToRemove);
                                    //add restriction referenced
                                    $parentNode->appendChild($restrictionToAdd->item(0));
                                }
                                $element->appendChild($extensionStructure);
                            }
                            
                            $break = true;
                            break;
                        }
                    }
                }
                if($break)
                    break;
            }
        }
        
        
    }
    
}

?>
