<?php   if ( !file_exists(dirname(__FILE__).'/bulkMessage.php')) exit('No direct script access allowed');
        if ( class_exists('bulkMessage') == FALSE ) require dirname(__FILE__).'/bulkMessage.php';
    
        class downloadFile extends bulkMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'fileReferenceId'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'taskReferenceId'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
                
                protected function getHeaders() {	
                        $headers                                    = LoadSession::buildEbayHeaders();
                        $contentType                                = 'text/xml';
                        array_push($headers, 'Content-Type: ' . $contentType)	;
                        array_push($headers, 'X-EBAY-SOA-SERVICE-NAME: FileTransferService');
                        return $headers;
                }
        }

