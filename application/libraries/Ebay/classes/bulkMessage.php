<?php   if ( !file_exists(dirname(__FILE__).'/LoadSession.php')) exit('No direct script access allowed');
        if ( class_exists('LoadSession') == FALSE ) require dirname(__FILE__).'/LoadSession.php';
        
        class bulkMessage extends LoadSession {
                public $requestXmlBody      = '';
                public $requestSubXmlBody   = '';
                public static $_elements = array();

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function buildSubMessage($request = '', $root_name = 'main') {
                        if ( is_array($request) ) :
                                foreach ( self::$_elements[$root_name] as $name => $field ) :
                                        if ( array_key_exists($name, $request) || $field['required'] ) :
                                                if ( $field['array'] ) :
                                                        if ( $field['type'] == 'string' ) :
                                                                if ( is_array($request[$name]) ) :
                                                                        foreach ( $request[$name] as $tes ) :
                                                                                foreach ( $tes as $value ) :
                                                                                        $this->requestSubXmlBody    .= "<".$name.">".$value."</".$name.">";
                                                                                endforeach;
                                                                        endforeach;
                                                                else :
                                                                        $this->requestSubXmlBody    .= "<".$name.">".$request[$name]."</".$name.">";
                                                                endif;
                                                        elseif ( array_key_exists('_Repeat', $request[$name]) ) :
                                                                foreach ( $request[$name]['_Repeat'] as $repeat ) :
                                                                        if ( is_array($repeat) ) :
                                                                                $this->requestSubXmlBody    .= '<'.$name.'>';
                                                                                $this->buildSubMessage($repeat, $name);
                                                                                $this->requestSubXmlBody    .= '</'.$name.'>';
                                                                        else :
                                                                                $this->requestSubXmlBody    .= "<".$name.">".$repeat."</".$name.">";
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                                $this->requestSubXmlBody    .= '<'.$name.'>';
                                                                $this->buildSubMessage($request[$name], $name);
                                                                $this->requestSubXmlBody    .= '</'.$name.'>';
                                                        endif;
                                                else :
                                                        $this->requestSubXmlBody    .= "<".$name.">".$request[$name]."</".$name.">";
                                                endif;
                                        endif;
                                endforeach;
                        endif;
                        return $this->requestSubXmlBody;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function buildMessages($request = '') {
                        $this->requestXmlBody               .= '<?xml version="1.0" encoding="utf-8"?>';
                        $this->requestXmlBody               .= '<'.$this->method.'Request xmlns:sct="http://www.ebay.com/soaframework/common/types" xmlns="http://www.ebay.com/marketplace/services">';
                        $this->requestXmlBody               .= $this->buildSubMessage($request, 'main');
                        $this->requestXmlBody               .= '</'.$this->method.'Request>';
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($session = '') {
                        parent::__construct($session);
                }
        }

