<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class ConfirmIdentity extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        parent::$_elements['main']          = array(
                                'SessionID'                 => array(
                                        'required'          => false,
                                        'type'              => 'string',
                                        'array'             => false,
                                ),
                        );
                }
        }

