<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class ReviseItem extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['PictureDetails']        = array(
                                'GalleryType'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'GalleryURL'                        => array(
					'required'                  => false,
					'type'                      => 'uri',
					'array'                     => false,
				),
                                'PictureURL'                        => array(
					'required'                  => false,
					'type'                      => 'uri',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ItemSpecifics']         = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['NameValueList']         = array(
                                'Name'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Value'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Item']                  = array(
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Title'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'Description'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'ItemSpecifics'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'PictureDetails'                    => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'PaymentMethods'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'PayPalEmailAddress'                => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'Item'                              => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

