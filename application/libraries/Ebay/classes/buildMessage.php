<?php   if ( !file_exists(dirname(__FILE__).'/GetSession.php')) exit('No direct script access allowed');
        if ( class_exists('GetSession') == FALSE ) require dirname(__FILE__).'/GetSession.php';

        class buildMessage extends GetSession {
                public $requestXmlBody      = '';
                public $requestSubXmlBody   = '';
                public static $_elements = array();

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function buildSubMessage($request = '', $root_name = 'main', $t = "\t") {
                        $t                          = "\t".$t;
                        if ( is_array($request) ) :
                                foreach ( self::$_elements[$root_name] as $name => $field ) :
                                        if ( array_key_exists($name, $request) || $field['required'] ) :
                                                if ( $field['array'] ) :
                                                        if ( $field['type'] == 'string' ) :
                                                                if ( is_array($request[$name]) ) :
                                                                        foreach ( $request[$name] as $tes ) :
                                                                                foreach ( $tes as $value ) :
                                                                                        $this->requestSubXmlBody    .= $t."<".$name.">".(($field['type'] == 'string') ? "<![CDATA[" : '').$value.(($field['type'] == 'string') ? "]]>" : '')."</".$name.">"."\r\n";
                                                                                endforeach;
                                                                        endforeach;
                                                                else :
                                                                        $this->requestSubXmlBody    .= $t."<".$name.">".(($field['type'] == 'string') ? "<![CDATA[" : '').$request[$name].(($field['type'] == 'string') ? "]]>" : '')."</".$name.">"."\r\n";
                                                                endif;
                                                        elseif ( array_key_exists('_Repeat', $request[$name]) ) :
                                                                foreach ( $request[$name]['_Repeat'] as $repeat ) :
                                                                        if ( is_array($repeat) ) :
                                                                                $this->requestSubXmlBody    .= $t.'<'.$name.'>'."\r\n";
                                                                                $this->buildSubMessage($repeat, $name, $t);
                                                                                $this->requestSubXmlBody    .= $t.'</'.$name.'>'."\r\n";
                                                                        else :
                                                                                $this->requestSubXmlBody    .= $t."<".$name.">".(($field['type'] == 'string') ? "<![CDATA[" : '').$repeat.(($field['type'] == 'string') ? "]]>" : '')."</".$name.">"."\r\n";
                                                                        endif;
                                                                endforeach;
                                                        else :
                                                                $this->requestSubXmlBody    .= $t.'<'.$name.'>'."\r\n";
                                                                $this->buildSubMessage($request[$name], $name, $t);
                                                                $this->requestSubXmlBody    .= $t.'</'.$name.'>'."\r\n";
                                                        endif;
                                                else :
                                                        $this->requestSubXmlBody    .= $t."<".$name.">".(($field['type'] == 'string') ? "<![CDATA[" : '').$request[$name].(($field['type'] == 'string') ? "]]>" : '')."</".$name.">"."\r\n";
                                                endif;
                                        endif;
                                endforeach;
                        endif;
                        return $this->requestSubXmlBody;
                }

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                function buildMessages($request = '') {
                        $this->requestXmlBody               .= '<?xml version="1.0" encoding="utf-8"?>';
                        $this->requestXmlBody               .= '<'.$this->method.'Request xmlns="urn:ebay:apis:eBLBaseComponents">';
                        if ( !empty($this->token) ) 
                        $this->requestXmlBody               .= '<RequesterCredentials><eBayAuthToken>'.$this->token.'</eBayAuthToken></RequesterCredentials>';
                        $this->requestXmlBody               .= $this->buildSubMessage($request, 'main');
                        $this->requestXmlBody               .= '</'.$this->method.'Request>';
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                public function __construct($session = '') {
                        parent::__construct($session);
                }
        }

