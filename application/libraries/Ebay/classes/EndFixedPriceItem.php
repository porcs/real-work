<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class EndFixedPriceItem extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'EndingReason'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Version'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

