<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class SetUserPreferences extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['SellerPaymentPreferences']            = array(
                                'PayPalPreferred'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PayPalAlwaysOn'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'SellerPaymentPreferences'          => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                }
        }

