<?php   if ( !file_exists(dirname(__FILE__).'/BulkData.php')) exit('No direct script access allowed');
        if ( class_exists('BulkData') == FALSE ) require dirname(__FILE__).'/BulkData.php';
    
        class BulkDataExchange extends BulkData {
                public function __construct($session = '') {
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['PrimaryCategory']       = array(
                                'CategoryID'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ReturnPolicy']          = array(
                                'ReturnsAcceptedOption'             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'RefundOption'                      => array(
                                        'required'                  => false,
                                        'type'                      => 'string',
                                        'array'                     => false,
				),
                                'ReturnsWithinOption'               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'ShippingCostPaidByOption'          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'Description'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ShippingDetails']       = array(
                                'ShippingServiceOptions'            => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'InternationalShippingServiceOption' => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['InternationalShippingServiceOption'] = array(
                                'ShippingService'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShippingServiceAdditionalCost'     => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
				'ShippingServiceCost'               => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'ShippingServicePriority'           => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'ShipToLocation'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ShippingServiceOptions'] = array(
                                'ShippingService'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShippingServiceAdditionalCost'     => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
				'ShippingServiceCost'               => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'ShippingServicePriority'           => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'FreeShipping'                      => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ItemSpecifics']         = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['NameValueList']         = array(
                                'Name'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Value'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['PictureDetails']        = array(
                                'GalleryType'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'GalleryURL'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'PictureURL'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Variations']            = array(
                                'VariationSpecificsSet'             => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Variation'                         => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Pictures'                          => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecificsSet'] = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                                
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Variation']             = array(
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'Quantity'                          => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'VariationSpecifics'                => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'DiscountPriceInfo'                 => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecifics']    = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['DiscountPriceInfo']     = array(
                                'MadeForOutletComparisonPrice'      => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'MinimumAdvertisedPrice'            => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'MinimumAdvertisedPriceExposure'    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'OriginalRetailPrice'               => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'PricingTreatment'                  => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'SoldOneBay'                        => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Pictures']              = array(
                                'VariationSpecificName'             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'VariationSpecificPictureSet'       => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecificPictureSet'] = array(
                                'VariationSpecificValue'            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PictureURL'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'GalleryURL'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ProductListingDetails'] = array(
                                'UseStockPhotoURLAsGallery'         => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'IncludeStockPhotoURL'              => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'StockPhotoURL'              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Item']                  = array(
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Title'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Description'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PrimaryCategory'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'ConditionID'                       => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
				'ConditionDisplayName'              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'CategoryMappingAllowed'            => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'DispatchTimeMax'                   => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'ListingDuration'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Country'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Currency'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Location'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PostalCode'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Quantity'                          => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'PaymentMethods'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
				'PayPalEmailAddress'                => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'BuyItNowPrice'                     => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'InventoryTrackingMethod'           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ProductListingDetails'             => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'ItemSpecifics'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'PictureDetails'                    => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Site'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ReturnPolicy'                      => array(
                                        'required'                  => false,
                                        'type'                      => 'token',
                                        'array'                     => true,
                                ),
                                'ShippingDetails'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
				'InternationalShippingServiceOption' => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Variations'                        => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => false,
				),
                                'Version'                           => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => false,
				),
                                'MessageID'                         => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Item'                              => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'EndingReason'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        parent::__construct($session);
                }
        }

