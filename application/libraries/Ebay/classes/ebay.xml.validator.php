<?php
/**
 * This class evaluates XML generated against XSD file
 *
 * @author eturcios
 */
class EBayXmlValidator {
    public function validateRequest(DOMDocument $request){
        $envelope = EBayXSD::getMainDom();
        $envelopeXPath = new DOMXPath($envelope);
        $name = "BulkDataExchangeRequests";
        $result_xsd = $envelopeXPath->query('' . EBayXSD::NS_PREFFIX . 'element[@name="' . $name .'"]');
        if($result_xsd->length == 0){
            self::displayError($name . " element was not found on XSD");
        }
        $item_xsd = $result_xsd->item(0);
        self::replaceReferences($item_xsd, $envelope);
        EBayXSD::getElementStructure($item_xsd, $envelope);
        $result_xml = $request->getElementsByTagName($name);
        if($result_xml->length == 0){
            self::displayError($name . " element was not found on generated XML");
        }
        self::validateXMLDefinition($result_xml, $result_xsd);
    }
    
    private function replaceReferences(DOMElement &$reference, DOMDocument &$productDom){
            $xpath = new DOMxpath($productDom);
            //Replace referenced elements to their corresponding structure
            $query = './/' . EBayXSD::NS_PREFFIX . 'element[@ref]';
            $references = $xpath->query($query, $reference);
            if($references->length > 0){
                //Obtains structure from all referenced elements of product data
                foreach($references as $node){
                    if($node instanceof DOMElement){ 
                        $new_node = self::replaceReferencedElements($node, $productDom);
                        if($new_node== null)
                            continue;
                        $new = $productDom->importNode($new_node);
                        $node->parentNode->replaceChild($new, $node);
                        foreach($new->childNodes as $child){
                            if(!$child instanceof DOMElement)
                                continue;
                            self::replaceReferences($child, $productDom);
                        }
                    }
                }
            }
    }
    
    private function replaceReferencedElements(DOMElement $element, DOMDocument &$productDom){
        return EBayXSD::getReferencedStructure($element, $productDom);
        
    }

    private function validateXMLDefinition(DOMNodeList $xml, DOMNodeList $xsd){
        
        foreach($xsd as $xsd_node){
            //DOMText might appear here, that's why only DOMElement is considered
            if(!$xsd_node instanceof DOMElement){
                continue;
            }
            
            EBayXSD::getElementStructure($xsd_node, $xsd_node->ownerDocument);
            
            $xpath = new DOMXPath($xsd_node->ownerDocument);
            $nodes = $xpath->query('' . EBayXSD::NS_PREFFIX . 'element[@type]');
            foreach($nodes as $ref){
                EBayXSD::getElementStructure($ref, $xsd_node->ownerDocument);
            }
            
            $xsd_name = $xsd_node->getAttribute("name");
            $xsd_tag = str_replace(EBayXSD::NS_PREFFIX, "", $xsd_node->tagName);
            
            //print_r ("<div>XSD: " . $xsd_tag . " => " . $xsd_name . "</div>".PHP_EOL);
            
            if($xsd_tag == "element") {
                
                $cur= 0;
                $min = $xsd_node->getAttribute("minOccurs") != "" ? $xsd_node->getAttribute("minOccurs") : 1;
                $max = $xsd_node->getAttribute("maxOccurs") != "" ? $xsd_node->getAttribute("maxOccurs") : 1;
                
                foreach($xml as $xml_node){ 
                    if(!$xml_node instanceof DOMElement){
                        continue;
                    }
                    if($xsd_name != $xml_node->tagName){
                        continue;
                    }
                    //print_r ("<div>XML: " . $xml_node->tagName . "</div>".PHP_EOL);
                    self::validateSingleElement($xml_node, $xsd_node);
                    $cur += 1;
                }
                //********************************************************************//
                //                      Validate Min/Max Occurs                       //
                //********************************************************************//
                if($cur < $min){
                    self::displayError("Element &lt;" . $xsd_name . "&gt; must exists at least: " . $min. ". And currently " . $cur . " were found");
                }
                if($max != "unbounded" && $cur > $max){
                    self::displayError("Element &lt;" . $xsd_name . "&gt; must exists at most: " . $min. ". And currently " . $cur . " were found");
                }
                
            }elseif($xsd_tag == "choice"){ 
                $xpath = new DOMXPath($xsd_node->ownerDocument);
                
                $choices = $xpath->query('' . EBayXSD::NS_PREFFIX . 'element', $xsd_node);
                $min = $xsd_node->getAttribute("minOccurs");
                $chosen = false;
                $values = "";
                
                foreach($xml as $xml_node){ 
                    if(!$xml_node instanceof DOMElement){
                        continue;
                    }
                    foreach($choices as $choice){ 
                        if(!$choice instanceof DOMElement){
                            continue;
                        }
                        $choice_name = $choice->getAttribute("name");
                        if($values){
                            $values .= ", " . $choice_name;
                        }else{
                            $values .= $choice_name;
                        }
                        
                        if($xml_node->tagName == $choice_name){ 
                            $chosen = true;
                            if($choice->getAttribute("type")){
                                EBayXSD::getElementStructure($choice, $choice->ownerDocument);
                            }
                            self::validateXMLDefinition($xml_node->childNodes, $choice->childNodes);
                            break;
                        }
                    }
                    if($chosen)
                        break;
                }
                if($min == "" && !$chosen){
                    self::displayError('One of these values: "'. $values . '" must be set');
                }
                
            }else{
                
                self::validateXMLDefinition($xml, $xsd_node->childNodes);
            }
        }
    }
    
    /**
     * Validate an XML Element against its XSD
     * @param DOMElement $xml_node
     * @param DOMElement $xsd_node
     */
    private function validateSingleElement(DOMElement $xml_node, DOMElement $xsd_node){
            $value = $xml_node->nodeValue;
            $xml_tag = $xml_node->tagName;
            $xsd_xpath = new DOMXPath($xsd_node->ownerDocument); 
            $type = $xsd_node->getAttribute("type");
            
            if(!stripos($type, EBayXSD::NS_PREFFIX ) && $xsd_xpath->query('.//' . EBayXSD::NS_PREFFIX . 'element',$xsd_node)->length == 0){
                EBayXSD::getElementStructure($xsd_node, $xsd_node->ownerDocument);
            }
            
            if($xsd_node->getElementsByTagName("element")->length > 0){
                //********************************************************************//
                //           Validate Children Elements (recursive)                   //
                //********************************************************************//
                foreach($xsd_node->childNodes as $xsd_child){
                    if(!$xsd_child instanceof DOMElement){
                        continue;
                    }
                    self::validateXMLDefinition($xml_node->childNodes, $xsd_child->childNodes);
                }
            }else{ 
                //********************************************************************//
                //                  Verify if exists a restriction                    //
                //********************************************************************//
                $xpath = new DOMXPath($xsd_node->ownerDocument);
                $result1 = $xpath->query('.//' . EBayXSD::NS_PREFFIX . 'simpleType/' . EBayXSD::NS_PREFFIX . 'restriction',$xsd_node);
                $result2 = $xpath->query('.//' . EBayXSD::NS_PREFFIX . 'simpleContent/' . EBayXSD::NS_PREFFIX . 'restriction',$xsd_node);
                if($result1->length > 0){
                    $restriction = $result1->item(0);
                    //********************************************************************//
                    //                           Check XSD Base Type                      //
                    //********************************************************************//
                    self::validateXSDBase($value, $restriction, $xml_tag);
                }elseif($result2->length > 0){
                    $restriction = $result2->item(0);
                    //********************************************************************//
                    //                           Check XSD Base Type                      //
                    //********************************************************************//
                    self::validateXSDBase($value, $restriction, $xml_tag);
                }else{
                    //********************************************************************//
                    //                           Check XSD Data Type                      //
                    //********************************************************************//
                    self::validateXSDType($xml_node, $xsd_node);
                }
                
            }
    }
    
    private function validateXSDBase($value, DOMElement $restriction, $xml_tag){
        $base= str_replace("ns:","",$restriction->getAttribute("base"));
        
        self::validateXMLValue($xml_tag, $value, $base);
        //********************************************************************//
        //                             Check Pattern                          //
        //********************************************************************//
        $patterns = $restriction->getElementsByTagName("pattern");
        foreach($patterns as $pattern_result){
            $validation = $pattern_result->getAttribute("value");
            if(!preg_match("/" . $validation . "/", $value)){
                self::displayError("Value for " . $xml_tag . " is non valid (according to its pattern)");
            }

        }
        //********************************************************************//
        //                             Check Enumeration                      //
        //********************************************************************//
        $enumerations = $restriction->getElementsByTagName("enumeration");
        $enumeration_ok = false;
        foreach($enumerations as $enumeration_result){
            $validation = $enumeration_result->getAttribute("value");
            if($value == $validation){
                $enumeration_ok = true;
                break;
            }
        }
        if($enumerations->length > 0 && !$enumeration_ok){
            self::displayError('"' . $value . '" is not a valid value set for: '. $xml_tag);
        }
        //********************************************************************//
        //                             Check Min Length                       //
        //********************************************************************//
        $minLength = $restriction->getElementsByTagName("minLength");
        if($minLength->length > 0){
            $length = $minLength->item(0)->getAttribute("value");
            if(strlen($value) < $length){
                self::displayError('"' . $value . '" length must be greater or equal to: '. $length . ' for the element: ' . $xml_tag);
            }
        }
        //********************************************************************//
        //                             Check Max Length                       //
        //********************************************************************//
        $maxLength = $restriction->getElementsByTagName("maxLength");
        if($maxLength->length > 0){
            $length = $maxLength->item(0)->getAttribute("value");
            if(strlen($value) > $length){
                self::displayError('"' . $value . '" length must be less or equal to: '. $length . ' for the element: ' . $xml_tag);
            }
        }
        //********************************************************************//
        //                           Check Total DIgits                       //
        //********************************************************************//
        $totalDigits = $restriction->getElementsByTagName("totalDigits");
        if($totalDigits->length > 0){ 
            $length = $totalDigits->item(0)->getAttribute("value");
            $cur_digits = preg_match_all( "/[0-9]/", $value, $dummy_array);
            
            if($cur_digits > $length){
                self::displayError('"' . $value . '" is invalid, digits must be less or equal to: '. $length . ' for the element: ' . $xml_tag);
            }
        }
        //********************************************************************//
        //                           Check Fraction DIgits                       //
        //********************************************************************//
        $fractionDigits = $restriction->getElementsByTagName("fractionDigits");
        if($fractionDigits->length > 0){
            $length = $fractionDigits->item(0)->getAttribute("value");
            $cur_fraction_digits = preg_match_all( "/\.[0-9]/", $value, $dummy_array);
            
            if($cur_fraction_digits > $length){
                self::displayError('"' . $value . '" is invalid, fraction digits must be less or equal to: '. $length . ' for the element: ' . $xml_tag);
            }
        }
    }
                
    /**
     * Validate basic XSD type
     * @param DOMElement $xml_node
     * @param DOMElement $xsd_node
     */
    private function validateXSDType(DOMElement $xml_node, DOMElement $xsd_node){
        $type = $xsd_node->getAttribute("type");
        $tag = $xml_node->tagName;
        $value = $xml_node->nodeValue;
        if($type){
            self::validateXMLValue($tag, $value, $type);
        }
    }
    
    private function validateXMLValue($tag, $value, $type){
        
        $type = str_replace(EBayXSD::NS_PREFFIX, "", $type);
        
        if($type){
            switch($type){
                case "boolean":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (true/false). Currently an object is set.");
                    }elseif($value != "false" && $value != "true" && $value != "1" && $value != "0"){
                        self::displayError("Element: " . $tag . " must have a boolean value (true/false). Current value: " . $value);
                    }
                    break;
                case "negativeInteger":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (..,-2,-1). Currently an object is set.");
                    }elseif($value != "" && intval($value) >= 0){
                        self::displayError("Element: " . $tag . " value must be a valid Negative Integer (..,-2,-1). Current value: " . $value);
                    }
                    break;
                case "nonNegativeInteger":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (0,1,2,..). Currently an object is set.");
                    }elseif($value != "" && intval($value) < 0){
                        self::displayError("Element: " . $tag . " value must be a Non Negative Integer (0,1,2,..). Current value: " . $value);
                    }
                    break;
                case "nonPositiveInteger":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (..,-2,-1,0). Currently an object is set.");
                    }elseif($value != "" && intval($value) > 0){
                        self::displayError("Element: " . $tag . " value must be a valid Non Positive Integer (..,-2,-1,0). Current value: " . $value);
                    }
                    break;
                case "positiveInteger":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (1,2,..). Currently an object is set.");
                    }elseif($value != "" && intval($value) <= 0){
                        self::displayError("Element: " . $tag . " value must be a valid Positive Integer (1,2,..). Current value: " . $value);
                    }
                    break;
                case "decimal":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value (..,-2,-1). Currently an object is set.");
                    }elseif($value != "" && !is_numeric($value)){
                        self::displayError("Element: " . $tag . " value must be a valid number. Current value: " . $value);
                    }
                    break;
                case "int":
                case "integer":
                    if(is_object($value)){
                        self::displayError("Element: " . $tag . " must have a " . $type . " value. Currently an object is set.");
                    }elseif($value != "" && !ctype_digit($value)){
                        self::displayError("Element: " . $tag . " value must be a valid Integer. Current value: " . $value."");
                    }
                    break;

            }
        } 
    }
    
    private function displayError($msg){
            $this->_xmlError = $msg;
    }
    
}
