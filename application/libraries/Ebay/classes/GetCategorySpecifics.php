<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
        
        class GetCategorySpecifics extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['CategorySpecific']          = array(
                                'CategoryID'                            => array(
                                        'required'                      => false,
                                        'type'                          => 'string',
                                        'array'                         => false,
                                ),
                        );
                        
                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                      = array(
                                'CategorySpecific'                      => array(
                                        'required'                      => false,
                                        'type'                          => 'token',
                                        'array'                         => true,
                                ),
                                'CategoryID'                            => array(
                                        'required'                      => false,
                                        'type'                          => 'string',
                                        'array'                         => true,
                                ),
                                'CategorySpecificsFileInfo'             => array(
                                        'required'                      => false,
                                        'type'                          => 'boolean',
                                        'array'                         => false,
                                ),
                        );
                }
        }

