<?php
    class GetSession {
        public $compatability;
        public $devID;
        public $appID;
        public $certID;
        public $token;
        public $runame;
        public $serverUrl;
        public $method;
        public $siteID;
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public function __construct($session_array = '') {
                $this->compatability                        = isset($session_array['compatability']) ? $session_array['compatability'] : '';
                $this->devID                                = isset($session_array['devID']) ? $session_array['devID'] : '';
                $this->appID                                = isset($session_array['appID']) ? $session_array['appID'] : '';
                $this->certID                               = isset($session_array['certID']) ? $session_array['certID'] : '';
                $this->token                                = isset($session_array['token']) ? $session_array['token'] : '';
                $this->runame                               = isset($session_array['runame']) ? $session_array['runame'] : '';
                $this->serverUrl                            = isset($session_array['serverUrl']) ? $session_array['serverUrl'] : '';
                $this->method                               = isset($session_array['method']) ? $session_array['method'] : '';
                $this->siteID                               = isset($session_array['siteID']) ? $session_array['siteID'] : '';
        }
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public function sendHttpRequest($requestBody) {
		$headers                                    = $this->buildEbayHeaders();
		$connection                                 = curl_init();
                curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($connection, CURLOPT_URL, $this->serverUrl);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $requestBody);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		$response                                   = curl_exec($connection);
		curl_close($connection);
		return $response;
	}
	
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private function buildEbayHeaders() {
		$headers = array (
                        'X-EBAY-API-COMPATIBILITY-LEVEL: '  . $this->compatability,
			'X-EBAY-API-DEV-NAME: '             . $this->devID,
			'X-EBAY-API-APP-NAME: '             . $this->appID,
			'X-EBAY-API-CERT-NAME: '            . $this->certID,
			'X-EBAY-API-CALL-NAME: '            . $this->method,			
			'X-EBAY-API-SITEID: '               . $this->siteID,
		);
		return $headers;
	}
    }
