<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class RelistFixedPriceItem extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['PrimaryCategory']       = array(
                                'CategoryID'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ReturnPolicy']          = array(
                                'ReturnsAcceptedOption'             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'RefundOption'                      => array(
                                        'required'                  => false,
                                        'type'                      => 'string',
                                        'array'                     => false,
				),
                                'ReturnsWithinOption'               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'ShippingCostPaidByOption'          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ShippingDetails']       = array(
                                'ShippingServiceOptions'            => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['InternationalShippingServiceOption'] = array(
                                'ShippingServiceOptions'            => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ShippingServiceOptions'] = array(
                                'ShippingType'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShippingService'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShippingServiceAdditionalCost'     => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
				'ShippingServiceCost'               => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'ShippingServicePriority'           => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'ShippingTermsInDescription'        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ShipToLocation'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'FreeShipping'                      => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ItemSpecifics']         = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['NameValueList']         = array(
                                'Name'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Value'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['PictureDetails']        = array(
                                'GalleryType'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'GalleryURL'                        => array(
					'required'                  => false,
					'type'                      => 'uri',
					'array'                     => false,
				),
                                'PictureURL'                        => array(
					'required'                  => false,
					'type'                      => 'uri',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Variations']            = array(
                                'VariationSpecificsSet'             => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Variation'                         => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Pictures'                          => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'ModifyNameList'                    => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                            
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecificsSet'] = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                                
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Variation']             = array(
                                'Delete'                            => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Quantity'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'VariationSpecifics'                => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecifics']    = array(
                                'NameValueList'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ModifyNameList']        = array(
                                'ModifyName'                        => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ModifyName']            = array(
                                'Name'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'NewName'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Pictures']              = array(
                                'VariationSpecificName'             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'VariationSpecificPictureSet'       => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['VariationSpecificPictureSet'] = array(
                                'VariationSpecificValue'            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PictureURL'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['Item']                  = array(
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Title'                             => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Description'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PrimaryCategory'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'ConditionID'                       => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
				'ConditionDisplayName'              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'StartPrice'                        => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'CategoryMappingAllowed'            => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'DispatchTimeMax'                   => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'ListingDuration'                   => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Country'                           => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Currency'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Location'                          => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'PostalCode'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Quantity'                          => array(
					'required'                  => false,
					'type'                      => 'int',
					'array'                     => false,
				),
                                'PaymentMethods'                    => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
				'PayPalEmailAddress'                => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ReturnPolicy'                      => array(
                                        'required'                  => false,
                                        'type'                      => 'token',
                                        'array'                     => true,
                                ),
                                'ShippingDetails'                   => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
				'InternationalShippingServiceOption' => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'BuyItNowPrice'                     => array(
					'required'                  => false,
					'type'                      => 'amount',
					'array'                     => false,
				),
                                'ItemSpecifics'                     => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'PictureDetails'                    => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'Site'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'Variations'                        => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                        );
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'Item'                              => array(
					'required'                  => false,
					'type'                      => 'token',
					'array'                     => true,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

