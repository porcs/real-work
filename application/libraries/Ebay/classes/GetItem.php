<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetItem extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['ActiveList']            = array(
                                'Sort'                              => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );

                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'ItemID'                            => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'SKU'                               => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'TransactionID'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'VariationSKU'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => true,
				),
                                'OutputSelector'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

