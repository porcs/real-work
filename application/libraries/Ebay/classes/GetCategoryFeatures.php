<?php   if ( !file_exists(dirname(__FILE__).'/buildMessage.php')) exit('No direct script access allowed');
        if ( class_exists('buildMessage') == FALSE ) require dirname(__FILE__).'/buildMessage.php';
    
        class GetCategoryFeatures extends buildMessage {
                public function __construct($session = '') {
                        parent::__construct($session);
                        
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        parent::$_elements['main']                  = array(
                                'CategoryID'                        => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'FeatureID'                         => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ViewAllNodes'                      => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'AllFeaturesForCategory'            => array(
					'required'                  => false,
					'type'                      => 'boolean',
					'array'                     => false,
				),
                                'DetailLevel'                       => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                                'ErrorLanguage'                     => array(
					'required'                  => false,
					'type'                      => 'string',
					'array'                     => false,
				),
                        );
                }
        }

