<?php

require_once(dirname(__FILE__)."/../models/feedModel.php");

class errorResolutions extends feedModel
{
    public function __construct($project, $data, $debug = false)
    {
        parent::__construct($project, $data, $debug);
        
        $this->data =& $data;
    }




    ////////////////////////////////////////////////////////////////////////////
    public function getResult($return = array(), $count  = 0, $batch_id = '') 
    {
        if ( !empty($this->data['post'])) {
            $lang = $this->data['langstore'];
            
            switch($this->data['post']['case'])
            {
                case 'Index' :
                    switch($this->data['post']['expect']['code']) 
                    {
                        case 'FB0001':
                            $this->data['column'] = ""
                                . "`sku_export` AS reference, "
                                . "`id_product`, "
                                . "`name_product`, "
                                . "`message`";
                            
                            $fields = array(
                                array(
                                    'id' => 'id_product',
                                    'text' => $lang['Product ID'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'name_product',
                                    'text' => $lang['Product name'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'message',
                                    'text' => $lang['Messages'],
                                    'style' => 'text-left'
                                )
                            );
                            break;
                    
                        case 'FB0016':
                            $this->data['column'] = ""
                                . "`sku_export` AS reference, "
                                . "`id_product`, "
                                . "`id_combination`, "
                                . "`sku_export` AS sku, "
                                . "`name_product`, "
                                . "`message`";
                            
                            $fields = array(
                                array(
                                    'id' => 'sku',
                                    'text' => $lang['SKU'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'id_combination',
                                    'text' => $lang['Combination ID'],
                                    'style' => 'text-center'
                                ),
                                array(
                                    'id' => 'name_product',
                                    'text' => $lang['Product name'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'message',
                                    'text' => $lang['Messages'],
                                    'style' => 'text-left'
                                )
                            );
                            break;
                    
                        case 'FB0002':
                        case 'FB0003':
                        case 'FB0004':
                        case 'FB0005':
                        case 'FB0006':
                        case 'FB0007':
                        case 'FB0008':
                        case 'FB0009':
                        case 'FB0010':
                        case 'FB0011':
                        case 'FB0012':
                        case 'FB0013':
                        case 'FB0014':
                        case 'FB0015':
                        case 'FB0017':
                        case 'FB0018':
                        case 'FB0019':
                        case 'FB0020':
                        case 'FB0021':
                        case 'FB0022':
                            $this->data['column'] = ""
                                . "`sku_export` AS reference, "
                                . "`id_product`, "
                                . "`sku_export` AS sku, "
                                . "`name_product`, "
                                . "`message`";
                            
                            $fields = array(
                                array(
                                    'id' => 'sku',
                                    'text' => $lang['SKU'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'name_product',
                                    'text' => $lang['Product name'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'message',
                                    'text' => $lang['Messages'],
                                    'style' => 'text-left'
                                )
                            );
                            break;
                    
                        default:
                            $this->data['column'] = ""
                                . "`code` AS id, "
                                . "`code`, "
                                . "`message`, "
                                . "count(`code`) AS total";
                            
                            $fields = array(
                                array(
                                    'id' => 'code',
                                    'text' => $lang['Code'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'message',
                                    'text' => $lang['Messages'],
                                    'style' => 'text-left'
                                ),
                                array(
                                    'id' => 'total',
                                    'text' => $lang['Total'],
                                    'style' => 'text-left'
                                )
                            );
                            break;
                    }
                    
                    $result = $this->getTasks();
                    return array(
                        'data' => $result['ebay_statistics_log'], 
                        'count' => $result['count'], 
                        'fields' => $fields
                    );
                
                default :
                    $result = $this->getTasks();
                    return array(
                        'data' => $result['ebay_statistics_log'], 
                        'count' => $result['count'], 
                        'batch_id' => $result['get_batchID']['batch_id']
                    );
            }
        }
        
        return array('data' => $return, 'count' => $count, 'batch_id' => $batch_id);
    }



    ////////////////////////////////////////////////////////////////////////////
    public function get() {
        return $this->getResult();
    }




    ////////////////////////////////////////////////////////////////////////////
    public function set($config = array())
    {   
        return $this->setTasks();
    }
}