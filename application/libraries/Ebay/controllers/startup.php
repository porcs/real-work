<?php

require_once(dirname(__FILE__)."/../models/feedModel.php");

class startup extends feedModel
{
    use patchModel;
    
    public function __construct($project, $data, $debug = false)
    {
        parent::__construct($project, $data, $debug);
    }
}
