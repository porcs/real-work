<?php 

class Configuration {
    public static $path = array(
        'libraries'         => "/application/libraries/Ebay/",
        'lb_controller'     => "/application/libraries/Ebay/controllers/",
        'tasks'             => "/application/libraries/Ebay/models/bin/",
        'services'          => "/application/libraries/Ebay/models/conf/",
    );
    
    public static $init_method = array(
        'debugMode',
        'getSessions',
        'getPackages',
        'getEnvironment',
        'getCountry',
        'getSite',
        'getFlag',
        'getPages',
        'getDocument',
        'getDefaultLang',
        'getLink',
        'setHeader',
        'checkLogin',
        'checkShop',
        'checkTable',
        'checkPackages',
        'checkSites',
        'initialJsonData',
        'assignValue'
    );

    ////////////////////////////////////////////////////////////////////////////
    public static $model_definition = array(
        'getConfiguration'          => array('user_id'),
        'geteBayConfiguration'      => array('user_id', 'id_site'),
        'getSiteFromPackages'       => array('id_marketplace', 'id_packages'),
        'getPackagesFromSite'       => array('id_marketplace', 'id_site'),
        'getDataPackagesFromSite'   => array('id_marketplace', 'user_id', 'id_site'),
        'getCountryFromPackages'    => array('id_packages'),
    );

    ////////////////////////////////////////////////////////////////////////////
    public static $model_definition_type = array(
        'user_id'                   => 'int',
        'id_site'                   => 'int',
        'id_marketplace'            => 'int',
        'id_packages'               => 'int',
    );
    
    public static $name_definitions = array(
        'user_id'       => 'id',
        'user_name'     => 'user_name',
        'id_shop'       => 'id_shop',
        'shop_default'  => 'shop_default',
        'language'      => 'language',
        'remote'        => 'remote',
        'currency'      => 'user_currency',
        'dir_server'    => 'dir_server',
        'base_url'      => 'base_url',
        'cdn_url'       => 'base_url',
        'base_link'     => 'base_url',
        'cdn_link'      => 'base_url',
        'id_region'     => 'id_region',
        'region'        => 'region',
        'ext'           => 'ext',
        'domain'        => 'domain',
        'id_site'       => 'id_site_ebay',
        'country'       => 'country',
        'packages'      => 'packages',
    );
    
    
    public static $usertest_definitions = array(
        'id'            => '342',
        'user_name'     => 'u00000000000342',
        'user_email'    => 'common-service3@hotmail.com',
        'shop_default'  => 'Eclairage Design',
        'language'      => 'english',
        'user_currency' => 'EUR',
        'id_shop'       => '1',
    );
    
//    public static $header_definitions = function()
//    {
//        
//    };
            
    
    public static $init_configuration = array(
        'EBAY_DEVID'            => 'devID',
        'EBAY_USER'             => 'userID',
        'EBAY_APPID'            => 'appID',
        'EBAY_CERTID'           => 'certID',
        'EBAY_TOKEN'            => 'token',
        'EBAY_APPMODE'          => 'appMode',
        'EBAY_SITE_ID'          => 'site',
        'EBAY_CURRENCY'         => 'currency',
        'EBAY_DISPATCH_TIME'    => 'dispatch_time',
        'EBAY_LI_DURATION'      => 'listing_duration',
        'EBAY_POSTAL_CODE'      => 'postal_code',
        'EBAY_RE_DAYS'          => 'returns_within',
        'EBAY_RE_INFORMATION'   => 'returns_information',
        'EBAY_RE_PAYS'          => 'returns_pays',
        'EBAY_RE_POLICY'        => 'returns_policy',
        'PAYER_EMAIL'           => 'paypal_email',
        'EBAY_ADDRESS'          => 'address',
        'FEED_MODE'             => 'feed_mode',
        'EBAY_CATEGORY'         => 'category_json',
        'EBAY_PAYMENT'          => 'payment_method',
        'API_TEMPLATE'          => 'template',
        'EBAY_NO_IMAGE'         => 'no_image',
        'EBAY_DISCOUNT'         => 'discount',
        'EBAY_SYNCHRONIZE'      => 'sync',
        'EBAY_JUST_ORDERS'      => 'just_orders',
        'EBAY_WIZARD'           => 'wizard',
        'EBAY_WIZARD_FINISH'    => 'wizard_finish',
        'EBAY_WIZARD_START'     => 'wizard_start',
        'EBAY_WIZARD_SUCCESS'   => 'wizard_success',
        'EBAY_PROFILE_RULES'    => 'profile_rules',
    );
    
    public static $sql_templates = array(
        'select'      => 'SELECT column from where groupby orderby limit offset',
        'insert'      => 'INSERT INTO table (column) values',
        'update'      => 'UPDATE table set where',
        'delete'      => 'DELETE from where',
        'replace'     => 'REPLACE INTO table (column) values',
    );
    
    public static $tb_def = array(
        "ebay_tax",
        "ebay_mapping_category",
        "ebay_mapping_secondary_category",
        "ebay_mapping_store_category",
        "ebay_mapping_secondary_store_category",
        "ebay_mapping_univers",
        "ebay_mapping_templates",
        "ebay_mapping_carrier_domestic",
        "ebay_mapping_carrier_international",
        "ebay_mapping_carrier_country_selected",
        "ebay_mapping_carrier_default",
        "ebay_mapping_attribute_selected",
        "ebay_mapping_attribute_value",
        "ebay_mapping_carrier_rules",
        "ebay_mapping_condition",
        "ebay_mapping_condition_description",
        "ebay_mapping_specific_selected",
        "ebay_mapping_specific_value",
        "ebay_store_category",
        "ebay_statistics_log",
        "ebay_statistics",
        "ebay_statistics_combination",
        "ebay_product_details",
        "ebay_product_item_id",
        "ebay_job_task",
        "ebay_price_modifier",
        "ebay_profiles_group",
        "ebay_profiles",
        "ebay_profiles_details",
        "ebay_profiles_payment",
        "ebay_profiles_return",
        "ebay_profiles_description",
        "ebay_profiles_condition",
        "ebay_profiles_shipping",
        "ebay_profiles_variation",
        "ebay_profiles_specific",
        "ebay_profiles_mapping",
        "ebay_attribute_univers",
        "ebay_synchronization_store",
        "ebay_synchronization_attribute",
        "ebay_attribute_override",
        "ebay_api_logs",
        "ebay_explode_product",
    );
    
    public static $prev_def = array(
        'auth_setting' => '%sebay/authentication/%d',
        'payment_method' => '%sebay/auth_setting/%d',
        'return_method' => '%sebay/payment_method/%d',
        'templates' => '%sebay/return_method/%d',
        'price_modifier' => '%sebay/templates/%d',
        'mapping/mapping_carriers' => '%sebay/price_modifier/%d',
        'mapping/mapping_carriers_cost' => '%sebay/mapping/mapping_carriers/%d',
        'mapping/mapping_templates' => '%sebay/mapping/mapping_carriers/%d',
        'mapping/univers' => '%sebay/mapping/mapping_templates/%d',
        'mapping/mapping_categories' => '%sebay/mapping/univers/%d',
        'mapping/mapping_conditions' => '%sebay/mapping/mapping_categories/%d',
        'mapping/variation' => '%sebay/mapping/mapping_conditions/%d',
        'mapping/variation_values' => '%sebay/mapping/variation/%d',
        'mappingFeatures' => '%sebay/mapping/variation_values/%d',
        'mappingFeatureValues' => '%sebayv1/mappingFeatures/%d',
        'actions_products' => '%sebayv1/mappingFeatureValues/%d',
    );
    
    public static $next_def = array(
        'authentication' => '%sebay/auth_setting/%d',
        'auth_setting' => '%sebay/payment_method/%d',
        'payment_method' => '%sebay/return_method/%d',
        'return_method' => '%sebay/templates/%d',
        'templates' => '%sebay/price_modifier/%d',
        'price_modifier' => '%sebay/mapping/mapping_carriers/%d',
        'mapping/mapping_carriers' => '%sebay/mapping/mapping_carriers_cost/%d',
        'mapping/mapping_carriers_cost' => '%sebay/mapping/mapping_templates/%d',
        'mapping/mapping_templates' => '%sebay/mapping/univers/%d',
        'mapping/univers' => '%sebay/mapping/mapping_categories/%d',
        'mapping/mapping_categories' => '%sebay/mapping/mapping_conditions/%d',
        'mapping/mapping_conditions' => '%sebay/mapping/variation/%d',
        'mapping/variation' => '%sebay/mapping/variation_values/%d',
        'mapping/variation_values' => '%sebayv1/mappingFeatures/%d',
        'mappingFeatures' => '%sebayv1/mappingFeatureValues/%d',
        'mappingFeatureValues' => '%sebay/actions_products/%d',
    );
    
    public static $authentication = array(
        'title_authentication',
        'generate_token',
        'ON',
        'integration_ebay_authorization_help_01',
        'integration_authorization_wizard',
        'integration_ebay_authorization_help_02',
        'user_not_available',
        'auth_is_processing',
    );
    
    public static $index = array();
    public static $mappingFeatures = array(
        'This attribute provide the free text. You can use this value for',
        'mapping a new value',
        'for',
        'prev',
        'next'
    );
    public static $mappingFeatureValues = array(
        'This attribute provide the free text. You can use this value for',
        'mapping a new value',
        'for',
        'prev',
        'next'
    );
    public static $errorResolutions = array(
        'Products',
        'Code',
        'Messages',
        'Total SKUs',
        'SKU',
        'Empty data',
        'Search',
        'Per page',
        'Previous',
        'Product ID',
        'Product name',
        'Combination ID',
        'Next',
        'Solved',
        'Showing 1 to {0} of {1} entries'
    );
    public static $get_error_resolutions = array(
        'Combination ID',
        'Messages',
        'Product ID',
        'Product name',
        'Code',
        'Total SKUs',
        'SKU',
    );
    public static $save_error_resolutions = array();
    public static $test_error_resolutions = array();
}