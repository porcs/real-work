<?php

require_once(dirname(__FILE__) . '/FeedBiz/config/orders.php');
require_once(dirname(__FILE__) . '/FeedBiz/config/stock.php');

class FeedBiz_Orders extends ObjectModelOrder {
    
        public function __construct( $user, $id = null ) {	    
                foreach ( $user as $id_user )
                        parent::__construct( $id_user, (int)$id );
        }

        public function updateOrder( $user, $seller_order_id, $debug=null/*, $orders*/ ) {
                      
            $update_order = $this->getOrdersBySellerOrderID($user , $seller_order_id );
            $id_order = isset($update_order['id_orders']) ? $update_order['id_orders'] : null;
            $message = '';
            $pass = false;
            
            if(isset($id_order)){

                require_once(dirname(__FILE__) . '/UserInfo/configuration.php');

                $user_config = new UserConfiguration();
                $user_data = $user_config->getUserByUsername($user);
                $id_customer = $user_data['id_customer'];

                // get shop url
                $shop_url = $user_config->getShopInfo($id_customer) ;

                if(isset($shop_url['order_data']) && !empty($shop_url['order_data'])) {

                    $url = $shop_url['order_data'];

                    $conj = strpos($url, '?') !== FALSE ? '&' : '?';
                    $preparedURL = $url . $conj . sprintf('id_order=%s&fbtoken=%s', $seller_order_id, $user_data['user_code']);

                    if(function_exists('fbiz_get_contents')){
                        $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                    }else{
                        $return = @file_get_contents($preparedURL);
                    }

                    // set to order table
                    if(isset($return)){

                        $return_order = simplexml_load_string($return, 'SimpleXMLElement', LIBXML_NOCDATA);
                        
                        $Orders = new Orders( $user, null, $debug );
                        $order = $Orders->updateOrders( $id_order, $return_order );

                        if(isset($order['pass']) && $order['pass']){

                            // Hook
                            require_once(dirname(__FILE__).'/../../libraries/Hooks.php');
    
                            $hook = new Hooks();
                            $hook->_call_hook('save_orders', array(
                                'user_name' => $user,
                                'id_shop' => $order['id_shop'],
                                'site' => $order['site'],
                                'id_order' => $order['id_order']
                            ));

                            $pass = true;
                            $message = 'import success';

                        } else {

                            if($return_order instanceof SimpleXMLElement) {

                                // log orders
                                $message = isset($order['output']) ? $order['output'] : 'Import order unsuccessful';

                                $logs[] = array(
                                    'batch_id'		=> (string) $return_order->References->Id,
                                    'request_id'        => (int) $return_order->References->Id,
                                    'error_code'        =>  1,
                                    'message'		=>  $message,
                                    'date_add'		=>  date('Y-m-d H:i:s'),
                                    'id_shop'		=>  (int) $return_order['IdShop'],
                                    'id_marketplace'	=>  0,
                                    'site'              => (string) $return_order['LanguageCode']
                                );
                                $this->insertLog($user, $logs);
                            }
                        }
                    }
                }
            }

            return array('pass'=>$pass, 'message'=>$message);
        }

        public function setOrder( $user, $orders, $debug=null ) {
		$Orders = new Orders( $user, null, $debug );
		return $Orders->importOrders( $orders );
        }

        public function exportOrderList( $user, $id_order = null ) {
                if ($id_order) 
                        return $this->getOrdersListById( $user, $id_order );

                return $this->getOrdersList( $user );
        }

        public function getOrdersListById( $user , $id_order ) {
	    
                if ( !isset( $user ) || empty( $user ) || !isset( $id_order ) || empty( $id_order ) )
                        return false;
		
                $order_list = new Orders( $user );
		$orders = $order_list->getOrdersListById( $user , $id_order );

                return $orders;
        }
	
	public function getOrdersByID( $user , $id_order ) {
	    
                $order_list = new Orders( $user );
                $data = $order_list->getOrderByID( $id_order );
                return $data;
        }

        public function getOrdersBySellerOrderID( $user, $seller_order_id, $order_id=null) {

                $order_list = new Orders( $user );
                $data = $order_list->getOrderBySellerOrderId($seller_order_id, $order_id);
                return $data;
        }
        
        public function getOrdersList( $user , $sales_channal = null, $id_shop = null, $id_site = null, $id_lang = null ) {
                if ( !isset( $user ) || empty( $user ) )
                        return false;

                $order_list = new Orders( $user );
                $data = $order_list->exportOrders( $sales_channal, $id_shop, $id_site, $id_lang );
                return $data;
        }
	
        public function updateOrderStatus($user, $id_order , $status, $id_invoice)
        {
            $order = new Orders($user);
            return $order->updateOrderStatus($id_order, $status, $id_invoice);
        }
	
        public function updateOrderErrorStatus($user, $id_order , $status, $comment)
        {
            $order = new Orders($user);
            return $order->updateOrderErrorStatus($id_order, $status, $comment);
        }
    
        //Retrieve orders that have no TRACKING NUMBER
        public function getOrdersNoTracking($user, $id_arr){
            $orderShippings = new OrderShippings($user);
            return $orderShippings->getOrdersNoTracking($id_arr);
        }
        
        public function getOrdersForShip($user){
            $orderShippings = new OrderShippings($user);
            return $orderShippings->getOrdersForShip();
        }
        
        public function updateOrderTracking($user, $orders){
            $orderShippings = new OrderShippings($user);
            return $orderShippings->updateOrderTracking($orders);
        }        
        
        public function updateOrderTrackingByIdOrders($user, $orders, $fbtoken=null, $url=null, $batchID=null, $debug=false){  //2016-01-15
            $orderShippings = new OrderShippings($user);
            return $orderShippings->updateOrderTrackingByIdOrders($orders, $fbtoken, $url, $batchID, $debug);
        }
        
        public function updateShippedStatus($user, $orders, $where = null){
            $orderShippings = new OrderShippings($user);
            return $orderShippings->updateShippedStatus($orders, $where);
        }
        
        public function insertLog($user, $logs){
            $orderLog = new OrderLog($user);
            return $orderLog->insertLog($logs);
        }
        
        public function stockMovement($user, $stockmovement_url, $fbtoken, $id_shop, $batch_id, $id_marketplace, $site, $imported_order, $marketplace = null, $is_addStock=false, $action_type=null) {
        	$order = new Orders($user);
        	return $order->stockMovement($stockmovement_url, $fbtoken, $id_shop, $batch_id, $id_marketplace, $site, $imported_order, $marketplace, $is_addStock, $action_type);
        }
        
        public function getNumberOrderInRange($user){
            $order = new Orders($user);
            return $order->getNumberOrderInRange();
        }
        
        public function getOrdersDataGraph($user){
            $order = new Orders($user);
            return $order->getOrdersDataGraph();
        }
        
        public function updateOrderCanceled($user, $fbtoken, $url, $orders, $batchID, $by_seller_order_id=false, $debug=false, $allow_send_to_shop=true){
            $order = new Orders($user);
            return $order->updateOrderCanceled($fbtoken, $url, $orders, $batchID, $by_seller_order_id, $debug, $allow_send_to_shop);
        }
        
        public function changeOrderAddressName($user, $id_orders, $name){
            $order = new OrderBuyers($user);
            return $order->changeOrderAddressName($id_orders, $name);
        }

        public function changeOrderAddress($user, $id_orders, $data){ //07072016
            $order = new OrderBuyers($user);
            return $order->changeOrderAddress($id_orders, $data);
        }
	
        public function getMultichannelOrders($user, $id_shop, $start, $limit, $order_by, $search, $num_row, $marketplaces){
            $order = new Orders($user);
            return $order->getMultichannelOrder($id_shop, $start, $limit, $order_by, $search, $num_row, $marketplaces);
        }
        	
        public function findOrdersFromSellerOrderId($user, $id_order_seller){
            $order = new OrderInvoices($user);
            return $order->getOrderInvoiceBySellerOrderId($id_order_seller);
        }
}