<?php

if (!function_exists('html_special')) :

    function html_special($var) {
        if (is_array($var)) :
            return array_map('html_special', $var);
        else :
            return htmlspecialchars($var, ENT_QUOTES, 'UTF-8');
        endif;
    }

endif;

if (!function_exists('encode_cdata')) :

    function encode_cdata($var, $type = 'string', $elementName = null) {
        if (is_array($var)) :
            return array_map('encode_cdata', $var);
        else :
            return (gettype($var) == $type && !empty($var)) ? ("<![CDATA[" . $var . "]]>") : $var;
        endif;
    }

endif;

if (!function_exists('encode_number_int')) :

    function encode_number_int($var, $decimals = 0, $dec_point = ".", $thousands_sep = ",") {
        if (is_array($var)) :
            foreach ($var as $key => $agrs) :
                $var[$key] = encode_number_int($agrs, $decimals, $dec_point, $thousands_sep);
            endforeach;
        else :
            if (is_numeric($var)) :
                if (is_int(filter_var($var, FILTER_VALIDATE_INT))) :
                    return number_format($var, 0, $dec_point, $thousands_sep);
                elseif (is_float(filter_var($var, FILTER_VALIDATE_FLOAT))) :
                    return $var;
                endif;
            endif;
        endif;
        return $var;
    }

endif;

if (!function_exists('encode_number_float')) :

    function encode_number_float($var, $decimals = 0, $dec_point = ".", $thousands_sep = ",") {
        if (is_array($var)) :
            foreach ($var as $key => $agrs) :
                $var[$key] = encode_number_float($agrs, $decimals, $dec_point, $thousands_sep);
            endforeach;
        else :
            if (is_numeric($var)) :
                if (is_int(filter_var($var, FILTER_VALIDATE_INT))) :
                    return $var;
                elseif (is_float(filter_var($var, FILTER_VALIDATE_FLOAT))) :
                    if (preg_match('/^[0-9]{1,7}(?:\.[0-9]{1,2})?$/', $var)) :
                        return number_format($var, $decimals, $dec_point, $thousands_sep);
                    endif;
                endif;
            endif;
        endif;
        return $var;
    }

endif;

if (!function_exists('encode_number_format')) :

    function encode_number_format($var, $decimals = 0, $dec_point = ".", $thousands_sep = ",") {
        if (is_array($var)) :
            foreach ($var as $key => $agrs) :
                $var[$key] = encode_number_format($agrs, $decimals, $dec_point, $thousands_sep);
            endforeach;
        else :
            if (is_numeric($var)) :
                if (is_int(filter_var($var, FILTER_VALIDATE_INT))) :
                    return number_format($var, 0, $dec_point, $thousands_sep);
                elseif (is_float(filter_var($var, FILTER_VALIDATE_FLOAT))) :
                    if (preg_match('/^[0-9]{1,7}(?:\.[0-9]{1,2})?$/', $var)) :
                        return number_format($var, $decimals, $dec_point, $thousands_sep);
                    endif;
                endif;
            endif;
        endif;
        return $var;
    }

endif;

if (!function_exists('array_remove_empty')) :
    function array_remove_empty($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = array_remove_empty($haystack[$key]);
            }

            if (empty($haystack[$key])) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }
endif;

// Tools load language file
if (!function_exists('load')) :
    function load($file, $language) {
        $langfile = $file . '_lang.php';
        
        if (file_exists(dirname(__FILE__) . '/../' . 'language/' . $language . '/' . $langfile)) {
            include(dirname(__FILE__) . '/../' . 'language/' . $language . '/' . $langfile);
        } else {   
            return ;   
        }
        if (!isset($lang) || empty($lang)) {
            return ;
        }
        global $l;
        $l = $lang;
    }
endif; 

// Tools language line 
if (!function_exists('l')) :
    function l($line) {
        global $l;        
        return (empty($line) || !isset($l[$line])) ? $line : $l[$line];
    }
endif;     

// special char
if (!function_exists('pregCityName')) :
    function pregCityName($city)
    {
        if(preg_match('/^[^!<>;?=+@#"°{}_$%]*$/ui', $city)){
            return str_replace(array('[','!','<','>',';','?','=','+','@','#','"','°','{','}','_','$','%',']','*','&','#'),'', $city);
        }
        
        return $city;
    }
endif; 

if (!function_exists('pregAddress')) :
    function pregAddress($address)
    {
        if(empty($address) OR preg_match('/^[^!<>?=+@{}_$%]*$/ui', $address)){
            return str_replace(array('[','!','<','>','?','=','+','@','{','}','_','$','%',']','*','&','#'),'', $address);
        }
        
        return $address;
    }
endif;


    /**
     * @param $startDate
     * @param $endDate
     * @param array $holidays
     *
     * @return float|int
     */
if (!function_exists('getWorkingDays')) :
    function getWorkingDays($startDate, $endDate, $holidays = array())
    {
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date('N', $startDate);
        $the_last_day_of_week = date('N', $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }

            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) {
                $no_remaining_days--;
            }
        } else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            } else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
        //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0) {
            $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach ($holidays as $holiday) {
            $time_stamp = strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date('N', $time_stamp) != 6 && date('N', $time_stamp) != 7) {
                $workingDays--;
            }
        }

        return $workingDays;
    }
endif;

if (!function_exists('nameToKey')) :
    function nameToKey($name)
    {
        return strtolower(str_replace(' ', '_', $name));
    }
endif;


if (!function_exists('time_elapsed_string')) :
    function time_elapsed_string($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a        = array(365 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );
        $a_plural = array('year' => 'years',
            'month' => 'months',
            'day' => 'days',
            'hour' => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);

                return array('time' => $r, 'ago' => ($r > 1 ? $a_plural[$str] : $str));
            }
        }
    }

endif;

if (!function_exists('arrayToXML')) :
    function arrayToXML(array $source, DOMElement &$dom, $use_cdata=false){
        foreach($source as $key=>$value){

	    $attributeName = '';
	    $attributevalue = 0;

	    if(is_numeric($key)) {
		$tagName = "item";
		$attributeName = 'ID';
		$attributevalue = $key;
	    } else {
		$tagName = $key;
	    }

	    if(strpos($tagName, ':')) {  // 11/12/2015
		$Names = explode(':', $tagName);
		$tagName = isset($Names[0])? $Names[0] : $tagName;

		if(isset($Names[1]))
		    $attributeName = isset($Names[1])? $Names[1] : $tagName;
	    }

            //FIX FOR TESTING, AS ARRAY HAS INTEGERS AS INDEXES
            if(!isset($history[$tagName][$attributevalue]) && isset($tagName) && !empty($tagName))
            {
                $node = $dom->ownerDocument->createElement($tagName);
            }
            $history[$tagName][$attributevalue] = true;

            if(is_array($value)){

                if($attributevalue != 0)
                        $node->setAttribute($attributeName, $attributevalue);

                arrayToXML($value, $node);
            }else{

                if(isset($attributeName) && !empty($attributeName)) { // 11/12/2015
                    if($attributevalue != 0) {
                        $node->setAttribute($attributeName, $attributevalue);
                    } else {
                        $node->setAttribute($attributeName, $value);
                    }
                } else{
                    if(!is_int($value) && !is_float($value) && !is_integer($value) && $use_cdata) {
                        $node->appendChild($dom->ownerDocument->createCDATASection($value));
                    } else {
                        $node->nodeValue = htmlspecialchars($value);
                    }

                }
            }

            $dom->appendChild($node);
        }
    }
endif;

if (!function_exists('xmlToarray')) :
    
    function xmlToarray ($xmlObject)
    {
        $array = json_decode(json_encode((array)$xmlObject), TRUE);
        return rekey($array);
    }

    function rekey( $input )
    {
        if(empty($input))
            return ;
        
        $out = array();
        if(is_array($input)) {
            foreach( $input as $i => $v ) {
                if ( $i === '@attributes' ) {
                    $out = addToArray($v) ;
                } else {
                    if(is_array($v)){
                        $out[$i] = rekey( $v );
                    } else {
                        $out[$i] = $v;
                    }
                }
            }
        } else {
            $out = $input;
        }
        return $out;
    }

    function addToArray($data){
        foreach ( $data as $key => $value ){
            $array[$key] = $value;
        }
        return $array;
    }

endif;

if (!function_exists('is_serialized')) :
    function is_serialized($value, &$result = null)
    {
        if (!is_string($value)) {
            return false;
        }
        if ($value === 'b:0;') {
            $result = false;
            return true;
        }
        $length = strlen($value);
        $end = '';
        if (isset($value[0])) {
            switch ($value[0]) {
            case 's': if ($value[$length - 2] !== '"') {
     return false;
 }
            case 'b':
            case 'i':
            case 'd': $end .= ';';
            case 'a':
            case 'O': $end .= '}';
                if ($value[1] !== ':') {
                    return false;
                }
                switch ($value[2]) {
                    case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
                        break;
                    default: return false;
                }
            case 'N': $end .= ';';
                if ($value[$length - 1] !== $end[0]) {
                    return false;
                }
                break;
            default:
                return false;
        }
        }
        if (($result = @unserialize($value)) === false) {
            $result = null;
            return false;
        }
        return true;
    }
endif;