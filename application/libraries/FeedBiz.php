<?php

require_once(dirname(__FILE__) . '/FeedBiz/config/products.php');

class FeedBiz extends ObjectModel {

    public $lang;
    
    public function __construct($user, $id = null, $id_lang = null) {
	//$user = array('id_user' => 1);
        foreach ($user as $id_user) {
            parent::__construct($id_user, (int) $id, $id_lang);
        }
    }

    public function getLanguage($id_shop) {
        $language = Language::getLanguages($id_shop);
        return $language;
    }

    public function checkLanguage($iso_code, $id_shop) {
        $language = Language::getLanguages($id_shop);

        foreach ($language as $l) {
            if ($l['iso_code'] == $iso_code)
                return $l;
        }

        foreach (Language::getLanguageDefault($id_shop) as $default_lang)
            return $default_lang;
    }

    public function getLanguageDefault($id_shop) {
        $language = Language::getLanguageDefault($id_shop,objectModel::$user);
        return $language;
    }

    public function getPid($user) {
        $ImportLog = new ImportLog($user);
        return $ImportLog->get_pid();
    }

    public function getShops($user) {
        $shops = new Shop($user);
        return $shops->getShops();
    }

    public function getDefaultShop($user) {
        $shops = new Shop($user);
        return $shops->getDefaultShop();
    }

    public function getOfferShops($user) {
        $shops = new Shop($user);
        return $shops->getOfferShops($user);
    }

    public function getProfilePrice($user, $id_shop, $id_mode) {
        $shops = new Shop($user);
        return $shops->getProfilePrice($user, $id_shop, $id_mode);
    }

    public function setDefaultShop($user, $data) {
        $shops = new Shop($user);
        return $shops->setDefaultShop($data);
    }

    public function getProductNameByID($user, $id_shop, $id_product, $lang = null) {
        $l = $this->checkLanguage($lang, $id_shop);
        $product = new Product($user);
        return $product->getProductNameByID($id_product, $id_shop, $l['id_lang']);
    }

    public function getProductBySKU($id_shop, $SKU) {
        $product = new Product(objectModel::$user);
        return $product->getProductBySKU($id_shop, $SKU);
    }

    public function getProductOffer($id_product, $id_shop) {
        $product = new Product(objectModel::$user);
        return $product->getProductOffer($id_product, $id_shop);
    }

    public function getProductCombinationOffer($id_product, $id_product_attribute, $id_shop) {
        $product = new Product(objectModel::$user);
        return $product->getProductCombinationOffer($id_product, $id_product_attribute, $id_shop);
    }

    public function getProductList($id_shop, $is_active = true, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null) {
        $product_list = new Product(objectModel::$user);
        return $product_list->getProducts($id_shop, $is_active, $start, $limit, $order_by, $search, $num_row);
    }

    public function exportProductList($user, $id_shop, $mode = null, $lang = null, $page = 1) {
        if (!isset($user) || empty($user))
            return false;

        $data = $addtional = array();
        $id_lang = null;

        if (isset($lang) && !empty($lang)) {
            $l = $this->checkLanguage($lang, $id_shop);
            $id_lang = $l['id_lang'];
        }

        //get category selected
        $categories = new Category($user, 'offers');
        $addtional['category'] = $categories->getSelectedCategoryID($id_shop, $mode);
        $product_list = new Product($user);

        //get exclude Manufacturers
        $addtional['manufacturer'] = $this->getExcludeManufacturers($user, $id_shop, $mode);

        //get exclude Suppliers
        $addtional['supplier'] = $this->getExcludeSuppliers($user, $id_shop, $mode);

        //get Carriers Selected
        $addtional['carrier'] = $this->getSelectedCarriers($user, $id_shop, $mode, true);

        //limit page
        $addtional['page'] = $page;

        foreach ($product_list->exportProducts($id_shop, $mode, $id_lang, $addtional) as $product) {
            
            if (isset($product) && !empty($product) && isset($product->id_product)) {
		
                $data['product'][$product->id_product]['id_product'] = $product->id_product;
                $data['product'][$product->id_product]['name'] = $product->name;
                $data['product'][$product->id_product]['description'] = $product->description;
                $data['product'][$product->id_product]['description_short'] = isset($product->description_short) ? $product->description_short : '';
		$data['product'][$product->id_product]['link'] = isset($product->link) ? $product->link : '';
                $data['product'][$product->id_product]['sku'] = isset($product->sku) ? $product->sku : '';
                $data['product'][$product->id_product]['upc'] = isset($product->upc) ? $product->upc : '';
                $data['product'][$product->id_product]['ean13'] = isset($product->ean13) ? $product->ean13 : '';
                $data['product'][$product->id_product]['reference'] = isset($product->reference) ? $product->reference : '';
                $data['product'][$product->id_product]['quantity'] = isset($product->quantity) ? $product->quantity : '';
                $data['product'][$product->id_product]['condition'] = isset($product->condition) ? $product->condition : '';
                $data['product'][$product->id_product]['weight'] = isset($product->weight) ? $product->weight : '';
                $data['product'][$product->id_product]['price'] = isset($product->price) ? $product->price : 0;
                $data['product'][$product->id_product]['wholesale_price'] = isset($product->wholesale_price) ? $product->wholesale_price : 0;
                $data['product'][$product->id_product]['manufacturer'] = isset($product->manufacturer) ? $product->manufacturer : '';
                $data['product'][$product->id_product]['supplier'] = isset($product->supplier) ? $product->supplier : '';
                $data['product'][$product->id_product]['width'] = isset($product->width) ? $product->width : '';
                $data['product'][$product->id_product]['height'] = isset($product->height) ? $product->height : '';
                $data['product'][$product->id_product]['depth'] = isset($product->depth) ? $product->depth : '';
                $data['product'][$product->id_product]['weight'] = isset($product->weight) ? $product->weight : '';
                $data['product'][$product->id_product]['active'] = isset($product->active) ? $product->active : '';
                $data['product'][$product->id_product]['currency'] = isset($product->currency) ? $product->currency : '';
                $data['product'][$product->id_product]['feature'] = isset($product->feature) ? $product->feature : '';

                if (isset($product->on_sale) && $product->on_sale == 1)
                    $data['product'][$product->id_product]['sale'] = $product->sale;

                $data['product'][$product->id_product]['carrier'] = isset($product->carrier) ? $product->carrier : '';

                if (isset($product->currency) && !empty($product->currency)) {
                    foreach ($product->currency as $currency)
                        if (isset($currency['name']) || !empty($currency['name']))
                            $data['product'][$product->id_product]['currency'] = strtolower($currency['name']);
                }
                else {
                    $data['product'][$product->id_product]['currency'] = '';
                }

                if (isset($product->id_category_default))
                    $data['product'][$product->id_product]['id_category_default'] = $product->id_category_default;
                else
                    $data['product'][$product->id_product]['category'] = '';

                if (isset($product->image) && !empty($product->image)) {
                    foreach ($product->image as $key_image => $image) {
                        if (isset($image['image_url']) || !empty($image['image_url'])) {
                            $data['product'][$product->id_product]['images'][$key_image]['url'] = $image['image_url'];
                            $data['product'][$product->id_product]['images'][$key_image]['type'] = $image['image_type'];
                        }
                    }
                }

                // Product Option
                if (isset($product->no_price_export) && !empty($product->no_price_export)) {
                    $data['product'][$product->id_product]['no_price_export'] = $product->no_price_export;
                }
                if (isset($product->no_quantity_export) && !empty($product->no_quantity_export)) {
                    $data['product'][$product->id_product]['no_quantity_export'] = $product->no_quantity_export;
                }
                if (isset($product->latency) && !empty($product->latency)) {
                    $data['product'][$product->id_product]['latency'] = $product->latency;
                }
                if (isset($product->shipping) && !empty($product->shipping)) {
                    $data['product'][$product->id_product]['shipping'] = $product->shipping;
                }

                $data['product'][$product->id_product]['combination'] = isset($product->combination) ? $product->combination : '';
            }
        }

        return $data;
    }

    public function exportProductsArray($user, $id_shop, $mode = null, $lang = null, $page = null, $id_products = array(),$exclude_data=array()) {
        if (!isset($user) || empty($user))
            return false;

        $data = $addtional = array();
        $id_lang = null;

        if (isset($lang) && !empty($lang)) {
            $l = $this->checkLanguage($lang, $id_shop);
            $id_lang = $l['id_lang'];
        }
	
        //get category selected
        $categories = new Category($user, 'offers');
        $addtional['category'] = $categories->getSelectedCategoryID($id_shop, $mode);
        $product_list = new Product($user);

        //get exclude Manufacturers
        $addtional['manufacturer'] = $this->getExcludeManufacturers($user, $id_shop, $mode);

        //get exclude Suppliers
        $addtional['supplier'] = $this->getExcludeSuppliers($user, $id_shop, $mode);

        //get Carriers Selected
//        $addtional['carrier'] = $this->getSelectedCarriers($user, $id_shop, $mode, true);

        //limit page
        $addtional['page'] = $page;
        
        if(!empty($exclude_data)){
            $addtional  = array_merge($addtional,$exclude_data);
        }

        //Condition dictionary
        $conditionDictionary = Conditions::getConditions($id_shop);

        foreach ($product_list->exportProductsArray($id_shop, $mode, $id_lang, $addtional, $id_products) as $product) {
            if (isset($product) && !empty($product) && isset($product['id_product']) && !empty($product['name'])) {
                $id_product = $product['id_product'];
                $data['product'][$id_product]['id_product'] = $id_product;
                $data['product'][$id_product]['name'] = $product['name'];
                $data['product'][$id_product]['description'] = $product['description'];
                $data['product'][$id_product]['description_short'] = isset($product['description_short']) ? $product['description_short'] : '';
                $data['product'][$id_product]['link'] = isset($product['link']) ? $product['link'] : '';
                $data['product'][$id_product]['sku'] = isset($product['sku']) ? $product['sku'] : '';
                $data['product'][$id_product]['upc'] = isset($product['upc']) ? $product['upc'] : '';
                $data['product'][$id_product]['ean13'] = isset($product['ean13']) ? $product['ean13'] : '';
                $data['product'][$id_product]['reference'] = isset($product['reference']) ? $product['reference'] : '';
                $data['product'][$id_product]['quantity'] = isset($product['quantity']) ? $product['quantity'] : '';
                $data['product'][$id_product]['condition'] = isset($product['id_condition']) ? $conditionDictionary[$product['id_condition']] : '';
                $data['product'][$id_product]['weight'] = isset($product['weight']) ? $product['weight'] : '';
                $data['product'][$id_product]['price'] = isset($product['price']) ? $product['price'] : '';
                $data['product'][$id_product]['manufacturer'] = isset($product['manufacturer']) ? $product['manufacturer'] : '';
                $data['product'][$id_product]['supplier'] = isset($product['supplier']) ? $product['supplier'] : '';
                $data['product'][$id_product]['width'] = isset($product['width']) ? $product['width'] : '';
                $data['product'][$id_product]['height'] = isset($product['height']) ? $product['height'] : '';
                $data['product'][$id_product]['depth'] = isset($product['depth']) ? $product['depth'] : '';
                $data['product'][$id_product]['active'] = isset($product['active']) ? $product['active'] : '';
                $data['product'][$id_product]['currency'] = isset($product['currency']) ? $product['currency'] : '';
                $data['product'][$id_product]['feature'] = isset($product['feature']) ? $product['feature'] : '';
                $data['product'][$id_product]['disable'] = isset($product['disable']) ? $product['disable'] : '';
                $data['product'][$id_product]['base_price'] = isset($product['base_price']) ? $product['base_price'] : '';

                if (isset($product['on_sale']) && $product['on_sale'] == 1)
                    $data['product'][$id_product]['sale'] = $product['sale'];

                $data['product'][$id_product]['carrier'] = isset($product['carrier']) ? $product['carrier'] : '';

                if (isset($product['currency']) && !empty($product['currency'])) {
                    foreach ($product['currency'] as $currency)
                        if (isset($currency['name']) || !empty($currency['name']))
                            $data['product'][$id_product]['currency'] = strtolower($currency['name']);
                }
                else {
                    $data['product'][$id_product]['currency'] = '';
                }

                if (isset($product['id_category_default']))
                    $data['product'][$id_product]['id_category_default'] = $product['id_category_default'];
                else
                    $data['product'][$id_product]['category'] = '';

                if (isset($product['image']) && !empty($product['image'])) {
                    foreach ($product['image'] as $key_image => $image) {
                        if (isset($image['image_url']) || !empty($image['image_url'])) {
                            $data['product'][$id_product]['images'][$key_image]['url'] = $image['image_url'];
                            $data['product'][$id_product]['images'][$key_image]['type'] = $image['image_type'];
                        }
                    }
                }

                $data['product'][$id_product]['combination'] = isset($product['combination']) ? $product['combination'] : '';
            }
        }

        return $data;
    }

    public function exportProductsForTableList($user, $id_shop, $mode = null, $lang = null, $option = array()) {
        if (!isset($user) || empty($user))
            return false;

        $addtional = array();
        $id_lang = null;

        if (isset($lang) && !empty($lang)) {
            $l = $this->checkLanguage($lang, $id_shop);
            $id_lang = $l['id_lang'];
        } 
        //get category selected
        if (isset($option['skip_selected_category']) && $option['skip_selected_category']) {
            $categories = new Category($user, 'offers');
            $addtional['category'] = $categories->getSelectedCategoryID($id_shop, $mode);
        }

        if (isset($option['start'])) {
            $addtional['start'] = $option['start'];
        }
        if (isset($option['length'])) {
            $addtional['length'] = $option['length'];
        }
        if (isset($option['condition'])) {
            $addtional['condition'] = $option['condition'];
        }
        if (isset($option['category'])) {
            $addtional['category'] = $option['category'];
        }
        if (isset($option['search'])) {
            $addtional['search'] = $option['search'];
        }

        $product_list = new Product($user);
        $data = $product_list->exportProductsForTableList($id_shop, $mode, $id_lang, $addtional);

        return $data;
    }

    public function getProductById($user, $id_product, $lang) {
	
        if (!isset($user) || empty($user) || !isset($id_product) || empty($id_product) || !isset($lang) || empty($lang))
            return false;

        $product_list = new Product($user, $id_product);

        return $product_list;
	
    }

    public function getCategories($user, $lang, $id_shop) {
	
        if (!isset($user) || empty($user))
            return false;

        $l = $this->checkLanguage($lang, $id_shop);
        $categories = new Category($user);

        $root = $categories->getRootCategories($id_shop);

        $sub_category = $categories->getSubCategories($root, $id_shop, $l['id_lang']);

        return $sub_category;
	
    }

    public function getBigCategories($user, $id_shop, $lang = 'en') {
	
        if (!isset($user) || empty($user))
            return false;

        $categories = new Category($user);
        $root = $categories->getRootCategories($id_shop);
        $size_category = $categories->getBigCategories($root, $id_shop);

        return $size_category;
    }

    public function getSubArrCategories($user,$id_shop=null, $lang = 'en') {
	
        $categories = new Category($user);
        $root = $categories->getRootCategoriesAllShop($id_shop);
        $out = array();
        $root_size = sizeof($root);
         
	
        foreach ($root as $r) {
            if ($r['shop_id'] == 0)
                continue;

            $sub_category = $categories->getSubArrCategories($r['root_id'], $r['shop_id'], true);
            
            $res = $categories->getCatMapSumProd($sub_category, $r['shop_id']);

            $out = array_merge($out, $res);
        }
//        echo'<pre>';
//        print_r(array($root_size,$out[0]['cat_list'][0],$root[0]['root_id']));exit;
        if($root_size==1 && isset($out[0]['cat_list'][0]) && $out[0]['cat_list'][0]==$root[0]['root_id']){
            $root = $categories->getRootCategoriesAllShop($id_shop,true);
            
            $out = array();
            foreach ($root as $r) {
                if ($r['shop_id'] == 0)
                    continue; 
                $sub_category = $categories->getSubArrCategories($r['root_id'], $r['shop_id'], true); 
                $res = $categories->getCatMapSumProd($sub_category, $r['shop_id']); 
                $out = array_merge($out, $res);
            }
        }
//        echo "<pre>";
//        print_r($out);exit;
	
        $out2 = $out;
        $cat_list = array();
        $sum = $all_sum = 0;
        $i = 0;
//        echo '<pre>';
//	print_r($out);exit;
        foreach ($out as $ox) {
            $max = $max_id = $max_all = 0;
            foreach ($out2 as $k => $o) {
                if ($max > $o['num']) {
                    $tmp = $max;
                } else {
                    $max = $o['num'];
                    $max_all = $o['all_num'];
                    $max_id = $k;
                }
            }
            unset($out2[$max_id]);
            $sum += $max;
            $all_sum += $max_all;
            $i++;
            if ($i >= 6 || $max <= 0 || $out[$max_id]['name'] == '') {
                continue;
            }
            $cat_list[] = $out[$max_id];
        }
	
	$cat_list['sum'] = $sum;
        $cat_list['all_sum'] = $all_sum;
	
        return $cat_list;
    }

    public function getRootCategories($user, $lang, $id_shop) {

        $l = $this->checkLanguage($lang, $id_shop);
        $categories = new Category($user);
        $root = $categories->getRootCategories($id_shop);
        $name = $categories->getCategoryNameByID($root, $id_shop, $l);

        if (isset($name))
            foreach ($name as $name_lang)
                $name = $name_lang;

        return array('id' => $root, 'name' => $name);
    }
    
    public function getAllRootCategories($user, $id_shop,$id_lang=null,$selected=false) {
        $categories = new Category($user, 'products'); 
        $root = $categories->getAllRootCategories($id_shop,$selected);
        $out= array();
        if(is_array($root)){
            foreach($root as $r){
               $name = $categories->getCategoryNameByID($r, $id_shop,$id_lang); 
               $out[] =  array('id' => $r, 'name' => $name);
            }
            
        }else{
            $name = $categories->getCategoryNameByID($root, $id_shop,$id_lang);
            $out[] =  array('id' => $root, 'name' => $name);
        }

        return $out;
    }

    public function getCategoryByProductId($user, $id_product, $lang, $id_shop) {
        $id_category = array();
        if (!isset($user) || empty($user))
            return false;

        $l = $this->checkLanguage($lang, $id_shop);

        $categories = new Category($user);
        $category = $categories->getCategoryByProductID($id_product, $id_shop);

        foreach ($category as $key => $cate) {
            $sub_category = $categories->getSubCategories($key, $id_shop, $l['id_lang']);

            $id_category[$key]['name'] = $cate['name'][$l['iso_code']];
            $id_category[$key]['child'] = $sub_category;
        }

        return $category;
    }

    public function getConditions($user, $id_shop) {
        $conditions = new Conditions($user);
        return $conditions->getConditions($id_shop);
    }

    public function getConditionMapping($user, $id_shop, $id_market = null) {
        $conditions = new Conditions($user);
        return $conditions->getConditionMapping($id_shop, $id_market);
    }

    public function getConditionMappingByName($user, $id_shop, $id_market = null, $name = null) {
        $conditions = new Conditions($user);
        return $conditions->getConditionMappingByName($id_shop, $id_market, $name);
    }

    public function setConditionMapping($user, $id_shop, $id_marketplace, $market_condition, $shop_condition) {
        $conditions = new Conditions($user);
        return $conditions->setConditionMapping($id_shop, $id_marketplace, $market_condition, $shop_condition);
    }

    public function removeConditionMapping($user, $id_shop, $id_marketplace, $market_condition) {
        $conditions = new Conditions($user);
        return $conditions->removeConditionMapping($id_shop, $id_marketplace, $market_condition);
    }

    public function getTaxes($user, $lang, $id_shop) {
        $taxes = new Tax($user);
        $taxs = $taxes->getTaxes($id_shop);
        $return_tax = array();

        $l = $this->checkLanguage($lang, $id_shop);

        if (isset($taxs) || !empty($taxs))
            foreach ($taxs as $key => $tax) {
                $return_tax[$key]['id'] = $key;
                $return_tax[$key]['rate'] = $tax['rate'];
                $return_tax[$key]['name'] = $tax['name'][$l['iso_code']];
            }

        return $return_tax;
    }
    
    public function getDefaultCurrency($id_shop) {
        $currencies = new Currency(objectModel::$user);
        $currency = $currencies->getDefaultCurrency($id_shop);

        return $currency;
    }

    public function getCurrency($user, $id_shop) {
        $currencies = new Currency($user);
        $currency = $currencies->getCurrency($id_shop);

        return $currency;
    }

    public function getUnits($user, $id_shop) {
        $units = new Unit($user);
        $unit = $units->getUnits($id_shop);

        return $unit;
    }

    public function getCarriers($user, $id_shop) {
        $carriers = new Carrier($user);
        $carrier = $carriers->getCarriers($id_shop);

        return $carrier;
    }

    public function checkSelectedCategoriesOffer($user, $id_shop, $id_mode) {
        $categories = new Category($user, 'offers');
        $category = $categories->checkSelectedCategory($id_shop, $id_mode);

        return $category;
    }

    public function getSelectedCategoriesOffer($user, $id_shop, $id_mode, $id_parent, $is_id = false, $id_lang = null) {
	
        $categories = new Category($user, 'offers');
        $category = $categories->getSelectedCategory($id_parent, $id_shop, $id_mode, $is_id, $id_lang);

        if(!isset($this->obj_categories_offer)){
            $this->obj_categories_offer =new Category($user, 'offers');
        }
	
        $category = $this->obj_categories_offer->getSelectedCategory($id_parent, $id_shop, $id_mode, $is_id, $id_lang);
        return $category;
    }

    public function saveSelectedCategory($user, $data, $id_shop, $id_mode) {
        $categories = new Category($user);

        if (!$categories->saveSelectedCategory($data, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function getSelectedCarriers($user, $id_shop, $id_mode = null, $selected_only = false) {
        $carriers = new Carrier($user);

        if ($selected_only == true)
            return $carriers->getCarriersSelected($id_shop, $id_mode);

        $carrier = $carriers->getSelectedCarriers($id_shop, $id_mode);
	
        if (!$carrier)
            return FALSE;

        return $carrier;
    }

    public function saveSelectedCarriers($user, $data, $id_shop, $id_mode) {
        $carriers = new Carrier($user);

        if (!$carriers->saveSelectedCarriers($data, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function getAttributes($user, $lang, $id_shop, $id_attribute_group = null, $id_attribute = null) {
        $return_attr = array();
        $l = $this->checkLanguage($lang, $id_shop);

        $attributes = new Attribute($user);
        $attribute = $attributes->getAttributes($id_shop, $l['id_lang'], $id_attribute_group, $id_attribute);

        if (isset($attribute) || !empty($attribute))
            foreach ($attribute as $keys => $attr)
                $return_attr[$keys] = $attr;

        return $return_attr;
    }

    public function getProductAttributeById($user, $id_product_attribute, $id_shop) {
        $attributes = new ProductAttribute($user);
        $attribute = $attributes->getProductAttributeById($id_product_attribute, $id_shop);

        return $attribute;
    }

    public function getAttributeValuesExcludeMappingValues($user, $lang, $id_shop, $id_mode) {

        $return_attr = array();
        $l = $this->checkLanguage($lang, $id_shop);

        $attributes = new Mapping($user);
        $attribute = $attributes->getAttributeValuesExcludeMappingValues($id_shop, $id_mode, $l['id_lang']);

        if (isset($attribute) || !empty($attribute))
            foreach ($attribute as $key => $attr) {
                $return_attr[$key]['id_attribute_group'] = $attr['id_attribute_group'];
                $return_attr[$key]['id_attribute'] = $attr['id_attribute'];
                $return_attr[$key]['name'] = $attr['name'][$l['iso_code']];
            }

        return $return_attr;
    }

    public function getMappingAttribute($user, $id_shop, $id_mode) {
        $mattributes = new Mapping($user);
        $mattribute = $mattributes->getMappingAttribute($id_shop, $id_mode);

        return $mattribute;
    }

    public function saveMappingAttribute($user, $data, $id_shop, $id_mode) {
        $mattributes = new Mapping($user);
        $mattribute = $mattributes->saveMapping('attribute', $data, $id_shop, $id_mode);

        if (!$mattribute)
            return FALSE;

        return true;
    }

    public function getMappingCharacteristics($user, $id_shop, $id_mode) {
        $characteristics = new Mapping($user);
        $character = $characteristics->getMappingCharacteristics($id_shop, $id_mode);

        return $character;
    }

    public function saveMappingCharacteristics($user, $data, $id_shop, $id_mode) {
        $characteristics = new Mapping($user);
        $character = $characteristics->saveMapping('characteristic', $data, $id_shop, $id_mode);

        if (!$character)
            return FALSE;

        return $character;
    }

    public function getManufacturerExcludeMapping($user, $id_shop, $id_mode) {
        $return_attr = array();

        $manufacturers = new Mapping($user);
        $manufacturer = $manufacturers->getManufacturerExcludeMapping($id_shop, $id_mode);

        if (isset($manufacturer) || !empty($manufacturer))
            foreach ($manufacturer as $key => $manufac) {
                $return_attr[$key]['name'] = $manufac['name'];
            }

        return $return_attr;
    }

    public function getMappingManufacturer($user, $id_shop, $id_mode) {
        $manufacturers = new Mapping($user);
        $manufacturer = $manufacturers->getMappingManufacturer($id_shop, $id_mode);

        return $manufacturer;
    }

    public function saveMappingManufacturers($user, $data, $id_shop, $id_mode) {
        $mattributes = new Mapping($user);
        $mattribute = $mattributes->saveMapping('manufacturer', $data, $id_shop, $id_mode);

        if (!$mattribute)
            return FALSE;

        return true;
    }

    public function getManufacturers($user, $id_shop) {
        $manufacturers = new Manufacturer($user);
        $manufacturer = $manufacturers->getManufacturers($id_shop);

        return $manufacturer;
    }

    public function getManufacturerById($user, $id) {
        if (!isset($user) || empty($user))
            return FALSE;

        $manufacturer = new Manufacturer($user, $id);
        $return = array();
        $return['id_manufacturer'] = $manufacturer->id_manufacturer;
        $return['name'] = $manufacturer->name;

        return $return;
    }

    public function getFilterManufacturers($user, $id_shop, $id_mode) {
        $manufacturers = new Filter($user);
        $manufacturer = $manufacturers->getFilterManufacturers($id_shop, $id_mode);

        return $manufacturer;
    }

    public function getExcludeManufacturers($user, $id_shop, $id_mode = null) {
        $manufacturers = new Filter($user);
        $manufacturer = $manufacturers->getExcludeManufacturers($id_shop, $id_mode);
        $manufacturer_data = array();

        foreach ($manufacturer as $key => $manu_fac)
            $manufacturer_data[$key] = $this->getManufacturerById($user, $manu_fac);

        return $manufacturer_data;
    }

    public function getSupplierById($user, $id) {
        if (!isset($user) || empty($user))
            return FALSE;

        $supplier = new Supplier($user, $id);
        $return = array();
        $return['id_supplier'] = $supplier->id_supplier;
        $return['name'] = $supplier->name;

        return $return;
    }

    public function getFilterSuppliers($user, $id_shop, $id_mode) {
        $suppliers = new Filter($user);
        $supplier = $suppliers->getFilterSuppliers($id_shop, $id_mode);

        return $supplier;
    }

    public function getExcludeSuppliers($user, $id_shop, $id_mode = null) {
        $suppliers = new Filter($user);
        $supplier = $suppliers->getExcludeSuppliers($id_shop, $id_mode);
        $supplier_data = array();

        foreach ($supplier as $key => $supp)
            $supplier_data[$key] = $this->getSupplierById($user, $supp);

        return $supplier_data;
    }

    public function savefilters($user, $data) {
        $filters = new Filter($user);
        $filter = $filters->saveFilters($data);

        if (!$filter)
            return FALSE;

        return $filter;
    }

    public function truncate_table($user, $table) {
        $db = new Db($user, ObjectModel::$database);
        $truncate = $db->truncate_table($table);

        if (!$truncate)
            return FALSE;

        return true;
    }

    public function get_token($user) {
        $otp = new OTP($user);
        $token = $otp->get_token();
        return $token;
    }

    public function get_offer_token($user) {
        $otp = new OTP($user);
        $token = $otp->get_offer_token($user);
        return $token;
    }

    public function get_last_import_log($user, $type, $shop) {
        $list = new ImportLog($user);
        return $list->get_last_import_log($type, $shop);
    }

    public function get_log_product($user, $id_shop = null, $keywords = null, $order_by = null, $limit = null, $start = null, $num_rows = false, $dash_board_log = false,$ignore_case = false) {
        $log = new ImportLog($user);
        if ($dash_board_log) {
            $num_product = (int) $num_rows;
            $list = $log->get_log_dashboard($num_product, $id_shop);
        } else {
            $list = $log->get_log($id_shop, $keywords, $order_by, $limit, $start, $num_rows,'product',$ignore_case);
        }
        return $list;
    }

    public function get_log_offer($user, $id_shop = null, $limit = null, $start = null, $order_by = null, $keywords = null, $num_rows = false, $dash_board_log = false,$ignore_case = false) {
        $log = new ImportLog($user);
        if ($dash_board_log) {            
            $list = array();
        } else {
            $list = $log->get_log($id_shop, $keywords, $order_by, $limit, $start, $num_rows, 'offer',$ignore_case);
        }

        return $list;
    }

    public function getNumberOfProduct($user, $id_shop) {
        $product_list = new Product($user);
        return $product_list->getNumRowProducts($id_shop);
    }

    public function getNumberOfOffer($user, $id_shop) {
        $product_list = new Product($user);
        return $product_list->getNumRowOffers($user, $id_shop);
    }

    public function getImportLog($user, $limit = 0, $start = null, $order_by = null, $keywords = null, $num_rows = false) {
        $list = new ImportLog($user);
        return $list->get_import_log($limit, $start , $order_by , $keywords , $num_rows  );
    }

    public function downloadLog($user, $type, $value = null) {
        $list = new ImportLog($user);
        return $list->download_log($type, $value);
    }

    public function get_images($user, $id_shop) {
        $images = new ProductImage($user);
        return $images->getImages($id_shop);
    }

    public function set_images($user, $sql) {
        $images = new ProductImage($user);
        return $images->setImages($sql);
    }

    public function calculate_price($id_category, $price, $id_mode) {
        $return_price = $price;

        $product = new Product(objectModel::$user);
        $calculate_price = $product->calculate_price($id_category, $price, $id_mode);

        if (isset($calculate_price) && !empty($calculate_price))
            $return_price = $calculate_price['price_overide'];

        return $return_price;
    }

    public function get_market_dashboard_log($id_shop, $market = 'amazon') {
        $log = new Log(objectModel::$user);
        return $log->get_market_log($id_shop, $market);
    }

    public function getFeatures($id_shop, $id_lang = null, $id_feature = null, $id_feature_value = null) {
        $feature = new Feature(objectModel::$user);
        return $feature->getFeatures($id_shop, $id_lang, $id_feature, $id_feature_value);
    }

    public function getJsonAllCategory($user = null, $id_shop = null, $id_lang = null, $type = null) {

        if (empty($user))
            return false;
	
        $dir_temp = USERDATA_PATH . $user . '/json/';
        if (!file_exists($dir_temp)) {
            $old = umask(0);
            mkdir($dir_temp, 0777, true);
            umask($old);
        }
	
        $file_path = $dir_temp . 'all_category.json';
        $file_tree_path = $dir_temp . 'tree_category.json';
        $categories = new Category($user);
        $time = $categories->getCategoryMaxDate($this->id_shop);
        $new_file = true;
	
        if (file_exists($file_path)) {
            $mtime = filemtime($file_tree_path);
            if (strtotime($time) > $mtime) {
                $new_file = true;
            } else {
                $new_file = false;
            }
        } else {
            $new_file = true;
        }
	
	$new_file = true; 
	
        if ($new_file) {
	    
            if (file_exists($file_path))
                unlink($file_path);
	    
            if (file_exists($file_tree_path))
                unlink($file_tree_path);
	    
            $this->cat_list = $categories->getCategoriesSortRoot($id_shop, $id_lang);            
            $this->his_cat = array();
            $this->cat_out_list = array();
            $cat_root = array();
	    
            foreach ($this->cat_list as $id => $cat) {

                $par_id = $cat['id_parent'];
                if ($cat['is_root_category'] != 0)
                    $cat_root[] = $id;
                if (!isset($this->cat_list[$par_id]['cat_id_list']))
                    $this->cat_list[$par_id]['cat_id_list'] = array();
                $this->cat_list[$par_id]['cat_id_list'][] = $id;
            }
	    
            $cat_tree_out = array();
            foreach ($cat_root as $id) {
                $cat_tree_out[$id] = $this->recurGetTreeCategory($id);
            }
	    
            $enc_cat = json_encode($this->cat_out_list);
            $enc_tree_cat = json_encode($cat_tree_out);

            file_put_contents($file_path, $enc_cat);
            file_put_contents($file_tree_path, $enc_tree_cat);
	    
            if ($type == 'tree') {
                return $enc_tree_cat;
            } else {
                return $enc_cat;
            }
	    
        } else {
	    
            if ($type == 'tree') {
                return (file_get_contents($file_tree_path));
            } else {
                return (file_get_contents($file_path));
            }
	    
        }
    }

    public function recurGetTreeCategory($parent_id, $sub = 0) {
	
        $out_cat = array();
        $out_cat = $this->cat_list[$parent_id];
	
        if (!in_array($parent_id, $this->his_cat) && isset($this->cat_list[$parent_id]['name'])) {
	    
            $pid = 'c' . $parent_id;
            $this->cat_out_list[$pid] = $this->cat_list[$parent_id];
            $this->cat_out_list[$pid]['id'] = $parent_id;
            $this->cat_out_list[$pid]['lv'] = $sub;
            $this->his_cat[] = $parent_id;
	    
            if (isset($this->cat_list[$parent_id]['cat_id_list'])) {
                foreach ($this->cat_list[$parent_id]['cat_id_list'] as $id) {
                    $out_cat['cat_list'][$id] = $this->recurGetTreeCategory($id, $sub + 1);
                }
            }
	    
            unset($this->cat_out_list[$pid]['cat_id_list']);
            unset($this->cat_out_list[$pid]['is_root_category']);
            unset($out_cat['cat_id_list']);
            unset($out_cat['is_root_category']);
        }
	
        return $out_cat;
    }

    /* Marketplace Product Option */
    public function marketplace_product_option($id_country = null, $MarketplaceID = null) {
        $mpo = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $mpo->checke_table_exit();
    }
    
    public function marketplace_getProductOption($id_shop, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->getProducts($id_shop, $start, $limit, $order_by, $search, $num_row);
    }
    
    public function marketplace_getCategoryOption($id_shop, $start=null, $limit=null, $order_by=null, $search=null, $num_row = null, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->getCategories($id_shop, $start, $limit, $order_by, $search, $num_row);
    }
    
    public function save_marketplace_product_option($data, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->save_marketplace_product_option($data, $id_shop);
    }
    
    public function save_marketplace_category_option($data, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->save_marketplace_category_option($data, $id_shop);
    }
    
    public function reset_marketplace_product_option($data, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->reset_marketplace_product_option($data, $id_shop);
    }
    
    public function reset_marketplace_category_option($data, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->reset_marketplace_category_option($data, $id_shop);
    }
    
    public function marketplace_getProductOptionBySku($sku, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->get_product_option_by_sku($sku, $id_shop);
    }
    
    public function marketplace_ProductOptionDownloadTemplate($id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->product_options_download_template();
    }
    
    public function marketplace_ProductOptionDownload($values, $id_shop, $id_country=null,$MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->product_options_download($values, $id_shop);
    }
    
    public function marketplace_ProductOptionUpload($data, $id_shop, $id_country=null, $MarketplaceID=null) {
        $product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, $id_country, $MarketplaceID);
        return $product_list->product_options_upload($data, $id_shop);
    }
    
    public function marketplace_getProductOptionGetFields($MarketplaceID=null) {
	$product_list = new MarketplaceProductOption(objectModel::$user, null, null, null, null, null, $MarketplaceID);
        return $product_list->get_fields_options();
    }
    
    public function GetMarketplaceById($MarketplaceID){
	return ObjectModel::get_marketplace_by_id($MarketplaceID);
    }
    
    /* get product update log : when product has changed we will log to product_update_log in log.db : eg. product change from inactive to active */
    public function getProductUpdateLog($user, $id_shop, $datetime) {
        $ImportLog = new ImportLog($user);
        return $ImportLog->get_product_update_log($id_shop, $datetime);
    }
}
