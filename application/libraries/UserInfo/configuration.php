<?php

require_once(dirname(__FILE__).'/../../../libraries/ci_db_connect.php');

class UserConfiguration {
    
    public function __construct() {
        $this->mysql_db = new ci_db_connect(); 
    }
    
    public function getShopInfo($id_customer){
         
        $mysql_db = $this->mysql_db;  
        
        $sql = "SELECT * FROM configuration WHERE id_customer = $id_customer AND name IN ('FEED_BIZ', 'FEED_MODE','FEED_SHOP_INFO','FIRST_CONF')" ; 
        $query = $mysql_db->select_query($sql);
        $data = array();
	 	
        while($row = $mysql_db->fetch($query)){ 
            $data[$row['name']]['value'] = $row['value'];
            $data[$row['name']]['config_date'] = $row['config_date'];
        }
	
        $params = array();

        if(isset($data['FEED_BIZ']['value']) && !empty($data['FEED_BIZ']['value']))
            $feed_biz = unserialize(base64_decode($data['FEED_BIZ']['value']));

        if(isset($data['FEED_SHOP_INFO']['value']) && !empty($data['FEED_SHOP_INFO']['value']))
            $feed = unserialize(base64_decode($data['FEED_SHOP_INFO']['value']));
     
        $base_url = isset($feed_biz['base_url']) ? $feed_biz['base_url'] : null;

        if(!isset($feed['url']) || empty($feed['url']))
            return null;

        $url = $feed['url']['orderimport'];
        $params['name'] = $feed['name'];
        $params['settings'] = $feed['url']['settings'];
        $params['products'] = $feed['url']['products'];
        $params['offers'] = $feed['url']['offers'];
        $params['order_url'] = $url;
        $params['url_stockmovement'] = $feed['url']['stockmovement'];
        $params['order_cancel'] = isset($feed['url']['ordercancel']) ? $feed['url']['ordercancel'] : '';
        $params['base_url'] = $base_url;
        $params['order_status'] = isset($feed['url']['orderstatus']) ? $feed['url']['orderstatus'] : ''; // added : 2016-02-12 : to add url for update order status to Shop.
        $params['url_update_options'] = isset($feed['url']['updateoptions']) ? $feed['url']['updateoptions'] : ''; // added:2016-02-12 :to add url for update offer option to Shop.
        $params['url_invoice'] = isset($feed['url']['invoice']) ? $feed['url']['invoice'] : ''; // added:2016-05-27 :to add url for Invoice Email.
        $params['url_messaging'] = isset($feed['url']['messaging']) ? $feed['url']['messaging'] : ''; // added:2016-05-27 :to add url for Customer messaging.
        $params['url_stockmovementfba'] = isset($feed['url']['stockmovementfba']) ? $feed['url']['stockmovementfba'] : ''; // added:2016-10-19 
        $params['order_data'] = isset($feed['url']['orderdata']) ? $feed['url']['orderdata'] : ''; // added:2016-10-19
        return $params;
    }
    
    public function getUserCode($id_customer){
        
        $user_code = '';
        $mysql_db = $this->mysql_db;  
        
        $sql = "SELECT user_code FROM users WHERE id = $id_customer ;" ; 
        $query = $mysql_db->select_query($sql);
        
        while($row = $mysql_db->fetch($query)){ 
            $user_code =  $row['user_code'];
        }
        
        return $user_code;
    }
    
    public function getUserDefaultLaguage($id_customer){
        
        $DefaultLaguage = '';
        $mysql_db = $this->mysql_db;  
        
        $sql = "SELECT l.language_name as name FROM users u LEFT JOIN languages l ON u.user_default_language = l.id WHERE u.id = $id_customer ;" ; 
        $query = $mysql_db->select_query($sql);
        
        while($row = $mysql_db->fetch($query)){ 
            $DefaultLaguage =  $row['name'];
        }
        
        return $DefaultLaguage;
    }
    public function getUserDefaultLaguageByUserName($username){
        
        $DefaultLaguage = '';
        $mysql_db = $this->mysql_db;  
        
        $sql = "SELECT l.language_name as name FROM users u LEFT JOIN languages l ON u.user_default_language = l.id WHERE u.user_name = '$username' ;" ; 
        $query = $mysql_db->select_query($sql);
        
        while($row = $mysql_db->fetch($query)){ 
            $DefaultLaguage =  $row['name'];
        }
        
        return $DefaultLaguage;
    }
     public function getAllUserDefaultLaguage( ){
        
        $DefaultLaguage = '';
        $mysql_db = $this->mysql_db;  
        
        $sql = "SELECT l.language_name as name, u.id as id FROM users u LEFT JOIN languages l ON u.user_default_language = l.id WHERE 1 ;" ; 
        $query = $mysql_db->select_query($sql);
        
        while($row = $mysql_db->fetch($query)){ 
            $DefaultLaguage[$row['id']] =  $row['name'];
        }
        
        return $DefaultLaguage;
    }
    public function prepare_argv($input) {
        $add = '&';
        $url_feed = $input['base_url'];

        if (strpos($url_feed, 'index.php/feedbiz/connector/') !== false) {
            $url_order = isset($input['id_order']) ? '/fborder/' . $input['id_order'] : '';
            $otp = isset($input['otp']) ? '/otp/' . $input['otp'] : '';
            $add_url = "index/fbtoken/" . $input['token'] . $url_order . $otp;
        } else {
            $url_order = isset($input['id_order']) ? "&fborder=" . $input['id_order'] : '';
            $otp = isset($input['otp']) ? "&otp=" . $input['otp'] : '';
            //other store
            if (strpos($url_feed, '?') === false) {
                $add = '?';
            }
            $add_url = $add . "fbtoken=" . $input['token'] . $url_order . $otp;
        }
        return $input['url'] . $add_url;
    }
    
    public function check_site_verify($user_id=0){
        $mysql_con = $this->mysql_db;
        $sql = "select * from configuration where id_customer = '$user_id' and name in ('FEED_BIZ', 'FEED_SHOP_INFO' )";

        $q = $mysql_con->select_query($sql);
        while($row = $mysql_con->fetch($q)){ 
            $input[strtolower($row['name'])] = unserialize(str_replace(' ','_',base64_decode($row['value'])));
        }  
        
        $sql2 = "select user_name,user_code,user_email from users where id = '$user_id'";
        $q2 = $mysql_con->select_query($sql2);
        $row2 = $mysql_con->fetch($q2);
        $input['feed_token'] = $row2['user_code']; 
        $input['user_name'] = $row2['user_name']; 
        $input['user_id'] = $user_id;
        $input['user_email'] = $row2['user_email']; 
        
        return $input;
    }
    
    public function record_history($user_id,$shop_name,$type,$next_time){
        $other = array(
            'next_time' => strtotime("+{$next_time} minutes")
        );
        $mysql_con = $this->mysql_db;
        $data=array(
            'user_id'=>$user_id,
            'user_name'=>'system',
            'history_action'=> $type,
            'history_date_time'=>date('Y-m-d H:i:s'),
            'history_table_name'=>str_replace('_',' ',$shop_name),
            'history_data'=>  json_encode($other),
    );
    $mysql_con->add_db('histories',$data);
    }
    
    public function feed_prepare_argv ($input,$type = 'products',$shop_type=''){ 
        $add = '&';
        $url_feed = $input['feed_shop_info']['url'][$type];
        
		if(strpos($url_feed,'?') === false){
            $add='?';
        } 
        $add_url = $add."fbtoken=".$input['feed_token'];
         
         return json_encode(
                    array(
                        'username' =>$input['feed_biz']['username'], 
                        'gdata' => array(
                            'fileurl' => $input['feed_shop_info']['url'][$type].$add_url,
                            'setting_fileurl' => $input['feed_shop_info']['url']['settings'].$add_url,
                            'ws_token'   => $input['feed_token'],
                        )
                )); 
    }
    
    public function unverify_site($user_id,$input){
        return true;
        $mysql_con = $this->mysql_db;
        if(empty($input)||!isset($input['feed_biz'])||!isset($input['feed_biz']['verified'])) return;
        $input['feed_biz']['verified'] = '';
        $today  = date('Y-m-d H:i:s', time());
        $ex = base64_encode(serialize($input['feed_biz']));
        $sql = "update configuration set value='$ex',config_date='$today' where id_customer = '$user_id' and name = 'FEED_BIZ' ";
        $mysql_con->select_query($sql);
    }
    
    public function check_ebay_config($user_id){
        $mysql_con = $this->mysql_db;
        $sql = "select id_customer from configuration where id_customer ='$user_id' and name in ('EBAY_TOKEN' ,'EBAY_USER') and value <> '' ";
        return $mysql_con->count_rows($mysql_con->select_query($sql));
        
    }

    public function get_amazon_update_site($user_id,$shop_id_list){
        return $this->getAmazonAllCron($user_id,$shop_id_list);
    }
    public function get_amazon_fba_update_site($user_id,$shop_id_list){
        return $this->getAmazonAllCron($user_id,$shop_id_list,false,true);
    }
    public function getAmazonAllCron($user_id,$shop_id_list,$all_cron=false,$fba=false){
        $mysql_con = $this->mysql_db;
        $add='';
        $add2='';
        if(!$all_cron  ){
            $add = " and c.cron_status = 2 ";
            $add2 = " and cs.status = 0 ";
        }
        if($fba){
                $sql = "select * from  cron_tasks c left join cronjob_status cs on   c.cron_id = cs.cron_id  and cs.id_customer = '$user_id' $add2 where    c.cron_marketplace = 'amazon' and c.cron_type = 'fba' $add and c.cron_status != 0";
        }else{
                $sql = "select * from  cron_tasks c left join cronjob_status cs on   c.cron_id = cs.cron_id  and cs.id_customer = '$user_id' $add2 where    c.cron_marketplace = 'amazon' $add and c.cron_status != 0";
        }
//        echo $sql."\n";
        $q = $mysql_con->select_query($sql);
        $deny = array();
        $all_action = array();
//        $cron_list = array();
        while($row = $mysql_con->fetch($q)){
            if($row['ext']==''){
                $row['ext']='all';
            }
            if($row['status']*1==0){
                $deny[$row['ext']][$row['cron_action']] = true;
            }else{
//                if($all_cron){
                    $deny[$row['ext']][$row['cron_action']] = false;
//                }else{
//
//                }
            }
            $deny['all'][$row['cron_action']]=false;

//            $cron_list[] = $row;
        }
        if($all_cron){
            foreach($deny['all'] as $kact=>$vx){
                foreach($deny as $ext=>$v){
                    if($kact=='all')continue;
                    if(!isset($deny[$ext][$kact])){
                        $deny[$ext][$kact]=$vx;
                    }
                }
            }
        }

        if($fba){
            $sql = "select u.user_name,ac.ext,ac.id_shop,ac.region,op.id_region from amazon_fba ac   ,users u ,offer_price_packages op ,user_package_details up where "
        . "    ac.fba_master_platform  = '1'    and  u.id = ac.id_customer and u.id = '$user_id' and ac.id_shop in ('".implode("','",$shop_id_list)."') "
        . " and op.ext_offer_sub_pkg = ac.ext and up.id_users = u.id and up.id_package = op.id_offer_price_pkg "
        . " group by ac.id_customer,id_country,id_shop ";
        }else{
            $sql = "select u.user_name,ac.ext,ac.id_shop,ac.countries ,ac.id_country,ac.cron_create_products,ac.cron_delete_products from amazon_configuration ac   ,users u ,offer_price_packages op ,user_package_details up where "
            . "    ac.active = '1'    and  u.id = ac.id_customer and u.id = '$user_id' and ac.id_shop in ('".implode("','",$shop_id_list)."') "
            . " and op.ext_offer_sub_pkg = ac.ext and up.id_users = u.id and up.id_package = op.id_offer_price_pkg "
            . " group by ac.id_customer,id_country,id_shop ";
        }
//        echo $sql."\n";
        $q = $mysql_con->select_query($sql);
        $out = array();
        while($row = $mysql_con->fetch($q)){
            $row['deny']=array();
            if(isset($deny[$row['ext']])){
                $row['deny']=$deny[$row['ext']];
            }else{
                if($all_cron){
                    $row['deny']=$deny['all'];
                }
            }
            if($fba){
                $out[$row['id_region']]=$row;
            }else{
                $out[]=$row;
            }
        }
        return $out;
    }
    
    
    
    public function getSiteByIdMargetplace($id_marketplace = null){
        
        $mysql_db = $this->mysql_db;  
        $data = array();
        $sql = "SELECT * FROM offer_price_packages op  "
	    . "LEFT JOIN offer_sub_packages sub ON (sub.id_offer_sub_pkg = op.id_offer_sub_pkg) and op.offer_pkg_type != '3' " ;  
	
        if(isset($id_marketplace) && !empty($id_marketplace))
            $sql .= " WHERE op.id_offer_pkg = ".(int)$id_marketplace." ; " ;  
        
        $query = $mysql_db->select_query($sql);
        while($row = $mysql_db->fetch($query)){ 
            if(isset($row['id_offer_sub_pkg']) && !empty($row['id_offer_sub_pkg']) && isset($row['ext_offer_sub_pkg']) && !empty($row['ext_offer_sub_pkg'])){
                $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['ext'] = isset($row['ext_offer_sub_pkg']) ? $row['ext_offer_sub_pkg'] : null;
                $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['domain'] = isset($row['domain_offer_sub_pkg']) ? $row['domain_offer_sub_pkg'] : null;
                $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_region'] = isset($row['id_region']) ? $row['id_region'] : null;
                $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['id_site_ebay'] = isset($row['id_site_ebay']) ? $row['id_site_ebay'] : null;
                $data[$row['id_offer_pkg']][$row['id_offer_sub_pkg']]['iso_code'] = isset($row['iso_code']) ? $row['iso_code'] : null;
            }
        }
        
        return $data;
    }
    
    public function getMargetplaceById($id_marketplace, $id_site = null){
        
        $mysql_db = $this->mysql_db;  
        $data = array();
        
        $sql_join = $sql_and = '';
        if(isset($id_site) && !empty($id_site)){
            $sql_join = "LEFT JOIN offer_price_packages opp ON op.id_offer_pkg = opp.id_offer_pkg ";
            if($id_marketplace==3){
                $sql_and = "AND opp.id_site_ebay = ".(int)$id_site." " ;
            }else{
                $sql_and = "AND opp.id_offer_sub_pkg = ".(int)$id_site." " ;
            }
            $data['id_site'] = $id_site;
        }        
        $sql = "SELECT * FROM offer_packages op $sql_join WHERE op.id_offer_pkg = ".(int)$id_marketplace." $sql_and AND op.is_marketplace = 1" ;         
        
        $query = $mysql_db->select_query($sql);
        
        while($row = $mysql_db->fetch($query)){ 
            $data['id_marketplace'] = $id_marketplace;            
            $data['name'] = $row['name_offer_pkg'];
            $data['ext'] = isset($row['ext_offer_sub_pkg']) ? $row['ext_offer_sub_pkg'] : null;
            $data['domain'] = isset($row['domain_offer_sub_pkg']) ? $row['domain_offer_sub_pkg'] : null;
        }
        
        return $data;
    }
    
    public function getAllUsers($include_test_user = false){
         	
	$today = date('Y-m-d');
	$yesterday = date('Y-m-d H:i:s', strtotime('-1 week'));
	$add='';
        if($include_test_user==false){
            $add = " and u.user_test_flag = '0' ";
        }
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                u.id,
                u.user_name,
                u.user_email,
                u.user_first_name,
                u.user_las_name,
		u.user_status,
		u.user_created_on,
                u.user_module_ver,
                c.name as cname,
                c.value as shop_info,
                opp.ext_offer_sub_pkg as ext,
                opp.domain_offer_sub_pkg as domain,              
                opp.id_offer_pkg as id_offer_pkg,
                op.id_offer_pkg as id_marketplace,
                op.name_offer_pkg as marketplace ,
                l.language_name as language
                from users u
                LEFT JOIN configuration c ON u.id = c.id_customer AND (c.name = 'FEED_SHOP_INFO')
                LEFT JOIN user_package_details upd ON u.id = upd.id_users
                LEFT JOIN offer_price_packages opp ON upd.id_package = opp.id_offer_price_pkg 
                LEFT JOIN offer_packages op ON op.id_offer_pkg = opp.id_offer_pkg
                LEFT JOIN languages l ON l.id = u.user_default_language
                where 1 $add 
                GROUP BY u.id, op.name_offer_pkg
                ORDER BY u.user_created_on DESC, u.id DESC;" ;
        
    	$query = $mysql_db->select_query($sql);   
        
	$data['active'] = 0;
	$data['inactive'] = 0;
	
    	while($row = $mysql_db->fetch($query)){
            
	    $Feedbiz = new FeedBiz(array($row['id']));
	    
	    if(!isset($data['users'][$row['id']]) && $row['user_status'] == 1)
		$data['active']++;
	    else if(!isset($data['users'][$row['id']]))
		$data['inactive']++;
	    
	    $data['users'][$row['id']]['id_customer'] = $row['id'] ;            
            $data['users'][$row['id']]['user_name'] = $row['user_name'];            
            $data['users'][$row['id']]['user_email'] = $row['user_email'];            
            $data['users'][$row['id']]['user_first_name'] = $row['user_first_name'];            
            $data['users'][$row['id']]['user_las_name'] = $row['user_las_name'];  
            $data['users'][$row['id']]['user_status'] = $row['user_status'];  
            $data['users'][$row['id']]['user_created_on'] = $row['user_created_on'];
            $data['users'][$row['id']]['user_module_ver'] = $row['user_module_ver'];
            $data['users'][$row['id']]['language'] = $row['language'];
	    $data['users'][$row['id']]['shop_info'] = unserialize(base64_decode($row['shop_info'])); 
	    
	    // get number of shop offer 
	    $shop = $Feedbiz->getDefaultShop($row['user_name']);
	    
	    if(isset($shop['id_shop']))
		$id_shop = $shop['id_shop'];
	    else 
		$id_shop = 1;
	    
	    $data['users'][$row['id']]['no_offer_shop'] = $Feedbiz->getNumberOfProduct($row['user_name'], $id_shop);
	    if(!isset($histtory['marketplace'][$row['id']][$row['id_marketplace']])) {
		if(!empty($row['marketplace'])) {

		    $Marketplaces = $this->getSiteByIdMargetplace($row['id_marketplace']);
		    
		    // get number of marketplace offer by id offer
		    switch($row['marketplace']){
			
			case 'Amazon' : 
			    $amazon = new AmazonMessageSummary($row['user_name']);
			    $no_offer_marketplace = 0;
                            $no_create_error_marketplace = 0;
			    $number_of_domain = 1;
			    if(isset($Marketplaces[$row['id_marketplace']])) {
				foreach($Marketplaces[$row['id_marketplace']] as $id_country => $Marketplace) {
				    $inventory = $amazon->getSummaryInventory($id_country, $id_shop);
				    $no_offer_marketplace = $no_offer_marketplace + $inventory ;
                                    $error = $amazon->getSummaryNumErrorLastDay($id_country, $id_shop);
				    $no_create_error_marketplace += $error ;
				    if($inventory > 0)
					$number_of_domain++;
				}
				$no_offer_marketplace = ceil($no_offer_marketplace / $number_of_domain);
			    }

			    $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_offer_marketplace'] = $no_offer_marketplace;
                            $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_create_error_marketplace'] = $no_create_error_marketplace;
			    break;
			    
			case 'eBay' : 
			    $error=null;
			    $no_offer_marketplace = 0;
			    if($this->getEbayTokenByUserID($row['id']))
			    {
				$id_site_ebays = $this->getEbaySiteByUserID($row['id']);

				if(isset($id_site_ebays)) {
                                    foreach ($id_site_ebays as $id_site_ebay) {
                                        $param = array($row['user_name'], $row['id'], $row['id_marketplace']);
                                        $ebayexport = new ebayexport($param);
                                        $result = $ebayexport->products_monitoring_user_report($id_site_ebay, $id_shop);
                                        if($result){
                                            $no_offer_marketplace = ($result > $no_offer_marketplace) ? $result : $no_offer_marketplace;
                                        }
                                        if(empty($error)){
                                            $error = $ebayexport->products_monitoring_create_error_report();
                                        }
                                    }
				}

				$data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_offer_marketplace'] = $no_offer_marketplace;
			    } else {
				$data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_offer_marketplace'] = 0;
			    }
                            
                            $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_create_error_marketplace'] = $error;
			    
			    break;
			    
			case 'google_shopping' : 
			    $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_offer_marketplace'] = 0;
                            $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_create_error_marketplace'] = 0;
			    break;
			case 'General' : 
			    $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_offer_marketplace'] = 0;
                            $data['users'][$row['id']]['marketplace'][$row['marketplace']]['no_create_error_marketplace'] = 0;
			    break;
		    }
		}
	    }
	    
	    $histtory['marketplace'][$row['id']][$row['id_marketplace']] = true;
	    
	    if(strtotime($yesterday) < strtotime($row['user_created_on']))
		$data['users'][$row['id']]['is_new'] = true;  
	    
    	}
	
    	return $data;
    }
    
    public function getAllUser($include_test_user = false){
        $add='';
        if($include_test_user==false){
            $add = " and u.user_test_flag = '0' ";
        }
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                u.id,
                u.user_name,
                u.user_email,
                u.user_first_name,
                u.user_las_name,
                c.name as cname,
                c.value as shop_info,
                opp.ext_offer_sub_pkg as ext,
                opp.domain_offer_sub_pkg as domain,
                opp.id_site_ebay as id_site,
                opp.id_offer_pkg as id_offer_pkg,
                op.id_offer_pkg as id_marketplace,
                op.name_offer_pkg as marketplace
                from users u
                LEFT JOIN configuration c ON u.id = c.id_customer
                INNER JOIN user_package_details upd ON u.id = upd.id_users
                INNER JOIN offer_price_packages opp ON upd.id_package = opp.id_offer_price_pkg 
                INNER JOIN offer_packages op ON op.id_offer_pkg = opp.id_offer_pkg
                where (c.name = 'FEED_SHOP_INFO') $add  
                GROUP BY u.id, op.name_offer_pkg 
                ORDER BY u.user_last_login DESC;" ;
        
    	$query = $mysql_db->select_query($sql);   
        
    	while($row = $mysql_db->fetch($query)){
            
            // check ebay token
            if((int)$row['id_offer_pkg'] == 3){
                if(!$this->getEbayTokenByUserID($row['id']))
                    continue;
            }          
            
            $data[$row['id']]['id_customer'] = $row['id'] ;            
            $data[$row['id']]['shop_info'] = unserialize(base64_decode($row['shop_info']));            
            $data[$row['id']]['user_name'] = $row['user_name'];            
            $data[$row['id']]['user_email'] = $row['user_email'];            
            $data[$row['id']]['user_first_name'] = $row['user_first_name'];            
            $data[$row['id']]['user_las_name'] = $row['user_las_name'];  
            $data[$row['id']]['language'] = $this->getUserDefaultLaguage($row['id']);
            $data[$row['id']]['marketplace'][$row['id_marketplace']]['name'] = $row['marketplace'];  
            $data[$row['id']]['marketplace'][$row['id_marketplace']]['id_marketplace'] = $row['id_marketplace'];               
    	}
    	
    	return $data;
    }
    
    public function getUserById($id_customer){
      
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                u.id,
                u.user_name,
                u.user_email,
                u.user_first_name,
                u.user_las_name,
                u.user_code
                from users u               
                where u.id = ".(int)$id_customer." ;" ;
        
    	$query = $mysql_db->select_query($sql);   
        
    	while($row = $mysql_db->fetch($query)){           
            $data['id_customer'] = $row['id'] ;            
            $data['user_name'] = $row['user_name'];            
            $data['user_email'] = $row['user_email'];            
            $data['user_first_name'] = $row['user_first_name'];            
            $data['user_las_name'] = $row['user_las_name'];  
            $data['user_code'] = $row['user_code'];
    	}
    	
    	return $data;
    }
    
    public function getUserByUsername($user_name){
      
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                u.id,
                u.user_name,
                u.user_email,
                u.user_first_name,
                u.user_las_name,             
                u.user_code              
                from users u               
                where u.user_name = '".$user_name."' ;" ;
        
    	$query = $mysql_db->select_query($sql);   
        
    	while($row = $mysql_db->fetch($query)){           
            $data['id_customer'] = $row['id'] ;            
            $data['user_name'] = $row['user_name'];            
            $data['user_email'] = $row['user_email'];            
            $data['user_first_name'] = $row['user_first_name'];            
            $data['user_las_name'] = $row['user_las_name'];  
            $data['user_code'] = $row['user_code'];  
    	}
    	
    	return $data;
    }
    
    public function getUserByUsercode($user_code){
      
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                u.id,
                u.user_name,
                u.user_email,
                u.user_first_name,
                u.user_las_name              
                from users u               
                where u.user_code = '".$user_code."' ;" ;
        
    	$query = $mysql_db->select_query($sql);   
        
    	while($row = $mysql_db->fetch($query)){           
            $data['id_customer'] = $row['id'] ;            
            $data['user_name'] = $row['user_name'];            
            $data['user_email'] = $row['user_email'];            
            $data['user_first_name'] = $row['user_first_name'];            
            $data['user_las_name'] = $row['user_las_name'];  
    	}
    	
    	return $data;
    }
    
    public function getEbayTokenByUserID($id_customer){
         
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT value from configuration WHERE name = 'EBAY_TOKEN' AND id_customer = $id_customer ; " ;
    	$query = $mysql_db->select_query($sql);
        
    	while($row = $mysql_db->fetch($query)){
            if(!isset($row['value']) && empty($row['value']))
                return false;
            $data = $row['value'];
    	}
        	
    	return $data;
    }
    public function getEbaySiteByUserID($id_customer){
         
	$mysql_db = $this->mysql_db;
    	$data = array();
	$sql = "SELECT value from ebay_configuration WHERE name = 'EBAY_SITE_ID' AND id_customer = $id_customer ; " ;
	$query = $mysql_db->select_query($sql);

	while($row = $mysql_db->fetch($query)){
	     if(!isset($row['value']) && empty($row['value']))
		    return false;
		$data[$row['value']] = $row['value'];
	}
	
    	return $data;
    }
    
    public function getAllEbayUser(){
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT
                    c.id_customer,
                    c2.value AS FEED_BIZ,
                    u.user_name,
    				u.user_email,
    				u.user_first_name,
    				u.user_las_name,
                    offer_price_packages.ext_offer_sub_pkg as ext,
                    offer_price_packages.domain_offer_sub_pkg as domain,
                    offer_price_packages.id_site_ebay as id_site
                    from configuration c
    				JOIN configuration c2 ON c.id_customer = c2.id_customer
    				JOIN users u ON u.id = c.id_customer
                    INNER JOIN user_package_details ON u.id = user_package_details.id_users
                    INNER JOIN offer_price_packages ON user_package_details.id_package = offer_price_packages.id_offer_price_pkg and offer_price_packages.id_offer_pkg = '3'
                    where c.name = 'EBAY_TOKEN' AND c2.name = 'FEED_BIZ'
    				ORDER BY u.user_last_login DESC" ;
    	$query = $mysql_db->select_query($sql);
    	
    	while($row = $mysql_db->fetch($query)){
    		$data[] = $row;
    	}
    	
    	return $data;
    }
    
    public function getEbaySiteDictionary(){
    	$mysql_db = $this->mysql_db;
    	$data = array();
    	$sql = "SELECT id_site, name FROM ebay_sites" ;
    	$query = $mysql_db->select_query($sql);    	
    	while($row = $mysql_db->fetch($query)){
    		$data[$row['id_site']] = $row;
    	}
    	
    	return $data;
    }
} 