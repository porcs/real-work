<?php

require_once(dirname(__FILE__).'/../../../libraries/ci_db_connect.php');

class FeedbizOTP {
    
    public function __construct() {
            $this->db = new ci_db_connect(); 
    }
     
    public function get_otp($id_customer) { 
        
            $query = $this->db->select_query('SELECT * FROM token WHERE user_code = "' . $id_customer . '" ; ');
            $data = array();

            while($row = $this->db->fetch($query)){ 
                $data  = $row;
            }

            return $data;
    }
    
    public function set_otp($data) {
        
            if(!$this->db->add_db('token', $data)) 
                    return(false);
            return(true);
    }
    
    public function del_otp($source, $request_id, $user_code) {
        
            if(!$this->db->del('token', 'user_code = "' . $user_code .'" AND source = "' . $source . '" AND request_id = "' . $request_id . '" ; '))
                    return(false);
            return(true);
    }
    
    public function generate_otp($source, $request_id, $token) {
        
            $rand = mt_rand(0, 999999999);
            $otp = md5($rand . $token);
            $data = array();
            $data['user_code'] = $token;
            $data['source'] = $source;
            $data['request_id'] = $request_id;
            $data['token'] = $otp;
            $data['status'] = 1;
            $data['date_add'] = date('Y-m-d H:i:s');

            if ($this->set_otp($data))
                return $data['token'];
            else
                return false;
    }

}