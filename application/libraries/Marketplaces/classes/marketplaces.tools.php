<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

class MarketplacesTools 
{	
	/* 
	 * Return Mixed; 
	 * $marketplace_id int: id marketplace;
	 * $all boolean: get all value, default are false;	 
	 */
	public static function getMarketplaceByID($marketplace_id, $all = false)
	{
		$mysql_db = new ci_db_connect();
		$sql = "SELECT * FROM offer_packages WHERE id_offer_pkg = '$marketplace_id' AND is_marketplace = '1' ; " ;
		
		$query = $mysql_db->select_query($sql);
		$data = array();

		while($row = $mysql_db->fetch($query))
		{ 
			$data['id'] = (int) $row['id_offer_pkg'];
			$data['name'] = $row['name_offer_pkg'];
			$data['status'] = $row['status_offer_pkg'];
		}
		
		if($all)
		{
			return $data;
		} else {
			return (string) $data['name']; // Marketplace name
		}
	}
	
	/* 
	 * Return Mixed; 
	 * $marketplace string: marketplace name;
	 * $all boolean: get all value, default are false; 
	 */	
	public static function getMarketplaceByName($marketplace, $all = false)
	{
		$mysql_db = new ci_db_connect();
		$sql = "SELECT * FROM offer_packages WHERE name_offer_pkg = '$marketplace' AND is_marketplace = '1' ; " ;
		
		$query = $mysql_db->select_query($sql);
		$data = array();

		while($row = $mysql_db->fetch($query))
		{ 
			$data['id'] = (int) $row['id_offer_pkg'];
			$data['name'] = $row['name_offer_pkg'];
			$data['status'] = $row['status_offer_pkg'];
		}
		
		if($all)
		{
			return $data;
		} else {
			return (int) $data['id']; // Marketplace id
		}
	}
	
	/* 
	 * Return array; 
	 * $id_country int: country id (id_offer_sub_kpg);
	 * $iso_code string: iso code (iso_code);
	 * $language_code string: language code (language_code);
	 * $name string(%): country name (title_offer_sub_kpg);
	 */
	public static function getCountry($id_country=null, $iso_code=null, $language_code=null, $name=null)
	{
		$mysql_db = new ci_db_connect();
		$where = '';
		
		if(isset($id_country) && !empty($id_country))
		{
			$where = " AND id_offer_sub_pkg = $id_country ";
		}
		
		if(isset($iso_code) && !empty($iso_code))
		{
			$where = " AND iso_code = $iso_code ";
		}
		
		if(isset($language_code) && !empty($language_code))
		{
			$where = " AND language_code = $language_code ";
		}
		
		if(isset($name) && !empty($name))
		{
			$where = " AND title_offer_sub_pkg LIKE '%$name%' ";
		}
		
		$sql = "SELECT * FROM offer_sub_packages WHERE status_offer_sub_pkg = 'active' $where; " ;
		
		$query = $mysql_db->select_query($sql);
		$data = array();
		
		while($row = $mysql_db->fetch($query))
		{ 
			$data['id_country']	=  $row['id_offer_sub_pkg'];
			$data['name']		=  $row['title_offer_sub_pkg'];
			$data['iso_code']	=  $row['iso_code'];
			$data['language_code']	=  $row['language_code'];
		}
		
		return ($data); 		
	}   
		
	/*
	 * Return string;
	 * $str string;
	 */
	public static function toKey($str)
	{
		$str = str_replace(
			array('-', ',', '.', '/', '+', '.', ':', ';', '>', '<', '?', '(', ')', '!'),
			array('_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm'), 
			$str
		);

		return strtolower(preg_replace('/[^A-Za-z0-9_]/', '', $str));
	}
	
	/*
	 * Return Mix;
	 * $value string;
	 */
	public static function is_serialized($value, &$result = null) {
            
		if (!is_string($value)) 
		{ 
			return false; 
		}
		
		if ($value === 'b:0;') 
		{
			$result = false;
			return true;
		}
		
		$length = strlen($value);
		$end = '';
		
		if(isset($value[0])) 
		{
			switch ($value[0]) 
			{
				case 's': if ($value[$length - 2] !== '"') return false;
				case 'b':
				case 'i':
				case 'd': $end .= ';';
				case 'a':
				case 'O': $end .= '}';
					if ($value[1] !== ':') return false;
					switch ($value[2]) {
						case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: break;
						default: return false;
					}
				case 'N': $end .= ';';
					if ($value[$length - 1] !== $end[0]) {
						return false;
					}
					break;
				default:
					return false;
			}
		}
		
		if (($result = @unserialize($value)) === false) 
		{
			$result = null;
			return false;
		}
		
		return true;
	}   
	
	public static function encode($configuration)
	{
		return base64_encode($configuration);//TODO: Validation: Configuration Requirement
	}

	public static function decode($configuration)
	{
		return base64_decode($configuration);//TODO: Validation: Configuration Requirement
	}
	
	public static function load($file, $language) 
	{
		$langfile = $file . '_lang.php';

		if (file_exists(dirname(__FILE__) . '/../../../' . 'language/' . $language . '/' . $langfile)) 
			include(dirname(__FILE__) . '/../../../' . 'language/' . $language . '/' . $langfile);
		else return;

		if (!isset($lang) || empty($lang)) 
			return;

		global $l;
		$l = $lang;
	}

	public static function l($line) 
	{
		global $l;
		return (empty($line) || !isset($l[$line])) ? $line : $l[$line];
	}
	
	public static function clean_strip_tags($html)
	{
		$text = $html;

		$text = str_replace(array('</li>', '</LI>'), "\n</li>", $text);
		$text = str_replace(array('<BR', '<br'), "\n<br", $text);

		$text = strip_tags($text);

		$text = str_replace('&#39;', "'", $text);

		$text = mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8');
		$text = str_replace('&nbsp;', ' ', $text);
//		$text = html_entity_decode($text, ENT_NOQUOTES, 'UTF-8');

		$text = str_replace('&', '&amp;', $text);
		$text = str_replace('"', "&#34;", $text);

		$text = preg_replace('#\s+[\n|\r]+$#i', '', $text); // empty
		$text = preg_replace('#[\n|\r]+#i', "\n", $text); // multiple-return
		$text = preg_replace('#(\s)\n+#i', "\n", $text); // multiple-return
		$text = preg_replace('#^[\n\r\s]#i', '', $text);

		$text = preg_replace('/[\x{0001}-\x{0009}]/u', '', $text);
		$text = preg_replace('/[\x{000b}-\x{001f}]/u', '', $text);
		$text = preg_replace('/[\x{0080}-\x{009F}]/u', '', $text);
		$text = preg_replace('/[\x{0600}-\x{FFFF}]/u', '', $text);

		$text = preg_replace('/\x{000a}/', "\n", $text);
		$text = preg_replace('/\n/', "<br />\n", $text);
		$text = preg_replace('/$/', "<br />\n\n", $text);

		return ($text);
	}

	public static function encodeText($string, $verySafe = false)
	{
		if ($verySafe)
		{
			$string = str_replace('’', "'", $string);
			$string = @utf8_encode(utf8_decode($string));
			$string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
			$string = self::stripInvalidXml($string);
			$string = str_replace('&#39;', "'", $string);
		}

		return (trim($string));
	}


	public static function stripInvalidXml($value)
	{
		$ret = '';
		$current = null;
		
		if (empty($value))
			return $ret;

		$length = strlen($value); //TODO: Multibyte dance, do not replace by Tools::strlen !
		
		for ($i = 0; $i < $length; $i++)
		{
			$current = ord($value{$i});
			if (($current == 0x9) || ($current == 0xA) ||($current == 0xD) ||(($current >= 0x20) 
			    && ($current <= 0xD7FF)) ||(($current >= 0xE000) 
			    && ($current <= 0xFFFD)) ||(($current >= 0x10000) 
			    && ($current <= 0x10FFFF)))
			{
				$ret .= chr($current);
			} else {
				$ret .= ' ';
			}
		}

		return $ret;
	}
	
	public static function ceil_time($time) 
	{
		date_default_timezone_set('UTC');
		$date = strtotime(date('Y-m-d', $time));
		$chk_date = strtotime(date('Y-m-d', $time + 43200));
		
		if ($date == $chk_date) {
			$out = $date;
		} else {
			$out = $chk_date;
		}
		
		return $out;
	}

        public static function diff_date($date_from, $date_to)
	{
                $startTimeStamp = strtotime($date_from);
                $endTimeStamp = strtotime($date_to);
                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                $numberDays = $timeDiff/86400;  // 86400 seconds in one day

                // and you might want to convert to integer
                return intval($numberDays);
        }
}