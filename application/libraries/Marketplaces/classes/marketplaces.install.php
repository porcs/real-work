<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MarketplacesInstall
{
	public $db;  
	public $tb_prefix = '';  
        public $all_tbl_name;
        public $marketplace_name = '';   
        protected $tbl_structure;
        protected $tbl_user_structure;
        protected $tbl_main_structure;
        public $addtional_main_tbl_field=array();
        protected $addtional_user_tbl_field=array();
        private $db_engine = " ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci";
        private $db_default_length = array('int'=>11,'bigint'=>20,'tinyint'=>4,'text'=>0,'datetime'=>0,'varchar'=>32);
	private $username = '';
        private $dir_tb_ver = '';
        private $tb_version = '';
        private $dbx = null;
        
        private $user_tb_ver ='';
	
	public function __construct($user, $marketplace) {
           
            require_once dirname(__FILE__).'/../../db.php';
            $this->db = new Db($user);
            $this->username = $user;
            $this->marketplace_name = $marketplace;
            $this->tb_prefix=$marketplace;//.'_'.$this->db_mid;
            //install main database
            $add='';
            if (php_sapi_name() == "cli") {
                $add="cli_";
            }
            include(dirname(__FILE__) . '/../../../../application/config/database.php');  
            $this->dbx = mysqli_connect($db['default']['hostname'],$db['default']['username'], $db['default']['password']) or die('Could not connect to server.' );
            mysqli_select_db($this->dbx,$db['default']['database'] ) or die('Could not select database.'); 
            $this->dir_tb_ver = USERDATA_PATH."json/{$add}{$this->tb_prefix}_main_tb_ver.txt";
            $this->define_main_db_structure($marketplace);
            $this->built_tables(true);
            //install user database
            
            $this->dir_tb_ver = USERDATA_PATH.$user."/json/{$add}{$this->tb_prefix}_tb_ver.txt";
            $this->define_db_structure($marketplace);
            $this->built_tables();
        }
        
        public function get_tb_version(){
             $this->tb_version = md5(json_encode($this->tbl_structure));
             return $this->tb_version;
        }
        
        public function compare_tb_version(){
            $this->get_tb_version();
            if(file_exists($this->dir_tb_ver)){
                $this->user_tb_ver = file_get_contents($this->dir_tb_ver ); 
            }else{
                return false;
            }
             if($this->user_tb_ver!=$this->tb_version){
                return false;
            }
            return true;
        }
        public function record_tb_version(){
            if(!file_exists(dirname($this->dir_tb_ver))){
                mkdir(dirname($this->dir_tb_ver),0777 , true);
            } 
            if($this->user_tb_ver!=$this->tb_version){
                if(is_writable($this->dir_tb_ver)){
                    file_put_contents($this->dir_tb_ver, $this->tb_version);
                }
            }
        }
        
        public function built_tables($main_db=false){ 
            if($this->compare_tb_version())return true;
            $sql = ""; 
            foreach($this->all_tbl_name as $tbname){   
                    $sql .= $this->alter_db_structure($tbname,$main_db); 
            }
            $this->record_tb_version();
            if(trim($sql)==''){
                return true;
            }else{
                
                $sql_list = explode(';',$sql);
                foreach($sql_list as $q){ 
                    if(trim($q)=='')continue;
                    if($main_db){ 
                        mysqli_query($this->dbx,$q);
                    }else{
                        $this->db->db_exec($q,false,false);
                    }
                } 
                if($main_db){
                    mysqli_close($this->dbx);
                }
            } 
        }
        
        public function create_table($tbname){
            if(!isset($this->tbl_structure[$tbname])){
                return '';
            }
             
            $primary_list = array();
            $column_list = array();
            foreach($this->tbl_structure[$tbname]['field'] as $field_name =>$info){ 
                $attr = $this->get_str_field_attr($tbname,$field_name,true);
                if($attr===false){ continue; }
                
                $column_list[$field_name] = trim(" $field_name $attr ");
                if((isset($info['primary']) && $info['primary']==true) 
                        || isset($info['auto_increment']) && $info['auto_increment']==true ){
                    $primary_list[$field_name]=$field_name;
                }
            }
            if(empty($column_list)) return '';
            if(!empty($primary_list)){
                $column_list['primary_key'] = " PRIMARY KEY (".  implode(',', $primary_list).")";
            }
            
            $sql = "CREATE TABLE `$tbname` (".implode(',',$column_list).") ".$this->db_engine." ;";
            return $sql;
            
        }
        
        public function alter_db_structure($tbname,$main_db){
            if(empty($tbname) || trim($tbname)==''){return '';} 
            $exists = false;
            if($main_db==true){
                $res = mysqli_query($this->dbx,"SHOW TABLES LIKE '$tbname';");
                if(mysqli_num_rows($res)>0){
                    $exists = true;
                }
            }else{
                $exists=$this->db->db_table_exists($tbname);
            }
            if(!$exists){ 
                $sql='';
                foreach($this->tbl_structure as $to_tb_name => $tb){  //check need to rename table?
                    if(isset($tb['alter_name']) ){
                        foreach($tb['alter_name'] as $target_name){
                            
                            $exists = false;
                            if($main_db==true){
                                $res = mysqli_query($this->dbx,"SHOW TABLES LIKE '$target_name';");
                                if(mysqli_num_rows($res)>0){
                                    $exists = true;
                                }
                            }else{
                                $exists=$this->db->db_table_exists($tbname);
                            }
                            
                            if($exists){
                                $sql = "RENAME TABLE `{$target_name}` TO `{$to_tb_name}`;"; 
                                break;
                            } 
                        }
                    }
                }
                
                if(!empty($sql)){// if not found rename ,create new table
                    return $sql;
                }else{ 
                    return $this->create_table($tbname);
                }
            }else{ //found table exists
                $sql = "SHOW COLUMNS FROM {$tbname}"; 
                
                if($main_db==true){
                    $res = mysqli_query($this->dbx,$sql);
                    $current_table_structure=array();
                    if($d = mysqli_fetch_assoc($res)){
                        $current_table_structure[]=$d;
                    }
                }else{
                    $current_table_structure = $this->db->db_query_str($sql);
                }
                
                foreach($current_table_structure as $k=>$v){ //check column attribute for alter add,modify or rename
                    $all_default_key = array_keys($v);
                    $tmp = explode('(',$v['Type']);
                    $v['length']=0;
                    if(isset($tmp[1])){
                        $v['length'] = trim($tmp[1],')');
                        $v['type'] = $tmp[0];
                    }else{
                        $v['type'] = $tmp[0];
                    }
                    $v['not_null'] = true;
                    if($v['Null']=='YES'){
                        $v['not_null']=false;
                    }
                    $v['primary'] = false;
                    if($v['Key']=='PRI'){
                        $v['primary']=true;
                    }
                    $v['auto_increment']=false;
                    if($v['Extra']=='auto_increment'){
                        $v['auto_increment']=true;
                    }
                    $v['default']=null;
                    if(!empty($v['Default'])){
                        $v['default']=$v['Default'];
                    }
                    
                    $current_table_structure[$v['Field']] = $v;
//                    
                    foreach($all_default_key as $key){
                        unset($current_table_structure[$v['Field']][$key]);
                    }
                        unset($current_table_structure[$k]);
                    
                    
                }
                
                $alter_auto_increment = false;
                $alter_primary = false;
                $primary_field=array();
                $auto_inc_field = '';
                $tb_field = $this->tbl_structure[$tbname]['field'];
                $alter_add_field = array();
                $alter_update_field = array();
                $alter_rename_field = array(); 
                $alter_sql='';
                //----start to check rename column
                foreach($current_table_structure as $from_name =>$v){
                    if(isset($tb_field[$from_name]))continue;
                    foreach($tb_field as $field_name => $info){
                        if(!isset($info['alter_name']))continue;
                        if(in_array($from_name, $info['alter_name'])){
                            $alter_rename_field[$from_name]=$field_name;
                            break;
                        }
                    }
                } 
                foreach($tb_field as $field_name => $info){
                    $is_pri=false;
                    $is_auto_inc=false;
                    if(isset($info['primary']) && $info['primary']===true){
                        $primary_field[$field_name]=$field_name;
                        $is_pri=true;
                    }else{
                        $info['primary'] = false;
                    }
                    if(isset($info['auto_increment']) && $info['auto_increment']===true){
                        $auto_inc_field=$field_name;
                        $primary_field[$field_name]=$field_name;
                        $is_auto_inc=true;
                    }else{
                        $info['auto_increment'] = false;
                    } 
                    if(!isset($info['not_null'])){
                        $info['not_null']=false;
                    }
                    if(!isset($info['default'])){
                        $info['default']=null;
                    }
                    if(!isset($info['length'])){
                        $info['length']=$this->db_default_length[$info['type']];
                    } 
                    //--------start to check add new column or update attribute
                    if(!isset($current_table_structure[$field_name]) && !isset($alter_rename_field[$field_name])){
                        if($is_pri==true || $is_auto_inc==true){ $alter_primary=true; }  
                        $alter_add_field[] = $field_name;
                    }else{
                        $tbl_str = $current_table_structure[$field_name];
                        foreach($info as $k => $v){
                            if($k!='alter_name' && isset($tbl_str[$k]) && $tbl_str[$k]!=$v){
                                if(($k=='primary' || $k=='auto_increment') && ($tbl_str[$k]==true || $v==true)){
                                    $alter_primary=true;
                                }  
                                $alter_update_field[]=$field_name;
                            }
                        } 
                    }
                }
                //---------start get sql for alter
                
                if($alter_primary===true){
                    $sql = "ALTER TABLE {$tbname} DROP PRIMARY KEY;";
                    $alter_sql.=$sql;  
                }   
                if(!empty($alter_rename_field)){
                    foreach($alter_rename_field as $from_name => $to_name){
                        $attr = $this->get_str_field_attr($tbname,$to_name);
                        if( $attr !== false){
                            $sql="ALTER TABLE `{$tbname}` CHANGE COLUMN `{$from_name}` `{$to_name}` {$attr};";
                            $alter_sql.=$sql;
                        } 
                    }
                } 
                if(!empty($alter_add_field)){
                    foreach($alter_add_field as $field_name){
                        if(in_array($field_name,$alter_rename_field))continue;
                        $attr = $this->get_str_field_attr($tbname,$field_name);
                        if( $attr !== false){
                            $sql="ALTER TABLE `{$tbname}` ADD COLUMN  `{$field_name}`  {$attr};";
                            $alter_sql.=$sql;
                        } 
                    }
                } 
                if(!empty($alter_update_field)){
                    foreach($alter_update_field as $field_name){
                        $attr = $this->get_str_field_attr($tbname,$field_name);
                        if( $attr !== false){
                            $sql="ALTER TABLE `{$tbname}` MODIFY COLUMN  `{$field_name}`  {$attr};";
                            $alter_sql.=$sql;
                        } 
                    }
                }
                if($alter_primary===true){ 
                    if(!empty($primary_field)){
                        $sql = "ALTER TABLE {$tbname} ADD PRIMARY KEY (".implode(',',$primary_field).");";
                        $alter_sql.=$sql; 
                    } 
                    if(!empty($auto_inc_field)){
                        $attr = $this->get_str_field_attr($tbname,$auto_inc_field,true);
                        $sql="ALTER TABLE `{$tbname}` MODIFY COLUMN  `{$auto_inc_field}`  {$attr};";
                        $alter_sql.=$sql;
                    }
                } 
                 
            }
            return $alter_sql;
        }
        
        public function get_str_field_attr($tbname,$field,$auto_increment=false){
             if(!isset($this->tbl_structure[$tbname]['field'][$field])){
                 return false;
             }
             $attr = $this->tbl_structure[$tbname]['field'][$field];
             $type = $attr['type'];
             $length = isset($attr['length'])?$attr['length']:$this->db_default_length[$type];
             $length = $length=='0'?'':"({$length})";
             $auto_inc = (isset($attr['auto_increment']) && $attr['auto_increment']===true)?'AUTO_INCREMENT':'';
             $not_null = (isset($attr['not_null']) && $attr['not_null']===true)?'NOT NULL':'';
             $default = isset($attr['default'])&&!empty($attr['default'])?" DEFAULT '".$attr['default']."' ":($not_null==true?'':'DEFAULT NULL');
             if(!$auto_increment){
                 $auto_inc='';
             }
             return " {$type}{$length} {$not_null} {$default} {$auto_inc} ";
        }
        
        public function define_db_structure($marketplace){
            $mkp_tables = array();
            $tb_prefix=$marketplace;
            $mkp_tables["{$tb_prefix}_profiles"] = 
                        array( 
                            'alter_name'=>array("{$tb_prefix}_profiles"),//example alter name for rename table when table not exists
                            'field'=>array(
                                'id_profile'=>array('type'=>'bigint','length'=>'20','not_null'=>true,'primary'=>true,
                                                    'auto_increment'=>true,'default'=>0,'alter_name'=>array('id_profile')),//example all parameters
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'is_default'=>array('type'=>'tinyint'),
                                'id_model'=>array('type'=>'bigint','not_null'=>true),
                                'name'=>array('type'=>'text'),
                                'out_of_stock'=>array('type'=>'int'),
                                'synchronization_field'=>array('type'=>'text'),
                                'title_format'=>array('type'=>'tinyint'),
                                'html_description'=>array('type'=>'tinyint'),
                                'description_field'=>array('type'=>'tinyint'),
                                'price_rules'=>array('type'=>'text'),
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_categories_selected"] = 
                        array( 
                            'field'=>array(
                                'id_category_selected'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'id_category'=>array('type'=>'int','not_null'=>true), 
                                'id_profile'=>array('type'=>'int'), 
                                'date_add'=>array('type'=>'datetime'), 
                            ),
                        );
            $mkp_tables["{$tb_prefix}_mapping_carriers"] = 
                        array( 
                            'field'=>array(
                                'id_mapping'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'id_carrier'=>array('type'=>'int','not_null'=>true), 
                                'mapping'=>array('type'=>'text'),
                                'other'=>array('type'=>'text'),
                                'type'=>array('type'=>'varchar','length'=>20), 
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_mapping_attributes"] = 
                        array( 
                            'field'=>array(
                                'id_mapping'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'id_attribute_group'=>array('type'=>'int','not_null'=>true),
                                'id_attribute'=>array('type'=>'int','not_null'=>true),
                                'is_color'=>array('type'=>'tinyint'),
                                'attribute_field'=>array('type'=>'text'),
                                'mapping'=>array('type'=>'text'), 
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_reports"] = 
                        array( 
                            'field'=>array(
                                'id_log'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'batch_id'=>array('type'=>'varchar','not_null'=>true),
                                'sku'=>array('type'=>'text'),
                                'id_message'=>array('type'=>'int'),
                                'id_feed'=>array('type'=>'varchar'),
                                'result_code'=>array('type'=>'text'),
                                'result_message'=>array('type'=>'varchar'), 
                                'result_description'=>array('type'=>'text'),  
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_logs"] = 
                        array( 
                            'field'=>array(
                                'id_log'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'batch_id'=>array('type'=>'varchar','not_null'=>true),
                                'action_type'=>array('type'=>'text'),
                                'no_send'=>array('type'=>'int'),
                                'no_skipped'=>array('type'=>'varchar'), 
                                'no_process'=>array('type'=>'int'), 
                                'no_process'=>array('type'=>'int'), 
                                'no_success'=>array('type'=>'int'),  
                                'no_error'=>array('type'=>'int'), 
                                'no_warning'=>array('type'=>'int'), 
                                'is_cron'=>array('type'=>'tinyint'), 
                                'detail'=>array('type'=>'text'), 
                                'date_upd'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_logs_details"] = 
                        array( 
                            'field'=>array(
                                'id_log'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'batch_id'=>array('type'=>'varchar','not_null'=>true),
                                'action_type'=>array('type'=>'text'),
                                'action_process'=>array('type'=>'text'),
                                'sku'=>array('type'=>'text'), 
                                'message'=>array('type'=>'text'),  
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_log_products_skip"] = 
                        array( 
                            'field'=>array(
                                'batch_id'=>array('type'=>'varchar','length'=>'64','not_null'=>true),
                                'reference'=>array('type'=>'varchar','length'=>'32'),
                                'id_shop'=>array('type'=>'int'),
                                'message'=>array('type'=>'text'),
                                'date_add'=>array('type'=>'datetime'), 
                            ), 
                        );
            $mkp_tables["{$tb_prefix}_mapping_features"] = 
                        array( 
                            'field'=>array(
                                'id_mapping'=>array('type'=>'bigint','not_null'=>true,'primary'=>true,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true),
                                'id_feature'=>array('type'=>'int','not_null'=>true),
                                'id_feature_value'=>array('type'=>'int','not_null'=>true),
                                'is_color'=>array('type'=>'tinyint'),
                                'attribute_field'=>array('type'=>'text'), 
                                'mapping'=>array('type'=>'text'),  
                                'date_add'=>array('type'=>'datetime'), 
                            ),
                        );
            $mkp_tables["{$tb_prefix}_models"] = 
                        array( 
                            'field'=>array(
                                'id_model'=>array('type'=>'bigint','not_null'=>true,'primary'=>true ,'auto_increment'=>true),
                                'id_country'=>array('type'=>'int','not_null'=>true ),
                                'id_shop'=>array('type'=>'int','not_null'=>true ),
                                'name'=>array('type'=>'text' ),
                                'product_universe'=>array('type'=>'text' ),
                                'product_type'=>array('type'=>'text'),
                                'variation_theme'=>array('type'=>'text'), 
                                'specific_fields'=>array('type'=>'text'),  
                                'date_add'=>array('type'=>'datetime'), 
                            ),
                            'version'=>'1.01'
                        );
            $this->all_tbl_name = array_keys($mkp_tables);
            $this->tbl_user_structure = $mkp_tables;
            $this->tbl_structure = $mkp_tables;
            if(!empty($this->addtional_user_tbl_field)){
                $this->additional_db_field($this->addtional_user_tbl_field,false,$marketplace);
            }
            return $this->tbl_structure;
        }
  
        public function define_main_db_structure($marketplace){
            $mkp_tables = array();
            $tb_prefix=$marketplace;
            $mkp_tables["{$tb_prefix}_configurations"] = 
                        array( 
                            'alter_name'=>array("{$tb_prefix}_configurations"),//example alter name for rename table when table not exists
                            'field'=>array(
                                'id_customer'=>array('type'=>'int','length'=>'11','not_null'=>true,'primary'=>true,
                                                    'auto_increment'=>false,'default'=>null,'alter_name'=>array('id_customer')),//example all parameters
                                'user_name'=>array('type'=>'text','not_null'=>true),
                                'id_shop'=>array('type'=>'int','not_null'=>true,'primary'=>true),
                                'id_marketplace'=>array('type'=>'int','primary'=>true),
                                'id_country'=>array('type'=>'int','primary'=>true),
                                'ext'=>array('type'=>'varchar','length'=>10),
                                'countries'=>array('type'=>'text'),
                                'currency'=>array('type'=>'varchar','length'=>3),
                                'id_lang'=>array('type'=>'int','length'=>11),
                                'iso_code'=>array('type'=>'varchar','length'=>3),
                                'id_region'=>array('type'=>'varchar','length'=>10),
                                'active'=>array('type'=>'tinyint'),
                                'api_key'=>array('type'=>'text'),
                                'token_key'=>array('type'=>'text'),
                                'secret_key'=>array('type'=>'text'),
                                'merchant_key'=>array('type'=>'text'),
                                'merchant_token'=>array('type'=>'text'), 
                            ), 
                            'version'=>'1.0'
                        );
           
            $this->all_tbl_name = array_keys($mkp_tables);
            $this->tbl_main_structure = $mkp_tables;
            $this->tbl_structure = $mkp_tables;
            if(!empty($this->addtional_main_tbl_field)){
                $this->additional_db_field($this->addtional_main_tbl_field,true,$marketplace);
            }
            return $this->tbl_structure;
        }
        
        public function additional_db_field( $add_field,$main=false,$marketplace='' ){  
            if(!is_array($add_field) || empty($add_field)){
                return false;
            }
            if(empty($marketplace)){
                $marketplace = $this->tb_prefix;
            }
            if($main){  
                $type = 'tbl_main_structure';
            }else{ 
                $type = 'tbl_user_structure'; 
            } 
            foreach($add_field as $tbl_name => $data){
                $tmp = $this->$type;
                
                if(!isset($data['field'])){ 
                    return false;
                } 
                
                if(!isset($tmp[$tbl_name])){ 
                    return false;
                }
                $this->tbl_structure[$tbl_name] = $tmp[$tbl_name];
                $this->tbl_structure[$tbl_name]['field']= array_merge($this->tbl_structure[$tbl_name]['field'],$data['field']);
            }
             
             
        }
}