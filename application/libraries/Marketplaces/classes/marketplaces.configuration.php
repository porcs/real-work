<?php

require_once(dirname(__FILE__) . '/marketplaces.database.php');

class MarketplacesConfiguration extends MarketplacesDatabase
{	
	public function __construct($user, $marketplace) {
		
		parent::__construct($user, $marketplace);
		
		$this->user = $user;
		$this->marketplace = $marketplace;
	}

       /** e.g : 
        * $restriction = array(
                'id_model'	=> array ('operation' => '=' ,'value' => $value['id_model']),
                'id_shop'	=> array ('operation' => '=' ,'value' => $value['id_shop']),
                'id_country'	=> array ('operation' => '=' ,'value' => $value['id_country']),
        );*/
        public function get_data($table, $restriction=array(), $where_operation="AND")
	{
		$mysql_db = new ci_db_connect();
                
		$sql = "SELECT * FROM ".$table." ";

                if(is_array($restriction) && !empty($restriction)) {

                        $where = array();
                        
                        foreach ($restriction as $key => $values)
                        {
                                $operation = $values['operation'];
                                $value = $values['value'];
                                
                                if($value === '' || is_null($value)){
                                        $value = 'NULL';
                                } else if(is_int($value)) {
                                        $value = (int)$value;
                                } else if(is_float($value)) {
                                        $value = (float)$value;
                                } else {
                                        $value = $mysql_db->escape_str($value);
                                }
                                
                                $where[] =  $key . " $operation " . $value;

                        }

                        $query_string = ' WHERE ' . implode(" $where_operation ", $where) ;

                        $sql .= $query_string ;
                }

//                var_dump($sql); exit;
		$rows = $mysql_db->db_array_query($sql);

		foreach ($rows as $row)
		{
			return $row;
		}

	}

        public function set_data($table, $data)
	{
		if (!empty($data))
		{
			if(!$this->save($table, $data))
				return false;
		}
		return true;
	}	
        
	public function getParameters($id_shop, $id_country)
	{
		$mysql_db = new ci_db_connect();  
                $user_name = $mysql_db->escape_str($this->user);
                $id_country = (int)$id_country;
                $id_shop = (int)$id_shop;
		$sql = "SELECT * FROM ".$this->marketplace."_configurations ac 
			LEFT JOIN users u ON (u.id = ac.id_customer)
			LEFT JOIN languages l ON (l.id = u.user_default_language)
			WHERE /*ac.active = 1 and*/ ac.user_name = '".$user_name."' and ac.id_country = ".$id_country." and ac.id_shop = ".$id_shop."" ;
		
		$rows = $mysql_db->db_array_query($sql);  
		
		foreach ($rows as $row)
		{
			return $row;
		}
		
	}	
	    	
	public function setParameters($data)
	{	
		if(!isset($data['id_customer']) || !isset($data['user_name']) || !isset($data['id_shop']) || !isset($data['id_country']) || !isset($data['ext']))
			return (false);
		
		$filte_data = array();
		$mysql_db = new ci_db_connect();
		
		$filter_key = array('id_customer', 'user_name', 'id_shop', 'id_marketplace', 'id_country', 'ext', 'countries', 'currency', 
		    'id_lang', 'iso_code', 'id_region', 'active', 'api_key', 'token_key', 'secret_key', 'merchant_key', 'merchant_token');
		
		// Fixed value
		foreach ($filter_key as $key => $value)
		{
			$filte_data[$value] = isset($data[$value]) ? $data[$value] : null ;
		}
		
		// Additional value 
		foreach ($data as $key => $value)
		{
			if(!in_array($key, $filter_key))
			{
				$filte_data[$key] = !empty($value) ? $value : null ;
			}
		}
		
		$where = array(
			'id_customer' => $data['id_customer'], 
			'ext' => $data['ext'], 
			'id_shop' => $data['id_shop']
		);
		$mysql_db->where($where);
		$mysql_db->get( $this->marketplace.'_configurations' );
		
		if($mysql_db->affected_rows() != 0)
		{
			$result = $mysql_db->update($this->marketplace.'_configurations', $filte_data, $where);
		} else {
			$result = $mysql_db->add_db($this->marketplace.'_configurations', $filte_data);
		}

		if($result)  
		{
			return (true);
		}

		return (false);
	}

	public function getModels($id_shop, $id_country, $id_model=null)
	{		
		$profiles = $where = array();
		$where['id_shop'] = $id_shop;
		$where['id_country'] = $id_country;
		
		if(isset($id_model) && !empty($id_model))
			$where['id_model'] = $id_model;
		
		$result = $this->get('models', $where);
		
		foreach ($result as $values) 
		{
			$keys = $values['id_model'];

			foreach ($values as $key => $value) 
			{
				if (MarketplacesTools::is_serialized($value)) 
				{
					$profiles[$keys][$key] = unserialize($value);
				} else {
					$profiles[$keys][$key] = $value;
				}
			}			
			
		}
		
		return $profiles;
	}
		
	public function setModels($id_shop, $id_country, $data)
	{
		$date_add = date('Y-m-d H:i:s');
		$pass = true; 		
		
		if (isset($data) && !empty($data)) 
		{
			foreach ($data as $key => $value) 
			{
                                unset($values);
				$values[$key] = $value;
				$values[$key]['id_shop'] = $id_shop;
				$values[$key]['id_country'] = $id_country;
				$values[$key]['date_add'] = $date_add;
				
				if(isset($value['id_model']) && !empty($value['id_model'])) // we will update when item has id_model 
				{
					$where = array(
						'id_model'	=> array ('operation' => '=' ,'value' => $value['id_model']),
						'id_shop'	=> array ('operation' => '=' ,'value' => $value['id_shop']),
						'id_country'	=> array ('operation' => '=' ,'value' => $value['id_country']),
					);
					
					unset($values[$key]['id_model']);
					if(!$this->update('models', $values, $where))
						$pass = false;
				} 
				else 
				{		
					if(!$this->save('models', $values))
						$pass = false;
				}
			}
			
		}
		
		if($pass)
			return (true);
		else 
			return (false);
	}
		
	public function deleteModels($id_shop, $id_country, $id_profile)
	{
		$where = array();
		$where['id_shop'] = array("operation" => " = ", "value" => (int) $id_shop);
		$where['id_country'] = array("operation" => " = ", "value" => (int) $id_country);
		$where['id_model'] = array("operation" => " = ", "value" => (int) $id_profile);

		return $this->delete('models', $where);
	}
	
	public function getProfiles($id_shop, $id_country, $id_profile=null)
	{		
		$profiles = $where = array();
		$where['id_shop'] = $id_shop;
		$where['id_country'] = $id_country;
		
		if(isset($id_profile) && !empty($id_profile))
			$where['id_profile'] = $id_profile;
		
		$result = $this->get('profiles', $where);
		
		foreach ($result as $values) 
		{
			$keys = $values['id_profile'];

			foreach ($values as $key => $value) 
			{
				if (MarketplacesTools::is_serialized($value)) 
				{
					$profiles[$keys][$key] = unserialize($value);
				} else {
					$profiles[$keys][$key] = $value;
				}
			}			
			
		}
		
		return $profiles;
	}
		
	public function setProfiles($id_shop, $id_country, $data)
	{
		$date_add = date('Y-m-d H:i:s');
		$pass = true;
		
		if (isset($data) && !empty($data)) 
		{
			foreach ($data as  $key => $value) 
			{
                                unset($values);
				$values[$key] = $value;
				$values[$key]['id_shop'] = $id_shop;
				$values[$key]['id_country'] = $id_country;
				$values[$key]['date_add'] = $date_add;
				
				if(isset($value['id_profile']) && !empty($value['id_profile'])) // we will update when item has id_profile 
				{
					$where = array(
						'id_profile'	=> array ('operation' => '=' ,'value' => $value['id_profile']),
						'id_shop'	=> array ('operation' => '=' ,'value' => $value['id_shop']),
						'id_country'	=> array ('operation' => '=' ,'value' => $value['id_country']),
					);
					
					unset($values[$key]['id_profile']);
					if(!$this->update('profiles', $values, $where))
						$pass = false;
				} 
				else 
				{
				    
					if(!$this->save('profiles', $values))
						$pass = false;
				}
			}
			
		}
		
		if($pass)
			return (true) ;
		else
			return (false) ;
	}		
		
	public function deleteProfiles($id_shop, $id_country, $id_profile)
	{
		$where = array();
		$where['id_shop'] = array("operation" => " = ", "value" => (int) $id_shop);
		$where['id_country'] = array("operation" => " = ", "value" => (int) $id_country);
		$where['id_profile'] = array("operation" => " = ", "value" => (int) $id_profile);

		return $this->delete('profiles', $where);
	}	
        
        public function getProductOption($id_shop, $id_country, $id_product=null)
	{		
		$product_option = $where = array();
		$where['id_shop'] = $id_shop;
		$where['id_country'] = $id_country;
		
		if(isset($id_product) && !empty($id_product))
			$where['id_product'] = $id_product;

                $this->prefix = 'google_shopping_';
		$result = $this->get('product_option', $where);
                $this->prefix = 'google_';
		
		foreach ($result as $values) 
		{
			$keys = $values['id_product'];

			foreach ($values as $key => $value) 
			{
				if (MarketplacesTools::is_serialized($value)) 
				{
					$product_option[$keys][$key] = unserialize($value);
				} else {
					$product_option[$keys][$key] = $value;
				}
			}			
			
		}
		
		return $product_option;
	}
	
	public function getMappingCarriers($id_shop, $id_country, $id_carrier=null)
	{
		$content = array();
		$carrier = '';
		
		if (isset($id_carrier) && !empty($id_carrier))
		    $carrier = " AND id_carrier = " . $id_carrier . " ";
		
		$sql = "SELECT MAX(c.id_carrier) as id_carrier,
			c.id_carrier_ref as id_carrier_ref,
			c.id_tax as id_tax,
			c.name as name,
			mc.id_mapping as id_mapping,
			mc.mapping as mapping,
			mc.type as type,mc.other as other,
			CASE WHEN mc.id_carrier IS NOT NULL THEN 'true' ELSE null END AS selected 
			FROM " . $this->product . "carrier c
			LEFT JOIN " . $this->prefix . "mapping_carriers mc ON (mc.id_carrier = c.id_carrier_ref OR mc.id_carrier = c.id_carrier) AND mc.id_shop = c.id_shop
			WHERE c.id_shop = " . $id_shop . "  
				" . $carrier . " 
				AND mc.id_country = " . $id_country . "
				AND mc.id_shop = " . $id_shop . " 
			GROUP BY mc.id_mapping
			ORDER BY c.name ASC";
		
		$result = $this->db->db_query_str($sql);
		
		foreach ($result as $value)
		{
		    $keys = $value['id_carrier'];
		    foreach ($value as $key => $val) 
		    {
			if (!is_int($key))
			{
			    $content[$keys][$key] = $val;
			}
		    }
		}
		
		return $content;
		
	}	
	
	public function setMappingCarriers($id_shop, $id_country, $data)
	{
		$values = $where = array();
		$table = 'mapping_carrier';
		$date_add = date('Y-m-d H:i:s');
		
		foreach ($data as $key => $value) 
		{    
			$values[$key] = $value;
			$values[$key]['id_shop'] = (int)$id_shop;
			$values[$key]['id_country'] = (int)$id_country;
			$values[$key]['date_add'] = $date_add;
		}

		if (!empty($values)) 
		{
			if(!$this->save($table, $values))
				return false;
			
			$where['id_shop'] = array("operation" => " = ", "value" => (int)$id_shop);
			$where['id_country'] = array("operation" => " = ", "value" => (int)$id_country);
			$where['date_add'] = array("operation" => " < ", "value" => $date_add);
		}

		return $this->delete($table, $where);
	}	
	
	public function getSelectedCategories($id_shop, $id_country, $id_category=null)
	{
		$content = $where = array();
		
		if (isset($id_shop) && !empty($id_shop)) 
		{
			$where['id_shop'] = $id_shop;
		}
		
		if (isset($id_country) && !empty($id_country)) 
		{
			$where['id_country'] = $id_country;
		}
		
		if (isset($id_category) && !empty($id_category)) 
		{
			$where['id_category'] = $id_category;
		}
		
		$result = $this->get('categories_selected', $where);
		
		foreach ($result as $value) 
		{
			$id = $value['id_category'];
			
			$content[$id]['id_category_selected'] = $value['id_category_selected'];
			$content[$id]['id_category'] = $value['id_category'];
			$content[$id]['id_country'] = $value['id_country'];
			$content[$id]['id_shop'] = $value['id_shop'];
			$content[$id]['id_profile'] = $value['id_profile'];
		}
		
		return $content;
	}
	
	public function setSelectedCategories($id_shop, $id_country, $data)
	{   
		$values = $where = array();
		$date_add = date('Y-m-d H:i:s');
		$table = "categories_selected";

		foreach ($data as $key => $value) 
		{
			if (!isset($value['id_category']) && empty($value['id_category'])) 
				continue;
			$values[$key] = $value;			
			$values[$key]['id_shop'] = $id_shop;
			$values[$key]['id_country'] = $id_country;
			$values[$key]['date_add'] = $date_add;
		}
		
		if (!empty($values)) 
		{
			if(!$this->save($table, $values))
				return false;
		}

                $where['id_shop'] = array("operation" => " = ", "value" => (int)$id_shop);
                $where['id_country'] = array("operation" => " = ", "value" => (int)$id_country);
                $where['date_add'] = array("operation" => " < ", "value" => $date_add);

		return $this->delete($table, $where);
	}
	
	public function getMappingsAttributes($id_shop, $id_country, $attribute_field=null, $id_attribute_group=null, $id_attribute=null, $id_mapping=null )
	{   
		$content = array();
		$lang = $attribute_group = $attribute = $attribute_field_fields = $mapping = '';
		
		$id_lang = $this->checkLanguage($id_shop, $id_country, true);
		
		if (isset($id_lang) && !empty($id_lang)) 
		{
			$lang = " AND agl.id_lang = " . $id_lang . " AND al.id_lang = " . $id_lang;
		}
		
		if (isset($attribute_field) && !empty($attribute_field)) 
		{
			$attribute_field_fields = " AND ma.attribute_field = '" . $attribute_field . "' ";
		}
		
		if (isset($id_attribute_group) && !empty($id_attribute_group)) 
		{
			if (is_array($id_attribute_group)) 
			{
				$attribute_group = " AND ag.id_attribute_group IN (" . implode(',', $id_attribute_group) . ") ";
			} else {
				$attribute_group = " AND ag.id_attribute_group = " . $id_attribute_group . " ";
			}
		}
		
		if (isset($id_attribute) && !empty($id_attribute)) 
		{
			$attribute = " AND al.id_attribute = " . $id_attribute . " ";
		}
		
		if (isset($id_mapping) && !empty($id_mapping)) 
		{
			$mapping = " AND ma.id_mapping = " . $id_mapping . " ";
		}
		
		$sql = "SELECT 
			    agl.id_attribute_group as id_attribute_group, 
			    al.id_attribute as id_attribute, 
			    agl.name as 'name', 
			    al.name as attribute, 
			    ma.id_mapping as id_mapping, 
			    ma.attribute_field as attribute_field, 
			    ma.mapping as mapping, 
			    CASE WHEN ma.is_color IS NOT NULL THEN ma.is_color ELSE ag.is_color_group END AS is_color, 
			    CASE WHEN ma.id_attribute_group IS NOT NULL AND ma.id_attribute IS NOT NULL THEN 'true' ELSE NULL END AS selected 
			FROM " . $this->prefix . "categories_selected cs 
			LEFT JOIN " . $this->product . "product p ON p.id_category_default = cs.id_category AND p.id_shop = cs.id_shop 
			LEFT JOIN " . $this->product . "product_attribute pa ON pa.id_product = p.id_product AND pa.id_shop = p.id_shop  
			LEFT JOIN " . $this->product . "product_attribute_combination pac ON pac.id_product_attribute = pa.id_product_attribute AND pac.id_shop = pa.id_shop 
			LEFT JOIN " . $this->product . "attribute_group ag ON ag.id_attribute_group = pac.id_attribute_group AND ag.id_shop = pac.id_shop 
			LEFT JOIN " . $this->product . "attribute_group_lang agl ON agl.id_attribute_group = pac.id_attribute_group AND agl.id_shop = pac.id_shop 
			LEFT JOIN " . $this->product . "attribute_lang al ON al.id_attribute = pac.id_attribute AND al.id_shop = pac.id_shop 
			LEFT JOIN " . $this->prefix . "mapping_attributes ma ON ma.id_attribute_group=pac.id_attribute_group 
				    AND ma.id_attribute=pac.id_attribute AND ma.id_shop=pac.id_shop 
			WHERE cs.id_country = " . $id_country . " 
			    AND cs.id_shop = " . $id_shop . " 
			    " . $lang . " 
			    " . $attribute_group . " 
			    " . $attribute . "  
			    " . $attribute_field_fields . " 
			    " . $mapping . " 
			GROUP BY agl.id_attribute_group, al.id_attribute, ma.attribute_field, agl.id_lang            
			ORDER BY agl.name ASC";

		$result = $this->db->db_query_str($sql);
		
		foreach ($result as $keys => $value) 
		{
			foreach ($value as $key => $val) 
			{
				if (!is_int($key)) 
				{
					if (MarketplacesTools::is_serialized($val)) 
					{
						$content[$keys][$key] = unserialize($val);
						
					} else {
						$content[$keys][$key] = $val;
					}
				}
			}
		}
		
		return $content;
	}
	
	public function setMappingsAttributes($id_shop, $id_country, $data)
	{   
		$values = $where = array();
		$table = 'mapping_attributes';
		$date_add = date('Y-m-d H:i:s');
		$key = 0;
		
		foreach ($data as $id_attribute_group => $attributes) 
		{    
			$is_color = 0;
			foreach ($attributes as $id_attribute => $attribute) 
			{    
				foreach ($attribute as $attribute_field => $mapping) 
				{    
					if ($attribute_field == 'Color') 
					{
						$is_color = 1;
					}
					if (!empty($mapping)) 
					{
						$values[$key]['id_shop'] = (int)$id_shop;
						$values[$key]['id_country'] = (int)$id_country;
						$values[$key]['id_attribute_group'] = (int)$id_attribute_group;
						$values[$key]['id_attribute'] = (int)$id_attribute;
						$values[$key]['is_color'] = (int)$is_color;
						$values[$key]['attribute_field'] = trim($attribute_field);
						$values[$key]['mapping'] = trim($mapping);
						$values[$key]['date_add'] = $date_add;
						$key++;
					}
				}
			}
		}

		if (!empty($values)) 
		{
			if(!$this->save($table, $values))
				return false;
			
			$where['id_shop'] = array("operation" => " = ", "value" => (int)$id_shop);
			$where['id_country'] = array("operation" => " = ", "value" => (int)$id_country);
			$where['date_add'] = array("operation" => " < ", "value" => $date_add);
		}

		return $this->delete($table, $where);	
	}	
	
	public function getMappingsFeatures($id_shop, $id_country, $attribute_field=null, $id_feature=null, $id_feature_value=null, $id_mapping=null )
	{   

		$content = array();
		$lang = $feature_group = $feature = $attribute_field_fields = $mapping = '';
		
		$id_lang = $this->checkLanguage($id_shop, $id_country, true);
		
		if (isset($id_lang) && !empty($id_lang)) 
		{
			$lang = "  AND f.id_lang = " . $id_lang . " AND fv.id_lang = " . $id_lang;
		}
		
		if (isset($attribute_field) && !empty($attribute_field)) 
		{
			$attribute_field_fields = " AND mf.attribute_field = '" . $attribute_field . "' ";
		}
		
		if (isset($id_feature_value) && !empty($id_feature_value)) 
		{
			$feature = " AND fv.id_feature_value = " . $id_feature_value . " ";
		}
		
		if (isset($id_feature) && !empty($id_feature)) 
		{
			if (is_array($id_feature)) 
			{
				$list_id_feature_value = array();
				$sql_feature = "SELECT id_feature_value FROM " . $this->product . "product_feature 
						WHERE id_shop = " . $id_shop . " AND id_feature IN (" . implode(',', $id_feature) . ") GROUP BY id_feature_value ";
				$result_feature = $this->db->db_query_str($sql_feature);
				
				foreach ($result_feature as $value_feature) 
				{
					$list_id_feature_value[] = (int)$value_feature['id_feature_value'];
				}
				
				if(!empty($list_id_feature_value))
				{
					$feature = " AND fv.id_feature_value IN (" . implode(',', $list_id_feature_value) . ") ";
				}
				
				$feature_group = " AND f.id_feature IN (" . implode(',', $id_feature) . ") ";
				
			} else {
			    
				$feature_group = " AND f.id_feature = " . $id_feature . " ";
				
			}
		}
		
		if (isset($id_mapping) && !empty($id_mapping)) 
		{
			$mapping = " AND ma.id_mapping = " . $id_mapping . " ";
		}
		
		
		$sql = "SELECT 
			    f.id_feature as id_feature,
			    f.name as 'name',
			    fv.id_feature_value as id_feature_value,
			    fv.value as 'value',
			    mf.attribute_field as attribute_field,
			    mf.mapping as mapping,
			    CASE WHEN mf.is_color IS NOT NULL THEN mf.is_color ELSE 0 END AS is_color,
			    CASE WHEN mf.id_feature IS NOT NULL AND mf.id_feature_value IS NOT NULL THEN 'true' ELSE NULL END AS selected
			FROM " . $this->prefix . "categories_selected acs 
			LEFT JOIN " . $this->product . "product p ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop 
			LEFT JOIN " . $this->product . "product_feature pf ON pf.id_product = p.id_product AND pf.id_shop = p.id_shop
			LEFT JOIN " . $this->product . "feature f ON f.id_feature = pf.id_feature AND f.id_shop = pf.id_shop
			LEFT JOIN " . $this->product . "feature_value fv ON fv.id_feature_value = pf.id_feature_value AND fv.id_shop = pf.id_shop
			LEFT JOIN " . $this->prefix . "mapping_features mf ON mf.id_feature = pf.id_feature 
				AND mf.id_feature_value = pf.id_feature_value AND mf.id_shop = pf.id_shop AND mf.id_country = acs.id_country
			WHERE acs.id_country = $id_country 
			AND acs.id_shop = $id_shop
			" . $lang . "  
			" . $feature_group . "  
			" . $feature . "  
			" . $attribute_field_fields . " 
			" . $mapping . " 
			GROUP BY f.id_feature, fv.id_feature_value, mf.attribute_field, f.id_lang 
			ORDER BY f.name ASC ; ";
		
		$result = $this->db->db_query_str($sql);
		
		foreach ($result as $keys => $value) 
		{
			foreach ($value as $key => $val) 
			{
				if (!is_int($key)) 
				{
					if (MarketplacesTools::is_serialized($val)) 
					{
						$content[$keys][$key] = unserialize($val);
						
					} else {
						$content[$keys][$key] = $val;
					}
				}
			}
		}
		
		return $content;
	}
	
	public function setMappingsFeatures($id_shop, $id_country, $data)
	{   
		$values = $where = array();
		$table = 'mapping_features';
		$date_add = date('Y-m-d H:i:s');
		$key = 0;
		
		foreach ($data as $id_feature => $features) 
		{    
			$is_color = 0;
			foreach ($features as $id_feature_value => $feature) 
			{    
				foreach ($feature as $attribute_field => $mapping) 
				{    
					if ($attribute_field == 'Color') 
					{
						$is_color = 1;
					}
					if (!empty($attr)) 
					{
						$values[$key]['id_shop'] = (int)$id_shop;
						$values[$key]['id_country'] = (int)$id_country;
						$values[$key]['id_feature'] = (int)$id_feature;
						$values[$key]['id_feature_value'] = (int)$id_feature_value;
						$values[$key]['is_color'] = (int)$is_color;
						$values[$key]['attribute_field'] = trim($attribute_field);
						$values[$key]['mapping'] = trim($attr);
						$values[$key]['date_add'] = $date_add;
						$key++;
					}
				}
			}
		}

		if (!empty($values)) 
		{
			if(!$this->save($table, $values))
				return false;
			
			$where['id_shop'] = array("operation" => " = ", "value" => (int)$id_shop);
			$where['id_country'] = array("operation" => " = ", "value" => (int)$id_country);
			$where['date_add'] = array("operation" => " < ", "value" => $date_add);
		}

		return $this->delete($table, $where);	
	}

        public function getLogs($id_shop, $id_country, $start, $limit, $order_by, $search, $numrow=false){

                $logs = $where = array();
                $sql = "SELECT * FROM ".$this->prefix."logs WHERE id_shop=$id_shop AND id_country=$id_country ";

		if(isset($search) && !empty($search))
                {
                    $sql .= " AND (batch_id LIKE '%$search%' OR action_type LIKE '%$search%' OR date_upd LIKE '%$search%' ) ";
                }

                if(isset($order_by) && !empty($order_by)){
                    $sql .= "ORDER BY $order_by ";
                }

                if(isset($start) && isset($limit)){
                    $sql .= "LIMIT $limit OFFSET $start ";
                }

                if($numrow){
                    $result = $this->db->db_sqlit_query($sql);
                    $row = $this->db->db_num_rows($result);
                    return $row;
                } else {
                    $result = $this->db->db_query_str($sql);
                    foreach ($result as $keys => $values)
                    {
                            foreach ($values as $key => $value)
                            {
                                    if (MarketplacesTools::is_serialized($value))
                                    {
                                            $logs[$keys][$key] = unserialize($value);
                                    } else {
                                            $logs[$keys][$key] = $value;
                                    }
                            }
                    }

                    return $logs;
                }
        }

        public function getReports($id_shop, $id_country, $start, $limit, $order_by, $search, $numrow=false){

                $report = $where = array();
                $sql = "SELECT * FROM ".$this->prefix."reports WHERE id_shop=$id_shop AND id_country=$id_country ";
		if(isset($search) && !empty($search))
                {
                    $sql .= " AND (batch_id LIKE '%$search%' OR sku LIKE '%$search%' OR result_code LIKE '%$search%' OR result_message LIKE '%$search%' OR result_description LIKE '%$search%' OR date_add LIKE '%$search%' ) ";
                }

                if(isset($order_by) && !empty($order_by)){
                    $sql .= "ORDER BY $order_by ";
                }

                if(isset($start) && isset($limit)){
                    $sql .= "LIMIT $limit OFFSET $start ";
                }

                if($numrow){
                    $result = $this->db->db_sqlit_query($sql);
                    $row = $this->db->db_num_rows($result);
                    return $row;
                } else {
                    $result = $this->db->db_query_str($sql);
                    foreach ($result as $keys => $values)
                    {
                            foreach ($values as $key => $value)
                            {
                                    if (MarketplacesTools::is_serialized($value))
                                    {
                                            $report[$keys][$key] = unserialize($value);
                                    } else {
                                            $report[$keys][$key] = $value;
                                    }
                            }
                    }

                    return $report;
                }
        }

        public function getLogProductsSkip($id_shop, $start, $limit, $order_by, $batch_id, $numrow=false){

                $logs = $where = array();
                $sql = "SELECT * FROM ".$this->prefix."log_products_skip WHERE id_shop=$id_shop ";

		if(isset($batch_id) && !empty($batch_id))
                {
                    $sql .= " AND batch_id = '$batch_id' ";
                }

                if(isset($order_by) && !empty($order_by)){
                    $sql .= "ORDER BY $order_by ";
                }

                if(isset($start) && isset($limit)){
                    $sql .= "LIMIT $limit OFFSET $start ";
                }
               
                if($numrow){
                    $result = $this->db->db_sqlit_query($sql);
                    $row = $this->db->db_num_rows($result);
                    return $row;
                } else {
                    $result = $this->db->db_query_str($sql);
                    foreach ($result as $keys => $values)
                    {
                            foreach ($values as $key => $value)
                            {
                                    if (MarketplacesTools::is_serialized($value))
                                    {
                                            $logs[$keys][$key] = unserialize($value);
                                    } else {
                                            $logs[$keys][$key] = $value;
                                    }
                            }
                    }

                    return $logs;
                }
        }

        public function setLogs($id_shop, $id_country, $batch_id, $action_type=null, $no_send=null, $no_skipped=null, $no_process=null, $no_success=null, $no_error=null, $no_warning=null, $is_cron=null, $detail=array())
	{
                $values = array();
		$table = 'logs';

                $values['id_country'] = (int)$id_country;
                $values['id_shop'] = (int)$id_shop;
                $values['batch_id'] = $batch_id;
                $values['action_type'] = $action_type;
                $values['no_send'] = $no_send;
                $values['no_skipped'] = $no_skipped;
                $values['no_process'] = $no_process;
                $values['no_success'] = $no_success;
                $values['no_error'] = $no_error;
                $values['no_warning'] = $no_warning;
                $values['is_cron'] = $is_cron;
                $values['detail'] = !empty($detail) ? (is_array($detail) ? serialize($detail) : $detail): '';
                $values['date_upd'] = date('Y-m-d H:i:s');

                return (bool) $this->save($table, array($values));
        }
        
        public function setLogProductsSkip($id_shop, $reference, $batch_id=null, $message=null)
	{
                $values = array();
                $values['batch_id'] = $batch_id;
                $values['reference'] = $reference;
                $values['id_shop'] = (int)$id_shop;
                $values['message'] = $message;
                $values['date_add'] = date('Y-m-d H:i:s');
                return $values;
        }

        public function saveLogProductsSkip($data)
	{
		$table = 'log_products_skip';
                return (bool) $this->set_data($table, $data);
        }
}
