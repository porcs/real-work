<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/marketplaces.tools.php');
require_once(dirname(__FILE__) . '/../../FeedBiz.php');

class MarketplacesDatabase
{
	public $prefix;
	public $product;
	public $offer;
	
	public function __construct($user, $marketplace) {
		
		// set prefix
		$this->prefix = MarketplacesTools::toKey($marketplace) . '_';
		$this->product = 'products_';
		$this->offer = 'offers_';
		
		// open database
		$this->db = new Db($user);
		
		$this->feedbiz = new FeedBiz(array($user));
	}
	
	
	public function checkLanguage($id_shop, $id_country, $only_id_lang=false, $only_language_code=false) 
	{
		$country = MarketplacesTools::getCountry($id_country);
		
		if(isset($country['language_code']) && !empty($country['language_code'])) 
		{
			$language = $this->feedbiz->checkLanguage($country['language_code'], $id_shop);			
		}
		
		if(!isset($language) || empty($language))
		{
			$lang_default = $this->feedbiz->getLanguageDefault($id_shop);
			
			if (isset($lang_default) && !empty($lang_default)) 
			{
				foreach ($lang_default as $ld)
				{
					$language = $ld;
				}
				
			} else {
				return (false);
			}
		}
		
		if($only_id_lang)
		{
			return isset($language['id_lang']) ? (int)$language['id_lang'] : (false);
		}
		
		if($only_language_code)
		{
			return isset($language['iso_code']) ? $language['iso_code'] : (false);
		}
		
		return $language;
	}
	
	/*
	 * Return array;
	 * $table string: table name;
	 * $fields null/array: selected field;
	 * $where null/array: where field;
	 * $orderBy string: order by field;
	 */
	public function get($table, $where=array(), $fields=array(), $orderBy=null)
	{
		$this->_from($this->prefix . $table);
		
		if (!empty($fields))
			$this->db->select($fields);		
		
		if (!empty($where)) 
			$this->db->where($where);
		
		if (isset($orderBy) && !empty($orderBy))
			$this->db->orderBy($orderBy);        

		return $this->db->db_array_query($this->db->query);        
	}
	
	/*
	 * Return boolean;
	 * $table string: table name;
	 * $data array: data field;
	 */
	public function save($table, $data) 
	{
		$sql = '';
		
		if (isset($data) && !empty($data)) 
		{
			if(is_array($data) ) {
			    
				foreach ($data as $d) 
				{
					$sql .= $this->_replace_string($this->prefix . $table, $d);
				}
				
			} else {
			    
				$sql .= $this->_replace_string($this->prefix . $table, $data);
				
			}
				
		}
		
		return $this->_exec_query($sql);
	}
	
	public function delete($table, $where = array())
	{
		$sql = $this->_delete_string($this->prefix . $table, $where) ;
		return $this->_exec_query($sql);
	}
	
	public function update($table, $data, $where = array())
	{
		$sql = '';
		
		if (isset($data) && !empty($data)) 
		{
			if(is_array($data) ) {
			    
				foreach ($data as $d) 
				{
					$sql .= $this->_update_string($this->prefix . $table, $d, $where);
				}
				
			} else {
			    
				$sql .= $this->_update_string($this->prefix . $table, $data, $where);
			}			
		}
		
		return $this->_exec_query($sql);
	}		

	private function _from($table, $alias=null, $no_prefix=true)
	{
		return  $this->db->from($table, $alias, $no_prefix);
	}

	private function _leftJoin($table, $alias=null, $on=null, $no_prefix=true)
	{
		return  $this->db->leftJoin($table, $alias, $on, $no_prefix);
	}    
	
	public function _replace_string($table, $data, $multi_value = false, $no_prefix = true)
	{
		if (isset($data) && !empty($data)) 
		{
			$filter_data = $this->_filter_data($table, $data);
			return $this->db->replace_string($table, $filter_data, $multi_value, $no_prefix);                   
		}
		
		return null;
	}

	private function _update_string($table, $data, $where = array(), $limit = 0, $no_prefix = true) 
	{
		if (isset($data) && !empty($data)) 
		{
			return $this->db->update_string($table, $data, $where, $limit, $no_prefix);
		}
		
		return null;
	}

	private function _insert_string($table, $data, $no_prefix = true) 
	{
		if (isset($data) && !empty($data))
		{
			$filter_data = $this->_filter_data($table, $data);
			return $this->db->insert_string($table, $filter_data, $no_prefix);
		}
		return null;
	}

	private function _delete_string($table, $where = array(), $no_prefix = true) 
	{
		return $this->db->delete_string($table, $where, $no_prefix);
	}
	
	private function _truncate_table($table, $id_shop=null, $id_country=null, $where_fields=null, $no_prefix=true) 
	{
		$where = '';
		
		if (isset($id_shop) && !empty($id_shop)) 
		{
			$where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . ' id_shop = ' . $id_shop . ' ';
		}
		
		if (isset($id_country) && !empty($id_country))
		{
			$where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . 'id_country = ' . $id_country . ' ';
		}
		
		if (isset($where_fields) && !empty($where_fields))
		{
			$where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . $where_fields;
		}

		return $this->db->truncate_table($table . $where . " ; ", $no_prefix);
	}

	private function _exec_query($sql, $transaction=true, $multi_query=true, $direct=false, $commit=true, $asyn=false) 
	{
		return $this->db->db_exec($sql, $transaction, $multi_query, $direct, $commit, $asyn);        
	}
	
	private function _filter_data($table, $data) 
	{
		$filtered_data = array();
		$columns = $this->_list_fields($table);
		
		if (is_array($data)) 
		{
			foreach ($columns as $column) 
			{
				if (array_key_exists($column, $data)) 
				{
					if (is_array($data[$column])) 
					{
						$filtered_data[$column] = serialize($data[$column]);
					} else {
						if ($data[$column] == '0') 
						{
							$filtered_data[$column] = $data[$column];
						} else {
							$filtered_data[$column] = $data[$column];
						}
					}
				} else {
					$filtered_data[$column] = '';
				}
			}
		}
		
		return $filtered_data;
	}

	private function _list_fields($table) 
	{
		return $this->db->get_all_column_name($table, true, true);
	}    

	private function _table_exists($table, $no_prefix = true)
	{
		return $this->db->db_table_exists($table, $no_prefix);
	}
}
