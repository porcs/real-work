<form class="form-horizontal col-sm-8 treeview" action="?active=panel_categories" role="form" method='POST'>
    {function profiles_select } 
        <select name='profile_cat[{$category}]'>
            <option value='' ></option>
            {foreach $data as $profile}
                <option value='{$profile['id']}' {if in_array($category,$profile['categories'])}selected{/if} >{$profile['name']}</option>
            {/foreach}
        </select>
    {/function}

    <ul class="level list-group">
        {function menu level=0 parent=0} 
            {foreach $categories as $cat}
                {*$cat->id_parent*}
                {if $cat->lv >= $level && $parent == $cat->id_parent}
                    <li class="list-group-item">
                        {if $level > 0}
                            {for $var=1 to $level}
                                <span class="indent"></span>
                            {/for}
                        {/if}
                        <span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>
                        {$cat->name->$iso_code}
                        {profiles_select data=$profiles category=$cat->id}</li>
                        {menu data=$cat level=$level+1 parent=$cat->id}
                    {else}                                        

                {/if}
            {/foreach}
        {/function}
    </ul>

    {menu data=$categories}
    <button type="submit" class="btn btn-primary">
        Save
    </button>

</form>