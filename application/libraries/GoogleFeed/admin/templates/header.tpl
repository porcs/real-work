<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin</title>

        <link href="templates/css/bootstrap.min.css" rel="stylesheet">
        <link href="templates/css/select2.css" rel="stylesheet">
        <link href="templates/css/select2-bootstrap.css" rel="stylesheet">
        <link href="templates/css/style.css" rel="stylesheet">

    </head>
    <body>