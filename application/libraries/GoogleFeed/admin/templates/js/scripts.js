jQuery(function() {
//    $(".profile_select, .google_fields").select2({
//        theme: "bootstrap",
//        placeholderOption: function() {
//            return undefined;
//        }
//    });

    $('.back').click(function() {
        window.location.href = window.location.href + "&back";
    });

    $('.open-profile').click(function(e) {
        e.preventDefault();
        var url = location.origin + location.pathname + $(this).attr('href');
        window.open(url);
    });

    $('.google_fields').change(function() {
        var id = this.id.replace(/(\[.*\])/g, "");
        console.log(this);
        if ($(this).val() == 'add_text') {
            $('#' + id + '\\[text\\]').attr("disabled", false).removeClass('hidden');
            //$( '#' + id + '\\[text\\] input[name="' + id + '[item]"]' ).attr( "disabled", false ).removeClass( 'hidden' );
        } else if (!$('#' + id + '\\[text\\]').hasClass("hidden")) {
            $('#' + id + '\\[text\\]').attr("disabled", true).addClass('hidden');
            //$( '#' + id + '\\[text\\] input[name="' + id + '[item]"]' ).attr( "disabled", true ).addClass( 'hidden' );
        }
    });

    $('.profile_select').change(function() {
        var id = this.id.replace(/(\[.*\])/g, "")
        if ($(this).val() == 'add_text') {
            //$( '#' + id + '\\[text\\]' ).removeClass( 'hidden' );
            $('#' + id + '\\[text\\] input[name="' + id + '[combinations]"]').attr("disabled", false).removeClass('hidden');
        } else if ($('#' + id + '\\[text\\]').css('display') == 'block') {
//            $( '#' + id + '\\[text\\]' ).addClass( 'hidden' );
            $('#' + id + '\\[text\\] input[name="' + id + '[combinations]"]').attr("disabled", true).addClass('hidden');
        }
    });

    checkGroupCheckboxes();
    $('#radio3,#radio4').change(function() {
        checkGroupCheckboxes();
    });

});


function checkGroupCheckboxes()
{
    if ($("#radio3").prop("checked"))
    {
        //$('#form_group_rules')
       $('.profile_grouping').stop().show("slow");
    } else
    {
        $('.profile_grouping').stop().hide("slow");
    }

}