<form class="form-horizontal" role="form" method='POST'>
    {if isset($profile['id']) }<input type="hidden" class="form-control" name="profile[id]" value="{$profile['id']}">{/if}
   <div class="form-group">
        <label for="inputProfile3" class="col-sm-2 control-label">Profile name</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="profile[name]" value='{if isset($profile['name']) }{$profile['name']}{/if}' id="inputProfile3">
        </div>
    </div>
{*   <div class="form-group">
        <label for="inputTitle3" class="col-sm-2 control-label">Title</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="profile[title]" value='{if isset($profile['data']['title']) }{$profile['data']['title']}{/if}'  id="inputTitle3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputDescripcion3" class="col-sm-2 control-label">Description</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="profile[description]" value='{if isset($profile['data']['description']) }{$profile['data']['description']}{/if}' id="inputDescripcion3">
        </div>
    </div>
    <div class="form-group">
        <label for="inputLink3" class="col-sm-2 control-label">Link</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="profile[link]" value='{if isset($profile['data']['link']) }{$profile['data']['link']}{/if}' id="inputLink3">
        </div>
    </div>*}


   {* <pre>{print_r($g_fields, true)}</pre>*}
    {foreach $g_fields as $google_group=>$field}
        {if $google_group=="Item Groupings"}
            <h3 class="delimeter">Group?</h3>
            <div class="form-group ">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <div class="funkyradio">
                        <div class="col-sm-2">
                            <div class="funkyradio-success">
                                <input type="radio" value="Yes" {if isset($profile['data']['group']) and $profile['data']['group']=="Yes" }checked{/if} name="profile[group]" id="radio3" />
                                <label for="radio3">Yes</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="funkyradio-danger">
                                <input type="radio" value="No" {if isset($profile['data']['group']) and $profile['data']['group']=="No" }checked{/if} name="profile[group]" id="radio4" />
                                <label for="radio4">No</label>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        {/if}            
        <h3 class="delimeter">{$google_group}</h3>
        <div class="form-group ">
            <div class="col-sm-2">
                <h4 class="delimeter">Google feed attribute</h4>
            </div>
            <div class=" col-sm-3">
                <h4 class="delimeter">Mapping for single Item</h4>
            </div>
            <div class=" col-sm-3">
                {*                <h4 class="delimeter">Mapping for Combinations</h4>*}
                <h4 class="delimeter">Default value</h4>
            </div>
        </div>
        {foreach $field as $l=>$f}
            <div class="form-group {if $f['required']} required {/if}">
                <label for="{$l}[item]" class="col-sm-2 control-label">{$l}:</label>
                <div class=" col-sm-3">
                    {$c=0}
                    {$d=0}
                    {if (isset($profile['data']['item_mapping'][$l]))&&($profile['data']['item_mapping'][$l]) }
                        {if $profile['data']['item_mapping'][$l]['type']=="text"}
                            {$c=$profile['data']['item_mapping'][$l]['value']}
                            {$d=true}
                        {elseif $profile['data']['item_mapping'][$l]['type']=="array_text"}
                            {$c=$profile['data']['item_mapping'][$l]['value']}
                            {$d=true}
                        {else}
                            {$a=$profile['data']['item_mapping'][$l]['type']}
                            {$b=$profile['data']['item_mapping'][$l]['value']}
                            {$c="`$a`|`$b`"}
                        {/if}
                    {/if}
                    <select class="form-control google_fields" {if (isset($f['multiselect']))} multiple name="{$l}[item][]" {else} name="{$l}[item]"  {/if}  id="{$l}[item]"  {if $f['required']} required {/if}>
                        <option value=""> </option>

                        {if isset($f['not_default'])}
                            {$d=false}
                            {foreach $f['values'] as $item_key=>$item}
                                {*<option  value="">{$c} {$item}</option>*}
                                
                               {if !is_array($c)}
                                <option {if ($c)&&($item_key==$c)} selected {/if} value="{$item_key}">{$item}</option>
                                {else}
                                  <option {if (in_array($item_key,$c))} selected {/if} value="{$item_key}">{$item}</option>
                                {/if}
                            {/foreach}
                        {else}
                            <option value="add_text" {if $d==true}selected {/if}>Default value</option>

                            <optgroup label="Fields">
                                {foreach $item_fields as $if}
                                    {$check="item_field|`$if`"}
                                    <option {if ($c)&&($check==$c) } selected {/if} value="item_field|{$if}">{$if}</option>
                                {/foreach}
                            </optgroup>
                            <optgroup label="Features">
                                {foreach $item_features as $k=>$if}
                                    {$check="feature|`$k`"}
                                    <option {if ($c)&&($check==$c) } selected {/if} value="feature|{$k}">{$if['name']}</option>
                                {/foreach}
                            </optgroup>

                            <optgroup label="Attributes">
                                {foreach $item_attributes as $k=>$if}
                                    {$check="attribute|`$k`"}
                                    <option {if ($c)&&($check==$c) } selected {/if} value="attribute|{$k}">{$if['name']}</option>
                                {/foreach}
                            </optgroup>
                            <optgroup label="Combination Fields">
                                {foreach $item_comb_names as $if}
                                    {$check="comb_field|`$if`"}
                                    <option {if ($c)&&($check==$c) } selected {/if}  value="comb_field|{$if}">{$if}</option>
                                {/foreach}
                            </optgroup>

                        {/if}

                    </select>

                </div>
                <div class="col-sm-3">
                    {if isset($f['values'])}
                        <select class="form-control {if $d!=true}hidden{/if}" id="{$l}[text]" name="{$l}[item]" id="{$l}[text]" {if $d!=true}disabled{/if} {if $f['required']} required {/if}>

                            {foreach $f['values'] as $item}
                                {*<option  value="">{$c} {$item}</option>*}
                                <option {if ($c)&&($item==$c) } selected {/if} value="{$item}">{$item}</option>

                            {/foreach}
                        </select>
                    {else}
                        <input type="text" value="{if $d==true}{$profile['data']['item_mapping'][$l]['value']}{/if}" name="{$l}[item]" id="{$l}[text]" class="form-control {if $d!=true}hidden{/if}" {if $d!=true}disabled{/if} {if $f['required']} required {/if}>
                    {/if}
                </div>
                {*
                <div class=" col-sm-3">

                {$c=0}
                {$dc=0}
                {if (isset($profile['data']['comb_mapping'][$l]))&&($profile['data']['comb_mapping'][$l]) }
                {if $profile['data']['comb_mapping'][$l]['type']=="text"}
                {$c=$profile['data']['comb_mapping'][$l]['value']}
                {$dc=true}
                {else}
                {$a=$profile['data']['comb_mapping'][$l]['type']}
                {$b=$profile['data']['comb_mapping'][$l]['value']}
                {$c="`$a`|`$b`"}
                {/if}
                {/if}

                {*{$item_features|@print_r} *}
                {*<select class="form-control profile_select" id="{$l}[combinations]" name="{$l}[combinations]" {if $f['required']} required {/if}>
                <option value=""> </option>

                {if isset($f['values'])}
                {$dc=false}
                {foreach $f['values'] as $item}
                <option {if ($c)&&($item==$c) } selected {/if} value="{$item}">{$item}</option>
                {/foreach}
                {else}
                <option value="add_text" {if $dc==true}selected {/if}>Default value</option>
                <optgroup label="Attributes">
                {foreach $item_attributes as $k=>$if}
                {$check="attribute|`$k`"}
                <option {if ($c)&&($check==$c) } selected {/if} value="attribute|{$k}">{$if['name']}</option>
                {/foreach}
                </optgroup>
                <optgroup label="Fields">
                {foreach $item_comb_names as $if}
                {$check="comb_field|`$if`"}
                <option {if ($c)&&($check==$c) } selected {/if}  value="comb_field|{$if}">{$if}</option>
                {/foreach}
                </optgroup>
                <optgroup label="Parent Fields">
                {foreach $item_fields as $if}
                {$check="item_field|`$if`"}
                <option {if ($c)&&($check==$c) } selected {/if} value="item_field|{$if}">{$if}</option>
                {/foreach}
                </optgroup>
                <optgroup label="Parent Features">
                {foreach $item_features as $k=>$if}
                {$check="feature|`$k`"}
                <option {if ($c)&&($check==$c) } selected {/if} value="feature|{$k}">{$if['name']}</option>
                {/foreach}
                </optgroup>
                {/if}
                </select>

                </div>*}
                {if $google_group=="Item Groupings"}
                    <div class="col-sm-3 profile_grouping">
                        <div class="funkyradio">

                            <div class="funkyradio-success">

                                <input type="checkbox" name="profile[grouping][{$l}]" id="checkbox{$l}" {if isset($profile['data']['grouping'][$l])}checked{/if}/>
                                <label for="checkbox{$l}">Grouping by {$l}?</label>
                            </div>
                        </div>
                    </div>
                {/if}
                <input type="hidden" name="{$l}[tag]" id="{$l}[tag]" value="{$f['tag']}">
            </div>
            {*            <div id="{$l}[text]" class="{if $d!=true && $dc!=true}hidden{/if} form-group {if $f['required']} required {/if}">
            <label for="{$l}[text]" class="col-sm-2 control-label">Default value:</label>
            <div class=" col-sm-3">
            <input type="text" value="{if $d==true}{$profile['data']['item_mapping'][$l]['value']}{/if}" name="{$l}[item]" style="margin-top:5px" class="form-control {if $d!=true}hidden{/if}" {if $d!=true}disabled{/if} {if $f['required']} required {/if}>
            </div>
            <div class=" col-sm-3">
            <input type="text" value="{if $dc==true}{$profile['data']['comb_mapping'][$l]['value']}{/if}" name="{$l}[combinations]" style="margin-top:5px" class="form-control {if $dc!=true}hidden{/if}" {if $dc!=true}disabled{/if} {if $f['required']} required {/if}>
            </div>
            </div>*}
        {/foreach}
    {/foreach}

    <h3 class="delimeter">Export Shipping(carrier) ?</h3>
    <div class="form-group ">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <div class="funkyradio">
                <div class="col-sm-2">
                    <div class="funkyradio-success">
                        <input type="radio" value="Yes" {if isset($profile['data']['shipping']) and $profile['data']['shipping']=="Yes" }checked{/if} name="profile[shipping]" id="radio5" />
                        <label for="radio5">Yes</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="funkyradio-danger">
                        <input type="radio" value="No" {if !isset($profile['data']['shipping']) or $profile['data']['shipping']=="No" }checked{/if} name="profile[shipping]" id="radio6" />
                        <label for="radio6">No</label>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">

            <button  class="btn btn-default back">
                To profiles
            </button>
            <button type="submit" name='save_profile' class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>
