<form class="form-horizontal" action="?active=panel_profiles" role="form" method='POST'>
    <div class="span7">   
        <div class="widget stacked widget-table action-table">

            <button type="submit" name='add_profile' class="btn btn-primary" style="margin:5px 0">
                <i class="glyphicon glyphicon-plus"> Create new profile</i>	
            </button>
            <span class="languages">
                {foreach $exist_langs as $lang}
                    <a href="?profile_lang={$lang['iso_code']}" target="_blank" name='open_profile_xml[{$lang['id_lang']}]' class="btn btn-default open-profile">
                        <i class="glyphicon glyphicon-save"></i> {$lang['iso_code']}
                    </a>
                {/foreach}
            </span>
            <div class="common">
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Common settings</h3>
                </div> <!-- /widget-header -->

                <div class="form-group">
                    <label for="inputTitle3" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[title]" value='{if isset($common['title']) }{$common['title']}{/if}'  id="inputTitle3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputDescripcion3" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[description]" value='{if isset($common['description']) }{$common['description']}{/if}' id="inputDescripcion3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLink3" class="col-sm-2 control-label">Link</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[link]" value='{if isset($common['link']) }{$common['link']}{/if}' id="inputLink3">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name='save_common' class="btn btn-primary">
                            Save
                        </button>
                    </div>
                </div>

            </div>
            <div class="widget-header">
                <i class="icon-th-list"></i>
                <h3>Table</h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {foreach $profiles as $profile}
                            <tr>
                                <td>{$profile['id']}</td>
                                <td>{$profile['name']}</td>
                                <td class="td-actions">
                                    {*   <a href="javascript:;" class="btn btn-small btn-default">
                                                                                                                       
                                    </a>*}
                                    <button type="submit" name='edit_profile[{$profile['id']}]' class="btn btn-default">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button>

                                    <button type="submit" name='delete_profile[{$profile['id']}]' class="btn btn-danger">
                                        <i class="glyphicon glyphicon-trash"></i>	
                                    </button>

                                </td>
                            </tr>
                        {/foreach}


                    </tbody>
                </table>


            </div> <!-- /widget-content -->

        </div> <!-- /widget -->
    </div>
</form>