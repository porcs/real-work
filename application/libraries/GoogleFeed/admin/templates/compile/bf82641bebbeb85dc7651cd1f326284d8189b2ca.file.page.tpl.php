<?php /* Smarty version Smarty-3.1.16, created on 2015-12-25 04:26:50
         compiled from ".\templates\page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21956567cc58a9704b7-27952466%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bf82641bebbeb85dc7651cd1f326284d8189b2ca' => 
    array (
      0 => '.\\templates\\page.tpl',
      1 => 1450695723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21956567cc58a9704b7-27952466',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'save_message' => 0,
    'categ_active' => 0,
    'profile_active' => 0,
    'profile' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_567cc58a99e4d2_29141873',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567cc58a99e4d2_29141873')) {function content_567cc58a99e4d2_29141873($_smarty_tpl) {?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 admin-tab">
            <?php if ($_smarty_tpl->tpl_vars['save_message']->value) {?>
                <?php echo $_smarty_tpl->tpl_vars['save_message']->value;?>

            <?php }?>
            <div class="tabbable" id="tabs-202328">
                <ul class="nav nav-tabs">
                    <li class="<?php echo $_smarty_tpl->tpl_vars['categ_active']->value;?>
">
                        <a href="#panel-categories" data-toggle="tab">Categories</a>
                    </li>
                    <li class="<?php echo $_smarty_tpl->tpl_vars['profile_active']->value;?>
">
                        <a href="#panel-profiles" data-toggle="tab">Profiles</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane <?php echo $_smarty_tpl->tpl_vars['categ_active']->value;?>
" id="panel-categories">
                        <?php echo $_smarty_tpl->getSubTemplate ('tab-categories.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    </div>

                    <div class="tab-pane <?php echo $_smarty_tpl->tpl_vars['profile_active']->value;?>
" id="panel-profiles">
                        <?php if (is_array($_smarty_tpl->tpl_vars['profile']->value)) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ('profile.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                            <?php } else { ?>
                            <?php echo $_smarty_tpl->getSubTemplate ('tab-profiles.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
    
                        <?php }?>
                        

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
<?php }} ?>
