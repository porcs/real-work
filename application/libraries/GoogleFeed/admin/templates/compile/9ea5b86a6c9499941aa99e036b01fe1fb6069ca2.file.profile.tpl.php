<?php /* Smarty version Smarty-3.1.16, created on 2015-12-25 04:26:53
         compiled from ".\templates\profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:30733567cc58dde06e5-82202898%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ea5b86a6c9499941aa99e036b01fe1fb6069ca2' => 
    array (
      0 => '.\\templates\\profile.tpl',
      1 => 1450695723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30733567cc58dde06e5-82202898',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'profile' => 0,
    'g_fields' => 0,
    'google_group' => 0,
    'field' => 0,
    'f' => 0,
    'l' => 0,
    'a' => 0,
    'b' => 0,
    'c' => 0,
    'item_key' => 0,
    'item' => 0,
    'd' => 0,
    'item_fields' => 0,
    'if' => 0,
    'check' => 0,
    'item_features' => 0,
    'k' => 0,
    'item_attributes' => 0,
    'item_comb_names' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_567cc58e065304_40092882',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567cc58e065304_40092882')) {function content_567cc58e065304_40092882($_smarty_tpl) {?><form class="form-horizontal" role="form" method='POST'>
    <?php if (isset($_smarty_tpl->tpl_vars['profile']->value['id'])) {?><input type="hidden" class="form-control" name="profile[id]" value="<?php echo $_smarty_tpl->tpl_vars['profile']->value['id'];?>
"><?php }?>
   <div class="form-group">
        <label for="inputProfile3" class="col-sm-2 control-label">Profile name</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="profile[name]" value='<?php if (isset($_smarty_tpl->tpl_vars['profile']->value['name'])) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value['name'];?>
<?php }?>' id="inputProfile3">
        </div>
    </div>



   
    <?php  $_smarty_tpl->tpl_vars['field'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['field']->_loop = false;
 $_smarty_tpl->tpl_vars['google_group'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['g_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['field']->key => $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->_loop = true;
 $_smarty_tpl->tpl_vars['google_group']->value = $_smarty_tpl->tpl_vars['field']->key;
?>
        <?php if ($_smarty_tpl->tpl_vars['google_group']->value=="Item Groupings") {?>
            <h3 class="delimeter">Group?</h3>
            <div class="form-group ">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <div class="funkyradio">
                        <div class="col-sm-2">
                            <div class="funkyradio-success">
                                <input type="radio" value="Yes" <?php if (isset($_smarty_tpl->tpl_vars['profile']->value['data']['group'])&&$_smarty_tpl->tpl_vars['profile']->value['data']['group']=="Yes") {?>checked<?php }?> name="profile[group]" id="radio3" />
                                <label for="radio3">Yes</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="funkyradio-danger">
                                <input type="radio" value="No" <?php if (isset($_smarty_tpl->tpl_vars['profile']->value['data']['group'])&&$_smarty_tpl->tpl_vars['profile']->value['data']['group']=="No") {?>checked<?php }?> name="profile[group]" id="radio4" />
                                <label for="radio4">No</label>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        <?php }?>            
        <h3 class="delimeter"><?php echo $_smarty_tpl->tpl_vars['google_group']->value;?>
</h3>
        <div class="form-group ">
            <div class="col-sm-2">
                <h4 class="delimeter">Google feed attribute</h4>
            </div>
            <div class=" col-sm-3">
                <h4 class="delimeter">Mapping for single Item</h4>
            </div>
            <div class=" col-sm-3">
                
                <h4 class="delimeter">Default value</h4>
            </div>
        </div>
        <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_smarty_tpl->tpl_vars['l'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['field']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
 $_smarty_tpl->tpl_vars['l']->value = $_smarty_tpl->tpl_vars['f']->key;
?>
            <div class="form-group <?php if ($_smarty_tpl->tpl_vars['f']->value['required']) {?> required <?php }?>">
                <label for="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item]" class="col-sm-2 control-label"><?php echo $_smarty_tpl->tpl_vars['l']->value;?>
:</label>
                <div class=" col-sm-3">
                    <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(0, null, 0);?>
                    <?php $_smarty_tpl->tpl_vars['d'] = new Smarty_variable(0, null, 0);?>
                    <?php if ((isset($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]))&&($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value])) {?>
                        <?php if ($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['type']=="text") {?>
                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['value'], null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['d'] = new Smarty_variable(true, null, 0);?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['type']=="array_text") {?>
                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['value'], null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['d'] = new Smarty_variable(true, null, 0);?>
                        <?php } else { ?>
                            <?php $_smarty_tpl->tpl_vars['a'] = new Smarty_variable($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['type'], null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['b'] = new Smarty_variable($_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['value'], null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['a']->value)."|".((string)$_smarty_tpl->tpl_vars['b']->value), null, 0);?>
                        <?php }?>
                    <?php }?>
                    <select class="form-control google_fields" <?php if ((isset($_smarty_tpl->tpl_vars['f']->value['multiselect']))) {?> multiple name="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item][]" <?php } else { ?> name="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item]"  <?php }?>  id="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item]"  <?php if ($_smarty_tpl->tpl_vars['f']->value['required']) {?> required <?php }?>>
                        <option value=""> </option>

                        <?php if (isset($_smarty_tpl->tpl_vars['f']->value['not_default'])) {?>
                            <?php $_smarty_tpl->tpl_vars['d'] = new Smarty_variable(false, null, 0);?>
                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['item_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['f']->value['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item_key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                                
                                
                               <?php if (!is_array($_smarty_tpl->tpl_vars['c']->value)) {?>
                                <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['item_key']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['item_key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</option>
                                <?php } else { ?>
                                  <option <?php if ((in_array($_smarty_tpl->tpl_vars['item_key']->value,$_smarty_tpl->tpl_vars['c']->value))) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['item_key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</option>
                                <?php }?>
                            <?php } ?>
                        <?php } else { ?>
                            <option value="add_text" <?php if ($_smarty_tpl->tpl_vars['d']->value==true) {?>selected <?php }?>>Default value</option>

                            <optgroup label="Fields">
                                <?php  $_smarty_tpl->tpl_vars['if'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['if']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item_fields']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['if']->key => $_smarty_tpl->tpl_vars['if']->value) {
$_smarty_tpl->tpl_vars['if']->_loop = true;
?>
                                    <?php $_smarty_tpl->tpl_vars['check'] = new Smarty_variable("item_field|".((string)$_smarty_tpl->tpl_vars['if']->value), null, 0);?>
                                    <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['check']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?> value="item_field|<?php echo $_smarty_tpl->tpl_vars['if']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['if']->value;?>
</option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Features">
                                <?php  $_smarty_tpl->tpl_vars['if'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['if']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['if']->key => $_smarty_tpl->tpl_vars['if']->value) {
$_smarty_tpl->tpl_vars['if']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['if']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['check'] = new Smarty_variable("feature|".((string)$_smarty_tpl->tpl_vars['k']->value), null, 0);?>
                                    <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['check']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?> value="feature|<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['if']->value['name'];?>
</option>
                                <?php } ?>
                            </optgroup>

                            <optgroup label="Attributes">
                                <?php  $_smarty_tpl->tpl_vars['if'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['if']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_attributes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['if']->key => $_smarty_tpl->tpl_vars['if']->value) {
$_smarty_tpl->tpl_vars['if']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['if']->key;
?>
                                    <?php $_smarty_tpl->tpl_vars['check'] = new Smarty_variable("attribute|".((string)$_smarty_tpl->tpl_vars['k']->value), null, 0);?>
                                    <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['check']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?> value="attribute|<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['if']->value['name'];?>
</option>
                                <?php } ?>
                            </optgroup>
                            <optgroup label="Combination Fields">
                                <?php  $_smarty_tpl->tpl_vars['if'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['if']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item_comb_names']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['if']->key => $_smarty_tpl->tpl_vars['if']->value) {
$_smarty_tpl->tpl_vars['if']->_loop = true;
?>
                                    <?php $_smarty_tpl->tpl_vars['check'] = new Smarty_variable("comb_field|".((string)$_smarty_tpl->tpl_vars['if']->value), null, 0);?>
                                    <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['check']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?>  value="comb_field|<?php echo $_smarty_tpl->tpl_vars['if']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['if']->value;?>
</option>
                                <?php } ?>
                            </optgroup>

                        <?php }?>

                    </select>

                </div>
                <div class="col-sm-3">
                    <?php if (isset($_smarty_tpl->tpl_vars['f']->value['values'])) {?>
                        <select class="form-control <?php if ($_smarty_tpl->tpl_vars['d']->value!=true) {?>hidden<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[text]" name="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item]" id="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[text]" <?php if ($_smarty_tpl->tpl_vars['d']->value!=true) {?>disabled<?php }?> <?php if ($_smarty_tpl->tpl_vars['f']->value['required']) {?> required <?php }?>>

                            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['f']->value['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
                                
                                <option <?php if (($_smarty_tpl->tpl_vars['c']->value)&&($_smarty_tpl->tpl_vars['item']->value==$_smarty_tpl->tpl_vars['c']->value)) {?> selected <?php }?> value="<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</option>

                            <?php } ?>
                        </select>
                    <?php } else { ?>
                        <input type="text" value="<?php if ($_smarty_tpl->tpl_vars['d']->value==true) {?><?php echo $_smarty_tpl->tpl_vars['profile']->value['data']['item_mapping'][$_smarty_tpl->tpl_vars['l']->value]['value'];?>
<?php }?>" name="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[item]" id="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[text]" class="form-control <?php if ($_smarty_tpl->tpl_vars['d']->value!=true) {?>hidden<?php }?>" <?php if ($_smarty_tpl->tpl_vars['d']->value!=true) {?>disabled<?php }?> <?php if ($_smarty_tpl->tpl_vars['f']->value['required']) {?> required <?php }?>>
                    <?php }?>
                </div>
                
                
                <?php if ($_smarty_tpl->tpl_vars['google_group']->value=="Item Groupings") {?>
                    <div class="col-sm-3 profile_grouping">
                        <div class="funkyradio">

                            <div class="funkyradio-success">

                                <input type="checkbox" name="profile[grouping][<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
]" id="checkbox<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
" <?php if (isset($_smarty_tpl->tpl_vars['profile']->value['data']['grouping'][$_smarty_tpl->tpl_vars['l']->value])) {?>checked<?php }?>/>
                                <label for="checkbox<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
">Grouping by <?php echo $_smarty_tpl->tpl_vars['l']->value;?>
?</label>
                            </div>
                        </div>
                    </div>
                <?php }?>
                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[tag]" id="<?php echo $_smarty_tpl->tpl_vars['l']->value;?>
[tag]" value="<?php echo $_smarty_tpl->tpl_vars['f']->value['tag'];?>
">
            </div>
            
        <?php } ?>
    <?php } ?>

    <h3 class="delimeter">Export Shipping(carrier) ?</h3>
    <div class="form-group ">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <div class="funkyradio">
                <div class="col-sm-2">
                    <div class="funkyradio-success">
                        <input type="radio" value="Yes" <?php if (isset($_smarty_tpl->tpl_vars['profile']->value['data']['shipping'])&&$_smarty_tpl->tpl_vars['profile']->value['data']['shipping']=="Yes") {?>checked<?php }?> name="profile[shipping]" id="radio5" />
                        <label for="radio5">Yes</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="funkyradio-danger">
                        <input type="radio" value="No" <?php if (!isset($_smarty_tpl->tpl_vars['profile']->value['data']['shipping'])||$_smarty_tpl->tpl_vars['profile']->value['data']['shipping']=="No") {?>checked<?php }?> name="profile[shipping]" id="radio6" />
                        <label for="radio6">No</label>
                    </div>
                </div>            
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">

            <button  class="btn btn-default back">
                To profiles
            </button>
            <button type="submit" name='save_profile' class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>
<?php }} ?>
