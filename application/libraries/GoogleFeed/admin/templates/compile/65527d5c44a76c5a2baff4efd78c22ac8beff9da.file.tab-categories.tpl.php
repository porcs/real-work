<?php /* Smarty version Smarty-3.1.16, created on 2015-12-25 04:26:50
         compiled from ".\templates\tab-categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27821567cc58a9a6bb6-49846553%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65527d5c44a76c5a2baff4efd78c22ac8beff9da' => 
    array (
      0 => '.\\templates\\tab-categories.tpl',
      1 => 1450695723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27821567cc58a9a6bb6-49846553',
  'function' => 
  array (
    'profiles_select' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
    'menu' => 
    array (
      'parameter' => 
      array (
        'level' => 0,
        'parent' => 0,
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'category' => 0,
    'data' => 0,
    'profile' => 0,
    'categories' => 0,
    'cat' => 0,
    'level' => 0,
    'parent' => 0,
    'iso_code' => 0,
    'profiles' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_567cc58aa1e2c5_15147918',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567cc58aa1e2c5_15147918')) {function content_567cc58aa1e2c5_15147918($_smarty_tpl) {?><form class="form-horizontal col-sm-8 treeview" action="?active=panel_categories" role="form" method='POST'>
    <?php if (!function_exists('smarty_template_function_profiles_select')) {
    function smarty_template_function_profiles_select($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['profiles_select']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?> 
        <select name='profile_cat[<?php echo $_smarty_tpl->tpl_vars['category']->value;?>
]'>
            <option value='' ></option>
            <?php  $_smarty_tpl->tpl_vars['profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['profile']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['profile']->key => $_smarty_tpl->tpl_vars['profile']->value) {
$_smarty_tpl->tpl_vars['profile']->_loop = true;
?>
                <option value='<?php echo $_smarty_tpl->tpl_vars['profile']->value['id'];?>
' <?php if (in_array($_smarty_tpl->tpl_vars['category']->value,$_smarty_tpl->tpl_vars['profile']->value['categories'])) {?>selected<?php }?> ><?php echo $_smarty_tpl->tpl_vars['profile']->value['name'];?>
</option>
            <?php } ?>
        </select>
    <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>


    <ul class="level list-group">
        <?php if (!function_exists('smarty_template_function_menu')) {
    function smarty_template_function_menu($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['menu']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?> 
            <?php  $_smarty_tpl->tpl_vars['cat'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cat']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cat']->key => $_smarty_tpl->tpl_vars['cat']->value) {
$_smarty_tpl->tpl_vars['cat']->_loop = true;
?>
                
                <?php if ($_smarty_tpl->tpl_vars['cat']->value->lv>=$_smarty_tpl->tpl_vars['level']->value&&$_smarty_tpl->tpl_vars['parent']->value==$_smarty_tpl->tpl_vars['cat']->value->id_parent) {?>
                    <li class="list-group-item">
                        <?php if ($_smarty_tpl->tpl_vars['level']->value>0) {?>
                            <?php $_smarty_tpl->tpl_vars['var'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['var']->step = 1;$_smarty_tpl->tpl_vars['var']->total = (int) ceil(($_smarty_tpl->tpl_vars['var']->step > 0 ? $_smarty_tpl->tpl_vars['level']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['level']->value)+1)/abs($_smarty_tpl->tpl_vars['var']->step));
if ($_smarty_tpl->tpl_vars['var']->total > 0) {
for ($_smarty_tpl->tpl_vars['var']->value = 1, $_smarty_tpl->tpl_vars['var']->iteration = 1;$_smarty_tpl->tpl_vars['var']->iteration <= $_smarty_tpl->tpl_vars['var']->total;$_smarty_tpl->tpl_vars['var']->value += $_smarty_tpl->tpl_vars['var']->step, $_smarty_tpl->tpl_vars['var']->iteration++) {
$_smarty_tpl->tpl_vars['var']->first = $_smarty_tpl->tpl_vars['var']->iteration == 1;$_smarty_tpl->tpl_vars['var']->last = $_smarty_tpl->tpl_vars['var']->iteration == $_smarty_tpl->tpl_vars['var']->total;?>
                                <span class="indent"></span>
                            <?php }} ?>
                        <?php }?>
                        <span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>
                        <?php echo $_smarty_tpl->tpl_vars['cat']->value->name->{$_smarty_tpl->tpl_vars['iso_code']->value};?>

                        <?php smarty_template_function_profiles_select($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['profiles']->value,'category'=>$_smarty_tpl->tpl_vars['cat']->value->id));?>
</li>
                        <?php smarty_template_function_menu($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['cat']->value,'level'=>$_smarty_tpl->tpl_vars['level']->value+1,'parent'=>$_smarty_tpl->tpl_vars['cat']->value->id));?>

                    <?php } else { ?>                                        

                <?php }?>
            <?php } ?>
        <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

    </ul>

    <?php smarty_template_function_menu($_smarty_tpl,array('data'=>$_smarty_tpl->tpl_vars['categories']->value));?>

    <button type="submit" class="btn btn-primary">
        Save
    </button>

</form><?php }} ?>
