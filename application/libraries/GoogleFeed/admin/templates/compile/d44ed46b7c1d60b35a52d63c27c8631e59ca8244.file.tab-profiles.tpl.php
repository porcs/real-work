<?php /* Smarty version Smarty-3.1.16, created on 2015-12-25 04:26:50
         compiled from ".\templates\tab-profiles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11234567cc58aa297f8-94480240%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd44ed46b7c1d60b35a52d63c27c8631e59ca8244' => 
    array (
      0 => '.\\templates\\tab-profiles.tpl',
      1 => 1450695723,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11234567cc58aa297f8-94480240',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'exist_langs' => 0,
    'lang' => 0,
    'common' => 0,
    'profiles' => 0,
    'profile' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_567cc58aa64cd2_26603281',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_567cc58aa64cd2_26603281')) {function content_567cc58aa64cd2_26603281($_smarty_tpl) {?><form class="form-horizontal" action="?active=panel_profiles" role="form" method='POST'>
    <div class="span7">   
        <div class="widget stacked widget-table action-table">

            <button type="submit" name='add_profile' class="btn btn-primary" style="margin:5px 0">
                <i class="glyphicon glyphicon-plus"> Create new profile</i>	
            </button>
            <span class="languages">
                <?php  $_smarty_tpl->tpl_vars['lang'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['lang']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['exist_langs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['lang']->key => $_smarty_tpl->tpl_vars['lang']->value) {
$_smarty_tpl->tpl_vars['lang']->_loop = true;
?>
                    <a href="?profile_lang=<?php echo $_smarty_tpl->tpl_vars['lang']->value['iso_code'];?>
" target="_blank" name='open_profile_xml[<?php echo $_smarty_tpl->tpl_vars['lang']->value['id_lang'];?>
]' class="btn btn-default open-profile">
                        <i class="glyphicon glyphicon-save"></i> <?php echo $_smarty_tpl->tpl_vars['lang']->value['iso_code'];?>

                    </a>
                <?php } ?>
            </span>
            <div class="common">
                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Common settings</h3>
                </div> <!-- /widget-header -->

                <div class="form-group">
                    <label for="inputTitle3" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[title]" value='<?php if (isset($_smarty_tpl->tpl_vars['common']->value['title'])) {?><?php echo $_smarty_tpl->tpl_vars['common']->value['title'];?>
<?php }?>'  id="inputTitle3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputDescripcion3" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[description]" value='<?php if (isset($_smarty_tpl->tpl_vars['common']->value['description'])) {?><?php echo $_smarty_tpl->tpl_vars['common']->value['description'];?>
<?php }?>' id="inputDescripcion3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLink3" class="col-sm-2 control-label">Link</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="common[link]" value='<?php if (isset($_smarty_tpl->tpl_vars['common']->value['link'])) {?><?php echo $_smarty_tpl->tpl_vars['common']->value['link'];?>
<?php }?>' id="inputLink3">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name='save_common' class="btn btn-primary">
                            Save
                        </button>
                    </div>
                </div>

            </div>
            <div class="widget-header">
                <i class="icon-th-list"></i>
                <h3>Table</h3>
            </div> <!-- /widget-header -->

            <div class="widget-content">

                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php  $_smarty_tpl->tpl_vars['profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['profile']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['profile']->key => $_smarty_tpl->tpl_vars['profile']->value) {
$_smarty_tpl->tpl_vars['profile']->_loop = true;
?>
                            <tr>
                                <td><?php echo $_smarty_tpl->tpl_vars['profile']->value['id'];?>
</td>
                                <td><?php echo $_smarty_tpl->tpl_vars['profile']->value['name'];?>
</td>
                                <td class="td-actions">
                                    
                                    <button type="submit" name='edit_profile[<?php echo $_smarty_tpl->tpl_vars['profile']->value['id'];?>
]' class="btn btn-default">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button>

                                    <button type="submit" name='delete_profile[<?php echo $_smarty_tpl->tpl_vars['profile']->value['id'];?>
]' class="btn btn-danger">
                                        <i class="glyphicon glyphicon-trash"></i>	
                                    </button>

                                </td>
                            </tr>
                        <?php } ?>


                    </tbody>
                </table>


            </div> <!-- /widget-content -->

        </div> <!-- /widget -->
    </div>
</form><?php }} ?>
