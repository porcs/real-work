
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 admin-tab">
            {if $save_message}
                {$save_message}
            {/if}
            <div class="tabbable" id="tabs-202328">
                <ul class="nav nav-tabs">
                    <li class="{$categ_active}">
                        <a href="#panel-categories" data-toggle="tab">Categories</a>
                    </li>
                    <li class="{$profile_active}">
                        <a href="#panel-profiles" data-toggle="tab">Profiles</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane {$categ_active}" id="panel-categories">
                        {include file='tab-categories.tpl'}
                    </div>

                    <div class="tab-pane {$profile_active}" id="panel-profiles">
                        {if is_array($profile)}
                            {include file='profile.tpl'}
                            {else}
                            {include file='tab-profiles.tpl'}    
                        {/if}
                        

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
