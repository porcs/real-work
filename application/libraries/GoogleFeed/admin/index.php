<?php

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);

define('SMARTY_DIR', '../../Smarty/');
require_once(SMARTY_DIR . 'Smarty.class.php');

require_once(dirname(__FILE__) . '/config.php');
require_once(dirname(__FILE__) . '/google_feed/GoogleFeedBiz.php');
require_once(dirname(__FILE__) . '/google_feed/GoogleFeed.php');
require_once(dirname(__FILE__) . '/google_feed/GoogleSpider.php');
//print_r($_REQUEST);

ini_set("memory_limit", "512M");

$feedbiz = new GoogleFeedBiz(array($user_name));
//$feedbiz->saveCategoryLanguages(GoogleSpider::getLocales());
//die();
//if (isset($_GET['open_profile'])) {
//    $profile = $feedbiz->getProfileById($_GET['open_profile']);
//    $language = $feedbiz->checkLanguage($iso_code, $id_shop);
//    $id_lang = $language['id_lang'];
//
//    $feedbiz->getXmlFeedByArray($profile, $user_name, $id_shop, $id_mode, $id_lang, $iso_code,$_GET['open_profile']);
//    die();
//}

if (isset($_REQUEST['save_common'])) {
    $feedbiz->saveCommon($_REQUEST['common']);
}

if (isset($_GET['profile_lang'])) {
    $language = $feedbiz->checkLanguage($_GET['profile_lang'], $id_shop);
    $id_lang = $language['id_lang'];
    
    $profiles = $feedbiz->getProfiles();
    
    $rearrange = array();
    foreach ($profiles as $profile) {
        $rearrange[$profile['id']] = $profile;
    }
    $feedbiz->getXmlFeedByArray($user_name, $id_shop, $id_lang, $id_mode, $_GET['profile_lang'], $rearrange);
    die();
}

$save_message = '';
$category = json_decode($feedbiz->getJsonAllCategory($user_name, $id_shop)); //Category for this user
//print_r($category);
$google_fields = GoogleFeed::getFeedSpecification();

$item_fields = $feedbiz->getFieldsNames($id_shop, $iso_code);
$item_features = $feedbiz->getFeaturesNames($id_shop, $iso_code);
$item_attributes = $feedbiz->getAttributesNames($id_shop, $iso_code);
$item_comb_names = $feedbiz->getCombFieldsNames($id_shop, $iso_code);

if (isset($_REQUEST['profile_cat'])) {
    saveProfilesCat($_REQUEST['profile_cat']);
    $save_message = '<div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        ×
                    </button>
                    <h4>
                        Success!
                    </h4> <strong>Category updated successfully</strong>
                </div>';
}

if (isset($_REQUEST['delete_profile']))
    deleteProfiles($_REQUEST['delete_profile']);

$profile = "";
if (isset($_REQUEST['edit_profile'])) {
    reset($_REQUEST['edit_profile']);
    $profile = $feedbiz->getProfileById(key($_REQUEST['edit_profile']));
}
if (isset($_REQUEST['add_profile'])) {
    $profile = array();
}
$panel_categories = 'active';
$panel_profiles = '';
if (isset($_REQUEST['active'])) {
    if ($_REQUEST['active'] == 'panel_profiles') {
        $panel_categories = '';
        $panel_profiles = 'active';
    }
}
//print_r($profile);
//die();
if (isset($_REQUEST['back'])) {
    $profile = '';
}
if (isset($_REQUEST['save_profile'])) {
    saveProfile();
    $save_message = '<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
					×
				</button>
				<h4>
					Success!
				</h4> <strong>Profile updated successfully</strong>
			</div>';
}

$category = json_decode($feedbiz->getJsonAllCategory($user_name, $id_shop)); //Category for this user
//var_dump($category);
//die();
$profiles = $feedbiz->getProfiles();
$smarty = new Smarty();

$smarty->template_dir = './templates/';
$smarty->compile_dir = './templates/compile/';
$smarty->cache_dir = './templates/cache/';

$smarty->caching = false;
$smarty->error_reporting = E_ALL;

foreach ($google_fields as $key => $field) {
    foreach ($field as $k => $f) {
        if (isset($f['values']) && !is_array($f['values'])) {
	    
            $google_fields[$key][$k]['values'] = $feedbiz->getTaxonomyByLanguage($f['values'], $iso_code);
            $google_fields[$key][$k]['not_default'] = true;
        }
        if (isset($f['multiselect']))
            $google_fields[$key][$k]['multiselect'] = true;
    }
}
//exit;
//var_dump($google_fields);
//die();

$common['link'] = $feedbiz->getCommonValue('link');
$common['description'] = $feedbiz->getCommonValue('description');
$common['title'] = $feedbiz->getCommonValue('title');

$smarty->assign('categ_active', $panel_categories);
$smarty->assign('common', $common);
$smarty->assign('save_message', $save_message);
$smarty->assign('profile_active', $panel_profiles);
$smarty->assign('iso_code', $iso_code);
$smarty->assign('categories', $category);
$smarty->assign('g_fields', $google_fields);
$smarty->assign('item_fields', $item_fields);
$smarty->assign('item_features', $item_features);
$smarty->assign('item_attributes', $item_attributes);
$smarty->assign('item_comb_names', $item_comb_names);
$smarty->assign('profiles', $profiles);
$smarty->assign('profile', $profile);
$smarty->assign('exist_langs', $feedbiz->getLanguage($id_shop));
$smarty->display('header.tpl');
$smarty->display('page.tpl');
$smarty->
        display('footer.tpl');

function saveProfilesCat($cats) {
    global $feedbiz;
    foreach ($cats as $cat_id => $profile_id) {
        $feedbiz->setProfileToCategory($cat_id, $profile_id);
    }
}

function deleteProfiles($ids) {
    global $feedbiz;
    foreach ($ids as $id => $v) {
        $feedbiz->deleteProfile($id);
    }
}

function saveProfile() {
    global $feedbiz;
    $item_mapping = array();
    $comb_mapping = array();

    $profile = $_REQUEST['profile'];
    
   
    foreach (GoogleFeed::getFeedSpecification() as $spec_group => $attr) {
        foreach ($attr as $name => $attr) {
	     
	    if(isset($_REQUEST[$name]['item']))
            $profile['item_mapping'][$name] = createMappingArray($_REQUEST[$name]['item'], $_REQUEST[$name]['tag']);
            //$profile['comb_mapping'][$name] = createMappingArray($_REQUEST[$name]['combinations'], $_REQUEST[$name]['tag']);
        }
    } 
    
    //var_dump($profile);
    if (isset($profile["id"]))
        $feedbiz->saveProfile($profile["id"], $profile["name"], $profile);
    else
        $feedbiz->newProfile($profile["name"], $profile);
}

function createMappingArray($value, $tag) {
    if (!$value)
        return array();

    if (is_array($value)) 
      return array("type" => "array_text", "value" => serialize($value), "tag" => $tag);  
    

    $ex = explode("|", $value);


    if (isset($ex[1]))
        return array("type" => $ex[0], "value" => $ex[1], "tag" => $tag);
    else
        return array("type" => "text", "value" => $ex[0], "tag" => $tag);
}
