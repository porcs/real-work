<?php

class GoogleSpider {

    public static function getURL($url, $data = array()) {

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    $response = curl_exec($ch);
	    curl_close($ch);

	    return $response;
    }

    public static function downloadFile($url) {
        set_time_limit(0);
        $fp = fopen(dirname(__FILE__) . '/localfile.tmp', 'w+'); //This is the file where we save the    information
        $ch = curl_init(str_replace(" ", "%20", $url)); //Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        rewind($fp);
        return $fp;
    }

    public static function getLocales() {
        
        $l2 = 'a:16:{s:2:"en";s:5:"en-US";s:2:"br";s:5:"pt-BR";s:2:"cs";s:5:"cs-CZ";s:2:"da";s:5:"da-DK";s:2:"de";s:5:"de-DE";s:2:"es";s:5:"es-ES";s:2:"fr";s:5:"fr-FR";s:2:"it";s:5:"it-IT";s:2:"ja";s:5:"ja-JP";s:2:"nl";s:5:"nl-NL";s:2:"nn";s:5:"no-NO";s:2:"pl";s:5:"pl-PL";s:2:"ru";s:5:"ru-RU";s:2:"sv";s:5:"sv-SE";s:2:"tr";s:5:"tr-TR";s:2:"cn";s:5:"zh-CN";}';
        //$l2='a:8:{s:2:"ja";s:5:"ja-JP";s:2:"nl";s:5:"nl-NL";s:2:"nn";s:5:"no-NO";s:2:"pl";s:5:"pl-PL";s:2:"ru";s:5:"ru-RU";s:2:"sv";s:5:"sv-SE";s:2:"tr";s:5:"tr-TR";s:2:"cn";s:5:"zh-CN";}';
        //после японоского ничего не добавил
        return unserialize($l2);

        $languages = array(
            "af", "ach", "ak", "am", "ar", "az", "be", "bem", "bg", "bh", "bn", "br", "bs", "ca", "chr", "ckb", "co",
            "crs", "cs", "cy", "da", "de", "ee", "el", "en", "eo", "es", "es-419", "et", "eu", "fa", "fi", "fo", "fr", "fy",
            "ga", "gaa", "gd", "gl", "gn", "gu", "ha", "haw", "hi", "hr", "ht", "hu", "hy", "ia", "id", "ig", "is", "it", "iw",
            "ja", "jw", "ka", "kg", "kk", "km", "kn", "ko", "kri", "ku", "ky", "la", "lg", "ln", "lo", "loz", "lt", "lua", "lv",
            "mfe", "mg", "mi", "mk", "ml", "mn", "mo", "mr", "ms", "mt",
            "ne", "nl", "nn", "no", "nso", "ny", "nyn", "oc", "om", "or", "pa", "pcm", "pl", "ps", "pt-BR", "pt-PT",
            "qu", "rm", "rn", "ro", "ru", "rw", "sd", "sh", "si", "sk", "sl", "sn", "so", "sq", "sr", "sr-ME", "st",
            "su", "sv", "sw", "ta", "te", "tg", "th", "ti", "tk", "tl", "tn", "to", "tr", "tt", "tum", "tw", "ug", "uk",
            "ur", "uz", "vi", "wo", "xh", "xx-bork", "xx-elmer", "xx-hacker", "xx-klingon", "xx-pirate", "yi", "yo",
            "zh-CN", "zh-TW", "zu"
        );

        $locales = array();
        foreach ($languages as $lang) {
            $page = GoogleSpider::getURL("https://support.google.com/merchants/answer/160081?hl=" . $lang);
            if (!preg_match('#<a href="http://www.google.com/basepages/producttype/taxonomy-with-ids.(.*?).txt">#', $page, $m)) {
                if (preg_match('#<a href="(.*?)">here#i', $page, $m)) {
                    $page = GoogleSpider::getURL($m[1]);
                    preg_match('#<a href="http://www.google.com/basepages/producttype/taxonomy-with-ids.(.*?).txt">#', $page, $m);
                }
            }
            if (isset($m[1]))
                $locales[$lang] = $m[1];
        }
        return serialize($locales);
    }

}
