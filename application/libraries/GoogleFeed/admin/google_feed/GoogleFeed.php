<?php

class GoogleFeed {

    static public function getFeedSpecification() {
        $attributes = array();

        $attributes['General attributes'] = array(
            'id' => array("required" => 1, "tag" => 'g:id'),
            'title' => array("required" => 1, "tag" => 'title'),
            'description' => array("required" => 1, "tag" => 'description'),
            'google_product_category' => array("required" => 1, "tag" => 'g:google_product_category', "values" => "google_category_lang"),
            'product_type' => array("required" => 0, "tag" => 'g:product_type',"multiselect"=>"true","values" => "google_category_lang"),
            'link' => array("required" => 1, "tag" => 'link'),
            'mobile_link' => array("required" => 0, "tag" => 'g:mobile_link'),
            'image_link' => array("required" => 1, "tag" => 'g:image_link'),
            'additional_image_link' => array("required" => 0, "tag" => 'g:additional_image_link'),
            'condition' => array("required" => 1, "tag" => 'g:condition', 'values' => array("new", "used", "refurbished")),
        );

        $attributes['Unique Product Identifiers'] = array(
            'gtin' => array("required" => 1, "tag" => 'g:gtin'),
            'mpn' => array("required" => 1, "tag" => 'g:mpn'),
            'brand' => array("required" => 1, "tag" => 'g:brand'),
            'identifier_exists' => array("required" => 0, "tag" => 'g:identifier_exists', 'values' => array("TRUE", "FALSE")),
        );

        $attributes['Availability & Price'] = array(
            'availability' => array("required" => 1, "tag" => 'g:availability', 'values' => array("in stock", "out of stock", "preorder")),
            'availability_date' => array("required" => 0, "tag" => 'g:availability_date'), //2011-03-01T13:00-0800
            'price' => array("required" => 1, "tag" => 'g:price'),
            'sale_price' => array("required" => 0, "tag" => 'g:sale_price'),
            'sale_price_effective_date' => array("required" => 0, "tag" => 'g:sale_price_effective_date'), //2011-03-01T13:00-0800/2011-03-11T15:30-0800
        );

        $attributes['Item Groupings'] = array(
            /* 'item_group_id' => array("required" => 0, "tag" => 'g:item_group_id'), */
            'color' => array("required" => 0, "tag" => 'g:color'),
            'gender' => array("required" => 0, "tag" => 'g:gender', 'values' => array("male", "female", "unisex")),
            'age_group' => array("required" => 0, "tag" => 'g:age_group', 'values' => array("newborn", "infant", "toddler", "kids", "adult")),
            'material' => array("required" => 0, "tag" => 'g:material'),
            'pattern' => array("required" => 0, "tag" => 'g:pattern'),
            'size' => array("required" => 0, "tag" => 'g:size'),
            'size_type' => array("required" => 0, "tag" => 'g:size_type', 'values' => array("regular", "petite", "plus", "big and tall", "maternity")),
            'size_system' => array("required" => 0, "tag" => 'g:size_system', 'values' => array("US", "UK", "EU", "DE", "FR", "JP", "CN (China)", "IT", "BR", "MEX", "AU")),
        );

        $attributes['Custom Attributes'] = array(
            'custom_label_0' => array("required" => 0, "tag" => 'g:custom_label_0'),
            'custom_label_1' => array("required" => 0, "tag" => 'g:custom_label_1'),
            'custom_label_2' => array("required" => 0, "tag" => 'g:custom_label_2'),
            'custom_label_3' => array("required" => 0, "tag" => 'g:custom_label_3'),
            'custom_label_4' => array("required" => 0, "tag" => 'g:custom_label_4'),
        );

        $attributes['Specific Attributes'] = array(
            'multipack' => array("required" => 0, "tag" => 'g:multipack'),
            'is_bundle' => array("required" => 0, "tag" => 'g:is_bundle', 'values' => array("TRUE", "FALSE")),
            'adult' => array("required" => 0, "tag" => 'g:adult', 'values' => array("TRUE", "FALSE")),
            'adwords_redirect' => array("required" => 0, "tag" => 'g:adwords_redirect'),
            'expiration_date' => array("required" => 0, "tag" => 'g:expiration_date'),
            'energy_efficiency_class' => array("required" => 0, "tag" => 'g:energy_efficiency_class', 'values' => array("G", "F", "E", "D", "C", "B", "A", "A+", "A++", "A+++")),
            'unit_pricing_measure' => array("required" => 0, "tag" => 'g:unit_pricing_measure'),
            'unit_pricing_base_measure' => array("required" => 0, "tag" => 'g:unit_pricing_base_measure'),
            'shipping_weight' => array("required" => 0, "tag" => 'g:shipping_weight'),
            'shipping_label' => array("required" => 0, "tag" => 'g:shipping_label'),
        );
        return $attributes;
    }

    static public function getGoogleTaxonomyArray($name) {
        $handle = fopen(__DIR__ . '/' . $name . ".txt", "r");
        if (!$handle)
            return array();
        $tax = array();
        while (($data = fgets($handle, 1000)) !== FALSE) {
            $tax[] = trim($data);
        }
        fclose($handle);
        return $tax;
    }

    static public function exampleProfile() {
        $profile = array();

        $profile['name'] = 'Bla bla bla';
        $profile['title'] = 'Bla bla bla';
        $profile['description'] = 'Bla bla bla';
        $profile['link'] = 'http://yourhost.com/google-products-original/';

        $mapping = array();

        $mapping = array(
            'id' => array("type" => 'field', 'value' => 'id_product', "comb_value" => "id_product_attribute", "tag" => 'g:id', "multiple_only" => 0),
            'item_group_id' => array("type" => 'field', 'value' => 'id_product', "comb_value" => "", "tag" => 'g:item_group_id', "multiple_only" => 1),
            'title' => array("type" => 'field', 'value' => 'name', "comb_value" => "", "tag" => 'title', "multiple_only" => 0),
            'description' => array("type" => 'field', 'value' => 'description', "comb_value" => "", "tag" => 'description', "multiple_only" => 0),
            'google_product_category' => array("type" => 'text', 'value' => 'Animals & Pet Supplies > Pet Supplies > Bird Supplies', "comb_value" => "", "tag" => 'g:google_product_category', "multiple_only" => 0),
            'product_type' => array("type" => 'text', 'value' => 'Bird Supplies', "comb_value" => "", "tag" => 'g:product_type', "multiple_only" => 0),
            'link' => array("type" => 'text', 'value' => 'http://yourhost.com/google-products-original/', "comb_value" => "", "tag" => 'link', "multiple_only" => 0),
            'color' => array("type" => 'attribute', 'value' => '1', "comb_value" => "", "tag" => 'g:color', "multiple_only" => 1),
            'size' => array("type" => 'attribute', 'value' => '1', "comb_value" => "", "tag" => 'g:size', "multiple_only" => 1),
            'age_group' => array("type" => 'feature', 'value' => '13', "comb_value" => "", "tag" => 'g:age_group', "multiple_only" => 0),
            'price' => array("type" => 'field', 'value' => 'price', 'comb_value' => 'price', "tag" => 'g:price', "multiple_only" => 0),
        );

        $profile['mapping'] = $mapping;
        return $profile;
    }

}
