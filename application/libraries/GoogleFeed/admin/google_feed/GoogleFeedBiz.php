<?php

require_once(dirname(__FILE__) . '/../../../FeedBiz.php');

class GoogleFeedBiz extends FeedBiz {

    /**
     * get array of features name
     *
     * exec own function from Feature class
     * 
     * 
     * @param int $id_shop shop identifier
     * @param string $lang language e.g [en|fr]
     * @return array array of features names
     */
    public function getFeaturesNames($id_shop, $lang, $id_feature = null, $id_feature_value = null) {

        $feature = new Feature(objectModel::$user);
        $l = $this->checkLanguage($lang, $id_shop);
        return $feature->getFeaturesNames($id_shop, $l['id_lang'], $id_feature, $id_feature_value);
    }

    /**
     * get hardcode array of fields from item
     * 
     * @return array
     */
    public function getFieldsNames() {

        $fields = array(
            'id_product',
            'name',
            'description',
            'description_short',
            'link',
            'sku',
            'upc',
            'ean13',
            'reference',
            'quantity',
            'condition',
            'weight',
            'price',
            'wholesale_price',
            'manufacturer',
            'supplier',
            'width',
            'height',
            'depth',
            'weight',
            'active',
            'currency',
            'sale',
            'carrier',
            'currency',
            'id_category_default',
            'category',
            'images',
            'no_price_export',
            'no_quantity_export',
            'latency',
        );

        return $fields;
    }

    /**
     *  get hardcode array of fields from item's combinations
     *
     * @return array
     */
    public function getCombFieldsNames() {

        $fields = array(
            "id_product_attribute",
            "reference",
            "ean13",
            "sku",
            "upc",
            "price",
            "price_type",
            "quantity",
            "wholesale_price"
        );

        return $fields;
    }

    /**
     * get array of attributes name
     *
     * exec own function from attribute class
     * 
     * 
     * @param int $id_shop shop identifier
     * @param string $lang language e.g [en|fr]
     * @return array array of features names
     */
    public function getAttributesNames($id_shop, $lang) {
        $return_attr = array();
        $l = $this->checkLanguage($lang, $id_shop);
        $attributes = new Attribute(objectModel::$user);

        $attribute = $attributes->getAttributesNames($id_shop, $l['id_lang']);


        return $attribute;
    }

    /**
     * Get profile data by ID
     *
     * This function get profile from database by profile_id
     * 
     * 
     * @param int $id profile identifier
     * @return array profile
     */
    public function getProfileById($id) {
        $_db = ObjectModel::$db;
        $_db->select(array('*'));
        $_db->from('google_profile', "a", true);

        $arr_where['a.id'] = (int) $id;

        $_db->where($arr_where);

        $result = $_db->db_array_query($_db->query);
        if ($result) {
            $result[0]['data'] = unserialize($result[0]['data']);
            foreach ($result[0]['data']['item_mapping'] as &$value) {
                if (!$value)
                    continue;

                if ($value['type'] == "array_text")
                    $value['value'] = unserialize($value['value']);
            }
            return $result[0];
        } else
            return false;
    }

    /**
     * Retrieves the category associated with this profile
     *
     * This is used in the interface category
     * 
     * 
     * @param int $id profile identifier
     * @return array array of categories IDs
     */
    public function getCategoriesProfileById($id) {
        $_db = ObjectModel::$db;
        $_db->select(array('*'));
        $_db->from('google_profile_category', "a", true);

        $arr_where['a.profile_id'] = (int) $id;

        $_db->where($arr_where);

        $result = $_db->db_array_query($_db->query);
        if ($result) {
            foreach ($result as $cat) {
                $category[] = $cat['category_id'];
            }
            return $category;
        } else
            return false;
    }

    /**
     * Gets an array where the key is Category and 
     * value is a profile associated with that category
     *
     * This is used in the interface category
     * 
     * @return array array of categories 
     */
    public function getCategotiresToProfiles() {
        $_db = ObjectModel::$db;
        $_db->select(array('*'));
        $_db->from('google_profile_category', "a", true);

        $result = $_db->db_array_query($_db->query);
        if ($result) {
            foreach ($result as $cat) {
                $category[$cat['category_id']] = $cat['profile_id'];
            }
            return $category;
        } else
            return false;
    }

    /**
     * Delete profile by ID
     * 
     * @param int $id profile identifier
     * @return array array of categories 
     */
    public function deleteProfile($id) {
        $_db = ObjectModel::$db;
        $where = array();
        $where['id'] = array("value" => $id, "operation" => "=");
        $sql = $_db->delete_string('google_profile', $where, true);

        if ($_db->db_exec($sql, false, false))
            return true;
        return false;
    }

    /**
     * Gets an array of profiles
     *
     * This is used in profile interface
     * 
     * @return array array of profiles 
     */
    public function getProfiles() {
        $_db = ObjectModel::$db;
        $_db->select(array('*'));
        $_db->from('google_profile', "a", true);

        $result = $_db->db_array_query($_db->query);
        foreach ($result as &$value) {

            $_db->select(array('category_id'));
            $_db->from('google_profile_category', "a", true);
            $arr_where['a.profile_id'] = (int) $value['id'];
            $_db->where($arr_where);
            $cat_prof = $_db->db_array_query($_db->query);
            $category = array();
            foreach ($cat_prof as $cat) {
                $category[] = $cat['category_id'];
            }
            $value['categories'] = $category;
            $value['data'] = unserialize($value['data']);
        }
        unset($value);

        return $result;
    }

    /**
     * Get the value belonging to the profile. Name, link, description, for example
     *
     * This is used in profile interface
     * 
     * @return string common value 
     */
    public function getCommonValue($key) {
        $_db = ObjectModel::$db;

        $sql = "SELECT `value` FROM `google_profile_common` WHERE `key`= '$key'";
        $res = $_db->db_query_str($sql, false, false);

        if (isset($res[0]))
            return $res[0]['value'];

        return '';
    }

    /**
     * Save common values in DB
     *
     * This is used in profile interface
     * 
     * @return null
     */
    public function saveCommon($data) {
        $_db = ObjectModel::$db;
        foreach ($data as $k => $item) {
            $sql = "INSERT INTO `google_profile_common` VALUES ('$k', '$item' ) ON DUPLICATE KEY UPDATE value = '$item'";
            $_db->db_exec($sql, false, false);
        }
    }

    /**
     * Downloads in the database category for all the above language
     *
     * @param array $locales all locales for import in DB
     * @return null
     */
    public function saveCategoryLanguages($locales) {
        $url = "http://www.google.com/basepages/producttype/taxonomy-with-ids.";

        $_db = ObjectModel::$db;
        $sql = "TRUNCATE `google_category_lang`;";
        $_db->db_exec($sql, false, false);
        $sql = "SET autocommit=0;";
        $_db->db_exec($sql, false, false);

        foreach ($locales as $lang => $locale) {
            $handle = GoogleSpider::downloadFile($url . $locale . ".txt");
            fgets($handle, 1000);
            while (($data = fgets($handle, 1000)) !== FALSE) {
                $m = explode("-", $data);
                $m = array_map("trim", $m);
                $insert['lang'] = $lang;
                $insert['id'] = $m[0];
                $insert['text'] = $m[1];
                $sql = $_db->insert_string('google_category_lang', $insert, true);
                $_db->db_exec($sql, false, false);
            }
            $meta_data = stream_get_meta_data($handle);
            $filename = $meta_data["uri"];
            fclose($handle);
            unlink($filename);
        }
        $sql = "COMMIT;";
        $_db->db_exec($sql, false, false);
    }

    /**
     * Get taxonomy name by language, for example: category from google
     *
     * @param string $table reference table with taxonomy from DB
     * @param string $iso_code taxonomy language code
     * @return array array of all taxonomy names for this language code
     */
    public function getTaxonomyByLanguage($table, $iso_code) {

        $_db = ObjectModel::$db;

        $_db->select(array('id', "text"));
        $_db->from($table, "a", true);
        $arr_where['a.lang'] = $iso_code;
        $_db->where($arr_where);
        $tax_arr = $_db->db_array_query($_db->query);
        $taxonomy = array();
        foreach ($tax_arr as $tax) {
            $taxonomy[$tax['id']] = $tax['text'];
        }

        return $taxonomy;
    }

    /**
     * Update old profile from admin interface
     *
     * @param string $id id for old profile
     * @param string $name for profile
     * @param string $mapping profile mapping fields
     * @return string return id profile if success if not return FALSE;
     */
    public function saveProfile($id, $name, $mapping) {
        $_db = ObjectModel::$db;

        $data = array();
        $data['data'] = serialize($mapping);
        $data['name'] = $name;
        $where = array();
        $where['id'] = array("value" => $id, "operation" => "=");
        $sql = $_db->update_string('google_profile', $data, $where, 0, true);

        if ($_db->db_exec($sql, false, false))
            return $_db->db_last_insert_rowid();
        return false;
    }

    /**
     * Save new profile from admin interface
     *
     * @param string $name for profile
     * @param string $mapping profile mapping fields
     * @return string return id profile if success if not return FALSE;
     */
    public function newProfile($name, $mapping) {

        $_db = ObjectModel::$db;
        $data = array();
        $data['data'] = serialize($mapping);
        $data['name'] = $name;
        $sql = $_db->insert_string('google_profile', $data, true);


        if ($_db->db_exec($sql, false, false))
            return $_db->db_last_insert_rowid();
        return false;
    }

    /**
     * Bind the profile to a category
     *
     * @param int $category_id category ID
     * @param int $profile_id profile ID
     * @return string return id if success if not return FALSE;
     */
    public function setProfileToCategory($category_id, $profile_id) {
        $_db = ObjectModel::$db;
        $data = array();
        $data['category_id'] = $category_id;
        $data['profile_id'] = $profile_id;

        $sql = $_db->replace_string('google_profile_category', $data, false, true);

        if ($_db->db_exec($sql, false, false))
            return $_db->db_last_insert_rowid();
        return false;
    }

    /**
     * Get category names for this IDs(product type IDs from interface) and language code
     *
     * @param string $types_id product type IDs from interface
     * @param int $lang language code
     * @return array return array of names or FALSE
     */
    public function getGoogleTypes($types_id, $lang) {
        $_db = ObjectModel::$db;
        $_db->select(array('text'));
        $_db->from('google_category_lang', '', true);

        $arr_where['id'] = "($types_id)";
        $_db->where($arr_where, 'IN');
        $_db->where(array('lang' => $lang));

        $result = $_db->db_array_query($_db->query);
        if ($result) {
            foreach ($result as $k => $item) {
                $result[$k] = htmlspecialchars($item['text']);
            }
        } else {
            return false;
        }

        return $result;
    }

    /**
     * The processing function. in order to properly form an array of 
     * combinations (and merging into groups by specified rules from profile) 
     * before creating the XML
     *
     * @param array $product product data
     * @param array $profile profile data(mapping fields)
     * @param string $iso_code language code
     * @return array return array of names or FALSE
     */
    function prepareCombinations($product, $profile, $iso_code) {
	
//        print_r($combs);
//        print_r($fields);
        $combinations = array();
        if (!empty($product['combination'])) {
            //if product have combinations then prepare by each combination
            foreach ($product['combination'] as $comb_id => &$comb) {
                foreach ($profile['item_mapping'] as $key_map_item => $map_item) {
                    //itterate by item mapping of profile
                    if (!$map_item)
                        continue;
                    //preparing items by their type
                    switch ($map_item['type']) {
                        case "text":
                            $comb[$key_map_item] = htmlspecialchars($map_item['value']);
                            break;
                        case "array_text":
                            //if type is product_type, get google types names by their id and iso_code
                            if ($key_map_item == 'product_type') {
                                $types = implode(',', unserialize($map_item['value']));
                                $comb[$key_map_item] = $this->getGoogleTypes($types, $iso_code);
                            }
                            break;
                        case "item_field":
                            //getting item field value
                            if (!isset($product[$map_item['value']]))
                                break;
			    
			    //var_dump($product[$map_item['value']], isset($product[$map_item['value']][$iso_code]));
                            $node_val = isset($product[$map_item['value']][$iso_code]) && is_array($product[$map_item['value']]) ? $product[$map_item['value']][$iso_code] : $product[$map_item['value']];
			    
                            //if type is product image
                            if (isset($product[$map_item['value']][1]) && isset($product[$map_item['value']][1]['url']) && ($map_item['value'] == 'images'))
                                $node_val = $product[$map_item['value']][1]['url'];

                            //if value is manufacturer
                            if ($map_item['value'] == 'manufacturer') {
				if(isset($product[$map_item['value']][0]))
				    $node_val = array_values($product[$map_item['value']][0]);
                            }

                            //if value is weight
                            if ($map_item['value'] == 'weight') {
                                $node_val = $product[$map_item['value']]['value'] . ' ' . $product[$map_item['value']]['unit'];
                            }
                            
                            if (is_array($node_val) || (empty($node_val)))
                                break;
                            $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                            break;
                        case "comb_field":
                            //getting comb field value
                            $node_val = isset($comb[$map_item['value']][$iso_code]) ? $comb[$map_item['value']][$iso_code] : $comb[$map_item['value']];
                            
                            if (is_array($node_val) || (empty($node_val)))
                                break;
                            $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                            break;
                        case "feature":
                            //getting feature value
                            if (!isset($product['feature'][$map_item['value']]['value']))
                                break;

                            $ft = array_shift($product['feature'][$map_item['value']]['value']);
                            if (!isset($ft['name'][$iso_code]))
                                break;

                            $comb[$key_map_item] = $ft['name'][$iso_code];
                            break;
                        case "attribute":
                            //getting attribute value
                            //var_dump($key_map_item,$map_item['value'],$comb['attributes'],$comb['attributes'][$map_item['value']]['value']);
                            //die();

                            if (!isset($comb['attributes'][$map_item['value']]['value']))
                                break;

                            $att = array_shift($comb['attributes'][$map_item['value']]['value']);
                            if (!isset($att['name'][$iso_code]))
                                break;

                            $comb[$key_map_item] = $att['name'][$iso_code];
                            break;

                        default:
                            break;
                    }
                    $comb['parent_id'] = $product['id_product'];
                }

                //if shipping is checked then busting all existing carries
                if (isset($profile['shipping']))
                    if ($profile['shipping'] == "Yes")
                        foreach ($product['carrier'] as $key => $value) {
                            $comb['shipping_array'][] = array("g:service" => $value['name'], "g:price" => $value['price']);
                        }

                $combinations[''][] = $comb;
            }

            unset($comb);

//        $combinations[''] = array_merge($combinations[''], $combinations['']);
//        $combinations[''][0]['size_type'] = 'ssss';
            if (count($combinations['']) > 1)
                //if grouping is checked then group selected fields
                if ($profile['group'] == "Yes")
                    foreach ($profile['grouping'] as $field => $value) {
                        $combinations = $this->combinationsGroupingByField($combinations, $field);
                    }
        } else {
            //if product doesn't have combinations we doing the same staff but without busting combinations
            $comb = array();
            foreach ($profile['item_mapping'] as $key_map_item => $map_item) {
                if (!$map_item)
                    continue;
                //preparing items by their type
                switch ($map_item['type']) {
                    case "text":
                        $comb[$key_map_item] = htmlspecialchars($map_item['value']);
                        break;
                    case "array_text":
                        //if type is product_type, get google types names by their id and iso_code
                        if ($key_map_item == 'product_type') {
                            $types = implode(',', unserialize($map_item['value']));
                            $comb[$key_map_item] = $this->getGoogleTypes($types, $iso_code);
                        }
                        break;
                    case "item_field":
                        //getting item field value
                        if (!isset($product[$map_item['value']]))
                            break;
                        $node_val = isset($product[$map_item['value']][$iso_code]) ? $product[$map_item['value']][$iso_code] : $product[$map_item['value']];
                        //if product image
                        if (isset($product[$map_item['value']][1]['url']) && ($map_item['value'] == 'images'))
                            $node_val = $product[$map_item['value']][1]['url'];

                        if ($map_item['value'] == 'manufacturer') {
                            $node_val = array_values($product[$map_item['value']][0]);
                        }

                        if ($map_item['value'] == 'weight') {
                            $node_val = $product[$map_item['value']]['value'] . ' ' . $product[$map_item['value']]['unit'];
                        }

                        if (is_array($node_val) || (empty($node_val)))
                            break;
                        $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                        break;
                    case "feature":
                    //getting feature value
                        if (!isset($product['feature'][$map_item['value']]['value']))
                            break;

                        $ft = array_shift($product['feature'][$map_item['value']]['value']);
                        if (!isset($ft['name'][$iso_code]))
                            break;

                        $comb[$key_map_item] = $ft['name'][$iso_code];
                        break;

                    default:
                        break;
                }
            }

            //if shipping is checked then busting all existing carries
            if (isset($profile['shipping']))
                if ($profile['shipping'] == "Yes")
                    foreach ($product['carrier'] as $key => $value) {
                        $comb['shipping_array'][] = array("g:service" => $value['name'], "g:price" => $value['price']);
                    }

            $combinations[''][] = $comb;
        }


        return $combinations;
    }

    /**
     * Helper function to group combinations by a certain field 
     *
     * @param array $combinations array of combinations 
     * @param string $field field key for grouping

     * @return array An array of grouped combinations
     */
    function combinationsGroupingByField($combinations, $field) {

        foreach ($combinations as $comb_group => $combinations_array) {
            foreach ($combinations_array as $key => $comb) {
                if (isset($comb[$field])) {
                    $combinations[$comb_group . $comb[$field]][] = $comb; //add new
                    unset($combinations[$comb_group][$key]);
                }
            }
            if (empty($combinations[$comb_group]))
                unset($combinations[$comb_group]);
        }


        return $combinations;
    }

    /**
     * The main function for generating XML
     * 
     * Takes to cycle all items from DB in the specified rules and generates them XML
     * 
     *
     * @param string $user user ID
     * @param int $id_shop shop ID
     * @param string $lang language id
     * @param string $mode user ID
     * @param array $iso_code language code
     * @param string $profiles array of profiles
     * @return output Show xml on page
     */
    //function getXmlFeedByArray($profile, $user, $id_shop, $mode = null, $lang = null, $iso_code = "fr", $profile_id) {
    function getXmlFeedByArray($user, $id_shop, $lang, $mode = null, $iso_code, $profiles) {
//        $profile = $profile['data'];
//        if (!isset($profile['id']))
//            $profile['id'] = $profile_id;
//
        //get array of currencies by id_shop
        $currencies = array();
        foreach ($this->getCurrency($user, $id_shop) as $cur) {
            $currencies[mb_strtolower($cur['name'])] = $cur['iso_code'];
        }

        //get profiles linked to categories
        $categories_to_profiles = $this->getCategotiresToProfiles();
        if (!$categories_to_profiles)
            return;

//        print_r($profile);die();
        header('Content-Type: text/xml; charset=utf-8', true); //set document header content type to be XML

        $xml = new DOMDocument("1.0", "UTF-8"); // Create new DOM document.
//create "RSS" element
        $rss = $xml->createElement("rss");
        $rss_node = $xml->appendChild($rss); //add RSS element to XML node
        $rss_node->setAttribute("version", "2.0"); //set RSS version
//set attributes
        $rss_node->setAttribute("xmlns:g", "http://base.google.com/ns/1.0");


//create "channel" element under "RSS" element
        $channel = $xml->createElement("channel");
        $channel_node = $rss_node->appendChild($channel);

//add general elements under "channel" node

        $channel_node->appendChild($xml->createElement("title", $this->getCommonValue("title"))); //title
        $channel_node->appendChild($xml->createElement("description", $this->getCommonValue("description")));  //description
        $channel_node->appendChild($xml->createElement("link", $this->getCommonValue("link"))); //website link 
        $channel_node->appendChild($xml->createElement("language", $iso_code));  //language


        $page = 1;
        $data = $this->exportProductsArray($user, $id_shop, $mode, $iso_code, $page);

            //start busting products
            foreach ($data['product'] as $key => $product) {
                
                if (!isset($categories_to_profiles[$product['id_category_default']]))
                    continue;

                $profile_id = $categories_to_profiles[$product['id_category_default']];
                $profile = $profiles[$profile_id];
                $profile = $profile['data'];
//                if (count($product['combination']) < 2)
//                    continue;
                $combinations = $this->prepareCombinations($product, $profile, $iso_code);
                
//                if ($key==10)
//                $product['combination']=array();
                foreach ($combinations as $comb_group_name => $comb_array) {
                    foreach ($comb_array as $key => $comb) {


                        $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"
                        if ($comb_group_name !== "")
                            $item_node->appendChild($xml->createElement("g:item_group_id", $comb["parent_id"] . "-" . $comb_group_name));

                        foreach ($profile['item_mapping'] as $key_map_item => $map_item) {
                            //var_dump($map_item);
                            //var_dump(isset($comb[$key_map_item]));
                            if (isset($comb[$key_map_item]))
                                if (!is_array($comb[$key_map_item])) {
                                    if (strlen($comb[$key_map_item]) < 50) {
                                        if ($map_item['tag'] == "g:id")
                                            $comb[$key_map_item] .= '-' . $iso_code;
                                        if ($map_item['tag'] == "g:price")
                                            $comb[$key_map_item] = round($comb[$key_map_item], 2) . ' ' . $currencies[$product['currency']];


                                        $item_node->appendChild($xml->createElement($map_item['tag'], $comb[$key_map_item]));
                                    } else {
                                        //if lenght of item bigger than 50 - wrap data in CDATA 
                                        $node = $item_node->appendChild($xml->createElement($map_item['tag']));
                                        $cdata_content = $xml->createCDATASection($comb[$key_map_item]);
                                        $cdata_node = $node->appendChild($cdata_content);
                                    }
                                } else {
                                    //section for multiplie nodes like product_type
                                    foreach ($comb[$key_map_item] as $item) {
                                        $node = $item_node->appendChild($xml->createElement($map_item['tag']));
                                        $cdata_content = $xml->createCDATASection($item);
                                        $cdata_node = $node->appendChild($cdata_content);
                                    }
                                }
                        }
                        
                        if (isset($comb['shipping_array'])) {//if shipping checkbox enabled
                            foreach ($comb['shipping_array'] as $shipping) {
                                $shipping_node = $item_node->appendChild($xml->createElement("shipping")); //create a new node called "shipping
                                foreach ($shipping as $tag => $ship_value) {
                                    $shipping_node->appendChild($xml->createElement($tag, $ship_value));
                                }
                            }
                        }
                    }
                }
            }

            flush();
        //}

        echo $xml->saveXML();
    }

}
