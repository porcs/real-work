<?php

require_once(dirname(__FILE__) . '/por.config.php');
require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.install.php');

class PorInstall extends MarketplacesInstall
{
    const _PREFIX = _PREFIX_;
     
    public function __construct($user) 
    {
	parent::__construct($user, self::_PREFIX);
    }
}