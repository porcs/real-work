<?php

require_once(dirname(__FILE__) . '/../../tools.php');
require_once( dirname(__FILE__) . '/../../Smarty/Smarty.class.php' );
require_once realpath(dirname(__FILE__)).'/../classes/amazon.dispatcher.php';
require_once realpath(dirname(__FILE__)).'/../classes/amazon.tools.php';
require_once realpath(dirname(__FILE__)).'/../classes/amazon.xsd.php';
require_once realpath(dirname(__FILE__)).'/../classes/amazon.userdata.php';
require_once realpath(dirname(__FILE__)).'/../classes/amazon.database.php';
require_once realpath(dirname(__FILE__)).'/../../../../libraries/ci_db_connect.php';

if(!defined("BASEPATH"))
    define('BASEPATH',dirname(__FILE__).'/../../../../system');

if(!isset($config['base_url']))
    include(dirname(__FILE__) . '/../../../config/config.php');

// Report all PHP errors
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Set time zone
date_default_timezone_set('UTC');

define('_ADMIN_EMAIL_', 'support@feed.biz');
define('_EMAIL_LOGO_', '/nblk/images/feedbiz_logob.png');
define('_FEED_LANG_FILE_', 'amazon_feed');
define('_REPORT_LANG_FILE_', 'amazon_report');
define('_ORDER_LANG_FILE_', 'amazon_order');
define('_REPRICING_FILE_', 'amazon_repricing');
if(!defined('_BASE_URL_')){
    define('_BASE_URL_', $config['base_url']);
}
define('_SITE_TITLE_', 'Feed.biz');
define('_ID_MARKETPLACE_AMZ_', 2);
if(!defined('_MARKETPLACE_NAME_')){
    define('_MARKETPLACE_NAME_', 'Amazon');
}
define('_TABLE_PREFIX_', '');

if(!defined("DIRECTORY_SEPARATOR"))
    define('DIRECTORY_SEPARATOR', "/");

// Smarty
global $smarty;
$smarty = new Smarty();
$smarty->compile_dir = dirname(__FILE__) . "/../../../views/templates_c";
$smarty->template_dir = dirname(__FILE__) . "/../../../views/templates";