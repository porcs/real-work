<?php
require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__).'/amazon.tools.php');

class ValidValue {
    
    const TABLE 		= 'amazon_valid_values';
    const TABLE_CUSTOM          = 'amazon_valid_values_custom';
    const FILE_GZ		= 'amazon_valid_values.sql.gz';
    const FILE_SQL		= 'amazon_valid_values.sql';
    const FILE_MD5		= 'amazon_valid_values.md5';
    
    const SOURCE_URL	= 'https://dl.dropboxusercontent.com/u/53469716/amazon/data/'; // Public directory on DropBox
   
    public $AttributeException;
    public $FieldException;
    public $MeasureException;
    public $SWVGException;
    public $AttributePrimaryFields;
    public $ext_id;
    public static $CountryException = array(
        'CountryOfOrigin' => 1,
    );
            
    public function __construct($ext) {  
        
        $this->ci = & get_instance();          
        $this->ext_id = Amazon_Tools::domainToId($ext);
        
        $this->AttributeException = array(
            'Size' => 'SizeMap',
            'Color' => 'ColorMap',
        );        
        
        $this->AttributePrimaryFields = array(
            'ASIN' => true,
            'SKU' => true,
            'Color' => true, 
            'ColorMap' => true,
            'Size' => true, 
            'SizeMap' => true,
            'StandardProductID' => true,
            'ConditionType' => true,
            'Title' => true,
            'Manufacturer' => true,
            'Brand' => true,            
            'PartNumber' => true,            
            'ManufacturerPartNumber' => true,
            'ItemPackageQuantity' => true,            
            'DistributionDesignation' => true,            
            'ProductType' => true,            
            'GCID' => true,            
            'Material' => true,            
            'ProductCategory' => true,
        );
    }    
                
    public function getAttributesForProductType($product_type=null){        

	$merged = array();
        $merged['main'] = $this->AttributePrimaryFields;

	$where_universe = $universe_field = '';
	if(isset($product_type) && !empty($product_type)){

            if ($product_type == 'ClothingAccessories')
                $product_type = 'ProductClothing';
            
	    $where_universe = "AND universe = '" . trim($product_type) . "'";
	} else {
	    $universe_field = ', universe';
	}
	
        $sql =  "SELECT attribute_field $universe_field FROM amazon_valid_values "
                . "WHERE region = '" . $this->ext_id ."' $where_universe AND attribute_field NOT IN ('SizeMap', 'ColorMap') "
                . "GROUP BY attribute_field"; 
        
        $query = $this->ci->db->query($sql);
        $arrRows = array();
        
        foreach ($query->result() as $rows){
	    
	    if(isset($product_type) && !empty($product_type)){
		
		$arrRows[$rows->attribute_field] = true;
		$merged = array_merge($this->AttributePrimaryFields, $arrRows);
		
	    } else {
		
		$arrRows[$rows->universe][$rows->attribute_field] = true;
		$merged[$rows->universe] = array_merge($this->AttributePrimaryFields, $arrRows[$rows->universe]);
	    }
        }
	
        return $merged;   
    }   
        
    public function get_valid_value($product_type, $attribute_field, $product_sub_type = null ) {
        
        $data = $where = $history = array();
        
        if(isset(ValidValue::$CountryException[$attribute_field])){
            $rows = $this->getCountries();
            foreach($rows as $key => $row) {
                $data[$key]['value'] = $row->iso_code;
                $data[$key]['desc'] = $row->name;
            }           
        } else {
                               
            $where['region'] = $this->ext_id;
            $where['universe'] = $product_type;
            $where['attribute_field'] = isset($this->AttributeException[$attribute_field]) ? $this->AttributeException[$attribute_field] : $attribute_field;
            $this->ci->db->where($where);
            $query = $this->ci->db->from('amazon_valid_values');
            $query = $this->ci->db->get();
            $rows = $query->result();
            
            foreach($rows as $key => $row) {
                /* Check if they has product_sub_type else set this value to all field */
                if(isset($row->product_type) && !empty($row->product_type) && isset($product_sub_type) && !empty($product_sub_type) )
                {
                    $history[$row->product_type] = true;
                }
                if($row->product_type === $product_sub_type && isset($history) && !empty($history))
                {
                    $data[$key]['value'] = $row->valid_value;
                    $data[$key]['desc'] = $row->valid_value;
                }
                elseif(!isset($history) || empty($history))
                {
                    $data[$key]['value'] = $row->valid_value;
                    $data[$key]['desc'] = $row->valid_value;
                }           
            }               
        }

        return $data;
    }
    
    public function get_mapping_colors($array_name) {
        
        $query_parts = array();
        foreach ($array_name as $val) {
            if(isset($val) && !empty($val)){
                $query_parts[] = "'".$this->ci->db->escape_str($val)."'";
            }
        }
                       
        $string = implode(' OR value = ', $query_parts);        
       
        $tank = "SELECT color_name FROM mapping_color WHERE region = '".$this->ext_id."' AND value = {$string} ; " ;
           
        $row = $this->ci->db->query($tank);
        return $row->result();
        
    }
    
     public function move_mapping_colors() {
        
        $history = $data = array();
       
        $regions =  array('de','es','fr','it','uk','us','ca') ;
        
        foreach ($regions as $region) {
            
            $this->ci->db->query("SET NAMES 'UTF8';");
            $ext = 'color_'.$region;
           
            $sql = " SELECT color_name, $ext FROM mapping_colors  ; " ;
            
            $row = $this->ci->db->query($sql);
            $return = $row->result();
            
            foreach ($return as $val) {
                
                $color_value = unserialize($val->$ext);
                if(is_array($color_value)) {
                    foreach ($color_value as $cv){
                        $color_val = strtolower(!empty($cv) ? $cv : $val->color_name);
                        if(!isset($history[$region][$color_val])){
                            $data[] = array(
                                        'color_name' => $val->color_name,
                                        'region' => $region,
                                        'value' => $color_val,
                                        'date_add' => date('Y-m-d H:i:s')
                                     );
                        }    
                        $history[$region][$color_val] = true;
                    }
                } else {
                    $color_val = strtolower(!empty($color_value) ? $color_value : $val->color_name);
                    if(!isset($history[$region][$color_value])){
                        $data[] = array(
                                        'color_name' => $val->color_name,
                                        'region' => $region,
                                        'value' => $color_val,
                                        'date_add' => date('Y-m-d H:i:s')
                                     );
                    }
                    $history[$region][$color_val] = true;
                }
            }
        }
        //var_dump($data); exit;
        return ($this->ci->db->insert_batch('mapping_color', $data));        
    }
    
    public function getCountries(){ 
        
        $CountryIDRegion = array(
            'fr'    => 'fr',
            'us'    => 'en',
            'de'    => 'de',
            'it'    => 'it',
            'es'    => 'es',
            'uk'    => 'en',
            'ca'    => 'en',
            'mx'    => 'en',
            'com.mx'    => 'en',
            'com_mx'    => 'en',
        );
        
        $sql =  "SELECT * FROM country WHERE region = '" . $CountryIDRegion[$this->ext_id] . "' ; "; 
        $query = $this->ci->db->query($sql);
        
        return $query->result();
    }
    
    public function getCountryISOByName($name){ 
        
        $CountryIDRegion = array(
            '2' => 'fr',
            '3' => 'en',
            '4' => 'de',
            '5' => 'it',
            '6' => 'es',
            '7' => 'en',
            '10' => 'en',
            '32' => 'en',
        );
        
        $sql =  "SELECT iso_code FROM country WHERE region = '" . $CountryIDRegion[$this->ext_id] . "' AND name like '%".$name."%'; "; 
        $query = $this->ci->db->query($sql);
        
        foreach ($query->result() as $rows){
            return $rows->iso_code;
        }
    }  
}