<?php

require_once(dirname(__FILE__) . '/amazon.tools.php');

class AmazonSettings {
    
    const MANDATORY = 1;
    const RECOMMENDED = 2;
    
    public static $universe = 'universe';
    public static $product_types = 'product_types';
    public static $attributes = 'attributes';
    public static $mandatory_key = array('obligatoire', 'mandatory', 'Obligatoire', 'Required', 'Obbligatorio','Erforderlich','Obligatorio');
    public static $recommended_key = array('souhait', 'Souhaité', 'Preferred', 'Consigliato','Empfohlen','Recomendado');

    public static function get_translations($lang, $universe, $target = 'attributes') {
        
        $output_translations = array();
        $datadir = dirname(__FILE__) . '/../valid_value/translations/'.$target.'/';
     
        if ($universe == 'ClothingAccessories') {
            $universe = 'ProductClothing';
        }

        $filename = sprintf('%s.ini.gz', Amazon_Tools::toKey($universe));
       
        $file = $datadir . $filename;
        
        if (file_exists($file) && is_readable($file)) {
            $ini = parse_ini_string(file_get_contents('compress.zlib://' . $file), true);
            
            if (!is_array($ini) || !count($ini)){
                return(false);
            }

            if (array_key_exists($lang, $ini) && is_array($ini[$lang]) && count($ini[$lang])) {
                $output_translations = array();
                
                foreach ($ini[$lang] as $key => $translation) {
                    $new_key = Amazon_Tools::toKey(str_replace('_', '', $key));
                    $output_translations[$new_key] = Amazon_Tools::ucfirst(trim(stripslashes($translation)));
                }
            } elseif (array_key_exists('en', $ini) && is_array($ini['en']) && count($ini['en'])) {
                $output_translations = array();

                foreach ($ini['en'] as $key => $translation) {
                    $new_key = Amazon_Tools::toKey(str_replace('_', '', $key));
                    $output_translations[$new_key] = Amazon_Tools::ucfirst(trim(stripslashes($translation)));
                }
            }

            if (count($output_translations)) {
                return($output_translations);
            }
        }
        return(false);
    }

    public static function get_field_translation($lang, $universe) {
        $translations = array();
        
         $lang = Amazon_Tools::domainToId($lang); 
         
        if (!array_key_exists($lang, $translations) || !count($translations[$lang])) {
            $translations[$lang] = self::get_translations($lang, $universe);
        }
        
        return $translations[$lang];
    }

    public static function get_product_type_translation($lang, $universe) {
        $translations = array();
        
        $lang = Amazon_Tools::domainToId($lang); 
        
        if (!array_key_exists($lang, $translations) || !count($translations[$lang]))
            $translations[$lang] = self::get_translations($lang, $universe, 'product_types');
        
        return $translations[$lang];
     }

    public static function get_universes_translation($lang) {
        static $translations = array();
        
        $lang = Amazon_Tools::domainToId($lang);        
      
        if (!array_key_exists($lang, $translations) || !count($translations[$lang]))
            $translations[$lang] = self::get_translations($lang, 'universes', 'universes');

        if (array_key_exists($lang, $translations) && is_array($translations[$lang]) && count($translations[$lang])) {
            return($translations[$lang]);
        }

        return(null);
    }

    public static function get_fields_settings($lang, $universe, $product_type) {
        
        $datadir = dirname(__FILE__) . '/../valid_value/models/';

        if ($universe == 'ClothingAccessories')
            $universe = 'ProductClothing';
        if($universe == 'ProductClothing' && $product_type == 'ClothingAccessories')
            $product_type = 'accessory';
            
        $sub_dir = $datadir . Amazon_Tools::tokey($universe) . '/';
        
        $filename = sprintf('%s.%s.csv.gz', Amazon_Tools::tokey($product_type), $lang);
        $file = sprintf('%s%s', $sub_dir, $filename);
        
        if (file_exists($file) && is_readable($file)) {
            if (!($csvfh = fopen('compress.zlib://' . $file, 'r')))
                return(false);

            $field_settings = array();

            while ($data = fgetcsv($csvfh, 1024, ";")) {

                if (count($data) < 5) {
                    continue;
                }

                $key = str_replace('_', '', Amazon_Tools::toKey($data[0]));
                $name = str_replace(' ', '', Amazon_Tools::ucwords(str_replace('_', ' ', $data[0])));
                $translation = $data[1];
                $description = $data[2];
                $tip = $data[3];
                $sample = $data[4];
                $type_key = isset($data[5]) ? $data[5] : '';

                $type = null;
                if(strlen($type_key) > 0) {
                    if (in_array($type_key, self::$mandatory_key)) {
                        $type = self::MANDATORY;
                    } else if (in_array($type_key, self::$recommended_key)) {
                        $type = self::RECOMMENDED;
                    } 
                }
                
                $field_setting = array();
                $field_setting['key'] = $key;
                $field_setting['name'] = $name;
                $field_setting['translation'] = $translation;
                $field_setting['description'] = $description;
                $field_setting['tip'] = $tip;
                $field_setting['sample'] = $sample;
                $field_setting['type'] = $type;
                $field_settings[$key] = $field_setting;
            }
            
            if (count($field_settings)) {
                return($field_settings);
            }
        }
        return(false);
    }

    public static function get_field_settting($lang, $universe, $product_type) {
        
        $fields_settings = array();
        
        $lang = Amazon_Tools::domainToId($lang); 
        
        if (!array_key_exists($lang, $fields_settings) || !count($fields_settings[$lang]))
            $fields_settings[$lang] = self::get_fields_settings($lang, $universe, $product_type);

        if (is_array($fields_settings[$lang]) && count($fields_settings[$lang])) {
            return($fields_settings[$lang]);
        }
        
        return(null);
    }
    
}