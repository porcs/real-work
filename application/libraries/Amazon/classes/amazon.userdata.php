<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__).'/../../UserInfo/configuration.php');

class AmazonUserData {
  
    public static function get($info, $repricing = false, $fba = false){
        
	if(!isset($info['user_id']) && !isset($info['user_name'])){ // verify user authenticate
	    return false;
	}
	if(!isset($info['ext']) && !isset($info['site'])){ // verify marketplace site
	    return false;
	}
	
        $mysql_db = new ci_db_connect();  
        $sql = "SELECT * FROM amazon_configuration ac 
                LEFT JOIN users u ON (u.id = ac.id_customer)
                LEFT JOIN languages l ON (l.id = u.user_default_language)
                WHERE ac.active = 1 AND ac.id_shop = ".(int) $info['id_shop']." ";
	
	if(isset($info['user_id'])){
	    $sql .= "AND ac.id_customer = ".(int) $info['user_id']." ";
	}
	
	if(isset($info['user_name'])){
	    $sql .= "AND ac.user_name = '".$info['user_name']."' ";
	} 
	
	if(isset($info['ext'])){
	    $sql .= "AND ac.ext = '".$info['ext']."' ";
	} 
	
	if(isset($info['site'])){
	    $sql .= "AND ac.id_country = ".(int) $info['site']." ";
	}

        $query = $mysql_db->select_query($sql);
        $data = array();
        while($row = $mysql_db->fetch($query)){ 
            
            $data['id_customer'] = (int) $row['id_customer'] ;
            $data['user_name'] = $row['user_name'];
            $data['user_code'] = $row['user_code'];
            $data['user'] = array('email' => $row['user_email'] , 'firstname' => $row['user_first_name'], 'lastname' => $row['user_las_name']) ;
            $data['id_country'] = $row['id_country'];
            $data['ext'] = $row['ext'] ;
            $data['countries'] = str_replace(array(' '), '_', $row['countries']);
            $data['currency'] = $row['currency'];
            $data['merchant_id'] = $row['merchant_id'];
            $data['aws_id'] = $row['aws_id'];
            $data['secret_key'] = $row['secret_key'];
            $data['auth_token'] = $row['auth_token'];
            $data['id_mode'] = $row['id_mode'];
            $data['id_shop'] = $row['id_shop'];
            $data['id_lang'] = $row['id_lang'];
            $data['iso_code'] = $row['iso_code'];
            $data['id_region'] = $row['id_region'];
            $data['allow_automatic_offer_creation'] = isset($row['allow_automatic_offer_creation']) && $row['allow_automatic_offer_creation'] == 1 ? true : false;
            $data['synchronize_quantity'] = isset($row['synchronize_quantity'] ) && $row['synchronize_quantity'] == 1 ? true : false;
            $data['synchronize_price'] = isset($row['synchronize_price']) && $row['synchronize_price'] == 1 ? true : false;
            $data['synchronization_field'] = 'ean13';
            $data['active'] = isset($row['active']) && $row['active'] == 1 ? true : false;
            $data['creation'] = isset($row['creation']) && $row['creation'] == 1 ? true : false;
            $data['language'] = isset($row['language_name']) ? $row['language_name'] : 'english' ;  
            $data['id_marketplace'] = isset($info['id_marketplace']) ? $info['id_marketplace'] : 2 ;             
            $data['allow_send_order_cacelled'] = isset($row['allow_send_order_cacelled']) && $row['allow_send_order_cacelled'] == 1 ? true : false;
            $data['create_out_of_stock'] = isset($row['create_out_of_stock']) && $row['create_out_of_stock'] == 1 ? true : false;
        }        
        
        if(!empty($data) && $repricing){
            $sql = "select name, value from amazon_repricing where id_customer = ".$data['id_customer']." and ext = '".$data['ext']."' and id_shop = ".$data['id_shop']." ; "; 
            $query = $mysql_db->select_query($sql);
            while($row = $mysql_db->fetch($query)){
                 $data['repricing_' . $row['name']] = $row['value'];
            }
        }
	
	// Get shop info
	if(!empty($data)) {
	    
	    $ShopInfo = self::getShopInfo((int) $data['id_customer']);
	    $data['shop_name'] =  str_replace('_',' ',$ShopInfo['name']);
	}
	
        if(!empty($data) && $fba){
	    
	    $fba_info = self::getFBA($data['id_customer'], $data['ext'], $data['id_shop']);
	    $data = array_merge($data, $fba_info);
	    
        }
	
        return $data;
    }
    
    public static function getShopInfo($id_customer){      
	
        $config = new UserConfiguration();
        return $config->getShopInfo((int) $id_customer);        
       
    }
    
    /*public static function getFBAformulaByRegion($user_id, $id_shop, $region, $is_master=false){
	
	$mysql_db = new ci_db_connect();
	$data = array();
	
	$master = '';
	if($is_master)
	    $master = ' and fba_master_platform = 1 ';
	
	$sql = "select * from amazon_fba where id_customer = ".(int)$user_id." and region = '".$region."' and id_shop = ".(int)$id_shop." $master ; "; 
	$query = $mysql_db->select_query($sql);
	
	while($row = $mysql_db->fetch($query)){
	    
	    $key = $row['id_country'];
	    $data[$key]['region'] = $row['region'];
	    $data[$key]['fba_formula'] = $row['fba_formula'];
	    $data[$key]['fba_multichannel'] = $row['fba_multichannel'];
	    $data[$key]['fba_multichannel_auto'] = $row['fba_multichannel_auto'];
	    $data[$key]['fba_decrease_stock'] = $row['fba_decrease_stock'];
	    $data[$key]['fba_stock_behaviour'] = $row['fba_stock_behaviour'];
	    $data[$key]['fba_order_state'] = $row['fba_order_state'];
	    $data[$key]['fba_multichannel_state'] = $row['fba_multichannel_state'];
	    $data[$key]['fba_multichannel_sent_state'] = $row['fba_multichannel_sent_state'];
	    $data[$key]['fba_master_platform'] = $row['fba_master_platform'];
	}
	
	return $data;
    }*/
    
    public static function getFBA($user_id, $ext, $id_shop=null){
	
	$mysql_db = new ci_db_connect();
	$data = array();
	
	$sql = "select * from amazon_fba where id_customer = ".(int)$user_id." and ext = '".$ext."' "; 
	
	if(isset($id_shop) && !empty($id_shop))
	    $sql .= " and id_shop = ".(int)$id_shop." ; "; 
	    	
	$query = $mysql_db->select_query($sql . " ; ");
	
	while($row = $mysql_db->fetch($query)){
	    
	    $data['region'] = $row['region'];
	    $data['fba_formula'] = $row['fba_formula']; 
	    $data['fba_estimated_fees'] = $row['fba_estimated_fees'];
	    $data['fba_fulfillment_fees'] = $row['fba_fulfillment_fees'];	    
	    $data['default_fba_fees'] = $row['default_fba_fees'];
	    $data['default_fba_estimated_fees'] = $row['default_fba_estimated_fees'];
	    $data['default_fba_fulfillment_fees'] = $row['default_fba_fulfillment_fees'];	    
	    $data['fba_multichannel'] = $row['fba_multichannel'];
	    $data['fba_multichannel_auto'] = $row['fba_multichannel_auto'];
	    $data['fba_decrease_stock'] = $row['fba_decrease_stock'];
	    $data['fba_stock_behaviour'] = $row['fba_stock_behaviour'];
	    $data['fba_order_state'] = $row['fba_order_state'];
	    $data['fba_multichannel_state'] = $row['fba_multichannel_state'];
	    $data['fba_multichannel_sent_state'] = $row['fba_multichannel_sent_state'];
	    $data['fba_master_platform'] = $row['fba_master_platform'];
	}
	
	return $data;
    }
    
    public static function getMasterFBA($user_name, $site, $id_shop, $check_nearly_region=true){
	
	$mysql_db = new ci_db_connect();
	$data = array();
	
	if($check_nearly_region) // to get nearly country
	{
	    // 1. check Amazon fba site is active
	    $sql1 = "select * from amazon_fba where user_name = '$user_name' and id_shop = ".(int)$id_shop." and id_country = ".(int)$site." ;";
	    $query1 = $mysql_db->select_query($sql1);

	    while($row1 = $mysql_db->fetch($query1)){
		return $row1;
	    }	
	}
	
	//2. if not activate Amazon fba site find master region
	$sql2 = "select * from amazon_fba 
		where user_name = '$user_name' 
		and id_shop = ".(int)$id_shop." 
		and fba_master_platform = 1
		and region = (
		    select r.name_region as region from region r
		    left join offer_sub_packages osp on osp.id_region = r.id_region
		    where osp.id_offer_sub_pkg = ".(int)$site."
		) ;";

	$query2 = $mysql_db->select_query($sql2);
	
	while($row2 = $mysql_db->fetch($query2)){
	    return $row2;
	}
	
	//3. if master region in this region not set, but have site in this region use the site
	$sql3 = "select * from amazon_fba 
		where user_name = '$user_name' 
		and id_shop = ".(int)$id_shop." 
		and region = (
		    select r.name_region as region from region r
		    left join offer_sub_packages osp on osp.id_region = r.id_region
		    where osp.id_offer_sub_pkg = ".(int)$site."
		) ;";
	
	$query3 = $mysql_db->select_query($sql3);
	
	while($row3 = $mysql_db->fetch($query3)){
	    return $row3;
	}
	
	//4. if not found site in this region find other region
	$sql4 = "select * from amazon_fba 
		where user_name = '$user_name' 
		and id_shop = ".(int)$id_shop." 
		and fba_master_platform = 1 ;";
	
	$query4 = $mysql_db->select_query($sql4);
	
	while($row4 = $mysql_db->fetch($query4)){
	    return $row4;
	}
	
	return $data;
    }

    public static function getMasterFBAByRegion($user_name, $id_shop, $region){

	$mysql_db = new ci_db_connect();
	$data = array();

	$sql4 = "select * from amazon_fba 
		where user_name = '$user_name'
		and id_shop = ".(int)$id_shop."
                and region = '$region' 
		and fba_master_platform = 1 ;";

	$query4 = $mysql_db->select_query($sql4);

	while($row4 = $mysql_db->fetch($query4)){
	    return $row4;
	}

	return $data;
    }

    public static function getExtFBA($user_id, $id_shop, $region=null){
	
	$data = array();
	if(!isset($region) || empty($region)){
	    return $data;
	} 
	
	$mysql_db = new ci_db_connect();
	
	$sql = "select * from amazon_fba where id_customer = ".(int)$user_id." and id_shop = ".(int)$id_shop." AND region = '$region' ; "; 
	
	$query = $mysql_db->select_query($sql);
	
	while($row = $mysql_db->fetch($query)){	    
	    $data[$row['ext']] = $row['id_country'];	   
	}
	
	return $data;
	
    }

     public static function getAmazonCountriesByUsername($user_name, $id_shop, $only_active=false){

	$data = array();
	$mysql_db = new ci_db_connect();

	$sql = "select * from amazon_configuration where user_name = '$user_name' and id_shop = ".(int)$id_shop." ";

        if($only_active){
            $sql .= " and active = 1 ";
        }
        
	$query = $mysql_db->select_query($sql);

	while($row = $mysql_db->fetch($query)){
	    $data[$row['ext']] = $row['id_country'];
	}

	return $data;

    }
    
     public static function getCountryById($id_country){
	
	$data = array();
	if(!isset($region) || empty($region)){
	    return $data;
	} 
	
	$mysql_db = new ci_db_connect();
	
	$sql = "select * from amazon_fba where id_customer = ".(int)$user_id." and id_shop = ".(int)$id_shop." AND region = '$region' ; "; 
	$query = $mysql_db->select_query($sql);
	
	while($row = $mysql_db->fetch($query)){	    
	    $data[$row['ext']] = $row['id_country'];	   
	}
	
	return $data;
	
    }
} 