<?php

require_once(dirname(__FILE__) . '/../../FeedBiz.php');
require_once dirname(__FILE__) . '/amazon.database.php';
require_once dirname(__FILE__) . '/../functions/automaton.php';
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class AmazonReports 
{
    public function __construct($params, $debug=false, $cron=false)
    {
        $this->cron = $cron;
        $this->debug = $debug;
        $this->params = $params;
	
        $this->batchID = uniqid();
        $this->time = date('Y-m-d H:i:s');
        
        Amazon_Tools::load(_REPORT_LANG_FILE_, $this->params->language); 
        
        $this->message_type = sprintf('%s %s', Amazon_Tools::l('Amazon get report started on'), '') ;
        $this->message = new stdClass();
        $this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
	
        if(!$this->debug){ 
	    $this->startProcess(); 	    
	}
	
        $this->automaton = $this->AmazonAutomaton();
    }    

    //AFN_INVENTORY_DATA
    public function getReportAfnInventoryData()
    {
        $result = array();
        $output = "";
        $status = 0;
        $this->message->title = sprintf(Amazon_Tools::l('Amazon%s FBA - get Inventory started on %s'), '' /*$user->ext*/, date('Y-m-d H:i:s')) ;
        $this->message->type = sprintf(Amazon_Tools::l('Amazon%s FBA - get Inventory started on %s'), '' /*$user->ext*/, '');

        $Automaton = $this->automaton->Dispatch(AmazonAutomaton::AFN_INVENTORY_DATA, $status);

        if(!isset($Automaton) || empty($Automaton))
        {
            $this->message->error = Amazon_Tools::l('Unable to connect Automaton');
            if(isset($this->proc_rep))
            {
                $this->proc_rep->set_error_msg($this->message);
                $this->proc_rep->finish_task();
            }
            echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
            die();
        }
        else
        {
            $status = 1;
            $count = 1;
            do{
		if($this->debug)
		    echo '<pre> Step :' . print_r($Automaton->step, true) . '</pre>';

                if(isset($Automaton->errors) || !empty($Automaton->errors))
                {
                    $this->message->error = $Automaton->errors;
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }

                if($Automaton->processTimer > AmazonAutomaton::LIMIT_TIME)
                {
                    $this->message->error = sprintf(Amazon_Tools::l('TimeOut, Limit time : s, Process Time : s'),AmazonAutomaton::LIMIT_TIME,$Automaton->processTimer);

                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }

                $Automaton->processTimer = (int)$Automaton->processTimer + AmazonAutomaton::FIVE_MINUTES;

                if(isset($Automaton->message) && !empty($Automaton->message))
                {
                    $this->message->report = new stdClass();
                    $this->message->report->message = $Automaton->message;

                    if(isset($this->proc_rep))
                        $this->proc_rep->set_status_msg($this->message);
                }
                list($usec, $sec) = explode(" ", microtime());
                $start_time = $current_time = $sec;
                if($count==1)
                {
                    if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer)){
                        sleep($Automaton->resubmitTimer);
                    }
                }

                if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer))
                {
                    $limit_sec = $Automaton->resubmitTimer;

                    $sleep_step = 10;
                    while( ($current_time - $start_time) <= $limit_sec){

                        sleep($sleep_step);
                        list($usec, $sec) = explode(" ", microtime());
                        $current_time =$sec;
                    }
                }

                $count++;

                $result = $this->automaton->WizardProcess($status, $this->message->title, AmazonAutomaton::AFN_INVENTORY_DATA);

            }

            while ($Automaton->step < AmazonAutomaton::STEP_POST_PROCESS);
            
            $json = json_encode(
                    array(
                        'status' => isset($this->message->error) && strlen($this->message->error) ? 'error' : 'success' ,
                        'message' =>  isset($this->message->error) ? $this->message->error : $output)
                    ) ;
            
            if (isset($result) && !empty($result))
            {
                $this->message->report->status = Amazon_Tools::l('Success');
                $this->message->report->message = sprintf('%s', Amazon_Tools::l('success_remark'));
                if(isset($this->proc_rep))
                    $this->proc_rep->set_status_msg($this->message);

                echo $json ;

                if(isset($this->proc_rep) || !empty($this->proc_rep))
                    $this->proc_rep->finish_task($Automaton->flag);

                // start FBA management
                require_once dirname(__FILE__) . '/../functions/amazon.fba.stock.php';

                $amazon_stocks = new AmazonFBAStock($this->params, $this->cron, $this->debug);
                $amazon_stocks->Dispatch(false, false, 1, $result);

            } else {

                $this->message->report->status = Amazon_Tools::l('Fail');
                $this->message->report->message = sprintf('%s', Amazon_Tools::l('Fail, nothing todo with FBA'));
                if(isset($this->proc_rep))
                    $this->proc_rep->set_status_msg($this->message);

                echo $json ;

                if(isset($this->proc_rep) || !empty($this->proc_rep))
                    $this->proc_rep->finish_task($Automaton->flag);
                
            }
        }
    }

    //_GET_MERCHANT_LISTINGS_DATA_
    public function getShippingGroups()
    {
        $user = $this->params;

        if($this->debug) 
	    echo '<pre>' . print_r($user, true) . '</pre>';
        
        $output = "";
        $status = 0; 
        $this->message->title = sprintf(Amazon_Tools::l('Amazon%s get shipping groups started on %s'), $user->ext, date('Y-m-d H:i:s')) ;
        $this->message->type = sprintf(Amazon_Tools::l('Amazon%s  get shipping groups started on %s'), $user->ext, '');
       
        $Automaton = $this->automaton->Dispatch(AmazonAutomaton::MERCHANT_ACTIVE_LISTINGS_DATA, $status);
        
        if(!isset($Automaton) || empty($Automaton))
        {
            $this->message->error = Amazon_Tools::l('Unable to connect Automaton');
            if(isset($this->proc_rep))
            {
                $this->proc_rep->set_error_msg($this->message);
                $this->proc_rep->finish_task();
            }
            echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
            die();
        }
        else
        {
            $status = 1;
            $count = 1;
            do{
		if($this->debug) 
		    echo '<pre> Step :' . print_r($Automaton->step, true) . '</pre>';
		
                if(isset($Automaton->errors) || !empty($Automaton->errors))
                {
                    $this->message->error = $Automaton->errors;
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                if($Automaton->processTimer > AmazonAutomaton::LIMIT_TIME)
                {
                    $this->message->error = sprintf(Amazon_Tools::l('TimeOut, Limit time : s, Process Time : s'),AmazonAutomaton::LIMIT_TIME,$Automaton->processTimer);
                    
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                $Automaton->processTimer = (int)$Automaton->processTimer + AmazonAutomaton::FIVE_MINUTES;
                
                if(isset($Automaton->message) && !empty($Automaton->message))
                {
                    $this->message->report = new stdClass();
                    $this->message->report->message = $Automaton->message;
                    
                    if(isset($this->proc_rep))
                        $this->proc_rep->set_status_msg($this->message);
                }
                list($usec, $sec) = explode(" ", microtime());
                $start_time = $current_time = $sec;
                if($count==1)
                { 
                    if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer)){
                        sleep($Automaton->resubmitTimer);
                    }
                }
                
                if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer))
                {
                    $limit_sec = $Automaton->resubmitTimer;
                    
                    $sleep_step = 10;
                    while( ($current_time - $start_time) <= $limit_sec){ 
                        
                        sleep($sleep_step); 
                        list($usec, $sec) = explode(" ", microtime());
                        $current_time =$sec;
                    }
                }
                
                $count++;                
                
                $this->automaton->WizardProcess($status, $this->message->title,AmazonAutomaton::MERCHANT_ACTIVE_LISTINGS_DATA);
            }
            while ($Automaton->step < AmazonAutomaton::STEP_POST_PROCESS);  
            
            $this->message->report->status = Amazon_Tools::l('Success');
            $this->message->report->message = sprintf('%s', Amazon_Tools::l('success_remark'));   
            if(isset($this->proc_rep))
                $this->proc_rep->set_status_msg($this->message);
        }
                
        $json = json_encode( 
                array(
                    'status' => isset($this->message->error) && strlen($this->message->error) ? 'error' : 'success' , 
                    'message' =>  isset($this->message->error) ? $this->message->error : $output)
                ) ;
        echo $json ;

        if(isset($this->proc_rep) || !empty($this->proc_rep))
            $this->proc_rep->finish_task($Automaton->flag);
	
	exit;
    }
    
    //_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_
    public function getReportFulfillmentFees($datefrom, $dateto)
    {
        $user = $this->params;

        if($this->debug) 
	    echo '<pre>' . print_r($user, true) . '</pre>';
        
        $output = "";
        $status = 0; 
        $this->message->title = sprintf(Amazon_Tools::l('Amazon%s FBA - get data started on %s'), '' /*$user->ext*/, date('Y-m-d H:i:s')) ;
        $this->message->type = sprintf(Amazon_Tools::l('Amazon%s FBA - get data started on %s'), '' /*$user->ext*/, '');
       
        $Automaton = $this->automaton->Dispatch(AmazonAutomaton::FULFILLMENT_FEES, $status, null, null, null, $datefrom, $dateto);
        
        if(!isset($Automaton) || empty($Automaton))
        {
            $this->message->error = Amazon_Tools::l('Unable to connect Automaton');
            if(isset($this->proc_rep))
            {
                $this->proc_rep->set_error_msg($this->message);
                $this->proc_rep->finish_task();
            }
            echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
            die();
        }
        else
        {
            $status = 1;
            $count = 1;
            do{
		if($this->debug) 
		    echo '<pre> Step :' . print_r($Automaton->step, true) . '</pre>';
		
                if(isset($Automaton->errors) || !empty($Automaton->errors))
                {
                    $this->message->error = $Automaton->errors;
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                if($Automaton->processTimer > AmazonAutomaton::LIMIT_TIME)
                {
                    $this->message->error = sprintf(Amazon_Tools::l('TimeOut, Limit time : s, Process Time : s'),AmazonAutomaton::LIMIT_TIME,$Automaton->processTimer);
                    
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                $Automaton->processTimer = (int)$Automaton->processTimer + AmazonAutomaton::FIVE_MINUTES;
                
                if(isset($Automaton->message) && !empty($Automaton->message))
                {
                    $this->message->report = new stdClass();
                    $this->message->report->message = $Automaton->message;
                    
                    if(isset($this->proc_rep))
                        $this->proc_rep->set_status_msg($this->message);
                }
                list($usec, $sec) = explode(" ", microtime());
                $start_time = $current_time = $sec;
                if($count==1)
                { 
                    if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer)){
                        sleep($Automaton->resubmitTimer);
                    }
                }
                
                if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer))
                {
                    $limit_sec = $Automaton->resubmitTimer;
                    
                    $sleep_step = 10;
                    while( ($current_time - $start_time) <= $limit_sec){ 
                        
                        sleep($sleep_step); 
                        list($usec, $sec) = explode(" ", microtime());
                        $current_time =$sec;
                    }
                }
                
                $count++;                
                
                $this->automaton->WizardProcess($status, $this->message->title, AmazonAutomaton::FULFILLMENT_FEES);
            }
            while ($Automaton->step < AmazonAutomaton::STEP_POST_PROCESS);  
            
            $this->message->report->status = Amazon_Tools::l('Success');
            $this->message->report->message = sprintf('%s', Amazon_Tools::l('success_remark'));   
            if(isset($this->proc_rep))
                $this->proc_rep->set_status_msg($this->message);
        }
                
        $json = json_encode( 
                array(
                    'status' => isset($this->message->error) && strlen($this->message->error) ? 'error' : 'success' , 
                    'message' =>  isset($this->message->error) ? $this->message->error : $output)
                ) ;
        echo $json ;

        if(isset($this->proc_rep) || !empty($this->proc_rep))
            $this->proc_rep->finish_task($Automaton->flag);
    }

    public function getReport()
    {
        $user = $this->params;
	
        if($this->debug) 
	    echo '<pre>' . print_r($user, true) . '</pre>';
        
        $output = "";
        
        $status = 0; 
        $step = isset($user->mode) ?
		( ($user->mode == AmazonAutomaton::ACTION_WIZARD_CREATION_MODE) ? AmazonAutomaton::CREATION_MODE : AmazonAutomaton::MATCHING_MODE ) :
		( AmazonAutomaton::CREATION_MODE ) ;		
        
	if($this->debug) echo '<pre> Mode :' . print_r($step, true) . '</pre>';
	
        $this->message->title = sprintf('%s %s', Amazon_Tools::l('Amazon Marketplace Automaton started on'), $this->time) ;
        $this->message->type = $this->message_type;
        
        $Automaton = $this->automaton->Dispatch($step, $status);
        
        if(!isset($Automaton) || empty($Automaton))
        {
            $this->message->error = Amazon_Tools::l('Unable to connect Automaton');
            if(isset($this->proc_rep))
            {
                $this->proc_rep->set_error_msg($this->message);
                $this->proc_rep->finish_task();
            }
            echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
            die();
        }
        else
        {
            $status = 1;
            $count = 1;
            do{
		if($this->debug) 
		    echo '<pre> Step :' . print_r($Automaton->step, true) . '</pre>';
		
                if(isset($Automaton->errors) || !empty($Automaton->errors))
                {
                    $this->message->error = $Automaton->errors;
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                if($Automaton->processTimer > AmazonAutomaton::LIMIT_TIME)
                {
                    $this->message->error = sprintf(Amazon_Tools::l('TimeOut, Limit time : s, Process Time : s'),AmazonAutomaton::LIMIT_TIME,$Automaton->processTimer);
                    
                    if(isset($this->proc_rep))
                    {
                        $this->proc_rep->set_error_msg($this->message);
                        $this->proc_rep->finish_task();
                    }
                    echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
                    die();
                }
                
                $Automaton->processTimer = (int)$Automaton->processTimer + AmazonAutomaton::THREE_MINUTES;
                
                if(isset($Automaton->message) && !empty($Automaton->message))
                {
                    $this->message->report = new stdClass();
                    $this->message->report->message = $Automaton->message;
                    
                    if(isset($this->proc_rep))
                        $this->proc_rep->set_status_msg($this->message);
                }
                list($usec, $sec) = explode(" ", microtime());
                $start_time = $current_time = $sec;
                if($count==1)
                { 
                    if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer)){
                        sleep($Automaton->resubmitTimer);
                    }
                }
                
                if(isset($Automaton->resubmitTimer) && !empty($Automaton->resubmitTimer))
                {
                    $limit_sec = $Automaton->resubmitTimer;
                    
                    $sleep_step = 10;
                    while( ($current_time - $start_time) <= $limit_sec){ 
                        
                        sleep($sleep_step); 
                        list($usec, $sec) = explode(" ", microtime());
                        $current_time =$sec;
                    }
                }
                
                $count++;                
                
                $this->automaton->WizardProcess($status, $this->message->title);
            }
            while ($Automaton->step < AmazonAutomaton::STEP_POST_PROCESS);  
            
            $this->message->report->status = Amazon_Tools::l('Success');
            $this->message->report->message = sprintf('%s', Amazon_Tools::l('success_remark'));   
            if(isset($this->proc_rep))
                $this->proc_rep->set_status_msg($this->message);
        }
                
        $json = json_encode( 
                array(
                    'status' => isset($this->message->error) && strlen($this->message->error) ? 'error' : 'success' , 
                    'message' =>  isset($this->message->error) ? $this->message->error : $output)
                ) ;
        echo $json ;

        if(isset($this->proc_rep) || !empty($this->proc_rep))
            $this->proc_rep->finish_task($Automaton->flag);
    }
    
    public function startProcess()
    {
        $process_title =  'Amazon Marketplace Get Report Processing';
        $process_type =  'get_report_' . $this->params->countries;
    
        $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, false );
        $this->proc_rep->set_process_type($process_type); 
        $this->proc_rep->set_popup_display(false); 
        $this->proc_rep->set_marketplace('amazon');
        
        $process =$this->proc_rep->has_other_running($process_type);
        if($process)        
        {
            $this->proc_rep->finish_task();
            echo json_encode( array('status' => 'error' , 'message' => 'process are running, cannot get report until old process done')) ;
            die();
        }
    }
            
    private function AmazonAutomaton()
    {
        $auth = array(
            'user_name' => trim($this->params->user_name),
            'id_shop' => trim($this->params->id_shop),
            'id_country' => trim($this->params->id_country),
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),       
            'language' => ltrim($this->params->language),    
            'id_lang' => trim($this->params->id_lang),  
            'batchID' => trim($this->batchID),  
            'time' => trim($this->time),  
            'ext' => trim($this->params->ext),  
        );

        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
                $auth['auth_token'] = trim($this->params->auth_token);
        }
		
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => str_replace(array(".",".."), "_", ltrim($this->params->ext, "."))
        );
	
        if (!$automaton = new AmazonAutomaton($auth, $marketPlace, null, $this->debug, $this->cron))
        {
            $this->message->error = Amazon_Tools::l('Unable to login');
            
            if(isset($this->proc_rep))
            {
                $this->proc_rep->set_error_msg($this->message);
                $this->proc_rep->finish_task();
            }
            
            echo json_encode( array('status' => 'error' , 'message' => $this->message->error)) ;
            die();
        }
        return $automaton;
    }    
}