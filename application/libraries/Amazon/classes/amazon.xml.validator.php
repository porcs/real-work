<?php
/**
 * This class evaluates XML generated against XSD file
 *
 * @author eturcios
 */
class AmazonXmlValidator {
    private static $xml;
    private static $SKU;    
    private static $type; 
    private static $product; 
    private static $category;
    private static $non_mandatory_exceptions = array(
                                                    'Clothing'=>array('ClothingType'),
                                                    'Shoes'=>array('ClothingType')
                                               );
    private static $override_exception = array('Override' => array('IsShippingRestricted' => 1));
    private static $universe_definition = array();
    
    public static function validateEnvelope(DOMDocument $request, $type='Product', $data = null){
        
        $envelope = new DOMDocument();
        $envelope->loadXML(AmazonXSD::cache(AmazonXSD::$envelopeXSD));
        self::addEnvelopeDetailChoice($envelope, $type);        
        
        $envelopeXPath = new DOMXPath($envelope);
        $name = "AmazonEnvelope";
        
        //Look for element on XML Schema Definition
        $result_xsd = $envelopeXPath->query('xsd:element[@name="' . $name .'"]');
        if($result_xsd->length == 0){
            self::displayError(sprintf(Amazon_Tools::l("element_was_not_found_on_XSD"), $name), 1);
        }
        $item_xsd = $result_xsd->item(0);
        //Look for element on generated XML
        $result_xml = $request->getElementsByTagName($name);
        if($result_xml->length == 0){
            self::displayError(sprintf(Amazon_Tools::l("element_was_not_found_on_generated_XML"), $name), 2);
        }
       
        if(isset($data) && !empty($data)){
            self::$product = $data;
        }
            
        self::$type = $type;
        self::replaceReferences($item_xsd, $envelope);
        self::validateXMLDefinition($result_xml, $result_xsd);
    }
    
    /**
     * Includes the respective choice (FulfillmentCenter, Inventory, Product, etc) for this Schema
     * @param DOMDocument $envelope Envelope XSD used for validation
     * @param string $type Type of choice to append
     */
    public static function addEnvelopeDetailChoice(DOMDocument &$envelope, $type){

        $choices = $envelope->getElementsByTagName("choice");
        
        if($choices->length == 0){
            self::displayError(Amazon_Tools::l("Choice_of_content_could_not_be_set_for_this_Schema"), 3);
        }
        if(!isset(AmazonXSD::$choiceSchemas[$type])){            
            self::displayError(sprintf(Amazon_Tools::l("Type_s_could_not_be_included_in_envelope"), $type), 4);
        }

        $choice = $choices->item(0);
        $xpath = AmazonXSD::$choiceSchemas[$type];
        $type_list = $xpath->query('//xsd:element[@name="' . $type . '"]');
        
        foreach($type_list as $type_dom){           
            $choice->parentNode->replaceChild($envelope->importNode($type_dom, true), $choice);
            break;
        }
    }
    /**
     * Includes the soecific choice elements
     * @param DOMDocument $envelope Envelope XSD used for validation
     * @param string $type Type of choice to append
     */
    public static function addSpecificDetailChoice(DOMElement &$xsd, DOMElement &$xml){
        self::$xml = $xml;       
         
        //********************************************************************//
        //               Add Choices for Product Data                         //
        //********************************************************************//
        $xpath = new DOMXPath($xsd->ownerDocument);
        $xsdProductData = $xpath->query('//xsd:element[@name="ProductData"]');
        $break = false;
        $products = $xml->getElementsByTagName("Product");
        
        if($products->length > 0 && $xsdProductData->length > 0){
            
            foreach($products as $product){
                
                $productDatas = $product->getElementsByTagName("ProductData");
                
                if($productDatas->length > 0){
                    
                    $type = $productDatas->item(0)->firstChild;
                   
                    if($type){
                        
                        $tagsname = $type->tagName;
                       
                        $choices_02 = $xpath->query('.//xsd:choice', $xsdProductData->item(0));
                        $alredyAppended = $xpath->query('.//xsd:complexType/xsd:choice/xsd:element[@name="' . $tagsname . '"]', $xsdProductData->item(0))->length;

                        if($choices_02->length > 0 && !$alredyAppended){
                            $choice_01 = $choices_02->item(0);
                            $new_choice = $xsd->ownerDocument->createElementNS("http://www.w3.org/2001/XMLSchema","xsd:choice");

                            $universeXSD = isset(AmazonXSD::$universe[$tagsname]) ? AmazonXSD::$universe[$tagsname] : $tagsname;
                            
                            //universe mapping
                            $universe = $universeXSD;
                            foreach(AmazonXmlProductData::$_universe_tag_mapping as $name=>$mapping){
                                if($mapping == $universeXSD){
                                    $universeXSD=$name;
                                    break;
                                }
                            }
                            
                            if(!isset(self::$universe_definition[$universeXSD])){
                                //echo 'load 1 : 1 + ' ; 
                                self::$universe_definition[$universeXSD] = new DOMDocument();
                                self::$universe_definition[$universeXSD]->loadXML( AmazonXSD::cache(AmazonXSD::$definitionURL . $universeXSD . '.xsd') );
                            }
                            
                            $prodXPath = new DOMXPath(self::$universe_definition[$universeXSD]);
                            
                            /*$productDom = new DOMDocument();
                            $productDom->loadXML( AmazonXSD::cache(AmazonXSD::$definitionURL . $universeXSD . '.xsd') );
                            echo 'load 2 : 1 + ' ; 
                            $prodXPath = new DOMXPath($productDom);*/                            
                            
                            $toAppend = $prodXPath->query('xsd:element[@name="'. $tagsname . '"]');
                            foreach($toAppend as $node){
                                self::replaceReferences($node, $node->ownerDocument);
                                $new_choice->appendChild(
                                    $xsd->ownerDocument->importNode($node, true)
                                );
                            }                                   

                            $choice_01->parentNode->replaceChild($xsd->ownerDocument->importNode($new_choice, true), $choice_01);
                            $break = true;
                        }
                    }

                    foreach(self::$non_mandatory_exceptions as $x=>$y){
                        foreach($xpath->query('.//xsd:element[@name="' . $x . '"]', $xsdProductData->item(0)) as $child){

                            $name = $child->getAttribute ("name"); 
                            if(isset(self::$non_mandatory_exceptions[$name])){ 
                                foreach(self::$non_mandatory_exceptions[$name] as $i){
                                    $exceptions = $xpath->query('.//xsd:element[@name="' . $i . '"]', $child);
                                    foreach($exceptions as $exception){
                                        if(! ($exception instanceof DOMElement))
                                            continue;
                                        //$det_name = $exception->getAttribute("name");
                                        $min_occurs = $exception->getAttribute("minOccurs") != "" ? $exception->getAttribute("minOccurs") : 1;

                                        if($min_occurs){
                                            $exception->setAttribute("minOccurs", 0);
                                        }

                                    }
                                }
                            }
                        }
                    }
                    if($break)
                        break;
                }
            }
        }    
    }
    
    /**
     * Replaces references made to other types
     * @param DOMElement $reference
     * @param DOMDocument $productDom
     */
    private static function replaceReferences(DOMElement &$reference, DOMDocument &$productDom){
            $xpath = new DOMxpath($productDom);
            //Replace referenced elements to their corresponding structure
            $query = './/xsd:element[@ref]';
            $references = $xpath->query($query, $reference);
            if($references->length > 0){
                //Obtains structure from all referenced elements of product data
                foreach($references as $node){
                    if($node instanceof DOMElement){ 
                        $new_node = self::replaceReferencedElements($node, $productDom);
                        if($new_node== null)
                            continue;
                        $new = $productDom->importNode($new_node);
                        $node->parentNode->replaceChild($new, $node);
                        foreach($new->childNodes as $child){
                            if(!$child instanceof DOMElement)
                                continue;
                            self::replaceReferences($child, $productDom);
                        }
                    }
                }
            }
    }
    
    /**
     * Obtains structure of an element, when this reffers another element, by "ref" attribute
     * CLONED from AmazonForm Class.
     * @param DOMElement $element
     * @return boolean
     */
    private static function replaceReferencedElements(DOMElement $element, DOMDocument &$productDom){
        return AmazonXSD::getReferencedStructure($element, $productDom);
        
    }
    
    /**
     * 
     * @param DOMNodeList $xml
     * @param DOMNodeList $xsd
     */
    public static function validateXMLDefinition(DOMNodeList $xml, DOMNodeList $xsd){
        
        //print_r(array("xsd"=>$xsd->saveXML()));
        //print "<pre>" . print_r($xml->item(0)->ownerDocument->saveXML($xml->item(0)), true) . "</pre>";
        //global $product;
        $product = self::$product;
        
        if($xml->length > 0 && $xml->item(0)->parentNode){
            $tagsname = $xml->item(0)->nodeName;
            if($tagsname == "SKU") {
                self::$SKU = $xml->item(0)->nodeValue;
                self::$category = isset($product[self::$SKU]['ProductData']['product_category']) ? $product[self::$SKU]['ProductData']['product_category'] : '';               
            }          
        }
        
        foreach($xsd as $xsd_node){
            //DOMText might appear here, that's why only DOMElement is considered
            if(!$xsd_node instanceof DOMElement){
                continue;
            }
            /******************************************************************
             * These elements are not defined in any XSD, 
             * that's why it's ref is not found.
             *  - Amazon-Only
             *  - Amazon-Vendor-Only
             ******************************************************************/
            if($xsd_node->getAttribute("ref") == "Amazon-Vendor-Only" ||
               $xsd_node->getAttribute("ref") == "Amazon-Only")
                continue;
            
            AmazonXSD::getElementStructure($xsd_node, $xsd_node->ownerDocument);
            $xpath = new DOMXPath($xsd_node->ownerDocument);
            foreach($xpath->query('xsd:element[@type]') as $ref){
                AmazonXSD::getElementStructure($ref, $ref->ownerDocument);
            }
            $xsd_name = $xsd_node->getAttribute("name");
            $xsd_tag = str_replace("xsd:", "", $xsd_node->tagName);

            if($xsd_tag == "element") {

                $cur= 0;
                $min = $xsd_node->getAttribute("minOccurs") != "" ? $xsd_node->getAttribute("minOccurs") : 1;
                $max = $xsd_node->getAttribute("maxOccurs") != "" ? $xsd_node->getAttribute("maxOccurs") : 1;
                
                foreach($xml as $xml_node){ 
                   
                    if(!$xml_node instanceof DOMElement){
                        continue;
                    }
                    if($xsd_name != $xml_node->tagName){
                        continue;
                    }
                    self::addSpecificDetailChoice($xsd_node, $xml_node);  
                    //print_r ("<div>XML: " . $xml_node->tagName . "</div>".PHP_EOL);
                    self::validateSingleElement($xml_node, $xsd_node);
                    $cur += 1;
                }
                //********************************************************************//
                //                      Validate Min/Max Occurs                       //
                //********************************************************************//                
                if($cur < $min){
                    //continue;
                    if($xsd_name == "Message") {
                        self::displayError(sprintf(Amazon_Tools::l("No_items_found"), $min, $cur), 5);
                    } else {
                        self::displayError(sprintf(Amazon_Tools::l("element_must_exists_at_least"), $xsd_name, $min, $cur), 6);
                    }
                }
                if($max != "unbounded" && $cur > $max){   
                    self::displayError(sprintf(Amazon_Tools::l("element_must_exists_at_most"), $xsd_name, $max, $cur), 7);
                }
                
            }elseif($xsd_tag == "choice"){ 
                
                //print "<pre>" . print_r($xsd_node->ownerDocument->saveXML($xsd_node), true) . "</pre>";
                $xpath = new DOMXPath($xsd_node->ownerDocument);
                
                $choices = $xpath->query("xsd:element", $xsd_node);
                $min = $xsd_node->getAttribute("minOccurs");
                $chosen = false;
                $values = "";
                
                foreach($xml as $xml_node){ 
                    if(!$xml_node instanceof DOMElement){
                        continue;
                    }
                    foreach($choices as $choice){ 
                        if(!$choice instanceof DOMElement){
                            continue;
                        }

                        self::addSpecificDetailChoice($xsd_node, $xml_node); 
                        
                        $choice_name = $choice->getAttribute("name");
                        
                         
                        if($values){
                            $values .= ", " . $choice_name;
                        }else{
                            $values .= $choice_name;
                        }
                        //print "<pre>" . print_r(' $xml_node - >' . $xml_node->tagName . ' self::$category - >' . true) . "</pre>";
                        if($xml_node->tagName == $choice_name){
                            $chosen = true;
                            self::validateXMLDefinition($xml_node->childNodes, $choice->childNodes);
                            break;
                        } else if(isset(self::$override_exception[self::$type][$choice_name])){
                             $chosen = true;
                        }
                    }
                    if($chosen)
                        break;
                }
                if($min == "" && !$chosen && $values){
                    self::displayError(sprintf(Amazon_Tools::l("One_of_these_values_s_must_be_set"), $values), 8);
                }
                
            }else{                
                self::validateXMLDefinition($xml, $xsd_node->childNodes);
            }
        }
    }
    
    /**
     * Validate an XML Element against its XSD
     * @param DOMElement $xml_node
     * @param DOMElement $xsd_node
     */
    private static function validateSingleElement(DOMElement $xml_node, DOMElement $xsd_node){
        
            $value = $xml_node->nodeValue;
            $xml_tag = $xml_node->tagName;
           
            $xsd_xpath = new DOMXPath($xsd_node->ownerDocument); 
            $type = $xsd_node->getAttribute("type");
            
            if(!stripos($type, "xsd:") && $xsd_xpath->query(".//xsd:element",$xsd_node)->length == 0){
                AmazonXSD::getElementStructure($xsd_node, $xsd_node->ownerDocument);
            }
            
            if($xsd_node->getElementsByTagName("element")->length > 0){
                //********************************************************************//
                //           Validate Children Elements (recursive)                   //
                //********************************************************************//
                foreach($xsd_node->childNodes as $xsd_child){
                    if(!$xsd_child instanceof DOMElement){
                        continue;
                    }
                    self::validateXMLDefinition($xml_node->childNodes, $xsd_child->childNodes);
                }
            }else{
                //********************************************************************//
                //                  Verify if exists a restriction                    //
                //********************************************************************//
                $xpath = new DOMXPath($xsd_node->ownerDocument);
                $result1 = $xpath->query(".//xsd:simpleType/xsd:restriction",$xsd_node);
                $result2 = $xpath->query(".//xsd:simpleContent/xsd:restriction",$xsd_node);
                $result3 = $xpath->query(".//xsd:simpleContent/xsd:extension",$xsd_node);
                if($result1->length > 0){
                    $restriction = $result1->item(0);
                    //********************************************************************//
                    //                           Check XSD Base Type                      //
                    //********************************************************************//
                    
                    self::validateXSDBase($value, $restriction, $xml_tag);
                }elseif($result2->length > 0){
                    $restriction = $result2->item(0);
                    //********************************************************************//
                    //                           Check XSD Base Type                      //
                    //********************************************************************//
                    self::validateXSDBase($value, $restriction, $xml_tag);
                }elseif($result3->length > 0){
                    $restriction = $result3->item(0);
                    //********************************************************************//
                    //                           Check XSD Base Type                      //
                    //********************************************************************//
                    self::validateXSDBase($value, $restriction, $xml_tag);
                }else{
                    //********************************************************************//
                    //                           Check XSD Data Type                      //
                    //********************************************************************//
                    self::validateXSDType($xml_node, $xsd_node);
                }
                
            }
    }
    
    private static function validateXSDBase($value, DOMElement $restriction, $xml_tag){
        $base= $restriction->getAttribute("base");
        self::validateXMLValue($xml_tag, $value, $base);
        //********************************************************************//
        //                             Check Pattern                          //
        //********************************************************************//
        $patterns = $restriction->getElementsByTagName("pattern");
        foreach($patterns as $pattern_result){
            $validation = $pattern_result->getAttribute("value");
            if(!preg_match("/" . $validation . "/", $value)){
                self::displayError(sprintf(Amazon_Tools::l("Value_for_s_is_non_valid"), $xml_tag), 9);
            }

        }
        //********************************************************************//
        //                             Check Enumeration                      //
        //********************************************************************//
        $enumerations = $restriction->getElementsByTagName("enumeration");
        $enumeration_ok = false;
        foreach($enumerations as $enumeration_result){
            $validation = $enumeration_result->getAttribute("value");
            //$validation = isset(AmazonForm::$variation_theme_exception[$validation]) ? AmazonForm::$variation_theme_exception[$validation] : $validation;
          
            if($value == $validation){
                $enumeration_ok = true;
                break;
            }
        }
        if($enumerations->length > 0 && !$enumeration_ok){
            self::displayError(sprintf(Amazon_Tools::l("s_is_not_a_valid_value_se_for_s"), $value, $xml_tag), 10);
        }
        //********************************************************************//
        //                             Check Min Length                       //
        //********************************************************************//
        $minLength = $restriction->getElementsByTagName("minLength");
        if($minLength->length > 0){
            $length = $minLength->item(0)->getAttribute("value");
            if(strlen($value) < $length){
                self::displayError(sprintf(Amazon_Tools::l("s_length_must_be_greater_or_equal_to_s_for_the_element"), $value, $length, $xml_tag), 11);
            }
        }
        //********************************************************************//
        //                             Check Max Length                       //
        //********************************************************************//
        $maxLength = $restriction->getElementsByTagName("maxLength");
        if($maxLength->length > 0){
            $length = $maxLength->item(0)->getAttribute("value");
            if(strlen($value) > $length){
                self::displayError(sprintf(Amazon_Tools::l("s_length_must_be_less_or_equal_to_s_for_the_element"), $value, $length, $xml_tag), 12);
            }
        }
        //********************************************************************//
        //                           Check Total DIgits                       //
        //********************************************************************//
        $totalDigits = $restriction->getElementsByTagName("totalDigits");
        if($totalDigits->length > 0){ 
            $length = $totalDigits->item(0)->getAttribute("value");
            $cur_digits = preg_match_all( "/[0-9]/", $value, $dummy_array);
            
            if($cur_digits > $length){
                self::displayError(sprintf(Amazon_Tools::l("s_is_invalid_digits_must_be_less_or_equal_to_s_for_the_element"), $value, $length, $xml_tag), 13);
            }
        }
        //********************************************************************//
        //                           Check Fraction DIgits                       //
        //********************************************************************//
        $fractionDigits = $restriction->getElementsByTagName("fractionDigits");
        if($fractionDigits->length > 0){
            $length = $fractionDigits->item(0)->getAttribute("value");
            $cur_fraction_digits = preg_match_all( "/\.[0-9]/", $value, $dummy_array);
            
            if($cur_fraction_digits > $length){
                self::displayError(sprintf(Amazon_Tools::l("s_is_invalid_fraction_digits_must_be_less_or_equal_to_s_for_the_element"), $value, $length, $xml_tag), 14);
            }
        }
    }
                
    /**
     * Validate basic XSD type
     * @param DOMElement $xml_node
     * @param DOMElement $xsd_node
     */
    private static function validateXSDType(DOMElement $xml_node, DOMElement $xsd_node){
        $type = $xsd_node->getAttribute("type");
        if(!stripos($type, "xsd:")){
            AmazonXSD::getElementStructure($xsd_node, $xsd_node->ownerDocument);
        }
        $tag = $xml_node->tagName;
        $value = $xml_node->nodeValue;
        if($type){
            self::validateXMLValue($tag, $value, $type);
        }
    }
    
    private static function validateXMLValue($tag, $value, $type){
        
        $type = str_replace("xsd:", "", $type);
       
        if($type){
            switch($type){
                case "boolean":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_value_Currently_an_object_is_set"), $tag, $type), 15);
                    }elseif($value != "false" && $value != "true"){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_boolean_value_Currently_value_s"), $tag, $value), 16);
                    }
                    break;
                case "negativeInteger":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_value_negativeInteger_Currently_an_object_is_set"), $tag, $type), 17);
                    }elseif($value != "" && intval($value) >= 0){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_valid_negativeInteger_Currently_value_s"), $tag, $value), 18);
                    }
                    break;
                case "nonNegativeInteger":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_nonNegativeInteger_Currently_an_object_is_set"), $tag, $type), 19);
                    }elseif($value != "" && intval($value) < 0){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_nonNegativeInteger_Currently_value_s"), $tag, $value), 20);
                    }
                    break;
                case "nonPositiveInteger":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_nonPositiveInteger_Currently_an_object_is_set"), $tag, $type), 21);
                    }elseif($value != "" && intval($value) > 0){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_nonPositiveInteger_Currently_value_s"), $tag, $value), 22);
                    }
                    break;
                case "positiveInteger":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_positiveInteger_Currently_an_object_is_set"), $tag, $type), 23);
                    }elseif($value != "" && intval($value) <= 0){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_positiveInteger_Currently_value_s"), $tag, $value), 24);
                    }
                    break;
                case "decimal":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_decimal_Currently_an_object_is_set"), $tag, $type), 25);
                    }elseif($value != "" && !is_numeric($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_decimal_Currently_value_s"), $tag, $value), 26);
                    }
                    break;
                case "integer":
                    if(is_object($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_s_integer_Currently_an_object_is_set"), $tag, $type), 27);
                    }elseif($value != "" && !is_int($value)){
                        self::displayError(sprintf(Amazon_Tools::l("element_s_must_have_a_integer_Currently_value_s"), $tag, $value), 28);
                    }
                    break;

            }
        } 
    }
    
    /**
     * Display a message error and stop program execution
     * @param type $msg
     */
    public static function displayError($msg, $k){
        
        global $error_logs; 
        
        $product = self::$product;
        if(strlen((string)$msg) > 1) {
            if(isset(self::$category) && !empty(self::$category)){
                $error_logs[self::$type][self::$category][$k]['message'] = (string)$msg;
                $error_logs[self::$type][self::$category][$k]['sku'][self::$SKU] =
                        isset($product[self::$SKU]['ProductDescription']['Title']) ? $product[self::$SKU]['ProductDescription']['Title'] : '';
            }else{
                if(!isset(self::$type) || empty(self::$type))
                    self::$type = 'Product';
                $error_logs[self::$type]['main'][$k]['message'] = (string)$msg;
            }
        }
    }

}