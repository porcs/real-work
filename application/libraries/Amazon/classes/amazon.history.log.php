<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class AmazonHistoryLog {
  
    public static function set($data, $detail=null){
        
        $mysql_db = new ci_db_connect();          
	
        $batch_id = 'check exist other process of ' . $data['countries'] . ' running';
        $rep = new report_process($data['user_name'], $batch_id);
        $rep->end_current_process();
        if($rep->has_other_running("amazon{$data['ext']}_{$data['action']}")){
           return false;
        }
        $other = array(
            'next_time' => strtotime("+{$data['next_time']} minutes"),
            'detail' => $detail
        );

	// If user_id doesn't exist.
	if(!isset($data['user_id']) || empty($data['user_id']))
	{
	    if(isset($data['user_name']))
	    {
		
		$query = $mysql_db->select_query("SELECT id FROM users WHERE user_name = '".$data['user_name']."' " );
		while($row = $mysql_db->fetch($query)){ 
		    $data['user_id'] = $row['id'];
		}
		
	    } else {
		
		return false;
		
	    }
	}
	    
        $data_history = array(
            'user_id' => $data['user_id'],
            'user_name' => $data['owner'],
            'history_action' => "amazon{$data['ext']}_{$data['action']}",
            'history_date_time' => date('Y-m-d H:i:s'),
            'history_table_name' => $data['shop_name'],
            'history_data' => json_encode($other),
        );

        $mysql_db->add_db('histories', $data_history);
        $batch_id = 'check exist amazon running';
        
    }
}