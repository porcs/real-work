<?php

require_once(dirname(__FILE__) . '/amazon.database.php');
require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/../../FeedBiz.php');

class AmazonErrorResolution {   
    
    const _EDIT_ATTRIBUTE = 'edit_attribute';
    const _RECREATE = 'recreate';
    const _SOLVED = 'solved';
    const _CREATION = 'creation';
    const _RESOLVED = 'Resolved';
    const _OVERRIDE = 'override';
    
    public $db;
    
    public static $err_attribute_exeption = array(
        'Itemid'=>'ASIN','item_id'=>'ASIN', 
        'item_name'=>'Title', 'ItemName'=>'Title', 
        'StandardProductId' => 'StandardProductID', 'Standardproductid' => 'StandardProductID', 
        'manufacturer' => 'Manufacturer',
        'Manufacturer' => 'Manufacturer',
        'part_number' => 'PartNumber',
//        'PartNumber' => 'ManufacturerPartNumber',
//        'part_number' => 'ManufacturerPartNumber',
//        'MfrPartNumber' => 'ManufacturerPartNumber',
        'ItemPackageQuantity' => 'ItemPackageQuantity',
        'DistributionDesignation' => 'DistributionDesignation',
        'ProductType' => 'ProductType', 'product_type' => 'ProductType', 
        'Gcid' => 'GCID', 'gcid' => 'GCID', 
        'material' => 'Material',
        'model_number' => 'ModelNumber',
        'ProductCategory' => 'ProductCategory',
        'product_category' => 'ProductCategory'
    );  

    public static $feed_type = array(
        'Create' => array('Image', 'Product', 'Relationship'),
        'Sync' => array('Price', 'Inventory', 'Shipping_override')
    );

    public static $query_feed_type = array(
        'Create' => "AND feed_type IN ('Image', 'Product', 'Relationship')",
        'Sync' => "AND feed_type IN ('Price', 'Inventory', 'Shipping_override')"
    );
    
    private $delete_report_product;
    
    public function __construct($user) {
        $this->db = new stdClass();       
        $this->amazon_model = new AmazonDatabase($user);
        $this->db->product = $this->amazon_model->db->product;
        $this->delete_report_product = array(AmazonErrorResolution::_RECREATE => 1);
    }
    
    public function message_pattern($message, $msg_pattern = null, $msg_name = null){

        $search_pattern = "%s";
        if(!isset($msg_pattern) || empty($msg_pattern)){
            $msg_pattern = "conflicting: %s (%s ''%s'' / %s''%s'')";
        }
        if(!isset($msg_name) || empty($msg_name)){
            $msg_name = array("attribute_field", "Merchant", "Merchant_value", "Amazon", "Amazon_value");                                       
        }        
        $explode_msg = explode($search_pattern, $msg_pattern);
        
        // find number of message
        if(isset($explode_msg[0]) && !empty($explode_msg[0])){   
            
            $msgs = explode($explode_msg[0], $message); 
            $msg2 = '';
            foreach($msgs as $mk => $mv){
                if($mk != 0 ){
                    $msg2 .= "[sp]" . $mv;
                }
            }
            $msgs = explode('[sp]', $msg2);                    
            foreach ($msgs as $k => $m){                
                if(!empty($m)){
                    $explode_m = explode(".", $m);
                    foreach ($explode_m as $em_key => $em_value){
                        if(!empty($em_value) && strpos($em_value, "Merchant") ){
                            $msgs[$k] = $em_value;
                        } 
                    }
                }else {
                    unset( $msgs[$k] );
                }
            }
        }
        
        $word = array();
        foreach ($explode_msg as $ekey => $eval){
            if(!empty($eval)) {
                $history_attr_field = array();
                foreach ($msgs as $msg){
                    $t = str_replace($explode_msg, "[sp]", $msg);  
                    $t = explode("[sp]", $t);
                    foreach ($t as $tk => $tt){
                        $t[$tk] = str_replace(array(' "', ' / ', '), '/*, ')'*/), '', $tt);
                        if(isset($t[(count($t) - 1)]) && strlen($t[(count($t) - 1)]) <= 0){
                            unset($t[(count($t) - 1)]);
                        }
                    }
                    $c = round((count($t))/round(count($t)/count($msg_name))); // number of split
                    $i = 0;  
                    foreach ($t as $msg_k => $msg_t){
                        $msg_t = str_replace(", ", "", $msg_t);
                        if($msg_k == ($c*$i)){ // set array key to attribute_key                        
                            $key = trim($t[$msg_k]);
                            $i++; // next key
                        }
                       
                        if($msg_k >= count($msg_name)){ // add more key to count
                            $count = $count + 1;
                        } else {
                            $count = $ekey;
                        }
                        
                        if(isset($msg_name[$ekey]) && isset($t[$count])){
                            if($msg_name[$ekey] == 'attribute_field'){
                                $t[$count] = str_replace(array(" ","_",","), "", ucwords(str_replace(array("_"), " ", $t[$count])));
                                if(isset(AmazonErrorResolution::$err_attribute_exeption[$t[$count]]))
                                    $t[$count] = AmazonErrorResolution::$err_attribute_exeption[$t[$count]];
                            }
                            
                            $key = (str_replace(", ", "", $key));
                            $word[$key][$msg_name[$ekey]] = str_replace("''", "", trim($t[$count]));

                            // Find matching
                            /*$merchant_value = isset($word[$key]['Merchant_value']) ? $word[$key]['Merchant_value'] : null;
                            $amazon_value = isset($word[$key]['Amazon_value']) ? $word[$key]['Amazon_value'] : null;

                            $matching_number = strcmp("$merchant_value","$amazon_value");
                            if(isset($amazon_value)){

                                if(isset($history_attr_field[$key]["matching_number"])){
                                    if($history_attr_field[$key]['matching_number'] == 0){ // 100% match
                                        $merchant_value = $history_attr_field[$key]["Merchant_value"];
                                        $amazon_value = $history_attr_field[$key]["Amazon_value"];
                                        $matching_number = $history_attr_field[$key]['matching_number'];
                                        $word[$key]['Amazon_value'] = $amazon_value;
                                        continue;
                                    }
                                    if(abs($history_attr_field[$key]['matching_number']) > abs($matching_number)){ // Almost Match
                                        $word[$key]['Amazon_value'] = $history_attr_field[$key]["Amazon_value"];
                                    }
                                }
                                $history_attr_field[$key]["Merchant_value"] = $merchant_value;
                                $history_attr_field[$key]["Amazon_value"] = $amazon_value;
                                $history_attr_field[$key]['matching_number'] = $matching_number;
                            }*/
                        }
                    }
                }
            }
        }
        return $word;
    }
    
    public function getModels($id_country, $id_shop, $skus=array()){
	
	$list_sku = '';
	if(isset($skus) && !empty($skus)){
	    $list_sku = " AND (pa.reference = '".$skus."' OR p.reference = '".$skus."') ";
	}
	
        $sql = "SELECT 
		    CASE 
			WHEN pa.reference IS NOT NULL THEN pa.reference 
			WHEN p.reference IS NOT NULL THEN p.reference
		    END AS sku, 
                    p.id_product AS id_product,
                    pa.id_product_attribute AS id_product_attribute,
                    p.id_category_default AS id_category_default,	
                    acs.id_profile AS id_profile,
                    ap.id_model AS id_model,
                    am.universe AS universe,
                    am.product_type AS product_type
                FROM ".AmazonDatabase::$p_pref."product p 
                LEFT JOIN ".AmazonDatabase::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_category_selected acs ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_profile ap ON acs.id_profile = ap.id_profile AND acs.id_shop = ap.id_shop AND acs.id_country = ap.id_country
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_model am ON ap.id_model = am.id_model AND ap.id_shop = am.id_shop AND ap.id_country = am.id_country
                WHERE acs.id_country = $id_country AND ap.id_country = $id_country AND acs.id_country = $id_country AND p.id_shop = $id_shop $list_sku
		GROUP BY pa.reference ASC, p.reference ASC;";
	$result = $this->db->product->db_array_query($sql);
	$data = array();
	
        foreach ($result as $value ){
            $data = $value;
        }
        
        return $data;
    }
    public function getModelsList($id_country, $id_shop, $skus=array()){

	$list_sku = '';
        $keys = array();
	if(isset($skus) && !empty($skus) && is_array($skus)){
	    $list_sku = " AND ( pa.reference in ('".implode("','",$skus)."') OR p.reference = ('".implode("','",$skus)."') ) ";
            foreach($skus as $key => $sku){
                $keys[$sku] = $key;
            }
	}

        $sql = "SELECT
		    CASE
			WHEN pa.reference IS NOT NULL THEN pa.reference
			WHEN p.reference IS NOT NULL THEN p.reference
		    END AS sku,
                    p.id_product AS id_product,
                    pa.id_product_attribute AS id_product_attribute,
                    p.id_category_default AS id_category_default,
                    acs.id_profile AS id_profile,
                    ap.id_model AS id_model,
                    am.universe AS universe,
                    am.product_type AS product_type
                FROM ".AmazonDatabase::$p_pref."product p
                LEFT JOIN ".AmazonDatabase::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_category_selected acs ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_profile ap ON acs.id_profile = ap.id_profile AND acs.id_shop = ap.id_shop AND acs.id_country = ap.id_country
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_model am ON ap.id_model = am.id_model AND ap.id_shop = am.id_shop AND ap.id_country = am.id_country
                WHERE acs.id_country = $id_country AND ap.id_country = $id_country AND acs.id_country = $id_country AND p.id_shop = $id_shop $list_sku
		GROUP BY pa.reference ASC, p.reference ASC;";
	$result = $this->db->product->db_array_query($sql);
	$data = array();

        foreach ($result as $value ){
            $data[$keys[$value['sku']]] = $value;
        }

        return $data;
    }
    
    public function getModelBySKU($sku, $id_country, $id_shop){
        $sql = "SELECT 
                    p.id_product AS id_product,
                    pa.id_product_attribute AS id_product_attribute,
                    p.id_category_default AS id_category_default,	
                    acs.id_profile AS id_profile,
                    ap.id_model AS id_model,
                    am.universe AS universe,
                    am.product_type AS product_type
                FROM ".AmazonDatabase::$p_pref."product p 
                LEFT JOIN ".AmazonDatabase::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_category_selected acs ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_profile ap ON acs.id_profile = ap.id_profile AND acs.id_shop = ap.id_shop AND acs.id_country = ap.id_country
                LEFT JOIN ".AmazonDatabase::$m_pref."amazon_model am ON ap.id_model = am.id_model AND ap.id_shop = am.id_shop AND ap.id_country = am.id_country
                WHERE acs.id_country = $id_country AND ap.id_country = $id_country AND acs.id_country = $id_country AND p.id_shop = $id_shop AND 
                ( 
                    pa.reference = CASE WHEN pa.reference = '$sku' THEN '$sku' END 
                    OR p.reference = CASE WHEN p.reference = '$sku' THEN '$sku' END 
                    OR pa.sku = CASE WHEN pa.sku = '$sku' THEN '$sku' END 
                    OR p.sku = CASE WHEN p.sku = '$sku' THEN '$sku' END 
                    OR pa.ean13 = CASE WHEN pa.ean13 = '$sku' THEN '$sku' END 
                    OR p.ean13 = CASE WHEN p.ean13 = '$sku' THEN '$sku' END 
                    OR pa.upc = CASE WHEN pa.upc = '$sku' THEN '$sku' END
                    OR p.upc = CASE WHEN p.upc = '$sku' THEN '$sku' END 
                ) ;";
        
        return $this->db->product->db_array_query($sql);
    }
    
    public function getAmazonOverrides($id_shop, $id_country, $id_model = null) {
         
        $table = AmazonDatabase::$m_pref.'amazon_attribute_override';        
        $sql = "SELECT override_value, attribute_field, id_product, id_product_attribute, sku FROM $table 
               WHERE id_shop = " . $id_shop . " AND id_country = " . $id_country . " ";
              
        if(isset($id_model) && !empty($id_model)){
            $sql .= " AND id_model = " . $id_model . " ";
        }
        $sql .= ";";
        $result = $this->db->product->db_array_query($sql);
        
        $data = array();
        foreach ($result as $value ){                
            $data[$value['sku']][$value['attribute_field']] = $value['override_value'];
        }
        
        return $data;
    }
    
    public function getAmazonOverride($id_shop, $id_country, $sku = null, $attribute_field = null, $id_product = null, $id_product_attribute= null, $id_model = null) {
         
        $table = AmazonDatabase::$m_pref.'amazon_attribute_override';        
        $sql = "SELECT override_value,attribute_field FROM $table 
               WHERE id_shop = " . $id_shop . " AND id_country = " . $id_country . " ";
        
        if(isset($sku) && !empty($sku)){
            $sql .= " AND sku = '" . $sku . "' ";
        }
        if(isset($attribute_field) && !empty($attribute_field)){
            $sql .= " AND attribute_field = '" . $attribute_field . "' ";
        }
        if(isset($id_product) && !empty($id_product)){
            $sql .= " AND id_product = " . $id_product . " ";
        }
        if(isset($id_product_attribute) && !empty($id_product_attribute)){
            $sql .= " AND id_product_attribute = " . $id_product_attribute . " ";
        }
        if(isset($id_model) && !empty($id_model)){
            $sql .= " AND id_model = " . $id_model . " ";
        }
        $sql .= ";";
        $result = $this->db->product->db_array_query($sql);
       
        $data = array();
        foreach ($result as $value ){
            $data[$value['attribute_field']] = $value['override_value'];
        }
        
        return $data;
    }
        
    public function get_amazon_error_result_description($data_config, $error_code=null, $sku = null, $allow_overide = false, $all = false) {

        $content = array();
	$result_message_code = '';
	if(isset($error_code) && !empty($error_code))
	    $result_message_code = "AND result_message_code = '$error_code'";
	
        $sql = "SELECT sku, result_description, result_message_code
               FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log 
               WHERE id_shop = " . $data_config['id_shop'] . " AND id_country = " . $data_config['id_country'] . " $result_message_code ";
        if(!isset($error_code) || !(in_array($error_code, array('8541', '8542'))))
            $sql .= "AND result_code != '".AmazonErrorResolution::_RESOLVED."' ";
               
        if(isset($sku) && !empty($sku)){
		
	    if(is_array($sku)) {
             $sql .= " AND sku IN ('".  implode("','", $sku)."') ";
	    } else {
		$sql .= " AND sku = '$sku'";
	    }
        } 
        
        $sql .= " GROUP BY sku ;";

        $result_group = $this->db->product->db_array_query($sql);
        foreach ($result_group as $value) { 
	    if(!$all)
                $content['message'] = $value['result_description']; 
	    else
		$content[$value['result_message_code']][$value['sku']]['message'] = $value['result_description']; 
        }
        return $content;
    }
    
    public function get_amazon_error_log_details($data_config, $error_code, $action_type, $allow_overide=null, $start=null, $limit=null, $order_by=null, $search=null, $num_rows=false, $sku=null, $is_group=true) {

        $content = $list_batch = array();
        $id_sunmission_feed = '';
        
	if(!isset($this->list_id_sunmission_feed) || empty($this->list_id_sunmission_feed)) {         
	    $sql_batch = "SELECT * FROM ".AmazonDatabase::$m_pref."amazon_submission_list_log
            WHERE id_country = ".(int)$data_config['id_country']." AND id_shop = ".(int)$data_config['id_shop']."
            ".(isset(self::$query_feed_type[$action_type]) ? self::$query_feed_type[$action_type] : '')." " ;
	    $result_batch = $this->db->product->db_array_query($sql_batch);            
            foreach ($result_batch as $value) {
                $message = unserialize(base64_decode($value['message']));              
                if(isset($message['FeedSunmissionId']) && !empty($message['FeedSunmissionId'])){
                    $list_id_sunmission_feed[] = $message['FeedSunmissionId'];
                }
            }
	    $this->list_id_sunmission_feed = $list_id_sunmission_feed;
	} else {
	    $list_id_sunmission_feed = $this->list_id_sunmission_feed;
	}
	
        if(!empty($list_id_sunmission_feed)){
            $id_sunmission_feed  = " AND asrl.id_submission_feed IN ('".  implode("', '", $list_id_sunmission_feed)."')";
        }
        
        $sql = "SELECT ";
        
	if($num_rows){
	    $sql .= "asrl.sku as sku ";
	} else {
	    $sql .= "asrl.sku as sku, asrl.result_description as result_description, asrl.result_code as result_code  ";
	}

        $result_code = '';
        if(!in_array($error_code, array('8541', '8542'))){
            $result_code = " AND asrl.result_code != '".AmazonErrorResolution::_RESOLVED."' ";
        } else {
            $order_by = " asrl.result_code ASC" . (isset($order_by) ? ", " . $order_by : " ") ;
        }
        
        $sql .= "FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log asrl               
               WHERE asrl.result_message_code = $error_code  
               AND asrl.id_shop = ".$data_config['id_shop']."  
               AND asrl.id_country = ".$data_config['id_country']."  
               $result_code
               $id_sunmission_feed ";
       
        if(isset($sku) && !empty($sku)){
             $sql .= " AND asrl.sku = '$sku'";
        }
      
        if(isset($search) && !empty($search)){
             $sql .= " AND (asrl.sku LIKE '%$search%')";
        }
        if($is_group){
            $sql .= " GROUP BY asrl.sku";
        }
        if(isset($order_by) && !empty($order_by) && !$num_rows){
             $sql .= " ORDER BY $order_by ";
        }
        if(isset($start) && isset($limit)){
             $sql .= " LIMIT $limit OFFSET $start ";
        }
        $sql .= " ;";
       
        if($num_rows){
	    
            $result_group = $this->db->product->db_array_query($sql);           
            $row = count($result_group);            
            return $row;
            
        } else {
            $result_group = $this->db->product->db_array_query($sql);
            foreach ($result_group as $key => $value) {
                
                $product_detail = $this->amazon_model->getProductBySKU($data_config['id_shop'], $value['sku']);

                if(!($product_detail))
                    continue;
                
                $id_product_attribute = $variant = $standard_id = '';
                
                if(isset($product_detail) && !empty($product_detail)) {
                    
                    $product = new Product($data_config['user_name'], $product_detail['id_product'], $data_config['id_lang']);

                    if(isset($product->ean13) && !empty($product->ean13)){
                        $standard_id = $product->ean13;
                    }else if(isset($product->sku) && !empty($product->sku)){
                        $standard_id = $product->sku;
                    }else if(isset($product->upc) && !empty($product->upc)){
                        $standard_id = $product->upc;
                    }

                    $price = $product->price;
                    $quantity = $product->quantity;
                    $image = isset($product->image[1]['url']) ? $product->image[1]['url']: null;
                   
                    if(isset($product->combination) && !empty($product->combination)){

                        foreach ($product->combination as $combination)
                        {
                            if(isset($combination['reference']) && !empty($combination['reference'])){
                                $type = 'reference';
                            } else if(isset($combination['ean13']) && !empty($combination['ean13'])){
                                $type = 'ean13';
                            }else if(isset($combination['sku']) && !empty($combination['sku'])){
                                $type = 'sku';
                            }else if(isset($combination['upc']) && !empty($combination['upc'])){
                                $type = 'upc';
                            }else{
                                $type = 'id_product_attribute';
                            }

                            if($combination[$type] == $value['sku']){   

                                if(isset($combination['ean13']) && !empty($combination['ean13'])){
                                    $standard_id = 'EAN:' . $combination['ean13'];
                                }else if(isset($combination['sku']) && !empty($combination['sku'])){
                                    $standard_id = 'SKU:' . $combination['sku'];
                                }else if(isset($combination['upc']) && !empty($combination['upc'])){
                                    $standard_id = 'UPC:' . $combination['upc'];
                                }

                                $id_product_attribute = $combination['id_product_attribute'];
                                $price = $combination['price'];
                                $quantity = $combination['quantity'];
                                $image = isset($combination['images'][1]['url']) ? $combination['images'][1]['url']: null;

                                $attr = ''; 
                                if(isset($combination['attributes']) && !empty($combination['attributes'])){

                                    foreach ($combination['attributes'] as $attribute){

                                        $attr .= $attribute['name'][$data_config['iso_code']];                                    
                                        if(isset($attribute['value']) && !empty($attribute['value'])){ 

                                            foreach ($attribute['value'] as $attribute_value){

                                                $attr .= " " .$attribute_value['name'][$data_config['iso_code']];
                                            }
                                        }
                                    }
                                }
                            }
                        }                    
                    }   
                    $content[$key]['sku'] = $value['sku'];  
                    $content[$key]['key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $content[$key]['sku']);
                    $content[$key]['id_product'] = $product->id;
                    $content[$key]['price'] = $price;
                    $content[$key]['quantity'] = $quantity;
                    $content[$key]['image'] = $image;
                    $content[$key]['result_code'] = $value['result_code'];
                    $content[$key]['standard_id'] = $standard_id;

                    if(isset($id_product_attribute) && !empty($id_product_attribute)) {
                        $content[$key]['id_product_attribute'] = $id_product_attribute;
                        if(isset($attr) && !empty($attr)) {
                            $content[$key]['variant'] = $attr;
                        }
                    }
		    
		    $content[$key]['asin'] = $this->get_asin_from_message($value['result_description']);
		    
		    if(strlen($content[$key]['asin']) && isset($data_config['ext']))
		    {
			$override = $this->getAmazonOverride((int)$data_config['id_shop'], (int)$data_config['id_country'], $value['sku']/*, 'ASIN'*/);

			if(isset($override['ASIN']) && !empty($override['ASIN'])) { // have ASIN override
                            $content[$key]['asin'] = $override['ASIN'];
			    $content[$key]['override_asin'] = true;
                            unset($override['ASIN']); // unset ASIn to check next step is they have other override data ?
                        }

                        if(count($override) <= 0) { // have override data already
			    $content[$key]['result_code'] = 'Error';
                        } else {
                            if($value['result_code'] != "Error") {
                                $content[$key]['override'] = $override;
                            }
                        }

			$content[$key]['link'] = 'http://www.amazon'.$data_config['ext'].'/dp/'.$content[$key]['asin'];
		    }
		    
		    if($allow_overide == 1){
			$content[$key]['message'] = $value['result_description']; 
		    }
                }
            }
            return $content;
        }
    }
    
    public function get_amazon_error_log_all_details($id_shop, $id_country, $action_type, $list_id_sunmission_feed = array()) {
        
        $id_sunmission_feed = '';
	$row = array();
        
	if(!isset($list_id_sunmission_feed) || empty($list_id_sunmission_feed)) {
	    $sql_batch = "SELECT * FROM ".AmazonDatabase::$m_pref."amazon_submission_list_log
            WHERE id_country = ".(int)$id_country." AND id_shop = ".(int)$id_shop." ".(isset(self::$query_feed_type[$action_type]) ? self::$query_feed_type[$action_type] : '')." " ;

	    $result_batch = $this->db->product->db_array_query($sql_batch);
            foreach ($result_batch as $value) {
                $message = unserialize(base64_decode($value['message']));
                if(isset($message['FeedSunmissionId']) && !empty($message['FeedSunmissionId'])){
                    $list_id_sunmission_feed[] = $message['FeedSunmissionId'];
                }
            }
        }

        if(!empty($list_id_sunmission_feed)){
            $id_sunmission_feed  = " AND asrl.id_submission_feed IN ('".  implode("', '", $list_id_sunmission_feed)."') ";
        }

        $sql = "SELECT count(t.sku) as sku, t.result_message_code FROM (";
        $sql .= "SELECT ";
	$sql .= "asrl.sku as sku, asrl.result_message_code as result_message_code ";
        $sql .= "FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log asrl
               LEFT JOIN ". AmazonDatabase::$m_pref."amazon_attribute_override ao ON asrl.sku = ao.sku AND asrl.id_shop = ao.id_shop AND asrl.id_country = ao.id_country 
               WHERE asrl.id_shop = ".$id_shop."  
               AND asrl.id_country = ".$id_country."  
               AND asrl.result_code <> '".AmazonErrorResolution::_RESOLVED."' 
               AND ( CASE WHEN ao.sku IS NOT NULL THEN ao.attribute_field = 'ASIN' ELSE  ao.sku IS NULL END ) 
               $id_sunmission_feed ";
        $sql .= " GROUP BY asrl.sku, asrl.result_message_code ";
        $sql .= " ) t GROUP BY t.result_message_code ; ";
       
	$result_group = $this->db->product->db_array_query($sql);
	foreach ($result_group as $value) { 
	    $row[$value['result_message_code']] = $value['sku'];
	}
      
	return $row;
            
    }
    
    public function get_amazon_error_log($id_country, $id_shop, $action_type = null, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false, $where=null) {
               
        $content = $list_batch = array();
        $search_by_sku = false;
        $id_sunmission_feed = '';

        $sql_batch = "SELECT * FROM ".AmazonDatabase::$m_pref."amazon_submission_list_log "
		. "WHERE id_country = $id_country AND id_shop = $id_shop ".(isset(self::$query_feed_type[$action_type]) ? self::$query_feed_type[$action_type] : '')." " ;
        $result_batch = $this->db->product->db_array_query($sql_batch);

        foreach ($result_batch as $value) {
            $message = unserialize(base64_decode($value['message']));          
            if(isset($message['FeedSunmissionId']) && !empty($message['FeedSunmissionId']))
                $list_id_sunmission_feed[] = $message['FeedSunmissionId'];
        }
        
        if(!empty($list_id_sunmission_feed)){
            $id_sunmission_feed  = " AND asrl.id_submission_feed IN ('".  implode("', '", $list_id_sunmission_feed)."') ";
        }
	
        if(empty($id_sunmission_feed)) {
	    if($num_rows){
		return 0;
	    } else {
		return $content;
	    }
	}
	
	if(!$num_rows){
	    $rows = $this->get_amazon_error_log_all_details($id_shop, $id_country, $action_type, $list_id_sunmission_feed);
	}
	
        $sql = "SELECT ";
	if($num_rows){
	    $sql .= " asrl.id_log ";
	} else {
	    $sql .= " asrl.id_log as id_log, asrl.id_message as id_message, asrl.sku as sku, asrl.result_message_code as result_message_code, 
		    asrl.result_description as result_description ";
	}
		
        $sql .= " FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log asrl 
                WHERE asrl.id_country = $id_country  
                AND asrl.id_shop = $id_shop $id_sunmission_feed ";
        
        if(isset($search['all']) && !empty($search['all'])){
             $sql .= " AND (asrl.result_message_code LIKE '%".$search['all']."%' OR asrl.result_description LIKE '%".$search['all']."%')";  
        }   
        
        // Search by sku   
        if(isset($search['sku']) && !empty($search['sku'])){
            $sql .= " AND (asrl.sku LIKE '%".$search['sku']."%') ";
            $search_by_sku = true;
        } else {
            $sql .= " AND asrl.result_code != '".AmazonErrorResolution::_RESOLVED."' ";
        }

        if(isset($where) && strlen($where)){
            $sql .= " AND $where ";
        }

	$sql .= " AND asrl.sku IS NOT NULL ";
        $sql .= " GROUP BY asrl.result_message_code ";
        
        if(isset($order_by) && !empty($order_by) && !$num_rows){
             $sql .= " ORDER BY $order_by ";
        } 
        
        if(isset($start) && isset($limit)){
             $sql .= " LIMIT $limit OFFSET $start ";
        }
        $sql .= " ;";       
	
        if($num_rows){
            $result = $this->db->product->db_sqlit_query($sql);
            $row = $this->db->product->db_num_rows($result);
            return $row;
        } else {
            $result = $this->db->product->db_sqlit_query($sql);
            $result_group = $this->db->product->db_array_query($sql);
            foreach ($result_group as $value) {
		
                $content[$value['id_log']]['code'] = $value['result_message_code'];
                $content[$value['id_log']]['description'] = $value['result_description'];  
                $content[$value['id_log']]['rows'] = isset($rows[$value['result_message_code']]) ? $rows[$value['result_message_code']] : 0;

                if($search_by_sku){
                   $content[$value['id_log']]['sku'] = $value['sku'];
                }
                
//                if(!$content[$value['id_log']]['rows']){
//                    unset($content[$value['id_log']]);
//                }
            }  
            return $content;
        }
    }

    public function remove_amazon_attribute_override($sku, $id_shop, $id_country, $error_code, $action_type=null) {
        if($this->update_result_code($id_shop,$id_country, $sku, $error_code, $action_type, null, false, null, 'Error')){
            $table = AmazonDatabase::$m_pref.'amazon_attribute_override';
            $sql = "DELETE FROM $table WHERE sku = '$sku' AND id_shop =  $id_shop AND id_country = $id_country ; ";
            return $this->db->product->db_exec($sql);
        }
        return (false);
    }

    public function save_amazon_attribute_override($data, $error_code, $action_type=null, $actions=null, $is_update_falg=false) {
        
        $table = AmazonDatabase::$m_pref.'amazon_attribute_override';        
        
        // check if this attribute exit
        $sql = "SELECT sku FROM $table 
               WHERE sku = '" . $data['sku'] . "' 
               AND id_shop = " . $data['id_shop'] . " 
               AND id_country = " . $data['id_country'] . " 
               AND id_model = " . $data['id_model'] . " 
               AND attribute_field = '" . $data['attribute_field'] . "' ; ";
        
        $result = $this->db->product->db_sqlit_query($sql);
        $row = $this->db->product->db_num_rows($result);
        
        $filter_data = $data;
        
        if ($row > 0) {
            $where = array(
                "sku" => array('operation' => "=", 'value' => $data['sku']),
                "id_shop" => array('operation' => "=", 'value' => $data['id_shop']),
                "id_country" => array('operation' => "=", 'value' => $data['id_country']),
                "id_model" => array('operation' => "=", 'value' => $data['id_model']),
                "attribute_field" => array('operation' => "=", 'value' => $data['attribute_field']),
            );
            $sql_excute =  $this->amazon_model->update_string($table, $filter_data, $where);
            
        } else {
            $sql_excute = $this->amazon_model->insert_string($table, $filter_data);
        }
        if($this->update_result_code($data['id_shop'], $data['id_country'], $data['sku'], $error_code, $action_type, $actions, $is_update_falg)) {
            return $this->db->product->db_exec($sql_excute);
        } else {
	    
	    if($is_update_falg)
		return true;
	    
            return false;
        }
    }
    
    public function update_result_code($id_shop, $id_country, $sku, $error_code, $action_type, $actions = null, $is_update_falg=false, $ext=null, $result_code=null){
        
	$sql_excute = '';
        $list_id_sunmission_feed = array();

	//Update error code in amazon_submission_result_log
        $sql_batch = "SELECT * FROM ".AmazonDatabase::$m_pref."amazon_submission_list_log "
                . "WHERE id_country = ".$id_country." AND id_shop = ".$id_shop." ".(isset(self::$query_feed_type[$action_type]) ? self::$query_feed_type[$action_type] : '')."; ";
        
        $result_batch = $this->db->product->db_array_query($sql_batch);
        
        foreach ($result_batch as $value) {
            $message = unserialize(base64_decode($value['message']));
            if(isset($message['FeedSunmissionId']) && !empty($message['FeedSunmissionId']))
                $list_id_sunmission_feed[] = $message['FeedSunmissionId'];
        }
        
        $result_log_where = array(
            "sku" => array('operation' => "=", 'value' => $sku),
            "id_shop" => array('operation' => "=", 'value' => $id_shop),
            "id_country" => array('operation' => "=", 'value' => $id_country),
            "result_message_code" => array('operation' => "=", 'value' => $error_code),
            "id_submission_feed" => array('operation' => " IN ", 'value' => $list_id_sunmission_feed),
        );

        if(isset($result_code) && !empty($result_code)) {
            $result_log_data = array(
                'result_code' => $result_code
            );
        } else {
            $result_log_data = array(
                'result_code' => AmazonErrorResolution::_RESOLVED
            );
        }
        
	if(!$is_update_falg){
	    $sql_excute .= $this->amazon_model->update_string(AmazonDatabase::$m_pref.'amazon_submission_result_log', $result_log_data, $result_log_where);
        }

        // find id_product to recreate all in relationship
        $product = $this->amazon_model->getProductBySKU($id_shop, $sku);
       
        # Update amazon_inventory action_type to $actions
        if(isset($actions) && !empty($actions)){
            if($actions == self::_RECREATE){                
                $amazon_inventory_where = array(
                    "id_product" => array('operation' => "=", 'value' => $product['id_product']),
                    "id_shop" => array('operation' => "=", 'value' => $id_shop),
                    "id_country" => array('operation' => "=", 'value' => $id_country),
                );
            } else {
                $amazon_inventory_where = array(
                    "sku" => array('operation' => "=", 'value' => $sku),
                    "id_shop" => array('operation' => "=", 'value' => $id_shop),
                    "id_country" => array('operation' => "=", 'value' => $id_country),
                );
            }
            $amazon_inventory_data = array(
                'action_type' => $actions,
                'date_upd' => date('Y-m-d H:i:s'),
            );
            $sql_excute .= $this->amazon_model->update_string(AmazonDatabase::$m_pref.'amazon_inventory', $amazon_inventory_data, $amazon_inventory_where );
        }

        // Product Error
        if($action_type == "Create"){

            $id_product = $product['id_product'] ;
            $id_product_attribute = isset($product['id_product_attribute']) ? $product['id_product_attribute'] : null;
            $sql_excute .= $this->flag_product_update($id_shop, $id_country, $sku, $id_product, $id_product_attribute, 5000);
            // remove the override attribute for the error 5000: <--Invalid content was found starting with element ''xx''. One of ''{xx}''-->
            /*if($error_code == 5000){
                // get message from amazon_submission_result_log
                $sql_message = "SELECT result_description FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log "
                    . "WHERE id_country = ".$id_country." AND id_shop = ".$id_shop." AND sku = '".$sku."' AND result_message_code = '5000' "
                    . "AND result_description LIKE 'XML Parsing Error at Line%' ORDER BY date_add DESC LIMIT 1 ; ";
                $result_message = $this->db->product->db_array_query($sql_message);
                foreach ($result_message as $value) {
                    if(isset($value['result_description']) && !empty($value['result_description'])){
                        $subject = $value['result_description'];
                        $pattern = '/element \'\'(?P<field>\w+)\'\'. One of/';
                        preg_match($pattern, $subject, $matches);
                        if(isset($matches['field']) && !empty($matches['field'])){
                            $field_required = $matches['field'];
                            if(isset(AmazonErrorResolution::$err_attribute_exeption[$field_required]))
                                $field_required = AmazonErrorResolution::$err_attribute_exeption[$field_required];

                            $sql_excute .= " DELETE FROM ".AmazonDatabase::$m_pref."amazon_attribute_override WHERE id_country = ".$id_country." AND id_shop = ".$id_shop." "
                                . "AND sku = '$sku' AND attribute_field = '$field_required' ; ";
                        }
                    }
                }
            }
            // update product_update_log to current date when get update product will union this table
            $product_update_type = 'solved';
            $product_update_status = AmazonErrorResolution::_RESOLVED;
            $sql_excute .= " REPLACE INTO log_product_update_log ( "
                            ."id_product"
                            .(isset($product['id_product_attribute']) ? " ,id_product_attribute" : "")
                            .", id_shop, product_update_type, product_update_status, date_add ) "                
                            ." VALUES (".$product['id_product'].(isset($product['id_product_attribute']) ? " ,".$product['id_product_attribute'] : "")
                            .",".$id_shop.",'".$product_update_type."','".$product_update_status."','".date('Y-m-d H:i:s')."' ) ;";*/
        // Offer Error
        } else {
            // update to offers_flag_update
            if(isset($ext)){
                $sql_excute .= ImportLog::update_flag_offers(_ID_MARKETPLACE_AMZ_, $ext, $id_shop, $product['id_product'], 1);
            }
        }
        
        return $this->db->product->db_exec($sql_excute);
    }

    public function flag_product_update($id_shop, $id_country, $sku, $id_product, $id_product_attribute=null, $error_code=null){
        $sql_excute = '';
        
        // remove the override attribute for the error 5000: <--Invalid content was found starting with element ''xx''. One of ''{xx}''-->
        if(isset($error_code) && $error_code == 5000){
            // get message from amazon_submission_result_log
            $sql_message = "SELECT result_description FROM ".AmazonDatabase::$m_pref."amazon_submission_result_log "
                . "WHERE id_country = ".$id_country." AND id_shop = ".$id_shop." AND sku = '".$sku."' AND result_message_code = '5000' "
                . "AND result_description LIKE 'XML Parsing Error at Line%' ORDER BY date_add DESC LIMIT 1 ; ";

            $result_message = $this->db->product->db_array_query($sql_message);

            foreach ($result_message as $value) {
                if(isset($value['result_description']) && !empty($value['result_description'])){
                    $subject = $value['result_description'];
                    $pattern = '/element \'\'(?P<field>\w+)\'\'. One of/';
                    preg_match($pattern, $subject, $matches);
                    if(isset($matches['field']) && !empty($matches['field'])){
                        $field_required = $matches['field'];
                        if(isset(AmazonErrorResolution::$err_attribute_exeption[$field_required]))
                            $field_required = AmazonErrorResolution::$err_attribute_exeption[$field_required];

                        $sql_excute .= " DELETE FROM ".AmazonDatabase::$m_pref."amazon_attribute_override WHERE id_country = ".$id_country." AND id_shop = ".$id_shop." "
                            . "AND sku = '$sku' AND attribute_field = '$field_required' ; ";
                    }
                }
            }
        }

        // update product_update_log to current date when get update product will union this table
        $product_update_type = 'solved';
        $product_update_status = AmazonErrorResolution::_RESOLVED;
        $sql_excute .= " REPLACE INTO log_product_update_log ( "
                        ."id_product"
                        .(isset($id_product_attribute) ? " ,id_product_attribute" : "")
                        .", id_shop, product_update_type, product_update_status, date_add ) "
                        ." VALUES (".(int)$id_product.(isset($id_product_attribute) ? " ,".(int)$id_product_attribute : "")
                        .",".$id_shop.",'".$product_update_type."','".$product_update_status."','".date('Y-m-d H:i:s')."' ) ;";

        return $sql_excute;
    }

    public function get_asin_from_message($message){
	
	$ASIN = '';
	
	if(!empty($message)){
	    
	    if (strpos($message, 'ASIN') !== false) {
		
		$messages= preg_split('/ASIN/', $message);
		
		foreach($messages as $mess){

		    $mess = substr(trim($mess), 0, 10);
		    
		    if(Amazon_Tools::ValidateASIN($mess))
			$ASIN = $mess;
		}
	    }
	}
	return $ASIN;
    }
}