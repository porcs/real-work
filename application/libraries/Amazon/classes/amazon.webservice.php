<?php

if (!defined('AMAZON_MARKETPLACE_VERSION'))
    define('AMAZON_MARKETPLACE_VERSION', '1.0');

class Amazon_WebService {

    public $_debug;
    public $_cr;
    public $_att;
    public $region;
    public $mid;
    public $mpid;
    public $awsak;
    public $sk;
    const LF = "\n";
    const CRLF = "\r\n";
    
    public function __construct($auth, $from, $marketPlaces = null, $debug = false, $cr = "<br/>\n") {
        
        if ($debug == true) $this->_debug = true;
        else $this->_debug = false;

        $this->_cr = $cr; 
        $this->_att = "<font color=\"red\">!!!</font>";
        $this->setAuth($auth);

        if ($marketPlaces) $this->MarketPlaces = $marketPlaces;
        else $this->MarketPlaces = null;

        $this->region = $from['Country'];

        if ($this->_debug)
            printf('Constructor Completed successfully%s', $this->_cr);
	
        if ($this->_debug)
	    printf("auth : %s $this->_cr from : %s marketPlaces : %s", nl2br(print_r($auth, true)), nl2br(print_r($from, true)), nl2br(print_r($marketPlaces, true)));
	
        if ($this->_debug)
            printf("$this->_cr Constructor. Object of the Service class created succesfully" . $this->_cr);
    }

    private function responseCallback($ch, $string) {
        $httpStatusCode = (int) curl_getinfo($this->curlHandle, CURLINFO_HTTP_CODE);
        // For unsuccessful responses, i.e. non-200 HTTP responses, we write the response body
        // into a separate stream.
        if ($httpStatusCode == 200) {
            return fwrite($this->responseBodyContents, $string);
        } else {
            return NULL;
        }
    }

    private function parseHttpHeader($header) {
        $parsedHeader = array();
        foreach (explode("\n", $header) as $line) {
            $splitLine = preg_split('/:\s/', $line, 2, PREG_SPLIT_NO_EMPTY);
            if (sizeof($splitLine) == 2) {
                $parsedHeader[trim($splitLine[0])] = trim($splitLine[1]);
            }
        }
        return $parsedHeader;
    }

    private function _endPointURL() {
        $region = self::mws_endPointURL($this->region);
        if(empty($region)){
            if ($this->_debug) {
                printf('%s_endPointURL() function. Error. Incorrect Region Code "%s" . Please verify and try again.%s', $this->_att, $this->region, $this->_cr);
                die;
            }
                return(null);
        }
        return $region;
    }
    
    public static function mws_endPointURL($region) {
        switch (strtolower(trim($region))) {
            case 'com' : return('mws.amazonservices.com');
            case 'us' : return('mws.amazonservices.com');
            case 'ca' : return('mws.amazonservices.ca');
            case 'jp' :
            case 'ja' : return('mws.amazonservices.jp');
            case 'cn' : return('mws.amazonservices.cn');
            case 'in' : return('mws.amazonservices.in');
            case 'uk' : 
            case 'co_uk' : 
            case 'co.uk' : return('mws-eu.amazonservices.com');
            case 'de' :
            case 'es' :
            case 'fr' :
            case 'it' :
            case 'uk' : return('mws-eu.amazonservices.com');
            case 'com_mx' : 
            case 'com.mx' : 
            case 'mx' : return ('mws.amazonservices.com.mx'); 
            default : 
                return(null);
        }
    }
     
    public static function marketplaceIdToFullfillmentCenterId($marketplaceID)
    {
        switch ($marketplaceID)
        {
            case 'A13V1IB3VIYZZH' :
            case 'A1RKKUPIHCS9HS' :
            case 'A1PA6795UKMFR9' :
            case 'APJ6JRA9NG5V4' :
            case 'A1F83G8C2ARO7P' : // Europe
                $fullfillmentCenterId = 'AMAZON_EU';
                break;
            case 'ATVPDKIKX0DER' : // US
                $fullfillmentCenterId = 'AMAZON_NA';
                break;
            case 'A2EUQ1WTGCTBG2' : // Canada
                $fullfillmentCenterId = '?';
                break;
            case 'A1VC38T7YXB528' : // Japan
                $fullfillmentCenterId = 'AMAZON_JP';
                break;
            case 'A21TJRUUN4KGV' : // India
                $fullfillmentCenterId = 'AMAZON_IN'; // to be verified
                break;
            default :
                $fullfillmentCenterId = '?';
                break;
        }

        return ($fullfillmentCenterId);
    }
    
    public static function languageToFullfillmentCenterId($lang)
    {
        switch ($lang)
        {
            case 'en' :
            case 'fr' :
            case 'es' :
            case 'de' :
            case 'it' :
            case 'uk' : // UK
            case 'co_uk' : // UK
            case 'co.uk' : // UK
            case 'gb' : // UK
                $fullfillmentCenterId = 'AMAZON_EU'; // Europe
                break;
            case 'com' : // US
                $fullfillmentCenterId = 'AMAZON_NA';
                break;
            case 'ca' : // Canada
                $fullfillmentCenterId = '?';
                break;
            case 'jp' : // Japan
                $fullfillmentCenterId = 'AMAZON_JP';
                break;
            case 'in' : // India
                $fullfillmentCenterId = 'AMAZON_IN'; // to be verified
                break;
            default :
                $fullfillmentCenterId = '?';
                break;
        }

        return ($fullfillmentCenterId);
    }

    private function lang2MarketplaceId($lang) {
        switch ($lang) {            
            case 'en' : // 
            case 'fr' : // France
                return('A13V1IB3VIYZZH');
            case 'es' : // Spain
                return('A1RKKUPIHCS9HS');
            case 'de' : // Germany
                return('A1PA6795UKMFR9');
            case 'it' : // Italy
                return('APJ6JRA9NG5V4');
            case 'uk' : // UK
            case 'co_uk' : // UK
            case 'co.uk' : // UK
            case 'gb' : // UK
                return('A1F83G8C2ARO7P');
            case 'jp' : // Japan
                return('A1VC38T7YXB528');
            case 'in' : // India
                return('A21TJRUUN4KGV');
            case 'ca' : // Canada
                return('A2EUQ1WTGCTBG2');
            case 'com' : // 
                return('ATVPDKIKX0DER');
            case 'com_mx' : 
            case 'com.mx' : 
            case 'mx' : // Mexico
                return ('A1AM78C64UM0Y8');  
        }
        return(null);
    }
    
    private function _caller() {
        $trace = debug_backtrace();
        $caller = null;
        if (isset($trace[2]) && !empty($trace[2])) $caller = $trace[2];

        $ret = 'called by ' . $caller['function'] . '() ';
        if (isset($caller['class'])) $ret .= 'in ' . $caller['class'];

        return($ret);
    }

    private function setAuth($auth) {
        if ($this->_debug)
            printf('setAuth() call - called by %s %s', $this->_caller(), $this->_cr);
        if ($auth['MerchantID'] != null)
            $this->mid = $auth['MerchantID'];
        else {
            if ($this->_debug)
                printf('setAuth function. %s Error %s == null . Invalid value.%s', $this->_att, $auth['MerchantID'], $this->_cr);
            return false;
        }

        $this->user_name = ( isset($auth['user_name']) && !empty($auth['user_name']) ) ? $auth['user_name'] : null;
        $this->language = ( isset($auth['language']) && !empty($auth['language']) ) ? $auth['language'] : null;
        $this->lang = ( isset($auth['lang']) && !empty($auth['lang']) ) ? $auth['lang'] : null;
        $this->mpid = $this->lang2MarketplaceId($this->lang);
        $this->user = ( isset($auth['user_name']) && !empty($auth['user_name']) ) ? $auth['user_name'] : null;
        $this->id_shop = ( isset($auth['id_shop']) && !empty($auth['id_shop']) ) ? $auth['id_shop'] : null;
        $this->id_mode = ( isset($auth['id_mode']) && !empty($auth['id_mode']) ) ? $auth['id_mode'] : null;
        $this->id_lang = ( isset($auth['id_lang']) && !empty($auth['id_lang']) ) ? $auth['id_lang'] : null;
        $this->id_country = ( isset($auth['id_country']) && !empty($auth['id_country']) ) ? $auth['id_country'] : null;
        $this->status = ( isset($auth['status']) && !empty($auth['status']) ) ? $auth['status'] : null;
        $this->synchronization_field = ( isset($auth['synchronization_field']) && !empty($auth['synchronization_field']) ) ? $auth['synchronization_field'] : 'ean13';
        $this->ext = ( isset($auth['ext']) ) ? $auth['ext'] : '';
        
        //auth_token
        if( isset($auth['auth_token']) && !empty($auth['auth_token'])){
            $this->auth_token = $auth['auth_token'];
        }
        
        if ($auth['AWSAccessKeyID'] != null)
            $this->awsak = $auth['AWSAccessKeyID'];
        else {
            if ($this->_debug)
                printf('setAuth function. %s Error %s == null . Invalid value.%s', $this->_att, $auth['AWSAccessKeyID'], $this->_cr);
            return false;
        }
        if ($auth['SecretKey'] != null)
            $this->sk = $auth['SecretKey'];
        else {
            if ($this->_debug)
                printf("setAuth function. %s Error %s == null . Invalid value.%s", $this->_att, $auth['SecretKey'], $this->_cr);
            return false;
        }
        return;
        if ($this->_debug)
            printf('%ssetAuth - Function Sets Authentification Parameters :  
                    MerchantID : %s%s MarketplaceID : %s%s AWSAccessKeyID : %s%s AWSSecretKey : %s%s', $this->_cr, $this->mid, $this->_cr, $this->mpid, $this->_cr, $this->awsak, $this->_cr, $this->sk, $this->_cr);
        return true;
    }

    private function _processFeed($feedType, $feedContent) {
        
        if ($this->_debug) {
            print("$this->_cr _processFeed() function started.  $this->_cr
                Now we send following request to the WebService: $this->_cr
                \$feedType : $feedType $this->_cr
                \$feedContent:    $this->_cr
                   $feedContent  $this->_cr");
        }

        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feedContent);
        rewind($feedHandle);

        $parameters = array(
            'Action' => 'SubmitFeed',
            'MarketplaceIdList.Id.1' => $this->mpid,
            'Merchant' => $this->mid,
            'FeedType' => $feedType,
            'SignatureVersion' => 2,
            'SignatureMethod' => 'HmacSHA256',
        );        
        
        // Amazon Europe
        if (isset($this->MarketPlaces) && count($this->MarketPlaces)) {
            $i = 0;
            foreach ($this->MarketPlaces as $idx => $marketPlace) {
                $i++;
                $parameters['MarketplaceIdList.Id.' . $i] = $marketPlace;
            }
        }
        
        $response = $this->_callWSs("Feeds", $parameters, $feedContent);

        if ($response === false) {
            if ($this->_debug)
                printf("response is false: %s $this->_cr", $this->_caller());
            return false;
        }
        else {
            if ($this->_debug)
                printf("response is true: %s $this->_cr", nl2br(print_r($response, true)));
        }
        return($response);
    }

    private function getFeedSubmissionList($SubmissionId) {
        
        if ($this->_debug == true)
            printf("$this->_cr getFeedSubmissionList call. $this->_cr");
        
        if (!$SubmissionId || empty($SubmissionId) || $SubmissionId == NULL) {
            if ($this->_debug == true)
                printf("$this->_cr FeedSubmissionId is empty. $this->_cr");
            return 'Missing Submission Id';
        }

        $params['Action'] = 'GetFeedSubmissionList';
        $params['Marketplace'] = $this->mpid;
        $params['merchantID'] = $this->mid;
        $params['awsAccountID'] = $this->awsak;
        $params['secretKey'] = $this->sk;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['FeedSubmissionIdList.Id.1'] = $SubmissionId;
                 
        $response = $this->_callWSs('Feeds', $params, NULL, $returnXML = true);
        //$this->generate_xml($this->user_name, 'GetFeedSubmissionList_' . $SubmissionId, $response->saveXML());

        if (!$response)
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionList ERROR $this->_att response is false or null - response is: %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response, true)));

        if (!is_object($response))
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionList ERROR $this->_att response must be an object - response is: %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response, true)));

        if (isset($response) && is_object($response)) {
            if ($response->Error) {
                if ($this->_debug)
                    printf("%s/%s getFeedSubmissionList ERROR $this->_att message : %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response->Error->Message, true)));
            }

            if ($this->_debug)
                printf("$this->_cr getFeedSubmissionList() function. FeedProcessingStatus returned : %s $this->_cr", nl2br(print_r($response->GetFeedSubmissionListResult, true)));
        }
        return($response);
    }       

    public function generate_xml($user, $filename, $content) {
        
        $directory = dirname(__FILE__) . '/../../../../';
        $output_dir = USERDATA_PATH . $user . '/amazon/';

        // Files
        $offers_file = $output_dir . $filename . '.xml';

        // Check rights
        $old = umask(0);  
        if (!is_dir($output_dir) && !mkdir($output_dir, 0775, true)) {
            if ($this->_debug)
                sprintf('%s(%s): %s', basename(__FILE__), __LINE__, 'Unable to create path');
            return false;
        }

        if (!is_writable($output_dir))
            @chmod($output_dir, 0775);
        umask($old);
        if (isset($content)) {
            $current_wd = getcwd();
            // Windows Format
            $content = str_replace(self::LF, self::CRLF, $content);
            if (file_put_contents($offers_file, $content) === false) {
                if ($this->_debug)
                    sprintf('%s(%s): %s', basename(__FILE__), __LINE__, 'Unable to write to the XML file');
                return false;
            }
            chdir($current_wd);
        }
        return $offers_file;
    }

    public function unlink_file($user, $filename, $path = null) {
        
        $directory = dirname(__FILE__) . '/../../../../';
        $output_dir = USERDATA_PATH . $user . '/amazon/';

        if (isset($path) && !empty($path))
            $output_dir .= $path . '/';

        // Files
        $offers_file = $output_dir . $filename;

        // Check rights
        if (is_dir($output_dir) && file_exists($offers_file)) {
            @unlink($offers_file);
        }
    }

    public function simpleCallWS($params, $API, $returnXML = true) {
        
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params["AWSAccessKeyId"] = $this->awsak;
       
        if(isset($this->auth_token) && !empty($this->auth_token)){
                $params["MWSAuthToken"] = $this->auth_token;
        }
		
        $params["Timestamp"] = gmdate("Y-m-d\TH:i:s\Z");

        if (!isset($params["Version"]))
            $params["Version"] = "2011-07-01";

        $uri = '/' . trim($API);

        switch ($API) {
            case 'Reports' :
            case 'ReportsDownload' :
                $uri = '/';
                break;
            case 'Products' :
                $params["Version"] = "2011-10-01";
                break;
        }

        ksort($params);

        $method = "POST";
        $host = $this->_endPointURL();
        
        foreach ($params as $param => $value) {
            $param = str_replace("%7E", "~", rawurlencode($param));
            $value = str_replace("%7E", "~", rawurlencode($value));
            $canonicalized_query[] = $param . "=" . $value;
        }

        // create the canonicalized query
        $canonicalized_query = implode("&", $canonicalized_query);

        // create the string to sign
        $string_to_sign = $method . "\n" . $host . "\n" . $uri . "\n" . $canonicalized_query;

        // calculate HMAC with SHA256 and base64-encoding
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->sk, True));

        // encode the signature for the request
        $signature = str_replace("%7E", "~", rawurlencode($signature));

        //open connection
        $ch = curl_init();

        //working with headers
        $header = NULL;

        $curlOptions = array(
            CURLOPT_POST => true,
            CURLOPT_USERAGENT => 'Feed.Biz API/Common-Services/' . AMAZON_MARKETPLACE_VERSION . ' (Language=PHP/' . phpversion() . ')',
            CURLOPT_RETURNTRANSFER => true,
        );

        $curlOptions[CURLOPT_URL] = "https://" . $host . $uri;
        $curlOptions[CURLOPT_POSTFIELDS] = $canonicalized_query . "&Signature=" . $signature;
        $curlOptions[CURLOPT_SSL_VERIFYPEER] = false;
        $curlOptions[CURLOPT_VERBOSE] = false;

        $curlHandle = curl_init();
        curl_setopt_array($curlHandle, $curlOptions);

        if (($result = curl_exec($curlHandle)) == false)
            $pass = false;
        else
            $pass = true;
        
        if ($this->_debug) {
            echo $this->_caller();
            echo "Params: $this->_cr";
            echo nl2br(print_r($curlOptions, true));
            echo "CURL Data: $this->_cr";
            echo nl2br(print_r($params, true));
            echo $this->_cr;
            echo nl2br(print_r(curl_getinfo($curlHandle), true));  // get error info
            echo "cURL error number:" . curl_errno($curlHandle) . $this->_cr; // print error info
            echo "cURL error:" . curl_error($curlHandle) . $this->_cr;
        }
        if (!$pass)
            return(false);

        curl_close($curlHandle);

        if ($this->_debug) {
//            $XML = new DOMDocument();
//            $XML->loadXML($result);
//            $XML->formatOutput = true;
	    if(is_object($result)) {
		$XML = new SimpleXMLElement($result);
		$XML = $XML->saveXML();
	    } else {
		$XML = $result;
	    }
            echo '<b class="amazon-url">' . $curlOptions[CURLOPT_URL] . '</b>';
            echo '<pre class="amazon-xml">' . htmlentities($XML) . '</pre>';
        }
        if ($returnXML) {
            try {
                $xml = new SimpleXMLElement($result);
                return($xml);
            } catch (Exception $e) {
                echo("Exception Caught: " . $e->getMessage() . "\n");
                return false;
            }
        } else {
            return($result);
        }
    }

    public function ListMarketplaceParticipations($returnXML = true) {
        
        if ($this->_debug == true)
            printf("$this->_cr ConnectionCheck call. $this->_cr");
        $params = array();
        $params['Action'] = 'ListMarketplaceParticipations';
        $API = 'Sellers';
        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListMarketplaceParticipationsResult)) {
            if ($returnXML)
                return($xml);
            else
                return(true);
        } else
            return($xml);
    }

    public function _callWSs($WSTR, $params, $feedContent = NULL, $returnXML = false) {
        
        if ($this->_debug)
            printf("$this->_cr _callWSs() fonction starts here $this->_cr");
	
        $contentMD5 = NULL;
        $handle = NULL;

        if (!($feedContent === NULL)) {
	    
            if ($this->_debug)
                printf("$this->_cr We have feed content. It seems, that this operation is to submit feed. Calculating MD5 for this feed... $this->_cr");
	    
            $contentMD5 = $ContentMd5 = base64_encode(md5($feedContent, true));
	    
            if ($this->_debug)
                printf("$this->_cr Now we start to work we handle to upload data to the server $this->_cr");
	    
            $handle = @fopen('php://temp', 'rw+');
            fwrite($handle, $feedContent);
            rewind($handle);
        }

        $method = "POST";
        $host = $this->_endPointURL();
        $uri = NULL;

        // additional parameters
        $params["AWSAccessKeyId"] = $this->awsak;
		
        if(isset($this->auth_token) && !empty($this->auth_token)){
            $params["MWSAuthToken"] = $this->auth_token;
        }
		
        // GMT timestamp
        $params["Timestamp"] = gmdate("Y-m-d\TH:i:s\Z");
        $header = NULL;
        switch ($WSTR) {
            case 'Feeds' :
                $header = array(
                    'Expect: ',
                    'Accept: ',
                    'Transfer-Encoding: chunked',
                    'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                    'Content-MD5: ' . $contentMD5
                );

            case 'Reports' :
            case 'ReportsDownload' :
                $params["Version"] = '2009-01-01';
                $uri = '/';
                break;
            case 'Orders' :
                $params["Version"] = '2013-09-01';
                //'2011-01-01'; Old version Orders 2011-01-01 – to be deprecated 2/28/17. Get the new version Orders 2013-09-01 with the latest Client Libraries.
                $uri = '/Orders/';
                break;
            case 'Products' :
                $params["Version"] = '2011-10-01';
                $uri = '/Products/';
                break;
            case 'FulfillmentInventory' :
                $params["Version"] = '2010-10-01';
                $uri = '/FulfillmentInventory/';
                break;
            case 'FulfillmentOutboundShipment' :
                $params["Version"] = '2010-10-01';
                $uri = '/FulfillmentOutboundShipment/';
                break;

            default :
                if ($this->_debug)
                    printf("ERROR! Wrong Service" . $this->_cr);
                return(false);
        }

        if (!isset($params['SignatureVersion']))
            $params['SignatureVersion'] = '2';
        if (!isset($params['SignatureMethod']))
            $params['SignatureMethod'] = 'HmacSHA256';
        if (!isset($params['SellerId']))
            $params['SellerId'] = $this->mid;

        // sort the parameters
        ksort($params);
        
        // create the canonicalized query
        $canonicalized_query = array();

        foreach ($params as $param => $value) {
            $param = str_replace("%7E", "~", rawurlencode($param));
            $value = str_replace("%7E", "~", rawurlencode($value));
            $canonicalized_query[] = $param . "=" . $value;
        }

        $canonicalized_query = implode("&", $canonicalized_query);

        // create the string to sign
        $string_to_sign = $method . "\n" . $host . "\n" . $uri . "\n" . $canonicalized_query;

        // calculate HMAC with SHA256 and base64-encoding
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->sk, True));

        // encode the signature for the request
        $signature = str_replace("%7E", "~", rawurlencode($signature));

        $curlOptions = array(
            CURLOPT_POST => true,
            CURLOPT_USERAGENT => 'Feed.biz/Feed.biz Automaton/' . AMAZON_MARKETPLACE_VERSION . ' (Language=PHP/' . phpversion() . ')',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_DNS_USE_GLOBAL_CACHE => false,
            CURLOPT_DNS_CACHE_TIMEOUT => 5,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
        );

        if ($WSTR === 'Feeds') {
            $curlOptions[CURLOPT_HTTPHEADER] = $header;

            if ($handle)
                $curlOptions[CURLOPT_INFILE] = $handle;

            $curlOptions[CURLOPT_URL] = "https://" . $host . $uri . "?" . $canonicalized_query . "&Signature=" . $signature;
        }
        else {
            $curlOptions[CURLOPT_URL] = "https://" . $host . $uri;
            $curlOptions[CURLOPT_POSTFIELDS] = $canonicalized_query . "&Signature=" . $signature;

            if ($WSTR === 'ReportsDownload') {
                $this->responseBodyContents = @fopen('php://memory', 'rw+');
                $this->headerContents = @fopen('php://memory', 'rw+');
                $this->errorResponseBody = @fopen('php://memory', 'rw+');

                $curlOptions[CURLOPT_WRITEFUNCTION] = array($this, 'responseCallback');
                $curlOptions[CURLOPT_HEADERFUNCTION] = array($this, 'headerCallback');
            }
        }

        if ($this->_debug) {
            echo("$this->_cr Request is: " . $curlOptions[CURLOPT_URL] . "$this->_cr");
        }
        $curlOptions[CURLOPT_SSL_VERIFYPEER] = false;

        if ($this->_debug)
            $curlOptions[CURLOPT_VERBOSE] = true;

        $this->curlHandle = curl_init();
        curl_setopt_array($this->curlHandle, $curlOptions);
	
	if ($this->_debug) {
	    echo "curlOptions: $this->_cr";
	    echo nl2br(print_r($curlOptions, true));
	}
	 
        $result = curl_exec($this->curlHandle);
        if ($this->_debug) {
            printf("%s/s%s _callWSs $this->_cr", basename(__FILE__), __LINE__);
            echo $this->_caller();
            echo "Params: $this->_cr";
            echo nl2br(print_r($params, true));
            echo "CURL Data: $this->_cr";
            echo nl2br(print_r($result, true));
            echo $this->_cr;
            echo nl2br(print_r(curl_getinfo($this->curlHandle), true));  // get error info
            echo "cURL error number:" . curl_errno($this->curlHandle) . $this->_cr; // print error info
            echo "cURL error:" . curl_error($this->curlHandle) . $this->_cr;
        }

        if ($WSTR === 'Feeds' && $handle)
            fclose($handle);
        if ($WSTR === 'ReportsDownload') {
            rewind($this->headerContents);
            $header = stream_get_contents($this->headerContents);
            $parsedHeader = $this->parseHttpHeader($header);

            rewind($this->responseBodyContents);
            $content = stream_get_contents($this->responseBodyContents);

            @fclose($this->headerContents);
            @fclose($this->errorResponseBody);

            if ($this->_debug) {
                printf("$this->_cr Request completed successfully. Function returns Text element. Response is $this->_cr
                      $content $this->_cr");
            }

            return $content;
        }

        curl_close($this->curlHandle);

        if ($result === false) {
            if ($this->_debug) {
                printf("%s/%s _callWSs ERROR $this->_att $this->_cr", basename(__FILE__), __LINE__);
                echo $this->_caller();
                echo "Params: $this->_cr";
                echo nl2br(print_r($params, true));
                echo "CURL Data: $this->_cr";
                echo nl2br(print_r($result, true));
                echo $this->_cr;
                echo nl2br(print_r(curl_getinfo($this->curlHandle), true));  // get error info
                echo "cURL error number:" . curl_errno($this->curlHandle) . $this->_cr; // print error info
                echo "cURL error:" . curl_error($this->curlHandle) . $this->_cr;
            }
            return false;
        }

        // No XML data
        if ($result && $params['Action'] == 'GetReport') {
            if ($this->_debug)
                printf("Data is not XML");
            return($result);
        }

        try {
            $pxml = new SimpleXMLElement($result);
        } catch (Exception $e) {
            echo("Exception Caught: " . $e->getMessage() . "\n");
            return false;
        }

        if ($pxml === false) {
            if ($this->_debug)
                printf("ERROR! no xml (\$pxml === false)");
            return false; // no xml
        }

        if ($returnXML) {
            return($pxml);
        }

        if ($this->_debug) {
            $r = $pxml->saveXML();
            print("$this->_cr Request completed successfully. Function returns simpleXml element. Response is $this->_cr $r $this->_cr");
        }

        return $pxml;
    }

    public function updateFeeds($xml, $type) {
        
        if ($this->_debug)
            printf("$this->_cr updateProducts() function. %s $this->_cr", $this->_caller());
        $SubmitIDs = array();

        switch ($type) {
            case 'product' :
                $Submissions = $this->_processFeed("_POST_PRODUCT_DATA_", $xml);
                break;
            case 'price' :
                $Submissions = $this->_processFeed("_POST_PRODUCT_PRICING_DATA_", $xml);
                break;
            case 'inventory' :
                $Submissions = $this->_processFeed("_POST_INVENTORY_AVAILABILITY_DATA_", $xml);
                break;
            case 'image' :
                $Submissions = $this->_processFeed("_POST_PRODUCT_IMAGE_DATA_", $xml);
                break;
            case 'relationship' :
                $Submissions = $this->_processFeed("_POST_PRODUCT_RELATIONSHIP_DATA_", $xml);
                break;
            case 'orderfulfillment' :
                $Submissions = $this->_processFeed("_POST_ORDER_FULFILLMENT_DATA_", $xml);
                break;
            case 'shipping_override' :
                $Submissions = $this->_processFeed("_POST_PRODUCT_OVERRIDES_DATA_", $xml);
                break;
            case 'orderacknowledgement' :
                $Submissions = $this->_processFeed("_POST_ORDER_ACKNOWLEDGEMENT_DATA_", $xml);
                break;
        }

        if ($Submissions === false) {
            if ($this->_debug)
                printf("$this->_cr updateProducts() function. $this->_att Error. There are no products to update. All products are skipped. Function terminated. $this->_cr");
            return false;
        }

        if (!isset($Submissions->SubmitFeedResult->FeedSubmissionInfo)) {
            if ($this->_debug)
                printf("$this->_cr updateProducts() function. $this->_att Error. No submission feed data. Return is: %s $this->_cr", nl2br(print_r($Submissions, true)));
            return(false);
        }
        elseif (!isset($Submissions->SubmitFeedResult)) {
            if ($this->_debug)
                printf("$this->_cr updateProducts() function. $this->_att Error. No submission feed info. Return is: %s $this->_cr", nl2br(print_r($Submissions, true)));
            return(false);
        }

        if (isset($Submissions) && is_object($Submissions)) {
            if ($this->_debug)
                printf("$this->_cr updateProducts() function. partiallyUpdateProducts returned : %s $this->_cr", nl2br(print_r($Submissions->SubmitFeedResult, true)));

            if (isset($Submissions->Error))
                return($Submissions);

            $SubmitIDs = strval($Submissions->SubmitFeedResult->FeedSubmissionInfo->FeedSubmissionId);
        }

        if ($this->_debug)
            printf("$this->_cr updateProducts() function. Returns: %s $this->_cr", nl2br(print_r($SubmitIDs, true)));

        return($SubmitIDs);
    }
    
    private function CheckRequestFeedSubmissionList($amazonDatabase){
        
        if(isset($amazonDatabase) && !empty($amazonDatabase)) {
            $where_fields = array(
                'message' => array(
                    'value' => 'RequestFeedSubmission',
                    'operation' => '='
                ),
                'requestReportTime' => array(
                    'value' => date('Y-m-d H:i:s', strtotime('now - 2 min')),
                    'operation' => '>='
                )
            );
       
            $request_report_log = $amazonDatabase->get_request_report_log(null, null, $where_fields);
        
            if(sizeof($request_report_log) > 5){
                return false;
            }
        }
        
        return true;
    }
    
    private function SaveRequestFeedSubmissionList($requestId, $status, $step, $feed_type, $params, $time, $amazonDatabase){ 
        if(isset($amazonDatabase) && !empty($amazonDatabase)) {
            $data = array(
                'id_country' => $params->id_country,
                'id_shop' => $params->id_shop,
                'requestReportTime' => date('Y-m-d H:i:s'),
                'reportTime' => $time,
                'requestId' => $requestId,
                'reportProcessingStatus' => $status,
                'step' => $step,
                'title' => $feed_type,
                'message' => 'RequestFeedSubmission',
                'date_add' => date("Y-m-d H:i:s"),
            );
            $amazonDatabase->save_request_report($data, false);    
        }
    }    
    
    public function getFeedSubmissionListResult($SubmissionId, $amazonDatabase = null , $params = null, $time = null, $feed_type = null) {
        
        $status = '';
        
        if (!isset($SubmissionId) || empty($SubmissionId)) {
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionListResult ERROR $this->_att : FeedSubmissionId is required $this->_cr", basename(__FILE__), __LINE__);
            return 'Missing Submission Id';
        }
        
        // wait 10 sec.
        sleep(10);
        
        do {    
            
            $FeedSubmissionList = $this->getFeedSubmissionList($SubmissionId);

            if (!isset($FeedSubmissionList) || empty($FeedSubmissionList)){
                if ($this->_debug) {
                    printf("%s/%s getFeedSubmissionListResult ERROR $this->_att : %s $this->_cr", basename(__FILE__), __LINE__, $FeedSubmissionList);
                }
            }
            if (isset($FeedSubmissionList->Error)) {
                if ($this->_debug) {
                    printf("%s/%s getFeedSubmissionList ERROR $this->_att message : %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($FeedSubmissionList->Error->Message, true)));
                }
            }
            if (isset($FeedSubmissionList->GetFeedSubmissionListResult->FeedSubmissionInfo->FeedProcessingStatus)) {
                $status = strval($FeedSubmissionList->GetFeedSubmissionListResult->FeedSubmissionInfo->FeedProcessingStatus);
            }
            if ($status != '_DONE_' && !empty($status)) {
                if($feed_type == "image"){
                    sleep(180); //180
                } else {
                    sleep(120); //120
                }
            }

            if ($this->_debug) {
                printf("$this->_cr FeedProcessingStatus %s $this->_cr", (print_r($FeedSubmissionList, true)));
            }
                
        }
        while ($status != "_DONE_");

        if ($status == '_DONE_' && isset($FeedSubmissionList))
            return($FeedSubmissionList);
        else
            $this->getFeedSubmissionListResult($SubmissionId, $amazonDatabase, $params, $time, $feed_type);
    }

    public function getFeedSubmissionResult($SubmissionId) {
        
        if ($this->_debug == true)
            printf("$this->_cr getFeedSubmissionResult call. $this->_cr");

        $params['Action'] = 'GetFeedSubmissionResult';
        $params['Marketplace'] = $this->mpid;
        $params['Merchant'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['FeedSubmissionId'] = $SubmissionId;
        $response = $this->_callWSs('Feeds', $params, NULL, $returnXML = true);

        if (isset($response->Error)/*$response->Error*/) {
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionResult ERROR $this->_att message : %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response->Error->Message, true)));
            return($response);
        }

        if (!$response) {
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionResult ERROR $this->_att response is false or null - response is: %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response, true)));
            return(false);
        }

        if (!is_object($response)) {
            if ($this->_debug)
                printf("%s/%s getFeedSubmissionResult ERROR $this->_att response must be an object - response is: %s $this->_cr", basename(__FILE__), __LINE__, nl2br(print_r($response, true)));
            return(false);
        }

        return($response);
    }

    public function GetUnshippedOrdersList($createdAfterDate, $createdBeforeDate = null, $Status = 'All', $FBA = false, $returnXML = false) {
        
        if ($this->_debug == true){
            printf("$this->_cr GetUnshippedOrdersList call. $this->_cr");
            printf("$this->_cr CreatedAfterDate: $createdAfterDate . $this->_cr");
            printf("$this->_cr CreatedBeforeDate: $createdBeforeDate . $this->_cr");
            printf("$this->_cr Status: $Status. $this->_cr");
	}
	
        $params = array();
        $params['Action'] = 'ListOrders';
        $params['LastUpdatedAfter'] = $createdAfterDate;
        
        if ($createdBeforeDate != null) {
            $params['LastUpdatedBefore'] = $createdBeforeDate;
        }
        
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['MarketplaceId.Id.1'] = $this->mpid;

        if (isset($this->MarketPlaces) && count($this->MarketPlaces)) {
            $i = 1;
            foreach ($this->MarketPlaces as $idx => $marketPlace) {
                if (empty($marketPlace))
                    continue;
                if ($marketPlace == $this->mpid)
                    continue;
                $i++;
                $params['MarketplaceId.Id.' . $i] = $marketPlace;
            }
        }

        if (!empty($FBA) && is_string($FBA) && !is_numeric($FBA)) {
            $params['FulfillmentChannel.Channel.1'] = $FBA;
        }
	
	$params['OrderStatus.Status.1'] = 'Pending';

        if ($FBA)
        {
            $params['OrderStatus.Status.2'] = 'Unshipped';
            $params['OrderStatus.Status.3'] = 'PartiallyShipped';
            $params['OrderStatus.Status.4'] = 'Shipped';
            $params['OrderStatus.Status.5'] = 'Canceled';
        }
        elseif ($Status == 'Shipped')
        {
            $params['OrderStatus.Status.2'] = 'Shipped';
        }
        elseif ($Status == 'Unshipped' || $Status == 'PartiallyShipped')
        {
            $params['OrderStatus.Status.2'] = 'Unshipped';
            $params['OrderStatus.Status.3'] = 'PartiallyShipped';
        }
        else if($Status != 'Pending') 
        {
            if ($Status != 'All')
            {
                $params['OrderStatus.Status.2'] = $Status;
            }
            else
            {
                unset($params['OrderStatus.Status.1']);
            }
        }	

        $params['MaxResultsPerPage'] = 100;

        if ($this->_debug == true)
            printf("$this->_cr GetUnshippedOrdersList function. Start request $this->_cr");
	
	if ($this->_debug == true)
            echo 'Webservice Params : <pre>'. print_r($params, true) .'</pre>' ;
	
        $data = $this->_callWSs('Orders', $params, null, $returnXML); // remove comment
        //$pxml = file_get_contents('D:\Feedbiz_Local\assets\apps\users\Praew\amazon\order.xml');
	//$data = new SimpleXMLElement($pxml);

        if ($this->_debug) {
            if (is_object($data))
                echo nl2br(print_r($data->asXML(), true));
            else
                echo nl2br(print_r($data, true));
        }

        if (isset($data->Error) && $returnXML)
            return($data);

        if (isset($data->ListOrdersResult))
            $data = $data->ListOrdersResult;

        if ($data === null)
            return false; // no orders available

        return $data;
    }

    public function GetOrderItems($amazonOrderId) {

        $params = array();
        $params['Action'] = 'ListOrderItems';
        $params['AmazonOrderId'] = $amazonOrderId;
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';

	if ($this->_debug == true)
            echo 'GetOrderItems Params : <pre>'. print_r($params, true) .'</pre>' ;
	
        $itemsXml = $this->_callWSs('Orders', $params); // remove comment
	
	/*DEBUG*/
	//file_put_contents(dirname(__FILE__).'/../orderImport/Orders_Item_Pending.xml', $itemsXml->saveXML());
	//$pxml = file_get_contents('D:\SVN\feed-biz\trunk\feed-biz-db\assets\apps\users\u00000000000026\amazon\orders\Orders_Item_171-6197538-1865155.xml');
	//$itemsXml = new SimpleXMLElement($pxml);
	/*DEBUG*/
		
        if ($itemsXml === false) {
            if ($this->_debug)
                printf("$this->_cr $this->_att $this->_cr getOrderItems function(). An error happened during items xml request $this->_cr");
            return false;
        }

        if ($this->_debug == true) {
            $d = $itemsXml->saveXML();
            printf("$this->_cr getOrderItems function(). We get next Item xml from the order: $this->_cr $d $this->_cr");
        }

        if (!isset($itemsXml->ListOrderItemsResult->OrderItems)) {
            if ($this->_debug == true)
                printf("$this->_cr getOrderItems function(). Unable to fetch items returned value is: %s$this->_cr $d $this->_cr", nl2br(print_r($itemsXml, true)));

            return(false);
        }

        $ItemsSimpleXMLElement = $itemsXml->ListOrderItemsResult;
	
        return($ItemsSimpleXMLElement);
    }

    public function GetOrdersByNextToken($token) {
        
        if ($this->_debug == true)
            printf("$this->_cr GetOrdersByNextToken call. $this->_cr");

        $params = array();
        $params['Action'] = 'ListOrdersByNextToken';
        $params['NextToken'] = $token;
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['MarketplaceId.Id.1'] = $this->mpid;

        if ($this->_debug == true)
            printf("$this->_cr GetOrdersByNextToken function. Start request - Token: %s $this->_cr", $token);

        $data = $this->_callWSs('Orders', $params);

        if ($this->_debug) {
            $d = $data->asXML();
            printf("$this->_cr $d <$this->_cr");
        }

        if (isset($data->ListOrdersByNextTokenResult))
            $data = $data->ListOrdersByNextTokenResult;
        if ($data === null)
            return false; // no orders available

        return($data);
    }
    
    public function ServiceStatus($API, $returnXML = false, $Version = '2013-09-01') {
        
        if ($this->_debug == true) printf("$this->_cr ConnectionCheck call. $this->_cr");
        $params = array();
        $params['Action'] = 'GetServiceStatus';
        $params["Version"] = $Version;
        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->GetServiceStatusResult)) {
            if ($returnXML) return($xml);
            else return(true);
        } else return($xml);
    }
    
    public function ListSubscriptions($returnXML = true) {
        if ($this->_debug == true)  printf("$this->_cr ListSubscriptions call. $this->_cr");

        $params = array();
        $params['Action'] = 'ListSubscriptions';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListSubscriptionsResult)) {
            if ($returnXML) return ($xml);
            else return (true);
        }
        return ($xml);
    }

    public function GetSubscription($URL, $notificationType = 'AnyOfferChanged', $returnXML = true) {
        if ($this->_debug == true) printf("$this->_cr GetSubscription call. $this->_cr");

        $params = array();
        $params['Action'] = 'GetSubscription';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['NotificationType'] = $notificationType;
        $params['Destination.DeliveryChannel'] = 'SQS';
        $params['Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Destination.AttributeList.member.1.Value'] = $URL;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListSubscriptionsResult)) {
            if ($returnXML) return ($xml);
            else return (true);
        }
        return ($xml);
    }

    public function CreateSubscription($URL, $notificationType = 'AnyOfferChanged', $returnXML = true) {
        if ($this->_debug == true) printf("$this->_cr CreateSubscription call. $this->_cr");

        $params = array();
        $params['Action'] = 'CreateSubscription';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['Subscription.NotificationType'] = $notificationType;
        $params['Subscription.IsEnabled'] = 'true';
        $params['Subscription.Destination.DeliveryChannel'] = 'SQS';
        $params['Subscription.Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Subscription.Destination.AttributeList.member.1.Value'] = $URL;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListSubscriptionsResult))  {
            if ($returnXML) return ($xml);
            else return (true);
        }
        return ($xml);
    }

    public function DeleteSubscription($URL, $notificationType = 'AnyOfferChanged', $returnXML = true) {
        if ($this->_debug == true)  printf("$this->_cr DeleteSubscription call. $this->_cr");

        $params = array();
        $params['Action'] = 'DeleteSubscription';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['NotificationType'] = $notificationType;
        $params['Subscription.IsEnabled'] = 'true';
        $params['Destination.DeliveryChannel'] = 'SQS';
        $params['Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Destination.AttributeList.member.1.Value'] = $URL;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListSubscriptionsResult)) {
            if ($returnXML) return ($xml);
            else  return (true);
        }
        return ($xml);
    }

    public function SendTestNotificationToDestination($URL, $returnXML = true){
        if ($this->_debug == true)  printf("$this->_cr SendTestNotificationToDestination call. $this->_cr");

        $params = array();
        $params['Action'] = 'SendTestNotificationToDestination';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['Destination.DeliveryChannel'] = 'SQS';
        $params['Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Destination.AttributeList.member.1.Value'] = $URL;

        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->SendTestNotificationToDestinationResult))  {
            if ($returnXML)  return ($xml);
            else return (true);
        }
        return ($xml);
    }

    public function RegisterDestination($URL, $returnXML = true){
        if ($this->_debug == true) printf("$this->_cr RegisterDestination call. $this->_cr");

        $params = array();
        $params['Action'] = 'RegisterDestination';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['Destination.DeliveryChannel'] = 'SQS';
        $params['Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Destination.AttributeList.member.1.Value'] = $URL;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->RegisterDestinationResult)) {
            if ($returnXML)  return ($xml);
            else return (true);
        }
        return ($xml);
    }

    public function DeregisterDestination($URL, $returnXML = true){
        if ($this->_debug == true) printf("$this->_cr DeregisterDestination call. $this->_cr");

        $params = array();
        $params['Action'] = 'DeregisterDestination';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $params['Destination.DeliveryChannel'] = 'SQS';
        $params['Destination.AttributeList.member.1.Key'] = 'sqsQueueUrl';
        $params['Destination.AttributeList.member.1.Value'] = $URL;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->DeregisterDestinationResult))  {
            if ($returnXML)  return ($xml);
            else  return (true);
        }
        return ($xml);
    }

    public function ListRegisteredDestinations($returnXML = true) {
        
        if ($this->_debug == true) printf("$this->_cr ConnectionCheck call. $this->_cr");

        $params = array();
        $params['Action'] = 'ListRegisteredDestinations';
        $params['Version'] = '2013-07-01';
        $params['MarketplaceId'] = $this->mpid;
        $API = 'Subscriptions';

        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->ListRegisteredDestinationsResult)) {
            if ($returnXML) return ($xml);
            else return (true);
        }
        
        return ($xml);
    }
    
    public function GetMyPriceForSKU($SKUs, $returnXML = true) {
        
        if ($this->_debug == true)  printf("$this->_cr GetMyPriceForSKU call. $this->_cr");

        if (!is_array($SKUs) || !count($SKUs)) return(null);

        $params = array();
        $params['Action'] = 'GetMyPriceForSKU';
        $params['MarketplaceId'] = $this->mpid;

        $API = 'Products';

        $i = 0;

        foreach ($SKUs as $sku) {
            $i++;
            $params['SellerSKUList.SellerSKU.' . $i] = $sku['SKU'];
        }
        
        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->GetMyPriceForSKUResult)) {
            if ($returnXML)
                return ($xml);
            else
                return (true);
        }
        return ($xml);
    }

    public function GetMyPriceForASIN($ASINs, $returnXML = true) {
        
        if ($this->_debug == true)  printf("$this->_cr GetMyPriceForASIN call. $this->_cr");

        if (!is_array($ASINs) || !count($ASINs)) return(null);

        $params = array();
        $params['Action'] = 'GetMyPriceForASIN';
        $params['MarketplaceId'] = $this->mpid;

        $API = 'Products';

        $i = 0;

        foreach ($ASINs as $asin) {
            $i++;
            $params['ASINList.ASIN.' . $i] = $asin;
        }
        
        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->GetMyPriceForASINResult)) {
            if ($returnXML)
                return ($xml);
            else
                return (true);
        }
        return ($xml);
    }
    
    public function GetCompetitivePricingForSKU($SKUs, $returnXML = true) {
        
        if ($this->_debug == true)  printf("$this->_cr GetCompetitivePricingForSKU call. $this->_cr");

        if (!is_array($SKUs) || !count($SKUs)) return(null);

        $params = array();
        $params['Action'] = 'GetCompetitivePricingForSKU';
        $params['MarketplaceId'] = $this->mpid;
        $API = 'Products';

        $i = 0;

        foreach ($SKUs as $sku) {
            $i++;
            $params['SellerSKUList.SellerSKU.' . $i] = $sku['SKU'];
        }
        
        $xml = $this->simpleCallWS($params, $API, $returnXML);

        if (isset($xml->GetCompetitivePricingForSKUResult)) {
            if ($returnXML)
                return ($xml);
            else
                return (true);
        }
        return ($xml);
    }

    /* FBA */
    //
    public function ListAllFulfillmentOrders($Date, $maxQueries = 10)  {
        $params = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';

        $querytime = strtotime($Date);
        $now = time();
        $last_month = time() - (86400 * 30);
	
        if ($querytime > $now || $querytime < $last_month) {
            if ($this->_debug) {
                printf('ListAllFulfillmentOrders() function. %sWarning. Wrong Date.%s', $this->_att, $Date, $this->_cr);
            }

            return (false);
        }

        $params['QueryStartDateTime'] = gmdate('Y-m-d\TH:i:s\Z', $querytime);

        $dataset = array();
        $index = 0;
        $nextToken = 1;

        for ($i = 0; $i < $maxQueries && $nextToken; $i++) {
            if ($index == 0) {
                $params['Action'] = 'ListAllFulfillmentOrders';
		
		if ($this->_debug) {
		    
		    $pxml = file_get_contents(dirname(__FILE__) . '/../orderImport/ListAllFulfillmentOrdersResponse-Test-Foxchip-FBA-Order.xml');	
		    $data = new SimpleXMLElement($pxml);
		    
		} else {
		    
		    $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);		
		    
		}
		
                $responseItem = 'ListAllFulfillmentOrdersResult';
            } else {
                $params['Action'] = 'ListAllFulfillmentOrdersByNextToken';
                $params['NextToken'] = (string)$nextToken;
                $data = $this->_callWSs('FulfillmentOutboundShipment', $params);
                $responseItem = 'ListAllFulfillmentOrdersByNextTokenResult';
            }
            if ($data === false) {
                if ($this->_debug) {
                    printf("$this->_cr $this->_att An error happened during ListAllFulfillmentOrders request $this->_cr");
                }

                return false;
            }

            if (isset($data->{$responseItem}->NextToken) && $data->{$responseItem}->NextToken) {
                $nextToken = $data->{$responseItem}->NextToken;
            } else {
                $nextToken = null;
            }

            if (!isset($data->{$responseItem})) {
                if ($this->_debug) {
                    printf("ListAllFulfillmentOrders() function failed $this->_att An error happened during request... $this->_cr");
                }

                return false;
            }

            if ($this->_debug) {
                if ($data instanceof SimpleXMLElement) {
                    echo '<pre>';
                    echo htmlentities(print_r($data->asXML(), true));
                    echo '</pre>';
                } else {
                    echo nl2br(print_r($data, true));
                }
            }

            if (!isset($data->{$responseItem})) {
                return false;
            }

            if (!isset($data->{$responseItem}->FulfillmentOrders->member)) {
                if ($this->_debug) {
                    printf("ListAllFulfillmentOrders()  $this->_att empty inventory... $this->_cr");
                }

                return false;
            }
            $dataset[$index++] = $data->{$responseItem}->FulfillmentOrders;
        }
	
        return ($dataset);
    }

    public function CancelFulfillmentOrder($orderId)  {	
        $params = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['Action'] = 'CancelFulfillmentOrder';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';
        $params['SellerFulfillmentOrderId'] = $orderId;

        $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);
	
	/*Test put content file
	$test_path = dirname(__FILE__) . '/../../../../assets/apps/users/' . $this->user_name . '/amazon/';
	file_put_contents($test_path.'CancelFulfillmentOrder.txt', print_r($data->asXML(), true));
	file_put_contents($test_path.'CancelFulfillmentOrder.xml', $data->asXML());
	Test put content file*/
	
        if ($data === false) {
            if ($this->_debug) {
                printf("$this->_cr $this->_att An error happened during CancelFulfillmentOrder request $this->_cr");
            }

            return false;
        }

        if ($this->_debug) {
            if ($data instanceof SimpleXMLElement) {
                echo '<pre>';
                echo htmlentities(print_r($data->asXML(), true));
                echo '</pre>';
            } else {
                echo nl2br(print_r($data, true));
            }
        }

        if (isset($data->Error->Code)) {
            if ($this->_debug) {
                printf("CancelFulfillmentOrder()  $this->_att some error in the request answer... $this->_cr");
            }
        } elseif (!isset($data->ResponseMetadata->RequestId)) {
            if ($this->_debug) {
                printf("CancelFulfillmentOrder()  $this->_att some other error in the request answer... $this->_cr");
            }

            return false;
        }

        return ($data);
    }

    public function GetFulfillmentOrder($orderId) {
        $params = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['Action'] = 'GetFulfillmentOrder';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';
        $params['SellerFulfillmentOrderId'] = $orderId;
	
	if ($this->_debug) {
	    
	    $pxml = file_get_contents(dirname(__FILE__) . '/../orderImport/GetFulfillmentOrderResponse-Test-Foxchip-FBA-Order-1.xml');	
	    $data = new SimpleXMLElement($pxml);
	    
	} else {
	    
	    $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);	
	    
	}
	
        if ($data === false) {
            if ($this->_debug) {
                printf("$this->_cr $this->_att An error happened during GetFulfillmentOrder request $this->_cr");
            }

            return false;
        }

        if ($this->_debug) {
            if ($data instanceof SimpleXMLElement) {
                echo '<pre>';
                echo htmlentities(print_r($data->asXML(), true));
                echo '</pre>';
            } else {
                echo nl2br(print_r($data, true));
            }
        }

        if (isset($data->Error->Code)) {
            if ($this->_debug) {
                printf("GetFulfillmentOrder()  $this->_att some error in the request answer... $this->_cr");
            }
        } elseif (!isset($data->ResponseMetadata->RequestId)) {
            if ($this->_debug) {
                printf("GetFulfillmentOrder()  $this->_att some other error in the request answer... $this->_cr");
            }

            return false;
        }
	
        return ($data);
    }
    
    /*
    public function GetPackageTrackingDetails($PackageNumber)  {
        $params = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['Action'] = 'GetPackageTrackingDetails';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';
        $params['PackageNumber'] = $PackageNumber;

        $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);

        if ($data === false) {
            if ($this->_debug) {
                printf("$this->_cr $this->_att An error happened during GetFulfillmentOrder request $this->_cr");
            }

            return false;
        }

        if ($this->_debug) {
            if ($data instanceof SimpleXMLElement) {
                echo '<pre>';
                echo htmlentities(print_r($data->asXML(), true));
                echo '</pre>';
            } else {
                echo nl2br(print_r($data, true));
            }
        }

        if (isset($data->Error->Code)) {
            if ($this->_debug) {
                printf("GetFulfillmentOrder()  $this->_att some error in the request answer... $this->_cr");
            }
        } elseif (!isset($data->ResponseMetadata->RequestId)) {
            if ($this->_debug) {
                printf("GetFulfillmentOrder()  $this->_att some other error in the request answer... $this->_cr");
            }

            return false;
        }

        return ($data);
    }*/

    public function UpdateFulfillmentOrder($order, $returnXML = false)  {
	
        $params = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['Action'] = 'UpdateFulfillmentOrder';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';

        // Testing all required fields are present
        $pass = true;
        foreach (array(
                     'SellerFulfillmentOrderId',
                     'DisplayableOrderId',
                     'DisplayableOrderComment'
                 ) as $field) {
            if (!isset($order[$field]) || empty($order[$field]) || (is_array($order[$field]) && !count($order[$field]))) {
                $pass = false;
                if ($this->_debug) {
                    printf('UpdateFulfillmentOrder() function. %sError. One of the required parameters is missing: %s.%s',
                        $this->_att, $field, $this->_cr);
                }
            }
        }
        if (!$pass) {
            return (false);
        }

        $params['SellerFulfillmentOrderId'] = mb_substr($order['SellerFulfillmentOrderId'], 0, 40);
        $params['DisplayableOrderId'] = trim(mb_substr($order['DisplayableOrderId'], 0, 40));
        $params['DisplayableOrderComment'] = mb_substr($order['DisplayableOrderComment'], 0, 1000);
	
	if ($this->_debug) {
	    echo '<br/> Amazon_WebService -> UpdateFulfillmentOrder() <br/>  <b>Params - :</b> <pre>' . print_r($params, true);
	}
	
        $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);

        if ($returnXML) {
            //
            return ($data);
        } else {
            if ($data === false) {
                if ($this->_debug) {
                    printf("$this->_cr $this->_att An error happened during UpdateFulfillmentOrder request $this->_cr");
                }

                return false;
            }

            if ($this->_debug) {
                if ($data instanceof SimpleXMLElement) {
                    echo '<pre>';
                    echo htmlentities(print_r($data->asXML(), true));
                    echo '</pre>';
                } else {
                    echo nl2br(print_r($data, true));
                }
            }

            if (!isset($data->ResponseMetadata->RequestId)) {
                if ($this->_debug) {
                    printf("UpdateFulfillmentOrder()  $this->_att some error in the request answer... $this->_cr");
                }

                return false;
            }

            return ((string)$data->ResponseMetadata->RequestId);
        }
    }
    
    public function CreateFulfillmentOrder($order, $returnXML = false)  {
	
        $params = $fields = array();
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['Action'] = 'CreateFulfillmentOrder';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['Version'] = '2010-10-01';

        $params['MarketplaceID'] = $this->mpid;
	
        // Testing all required fields are present
        $pass = true;
        foreach (array(
                     'SellerFulfillmentOrderId',
                     'DisplayableOrderId',
                     'DisplayableOrderDateTime',
                     'DisplayableOrderComment',
                     'ShippingSpeedCategory',
                     'DestinationAddress',
                     'Items'
                 ) as $field) {
            if (!isset($order[$field]) || empty($order[$field]) || (is_array($order[$field]) && !count($order[$field]))) {
                $pass = false;
		$fields[] = $field;
                if ($this->_debug) {
                    printf('CreateFulfillmentOrder() function. %sError. One of the required parameters is missing: %s.%s',
                        $this->_att, $field, $this->_cr);
                }
            }
        }
        if (!$pass) {
            return array('pass'=>$pass, 'output' =>  'One of the required parameters is missing: ' . implode(', ', $fields) );
        }

        $params['SellerFulfillmentOrderId'] = mb_substr($order['SellerFulfillmentOrderId'], 0, 40);
        $params['DisplayableOrderId'] = trim(mb_substr($order['DisplayableOrderId'], 0, 40));
        $params['DisplayableOrderDateTime'] = $order['DisplayableOrderDateTime'];
        $params['DisplayableOrderComment'] = mb_substr($order['DisplayableOrderComment'], 0, 1000);
        $params['ShippingSpeedCategory'] = $order['ShippingSpeedCategory'];        

        // Emails
        if (isset($order['NotificationEmailList']) && is_array($order['NotificationEmailList']) && count($order['NotificationEmailList'])) {
            $count = 1;
            foreach ($order['NotificationEmailList'] as $key => $email) {
                if (function_exists('filter_var') && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    continue;
                }

                $params['NotificationEmailList.member.'.$count] = substr($email, 0, 64);
                $count++;
            }
        }

        // Format Address :
        //
        $pass = true;
        foreach (array('Name', 'Line1', 'City', 'StateOrProvinceCode', 'CountryCode') as $field) {
            if (!isset($order['DestinationAddress'][$field]) || empty($order['DestinationAddress'][$field])) {
                $pass = false;
		$fields[] = $field;
                if ($this->_debug) {
                    printf('CreateFulfillmentOrder() function. %sError. One of the Address required parameters is missing: %s.%s',
                        $this->_att, $field, $this->_cr);
                }
            }
        }
        if (!$pass) {
	    return array('pass'=>$pass, 'output' =>  'One of the Address required parameters is missing: ' . implode(', ', $fields) );
            //return (false);
        }
	
        $params['DestinationAddress.Name'] = mb_substr($order['DestinationAddress']['Name'], 0, 50);
        $params['DestinationAddress.Line1'] = mb_substr($order['DestinationAddress']['Line1'], 0, 50);

        if (isset($order['DestinationAddress']['Line2']) && !empty($order['DestinationAddress']['Line2'])) {
            $params['DestinationAddress.Line2'] = mb_substr($order['DestinationAddress']['Line2'], 0, 60);
        }

        if (isset($order['DestinationAddress']['Line3']) && !empty($order['DestinationAddress']['Line3'])) {
            $params['DestinationAddress.Line3'] = mb_substr($order['DestinationAddress']['Line3'], 0, 60);
        }

        if (isset($order['DestinationAddress']['DistrictOrCounty']) && !empty($order['DestinationAddress']['DistrictOrCounty'])) {
            $params['DestinationAddress.DistrictOrCounty'] = mb_substr($order['DestinationAddress']['DistrictOrCounty'],
                0, 150);
        }

        if (isset($order['DestinationAddress']['City']) && !empty($order['DestinationAddress']['City'])) {
            $params['DestinationAddress.City'] = mb_substr($order['DestinationAddress']['City'], 0, 50);
        }

        if (isset($order['DestinationAddress']['StateOrProvinceCode']) && !empty($order['DestinationAddress']['StateOrProvinceCode'])) {
            $params['DestinationAddress.StateOrProvinceCode'] = mb_substr($order['DestinationAddress']['StateOrProvinceCode'],
                0, 50);
        }

        if (isset($order['DestinationAddress']['CountryCode']) && !empty($order['DestinationAddress']['CountryCode'])) {
            $params['DestinationAddress.CountryCode'] = strtoupper(mb_substr($order['DestinationAddress']['CountryCode'],
                0, 2));
        }

        if (isset($order['DestinationAddress']['PostalCode']) && !empty($order['DestinationAddress']['PostalCode'])) {
            $params['DestinationAddress.PostalCode'] = mb_substr($order['DestinationAddress']['PostalCode'], 0, 20);
        }

        if (isset($order['DestinationAddress']['PhoneNumber']) && !empty($order['DestinationAddress']['PhoneNumber'])) {
            $params['DestinationAddress.PhoneNumber'] = mb_substr($order['DestinationAddress']['PhoneNumber'], 0, 20);
        }
	
        if (!is_array($order['Items']) || !count($order['Items'])) {
	    $pass = false;
            if ($this->_debug) {
                printf('CreateFulfillmentOrder() function. %sWarning. Items: empty list - nothing to do.%s',
                    $this->_att, $this->_cr);
            }
	    return array('pass'=>$pass, 'output' =>  'Items: empty list - nothing to do. ');
            //return (false);
        }
	
        $itemCount = 0;
        foreach ($order['Items'] as $Item) {
            if (!$this->checkSKU($Item['SKU'])) {
		if ($this->_debug) {
		    printf('CreateFulfillmentOrder() function. Warning. SKU: invalid value. Skipping the product and continue.%s',
			$this->_cr);
		}
                continue;
            }

            // Check for required field
            //
            $pass = true;
            foreach (array('SellerFulfillmentOrderItemId', 'Quantity') as $field) {
                if (!isset($Item[$field]) || empty($Item[$field])) {
                    $pass = false;
		    if ($this->_debug) {
			printf('CreateFulfillmentOrder() function. %sError. One of the Item required parameters is missing: %s.%s',
			    $this->_att, $field, $this->_cr);
		    }
                }
            }
            if (!$pass) {
                continue;
            }

            $itemCount++;

            $params['Items.member.'.$itemCount.'.SellerSKU'] = $Item['SKU'];
            $params['Items.member.'.$itemCount.'.SellerFulfillmentOrderItemId'] = $Item['SellerFulfillmentOrderItemId'];
            $params['Items.member.'.$itemCount.'.Quantity'] = $Item['Quantity'];

            if (isset($Item['PerUnitDeclaredValue.Value']) && !empty($Item['PerUnitDeclaredValue.Value'])) {
                $params['Items.member.'.$itemCount.'.PerUnitDeclaredValue.Value'] = $Item['PerUnitDeclaredValue.Value'];
                $params['Items.member.'.$itemCount.'.PerUnitDeclaredValue.CurrencyCode'] = $Item['PerUnitDeclaredValue.CurrencyCode'];
            }
            if (isset($Item['GiftMessage']) && !empty($Item['GiftMessage'])) {
                $params['Items.member.'.$itemCount.'.GiftMessage'] = mb_substr($Item['GiftMessage'], 0, 512);
            }

            if (isset($Item['DisplayableComment']) && !empty($Item['DisplayableComment'])) {
                $params['Items.member.'.$itemCount.'.DisplayableComment'] = mb_substr($Item['DisplayableComment'], 0,
                    250);
            }

            if (isset($Item['FulfillmentNetworkSKU']) && !empty($Item['FulfillmentNetworkSKU'])) {
                $params['Items.member.'.$itemCount.'.FulfillmentNetworkSKU'] = $Item['FulfillmentNetworkSKU'];
            }

            if (isset($Item['OrderItemDisposition']) && !empty($Item['OrderItemDisposition'])) {
                $params['Items.member.'.$itemCount.'.OrderItemDisposition'] = $Item['OrderItemDisposition'];
            }
        }

        if (count($order['Items']) != $itemCount) {
	    $pass = false;
	    if ($this->_debug) {
		printf('CreateFulfillmentOrder() function. %sError. Items Count: Expected item count differ, order aborted.%s',
		    $this->_att, $this->_cr);
	    }
	    return array('pass'=>$pass, 'output' => 'Items Count: Expected item count differ, order aborted. ');
            //return (false);
        }
	
        $data = $this->_callWSs('FulfillmentOutboundShipment', $params, null, true);
	
	/*Test put content file*/
	//$test_path = dirname(__FILE__) . '/../../../../assets/apps/users/' . $this->user_name . '/amazon/';
	//file_put_contents($test_path.'CreateFulfillmentOrder.txt', print_r($data->asXML(), true));
	//file_put_contents($test_path.'CreateFulfillmentOrder.xml', $data->asXML());
	/*Test put content file*/
	
        if ($returnXML) {   
	    
            return array('pass'=>$pass, 'output' => $data);
	    
        } else {
            if ($data === false) {
		$pass = false;
                if ($this->_debug) {
                    printf("$this->_cr $this->_att An error happened during CreateFulfillmentOrder request $this->_cr");
                }
		return array('pass'=>$pass, 'output' =>  'An error happened during Create Fulfillment Order request. ');
                //return false;
            }

            if ($this->_debug) {
                if ($data instanceof SimpleXMLElement) {
                    echo '<pre>';
                    echo htmlentities(print_r($data->asXML(), true));
                    echo '</pre>';
                } else {
                    echo nl2br(print_r($data, true));
                }
            }

            if (!isset($data->ResponseMetadata->RequestId)) {
		$pass = false;
                if ($this->_debug) {
                    printf("CreateFulfillmentOrder()  $this->_att some error in the request answer... $this->_cr");
                }
		return array('pass'=>$pass, 'output' =>  'Create Fulfillment Order, some error in the request answer.... ');
                //return false;
            }

            return  array('pass'=>$pass, 'output' => (string)$data->ResponseMetadata->RequestId);
	    //((string)$data->ResponseMetadata->RequestId);
        }
    }
    
    
    private function checkSKU($SKU)
    {
        return ($SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU));
    }
    
    public function ListInventoryBySKU($SKUs)  {
       
        $params = array();

        if (!is_array($SKUs) || !count($SKUs)) {
            if ($this->_debug) {
                printf('ListInventorySKU() function. %sWarning. SKU: empty list - nothing to do.%s', $this->_att, $this->_cr);
            }

            return (false);
        }

        $count = 1;

        foreach ($SKUs as $SKU) {
            if (!$this->checkSKU($SKU)) {
                if ($this->_debug) {
                    printf('ListInventorySKU() function. Warning. SKU: "%s" invalid value. Skipping the product and continue.%s',
                        $SKU, $this->_cr);
                }
                continue;
            }

            $params['SellerSkus.member.'.$count] = (string)$SKU;
            $count++;

            if ($count > 50) {
                if ($this->_debug) {
                    printf('ListInventorySKU() function. Warning. This function is restricted to 50 items.%s',
                        $this->_cr);
                }
                break;
            }
        }

        if (!($datasets = $this->_ListInventorySupply($params))) {
            if ($this->_debug) {
                printf('ListInventoryBySKU() function. _ListInventorySupply failed.%s', $this->_cr);
            }
        }
        if ($this->_debug) {
            printf('ListInventoryBySKU() function. _ListInventorySupply returned %d dataset.%s', count($datasets),
                $this->_cr);
        }

        $result = array();
        foreach ($datasets as $dataset) {
            foreach ($dataset as $inventoryItem) {
                $result[(string)$inventoryItem->SellerSKU] = array();
                $result[(string)$inventoryItem->SellerSKU]['SKU'] = (string)$inventoryItem->SellerSKU;
                $result[(string)$inventoryItem->SellerSKU]['InStockSupplyQuantity'] = (int)$inventoryItem->InStockSupplyQuantity;
                $result[(string)$inventoryItem->SellerSKU]['TotalSupplyQuantity'] = (int)$inventoryItem->TotalSupplyQuantity;
            }
        }

        return ($result);
    }

    private function _ListInventorySupply($params, $maxQueries = 100) {
	
        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['ResponseGroup'] = 'Basic';
        $params['Version'] = '2011-03-01';

        $dataset = array();
        $index = 0;
        $nextToken = 1;

        for ($i = 0; $i < $maxQueries && $nextToken; $i++) {
            if ($index == 0) {
                $params['Action'] = 'ListInventorySupply';
                $data = $this->_callWSs('FulfillmentInventory', $params);
                $responseItem = 'ListInventorySupplyResult';
            } else {
                $params['Action'] = 'ListInventorySupplyByNextToken';
                $params['NextToken'] = $nextToken;
                $data = $this->_callWSs('FulfillmentInventory', $params);
                $responseItem = 'ListInventorySupplyByNextTokenResult';
            }
            if ($data === false) {
                if ($this->_debug) {
                    printf("$this->_cr $this->_att An error happened during ListInventorySupply request $this->_cr");
                }

                return false;
            }

            if (isset($data->{$responseItem}->NextToken) && $data->{$responseItem}->NextToken) {
                $nextToken = $data->{$responseItem}->NextToken;
            } else {
                $nextToken = null;
            }

            if (!isset($data->{$responseItem})) {
                if ($this->_debug) {
                    printf("ListInventorySupply() function failed $this->_att An error happened during request... $this->_cr");
                }

                return false;
            }

            if ($this->_debug) {
                if ($data instanceof SimpleXMLElement) {
                    echo '<pre>';
                    echo htmlentities(print_r($data->asXML(), true));
                    echo '</pre>';
                } else {
                    echo nl2br(print_r($data, true));
                }
            }

            if (!isset($data->{$responseItem})) {
                return false;
            }

            if (!isset($data->{$responseItem}->InventorySupplyList->member)) {
                if ($this->_debug) {
                    printf("ListInventorySupply()  $this->_att empty inventory... $this->_cr");
                }

                return false;
            }
            $dataset[$index++] = $data->{$responseItem}->InventorySupplyList->member;
        }

        return ($dataset);
    }
    
    
    public function ListInventoryByDate($Date) {
     
	$params = array();
        $querytime = strtotime($Date);
        $now = time();
        $last_month = time() - (86400 * 60);

        if ($querytime > $now || $querytime < $last_month) {
            if ($this->_debug) {
                printf('ListInventoryByDate() function. %sWarning. Wrong Date.%s', $this->_att, $Date, $this->_cr);
            }
            return (false);
        }

        $params['QueryStartDateTime'] = gmdate('Y-m-d\T00:00:00\Z', $querytime);

        if ($this->_debug) {
            printf('ListInventoryByDate() function. _ListInventorySupply params: %s.%s', $this->_cr, print_r($params));
        }

        if (!($datasets = $this->_ListInventorySupply($params))) {
            if ($this->_debug) {
                printf('ListInventoryByDate() function. _ListInventorySupply failed.%s', $this->_cr);
            }
        }
        if ($this->_debug) {
            printf('ListInventoryByDate() function. _ListInventorySupply returned %d dataset.%s', count($datasets),
                $this->_cr);
        }

        $result = array();
        if (is_array($datasets)) {
            foreach ($datasets as $dataset) {
                foreach ($dataset as $inventoryItem) {
                    $result[(string)$inventoryItem->SellerSKU] = array();
                    $result[(string)$inventoryItem->SellerSKU]['SKU'] = (string)$inventoryItem->SellerSKU;
                    $result[(string)$inventoryItem->SellerSKU]['InStockSupplyQuantity'] = (int)$inventoryItem->InStockSupplyQuantity;
                    $result[(string)$inventoryItem->SellerSKU]['TotalSupplyQuantity'] = (int)$inventoryItem->TotalSupplyQuantity;
                }
            }
        }

        return ($result);
    }

    /*public function GetCanceledOrdersList($createdAfterDate, $createdBeforeDate = null, $FBA = false, $returnXML = true) {
        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList call. $this->_cr");
        }

        $params = array();
        $params['Action'] = 'ListOrders';
        $params['CreatedAfter'] = $createdAfterDate;

        if ($createdBeforeDate != null) {
            $params['CreatedBefore'] = $createdBeforeDate;
        }

        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['MarketplaceId.Id.1'] = $this->mpid;

        // Amazon Europe
        //
        if (isset($this->MarketPlaces) && count($this->MarketPlaces)) {
            $i = 1;
            foreach ($this->MarketPlaces as $marketPlace) {
                if (empty($marketPlace)) {
                    continue;
                }
                if ($marketPlace == $this->mpid) {
                    continue;
                }

                $i++;
                $params['MarketplaceId.Id.'.$i] = $marketPlace;
            }
        }

        $params['OrderStatus.Status.1'] = 'Canceled';
        $params['MaxResultsPerPage'] = 100;

        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList function. Start request $this->_cr");
        }

        $data = $this->_callWSs('Orders', $params, null, $returnXML);

        if ($this->_debug) {
            if (is_object($data)) {
                echo nl2br(print_r($data->asXML(), true));
            } else {
                echo nl2br(print_r($data, true));
            }
        }

        if (isset($data->Error) && $returnXML) {
            return ($data);
        }

        if (isset($data->ListOrdersResult->NextToken)) {
            $nextToken = $data->ListOrdersResult->NextToken;
        } else {
            $nextToken = null;
        }

        if (isset($data->ListOrdersResult)) {
            $data = $data->ListOrdersResult->Orders;
        }

        if ($data === null) {
            return false;
        }// no orders available

        $Orders = array();
        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList function(). Start creating orders class instances. $this->_cr");
        }

        for ($i = 0; $i > -1; $i++) {
            if (!isset($data->Order)) {
                if ($this->_debug == true) {
                    printf("$this->_cr GetUnshippedOrdersList - Error or No Pending Order");
                }
                break;
            }
            if ($data->Order[$i] === null) {
                if ($i == 0) {
                    return false;
                } // no orders available
                break; //orders ended
            }

            $Orders[$i] = new PlacedOrder($data->Order[$i], null, $this->_debug);
        }

        while ($nextToken) {
            $nextToken = $this->GetOrdersByNextToken($nextToken, $Orders);
        }

        return $Orders;
    }

    public function GetUnshippedOrdersList($createdAfterDate, $createdBeforeDate = null, $Status = 'All', $FBA = false, $returnXML = false ) {
        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList call. $this->_cr");
        }

        $params = array();
        $params['Action'] = 'ListOrders';
        $params['CreatedAfter'] = $createdAfterDate;

        if ($createdBeforeDate != null) {
            $params['CreatedBefore'] = $createdBeforeDate;
        }

        $params['SellerId'] = $this->mid;
        $params['SignatureVersion'] = '2';
        $params['SignatureMethod'] = 'HmacSHA256';
        $params['MarketplaceId.Id.1'] = $this->mpid;

        // Amazon Europe
        //
        if (isset($this->MarketPlaces) && count($this->MarketPlaces)) {
            $i = 1;
            foreach ($this->MarketPlaces as $marketPlace) {
                if (empty($marketPlace)) {
                    continue;
                }
                if ($marketPlace == $this->mpid) {
                    continue;
                }

                $i++;
                $params['MarketplaceId.Id.'.$i] = $marketPlace;
            }
        }

        if ($FBA) {
            $params['OrderStatus.Status.1'] = 'Unshipped';
            $params['OrderStatus.Status.2'] = 'PartiallyShipped';
            $params['OrderStatus.Status.3'] = 'Shipped';
        } elseif ($Status == 'Shipped') {
            //
            $params['OrderStatus.Status.1'] = 'Shipped';
        } elseif ($Status == 'Unshipped' || $Status == 'PartiallyShipped') {
            $params['OrderStatus.Status.1'] = 'Unshipped';
            $params['OrderStatus.Status.2'] = 'PartiallyShipped';
        } else {
            if ($Status != 'All') {
                $params['OrderStatus.Status.1'] = $Status;
            }
        }

        $params['MaxResultsPerPage'] = 50;

        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList function. Start request $this->_cr");
        }

        $data = $this->_callWSs('Orders', $params, null, $returnXML);

        if ($this->_debug) {
            if (is_object($data)) {
                echo nl2br(print_r($data->asXML(), true));
            } else {
                echo nl2br(print_r($data, true));
            }
        }

        if (isset($data->Error) && $returnXML) {
            return ($data);
        }

        if (isset($data->ListOrdersResult->NextToken)) {
            $nextToken = $data->ListOrdersResult->NextToken;
        } else {
            $nextToken = null;
        }

        if (isset($data->ListOrdersResult)) {
            $data = $data->ListOrdersResult->Orders;
        }

        if ($data === null) {
            return false;
        }// no orders available

        $Orders = array();
        if ($this->_debug == true) {
            printf("$this->_cr GetUnshippedOrdersList function(). Start creating orders class instances. $this->_cr");
        }

        for ($i = 0; $i > -1; $i++) {
            if (!isset($data->Order)) {
                if ($this->_debug == true) {
                    printf("$this->_cr GetUnshippedOrdersList - Error or No Pending Order");
                }
                break;
            }
            if ($data->Order[$i] === null) {
                if ($i == 0) {
                    return false;
                } // no orders available
                break; //orders ended
            }

            $Orders[$i] = new PlacedOrder($data->Order[$i], null, $this->_debug);
        }

        while ($nextToken) {
            $nextToken = $this->GetOrdersByNextToken($nextToken, $Orders);
        }

        return $Orders;
    }

    public function confirmOrderBase($amazonOrderId)  {
        return $this->confirmOrder($amazonOrderId, null, null, null);
    }

    public function confirmMultipleOrders($amazonShippings) {
        if ($this->_debug == true) {
            printf("$this->_cr confirmMultipleOrders call. $this->_cr");
        }

        $Document = new DOMDocument();
        $Messages = array();

        if ($this->_debug == true) {
            printf("$this->_cr confirmMultipleOrders function. Create messages here $this->_cr");
        }

        $m = 0;
        if (is_array($amazonShippings) && count($amazonShippings)) {
            foreach ($amazonShippings as $amazonShipping) {
                if (!isset($amazonShipping['order_id']) || empty($amazonShipping['order_id'])) {
                    if ($this->_debug == true) {
                        printf("$this->_cr Missing order_id: %s $this->_cr", print_r($amazonShipping, true));
                    }
                    continue;
                }

                if ((!isset($amazonShipping['carrier']) || empty($amazonShipping['carrier'])) && (!isset($amazonShipping['carrier_name']) || empty($amazonShipping['carrier_name']))) {
                    if ($this->_debug == true) {
                        printf("$this->_cr Missing carrier info: %s $this->_cr", print_r($amazonShipping, true));
                    }
                    continue;
                }

                $Messages[$m] = $this->createOrderFulfillmentMessage($Document, $amazonShipping['order_id'],
                    $amazonShipping['carrier'], $amazonShipping['carrier_name'], null,
                    $amazonShipping['shipping_number'], $m + 1, $amazonShipping['timestamp']);
                $m++;
            }
        }
        if (!$m) {
            if ($this->_debug == true) {
                printf("$this->_cr confirmMultipleOrders function. No Message to Send, returning to main function $this->_cr");
            }

            return (false);
        }
        $feedDOM = $this->CreateFeed($Document, 'OrderFulfillment', $Messages);

        if ($this->_debug) {
            $feedDOM->formatOutput = true;
        }

        $feed = $feedDOM->saveXML();

        if ($this->_debug) {
            echo nl2br(str_replace(' ', '&nbsp;', htmlentities($feed)));

            printf('Complete Feed is: %s'.$this->_cr, $feed);
        }

        if ($this->_debug == true) {
            printf("$this->_cr confirmMultipleOrders function. Now we send following query to WebService:  $this->_cr $feed $this->_cr");
        }

        $data = $this->processFeed('_POST_ORDER_FULFILLMENT_DATA_', $feed);

        if ($data === false && $this->_debug == true) {
            //
            printf("$this->_cr confirmMultipleOrders function finished with an error because of the prblem with query sending.. $this->_cr");
        } else {
            if ($this->_debug == true) {
                printf("$this->_cr confirmMultipleOrders function finished successfuly $this->_cr");
            }
        }

        return $data;
    }

    public function cancelOrder($Order){
        $Orders = array();
        $Orders[0] = $Order;

        return $this->cancelOrders($Orders);
    }

    public function cancelOrders($Orders) {
        if ($this->_debug == true) {
            printf("$this->_cr cancelOrders() call. Creating Messages. $this->_cr");
        }

        $Document = new DOMDocument();
        $count_orders = count($Orders);
        for ($i = 0; $i < $count_orders; $i++) {
            if ($this->_debug == true) {
                $m = $i + 1;
                $oid = $Orders[$i]->AmazonOrderId;
                printf("$this->_cr cancelOrders() call. Creating Message $m for OrderID $oid. $this->_cr");
            }
            $mess = $this->createOrderAcknowledgementMessage($Document, $Orders[$i]->AmazonOrderId, $i + 1);
        }

        $Messages = array();
        $Messages[0] = $mess;
        $feedDOM = $this->CreateFeed($Document, 'OrderAcknowledgement', $Messages);
        $feed = $feedDOM->saveXML();

        if ($this->_debug == true) {
            printf("$this->_cr cancelOrders() function. Now function creates the following feed: $this->_cr $feed $this->_cr");
        }

        $data = $this->processFeed('_POST_ORDER_ACKNOWLEDGEMENT_DATA_', $feed);

        if ($data == false && $this->_debug == true) {
            printf("$this->_cr cancelOrders() function finished with an error because of the prblem with query sending.. $this->_cr");
        } elseif ($this->_debug == true) {
            printf("$this->_cr cancelOrders() function finished successfuly $this->_cr");
        }

        return $data;
    }*/

    /*private function createOrderAcknowledgementMessage(DOMDocument $Document, $OrderID, $messageID, $merchand_order_id) {
        $message = $Document->createElement('Message');
        $messageIDX = $Document->createElement('MessageID');
        $message->appendChild($messageIDX);
        $messageIDText = $Document->createTextNode($messageID);
        $messageIDX->appendChild($messageIDText);
        $OrderAcknowledgement = $Document->createElement('OrderAcknowledgement');
        $message->appendChild($OrderAcknowledgement);

        $AmazonOrderID = $Document->createElement('AmazonOrderID');
        $OrderAcknowledgement->appendChild($AmazonOrderID);
        $AmazonOrderIDText = $Document->createTextNode($OrderID);
        $AmazonOrderID->appendChild($AmazonOrderIDText);

        $MerchandOrderID = $Document->createElement('MerchantOrderID');
        $OrderAcknowledgement->appendChild($MerchandOrderID);
        $MerchandOrderIDText = $Document->createTextNode($merchand_order_id);
        $MerchandOrderID->appendChild($MerchandOrderIDText);

        $StatusCode = $Document->createElement('StatusCode');
        $OrderAcknowledgement->appendChild($StatusCode);
        $StatusCodeText = $Document->createTextNode('Success');
        $StatusCode->appendChild($StatusCodeText);

        return $message;
    }

    public function cancelOrderByID($OrderID) {
        $OrderIDs = array();
        $OrderIDs[0] = $OrderID;

        return $this->cancelOrdersByIDs($OrderIDs);
    }

    public function cancelOrdersByIDs($OrderIDs) {
        if ($this->_debug == true) {
            printf("$this->_cr cancelOrders() call. Creating Messages. $this->_cr");
        }

        $Document = new DOMDocument();
        $count_orderids = count($OrderIDs);
        for ($i = 0; $i < $count_orderids; $i++) {
            if ($this->_debug == true) {
                $m = $i + 1;
                printf("$this->_cr cancelOrders() call. Creating Message $m for OrderID $OrderIDs[$i]. $this->_cr");
            }
            $mess = $this->createOrderAcknowledgementMessage($Document, $OrderIDs[$i], $i + 1);
        }

        $Messages = array();
        $Messages[0] = $mess;
        $feedDOM = $this->CreateFeed($Document, 'OrderAcknowledgement', $Messages);
        $feed = $feedDOM->saveXML();

        if ($this->_debug == true) {
            printf("$this->_cr cancelOrders() function. Now function creates the following feed: $this->_cr $feed $this->_cr");
        }

        $data = $this->processFeed('_POST_ORDER_ACKNOWLEDGEMENT_DATA_', $feed);

        if ($data == false && $this->_debug == true) {
            printf("$this->_cr cancelOrders() function finished with an error because of the prblem with query sending.. $this->_cr");
        } elseif ($this->_debug == true) {
            printf("$this->_cr cancelOrders() function finished successfuly $this->_cr");
        }

        return $data;
    }*/
    
}