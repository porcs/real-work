<?php

class AmazonDispatcher{
        /**
	 * @var Dispatcher
	 */
	private static $instance = null;
        private $default_controller = 'Amazon';
        private $controller;
	
        public function __construct() {
            $this->controllers_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR;
        }
        /**
	 * Get current instance of dispatcher (singleton)
	 *
	 * @return Dispatcher
	 */
	public static function getInstance()
	{
                if(!defined('_AMAZON_ROOR_DIR_'))
                    die;
		if (!self::$instance)
			self::$instance = new AmazonDispatcher();
		return self::$instance;
	}
        
        
	/**
	 * Get list of all available FO controllers
	 *
	 * @var mixed $dirs
	 * @return array
	 */
	public static function getControllers()
	{	
                $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR;
		return self::getControllersInDirectory($dir);
	}
        
        /**
	 * Retrieve the controller from url or request uri if routes are activated
	 *
	 * @return string
	 */
	public function getController()
	{		
		if ($this->controller)
		{
			$_GET['controller'] = $this->controller;
			return $this->controller;
		}
	
		$controller = Amazon_Tools::getValue('controller');
	
		if (isset($controller) && is_string($controller) && preg_match('/^([0-9a-z_-]+)\?(.*)=(.*)$/Ui', $controller, $m))
		{
			$controller = $m[1];
			if (isset($_GET['controller']))
				$_GET[$m[2]] = $m[3];
			else if (isset($_POST['controller']))
				$_POST[$m[2]] = $m[3];
		}
                $this->controller = $controller;

		$this->controller = str_replace('-', '', $this->controller);
		$_GET['controller'] = $this->controller;
		return $this->controller;
	}
        
        /**
	 * Get list of available controllers from the specified dir
	 *
	 * @param string dir directory to scan (recursively)
	 * @return array
	 */
	public static function getControllersInDirectory($dir)
	{
		if (!is_dir($dir))
			return array();

		$controllers = array();
		$controller_files = scandir($dir);
		foreach ($controller_files as $controller_filename)
		{
			if ($controller_filename[0] != '.')
			{
				if (!strpos($controller_filename, '.php') && is_dir($dir.$controller_filename))
					$controllers += self::getControllersInDirectory($dir.$controller_filename.DIRECTORY_SEPARATOR);
				elseif ($controller_filename != 'index.php')
				{
					$key = str_replace(array('controller.php', '.php'), '', strtolower($controller_filename));
					$controllers[$key] = basename($controller_filename, '.php'); 
				}
			}
		}

		return $controllers;
	}
        
        
	/**
	 * Find the controller and instantiate it
	 */
	public function dispatch()
	{
		$controllers = self::getControllers();
                $controllers_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR;
		// Get current controller
		$this->getController();
                
		if (!$this->controller)
			$this->controller = $this->default_controller;
                
                if (!isset($controllers[strtolower($this->controller)]))
                    $this->controller = $this->default_controller;
                
                $controller_class = $controllers[strtolower($this->controller)];
                
		// Instantiate controller
		try
		{
                        require_once($controllers_dir . $controller_class . '.php');
			// Loading controller
			$controller = new $controller_class;

			// Running controller
			$controller->display();
		}
		catch (Exception $e)
		{
			$e->getMessage();
		}
	}
        
    
}
?>
