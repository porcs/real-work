<?php

require_once(dirname(__FILE__) . '/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__) . '/amazon.tools.php');
require_once(dirname(__FILE__) . '/../controllers/amazon.display.carrier.php');

class UpdateValidValues {

    const TABLE = 'amazon_valid_values';
    const TABLE_CUSTOM = 'amazon_valid_values_custom';
    const FILE_GZ = 'amazon_valid_values.sql.gz';
    const FILE_SQL = 'amazon_valid_values.sql';
    const FILE_MD5 = 'amazon_valid_values.md5';
    const SOURCE_URL = 'https://s3-us-west-2.amazonaws.com/common-services-public/amazon/'; // Public directory on DropBox
    
    public $db;
    public $import;
    public $error;
    public $success;
    private $productTypeException;
    private $productSubTypeException;

    public function __construct() {
        
        $this->db = new ci_db_connect();
        $this->createLogTable();

        $this->productSubTypeException = array(
            'AutoAccessory' => array(
                'Rims|Wheels' => 'Rims',
            ),
            'Computers' => array(
                '(PC|PersonalComputer)' => 'PersonalComputer',
            ),
            'CE' => array(
                '(MediaPlayerOrEReaderAccessory|AccessoryOrPartOrSupply)' => 'MediaPlayerOrEReaderAccessory',
                '(MiscAudioComponents|AudioOrVideo)' => 'MiscAudioComponents',
                '(RadioOrClockRadio|Radio)' => 'RadioOrClockRadio',
            ),
        );

        $this->productTypeException = array(
            'AutoAccessory' => array(
                'InnerMaterialType' => 'InnerMaterial',
                'OuterMaterialType' => 'OuterMaterial',
                'MaterialType' => 'Material',
                'DepartmentName' => 'Department',
                'Rims|Wheels' => 'Rims',
            ),
            'Beauty' => array(
                'Specialty' => 'ItemSpecialty',
                'ScentName' => 'Scent'
            ),
            'Computers' => array(
                'MaximumScanningSize' => 'MaxScanningSize',
                'MinimumScanningSize' => 'MinScanningSize',
                'MediaSizeMaximum' => 'PrinterMediaSizeMaximum',
                'PrinterOutput' => 'PrinterOutputType',
                'PowerSourceType' => 'PowerSource',
                'BatteryType' => 'Battery',
                'GraphicsRamType' => 'GraphicsRAMType',
                'OpticalStorageDevice' => 'OpticalStorageDeviceType',
                'Format' => 'DigitalMediaFormat',
                'SurroundSoundChannelConfiguration' => 'SpeakerSurroundSoundChannelConfiguration',
            ),
            'CE' => array(
                'MediaTypeBase' => 'MediaType',
                'PowerSourceType' => 'PowerSource',
                'WirelessCommunicationTechnology' => 'WirelessTechnology',
                '3dTechnology' => 'ThreeDTechnology',
                'ScreenSurfaceDescription' => 'ScreenFinish',
                'SpeakerGrilleDescription' => 'GrilleRemoveability',
                'SpeakerGrilleMaterialType' => 'SpeakerGrilleMaterial',
                'ItemShape' => 'Shape',
                'ExteriorFinish' => 'EnclosureFinish',
                'TrafficFeaturesDescription' => 'TrafficFeatures',
            ),
            'EntertainmentCollectibles' => array(
                'StyleName' => 'EntertainmentType',
                'Originality' => 'CollectibleType',
            ),
            'FoodAndBeverages' => array(
                'FlavorName' => 'Flavor',
                'packageTypeName' => 'ContainerType',
                'Specialty' => 'ItemSpecialty',
                'UnitCountType' => 'UnitCount',
            ),
            'Health' => array(
                'Specialty' => 'ItemSpecialty'
            ),
            'Home' => array(
                'MaterialType' => 'Material',
                'ThermalPerformanceDescription' => 'FabricWarmthDescription',
                'ItemShape' => 'Shape',
                'BatteryType' => 'Battery',
                'FinishType' => 'Finish',
                'Ecole_lycée_et_université' => 'Ecole,_lycée_et_université',
                'Gorilles_singes_et_chimpanzés' => 'Gorilles,_singes_et_chimpanzés',
                'Léopards_lions_et_tigres' => 'Léopards,_lions_et_tigres',
                'Villes_magasins_et_restaurants' => 'Villes,_magasins_et_restaurants',
            ),
            'Industrial' => array('MaterialType' => 'Material'),
            'Jewelry' => array(
                'MaterialType' => 'Material',
                'CertificateType' => 'Certificate',
                'PearlSurfaceBlemishes' => 'PearlSurfaceMarkingsAndBlemishes',
                'StoneCreationMethod' => 'Stone'
            ),
            'LargeAppliances' => array(
                'HeatingElementType' => 'HeatingElements',
                'HeatingMethod' => 'HeatingMode',
            ),
            'Lighting' => array(
                'SpecificUsesForProduct' => 'SpecificUses',
                'IncludedComponents' => 'IncludedComponent',
                'ShadeMaterialType' => 'ShadeMaterial',
                'MaterialType' => 'Material',
                'LightSourceType' => 'BulbType',
                'PowerSourceType' => 'PowerSource',
                'LightSourceSpecialFeatures' => 'BulbSpecialFeatures',
                'BatteryType' => 'Battery',
            ),
            'Luggage' => array(
                'StyleName' => 'Style',
                'DepartmentName' => 'Department',
                'CollectionName' => 'Collection',
                'SubjectCharacter' => 'Character',
                'BatteryType' => 'Battery',
                'BatteryCellComposition' => 'BatteryComposition',
            ),
            'MusicalInstruments' => array(
                'BatteryType' => 'Battery',
                'BodyMaterialType' => 'BodyMaterial',
                'NeckMaterialType' => 'NeckMaterial',
                'SkillLevel' => 'ProficiencyLevel',
                'StringMaterialType' => 'StringMaterial',
                'TopMaterialType' => 'TopMaterial',
                'BackMaterialType' => 'BackMaterial',
                'FretboardMaterialType' => 'FretboardMaterial',
            ),
            'Office' => array(
                'ItemHardness' => 'PencilLeadHardness',
                'CoverMaterialType' => 'CoverMaterial',
                'TabCut' => 'TabCutType',
                'CornerStyle' => 'PaperCornerType',
                'MfgWarrantyDescriptionType' => 'ManufacturerWarrantyType',
            ),
            'PetSupplies' => array(
                'ExternalTestingCertification' => 'ExternalCertification',
                'BatteryType' => 'Battery',
                'Efficiency' => 'EnergyEfficiencyRating',
            ),
            'ProductClothing' => array(
                'DepartmentName' => 'Department',
                'InnerMaterialType' => 'InnerMaterial',
                'OuterMaterialType' => 'OuterMaterial',
            ),
            'Shoes' => array(
                'ClosureType' => 'ShoeClosureType',
                'InnerMaterialType' => 'InnerMaterial',
                'Lifestyle' => 'OccasionAndLifestyle',
                'Seasons' => 'Season',
                'DepartmentName' => 'Department',
                'ShaftDiameter' => 'ShaftDiameterDerived',
            ),
            'Sports' => array(
                'MaterialType' => 'Material',
                'Seasons' => 'Season',
                'SportType' => 'Sport',
                'DepartmentName' => 'Department',
                'GolfClubFlex' => 'GolfFlex',
                'BikeType' => 'MountainBikeType',
                'FrameMaterialType' => 'FrameMaterial',
                'AssemblyInstructions' => 'Buildup',
                'LensMaterialType' => 'LensMaterial',
                'BladeEdgeType' => 'BladeType',
                'HandOrientation' => 'Hand',
                'ShaftStyleType' => 'ShaftType',
                'StyleName' => 'JerseyType',
                'SpecificUsesForProduct' => 'SpecificUsageForProduct',
                'BatteryType' => 'Battery',
            ),
            'SWVG' => array(
                'EsrbAgeRating' => 'ESRBRating',
                'SystemRequirementsPlatform' => 'HardwarePlatform',
                'Format' => 'MediaFormat',
            ),
            'Toys' => array(
                'RecommendedUsesForProduct' => 'RecommendedUse',
                'LanguageValue' => 'Language',
                'BatteryType' => 'Battery',
            ),
            'ToysBaby' => array(
                'UnknownSubject' => 'Subject',
                'ScaleName' => 'Scale',
                'RecommendedUsesForProduct' => 'RecommendedUse',
                'LanguageValue' => 'Language',
                'BatteryType' => 'Battery',
            ),
            'Motorcycles' => array(
                'MaterialType' => 'Material'
            ),
	    'Sports' => array(
                'MaterialType' => 'Material',
                'SportType' => 'Sport',
		'StyleName' => 'JerseyType',
            ),
        );

        $this->SWVGException = array(
            'VideoGames' => 'ConsoleVideoGamesGenre',
            'SoftwareGames' => 'SoftwareVideoGamesGenre',
            'Software' => 'ChildrensSoftwareGenre',
        );

        $this->FieldException = array(
            'feed_product_type' => true,
            'external_product_id_type' => true,
            'update_delete' => true,
            'currency' => true,
            'offering_can_be_gift_messaged' => true,
            'offering_can_be_giftwrapped' => true,
            'is_discontinued_by_manufacturer' => true,
            'missing_keyset_reason' => true,
            'fulfillment_center_id' => true,
            'relationship_type' => true,
            'variation_theme' => true,
            'parent_child' => true,
            'condition_type' => true,
            'product_tax_code' => true,
            'eu_toys_safety_directive_age_warning' => true,
            'eu_toys_safety_directive_warning' => true,
        );

        $this->FileNameException = array(
            'Clothing' => "ProductClothing",
            'ConsumerElectronics' => "CE",
            'SWVG' => "SWVG",
            'SoftwareVideoGames' => "SWVG",
        );

        //unit_of_measure
        $this->MeasureException = 'unit_of_measure';
        $this->IssetException = 'is_';
    }
    
    public function DownloadFlatFile() {
        
        $error = '';
        $remote_url = 'https://s3.amazonaws.com/seller-templates/';
        $local_dir = dirname(__FILE__) . '/../valid_value/flat.file';
        
        $file_headers = @get_headers($remote_url);
        
        if(isset($file_headers[0]) && $file_headers[0] != 'HTTP/1.1 404 Not Found') {  
            
            //$contents = @file_get_contents($remote_url);  //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.
            $contents = Amazon_Tools::file_get_contents($remote_url);  //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.
            
            $content = new SimpleXMLElement($contents);
            
            if(isset($content)){
                foreach ($content as $ListBucketResult){
                    
                    foreach ($ListBucketResult as $fileKey => $fileContents){
                        
                        if(strval($fileKey) == 'Key') {
                            
                            $file = explode('/', strval($fileContents));
                            $region = isset($file[(sizeof($file) - 2)]) ? $file[(sizeof($file) - 2)] : null; // region size - 2
                            $filename = isset($file[(sizeof($file) - 1)]) ? $file[(sizeof($file) - 1)] : null; // filename size - 1
                            
                            if(isset($region) && !empty($region) && isset($filename) && !empty($filename)){
                                
                                $local_folder = $local_dir . '/' . $region;
                                $local_file = $local_dir . '/' . $region . '/' . $filename ;
                                        
                                // check folder exist
                                if (!is_dir($local_folder)) {
                                    
                                    if (!@mkdir($local_folder)) {
                                        $error .= sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to create '.$region.' directory');
                                        continue;
                                    }
                                    
                                    @chmod($local_folder, 0777);

                                    if (file_put_contents('.htaccess', "deny from all\n") === false) {
                                        $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to write into '.$region.' directory');
                                        continue;
                                    }
                                    
                                }
                                
                                $file_header = @get_headers($remote_url . strval($fileContents));

                                if(isset($file_header[0]) && $file_header[0] != 'HTTP/1.1 404 Not Found') {  

                                    //$fileContent = @file_get_contents($remote_url . strval($fileContents));  
                                    $fileContent = Amazon_Tools::file_get_contents($remote_url . strval($fileContents));  

                                    if (file_exists($local_file)) {
                                        if (strlen($fileContent) == filesize($local_file)) {
                                            //$error = $this->log(sprintf('%s - %s', $filename, 'Local file is not expired'), $region);
                                            //$this->db->select_query($error);   
                                            continue; // Local file is not expired
                                        }
                                    } 
                                    
                                    if (isset($fileContent) && strlen($fileContent) > 128) {
                                        if (file_exists($local_file)) {
                                            @unlink($local_file);
                                        }

                                        file_put_contents($local_file, $fileContent);                                       
                                    }
                                }
                               
                            }
                            
                        }
                    }
                }
            }
            return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Success');
            
        } else {
            return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Failed to download templates file on the https://s3.amazonaws.com/seller-templates');
        }
    }
    
    public function InstallShippingMethods($type = Amazon_Carrier::SHIPPING_STANDARD, $force = false)
    {      
        $pass = true && $this->initImportDirectory('carriers');
        
        $datadir = $this->import;

        if (!is_dir($datadir))
            @mkdir($datadir);
        
        if($pass) {
            
            $type = array( 
                Amazon_Carrier::SHIPPING_STANDARD, 
                Amazon_Carrier::SHIPPING_EXPRESS
            );

            foreach ($type as $t){
                $filename = sprintf('amazon_%s_carriers.ini', $t);
                $remote_dir = 'settings/carriers/';
                if(!$this->cache($filename, $datadir, $remote_dir, self::SOURCE_URL, $force)){
                    return false ;
                }
            }
            
            return 'Shipping Methods file successfully download';
        }
        
        return false;
    }

    public function InstallValidValues() {

        $pass = $this->DownloadValidValues();

        if ($pass) {

            if (file_exists($this->import . UpdateValidValues::FILE_SQL)) {

                if (!filesize($this->import . UpdateValidValues::FILE_SQL)) {

                    $pass = false;
                    $this->error = sprintf('%s: "%s"', 'File empty', $this->import . UpdateValidValues::FILE_SQL);
                } elseif (!$this->importSQL($this->import . UpdateValidValues::FILE_SQL)) {

                    $pass = false;
                    $this->error = sprintf('%s: "%s"', 'SQL import failed', $this->import . UpdateValidValues::FILE_SQL);
                }
            } else {

                $pass = false;
                $this->error = sprintf('%s: "%s"', 'Unable to open file', $this->import . UpdateValidValues::FILE_GZ);
            }
        }

        if ($pass) {
            $this->success = 'Valid values file successfully inserted in database';
            return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, $this->success );
        } else {
            return false;
        }

    }

    public function DownloadValidValues() {

        $file_content_gz = null;
        $pass = true && $this->initImportDirectory('import');

        if ($pass) {

	    //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.
            $md5sum = Amazon_Tools::file_get_contents(UpdateValidValues::SOURCE_URL .'data/'. UpdateValidValues::FILE_MD5);
            
            if (!strpos($md5sum, UpdateValidValues::FILE_GZ)) {
                $pass = false;
                $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Failed to download md5sum file on the server');
            } else {

                $md5_remote = strstr($md5sum, ' ', true);

                if (preg_match('/^[a-f0-9]{32}$/', $md5_remote)) {

                    $file_content_gz = Amazon_Tools::file_get_contents(UpdateValidValues::SOURCE_URL . 'data/' . UpdateValidValues::FILE_GZ);

                    if (file_put_contents($this->import . UpdateValidValues::FILE_GZ, $file_content_gz)) {

                        $md5_local = md5_file($this->import . UpdateValidValues::FILE_GZ);
                        if (!strlen($md5_local) && strlen($md5_remote) && $md5_local == $md5_remote) {

                            $pass = false;
                            $this->error = sprintf('%s(%d): %s (%s / %s)', basename(__FILE__), __LINE__, 'md5 keys mismatch', $md5_local, $md5_remote);
                        } else {

                            $buffer_size = 4096;
                            $file = gzopen($this->import . UpdateValidValues::FILE_GZ, 'rb');
                            $fout = fopen($this->import . UpdateValidValues::FILE_SQL, 'wb');
                            if (!$fout) {
                                $pass = false;
                                $this->error = sprintf('%s: "%s"', 'Failed to open file for writing', $this->import . UpdateValidValues::FILE_SQL);
                            } else {
                                while (!gzeof($file)) {
                                    fwrite($fout, gzread($file, $buffer_size));
                                }
                                fclose($fout);
                                gzclose($file);
                            }
                            if (!file_exists($this->import . UpdateValidValues::FILE_SQL) || !filesize($this->import . UpdateValidValues::FILE_SQL)) {
                                $pass = false;
                                $this->error = sprintf('%s: "%s"', 'Extraction failed', $this->import . UpdateValidValues::FILE_SQL);
                            }
                        }
                    } else {
                        $pass = false;
                        $this->error = sprintf('%s: "%s"', 'Failed to save file to', $this->import . UpdateValidValues::FILE_GZ);
                    }
                } else {
                    $pass = false;
                    $this->error = sprintf('%s(%d): %s', basename(__FILE__), __LINE__, 'Wrong md5 key returned by remote server');
                }
            }
        }

        if ($pass) {
            $this->success = 'Valid values file successfully downloaded and installed';
        }

        return $pass;
    }

    public function InstallFieldSettings() {

        static $fields_settings = array();
        $error = '';
        $pass = true && $this->initImportDirectory('models');
        $region = $this->getRegion();
        
        if ($pass) {
            
            $sql = "SELECT universe, product_type FROM amazon_valid_values GROUP BY universe, product_type; ";
            $query = $this->db->select_query($sql);
            
            while ($rows = $this->db->fetch($query)) {
                
                foreach ($region as $lang){
                    
                    if (!isset($fields_settings[$lang][$rows['universe']][$rows['product_type']])) {
                        if (!$this->DownloadFieldsSettings($lang, $rows['universe'], $rows['product_type'])) {
                            $error .= $this->log('Failed to download file on the server', $rows['universe'], $rows['product_type']);
                        }
                        $fields_settings[$lang][$rows['universe']][$rows['product_type']] = true;
                    }
                }
            }
        }

        if (strlen($error) > 10) {
            $this->db->select_query($error);
            return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Nothing to update, file already exits and not expired');
        }
        
        return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Install Field Settings Success');
    }

    public function DownloadFieldsSettings($lang, $universe, $product_type) {

        $datadir = $this->import;

        if (!is_dir($datadir))
            @mkdir($datadir);

        if ($universe == 'ClothingAccessories')
            $universe = 'ProductClothing';

        $universe = Amazon_Tools::tokey($universe);

        $product_type = Amazon_Tools::tokey($product_type);
        if (strlen($product_type) <= 0) {
            $product_type = $universe;
        }

        $remote_dir = 'settings/models/' . $universe . '/';
        $sub_dir = $datadir . $universe . DIRECTORY_SEPARATOR;

        if (!is_dir($sub_dir)) {
            @mkdir($sub_dir);
        }

        $filename = sprintf('%s.%s.csv.gz', $product_type, $lang);
        
        return $this->cache($filename, $sub_dir, $remote_dir, UpdateValidValues::SOURCE_URL);
    }
    
    public function InstallFieldTranslations() {

        $error = '';
        $pass = true && $this->initImportDirectory('translations');
        
        if ($pass) {
            
            $targets = array('universes', 'attributes', 'product_types');

            foreach ($targets as $target) {
                
                $fields_settings = array();
                
                if($target == 'universes'){
                    if (!$this->DownloadTranslations($target, $target)) {
                        $error = $this->log('Failed to download translate file on the server', $target);
                        $this->db->select_query($error);
                    }
                } else {
                    
                    $sql = "SELECT universe FROM amazon_valid_values WHERE universe != '' GROUP BY universe; ";
                    $query = $this->db->select_query($sql);
                   
                    while ($rows = $this->db->fetch($query)) {
                        if (!isset($fields_settings[$rows['universe']])) {
                            if (!$this->DownloadTranslations($rows['universe'], $target)) {
                                $error = $this->log('Failed to download translate file on the server', $rows['universe']);
                                $this->db->select_query($error);
                            }
                            $fields_settings[$rows['universe']] = true;
                        }
                    }
                }
            }
        }

        if (strlen($error) > 10) {
            return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Nothing to update, file already exits and not expired');
        }
        
        return sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Install Field Translations Success');

    }
    
    public function DownloadTranslations($universe, $target = 'attributes')
    {
            $datadir = $this->import .$target.'/';
            
            if (!is_dir($datadir))
                @mkdir($datadir);

            if ($universe == 'ClothingAccessories')
                $universe = 'ProductClothing';

            $universe = Amazon_Tools::tokey($universe);
            $remote_dir = sprintf('settings/translations/%s/', $target);
            $filename = sprintf('%s.ini.gz', Amazon_Tools::toKey($universe));
            
            return $this->cache($filename, $datadir, $remote_dir, UpdateValidValues::SOURCE_URL);

    }

    private function initImportDirectory($folder, $file = null) {

        $this->import = dirname(__FILE__) . '/../valid_value/' . $folder . '/';

        if (!is_dir($this->import)) {
            if (!@mkdir($this->import)) {
                $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to create '.$folder.' directory');
                return false;
            }
        }
        
        if (isset($file) && strlen($file)) {
            $this->import = $this->import . $file . '/';
            if (!is_dir($this->import)) {
                if (!@mkdir($this->import)) {
                    $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to create '.$file.' directory');
                    return false;
                }
            }
        }
        
        @chmod($this->import, 0777);

        if (file_put_contents($this->import . '.htaccess', "deny from all\n") === false) {
            $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to write into '.$folder.' directory');
            return false;
        }
        return true;
    }

    public function cache($file, $local_dir, $remote_dir, $url) {

        if (!is_dir($local_dir)) {
            return(false);
        }

        $local_file = $local_dir . $file;
        $remote_file = $url . $remote_dir . $file;

        if (!is_writable($local_dir)) {
            chmod($local_dir, 0775);
        }        
        
        $file_headers = @get_headers($remote_file . '?dl=1');
        
        if(isset($file_headers[0]) && $file_headers[0] != 'HTTP/1.1 404 Not Found') {  
            
            //$contents = @file_get_contents($remote_file . '?dl=1');  //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.
            $contents = Amazon_Tools::file_get_contents($remote_file . '?dl=1');  //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.

            if (file_exists($local_file)) {
                if (strlen($contents) == filesize($local_file)) {
                    $universe = explode('.', $file);
                    $error = $this->log(sprintf('%s - %s', $file, 'Local file is not expired'), $universe[0]);
                    $this->db->select_query($error);   
                    return(true); // Local file is not expired
                }
            }
        } else {
            return(false);
        }
        
        if (isset($contents) && strlen($contents) > 128) {
            if (file_exists($local_file)) {
                @unlink($local_file);
            }

            file_put_contents($local_file, $contents);
            return(true);
        }

        return(false);
    }

    public function tableExists($table) {

        $query = $this->db->select_query("SHOW TABLES LIKE '" . $table . "'");
        if ($this->db->count_rows($query) == 1) {
            return true;
        }
        return false;
    }

    public function tableClear($table) {
        return $pass = $this->db->truncate_table($table);
    }

    public function importSQL($file_sql) {

        $pass = true;

        if ($this->tableExists(UpdateValidValues::TABLE)) {
            $this->tableClear(UpdateValidValues::TABLE);
        }

        if (!file_exists($file_sql)) {
            return false;       
        } else if (!$sql = Amazon_Tools::file_get_contents($file_sql)) { //TODO: VALIDATION - Malfunctions with Tools::file_get_contents.
            return false;
        }

        $sql = str_replace(array('_DB_PREFIX_'), array(''), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", trim($sql));

        foreach ($sql as $query) {
            if (!$this->db->select_query($query)) {
                $this->log($query . ' - Fail.');
                continue;
            }
        }

        return($pass);
    }       
    
    public function createLogTable() {

        if (!$this->tableExists('amazon_valid_values_log')) {
            $this->db->select_query('
                CREATE TABLE `amazon_valid_values_log` (
                  `id_log` int(10) NOT NULL AUTO_INCREMENT,
                  `universe` varchar(32) DEFAULT NULL,
                  `product_type` varchar(64) DEFAULT NULL,
                  `message` text,
                  `date_add` datetime DEFAULT NULL,
                  PRIMARY KEY (`id_log`)
                ) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;');
        }
    }

    public function log($msg, $universe = null, $product_type = null) {
        return "INSERT INTO amazon_valid_values_log (universe, product_type, message, date_add) VALUES ('$universe', '$product_type', '$msg', '".date('Y-m-d H:i:s')."'); ";
    }

    /* Excute file */

    public function ExecuteFileValidValues() {

        static $fields_settings = array();
        
        $pass = true && $this->initImportDirectory('flat.file.csv');

        if ($pass) {

            if (is_dir($this->import)) {
                if ($dh = opendir($this->import)) {
                    while (($file = readdir($dh)) !== false) {
                        if ($file != '.htaccess') {

                            $filename = str_replace(array('Flat.File.', '.csv'), array(''), $file);
                            $name = explode('.', $filename);
                            $universe = isset($this->FileNameException[$name[0]]) ? $this->FileNameException[$name[0]] : $name[0];

                            if (isset($name[1]) && !empty($name[1])) {
                                $ext = $name[1];
                            } else {
                                $ext = 'us';
                            }

                            if (!empty($universe) && !empty($ext)) {
                                $this->bulk_valid_value($universe, $ext, $this->import . $file);
                            }
                        }
                    }
                    closedir($dh);
                } else {
                    $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to open directory file');
                }
            } else {
                $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to access directory');
            }
        }

        echo json_encode(
                array(
                    'error' => $this->error,
                    'pass' => $pass,
                )
        );

        exit;
    }

    public function bulk_valid_value($universe, $ext, $path) {

        $date = date('Y-m-d H:i:s');
        $insert_string = '';
        $valid_value = $this->_parseCSV($path);
        $ext = Amazon_Tools::domainToId($ext);

        foreach ($valid_value as $vvvk => $vvvd) {
            if (is_array($vvvd)) {
                foreach ($vvvd as $vvk => $vvd) {
                    foreach ($vvd as $vk => $vd) {
                        $product_type = null;
                        $attribute = str_replace(" ", "", ucwords(str_replace("_", " ", $vvvk)));

                        if (isset($this->productTypeException[$universe][$attribute]))
                            $attribute = $this->productTypeException[$universe][$attribute];

                        $value = '';

                        if (isset($vd) && !empty($vd)) {
                            $value = str_replace(array("'"), array("\'"), $vd);
                        }

                        if ($vvk == "All") {
                            $product_type = null;
                        } else {
                            $product_type = str_replace(" ", "", ucwords(str_replace("_", " ", $vvk)));
                        }

                        if (isset($this->productSubTypeException[$universe][$product_type])) {
                            $product_type = $this->productSubTypeException[$universe][$product_type];
                        }

                        if (isset($this->SWVGException[$product_type]) && $attribute == 'Genre') {
                            $attribute = $this->SWVGException[$product_type];
                        }

                        if (!strlen($insert_string)) {
                            $insert_string = "INSERT INTO amazon_valid_values (region, universe, product_type, attribute_field, valid_value, date_upd) VALUES "
                                    . "('" . $ext . "', '" . $universe . "','" . $product_type . "','" . $attribute . "','" . $value . "','" . $date . "')";
                        } else {
                            $insert_string .= ", ('" .$ext . "', '" . $universe . "','" . $product_type . "','" . $attribute . "','" . $value . "','" . $date . "')";
                        }
                    }
                }
            }
        }

        //exit;
        if (strlen($insert_string))
            $this->db->select_query($insert_string . '; ');

        // TRUNCATE TABLE
        $this->db->select_query("DELETE FROM `amazon_valid_values` WHERE `region` = '" . $ext . "' AND `universe` = '" . $universe . "' AND `date_upd` < '" . $date . "';");
    }

    private function _parseCSV($csvfile) {

        $datalist = $head = $subhead = array();
        $delimiter = ',';

        if ($csvfile) {
            $fp = fopen($csvfile, 'r');
            $buf = fread($fp, 1000);
            fclose($fp);
        } else {
            return $datalist;
        }

        $count1 = count(explode(',', $buf));
        $count2 = count(explode(';', $buf));
        if ($count2 > $count1)
            $delimiter = ';';

        $header = false;

        $fp = fopen($csvfile, 'r');
        $i = 0;

        while ($data = fgetcsv($fp, 0, $delimiter)) {

            if (!is_array($data)) {
                return false;
            }

            if ($i == 0) {
                foreach ($data as $khead => $vhead) {
                    if (!empty($vhead)) {
                        $head[$khead] = $vhead;
                    }
                }
            } else if ($i == 1) {
                foreach ($data as $ksubhead => $vsubhead) {
                    if (!empty($vsubhead)) {
                        $subhead[$ksubhead] = $vsubhead;
                    }
                }
            } else {
                foreach ($data as $key => $value) {
                    if (!empty($value)) {
                        if (isset($head[$key])) {
                            if (isset($subhead[$key]) && !isset($this->FieldException[$subhead[$key]]) && !strpos($subhead[$key], $this->MeasureException) && !strpos($subhead[$key], $this->IssetException)) {
                                if (strpos($head[$key], "- [")) {
                                    $explode_head = explode("-", $head[$key]);
                                    $header = 'All';
                                    if (isset($explode_head[1])) {
                                        $header = str_replace(array('[ ', ' ]'), '', trim($explode_head[1]));
                                    }
                                    $datalist[$subhead[$key]][$header][] = $value;
                                } else {
                                    $datalist[$subhead[$key]]['All'][] = $value;
                                }
                            }
                        }
                    }
                }
            }
            $i++;
        }
        fclose($fp);

        return $datalist;
    }
    
    public function dump_valid_value(){
        
        $this->initImportDirectory('export');
        $time = date('Ymd_His');
        $file = 'amazon_valid_values_'.$time.'.sql';
        $path = $this->import.$file;
        $path_to_mysql = 'D:/xampp/mysql/bin/';
       
        exec('mysqldump -u '._ci_db_username.' -p '._ci_db_password.' -h '._ci_db_hostname.' '.$path_to_mysql._ci_db_database.'.sql amazon_valid_values > '.$path);
        var_dump('mysqldump -u '._ci_db_username.' -p '._ci_db_password.' -h '._ci_db_hostname.' '.$path_to_mysql._ci_db_database.'.sql amazon_valid_values > '.$path);
        
    }    
    /* Excute file */
    
    public function getRegion(){
        $region = array();
        
        $sql = "SELECT region FROM amazon_valid_values GROUP BY region; ";
        $query = $this->db->select_query($sql);

        while ($rows = $this->db->fetch($query)) {
            array_push($region, $rows['region']);
        }
        
        return $region ;
    }
}