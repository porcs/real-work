<?php

/**
 * Deals with general XSD properties/methods used
 */
class AmazonXSD {
    
    private static $initialized = false;
    const UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
    /**************************************************************************
                                  Amazon XSD's URL
     **************************************************************************/
    public static $definitionURL = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/' ;
    public static $mainUrl = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/Product.xsd' ;
    public static $baseXSD = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/amzn-base.xsd' ;
    public static $envelopeXSD = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/amzn-envelope.xsd';
    public static $envelopeHeaderXSD = 'https://images-na.ssl-images-amazon.com/images/G/01/rainier/help/xsd/release_1_9/amzn-header.xsd';
    
    
    /**************************************************************************
                               Amazon DOM XML Schemas
     **************************************************************************/
    private static $mainDom;
    private static $mainXPath;
    private static $baseDom;
    private static $baseXPath;
    private static $envelopeDom;
    private static $envelopeXPath;
    private static $envelopeHeaderDom;
    private static $envelopeHeaderXPath;
    
    // Load FulfillmentCenter XSD
    private static $choiceFulfillmentCenter;
    private static $choiceFulfillmentCenterXPath;
    private static $choiceInventory;
    private static $choiceInventoryXPath;
    private static $choiceListings;
    private static $choiceListingsXPath;
    private static $choiceOrderAcknowledgement;
    private static $choiceOrderAcknowledgementXPath;
    private static $choiceOrderAdjustment;
    private static $choiceOrderAdjustmentXPath;
    private static $choiceOrderFulfillment;
    private static $choiceOrderFulfillmentXPath;
    private static $choiceOverride;
    private static $choiceOverrideXPath;
    private static $choicePrice;
    private static $choiceProduct;
    private static $choiceProductXPath;
    private static $choicePriceXPath;
    private static $choiceProcessingReport;
    private static $choiceProcessingReportXPath;
    private static $choiceProductImage;
    private static $choiceProductImageXPath;
    private static $choiceRelationship;
    private static $choiceRelationshipXPath;
    private static $choiceSettlementReport;
    private static $choiceSettlementReportXPath;
    
    public static $choiceSchemas;
    
    
    /**************************************************************************
              Amazon Product <universe tag> / "universe.xsd" Exceptions
     **************************************************************************/
    public static $universe = array(
                                     "SoftwareVideoGames"=>"SWVG",
                                     "GiftCard"=>"GiftCards",
                                    );
    public static $productType = array(
                                     "Tools"=>"HomeImprovementTools",
                                    );
    
    /**************************************************************************
                             Amazon Products Exceptions
     **************************************************************************/
    //indicates fields which product type is under a Simple Type Element
    public static $productTypeSimpleType =  array('PowerTransmission'=> 1,
                                            'RawMaterials' => 1,
                                            'Sports' => 1,
                                            'SportsMemorabilia' => 1,
                                            'Luggage'=>1,
                                            );
    
    //Product Type different paths
    public static $productTypeExceptions =  array(  'ProductClothing'=> '//xsd:schema/xsd:element[@name="Clothing"]/xsd:complexType/xsd:sequence/xsd:element[@name="ClassificationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="ClothingType"]/xsd:simpleType/xsd:restriction/*',
                                                    'ClothingAccessories'=> '//xsd:schema/xsd:element[@name="ClothingAccessories"]', //added 03-Nov-2014 Erick T.
                                                    'Shoes'=> '//xsd:schema/xsd:element[@name="Shoes"]/xsd:complexType/xsd:sequence/xsd:element[@name="ClothingType"]/xsd:simpleType/xsd:restriction/*',
                                                    'SWVG'=> '//xsd:schema/xsd:element[@name="SoftwareVideoGames"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]/xsd:complexType/xsd:choice/*',
                                                    'Sports'=> '//xsd:schema/xsd:element[@name="Sports"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]/xsd:simpleType/xsd:restriction/*',
                                                 );
    
    //when universe tag is same as product type tag, this is used to avoid duplicated tags
    public static $productTypeDuplicatedExceptions = array('ClothingAccessories'=>1, 'Luggage'=>1, 'Sports'=>1);
    
    //Product Subtype different paths
    public static $productSubtype = array(
                                           'CE' => '//xsd:schema/xsd:element[@name="CE"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductSubtype"]',
                                         );
    //Variation Theme different paths
    public static $variationThemeExceptions = array(
                                              'ProductClothing'=> '//xsd:schema/xsd:element[@name="Clothing"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]',
                                              'Toys'=> '//xsd:schema/xsd:element[@name="Toys"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]',
                                              'WineAndAlcohol'=> '//xsd:schema/xsd:element[@name="WineAndAlcohol"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]',
                                              'HomeImprovement'=> '//xsd:schema/xsd:complexType[@name="HomeImprovementTools"]/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]',
                                              'Sports'=> '//xsd:schema/xsd:element[@name="Sports"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]',
                                            );
    
    //Adds additional elements as variation data
    public static $variationDataAdditionalElements = array(
                                                'Toys'=> array('AgeRecommendation'=>'//xsd:schema/xsd:element[@name="Toys"]/xsd:complexType/xsd:sequence/xsd:element[@name="AgeRecommendation" or @ref="AgeRecommendation"]'),
                                            );
    
    //Adds additional paths to look for variation data elements
    public static $variationDataAdditionalPath = array(
                                                'Toys'=> '//xsd:schema/xsd:element[@name="Toys"]/xsd:complexType/xsd:sequence/xsd:element[not(@name="VariationData")][not(@name="ProductType")]',
                                            );
    
    //Adds additional specific fields paths
    public static $specificFieldsAdditions = array(
                                                'Toys'=>'//xsd:schema/xsd:element[@name="Toys"]/xsd:complexType/xsd:sequence/xsd:element[not(@name="VariationData")][not(@name="ProductType")]',
                                                'WineAndAlcohol'=>'//xsd:schema/xsd:element[@name="WineAndAlcohol"]/xsd:complexType/xsd:sequence/xsd:element[not(@name="ProductType")]',                                                
                                            );

    public static $recommededFieldsAdditions = array(
                                                'Sports'=>'//xsd:schema/xsd:element[@name="Sports"]/xsd:complexType/xsd:sequence/xsd:element[not(@name="VariationData")][not(@name="ProductType")]',
                                            );
    
    
    /**************************************************************************
           Initialize static variables setting respective DOM and DomXPath
     **************************************************************************/
    public static function initialize(){
//        if(self::$initialized)
//            return;
        
        //Load Main XSD
        self::$mainDom = new DOMDocument();
        self::$mainDom->loadXML(self::cache(self::$mainUrl));
        self::$mainDom->formatOutput = true;
        self::$mainXPath = new DOMXPath(self::$mainDom);
        
        //Load Base XSD
        self::$baseDom = new DOMDocument();
        self::$baseDom->loadXML(self::cache(self::$baseXSD));
        self::$baseDom->formatOutput = true;
        self::$baseXPath = new DOMXPath(self::$baseDom);
        
        //Load Envelope XSD
        self::$envelopeDom = new DOMDocument();
        self::$envelopeDom->loadXML(self::cache(self::$envelopeXSD));
        self::$envelopeDom->formatOutput = true;
        self::$envelopeXPath = new DOMXPath(self::$envelopeDom);
        
        //Load Envelope Header XSD
        self::$envelopeHeaderDom = new DOMDocument();
        self::$envelopeHeaderDom->loadXML(self::cache(self::$envelopeHeaderXSD));
        self::$envelopeHeaderDom->formatOutput = true;
        self::$envelopeHeaderXPath = new DOMXPath(self::$envelopeHeaderDom);
        
        /**************************************************************************
                             Load Additional XML Schemas
        **************************************************************************/ 
        // Load FulfillmentCenter XSD
        self::$choiceFulfillmentCenter = new DOMDocument();
        self::$choiceFulfillmentCenter->formatOutput = true;
        self::$choiceFulfillmentCenter->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'FulfillmentCenter.xsd') ) ;
        self::$choiceFulfillmentCenterXPath = new DOMXPath(self::$choiceFulfillmentCenter);
        
        // Load Inventory XSD
        self::$choiceInventory = new DOMDocument();
        self::$choiceInventory->formatOutput = true;
        self::$choiceInventory->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Inventory.xsd') ) ;
        self::$choiceInventoryXPath = new DOMXPath(self::$choiceInventory);
        
        // Load Listings XSD
        self::$choiceListings = new DOMDocument();
        self::$choiceListings->formatOutput = true;
        self::$choiceListings->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Listings.xsd') ) ;
        self::$choiceListingsXPath = new DOMXPath(self::$choiceListings);
        
        // Load OrderAcknowledgement XSD
        self::$choiceOrderAcknowledgement = new DOMDocument();
        self::$choiceOrderAcknowledgement->formatOutput = true;
        self::$choiceOrderAcknowledgement->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'OrderAcknowledgement.xsd') ) ;
        self::$choiceOrderAcknowledgementXPath = new DOMXPath(self::$choiceOrderAcknowledgement);
        
        // Load OrderAdjustment XSD
        self::$choiceOrderAdjustment = new DOMDocument();
        self::$choiceOrderAdjustment->formatOutput = true;
        self::$choiceOrderAdjustment->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'OrderAdjustment.xsd') ) ;
        self::$choiceOrderAdjustmentXPath = new DOMXPath(self::$choiceOrderAdjustment);
        
        // Load OrderFulfillment XSD
        self::$choiceOrderFulfillment = new DOMDocument();
        self::$choiceOrderFulfillment->formatOutput = true;
        self::$choiceOrderFulfillment->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'OrderFulfillment.xsd') ) ;
        self::$choiceOrderFulfillmentXPath = new DOMXPath(self::$choiceOrderFulfillment);
        
        // Load Override XSD
        self::$choiceOverride = new DOMDocument();
        self::$choiceOverride->formatOutput = true;
        self::$choiceOverride->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Override.xsd') ) ;
        self::$choiceOverrideXPath = new DOMXPath(self::$choiceOverride);
        
        // Load Price XSD
        self::$choicePrice = new DOMDocument();
        self::$choicePrice->formatOutput = true;
        self::$choicePrice->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Price.xsd') ) ;
        self::$choicePriceXPath = new DOMXPath(self::$choicePrice);
        
        // Load Product XSD
        self::$choiceProduct = new DOMDocument();
        self::$choiceProduct->formatOutput = true;
        self::$choiceProduct->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Product.xsd') ) ;
        self::$choiceProductXPath = new DOMXPath(self::$choiceProduct);
        
        // Load ProcessingReport XSD
        self::$choiceProcessingReport = new DOMDocument();
        self::$choiceProcessingReport->formatOutput = true;
        self::$choiceProcessingReport->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'ProcessingReport.xsd') ) ;
        self::$choiceProcessingReportXPath = new DOMXPath(self::$choiceProcessingReport);
        
        // Load ProductImage XSD
        self::$choiceProductImage = new DOMDocument();
        self::$choiceProductImage->formatOutput = true;
        self::$choiceProductImage->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'ProductImage.xsd') ) ;
        self::$choiceProductImageXPath = new DOMXPath(self::$choiceProductImage);
        
        // Load Relationship XSD
        self::$choiceRelationship = new DOMDocument();
        self::$choiceRelationship->formatOutput = true;
        self::$choiceRelationship->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'Relationship.xsd') ) ;
        self::$choiceRelationshipXPath = new DOMXPath(self::$choiceRelationship);
        
        // Load SettlementReport XSD
        self::$choiceSettlementReport = new DOMDocument();
        self::$choiceSettlementReport->formatOutput = true;
        self::$choiceSettlementReport->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . 'SettlementReport.xsd') ) ;
        self::$choiceSettlementReportXPath = new DOMXPath(self::$choiceSettlementReport);
        
        self::$choiceSchemas = array(
                                    "FulfillmentCenter"=>self::$choiceFulfillmentCenterXPath,
                                    "Inventory"=>self::$choiceInventoryXPath,
                                    "Listings"=>self::$choiceListingsXPath,
                                    "OrderAcknowledgement"=>self::$choiceOrderAcknowledgementXPath,
                                    "OrderAdjustment"=>self::$choiceOrderAdjustmentXPath,
                                    "OrderFulfillment"=>self::$choiceOrderFulfillmentXPath,
                                    "Override"=>self::$choiceOverrideXPath,
                                    "Price"=>self::$choicePriceXPath,
                                    "Product"=>self::$choiceProductXPath,
                                    "ProcessingReport"=>self::$choiceProcessingReportXPath,
                                    "ProductImage"=>self::$choiceProductImageXPath,
                                    "Relationship"=>self::$choiceRelationshipXPath,
                                    "SettlementReport"=>self::$choiceSettlementReportXPath
                              );
        self::$initialized = true;
    }
    
    /**************************************************************************
               Functions for accessing Base DOM Documents / DOM Xpaths
     **************************************************************************/
    public static function getMainDom(){
        self::initialize();
        return self::$mainDom;
    }
    public static function getMainXPath(){
        self::initialize();
        return self::$mainXPath;
    }
    public static function getBaseDom(){
        self::initialize();
        return self::$baseDom;
    }
    public static function getBaseXPath(){
        self::initialize();
        return self::$baseXPath;
    }
    public static function getEnvelopeDom(){
        self::initialize();
        return self::$envelopeDom;
    }
    public static function getEnvelopeXPath(){
        self::initialize();
        return self::$envelopeXPath;
    }
    public static function getEnvelopeHeaderDom(){
        self::initialize();
        return self::$envelopeHeaderDom;
    }
    public static function getEnvelopeHeaderXPath(){
        self::initialize();
        return self::$envelopeHeaderXPath;
    }
    
    /**************************************************************************
                             Amazon Common XSD Functions
     **************************************************************************/
    /*
     * Verifies if files version is expired, and if so they're downloaded from amazon site
     * Added by Olivier 2012/11/27
     */
    public static function cache($URL) 
    { 
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'xsd';  
        
        if ( ! is_dir($dir) )
                if ( ! mkdir($dir) )
                        return(false) ;
        
        if ( ! is_writable($dir) )
                @chmod($dir, 0775) ;
                
        $output_file = $dir . DIRECTORY_SEPARATOR . basename($URL) ;        
        
        if (  file_exists($output_file) && filesize($output_file) > 1024 )
            if ( (time() - filemtime($output_file)) < ((60 * 60 * 24 * 30) + rand(86400, 86400 * 7)) )        
                return(file_get_contents($output_file)) ;

        $contents = Amazon_Tools::file_get_contents($URL);
        
        if ( file_exists($output_file) && ! ($contents) )
           return(false) ;

        if ( ! is_writable($output_file) )
            @chmod($output_file, 0664) ;
        
        if ( ! file_put_contents($output_file, $contents) )
           return(false) ;
     
        return($contents) ;
    }
    
    
    /**
     * Obtains XML structure of current element
     * @param DOMNode $element
     * @return type
     */
    public static function getReferencedStructure(DOMNode &$element, DOMDocument &$productDom){
        
        if(!($element instanceof DOMElement)){
            return $element;
        }
        $productXPath = new DOMXPath($productDom);
        
        $ref = $element->getAttribute('ref');
        $min = $element->getAttribute('minOccurs') != "" ? $element->getAttribute('minOccurs') : "";
        $max = $element->getAttribute('maxOccurs') != "" ? $element->getAttribute('maxOccurs') : "";
        
        if($ref){ 
            $query = '//xsd:element[@name="' . $ref . '"]';        
            $xpath = array('prod'=>$productXPath, 
                           'base'=>self::$baseXPath, 
                           'main'=>self::$mainXPath, 
                           'header'=>self::$envelopeHeaderXPath,
                           'envelope' => self::$envelopeXPath,
                          );

            foreach($xpath as $x){
                $result = $x->query($query);
                if($result->length > 0){
                    foreach ($result as $node){
                        
                        $element = $productDom->importNode($node->cloneNode(true),true);
                        
                        if($min != ""){
                            $element->setAttribute('minOccurs',$min);
                        }
                        if($max != ""){
                            $element->setAttribute('maxOccurs',$max);
                        }
                        $query2 = './/xsd:element[@ref]'; 
                        $children_ref = $productXPath->query($query2, $element);
                        if($children_ref->length > 0){
                            foreach($children_ref as $child_ref){
                                $children_ref = self::getReferencedStructure($child_ref, $productDom);
                            }
                        }
                        return $element;
                    }
                }
            }
            
            //********************************************************************//
            //             Check for Base Choice element of Message               //
            //********************************************************************//
            
            foreach(self::$choiceSchemas as $x){
                $result = $x->query($query);
                if($result->length > 0){
                    foreach ($result as $node){
                        $element= $productDom->importNode($node->cloneNode(true),true);
                        if($min != ""){
                            $element->setAttribute('minOccurs',$min);
                        }
                        if($max != ""){
                            $element->setAttribute('maxOccurs',$max);
                        }
                        $query2 = './/xsd:element[@ref]'; 
                        $children_ref = $productXPath->query($query2, $element);
                        if($children_ref->length > 0){
                            foreach($children_ref as $child_ref){
                                $child_ref = self::getReferencedStructure($child_ref, $productDom);
                            }
                        }
                        return $element;
                    }
                }
            }
        }else{
            return $element;
        }
    }
    
    /**
     * Searches in XSD files related (Base or Product) for the XSD structure of current element
     * @param DOMElement $element
     */
    public static function getElementStructure(DOMElement &$element, DOMDocument &$productDom){
        
        $productXPath = new DOMXPath($productDom);
        
        $xpath = array( 'prod'=>$productXPath,
                        'base'=>self::$baseXPath, 
                        'main'=>self::$mainXPath, 
                        'header'=>self::$envelopeHeaderXPath,
                        'envelope' => self::$envelopeXPath,
                        'price'=>self::$choicePriceXPath,
                        'inventory'=>self::$choiceInventoryXPath
                       );  
        //obtain referenced structure of current element
        $element = self::getReferencedStructure($element, $productDom);
        //Get Structure from its Type definition
        $type = $element->getAttribute('type');
        
        if($type){
            $break = false;
            $arrTags = array('xsd:simpleType','xsd:complexType');            
            foreach($arrTags as $tag){
                $query = $tag . '[@name="' . $type . '"]';  
                foreach($xpath as $key=>$x){ 
                    $result = $x->query($query);
                    if($result->length > 0){
                        foreach ($result as $node){
                            $typeStructure = $productDom->importNode($node->cloneNode(true),true);
                            $typeStructure->removeAttribute('name');
                            $element->appendChild($typeStructure);
                            $element->removeAttribute('type');
                            $break = true;
                            break;
                        }
                    }
                }
                if($break)
                    break;
            }
        }
        
        //get current attributes and save them for later
        $attributesList = $element->getElementsByTagName('attribute');
        $attributes = array();
        foreach($attributesList as $attribute){
            $attributes[] = $attribute->cloneNode(true);
        }
        
        $extension = $element->getElementsByTagName('extension')->length > 0 ? 
                     $element->getElementsByTagName('extension')->item(0) : 
                    false;
        
        //If element has en extension, its data is obtained and attributes are added
        if($extension && $extension->getAttribute('base') != null){
            $break = false;
            $arrTags = array('xsd:simpleType','xsd:complexType');            
            foreach($arrTags as $tag){
                $query = $tag . '[@name="' . $extension->getAttribute('base') . '"]';        
                foreach($xpath as $x){
                    $result = $x->query($query);
                    if($result->length > 0){
                        foreach ($result as $node){
                            $extensionStructure = $productDom->importNode($node->cloneNode(true),true);
                            $extensionStructure->removeAttribute('name');
                            
                            /*****************************************************************************************
                             * if base element is an extension, its attributes are appended and the "base" is updated
                             *****************************************************************************************/
                            if($extensionStructure->getElementsByTagName("extension")->length > 0){ 
                                $new_extension = $extensionStructure->getElementsByTagName("extension")->item(0);
                                $cur_extension = $element->getElementsByTagName("extension")->item(0);
                                $cur_extension->setAttribute("base", $new_extension->getAttribute("base"));
                                foreach($new_extension->getElementsByTagName("attribute") as $attr){
                                    $cur_extension->appendChild($attr);
                                }
                                
                            }
                            /*****************************************************************************************
                             * if base element is restriction, and its extension is complextype, then restriciont 
                             * is included in simple content
                             *****************************************************************************************/
                            elseif($extensionStructure->getElementsByTagName("restriction")->length > 0 &&
                                   $element->getElementsByTagName("extension")->length > 0){ 
                                $new_restriction = $extensionStructure->getElementsByTagName("restriction")->item(0);
                                $cur_extension = $element->getElementsByTagName("extension")->item(0);
                                foreach($cur_extension->getElementsByTagName("attribute") as $attr){
                                    $new_restriction->appendChild($attr);
                                }
                                $cur_extension->parentNode->replaceChild($new_restriction, $cur_extension);
                                
                                
                            }else{
                                //base restriction
                                $restrictionToAdd = $extensionStructure->getElementsByTagName('restriction');
                                if($restrictionToAdd->length > 0){ 
                                    //add respective attributes
                                    foreach($attributes as $attribute){
                                        $restrictionToAdd->item(0)->appendChild($attribute);
                                    }
                                    //remove Extension node
                                    $extensionToRemove = $element->getElementsByTagName('extension')->item(0);
                                    $parentNode = $extensionToRemove->parentNode;
                                    $parentNode->removeChild($extensionToRemove);
                                    //add restriction referenced
                                    $parentNode->appendChild($restrictionToAdd->item(0));
                                }
                                $element->appendChild($extensionStructure);
                            }
                            
                            $break = true;
                            break;
                        }
                    }
                }
                if($break)
                    break;
            }
        }
        
    }
    
}

?>
