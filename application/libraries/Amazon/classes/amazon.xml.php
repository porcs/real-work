<?php

class AmazonXml 
{
    /**
     * Indicates if data is obtained from a Sqlite database
     */
    const NO_SQLITE = false;
    public static $user;
    public static $db;
    public static $debug;
    public static $exceptionType = array('OrderFulfillment' => 1, 'OrderAcknowledgement' => 1, 'Relationship' => 1 );    
    public static $productXml = array();
    
    //public $message; // 17/02/2016
        /**
     * Get the XML feed for a request
     * @param type $type Indicates the type of XML that will be generated
     */
    public static function getXmlRequest($type, $operationMode, $products, $db = null, $debug = false){
        
        global $user_data;
        AmazonXml::$debug = $debug;
        
        if (isset($db) && !empty($db)) {
            AmazonXml::$db = $db;
        } else {
            $user = str_replace(" ", "_", $user_data->user_name);
            AmazonXml::$db = new AmazonDatabase($user);
        }
        
        AmazonXml::$user = $user_data;         

        $feed = self::getXmlFeed($type, $operationMode, $products);
        return $feed;
    }     
    
    /*
     * Get the envelope XML, and its content is according to the feed_type
     * @param type $feed_type Feed to be generated: Inventory, Price, Product
     * @return type
     */
    private static function getXmlFeed($feed_type, $operationMode, $products){

        global $error_logs; 
        $products_list = $products;
        
        if(isset($products_list) && !empty($products_list))
        {
            $dom =  new DOMDocument("1.0", "utf-8");
            $dom->formatOutput = true;

            //Create AmazonEnvelope tag
            $AmazonEnvelope = $dom->createElement("AmazonEnvelope");
            $AmazonEnvelope->setAttribute("xsi:noNamespaceSchemaLocation", "amzn-envelope.xsd");
            $AmazonEnvelope->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            $dom->appendChild($AmazonEnvelope);

            //Create Header tag
            $Header = $dom->createElement("Header");
	    
            //Create DocumentVersion tag
            $DocumentVersion = $dom->createElement("DocumentVersion");
            $DocumentVersion->nodeValue = "1.01";
            $Header->appendChild($DocumentVersion);
	    
            //Create MerchantIdentifier tag
            $MerchantIdentifier = $dom->createElement("MerchantIdentifier");
            $MerchantIdentifier->nodeValue = AmazonXml::$user->merchant_id;
            $Header->appendChild($MerchantIdentifier);
            $AmazonEnvelope->appendChild($Header);

            //Create MessageType tag
            $MessageType = $dom->createElement("MessageType");
            $MessageType->nodeValue = "Product";

            if($feed_type == "Product")
                $oMode = ($operationMode == AmazonDatabase::DELETE) ? 'Delete' : (($operationMode == AmazonDatabase::CREATE) ? 'Update' : 'PartialUpdate') ;
            else
                $oMode = ($operationMode == AmazonDatabase::DELETE) ? 'Delete' : 'Update';
                       
            if(isset($products_list['product']))
                $data = $products_list['product'];
            
            switch($feed_type){

                case "Image":
                    $MessageType->nodeValue = "ProductImage";
                    break;
                case "Relationship":
                    $data = $products_list['relationship'];
                    $MessageType->nodeValue = "Relationship";
                    break;                
                case "OrderFulfillment":
                    $orders = array();
                    $data = $products_list;
                    $MessageType->nodeValue = "OrderFulfillment";
                    break;                
                case "OrderAcknowledgement":
		    $orders = array();
                    $data = $products_list;
                    $MessageType->nodeValue = "OrderAcknowledgement";
                    break;                
                case "Shipping_override":
                    $feed_type = 'Override';
                    $data = isset($products_list['override']) && !empty($products_list['override']) ? $products_list['override'] : null;                    
                    $MessageType->nodeValue = "Override";                    
                    break;                
            }
            
            if (!isset($data) || empty($data)) {
                return(false);
            }

            $AmazonEnvelope->appendChild($MessageType);

            $idx = 1;
            $update_product = $update_offer = '';
            $history = array();
           
            foreach ($data as $p) {
                if (!empty($p) && $p != null && $p != '') {
                     
                    $id_product = isset($p['id_product']) ? $p['id_product'] : null;
                    $id_product_attribute = isset($p['id_product_attribute']) && !isset($p['parent']) ? $p['id_product_attribute'] : null;
                    
                    if (!isset(self::$exceptionType[$feed_type])) {
                        if (!isset($p['SKU']) || empty($p['SKU'])){
                            continue;
                        }

                        //Only Creation Mode
                        if ($operationMode == AmazonDatabase::CREATE && $feed_type == "Product") {
                            if (!isset($p["ProductData"]['product_type']) || empty($p["ProductData"]['product_type'])) {
                                continue;
                            }
                        } 
                        
                        if ($feed_type == "Inventory" || $feed_type == "Price") {
                            if (isset($p['parent'])) {
                                continue;
                            }
                        }

                        if ($operationMode != AmazonDatabase::DELETE && $feed_type == "Product") {
                            if (!isset($p['ProductDescription']['Title']) || empty($p['ProductDescription']['Title']))
                                continue;
                        }
                    }
                    
                    // Do no export price
                    if ($feed_type == "Price") {
                        if (isset($p['no_price_export']) && $p['no_price_export']) {
                            continue;
                        }
                    }
                            
                    // Do no export quantity
                    if ($feed_type == "Inventory") {
                        if (isset($p['no_quantity_export']) && $p['no_quantity_export']) {
                            continue;
                        }
                    }

                    // If have no child skip
                    if ($feed_type == "Relationship") {
                        if(!isset($p["children"]) || empty($p["children"])) {
                            continue;
                        }
                    }
                    
                    // IF Feed Image
                    if ($feed_type == "Image") {
                        
                        //Create a message for each Image //ProductImage
                        if (isset($p["images"]) && !empty($p["images"])) {
                            
                            foreach ( $p["images"] as $image_key => $image) {
                                
                                //Create a message for each image
                                $messageImg = $dom->createElement("Message");

                                //messsage ID
                                $MessageID = $dom->createElement("MessageID");
                                $MessageID->nodeValue = $idx;
                                $messageImg->appendChild($MessageID);

                                // Operation Type
                                $OperationType = $dom->createElement("OperationType");
                                $OperationType->nodeValue = $oMode;
                                $messageImg->appendChild($OperationType);

                                $image_info = array();
                                $image_info['SKU'] = $p["SKU"];
                                $image_info['type'] = $image_key;
                                $image_info['url'] = $image;
                                self::getXmlImage($dom, $messageImg, $image_info);

                                //append the message element
                                $AmazonEnvelope->appendChild($messageImg);                       
                                $idx++;
                            }
                    
                        } else {
                            continue;
                        }
                        
                    } else {
                        
                        //Create a message for each product / combination
                        $message = $dom->createElement("Message");

                        //messsage ID
                        $MessageID = $dom->createElement("MessageID");
                        $MessageID->nodeValue = $idx;
                        $message->appendChild($MessageID);

                        //OperationType
                        if ($operationMode != 'Update Order') {

                            // exception for ShippingOverride when remove the Override price, we sent Operation type to delete
                            if($feed_type == "Override") {
                                if(isset($p['Shipping']['Amount']) && ($p['Shipping']['Amount'] === 0)) {
                                    $oMode = 'Delete';
                                } else {
                                    $oMode = ($operationMode == AmazonDatabase::DELETE) ? 'Delete' : (($operationMode == AmazonDatabase::CREATE) ? 'Update' : 'PartialUpdate') ;
                                }
                            } 
                            
                            $OperationType = $dom->createElement("OperationType");
                            $OperationType->nodeValue = $oMode;
                            $message->appendChild($OperationType);
                
                        }

                        switch ($feed_type) {
                            case "Product":   
                                if ($operationMode == AmazonDatabase::DELETE) {
                                    self::getXmlDelete($dom, $message, $p);
                                    if (AmazonXml::$debug){
					$update_product .= '<pre>';
                                    }
				    $update_product .= AmazonXml::$db->updateMatchProductToSync($p['SKU'], AmazonXml::$user->id_country, 
						AmazonXml::$user->id_shop, 'delete', $id_product, $id_product_attribute);
                                } else {
                                    if (!isset($p['ProductDescription']['Title']) || empty($p['ProductDescription']['Title'])){
                                        continue;
                                    }
                                    self::getXmlProduct($dom, $message, $p);     
				    if (AmazonXml::$debug){
					$update_product .= '<pre>';
				    }
				    $update_product .= AmazonXml::$db->updateMatchProductToSync($p['SKU'], AmazonXml::$user->id_country, 
						AmazonXml::$user->id_shop, 'send',$id_product , $id_product_attribute);
                                }                                
                                break;
                            case "Inventory":
                                self::getXmlInventory($dom, $message, $p);
                                break;
                            case "Price":
                                self::getXmlPrice($dom, $message, $p);
                                break;
                            case "Relationship":
                                self::getXmlRelationship($dom, $message, $p);
                                break;
                            case "OrderFulfillment":
                                if(isset($p['AmazonOrderID']) && !empty($p['AmazonOrderID']))
                                    array_push ($orders, $p['AmazonOrderID']);
                                self::getXmlOrderFulfillment($dom, $message, $p);
                                break;
                            case "OrderAcknowledgement":
                                if(isset($p['AmazonOrderID']) && !empty($p['AmazonOrderID']))
				    array_push ($orders, $p['AmazonOrderID']);
				self::getXmlOrderAcknowledgement($dom, $message, $p);
                                break;
                            case "Override":    
                                self::getXmlOverride($dom, $message, $p, $oMode);
                                break;
                        }
                        
                        if (!AmazonXml::$debug){
                            if($operationMode == AmazonDatabase::SYNC && AmazonXml::$user->cron && !isset($history[$id_product])) {
                                $update_offer .= ImportLog::update_flag_offers(AmazonXml::$user->id_marketplace, AmazonXml::$user->ext, AmazonXml::$user->id_shop, $id_product);
                            }
                        }
                        
                        $history[$id_product] = true;
                        //append the message element
                        $AmazonEnvelope->appendChild($message);                       
                        $idx++;
                    }
                }                
            }
            
            if($feed_type == "Image") $feed_type = "ProductImage";
            
            // check price xml feed if !empty
            if($feed_type != "Product"){
                if($dom->getElementsByTagName($feed_type)->length == 0) {
                     return(false);
                }
            }           

            //validate generated XML
            AmazonXmlValidator::validateEnvelope($dom, $feed_type, $data);
        }
        else
        {
            return(false);
        }
        
        if(isset($error_logs) || !empty($error_logs))
        {
            return self::setErrorLogs($feed_type); 
        }
        else 
        {    	    
            if(!AmazonXml::$debug){
                AmazonXml::$db->exec_query($update_product) ;            
                AmazonXml::$db->db->offer->db_exec($update_offer) ;
            } else {
		//echo $update_product;
	    }
            	    
            if ($operationMode == 'Update Order') {
                return array('xml' => $dom->saveXML(), 'orders' => $orders);
            } else {
                return $dom->saveXML();
            }
        }        
    }
    
    /**
     * Get XML Feed Type: Product
     * @global type $content
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlProduct(DOMDocument &$dom, DOMElement &$message, array $p){
        
        if(!isset($p) || empty($p))
        {
            AmazonXmlValidator::displayError("No products to send on Amazon");
            return false;
        }        
        
        if(isset($p["ProductData"]) && !empty($p["ProductData"])){
            $content = $p["ProductData"];
        }
        
        $Product = $dom->createElement("Product");
        
        if (isset($p["SKU"]) && !empty($p["SKU"]))
	{	    
	    $SKU = $dom->createElement('SKU');	    
	    $Product->appendChild($SKU);
	    
	    $SKUText = $dom->createTextNode($p["SKU"]);
	    $SKU->appendChild($SKUText);
        }
        
        if( isset($p["StandardProductID"]["reference"]) && !empty($p["StandardProductID"]["reference"]) )
        {
	    $StandardProductID = $dom->createElement('StandardProductID');
	    $Product->appendChild($StandardProductID);
	    
	    $StandardProductID_type = $dom->createElement("Type");	    
	    $StandardProductID->appendChild($StandardProductID_type);
	    $StandardProductIDText = $dom->createTextNode($p["StandardProductID"]["reference_type"]);
	    $StandardProductID_type->appendChild($StandardProductIDText);
	    
	    $StandardProductID_value = $dom->createElement("Value");
	    $StandardProductID->appendChild($StandardProductID_value);
	    $StandardProductIDvalue = $dom->createTextNode($p["StandardProductID"]["reference"]);
	    $StandardProductID_value->appendChild($StandardProductIDvalue);

        }
      
        if(isset($p["available_date"]) && !empty($p["available_date"]))
	{
	    $LaunchDate = $dom->createElement('LaunchDate');	    
	    $Product->appendChild($LaunchDate);
	    
	    $LaunchDateText = $dom->createTextNode($p["available_date"]);
	    $LaunchDate->appendChild($LaunchDateText);	    
        }

        if(isset($p["condition"]) && !empty($p["condition"]))
	{
	    $Condition = $dom->createElement('Condition');	    
	    $Product->appendChild($Condition);
	    
	    $ConditionType = $dom->createElement("ConditionType");
	    $Condition->appendChild($ConditionType);
	    
	    $ConditionText = $dom->createTextNode(ucwords($p["condition"])=="Used" ? "UsedAcceptable" : ucwords($p["condition"]));
	    $ConditionType->appendChild($ConditionText);	 

            // ConditionNote
            if(isset($p["condition_note"]) && !empty($p["condition_note"]))
	    {
		$ConditionNote = $dom->createElement("ConditionNote");
		$Condition->appendChild($ConditionNote);

		$ConditionNoteText = $dom->createTextNode($p["condition_note"]);
		$ConditionNote->appendChild($ConditionNoteText);   
            }            
        }

        if(isset($p["ProductDescription"]) && !empty($p["ProductDescription"]))
        {
            $DescriptionData = $dom->createElement("DescriptionData");
	    $Product->appendChild($DescriptionData);
	    
            if(isset($p["ProductDescription"]['Title']) && !empty($p["ProductDescription"]['Title']))
            {		
		$Title = $dom->createElement("Title");
		$DescriptionData->appendChild($Title);
		
		$TitleText = $dom->createTextNode(isset($p["ProductDescription"]['Title']) ? $p["ProductDescription"]['Title'] : '');
		$Title->appendChild($TitleText);  
            }

            if(isset($p["ProductDescription"]['Brand']) && !empty($p["ProductDescription"]['Brand']))
	    {
		$Brand = $dom->createElement("Brand");
		$DescriptionData->appendChild($Brand);
		
		$BrandText = $dom->createTextNode(isset($p["ProductDescription"]['Brand']) ? $p["ProductDescription"]['Brand'] : '');
		$Brand->appendChild($BrandText);		
            }
            
            if(isset($p["ProductDescription"]['Description']) && !empty($p["ProductDescription"]['Description']))
	    {		
		$DescriptionTag = $dom->createElement('Description');
                $DescriptionData->appendChild($DescriptionTag);

                $DescriptionText = $dom->createTextNode($p["ProductDescription"]["Description"]);
                $DescriptionTag->appendChild($DescriptionText);
            }
            
            //BulletPoint
            if(isset($p["ProductDescription"]['BulletPoint']) && !empty($p["ProductDescription"]['BulletPoint'])){ 
                /*Bullets may have multiple values*/
                foreach($p["ProductDescription"]['BulletPoint'] as $bullet_point)
                {
                    if(!empty($bullet_point)) {
			
			$BulletPoint = $dom->createElement("BulletPoint");
			$DescriptionData->appendChild($BulletPoint);

			$BulletPointText = $dom->createTextNode($bullet_point);
			$BulletPoint->appendChild($BulletPointText);
                    }
                }
            }
           
	    //ItemDimensions
            if(isset($p["ProductDescription"]['ItemDimensions'])  && !empty($p["ProductDescription"]['ItemDimensions']))
	    {
                $ItemDimensions = $dom->createElement("ItemDimensions");
		$DescriptionData->appendChild($ItemDimensions);
		
                if(isset($p["ProductDescription"]['ItemDimensions']["depth"]['value']))
		{
		    $Length = $dom->createElement("Length");
		    $ItemDimensions->appendChild($Length);

		    $LengthText = $dom->createTextNode($p["ProductDescription"]['ItemDimensions']["depth"]['value']);
		    $Length->appendChild($LengthText);
                    
                    if(isset($p["ProductDescription"]['ItemDimensions']["depth"]['unit']))
                        $Length->setAttribute("unitOfMeasure",  strtoupper($p["ProductDescription"]['ItemDimensions']["depth"]['unit']));
                }
		
                if(isset($p["ProductDescription"]['ItemDimensions']["width"]['value']))
		{
		    $Width = $dom->createElement("Width");
		    $ItemDimensions->appendChild($Width);

		    $WidthText = $dom->createTextNode($p["ProductDescription"]['ItemDimensions']["width"]['value']);
		    $Width->appendChild($WidthText);
                    
                    if(isset($p["ProductDescription"]['ItemDimensions']["width"]['unit']))
                        $Width->setAttribute("unitOfMeasure",  strtoupper($p["ProductDescription"]['ItemDimensions']["width"]['unit']));
                }
		
                if(isset($p["ProductDescription"]['ItemDimensions']["height"]['value']))
		{
		    $Height = $dom->createElement("Height");
		    $ItemDimensions->appendChild($Height);

		    $HeightText = $dom->createTextNode($p["ProductDescription"]['ItemDimensions']["height"]['value']);
		    $Height->appendChild($HeightText);
                    
                    if(isset($p["ProductDescription"]['ItemDimensions']["width"]['unit']))
                        $Height->setAttribute("unitOfMeasure",  strtoupper($p["ProductDescription"]['ItemDimensions']["width"]['unit']));
                }
		
                if(isset($p["ProductDescription"]['ItemDimensions']["weight"]['value']))
		{
		    $Weight = $dom->createElement("Weight");
		    $ItemDimensions->appendChild($Weight);

		    $WeightText = $dom->createTextNode($p["ProductDescription"]['ItemDimensions']["weight"]['value']);
		    $Weight->appendChild($WeightText);
                    
                    if(isset($p["ProductDescription"]['ItemDimensions']["weight"]['unit']))
                        $Weight->setAttribute("unitOfMeasure",  strtoupper($p["ProductDescription"]['ItemDimensions']["weight"]['unit']));
                }
            } 
	    
	    //PackageDimensions
            if(isset($p["ProductDescription"]['PackageDimensions']) && !empty($p["ProductDescription"]['PackageDimensions']))
	    {
		$PackageDimensions = $dom->createElement("PackageDimensions");
		$DescriptionData->appendChild($PackageDimensions);
		
                if(isset($p["ProductDescription"]['PackageDimensions']["depth"]['value']))
		{
		    $Length = $dom->createElement("Length");
		    $PackageDimensions->appendChild($Length);

		    $LengthText = $dom->createTextNode($p["ProductDescription"]['PackageDimensions']["depth"]['value']);
		    $Length->appendChild($LengthText);
		    
                    if(isset($p["ProductDescription"]['PackageDimensions']["depth"]['unit'])){
                        $Length->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['PackageDimensions']["depth"]['unit']));
                    }
                }
		
                if(isset($p["ProductDescription"]['PackageDimensions']["width"]['value']))
		{
		    $Width = $dom->createElement("Width");
		    $PackageDimensions->appendChild($Width);

		    $WidthText = $dom->createTextNode($p["ProductDescription"]['PackageDimensions']["width"]['value']);
		    $Width->appendChild($WidthText);
		    
                    if(isset($p["ProductDescription"]['PackageDimensions']["width"]['unit'])){
                        $Width->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['PackageDimensions']["width"]['unit']));
                    }
                }
		
                if(isset($p["ProductDescription"]['PackageDimensions']["height"]['value']))
		{
		    $Height = $dom->createElement("Height");
		    $PackageDimensions->appendChild($Height);

		    $HeightText = $dom->createTextNode($p["ProductDescription"]['PackageDimensions']["height"]['value']);
		    $Height->appendChild($HeightText);
		    
                    if(isset($p["ProductDescription"]['PackageDimensions']["height"]['unit'])){
                        $Height->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['PackageDimensions']["height"]['unit']));
                    }
                }
		
                if(isset($p["ProductDescription"]['PackageDimensions']["weight"]['value']))
		{
		    $Weight = $dom->createElement("Weight");
		    $PackageDimensions->appendChild($Weight);

		    $WeightText = $dom->createTextNode($p["ProductDescription"]['PackageDimensions']["weight"]['value']);
		    $Weight->appendChild($WeightText);
		    		    
                    if(isset($p["ProductDescription"]['PackageDimensions']["weight"]['unit'])){
                        $Weight->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['PackageDimensions']["weight"]['unit']));
                    }
                }
            }

	    //Package Weight
            if(isset($p["ProductDescription"]['Package']["weight"]['value']) && !empty($p["ProductDescription"]['Package']["weight"]['value']))
	    {
		// Package Weight
		$PackageWeight = $dom->createElement("PackageWeight");
		$DescriptionData->appendChild($PackageWeight);
		    
		$PackageWeightText = $dom->createTextNode($p["ProductDescription"]['Package']["weight"]['value']);
		$PackageWeight->appendChild($PackageWeightText);
		    
                if(isset($p["ProductDescription"]['Package']["weight"]['unit']))
		{
                    $PackageWeight->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['Package']["weight"]['unit']));
                }		
            }
	    
	    // Shipping Weight
	    if(isset($p["ProductDescription"]['Shipping']["weight"]['value']) && !empty($p["ProductDescription"]['Shipping']["weight"]['value']))
	    {	
		// Shipping Weight
		$ShippingWeight = $dom->createElement("ShippingWeight");
		$DescriptionData->appendChild($ShippingWeight);
		
		$ShippingWeightText = $dom->createTextNode($p["ProductDescription"]['Shipping']["weight"]['value']);
		$ShippingWeight->appendChild($ShippingWeightText);
		
                if(isset($p["ProductDescription"]['Shipping']["weight"]['unit']))
		{
                    $ShippingWeight->setAttribute("unitOfMeasure", strtoupper($p["ProductDescription"]['Shipping']["weight"]['unit']));
                }
            }
	    

            if(isset($p["ProductDescription"]["Manufacturer"]) && !empty($p["ProductDescription"]["Manufacturer"]))
	    {
		$Manufacturer = $dom->createElement("Manufacturer");
		$DescriptionData->appendChild($Manufacturer);
		
		$ManufacturerText = $dom->createTextNode($p["ProductDescription"]["Manufacturer"]);
		$Manufacturer->appendChild($ManufacturerText);
            }
             
            //SearchTerms
            if (isset($p["ProductDescription"]['SearchTerms']) && !empty($p["ProductDescription"]['SearchTerms'])) 
	    {
                if (is_array($p["ProductDescription"]['SearchTerms']) && count($p["ProductDescription"]['SearchTerms'])) 
		{
                    foreach ($p["ProductDescription"]['SearchTerms'] as $searchTerms) 
		    {      
                        $SearchTermsTag = $dom->createElement("SearchTerms");
                        $DescriptionData->appendChild($SearchTermsTag);

			$SearchTermsTagText = $dom->createTextNode($searchTerms);
			$SearchTermsTag->appendChild($SearchTermsTagText);
                    }
		    
                } 
		elseif (is_string($p["ProductDescription"]['SearchTerms']) && !empty($p["ProductDescription"]['SearchTerms'])) 
		{
		    $SearchTermsTag = $dom->createElement("SearchTerms");
		    $DescriptionData->appendChild($SearchTermsTag);

		    $SearchTermsTagText = $dom->createTextNode($p["ProductDescription"]['SearchTerms']);
		    $SearchTermsTag->appendChild($SearchTermsTagText);		
                }
            }

            if(isset($p["ProductDescription"]["ItemType"]) && !empty($p["ProductDescription"]["ItemType"]))
	    {
                $ItemType = $dom->createElement("ItemType");
                $DescriptionData->appendChild($ItemType);

		$ItemTypeText = $dom->createTextNode($p["ProductDescription"]["ItemType"]);
		$ItemType->appendChild($ItemTypeText);	
            }
            
            if(isset($p["ProductDescription"]["IsGiftWrapAvailable"]))
	    {
                $IsGiftWrapAvailable = $dom->createElement("IsGiftWrapAvailable");
                $DescriptionData->appendChild($IsGiftWrapAvailable);                

		$IsGiftWrapAvailableText = $dom->createTextNode($p["ProductDescription"]['IsGiftWrapAvailable'] ? 'true' : 'false');
		$IsGiftWrapAvailable->appendChild($IsGiftWrapAvailableText);	
            }
            
            if(isset($p["ProductDescription"]["IsGiftMessageAvailable"]))
	    {                         
                $IsGiftMessageAvailable = $dom->createElement("IsGiftMessageAvailable");
                $DescriptionData->appendChild($IsGiftMessageAvailable);

		$IsGiftMessageAvailableText = $dom->createTextNode($p["ProductDescription"]['IsGiftMessageAvailable'] ? 'true' : 'false');
		$IsGiftMessageAvailable->appendChild($IsGiftMessageAvailableText);
            }
                
            if(isset($p["ProductDescription"]["MfrPartNumber"]) && !empty($p["ProductDescription"]["MfrPartNumber"]))
	    {
                $MfrPartNumber = $dom->createElement("MfrPartNumber");
                $DescriptionData->appendChild($MfrPartNumber);

		$MfrPartNumberText = $dom->createTextNode($p["ProductDescription"]["MfrPartNumber"]);
		$MfrPartNumber->appendChild($MfrPartNumberText);
            }
            
            if(isset($p["ProductDescription"]["RecommendedBrowseNode"]) && !empty($p["ProductDescription"]["RecommendedBrowseNode"]) )
	    {
                foreach($p["ProductDescription"]["RecommendedBrowseNode"] as $browsenode)
		{
                    if ( empty($browsenode) || ! is_numeric($browsenode) )  
			continue ;

                    $RecommendedBrowseNodeTag = $dom->createElement("RecommendedBrowseNode");
                    $DescriptionData->appendChild($RecommendedBrowseNodeTag);
		    
                    $RecommendedBrowseNodeText = $dom->createTextNode($browsenode);
                    $RecommendedBrowseNodeTag->appendChild($RecommendedBrowseNodeText);                    
                }
            }
            
            // DistributionDesignation
            if(isset($p["ProductDescription"]["DistributionDesignation"]) && !empty($p["ProductDescription"]["DistributionDesignation"]) )
	    {
                $DistributionDesignationTag = $dom->createElement("DistributionDesignation");
                $DescriptionData->appendChild($DistributionDesignationTag);
                $DistributionDesignationText = $dom->createTextNode($p["ProductDescription"]["DistributionDesignation"]);
                $DistributionDesignationTag->appendChild($DistributionDesignationText);
            } 


	    // ['ProductDescription']['MerchantShippingGroupName']
            if(isset($p['ProductDescription']['MerchantShippingGroupName']) && !empty($p['ProductDescription']['MerchantShippingGroupName']) )
	    {
		$ShippingGroupName = $dom->createElement("MerchantShippingGroupName");
                $DescriptionData->appendChild($ShippingGroupName);		 

		$ShippingGroupNameText = $dom->createTextNode($p["ProductDescription"]["MerchantShippingGroupName"]);
		$ShippingGroupName->appendChild($ShippingGroupNameText); 
            }   
        } 
        
        if(isset($content) && !empty($content)){
            
            $ProductData = $dom->createElement("ProductData");
            $Product->appendChild($ProductData);
            
            if(isset(self::$productXml[$content["product_category"]][$content['product_type']]) 
                && !empty(self::$productXml[$content["product_category"]][$content['product_type']]) )
	    {
                $product_Xml = self::$productXml[$content["product_category"]][$content['product_type']];
            } else {
                self::$productXml[$content["product_category"]][$content['product_type']] = new AmazonXmlProductData($content["product_category"],$content['product_type']);
                $product_Xml = self::$productXml[$content["product_category"]][$content['product_type']];
            }
            
            $structure = $product_Xml->getProductType($content);
                        
            if($structure && $structure instanceof DOMElement)
	    {
                $ProductData->appendChild($dom->importNode($structure->cloneNode(true), true));
            }
	    elseif($structure && ($structure instanceof DOMNodeList || is_array($structure)) )
	    {
                foreach($structure as $st)
		{
                     $ProductData->appendChild($dom->importNode($st->cloneNode(true), true));
                }
            }

            if(!isset($structure) || $structure == false)
            {
                AmazonXmlValidator::displayError( '[SKU:'.$p['SKU'].'] '. Amazon_Tools::l("Product Category").': '.$content["product_category"] .', ' .Amazon_Tools::l("Product Type"). ": " .$content['product_type'].  " > " . Amazon_Tools::l("The product data was incorrect"), 29);
            }           
            
        }
                
        //RegisteredParameter
        if(isset($p["RegisteredParameter"]) && !empty($p["RegisteredParameter"]))
	{
            $RegisteredParameter = $dom->createElement("RegisteredParameter");
            $Product->appendChild($RegisteredParameter);

	    $RegisteredParameterText = $dom->createTextNode($p["RegisteredParameter"]);
	    $RegisteredParameter->appendChild($RegisteredParameterText);   
        }
                
        $message->appendChild($Product);
        
    }
    
    /**
     * Get XML Feed Type: Inventory
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlInventory(DOMDocument &$dom, DOMElement &$message, array $p){

        $Inventory = $dom->createElement("Inventory");

        if (isset($p["SKU"]) && !empty($p["SKU"])){
            $SKU = $dom->createElement("SKU");
            $SKU->nodeValue = $p["SKU"]; 
            $Inventory->appendChild($SKU);
        }
        
        if (isset($p["FBA"])){ 
            $FulfillmentCenterID = $dom->createElement("FulfillmentCenterID");
            $FulfillmentCenterID->nodeValue = $p["FBA"]; 
            $Inventory->appendChild($FulfillmentCenterID);

            $Lookup = $dom->createElement('Lookup');
            $Inventory->appendChild($Lookup);
            $Lookup->appendChild($dom->createTextNode('FulfillmentNetwork'));
        } else
       
        /********************************************************************
         *          Choice Element: Available / Quantity / Lookup
         ********************************************************************/
        /*if (isset($p["XXX"]) /*isset($p["available_date"]) && !empty($p["available_date"])){
            $Available = $dom->createElement("Available");
            $Available->nodeValue = $p["available_date"]; 
            $Inventory->appendChild($Available);
        }else*/
        if(isset($p["quantity"])/* && !empty($p["quantity"])*/){
	    
            $Quantity = $dom->createElement("Quantity");
            $Quantity->nodeValue = $p["quantity"]; 
            $Inventory->appendChild($Quantity);
	    
            /*if($p["quantity"] == 0){
                $Quantity->setAttribute("zero", "true");
            }*/
	    /*elseif(isset($p["XXX"])){
		$Lookup = $dom->createElement("Lookup");
		$Lookup->nodeValue = "FulfillmentNetwork"; //This is the only valid value
		$Inventory->appendChild($Lookup);
	    }

	    if (isset($p["XXX"])){
		$RestockDate = $dom->createElement("RestockDate");
		$RestockDate->nodeValue = $p["XXX"]; 
		$Inventory->appendChild($RestockDate);
	    }*/

	    if (isset($p["FulfillmentLatency"]) && !empty($p["FulfillmentLatency"])){
		$FulfillmentLatency = $dom->createElement("FulfillmentLatency");
		$FulfillmentLatency->nodeValue = $p["FulfillmentLatency"]; 
		$Inventory->appendChild($FulfillmentLatency);
	    }
        }

        if (isset($p["SwitchFulfillmentTo"])&& !empty($p["SwitchFulfillmentTo"]) ){
            $SwitchFulfillmentTo = $dom->createElement("SwitchFulfillmentTo");
            $SwitchFulfillmentTo->nodeValue = $p["SwitchFulfillmentTo"];  //value can be: MFN or AFN
            $Inventory->appendChild($SwitchFulfillmentTo);
        }
        
        $message->appendChild($Inventory);
        
    }
    
    /**
     * Get XML Feed Type: Price
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlPrice(DOMDocument &$dom, DOMElement &$message, array $p){
        
        $Price = $dom->createElement("Price");
        
        if (isset($p["SKU"]) && !empty($p["SKU"])){
            $SKU = $dom->createElement("SKU");
            $SKU->nodeValue = $p["SKU"]; 
            $Price->appendChild($SKU);
        }
        
        /*******************************************************************
         *                    AVAILABLE CURRENCY CODES
         *    USD, GBP, EUR, JPY, CAD, CNY, INR, AUD, BRL, MXN, DEFAULT
         *******************************************************************/
        if(isset(AmazonXml::$user->currency) && !empty(AmazonXml::$user->currency))
            $currency_code = trim(strtoupper(AmazonXml::$user->currency));
        else
            $currency_code = "DEFAULT";
        
        if(isset($p["rounding"]))
            $rounding = $p["rounding"] ;
        else
            $rounding = null ;

        if(isset($p["price"]) && !empty($p["price"])){
            
            $price = isset($rounding) ? sprintf('%.02f', round($p["price"], $rounding)) : sprintf('%.02f', $p["price"]) ;

            //"StandardPrice";
            $StandardPrice = $dom->createElement("StandardPrice");
            $StandardPrice->nodeValue = $price; //round($p["price"],4);
            $StandardPrice->setAttribute("currency", $currency_code);
            $Price->appendChild($StandardPrice);
        } 
        /*else {
            $StandardPrice = $dom->createElement("StandardPrice");
            $StandardPrice->nodeValue = 0;
            $StandardPrice->setAttribute("currency", $currency_code);
            $Price->appendChild($StandardPrice);
        }*/

       if(isset($p["MinPrice"]) && !empty($p["MinPrice"])){
            $MinimumSellerAllowedPrice = $dom->createElement("MinimumSellerAllowedPrice");
            if($currency_code == "EUR"){
                $MinimumSellerAllowedPrice->nodeValue = number_format($p["MinPrice"], 2, ',', ' ');
            } else if($currency_code == "JPY") {
                $MinimumSellerAllowedPrice->nodeValue = floor($p["MinPrice"]);
            } else {
                $MinimumSellerAllowedPrice->nodeValue = $p["MinPrice"];
            }
            $MinimumSellerAllowedPrice->setAttribute("currency", $currency_code);
            $Price->appendChild($MinimumSellerAllowedPrice);
        }
        
        if(isset($p["MaxPrice"]) && !empty($p["MaxPrice"])){
            $MaximumSellerAllowedPrice = $dom->createElement("MaximumSellerAllowedPrice");
            if($currency_code == "EUR"){
                $MaximumSellerAllowedPrice->nodeValue = number_format($p["MaxPrice"], 2, ',', ' ');
            } else if($currency_code == "JPY") {
                $MaximumSellerAllowedPrice->nodeValue = ceil($p["MaxPrice"]);
            } else {
                $MaximumSellerAllowedPrice->nodeValue = $p["MaxPrice"];
            }
            $MaximumSellerAllowedPrice->setAttribute("currency", $currency_code);
            $Price->appendChild($MaximumSellerAllowedPrice);
        }        
        
        if(isset($p["sale"]) && !empty($p["sale"])){
            $Sale = $dom->createElement("Sale");
            
            if(isset($p["sale"]["date_from"]) && !empty($p["sale"]["date_from"])){
                $StartDate = $dom->createElement("StartDate");
                $StartDate->nodeValue = $p["sale"]["date_from"];
                $Sale->appendChild($StartDate);
            }
            
            if(isset($p["sale"]["date_to"]) && !empty($p["sale"]["date_to"])){
                $EndDate = $dom->createElement("EndDate");
                $EndDate->nodeValue = $p["sale"]["date_to"];
                $Sale->appendChild($EndDate);
            }
            
            if(isset($p["sale"]["price"]) && !empty($p["sale"]["price"])){

                $sale_price = isset($rounding) ? sprintf('%.02f', round($p["sale"]["price"], $rounding)) : sprintf('%.02f', $p["sale"]["price"]) ;

                $SalePrice = $dom->createElement("SalePrice");
                $SalePrice->nodeValue = $sale_price;
                if($sale_price == 0){
                    $SalePrice->setAttribute("zero", "true");
                }
                $SalePrice->setAttribute("currency", $currency_code);
                $Sale->appendChild($SalePrice);
            }
            
            $Price->appendChild($Sale);
        }
        
        /*
        if(isset($p["XXX"])){
            $MAP = $dom->createElement("MAP");
            $MAP->nodeValue = $p["XXX"];
            if(round($p["XXX"]) == 0){
                $StandardPrice->setAttribute("zero", "true");
            }
            $MAP->setAttribute("currency", $currency_code);
            $Price->appendChild($MAP);
        }

        if(isset($p["XXX"])){
            $DepositAmount = $dom->createElement("DepositAmount");
            $DepositAmount->nodeValue = $p["XXX"];
            if(round($p["XXX"]) == 0){
                $StandardPrice->setAttribute("zero", "true");
            }
            $DepositAmount->setAttribute("currency", $currency_code);
            $Price->appendChild($DepositAmount);
        }*/

        /*if(isset($p["XXX"])){
            $CompareAt = $dom->createElement("CompareAt");
            $CompareAt->setAttribute("delete", false);
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX"];
            $CompareAt->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX"];
            $CompareAt->appendChild($EndDate);
            
            $CompareAtPrice = $dom->createElement("CompareAtPrice");
            $CompareAtPrice->nodeValue = $p["XXX"];
            $CompareAtPrice->setAttribute("currency", $currency_code);
            $CompareAt->appendChild($CompareAtPrice);
            
            $Price->appendChild($CompareAt);
        }
        
        if(isset($p["XXX"])){
            $Previous = $dom->createElement("Previous");
            $Previous->setAttribute("delete", false);
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX"];
            $Previous->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX"];
            $Previous->appendChild($EndDate);
            
            $PreviousPrice = $dom->createElement("PreviousPrice");
            $PreviousPrice->nodeValue = $p["XXX"];
            $PreviousPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($PreviousPrice);
            
            $Price->appendChild($Previous);
        }*/
        
        /*******************************************************************
         *                    RENTAL 0 to 9
         *******************************************************************/
        /*if(isset($p["XXX_0"])){
            $Rental_0 = $dom->createElement("Rental_0");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_0"];
            $Rental_0->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_0"];
            $Rental_0->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_0"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_0);
        }
        if(isset($p["XXX_1"])){
            $Rental_1 = $dom->createElement("Rental_1");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_1"];
            $Rental_1->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_1"];
            $Rental_1->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_1"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_1);
        }
        if(isset($p["XXX_2"])){
            $Rental_2 = $dom->createElement("Rental_2");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_2"];
            $Rental_2->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_2"];
            $Rental_2->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_2"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_2);
        }
        if(isset($p["XXX_3"])){
            $Rental_3 = $dom->createElement("Rental_3");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_3"];
            $Rental_3->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_3"];
            $Rental_3->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_3"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_3);
        }
        if(isset($p["XXX_4"])){
            $Rental_4 = $dom->createElement("Rental_4");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_4"];
            $Rental_4->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_4"];
            $Rental_4->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_4"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_4);
        }
        if(isset($p["XXX_5"])){
            $Rental_5 = $dom->createElement("Rental_5");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_5"];
            $Rental_5->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_5"];
            $Rental_5->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_5"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_5);
        }
        if(isset($p["XXX_6"])){
            $Rental_6 = $dom->createElement("Rental_6");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_6"];
            $Rental_6->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_6"];
            $Rental_6->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_6"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_6);
        }
        if(isset($p["XXX_7"])){
            $Rental_7 = $dom->createElement("Rental_7");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_7"];
            $Rental_7->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_7"];
            $Rental_7->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_7"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_7);
        }
        if(isset($p["XXX_8"])){
            $Rental_8 = $dom->createElement("Rental_8");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_8"];
            $Rental_8->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_8"];
            $Rental_8->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_8"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_8);
        }
        if(isset($p["XXX_9"])){
            $Rental_9 = $dom->createElement("Rental_9");
            
            $StartDate = $dom->createElement("StartDate");
            $StartDate->nodeValue = $p["XXX_9"];
            $Rental_9->appendChild($StartDate);
            
            $EndDate = $dom->createElement("EndDate");
            $EndDate->nodeValue = $p["XXX_9"];
            $Rental_9->appendChild($EndDate);
            
            $RentalPrice = $dom->createElement("RentalPrice");
            $RentalPrice->nodeValue = $p["XXX_9"];
            if(round($p["XXX"]) == 0){
                $RentalPrice->setAttribute("zero", "true");
            }
            $RentalPrice->setAttribute("currency", $currency_code);
            $Previous->appendChild($RentalPrice);
            
            $Price->appendChild($Rental_9);
        }
        if(isset($p["XXX"])){
            $CostPerClickBidPrice = $dom->createElement("CostPerClickBidPrice");
            $CostPerClickBidPrice->nodeValue = $p["XXX_9"];
            if(round($p["XXX"]) == 0){
                $CostPerClickBidPrice->setAttribute("zero", "true");
            }
            $Price->setAttribute("currency", $CostPerClickBidPrice);
        }*/
        
        $message->appendChild($Price);
        
    }
        
    /**
     * Get XML Feed Type: Image
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlImage(DOMDocument &$dom, DOMElement &$message, array $p){
        
        $ProductImage = $dom->createElement("ProductImage");
        
        if (isset($p["SKU"]) && !empty($p["SKU"])){
            $SKU = $dom->createElement("SKU");
            $SKU->nodeValue = $p["SKU"]; 
            $ProductImage->appendChild($SKU);
        }
        
        if (isset($p["type"])){
            $ImageType = $dom->createElement("ImageType");
            $ImageType->nodeValue = $p["type"]; 
            $ProductImage->appendChild($ImageType);
        }        
        if (isset($p["url"])){
            $ImageLocation = $dom->createElement("ImageLocation");
            $ImageLocation->nodeValue = $p["url"]; 
            $ProductImage->appendChild($ImageLocation);
        }
        
        $message->appendChild($ProductImage);
    }
    
    /**
     * Get XML Feed Type: Image
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlRelationship(DOMDocument &$dom, DOMElement &$message, array $p){
        
        $Relationship = $dom->createElement("Relationship");
        
        // Parent
        if (isset($p["parent"]) && !empty($p["parent"])){
            $ParentSKU = $dom->createElement("ParentSKU");
            $ParentSKU->nodeValue = $p["parent"]; 
            $Relationship->appendChild($ParentSKU);
        }
        
        if(isset($p["children"]) && !empty($p["children"]))
        {
            foreach ($p["children"] as $child)
            {
                $Relation = $dom->createElement("Relation");
                
                // Child SKU
                $Sku = $dom->createElement("SKU");
                $Sku->nodeValue = $child; 
                $Relation->appendChild($Sku);
                
                // Child Type
                $Type = $dom->createElement("Type");
                $Type->nodeValue = 'Variation'; 
                $Relation->appendChild($Type);
                
                $Relationship->appendChild($Relation);
            }
        }

        $message->appendChild($Relationship);
    }  
    
    /**
     * Get XML Feed Type: Image
     * @param DOMDocument $dom
     * @param DOMElement $message
     * @param array $p
     */
    public static function getXmlDelete(DOMDocument &$dom, DOMElement &$message, array $p){
                
        $Delete = $dom->createElement("Product");
        
        if (isset($p["SKU"]) && !empty($p["SKU"])){
            $SKU = $dom->createElement("SKU");
            $SKU->nodeValue = $p["SKU"]; 
            $Delete->appendChild($SKU);
        }

        $message->appendChild($Delete);
    }    
    
    public static function getXmlOrderFulfillment(DOMDocument $Document, DOMElement &$message, array $p, $timestamp = null)
    {
        if(isset($p['AmazonOrderID']) && !empty($p['AmazonOrderID']))
        {
            $OrderFulfillment = $Document->createElement("OrderFulfillment");
            $message->appendChild($OrderFulfillment);
            
            $AmazonOrderID = $Document->createElement("AmazonOrderID");
            $OrderFulfillment->appendChild($AmazonOrderID);
        
            $AmazonOrderIDText = $Document->createTextNode($p['AmazonOrderID']);
            $AmazonOrderID->appendChild($AmazonOrderIDText);
        
            $FulfillmentDate = $Document->createElement("FulfillmentDate");
            $OrderFulfillment->appendChild($FulfillmentDate);

            if ($timestamp == null || !is_numeric($timestamp)) {
                $t = time() - 5400;               
            } else {
                $t = $timestamp;
            }

            $date = date("c", $t);
            $FulfillmentDateText = $Document->createTextNode($date);
            $FulfillmentDate->appendChild($FulfillmentDateText);

            //FulfillmentData
            if ( isset($p['CarrierCode']) || isset($p['CarrierName']) )
            {
                $FulfillmentData = $Document->createElement("FulfillmentData");
                $OrderFulfillment->appendChild($FulfillmentData);
                
                if ( isset($p['CarrierCode']) && !empty($p['CarrierCode']) )
                {
                    $CarrierCode = $Document->createElement("CarrierCode");
                    $FulfillmentData->appendChild($CarrierCode);
                    $CarrierCodeText = $Document->createTextNode($p['CarrierCode']);
                    $CarrierCode->appendChild($CarrierCodeText);                
                }
                elseif ( isset($p['CarrierName']) && !empty($p['CarrierName']) )
                {
                    $CarrierName = $Document->createElement("CarrierName");
                    $FulfillmentData->appendChild($CarrierName);
                    $CarrierNameText = $Document->createTextNode($p['CarrierName']);
                    $CarrierName->appendChild($CarrierNameText);                
                }
                
                if (isset($p['ShippingMethod']) && !empty($p['ShippingMethod']))
                {
                    $ShippingMethod = $Document->createElement("ShippingMethod");
                    $FulfillmentData->appendChild($ShippingMethod);
                    $ShippingMethodText = $Document->createTextNode($p['ShippingMethod']);
                    $ShippingMethod->appendChild($ShippingMethodText);
                }
                
                if( isset($p['ShipperTrackingNumber']) && !empty($p['ShipperTrackingNumber']) )
                {
                    $ShipperTrackingNumber = $Document->createElement("ShipperTrackingNumber");
                    $FulfillmentData->appendChild($ShipperTrackingNumber);
                    $ShipperTrackingNumberText = $Document->createTextNode($p['ShipperTrackingNumber']);
                    $ShipperTrackingNumber->appendChild($ShipperTrackingNumberText);
                }
            }
        }

        return $message;
    }
    
    public static function getXmlOverride(DOMDocument &$dom, DOMElement &$message, array $p, $oMode){
        
        if(isset(AmazonXml::$user->currency) && !empty(AmazonXml::$user->currency)){
            $currency_code = trim(strtoupper(AmazonXml::$user->currency));
        } else {
            $currency_code = "DEFAULT";
        }
        
        $Override = $dom->createElement("Override");
        
        if (isset($p["SKU"]) && !empty($p["SKU"])){
            $SKU = $dom->createElement("SKU");
            $SKU->nodeValue = $p["SKU"]; 
            $Override->appendChild($SKU);
        }
        if (isset($p["Shipping"]) && !empty($p["Shipping"])){
            
            $ShippingOverride = $dom->createElement("ShippingOverride");
                
            if(isset($p["Shipping"]["Option"])){
                $ShipOption = $dom->createElement("ShipOption");
                $ShipOption->nodeValue = $p["Shipping"]["Option"]; 
                $ShippingOverride->appendChild($ShipOption);                                      
            }
            if(isset($p["Shipping"]["Type"])){
                $Type = $dom->createElement("Type");
                $Type->nodeValue = $p["Shipping"]["Type"]; 
                $ShippingOverride->appendChild($Type);                                      
            }

            if($oMode == 'Delete'){ // 2016-03-30 // Delete when shipping = 0

                $ShipAmount = $dom->createElement("ShipAmount");
                $ShipAmount->nodeValue = 0 ;
                $ShipAmount->setAttribute("currency", $currency_code);
                $ShippingOverride->appendChild($ShipAmount);

            } else {

                if(isset($p["Shipping"]["Amount"])){
                    $ShipAmount = $dom->createElement("ShipAmount");
                    $ShipAmount->nodeValue = $p["Shipping"]["Amount"];
                    $ShipAmount->setAttribute("currency", $currency_code);
                    $ShippingOverride->appendChild($ShipAmount);
                }
            }
            $Override->appendChild($ShippingOverride);
        }   
            

        $message->appendChild($Override);
    }  
    
    public static function getXmlOrderAcknowledgement(DOMDocument &$Document, DOMElement &$message,  array $order)
    {	
	$OrderAcknowledgement = $Document->createElement("OrderAcknowledgement");
	$message->appendChild($OrderAcknowledgement);

	$AmazonOrderID = $Document->createElement("AmazonOrderID");
	$OrderAcknowledgement->appendChild($AmazonOrderID);
	$AmazonOrderIDText = $Document->createTextNode($order['AmazonOrderID']);
	$AmazonOrderID->appendChild($AmazonOrderIDText);

	$MerchandOrderID = $Document->createElement('MerchantOrderID');
        $OrderAcknowledgement->appendChild($MerchandOrderID);	
        $MerchandOrderIDText = $Document->createTextNode($order['MerchantOrderID']);
        $MerchandOrderID->appendChild($MerchandOrderIDText);

        $StatusCode = $Document->createElement('StatusCode');
        $OrderAcknowledgement->appendChild($StatusCode);
        $StatusCodeText = $Document->createTextNode(isset($order['status']) ? ucfirst((string) $order['status']) : 'Success');
        $StatusCode->appendChild($StatusCodeText);	

        if (is_array($order['items']) && count($order['items'])) {
            $OrderAcknowledgement->appendChild($Items = $Document->createElement('Items'));

            foreach($order['items'] as $item) {
                $Items->appendChild($Item = $Document->createElement('Item'));

                if (array_key_exists('order_item_id', $item) && strlen($item['order_item_id'])) {
                    $Item->appendChild($Document->createElement('AmazonOrderItemCode', $item['order_item_id']));
                }
                if (array_key_exists('sku', $item) && strlen($item['sku'])) {
                    $Item->appendChild($Document->createElement('MerchantOrderItemID', $item['sku']));
                }
                if (array_key_exists('reason', $item) && strlen($item['reason'])) {
                    $Item->appendChild($Document->createElement('CancelReason', $item['reason']));
                }
            }
        }

        return $message;
    }
    
    private static function setErrorLogs($feed_type){
        
        global $error_logs;
      
        if(isset($error_logs[$feed_type]) && !empty($error_logs[$feed_type])){
            
            $dom =  new DOMDocument();
            $dom->formatOutput = true;

            //Create AmazonEnvelope tag
            $AmazonEnvelope = $dom->createElement("AmazonEnvelope");
            $AmazonEnvelope->setAttribute("xsi:noNamespaceSchemaLocation", "amzn-envelope.xsd");
            $AmazonEnvelope->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            $dom->appendChild($AmazonEnvelope);

            //Create Header tag
            $Header = $dom->createElement("Header");
            //Create DocumentVersion tag
            $DocumentVersion = $dom->createElement("DocumentVersion");
            $DocumentVersion->nodeValue = "1.01";
            $Header->appendChild($DocumentVersion);
            //Create MerchantIdentifier tag
            $MerchantIdentifier = $dom->createElement("MerchantIdentifier");        
            $MerchantIdentifier->nodeValue = AmazonXml::$user->merchant_id;
            $Header->appendChild($MerchantIdentifier);
            $AmazonEnvelope->appendChild($Header);

            //Create MessageType tag
            $MessageType = $dom->createElement("MessageType");
            $MessageType->nodeValue = "Error";
            $AmazonEnvelope->appendChild($MessageType);
                        
            foreach ($error_logs[$feed_type] as $id => $errors) {

                $message = $dom->createElement("Message");
                $messageID = $dom->createElement("Universe");
                $messageID->nodeValue = $id;
                $message->appendChild($messageID);               
                
                if(is_array($errors)){
                    foreach ($errors as $id => $error) {
                        if(isset($error['message']) && !empty($error['message'])){
                            $messageValue = $dom->createElement("Error");
                            $messageValue->setAttribute("ID", $id);
                            $message->appendChild($messageValue);

                            $ErrorMessage = $dom->createElement('ErrorMessage');
                            $ErrorMessage->nodeValue = htmlspecialchars($error['message']);
                            $messageValue->appendChild($ErrorMessage);

                            if(isset($error['sku']) && !empty($error['sku'])){
                                foreach ($error['sku'] as $sku => $sku_value) {
                                    $messageSKU = $dom->createElement("SKU");
                                    $messageSKU->nodeValue = $sku;
                                    $messageValue->appendChild($messageSKU);
                                }
                            }
                        }
                    }
                }

                $AmazonEnvelope->appendChild($message);
            }

            return $dom->saveXML();
        }
    }
    
}