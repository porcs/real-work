<?php

require_once(dirname(__FILE__).'/amazon.order.php');
require_once(dirname(__FILE__).'/amazon.product.php');
require_once(dirname(__FILE__).'/amazon.webservice.php');
require_once(dirname(__FILE__).'/../../FeedBiz/products/MarketplaceProductOption.php');
require_once(dirname(__FILE__) . '/../../eucurrency.php');

class AmazonMultiChannel extends Amazon_Order
{    
    const AMAZON_FBA_STATUS_SUBMITED = 'submited';
    const AMAZON_FBA_STATUS_RECEIVED = 'received';
    const AMAZON_FBA_STATUS_INVALID = 'invalid';
    const AMAZON_FBA_STATUS_PLANNING = 'planning';
    const AMAZON_FBA_STATUS_PROCESSING = 'processing';
    const AMAZON_FBA_STATUS_CANCELLED = 'cancelled';
    const AMAZON_FBA_STATUS_COMPLETE = 'complete';
    const AMAZON_FBA_STATUS_COMPLETEPARTIALLED = 'completepartialled';
    const AMAZON_FBA_STATUS_UNFULFILLABLE = 'unfulfillable';
    public static $errors                   = array();
    public $marketPlaceChannelStatus = null;

    public function __construct($user_name, $debug = false, $only_order_db = false)
    {	
	parent::__construct($user_name, $only_order_db, $debug) ;

	$this->debug = $debug;	
    }

    public function getMpStatus($id)
    {
        $sql = 'SELECT mp_channel_status FROM '.Amazon_Order::$m_pref .'mp_multichannel WHERE id_orders = '.(int)$id.' ; ';

	$result = $this->db->order->db_query_str($sql);
        foreach ($result as $value) {
	    $this->marketPlaceChannelStatus = $value['mp_channel_status'];
	    return (true);
        }
	
	return (false);
        
    }

    public function ordersByStatus($ps_status)
    {
	$statuses = rtrim(implode(', ', $ps_status));
	
        $sql = 'SELECT mp.id_orders, mp.seller_order_id, mp.mp_channel_status, os.tracking_number, date_add
		FROM '.Amazon_Order::$m_pref .'mp_multichannel mp 
		LEFT JOIN '.Amazon_Order::$or_pref .'order_shipping os ON os.id_orders = mp.id_orders  
		WHERE mp.mp_channel = "'.(self::AMAZON_FBA_MULTICHANNEL).'"
		AND mp.mp_status IN ('.$statuses.')
		AND mp.date_add > DATE_ADD(NOW(), INTERVAL -30 DAY)
		AND (os.tracking_number IS NULL OR os.tracking_number = "") 
		ORDER by mp.date_add ASC';
	
	if ($this->debug) {
	    echo "<pre>";
	    printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
	    echo $sql;
	    echo "</pre>\n";
	}
	
        if (!$result = $this->db->order->db_query_str($sql)) {
            return false;
        }

        return ($result);
    }

    public function isEligible($orders, $id_shop=null, $id_country=null, $id_marketplace=null, $check_order_item=true)
    {	
	$MasterFBA = AmazonUserData::getMasterFBA($this->user, $id_country, $id_shop, false);
	$id_country = $MasterFBA['id_country'];
	
	// get order item by id_order
	if(empty($orders) || !is_array($orders))
	{
	    if ($this->debug) {
                echo "<pre>Amazon, Debug Mode\n";
                printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
                echo 'Missing orders\n';
                echo "</pre>\n";
            }
	    return (false);
	}
	
	$list_order = array();
	foreach ($orders as $order){
	    if(isset($order['id_orders']) && !empty($order['id_orders']))
		$list_order[] = $order['id_orders'];		
	}
	
	$order_item = $this->getOrderItemById($list_order);
		
        if (!$order_item || !is_array($order_item) || !count($order_item)) {
            if ($this->debug) {
                echo "<pre>Amazon, Debug Mode\n";
                printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
                echo 'Order returned no products ('.implode(', ', $list_order).') \n';
                echo '</pre>\n';
            }
            return (false);
        }
	
	
        // Require all the ordered products are FBA
        //
	if($check_order_item) {

	    $AmazonProductOption = new MarketplaceProductOption($this->user, null, null, null, null, $id_country, $id_marketplace);	   

	    foreach ($order_item as $product) {
		 
		if (!Amazon_Tools::ValidateSKU($product['reference'])) {
		    if ($this->debug) {
			echo "<pre>Amazon, Debug Mode\n";
			printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
			echo 'AmazonTools::ValidateSKU, invalid SKU ('.$product['product_reference'].")\n";
			echo "</pre>\n";
		    }
		    return (false);
		}
		
		// Product Options
                $options = $AmazonProductOption->get_product_option_by_sku($product['reference'], $id_shop);
                if ($this->debug) {
                    echo "<pre>Product Options : ";
                    echo $product['reference']. " : ";
                    echo print_r($options, true);
                    echo "</pre>\n";
                }
		if (!$options) {
		    if ($this->debug) {
			echo "<pre>Amazon, Debug Mode\n";
			printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
			echo 'No product options available for this product ('.$product['reference'].")\n";
			echo "</pre>\n";
		    }
		    return (false);
		}

		if (!isset($options['fba']) || !(bool)$options['fba']) {
		    if ($this->debug) {
			echo "<pre>Amazon, Debug Mode\n";
			printf('%s, line %d'."\n", basename(__FILE__), __LINE__);
			echo 'FBA flag is not set to on ('.$product['reference'].")\n";
			echo "</pre>\n";
		    }
		    return (false);
		}
	    }
	}
	
        return ($orders);
    }

    public function CancelFulfillmentOrder($order_id)
    {    
        $result = $this->api->CancelFulfillmentOrder($order_id);
	
	if(!$result){
	    $error = sprintf(Amazon_Tools::l('%s(#%d): Impossible to retrieve the order from Amazon'), basename(__FILE__), __LINE__) ;
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            return (false);
	} 	
	else
        {            	   	    	    
            // update mp_multichannel	    	    
	    if(!$this->updateMpChannelStatus($order_id, self::AMAZON_FBA_STATUS_CANCELLED))
	    {
		$error = sprintf(Amazon_Tools::l('Unable to update MpChannel the order: %s'), $order_id);
		if ($this->debug)
		{
		    print $error."<br />\n";
		}
		self::$errors[] = $error;
		return (false);
	    }	
        }
	
        return ($result);
    }
    
    public function updateSellerOrderId($id, $sellerOrderId)
    {
	$sql = 'UPDATE '.Amazon_Order::$m_pref .'mp_multichannel
		SET seller_order_id= "'.($sellerOrderId).'"
		WHERE id_orders = '.(int) $id . '; ';
	
        if (!$this->db->order->db_exec($sql)) {
            return (false);
        }

        return (true);
    }
    
    public function updateMpChannel($id, $marketPlaceChannel)
    {
        $this->marketPlaceChannel = $marketPlaceChannel;
	
	$sql = 'UPDATE '.Amazon_Order::$m_pref .'mp_multichannel
		SET mp_channel = "'.($this->marketPlaceChannel).'"
		WHERE id_orders = '.(int) $id . '; ';
	
        if (!$this->db->order->db_exec($sql)) {
            return (false);
        }

        return (true);
    }
    
    public function updateMpChannelStatus($id, $status)
    {
        $this->marketPlaceChannelStatus = strtolower($status);

        $sql = 'UPDATE '.Amazon_Order::$m_pref .'mp_multichannel
		SET mp_channel_status = "'.($this->marketPlaceChannelStatus).'"
		WHERE id_orders = '.(int) $id . '; ';
	
	/*$sql = 'UPDATE '.Amazon_Order::$or_pref .'orders
		SET order_status = "'.$status.'"
		WHERE id_orders = '.(int) $id . '; ';*/
	
        if (!$this->db->order->db_exec($sql)) {
            return (false);
        }

        return (true);
    }
    
    public function updateMpStatus($id, $status)
    {
        $sql = 'UPDATE '.Amazon_Order::$m_pref .'mp_multichannel
		SET mp_status = "'.($status).'"
		WHERE id_orders = '.(int) $id . '; ';
	
        if (!$this->db->order->db_exec($sql)) {
            return (false);
        }

        return (true);
    }

    public function GetPackageTrackingDetails($PackageNumber, $id_lang, $debug = false)
    {
        // Init
        //
        $amazon = AmazonTools::selectPlatforms($id_lang, $debug);

        if ($debug) {
            echo nl2br(print_r($amazon['auth'], true).print_r($amazon['params'], true).print_r($amazon['platforms'],
                    true));
        }

        $pass = true;

        if (!($amazonAPI = new AmazonWebService($amazon['auth'], $amazon['params'], $amazon['platforms'], $debug))) {
            $error = 'Unable to login';
            if ($debug) {
                print $error."<br />\n";
            }
            self::$errors[] = $error;

            return (false);
        }

        $result = $amazonAPI->GetPackageTrackingDetails($PackageNumber);

        if (!$result) {
            $error = 'Impossible to retrieve the order from Amazon';
            if ($debug) {
                print $error."<br />\n";
            }

            return (false);
        }

        return ($result);
    }

    public function ListAllFulfillmentOrders($date)
    {
        return $this->api->ListAllFulfillmentOrders($date);
    }
    
    public function GetFulfillmentOrder($order_id, $full_element=false)
    {
	$result = $this->api->GetFulfillmentOrder($order_id);
	
	if(!$result){
	    $error = sprintf(Amazon_Tools::l('%s(#%d): Impossible to retrieve the order from Amazon'), basename(__FILE__), __LINE__) ;
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            return (false);
	} 	
	else if(isset($result->Error)){
	    
	    $error = sprintf(Amazon_Tools::l('Amazon Error - Code : %s, Message : %s'), strval($result->Error->Code), strval($result->Error->Message));    
	    if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;	    
            return (false);
	}
	else
        {        
		    
	    if($full_element)
		return $result;
	    
	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->ReceivedDateTime)) {
		$orderInfo['ReceivedDateTime'] = date('Y-m-d H:i:s', strtotime((string)$result->GetFulfillmentOrderResult->FulfillmentOrder->ReceivedDateTime));
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->StatusUpdatedDateTime)) {
		$orderInfo['StatusUpdatedDateTime'] = date('Y-m-d H:i:s', strtotime((string)$result->GetFulfillmentOrderResult->FulfillmentOrder->StatusUpdatedDateTime));
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->ShippingSpeedCategory)) {
		$orderInfo['ShippingSpeedCategory'] = (string)$result->GetFulfillmentOrderResult->FulfillmentOrder->ShippingSpeedCategory;
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->FulfillmentMethod)) {
		$orderInfo['FulfillmentMethod'] = (string)$result->GetFulfillmentOrderResult->FulfillmentOrder->FulfillmentMethod;
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->FulfillmentOrderStatus)) {
		$orderInfo['FulfillmentOrderStatus'] = (string)$result->GetFulfillmentOrderResult->FulfillmentOrder->FulfillmentOrderStatus;
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DisplayableOrderId)) {
		$orderInfo['DisplayableOrderId'] = (string)$result->GetFulfillmentOrderResult->FulfillmentOrder->DisplayableOrderId;
	    }


	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrderItem->member->EstimatedShipDateTime)) {
		$orderInfo['EstimatedShipDateTime'] = date('Y-m-d', strtotime((string)$result->GetFulfillmentOrderResult->FulfillmentOrderItem->member->EstimatedShipDateTime));
	    }

	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrderItem->member->EstimatedArrivalDateTime)) {
		$orderInfo['EstimatedArrivalDateTime'] = date('Y-m-d',strtotime((string)$result->GetFulfillmentOrderResult->FulfillmentOrderItem->member->EstimatedArrivalDateTime));
	    }
	    
	    // Displayable Order Comment
	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DisplayableOrderComment)) {
		$orderInfo['Comment'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DisplayableOrderComment;
	    }
	    
	    // Displayable Order Comment
	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->IsGift)) {
		$orderInfo['IsGift'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->IsGift;
	    }
	     
	    // Order shipment
	    if(isset($result->GetFulfillmentOrderResult->FulfillmentShipment))
	    {
		// Amazon Shipment Id
		if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->AmazonShipmentId)) {
		    $orderInfo['Shipment']['AmazonShipmentId'] = (string)$result->GetFulfillmentOrderResult->FulfillmentShipment->member->AmazonShipmentId;
		}

		// Fulfillment Shipment Status
		if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentStatus)) {
		    $orderInfo['Shipment']['FulfillmentShipmentStatus'] = (string)$result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentStatus;
		}

		// Tracking Number
		if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->TrackingNumber)) {
		    $orderInfo['Shipment']['TrackingNumber'] = 
			    (string)$result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->TrackingNumber;
		}

		// Tracking Number
		if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode)) {
		    $orderInfo['Shipment']['CarrierCode'] = (string)$result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode;
		}

		// Tracking Number
		if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode)) {
		    $orderInfo['Shipment']['CarrierCode'] = (string)$result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode;
		}
	    }
	    
	    // Order Item
	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrderItem->member)) {
		
		foreach ($result->GetFulfillmentOrderResult->FulfillmentOrderItem->member as $FulfillmentOrderItem)
		{
		    if(isset($FulfillmentOrderItem->SellerSKU)) {
			
			$orderInfo['Items']['Count'] = (string) $FulfillmentOrderItem->count();
			$SKU = (string) $FulfillmentOrderItem->SellerSKU;
			$orderInfo['Items'][$SKU]['SKU'] = $SKU;
			
			if (isset($FulfillmentOrderItem->Quantity)) 
			    $orderInfo['Items'][$SKU]['Quantity'] = (string) $FulfillmentOrderItem->Quantity;
			
			if (isset($FulfillmentOrderItem->PerUnitDeclaredValue->CurrencyCode)) 
			    $orderInfo['Items'][$SKU]['PerUnitDeclaredValueCurrencyCode'] = (string) $FulfillmentOrderItem->PerUnitDeclaredValue->CurrencyCode;
			
			if (isset($FulfillmentOrderItem->PerUnitDeclaredValue->Value)) 
			    $orderInfo['Items'][$SKU]['PerUnitDeclaredValueValue'] = (string) $FulfillmentOrderItem->PerUnitDeclaredValue->Value;
			
		    }
		}
	    }
	    
	    // Order Destination Address
	    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder)) {
		    
		    $Line1 = $Line2 = $Line3 = '';
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->PhoneNumber)) 
			$orderInfo['Address']['PhoneNumber'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->PhoneNumber;
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->City)) 
			$orderInfo['Address']['City'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->City;
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->PostalCode)) 
			$orderInfo['Address']['PostalCode'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->PostalCode;
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Name)) 
			$orderInfo['Address']['Name'] = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Name;
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line1)) 
			$Line1 = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line1 . ' ';
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line2)) 
			$Line2 = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line2 . ' ';
		    
		    if (isset($result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line3)) 
			$Line3 = (string) $result->GetFulfillmentOrderResult->FulfillmentOrder->DestinationAddress->Line3 . ' ';
		    
		    $orderInfo['Address']['Line'] = $Line1 . $Line2 . $Line3;
	    }
	    
	     
	    //	    //$amazonMultiChannelOrder->updateMpChannelStatus($orderInfo['FulfillmentOrderStatus']);
	    //            // update mp_multichannel	    	    
	    //	    if(!$this->updateMpChannelStatus($order_id, $orderInfo['FulfillmentOrderStatus']))
	    //	    {
	    //		$error = sprintf(Amazon_Tools::l('Unable to update MpChannel the order: %s'), $order_id);
	    //		if ($this->debug)
	    //		{
	    //		    print $error."<br />\n";
	    //		}
	    //		self::$errors[] = $error;
	    //		return (false);
	    //	    }	
	    
	    //updateMpStatus
	    
        }
	
	//var_dump($orderInfo); exit;
        return ($orderInfo);
	
    }
    
    public function UpdateFulfillmentOrder($order)
    {	
	$id_order = (int) $order['id_orders'];
	if(!isset($order['seller_order_id']) || empty($order['seller_order_id'])) {
            $error = sprintf(Amazon_Tools::l('Seller order id is required for update order fulfillment to Amazon - id_order: %d is missing Seller Order ID'), $id_order);
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            return (false);
	}
	
	$seller_order_id = (int) $order['seller_order_id'];
	$shop_name = $order['sales_channel'];

        $AmazonOrder = array();
        $AmazonOrder['SellerFulfillmentOrderId'] = $id_order;
        $AmazonOrder['DisplayableOrderId'] = $seller_order_id;
        $AmazonOrder['DisplayableOrderComment'] = sprintf('Order #%s from %s', $seller_order_id, $shop_name);

        $AmazonOrder = Amazon_Tools::array_map_recursive('utf8_decode', $AmazonOrder);

        $result = $this->api->UpdateFulfillmentOrder($AmazonOrder, true);

        if (isset($result->ResponseMetadata->RequestId) && preg_match('/([0-9A-Fa-f]{4,16}[\-]{0,}){5}/', (string)$result->ResponseMetadata->RequestId))
        {
            $AmazonOrder['Response'] = (string)$result;		   

            // update mp_multichannel
	    if(!$this->updateSellerOrderId($id_order, $seller_order_id))
	    {
		$error = sprintf(Amazon_Tools::l('Unable to update MpChannel the order: %s'), $seller_order_id);
		if ($this->debug)
		{
		    print $error."<br />\n";
		}
		self::$errors[] = $error;
		return (false);
	    }	    

            return ($AmazonOrder);
        }
        elseif (isset($result->Error))
        {
	    $error = sprintf(Amazon_Tools::l('Amazon Error - Code:%s Message:%s'), $result->Error->Code, $result->Error->Message);   
	    if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            return (false);
        }
        else
        {
	    $error = sprintf(Amazon_Tools::l('%s(#%d): Unknown Error, content: {%s}'), basename(__FILE__), __LINE__, print_r($result, true));
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            return (false);
        }
    }
    
    public function CreateFulfillmentOrder($order, $params)
    {		
	$id_order = (int) $order['id_orders'];
	$seller_order_id = isset($order['seller_order_id']) && !empty($order['seller_order_id']) ? (int) $order['seller_order_id'] : (int) $order['id_orders'];
	$id_carrier = (int) $order['shipment']['id_carrier'];
	$id_country = (int) $params->id_country;
	
	$NotificationEmailList = array(
	    $params->user->email
	);	

	// 1. get carrier from amazon_mapping_carrier where type = fba
	if (!$carriers_multichannel = $this->getMappingIdCarrierByName(null, $params->id_shop, $id_carrier, 'fba', $id_country))
        {
            $error = sprintf(Amazon_Tools::l('Carrier Mapping not found for this entry - Shop order id : %d - carrier id: %d'), $seller_order_id, $id_carrier);
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }
	
	$ShippingSpeedCategory = $carriers_multichannel;
		
	$currencies = new Eucurrency();
        $currency = $order['id_currency'];
        $shop_name = $order['sales_channel'];	
        $customer = $order['buyer'];	
	
	if (!isset($customer['id_buyer']) || empty($customer['id_buyer']))
        {
            $error = sprintf(Amazon_Tools::l('Order id : %d, %s - %d'), $seller_order_id, Amazon_Tools::l('Unable to find customer'), $customer['id_buyer']);
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }

        $address = $order['buyer'];

        if (!isset($customer['name']) || empty($customer['name']))
        {
	    $error = sprintf(Amazon_Tools::l('Order id : %d, %s - %d'), $seller_order_id, Amazon_Tools::l('Unable to find address'), $customer['id_buyer']);
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }
	
        $AmazonOrder = array();
        $AmazonOrder['SellerFulfillmentOrderId'] = $id_order;
        $AmazonOrder['DisplayableOrderId'] = $seller_order_id;
        $AmazonOrder['DisplayableOrderDateTime'] = gmdate('Y-m-d\TH:i:s\Z', time());
        $AmazonOrder['DisplayableOrderComment'] = self::filter(sprintf('Order #%s from %s', $seller_order_id, $shop_name));
        $AmazonOrder['ShippingSpeedCategory'] = $ShippingSpeedCategory;

        $AmazonOrder['NotificationEmailList'] = array();

        if(isset($customer['email']) && !empty($customer['email'])){
            $AmazonOrder['NotificationEmailList'][] = self::filter($customer['email']);
        }

        foreach ($NotificationEmailList as $NotificationEmail)
        {
	    if(isset($NotificationEmail) && !empty($NotificationEmail)){	  
		$AmazonOrder['NotificationEmailList'][] = self::filter($NotificationEmail); //Configuration::get('PS_SHOP_EMAIL');		
	    }
        }
	
        $AmazonOrder['DestinationAddress'] = array();
        $AmazonOrder['DestinationAddress']['Name'] = self::filter($address['address_name']);
	$AmazonOrder['DestinationAddress']['Line1'] = self::filter($address['address1']);
	$AmazonOrder['DestinationAddress']['Line2'] = self::filter($address['address2']);
	$AmazonOrder['DestinationAddress']['Line3'] = null;
        $AmazonOrder['DestinationAddress']['City'] = self::filter($address['city']);
        $AmazonOrder['DestinationAddress']['PostalCode'] = self::filter($address['postal_code']);
        $AmazonOrder['DestinationAddress']['CountryCode'] = self::filter($address['country_code']);
        $AmazonOrder['DestinationAddress']['PhoneNumber'] = self::filter($address['phone']);
	 
        // Mandatory: Required by Amazon
        if (isset($address['state_region']) && !empty($address['state_region']))
        {
            $AmazonOrder['DestinationAddress']['StateOrProvinceCode'] = self::filter($address['state_region']);
	    /*$AmazonOrder['DestinationAddress']['StateOrProvinceCode'] = $state->iso_code ? $state->iso_code : $state->name;
	    } else{ $AmazonOrder['DestinationAddress']['StateOrProvinceCode'] = Country::getNameById($id_lang, $address->id_country);
	    }*/
        }  else {
	    
	    if (isset($address['country_code']) && !empty($address['country_code']))
	    {
		$AmazonOrder['DestinationAddress']['StateOrProvinceCode'] = self::filter($address['country_code']);
		
	    } else {

		$error = sprintf(Amazon_Tools::l('Order id : %d, %s'), $seller_order_id, Amazon_Tools::l('Missing state, Amazon Required "StateOrProvinceCode"'));
		if ($this->debug)
		{
		    print $error."<br />\n";
		}
		self::$errors[] = $error;
                $this->update_order_error($id_order, $error) ;
		return (false);
	    }
	}

        foreach ($AmazonOrder['DestinationAddress'] as $key => $val)
        {
            if (function_exists('filter_var'))
            {
                $sanitized = filter_var($val, FILTER_SANITIZE_STRING);
            }
            else
            {
                $sanitized = $val;
            }

            $AmazonOrder['DestinationAddress'][$key] = $sanitized;
        }
	
	$AmazonOrder['DestinationAddress'] = self::capitalize_recursive($AmazonOrder['DestinationAddress']);

        $AmazonOrder['Items'] = array();

        $products = $order['items'];	
	
        if (!isset($products) || !is_array($products) || !count($products))
        {
	    $error = sprintf(Amazon_Tools::l('%s - %d'), Amazon_Tools::l('Empty or wrong cart for order:"'), $seller_order_id);
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }

	$AmazonProductOption = new MarketplaceProductOption($this->user, null, null, null, null, $id_country, $params->id_marketplace);
	
        $SKUCheck = array();
        $index = 0;
        foreach ($products as $cart_product)
        {
            $SKU = $cart_product['reference'];
	    
            if (empty($SKU))
            {
                $error = sprintf(Amazon_Tools::l('Missing Reference(SKU) for product: %d/%d'),  $cart_product['id_product'], $cart_product['id_product_attribute']);
                if ($this->debug)
                {
                    print $error."<br />\n";
                }
                self::$errors[] = $error;
                continue;
            }

            if (!($options = $AmazonProductOption->get_product_option_by_sku($cart_product['reference'], $params->id_shop)))
            {	 
                $error = sprintf(Amazon_Tools::l('Shop order id : %s, Not FBA product, Uneligible product: %d/%d'), $seller_order_id, $cart_product['id_product'], $cart_product['id_product_attribute']);
                if ($this->debug)
                {
                    print $error."<br />\n";
                }
		self::$errors[] = $error;
                continue;
            }
	    
            if (!isset($options['fba']) || !(bool)$options['fba'])
            {
                $error = sprintf(Amazon_Tools::l('Shop order id : %s, Not FBA product: %d/%d'), $seller_order_id, $cart_product['id_product'], $cart_product['id_product_attribute']);
                if ($this->debug)
                {
                    print $error."<br />\n";
                }
                self::$errors[] = $error;
                continue;
            }
            //$SellerID = sprintf('%s_%s_%s', (int) $id_order, (int) $cart_product['id_product'], (int) $cart_product['id_product_attribute']);
	    $SellerID = sprintf('i-%d-%d-%d', (int)$id_order, (int) $cart_product['id_product'], (int) $cart_product['id_product_attribute']);
            $Price = (float) $cart_product['unit_price_tax_incl'];
            $Quantity = isset($cart_product['quantity']) ? (int) $cart_product['quantity'] : 1;

            $SKUCheck[$index] = $SKU;
            $AmazonOrder['Items'][$index]['SKU'] = self::filter($SKU);
            $AmazonOrder['Items'][$index]['SellerSKU'] = self::filter($SKU);
            $AmazonOrder['Items'][$index]['SellerFulfillmentOrderItemId'] = self::filter($SellerID);
            $AmazonOrder['Items'][$index]['Quantity'] = $Quantity;
	    
            if ($params->currency != $currency)
            {
                $newPrice = $currencies->doDiffConvert($Price, $currency, $params->currency);  
                $AmazonOrder['Items'][$index]['PerUnitDeclaredValue.CurrencyCode'] = $params->currency;
                $AmazonOrder['Items'][$index]['PerUnitDeclaredValue.Value'] = $newPrice;
            }
            else
            {
                $AmazonOrder['Items'][$index]['PerUnitDeclaredValue.CurrencyCode'] = $params->currency;
                $AmazonOrder['Items'][$index]['PerUnitDeclaredValue.Value'] = $Price;
            }	    
	   
            $AmazonOrder['Items'][$index]['DisplayableComment'] = self::filter($cart_product['product_name']);
            $index++;
        }	

        // Check availability of products
        //
//        $result = $this->api->ListInventoryBySKU($SKUCheck);
//
//        if (!$result || !is_array($result))
//        {
//            $error = sprintf(Amazon_Tools::l('Product availability check failed for order id: %s'), $seller_order_id);
//            if ($this->debug)
//            {
//                print $error."<br />\n";
//            }
//            self::$errors[] = $error;
//            return (false);
//        }
//        if (!count($result))
//        {
//            $error = sprintf(Amazon_Tools::l('Product availability, no items available for order id: %s'), $seller_order_id);
//            if ($this->debug)
//            {
//                print $error."<br />\n";
//            }
//            self::$errors[] = $error;
//            return (false);
//        }
//        $indexes = array_flip($SKUCheck);
//
//        // Verify Quantities
//        foreach ($result as $Item)
//        {
//            if (!isset($indexes[$Item['SKU']]))
//            {
//                continue;
//            }
//
//            $index = $indexes[$Item['SKU']];
//            if (isset($Item['InStockSupplyQuantity']) && $Item['InStockSupplyQuantity'] >= $AmazonOrder['Items'][$index]['Quantity'])
//            {
//		$error = sprintf(Amazon_Tools::l('Availability Check: %s - Quantity: %s'), $Item['SKU'], $Item['InStockSupplyQuantity']);
//                if ($this->debug)
//                {
//                    print $error."<br />\n";
//                }
//                self::$errors[] = $error;
//                unset($SKUCheck[$index]);
//            }
//        }
//
//        // Remaining products in SKUCheck: unavailable products or not enough stock
//        if (count($SKUCheck))
//        {
//            $error = sprintf(Amazon_Tools::l('Product availability, not enough stock to fulfill the order: %s'), $seller_order_id);
//            if ($this->debug)
//            {
//                print $error."<br />\n";
//            }
//            self::$errors[] = $error;
//            return (false);
//        }

        //$AmazonOrder = Amazon_Tools::array_map_recursive('utf8_decode', $AmazonOrder);
	//$AmazonOrder['Items'] = self::capitalize_recursive($AmazonOrder['Items']);
	
	if ($this->debug){
	     echo '<br/> AmazonMultiChannel -> CreateFulfillmentOrder() <br/>  <b>Order - :</b> <pre>' . print_r($AmazonOrder, true);
	}
	
        $result = $this->api->CreateFulfillmentOrder($AmazonOrder, true);
	$pass = isset($result['pass']) ? (bool) $result['pass'] : false ;
	$output = isset($result['output']) ? $result['output'] : null ;
	
	if(!$pass){
	    $error = isset($result['output']) ? $result['output'] : 
		sprintf(Amazon_Tools::l('%s(#%d): Unknown Error, content: {%s}'), basename(__FILE__), __LINE__, print_r($result, true)) ;
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
	} 
	else if(isset($output) && isset($output->ResponseMetadata->RequestId) && preg_match('/([0-9A-Fa-f]{4,16}[\-]{0,}){5}/', (string)$output->ResponseMetadata->RequestId))
        {
            $AmazonOrder['Response'] = (string)$output->ResponseMetadata->RequestId;		   
            // Restock Product
            /*foreach ($products as $product)
            {
                $id_product = (int)$product['product_id'];
                $id_product_attribute = (int)$product['product_attribute_id'] ? (int)$product['product_attribute_id'] : null;

                $SellerID = sprintf('%d_%d_%d', (int)$id_order, (int)$id_product, (int)$id_product_attribute);

                foreach ($AmazonOrder['Items'] as $key => $Item)
                {
                    if ($Item['SellerFulfillmentOrderItemId'] == $SellerID)
                    {
                        break;
                    }
                }

                //TODO: I forgot why this code. I comment out till i will find the reason :s
                /*
                if (version_compare(_PS_VERSION_, '1.5', '>='))
                {
                    //
                    StockAvailable::updateQuantity($id_product, $id_product_attribute ? $id_product_attribute : null, (int)$Item['Quantity']);
                }
                else
                {
                    $productQuantity = Product::getQuantity((int)$id_product, $id_product_attribute ? $id_product_attribute : null);
                    AmazonProduct::updateProductQuantity($id_product, $id_product_attribute ? $id_product_attribute : null, $productQuantity + (int)$Item['Quantity']);
                }
                 
            }*/

            // update mp_multichannel
	    $order_id = null;
	    $OrderStatus = constant('Amazon_Order::'.strtoupper($order['order_status']));	    
	    if(!$this->setMpMultichannel(
		    $id_order, 
		    $params->id_shop, 
		    $params->id_country, 
		    $order_id, 
		    $OrderStatus, 
		    self::AMAZON_FBA_MULTICHANNEL, 
		    self::AMAZON_FBA_STATUS_SUBMITED, 
		    $seller_order_id))
	    {
		$error = sprintf(Amazon_Tools::l('Unable to update MpChannel the order: %s'), $seller_order_id);
		if ($this->debug)
		{
		    print $error."<br />\n";
		}
		self::$errors[] = $error;
                $this->update_order_error($id_order, $error) ;
		return (false);
	    }	
	    
            return ($AmazonOrder);
        }
        elseif (isset($output) && isset($output->Error))
        {
	    $error = sprintf(Amazon_Tools::l('Amazon Error - Code:%s Message:%s'), $output->Error->Code, $output->Error->Message);            
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }
        else
        {
	    $error = sprintf(Amazon_Tools::l('%s(#%d): Unknown Error, content: {%s}'), basename(__FILE__), __LINE__, print_r($result, true));
            if ($this->debug)
            {
                print $error."<br />\n";
            }
            self::$errors[] = $error;
            $this->update_order_error($id_order, $error) ;
            return (false);
        }
    }
    
    public function AmazonApi($params) {
        
        $auth = array(
            'MerchantID' => trim($params->merchant_id),
            'AWSAccessKeyID' => trim($params->aws_id),
            'SecretKey' => trim($params->secret_key),
            'lang' => ltrim($params->ext, "."),
        );
        if(isset($params->auth_token) && !empty($params->auth_token)){
            $auth['auth_token'] = trim($params->auth_token);
        }        
        $marketPlace = array(
            'Currency' => $params->currency,
            'Country' => ltrim($params->ext, "."),
        );
	
        if (!$this->api = new Amazon_WebService($auth, $marketPlace, null, $this->debug)) {
	    return (false);           
        }
	return (true);
    }
    
    public static function filter($text)
    {
        $text = mb_ereg_replace('[\x00-\x1F\x21-\x2C\x3A-\x3F\x5B-\x60\x7B-\x7F\x2E\x2F]]', '', $text); // remove non printable
        $text = mb_ereg_replace('[!<>?=+{}_$%&]*$', '', $text);// remove chars rejected by Validate class

        return trim($text);
    }

    public static function capitalize_recursive($arr)
    {
        $ret = array();
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $ret[$key] = self::capitalize_recursive($val);
            } else {
                $text = mb_convert_encoding($val, 'HTML-ENTITIES', 'UTF-8');

                $searches = array('&szlig;', '&(..)lig;', '&([aouAOU])uml;', '&(.)[^;]*;');
                $replacements = array('ss', '\\1', '\\1'.'e', '\\1');

                foreach ($searches as $key2 => $search) {
                    $text = mb_ereg_replace($search, $replacements[$key2], $text);
                }
                $ret[$key] = utf8_decode(strtoupper(html_entity_decode($text)));
            }
        }
        return $ret;
    }

    /*public static function capitalize_recursive($arr)
    {
        $ret = array();
        foreach($arr as $key => $val)
        {
            if(is_array($val))
                $ret[$key] = self::capitalize_recursive($val);
            else
            {
                $text = mb_convert_encoding($val, 'HTML-ENTITIES', 'UTF-8');

                $searches = array('&szlig;', '&(..)lig;', '&([aouAOU])uml;', '&(.)[^;]*;');
                $replacements = array('ss', '\\1', '\\1'.'e', '\\1');

                foreach ($searches as $key2 => $search)
                {
                    $text = mb_ereg_replace($search, $replacements[$key2], $text);
                }

                if(in_array($key, array("SKU", "SellerSKU"))){
                    $string = html_entity_decode($text);
                } else {
                    $string = strtoupper(html_entity_decode($text));
                }
                
                $ret[$key] = utf8_decode($string);
            }
        }
        return $ret;
    }*/
    
}
