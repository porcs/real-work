<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/amazon.database.php');
require_once(dirname(__FILE__) . '/amazon.tools.php');

class AmazonStrategy {
    
    const _table  = "amazon_repricing_strategies";
    const _table_product_strategies  = "amazon_repricing_product_strategies";
    const _behaviorInactive  = 1;
    const _behaviorAutomatic  = 2;
    const _behaviorManual  = 3;

    public $id_lang;
    public $id_product_attribute = null;
    public static $m_pref = '';
    
    protected $ASIN = null;
    
    public function __construct($user, $debug = false) {
       
        $this->db = new stdClass();
        $this->db->product = new Db($user, 'products');
        $this->user = $user;
        $this->debug = $debug;
	
	if(defined('_TABLE_PREFIX_')) self::$m_pref = _TABLE_PREFIX_;
    }    
    
    public function get_strategies($id_shop, $id_country){
        
        $sql = "SELECT * FROM " . self::$m_pref . AmazonStrategy::_table ."  WHERE id_shop = $id_shop AND id_country = $id_country ; ";
        $result = $this->db->product->db_query_str($sql);
        $data = array();
        foreach ($result as $values) {
            $data[$values['id_strategy']]['id_strategy'] = $values['id_strategy'];
            $data[$values['id_strategy']]['name'] = $values['rs_name'];
            $data[$values['id_strategy']]['aggressivity'] = $values['rs_aggressivity'];
            $data[$values['id_strategy']]['base'] = $values['rs_base'];
            $data[$values['id_strategy']]['limit'] = $values['rs_limit'];
            $data[$values['id_strategy']]['delta_min'] = $values['rs_delta_min'];
            $data[$values['id_strategy']]['delta_max'] = $values['rs_delta_max'];
            $data[$values['id_strategy']]['active'] = $values['rs_active'];
            
            if(isset($values['rs_active'])){
                switch ($values['rs_active']){
                    case AmazonStrategy::_behaviorInactive :
                        $data[$values['id_strategy']]['inactive'] = true;
                        break;
                    case AmazonStrategy::_behaviorAutomatic :
                        $data[$values['id_strategy']]['automatic'] = true;
                        break;
                    case AmazonStrategy::_behaviorManual :
                        $data[$values['id_strategy']]['manual'] = true;
                        break;
                }
            }
            
        } 
        return $data;
    }
    
    public function get_products_strategies($id_shop, $id_country, $products){
        
        $data = $id_product = array();
        
        foreach ($products as $product){
            if(is_array($product) && isset($product['id_product']))
                $id_product[] =  $product['id_product'] ;
            else if(is_object($product) && isset($product->id_product))
                $id_product[] =  $product->id_product ;  
            else 
                $id_product = $products;
        }
        if(isset($id_product) && !empty($id_product)){
            $list = implode(", ", $id_product);
            $sql = "SELECT * FROM " .self::$m_pref. AmazonStrategy::_table_product_strategies ."  "
                    . "WHERE id_shop = $id_shop AND id_country = $id_country AND id_product IN ($list) ; ";
            $result = $this->db->product->db_query_str($sql);
            foreach ($result as $values) {
                $data[(int)$values['id_product']][(int)$values['id_product_attribute']]['minimum_price'] = $values['minimum_price'];
                $data[(int)$values['id_product']][(int)$values['id_product_attribute']]['target_price'] = $values['target_price'];
            } 
        }
        return $data;
    }
    
    public function save($strategies) {        
        
        $sql = '';
        if( !isset($strategies['strategy']) || empty($strategies['strategy']) 
        || !isset($strategies['id_shop']) || empty($strategies['id_shop']) 
        || !isset($strategies['id_country']) || empty($strategies['id_country']))
            return;      
        
        foreach ($strategies['strategy'] as $key => $values) {
            
            if (!isset($values['rs_name']) || empty($values['rs_name']))
                continue;
            
            $values['id_country'] = (int)$strategies['id_country'];
            $values['id_shop'] = (int)$strategies['id_shop'];
            $values['date_upd'] = date('Y-m-d H:i:s');
            $values['rs_active'] = isset($values['rs_active']) ? (int)$values['rs_active'] : 0;
                
            if(isset($values['id_strategy']) && !empty($values['id_strategy'])){
                // Update
                $where = array(
                    'id_strategy'   => array('value' => (int)$values['id_strategy'], 'operation' => '='), 
                    'id_country'    => array('value' => (int)$values['id_country'], 'operation' => '='),
                    'id_shop'       => array('value' => (int)$values['id_shop'], 'operation' => '=')
                );
                unset($values['id_strategy']);
                $sql .= $this->update_string(self::$m_pref.AmazonStrategy::_table, $values, $where);
            } else {
                // Insert
                $sql .= $this->insert_string(self::$m_pref.AmazonStrategy::_table, $values, true);
            }
        }
      
        $exec = $this->db->product->db_exec($sql);
        
        return $exec;
    }
  
    public function delete($id_strategy, $id_shop, $id_country) {
        
        if( !isset($id_strategy) || empty($id_strategy) || !isset($id_shop) || empty($id_shop) || !isset($id_country) || empty($id_country))
            return;      
        
        $where = array(
            'id_strategy'   => array('value'=>$id_strategy, 'operation' => '='),
            'id_shop'       => array('value'=>$id_shop, 'operation' => '='),
            'id_country'    => array('value'=>$id_country, 'operation' => '=')
        );
        
        $sql = $this->delete_string(self::$m_pref.AmazonStrategy::_table, $where);
        return $this->db->product->db_exec($sql);
    }
    
    public function insert_string($table, $data, $no_prefix = true) {
        return $this->db->product->insert_string($table, $data, $no_prefix);
    }

    public function update_string($table, $data, $where = array(), $limit = 0, $no_prefix = true) {
        return $this->db->product->update_string($table, $data, $where, $limit, $no_prefix);
    }

    public function delete_string($table, $where = array(), $no_prefix = true) {
        return $this->db->product->delete_string($table, $where, $no_prefix);
    }
    
}