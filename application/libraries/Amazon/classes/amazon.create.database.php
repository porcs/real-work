<?php

class AmazonCreateDatabase{
    
    public $db;
    public static $m_pref = '';
    public static $o_pref = 'orders_';
    
    public function __construct($database, $user_name = null) 
    {
        if(isset($database) && !empty($database))
            $this->db = $database;    
        
        if(isset($user_name) && !empty($user_name))
            $this->user_name = $user_name;    
    }
    
    public function create_table()
    {
        if($this->check_table_exists('amazon_log_display') == false){
            $this->create_amazon_log_display_table();
        } else {
            //repricing, messaging, fba, advertising
        }
        /*Parameter*/
        if($this->check_table_exists('amazon_submission_list_log') == false){
            $this->create_amazon_submission_list_log_table();
        }
        if($this->check_table_exists('amazon_submission_result_log') == false){
            $this->create_amazon_submission_result_log_table();
        }
        if($this->check_table_exists('amazon_validation_log') == false){
            $this->create_amazon_validation_log_table();
        }        
        /*Mappings*/
        if($this->check_table_exists('amazon_mapping_carrier') == false){
            $this->create_amazon_mapping_carrier_table();
        }
        if($this->check_table_exists('amazon_mapping_attribute') == false){
            $this->create_amazon_mapping_attribute_table();
        }
        if($this->check_table_exists('amazon_mapping_feature') == false){
            $this->create_amazon_mapping_feature_table();
        }        
        /*Profiles*/
        if($this->check_table_exists('amazon_profile') == false){
            $this->create_amazon_profile_table();
        }        
        /*Models*/
        if($this->check_table_exists('amazon_model') == false){
            $this->create_amazon_model_table();
        }        
        /*Categories*/
        if($this->check_table_exists('amazon_category_selected') == false){
            $this->create_amazon_category_selected_table();
        }   
        /*Report Request*/
        if($this->check_table_exists('amazon_request_report_log') == false){
            $this->create_amazon_request_report_log_table();
        }        
        /*Inventory*/
        if($this->check_table_exists('amazon_report_inventory') == false){
            $this->create_amazon_report_inventory_table();
        }
        if($this->check_table_exists('amazon_inventory') == false){
            $this->create_amazon_inventory_table();
        }
        if($this->check_table_exists('amazon_log') == false){
            $this->create_amazon_log_table();
        }
        if($this->check_table_exists('amazon_status') == false){
            $this->create_amazon_status_table();
        }        
        if($this->check_table_exists('amazon_attribute_override') == false){
            $this->create_amazon_attribute_override();
        }
        if($this->check_table_exists('amazon_valid_values_custom') == false){
            $this->create_amazon_valid_values_custom();
        }        
        if($this->check_table_exists('amazon_repricing_strategies') == false){
            $this->create_amazon_repricing_strategies();
        }        
        if($this->check_table_exists('amazon_repricing_product_strategies') == false){
            $this->create_amazon_repricing_product_strategies();
        }        
        if($this->check_table_exists('amazon_repricing_report') == false){
            $this->create_amazon_repricing_report();
        }        
        if($this->check_table_exists('amazon_repricing_log') == false){
            $this->create_amazon_repricing_log();
        }
        if($this->check_table_exists('amazon_repricing_flag') == false){
            $this->create_amazon_repricing_flag();
        }
        if($this->check_table_exists('amazon_fba_data') == false){
            $this->create_amazon_fba_data();
        }   
        if($this->check_table_exists('amazon_fba_log') == false){
            $this->create_amazon_fba_log();
        }   
        if($this->check_table_exists('amazon_fba_flag') == false){
            $this->create_amazon_fba_flag();
        }   
        if($this->check_table_exists('mp_multichannel') == false){
            $this->create_mp_multichannel();
        }
        if($this->check_table_exists('amazon_remote_cart') == false){
            $this->create_amazon_remote_cart();
        }   
        if($this->check_table_exists('amazon_orders') == false){
            $this->create_amazon_orders();
        }   
        if($this->check_table_exists('amazon_orders_items') == false){ // 27-02-2016
            $this->create_amazon_orders_items();
        }   
	if($this->check_table_exists('amazon_shipping_groups') == false){
            $this->create_amazon_shipping_groups();
        } 
	if($this->check_table_exists('amazon_disabled_products') == false){
            $this->create_amazon_disabled_product();
        }
	if($this->check_table_exists('amazon_order_missing_items') == false){
            $this->create_amazon_order_missing_items();
        }
        /*if($this->check_table_exists('amazon_advertising') == false){
            $this->create_amazon_advertising();
        }
        if($this->check_table_exists('amazon_advertising_log') == false){
            $this->create_amazon_advertising_log();
        }
        if($this->check_table_exists('amazon_advertising_products') == false){
            $this->create_amazon_advertising_products();
        }*/
        if($this->check_table_exists('amazon_update_flag') == false){
            $this->create_amazon_update_flag();
        }

        // 2016-10-10 Added isPrime to amazon_order
        if (!Db::fieldExists('mp_multichannel', 'is_prime')) {
            if(!$this->db->db_exec('ALTER IGNORE TABLE mp_multichannel ADD COLUMN is_prime TINYINT AFTER mp_channel ;')){
                return (false);
            }
        }
    } 

    private function check_table_exists($table_name) 
    {
        if($this->db->db_table_exists($table_name))
            return true;
        
        return false;
    }

    private function create_amazon_update_flag() {

        $sql = "CREATE TABLE ".self::$m_pref."amazon_update_flag (
                id_shop int(11) NOT NULL,
                id_country int(11) NOT NULL,
                id_product int(11)  NOT NULL,
                id_product_attribute int(11) ,
                update_type varchar(32) ,
                flag int(1)  NOT NULL,
                date_upd DATETIME,
                PRIMARY KEY (id_shop, id_country, id_product, id_product_attribute, update_type)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";

        if($this->db->db_exec($sql,false))
            return true;

        return false;
    }
    
    private function create_amazon_advertising_products() {

        $sql = "CREATE TABLE ".self::$m_pref."amazon_advertising_products (
                id_shop int(11) NOT NULL,
                id_country int(11),
                campaignId varchar(64) NOT NULL,
                adGroupId varchar(64) NOT NULL,
                adId varchar(64),
                sku varchar(64) NOT NULL,
                state varchar(16),
                date_add DATETIME,
                PRIMARY KEY (id_shop, id_country, campaignId, adGroupId, sku)
            ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";

        if($this->db->db_exec($sql,false))
            return true;

        return false;
    }
    private function create_amazon_advertising() {
        
        $sql = "CREATE TABLE ".self::$m_pref."amazon_advertising (
                id_shop int(11) NOT NULL,
                id_country int(11) NOT NULL,
                field_name VARCHAR(64),
                field_value TEXT,
                date_upd DATETIME,
                PRIMARY KEY (id_shop, id_country, field_name)
            ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";

        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    private function create_amazon_advertising_log() {

        $sql = "CREATE TABLE ".self::$m_pref."amazon_advertising_log (
                id_log int(11) NOT NULL auto_increment,
                id_shop int(11) NOT NULL,
                id_country int(11) NOT NULL,
                action VARCHAR(64),
                message TEXT,
                date_add DATETIME,
                PRIMARY KEY (id_log)
            ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";

        if($this->db->db_exec($sql,false))
            return true;

        return false;
    }
    private function create_amazon_disabled_product(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_disabled_products (
		    id_product int(11) NOT NULL,
		    id_product_attribute int(11) NOT NULL DEFAULT '0',
                    sku varchar(64),
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    date_upd DATETIME,
		    PRIMARY KEY (sku, id_shop, id_country) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_shipping_groups(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_shipping_groups (
		    /*id_group int(11) NOT NULL auto_increment,*/
		    id_shop int(11) NOT NULL,
		    id_country int(11),
		    group_key varchar(128),
		    group_name varchar(128),		  
		    date_upd DATETIME,
		    PRIMARY KEY (id_shop, id_country, group_key) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_order_missing_items(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_order_missing_items (
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    id_order int(11) NOT NULL,
		    id_marketplace_order_ref varchar(40) NOT NULL,
		    date_upd DATETIME,
		    PRIMARY KEY (id_shop, id_country, id_order) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_orders(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_orders (
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    mp_order_id varchar(40) NOT NULL,
		    mp_shipping varchar(64),
		    mp_channel varchar(16),
		    mp_status VARCHAR(24),
		    quantity INT(16),
		    price decimal(20,6),
		    isPrime TINYINT,
		    order_date DATETIME,
		    date_upd DATETIME,
		    PRIMARY KEY (id_shop, id_country, mp_order_id) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_orders_items(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_orders_items (
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    mp_order_id varchar(40) NOT NULL,
		    id_product int(11) NOT NULL,
		    id_product_attribute int(11) NOT NULL DEFAULT '0',
                    sku varchar(64),
		    quantity INT(16),
		    date_upd DATETIME,
		    PRIMARY KEY (id_shop, id_country, mp_order_id, id_product, id_product_attribute) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
	//price decimal(20,6),
	//product_name varchar(128),
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_remote_cart(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_remote_cart (
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    mp_order_id varchar(40) NOT NULL,
		    reference varchar(40) NOT NULL,
		    quantity INT(16) NOT NULL,
		    timestamp timestamp,
		    date_upd DATETIME,
		    PRIMARY KEY (mp_order_id, reference) 
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
	
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_fba_flag(){    
             
	$sql =  "CREATE TABLE ".self::$m_pref."amazon_fba_flag (            
		    id_product int(11) NOT NULL,
		    id_product_attribute int(11) NOT NULL DEFAULT '0',
                    sku varchar(64),
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    process varchar(64),
                    type varchar(64),
		    quantity_fba INTEGER,
		    quantity INTEGER,
                    delta INTEGER,
                    flag TINYINT,
                    flag_stock TINYINT,
                    date_upd DATETIME,
                    PRIMARY KEY (`sku`, `id_shop`, /*`id_country`,*/ `process`, `type`) 
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ";
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_fba_log(){    
             
	$sql =  'CREATE TABLE '.self::$m_pref.'amazon_fba_log (                 
                    id_log BIGINT NOT NULL auto_increment,
		    batch_id VARCHAR(32) NOT NULL,
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    sku varchar(64),
                    process varchar(64),
                    type varchar(64),
                    fba_status varchar(4),
                    message text,
                    date_add DATETIME,
                    PRIMARY KEY (id_log ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_fba_data(){    
        
        $sql =  "CREATE TABLE `".self::$m_pref."amazon_fba_data` (
		`id_product` int(11) NOT NULL,
		`id_product_attribute` int(11) NOT NULL DEFAULT '0',
		`sku` varchar(64) NOT NULL,
		`id_shop` int(11) NOT NULL,
		`id_country` int(11) NOT NULL DEFAULT '0',
		`fulfilled_by` varchar(64) DEFAULT NULL,
		`local_inventory` int(11),
		`fba` TINYINT,
		`fba_inventory` int(11),
		`your_price` float,
		`sales_price` float,
		`currency` varchar(3),
		`estimated_fee` float,
		`fulfilment_fee` float,
		`date_add` datetime DEFAULT NULL,
		`date_upd` datetime DEFAULT NULL,
		PRIMARY KEY (`sku`, `id_shop`/*, `id_country`*/)
	      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }    
    
    private function create_mp_multichannel(){
            
	$sql =  'CREATE TABLE '.self::$m_pref.'mp_multichannel (                 
		    id_orders int(11) NOT NULL,
		    seller_order_id VARCHAR(40),
		    id_shop int(11) NOT NULL,
		    id_country int(11) NOT NULL,
		    mp_shipping varchar(64),
		    mp_items varchar(16),
		    mp_channel varchar(16),
		    mp_channel_status varchar(32),
		    mp_order_id VARCHAR(24),
		    mp_status VARCHAR(24),
		    acknowledge_flag TINYINT,
		    date_add DATETIME,
		    PRIMARY KEY (id_orders ASC, id_shop ASC, id_country ASC)
		) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
	
	if($this->db->db_exec($sql))
	    return true;
	
        return false;
    }
    
    private function create_amazon_fba(){
            
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_fba (                 
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    fba_formula varchar(64),
                    fba_multichannel varchar(16),
                    fba_multichannel_auto varchar(16),
                    fba_decrease_stock varchar(16),
                    fba_stock_behaviour varchar(16),
                    fba_order_state varchar(16),
                    fba_multichannel_state varchar(16),
                    fba_multichannel_sent_state varchar(16),
                    date_add DATETIME,
                    PRIMARY KEY (id_shop ASC, id_country ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_repricing_flag(){    
        
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_repricing_flag (                 
                    id_product INTEGER NOT NULL,
                    id_product_attribute INTEGER,                 
                    sku text,
                    id_shop int(11) NOT NULL,
                    id_country int(11),
                    flag_type text,
                    flag TINYINT(1),
                    details text,
                    date_upd DATETIME,
                    PRIMARY KEY (id_product ASC, id_product_attribute ASC, id_shop ASC, id_country ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_repricing_log(){
            
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_repricing_log (                 
                    id_log BIGINT NOT NULL auto_increment,
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    message_count int(11),
                    sku text,
                    date_add DATETIME,
                    PRIMARY KEY (id_log ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_repricing_product_strategies(){
            
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_repricing_product_strategies (                 
                    id_product int(11) NOT NULL,
                    id_product_attribute int(11),
                    id_shop int(11) NOT NULL,
                    id_country int(11) NOT NULL,
                    sku varchar(64),
                    asin varchar(16),
                    minimum_price float,
                    target_price float,
                    actual_price float,
                    gap float,
                    date_upd DATETIME,
                    PRIMARY KEY (id_shop ASC, id_country ASC, id_product ASC, id_product_attribute ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    private function create_amazon_repricing_report(){
        
	// repricing_action = reprice/export
	// process_type = feedtype : sync/create/reprice
	// repricing_mode = FBA/MFN 
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_repricing_report (
                    sku varchar(64) NOT NULL,
                    id_shop INTEGER NOT NULL,
                    id_country INTEGER NOT NULL,
                    repricing_date VARCHAR(16),
                    repricing_timestamp VARCHAR(24),                
                    reprice decimal(20,6),
                    price decimal(20,6),
                    currency VARCHAR(3),
                    date_upd DATETIME,
                    repricing_action TEXT, 
                    process_type TEXT,
                    repricing_mode TEXT,
                    none_repricing tinyint(1),
                    PRIMARY KEY (sku ASC,id_shop ASC,id_country ASC, reprice ASC)  
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;';
        if($this->db->db_exec($sql,false))
                return true;
        
        return false;
    }
    private function create_amazon_repricing_strategies(){
        
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_repricing_strategies (
                    id_strategy INTEGER NOT NULL auto_increment,
                    id_shop INTEGER NOT NULL,
                    id_country INTEGER NOT NULL,
                    rs_name TEXT,
                    rs_aggressivity VARCHAR(32),
                    rs_base VARCHAR(32),
                    rs_limit VARCHAR(32),
                    rs_delta_min VARCHAR(32),
                    rs_delta_max VARCHAR(32),
                    rs_active  TINYINT,
                    date_upd  DATETIME,
                    PRIMARY KEY (id_strategy ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    private function create_amazon_valid_values_custom(){
        
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_valid_values_custom (
                    region varchar(3) NOT NULL,
                    universe varchar(32) NOT NULL,
                    product_type varchar(64) DEFAULT NULL,
                    attribute_field varchar(64) NOT NULL,
                    valid_value varchar(255) NOT NULL,
                    date_upd datetime DEFAULT NULL                
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_attribute_override (){
        
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_attribute_override (
                    sku varchar(64) NOT NULL,
                    id_product int(11),
                    id_product_attribute int(11),
                    id_shop int(11),
                    id_country int(11),
                    id_model int(11),
                    attribute_field varchar(255) NOT NULL,
                    override_value varchar(255) NOT NULL,
                    date_upd datetime
                    
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    private function create_amazon_log_display_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_log_display (
                    field_id BIGINT NOT NULL auto_increment,
                    field_name  VARCHAR(25),
                    field_order  INTEGER,
                    field_disabled  TINYINT,
                    field_toggle  TINYINT,
                    id_shop  INTEGER,
                    id_country  INTEGER,
                    menu  TEXT,
                    other  TEXT,
                    PRIMARY KEY (field_id ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;  ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_log_product_skip_table() 
    {
        $sql = "CREATE TABLE `".self::$m_pref."amazon_log_products_skip` (
                `batch_id` varchar(64) CHARACTER SET utf8 NOT NULL,
                `id_product` int(12) NOT NULL,
                `id_shop` int(12) NOT NULL,
                `message` varchar(16) CHARACTER SET utf8 NOT NULL,
                `date_add` datetime NOT NULL,
                PRIMARY KEY (`batch_id`,`products_id`,`id_shop`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_status_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_status (
                    id_status BIGINT NOT NULL auto_increment,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    action_type TEXT,
                    status TINYINT,                          
                    is_cron TINYINT,                
                    date_upd datetime,
                    detail text,
                    PRIMARY KEY (id_status ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_log_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_log (
                    id_log BIGINT NOT NULL auto_increment,
                    batch_id VARCHAR(32) NOT NULL,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    action_type TEXT,
                    no_send INTEGER,
                    no_skipped INTEGER,
                    no_process INTEGER,
                    no_success INTEGER,
                    no_error INTEGER,
                    no_warning INTEGER,                
                    is_cron TINYINT,                
                    date_upd datetime,
                    detail text,
                    PRIMARY KEY (id_log ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_inventory_table() 
    {
        //sku	asin	price	quantity
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_inventory (
                    id_product INTEGER NOT NULL,
                    id_product_attribute INTEGER,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    sku VARCHAR(64),
                    asin VARCHAR(64),
                    action_type VARCHAR(8),
                    price decimal(20,6),
                    quantity INTEGER,
                    details TEXT,
                    date_add datetime,
                    date_upd datetime,
                    PRIMARY KEY (sku ASC, id_country ASC, id_shop ASC)
                 ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if(!$this->db->db_exec($sql,false))
            return false;
        
        $sql='CREATE UNIQUE INDEX amazon_inventory_index ON amazon_inventory (sku ASC, id_country ASC, id_shop ASC);';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_report_inventory_table()
    {
        //sku	asin	price	quantity
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_report_inventory (
                    sku VARCHAR(64),
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    asin VARCHAR(64),
                    price decimal(20,6),
                    quantity INTEGER,
                    date_add datetime,
                    PRIMARY KEY (sku ASC, id_country ASC, id_shop ASC)                
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;
                ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_request_report_log_table() 
    {
        //"batch_id" VARCHAR(32) NOT NULL,
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_request_report_log (
                    id_log BIGINT NOT NULL auto_increment,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    requestReportTime datetime,
                    reportTime datetime,
                    requestId VARCHAR(16),
                    reportProcessingStatus VARCHAR(64),
                    reportId VARCHAR(16),
                    step TINYINT,
                    title TEXT,
                    message TEXT,
                    flag VARCHAR(64),
                    date_add datetime,
                    PRIMARY KEY (id_log ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_submission_list_log_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_submission_list_log (
                    id_log BIGINT NOT NULL auto_increment,
                    batch_id VARCHAR(32) NOT NULL,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    action_type TEXT,
                    feed_type TEXT,
                    message TEXT,
                    date_add datetime,
                    PRIMARY KEY (id_log ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_submission_result_log_table() 
    {
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_submission_result_log (
                    id_log BIGINT NOT NULL auto_increment,
                    batch_id VARCHAR(32) NOT NULL,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    sku varchar(64),
                    id_message INTEGER,
                    id_submission_feed VARCHAR(32),
                    result_code TEXT,
                    result_message_code VARCHAR(32),
                    result_description TEXT,
                    date_add datetime,
                    PRIMARY KEY (id_log ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
        
    private function create_amazon_validation_log_table() 
    {
        $sql =  'CREATE TABLE '.self::$m_pref.'amazon_validation_log (
                    id_log BIGINT NOT NULL auto_increment,
                    batch_id VARCHAR(32) NOT NULL,
                    action_process VARCHAR(32) NOT NULL,
                    action_type VARCHAR(32) NOT NULL,
                    id_country INTEGER NOT NULL,
                    id_shop INTEGER NOT NULL,
                    message TEXT,
                    id_message VARCHAR(32),
                    date_add datetime,
                    PRIMARY KEY (id_log ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_category_selected_table() 
    {
        $sql = ' CREATE TABLE '.self::$m_pref.'amazon_category_selected (
                    id_category_selected BIGINT NOT NULL auto_increment,
                    id_category INTEGER,
                    id_country INTEGER,
                    id_shop INTEGER,
                    id_mode INTEGER,
                    id_profile INTEGER,
                    date_add datetime,
                    PRIMARY KEY (id_category_selected ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_category_selected_wizard_table() 
    {
        $sql = ' CREATE TABLE '.self::$m_pref.'amazon_category_selected_wizard (
                    id_category_selected INTEGER NOT NULL,
                    id_category INTEGER,
                    id_country INTEGER,
                    id_shop INTEGER,
                    id_mode INTEGER,
                    id_profile_price INTEGER,
                    date_add datetime,
                    PRIMARY KEY (id_category_selected ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_profile_table() 
    {
        $sql = ' CREATE TABLE '.self::$m_pref.'amazon_profile (
                    id_profile BIGINT NOT NULL auto_increment,
                    id_country INTEGER,
                    id_shop INTEGER,
                    id_mode INTEGER,
                    id_model INTEGER,
                    is_default TINYINT,
                    name TEXT,
                    out_of_stock INTEGER,
                    synchronization_field TEXT,
                    title_format TINYINT,
                    html_description TINYINT,
                    description_field TINYINT,
                    code_exemption TEXT,
                    sku_as_supplier_reference TEXT,
                    recommended_browse_node TEXT,
                    key_product_features TEXT,
                    price_rules TEXT,
                    association TEXT,
                    category TEXT,
                    date_add datetime,
                    PRIMARY KEY (id_profile ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_model_table() 
    {
        $sql = ' CREATE TABLE '.self::$m_pref.'amazon_model (
                    id_model BIGINT NOT NULL auto_increment,
                    id_country INTEGER,
                    id_shop INTEGER,
                    id_mode INTEGER,
                    name TEXT,
                    universe TEXT,
                    product_type TEXT,
                    sub_product_type TEXT,
                    mfr_part_number VARCHAR(32),
                    variation_theme TEXT,
                    variation_data TEXT,
                    recommended_data TEXT,
                    specific_options TEXT,
                    specific_fields TEXT,
                    recommended_browse_node TEXT,
                    title_format TINYINT,
                    html_description TINYINT,
                    association TEXT,
                    category TEXT,
                    memory_usage VARCHAR(16),
                    date_add datetime,
                    PRIMARY KEY (id_model ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ; ';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_mapping_carrier_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_mapping_carrier (
                    id_mapping  BIGINT NOT NULL auto_increment,
                    id_shop INTEGER,
                    id_mode INTEGER,
                    id_country INTEGER,
                    id_carrier INTEGER,
                    type VARCHAR(20),
                    mapping TEXT,
                    other TEXT,
                    date_add datetime,
                    id_carrier_ref INTEGER,
                    PRIMARY KEY (id_mapping ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_mapping_attribute_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_mapping_attribute (
                    id_mapping BIGINT NOT NULL auto_increment,
                    id_shop  INTEGER,
                    id_mode  INTEGER,
                    id_country  INTEGER,
                    id_attribute_group  INTEGER,
                    id_attribute  INTEGER,
                    is_color  TINYINT,
                    product_type  TEXT,
                    product_sub_type  TEXT,
                    attribute_field  TEXT,
                    mapping  TEXT,
                    date_add  datetime,
                    PRIMARY KEY (id_mapping ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }
    
    private function create_amazon_mapping_feature_table() 
    {
        $sql = 'CREATE TABLE '.self::$m_pref.'amazon_mapping_feature (
                    id_mapping BIGINT NOT NULL auto_increment,
                    id_shop  INTEGER,
                    id_mode  INTEGER,
                    id_country  INTEGER,
                    id_feature  INTEGER,
                    id_feature_value  INTEGER,
                    is_color  TINYINT,
                    product_type  TEXT,
                    product_sub_type  TEXT,
                    attribute_field  TEXT,
                    mapping  TEXT,
                    date_add  datetime,
                    PRIMARY KEY (id_mapping ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;';
        
        if($this->db->db_exec($sql,false))
            return true;
        
        return false;
    }    
    
}