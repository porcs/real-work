<?php

class Amazon_Tools {

    public $lang;

    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 5) {
        
        if ($stream_context == null && preg_match('/^https?:\/\//', $url)){
            $stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)));
        }
        if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)){
            return @file_get_contents($url, $use_include_path, $stream_context);
        }elseif (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            if ($stream_context != null) {
                $opts = stream_context_get_options($stream_context);
                if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post') {
                    curl_setopt($curl, CURLOPT_POST, true);
                    if (isset($opts['http']['content'])) {
                        parse_str($opts['http']['content'], $datas);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
                    }
                }
            }
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } else {
            return false;
        }
    }

    public static function getValue($key, $default_value = false) {
        if (!isset($key) || empty($key) || !is_string($key))
            return false;
        $ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));

        if (is_string($ret) === true)
            $ret = urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret)));
        return !is_string($ret) ? $ret : stripslashes($ret);
    }

    public static function strip_html($html) {
        $text = strip_tags($html, '<br>');
        $text = preg_replace('#<br\s{0,}/{0,}>#i', "\n", $text); // br to newline
        $text = preg_replace('#[\n]+#i', "\n", $text); // multiple-return
        $text = preg_replace('#^[\n\r\s]+#i', "", $text); // trim
        $text = str_replace("\n", "<br />\n", $text); // newline to br        
        return(trim($text));
    }

    public static function remove_html_tag($html) {
        $text = strip_tags($html, '<br>');
        $text = preg_replace('#<br\s{0,}/{0,}>#i', "\n", $text); // br to newline
        $text = preg_replace('#[\n]+#i', "\n", $text); // multiple-return
        $text = preg_replace('#^[\n\r\s]+#i', "", $text); // trim
        $text = str_replace("\n", "", $text); // newline to br        
        return(trim($text));
    }

    public static function isUSMarketplaceId($marketplaceID) {
        return( trim($marketplaceID) == 'ATVPDKIKX0DER' );
    }

    public static function EAN_UPC_Check($code) {
	
        /*//first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;
        return ((int) $code == (int) ($digits . $check_digit));*/
	
	// 01/03/2016
	if (!is_numeric($code) || strlen($code) < 12) {
            return (false);
        }
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1}
        + $digits{3}
        + $digits{5}
        + $digits{7}
        + $digits{9}
        + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0}
        + $digits{2}
        + $digits{4}
        + $digits{6}
        + $digits{8}
        + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = (int)$next_ten - $total_sum;
        $last_digit = (int)substr($code, strlen($code) - 1, 1);

        return ((int)$last_digit == (int)$check_digit);
    }

    public static function load($file, $language) {

        $langfile = $file . '_lang.php';

        if (file_exists(dirname(__FILE__) . '/../../../' . 'language/' . $language . '/' . $langfile)) {
            include(dirname(__FILE__) . '/../../../' . 'language/' . $language . '/' . $langfile);
        } else {
            return;
        }

        if (!isset($lang) || empty($lang)) {
            return;
        }

        global $l;
        $l = $lang;
    }

    public static function l($line) {

        global $l;
        return (empty($line) || !isset($l[$line])) ? $line : $l[$line];
    }

    public static function rstr($value) {
        $str = $value;
        if (!empty($value)) {
            if (is_array($value)) {
                foreach ($value as $key => $val) {
                    $str[$key] = Amazon_Tools::rstr($val);
                }
            } else if (is_int($value)) {
                $str = trim($value);
            } else if (is_float($value)) {
                $str = trim($value);
            } else if (is_string($value)) {
                $str = trim(Amazon_Tools::encodeText($value));
            } else {
                $str = trim(Amazon_Tools::encodeText($value));
            }
        }
        return $str;
    }

    public static function ValidateASIN($ASIN) {
        return( $ASIN != null && strlen($ASIN) && preg_match('/[A-Z0-9]{10}/', $ASIN) );
    }

    public static function ValidateSKU($SKU) {
        return( $SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU) );
    }

    public static function EAN_UPC_isPrivate($code) {
        return(in_array(substr(sprintf('%013s', $code), 0, 1), array('2')));
    }

    public static function split_words($string, $max = 1) {
        $words = preg_split('/\s/', $string);
        $lines = array();
        $line = '';

        foreach ($words as $k => $word) {
            $length = strlen($line . ' ' . $word);
            //echo '<pre>' . print_r($word, true) . '</pre>';
            if (strpos($word, '.')) {
                if (!empty($line)) {
                    $lines[] = trim($line) . ' ' . $word;
                }
                $line = '';
            } else if ($length <= $max) {
                $line .= ' ' . $word;
            } else if ($length > $max) {
                if (!empty($line)) {
                    $lines[] = trim($line);
                }
                $line = $word;
            } else {
                $lines[] = trim($line) . ' ' . $word;
                $line = '';
            }
        }
        if (empty($lines) && strlen($line))
            $lines[] = ($line = trim($line)) ? $line : $word;


        return $lines;
    }

    public static function getMarketplaceTags($tags, $lang) {
        $tags_array = array();
        if (!isset($tags[$lang]) || empty($tags[$lang]))
            return;

        $tag_s = $tags[$lang];
        if (isset($tag_s) && !empty($tag_s)) {
            $tags_array = explode(PHP_EOL, wordwrap(trim(Amazon_Tools::encodeText($tag_s)), 50, PHP_EOL, FALSE));
            if (isset($tags_array) && is_array($tags_array) && !empty($tags_array))
                return (array_slice($tags_array, 0, 5));
        }
        return $tags_array;
    }

    public static function encodeText($string, $verySafe = false) {
	if ($verySafe)
	{
	    $string = str_replace('’', "'", $string);            
            if(!self::isJapanese($string)) {
                $string = @utf8_encode(utf8_decode($string));
            }
	    $string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
	    $string = self::stripInvalidXml($string);
	    $string = str_replace('&#39;', "'", $string);
	}

	return (trim($string));
    }

    public static function stripInvalidXml($value) {
	$ret = '';
	$current = null;
	if (empty($value))
	    return $ret;

	$length = strlen($value); //TODO: Multibyte dance, do not replace by Tools::strlen !
	for ($i = 0; $i < $length; $i++)
	{
	    $current = ord($value{$i});
	    if (($current == 0x9) || ($current == 0xA) ||($current == 0xD) ||(($current >= 0x20) && ($current <= 0xD7FF)) ||(($current >= 0xE000) && ($current <= 0xFFFD)) ||(($current >= 0x10000) && ($current <= 0x10FFFF)))
		$ret .= chr($current);
	    else
		$ret .= ' ';
	}

	return $ret;
    }

    public static function escape_str($str) {
        if (isset($str) && !empty($str)) {
            if (is_array($str)) {
                foreach ($str as $key => $val)
                    $str[$key] = $this->escape_str($val);
                return $str;
            }

            $str = sqlite_escape_string($str);
            return $str;
        }

        return '';
    }

    public static function spacify_str($world, $glue = ' ') {
        return preg_replace('/([a-z0-9])([A-Z])/', "$1$glue$2", $world);
    }

    public static function ceil_time($time) {
        date_default_timezone_set('UTC');
        $date = strtotime(date('Y-m-d', $time));
        $chk_date = strtotime(date('Y-m-d', $time + 43200));
        if ($date == $chk_date) {
            $out = $date;
        } else {
            $out = $chk_date;
        }
        return $out;
    }

    public static function floor_time($time) {
        date_default_timezone_set('UTC');
        $date = strtotime(date('Y-m-d', $time));
        $chk_date = strtotime(date('Y-m-d', $time + 43200));
        if ($date == $chk_date) {
            $out = $date;
        } else {
            $out = $date;
        }
        return $out;
    }
    
    // 2016-01-14
    public static function query_date($date1, $date2, $zone=null) {
	
	if (date('Ymd', strtotime($date2)) >= date('Ymd') || empty($date2))
        {
            $date1 = date('c', strtotime($date1));
            $date2 = date('c', strtotime('now - 7 min'));

           /* if(isset($zone))
            switch ($zone){
                case 'eu' :
                    $date1 = date('c', strtotime($date1));
                    $date2 = date('c', strtotime('now + 53 min'));
                    break;
                default :
                    $date1 = date('c', strtotime($date1));
                    $date2 = date('c', strtotime('now - 7 min'));
                    break;
            }*/
        }
        elseif (date('Ymd', strtotime($date1)) >= date('Ymd', strtotime($date2)))
        {
            $date1 = date('c', strtotime($date1 . ' 00:00'));
            $date2 = date('c', strtotime($date2 . ' 23:59:59'));
        }
        else
        {
            $date1 = date('c', strtotime($date1 . ' 00:00'));
            $date2 = date('c', strtotime($date2 . ' 23:59:59'));
        }  
	
	return array('date1' => $date1, 'date2' => $date2);
    }
	     
    /* Valid Value */ // 25-06-2015
    public static function ucfirst($str){
        return strtoupper(substr($str, 0, 1)).substr($str, 1);
    }

    public static function ucwords($str){
        return ucwords($str);
    }
    
    public static function toKey($str){
        
        $str = str_replace(
                array('-', ',', '.', '/', '+', '.', ':', ';', '>', '<', '?', '(', ')', '!'),
                array('_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm'), $str);
        
        return strtolower(preg_replace('/[^A-Za-z0-9_]/', '', $str));
    }
    
    public static function iso_codeToId($domain) {
        
        $domain = ltrim($domain, ".");
        switch ($domain) {
            case 'uk' :
            case 'co.uk' :
                return ('uk');
            case 'com' :
                return ('en');
            case 'mx' :
            case 'com_mx' :
            case 'com.mx' :
                return ('mx');
            case 'co.jp' :
                return ('jp');
            default:
                return ($domain);
        }
    }
    
    public static function domainToId($domain) {
        
        $domain = ltrim($domain, ".");
        switch ($domain) {
            case 'co.uk' :
                return ('uk');
            case 'com' :
                return ('us');
            case 'mx' :
            case 'com_mx' :
            case 'com.mx' :
                return ('mx');
            case 'co.jp' :
                return ('jp');
            default:
                return ($domain);
        }
    }
    
    public static function is_serialized($value, &$result = null) {
            
        if (!is_string($value)) { return false; }
        if ($value === 'b:0;') {
            $result = false;
            return true;
        }
        $length = strlen($value);
        $end = '';
        if(isset($value[0])) {
            switch ($value[0]) {
                case 's': if ($value[$length - 2] !== '"') return false;
                case 'b':
                case 'i':
                case 'd': $end .= ';';
                case 'a':
                case 'O': $end .= '}';
                    if ($value[1] !== ':') return false;
                    switch ($value[2]) {
                        case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: break;
                        default: return false;
                    }
                case 'N': $end .= ';';
                    if ($value[$length - 1] !== $end[0]) {
                        return false;
                    }
                    break;
                default:
                    return false;
            }
        }
        if (($result = @unserialize($value)) === false) {
            $result = null;
            return false;
        }
        return true;
    }   
    
    public static function Formula($price, $formula) {
        if (empty($price))
                return ($price);
        /*if (strpos($formula, '@') === false)
                return ($price);

        $formula = trim(str_replace(',', '.', $formula));
        $formula = preg_replace("/\\n/i", '', $formula);
        $formula = preg_replace("/\\r/i", '', $formula);

        if (preg_match('#([0-9\., ]*)%#', $formula, $result))
        {
                $toPercent = $price * ((float)$result[1] / 100);
                $formula = preg_replace('#([0-9\., ]*)%#', $toPercent, $formula);
        }
        $formula = str_replace('%', '', $formula);
        $equation = str_replace('@', $price ? $price : 0, $formula);
        $result = self::_matheval($equation);

        return ($result);*/    
	return $price + ($price * ((float)$formula / 100));
    }
    
    /*private static function _matheval($equation)
    {
        $equation = preg_replace('/[^0-9+\-.*\/()%]/', '', $equation);
        $equation = preg_replace('/([+-])([0-9]+)(%)/', '*(1\$1.\$2)', $equation);
        // you could use str_replace on this next line
        // if you really, really want to fine-tune this equation
        $equation = preg_replace('/([0-9]+)(%)/', '.\$1', $equation);
        if ($equation == '')
            $return = 0;
        else
            eval("\$return=" . $equation . ";");;//TODO: Validation: Backward compatibility requirement, will be deprecated and removed soon

        return $return;
    } */  
    
    public static function array_map_recursive($callback, $arr) {
        $ret = array();
        foreach($arr as $key => $val)
        {
            if(is_array($val))
                $ret[$key] = self::array_map_recursive($callback, $val);
            else
                $ret[$key] = $callback($val);
        }
        return $ret;
    }
    
    public static function array_merge_recursive_distinct ( array &$array1, array &$array2 ) {
	$merged = $array1;

	foreach ($array2 as $key => &$value) {
	    if (is_array($value) && isset($merged [$key]) && is_array($merged [$key])) {
		$merged [$key] = self::array_merge_recursive_distinct($merged [$key], $value);
	    } else {
		$merged [$key] = $value;
	    }
	}

	return $merged;
    }
    
    public static function clean_strip_tags($html) {
        $text = $html;

        $text = str_replace(array('</li>', '</LI>'), "\n</li>", $text);
        $text = str_replace(array('<BR', '<br'), "\n<br", $text);

        $text = strip_tags($text);

        $text = str_replace('&#39;', "'", $text);

        $text = mb_convert_encoding($text, 'HTML-ENTITIES');
        $text = str_replace('&nbsp;', ' ', $text);
        $text = html_entity_decode($text, ENT_NOQUOTES, 'UTF-8');

        $text = str_replace('&', '&amp;', $text);
        $text = str_replace('"', "&#34;", $text);

        $text = preg_replace('#\s+[\n|\r]+$#i', '', $text); // empty
        $text = preg_replace('#[\n|\r]+#i', "\n", $text); // multiple-return
        $text = preg_replace('#(\s)\n+#i', "\n", $text); // multiple-return
        $text = preg_replace('#^[\n\r\s]#i', '', $text);

        $text = preg_replace('/[\x{0001}-\x{0009}]/u', '', $text);
        $text = preg_replace('/[\x{000b}-\x{001f}]/u', '', $text);
        $text = preg_replace('/[\x{0080}-\x{009F}]/u', '', $text);

        if(!self::isJapanese($text)){
            $text = preg_replace('/[\x{0600}-\x{FFFF}]/u', '', $text);
        }

        $text = preg_replace('/\x{000a}/', "\n", $text);
        $text = preg_replace('/\n/', "<br />\n", $text);
        $text = preg_replace('/$/', "<br />\n\n", $text);

        return ($text);
    }
    
    // shipping group name 
    public static function is_dir_writeable($path) {
        $path = rtrim($path, '/\\');

        $testfile = sprintf('%s%stestfile_%s.chk', $path, DIRECTORY_SEPARATOR, uniqid());
        $timestamp = time();

        if (@file_put_contents($testfile, $timestamp))
        {
            $result = trim(file_get_contents($testfile));
            @unlink($testfile);

            if ((int)$result == (int)$timestamp)
            {
                return (true);
            }
        }

        return (false);
    }
    
    // 17/02/2016 : Bug: sometimes, some wrong EAN were not filtered
    public static function codeCheck($code) {
	if (!is_numeric($code) || strlen($code) < 12) {
	    return (false);
	}
	//first change digits to a string so that we can access individual numbers
	$digits = sprintf('%012s', Tools::substr(sprintf('%013s', $code), 0, 12));
	// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
	$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
	// 2. Multiply this result by 3.
	$even_sum_three = $even_sum * 3;
	// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
	$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
	// 4. Sum the results of steps 2 and 3.
	$total_sum = $even_sum_three + $odd_sum;
	// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
	$next_ten = (ceil($total_sum / 10)) * 10;
	$check_digit = (int)$next_ten - $total_sum;
	$last_digit = (int)Tools::substr($code, strlen($code) - 1, 1);

	return ((int)$last_digit == (int)$check_digit);
    }    
    
    public static function send_mail($title, $email_from, $email_from_name, $email_to, $message, $type='text/html', $setBcc=null){

        require_once dirname(__FILE__) . '/../../Swift/swift_required.php';
        $subject = $title;

        try {
            $transport = Swift_SmtpTransport::newInstance();
            $mailer = Swift_Mailer::newInstance($transport);
            $swift_message = Swift_Message::newInstance();
            $swift_message->setSubject($subject);
            $swift_message->setFrom(array($email_from => $email_from_name));
            $swift_message->setTo($email_to);
            $swift_message->setBody($message, $type);
            if(isset($setBcc)){                
                $swift_message->setBcc($setBcc);
            }
            if ($mailer->send($swift_message) == false) {
                return array('pass'=>false, 'output'=>'exception_handler_error');
            }
        } catch (Exception $ex) {
            return array('pass'=>false, 'output'=>$ex);
        }

        return array('pass'=>true);
    }

    //http://stackoverflow.com/questions/2856942/how-to-check-if-the-word-is-japanese-or-english-using-php
    public static function isJapanese($word) {
        return preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u', $word);
    }

}

if (!function_exists('is_serialized')) :
    function is_serialized($value, &$result = null)
    {
        if (!is_string($value)) {
            return false;
        }
        if ($value === 'b:0;') {
            $result = false;
            return true;
        }
        $length = strlen($value);
        $end = '';
        if (isset($value[0])) {
            switch ($value[0]) {
            case 's': if ($value[$length - 2] !== '"') {
                return false;
            }
            case 'b':
            case 'i':
            case 'd': $end .= ';';
            case 'a':
            case 'O': $end .= '}';
                if ($value[1] !== ':') {
                    return false;
                }
                switch ($value[2]) {
                    case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
                        break;
                    default: return false;
                }
            case 'N': $end .= ';';
                if ($value[$length - 1] !== $end[0]) {
                    return false;
                }
                break;
            default:
                return false;
        }
        }
        if (($result = @unserialize($value)) === false) {
            $result = null;
            return false;
        }
        return true;
    }
endif;