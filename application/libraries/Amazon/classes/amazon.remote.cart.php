<?php

require_once(dirname(__FILE__) . '/../../db.php');

class AmazonRemoteCart
{
    const TABLE = 'amazon_remote_cart';
    
    public static $m_pref = '';
     
    public function __construct($user, $debug = false) {
	
	$this->db = new Db($user);
	$this->debug = $debug;
	
	if(defined('_TABLE_PREFIX_')) 
	    self::$m_pref = _TABLE_PREFIX_;
    }
    
    public function addCart($id_shop, $id_country, $order_id, $SKU, $quantity, $timestamp)
    {
        $sql = "REPLACE INTO ".self::$m_pref.self::TABLE." (id_shop , id_country, mp_order_id, reference, quantity, timestamp, date_upd)  "
		."values(".(int)$id_shop.", ".(int)$id_country.", '$order_id', '$SKU', ".(int)$quantity.", '".date('Y-m-d H:i:s', $timestamp)."', '".date('Y-m-d H:i:s')."') ;";
	
	if(!$this->debug) {
	    return $this->db->db_exec($sql);       
	} else {
	    echo '<pre><b>addCart</b> '. $sql . ' </pre>';
	    return (true) ;
	}
    }

    public function inCart($id_shop, $id_country, $order_id, $SKU)
    {
        $sql = "SELECT reference FROM ".self::$m_pref.self::TABLE." "
		. "WHERE reference = '$SKU' AND mp_order_id = '$order_id' AND id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country."; " ;
	
	$result = $this->db->db_query_str($sql);
	
	if(isset($result[0]['reference']))
	    return (true);
	else 
	    return (false);
    }

    public function removeFromCart($id_shop, $id_country, $order_id, $SKU)
    {
	$sql = "DELETE FROM ".self::$m_pref.self::TABLE."  "
		. "WHERE reference = '$SKU' AND mp_order_id = '$order_id' AND id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country."; " ;
		
	if(!$this->debug) {
	    return $this->db->db_exec($sql);       
	} else {
	    echo '<pre><b>removeFromCart</b> '. $sql . ' </pre>';
	    return (true) ;
	}	
    }

    public function expiredCarts($id_shop, $id_country)
    {
	$sql = "SELECT * FROM ".self::$m_pref.self::TABLE." "
		. "WHERE timestamp < '".date('Y-m-d H:i:s', strtotime('-7 days'))."' AND id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country."; " ;

        if($this->debug)
            echo '<pre><b>expiredCarts</b> '. $sql . ' </pre>';

	$result = $this->db->db_query_str($sql);
        return($result);
    }

}