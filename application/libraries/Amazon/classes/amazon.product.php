<?php

if (!defined('BASEPATH')) define('BASEPATH', '');

require_once dirname(__FILE__) . '/amazon.config.php';
require_once(dirname(__FILE__) . '/amazon.database.php');
require_once(dirname(__FILE__) . '/amazon.error.resolution.php');
require_once dirname(__FILE__) . '/amazon.strategies.php';
require_once(dirname(__FILE__) . '/amazon.tools.php');
require_once(dirname(__FILE__) . '/../../FeedBiz.php');
require_once(dirname(__FILE__) . '/../../eucurrency.php');

class Amazon_Product {

    public $params;
    public $db_offer;
    public $operation_type;
    public $id_marketplace;
    public $MappingFeature = array();
    public $MappingAttribute = array();
    public $ProductRepricing = array();
    public $ShippingOverrideStd = null;
    public $ShippingOverrideExp = null;
    public $Amazon_override = array();
    public $ProductsStrategy = array();
    public $Amazon_fba = array();
    public $fullfillmentCenterId = array();
    public $Amazon_fba_logs = array();
    public $dimensionException = array('g'=>'gr');
    
    const LENGTH_TITLE = 500;
    const LENGTH_DESCRIPTION = 1900; // Maximum is 2000, we stlip wording.
    const IMAGE_DEFAULT = 'Main';
    const IMAGE_PT = 'PT';
    const EXEMPTION_NONE = 0;
    const EXEMPTION_COMPATIBILITY = 1;
    const EXEMPTION_MODEL_NUMBER = 2;
    const EXEMPTION_MODEL_NAME = 3;
    const EXEMPTION_MFR_PART_NUMBER = 4;
    const EXEMPTION_CATALOG_NUMBER = 5;
    const EXEMPTION_GENERIC = 10;


    public function __construct($params, $db) {

        $this->params = $params;
	
        $this->database = $db;
        $this->error_resolution = new amazonErrorResolution($this->params->user_name);
        $this->id_marketplace = (defined('_ID_MARKETPLACE_AMZ_')) ? _ID_MARKETPLACE_AMZ_ : 2;
        $this->debug = $db->debug;
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);        
    }

    public function export_product($products, $operationMode, $option = null) {
          
        $params = $this->params;
        
        if(isset($params->update_type)){
            $update_type = $params->update_type;
        }
        if(isset($option->update_type)){
            $update_type = $option->update_type;
        }

        $iso_code = $params->iso_code;        
        $this->operation_type = $operationMode;
        $fba = $repricing = false;
		
        $ShippingGroupNames = $this->database->get_shipping_group_names($params->id_shop, $params->id_country);
        $ShippingOverride = $this->database->get_mapping_shipping_override($params->id_country, $params->id_shop);

        if(isset($ShippingOverride['standard']['mapping']) && !empty($ShippingOverride['standard']['mapping']))
            $this->ShippingOverrideStd = $ShippingOverride['standard']['mapping'];
        
        if(isset($ShippingOverride['express']['mapping']) && !empty($ShippingOverride['express']['mapping']))
            $this->ShippingOverrideExp = $ShippingOverride['express']['mapping'];
        
        $feature_code_examption = $feature_synchronization_field = $feature_sku_as_supplier_reference = $feature_price_rules = $feature_second_hand = false;
        $features = $this->database->get_configuration_features($params->id_country, $params->id_shop);

        if(isset($features['code_examption']) && !empty($features['code_examption'])){
            $feature_code_examption = true;
        }
        if(isset($features['synchronization_field']) && !empty($features['synchronization_field'])){
            $feature_synchronization_field = true;
        }
        if(isset($features['sku_as_supplier_reference']) && !empty($features['sku_as_supplier_reference'])){
            $feature_sku_as_supplier_reference = true;
        }
        if(isset($features['price_rules']) && !empty($features['price_rules'])){
            $feature_price_rules = true;
        }
        if(isset($features['second_hand']) && !empty($features['second_hand'])){
            $feature_second_hand = true;
        } 
        if(isset($features['fba']) && $features['fba']){
            $fba = true;
        } 
        if(isset($features['repricing']) && $features['repricing']){
            $repricing = true;
        } 
        
        $currencies = new Eucurrency();
                
        switch ($params->synchronization_field) {
            case 'ean13' :
                $ps_code = 'ean13';
                $az_code = 'EAN';
                break;
            case 'upc' :
                $ps_code = 'upc';
                $az_code = 'UPC';
                break;
            case 'reference' :
                $ps_code = 'reference';
                $az_code = 'SKU';
                break;
            case 'both' :
                $params->sync_both_mode = true;
                break;
        }

        // Products Update
        $productsUpdate = new stdClass();
        $RepriceProductsUpdated = new stdClass();
        
        $Relationship = $data = $skip_message =  $models = $Override = array();
        $u = $productIndex = $skipped = $masterProduct = 0;        
        
        if ($products) {
             
	    if($this->debug){
		echo 'count product #1 : <pre>' . count($products) . '</pre>';
            }
            $list_id_products = $list_skus = array();
            foreach ($products as $product){
                array_push($list_id_products, $product['id_product']);
                if(isset($product['sku']) && !empty($product['sku'])){
                    array_push($list_skus, $product['sku']);
                }
            }


            // FBA
            if($fba && $this->operation_type != AmazonDatabase::DELETE){
                $this->Amazon_fba = AmazonUserData::getFBA($params->id_customer, $params->ext, $params->id_shop);
                $this->fullfillmentCenterId = Amazon_WebService::languageToFullfillmentCenterId($params->iso_code);

		// fba_data
		if(isset($this->Amazon_fba['fba_estimated_fees']) || isset($this->Amazon_fba['fba_fulfillment_fees']))
		    $this->Amazon_fba_data = $this->database->get_fba_data($params->id_shop, $params->id_country, $list_skus) ;

                if(isset($this->Amazon_fba['fba_stock_behaviour']))
                {
                    require_once(dirname(__FILE__) . '/../functions/amazon.fba.stock.php');
                    $process = AmazonFBAStock::PROCESS_KEY;
                    $type = $this->Amazon_fba['fba_stock_behaviour'];
                    $order_by = 'date_add ASC';

                    // get fba logs //01/06/2016
                    $this->Amazon_fba_logs = $this->database->get_fba_logs($params->id_shop, $params->id_country, $list_skus, $process, $type, $order_by);
                }
            }
            
            // Repricing.
            if($repricing && $this->operation_type != AmazonDatabase::DELETE){
               
                // get product updated from log.db => table product_update_log on today to repricing the active and instock product.
                if( $this->operation_type == AmazonDatabase::SYNC) {
                    $datetime = date('Y-m-d', strtotime($params->time));
                    $product_updated_log = $this->database->feedbiz->getProductUpdateLog($params->user_name, $params->id_shop, $datetime);

                    if($this->debug)
                        echo 'count product_updated_log #1 : <pre>' . count($product_updated_log) . '</pre>';

                    // check flag update by id_product & id_country 
                    $product_updated_list = $this->database->get_amazon_repricing_flag($params->id_shop, $params->id_country, $product_updated_log, $datetime);

                    if($this->debug)
                        echo 'count product_updated_list #1 : <pre>' . count($product_updated_list) . '</pre>';
                    
		    unset($product_updated_log);
		    
                    if(is_array($product_updated_list) && !empty($product_updated_list) && count($product_updated_list) > 0){
                        foreach ($product_updated_list as $id_updated_list => $value_updated_list ){
                            array_push($list_id_products, $id_updated_list); // add to global query
                            array_push($products, $value_updated_list); // add to $products
			    array_push($list_skus, $value_updated_list['sku']); // add to sku list // use to get repricing product
                        }  
                    }       
                }		
                // Product Repricing 
                $this->ProductRepricing = $this->database->get_product_repricings($params->id_shop, $params->id_country, $list_skus);

                if($this->debug)
                    echo 'Product Repricing : <pre>' . count($this->ProductRepricing) . '</pre>';
     
                // Product Strategies
                $strategy = new AmazonStrategy($params->user_name, $this->debug);
                $this->ProductsStrategy = $strategy->get_products_strategies($params->id_shop, $params->id_country, $list_id_products);

                if($this->debug)
                    echo 'Product Strategy : <pre>' . count($this->ProductsStrategy) . '</pre>';
	    }
            
            // Get product details
            $Product = new Product($params->user_name);
            $product_list = $Product->exportProductsArray($params->id_shop, null, $params->id_lang, array('product_category'=>true), $list_id_products);              

            // Product Options
	    $AmazonProductOption = new MarketplaceProductOption($params->user_name, null, null, null, null, $params->id_country, $this->id_marketplace);	    
            $ProductOption = $AmazonProductOption->get_product_options($params->id_shop, null, $list_skus);
            
            // Error Resolution Override
            $this->Amazon_override = $this->error_resolution->getAmazonOverrides($params->id_shop, $params->id_country);	    
	    
            // Mapping condition
            $conditions = $this->database->feedbiz->getConditionMapping($params->user_name, $params->id_shop, $this->id_marketplace);
                    
	    // get disabled products 
	    $query_disabled_product = '';
	    $disabled_products = $this->database->get_disabled_products($params->id_shop, $params->id_country, $list_skus);

            // Profiles
	    $amazon_profiles = $this->database->get_amazon_profile($params->id_country, $params->id_mode, $params->id_shop);

            // Categories
	    $amazon_categories = $this->database->get_category_selected($params->id_country,  $params->id_mode, $params->id_shop, true);

            foreach ($products as $key => $val) {
                
                $productIndex++;
                $force = $disabled = false;
                $data = array();
                $profile = $parent_sku = $id_product_attribute = $data['manufacturer'] = $sku = null;

                // id_product
                $id_product = intval($val['id_product']);
                $skip_id = $id_product;
                $skip_name = $id_product;
                $data['id_product'] = $id_product;
                // id_product_attribute
                if (isset($val['id_product_attribute'])) {
                    $id_product_attribute = (int) $val['id_product_attribute'];
                    $skip_id = $id_product . '_' . $id_product_attribute;
                    $skip_name = $id_product . '/' . $id_product_attribute;

                    $data['id_product_attribute'] = $id_product_attribute;
                }

                // Product                             
                if(!isset($product_list[$id_product]) || empty($product_list[$id_product]))
                    continue;
                
                $details = $product_list[$id_product];  
                    
		// set rounding
                $data['rounding'] = 2; 
		
                // DELETE INACTIVE PRODUCT || DISABLE PRODUCT
                if ($this->operation_type == AmazonDatabase::DELETE 
			&& isset($option->delete) && ($option->delete == AmazonDatabase::DELETE_INACTIVE || $option->delete == AmazonDatabase::DELETE_DISABLED) 
			&& isset($val['sku'])) {

                    $val_sku = $val['sku'];

                    if ($details['has_attribute'] > 0) {
                        
                        if (isset($details['reference']) && !empty($details['reference'])) {
                            $reference = $details['reference'];
                        } else {
                            $reference = isset($details[$ps_code]) ? $details[$ps_code] : $val['sku'];
                        }

                        // Parent
                        // If product have changed reference we use old_reference to delete the product
                        if($option->delete == AmazonDatabase::DELETE_INACTIVE && isset($details['old_reference']) && !empty($details['old_reference'])){
                            $reference = $details['old_reference'];                            
                        }

                        //$productsUpdate[$reference] = $this->set_product($reference, $data, $profile, $id_product_attribute, true);
                        $productsUpdate->{"$reference"} = $this->set_product($reference, $data, $profile, $id_product_attribute, true);

                        // Children
                        // If product have changed reference we use old_reference to delete the product
                        if($option->delete == AmazonDatabase::DELETE_INACTIVE
                            && isset($details['combination'][$id_product_attribute]['old_reference']) && !empty($details['combination'][$id_product_attribute]['old_reference'])){
                            $val_sku = $details['combination'][$id_product_attribute]['old_reference'];                           
                        }

                    } else {
                        if($option->delete == AmazonDatabase::DELETE_INACTIVE
                            && isset($details['old_reference']) && !empty($details['old_reference'])){
                            $val_sku = $details['old_reference'];
                        }
                    }

                    $productsUpdate->{"$val_sku"} = $this->set_product($val_sku, $data);
                    continue;
                }		
		
                $name_attributes = null;
                $product_name = isset($details['name'][$iso_code]) ? $details['name'][$iso_code] : '';
                $skip_name = $product_name . " (ID" . $id_product . ") ";

                // Attribute Name
                if ($details['has_attribute'] > 0) {
                    if (isset($details['combination'][$id_product_attribute])) {
                        // Name 
                        $attr_name = array();
                        
                        if (isset($details['combination'][$id_product_attribute]['attributes'])) {
                            foreach ($details['combination'][$id_product_attribute]['attributes'] as $key => $attribute) {
                                if (isset($attribute['value'])) {
                                    foreach ($attribute['value'] as $value) {                                        
                                        if(isset($value['name'][$iso_code])){
                                            $attr_name[$key] = $value['name'][$iso_code];
                                        } else {
                                           $attr_name[$key] =  current($value['name']);
                                        }
                                        if(isset($attribute['name'][$iso_code]) && isset($value['name'][$iso_code])){
                                            $skip_attr_name[$key] = $attribute['name'][$iso_code] . ' ' . $value['name'][$iso_code];
                                        } else {
                                            $skip_attr_name[$key] = current($attribute['name']) . ' ' . current($value['name']);
                                        }
                                    }
                                }
                            }
                        }  
                        if (isset($attr_name) && !empty($attr_name)) {
                            $name_attributes = implode(", ", $attr_name);
                        }
                        if (isset($skip_attr_name) && !empty($skip_attr_name)) {
                            $skip_name_attributes = implode(", ", $skip_attr_name);
                        }
                    }
                }

                if (!empty($skip_name_attributes)) {
                    $skip_name = $product_name . " : " . $skip_name_attributes . " (ID" . $id_product . "/" . $id_product_attribute . ") ";
                }
                
                // If inactive product
                if ((!$details['active'])) {
                    // Skipp when create product
                    if($this->operation_type == AmazonDatabase::CREATE && (!isset($option->send_only_image) || !$option->send_only_image)){
                        $skip_message[$skip_id] = 'm000001'; //are_inactive	 
                        $skipped++;
                        continue;                        
                    // Send 0 when offer
                    } else {
                        $data['quantity'] = 0;
                    }
                }    
                
                if (!isset($val['id_product_attribute'])) {
                    $id_product_attribute = 0;
                }

                // if global disable product when create product, skipped
                if(isset($details['disable']) && $details['disable']){
		    /*if($this->operation_type == AmazonDatabase::CREATE) {
			$skip_message[$skip_id] = 'm000023'; //Disabled_product
			$skipped++;
			continue;
		    }*/
                    $disabled = true;
                }

                // Product option
                $product_option = isset($ProductOption[$id_product][$id_product_attribute]) ? $ProductOption[$id_product][$id_product_attribute] : null;
                
		// if disable product when create product, skipped
                if(isset($product_option['disable']) && !empty($product_option['disable'])){
		    if($this->operation_type == AmazonDatabase::CREATE) {
			$skip_message[$skip_id] = 'm000002'; //Disabled_product
			$skipped++;
			continue;
		    }
                }                
          
		// switch to the right category
                $category_set = isset($details['category']) ? $details['category'] : null;

                if (isset($category_set) && !empty($category_set)) {
		
                    $id_category = reset($category_set);
		    
                    if (count($category_set) > 1) {
                        if (in_array($details['id_category_default'], $category_set)) {
			    
                            $id_category = (int) $details['id_category_default'];
			    
                        } elseif (isset($amazon_profiles) && isset($amazon_categories)) {
			    
                            // Product has multiple categories in category selection
                            if (count(array_intersect($category_set, array_keys($amazon_categories))) > 1) {
				
				$category_profiles = array();
				
				foreach ($category_set as $cate_set) {
				    if(isset($amazon_categories[$cate_set]['id_profile']) && !empty($amazon_categories[$cate_set]['id_profile'])) {
					$category_profiles[] = $amazon_categories[$cate_set]['id_profile'];
				    }
				}
				
                                if (count($category_profiles) > 1) {
				    $skip_message[$skip_id] = "m000020;"; //Product "%s" has several profiles in serveral categories !
                                }
                            }
			    
                        }
			
                    }
                } elseif ($details['id_category_default']) {
                    $id_category = (int)$details['id_category_default'];
                } else {
		    
		    $skip_message[$skip_id] = "m000021;"; //Product "%s" has no category !
		    $skipped++;
                    continue;
                }
		
                // Manufacturer
                if (isset($details['id_manufacturer']) && isset($details['manufacturer'][$details['id_manufacturer']])) {
                    $data['id_manufacturer'] = $details['id_manufacturer'];
                    $data['manufacturer'] = $details['manufacturer'][$details['id_manufacturer']];
                }

                // Do not export price
                if ( (isset($product_option['no_price_export']) && $product_option['no_price_export'] == 1) || 
                     (isset($details['no_price_export']) && $details['no_price_export'] == 1) ){
                    
                    $data['no_price_export'] = true;                   
                        
                } 
                
                // Do not export quantity
                if ( (isset($product_option['no_quantity_export']) && $product_option['no_quantity_export'] == 1) || 
                     (isset($details['no_quantity_export']) && $details['no_quantity_export'] == 1) ) {
                    $data['no_quantity_export'] = true;
                }
                
                // force - product option
                if ( (isset($product_option['force']) && !empty($product_option['force'])) ) {
                    $force = $product_option['force'];
                }
                
		// Gift Message
                if (isset($product_option['gift_wrap']) && (bool)$product_option['gift_wrap']) {
                    $data['gift_wrap'] = true;
                    $data['gift_message'] = (bool)$product_option['gift_message'] ? true : false;
                } else {
                    $data['gift_wrap'] = false;
                    $data['gift_message'] = false;
                }
                
                // FBA 
		if($fba) {  
		    // FBA in product options
		    if(isset($product_option['fba'])){
			$data['fba'] = $product_option['fba'];
			if(isset($product_option['fba_value'])) {
			    $data['fba_value'] = $product_option['fba_value'];
			}
		    } 
		}
		
		// Browse Node
		if(isset($product_option['browsenode']) && $product_option['browsenode']){
                    $data['browsenode'] = array($product_option['browsenode']);
                }		
		
		// Shipping
                // if allow shipping override //2016-03-30
                if(isset($features['shipping_override']) && $features['shipping_override']){
                    // if have shipping override price
                    if (isset($product_option['shipping'])) {
                        $data['shipping'] = $product_option['shipping'];
                    } 
                }
                 
                // Profile
                if (isset($val['id_profile']) && !empty($val['id_profile'])) {
                    $profile_selected = $val['id_profile'];
                } else {
                    $profile_selected = $this->database->get_id_profile_selected($id_category, $params->id_country, $params->id_mode, $params->id_shop);
                }                    
		
                if (isset($amazon_profiles[$profile_selected]) && !empty($amazon_profiles[$profile_selected])) {
                    
		    $profile = $amazon_profiles[$profile_selected];	
		    
		    // price rules
                    $price_rules = null;
		    if (isset($profile['price_rules']['value']) && !empty($profile['price_rules']['value']) && $feature_price_rules){
			$price_rules = $profile['price_rules']['value'];
		    }
		    
                    // BulletPoint by features
                    if (isset($profile['key_product_features']['features']['value']) && !empty($profile['key_product_features']['features']['value'])) {
			
                        foreach ($profile['key_product_features']['features']['value'] as $feature_key => $feature) {                
			    
                            if (isset($details['feature'][$feature]['name']) && isset($details['feature'][$feature]['value'])) {
                                if (isset($profile['key_product_features']['features']['set_name']) && $profile['key_product_features']['features']['set_name']==1) {
                                    if(isset($details['feature'][$feature]['value'][$iso_code])){
                                        $data['bullet_point'][$feature_key] = $details['feature'][$feature]['name'] . ': ' . $details['feature'][$feature]['value'][$iso_code];
                                    }
                                } else {
                                    if(isset($details['feature'][$feature]['value'][$iso_code])){
                                        $data['bullet_point'][$feature_key] = $details['feature'][$feature]['value'][$iso_code];
                                    }
                                }
				
			    // Custom Value
                            } else if($feature == "CustomValue") {
				
				if(isset($profile['key_product_features']['features']['CustomValue'][$feature_key]) 
				&& !empty($profile['key_product_features']['features']['CustomValue'][$feature_key]))
				{
				    $data['bullet_point'][$feature_key] = $profile['key_product_features']['features']['CustomValue'][$feature_key];
				}
			    }
                        }			
			
                    // BulletPoint by description
		    } else if (isset($profile['key_product_features']['descriptions']) && !empty($profile['key_product_features']['descriptions'])) {

                        if (isset($details['description'][$iso_code]) && !empty($details['description'][$iso_code])) {
                            if ($profile['key_product_features']['descriptions'] == "description" && !empty($details['description'][$iso_code])) {
                                $descriptions = Amazon_Tools::remove_html_tag($details['description'][$iso_code]);
                            } else if ($profile['key_product_features']['descriptions'] == "description_short" && 
				    isset($details['description_short'][$iso_code]) && !empty($details['description_short'][$iso_code])) {
                                $descriptions = Amazon_Tools::remove_html_tag($details['description_short'][$iso_code]);
                            }

                            $description = Amazon_Tools::split_words($descriptions, 100);
                            if (isset($description) && !empty($description)) {
                                $data['bullet_point'] = $description;
                            }
                        }
                    }
                    
                    // IF CREATION MODE ADD PRODUCT DATA                    
                    $id_model = isset($profile['id_model']) ? $profile['id_model'] : '';

                    if (!isset($models[$id_model])) {
                        $models[$id_model] = $this->database->get_amazon_model_by_id($id_model, $params->id_shop, $params->id_lang);
                    }

                    $model = $models[$id_model];

                    if (isset($model[0])) {
                        $profile['model'] = isset($model[0]) ? $model[0] : null;
                    } elseif (isset($params->creation) && $params->creation == true && !isset($model[0])) {
			$skip_message[$skip_id] = "m000003;".$profile['name']; //Missing_model_in_s_profile_setting
                        $skipped++;
                        continue;
                    }
		                        
                    $out_of_stock = $profile['out_of_stock'];
                    
                    // Latency
                    if (isset($profile['association']['latency']) && !empty($profile['association']['latency']) && (int)$profile['association']['latency'] )
                        $data['latency'] = $profile['association']['latency'];
                        
                    if (isset($profile['code_exemption']) && !empty($profile['code_exemption']) && $feature_code_examption == true)
                        $data['p_code_exemption'] = true;

                    if (isset($profile['sku_as_supplier_reference']) && !empty($profile['sku_as_supplier_reference']) && $feature_sku_as_supplier_reference == true)
                        $data['p_sku_as_supplier_reference'] = true;

                    if (isset($profile['sku_as_supplier_reference']['unconditionnaly']) && !empty($profile['sku_as_supplier_reference']['unconditionnaly']))
                        $data['p_sku_as_sup_ref_unconditionnaly'] = true;

                    $profile_name = $profile['name'];
                    $profile_id = $profile['id_profile'];

                    if ($profile_id == false || empty($profile_name)) {
                        if (isset($params->creation) && $params->creation == true) {
			    $skip_message[$skip_id] = "m000004;(1)"; //Missing_profile_setting
                            $skipped++;
                            continue;
                        }
                    }

                    if (isset($profile['synchronization_field']) && !empty($profile['synchronization_field']) && $feature_synchronization_field == true) {
                        switch ($profile['synchronization_field']) {
                            case 'ean13' :
                                $ps_code = 'ean13';
                                $az_code = 'EAN';
                                break;
                            case 'upc' :
                                $ps_code = 'upc';
                                $az_code = 'UPC';
                                break;
                            case 'reference' :
                                $ps_code = 'reference';
                                $az_code = 'SKU';
                                break;
                            case 'both' :
                                $params->sync_both_mode = true;
                                break;
                        }
                    }                    
                    
                    //Item Type
                    if (isset($profile['recommended_browse_node']['item_type']) && !empty($profile['recommended_browse_node']['item_type'])) {
                        $data['item_type'] = $profile['recommended_browse_node']['item_type'];
                    }
                
                }  else if (isset($params->creation) && $params->creation == true) {
		    $skip_message[$skip_id] = "m000004;(2)"; //Missing_profile_setting
                    $skipped++;
                    continue;
                }     
		
		// bullet point from offer options
		if(isset($product_option) && !empty($product_option)) {
		    for ($i = 1 ; $i<=5 ; $i++)
		    if(isset($product_option['bullet_point' . $i]) && !empty($product_option['bullet_point' . $i])){			
			$data['bullet_point'][($i-1)] = $product_option['bullet_point' . $i];
		    }
		}	
                
                // Latency
                if (isset($product_option['latency']) && !empty($product_option['latency']) && (int)$product_option['latency'] ) {
                    $data['latency'] = $product_option['latency'];
                }               

                // Both Synch Mode (EAN/UPC)
                if (isset($params->sync_both_mode) && $params->sync_both_mode) {
                    $az_code = 'EAN';
                    $ps_code = 'ean13';
                    if (isset($details['ean13']) && !empty($details['ean13'])) {
                        $az_code = 'EAN';
                        $ps_code = 'ean13';
                    } elseif (isset($details['upc']) && !empty($details['upc'])) {
                        $az_code = 'UPC';
                        $ps_code = 'upc';
                    }
                }

                // Reference
                if (isset($details['reference']) && !empty($details['reference'])) {
                    $reference = $details['reference'];
                    $sku = 'reference';
                } else if (isset($details[$ps_code]) && !empty($details[$ps_code])) {
                    $reference = $details[$ps_code];
                    $sku = $ps_code;
                } else {
                    if (isset($details['ean13']) && !empty($details['ean13'])) {
                        $az_code = 'EAN';
                        $ps_code = 'ean13';
                        $sku = 'ean13';
                    } elseif (isset($details['upc']) && !empty($details['upc'])) {
                        $az_code = 'UPC';
                        $ps_code = 'upc';
                        $sku = 'upc';
                    } else {
                        $ps_code = ( isset($details['reference']) && !empty($details['reference']) ) ? 'reference' : '';
                        $sku = $ps_code;
                    }
                    if (isset($details[$ps_code]) && !empty($details[$ps_code])) {
                        $reference = $details[$ps_code];
                    } else {
			$skip_message[$skip_id] = "m000005;$az_code"; //Has_No_s
                        $skipped++;
                        continue;
                    }
                }

                //ASIN
		if(isset($profile['association']['use_asin']) && $profile['association']['use_asin']) { // if check use asin in profile
		    if(isset($product_option['asin1']) && !empty($product_option['asin1'])) {
                        $data['asin'] = $product_option['asin1'];
		    } else {
			$asin = $this->database->getAsinBySKU($params->id_country, $params->id_shop, $reference);
			$data['asin'] = (isset($asin) && !empty($asin)) ? $asin : ( (isset($val['asin']) && !empty($val['asin'])) ? $val['asin'] : null);
		    }
		}

                // Reference Type
                $data['reference'] = isset($details[$ps_code]) ? $details[$ps_code] : null ; //$reference;
                $data['reference_type'] = $az_code;

                // Description
                if (isset($details['description'][$iso_code]) && !empty($details['description'][$iso_code])) {
                    $product_description = $details['description'][$iso_code];
                    if (isset($profile['description_field'])) {
                        switch ($profile['description_field']) {
                            case 1: $product_description = $details['description'][$iso_code];
                                break;
                            case 2: $product_description = $details['description_short'][$iso_code];
                                break;
                            case 3: $product_description = $details['description'][$iso_code] . ' ' . $details['description_short'][$iso_code];
                                break;
                            case 4: $product_description = '';
                                break;
                        }
                    }
                    if (isset($profile) && $profile['html_description']) {
                        $data['description'] = trim($product_description);
                    } else {
                        $data['description'] = Amazon_Tools::clean_strip_tags(($product_description)); // 13/01/2016
                    }		    
                }
                
                // Condition 
                $mapping_condition = false;
                if (isset($details['id_condition']) && !empty($details['id_condition']) && isset($conditions[$details['id_condition']]['condition_value'])) {
                    $mapping_condition = $conditions[$details['id_condition']]['condition_value'];
                }

                // Allow conditon when Not Offer & when MODE_SYNC_MATCH
                if((!isset($update_type) || $update_type != 'offer') || (isset($params->mode) && $params->mode == AmazonDatabase::MODE_SYNC_MATCH))
                {
                    if ($mapping_condition) {

                        if($feature_second_hand == true) {            
                            $data['condition'] = $mapping_condition;                    
                        } else {                        
                            if($mapping_condition == 'New'){
                                $data['condition'] = 'New'; 
                            } else {
				$skip_message[$skip_id] = "m000006;$mapping_condition"; //Condition_S_not_allow
                                $skipped++;
                                continue;
                            }
                        }
                    } else {
                        
			if($feature_second_hand == true) {  
			    
			    if (!isset($details['id_condition']) || empty($details['id_condition'])) {
				$skip_message[$skip_id] = "m000007"; //Missing Condition
			    } 
			    else if (!isset($conditions[$details['id_condition']]['condition_value'])) {
				$skip_message[$skip_id] = "m000008"; //Missing Mapping Condition
			    }
			    $skipped++;
			    continue;			    
			} else {
			    $data['condition'] = 'New'; 
			}
                    }
                }
                
                // condition note
                if (isset($product_option['text']) && !empty($product_option['text'])) {
                    $data['condition_note'] = $product_option['text'];
                }

                // Quantity
                $data['quantity'] = $details['quantity'];                    

                // Price
                $original_price = isset($details['original_price']) ? $details['original_price'] : $details['price'];
                $price = $details['price'];
                $is_global_override = isset($details['global_override'])? (bool)$details['global_override'] : false;
                    
                // is Global Override
                //wholesale_price
                $data['wholesale_price'] = $details['wholesale_price'];
                        
                // Dimentions
                $data['width'] = $details['width'];
                $data['height'] = $details['height'];
                $data['depth'] = $details['depth'];
                $data['weight'] = $details['weight'];

                if(isset($data['width']['unit']) && isset($this->dimensionException[$data['width']['unit']]))
                {
                    $data['width']['unit'] = $this->dimensionException[$data['width']['unit']];
                }
                if(isset($data['height']['unit']) && isset($this->dimensionException[$data['height']['unit']]))
                {
                    $data['height']['unit'] = $this->dimensionException[$data['height']['unit']];
                }
                if(isset($data['depth']['unit']) && isset($this->dimensionException[$data['depth']['unit']]))
                {
                    $data['depth']['unit'] = $this->dimensionException[$data['depth']['unit']];
                }
                if(isset($data['weight']['unit']) && isset($this->dimensionException[$data['weight']['unit']]))
                {
                    $data['weight']['unit'] = $this->dimensionException[$data['weight']['unit']];
                }

                // Supplier reference
                if (isset($details['supplier_reference'])) {
                    $data['supplier_reference'] = $details['supplier_reference'];
                }

                // Available date
                $data['available_date'] = $details['available_date'];

                //Images
                $data['images'] = array();
                $ptm = 1;
                if (isset($details['image'])) {
                    foreach ($details['image'] as $img_key => $img_value) {
                         
                        if ($img_value['image_type'] == "default") {
                            $data['images'][Amazon_Product::IMAGE_DEFAULT] = $img_value['image_url'];
                        } else {
                            if ($ptm <= 8) {
                                $data['images'][Amazon_Product::IMAGE_PT . $ptm] = $img_value['image_url'];
                            }
                            $ptm++;
                        }
                    }
                }

                // Feature
                $data['feature'] = $details['feature'];

                // Tag
                if (isset($details['tags'][$id_product]) && !empty($details['tags'][$id_product])) {
                    $data['search_terms'] = Amazon_Tools::getMarketplaceTags($details['tags'][$id_product], $iso_code);
                }

                // Currency
                $to_currency = $params->currency;
                foreach ($details['currency'] as $currency) {
                    $from_currency = $currency['iso_code'];
                }

                // Name
                $master_name = trim(mb_substr($product_name, 0, self::LENGTH_TITLE));
                $standard_name = $master_name;
                $data['name'] = $standard_name;

                if (isset($profile)) {
                    switch ($profile['title_format']) {
                        case AmazonDatabase::FormatManufacturerTitle :
                            if (!isset($data['manufacturer']) || empty($data['manufacturer'])) {
                                $data['name'] = $standard_name;
                                break;
                            }
                            $master_name = trim(mb_substr(sprintf('%s - %s', $data['manufacturer'], $product_name), 0, self::LENGTH_TITLE));
                            $data['name'] = $master_name;
                            break;

                        case AmazonDatabase::FormatManufacturerTitleReference :
                            if (!isset($data['manufacturer']) || empty($data['manufacturer'])) {
                                $data['name'] = $standard_name;
                                break;
                            }
                            $master_name = trim(mb_substr(sprintf('%s - %s - %s', $data['manufacturer'], $product_name, trim($reference)), 0, self::LENGTH_TITLE));
                            $data['name'] = $master_name;
                            break;
                        default :
                            $data['name'] = $standard_name;
                            break;
                    }
                }

                // Combination
                if ($details['has_attribute'] > 0) {
                   
                    //Parent
                    if (isset($profile['model']) && !empty($profile['model'])) {
                        if (!isset($option->delete) || $option->delete != AmazonDatabase::DELETE_OUT_OF_STOCK) { 
                            $parent_sku = $reference;                           
                        }
                    }
                    
                    $data['has_attribute'] = $details['has_attribute'];
                    $data['combination'] = $details['combination'];
                    
                    if (isset($data['combination'][$id_product_attribute])) {
                        // ID 
                        $id_product_attribute = $data['combination'][$id_product_attribute]['id_product_attribute'];
                        $data['id_product_attribute'] = $id_product_attribute;

                        // Disable - Global
                        if(isset($data['combination'][$id_product_attribute]['disable']) && $data['combination'][$id_product_attribute]['disable']){
                            /*if($this->operation_type == AmazonDatabase::CREATE) {
                                $skip_message[$skip_id] = 'm000023'; //Disabled_product
                                $skipped++;
                                continue;
                            }*/
                            $disabled = true;
                        }
                        // Do not export price
                        if (isset($data['combination'][$id_product_attribute]['no_price_export']) && $data['combination'][$id_product_attribute]['no_price_export'] == 1) {
                            $data['no_price_export'] = true;
                        }
                        // Do not export quantity
                        if (isset($data['combination'][$id_product_attribute]['no_quantity_export']) && $data['combination'][$id_product_attribute]['no_quantity_export'] == 1) {
                            $data['no_quantity_export'] = true;
                        }

                        if (isset($data['combination'][$id_product_attribute]['reference']) && !empty($data['combination'][$id_product_attribute]['reference'])) {
                            $reference = $data['combination'][$id_product_attribute]['reference'];
                        } else if (isset($data['combination'][$id_product_attribute][$ps_code]) && !empty($data['combination'][$id_product_attribute][$ps_code])) {
                            $reference = $data['combination'][$id_product_attribute][$ps_code];
                        } else {
			    $skip_message[$skip_id] = "m000005;$az_code"; //Has_No_s
                            $skipped++;
                            continue;
                        }

                        //ASIN
                        if(isset($profile['association']['use_asin']) && $profile['association']['use_asin']) { // if check use asin in profile
                            if(isset($product_option['asin1']) && !empty($product_option['asin1'])) {
                                $data['asin'] = $product_option['asin1'];
                            } else {
                                $asin = $this->database->getAsinBySKU($params->id_country, $params->id_shop, $reference);
                                $data['asin'] = (isset($asin) && !empty($asin)) ? $asin : ( (isset($val['asin']) && !empty($val['asin'])) ? $val['asin'] : null);
                            }
                        }

                        $data['reference'] = isset($data['combination'][$id_product_attribute][$ps_code]) ? $data['combination'][$id_product_attribute][$ps_code] : null;//$reference;
                        $data['reference_type'] = $az_code;

                        if ($ps_code != 'reference' && (!isset($data['asin']) || empty($data['asin']))) {
                            $EAN_UPC = trim($data['reference']);

                            if (!$EAN_UPC) {
				$skip_message[$skip_id] = "m000009;$ps_code"; //Missing_standardProductID_type
                                $skipped++;
                                continue;
                            }

                            // Check EAN/UPC consistency
			    //EAN_UPC_Check
                            if (!Amazon_Tools::EAN_UPC_Check($EAN_UPC)) {
				$skip_message[$skip_id] = "m000010;$EAN_UPC"; //s_is_incorrect_EAN_UPC
                                $skipped++;
                                continue;
                            }
                        }

                        //quantity
                        $data['quantity'] = $data['combination'][$id_product_attribute]['quantity'];
                                                    
                        if (!$force && isset($data['combination'][$id_product_attribute]['force']) && !empty($data['combination'][$id_product_attribute]['force'])) {
                            $force = $data['combination'][$id_product_attribute]['force'];
                        }
                        
                        // New: 22/09/2016
                        if($operationMode == AmazonDatabase::CREATE && (!isset($data['fba']) || !$data['fba'])) {
                            if (!$this->params->create_out_of_stock && ($data['quantity'] <= 0) && !$force && (!isset($option->send_only_image) || !$option->send_only_image)) {
                                $skip_message[$skip_id] = "m000011"; //out_of_stock
                                $skipped++;
                                continue;
                            }
                        }
                        
                        // Price
                        $original_price = isset($data['combination'][$id_product_attribute]['original_price']) ? $data['combination'][$id_product_attribute]['original_price'] : $data['combination'][$id_product_attribute]['price'];
                        $price = $data['combination'][$id_product_attribute]['price'];
                        $is_global_override = isset($data['combination'][$id_product_attribute]['global_override'])? (bool)$data['combination'][$id_product_attribute]['global_override'] : false;

                        // wholesale_price
                        $data['wholesale_price'] = $data['combination'][$id_product_attribute]['wholesale_price'];
                        
                        // Dimentions
                        if (isset($data['combination'][$id_product_attribute]['weight']['value'])) {
                            $product_weight = isset($data['weight']['value']) ? floatval($data['weight']['value']) : 0;
                            $data['weight']['value'] = $product_weight + floatval($data['combination'][$id_product_attribute]['weight']['value']);
                            $data['weight']['unit'] = $data['combination'][$id_product_attribute]['weight']['unit'];
                        }

                        // Available date                        
                        $data['available_date'] = $data['combination'][$id_product_attribute]['available_date'];

                        //Images
                        if (isset($data['combination'][$id_product_attribute]['images']) && !empty($data['combination'][$id_product_attribute]['images'])) {
                            $data['images'] = array();
                            $pt = 1;
                            foreach ($data['combination'][$id_product_attribute]['images'] as $img_key => $img_value) {
                              
                                if (isset($img_value['url'])) {
                                    if ($img_value['type'] == "default") {
                                        $data['images'][Amazon_Product::IMAGE_DEFAULT] = $img_value['url'];
                                    } else {
                                        if ($ptm <= 8) {
                                            $data['images'][Amazon_Product::IMAGE_PT . $pt] = $img_value['url'];
                                        }
                                        $pt++;
                                    }
                                }
                            }
                        } 
                       
                        //Parent
                        if (isset($parent_sku) && !empty($parent_sku)) {
                            // NEW : 22/09/2016
                            if($data['quantity'] > 0 || $force || (isset($this->params->create_out_of_stock) && $this->params->create_out_of_stock) || (isset($data['fba']) && $data['fba'])){
                                $productsUpdate->{"$parent_sku"} = $this->set_product($parent_sku, $data, $profile, null, true);
                            }
                        } 
                    }
                    
                    //The goal is the first combination is the parent and the subsequent the childrens
                    if (isset($profile['model']['VariationTheme']) && !empty($profile['model']['VariationTheme'])) {
                        if (isset($reference) && !empty($reference) && isset($parent_sku)) {
                            if ($parent_sku != $reference) {
                                if (!isset($Relationship[$parent_sku])) {
                                    // Adding children to the relationships - first child
                                    $Relationship[$parent_sku] = array();
                                    $Relationship[$parent_sku]['parent'] = $parent_sku;
                                    $Relationship[$parent_sku]['children'] = array();
                                    $Relationship[$parent_sku]['children'][$reference] = $reference;                                  
                                    $productIndex++;
                                } else {
                                    // Adding children to the relationships - children         
                                    $Relationship[$parent_sku]['children'][$reference] = $reference;
                                }
                            } 
                        }                        
                    }
                }
                
                if (isset($history[$reference])) {
                    if ($skip_name != $history[$reference]) {
			$skip_message[$skip_id] = "m000012;$reference;".$history[$reference]; //Duplicate_entry_for_s_Previously_used_by_Product_ID_s_This_is_not_allowed
                        $skipped++;
                    }
                    continue;
                }

                if (!isset($reference) || empty($reference)) {
		    $skip_message[$skip_id] = "m000005"; //Has_No_s
                    $skipped++;
                    continue;
                }

                $check_length = false;
                if (isset($update_type) && ($update_type != 'offer')) { // check lenght when not offer
                    $check_length = true;
                }

                // StandardProductID 8-16
                if ($check_length && isset($data['reference']) && (strlen($data['reference']) < 8 || strlen($data['reference']) > 16)) {
                    if (!isset($parent_sku) || empty($parent_sku)) {
			//s_length_must_be_greater_or_equal_to_8_and_less_or_equal_to_16_Currently_value_s
			$skip_message[$skip_id] = "m000013;" . $az_code . ' - ' . $data['reference'] .";". strlen($reference); 
                        $skipped++;
                        continue;
                    }
                }
                
                // SKU 1-16
                if (/*$check_length && */isset($data['reference']) && (strlen($data['reference']) < 1 || strlen($data['reference']) > 16)) {
		    //s_length_must_be_greater_or_equal_to_8_and_less_or_equal_to_16_Currently_value_s
		    $skip_message[$skip_id] = "m000013;" . $az_code . ' - ' . $data['reference'] .";". strlen($reference);
                    $skipped++;
                    continue;
                }
                
                // SKU 1-40
                if ($check_length && isset($reference) && (strlen($reference) < 1 || strlen($reference) > 40)) {
		    //s_length_must_be_greater_or_equal_to_1_and_less_or_equal_to_40_Currently_value_s
		    $skip_message[$skip_id] = "m000014;" . $az_code . ' - ' . $reference .";". strlen($reference);
                    $skipped++;
                    continue;
                }
                
                if ($ps_code != 'reference' && (!isset($data['asin']) || empty($data['asin']))) {

                    $EAN_UPC = trim($data['reference']);

                    if (!$EAN_UPC) {
			$skip_message[$skip_id] = "m000009;$ps_code"; //Missing_standardProductID_type
                        $skipped++;
                        continue;
                    }
                    // Check EAN/UPC consistency
                    if (!Amazon_Tools::EAN_UPC_Check($EAN_UPC)) {
			$skip_message[$skip_id] = "m000010;$EAN_UPC"; //s_is_incorrect_EAN_UPC
                        $skipped++;
                        continue;
                    }
                }
                
                // Quantity
                if ($force) {
                     $data['quantity'] = $force;
                }
                
		// stock
		if (isset($out_of_stock) && (int)$out_of_stock){
		    $data['quantity'] -= $out_of_stock;
		}

                // Skip price = 0
                if($original_price < 0.01){
                    $skip_message[$skip_id] = "m000017;".$original_price;
                    $skipped++;
                    continue;
                }
                
                // DELETE //
                if ($this->operation_type == AmazonDatabase::DELETE && isset($option->delete) && $option->delete != AmazonDatabase::DELETE_INACTIVE) {
                    // If send only out of stock
                    if (isset($option->delete) && $option->delete == AmazonDatabase::DELETE_OUT_OF_STOCK) {
                        // Out of Stock                      
                        if ($data['quantity'] > 0) {
                            continue;
                        }
                        $val_sku = $val['sku'];
                        $productsUpdate->{"$val_sku"} = $this->set_product($val['sku']);
                        continue;
                    }
                }
                // END DELETE //
                else {
                    // Out of Stock policies
		    if($operationMode == AmazonDatabase::CREATE && (!isset($data['fba']) || !$data['fba']))
		    {
			if (!$this->params->create_out_of_stock && $data['quantity'] < 1 && (!isset($option->send_only_image) || !$option->send_only_image)) {
                            if(isset($Relationship[$parent_sku]['children'][$reference])){
                                unset($Relationship[$parent_sku]['children'][$reference]);
                            }
			    //Minimum of quantity required to be in stock to export the product: %s Current stock: %s
			    $skip_message[$skip_id] = "m000015;1;".$data['quantity'];
			    $skipped++;
			    continue;  
			}
		    }
                }
                
                // Quantity exceeds 99,999,999.
                if (isset($data['quantity']) && $data['quantity'] > 99999999 && (!isset($option->send_only_image) || !$option->send_only_image)) {  
		    //This error occurs when the available inventory you declare for a product is greater than 99,999,999. Current value : %s
		    $skip_message[$skip_id] = "m000016;".$data['quantity'];
                    $skipped++;
                    continue;  
                }

                if ($operationMode == AmazonDatabase::SYNC && isset($data['quantity']) && $data['quantity'] < 0 && (!isset($option->send_only_image) || !$option->send_only_image)) {
		    //This error occurs when the available inventory you declare for a product is less than 0. Current value : %s
                    $data['quantity'] = 0;
                }

		// recheck : sync disable product - if quantity = 0
		if(isset($product_option['disable']) || $disabled){
		    
		    if($this->operation_type == AmazonDatabase::CREATE) {

			if($product_option['disable']) { // skipped disabled when create
                            if(isset($Relationship[$parent_sku]['children'][$reference])){
                                unset($Relationship[$parent_sku]['children'][$reference]);
                            }
			    $skip_message[$skip_id] = "m000002"; //Disabled_product
			    $skipped++;
			    continue;
			} elseif ($disabled) {
                            if(isset($Relationship[$parent_sku]['children'][$reference])){
                                unset($Relationship[$parent_sku]['children'][$reference]);
                            }
                            $skip_message[$skip_id] = 'm000023'; //Disabled_product
                            $skipped++;
                            continue;
                        }
			
		    } else {
			
			// when sync
			// 
			if($product_option['disable'] || $disabled) {  // if product are disabled , $product_option['disable'] == 1
			    
			    // check if this product have in amazon_disabled_products : it's not first time , skipped
			    if(isset($disabled_products[$reference]) && $disabled_products[$reference]){

                                if($product_option['disable']) { // skipped disabled when create
                                    $skip_message[$skip_id] = "m000002"; //Disabled_product
                                    $skipped++;
                                    continue;
                                } elseif ($disabled) {
                                    $skip_message[$skip_id] = 'm000023'; //Disabled_product
                                    $skipped++;
                                    continue;
                                }

			    } else { // not have in amazon_disabled_products : first time , send zero

				// first time disabled
				$data['quantity'] = 0;
				$data['no_price_export'] =  true;

				// Log to amazon_disabled_product
				$data_disabled_product = array(
				    'id_product' => $id_product,
				    'id_product_attribute' => $id_product_attribute, 
				    'sku' => $reference, 
				    'id_shop' => $this->params->id_shop,
				    'id_country' => $this->params->id_country,
				    'date_upd' => date('Y-m-d H:i:s')
				);

				$query_disabled_product .= $this->database->replace_string(Amazondatabase::$m_pref.'amazon_disabled_products', $data_disabled_product);
			    }
			    
			} else { // if product are enabled but after disabled , $product_option['disable'] == 0
			    
			    // check if this product have in amazon_disabled_products : remove the product, now it's enabled
			    if(isset($disabled_products[$reference]) && $disabled_products[$reference]){
				
				$where_disabled_product = array(
				    'id_product' => array('operation' => "=", 'value' => $id_product),
				    'id_product_attribute' => array('operation' => "=", 'value' => $id_product_attribute),
				    'sku' => array('operation' => "=", 'value' => $reference),
				    'id_shop' => array('operation' => "=", 'value' => $this->params->id_shop),
				    'id_country' => array('operation' => "=", 'value' => $this->params->id_country),
				);
				
				$query_disabled_product .= $this->database->delete_string(Amazondatabase::$m_pref.'amazon_disabled_products', $where_disabled_product);
			    }
			    
			}
		    }
                }
		
		// recheck : inactive product
                if ((!$details['active'])) {
                    // Send 0 when offer
		    $data['quantity'] = 0;
                }    
		
                $history[$reference] = $skip_name; 
                
		//MerchantShippingGroupName
		if(isset($product_option['shipping_group']) && !empty($product_option['shipping_group'])) {
                    if(isset($ShippingGroupNames[$product_option['shipping_group']])) {
                        $data['shipping_group'] = $ShippingGroupNames[$product_option['shipping_group']];
                    }
		} else if(isset($profile['association']['shipping_group'])){
		    if(isset($ShippingGroupNames[$profile['association']['shipping_group']])) {
			$data['shipping_group'] = $ShippingGroupNames[$profile['association']['shipping_group']];
		    }
		}

                // FBA shipping template : 2016-08-25
                if ($fba && isset($data['fba']) && $data['fba']){
                    if(isset($this->Amazon_fba['default_shipping_template']) && isset($ShippingGroupNames[$this->Amazon_fba['default_shipping_template']])){
                        $data['shipping_group'] = $ShippingGroupNames[$this->Amazon_fba['default_shipping_template']];
                    }
                }
                
		// Rounding				
                if(isset($profile['price_rules']['rounding']) && !empty($profile['price_rules']['rounding']) && $feature_price_rules) {
                    $data['rounding'] = $profile['price_rules']['rounding'];
		}
		
		// Minimum Price
		if(isset($this->ProductsStrategy[$id_product][$id_product_attribute]['minimum_price'])) {
		    $data['minimum_price'] = $this->ProductsStrategy[$id_product][$id_product_attribute]['minimum_price'];
		    if (isset($from_currency) && !empty($from_currency)){
			$data['minimum_price'] = $currencies->doDiffConvert($data['minimum_price'], $from_currency, $to_currency);    
		    }
		}

		// Maximum Price                
		if(isset($this->ProductsStrategy[$id_product][$id_product_attribute]['target_price'])) {
		    $data['target_price'] = $this->ProductsStrategy[$id_product][$id_product_attribute]['target_price'];
		    if (isset($from_currency) && !empty($from_currency)){
			$data['target_price'] = $currencies->doDiffConvert($data['target_price'], $from_currency, $to_currency);    
		    }
		}

                // Price               
                $data['originalPrice'] = $original_price;
                $data['newPrice'] = $price;
                
		// 1. check price rule
                if (isset($price_rules) && !empty($price_rules) && $feature_price_rules && isset($profile['price_rules']['rounding']) && !$is_global_override)
                    $data['newPrice'] = Amazon_Product::price_rule($price_rules, $profile['price_rules']['rounding'], $data['newPrice']);
                
                // 2. check global price override
                if($is_global_override)
                    $data['newPrice'] = $price;
                
		// 3. check amazon price override
                if(isset($product_option['price']) && !empty($product_option['price'])) //Net Price for Marketplace. This value will replace your regular price
                    $data['newPrice'] = $product_option['price'];

                // Sale parent               
                if(!isset($product_option['price']) || !$product_option['price'] || empty($product_option['price'])) { // Skip Sale when product have price override
                    if(isset($details['on_sale']) && $details['on_sale']) {
                        if(!isset($profile['price_rules']['no_sale']) || empty($profile['price_rules']['no_sale']) || !$profile['price_rules']['no_sale']) { // if allow sale
                            if (isset($details['sale'][0]) && !empty($details['sale'][0])){
                                $data['sale'] = $details['sale'][0];
                            // Sale children
                            } else if (isset($details['sale'][$id_product_attribute]) && !empty($details['sale'][$id_product_attribute])) {
                                $data['sale'] = $details['sale'][$id_product_attribute];
                            }
                        }
                        if(isset($data['sale']) && !empty( $data['sale']) && $data['newPrice'] > 0) {
                             // sale date exception //2016-10-06
                            $sale_date_from = $data['sale']['date_from'];
                            $sale_date_to = $data['sale']['date_to'];
                            if($sale_date_from == "0000-00-00 00:00:00")
                                $data['sale']['date_from'] = date('Y-m-d H:i:s', strtotime('first day of January ' . date('Y')));
                            if($sale_date_to == "0000-00-00 00:00:00")
                                $data['sale']['date_to'] = date('Y-m-d H:i:s', strtotime('last day of December ' . date('Y')));
                            
                            $data['sale']['date_from'] = date('c', Amazon_Tools::ceil_time(strtotime($data['sale']['date_from'])));
                            $data['sale']['date_to'] = date('c', Amazon_Tools::ceil_time(strtotime($data['sale']['date_to'])));
                            if($data['sale']['reduction_type'] == 'amount'){
                                $data['sale']['price'] = $data['newPrice'] - $data['sale']['reduction'] ;
                            } else {
                                $data['sale']['price'] = ($data['newPrice'] - ( $data['newPrice'] * ($data['sale']['reduction']/100) ) );
                            }
                        }
                    }
                }
		
		// 4. Convert Price
                if (isset($from_currency) && !empty($from_currency)) {
		    
		    // Price 
                    $data['newPrice'] = $currencies->doDiffConvert($data['newPrice'], $from_currency, $to_currency);
                    $data['originalPrice'] = $currencies->doDiffConvert($data['originalPrice'], $from_currency, $to_currency);
                    
		    // Sale Price  
		    if(isset($data['sale']['price']) && $data['sale']['price'] > 0) {
			 $data['sale']['price'] = $currencies->doDiffConvert($data['sale']['price'], $from_currency, $to_currency);
		    }
		}
                
		// 5. check target price
		//if(isset($data['target_price'])) {
                //    $data['newPrice'] = $data['target_price'];
		//}
		       
		// 6. add fba to price.
		if ($fba && isset($data['fba']) && $data['fba']) {
		    		    
		    // Overide FBA Value Added
		    if (isset($data['fba_value']) && (float)$data['fba_value'] > 0) {

			$data['newPrice'] += (float)$data['fba_value'];

			if (isset($data['sale']) && !empty($data['sale'])) {

			    $data['sale']['price']  += (float)$data['fba_value'];
			    $data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
			    
			}

			$data['fba_value_added'] = $data['fba_value'];
		    } 
		    // FBA Price Value Added
		    elseif( (isset($this->Amazon_fba_data[$reference]['estimated_fee']) || isset($this->Amazon_fba_data[$reference]['fulfilment_fee']) ) ){
						
			$data['fba_value_added'] = 0;

    			// Estimated fee
    			if( isset($this->Amazon_fba_data[$reference]['estimated_fee']) && isset($this->Amazon_fba['fba_estimated_fees']) ) {
    			
    			    $estimated_fees = $this->Amazon_fba_data[$reference]['estimated_fee'];
    			    $fba_estimated_fees = Amazon_Tools::Formula($estimated_fees, $this->Amazon_fba['fba_estimated_fees']) - $estimated_fees;

			    $data['newPrice'] += (float) $fba_estimated_fees;

    			    if (isset($data['sale']) && !empty($data['sale'])) {
        				$data['sale']['price']  += (float)$fba_estimated_fees;
        				$data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
    			    }	

			    $data['fba_value_added'] += (float) $fba_estimated_fees;      

    			}
    			
    			//fba_fulfillment_fees
    			if( isset($this->Amazon_fba_data[$reference]['fulfilment_fee']) && isset($this->Amazon_fba['fba_fulfillment_fees']) ){
    			    
    			    $fulfillment_fees = $this->Amazon_fba_data[$reference]['fulfilment_fee'];
    			    $fba_fulfillment_fees = Amazon_Tools::Formula($fulfillment_fees, $this->Amazon_fba['fba_fulfillment_fees']) - $fulfillment_fees;
    			    
    			    $data['newPrice'] += (float) $fba_fulfillment_fees;

    			    if (isset($data['sale']) && !empty($data['sale'])) {
    				$data['sale']['price']  += (float) $fba_fulfillment_fees;
    				$data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
    			    }

			    $data['fba_value_added'] += (float) $fba_fulfillment_fees;  
    			
    			}	

		    }
                    elseif( isset($this->Amazon_fba['default_fba_fees']) ){

                        $data['fba_value_added'] = $this->Amazon_fba['default_fba_fees'];
                        $data['newPrice'] = $data['fba_value_added'] + $data['newPrice'] ;
                        
                    }
		    // FBA Default Price Value Added
		    elseif( (isset($this->Amazon_fba['default_fba_estimated_fees']) || isset($this->Amazon_fba['default_fba_fulfillment_fees']) ) ){
			
			$data['fba_value_added'] = 0;

    			// Estimated fee
    			if( isset($this->Amazon_fba['default_fba_estimated_fees'])) {
    			
			    $default_fba_estimated_fees = $original_price * ((float)$this->Amazon_fba['default_fba_estimated_fees'] / 100);
    			    $data['newPrice'] = $default_fba_estimated_fees + $data['newPrice'] ;
    			
			    if ($feature_price_rules && isset($data['sale']['price']) && !empty($data['sale']['price']))
			    {
				$data['sale']['price'] = Amazon_Tools::Formula($data['sale']['price'], $this->Amazon_fba['default_fba_estimated_fees']);
				$data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
			    }
			    
			    $data['fba_value_added'] = $default_fba_estimated_fees;
			    
    			}
    			
    			//fba_fulfillment_fees
    			if( isset($this->Amazon_fba['default_fba_fulfillment_fees'])) {
    			
			    $default_fba_fulfillment_fees = $original_price * ((float)$this->Amazon_fba['default_fba_fulfillment_fees'] / 100);
    			    $data['newPrice'] += $default_fba_fulfillment_fees + $data['newPrice'] ;
    			
			    if ($feature_price_rules && isset($data['sale']['price']) && !empty($data['sale']['price']))
			    {
				$data['sale']['price'] = Amazon_Tools::Formula($data['sale']['price'], $this->Amazon_fba['default_fba_fulfillment_fees']);
				$data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
			    }
			    
			    $data['fba_value_added'] += $default_fba_fulfillment_fees;

    			}	

		    }		
		    // FBA Formula
		    elseif (isset($this->Amazon_fba['fba_formula']))
		    {
    			$data['newPrice'] = Amazon_Tools::Formula($original_price, $this->Amazon_fba['fba_formula']);
    			
    			if ($feature_price_rules && isset($data['sale']['price']) && !empty($data['sale']['price']))
    			{
    			    $data['sale']['price'] = Amazon_Tools::Formula($data['sale']['price'], $this->Amazon_fba['fba_formula']);
    			    $data['sale']['price'] = round($data['sale']['price'], $data['rounding']);
    			}

			$data['fba_formula'] = $this->Amazon_fba['fba_formula'];
		    } 
		}
		   
		if (array_key_exists('minimum_price', $data))
		{
		    $data['minimum_price'] = min($data['minimum_price'], $data['newPrice']);
		}

		if (array_key_exists('target_price', $data))
		{
		    $data['target_price'] = max($data['target_price'], $data['newPrice']);
		}
		
                // Price 0
                if (!isset($data['newPrice']) || $data['newPrice'] < 0.01 || empty($data['newPrice'])) {  
		    //0.00 price (standard or sales) will not be accepted. Please ensure that price of at least equal to or greater than 0.01. Current value : %s
		    $skip_message[$skip_id] = "m000017;".$data['newPrice'];
                    $skipped++;
                    continue;  
                }		
		  
		// wholesale price convert rate
		if (isset($from_currency) && !empty($from_currency))
		    $data['wholesale_price'] = $currencies->doDiffConvert($data['wholesale_price'], $from_currency, $to_currency) ;
		
                // Name
                $master_name = trim(mb_substr($product_name, 0, self::LENGTH_TITLE));

                if (isset($name_attributes) && !empty($name_attributes) && isset($id_product_attribute)) {
                    $standard_name = trim(mb_substr(sprintf('%s - %s', $product_name, rtrim($name_attributes, ', ')), 0, self::LENGTH_TITLE));
                } else {
                    $standard_name = $master_name;
                }

                $data['name'] = $standard_name;
                
                if (isset($profile)) {
                    switch ($profile['title_format']) {
                        case AmazonDatabase::FormatManufacturerTitle :
                            if (!isset($data['manufacturer']) || empty($data['manufacturer'])) {
                                $data['name'] = $standard_name;
                                break;
                            }
                            $master_name = trim(mb_substr(sprintf('%s - %s', $data['manufacturer'], $product_name), 0, self::LENGTH_TITLE));

                            if (isset($name_attributes) && !empty($name_attributes) && isset($id_product_attribute)) {
                                $data['name'] = trim(mb_substr(sprintf('%s - %s - %s', $data['manufacturer'], $product_name, rtrim($name_attributes, ', ')), 0, self::LENGTH_TITLE));
                            } else {
                                $data['name'] = $master_name;
                            }
                            break;

                        case AmazonDatabase::FormatManufacturerTitleReference :
                            if (!isset($data['manufacturer']) || empty($data['manufacturer'])) {
                                $data['name'] = $standard_name;
                                break;
                            }
                            $master_name = trim(mb_substr(sprintf('%s - %s - %s', $data['manufacturer'], $product_name, trim($reference)), 0, self::LENGTH_TITLE));

                            if (isset($name_attributes) && !empty($name_attributes) && isset($id_product_attribute)) {
                                $data['name'] = trim(mb_substr(sprintf('%s - %s - %s - %s', $data['manufacturer'], $product_name, trim($reference), rtrim($name_attributes, ', ')), 0, self::LENGTH_TITLE));
                            } else {
                                $data['name'] = $master_name;
                            }
                            break;
                        default :
                            $data['name'] = $standard_name;
                            break;
                    }
                }
                $u = "$reference";

                # If has_attribute but empty $id_product_attribute --> this item is parents
                if (isset($details['has_attribute']) && $details['has_attribute'] > 0 && (!isset($id_product_attribute) || empty($id_product_attribute))) {
                    $productsUpdate->{"$u"} = $this->set_product($reference, $data, $profile, $id_product_attribute, true);
                } else {                    
                    $productsUpdate->{"$u"} = $this->set_product($reference, $data, $profile, $id_product_attribute);
                }

                // if VariationTheme not Match variation_data skip ; 10-06-2016
                if(!isset($update_type) || $update_type != 'offer') // when not offer
                {
                    if(isset($productsUpdate->{"$u"}['ProductData']['variation_data']['missing'])){
                        $VariationTheme = isset($profile['model']['VariationTheme']) ? $profile['model']['VariationTheme'] : $profile['name'];
                        $skip_message[$skip_id] = "m000024;" .$productsUpdate->{"$u"}['ProductData']['variation_data']['missing']. ";" . $VariationTheme . ";" . $profile['name'];
                        $skipped++;
                        unset($productsUpdate->{"$u"});
                        if(isset($Relationship[$parent_sku]['children'][$reference])){
                            unset($Relationship[$parent_sku]['children'][$reference]);
                        }
                        //continue;
                        $count_relation = !isset($count_relation) ? 1 : ($count_relation+1);
                    }

                    $count_relation = !isset($count_relation) ? 0 : $count_relation;
                    
                    // If parent have no children skip ; 
                    if(isset($details['has_attribute']) && $details['has_attribute'] > 0 && ($details['has_attribute'] == $count_relation) && isset($productsUpdate->{"$parent_sku"})){
                        //if($this->debug){
                        //    echo '<br/> Parent : ' . $parent_sku . '<br/>';
                        //    echo '<br/> Refernce : ' . $reference . '<br/>';
                        //    echo '<br/> has_attribute : ' . $details['has_attribute'] . '<br/>';
                        //    echo '<br/> count_relation : ' . $count_relation . '<br/>';
                        //}
                        
                        $skip_message[$skip_id] = "m000025";
                        $skipped++;
                        unset($productsUpdate->{"$parent_sku"});
                    }
                }


                # Shipping Override 05/08/2015
                if ((isset($this->ShippingOverrideStd) && !empty($this->ShippingOverrideStd)) || (isset($this->ShippingOverrideExp) && !empty($this->ShippingOverrideExp)) ) {

                    if($this->operation_type == AmazonDatabase::CREATE){
                        if(isset($data['shipping']) && !empty($data['shipping']) && $data['shipping']){
                            $Override[$reference]['SKU'] = $reference;
                            $Override[$reference]['Shipping']['Amount'] = round($data['shipping'], $data['rounding']);
                            $Override[$reference]['Shipping']['Type'] = 'Exclusive';

                            if(isset($this->ShippingOverrideExp)){
                                $Override[$reference]['Shipping']['Option'] = $this->ShippingOverrideExp;
                            } elseif(isset($this->ShippingOverrideStd)) {
                                $Override[$reference]['Shipping']['Option'] = $this->ShippingOverrideStd;
                            }
                        }
                    } else {
                        if(isset($data['shipping']) /*&& !empty($data['shipping']) && $data['shipping']*/) {
                            $Override[$reference]['SKU'] = $reference;
                            $Override[$reference]['Shipping']['Amount'] = round($data['shipping'], $data['rounding']);
                            $Override[$reference]['Shipping']['Type'] = 'Exclusive';

                            if(isset($this->ShippingOverrideExp))
                            {
                                $Override[$reference]['Shipping']['Option'] = $this->ShippingOverrideExp;
                            } elseif(isset($this->ShippingOverrideStd)) {
                                $Override[$reference]['Shipping']['Option'] = $this->ShippingOverrideStd;
                            }
                       }
                    }
                }  

                # repricing updated product 14/09/2015
                if(isset($productsUpdate->{"$u"}))
                {
                    if(isset($product_updated_list[$id_product])){

                        $RepriceProductsUpdated->{"$u"} = $productsUpdate->{"$u"};

                        // get condition for repricing only
                        if (isset($mapping_condition) && $mapping_condition) {
                            $RepriceProductsUpdated->{"$u"}['condition'] = $mapping_condition;
                        } else {

                            if (!isset($details['id_condition']) || empty($details['id_condition'])) {
                                $skip_message[$skip_id] = "m000018"; //Repricing failed due to condition missing
                            }
                            else if (!isset($conditions[$details['id_condition']]['condition_value'])) {
                                $skip_message[$skip_id] = "m000019"; //Repricing failed due to mapping condition missing
                            }

                            $skipped++;
                            unset($RepriceProductsUpdated->{"$u"});
                        }

                        // when trigger repricing skip to send inventory to avoid too much item process ; 09-06-2016
                        unset($productsUpdate->{"$u"});
                        $productIndex--;

                        $productReprice = !isset($productReprice) ? 0 : ($productReprice+1) ;

                    # if never reprice and not in a triggered list , do reprice when repricing is activated.
                    } else {

                        if($fba && isset($data['fba']) && !empty($data['fba'])){

                            if(!isset($this->ProductRepricing[$u]) || empty($this->ProductRepricing[$u]) || !$this->ProductRepricing[$u] ){
                                $RepriceProductsUpdated->{"$u"} = $productsUpdate->{"$u"};

                                // get condition for repricing only
                                if (isset($mapping_condition) && $mapping_condition) {
                                     $RepriceProductsUpdated->{"$u"}['condition'] = $mapping_condition;
                                } else {

                                    if (!isset($details['id_condition']) || empty($details['id_condition'])) {
                                        $skip_message[$skip_id] = "m000018"; //Repricing failed due to condition missing
                                    }
                                    else if (!isset($conditions[$details['id_condition']]['condition_value'])) {
                                        $skip_message[$skip_id] = "m000019"; //Repricing failed due to mapping condition missing
                                    }

                                    $skipped++;
                                    unset($RepriceProductsUpdated->{"$u"});
                                }
                            }
                        }
                    }
                } 
            }
	    
	    unset($ShippingGroupNames);
	    unset($ShippingOverride);
	    unset($features);
	    unset($currencies);
	    unset($Product);
	    unset($product_list);
	    unset($AmazonProductOption);
	    unset($ProductOption);
	    unset($conditions);
	    unset($disabled_products);
	    unset($amazon_profiles);
	    unset($amazon_categories);
        }
        
        if (isset($skip_message) && !empty($skip_message)) {
            $this->log_skipped($skip_message, 'warning');
        }
        
	if(strlen($query_disabled_product) > 10 ){
	    if($this->debug){
		 echo '<pre>query_disabled_product<br/>' . print_r($query_disabled_product, true) . '</pre>'; 
	    } else {
		$this->database->exec_query($query_disabled_product);
	    }
	}
	
	unset($query_disabled_product);
	unset($skip_message);
	
        $exportProducts = array();
        $exportProducts['productIndex'] = $productIndex;

        if(isset($productReprice)){
            $exportProducts['productReprice'] = $productReprice;
        }

        $exportProducts['skipped'] = $skipped;

        if (isset($productsUpdate) && !empty($productsUpdate)) {
            $exportProducts['product'] = (array) $productsUpdate;
            $exportProducts['relationship'] = $Relationship;
            
            if(isset($Override) && !empty($Override))
                $exportProducts['override'] = $Override;
            
            if(isset($RepriceProductsUpdated) && !empty($RepriceProductsUpdated))
                $exportProducts['repricing'] = (array) $RepriceProductsUpdated;
        }
	
	unset($productsUpdate);
	unset($Override);
	unset($RepriceProductsUpdated);
	
        return $exportProducts;
    }

    public function set_product($reference, $data = null, $profile = null, $id_product_attribute = null, $parent = false) {
       
        $reference = Amazon_Tools::rstr($reference);

        $params = $this->params;
        $iso_code = $params->iso_code;
        $id_product = null;
        $productsUpdate = array();

        $productsUpdate['SKU'] = Amazon_Tools::rstr($reference);
        $sku = $productsUpdate['SKU'];
        $id_model = null;

        if (isset($profile['id_model']) && !empty($profile['id_model'])) {
            $id_model = $profile['id_model'];
        }

        if (isset($data) && !empty($data)) {

            if (isset($data['id_product'])) {
                $productsUpdate['id_product'] = $data['id_product'];
                $id_product = $productsUpdate['id_product'];
            }

            if (isset($data['id_product_attribute'])) {
                $productsUpdate['id_product_attribute'] = $id_product_attribute = (int)$data['id_product_attribute'];
            } 

            if($parent) { // if parent // set attribute to 0
                $productsUpdate['id_product_attribute'] = $id_product_attribute = 0;
            }
            
//	    if((!isset($id_product_attribute) || empty($id_product_attribute))) {
//
//		if(isset($productsUpdate['id_product_attribute'])){
//		    $id_product_attribute = (int)$productsUpdate['id_product_attribute'];
//		} else {
//		    $id_product_attribute = 0;
//		}
//	    }
	    
            $override = isset($this->Amazon_override[$sku]) ? $this->Amazon_override[$sku] : null;

            if ($parent) {
                $productsUpdate['parent'] = true;
            }

            if (isset($override['SKU']) && !empty($override['SKU'])) {
                $productsUpdate['SKU'] = Amazon_Tools::rstr($override['SKU']);
            }

            // if creation we not use ASIN
            if ($this->operation_type == AmazonDatabase::CREATE) {
                if(isset($data['asin']) && !empty($data['asin'])) {
                    $productsUpdate['StandardProductID']['reference'] = $data['asin'];
                    $productsUpdate['StandardProductID']['reference_type'] = 'ASIN';
                } else if (!$parent && isset($data['reference']) && isset($data['reference_type'])) {
                    $productsUpdate['StandardProductID']['reference'] = isset($override['StandardProductID']) && !empty($override['StandardProductID']) ?
                            $override['StandardProductID'] : Amazon_Tools::rstr($data['reference']);
                    $productsUpdate['StandardProductID']['reference_type'] = Amazon_Tools::rstr($data['reference_type']);
                }
            } else {
                
                // Override ASIN
                if(isset($override['ASIN']) && !empty($override['ASIN'])){
                    $productsUpdate['StandardProductID']['reference'] = $override['ASIN'];
                    $productsUpdate['StandardProductID']['reference_type'] = 'ASIN';

                // Override ASIN on offer_options || amazon_inventory
                } else if(isset($data['asin']) && !empty($data['asin'])) {
                    $productsUpdate['StandardProductID']['reference'] = $data['asin'];
                    $productsUpdate['StandardProductID']['reference_type'] = 'ASIN';

                    
                } else if (isset($data['reference']) && isset($data['reference_type'])) {

                    /*if ( $this->operation_type == AmazonDatabase::CREATE && !$parent) {
                        $productsUpdate['StandardProductID']['reference'] = isset($override['StandardProductID']) && !empty($override['StandardProductID']) ?
                                $override['StandardProductID'] : Amazon_Tools::rstr($data['reference']);
                        $productsUpdate['StandardProductID']['reference_type'] = Amazon_Tools::rstr($data['reference_type']);
                    } elseif($this->operation_type == AmazonDatabase::SYNC && (isset($params->update_type) && ($params->update_type == 'product' || $params->update_type == 'all')) ){*/
                    if (!$parent) {
                         $productsUpdate['StandardProductID']['reference'] = isset($override['StandardProductID']) && !empty($override['StandardProductID']) ?
                                $override['StandardProductID'] : Amazon_Tools::rstr($data['reference']);
                        $productsUpdate['StandardProductID']['reference_type'] = Amazon_Tools::rstr($data['reference_type']);
                    }
                    //}
                }
            }

            if (isset($data['condition']) /*&& !$parent*/) {
                /*if($this->operation_type == AmazonDatabase::CREATE
                    || (isset($params->update_type) && ($params->update_type != 'offer'))
                    || (isset($params->mode) && $params->mode == AmazonDatabase::MODE_SYNC_MATCH))  // Allow conditon when MODE_SYNC_MATCH
                {*/
                    $productsUpdate['condition'] = isset($override['Condition']) && !empty($override['Condition']) ?
                            $override['Condition'] : Amazon_Tools::rstr($data['condition']);
                //}
            }
            
            // Latency
            if (isset($data['latency']) && !empty($data['latency']) && (int)$data['latency'] ) {
                $productsUpdate['FulfillmentLatency'] = $data['latency'];
            }         
            
	    $productsUpdate['repricing_mode'] = 'MFN';
	    
            // FBA // 1-MFN 2-FBA
	    $fba = isset($data['fba']) ? (bool) $data['fba'] : false ;

            if(isset($this->fullfillmentCenterId)){               
                if ($fba) {                    
                    if(isset($this->Amazon_fba_logs[$sku]) && $this->Amazon_fba_logs[$sku] != 2) // last fba in logs is not AFN(2), update SwitchFulfillmentTo
                        $productsUpdate['SwitchFulfillmentTo'] = 'AFN';

                    $productsUpdate['FBA'] = $this->fullfillmentCenterId;
                    $productsUpdate['repricing_mode'] = 'FBA';
                    if(isset($data['fba_value_added'])){
                        $productsUpdate['fba_value_added'] = $data['fba_value_added'];
                    }
                    if(isset($data['fba_formula'])){
                        $productsUpdate['fba_formula'] = $data['fba_formula'];
                    }
                } else {                    
                    if(isset($this->Amazon_fba_logs[$sku]) && $this->Amazon_fba_logs[$sku] != 1) // last fba in logs is not MFN(1), update SwitchFulfillmentTo
                        $productsUpdate['SwitchFulfillmentTo'] = 'MFN';
                }
            }
                
            // Condition Note 
            if(isset($data['condition_note']) && !empty($data['condition_note'])){
                $productsUpdate['condition_note'] = Amazon_Tools::rstr($data['condition_note']);
            } else if (isset($profile['association']['condition_note']) && !empty($profile['association']['condition_note'])) {
                $productsUpdate['condition_note'] = Amazon_Tools::rstr($profile['association']['condition_note']);
            }
            
            // no export price
            if (isset($data['no_price_export'])) {                
                $productsUpdate['no_price_export'] = $data['no_price_export'];
            } else {
                //check if product are repricing               
                if(isset($this->ProductRepricing[$productsUpdate['SKU']]['reprice'])) {
		    $productsUpdate['reprice'] =  $this->ProductRepricing[$productsUpdate['SKU']]['reprice'];
                    $productsUpdate['last_repricing_mode'] = isset($this->ProductRepricing[$productsUpdate['SKU']]['repricing_mode']) ?
                                                            $this->ProductRepricing[$productsUpdate['SKU']]['repricing_mode'] : null;
	            $productsUpdate['no_price_export'] =  true;
                } else {
                    // if repricing allow no_export price
                    $productsUpdate['no_price_export'] =  false ;
                }
            }
            
            if (isset($data['no_quantity_export'])) {
                $productsUpdate['no_quantity_export'] = $data['no_quantity_export'];
            }
            if (isset($data['quantity'])) {
                $productsUpdate['quantity'] = $data['quantity'];
            }
            	   
	     // Price
	    $productsUpdate['rounding'] = $data['rounding'];		
            
            if (isset($data['wholesale_price'])) {
                $productsUpdate['wholesale_price'] = $data['wholesale_price'];
            }
	    
            if (isset($data['available_date'])) {
                $productsUpdate['available_date'] = date('c', Amazon_Tools::ceil_time(strtotime($data['available_date'])));
            }
	    
            if(isset($data['originalPrice']) && !empty($data['originalPrice'])) {
                $productsUpdate['originalPrice'] = round($data['originalPrice'], $productsUpdate['rounding']);
            }

            if(isset($data['newPrice']) && !empty($data['newPrice'])) {
                $productsUpdate['price'] = $data['newPrice'];
                //sprintf('%.02f', round($data['newPrice'], $productsUpdate['rounding']));
                //round($data['newPrice'], $productsUpdate['rounding']);
            }
            
            if(isset($data['sale']) && !empty($data['sale'])) {
                $productsUpdate['sale'] = $data['sale'];      
                //sprintf('%.02f', round($data['sale']['price'], $productsUpdate['rounding']));
                //round($data['sale']['price'], $productsUpdate['rounding']);
            }
	     
	    // Minimum Price
	    if(isset($data['minimum_price'])) {
		$minimum_price = sprintf('%.02f', $data['minimum_price']);
		$productsUpdate['MinPrice'] = $minimum_price;
	    }
	    
	    // Maximum Price
	    if(isset($data['target_price'])) {
		$target_price = sprintf('%.02f', $data['target_price']);
		$productsUpdate['MaxPrice'] = $target_price;
	    }
         
	    // Shipping
	    if(isset($data['shipping'])) {
		$productsUpdate['ShippingOverride'] = $data['shipping'];
	    }
	    
            if (isset($data['images'])) {
                $productsUpdate['images'] = Amazon_Tools::rstr($data['images']);
            }

            // Product Sheet Elements
            if (!isset($productsUpdate['ProductDescription'])) {
                $productsUpdate['ProductDescription'] = array();
            }
            if (isset($data['name'])) {
                $productsUpdate['ProductDescription']['Title'] = isset($override['Title']) && !empty($override['Title']) ? $override['Title'] : Amazon_Tools::rstr($data['name']);
            }
            if (isset($data['description'])) {
		$productsUpdate['ProductDescription']['Description'] = mb_substr(Amazon_Tools::encodeText($data['description'], true), 0,self::LENGTH_DESCRIPTION); // 13/01/2016
            }

            if (isset($data['manufacturer']) && $data['manufacturer']) {

                $productsUpdate['ProductDescription']['Manufacturer'] = isset($override['Manufacturer']) && !empty($override['Manufacturer']) ? 
                        $override['Manufacturer'] : Amazon_Tools::rstr($data['manufacturer']);

                // In most cases it is same than manufacturer
                $productsUpdate['ProductDescription']['Brand'] = isset($override['Brand']) && !empty($override['Brand']) ? 
			$override['Brand'] : Amazon_Tools::rstr($data['manufacturer']);
            }

            // Bullet Point
            if (isset($data['bullet_point']) && !empty($data['bullet_point'])) {
                $productsUpdate['ProductDescription']['BulletPoint'] = array_slice(Amazon_Tools::rstr($data['bullet_point']), 0, 5);
            }

            // Search Terms
            if (isset($data['search_terms']) && !empty($data['search_terms'])) {
                $productsUpdate['ProductDescription']['SearchTerms'] = Amazon_Tools::rstr($data['search_terms']);
            }

            // Product Data
            if (isset($profile['model']) && !empty($profile['model'])) {

                if (!isset($productsUpdate['ProductData'])) {
                    $productsUpdate['ProductData'] = array();
                }

                //product category
                if (isset($profile['model']['product_category']) && !empty($profile['model']['product_category'])) {
                    $universe = Amazon_Tools::rstr($profile['model']['product_category']);
                    $productsUpdate['ProductData']['product_category'] = isset($override['ProductCategory']) && !empty($override['ProductCategory']) ?
                        $override['ProductCategory'] : $universe;
                }

                //product type
                if (isset($profile['model']['product_type']) && !empty($profile['model']['product_type'])) {
                    $product_type = Amazon_Tools::rstr($profile['model']['product_type']);
                    $productsUpdate['ProductData']['product_type'] = isset($override['ProductType']) && !empty($override['ProductType']) ? 
                        $override['ProductType'] : $product_type;
                }

                //product sub type
                if (isset($profile['model']['ProductSubtype']) && !empty($profile['model']['ProductSubtype']) && isset($data['has_attribute'])) {
                    $productsUpdate['ProductData']['ProductSubtype'] = Amazon_Tools::rstr($profile['model']['ProductSubtype']);
                }

                //Variation Theme
                if (isset($profile['model']['VariationTheme']) && !empty($profile['model']['VariationTheme'])) {
                    $productsUpdate['ProductData']['VariationTheme'] = Amazon_Tools::rstr($profile['model']['VariationTheme']);
                }

                // Get Mapping By universe > product_type
                if (!isset($this->MappingFeature[$universe][$product_type])) {
                    $this->MappingFeature[$universe][$product_type] = $this->database->getMappingFeatureFeed($params->id_country, $params->id_shop, $universe, $product_type);
                }
                if (!isset($this->MappingAttribute[$universe][$product_type])) {
                    $this->MappingAttribute[$universe][$product_type] = $this->database->getMappingAttributeFeed($params->id_country, $params->id_shop, $universe, $product_type);
                }
                
                //variation data
                if (isset($profile['model']['variation_data']) && !empty($profile['model']['variation_data'])) {
                    foreach ($profile['model']['variation_data'] as $attr_key => $attr) {

                        //Custom Value
                        if (isset($attr['CustomValue'])) {
                            $productsUpdate['ProductData']['variation_data'][$attr_key] = Amazon_Tools::rstr($attr['CustomValue']);
                        }
                        //Value
                        else if (isset($attr['value'])) {
                            $productsUpdate['ProductData']['variation_data'][$attr_key] = Amazon_Tools::rstr($attr['value']);
                        }
                        //Attribute
                        else if (isset($attr['attribute'])) {

                            $id_attr = $attr['attribute'];

                            if (isset($data['combination'][$id_product_attribute]['attributes'][$id_attr]['value']) && !$parent) {

                                foreach ($data['combination'][$id_product_attribute]['attributes'][$id_attr]['value'] as $a_key => $a_value) {

                                    if (isset($a_value['name'][$iso_code]) && !empty($a_value['name'][$iso_code])) {
                                        $productsUpdate['ProductData']['variation_data'][$attr_key] = Amazon_Tools::rstr($a_value['name'][$iso_code]);
                                    }

                                    if (isset($this->MappingAttribute[$universe][$product_type][$id_attr][$a_key][$attr_key])) {
                                        $Map = Amazon_Tools::rstr($this->MappingAttribute[$universe][$product_type][$id_attr][$a_key][$attr_key]);

                                        if ($attr_key == "Color" || $attr_key == "Size") {
                                            $productsUpdate['ProductData']['variation_data'][$attr_key . 'Map'] = $Map;
                                        } else {
                                            $productsUpdate['ProductData']['variation_data'][$attr_key] = $Map;
                                        }
                                    } else {
                                        if ($attr_key == "Color" || $attr_key == "Size" && (isset($a_value['name'][$iso_code]) && !empty($a_value['name'][$iso_code]))) {
                                            $productsUpdate['ProductData']['variation_data'][$attr_key . 'Map'] = Amazon_Tools::rstr($a_value['name'][$iso_code]);
                                        }
                                    }
                                }
                            }
                        }
                        //Feature
                        else if (isset($attr['feature'])) {

                            $id_attr = $attr['feature'];

                            if (isset($id_attr) && isset($data['feature'][$id_attr]['value'][$iso_code]) && !$parent) {

                                $id_feature = $data['feature'][$id_attr]['id_value'];
                                $productsUpdate['ProductData']['variation_data'][$attr_key] = Amazon_Tools::rstr($data['feature'][$id_attr]['value'][$iso_code]);

                                // Mapping
                                if (isset($this->MappingFeature[$universe][$product_type][$id_attr][$id_feature][$attr_key])) {
                                    $Map = Amazon_Tools::rstr($this->MappingFeature[$universe][$product_type][$id_attr][$id_feature][$attr_key]);

                                    if ($attr_key == "Color" || $attr_key == "Size") {
                                        $productsUpdate['ProductData']['variation_data'][$attr_key . 'Map'] = $Map;
                                    } else {
                                        $productsUpdate['ProductData']['variation_data'][$attr_key] = $Map;
                                    }
                                } else {
                                    if ($attr_key == "Color" || $attr_key == "Size") {
                                        $productsUpdate['ProductData']['variation_data'][$attr_key . 'Map'] = Amazon_Tools::rstr($data['feature'][$id_attr]['value'][$iso_code]);
                                    }
                                }
                            }
                        } 

                        if (isset($override[$attr_key]) && !empty($override[$attr_key])) {
                            $productsUpdate['ProductData']['variation_data'][$attr_key] = $override[$attr_key];
                        }

                        if (isset($override[$attr_key . 'Map']) && !empty($override[$attr_key . 'Map'])) {
                            $productsUpdate['ProductData']['variation_data'][$attr_key . 'Map'] = $override[$attr_key . 'Map'];
                        }

                        if((!isset($productsUpdate['ProductData']['variation_data'][$attr_key]) || empty($productsUpdate['ProductData']['variation_data'][$attr_key])) && !$parent){
                            $productsUpdate['ProductData']['variation_data']['missing'] = $attr_key;
                        }
                    } 

                    //Parentage
                    if (isset($profile['model']['VariationTheme']) && !empty($profile['model']['VariationTheme'])) {
                        //var_dump($sku, $parent);
                        if (isset($data['combination'][$id_product_attribute]) && !$parent) {
                            $productsUpdate['ProductData']['specific_fields']['Parentage'][0] = 'child';
                        } else {
                            if (isset($data['has_attribute']) && $parent)
                                $productsUpdate['ProductData']['specific_fields']['Parentage'][0] = 'parent';
                        }
                    }
                }

                //recommended data 
                if (isset($profile['model']['recommended_data']) && !empty($profile['model']['recommended_data'])) {

                    foreach ($profile['model']['recommended_data'] as $recom_key => $recom) {

                        //Custom Value
                        if (isset($recom['CustomValue'])) {
                            $productsUpdate['ProductData']['recommended_data'][$recom_key][0] = Amazon_Tools::rstr($recom['CustomValue']);
                        }
                        //Value
                        else if (isset($recom['value'])) {
                            $productsUpdate['ProductData']['recommended_data'][$recom_key][0] = Amazon_Tools::rstr($recom['value']);
                        } else {
                            // attribute 
                            if (isset($recom['attribute'])) {
                                
                                $id_recom = $recom['attribute'];

                                if (isset($id_recom) && isset($data['combination'][$id_product_attribute]['attributes'][$id_recom]['value'])) {

                                    foreach ($data['combination'][$id_product_attribute]['attributes'][$id_recom]['value'] as $re_key => $re_value) {

                                        if (isset($re_value['name'][$iso_code]) && !empty($re_value['name'][$iso_code])) {
                                            $productsUpdate['ProductData']['recommended_data'][$recom_key][$id_recom] = Amazon_Tools::rstr($re_value['name'][$iso_code]);
                                        }

                                        if (isset($this->MappingAttribute[$universe][$product_type][$id_recom][$re_key][$recom_key])) {
                                            $Map = Amazon_Tools::rstr($this->MappingAttribute[$universe][$product_type][$id_recom][$re_key][$recom_key]);

                                            if ($recom_key == "Color" || $recom_key == "Size") {
                                                if (!$parent) {
                                                    $productsUpdate['ProductData']['recommended_data'][$recom_key . 'Map'][$id_recom] = $Map;
                                                } else {
                                                    unset($productsUpdate['ProductData']['recommended_data'][$recom_key][$id_recom]);
                                                }
                                            } else {
                                                $productsUpdate['ProductData']['recommended_data'][$recom_key][$id_recom] = $Map;
                                            }
                                        } else {
                                            if ($recom_key == "Color" || $recom_key == "Size" && (isset($re_value['name'][$iso_code]) && !empty($re_value['name'][$iso_code]))) {
                                                $productsUpdate['ProductData']['recommended_data'][$recom_key . 'Map'][$id_recom] = Amazon_Tools::rstr($re_value['name'][$iso_code]);
                                            }
                                        }
                                    }
                                }
                                // feature
                            } else {

                                $id_recom = $recom['feature'];
                                if (isset($id_recom) && isset($data['feature'][$id_recom]['value'][$iso_code])) {
                                    $id_feature = $data['feature'][$id_recom]['id_value'];
                                    $productsUpdate['ProductData']['recommended_data'][$recom_key][$id_feature] = Amazon_Tools::rstr($data['feature'][$id_recom]['value'][$iso_code]);
                                    //The colour of the product. 
                                    //Note: Please do *NOT* add colour information to parent skus. Only child skus should have colour information associated to them.
                                    if (isset($this->MappingFeature[$universe][$product_type][$id_recom][$id_feature][$recom_key])) {
                                        $Map = Amazon_Tools::rstr($this->MappingFeature[$universe][$product_type][$id_recom][$id_feature][$recom_key]);

                                        if ($recom_key == "Color" || $recom_key == "Size") {
                                            if (!$parent) {
                                                $productsUpdate['ProductData']['recommended_data'][$recom_key . 'Map'][$id_feature] = $Map;
                                            } else {
                                                unset($productsUpdate['ProductData']['recommended_data'][$recom_key][$id_feature]);
                                            }
                                        } else {
                                            $productsUpdate['ProductData']['recommended_data'][$recom_key][$id_feature] = $Map;
                                        }
                                    } else {
                                        if ($recom_key == "Color" || $recom_key == "Size") {
                                            $productsUpdate['ProductData']['recommended_data'][$recom_key . 'Map'][$id_feature] = Amazon_Tools::rstr($data['feature'][$id_recom]['value'][$iso_code]);
                                        }
                                    }
                                }
                            }
                        }

                        // attribute_override
                        if (isset($override[$recom_key]) && !empty($override[$recom_key])) {
                            $productsUpdate['ProductData']['recommended_data'][$recom_key] = $override[$recom_key];
                        }

                        if (isset($override[$recom_key . 'Map']) && !empty($override[$recom_key . 'Map'])) {
                            $productsUpdate['ProductData']['recommended_data'][$recom_key . 'Map'] = $override[$recom_key . 'Map'];
                        }
                    }
                }

                //specific fields
                if (isset($profile['model']['specific_fields']) && !empty($profile['model']['specific_fields'])) {
                    foreach ($profile['model']['specific_fields'] as $spec_key => $spec) {
                        //Custom Value
                        if (isset($spec['CustomValue'])) {
                            $productsUpdate['ProductData']['specific_fields'][$spec_key][0] = Amazon_Tools::rstr($spec['CustomValue']);
                        }
                        //Value
                        else if (isset($spec['value'])) {
                            if (!isset($productsUpdate['ProductData']['specific_fields'][$spec_key][0])) {
                                $productsUpdate['ProductData']['specific_fields'][$spec_key][0] = Amazon_Tools::rstr($spec['value']);
                            }
                        } else {
                            //attribute
                            if (isset($spec['attribute'])) {
                                $id_spec = $spec['attribute'];
                                if (isset($id_spec) && isset($data['combination'][$id_product_attribute]['attributes'][$id_spec]['value'])) {
                                    foreach ($data['combination'][$id_product_attribute]['attributes'][$id_spec]['value'] as $sp_key => $sp_value) {

                                        if (isset($sp_value['name'][$iso_code]) && !empty($sp_value['name'][$iso_code])) {

                                            if (isset($this->MappingAttribute[$universe][$product_type][$id_spec][$sp_key][$spec_key])) {
                                                $Map = Amazon_Tools::rstr($this->MappingAttribute[$universe][$product_type][$id_spec][$sp_key][$spec_key]);
                                            }

                                            if ($spec_key == "Color" || $spec_key == "Size") {
                                                $productsUpdate['ProductData']['variation_data'][$spec_key] = Amazon_Tools::rstr($re_value['name'][$iso_code]);
                                                if (isset($Map) && !empty($Map)) {
                                                    $productsUpdate['ProductData']['variation_data'][$spec_key . 'Map'] = $Map;
                                                } else {
                                                    $productsUpdate['ProductData']['variation_data'][$spec_key . 'Map'] = Amazon_Tools::rstr($re_value['name'][$iso_code]);
                                                }
                                            } else {
                                                $productsUpdate['ProductData']['specific_fields'][$spec_key][$sp_key] = Amazon_Tools::rstr($sp_value['name'][$iso_code]);
                                                if (isset($Map) && !empty($Map)) {
                                                    $productsUpdate['ProductData']['specific_fields'][$spec_key][$sp_key] = $Map;
                                                }
                                            }
                                        }
                                    }
                                }
                                //feature
                            } else {
                                $id_spec = $spec['feature'];
                                if (isset($id_spec) && isset($data['feature'][$id_spec]['value'][$iso_code])) {
                                    $id_feature = $data['feature'][$id_spec]['id_value'];
                                    $productsUpdate['ProductData']['specific_fields'][$spec_key][$id_feature] = Amazon_Tools::rstr($data['feature'][$id_spec]['value'][$iso_code]);
                                    if (isset($this->MappingFeature[$universe][$product_type][$id_spec][$id_feature][$spec_key])) {
                                        $Map = Amazon_Tools::rstr($this->MappingFeature[$universe][$product_type][$id_spec][$id_feature][$spec_key]);

                                        if ($spec_key == "Color" || $spec_key == "Size") {
                                            $productsUpdate['ProductData']['specific_fields'][$spec_key . 'Map'][$id_feature] = $Map;
                                        } else {
                                            $productsUpdate['ProductData']['specific_fields'][$spec_key][$id_feature] = $Map;
                                        }
                                    } else {
                                        if ($spec_key == "Color" || $spec_key == "Size") {
                                            $productsUpdate['ProductData']['specific_fields'][$spec_key . 'Map'][$id_feature] = Amazon_Tools::rstr($data['feature'][$id_spec]['value'][$iso_code]);
                                        }
                                    }
                                }
                            }
                        }

                        // attribute_override
                        if (isset($override[$spec_key]) && !empty($override[$spec_key])) {
                            $productsUpdate['ProductData']['specific_fields'][$spec_key][0] = $override[$spec_key];
                        }

                        if (isset($override[$spec_key . 'Map']) && !empty($override[$spec_key . 'Map'])) {
                            $productsUpdate['ProductData']['specific_fields'][$spec_key . 'Map'][0] = $override[$spec_key . 'Map'];
                        }
                    }
                }

                //Amazon Attribute
                if (isset($profile['model']['amzn_attr']) && !empty($profile['model']['amzn_attr'])) {
                    $productsUpdate['ProductData']['amzn_attr'] = Amazon_Tools::rstr($profile['model']['amzn_attr']);
                }
            }

            // Code Examtion
            if (isset($data['p_code_exemption']) && $data['p_code_exemption'] == true/*isset($profile['code_exemption']) && !empty($profile['code_exemption'])*/) {

                $EAN_UPC_reference = $reference;

                if (isset($profile['code_exemption']['private']) && (int) $profile['code_exemption']['private']) {
                    $productsUpdate['RegisteredParameter'] = 'PrivateLabel';
                    $EAN_UPC_reference = Amazon_Tools::EAN_UPC_isPrivate($reference);
                } elseif ($profile['code_exemption']['feild'] == Amazon_Product::EXEMPTION_COMPATIBILITY) {
                    // 2013-03-23 - Added : EAN/UPC Exemption support for products outside Jewelry and Handycraft
                    $productsUpdate['RegisteredParameter'] = 'PrivateLabel';
                }

                switch ($profile['code_exemption']['feild']) {
                    case Amazon_Product::EXEMPTION_MODEL_NUMBER :
                        $productsUpdate['ProductData']['ModelNumber'] = $EAN_UPC_reference;
                        break;
                    case Amazon_Product::EXEMPTION_MODEL_NAME :
                        $productsUpdate['ProductData']['ModelName'] = $EAN_UPC_reference;
                        break;
                    case Amazon_Product::EXEMPTION_MFR_PART_NUMBER :
                        if (isset($profile['code_exemption']['concerns']) && $profile['code_exemption']['concerns'] == "Manufacturer") {
                            if ($data['id_manufacturer'] == $profile['code_exemption']['manufacturer'][0]) {
                                $productsUpdate['ProductDescription']['MfrPartNumber'] = $EAN_UPC_reference;
                            }
                        } else {
                            $productsUpdate['ProductDescription']['MfrPartNumber'] = $EAN_UPC_reference;
                        }
                        break;
                    case Amazon_Product::EXEMPTION_CATALOG_NUMBER :
                        $productsUpdate['ProductData']['MerchantCatalogNumber'] = $EAN_UPC_reference;
                        break;
                    case Amazon_Product::EXEMPTION_GENERIC :
                        break;
                }
            }

            //MfrPartNumber
            if (isset($data['p_sku_as_supplier_reference']) && isset($data['p_sku_as_sup_ref_unconditionnaly']))
                $productsUpdate['ProductDescription']['MfrPartNumber'] = $reference;
            elseif (isset($data['p_sku_as_supplier_reference']) && !empty($data['supplier_reference']))
                $productsUpdate['ProductDescription']['MfrPartNumber'] = $reference;
            elseif (isset($data['p_sku_as_supplier_reference']) && !empty($data['supplier_reference']))
                $productsUpdate['ProductDescription']['MfrPartNumber'] = $data['supplier_reference'];
            
            if(isset($override['ManufacturerPartNumber']) && !empty($override['ManufacturerPartNumber']))
                $productsUpdate['ProductDescription']['MfrPartNumber'] =  Amazon_Tools::rstr($override['ManufacturerPartNumber']);
             
            //Dimensions
            if (!empty($data['depth']) || !empty($data['width']) || !empty($data['height'])) {	
                $productsUpdate['ProductDescription']['PackageDimensions']['depth'] = Amazon_Tools::rstr($data['depth']);
                $productsUpdate['ProductDescription']['PackageDimensions']['width'] = Amazon_Tools::rstr($data['width']);
                $productsUpdate['ProductDescription']['PackageDimensions']['height'] = Amazon_Tools::rstr($data['height']);
		
                $productsUpdate['ProductDescription']['ItemDimensions']['depth'] = Amazon_Tools::rstr($data['depth']);
                $productsUpdate['ProductDescription']['ItemDimensions']['width'] = Amazon_Tools::rstr($data['width']);
                $productsUpdate['ProductDescription']['ItemDimensions']['height'] = Amazon_Tools::rstr($data['height']);
            }
	    
	    //weight
	    if (!empty($data['weight'])) {
		
		//package weight
                $productsUpdate['ProductDescription']['Package']['weight'] = Amazon_Tools::rstr($data['weight']);
		
		//package weight
                $productsUpdate['ProductDescription']['Shipping']['weight'] = Amazon_Tools::rstr($data['weight']);
            }
	    
	    if(isset($data['browsenode']) && !empty($data['browsenode'])){
		$productsUpdate['ProductDescription']['RecommendedBrowseNode'] = Amazon_Tools::rstr($data['browsenode']);
	    } else if (isset($profile['recommended_browse_node'])) {
                unset($profile['recommended_browse_node']['item_type']);
                $productsUpdate['ProductDescription']['RecommendedBrowseNode'] = Amazon_Tools::rstr($profile['recommended_browse_node']);
            }
            if (isset($data['item_type']) && !empty($data['item_type'])){
                $productsUpdate['ProductDescription']['ItemType'] = Amazon_Tools::rstr($data['item_type']);
            }
            if (isset($data['gift_wrap']) && $data['gift_wrap']){
                $productsUpdate['ProductDescription']['IsGiftWrapAvailable'] = true;
                $productsUpdate['ProductDescription']['IsGiftMessageAvailable'] = $data['gift_message'];
            } else {
                $productsUpdate['ProductDescription']['IsGiftWrapAvailable'] = false;
                $productsUpdate['ProductDescription']['IsGiftMessageAvailable'] = false;
            }
            if (isset($profile['category']['repricing']) && !empty($profile['category']['repricing'])) {
                $productsUpdate['repricing'] = $profile['category']['repricing'];
            }           
            
	    // Shipping Group Templates //2016-01-22
	    if (isset($data['shipping_group']) && !empty($data['shipping_group'])) {
                $productsUpdate['ProductDescription']['MerchantShippingGroupName'] = Amazon_Tools::rstr($data['shipping_group']);
            } 
            
            if(isset($override['PartNumber']) && !empty($override['PartNumber']))
                $productsUpdate['ProductData']['PartNumber'] =  Amazon_Tools::rstr($override['PartNumber']);

            // More override field :
            if(isset($override) && !empty($override)) {
                foreach ($override as $field_override => $value_override){
                    if(!isset($productsUpdate[$field_override]))
                        $productsUpdate['ProductDescription'][$field_override] = $value_override;
                }
            }
        }

        return $productsUpdate;
    }

    public function log_skipped($messages, $feed_type, $operationMode = null) {
        
        $history = array();
        $sql = '';
        foreach ($messages as $key => $message) {	    
	    
            $error_data = array(
                'batch_id' => $this->params->batchID,
                'id_country' => $this->params->id_country,
                'id_shop' => $this->params->id_shop,
                'action_process' => isset($operationMode) ? $operationMode : $this->operation_type,
                'action_type' => $feed_type,
                'message' => $message,
                'id_message' => $key,
                'date_add' => date("Y-m-d H:i:s"),
            );

            $result = array_search(strval($message).$key, $history);

            if ($result == false)
                $sql .= $this->database->save_validation_log($error_data, false);

            $history[] = strval($message).$key;
        }
        
        if(strlen($sql) > 10 && !$this->debug){
            $this->database->exec_query($sql);
        } else {
	    echo '<pre>' . print_r($sql, true) . '</pre>';
	}
    }

    public static function price_rule($price_rule, $rounding, $price) {

        foreach ($price_rule as $rule_key => $rule_value) {

            if ((float)$price <= (float)$rule_value['to'] && $price >= (float)$rule_value['from']) {
                
                $value = (float)$rule_value['value'];
                if ($rule_value['type'] == "percent") {
                    $price = $price + ( ($price * ($value) ) / 100 );
                } else if ($rule_value['type'] == "value") {
                    $price = $price + ($value);
                }
            }
        }

        if ($rounding == 0) {
            return $price;
        } else {
            return round($price, $rounding);
        }
    }

    private function attribute_override($params, $sku, $attribute_field, $id_product, $id_product_attribute, $id_model) {

        $override = array();

        // check attribute overide 
        $attr_override = $this->error_resolution->getAmazonOverride($params->id_shop, $params->id_country, $sku, $attribute_field, $id_product, $id_product_attribute, $id_model);
        if (!empty($attr_override)) {
            $override[$attribute_field] = $attr_override;
        }

        // check attribute map overide 
        $map_override = $this->error_resolution->getAmazonOverride($params->id_shop, $params->id_country, $sku, $attribute_field . 'Map', $id_product, $id_product_attribute, $id_model);
        if (!empty($map_override)) {
            $override[$attribute_field . 'Map'] = $map_override;
        }

        if (!empty($override)) {
            return $override;
        } else {
            return false;
        }
    }    
}