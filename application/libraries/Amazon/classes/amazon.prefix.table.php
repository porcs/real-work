<?php

require_once(dirname(__FILE__) . '/../../db.php');

class AmazonPrefixTable {
    
    public static function changePrefix($user, $old_prefix = 'products_', $new_prefix = ''){
	
	$db = new Db($user, 'products');
	
	$find = $old_prefix.'amazon_';
	$replace = $new_prefix."amazon_";
	$sql = "show tables like '{$find}%'";
	    $out = $db->db_query_string_fetch($sql); 
	    foreach($out as $o){ 
		foreach($o as $k=>$v){
		    $tb_name = $v;

		    if(strpos($tb_name,$find) ===false)continue;

		    $ntb = str_replace($find,$replace ,$tb_name);
		    $sql = "RENAME TABLE `{$tb_name}` TO `$ntb`;";
		    echo $sql.'<br>';
		    $db->db_exec($sql,false,false); 
		}
	    }
    }
}