<?php

if (!defined('BASEPATH'))
    define('BASEPATH', '');

require_once dirname(__FILE__) . '/amazon.config.php';
require_once(dirname(__FILE__) . '/amazon.tools.php');
require_once(dirname(__FILE__) . '/../../FeedBiz_Orders.php');
require_once dirname(__FILE__) . '/../../errorHandler.php';
require_once(dirname(__FILE__) . '/../../FeedBiz.php');

class Amazon_Order {

    const Amazon = 'Amazon';
    
    const PENDING = 1;
    const UNSHIPPED = 2;
    const PARTIALLYSHIPPED = 3;
    const SHIPPED = 4;
    const CANCELLED = 5;
    const CANCELED = 5;
    const CHECKED = 6;
    const ACTIVE = 1; // eBay // not pay
    const COMPLETED = 2; // eBay // paid
    
    const ORDER_PENDING_AVAILABILITY = 'PendingAvailability';
    const ORDER_PENDING = 'Pending';
    const ORDER_UNSHIPPED = 'Unshipped';
    const ORDER_PARTIALLYSHIPPED = 'PartiallyShipped';
    const ORDER_SHIPPED = 'Shipped';
    const ORDER_INVOICE_UNCONFIRMED = 'InvoiceUnconfirmed';
    const ORDER_CANCELED = 'Canceled';
    const ORDER_UNFULFILLABLE = 'Unfulfillable';
    
    const AMAZON_FBA_MULTICHANNEL = 'MAFN';
    const AMAZON_FBA_AMAZON = 'AFN';
    const AMAZON_FBA_MERCHANT = 'MFN';

    public $db;
    public $id_marketplace;
    public static $errors = array();
    public static $warnings = array();
    public static $warnings_cancelled = array();
    public static $messages = array();
    public static $MarketplaceID = _ID_MARKETPLACE_AMZ_;
    public static $Export_Orders = 'Export Orders';
    public static $or_pref = '', $p_pref = '', $o_pref = '', $m_pref = '';
    
    public static $orders = array();
    
    public function __construct($user, $only_order_db = false, $debug = false) {
        
        $this->debug = $debug;
        $this->db = new stdClass();
        $this->db->order = new Db($user, 'orders');
        $this->feedbiz_order = new FeedBiz_Orders(array($user));
	
	//set table prefix for order
	if(isset($this->db->order->prefix_table) && !empty($this->db->order->prefix_table))
	    self::$or_pref = $this->db->order->prefix_table;	

        if (!$only_order_db) {
	    
            $this->db->product = new Db($user, 'products');
            $this->db->offer = new Db($user, 'offers');

	    //set table prefix for product
	    if(isset($this->db->product->prefix_table) && !empty($this->db->product->prefix_table))
		self::$p_pref = $this->db->product->prefix_table;	 

	    //set table prefix for offer
	    if(isset($this->db->offer->prefix_table) && !empty($this->db->offer->prefix_table))
		self::$o_pref = $this->db->offer->prefix_table;
        }     
	
	if(defined('_TABLE_PREFIX_')) self::$m_pref = _TABLE_PREFIX_;
	
        $this->user = $user;
    }

    public function get_order_missing_item($where='')
    {
        $items = array();
        $sql_items =  "SELECT id_orders FROM ".self::$or_pref."order_items";
        $result_item = $this->db->order->db_query_str($sql_items);

        foreach ($result_item as $item){
            array_push($items, $item['id_orders']);
        }

        $sql = "SELECT o.id_orders, o.id_marketplace_order_ref, o.sales_channel, o.site, o.id_marketplace, o.order_status, o.payment_method, o.order_date, o.site, oi.seller_order_id "
            . "FROM ".self::$or_pref."orders o "
            . "LEFT JOIN ".self::$or_pref."order_invoice oi ON oi.id_orders = o.id_orders "
            . "WHERE o.id_orders NOT IN (".implode(",", $items).") $where ; ";
        return $this->db->order->db_query_str($sql);
    }
    
    public function get_configuration_features($id_country, $id_shop){
        
        $content = $where = array();
            
        $table = self::$m_pref.'amazon_log_display';
        
        if(!isset($this->db->product) || empty($this->db->product)){
            $this->db->product = new Db($this->user, 'products');
        }
        
        if(!$this->db_table_exists($this->db->product, $table)) return $content;
        
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;
        $where['menu'] = 'features';

        $this->db_from($this->db->product, $table);
        if (!empty($where)){
            $this->db->product->where($where);
        }
       
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $value) {
            $content[$value['field_name']] = $value['field_toggle'] ? 1:0;
        }
      
        return $content;
    }
    
    public function getUpdateOrderLists($id_shop, $id_country) {
        $orders = array();
        $sql = 'SELECT o.id_orders as "o.id_orders",o.id_marketplace_order_ref as "o.id_marketplace_order_ref",'
                . 'os.tracking_number as "os.tracking_number",os.id_carrier as "os.id_carrier" '
                . 'FROM '.self::$or_pref.'orders o'
                . ' LEFT JOIN '.self::$or_pref.'order_shipping os ON o.id_orders = os.id_orders '
                . ' WHERE o.id_shop = ' . ($id_shop) . ' '
                . ' AND o.id_marketplace = ' . (self::$MarketplaceID) . ' '
                . ' AND o.site = ' . $id_country . ' '
                . ' AND o.order_status = "Shipped"'
                . ' AND (o.shipping_status IS NULL'
                . ' OR o.shipping_status = "") ; ';
           
        $result = $this->db->order->db_query_str($sql);
        if (isset($result) && !empty($result))
            foreach ($result as $order) {

                if ($this->debug) {
                        echo '<pre>order : ' .print_r($order, true) . '</pre>';
                }
                if (!isset($order['o.id_orders']) || empty($order['o.id_orders'])) {
                    if ($this->debug) {
                        echo '<pre>Missing id_orders : ' .print_r($order['os.tracking_number'], true) . '</pre>';
                    }
                    continue;
                }
                
                $Carrier = $this->getMappingIdCarrierByName(null, $id_shop, $order['os.id_carrier'], 'outgoing', $id_country);

                if (isset($Carrier) && !empty($Carrier) && $Carrier) {
                    if (isset($Carrier['carrier_code']) && !empty($Carrier['carrier_code'])) {
                        $orders[$order['o.id_orders']]['CarrierCode'] = $Carrier['carrier_code'];
                    } else {
                        $orders[$order['o.id_orders']]['CarrierName'] = $Carrier['carrier_name'];
                    }
                } else {
                    if ($this->debug) {
                        echo '<pre><b>Missing carrier info</b> : ' .print_r($order['os.tracking_number'], true) . '</pre>';
                    }
                    continue;
                }

                $orders[$order['o.id_orders']]['OrderID'] = $order['o.id_orders'];
                $orders[$order['o.id_orders']]['AmazonOrderID'] = $order['o.id_marketplace_order_ref'];
                $orders[$order['o.id_orders']]['ShipperTrackingNumber'] = $order['os.tracking_number'];
            }
        return($orders);
    }
   
    public function getRemoteCartLists($id_shop, $id_country, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false, $status = null) {
        
        $orders = array();
        $sql = 'SELECT * FROM '.self::$m_pref.'amazon_remote_cart WHERE id_shop = ' . (int) $id_shop . ' AND id_country = ' . (int) $id_country . ' ' ;
	        
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
                    . "mp_order_id like '%" . $search . "%' OR "                
                    . "reference like '%" . $search . "%' OR "                 
                    . "quantity like '%" . $search . "%' OR "                 
                    . "timestamp like '%" . $search . "%' ";                  
            $sql .=  ") ";
        }
                
        if(isset($order_by) && !empty($order_by) && !$num_rows){
            $sql .= "ORDER BY $order_by ";
        }
       
        if(isset($start) && isset($limit) && !$num_rows){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
	 
        if($num_rows) {
	    
            $result = $this->db->order->db_sqlit_query($sql);
            $row = $this->db->order->db_num_rows($result);
            return $row;
	    
        } else {
	    
	    $result = $this->db->order->db_query_str($sql . "; ");        
            foreach ($result as $key => $order) {
                $id = $key;
                $orders[$id]['mp_order_id'] = $order['mp_order_id'];
                $orders[$id]['status'] = 'Pending';
                $orders[$id]['reference'] = $order['reference'];
                $orders[$id]['quantity'] = $order['quantity'];
                $orders[$id]['timestamp'] = date( 'Y-m-d H:i:s' , strtotime($order['timestamp']));
                $orders[$id]['date_upd'] = date( 'Y-m-d H:i:s' , strtotime($order['date_upd']));
	    }
	}
       
        return($orders);
    }
    
    public function getOrderLists($id_shop, $id_country, $start = null, $limit = null, $order_by = null, $search_list = null, $num_rows = false, $status = null) {

        $orders = $list_order_id = array();
        $sql_flag_mail_invoice = $sql_flag_mail_review = "";

        // check field flag message
        if($this->db->order->fieldExists( self::$or_pref."orders", 'flag_mail_invoice' )){
           $sql_flag_mail_invoice = ', o.flag_mail_invoice as "o.flag_mail_invoice" ';
        }
        if($this->db->order->fieldExists( self::$or_pref."orders", 'flag_mail_review' )){
           $sql_flag_mail_review = ', o.flag_mail_review as "o.flag_mail_review" ';
        }      

        if(!isset($search_list) || empty($search_list)){

            $sql = "SELECT id_orders FROM ".self::$or_pref."orders o WHERE o.id_shop = $id_shop AND o.id_marketplace = " .(self::$MarketplaceID). " AND o.site = $id_country ";
            if(isset($order_by) && !empty($order_by) && !$num_rows){
                $sql .= " ORDER BY $order_by ";
            }
            if(isset($start) && isset($limit) && !$num_rows){
                $sql .= " LIMIT $limit OFFSET $start ";
            }
            $result = $this->db->order->db_query_str($sql);

            foreach ($result as $order){
                array_push($list_order_id, $order['id_orders']);
            }
            $list_order_id = implode(", ", $list_order_id);
        }
        
        $sql = 'SELECT o.id_orders as "o.id_orders", o.id_marketplace_order_ref as "o.id_marketplace_order_ref",ob.name as "ob.name",ob.address_name as "ob.address_name", '
                . 'o.payment_method as "o.payment_method", o.total_paid as "o.total_paid", o.order_status as "o.order_status", o.shipping_date as "o.shipping_date", '
                . 'o.status as "o.status"'.$sql_flag_mail_invoice.$sql_flag_mail_review.', '
                . 'o.order_date as "o.order_date", '
                . '(CASE '
                . "WHEN o.comment LIKE '%waiting order items%' THEN NULL "
                . "WHEN o.comment LIKE '%Products do not match%' THEN NULL "
                . "WHEN o.comment LIKE '%Product items mismatch%' THEN NULL "
                . "ELSE o.comment "
                . 'END) AS "o.comment" ' ;
        
	if(!$num_rows){
            $sql .=',oi.invoice_no as "oi.invoice_no",os.tracking_number as "os.tracking_number",oi.seller_order_id as "oi.seller_order_id" ';
	    $sql .=',mp.mp_channel as "mp.mp_channel" ';
        } else {
            if(isset($search_list) && !empty($search_list)){
                $sql .=',oi.invoice_no as "oi.invoice_no",os.tracking_number as "os.tracking_number",oi.seller_order_id as "oi.seller_order_id" ';
                $sql .=',mp.mp_channel as "mp.mp_channel" ';
            }
        }

       $sql .=  'FROM '.self::$or_pref.'orders o'
                . ' JOIN '.self::$or_pref.'order_buyer ob ON o.id_orders = ob.id_orders ' ;

	if(!$num_rows) {
            $sql .= ' JOIN '.self::$or_pref.'order_shipping os ON os.id_orders = o.id_orders JOIN '.self::$or_pref.'order_invoice oi ON oi.id_orders = o.id_orders ' ;
            $sql .= ' LEFT JOIN '.self::$m_pref.'mp_multichannel mp ON mp.id_orders = o.id_orders AND mp.id_shop = o.id_shop ' ;
	}else {
            if(isset($search_list) && !empty($search_list)){
                $sql .= ' JOIN '.self::$or_pref.'order_shipping os ON os.id_orders = o.id_orders JOIN '.self::$or_pref.'order_invoice oi ON oi.id_orders = o.id_orders ' ;
                $sql .= ' LEFT JOIN '.self::$m_pref.'mp_multichannel mp ON mp.id_orders = o.id_orders AND mp.id_shop = o.id_shop ' ;
            }
        }

	$sql .=  ' WHERE o.id_shop = ' . (int) $id_shop
                . ' AND o.id_marketplace = ' . (self::$MarketplaceID) . ' '
                . ' AND o.site = ' . (int) $id_country . '  AND ob.site = ' . (int) $id_country . ' ';

        if(!empty($list_order_id)){
             $sql .= " AND o.id_orders IN ($list_order_id) ";
        }
        
        if(isset($search_list) && is_array($search_list) && !empty($search_list)){
            
            $sql .= "AND ( " ;

            foreach ($search_list as $key => $search){
                if(isset($search['value']) && !empty($search['value']) && $key > 0){
                    $sql .= " AND ";
                }
                $sql .= " ".$search['column']." like '%" . $search['value'] . "%' ";
            }
            
            $sql .=  ") ";
        }

        if(isset($status) && !empty($status)){
            $sql .= " AND " .$status;
        }

        if(isset($order_by) && !empty($order_by) && !$num_rows){
           $sql .= "ORDER BY $order_by ";
        }

        if((isset($search_list) && !empty($search_list)) && isset($start) && isset($limit) && !$num_rows){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
	 
        if($num_rows) {
            $result = $this->db->order->db_sqlit_query($sql);
            $row = $this->db->order->db_num_rows($result);
            return $row;
	    
        } else {
            //var_dump($sql); exit;
	    $result = $this->db->order->db_query_str($sql . "; ");        
            foreach ($result as $order) {
                $id = $order['o.id_orders'];
                $orders[$id]['id_orders'] = $order['o.id_orders'];
                $orders[$id]['id_marketplace_order_ref'] = $order['o.id_marketplace_order_ref'];
                $orders[$id]['buyer_name'] = $order['ob.name'];
                $orders[$id]['address_name'] = $order['ob.address_name'];
                $orders[$id]['payment_method'] = $order['o.payment_method'];
                $orders[$id]['total_paid'] = number_format($order['o.total_paid'], 2);
                $orders[$id]['order_status'] = $order['o.order_status'];
                $orders[$id]['order_date'] = date('Y-m-d H:i:s', strtotime($order['o.order_date']));
                $orders[$id]['invoice_no'] = $order['oi.invoice_no'];
                $orders[$id]['shipping_date'] = !empty($order['os.tracking_number']) ? date('Y-m-d', strtotime($order['o.shipping_date'])) : null;
                $orders[$id]['tracking_number'] = $order['os.tracking_number'];
                $orders[$id]['comment'] = $order['o.comment'];
                $orders[$id]['status'] = $order['o.status'];
                $orders[$id]['seller_order_id'] = $order['oi.seller_order_id'];
                $orders[$id]['mp_channel'] = $order['mp.mp_channel'];

                if(strlen($sql_flag_mail_invoice) > 0)
                    $orders[$id]['flag_mail_invoice'] = $order['o.flag_mail_invoice'];
                
                if(strlen($sql_flag_mail_review) > 0)
                    $orders[$id]['flag_mail_review'] = $order['o.flag_mail_review'];
            }  
	}
       
        return($orders);
    }
    
    public function getOrderPendingLists($id_shop, $start = null, $limit = null, $search = null, $num_rows = false) {
        
        $orders = array();
        $sql = "SELECT * FROM ".self::$m_pref."amazon_orders amo "
	    . "LEFT JOIN ".self::$m_pref."amazon_orders_items amoi ON amoi.mp_order_id = amo.mp_order_id AND amoi.id_shop = amo.id_shop AND amoi.id_country = amo.id_country "
	    . "WHERE amo.id_shop = " . (int) $id_shop . " "
	    . "AND amoi.id_shop = " . (int) $id_shop . " ";
	    
	$sql .= "AND amo.mp_status = '" . Amazon_Order::PENDING . "' ";
	
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
		. "amo.mp_order_id like '%" . $search . "%' OR "
		. "amoi.mp_order_id like '%" . $search . "%' OR "
		. "amoi.sku like '%" . $search . "%' OR "
		. "amoi.id_product like '%" . $search . "%' OR "
		. "amoi.id_product_attribute like '%" . $search . "%' OR "
		. "amoi.quantity like '%" . $search . "%' OR "
		. "amo.price like '%" . $search . "%' ";
            $sql .=  ") ";
        }       
        
	$sql .= "ORDER BY amoi.id_product ASC, amoi.id_product_attribute ASC, amo.date_upd ";
       
        if(isset($start) && isset($limit) && !$num_rows){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
	 
        if($num_rows) {
	    
            $result = $this->db->order->db_sqlit_query($sql);
            $row = $this->db->order->db_num_rows($result);
            return $row;
	    
        } else {
	    
	    $result = $this->db->order->db_query_str($sql . "; "); 
	    foreach ($result as $key => $order) {
		$id = $key;
		$orders[$id]['marketplace'] = Amazon_Order::Amazon;
		$orders[$id]['reference'] = $order['sku'];
		$orders[$id]['id_product'] = $order['id_product'];
		$orders[$id]['id_product_attribute'] = $order['id_product_attribute'];
		$orders[$id]['site'] = $order['id_country'];
		$orders[$id]['id_marketplace_order_ref'] = $order['mp_order_id'];
		$orders[$id]['quantity'] = $order['quantity'];
		$orders[$id]['order_date'] = date( 'Y-m-d H:i:s' , strtotime($order['order_date']));
		$orders[$id]['date_upd'] = date( 'Y-m-d H:i:s' , strtotime($order['date_upd']));
	    }
	    
	    return($orders);
	}
    }
    
    public function getOrderPendingByIdRef($id_marketplace_order_ref) {
        $sql = "SELECT * FROM ".self::$m_pref."amazon_orders WHERE mp_order_id = '$id_marketplace_order_ref' ; ";	
	$result = $this->db->order->db_query_str($sql);     
	
	if(isset($result[0]))
	    return($result[0]);
	else
	    return; 
    }
    
    public function getOrderPendingItemByIdRef($id_marketplace_order_ref) {
        $sql = "SELECT * FROM ".self::$m_pref."amazon_orders_items WHERE mp_order_id = '$id_marketplace_order_ref' ; ";
	$result = $this->db->order->db_query_str($sql);

	if(isset($result))
	    return($result);
	else
	    return;
    }

    public function getMappingIdCarrierByName($name = null, $id_shop = null, $id_carrier = null, $type = null, $id_country = null) {
        
        $sql = 'SELECT id_carrier, id_carrier_ref, mapping, other
                FROM '.self::$m_pref.'amazon_mapping_carrier 
                WHERE id_shop = ' . $id_shop;
	
        if (isset($id_country) && !empty($id_country))
		$sql .= ' AND id_country = ' . (int) $id_country . ' ';
	
        if (isset($name) && !empty($name))
		$sql .= ' AND mapping = "' . str_replace(array(", ", " ", "/"), '_', $name) . '" ';
	
        if (isset($id_carrier) && !empty($id_carrier))
		$sql .= ' AND id_carrier = ' . $id_carrier . ' ';
	
        if (isset($type) && !empty($type))
		$sql .= ' AND type = "' . $type . '" ';
	
        $sql .= ';';

        if($this->debug) {
            var_dump('function getMappingIdCarrierByName1', $sql);
        }
        
        $result = $this->db->product->db_query_str($sql);
	
        if (isset($result) && !empty($result)) { 
	    
            foreach ($result as $carrier) {
		
		/*if (isset($type) && $type == 'fba') {
		    
		    return $carrier['mapping'];
		    
                } else*/ if (isset($type) && $type == 'outgoing') {
		    
                    if ($carrier['mapping'] == 'Other') {
                        return array('carrier_name' => $carrier['other']);
			
                    } else {
                        return array('carrier_code' => $carrier['mapping']);
                    }
		    
                } else {                    
                    $sql_select = "SELECT MAX(c.id_carrier) AS id_carrier
                                    FROM ".self::$p_pref."carrier c
                                    LEFT JOIN ".self::$m_pref."amazon_mapping_carrier amc ON ( amc.id_carrier_ref = c.id_carrier_ref ) AND amc.id_shop = c.id_shop
                                    WHERE c.id_shop = $id_shop AND amc.id_shop = $id_shop AND (amc.id_carrier_ref = ".$carrier['id_carrier_ref'].")
                                    AND amc.id_country = $id_country ; " ;

                    if($this->debug) {
                        var_dump('function getMappingIdCarrierByName2', $sql_select);
                    }

                    $max_id_carrier = $this->db->product->db_query_str($sql_select);
                    
                    if(isset($max_id_carrier[0]['id_carrier'])) {
                        // Update Mapping Carrier
                        $sql_update = "UPDATE ".self::$m_pref."amazon_mapping_carrier SET id_carrier = " .$max_id_carrier[0]['id_carrier']. " "
				. "WHERE mapping = '" .$carrier['mapping']. "' "
                                . "AND id_carrier_ref = ".$carrier['id_carrier_ref']." AND id_shop = $id_shop AND id_country = $id_country ; ";
                        $this->db->product->db_exec($sql_update);
                        
                        if (isset($type) && $type == 'fba') {
                            return $carrier['mapping'];
                        } else {
                            return $max_id_carrier[0]['id_carrier'];
                        }
                    }
                }
            }
	    
        } else {
	    
            if (isset($type) && $type == 'outgoing' && isset($id_carrier) && !empty($id_carrier)) {
                $sql = 'SELECT name FROM '.self::$p_pref.'carrier WHERE id_shop = ' . $id_shop . ' AND id_carrier = ' . $id_carrier . '; ';
                $result = $this->db->product->db_query_str($sql);
                if (isset($result) && !empty($result)) {
                    foreach ($result as $carrier) {
                        return array('carrier_name' => $carrier['name']);
                    }
                }
            }
	    
        }
        
        return(false);
    }
    
    public function checkProductBySKU($SKU, $id_shop){
        $count = 0;

        $sql = 'SELECT pa.id_product FROM '.self::$p_pref.'product_attribute pa '
            . 'LEFT JOIN '.self::$p_pref.'product p ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop '
            . 'WHERE pa.reference = "'.trim($SKU).'" AND pa.id_shop = '.(int)$id_shop.' AND (p.date_upd IS NULL OR p.date_upd = "") ; ';
	
	$result1 = $this->db->order->db_sqlit_query($sql);
	$row1 = $this->db->order->db_num_rows($result1);
	$count += (int) $row1;
	
        $sql = 'SELECT id_product FROM '.self::$p_pref.'product '
            . 'WHERE reference = "'.(trim($SKU)).'" AND id_shop = '.(int)$id_shop.' AND (date_upd IS NULL OR date_upd = "") ; ';
	
        $result2 = $this->db->order->db_sqlit_query($sql);
	$row2 = $this->db->order->db_num_rows($result2);

	$count += (int) $row2;
	
        return ($count);
    }
    
    public function getProductBySKU($SKU, $id_shop, $params) {
	
        $sql = "SELECT p.id_product AS id_product, pa.id_product_attribute AS id_product_attribute
                FROM ".self::$p_pref."product p 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                WHERE p.id_shop = " . $id_shop . " 
                AND ( 
                    pa.reference = CASE WHEN pa.reference = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.reference = CASE WHEN p.reference = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.sku = CASE WHEN pa.sku = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.ean13 = CASE WHEN pa.ean13 = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.upc = CASE WHEN pa.upc = '" . $SKU . "' THEN '" . $SKU . "' END
                    OR p.sku = CASE WHEN p.sku = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.ean13 = CASE WHEN p.ean13 = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.upc = CASE WHEN p.upc = '" . $SKU . "' THEN '" . $SKU . "' END 
                ) AND p.active = 1 ; ";

        $result = $this->db->product->db_query_str($sql);
        if (isset($result) && !empty($result)) {
            foreach ($result as $product) {
                $details = new Product($params->user_name, $product['id_product'], $params->id_lang, $params->id_mode, $params->id_shop);

                if (isset($product['id_product_attribute']))
                    $details->id_product_attribute = $product['id_product_attribute'];

                return $details;
            }
        }
        return(false);
	
    }
    
    public function getProductsBySKUs($SKUs, $id_shop) {
	$details = array();
        $sql = "SELECT 
		    CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS sku,
		    CASE WHEN pa.quantity IS NOT NULL THEN pa.quantity ELSE p.quantity END AS quantity, 
		    p.id_product AS id_product, 
		    pa.id_product_attribute AS id_product_attribute
                FROM ".self::$p_pref."product p 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                WHERE p.id_shop = " . $id_shop . " 
                AND ( pa.reference IN ('".  implode("', '", $SKUs)."') OR p.reference IN ('".  implode("', '", $SKUs)."') ); ";

	$result = $this->db->product->db_query_str($sql);
	if (isset($result) && !empty($result)) {
            foreach ($result as $product) {
		$details[$product['sku']] = $product;
	    }
	}
	
        return $details;
    }

    public function checkByMpId($id_marketplace_order_ref, $id_shop, $site, $include_status = false) {

        $sql_status = '';
        if($include_status)
            $sql_status = ', o.`status` as `status` ';
        
        $sql = "SELECT o.id_orders as id_orders $sql_status
                FROM orders_orders o
                WHERE o.id_marketplace_order_ref = '" . $id_marketplace_order_ref . "'
                AND o.id_marketplace = " . self::$MarketplaceID . "
                AND o.id_shop = " . (int) $id_shop . "
                AND o.site = " . (int) $site . " ;";

        if($this->debug) {
            var_dump('function checkByMpId', $sql);
        }

        $result = $this->db->order->db_query_str($sql);

        if(isset($result[0])) {
            if($include_status) {
                return $result[0];
            } else {
                return $result[0]['id_orders'];
            }
        } 
        
        return(false);
        
    }

    public function getCurrencyIdByIsoCode($iso_code, $id_shop) {
        if (!isset($iso_code) || empty($iso_code))
            return NULL;
        if (!isset($id_shop) || empty($id_shop))
            return NULL;

        $id_currency = array();
        $_db = $this->db->offer;
        $_db->select(array('id_currency'));
        $_db->from('currency');
        $_db->where(array('iso_code' => strtoupper($iso_code), 'id_shop' => (int) $id_shop));
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $cur)
            $id_currency = $cur['id_currency'];

        return $id_currency;
    }
    
    public function getImportedOrderItem($id_marketplace_order_item_ref) {
        
    	if (!isset($id_marketplace_order_item_ref) || empty($id_marketplace_order_item_ref))
    		return NULL;
    
    	$_db = $this->db->order;
    	$_db->select(array('id_order_items'));
    	$_db->from('order_items');
    	$_db->where(array('id_marketplace_order_item_ref' => $id_marketplace_order_item_ref));
    	$result = $_db->db_array_query($_db->query);
    
    	$id_order_items = null;
    	foreach ($result as $cur)
    		$id_order_items = $cur['id_order_items'];
    
    	return $id_order_items;
    }
    
    public function checkImportedOrderItemAttribute($id_order_items, $attr_key, $id_attribute) {
        
    	if (!isset($id_order_items) || empty($id_order_items) )
    		return NULL;
    	
    	$return = false;
    	$_db = $this->db->order;
    	$_db->select(array('id_order_items'));
    	$_db->from('order_items_attribute');
    	$_db->where(array('id_order_items' => $id_order_items, 'id_attribute_group' => $attr_key, 'id_attribute' => $id_attribute));
    	$result = $_db->db_array_query($_db->query);    	
    	
    	foreach ($result as $cur){    		
    		$return = intval($cur['id_order_items']) > 0;
    	}
    
    	return $return;
    }
    
    public function checkImportedOrderDependency($table, $id_order) {
        
    	if (!isset($table) || empty($table) || !isset($id_order) || empty($id_order))
    		return NULL;
    
    	$return = false;
    	$_db = $this->db->order;
    	$_db->select(array('id_orders'));
    	$_db->from($table);
    	$_db->where(array('id_orders' => $id_order));
    	$result = $_db->db_array_query($_db->query);
    
    	foreach ($result as $cur){
    		$return = $cur['id_orders'] > 0;
    	}
    
    	return $return;
    }

    public function setOrder($data, $option = null) {
        
        $sorder = $uorder = $order_item = $order_item_attr = $invoice = $payment = $order_list = array();
        $total_discount = $total_shipping = $total_amount = $gift_amount = $total_quantity = $total_tax_rate = $total_shipping_tax = $total_giftwrap_tax = $total_paid = 0;
        $message = $sql = $gift_message = null;
        $today_date = date("Y-m-d H:i:s");

        //Params
        $id_shop = $option->id_shop;
        $site = $option->id_country;
        $id_lang = $option->id_lang;
        $id_region = strtoupper($option->id_region);

        // Charset conversion 
        foreach ($data as $order_key => $item) {
            if (!is_object($data->$order_key) && !is_array($data->$order_key) && strval($data->$order_key))
                if (mb_detect_encoding(strval($data->$order_key)) != 'UTF-8')
                    $data->$order_key = iconv("ISO-8859-15", "UTF-8//TRANSLIT", $item);
        }

        // Amazon Order Id
        $order_id = (string) $data->AmazonOrderId;       
	
	$pending_order = false;
	if ((string) $data->OrderStatus == Amazon_Order::ORDER_PENDING){
	    $pending_order = true;	    
	}

        $isPrime = isset($data->IsPrime) ? (bool) $data->IsPrime : false ;
        
	if(!$pending_order) {

	    if (isset($data->OrderStatus) && in_array((string)$data->OrderStatus, array('Canceled', 'Cancelled'))) {

		Amazon_Order::$warnings_cancelled[] = sprintf(Amazon_Tools::l('Order #%s, Skipping Cancelled Order'), $order_id);

                ## if pending order are cancelled - re stock
                #
                // 1. check is pending order ?
                $OrderPending = $this->getOrderPendingByIdRef($order_id);

                if( isset($OrderPending['mp_status']) && ($OrderPending['mp_status'] == constant('Amazon_Order::PENDING')) ){

                    // 2.update status to Cancelled
                    $pending_order_value = array(
                        'mp_status' => constant('Amazon_Order::'.strtoupper($data->OrderStatus)),
                        'date_upd' => date("Y-m-d H:i:s"),
                    );

                    $pending_order_where = array(
                        'id_shop' => array('operation' => '=', 'value' => (int) $id_shop),
                        'id_country' =>  array('operation' => '=', 'value' => (int) $site),
                        'mp_order_id' => array('operation' => '=', 'value' =>  $order_id ),
                    );

                    $pending_order_sql = $this->update_string(self::$m_pref.'amazon_orders', $pending_order_value, $pending_order_where, $this->db->order);
                    
                    if($this->exec_query($this->db->order, $pending_order_sql, false, false)) {

                        // 3. get item to restock
                        $PendingItems = $this->getOrderPendingItemByIdRef($order_id);

                        // 4. set to Amazon_Order::$orders
                        if(isset($PendingItems) && !empty($PendingItems)){

                            if($this->debug){
                                echo '<pre><b>Pending Item :</b> '.print_r($PendingItems, true).'</pre>';
                            }

                            foreach ($PendingItems as $pending_items){
                                Amazon_Order::$orders[$order_id]['items'][$pending_items['sku']] =  $pending_items;
                                Amazon_Order::$orders[$order_id]['items'][$pending_items['sku']]['reference'] = $pending_items['sku'];
                                Amazon_Order::$orders[$order_id]['items'][$pending_items['sku']]['quantity_in_stock'] = $pending_items['quantity'];
                                Amazon_Order::$orders[$order_id]['items'][$pending_items['sku']]['time'] = date('Y-m-d H:i:s');
                                Amazon_Order::$orders[$order_id]['FulfillmentChannel'] = $data->FulfillmentChannel ;
                                Amazon_Order::$orders[$order_id]['LastUpdateDate'] =  isset($data->LastUpdateDate) ? $data->LastUpdateDate : date('Y-m-d H:i:s');
                                Amazon_Order::$orders[$order_id]['OrderStatus'] =  isset($data->OrderStatus) ? $data->OrderStatus : null;
                            }
                        }
                    }
                }

		return(false);
	    }

	    // Check Address
	    if (!isset($data->Address) || !is_object($data->Address) || empty($data->Address) ) {
		return(false);
	    }
	    if (!isset($data->BuyerName) || empty($data->BuyerName)) {
                $data->BuyerName = 'Unknow';
		//return(false);
	    }
	    if (!isset($data->BuyerEmail) || empty($data->BuyerEmail)) {
		return(false);
	    }

	    // id currency
	    $id_currency = (int) $this->getCurrencyIdByIsoCode($data->OrderTotalCurrency, $id_shop);
	    $total_paid = isset($data->OrderTotalAmount) && ($data->OrderTotalAmount > 0) ? (float) $data->OrderTotalAmount : 0;
	    $sales_channel = isset($data->SalesChannel) && !empty($data->SalesChannel) ? $data->SalesChannel : Amazon_Order::Amazon . $option->ext;

	    //Save Order//        
	    $sorder['id_marketplace_order_ref'] = $order_id;
	    $sorder['sales_channel'] = $sales_channel;
	    $sorder['id_shop'] = (int) $id_shop;
	    $sorder['site'] = $site;
	    $sorder['id_marketplace'] = self::$MarketplaceID;
	    $sorder['id_lang'] = (int) $id_lang;
	    $sorder['id_region'] = $id_region;
	    $sorder['id_currency'] = isset($data->OrderTotalCurrency) ? $data->OrderTotalCurrency : null;
	    $sorder['order_type'] = isset($data->OrderType) ? $data->OrderType : null;
	    $sorder['order_status'] = isset($data->OrderStatus) ? $data->OrderStatus : null;
	    $sorder['payment_method'] = isset($data->PurchaseMethod) ? $data->PurchaseMethod : null;
	    $sorder['purchase_date'] = isset($data->PurchaseDate) ? $data->PurchaseDate : null;
	    $sorder['order_date'] = isset($data->PurchaseDate) ? $data->PurchaseDate : null;
	    $sorder['shipping_date'] = isset($data->EarliestShipDate) ? $data->EarliestShipDate : null;
	    $sorder['latest_shipping_date'] = isset($data->LatestShipDate) ? $data->LatestShipDate : null;
	    $sorder['delivery_date'] = isset($data->EarliestDeliveryDate) ? $data->EarliestDeliveryDate : null;
	    $sorder['latest_delivery_date'] = isset($data->LatestDeliveryDate) ? $data->LatestDeliveryDate : null;
	    $sorder['total_paid'] = $total_paid;
	    $sorder['date_add'] = $today_date;
	    $sorder['ip'] = $order_id;

	    // marketplace_order_number 5/6/2015
	    $sorder['marketplace_order_number'] = $order_id;

	    // Check Order Shipped 
	    if (isset($data->OrderStatus) && $data->OrderStatus === "Shipped") {
		$sorder['shipping_status'] = true;
	    }

	    if (!$id_order = $this->saveOrder('orders', $sorder)) {
		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import incomplete order'), $order_id);
		return(false);
	    }
	} 

	//Item 
	if (isset($data->Items) && !empty($data->Items)) {

	    $checkpass = true;

	    // Precheck
	    foreach ($data->Items as $key => $item) {
		
		$quantity = (int)$item->QuantityOrdered;

		if ($quantity <= 0) {
		    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Skipping zero quantity item for order #%s product SKU: %s'), $order_id, trim((string)$item->SKU));
		    unset($data->Items[$key]);
		} else {
		    
		    $SKU = trim((string)$item->SKU);
		    $productCheck = $this->checkProductBySKU($SKU, $id_shop);
		    
		    if ($productCheck == 0 || !$productCheck) {
			Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) SKU/Reference not found in your database. Please check existance of this product: "%s"'), $order_id, $SKU);
			$checkpass = false;
		    } elseif ($productCheck > 1) {
			Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Unable to import duplicate product "%s" - Please remove the duplicate product in your database.'), $order_id, $SKU);
			$checkpass = false;
		    }
		}
	    }
	    
	    if (!$checkpass) {
		return(false);
	    }
	    
	    foreach ($data->Items as $id_item => $item) {
		
		// Check SKU
		if (!isset($item->SKU) || empty($item->SKU)) {
		    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Item does not have SKU - #%s'), $order_id, $item->SKU);
		    continue;
		}
		
		// Product
		if (!$product = $this->getProductBySKU($item->SKU, $id_shop, $option)) {
		    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s'), $order_id, $item->SKU);
		    continue;
		}
		
		$order_item['reference'] = $item->SKU;
		$order_item['id_shop'] = (int) $id_shop;
		$order_item['id_product'] = $product->id_product;
		$order_item['id_product_attribute'] = isset($product->id_product_attribute) ? $product->id_product_attribute : null;
		$order_item['product_name'] = $item->Title;
		$order_item['quantity'] = (int) $item->QuantityOrdered;
		$order_item['id_marketplace_order_item_ref'] = $id_item;
		$order_item['id_marketplace_product'] = $item->ASIN;
		$total_quantity = $total_quantity + $order_item['quantity'];

		// Product quantity
		$quantity_in_stock = $product->quantity;
		if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]))
		    $quantity_in_stock = $product->combination[$product->id_product_attribute]['quantity'];
		$order_item['quantity_in_stock'] = $quantity_in_stock;

		// Product price
		$product_price = $product->price;
		if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['price']))
		    if (isset($product->combination[$product->id_product_attribute]['price']))
			$product_price = $product->combination[$product->id_product_attribute]['price'];
		$order_item['product_price'] = $product_price;
		
		if(!$pending_order) {
		    
		    $order_item['id_orders'] = $id_order;

		    // Product weight
		    $product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['weight']['value']))
			$product_weight = $product->combination[$product->id_product_attribute]['weight']['value'];
		    $order_item['product_weight'] = $product_weight;

		    // Shipping 
		    $order_item['shipping_price'] = (float) $item->ShippingPriceAmount;
		    $total_shipping = ( $total_shipping + $order_item['shipping_price'] );

		    // Tax
		    $order_item['tax_rate'] = (float) $item->TaxesInformation->ItemTaxAmount;
		    $total_tax_rate = $total_tax_rate + $order_item['tax_rate'];
		    $total_shipping_tax = $total_shipping_tax + (float) $item->TaxesInformation->ShippingTaxAmount;
		    $total_giftwrap_tax = $total_giftwrap_tax + (float) $item->TaxesInformation->GiftWrapTaxAmount; //20161027
		    
		    $item_quantity = 1;
		    if($order_item['quantity'] > 0)
			$item_quantity = (int) $order_item['quantity'];
			    
		    // Before VAT
		    $order_item['unit_price_tax_excl'] = ( (float) $item->ItemPriceAmount / $item_quantity );
		    $order_item['total_price_tax_excl'] = (float) $item->ItemPriceAmount;
		    $order_item['total_shipping_price_tax_excl'] = (float) $item->ShippingPriceAmount - $order_item['tax_rate'];

		    // After VAT
		    $order_item['unit_price_tax_incl'] = ( $order_item['unit_price_tax_excl'] + $order_item['tax_rate'] );
		    $order_item['total_price_tax_incl'] = ( $order_item['total_price_tax_excl'] + $order_item['tax_rate'] );
		    $order_item['total_shipping_price_tax_incl'] = (float) $item->ShippingPriceAmount;

		    // Condition
		    $order_item['id_condition'] = $product->id_condition;

		    // Promotion
		    $order_item['promotion'] = $item->PromotionId;

		    //Gift Message
		    $order_item['message'] = $item->GiftMessageText;
		    //$message = $item->GiftMessageText;
		    $gift_message = $item->GiftMessageText;
		    $gift_amount = $gift_amount + (float) $item->GiftWrapPrice;
                    
                    // added gift to carrier field
                    if($data->FulfillmentChannel == AmazonImportOrders::AFN){
                        $total_shipping  = ( $total_shipping + $gift_amount ) ;
                        $gift_amount = 0 ;
                    }

		    // Discount
		    if (isset($item->ShippingDiscount) && $item->ShippingDiscount > 0 && $data->FulfillmentChannel != AmazonImportOrders::AFN) {
			$total_discount = ( $total_discount + (float) $item->ShippingDiscount );
                        $total_shipping = ( $total_shipping - (float) $item->ShippingDiscount );
                    }

		    if (isset($item->PromotionDiscount) && $item->PromotionDiscount > 0) {
			$total_discount = ( $total_discount + (float) $item->PromotionDiscount );
                    }

		    // Total Amount
		    $total_amount = $total_amount + $order_item['total_price_tax_incl'];

		    if (!$id_order_item = $this->saveOrder('order_items', $order_item)) {
			Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s'), $order_id, $item->SKU);
			continue;
		    }		   

		    //Order item attribute
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['attributes'])) {
			foreach ($product->combination[$product->id_product_attribute]['attributes'] as $attr_key => $attr_value) {
			    $order_item_attr['id_order_items'] = $id_order_item;
			    $order_item_attr['id_attribute_group'] = $attr_key;

			    foreach ($product->combination[$product->id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
				$order_item_attr['id_attribute'] = $id_attribute;
			    }
			    $order_item_attr['id_shop'] = (int) $id_shop;
			    $sql .= $this->insert_string('order_items_attribute', $order_item_attr);
			}
		    }
		    
		    // payment
		    $payment['id_orders'] = $id_order;
		    $payment['id_order_items'] = $id_order_item;
		    $payment['payment_method'] = isset($data->PurchaseMethod) ? $data->PurchaseMethod : null;
		    $payment['payment_status'] = isset($data->OrderStatus) ? $data->OrderStatus : null;
		    $payment['id_currency'] = $id_currency;
		    $payment['Amount'] = (float) $item->ItemPriceAmount;

		    $sql .= $this->insert_string('order_payment', $payment);
		    
		} else {
		    $id_order_item = $order_item['id_product'] . '_' . $order_item['id_product_attribute'];
		}

		$order_list['items'][$id_order_item] = array(
		    'id_product' => $order_item['id_product'],
		    'id_product_attribute' => $order_item['id_product_attribute'],
		    'quantity' => $order_item['quantity'],
		    'quantity_in_stock' => $order_item['quantity_in_stock'],
		    'reference' => $order_item['reference'],
		    'product_name' => $order_item['product_name'],
		    'time' => $today_date
		);

	    }
	}
        
	if(!$pending_order) {

	    //check empty Order skip
	    if (empty($data->Items) || empty($order_item)) {
		
		// Update Order Status
		$eorder = array(
		    'status' => '0',
		    'comment' => "Product items mismatch"
		);
		$where = array(
		    'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		    'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		    'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
		    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
		);
		$e_sql = $this->update_string('orders', $eorder, $where);
		$this->exec_query($this->db->order, $e_sql, false, false);
	    }
	
	    // Buyer
	    if (!isset($data->BuyerEmail) || empty($data->BuyerEmail)) {
		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not add buyer'), $order_id);
	    } else {
		$buyer = array();
		$buyer['id_orders'] = $id_order;
		$buyer['site'] = $site;
		$buyer['id_buyer_ref'] = $order_id;
		$buyer['email'] = (string) $data->BuyerEmail;
		$buyer['name'] = (string) $data->BuyerName;
		$buyer['address_name'] = (string) $data->Address->Name;
		$buyer['address1'] = (string) $data->Address->AddressLine1;
		$buyer['address2'] = (string) $data->Address->AddressLine2;
		$buyer['city'] = (string) $data->Address->City;
		$buyer['state_region'] = (string) $data->Address->StateOrRegion;
		$buyer['postal_code'] = (string) $data->Address->PostalCode;
		$buyer['country_code'] = (string) $data->Address->CountryCode;
		$buyer['phone'] = (string) $data->Address->Phone;

		$sql .= $this->insert_string('order_buyer', $buyer);
	    }

	    // Shipping        
	    $shipping = array();
	    $shipping['id_orders'] = $id_order;
	    $shipping['shipment_service'] = trim((string) $data->ShipServiceLevel);
	    $shipping['shipping_services_level'] = trim((string) $data->ShipmentServiceLevelCategory);
	    
	    $id_carrier = $this->getMappingIdCarrierByName($shipping['shipment_service'], $id_shop, null, 'incoming', $site);
	    if (!$id_carrier) {
		Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Unable to associate the carrier (%s) for order #%s'), $shipping['shipment_service'], $order_id);
	    } else {
		$shipping['id_carrier'] = (int) $id_carrier;
	    }
	    $sql .= $this->insert_string('order_shipping', $shipping);
            
	    // Invoice
	    $invoice['id_orders'] = $id_order;        
	    $invoice['total_discount_tax_incl'] = $total_discount;
	    $invoice['total_paid_tax_incl'] = $total_paid;
	    $invoice['total_products'] = $total_quantity;
	    $invoice['total_shipping_tax_incl'] = $total_shipping;
	    $invoice['date_add'] = $today_date;
	    $sql .= $this->insert_string('order_invoice', $invoice);

	    // Taxes
	    $taxes['id_orders'] = $id_order;
	    $taxes['tax_amount'] = $total_tax_rate;
	    $taxes['tax_on_shipping_amount'] = $total_shipping_tax;

	    $sql .= $this->insert_string('order_taxes', $taxes);

	    // Update Order
	    $uorder['total_shipping'] = $total_shipping;
	    $uorder['total_discount'] = $total_discount;
	    $uorder['total_amount'] = $total_amount;
	    $uorder['gift_message'] = $gift_message;
	    $uorder['gift_amount'] = $gift_amount;
	    $uorder['comment'] = $message;

	    $where = array(
		'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
		'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
	    );

	    $sql .= $this->update_string('orders', $uorder, $where);  

	    if (!$this->exec_query($this->db->order, $sql, false, true, true)) {
		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Unable to execute parameters for order #%s'), $order_id);

		// Update Order Status
		$eorder = array(
		    'status' => '0',
		    'comment' => Amazon_Tools::l('Unable to save order items')
		);
		$where = array(
		    'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		    'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		    'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
		    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
		);
		$e_sql = $this->update_string('orders', $eorder, $where);
		$this->exec_query($this->db->order, $e_sql, false, false);
	    } else {
                // When IsPrim, create shipment.
                //if($isPrime){
               
                    //1. GetEligibleShippingServices
                    //$order_list['IsPrime']['order_id'] = $order_id;
                    //$order_list['IsPrime']['PackageDimensions'] = $order_id;

                //}
                // order id
                // PackageDimensions
                // ItemList
                // Weight
                // MustArriveByDate = > LatestShipDate

                //ShippingServiceOptions:
                //- DeliveryConfirmationWithAdultSignature - Delivery confirmation with adult signature.
                //- DeliveryConfirmationWithSignature - Delivery confirmation with signature.
                //- DeliveryConfirmationWithoutSignature - Delivery confirmation without signature.
                //- NoTracking - No delivery confirmation.

                // CarrierWillPickUp : true if the carrier will pick up the package, otherwise false.
                // DeclaredValue : CurrencyCode, Amount
            }
	}	
	
	if(isset($order_list) && !empty($order_list))
	    Amazon_Order::$orders[$order_id] = $order_list ;
        
        Amazon_Order::$orders[$order_id]['FulfillmentChannel'] = $data->FulfillmentChannel ;
        Amazon_Order::$orders[$order_id]['OrderStatus'] =  isset($data->OrderStatus) ? $data->OrderStatus : null;
	
	// update order multichannel
	if(isset($id_order) && !empty($id_order)){
            $this->setMpMultichannel($id_order, $id_shop, $site, $order_id, constant('Amazon_Order::'.strtoupper($data->OrderStatus)), $data->FulfillmentChannel, null, null, $isPrime);
	    Amazon_Order::$orders[$order_id]['AmazonOrderID'] = $order_id ; 
	    Amazon_Order::$orders[$order_id]['MerchantOrderID'] = $id_order ;
	    
	} else {
	    Amazon_Order::$orders[$order_id]['LastUpdateDate'] =  isset($data->LastUpdateDate) ? $data->LastUpdateDate : date('Y-m-d H:i:s');
	}

        if($this->debug){
            echo '<pre>$order_list["items"]</pre><pre>' . print_r($order_list['items'], true) . '</pre>';
        }

	// Log order pending
	if ($pending_order)
	{
	    // save to amazon_orders
	    $pending_order_value = array(
	       'id_shop' => (int) $id_shop,
	       'id_country' => (int) $site,
	       'mp_order_id' =>  $order_id,
	       'mp_shipping' =>   trim((string) $data->ShipmentServiceLevelCategory),
	       'mp_channel' => $data->FulfillmentChannel,
	       'mp_status' => constant('Amazon_Order::'.strtoupper($data->OrderStatus)),
	       'quantity' => $total_quantity,
	       'price' => $total_paid,
	       'order_date' => isset($data->PurchaseDate) ? $data->PurchaseDate : null,
	       'date_upd' => date("Y-m-d H:i:s"),
	    );
	    
	    $pending_order_sql = $this->replace_string(self::$m_pref.'amazon_orders', $pending_order_value, array(), $this->db->order);  	   

	    if($this->exec_query($this->db->order, $pending_order_sql, false, false)){
		
		// save to amazon_orders_items
		if(isset($order_list['items']) && !empty($order_list['items'])){

		    // check table exists
		    if($this->db_table_exists($this->db->order, self::$m_pref.'amazon_orders_items', true)) {
			    
			$pending_order_item_sql = '';

			foreach ($order_list['items'] as $order_l){

			    $pending_order_item_value = array(
				'id_shop'  => (int) $id_shop,
				'id_country' => (int) $site,
				'mp_order_id' =>  $order_id,
				'id_product' => (int) $order_l['id_product'],
				'id_product_attribute' => (int) $order_l['id_product_attribute'],
				'sku' => $order_l['reference'],
				'quantity' => (int) $order_l['quantity'],
				'date_upd' => date("Y-m-d H:i:s")		    
			    );

			    $pending_order_item_sql .= $this->replace_string(self::$m_pref.'amazon_orders_items', $pending_order_item_value, array(), $this->db->order);  
			}

			$this->exec_query($this->db->order, $pending_order_item_sql, false, true, true);
		    }		    
		}		
	    }
	    
	} else {
	    
	    // check order exits in amazon_orders
	    $OrderPending = $this->getOrderPendingByIdRef($order_id);
	   
	    if( isset($OrderPending['mp_status']) && ($OrderPending['mp_status'] != constant('Amazon_Order::'.strtoupper($data->OrderStatus))) ){
		
		$pending_order_value = array(
		    'mp_shipping' =>   trim((string) $data->ShipmentServiceLevelCategory),
		    'mp_channel' => $data->FulfillmentChannel,
		    'mp_status' => constant('Amazon_Order::'.strtoupper($data->OrderStatus)),
		    'quantity' => $total_quantity,
		    'price' => $total_paid,
		    'date_upd' => date("Y-m-d H:i:s"),
		);
		
		$pending_order_where = array(
		    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop),
		    'id_country' =>  array('operation' => '=', 'value' => (int) $site),
		    'mp_order_id' => array('operation' => '=', 'value' =>  $order_id ),
		);
		 
		$pending_order_sql = $this->update_string(self::$m_pref.'amazon_orders', $pending_order_value, $pending_order_where, $this->db->order);  	   

		$this->exec_query($this->db->order, $pending_order_sql, false, false);
	    }
	}
	
        return $order_list;
    }
    
    public function patchOrder($id_order, $data, $option = null) {
	
    	$sorder = $uorder = $order_item = $order_item_attr = $invoice = $payment = $order_list = array();
    	$total_discount = $total_shipping = $total_amount = $gift_amount = $total_quantity = $total_tax_rate = $total_shipping_tax = 0;
    	$message = $sql = $gift_message = null;
    	$today_date = date("Y-m-d H:i:s");
    
    	//Params
    	$id_shop = $option->id_shop;
    	$site = $option->id_country;
    	$id_lang = $option->id_lang;
    	$id_region = strtoupper($option->id_region);
    
    	// Charset conversion
    	foreach ($data as $order_key => $item) {
    		if (!is_object($data->$order_key) && !is_array($data->$order_key) && strval($data->$order_key))
    			if (mb_detect_encoding(strval($data->$order_key)) != 'UTF-8')
    				$data->$order_key = iconv("ISO-8859-15", "UTF-8//TRANSLIT", $item);
    	}
    
    	// Amazon Order Id
    	$order_id = (string) $data->AmazonOrderId;
    
    	// id currency
    	$id_currency = (int) $this->getCurrencyIdByIsoCode($data->OrderTotalCurrency, $id_shop);
    	$total_paid = isset($data->OrderTotalAmount) && ($data->OrderTotalAmount > 0) ? (float) $data->OrderTotalAmount : 0;
    	$sales_channel = isset($data->SalesChannel) && !empty($data->SalesChannel) ? $data->SalesChannel : Amazon_Order::Amazon . $option->ext;
        $isPrime = isset($data->IsPrime) ? (bool) $data->IsPrime : false ;
        
    	//Item
    	if (isset($data->Items) && !empty($data->Items)) {		
            foreach ($data->Items as $id_item => $item) {

                //file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/patchOrder-'.$id_order.'-1.txt', print_r($item, true));

                // Check SKU
                if (!isset($item->SKU) || empty($item->SKU)) {
                        Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Item does not have SKU - #%s'), $order_id, $item->SKU);
                        //continue;
                } else {
                    // Product
                    if (!$product = $this->getProductBySKU($item->SKU, $id_shop, $option)) {

                            Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s') . '(1)', $order_id, $item->SKU);
                            //continue;
                    }
                }

                if(!isset($product->id_product) || !$product->id_product){
                    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s') . '(2)', $order_id, $item->SKU);
                }

                //file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/patchOrder-'.$id_order.'-2.txt', print_r($product, true));

                if(isset($product) && !empty($product) && isset($product->id_product))
                {
                    $order_item['id_orders'] = $id_order;
                    $order_item['id_marketplace_order_item_ref'] = $id_item;
                    $order_item['id_shop'] = (int) $id_shop;
                    $order_item['id_product'] = $product->id_product;
                    $order_item['reference'] = $item->SKU;
                    $order_item['id_marketplace_product'] = $item->ASIN;
                    $order_item['id_product_attribute'] = isset($product->id_product_attribute) ? $product->id_product_attribute : null;
                    $order_item['product_name'] = $item->Title;
                    $order_item['quantity'] = (int) $item->QuantityOrdered;
                    $total_quantity = $total_quantity + $order_item['quantity'];

                    // Product quantity
                    $quantity_in_stock = $product->quantity;
                    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]))
                            $quantity_in_stock = $product->combination[$product->id_product_attribute]['quantity'];
                    $order_item['quantity_in_stock'] = $quantity_in_stock;

                    // Product weight
                    $product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
                    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['weight']['value']))
                            $product_weight = $product->combination[$product->id_product_attribute]['weight']['value'];
                    $order_item['product_weight'] = $product_weight;

                    // Product price
                    $product_price = $product->price;
                    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['price'])){
                        if (isset($product->combination[$product->id_product_attribute]['price'])) {
                            $product_price = $product->combination[$product->id_product_attribute]['price'];
                        }
                    }
                    $order_item['product_price'] = $product_price;

                    // Shipping
                    $order_item['shipping_price'] = (float) $item->ShippingPriceAmount;
                    $total_shipping = ( $total_shipping + $order_item['shipping_price'] );

                    // Tax
                    $order_item['tax_rate'] = (float) $item->TaxesInformation->ItemTaxAmount;
                    $total_tax_rate = $total_tax_rate + $order_item['tax_rate'];
                    $total_shipping_tax = $total_shipping_tax + (float) $item->TaxesInformation->ShippingTaxAmount;

                    $item_quantity = 1;
                    if($order_item['quantity'] > 0)
                        $item_quantity = (int) $order_item['quantity'];

                    // Before VAT
                    $order_item['unit_price_tax_excl'] = ((float) $item->ItemPriceAmount / $item_quantity);
                    $order_item['total_price_tax_excl'] = (float) $item->ItemPriceAmount;
                    $order_item['total_shipping_price_tax_excl'] = (float) $item->ShippingPriceAmount - $order_item['tax_rate'];

                    // After VAT
                    $order_item['unit_price_tax_incl'] = ( $order_item['unit_price_tax_excl'] + $order_item['tax_rate'] );
                    $order_item['total_price_tax_incl'] = ( $order_item['total_price_tax_excl'] + $order_item['tax_rate'] );
                    $order_item['total_shipping_price_tax_incl'] = (float) $item->ShippingPriceAmount;

                    // Condition
                    $order_item['id_condition'] = $product->id_condition;

                    // Promotion
                    $order_item['promotion'] = $item->PromotionId;

                    //Gift Message
                    $order_item['message'] = $item->GiftMessageText;
                    //$message = $item->GiftMessageText;
                    $gift_message = $item->GiftMessageText;
                    $gift_amount = $gift_amount + (float) $item->GiftWrapPrice;

                    // added gift to carrier field
                    if($data->FulfillmentChannel == AmazonImportOrders::AFN){
                        $total_shipping  = ( $total_shipping + $gift_amount ) ;
                        $gift_amount = 0 ;
                    }

                    // Discount
                    if (isset($item->ShippingDiscount) && $item->ShippingDiscount > 0 && $data->FulfillmentChannel != AmazonImportOrders::AFN) {
                            $total_discount = ( $total_discount + (float) $item->ShippingDiscount );
                            $total_shipping = ( $total_shipping - (float) $item->ShippingDiscount  );
                    }

                    if (isset($item->PromotionDiscount) && $item->PromotionDiscount > 0) {
                            $total_discount = ( $total_discount + (float) $item->PromotionDiscount );
                            //$total_paid = ($total_paid - (float) $item->PromotionDiscount);
                    }

                    // Total Amount
                    $total_amount = $total_amount + $order_item['total_price_tax_incl'];

                    $id_order_item = $this->getImportedOrderItem($id_item);
                    if(!isset($id_order_item) || empty($id_order_item)){
                            //this order item is not imported.
                            if (!$id_order_item = $this->saveOrder('order_items', $order_item)) {
                                    Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s') . '(3)', $order_id, $item->SKU);
                                    continue;
                            }
                    }

                    //Order item attribute
                    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['attributes'])) {
                            foreach ($product->combination[$product->id_product_attribute]['attributes'] as $attr_key => $attr_value) {
                                    $order_item_attr['id_order_items'] = $id_order_item;
                                    $order_item_attr['id_attribute_group'] = $attr_key;

                                    foreach ($product->combination[$product->id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
                                            $order_item_attr['id_attribute'] = $id_attribute;
                                    }
                                    $order_item_attr['id_shop'] = (int) $id_shop;
                                    if(!$this->checkImportedOrderItemAttribute($id_order_item, $attr_key, $id_attribute)){
                                            $sql .= $this->insert_string('order_items_attribute', $order_item_attr);
                                    }
                            }
                    }

                    $order_list['items'][$id_order_item] = array(
                                    'id_product' => $order_item['id_product'],
                                    'id_product_attribute' => $order_item['id_product_attribute'],
                                    'quantity' => $order_item['quantity'],
                                    'quantity_in_stock' => $order_item['quantity_in_stock'],
                                    'reference' => $order_item['reference'],
                                    'product_name' => $order_item['product_name'],
                                    'time' => $today_date
                    );

                }

                // payment
                $payment['id_orders'] = $id_order;
                $payment['id_order_items'] = isset($id_order_item) ? $id_order_item : $id_item;
                $payment['payment_method'] = isset($data->PurchaseMethod) ? $data->PurchaseMethod : null;
                $payment['payment_status'] = isset($data->OrderStatus) ? $data->OrderStatus : null;
                $payment['id_currency'] = $id_currency;
                $payment['Amount'] = (float) $item->ItemPriceAmount;

                if(!$this->checkImportedOrderDependency('order_payment', $id_order)){
                        $sql .= $this->insert_string('order_payment', $payment);
                }
            }
    	}
    
    	//check empty Order skip
    	/*if (empty($data->Items) || empty($order_item)) {
    		//Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Missing Items for order #%s'), $order_id);
    
    		// Update Order Status
    		$eorder = array(
    				'status' => 'error',
    				'comment' => Amazon_Tools::l('Product items mismatch')
    		);
    		$where = array(
    				'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
    				'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
    				'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
    				'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
    		);
    		$e_sql = $this->update_string('orders', $eorder, $where);
    		$this->exec_query($this->db->order, $e_sql, false, false);
    	}*/
    
    	// Buyer
    	if(!$this->checkImportedOrderDependency('order_buyer', $id_order)){
	    
	    	if (!isset($data->BuyerEmail) || empty($data->BuyerEmail)) {
	    		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not add buyer'), $order_id);
	    	} else {
	    		$buyer = array();
	    		$buyer['id_orders'] = $id_order;
	    		$buyer['site'] = $site;
	    		$buyer['id_buyer_ref'] = $order_id;
	    		$buyer['email'] = (string) $data->BuyerEmail;
	    		$buyer['name'] = (string) $data->BuyerName;
	    		$buyer['address1'] = (string) $data->Address->AddressLine1;
	    		$buyer['address2'] = (string) $data->Address->AddressLine2;
	    		$buyer['city'] = (string) $data->Address->City;
	    		$buyer['state_region'] = (string) $data->Address->StateOrRegion;
	    		$buyer['postal_code'] = (string) $data->Address->PostalCode;
	    		$buyer['country_code'] = (string) $data->Address->CountryCode;
	    		$buyer['phone'] = (string) $data->Address->Phone;
	    		$buyer['security_code_token'] = (string) $data->Address->Name;
	    		$buyer['address_name'] = (string) $data->Address->Name;
	    
	    		$sql .= $this->insert_string('order_buyer', $buyer);
	    	}
    	}
    
    	// Shipping
	$shipping = array();
	$shipping['id_orders'] = $id_order;
	$shipping['shipment_service'] = trim((string) $data->ShipServiceLevel);
	$shipping['shipping_services_level'] = trim((string) $data->ShipmentServiceLevelCategory);
	
	$id_carrier = $this->getMappingIdCarrierByName($shipping['shipment_service'], $id_shop, null, 'incoming', $site);
	if (!$id_carrier) {
	    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Unable to associate the carrier (%s) for order #%s'), $shipping['shipment_service'], $order_id);
	} else {
	    $shipping['id_carrier'] = (int) $id_carrier;
	}
	    
    	if(!$this->checkImportedOrderDependency('order_shipping', $id_order)){echo __LINE__;
    		$sql .= $this->insert_string('order_shipping', $shipping);
    	} else {	    
    		unset($shipping['id_orders']);		
		if (isset($data->Items) && !empty($data->Items)) {
		    $sql .= $this->update_string('order_shipping', $shipping, array('id_orders' => array('operation' => '=', 'value' => $id_order)));
		}
    	}

    	// Invoice
        $invoice['id_orders'] = $id_order;
        $invoice['total_discount_tax_incl'] = $total_discount;
        $invoice['total_paid_tax_incl'] = $total_paid;
        $invoice['total_products'] = $total_quantity;
        $invoice['total_shipping_tax_incl'] = $total_shipping;
        $invoice['date_add'] = $today_date;
	    	
    	if(!$this->checkImportedOrderDependency('order_invoice', $id_order)){
	    	$sql .= $this->insert_string('order_invoice', $invoice);
    	} else{
    		unset($invoice['id_orders']);
		if (isset($data->Items) && !empty($data->Items)) {
		    $sql .= $this->update_string('order_invoice', $invoice, array('id_orders' => array('operation' => '=', 'value' => $id_order)));
		}
    	}    	
    
    	// Taxes
        $taxes['id_orders'] = $id_order;
        $taxes['tax_amount'] = $total_tax_rate;
        $taxes['tax_on_shipping_amount'] = $total_shipping_tax;
	    
    	if(!$this->checkImportedOrderDependency('order_taxes', $id_order)){
	    	$sql .= $this->insert_string('order_taxes', $taxes);
    	} else {
    		unset($taxes['id_orders']);
		if (isset($data->Items) && !empty($data->Items)) {
		    $sql .= $this->update_string('order_taxes', $taxes, array('id_orders' => array('operation' => '=', 'value' => $id_order)));
		}
    	}
    
    	// Update Order
	if(isset($data->OrderStatus) && $data->OrderStatus == 'Shipped')
	    $uorder['order_status'] = $data->OrderStatus;
	
    	$uorder['total_shipping'] = $total_shipping;
    	$uorder['total_discount'] = $total_discount;
    	$uorder['total_amount'] = $total_amount;
    	$uorder['gift_message'] = $gift_message;
    	$uorder['gift_amount'] = $gift_amount;
    	$uorder['comment'] = $message;
        
    	$where = array(
    			'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
    			'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
    			'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
    			'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
    	);
	
	if (isset($data->Items) && !empty($data->Items)) {	
	    $sql .= $this->update_string('orders', $uorder, $where);
	}

        $this->setMpMultichannel($id_order, $id_shop, $site, $order_id, constant('Amazon_Order::'.strtoupper($data->OrderStatus)), $data->FulfillmentChannel, null, null, $isPrime);
	//$this->setMpMultichannel($id_order, $id_shop, $site, $order_id, constant('Amazon_Order::'.strtoupper($data->OrderStatus)), $data->FulfillmentChannel);
	
	if(!empty($sql))
    	if (!$this->exec_query($this->db->order, $sql, false, true, true)) {
    		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Unable to execute parameters for order #%s'), $order_id);
    
    		// Update Order Status
    		$eorder = array(
    				'status' => '0',
    				'comment' => Amazon_Tools::l('Unable to save order items')
    		);
    		$where = array(
    				'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
    				'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
    				'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
    				'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
    		);
    		$e_sql = $this->update_string('orders', $eorder, $where);
    		$this->exec_query($this->db->order, $e_sql, false, false);
    	}
    
	// check order exits in amazon_orders
	$OrderPending = $this->getOrderPendingByIdRef($order_id);
	
	if( isset($OrderPending['mp_status']) && ((int)$OrderPending['mp_status'] != constant('Amazon_Order::'.strtoupper($data->OrderStatus))) ){

	    $pending_order_value = array(
		'mp_shipping' => trim((string) $data->ShipmentServiceLevelCategory),
		'mp_channel' => $data->FulfillmentChannel,
		'mp_status' => constant('Amazon_Order::'.strtoupper($data->OrderStatus)),
		'quantity' => $total_quantity,
		'price' => $total_paid,
		'date_upd' => date("Y-m-d H:i:s"),
	    );

	    $pending_order_where = array(
		'id_shop' => array('operation' => '=', 'value' => (int) $id_shop),
		'id_country' =>  array('operation' => '=', 'value' => (int) $site),
		'mp_order_id' => array('operation' => '=', 'value' =>  $order_id ),
	    );

	    $pending_order_sql = $this->update_string(self::$m_pref.'amazon_orders', $pending_order_value, $pending_order_where, $this->db->order);  	   

	    $this->exec_query($this->db->order, $pending_order_sql, false, false);
	}

	// set to update stock movement
        if(isset($order_list) && !empty($order_list))
	    Amazon_Order::$orders[$order_id] = $order_list ;

        Amazon_Order::$orders[$order_id]['FulfillmentChannel'] = $data->FulfillmentChannel ;
        Amazon_Order::$orders[$order_id]['OrderStatus'] =  isset($data->OrderStatus) ? $data->OrderStatus : null;

	if(isset($id_order) && !empty($id_order)){

	    Amazon_Order::$orders[$order_id]['AmazonOrderID'] = $order_id ;
	    Amazon_Order::$orders[$order_id]['MerchantOrderID'] = $id_order ;

	} else {

	    Amazon_Order::$orders[$order_id]['LastUpdateDate'] =  isset($data->LastUpdateDate) ? $data->LastUpdateDate : date('Y-m-d H:i:s');

	}

    	return($order_list);
    }


    public function insert_string($table, $data, $db = null) {
	$no_prefix = true;
	if(!isset($db) || empty($db)){
	    $db = $this->db->order;
	    $no_prefix = false;
	} 
	
        $filter_data = $this->_filter_data($db, $table, $data, $no_prefix);
        return $db->insert_string($table, $filter_data, $no_prefix);
    }

    public function update_string($table, $data, $where = array(), $db = null, $limit = 0) {
	
	$no_prefix = true;
	if(!isset($db) || empty($db)){
	    $db = $this->db->order;
	    $no_prefix = false;
	} 
	
        $filter_data = $this->_filter_data($db, $table, $data, $no_prefix);
        return $db->update_string($table, $filter_data, $where, $limit, $no_prefix);
    }
    
    public function replace_string($table, $data, $where = array(), $db = null) {
	$no_prefix = true;
	if(!isset($db) || empty($db)){
	    $db = $this->db->order;
	    $no_prefix = false;
	} 
        $filter_data = $this->_filter_data($db, $table, $data, $no_prefix);
        return $db->replace_string($table, $filter_data, $where, $no_prefix);
    }

    public function saveOrder($table, $data) {
        $id = false;
        $sql = $this->insert_string($table, $data);
	
        if($this->debug) {
	    
            printf("$table");
            var_dump($sql);
	    
	    return rand(1, 100);
	     
        } else {        
	    
            if ($this->exec_query($this->db->order, $sql, false, false))
                $id = $this->db->order->db_last_insert_rowid(); 
	    
        }

        if($id) {
            return $id;
        } else {

            //file_put_contents(USERDATA_PATH.$this->user.'/saveOrder_'.$table.'_'.date('YmdHis').'.txt', print_r(array('data' => $data, 'SQL' => $sql), true));

            return false;
        }
    }
    
    public function saveOrderItems($id_shop, $id_order, $order_id, $order_items, $option=null) {
	
	//Set Ordered items (products)
        if (isset($order_items->OrderItems->OrderItem) && !empty($order_items->OrderItems->OrderItem)) {
            $items = array();
            foreach ($order_items->OrderItems->OrderItem as $key => $item) {
                $OrderItemId = (string) $item->OrderItemId;
                $items[$OrderItemId] = new OrderedItem($item);
            }
        }
	
	//Item
    	if (isset($items) && !empty($items)) {
		    
	    $sql = '';
	    $gift_message = $message = null;
	    $total_discount = $total_shipping = $total_amount = $gift_amount = $total_quantity = $total_tax_rate = $total_shipping_tax = $total_paid = 0;
	    
	    foreach ($items as $id_item => $item) {

		// Check SKU
		if (!isset($item->SKU) || empty($item->SKU)) {
			Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Item does not have SKU - #%s'), $order_id, $item->SKU);
			//continue;
		} else {
		    // Product
		    if (!$product = $this->getProductBySKU($item->SKU, $id_shop, $option)) {

			Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s'), $order_id, $item->SKU);
			//continue;
		    }
		}	

		if(!isset($product->id_product) || !$product->id_product){
		    Amazon_Order::$warnings[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s'), $order_id, $item->SKU);
		}
			
		if(isset($product) && !empty($product) && isset($product->id_product))
		{
		    
		    $order_item['id_orders'] = $id_order;
		    $order_item['id_marketplace_order_item_ref'] = $id_item;
		    $order_item['id_shop'] = (int) $id_shop;
		    $order_item['id_product'] = $product->id_product;
		    $order_item['reference'] = $item->SKU;
		    $order_item['id_marketplace_product'] = $item->ASIN;
		    $order_item['id_product_attribute'] = isset($product->id_product_attribute) ? $product->id_product_attribute : null;
		    $order_item['product_name'] = $item->Title;
		    $order_item['quantity'] = (int) $item->QuantityOrdered;
		    $total_quantity = $total_quantity + $order_item['quantity'];

		    // Product quantity
		    $quantity_in_stock = $product->quantity;
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]))
			    $quantity_in_stock = $product->combination[$product->id_product_attribute]['quantity'];
		    $order_item['quantity_in_stock'] = $quantity_in_stock;

		    // Product weight
		    $product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['weight']['value']))
			    $product_weight = $product->combination[$product->id_product_attribute]['weight']['value'];
		    $order_item['product_weight'] = $product_weight;

		    // Product price
		    $product_price = $product->price;
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['price'])){
			if (isset($product->combination[$product->id_product_attribute]['price'])) {
			    $product_price = $product->combination[$product->id_product_attribute]['price'];
			}
		    }
		    $order_item['product_price'] = $product_price;

		    // Shipping
		    $order_item['shipping_price'] = (float) $item->ShippingPriceAmount;
		    $total_shipping = ( $total_shipping + $order_item['shipping_price'] );

		    // Tax
		    $order_item['tax_rate'] = (float) $item->TaxesInformation->ItemTaxAmount;
		    $total_tax_rate = $total_tax_rate + $order_item['tax_rate'];
		    $total_shipping_tax = $total_shipping_tax + (float) $item->TaxesInformation->ShippingTaxAmount;

		    $item_quantity = 1;
		    if($order_item['quantity'] > 0)
			$item_quantity = (int) $order_item['quantity'];

		    // Before VAT
		    $order_item['unit_price_tax_excl'] = ((float) $item->ItemPriceAmount / $item_quantity);
		    $order_item['total_price_tax_excl'] = (float) $item->ItemPriceAmount;
		    $order_item['total_shipping_price_tax_excl'] = (float) $item->ShippingPriceAmount - $order_item['tax_rate'];

		    // After VAT
		    $order_item['unit_price_tax_incl'] = ( $order_item['unit_price_tax_excl'] + $order_item['tax_rate'] );
		    $order_item['total_price_tax_incl'] = ( $order_item['total_price_tax_excl'] + $order_item['tax_rate'] );
		    $order_item['total_shipping_price_tax_incl'] = (float) $item->ShippingPriceAmount;

		    // Condition
		    $order_item['id_condition'] = $product->id_condition;

		    // Promotion
		    $order_item['promotion'] = $item->PromotionId;

		    //Gift Message
		    $order_item['message'] = $item->GiftMessageText;
		    //$message = $item->GiftMessageText;
		    $gift_message = $item->GiftMessageText;
		    $gift_amount = $gift_amount + (float) $item->GiftWrapPrice;

                    // added gift to carrier field
                    if($data->FulfillmentChannel == AmazonImportOrders::AFN){
                        $total_shipping  = ( $total_shipping + $gift_amount ) ;
                        $gift_amount = 0 ;
                    }

		    // Discount
		    if (isset($item->ShippingDiscount) && $item->ShippingDiscount > 0) {
			    $total_discount = ( $total_discount + (float) $item->ShippingDiscount );
                            $total_shipping = ( $total_shipping - (float) $item->ShippingDiscount );
                    }

		    if (isset($item->PromotionDiscount) && $item->PromotionDiscount > 0) {
			    $total_discount = ( $total_discount + (float) $item->PromotionDiscount );
                    }

		    // Total Amount
		    $total_amount = $total_amount + $order_item['total_price_tax_incl'];

		    $id_order_item = $this->getImportedOrderItem($id_item);
		    
		    if(!isset($id_order_item) || empty($id_order_item)){
			    //this order item is not imported.
			    if (!$id_order_item = $this->saveOrder('order_items', $order_item)) {
				    Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Order ID (%s) Could not import product SKU : %s'), $order_id, $item->SKU);
				    continue;
			    }
		    }

		    //Order item attribute
		    if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['attributes'])) {
			    foreach ($product->combination[$product->id_product_attribute]['attributes'] as $attr_key => $attr_value) {
				    $order_item_attr['id_order_items'] = $id_order_item;
				    $order_item_attr['id_attribute_group'] = $attr_key;

				    foreach ($product->combination[$product->id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
					    $order_item_attr['id_attribute'] = $id_attribute;
				    }
				    $order_item_attr['id_shop'] = (int) $id_shop;
				    if(!$this->checkImportedOrderItemAttribute($id_order_item, $attr_key, $id_attribute)){
					    $sql .= $this->insert_string('order_items_attribute', $order_item_attr);
				    }
			    }
		    }

		    // payment
		    $payment['id_orders'] = $id_order;
		    $payment['id_order_items'] = isset($id_order_item) ? $id_order_item : $id_item;
		    $payment['Amount'] = (float) $item->ItemPriceAmount;

		    if(!$this->checkImportedOrderDependency('order_payment', $id_order)){
			    $sql .= $this->insert_string('order_payment', $payment);
		    }
		}
	    }

	    // Update Order
	    $uorder['total_shipping'] = $total_shipping;
	    $uorder['total_discount'] = $total_discount;
	    $uorder['total_amount'] = $total_amount;
	    $uorder['gift_message'] = $gift_message;
	    $uorder['gift_amount'] = $gift_amount;
	    $uorder['comment'] = $message;

	    $where = array(
		'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
		'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
	    );

	    $sql .= $this->update_string('orders', $uorder, $where);  

	    if (!$this->exec_query($this->db->order, $sql, false, true, true)) {
		Amazon_Order::$errors[] = sprintf(Amazon_Tools::l('Unable to execute parameters for order #%s'), $order_id);

		// Update Order Status
		$eorder = array(
		    'status' => '0',
		    'comment' => Amazon_Tools::l('Unable to save order items')
		);
		$where = array(
		    'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
		    'id_marketplace_order_ref' => array('operation' => '=', 'value' =>  $order_id ),
		    'id_marketplace' => array('operation' => '=', 'value' => self::$MarketplaceID),
		    'id_shop' => array('operation' => '=', 'value' => (int) $id_shop)
		);
		$e_sql = $this->update_string('orders', $eorder, $where);
		$this->exec_query($this->db->order, $e_sql, false, false);
	    }
    	}
    }
    
    public function saveOrderMissingItems($id_shop, $id_orders, $id_marketplace_order_ref){
	$data = array(
	   'id_shop' => $id_shop,
	   'id_order' => $id_orders,
	   'id_marketplace_order_ref' => $id_marketplace_order_ref,
	   'date_upd' => date('Y-m-d H:i:s'),
	);
	$sql = $this->insert_string(self::$m_pref.'amazon_order_missing_items', $data, $this->db->order);
        return $this->exec_query($this->db->order, $sql, false, false);                
    }
    
    public function getOrderMissingItems($id_shop) {
	 
	$orders = array();		
	$sql = "SELECT * FROM ".self::$m_pref."amazon_order_missing_items WHERE id_shop = ".(int)$id_shop." ; ";	
	$result = $this->db->order->db_query_str($sql);
	
	foreach ($result as $order){
	    $id = $order['id_order'];
	    $orders[$id]['id_shop'] = $order['id_shop'];
	    $orders[$id]['id_order'] = $order['id_order'];
	    $orders[$id]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
	    $orders[$id]['date_upd'] = $order['date_upd'];
	}
	
	return $orders;
    }
    
    public function getOrdersToUpdateFBA($id_shop, $id_orders, $channel=null) {
	 
	$orders = array();	
		
	if(is_array($id_orders)){
	    $where_order = " AND o.id_orders IN (" .implode(", ", $id_orders) .") ";
	} else {
	    $where_order = " AND o.id_orders = $id_orders ";
	}
	
	$sql = "SELECT * FROM ".self::$or_pref."orders o
		LEFT JOIN ".self::$or_pref."order_invoice oi ON oi.id_orders = o.id_orders AND (oi.seller_order_id IS NOT NULL OR oi.seller_order_id != '')
		LEFT JOIN ".self::$m_pref."mp_multichannel mm ON mm.id_orders = o.id_orders AND (mm.seller_order_id IS NOT NULL OR mm.seller_order_id != '')
		WHERE o.id_shop = $id_shop AND o.status = 1 $where_order ";
	
	if(isset($channel) && !empty($channel))
	    $sql .= " AND mm.mp_channel = '$channel' ";
	
	$sql .= " ;";
	
	$result = $this->db->order->db_query_str($sql);
	
	foreach ($result as $order){
	    
	    $id = $order['seller_order_id'];
	    $orders[$id]['id_orders'] = $order['id_orders'];
	    $orders[$id]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
	    $orders[$id]['seller_order_id'] = $order['seller_order_id'];
	    $orders[$id]['sales_channel'] = $order['sales_channel'];
	    $orders[$id]['id_shop'] = $order['id_shop'];
	    $orders[$id]['site'] = $order['site'];
	    $orders[$id]['id_marketplace'] = $order['id_marketplace'];
	    $orders[$id]['id_lang'] = $order['id_lang'];
	    
	}
	
	return $orders;
    }
    
    public function getOrdersToCreateFBA($id_shop, $id_order=null, $type=null) {
	 
	$orders = array();
	
	$marketplace_exceptions = array(
	    'eBay' =>  " AND CASE 
			WHEN o.id_marketplace = 3 
			THEN (o.order_status IN('Completed') AND (o.purchase_date IS NOT NULL AND date(o.purchase_date) NOT IN ('', '0000-00-00 00:00:00')))
			ELSE o.order_status IS NOT NULL
		   END"
	);
	
	// select order not in mp_multichannel and between 15 day and status = 1
	$yesturday = date('Y-m-d H:i:s', strtotime("-7 day")); //// -15 day
	$today = date('Y-m-d H:i:s');
	
	$where_order = " ";
	if(isset($id_order) && !empty($id_order))
	{
	    $where_order = " AND o.id_orders = $id_order ";

            if(!isset($type))
                $where_order .= "AND payment_method NOT LIKE 'Amazon%' AND id_marketplace <> 2 ";

	} else {
	    $where_order = " AND (o.date_add >= '$yesturday' AND o.date_add <= '$today')
			    AND o.id_orders NOT IN (SELECT id_orders FROM ".self::$m_pref."mp_multichannel WHERE id_shop = $id_shop ) "
                            . " AND payment_method NOT LIKE 'Amazon%' AND id_marketplace <> 2 ";
	}
		
	$sql = "SELECT * FROM ".self::$or_pref."orders o
		LEFT JOIN ".self::$or_pref."order_invoice oi ON oi.id_orders = o.id_orders /*AND (seller_order_id IS NOT NULL OR seller_order_id != '')*/
		LEFT JOIN ".self::$or_pref."order_shipping os ON os.id_orders = o.id_orders 
		LEFT JOIN ".self::$or_pref."order_buyer ob ON ob.id_orders = o.id_orders 
		WHERE o.id_shop = $id_shop /*AND o.status = 1*/ $where_order ";
	
	if(!empty($marketplace_exceptions)) {
	    foreach ($marketplace_exceptions as $exceptions){
		$sql .= " $exceptions " ;
	    }
	}
	
	$result = $this->db->order->db_query_str($sql. "; ");
	
	foreach ($result as $order){
	    
	    // Order
	    $id = $order['seller_order_id'];
	    $orders[$id]['id_orders'] = $order['id_orders'];
	    $orders[$id]['id_marketplace_order_ref'] = $order['id_marketplace_order_ref'];
	    $orders[$id]['seller_order_id'] = $order['seller_order_id'];
	    $orders[$id]['sales_channel'] = $order['sales_channel'];
	    $orders[$id]['id_shop'] = $order['id_shop'];
	    $orders[$id]['site'] = $order['site'];
	    $orders[$id]['id_marketplace'] = $order['id_marketplace'];
	    $orders[$id]['id_lang'] = $order['id_lang'];
	    $orders[$id]['order_type'] = $order['order_type'];
	    $orders[$id]['order_status'] = $order['order_status'];
	    $orders[$id]['payment_method'] = $order['payment_method'];
	    $orders[$id]['id_currency'] = $order['id_currency'];
	    $orders[$id]['total_discount'] = $order['total_discount'];
	    $orders[$id]['total_amount'] = $order['total_amount'];
	    $orders[$id]['total_paid'] = $order['total_paid'];
	    $orders[$id]['total_shipping'] = $order['total_shipping'];
	    $orders[$id]['order_date'] = $order['order_date'];
	    $orders[$id]['shipping_date'] = $order['shipping_date'];
	    $orders[$id]['latest_shipping_date'] = $order['latest_shipping_date'];
	    $orders[$id]['delivery_date'] = $order['delivery_date'];
	    $orders[$id]['latest_delivery_date'] = $order['latest_delivery_date'];
	    $orders[$id]['purchase_date'] = $order['purchase_date'];
	    $orders[$id]['gift_message'] = $order['gift_message'];
	    $orders[$id]['gift_amount'] = $order['gift_amount'];
	    //$orders[$id]['marketplace_order_number'] = $order['marketplace_order_number'];
	    //$orders[$id]['affiliate_id'] = $order['affiliate_id'];
	    //$orders[$id]['commission'] = $order['commission'];
	    //$orders[$id]['ip'] = $order['ip'];
	    //$orders[$id]['comment'] = $order['comment'];
	    //$orders[$id]['status'] = $order['status'];
	    //$orders[$id]['date_add'] = $order['date_add'];
	    //$orders[$id]['shipping_status'] = $order['shipping_status'];
	    
	    // Invoice
	    $orders[$id]['invoice']['id_invoice'] = $order['id_invoice'];
	    $orders[$id]['invoice']['total_discount_tax_excl'] = $order['total_discount_tax_excl'];
	    $orders[$id]['invoice']['total_discount_tax_incl'] = $order['total_discount_tax_incl'];
	    $orders[$id]['invoice']['total_paid_tax_excl'] = $order['total_paid_tax_excl'];
	    $orders[$id]['invoice']['total_paid_tax_incl'] = $order['total_paid_tax_incl'];
	    $orders[$id]['invoice']['total_products'] = $order['total_products'];
	    $orders[$id]['invoice']['total_shipping_tax_excl'] = $order['total_shipping_tax_excl'];
	    $orders[$id]['invoice']['total_shipping_tax_incl'] = $order['total_shipping_tax_incl'];
	    $orders[$id]['invoice']['invoice_no'] = $order['invoice_no'];
	    //$orders[$id]['invoice']['note'] = $order['note'];
	    
	    // shipping
	    $orders[$id]['shipment']['shipment_service'] = $order['shipment_service'];
	    $orders[$id]['shipment']['weight'] = $order['weight'];
	    $orders[$id]['shipment']['shipping_services_cost'] = $order['shipping_services_cost'];
	    $orders[$id]['shipment']['shipping_services_level'] = $order['shipping_services_level'];
	    $orders[$id]['shipment']['id_carrier'] = $order['id_carrier'];
	    //$orders[$id]['shipment']['tracking_number'] = $order['tracking_number'];
	    
	    // buyer
	    $orders[$id]['buyer']['id_buyer'] = $order['id_buyer'];
	    $orders[$id]['buyer']['email'] = $order['email'];
	    $orders[$id]['buyer']['name'] = $order['name'];
	    $orders[$id]['buyer']['address1'] = $order['address1'];
	    $orders[$id]['buyer']['address2'] = $order['address2'];
	    $orders[$id]['buyer']['city'] = $order['city'];
	    $orders[$id]['buyer']['district'] = $order['district'];
	    $orders[$id]['buyer']['state_region'] = $order['state_region'];
	    $orders[$id]['buyer']['country_code'] = $order['country_code'];
	    $orders[$id]['buyer']['country_name'] = $order['country_name'];
	    $orders[$id]['buyer']['postal_code'] = $order['postal_code'];
	    $orders[$id]['buyer']['phone'] = $order['phone'];
	    $orders[$id]['buyer']['address_name'] = $order['address_name'];
	    $orders[$id]['buyer']['id_buyer_ref'] = $order['id_buyer_ref'];
	    //$orders[$id]['buyer']['id_address_ref'] = $order['id_address_ref'];
	    //$orders[$id]['buyer']['security_code_token'] = $order['security_code_token'];
	    
	    // Items
	    $sql_item = "SELECT * FROM ".self::$or_pref."order_items WHERE id_shop = $id_shop AND id_orders = " . $order['id_orders'] . "; ";
	    $result_item  = $this->db->order->db_query_str($sql_item);
	    
	    foreach ($result_item  as $order_item ){
		$orders[$id]['items'][$order_item['id_order_items']] = $order_item;
	    }
	    
	}
	//var_dump($orders); exit;
	return $orders;
    }
    
    public function getMpMultichannelByMpOrderId($order_id) {
	 
	$sql = "SELECT * FROM ".self::$m_pref."mp_multichannel WHERE mp_order_id = '" . $order_id . "' ; ";
	$result  = $this->db->order->db_query_str($sql);
	
	if(isset($result) && !empty($result))
	    foreach ($result  as $order ){
		return $order;
	    }

	return (false);
    }    
    
    public function setMpMultichannel($id_order, $id_shop, $site, $order_id, $OrderStatus, $FulfillmentChannel, $FulfillmentChannelStatus=null, $seller_order_id=null, $isPrime=null) {
	 
	$mp_order = array(
	   'id_orders' => (int) $id_order,
	   'id_shop' => (int) $id_shop,
	   'id_country' => (int) $site,
	   'mp_order_id' =>  $order_id,
	   'mp_status' => $OrderStatus,
	   'mp_channel' => $FulfillmentChannel,
	   'date_add' => date("Y-m-d H:i:s"),
	);
	
	if(isset($isPrime))
	    $mp_order['is_prime'] = $isPrime;

	if(isset($FulfillmentChannelStatus) && !empty($FulfillmentChannelStatus))
	    $mp_order['mp_channel_status'] = $FulfillmentChannelStatus;
	
	if(isset($seller_order_id) && !empty($seller_order_id))
	    $mp_order['seller_order_id'] = $seller_order_id;
	    
	$where = array(
	    'id_orders' => (int) $id_order,
	    'id_shop' => (int) $id_shop,
	    'id_country' => (int) $site,
	);	
	
	//$mp_sql = $this->replace_string(self::$m_pref.'mp_multichannel', $mp_order, $where, $this->db->order); 
	if(!$this->getMpMultichannelByMpOrderId($order_id))
	    $mp_sql = $this->insert_string(self::$m_pref.'mp_multichannel', $mp_order, $this->db->order); 
	else
	    $mp_sql = $this->update_string(self::$m_pref.'mp_multichannel', $mp_order, $where, $this->db->order); 
	
	if(!$this->debug) 
	   return $this->exec_query($this->db->order, $mp_sql, false, false);
	else
	   echo '<pre>' . print_r($mp_sql, true) . '</pre>' ;	 

	return (true);
    }
    
    public function updateMpMultichannelOrderStatus($id_shop, $id_country, $id_order, $OrderStatus) {
	 
	$mp_order = array(
	   'mp_status' => $OrderStatus,
	);
	
	$mp_order_where = array(
	   'id_orders' => array('operation' => '=', 'value' => (int) $id_order),
	   'id_shop' => array('operation' => '=', 'value' => (int) $id_shop),
	   'id_country' => array('operation' => '=', 'value' => (int) $id_country),
	);

        if($this->debug){
            echo '<br/><b>Order Status ID '.$id_order.' : </b><pre> '  . print_r($mp_order_where, true) . '</pre>';
        }

	$mp_sql = $this->update_string(self::$m_pref.'mp_multichannel', $mp_order, $mp_order_where, $this->db->order); 
        return $this->exec_query($this->db->order, $mp_sql, false, false);
    }
    
    public function updateOrderAcknowledgementFlag($id_shop, $id_country, $orders, $flag) {
        $sql = '';
        foreach ($orders as $order) {
            $sql .= "UPDATE ".self::$m_pref."mp_multichannel SET acknowledge_flag = $flag "
		. "WHERE mp_order_id = '".$order."' AND id_shop = $id_shop AND id_country = $id_country ; ";
	}
        return $this->exec_query($this->db->order, $sql, false);
    }
    
    public function updateOrderAcknowledgementSellerID($id_shop, $id_country, $orders) {
	$sql = '';
	foreach ($orders as $order) {
	    if(isset($order['AmazonOrderID']) && isset($order['MerchantOrderID'])) {
		$sql .= "UPDATE ".self::$m_pref."mp_multichannel SET seller_order_id = '".$order['MerchantOrderID']."' "
		    . "WHERE mp_order_id = '".$order['AmazonOrderID']."' AND id_shop = $id_shop AND id_country = $id_country ; ";
	    }
	}
        return $this->exec_query($this->db->order, $sql, false);
    }
    
    public function getOrderAcknowledgementToUpdate($id_shop, $id_country) {
	
	$orders = array();
    	$sql = "SELECT * FROM ".self::$m_pref."mp_multichannel "
		. "WHERE (acknowledge_flag IS NULL OR acknowledge_flag = 0) AND id_shop = $id_shop AND id_country = $id_country AND mp_channel = 'MFN' ; ";
	$result = $this->db->order->db_query_str($sql);
	foreach ($result as $order) {
	    if(isset($order['seller_order_id']) && !empty($order['seller_order_id'])) {
		$orders[$order['id_orders']]['AmazonOrderID'] = $order['mp_order_id'];
		$orders[$order['id_orders']]['MerchantOrderID'] = $order['seller_order_id'];
	    }
	}
	return $orders;
    }
    
    public function getOrderItemById($id_orders) {
	if(is_array($id_orders)){
	    $where_order = ' id_orders IN (' .  implode(', ', $id_orders). ') ' ;
	} else {
	    $where_order = ' id_orders = ' .$id_orders. ' ';
	}
    	$sql = "SELECT * FROM ".self::$or_pref."order_items WHERE $where_order ; ";
	$result = $this->db->order->db_query_str($sql);
	return $result;
    }
    
    public function get_validation_log($message=null) {
        $where = '';
	$table = self::$m_pref. 'amazon_validation_log';

        if(isset($message) && !empty($message))
	    $where = " WHERE message LIKE '%".$this->db->order->escape_str($message)."%' " ;
        
    	$sql = "SELECT * FROM $table $where ; ";           
	$result = $this->db->order->db_query_str($sql);
	return $result;
    }
    
    public function exec_query($db, $sql, $transaction = true, $multi_query=true, $direct=false, $commit=true) {
	if($this->debug){
            var_dump(array('query'=>$sql));
	    return true;
	} else {
	    if (@$db->db_exec($sql, $transaction, $multi_query, $direct, $commit)){
		return true;
            } else {
		return false;
            }
	}
    }

    private function _filter_data($db, $table, $data, $no_prefix = false) {
        $filtered_data = array();
        $columns = $this->_list_fields($db, $table, $no_prefix);
	 
        if (is_array($columns)) {
            foreach ($columns as $column) {
                if (isset($data[$column])) {
                    if (array_key_exists($column, $data)) {
                        if (is_array($data[$column])) {
                            $filtered_data[$column] = serialize($data[$column]);
                        } else
                            $filtered_data[$column] = $data[$column];
                    }else {
                        $filtered_data[$column] = '';
                    }
                }
            }
        }
        return $filtered_data;
    }

    private function _list_fields($db, $table, $no_prefix = false) {
        return $db->get_all_column_name($table, true, $no_prefix);
    }
    
    public function db_table_exists($db, $table, $no_prefix = true){
        return $db->db_table_exists($table, $no_prefix);
    }
    
    public function db_from($db, $table, $alias = null, $no_prefix = true){
        return  $db->from($table, $alias, $no_prefix);
    }
    
    public function truncate_table($table, $id_shop = null, $id_country = null, $where_fields = null, $no_prefix = true) {
        $where = '';
        if (isset($id_shop) && !empty($id_shop))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . ' id_shop = ' . $id_shop . ' ';

        if (isset($id_country) && !empty($id_country))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . 'id_country = ' . $id_country . ' ';

        if (isset($where_fields) && !empty($where_fields))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . $where_fields;

        return $this->db->product->truncate_table($table . $where . " ; ", $no_prefix);
    }
    
    public function truncate_validation_log($id_shop, $id_country, $action_process = null, $action_type = null) {
        $process = $type = '';
        if (isset($action_process) && !empty($action_process))
            $process = ' AND action_process = "' . $action_process . '" ';
        if (isset($action_type) && !empty($action_type))
            $type = ' AND action_type = "' . $action_type . '" ';
        return $this->truncate_table(self::$m_pref.'amazon_validation_log', $id_shop, $id_country, $process.$type);
    }

    public function save_validation_log($data) {
	$table = self::$m_pref. 'amazon_validation_log';
        if (isset($data) && !empty($data)) {
            $sql = $this->insert_string($table, $data, $this->db->product);
            if($this->debug){
                var_dump(array('function'=>__FUNCTION__, 'line'=>__LINE__, 'query'=>$sql));
                return true;
            } else {
                return $this->db->product->db_exec($sql);
            }
        }
        return null;
    }

    public function save_response_log($data) {
        $table = self::$m_pref.'amazon_submission_list_log';
        if (isset($data) && !empty($data)) {
            $sql = $this->insert_string($table, $data, $this->db->product);
            return @$this->db->product->db_exec($sql);
        }

        return null;
    }

    public function save_submission_result_log($data) {
        return $this->feedbiz_order->insertLog($this->user, $data);
    }

    public function update_order_error($id_order, $error) {
        //$user, $id_order , $status, $comment
        return $this->feedbiz_order->updateOrderErrorStatus($this->user, $id_order, '1', $error);
    }

    public function update_order_shipping($data) {
        return $this->feedbiz_order->updateShippedStatus($this->user, $data, 'id_marketplace_order_ref');
    }

    public function update_log($data, $batch_id = null) {
        $table = self::$m_pref.'amazon_log';
        $sql = '';
        $db = $this->db->product;
        if (isset($data) && !empty($data)) {            
            if (isset($batch_id) && !empty($batch_id)) {
                $where = array(
                    'batch_id' => $batch_id,
                    'id_country' => $data['id_country'],
                    'id_shop' => $data['id_shop']
                );
                $sql = $this->update_string($table, $data, $where, $db);
            } else {
                $sql = $this->insert_string($table, $data, $db);
            }
            
            if (!$this->exec_query($db, $sql, false, false))
                return false;
        }
        return true;
    }

    public function downloadLog($data) {
        $batch_id = date('Ymd_His');
        if (isset($data['batch_id']) && !empty($data['batch_id']))
            $batch_id = $data['batch_id'];
        $table = 'log';
        $list = $where = array();
        $where['id_shop'] = $data['id_shop'];
        $where['id_marketplace'] = self::$MarketplaceID;
        $where['site'] = $data['id_country'];
        if (isset($batch_id) && !empty($batch_id))
            $where['batch_id'] = $batch_id;

        $this->db->order->from($table);
        if (!empty($where))
            $this->db->order->where($where);
        $result = $this->db->order->db_array_query($this->db->order->query);

        $list['header']['SKU'] = "Amazon Order ID";
        $list['header']['ResultMessageCode'] = "Error Code";
        $list['header']['ResultDescription'] = "Message";

        foreach ($result as $key => $value) {
            $list[$key]['id_marketplace_ref'] = $value['request_id'];
            $list[$key]['error_code'] = $value['error_code'];
            $list[$key]['message'] = $value['message'];
        }

        ob_start();
        ob_get_clean();
        $filename = 'amazon_order_' . $batch_id . '.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data)
            fputcsv($h, $data, ';');
    }
}

//PlacedOrder class.
//Items of this class can be generated from GetOrdersList() function of the Service class.
class PlacedOrder {

    public $AmazonOrderId; //String, This value can be used for order shipment or for order cancelation
    public $OrderStatus;
    public $OrderType;
    public $PurchaseDate; //String, The date when the customer placed the order.
    public $EarliestShipDate; //String, The date when the customer placed the order.
    public $LatestShipDate; 
    public $EarliestDeliveryDate; //String, The date when the customer placed the order.
    public $LatestDeliveryDate; 
    public $PurchaseMethod; //String, The date when the customer placed the order.
    public $OrderTotalCurrency;
    public $OrderTotalAmount;
    public $ShipmentServiceLevelCategory; //String.
    public $ShipServiceLevel; //String.
    public $FulfillmentChannel; //String.
    public $SalesChannel;
    public $NumberOfItemsUnshipped;
    public $NumberOfItemsShipped;
    public $MarketPlaceId;
    public $BuyerEmail;
    public $BuyerName;
    public $Address; // Type: AddressInTheOrder. Instance Of the AddressInTheOrder class. Address where we send our order
    public $Items; // Type: Array of the OrderedItem class instances. Ordered products, array of the OrderedItem class instances.
    public $_debug;

    public function __construct(SimpleXMLElement $order, $Items, $debug = false) {
        if ($debug == true)
            $this->_debug = true;
        else
            $this->_debug = false;

        $this->Items = null;
        $this->AmazonOrderId = (string) $order->AmazonOrderId;
        $this->OrderStatus = (string) $order->OrderStatus;
        $this->OrderType = (string) $order->OrderType;
        $this->PurchaseDate = (string) $order->PurchaseDate;
        $this->PurchaseMethod = (string) $order->PaymentMethod;
        $this->EarliestShipDate = (string) $order->EarliestShipDate;
        $this->EarliestDeliveryDate = (string) $order->EarliestDeliveryDate;
        $this->OrderTotalCurrency = (string) $order->OrderTotal->CurrencyCode;
        $this->OrderTotalAmount = (string) $order->OrderTotal->Amount;
        $this->ShipmentServiceLevelCategory = trim((string) $order->ShipmentServiceLevelCategory);
        $this->ShipServiceLevel = (string) $order->ShipServiceLevel;
        $this->FulfillmentChannel = (string) $order->FulfillmentChannel;
        $this->SalesChannel = (string) $order->SalesChannel;
        $this->NumberOfItemsUnshipped = (string) $order->NumberOfItemsUnshipped;
        $this->NumberOfItemsShipped = (string) $order->NumberOfItemsShipped;
        $this->MarketPlaceId = (string) $order->MarketplaceId;
        $this->BuyerEmail = (string) $order->BuyerEmail;
        $this->BuyerName = (string) $order->BuyerName;
        $this->IsPrime = (int) $order->IsPrime;
        $this->LatestShipDate = (string) $order->LatestShipDate;
        $this->LatestDeliveryDate = (string) $order->LatestDeliveryDate;
        $this->LastUpdateDate = (string) $order->LastUpdateDate; // 2/2/2016 to fixed Pending order send duplicate stockmovement

        $this->Address = new AddressInTheOrder($order->ShippingAddress);

        //Set Ordered items (products)
        if (isset($Items->OrderItems->OrderItem) && !empty($Items->OrderItems->OrderItem)) {
            $this->Items = array();
            foreach ($Items->OrderItems->OrderItem as $key => $item) {
                $OrderItemId = (string) $item->OrderItemId;
                $this->Items[$OrderItemId] = new OrderedItem($item);
            }
        } else {
            //file_put_contents(USERDATA_PATH.$this->user.'/'.(string)$order->AmazonOrderId.'_PlacedOrder.txt', print_r(array('order'=>$order, 'items'=>$Items), true));
        }
    }
}

class AddressInTheOrder {

    public $Name;
    public $AddressLine1;
    public $AddressLine2;
    public $City;
    public $StateOrRegion;
    public $PostalCode;
    public $CountryCode;
    public $Phone;

    public function __construct(SimpleXMLElement $Address) {
        $this->Name = (string) $Address->Name;
        $this->AddressLine1 = (string) $Address->AddressLine1;
        $this->AddressLine2 = (string) $Address->AddressLine2;
        $this->City = (string) $Address->City;
        $this->StateOrRegion = (string) $Address->StateOrRegion;
        $this->PostalCode = (string) $Address->PostalCode;
        $this->CountryCode = (string) $Address->CountryCode;
        $this->Phone = (string) $Address->Phone;
    }
}

class OrderedItem {

    public $ASIN; //The Amazon Standard Identification Number (ASIN) of the item.
    public $SKU;
    public $Title; //Title of the Item
    public $QuantityOrdered; //Number of products of this type we need to ship
    public $ItemPriceCurrency;
    public $ItemPriceAmount;
    public $ShippingPriceCurrency;
    public $ShippingPriceAmount;
    public $ShippingDiscount;
    public $PromotionId;
    public $PromotionDiscount;
    public $QuantityShipped;
    public $GiftMessageText;
    public $GiftWrapPrice;
    public $TaxesInformation;

    public function __construct(SimpleXMLElement $Item) {
        $this->ASIN = (string) $Item->ASIN;
        $this->SKU = (string) $Item->SellerSKU;
        $this->Title = (string) $Item->Title;
        $this->QuantityOrdered = (string) $Item->QuantityOrdered;
        $this->QuantityShipped = (string) $Item->QuantityShipped;
        $this->ItemPriceCurrency = (string) $Item->ItemPrice->CurrencyCode;
        $this->ItemPriceAmount = (string) $Item->ItemPrice->Amount;
        $this->ShippingPriceCurrency = (string) $Item->ShippingPrice->CurrencyCode;
        $this->ShippingPriceAmount = (string) $Item->ShippingPrice->Amount;

        if (isset($Item->ShippingDiscount->Amount) && floatval($Item->ShippingDiscount->Amount))
            $this->ShippingDiscount = (string) $Item->ShippingDiscount->Amount;
        else
            $this->ShippingDiscount = null;

        if (isset($Item->PromotionIds->PromotionId))
            $this->PromotionId = (string) $Item->PromotionIds->PromotionId;
        else
            $this->PromotionId = null;

        if (isset($Item->PromotionDiscount->Amount) && floatval($Item->PromotionDiscount->Amount))
            $this->PromotionDiscount = (string) $Item->PromotionDiscount->Amount;
        else
            $this->PromotionDiscount = null;

        if (isset($Item->GiftMessageText))
            $this->GiftMessageText = (string) $Item->GiftMessageText;
        else
            $this->GiftMessageText = null;

        if (isset($Item->GiftWrapPrice->Amount) && floatval($Item->GiftWrapPrice->Amount))
            $this->GiftWrapPrice = (string) $Item->GiftWrapPrice->Amount;
        else
            $this->GiftWrapPrice = null;

        $this->TaxesInformation = new InfoTaxesItems($Item->ItemTax->CurrencyCode, $Item->ItemTax->Amount, $Item->ShippingTax->CurrencyCode, $Item->ShippingTax->Amount, $Item->GiftWrapTax->CurrencyCode, $Item->GiftWrapTax->Amount, false);
    }
}

class InfoTaxesItems {

    public $ItemTaxCurrencyCode;
    public $ItemTaxAmount;
    public $ShippingTaxCurrencyCode;
    public $ShippingTaxAmount;
    public $GiftWrapTaxCurrencyCode;
    public $GiftWrapTaxAmount;
    public $_debug;
    public $_cr;

    public function __construct($itcc, $ita, $stcc, $sta, $gwtcc, $gwta, $debug = false) {
        if ($debug === true)
            $this->_debug = true;
        else
            $this->_debug = false;

        $this->_cr = "<br>";

        if ($this->_debug)
            printf("$this->_cr __construct starts to create instance of the Taxes class. $this->_cr");

        $this->ItemTaxCurrencyCode = (string) $itcc;
        $this->ItemTaxAmount = (string) $ita;
        $this->ShippingTaxCurrencyCode = (string) $stcc;
        $this->ShippingTaxAmount = (string) $sta;
        $this->GiftWrapTaxCurrencyCode = (string) $gwtcc;
        $this->GiftWrapTaxAmount = (string) $gwta;

        if ($this->_debug) {
            printf("$this->_cr Created object: $this->_cr");
            printf("$this->_cr Construct function finishes here $this->_cr");
        }
    }
}