<?php

require_once(dirname(__FILE__).'/amazon.sqs.php');

class AmazonRepricing {
    
    const SUBSCRIBE = 1;
    const CANCEL = 2;
    const CHECK = 3;
    const PURGE = 4;
        
    const REPRICING_WHOLESALE_PRICE = 1;
    const REPRICING_REGULAR_PRICE = 2;

    const INPUT_QUEUE = 1;
    const OUTPUT_QUEUE = 2;

    const MAX_RETRIEVE_MESSAGES = 100;
    const MAX_MESSAGES_AT_ONCE = 10;
    const LESS_MESSAGES_AT_ONCE = 10;
    const MAX_EMPTY_LOOPS = 5;
    const LENGTH_QUEUE_NAME = 40; // QueueName : Maximum 80 characters
    const RECIEVE_MESSAGE_TIME = 1200; // 20 min

    const MESSAGE_VISIBILITY_TIMEOUT = 60;

    static $queue_prefix = null;
    static $input_queue_name = null;
    static $output_queue_name = null;
    
    protected $params = null;
    protected $debug = null;
    
    public function __construct($params, $debug = false) {  
        $this->params = $params;
        $this->debug = $debug;
    }    
    
    public function setQueueName() {
        
        $queue_prefix = isset($this->params->shopname) ? sprintf('Feedbiz-%s', mb_substr($this->params->shopname, 0, AmazonRepricing::LENGTH_QUEUE_NAME)) : 'FeedBiz-Amazon';
        self::$queue_prefix = $queue_prefix;
        self::$input_queue_name = sprintf('%s-In', $queue_prefix);
        self::$output_queue_name = sprintf('%s-Out', $queue_prefix);
    }
    
    public function getQueueName($region, $type) {
        
        $this->setQueueName();

        switch ($type) {
            case self::INPUT_QUEUE:
                return(self::$input_queue_name . '-' . $region);
            case self::OUTPUT_QUEUE:
                return(self::$output_queue_name . '-' . $region);
        }
        return(null);
    }
    
    public function listQueues($awsKeyId, $awsSecretKey) {           
        $this->setQueueName();
        $sqs = new AmazonSQS($awsKeyId, $awsSecretKey);
        return $sqs->listQueues(self::$queue_prefix);         
    }
    
    public function countMessages($sqs, $queue)
    {
        $queue_info = $sqs->getQueueAttributes($queue, 'ApproximateNumberOfMessages');
        
        if (!(is_array($queue_info) && array_key_exists('RequestId', $queue_info) && array_key_exists('Attributes', $queue_info) && preg_match('/([a-z0-9]*-){4,}/', $queue_info['RequestId']))) {
            if ($this->debug)  {
                printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                var_dump($queue_info);
            }
                    
            return false;            
        }

        return((int)$queue_info['Attributes']['ApproximateNumberOfMessages']);
    }

    public function createQueues($awsKeyId, $awsSecretKey, $region, $existingQueues = array()) {
        
        $toCreate = array(
            self::INPUT_QUEUE => $this->getQueueName($region, self::INPUT_QUEUE),
            self::OUTPUT_QUEUE => $this->getQueueName($region, self::OUTPUT_QUEUE)
        );

        $queueList = array();

        if (is_array($existingQueues) && count($existingQueues)) {
            $queues = $existingQueues;
        } else {
            if (($queues = $this->listQueues($awsKeyId, $awsSecretKey)) === false)
                return(false);
        }
        
        // Exclude Existing Queues
        if(isset($queues['Queues'])) {
            foreach ($queues['Queues'] as $queue_url) {

                $url = trim(dirname(dirname($queue_url)));
                $queue = trim(basename($queue_url));

                if (strpos(AmazonSQS::ENDPOINT_US_EAST, $url) === false)
                    continue;

                if ($queue == $this->getQueueName($region, self::INPUT_QUEUE)) {

                    unset($toCreate[self::INPUT_QUEUE]);
                    $queueList[self::INPUT_QUEUE] = $queue_url;

                } elseif ($queue == $this->getQueueName($region, self::OUTPUT_QUEUE)) {

                    unset($toCreate[self::OUTPUT_QUEUE]);
                    $queueList[self::OUTPUT_QUEUE] = $queue_url;
                }
            }
        }

        if (count($toCreate)) {
            
            $sqs = new AmazonSQS($awsKeyId, $awsSecretKey);

            foreach ($toCreate as $queue_id => $queue) {
                
                $result = $sqs->createQueue($queue);

                if (!(is_array($result) && array_key_exists('RequestId', $result) && preg_match('/([a-z0-9]*-){4,}/', $result['RequestId']))) {
                    
                    if ($this->debug) {
                            printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                            var_dump($result);
                    }

                    return(false);
                }

                $queueList[$queue_id] = $result['QueueUrl'];

                $attributes = array();

                switch ($queue_id) {
                    
                    case self::INPUT_QUEUE:
                        $attributes['ReceiveMessageWaitTimeSeconds'] = 2;
                        $attributes['MessageRetentionPeriod'] = 3600;
                        break;
                    
                    case self::OUTPUT_QUEUE:
                        $attributes['MessageRetentionPeriod'] = 14400;
                        break;
                    
                }

                if (count($attributes)) {
                    $result = $sqs->setQueueAttributes($result['QueueUrl'], $attributes);

                    if (!(is_array($result) && array_key_exists('RequestId', $result) && preg_match('/([a-z0-9]*-){4,}/', $result['RequestId']))) {
                        if ($this->debug)  {
                            printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                            var_dump($result);
                        }
                    }
                }
                
            }
        }

        return($queueList);
    }    

    public function listRegisteredDestinations($amazonApi) {
        
        $registered_destinations = $amazonApi->ListRegisteredDestinations();
        $registered = array();

        if ($this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::ListRegisteredDestinations: %s', basename(__FILE__), __LINE__, nl2br(print_r($registered_destinations)));
            echo "</pre>\n";
        }

        if ($registered_destinations instanceof SimpleXMLElement) {
            $registered_destinations->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Subscriptions/2013-07-01');

            $xpath_result = $registered_destinations->xpath('//xmlns:DestinationList/xmlns:member/xmlns:AttributeList/xmlns:member');

            if ($this->debug) {
                echo "<pre>\n";
                printf('%s(#%d): AmazonRepricing::ListRegisteredDestinations - Queues Found: %s', basename(__FILE__), __LINE__, nl2br(print_r($xpath_result, true)));
                echo "</pre>\n";
            }

            if (is_array($xpath_result) && count($xpath_result)) {
                foreach ($xpath_result as $queue) {
                    if (!$queue instanceof SimpleXMLElement)
                        continue;
                    if (!property_exists($queue, 'Value'))
                        continue;

                    $queue_name = trim(basename((string) $queue->Value));
                    $registered[$queue_name] = (string) $queue->Value;
                }
                if ($this->debug) {
                    echo "<pre>\n";
                    printf('%s(#%d): Amazon - Queue Registered Found: %s', basename(__FILE__), __LINE__, nl2br(print_r($registered, true)));
                    echo "</pre>\n";
                }
            }
        }
        return($registered);
    }

    public function RegisterDestination($amazonApi, $target_queue) {
        
        if ($this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::RegisterDestination - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $register_result = $amazonApi->RegisterDestination($target_queue);

        if ($this->debug) {
            echo "<pre>\n";
            echo $register_result->asXML();
            echo "</pre>\n";
        }

        if (isset($register_result->ResponseMetadata->RequestId) && isset($register_result->RegisterDestinationResult))
            return(true);
        
        return(false);
    }
    
    public function deregisterDestination($amazonApi, $target_queue)
    {
        if ($this->debug)
        {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::DeregisterDestination - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $unregister_result = $amazonApi->DeregisterDestination($target_queue);

        if ($this->debug)
        {
            echo "<pre>\n";
            echo $unregister_result->asXML();
            echo "</pre>\n";
        }

        if (isset($unregister_result->ResponseMetadata->RequestId) && isset($unregister_result->DeregisterDestinationResult))
                return(true);
        
        return(false);
    }

    public function testQueue($amazonApi, $target_queue)
    {
        if ($this->debug)
        {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::testQueue - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $notification_result = $amazonApi->SendTestNotificationToDestination($target_queue);

        if ($this->debug)
        {
            echo "<pre>\n";
            echo $notification_result->asXML();
            echo "</pre>\n";
        }

        return($notification_result);
    }
    
    public function checkSubscription($amazonApi, $target_queue)
    {
        if ($this->debug)
        {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::checkSubscription - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $subscription_result = $amazonApi->GetSubscription($target_queue);

        if ($this->debug)
        {
            echo "<pre>\n";
            echo $subscription_result->asXML();
            echo "</pre>\n";
        }

        $subscription_result->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Subscriptions/2013-07-01');

        $xpath_result = $subscription_result->xpath('//xmlns:IsEnabled/text()');

        if (is_array($xpath_result))
        {
            $isEnabled = reset($xpath_result);

            if ((string)$isEnabled == 'true')
                    return(true);
        }
        return(false);
    }    
    
    public function createSubscription($amazonApi, $target_queue) 
    {
        if ($this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::createSubscription - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $subscription_result = $amazonApi->CreateSubscription($target_queue);

        if ($this->debug) {
            echo "<pre>\n";
            echo $subscription_result->asXML();
            echo "</pre>\n";
        }
        $subscription_result->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Subscriptions/2013-07-01');

        $xpath_result = $subscription_result->xpath('//xmlns:ResponseMetadata/xmlns:RequestId');

        if (is_array($xpath_result) && count($xpath_result))
            return(true);

        return(false);
    }

    public function deleteSubscription($amazonApi, $target_queue) 
    {
        if ($this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): AmazonRepricing::deleteSubscription - Target Queue: %s', basename(__FILE__), __LINE__, $target_queue);
            echo "</pre>\n";
        }
        $subscription_result = $amazonApi->DeleteSubscription($target_queue);

        if ($this->debug) {
            echo "<pre>\n";
            echo $subscription_result->asXML();
            echo "</pre>\n";
        }
        $subscription_result->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Subscriptions/2013-07-01');

        $xpath_result = $subscription_result->xpath('//xmlns:ResponseMetadata/xmlns:RequestId');

        if (is_array($xpath_result) && count($xpath_result))
            return(true);

        return(false);
    }

    public function setQueuePermission($awsKeyId, $awsSecretKey, $queue, $awsAccountId, $permission)
    {
        $permissions = array();
        $permissions[$awsAccountId] = $permission;

        $sqs = new AmazonSQS($awsKeyId, $awsSecretKey);
        $permission_result = $sqs->addPermission($queue, 'Prestashop Repricing Queue - ' . $permission, $permissions);

        if (!(is_array($permission_result) && array_key_exists('RequestId', $permission_result) && preg_match('/([a-z0-9]*-){4,}/', $permission_result['RequestId'])))
        {
            if ($this->debug)
            {
                printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                var_dump($permission_result);
            }

            return(false);
        }
        return(true);
    }    
    
    public function RetrieveMessages($awsKeyId, $awsSecretKey, $queue, $script_start_time, $max_execution_time, $verbose = false, $last_execution_time = null) {
        
        $messages_set = array();

        $sqs = new AmazonSQS($awsKeyId, $awsSecretKey);

        if ($verbose) {
            echo "<pre>\n";
            echo str_repeat('-', 160) . "\n";
            echo "Ready to receive messages from queue:" . $queue;
            echo "</pre>\n";
        }

        $nMessages = $this->countMessages($sqs, $queue);
        
        if(isset($last_execution_time)){
            
            $RetrieveTime = self::RECIEVE_MESSAGE_TIME; // 20 min
            $LastTime = strtotime($last_execution_time); 
            $Elapsed = time() - $LastTime;
            
            if ($nMessages <= self::LESS_MESSAGES_AT_ONCE && $Elapsed <= $RetrieveTime) { // if message less then 10 & last time less then 20 minute skiped.
                file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt', print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'queue'=>$queue,'last_execution_time'=>$last_execution_time,'nMessages'=>$nMessages,'Elapsed'=>$Elapsed), true), FILE_APPEND);
                return(false);
            }
        }
        
        $nMessagesToRetrieve = min($nMessages, self::MAX_RETRIEVE_MESSAGES);

        if (!$nMessagesToRetrieve) {
            if ($verbose) {
                echo "<pre>\n";
                echo "No pending messages in the queue, exiting";
                echo "</pre>\n";
            }
            return(false);
        }

        $loop_start_time = microtime(true);

        $supposedLoops = ceil($nMessagesToRetrieve / self::MAX_MESSAGES_AT_ONCE);
        $emptyLoops = 0;
        $effectiveLoops = 0;
        $messagesCount = 0;
        $fetch = true;
        $i = 0;

        while ($fetch) {
            $effectiveLoops++;

            if ($verbose) {
                echo "<pre>\n";
                printf('Loop: %d on %d supposed loops with %d empty loops, messages: %d on %d expected' . "\n", $effectiveLoops, $supposedLoops, $emptyLoops, $messagesCount, $nMessagesToRetrieve);
                echo "</pre>\n";
            }

            $messages = $sqs->receiveMessage($queue, self::MAX_MESSAGES_AT_ONCE, self::MESSAGE_VISIBILITY_TIMEOUT);

            $loop_average = (microtime(true) - $loop_start_time) / ($i + 1);
            $total_elapsed = microtime(true) - $script_start_time;
            $max_estimated = (($loop_start_time - $script_start_time) + $loop_average * $i * 1.4);

            if ($verbose) {
                echo "<pre>\n";
                printf('Loop average: %.02f, Max estimated: %.02f, Total Elapsed: %.02f' . "\n", $loop_average, $max_estimated, $total_elapsed);
                echo "</pre>\n";
            }

            if ($max_execution_time && ($max_estimated >= $max_execution_time || $total_elapsed >= $max_execution_time)) {
                if ($verbose) {
                    echo "<pre>\n";
                    printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__, 'Warning: time allowed is about to be reached, loop aborted', $max_execution_time, $max_estimated, $total_elapsed);
                    echo "</pre>\n";                   
                    return($messages_set);
                }
            }

            if (is_array($messages) && array_key_exists('Messages', $messages) && is_array($messages['Messages']) && count($messages['Messages'])) {
                $messagesCount += $messageCount = count($messages['Messages']);

                if (!$messageCount)
                    $emptyLoops++;

                $messages_set[] = $messages['Messages'];

                if ($verbose) {
                    echo "<pre>\n";
                    echo str_repeat('-', 160) . "\n";
                    echo 'Messages received:' . $messageCount . ' on ' . $messagesCount . '/' . $nMessagesToRetrieve;
                    echo "</pre>\n";
                }
            } elseif ($verbose) {
                echo "<pre>\n";
                echo "ERROR: Failed to retrieve message";
                echo "</pre>\n";
                var_dump($messages);
            }

            if ($messagesCount >= $nMessagesToRetrieve) {
                 file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'queue'=>$queue,'messagesCount'=>$messagesCount, 'nMessagesToRetrieve'=>$nMessagesToRetrieve), true), FILE_APPEND);
                break;
            }

            if ($messagesCount < $nMessagesToRetrieve && $emptyLoops > self::MAX_EMPTY_LOOPS) {
                if ($verbose) {
                    echo "<pre>\n";
                    echo "WARNING: Not all message have been retrieved, MAX_EMPTY_LOOPS reached";
                    echo "</pre>\n";
                }
                $fetch = false;
                file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'queue'=>$queue,'fetch'=>$fetch), true), FILE_APPEND);
            }
        }
        return($messages_set);
    }
}