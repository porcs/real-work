<?php

require_once "CurlRequest.php";

class AmazonAdvertising
{
    public $config = array(
            "clientId" => null,
            "clientSecret" => null,
            "region" => null,
            "accessToken" => null,
            "refreshToken" => null,
            "sandbox" => false
        );
    
    private $redirectUri = 'amazon_handle/login';
    private $endpoints = array(
        "na" => array(
            "prod"     => "advertising-api.amazon.com",
            "sandbox"  => "advertising-api-test.amazon.com",
            "tokenUrl" => "api.amazon.com/auth/o2/token"),
        "eu" => array(
            "prod"     => "advertising-api-eu.amazon.com",
            "sandbox"  => "advertising-api-test.amazon.com",
            "tokenUrl" => "api.amazon.com/auth/o2/token"
        )
    );
    private $endpoint = null;
    private $tokenUrl = null;
    private $requestId = null;
    private $apiVersion = "v1";
    private $userAgent = "AdvertisingAPI PHP Client Library v1.0";
    public $profileId;
    public $message;
    
    public function __construct($config=null)
    {
        if(isset($config) && !empty($config)){
            if (isset($config["profileId"]) && !empty($config["profileId"])) {
                $this->profileId = $config["profileId"];
                unset($config["profileId"]);
            }
            $this->_validateConfig($config);
            $this->_validateConfigParameters();
            $this->_setEndpoints();
            if (is_null($this->config["accessToken"]) && !is_null($this->config["refreshToken"])) {
//                $this->doRefreshToken();
            }
        }
    }
    public function doRefreshToken()
    {
        $headers = array(
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
            "User-Agent: {$this->userAgent}"
        );
        $refresh_token = rawurldecode($this->config["refreshToken"]);
        $params = array(
            "grant_type" => "refresh_token",
            "refresh_token" => $refresh_token,
            "client_id" => $this->config["clientId"],
            "client_secret" => $this->config["clientSecret"]);
        $data = "";
        foreach ($params as $k => $v) {
            $data .= "{$k}=".rawurlencode($v)."&";
        }
        $url = "https://{$this->tokenUrl}";
        $request = new CurlRequest();
        $request->setOption(CURLOPT_URL, $url);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
        $request->setOption(CURLOPT_POST, true);
        $request->setOption(CURLOPT_POSTFIELDS, rtrim($data, "&"));
        $response = $this->_executeRequest($request);
        $response_array = $response["response"];
        if (isset($response_array["access_token"]) && array_key_exists("access_token", $response_array)) {
            $this->config["accessToken"] = $response_array["access_token"];
        } else {
            $this->_logAndThrow("Unable to refresh token. 'access_token' not found in response. ". print_r($response, true));
        }
        return $response;
    }
    public function requestAccessToken($authorization_code, $host)
    {
        $headers = array(
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
            "User-Agent: {$this->userAgent}"
        );
        $params = array(
           "grant_type" => "authorization_code",
            "code" => rawurldecode($authorization_code),
            "redirect_uri" => rawurldecode($host.$this->redirectUri),
            "client_id" => $this->config["clientId"],
            "client_secret" => $this->config["clientSecret"]);
        $data = "";
        foreach ($params as $k => $v) {
            $data .= "{$k}=".rawurlencode($v)."&";
        }
        $url = "https://{$this->tokenUrl}";
        $request = new CurlRequest();
        $request->setOption(CURLOPT_URL, $url);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
        $request->setOption(CURLOPT_POST, true);
        $request->setOption(CURLOPT_POSTFIELDS, rtrim($data, "&"));
        $response = $this->_executeRequest($request);
        $response_array = $response["response"];
        if(isset($response_array)){
            if (array_key_exists("access_token", $response_array)) {
                $this->config["accessToken"] = $response_array["access_token"];
            }
            if (array_key_exists("refresh_token", $response_array)) {
                $this->config["refreshToken"] = $response_array["refresh_token"];
            }
            if (array_key_exists("token_type", $response_array)) {
                $this->config["tokenType"] = $response_array["token_type"];
            }
            if (array_key_exists("expires_in", $response_array)) {
                $this->config["expiresIin"] = $response_array["expires_in"];
            }
        }
        if(!isset($this->config["accessToken"]) || empty($this->config["accessToken"])) {
            $this->_logAndThrow("Unable to refresh token. 'access_token' not found in response. ". print_r($response, true));
            return (false);
        }        
        return (true);
    }
    public function getProfiles()
    {
        return $this->_operation("profiles");
    }
    public function registerProfile($data)
    {
        return $this->_operation("profiles/register", $data, "PUT");
    }
    public function registerProfileStatus($profileId)
    {
        return $this->_operation("profiles/register/{$profileId}/status");
    }
    public function getProfile($profileId)
    {        
        return $this->_operation("profiles/{$profileId}");
    }
    public function updateProfiles($data)
    {        
        return $this->_operation("profiles", $data, "PUT");
    }
    public function getCampaign($campaignId)
    {
        return array(
                    "success"=>true,
                    "code"=>200,
                    "response"=>
                    array(
                      "campaignId"=>57820821483919,
                      "name"=>"campaign 2",
                      "campaignType"=>"sponsoredProducts",
                      "targetingType"=>"manual",
                      "dailyBudget"=>1,
                      "startDate"=>"20160816",
                      "endDate"=>"20160831",
                      "state"=>"paused",
                    ),
                    "requestId"=>"0NTK8RW0H7PEBAH351G8"
                  );
        return $this->_operation("campaigns/{$campaignId}");
    }
    public function getCampaignEx($campaignId)
    {
        return $this->_operation("campaigns/extended/{$campaignId}");
    }
    public function createCampaigns($data)
    {
        return $this->_operation("campaigns", $data, "POST");
    }
    public function updateCampaigns($data)
    {
        return $this->_operation("campaigns", $data, "PUT");
    }
    public function archiveCampaign($campaignId)
    {
        return $this->_operation("campaigns/{$campaignId}", null, "DELETE");
    }
    public function listCampaigns($data = null)
    {
        return array(
                    "success"=>true,
                    "code"=>200,
                    "response"=>
                    array(
                      0=>
                      array(
                        'campaignId'=>52936459461681,
                        'campaignType'=>"sponsoredProducts",
                        'dailyBudget'=>1,
                        'endDate'=>"20160831",
                        'name'=>"Advertising Campaign 1",
                        'startDate'=>"20160816",
                        'state'=>"paused",
                        'targetingType'=>"manual",
                      ),
                      1=>
                      array(
                        'campaignId'=>57820821483919,
                        'campaignType'=>"sponsoredProducts",
                        'dailyBudget'=>1,
                        'endDate'=>"20160831",
                        'name'=>"campaign 2",
                        'startDate'=>"20160816",
                        'state'=>"paused",
                        'targetingType'=>"manual",
                      ),
                      2=>
                      array(
                        'campaignId'=>16791370897758,
                        'campaignType'=>"sponsoredProducts",
                        'dailyBudget'=>1,
                        'endDate'=>"20160831",
                        'name'=>"campaign 3",
                        'startDate'=>"20160816",
                        'state'=>"paused",
                        'targetingType'=>"manual",
                      ),
                      3=>
                      array(
                        'campaignId'=>61373432712798,
                        'campaignType'=>"sponsoredProducts",
                        'dailyBudget'=>1,
                        'endDate'=>"20160831",
                        'name'=>"campaign 4",
                        'startDate'=>"20160816",
                        'state'=>"paused",
                        'targetingType'=>"manual",
                      )
                    ),
                    "requestId"=>"07KNRMN5MWJYHFSEAV3X"
                  ); 
        return $this->_operation("campaigns", $data);
    }
    public function listCampaignsEx($data = null)
    {
        return $this->_operation("campaigns/extended", $data);
    }
    public function getAdGroup($adGroupId)
    {
        return $this->_operation("adGroups/{$adGroupId}");
    }
    public function getAdGroupEx($adGroupId)
    {
        return $this->_operation("adGroups/extended/{$adGroupId}");
    }
    public function createAdGroups($data)
    {
        return $this->_operation("adGroups", $data, "POST");
    }
    public function updateAdGroups($data)
    {       
        return $this->_operation("adGroups", $data, "PUT");
    }
    public function archiveAdGroup($adGroupId)
    {
        return $this->_operation("adGroups/{$adGroupId}", null, "DELETE");
    }
    public function listAdGroups($data = null)
    {
        return array(
                    "success"=>true,
                    "code"=>200,
                    "response"=> json_decode('[{"adGroupId":"210373127087553","name":"ad group 1","campaignId":"52936459461681","defaultBid":0.03,"state":"paused","campaignName":"Advertising Campaign 1"},{"adGroupId":"3764100938584","name":"ad group 2","campaignId":"57820821483919","defaultBid":0.02,"state":"paused","campaignName":"campaign 2"},{"adGroupId":"269770708184045","name":"Advertising group 3","campaignId":"52936459461681","defaultBid":0.02,"state":"paused","campaignName":"Advertising Campaign 1"}]', true),
                    "requestId"=>"07KNRMN5MWJYHFSEAV3X"
                  ); 
        return $this->_operation("adGroups", $data);
    }
    public function listAdGroupsEx($data = null)
    {
        return $this->_operation("adGroups/extended", $data);
    }
    public function getBiddableKeyword($keywordId)
    {
        return array("success" => true,
                    "code" => 200,
                    "response" => json_decode('{"keywordId":199937860393403,"adGroupId":210373127087553,"campaignId":52936459461681,"keywordText":"keyword text 1","matchType":"exact","state":"enabled"}', true),
                    "requestId" => '');
        return $this->_operation("keywords/{$keywordId}");
    }
    public function getBiddableKeywordEx($keywordId)
    {
        return $this->_operation("keywords/extended/{$keywordId}");
    }
    public function createBiddableKeywords($data)
    {
        return $this->_operation("keywords", $data, "POST");
    }
    public function updateBiddableKeywords($data)
    {
        return $this->_operation("keywords", $data, "PUT");
    }
    public function archiveBiddableKeyword($keywordId)
    {
        return $this->_operation("keywords/{$keywordId}", null, "DELETE");
    }
    public function listBiddableKeywords($data = null)
    {
        return array("success" => true,
                    "code" => 200,
                    "response" =>  json_decode('[{"keywordId":"199937860393403","adGroupId":"210373127087553","campaignId":"52936459461681","keywordText":"keyword text 1","matchType":"exact","state":"enabled","adGroupName":"ad group 1","campaignName":"Advertising Campaign 1"},{"keywordId":"36117949150007","adGroupId":"210373127087553","campaignId":"52936459461681","keywordText":"keyword text 2","matchType":"exact","state":"enabled","adGroupName":"ad group 1","campaignName":"Advertising Campaign 1"},{"keywordId":"130325773891587","adGroupId":"3764100938584","campaignId":"57820821483919","keywordText":"keyword text 3","matchType":"exact","state":"enabled","adGroupName":"ad group 2","campaignName":"campaign 2"},{"keywordId":"87677008061599","adGroupId":"269770708184045","campaignId":"52936459461681","keywordText":"keyword text 4","matchType":"exact","state":"enabled","adGroupName":"Advertising group 3","campaignName":"Advertising Campaign 1"},{"keywordId":"238837698090135","adGroupId":"269770708184045","campaignId":"52936459461681","keywordText":"keyword text 5","matchType":"exact","state":"enabled","adGroupName":"Advertising group 3","campaignName":"Advertising Campaign 1"},{"keywordId":"223290142317740","adGroupId":"269770708184045","campaignId":"52936459461681","keywordText":"keyword text 7","matchType":"exact","state":"enabled","adGroupName":"Advertising group 3","campaignName":"Advertising Campaign 1"},{"keywordId":"137580150150465","adGroupId":"269770708184045","campaignId":"52936459461681","keywordText":"keyword text 6","matchType":"exact","state":"enabled","adGroupName":"Advertising group 3","campaignName":"Advertising Campaign 1"},{"keywordId":"32572421290371","adGroupId":"210373127087553","campaignId":"52936459461681","keywordText":"keyword text 8","matchType":"exact","state":"enabled","adGroupName":"ad group 1","campaignName":"Advertising Campaign 1"},{"keywordId":"221383595431478","adGroupId":"210373127087553","campaignId":"52936459461681","keywordText":"keyword text 9","matchType":"exact","state":"enabled","adGroupName":"ad group 1","campaignName":"Advertising Campaign 1"},{"keywordId":"23433512113433","adGroupId":"210373127087553","campaignId":"52936459461681","keywordText":"keyword text 11","matchType":"exact","state":"enabled","adGroupName":"ad group 1","campaignName":"Advertising Campaign 1"}]', true),
                    "requestId" => '');
        return $this->_operation("keywords", $data);
    }
    public function listBiddableKeywordsEx($data = null)
    {
        return $this->_operation("keywords/extended", $data);
    }
    public function getNegativeKeyword($keywordId)
    {
        return $this->_operation("negativeKeywords/{$keywordId}");
    }
    public function getNegativeKeywordEx($keywordId)
    {
        return $this->_operation("negativeKeywords/extended/{$keywordId}");
    }
    public function createNegativeKeywords($data)
    {
        return $this->_operation("negativeKeywords", $data, "POST");
    }
    public function updateNegativeKeywords($data)
    {
        return $this->_operation("negativeKeywords", $data, "PUT");
    }
    public function archiveNegativeKeyword($keywordId)
    {
        return $this->_operation("negativeKeywords/{$keywordId}", null, "DELETE");
    }
    public function listNegativeKeywords($data = null)
    {
        return $this->_operation("negativeKeywords", $data);
    }
    public function listNegativeKeywordsEx($data = null)
    {
        return $this->_operation("negativeKeywords/extended", $data);
    }
    public function getCampaignNegativeKeyword($keywordId)
    {
        return $this->_operation("campaignNegativeKeywords/{$keywordId}");
    }
    public function getCampaignNegativeKeywordEx($keywordId)
    {
        return $this->_operation("campaignNegativeKeywords/extended/{$keywordId}");
    }
    public function createCampaignNegativeKeywords($data)
    {
        return $this->_operation("campaignNegativeKeywords", $data, "POST");
    }
    public function updateCampaignNegativeKeywords($data)
    {
        return $this->_operation("campaignNegativeKeywords", $data, "PUT");
    }
    public function removeCampaignNegativeKeyword($keywordId)
    {
        return $this->_operation("campaignNegativeKeywords/{$keywordId}", null, "DELETE");
    }
    public function listCampaignNegativeKeywords($data = null)
    {
        return $this->_operation("campaignNegativeKeywords", $data);
    }
    public function listCampaignNegativeKeywordsEx($data = null)
    {
        return $this->_operation("campaignNegativeKeywords/extended", $data);
    }
    public function getProductAd($productAdId)
    {
        return $this->_operation("productAds/{$productAdId}");
    }
    public function getProductAdEx($productAdId)
    {
        return $this->_operation("productAds/extended/{$productAdId}");
    }
    public function createProductAds($data)
    {
        return $this->_operation("productAds", $data, "POST");
    }
    public function updateProductAds($data)
    {
        return $this->_operation("productAds", $data, "PUT");
    }
    public function archiveProductAd($productAdId)
    {
        return $this->_operation("productAds/{$productAdId}", null, "DELETE");
    }
    public function listProductAds($data = null)
    {
        return $this->_operation("productAds", $data);
    }
    public function listProductAdsEx($data = null)
    {
        return $this->_operation("productAds/extended", $data);
    }
    public function requestSnapshot($recordType, $data = null)
    {
        return $this->_operation("{$recordType}/snapshot", $data, "POST");
    }
    public function getSnapshot($snapshotId)
    {
        $req = $this->_operation("snapshots/{$snapshotId}");
        if ($req["success"]) {
            $json = json_decode($req["response"], true);
            if ($json["status"] == "SUCCESS") {
                return $this->_download($json["location"]);
            }
        }
        return $req;
    }
    public function requestReport($recordType, $data = null)
    {
        return $this->_operation("{$recordType}/report", $data, "POST");
    }
    public function getReport($reportId)
    {
        $req = $this->_operation("reports/{$reportId}");
        if ($req["success"]) {
            $json = json_decode($req["response"], true);
            if ($json["status"] == "SUCCESS") {
                return $this->_download($json["location"]);
            }
        }
        return $req;
    }
    private function _download($location, $gunzip = false)
    {
        $headers = array();
        if (!$gunzip) {
            /* only send authorization header when not downloading actual file */
            array_push($headers, "Authorization: bearer {$this->config["accessToken"]}");
        }
        if (!is_null($this->profileId)) {
            array_push($headers, "Amazon-Advertising-API-Scope: {$this->profileId}");
        }
        $request = new CurlRequest();
        $request->setOption(CURLOPT_URL, $location);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
        if ($gunzip) {
            $response = $this->_executeRequest($request);
            $response["response"] = gzdecode($response["response"]);
            return $response;
        }
        return $this->_executeRequest($request);
    }
    private function _operation($interface, $params = array(), $method = "GET")
    {
        if(!isset($this->config['accessToken']) || empty($this->config['accessToken']))
            return (false);

        $headers = array(
            "Authorization: bearer {$this->config["accessToken"]}",
            "Content-Type: application/json",
            "User-Agent: {$this->userAgent}"
        );
        if (!is_null($this->profileId)) {
            array_push($headers, "Amazon-Advertising-API-Scope: {$this->profileId}");
        }
        $request = new CurlRequest();
        $url = "{$this->endpoint}/{$interface}";
        $this->requestId = null;
        $data = "";
        switch (strtolower($method)) {
            case "get":
                if (!empty($params)) {
                    $url .= "?";
                    foreach ($params as $k => $v) {
                        $url .= "{$k}=".rawurlencode($v)."&";
                    }
                    $url = rtrim($url, "&");
                }
                break;
            case "put":
            case "post":
            case "delete":
                if (!empty($params)) {
                    $data = json_encode($params);
                    $request->setOption(CURLOPT_POST, true);
                    $request->setOption(CURLOPT_POSTFIELDS, $data);
                }
                break;
            default:
                $this->_logAndThrow("Unknown verb {$method}.");
        }
        
        $request->setOption(CURLOPT_URL, $url);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
        $request->setOption(CURLOPT_CUSTOMREQUEST, strtoupper($method));
        //var_dump($url, $headers, $data, strtoupper($method));
        return $this->_executeRequest($request);
    }
    protected function _executeRequest($request)
    {
        $response = $request->execute();
        $this->requestId = $request->requestId;
        $response_info = $request->getInfo();
        $request->close();
        if ($response_info["http_code"] == 307) {
            /* application/octet-stream */
            return $this->_download($response_info["redirect_url"], true);
        }
        if (!preg_match("/^(2|3)\d{2}$/", $response_info["http_code"])) {
            $requestId = 0;
            $json = json_decode($response, true);
            if (!is_null($json)) {
                if (array_key_exists("requestId", $json)) {
                    $response_code = json_decode($response, true);
                    $requestId = $response_code["requestId"];
                }
            }
            return array("success" => false,
                    "code" => $response_info["http_code"],
                    "response" => json_decode($response, true),
                    "requestId" => $requestId);
        } else {          
            return array("success" => true,
                    "code" => $response_info["http_code"],
                    "response" => json_decode($response, true),
                    "requestId" => $this->requestId);
        }
    }
    private function _validateConfig($config)
    {
        if (is_null($config)) {
            $this->_logAndThrow("'config' cannot be null.");
        }
        foreach ($config as $k => $v) {
            if (array_key_exists($k, $this->config)) {
                $this->config[$k] = $v;
            } else {
                $this->_logAndThrow("Unknown parameter '{$k}' in config.");
            }
        }
        return true;
    }
    private function _validateConfigParameters()
    {
        foreach ($this->config as $k => $v) {
            if (is_null($v) && $k !== "accessToken" && $k !== "refreshToken") {
                $this->_logAndThrow("Missing required parameter '{$k}'.");
            }
            switch ($k) {
                case "clientId":
                    if (!preg_match("/^amzn1\.application-oa2-client\.[a-z0-9]{32}$/i", $v)) {
                        $this->_logAndThrow("Invalid parameter value for clientId.");
                    }
                    break;
                case "clientSecret":
                    if (!preg_match("/^[a-z0-9]{64}$/i", $v)) {
                        $this->_logAndThrow("Invalid parameter value for clientSecret.");
                    }
                    break;
                case "accessToken":
                    if (!is_null($v)) {
                        if (!preg_match("/^Atza(\||%7C|%7c).*$/", $v)) {
                            $this->_logAndThrow("Invalid parameter value for accessToken.");
                        }
                    }
                    break;
                case "refreshToken":
                    if (!is_null($v)) {
                        if (!preg_match("/^Atzr(\||%7C|%7c).*$/", $v)) {
                            $this->_logAndThrow("Invalid parameter value for refreshToken.");
                        }
                    }
                    break;
                case "sandbox":
                    if (!is_bool($v)) {
                        $this->_logAndThrow("Invalid parameter value for sandbox.");
                    }
                    break;
            }
        }
        return true;
    }
    private function _setEndpoints()
    {
        /* check if region exists and set api/token endpoints */
        if (array_key_exists(strtolower($this->config["region"]), $this->endpoints)) {
            $region_code = strtolower($this->config["region"]);
            if ($this->config["sandbox"]) {
                $this->endpoint = "https://{$this->endpoints[$region_code]["sandbox"]}/{$this->apiVersion}";
            } else {
                $this->endpoint = "https://{$this->endpoints[$region_code]["prod"]}/{$this->apiVersion}";
            }
            $this->tokenUrl = $this->endpoints[$region_code]["tokenUrl"];
        } else {
            $this->_logAndThrow("Invalid region.");
        }
        return true;
    }
    private function _logAndThrow($message, $action=null)
    {
        $this->message = $message;
        return $message;
        //error_log($message, 0);
        //throw new \Exception($message);
    }    
}