<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__) . '/../../FeedBiz.php');
require_once(dirname(__FILE__) . '/amazon.webservice.php');
require_once(dirname(__FILE__) . '/amazon.product.php');
require_once(dirname(__FILE__) . '/amazon.tools.php');
require_once(dirname(__FILE__) . '/amazon.error.resolution.php');

class AmazonDatabase {

    const MODE_SYNC = 'SYNCHRONIZE_ONLY';
    const MODE_MATCH = 'SYNCHRONIZE_MATCH';
    const MODE_SYNC_MATCH = 'SYNCHRONIZE_AND_MATCH';
    const SYNC = 'sync';
    const CREATE = 'create';
    const DELETE = 'delete';
    const SYNCHRONIZE = 'Update Offers';
    const CREATION = 'Product Creation';
    const FormatTitle = 1;
    const FormatManufacturerTitle = 2;
    const FormatManufacturerTitleReference = 3;
    const DELETE_OUT_OF_STOCK = 1;
    const DELETE_ALL_SENT = 2;
    const DELETE_INACTIVE = 3;
    const DELETE_DISABLED = 4;

    const REPRICING = 'repricing';
    const REPRICING_ANALYSIS = 'reprice';
    const REPRICING_EXPORT = 'export';
    const PRODUCT_STRATEGIES = 'amazon_repricing_product_strategies';
    
    public $db;
    public static $p_pref = '', $o_pref = '', $l_pref = '', $m_pref = '';

    public function __construct($user, $debug = false) {

        $this->db = new stdClass();
        $this->db->product = new Db($user, 'products');
        $this->db->offer = new Db($user, 'offers');
        $this->db->log = new Db($user, 'log');
        $this->feedbiz = new FeedBiz(array($user));
        $this->user = $user;
        $this->debug = $debug;
	
	//set table prefix for product
	if(isset($this->db->product->prefix_table) && !empty($this->db->product->prefix_table))
	    self::$p_pref = $this->db->product->prefix_table;	 
	
	//set table prefix for offer
	if(isset($this->db->offer->prefix_table) && !empty($this->db->offer->prefix_table))
	    self::$o_pref = $this->db->offer->prefix_table;

	//set table prefix for log
        self::$l_pref = 'log_';
	
	if(defined('_TABLE_PREFIX_')) self::$m_pref = _TABLE_PREFIX_;
    }

    public function getProductsFromFeedbizDatabase($user, $operationMode, $option = null) {
        
        $skipped = 0;
        $product_list = $products = array();

        // DELETE
        if ($operationMode == AmazonDatabase::DELETE) {
            $mode = AmazonDatabase::DELETE;
            if (isset($option->delete) && ($option->delete == AmazonDatabase::DELETE_INACTIVE || $option->delete == AmazonDatabase::DELETE_DISABLED)) {
                // DELETE CRON
                $mode = $option->delete;
            }
            $products = $this->exportProductListByMode($user, $mode);
            if (isset($products) && !empty($products)) {
                $export_product = new Amazon_Product($user, $this);
                $product_list = $export_product->export_product($products, $operationMode, $option);
            }
 
        } else {
            
            $export_product = new Amazon_Product($user, $this);
            
            if (isset($user->mode) && !empty($user->mode)) {
		
                // UPDATE
                $products = $this->exportProductListByMode($user, $user->mode, $option);
            } else {
		
                // CREATE  
                $products = $this->exportProductListAmazon($user->id_country, $user->id_shop, $user->id_mode, $user->iso_code, $option);
            }    
        
            if (isset($products) && !empty($products)) {
                
                if(isset($user->update_type)){
                    $option->update_type = $user->update_type;
                }
                
                $product_list = $export_product->export_product($products, $operationMode, $option/*repricing*/);
                //skipped 
                $product_list['skipped'] = $product_list['skipped'] + $skipped;
            }
        }
        return $product_list;
    }

    public function updateProductFlag($id_shop, $id_country, $id_product, $update_type = 'update_product', $flag = 1, $id_product_attribute=null){

        $sql = "REPLACE INTO ".self::$m_pref."amazon_update_flag ( "
                   ."id_product".(isset($id_product_attribute) ? " ,id_product_attribute" : "")
                   .", id_shop, id_country, update_type, flag, date_upd ) "
                   ." VALUES (".(int)$id_product.(isset($id_product_attribute) ? " ,".(int)$id_product_attribute : "")
                   .",".$id_shop.",'".$id_country."','$update_type', $flag,'".date('Y-m-d H:i:s')."' ) ;";

        return $sql;

    }

    //Get Matched products to sync
    public function updateMatchProductToSync($reference, $id_country, $id_shop, $action = 'confirm', $id_product = null, $id_product_attribute = null) {

        if (empty($reference) || empty($id_country) || empty($id_shop)) {
            return null;
        }

        $attr = $sql = $product_attribute = '';
        $date_add = date('Y-m-d H:i:s');
        $asin = null;

        if (!isset($id_product) || empty($id_product)) {
            $product = $this->getProductBySKU($id_shop, $reference);
            $id_product = isset($product['id_product']) && !empty($product['id_product']) ? $product['id_product'] : null;
            $id_product_attribute = isset($product['id_product_attribute']) && !empty($product['id_product_attribute']) ? $product['id_product_attribute'] : null;
        }
        if (isset($id_product_attribute) && !empty($id_product_attribute)) {
            $attr = ', id_product_attribute';
            $product_attribute = ',' . (int)$id_product_attribute;
        }

        if(isset($id_product) && !empty($id_product)){
            // DELETE amazon_submission_result_log
            if($action == 'send') {
                $sql .= "UPDATE ".self::$m_pref."amazon_submission_result_log SET result_code = '".AmazonErrorResolution::_RESOLVED."' "
                      . "WHERE sku = '$reference' AND id_shop = $id_shop AND id_country = $id_country ; ";

                $sql .= $this->updateProductFlag($id_shop, $id_country, $id_product, 'update_product', '0', $id_product_attribute);

                // Update to log_product_update_log to avoid to send a lot of update number ; 
                /*$product_update_type = 'solved';
                $product_update_status = 'success'; //AmazonErrorResolution::_RESOLVED;
                $sql .= " REPLACE INTO log_product_update_log ( "
                        ."id_product"
                        .(isset($id_product_attribute) ? " ,id_product_attribute" : "")
                        .", id_shop, product_update_type, product_update_status, date_add ) "
                        ." VALUES (".(int)$id_product.(isset($id_product_attribute) ? " ,".(int)$id_product_attribute : "")
                        .",".$id_shop.",'".$product_update_type."','".$product_update_status."','".date('Y-m-d H:i:s')."' ) ;";*/

            }
            if($action == 'delete'){
                $sql .= "DELETE FROM ".self::$m_pref."amazon_report_inventory WHERE id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country." AND sku = '".$reference."';";
            }
            $sql .= "REPLACE INTO ".self::$m_pref."amazon_inventory (id_product" . $attr . ",id_shop,id_country,sku,asin,action_type,date_add) "
                  . "VALUES (".(int)$id_product.$product_attribute.",".(int)$id_shop.",".(int)$id_country.",'".$reference."','".$asin."','".$action."','".$date_add."'); ";
        }
        return $sql;
    }

    //Get Matched products to sync
    public function exportProductListByMode($user, $mode = null, $option = null) {
        
        $product_list = array();
        $sql = '';
	
        switch ($mode) {
            case AmazonDatabase::MODE_SYNC: //synchronize only //product in amazon_report_inventory only
                if (isset($user->update_type) && $user->update_type == 'all') {
                    $case = $this->_case_check_report_inventory($user, false, true);
                } else {
                    $case = $this->_case_check_report_inventory($user, false, false);
                }
                $sql = "SELECT p.id_product AS id_product, pa.id_product_attribute AS id_product_attribute, acsw.id_category AS id_category,
                        CASE
                            WHEN pa.reference IS NOT NULL THEN pa.reference 
                            WHEN p.reference IS NOT NULL THEN p.reference                                                      
                        END AS sku
                        FROM ".self::$p_pref."product p
                        LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                        LEFT JOIN ".self::$m_pref."amazon_category_selected acsw ON p.id_category_default = acsw.id_category AND p.id_shop = acsw.id_shop 
                        WHERE p.id_shop = " . $user->id_shop . " AND acsw.id_country = " . $user->id_country . " AND acsw.id_shop = " . $user->id_shop . "  
                        $case
                        ORDER BY p.id_product, pa.id_product_attribute ASC ;";
                
                break;
            case AmazonDatabase::MODE_MATCH: // match only
            case AmazonDatabase::MODE_SYNC_MATCH: // synchronize and match //product in amazon_inventory action_type = 'confirm'
                $sql = "SELECT p.id_product AS id_product, 
                            ai.id_product_attribute AS id_product_attribute, 
                            ai.sku AS sku, 
                            ai.asin AS asin,
                            acsw.id_category AS id_category, 
                            acsw.id_profile AS id_profile_price
                        FROM ".self::$m_pref."amazon_inventory ai
                        LEFT JOIN ".self::$p_pref."product p ON p.id_product = ai.id_product AND p.id_shop = ai.id_shop
                        LEFT JOIN ".self::$m_pref."amazon_category_selected acsw ON p.id_category_default = acsw.id_category AND p.id_shop = acsw.id_shop
                        WHERE ai.id_country = " . $user->id_country . "  
                        AND ai.id_shop = " . $user->id_shop . " 
                        AND acsw.id_mode = " . $user->id_mode . " 
                        AND ai.action_type = 'confirm' 
                        GROUP BY ai.sku ; ";
                break;
            case AmazonDatabase::SYNC: //action send product offer // cron // to update offer
                $date_rage = date('Y-m-d', strtotime($user->time));
                $list_id_product = $list_id_product_attribute = '';

                if (isset($option->cron) && !empty($option->cron) && $option->cron && isset($user->id_marketplace) && isset($user->update_type)) {
                    $id_product = array();

                    if ($user->update_type == 'offer') {

                        $sql_offer = 'SELECT p.id_product as id_product FROM '.self::$o_pref.'product p 
                         LEFT JOIN '.self::$o_pref.'flag_update fu ON p.id_product = fu.id_product 
                         AND fu.id_site = "'.$user->ext.'" AND p.id_shop = fu.id_shop 
                         AND fu.id_marketplace = ' . $user->id_marketplace . ' 
                         WHERE (fu.flag IS NULL OR fu.flag = 1) AND p.id_shop = ' . $user->id_shop . ' ORDER BY p.id_product ;';

                    } else if($user->update_type == 'update_product_flag') {

                        $sql_offer = 'SELECT id_product '
                            . 'FROM '.self::$m_pref.'amazon_update_flag '
                            . 'WHERE id_shop = ' . $user->id_shop . ' AND id_country = '. $user->id_country .' AND flag = 1 AND update_type = "update_product" ; ';

                    } else {                        
                        $sql_offer = 'SELECT p.id_product as id_product '
                                . 'FROM '.self::$o_pref.'product p '
                                . 'WHERE p.id_shop = ' . $user->id_shop . ' AND date_add LIKE "%' . $date_rage . '%" ';
                        // union
                         $sql_offer .= 'UNION SELECT id_product '
                                . 'FROM '.self::$l_pref.'product_update_log '
                                . 'WHERE id_shop = ' . $user->id_shop . ' AND product_update_type = "solved" AND product_update_status = "'.AmazonErrorResolution::_RESOLVED.'" '
                                . 'AND date_add LIKE "%' . $date_rage . '%" ';
                    }
                    
                    $result_offer = $this->db->offer->db_query_str($sql_offer);
                    foreach ($result_offer as $offer) {
                        if (isset($offer['id_product']) && !in_array($offer['id_product'], $id_product, true)) {
                            array_push($id_product, $offer['id_product']);
                        }
                    }
                   
                    if ($user->update_type != 'all') {
                        $list_id_product = ' AND p.id_product IN (' . implode(',', $id_product) . ') ';
                    }
                }
                
                /*Get sent inventory*/
                $sql_get_inventory = "SELECT sku "
                        . "FROM ".self::$m_pref."amazon_report_inventory "
                        . "WHERE id_country = $user->id_country AND id_shop = $user->id_shop "
                        . "UNION SELECT sku FROM ".self::$m_pref."amazon_inventory "
                        . "WHERE id_country = $user->id_country AND id_shop = $user->id_shop  AND action_type != 'delete'";
                
                $result_inventory = $this->db->offer->db_query_str($sql_get_inventory);
                $list_inventory = array();
                foreach ($result_inventory as $inventory){
                    $list_inventory[] = $inventory['sku'];
                }
                
                $sql = "SELECT p.id_product AS id_product, pa.id_product_attribute AS id_product_attribute, acsw.id_category AS id_category,
                        CASE
                            WHEN pa.reference IS NOT NULL THEN pa.reference                          
                            WHEN p.reference IS NOT NULL THEN p.reference                             
                        END AS sku
                        FROM ".self::$p_pref."product p
                        LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                        LEFT JOIN ".self::$m_pref."amazon_category_selected acsw ON p.id_category_default = acsw.id_category AND p.id_shop = acsw.id_shop 
                        WHERE p.id_shop = $user->id_shop  AND acsw.id_country = $user->id_country AND acsw.id_shop = $user->id_shop  $list_id_product
                        AND ( CASE  
                        WHEN pa.reference IS NOT NULL THEN pa.reference IN ('".  implode("','", $list_inventory)."') 
                        WHEN p.reference IS NOT NULL THEN p.reference IN ('".  implode("','", $list_inventory)."') 
                        END ) ORDER BY p.id_product, pa.id_product_attribute ASC ; ";

                break;
            case AmazonDatabase::DELETE:
                $sql = "SELECT ai.id_product AS id_product, ai.id_product_attribute AS id_product_attribute, ai.sku AS sku
                        FROM ".self::$m_pref."amazon_inventory ai
                        WHERE ai.id_country = " . $user->id_country . "  
                        AND ai.id_shop = " . $user->id_shop . "
                        AND ai.action_type IN ('send','error') ; ";
                break;
            case AmazonDatabase::DELETE_INACTIVE: // Delete inactive product

                $where_additional = array();
                $where_additional[0]['field'] = 'action_type';
                $where_additional[0]['operation'] = 'IN';
                $where_additional[0]['value'] = '("send", "error")';
                $case = $this->_case_check_report_inventory($user, false, true, true, false, $where_additional);

                ////Inactive products////
                //1. get id_product from Products
                $sql1 = "SELECT
                        p.id_product AS id_product,
                        p.reference AS reference
                        FROM ".self::$p_pref."product p 
                        WHERE p.id_shop = " . $user->id_shop . " AND p.active = 0 AND ( p.date_upd != '' OR  p.date_upd IS NOT NULL) ";
                $result1 = $this->db->product->db_query_str($sql1);
                $list_id_product = $list_reference = array();
                foreach ($result1 as $product){
                    $list_id_product[$product['reference']] = $product['id_product'];
                    $list_reference[] = $product['reference'];
                }
               
                // 2. check if the product have other id_product which activate
                // : do not delete (we will use ProductManagement to update new id_product to the SKU then flag update product)
                if(!empty($list_id_product)) {
                    $sql2 = "SELECT reference FROM ".self::$p_pref."product
                            WHERE id_shop = " . $user->id_shop . " AND active = 1 AND reference IN ('". implode("','", $list_reference) ."'); ";
                    $result2 = $this->db->product->db_query_str($sql2);
                    foreach ($result2 as $product_active){
                        if(isset($list_id_product[$product_active['reference']]))
                            unset($list_id_product[$product_active['reference']]);
                    }
                }              

                ////Duplicate sku products////
                //1. get id_product from Products
                $duplicate_sql1 = "SELECT
                        CASE
                            WHEN pa.old_reference IS NOT NULL THEN pa.old_reference
                            WHEN p.old_reference IS NOT NULL THEN p.old_reference
                        END AS sku
                        FROM ".self::$p_pref."product p
                        LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                        WHERE p.id_shop = " . $user->id_shop . " AND
                        (((p.old_reference != '' OR p.old_reference IS NOT NULL) AND p.flag_update_ref >= (NOW() - INTERVAL 2 DAY))
                        OR ((pa.old_reference != '' OR pa.old_reference IS NOT NULL) AND pa.flag_update_ref >= (NOW() - INTERVAL 2 DAY)) )
                        $case ";
                $duplicate_result1 = $this->db->product->db_query_str($duplicate_sql1);
                $duplicate_list_id_product = $duplicate_list_id_product_attribute = $duplicate_list_reference = array();
                foreach ($duplicate_result1 as $product){
                    $duplicate_list_id_product[$product['sku']] = $product['sku'];
                    $duplicate_list_reference[] = $product['sku'];
                }
                
                // 2. check if the product have other id_product which activate
                // : do not delete (we will use ProductManagement to update new id_product to the SKU then flag update product)
                if(!empty($duplicate_list_id_product)) {
                    $duplicate_sql2 = "SELECT
                            CASE
                                WHEN pa.reference IS NOT NULL THEN pa.reference
                                WHEN p.reference IS NOT NULL THEN p.reference
                            END AS sku
                            FROM ".self::$p_pref."product p
                            LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                            WHERE p.id_shop = " . $user->id_shop . " AND p.active = 1 AND 
                            (p.reference IN ('". implode("','", $duplicate_list_reference) ."') OR pa.reference IN ('". implode("','", $duplicate_list_reference) ."') ); ";
                    $duplicate_result2 = $this->db->product->db_query_str($duplicate_sql2);
                    foreach ($duplicate_result2 as $product_duplicate){
                        if(isset($duplicate_list_id_product[$product_duplicate['sku']]))
                            unset($duplicate_list_id_product[$product_duplicate['sku']]);
                    }
                }

                // 3. get product from Amazon_inventory where id_product in list
                if(!empty($list_id_product)) {
                    $sql .= "SELECT ai.id_product AS id_product, ai.id_product_attribute AS id_product_attribute, ai.sku AS sku
                            FROM ".self::$m_pref."amazon_inventory ai
                            WHERE ai.id_country = " . $user->id_country . "
                            AND ai.id_shop = " . $user->id_shop . "
                            AND ai.id_product IN (". implode(',', $list_id_product) .") ";
                }
                if(!empty($duplicate_list_id_product)) {
                    $sql .= " UNION SELECT ai.id_product AS id_product, ai.id_product_attribute AS id_product_attribute, ai.sku AS sku
                                FROM ".self::$m_pref."amazon_inventory ai
                                WHERE ai.id_country = " . $user->id_country . "
                                AND ai.id_shop = " . $user->id_shop . "
                                AND ai.sku IN ('". implode(',', $duplicate_list_id_product) ."') ";
                }
                $sql .= " UNION SELECT ai.id_product AS id_product, ai.id_product_attribute AS id_product_attribute, ai.sku AS sku 
                        FROM ".self::$m_pref."amazon_inventory ai 
                        WHERE ai.action_type = 'recreate' AND ai.id_country = " . $user->id_country . "  AND ai.id_shop = " . $user->id_shop . " ; ";
                break;
            case AmazonDatabase::DELETE_DISABLED: // Delete disabled product
                $sql = "SELECT id_product, id_product_attribute, sku 
                        FROM ".self::$m_pref."amazon_product_option                        
                        WHERE id_shop = " . $user->id_shop . " AND id_country =  " . $user->id_country . "  
			AND c_disable = 1 ;  ";    
                break;
        }
        $result = $this->db->product->db_query_str($sql);
        if (isset($result) && !empty($result)) {
            $product_list = $result;
        }
        return $product_list;
    }

    // Get All Product From Product Table , Quantity > 0, Product to Match From amazon_category_selected_wizard 
    public function getProductToSync($ws, $mode, $page) {

        $product_list = array();
        $lang = $name = $case = '';

        if (isset($ws->language) && !empty($ws->language)) {
            $l = $this->feedbiz->checkLanguage($ws->language, $ws->id_shop);
            $lang = "AND pl.id_lang = " . $l['id_lang'] . " ";
            $name = "pl.name as name, ";
        }
       
        $start = ((5 * $page));
        $case = $this->_case_check_report_inventory($ws);
        $sql = "SELECT
                    p.id_product AS id_product,
                    pa.id_product_attribute AS id_product_attribute,
                    " . $name . "                   
                    p.id_category_default AS id_category,
                    m.name AS manufacturer,
                    pi.image_url AS image_url,
                    CASE WHEN pa.price > 0 THEN pa.price ELSE p.price END AS price,
                    CASE WHEN pa.sku IS NOT NULL THEN pa.sku ELSE p.sku END AS sku,
                    CASE WHEN pa.ean13 IS NOT NULL THEN pa.ean13 ELSE p.ean13 END AS ean13,
                    CASE WHEN pa.upc IS NOT NULL THEN pa.upc ELSE p.upc END AS upc,
                    CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference,
                    CASE WHEN pa.quantity > 0 THEN pa.quantity ELSE p.quantity END AS quantity	
                FROM ".self::$p_pref."product p 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON (pa.id_product = p.id_product AND pa.id_shop = p.id_shop)                
                LEFT JOIN ".self::$p_pref."product_lang pl ON (pl.id_product = p.id_product AND pl.id_shop = p.id_shop)
                LEFT JOIN ".self::$p_pref."product_attribute_image pai ON (pai.id_product_attribute = pa.id_product_attribute AND pai.id_shop = pa.id_shop)
                LEFT JOIN ".self::$p_pref."product_image pi ON (pi.id_product = p.id_product AND p.id_shop = pi.id_shop) 
                LEFT JOIN ".self::$p_pref."manufacturer m ON (m.id_manufacturer = p.id_manufacturer AND m.id_shop = p.id_shop)
                WHERE
                    CASE WHEN pai.id_image IS NOT NULL THEN pi.id_image = pai.id_image ELSE pi.id_image = pi.id_image END
                    " . $case . "  
                    AND p.id_category_default IN (
                        SELECT id_category FROM ".self::$m_pref."amazon_category_selected
                        WHERE id_country = " . $ws->id_country . " 
                        AND id_shop = " . $ws->id_shop . "
                        AND id_mode = " . $ws->id_mode . "
                        GROUP BY id_category 
                    )  
                    AND p.id_shop = " . $ws->id_shop . " /*AND pa.id_shop = " . $ws->id_shop . " AND pl.id_shop = " . $ws->id_shop . "
                    AND pai.id_shop = " . $ws->id_shop . " AND pi.id_shop = " . $ws->id_shop . "  AND m.id_shop = " . $ws->id_shop . " */
                    " . $lang . "
                GROUP BY p.id_product, pa.id_product_attribute
                ORDER BY p.id_product, pa.id_product_attribute ";

        $sql .= ' LIMIT 5 OFFSET ' . $start . ' ;';
      
        $result = $this->db->product->db_query_str($sql);
        
        if (isset($result)) {
            foreach ($result as $product) {
                $id_product = (int) $product['id_product'];

                $reference = isset($product['reference']) && !empty($product['reference']) ? $product['reference'] :
                            (isset($product['ean13']) && !empty($product['ean13']) ? $product['ean13'] :
                            (isset($product['upc']) && !empty($product['upc']) ? $product['upc'] :
                            (isset($product['sku']) && !empty($product['sku']) ? $product['sku'] : '') ) );

                if (empty($reference)) continue;

                $product_name = str_replace("&", "and", $product['name']);
                $attr_name = Product::getProductAttributeById($product['id_product_attribute'], $ws->id_shop, null, $ws->id_lang);

                if (isset($attr_name) && !empty($attr_name)) {
                    $iso_code = isset($l['iso_code']) ? $l['iso_code'] : (isset($ws->language) ? $ws->language : '');
                    foreach ($attr_name as $attribute) {
                        if (isset($attribute['value']) && !empty($attribute['value'])) {
                            foreach ($attribute['value'] as $id_attribute => $attribute_value) {
                                if(isset($attribute['name'][$iso_code]))
                                    $pname[$reference][$product['id_product_attribute']][$id_attribute] = ' ' . $attribute['name'][$iso_code];
                                if(isset($attribute_value['name'][$iso_code]))
                                    $pname[$reference][$product['id_product_attribute']][$id_attribute] .= ' - ' . $attribute_value['name'][$iso_code];
                            }
                        }
                    }
                }

                if (isset($pname[$reference]) && !empty($pname[$reference]))
                    $product_name = $product_name . ' :' . implode(",", $pname[$reference][$product['id_product_attribute']]);

                $productQuantity = $this->calculate_quantity($id_product, $ws->id_shop);
                $product_list[$reference]['id_product'] = $id_product;

                if (isset($product['id_product_attribute']) && !empty($product['id_product_attribute'])) {
                    $product_list[$reference]['id_product_attribute'] = (int) $product['id_product_attribute'];
                }
                $product_list[$reference]['name'] = !empty($product_name) ? $product_name : '';
                $product_list[$reference]['reference'] = $reference;
                $product_list[$reference]['reference_type'] = isset($product['ean13']) && !empty($product['ean13']) ? 'EAN' :
                                                            (isset($product['upc']) && !empty($product['upc']) ? 'UPC' :
                                                            (isset($product['sku']) && !empty($product['sku']) ? 'SKU' : '') );
                $product_list[$reference]['manufacturer'] = isset($product['manufacturer']) ? $product['manufacturer'] : '';
                $product_list[$reference]['ean13'] = isset($product['image_url']) ? $product['ean13'] : '';
                $product_list[$reference]['upc'] = isset($product['upc']) ? $product['upc'] : '';
                $product_list[$reference]['sku'] = isset($product['sku']) ? $product['sku'] : '';
                $product_list[$reference]['image_url'] = isset($product['image_url']) ? $product['image_url'] : '';
                $product_list[$reference]['quantity'] = $productQuantity != 0 ? $productQuantity : $product['quantity'];
            }
            
            
        }
        
        return($product_list);
    }

    public function calculate_quantity($id_product, $id_shop) {
        $quantity = 0;
        $query = 'SELECT quantity FROM '.self::$o_pref.'product WHERE id_product = ' . $id_product . 'AND id_shop = ' . $id_shop . '; ';
        $results = $this->db->offer->db_query_str($query);
        if (isset($results[0]) && !empty($results[0])) {
            $quantity = $results[0]['quantity'];
        }
        return $quantity;
    }
    
    public function exportProductListAmazon($id_country, $id_shop, $id_mode = null, $lang = null, $options = null, $only_inactive = false) {

        $data = $addtional = array();
        $language = $id_lang = $query = $limit = $category = $case = $active_product = $join_suppliers = $join_carriers = $suppliers = $carriers = $manufacturers = null;

        if (isset($lang) && !empty($lang)) {
            $l = $this->feedbiz->checkLanguage($lang, $id_shop);
            $id_lang = $l['id_lang'];
        }
	
        //get category selected
        $addtional['category'] = $this->get_category_selected($id_country, $id_mode, $id_shop);

        if (!isset($addtional['category']) || empty($addtional['category'])) {
            return $data;
        }
        //get exclude Manufacturers
	$addtional['manufacturer'] = $this->feedbiz->getExcludeManufacturers($this->user, $id_shop, null);
	
        //get exclude Suppliers
	$addtional['supplier'] = $this->feedbiz->getExcludeSuppliers($this->user, $id_shop, null);
	
        //get Carriers Selected
	$addtional['carrier'] = $this->feedbiz->getSelectedCarriers($this->user, $id_shop, null, true);
	
        if (isset($options->limiter) && ($options->limiter > 0)) {
            $limit = ' LIMIT ' . $options->limiter;
        }
        
        if(isset($options->id_product) && !empty($options->id_product)) {
            $id_product = ' AND p.id_product = ' . $options->id_product;
        }

        $_db = $this->db->product;
       
        if (isset($addtional) && !empty($addtional)) {
            foreach ($addtional as $key => $add) {
                switch ($key) {
                    case "category":
                        $category = isset($add) && !empty($add) ? ' AND p.id_category_default IN (' . implode(', ', $add) . ') ' : '';
                        break;
                    case "manufacturer":
                        foreach ($add as $manu)
                            $manufacturer[] = $manu['id_manufacturer'];
                        break;
                    case "supplier":
                        foreach ($add as $supp)
                            $supplier[] = $supp['id_supplier'];
                        break;
                    case "carrier":
                        foreach ($add as $carr)
                            $carrier[] = $carr['id_carrier'];
                        break;
                }
            }
        }
        
        if(isset($manufacturer) && !empty($manufacturer)) {
            $manufacturers = ' AND  p.id_manufacturer NOT IN (' . implode(', ', $manufacturer) . ') ';
        }
        if(isset($supplier) && !empty($supplier)) {
            $suppliers =  ' AND ps.id_supplier NOT IN (' . implode(', ', $supplier) . ') AND ps.id_shop = ' . (int)$id_shop . " " ;
            $join_suppliers =  " LEFT JOIN ".self::$p_pref."product_supplier ps ON ps.id_product = p.id_product AND ps.id_shop = p.id_shop ";
        }
        if(isset($carrier) && !empty($carrier)) {
            $carriers = ' AND pc.id_carrier IN (' . implode(', ', $carrier) . ') AND pc.id_shop = ' . (int)$id_shop . " " ;
            $join_carriers = " LEFT JOIN ".self::$p_pref."product_carrier pc ON pc.id_product = p.id_product AND pc.id_shop = p.id_shop ";
        }

        if (isset($options->actions) && $options->actions == AmazonDatabase::SYNC) {
            
        } else {
            $ws = new stdClass();
            $ws->id_country = $id_country;
            $ws->id_shop = $id_shop;

            if ((isset($options->send_relation_ship) && $options->send_relation_ship) || (isset($options->send_only_image) && $options->send_only_image) || (isset($options->send_shipping_override) && $options->send_shipping_override)) { // add more only shipping override //2016-03-30
                $case = $this->_case_check_report_inventory($ws, false, true, true, false);
            } else {
		
		$where_additional = array();
		$where_additional[0]['field'] = 'action_type';
		$where_additional[0]['operation'] = 'NOT IN';
		$where_additional[0]['value'] = '("delete")';
		
                $case = $this->_case_check_report_inventory($ws, true, true, true, false, $where_additional);
            }
        }
        
        if($only_inactive){
            $active_product = " AND p.active = 0 ";
            $language = " AND pl.id_lang = ". $id_lang ." " ;
        }
        
        $query .= "SELECT "
                . "p.id_product as id_product, "
                . "pa.id_product_attribute  as id_product_attribute, "
                . "CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS sku "; 
        
        if($only_inactive){
            $query .= ", pl.name as name ";
        }
        
        $query .= "FROM ".self::$p_pref."product p ";
        $query .= "LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND pa.id_shop = $id_shop $join_carriers $join_suppliers ";
        if($only_inactive){
            $query .= " LEFT JOIN ".self::$p_pref."product_lang pl ON p.id_product = pl.id_product AND p.id_shop = pl.id_shop AND pl.id_lang = ". $id_lang;
        }
        $query .= " WHERE p.id_shop = " . $id_shop . " " . $language ;
        
        // Not include deleted product : filter by date_upd
        if(isset($id_product)){
            
             $query .= $id_product;
             $query .= " GROUP BY p.id_product, pa.id_product_attribute ";
	     
        } else {
            
            $query .= $carriers.$category.$manufacturers.$suppliers.$active_product.$case;
            $query .= " AND (p.date_upd IS NULL OR p.date_upd = '') ";
	    $query .= " GROUP BY p.id_product, pa.id_product_attribute ";

            if (isset($limit)) {
                $query .= $limit . ' OFFSET 0 ';
            }

            if(!$only_inactive){            
                $query .= " UNION SELECT ai.id_product AS id_product, ai.id_product_attribute AS id_product_attribute, ai.sku AS sku "
			. "FROM ".self::$m_pref."amazon_inventory ai "
			. "WHERE ai.action_type = 'recreate' AND ai.id_country = $id_country AND ai.id_shop = $id_shop  "
			. "GROUP BY ai.id_product, ai.id_product_attribute  ";

            }
        }
        
        if (isset($limit)) {
            $query .= $limit . ' OFFSET 0 ';
        }
        $query .= ' ;';
        
        if ($result = $_db->db_query_str($query)) {
            $data = $result;
        }
        
	unset($addtional);
	
        return $data;
    }
    //$user, false
    private function _case_check_report_inventory($ws, $is_not_in = true, $is_union_sent = true, $attribute = true, $is_sent = true, $where_option = array()) {
        $inventory_action_type = $where_additional = '';
        if ($is_sent) {
            $inventory_action_type = "AND action_type = 'send'";
        }
        if(isset($where_option) && !empty($where_option)){
            foreach ($where_option as $where){            
                $where_additional .= " AND " . $where['field'] . " " . $where['operation'] . " " . $where['value'] ." ";
            }
        }
        $include_attribute = '';
        if ($is_not_in) {
            $action = "NOT IN";
        } else {
            $action = "IN";
        }
        $action_type = " SELECT sku FROM ".self::$m_pref."amazon_report_inventory WHERE id_country = " . $ws->id_country . " AND id_shop = " . $ws->id_shop . " ";
        if ($is_union_sent) {
            $action_type .= "UNION SELECT sku FROM ".self::$m_pref."amazon_inventory WHERE id_country = ".$ws->id_country." AND id_shop = ".$ws->id_shop." $inventory_action_type $where_additional";
        }        
        // query $action_type
        $_db = $this->db->product;
        $skus = array();
       
        $result = $_db->db_query_str($action_type);

        foreach ($result as $sku){
            $skus[] = $sku['sku'];
        }
        if(!empty($skus)){
            $list_sku =  "'" . implode("','", $skus) . "'";
            if ($attribute == true) {
                $include_attribute = "  WHEN pa.reference IS NOT NULL THEN pa.reference $action ($list_sku) ";
            }
            return " AND ( CASE
                            $include_attribute
                            WHEN p.reference IS NOT NULL THEN p.reference $action ($list_sku)
                        END )";
        }
    }

    // FBA
    public function get_fba_data($id_shop, $id_country=null, $list_sku = array()) {       
        
        $content = array();
        $table = self::$m_pref . 'amazon_fba_data';        
	
        if(!$this->db_table_exists($table)) return $content;
        
	if(isset($id_country) && !empty($id_country))
	    $where['id_country'] = $id_country;
	
        $where['id_shop'] = $id_shop;

        $fields = array('sku', 'estimated_fee', 'fulfilment_fee');

        $this->db->product->select($fields);

        $this->db_from($table);

        if (!empty($where)){
            $this->db->product->where($where);
        }
       
	if (!empty($list_sku) && is_array($list_sku)){
            $this->db->product->where(array('sku' => "('".  implode("','", $list_sku)."')"), "IN");
        }	
	
        $result = $this->db->product->db_array_query($this->db->product->query);
		
        foreach ($result as $fba_data) {	    
            $content[$fba_data['sku']] = $fba_data;
        }
        
        unset($result);

        return $content;
    }
    
    public function get_fba_flag($id_shop, $id_country=null, $process=null, $type=null, $flag=null, $flag_stock=null, $limit=null) {

        $content = array();
        $table = self::$m_pref . 'amazon_fba_flag';

        if(!$this->db_table_exists($table)) return $content;

        $where['id_shop'] = $id_shop;

	if(isset($id_country) && !empty($id_country))
	    $where['id_country'] = $id_country;

	if(isset($process) && !empty($process))
	    $where['process'] = $process;

	if(isset($type) && !empty($type))
	    $where['type'] = $type;

	if(isset($flag) && !empty($flag))
	    $where['flag'] = $flag;

	if(isset($flag_stock) && !empty($flag_stock)){
	    $where['flag_stock'] = $flag_stock;
        }
        
	if(isset($limit) && !empty($limit)){
	    $this->db->product->limit((int)$limit);
        }

        $this->db_from($table);
        if (!empty($where)){
            $this->db->product->where($where);
        }
        
        return $this->db->product->db_array_query($this->db->product->query);

    }

    /*public function get_fba_flag($id_shop, $id_country=null, $process=null, $type=null, $flag=null) {
        
        $content = array();
        $table = self::$m_pref . 'amazon_fba_flag';        
	
        if(!$this->db_table_exists($table)) return $content;
        
        $where['id_shop'] = $id_shop;
	
	if(isset($id_country) && !empty($id_country))
	    $where['id_country'] = $id_country;
	
	if(isset($process) && !empty($process))
	    $where['process'] = $process;
	
	if(isset($type) && !empty($type))
	    $where['type'] = $type;
	
	if(isset($flag) && !empty($flag))
	    $where['flag'] = $flag;

        $this->db_from($table);
        if (!empty($where)){
            $this->db->product->where($where);
        }
       
        return $this->db->product->db_array_query($this->db->product->query);
	
    }*/

    public function get_fba_logs($id_shop, $id_country=null, $sku=null, $process=null, $type=null, $order_by=null) {

        $content = array();
        $table = self::$m_pref . 'amazon_fba_log';

        if(!$this->db_table_exists($table)) return $content;

        $sql = "SELECT sku, fba_status FROM amazon_fba_log afl1 WHERE afl1.id_shop = $id_shop " ;
        
        if(isset($id_country) && !empty($id_country))
	    $sql .= " AND afl1.id_country = " . (int) $id_country;

	if(isset($process) && !empty($process))
	    $sql .= " AND afl1.process = '$process' ";

	if(isset($type) && !empty($type))
	     $sql .= " AND afl1.type = '$type' ";

	if(isset($sku) && !empty($sku)) {
            $sql .= " AND afl1.sku IN ('".implode ("','", $sku)."') ";
        }

        if (isset($order_by) && !empty($order_by)) {// e.g date_add DESC
            $sql .= " ORDER BY $order_by ";
        }
             
       $result = $this->db->product->db_query_str($sql."; ");
       
       foreach ($result as $data){
           $content[$data['sku']] = $data['fba_status'] ;
       }

       unset($result);

       return $content;
    }

    public function getCategories($id_shop, $id_country, $id_lang, $parent = null, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null) {        
        $product_list =  array();              
        $categories = new Category($this->user); 
        
        if(!isset($id_lang) || empty($id_lang)){
            $language = Language::getLanguageDefault($id_shop); 
            $lang = array_keys($language); 
            $id_lang = $lang[0]; 
        }        
        if(!isset($parent) || empty($parent)){
            $parent = $categories->getRootCategories($id_shop);
        } 
        
        $sql = "SELECT mpo.*, c.id_category AS id_category, cl.name AS category, c.id_parent AS id_parent 
                FROM ".self::$p_pref."category c
                LEFT JOIN ".self::$p_pref."product p ON c.id_category = p.id_category_default AND c.id_shop = p.id_shop
                LEFT JOIN ".self::$p_pref."category_lang cl ON c.id_category = cl.id_category AND c.id_shop = cl.id_shop 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".self::$m_pref.self::PRODUCT_STRATEGIES." mpo ON p.id_product = mpo.id_product AND p.id_shop = mpo.id_shop AND mpo.id_country = $id_country 
                WHERE c.id_shop = $id_shop AND cl.id_lang = $id_lang AND c.id_parent = $parent";
        
        if(isset($search) && !empty($search)){
            $sql .= "AND (cl.name LIKE '%" . $search . "%' OR c.id_category LIKE '%" . $search . "%' ) ";
        }        
        $sql .= " GROUP BY c.id_category ";
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY c.id_parent ASC, $order_by ";
        } else {
            $sql .= "ORDER BY c.id_category ASC ";
        }
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
        $sql .= " ; " ;
        
        if( isset($num_row) && !empty($num_row) ) {            
            $result = $this->db->product->db_sqlit_query($sql);
            $row = $this->db->product->db_num_rows($result);
            return $row;            
        } else {                       
            $result = $this->db->product->db_query_str($sql);            
            foreach ($result as $key => $data){                
                if(!empty($data['id_category'])){
                    $product_list[$key]['id_parent']            = $data['id_parent'];
                    $product_list[$key]['id_category']          = $data['id_category'];                   
                    $product_list[$key]['category']             = $data['category'];                   
                    $product_list[$key]['minimum_price']        = !empty($data['minimum_price']) ? $data['minimum_price'] : '';  
                    $product_list[$key]['target_price']         = !empty($data['target_price']) ? $data['target_price'] : '';  
                                        
                    $child = $this->getCategories($id_shop, $id_country, $id_lang, $product_list[$key]['id_category'], $start, $limit, $order_by, $search, $num_row);
                    if(!empty($child['category'])){                        
                        $product_list[$key]['children'] = $child['category'];                        
                    }
                }
            }            
            return array('category' => $product_list);
        }
    }    
    public function saveCategoriesStrategies($data, $id_shop, $id_country) {             
        $parent_value = $child_value = array();
        $sql_child = '';
        $save_date = date('Y-m-d H:i:s');
        
        foreach ($data as $value){
            if(isset($value['id_category']) && !empty($value['id_category'])) { 
                
                $parent_value['minimum_price'] = isset($value['minimum_price']) ? $value['minimum_price'] : 0;
                $parent_value['target_price'] = isset($value['target_price']) ? $value['target_price'] : 0;
                $parent_value['id_shop'] = $id_shop;            
                $parent_value['id_country'] = $id_country;
                $parent_value['date_upd'] = $save_date;                                            
            
                //Get Product in Category
                $products = $this->getProductByCategory($value['id_category'], $id_shop);
              
                if(isset($products) && !empty($products)){
                    foreach ($products as $product){
                        if(!isset($product['sku']) || empty($product['sku'])){
                            continue;
                        }
                        $child_value = array_merge($product, $parent_value);
                        $child_value['id_shop'] = $id_shop;            
                        $child_value['id_country'] = $id_country;
                        $child_value['date_upd'] = $save_date;
                        $sql_chk = "SELECT * FROM ".self::$m_pref.self::PRODUCT_STRATEGIES." "
                            . "WHERE id_product = ".(int)$child_value['id_product']." "                            
                            . "AND id_shop = ".(int)$child_value['id_shop']." "
                            . "AND id_country = ".(int)$child_value['id_country']." ";
                        if(isset($child_value['sku'])){
                            $sql_chk .= "AND sku = '".$child_value['sku']."' ";
                        }                      
                        if(isset($child_value['id_product_attribute']) && !empty($child_value['id_product_attribute'])) {
                            $sql_chk .=  "AND id_product_attribute = ".(int)$child_value['id_product_attribute']." ";
                        }                        
                        
                        $result = $this->db->product->db_sqlit_query($sql_chk);

                        if($this->db->product->db_num_rows($result) <= 0){
                            $sql_child .= $this->insert_string(self::$m_pref.self::PRODUCT_STRATEGIES, $child_value);
                        } else {
                            $where = array(
                                'id_product' => array('operation' => '=', 'value' => (int)$child_value['id_product']),                            
                                'sku' => array('operation' => '=', 'value' => $child_value['sku']),
                                'id_shop' => array('operation' => '=', 'value' => (int)$child_value['id_shop']),
                                'id_country' => array('operation' => '=', 'value' => (int)$child_value['id_country']),
                            );                            
                            if(isset($child_value['id_product_attribute']) && !empty($child_value['id_product_attribute'])){
                                $where['id_product_attribute'] = array('operation' => '=', 'value' => (int)$child_value['id_product_attribute']);
                            }                 

                            $sql_child .= $this->update_string(self::$m_pref.self::PRODUCT_STRATEGIES, $child_value, $where);
                        }
                    }
                    
                    $exec = $this->db->product->db_exec($sql_child);
                    
                    if(!$exec){
                        return false;
                    } else {
                        $details = self::$m_pref.self::PRODUCT_STRATEGIES;
                        $this->update_amazon_status($id_country, $id_shop, $save_date, $details);
                    }
                }
            }
        }
        return true;
    }
    
    public function getProductByCategory($id_category, $id_shop) {       
        $data = array();
        $sql = "SELECT "
                . "p.id_product as id_product, "
                . "pa.id_product_attribute as id_product_attribute, "
                . "CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END as sku "
                . "FROM ".self::$p_pref."product p "
                . "LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop "
                . "WHERE p.id_category_default = ".$id_category." AND p.id_shop = ".(int)$id_shop."; ";                
        $result = $this->db->product->db_query_str($sql);  
        if(isset($result)){
            foreach ($result as $key => $value){
                $data[$key]['id_product'] = (int)$value['id_product'];
                $data[$key]['id_product_attribute'] = (int)$value['id_product_attribute'];
                $data[$key]['sku'] = $value['sku'];
            }
        }
        return $data;
    }
    
    public function getProductStrategies($id_shop, $id_country, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null){        
        $product_list = array();   
        $sql = "SELECT 
                mpo.*, 
                p.id_product as id_product, 
                pa.id_product_attribute as id_product_attribute,  
                p.reference as parent_sku,  
                CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference               
                FROM ".self::$p_pref."product p
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".self::$m_pref.self::PRODUCT_STRATEGIES ." mpo ON p.id_product = mpo.id_product AND mpo.id_country = $id_country 
                    AND p.id_shop = mpo.id_shop  
                    AND ( pa.id_product_attribute = mpo.id_product_attribute OR mpo.id_product_attribute IS NULL OR mpo.id_product_attribute = 0)
                WHERE p.id_shop = $id_shop " ;
        
        if(isset($search['all']) && !empty($search['all'])){
            $sql .= "AND ( "
                    . "p.id_product LIKE '%" . $search['all'] . "%' OR "
                    . "pa.id_product_attribute LIKE '%" . $search['all'] . "%' OR "
                    . "p.reference LIKE '%" . $search['all'] . "%' OR "
                    . "pa.reference LIKE '%" . $search['all'] . "%' "
                    . ") ";
        } 
        if(isset($search['id_category']) && !empty($search['id_category'])){
            $sql .= "AND ( p.id_category_default = " . $search['id_category'] . " ) ";
        }        
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY $order_by ";
        } else {
            $sql .= "ORDER BY p.id_product asc, pa.reference asc";
        }        
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }        
        $sql .= " ; " ;
        
        if( isset($num_row) && !empty($num_row) ) {
            $result = $this->db->product->db_sqlit_query($sql);
            $row = $this->db->product->db_num_rows($result);
            return $row;
        } else {
            $result = $this->db->product->db_query_str($sql);
            foreach ($result as $key => $data){
                if(!empty($data['reference'])){
                    $product_list[$key]['key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $data['reference']);
                    $product_list[$key]['parent_sku'] = !empty($data['parent_sku']) ? $data['parent_sku'] : $data['reference'];
                    $product_list[$key]['parent_sku_key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $product_list[$key]['parent_sku']);
                    $product_list[$key]['id_product'] = $data['id_product'];
                    $product_list[$key]['id_product_attribute'] = $data['id_product_attribute'];
                    $product_list[$key]['reference'] = $data['reference'];  
                    $product_list[$key]['minimum_price'] = $data['minimum_price'];
                    $product_list[$key]['target_price'] = $data['target_price'];
                    $product_list[$key]['date_upd'] = $data['date_upd'];
                }
            }
            return $product_list;
        }
    }
    
    // return 1 row
    public function getProductStrategyBySku($sku, $id_shop, $id_country) {    
        $data = $history = array();
        $list = implode("','", (array)$sku);
        $sql = "SELECT * FROM ".self::$m_pref.self::PRODUCT_STRATEGIES." WHERE sku IN ('".$list."') AND id_shop = $id_shop AND id_country = $id_country ; ";
        $result = $this->db->product->db_query_str($sql);          
        if(isset($result)){
            foreach ($result as $value){
                foreach ($value as $k => $val){
                    if(!is_integer($k) && !is_int($k)){
                        if(isset($history['data'][$k]) && $history['data'][$k] != $val){
                            $data[$k] = 'disable';
                            $history['disable'][$k] = true;
                        } else {
                            if(!isset($history['disable'][$k])){
                                $data[$k] = $val;                            
                            }
                        }
                        $history['data'][$k] = $val;
                    }
                }
            }
        }
       
        return $data;
    }

    public function saveProductStrategy($data, $id_shop, $id_country) {

        $parent_value = $child_value = $list_id_product = array();
        $disabled = 'disabled';
        $sql_child = '';
        $save_date = date('Y-m-d H:i:s');

        foreach ($data as $key => $value){

            if(isset($value['id_product']) && !empty($value['id_product']) && isset($value['parent_sku']) && !empty($value['parent_sku'])) {

                $parent_value['id_product'] = $value['id_product'];
                $parent_value['sku'] = $value['parent_sku'];

                if(isset($value['minimum_price']) && $value['minimum_price'] != $disabled){
                    $parent_value['minimum_price'] = $value['minimum_price'];
                }

                if(isset($value['target_price']) && $value['target_price'] != $disabled){
                    $parent_value['target_price'] = $value['target_price'];
                }

                $parent_value['id_shop'] = $id_shop;
                $parent_value['id_country'] = $id_country;
                $parent_value['date_upd'] = $save_date;
            }

            if(isset($value['child']) && !empty($value['child'])){

                // If has child loop child with id_product_attribute / sku
                foreach ($value['child'] as $child_key => $child){

                    if(!empty($parent_value)) {
                        $child_value = $parent_value;
                    } else {
                        $child_value = $child;
                        $child_value['id_shop'] = $id_shop;
                        $child_value['id_country'] = $id_country;
                        $child_value['date_upd'] = date('Y-m-d H:i:s');
                    }

                    if(isset($child['id_product_attribute']) && !empty($child['id_product_attribute'])) {
                        $child_value['id_product_attribute'] = $child['id_product_attribute'];
                    }

                    $child_value['sku'] = $child['sku'];

                    unset($child_value['parent_sku']);

                    $filter_value = $child_value;

                    $sql_chk = "SELECT * FROM ".self::$m_pref.self::PRODUCT_STRATEGIES." "
                        . "WHERE id_product = ".(int)$child_value['id_product']." "
                        . "AND sku = '".$child_value['sku']."' "
                        . "AND id_shop = ".(int)$child_value['id_shop']." "
                        . "AND id_country = ".(int)$child_value['id_country']." ";

                    if(isset($child_value['id_product_attribute'])) {
                        $sql_chk .=  "AND id_product_attribute = ".(int)$child_value['id_product_attribute']." ";
                    }

                    $result = $this->db->product->db_sqlit_query($sql_chk);

                    if($this->db->product->db_num_rows($result) <= 0){
                        $sql_child .= $this->insert_string(self::$m_pref.self::PRODUCT_STRATEGIES, $filter_value);
                    } else {
                         $where = array(
                            'id_product' => array('operation' => '=', 'value' => (int)$child_value['id_product']),
                            'sku' => array('operation' => '=', 'value' => $child_value['sku']),
                            'id_shop' => array('operation' => '=', 'value' => (int)$child_value['id_shop']),
                            'id_country' => array('operation' => '=', 'value' => (int)$child_value['id_country']),
                        );

                        if(isset($child_value['id_product_attribute'])){
                            $where['id_product_attribute'] = array('operation' => '=', 'value' => (int)$child_value['id_product_attribute']);
                        }

                        $sql_child .= $this->update_string(self::$m_pref.self::PRODUCT_STRATEGIES, $filter_value, $where);
                    }
                    $list_id_product[$child_value['sku']] = array(
                        'SKU' => $child_value['sku'],
                        'id_product' => $child_value['id_product'],
                    );
                    if(isset($child_value['id_product_attribute'])){
                        $list_id_product[$child_value['sku']]['id_product_attribute'] = $child_value['id_product_attribute'];
                    }
                }
            }
        }

        if(strlen($sql_child)){
            $exec = $this->db->product->db_exec($sql_child);
        }
        if($exec && !empty($list_id_product)){
            //$details = self::$m_pref.self::PRODUCT_STRATEGIES;
            //$this->update_amazon_status($id_country, $id_shop, $save_date, $details);
            $this->db->product->db_exec($this->update_amazon_repricing_flag($id_shop, $id_country, $list_id_product, 1));
        }
        return $exec;
    }
    
    public function resetProductStrategy($id_product, $id_shop, $id_country){
        $sql = "DELETE FROM ".self::$m_pref.self::PRODUCT_STRATEGIES." WHERE id_product = $id_product AND id_shop =  $id_shop AND id_country = $id_country ; "; 
        return $this->db->product->db_exec($sql);
    }
    
    public function resetCategoryStrategy($id_category, $id_shop, $id_country){
        
        $sql = '';
        
        $products = $this->get_product_by_id_category($id_category, $id_shop);                
        if(isset($products) && !empty($products)){   
            foreach ($products as $product){
                $sql .= "DELETE FROM ".self::$m_pref.self::PRODUCT_STRATEGIES ."
                        WHERE id_product = ". $product['id_product']." AND id_shop =  $id_shop AND id_country = $id_country "; 
                if(isset($this->id_country) && !empty($this->id_country)){
                    $sql .= " AND id_country = ".$this->id_country." "; 
                }
                $sql .= " ; "; 
            }
        }
        
        if($this->db->product->db_exec($sql)){
            return true;
        }
        
        return false;
    }

    public function get_product_by_id_category($id_category, $id_shop){       
        $data = array();
        
        $sql = "SELECT "
                . "p.id_product as id_product, "
                . "pa.id_product_attribute as id_product_attribute, "
                . "CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS sku "
                . "FROM  ".self::$p_pref."product p "
                . "LEFT JOIN ".self::$p_pref."product_attribute pa ON pa.id_product = p.id_product AND pa.id_shop = p.id_shop "
                . "WHERE p.id_category_default = ".$id_category." AND p.id_shop = ".(int)$id_shop."; ";                
        $result = $this->db->product->db_query_str($sql);  
        
        if(isset($result)){
            
            foreach ($result as $key => $value){
                $data[$key]['id_product'] = $value['id_product'];
                $data[$key]['id_product_attribute'] = $value['id_product_attribute'];
                $data[$key]['sku'] = $value['sku'];
            }
        }
        
        return $data;
    }

    public function productStrategiesDownload($values, $id_shop, $id_country){
                
        $list = array();
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['sku'] = "SKU";
        $list['header']['minimum_price'] = "Minimum Price";
        $list['header']['target_price'] = "Maximum Price";
     
        $sql = "SELECT mpo.*, p.id_product as id_product,  pa.id_product_attribute as id_product_attribute,   
                CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference,                 
                CASE WHEN pa.quantity IS NOT NULL THEN pa.quantity ELSE p.quantity END AS quantity             
                FROM ".self::$p_pref."product p
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".self::$m_pref.self::PRODUCT_STRATEGIES." mpo ON p.id_product = mpo.id_product AND mpo.id_country = $id_country 
                AND p.id_shop = mpo.id_shop
                WHERE p.id_shop = $id_shop " ;
                    
        if(isset($values['category']) && !empty($values['category'])) {
            $list_category = implode(', ', $values['category']);
            if(strlen($list_category))
                $sql .= "AND ( p.id_category_default IN (" . $list_category . ") ) ";
        }
        if(isset($values['only_active']) && $values['only_active'] == 1) {
            $sql .= "AND p.active = 1 ";
        }
        $sql .= " GROUP BY p.id_product, pa.id_product_attribute ORDER BY p.id_product ASC, pa.id_product_attribute ASC; " ;
        $result = $this->db->product->db_query_str($sql);  
        
        if(isset($values['only_in_stock']) && $values['only_in_stock'] == 1) {
            $product = new Product($this->user);
        }
        
        if(isset($result)){
            foreach ($result as $key => $value){
                if(isset($values['only_in_stock']) && $values['only_in_stock'] == 1) {
                    // Check Offer
                    if(isset($value['id_product_attribute']) && !empty($value['id_product_attribute'])){
                        $offer = $product->getProductCombinationOffer($value['id_product'], $value['id_product_attribute'], $id_shop) ;
                        if(!empty($offer) && $offer['quantity'] <= 0) continue;
                    } else {
                        $offer = $product->getProductOffer($value['id_product'], $id_shop);
                        if(!empty($offer) && $offer['quantity'] <= 0) continue;
                    }
                    // Check Product Quantity
                    if(!isset($value['quantity']) || $value['quantity'] <= 0){
                        continue;
                    }
                }  
                $list[$key]['id_product'] = $value['id_product'];
                $list[$key]['id_product_attribute'] = $value['id_product_attribute'];
                $list[$key]['sku'] = $value['reference'];
                $list[$key]['minimum_price'] = $value['minimum_price'];
                $list[$key]['target_price'] = $value['target_price'];        
            }
        }        
                
        ob_start();
        ob_get_clean();
        
        $filename = 'amazon_product_strategies_'.$id_country.'.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) { 
            fputcsv($h, $data, ';'); 
        }
    } 
    
    public function productStrategiesDownloadTemplate(){
        
        $list = array();
        $list['header']['sku'] = "SKU";
        $list['header']['minimum_price'] = "Minimum Price";
        $list['header']['target_price'] = "Maximum Price";
        
        ob_start();
        ob_get_clean();
        
        $filename = 'amazon_product_strategies.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) { 
            fputcsv($h, $data, ';'); 
        }
    }  
    
    public function productStrategiesUpload($data, $id_shop, $id_country) {  
	
	$table = self::$m_pref.self::PRODUCT_STRATEGIES;
	
        $exe = false;
        $sql = '';
        $error_produst = $data_values = array();
        $i = $no_upload = $no_success = $error_error = $count = 0;
        $save_date = date('Y-m-d H:i:s');
                 
        foreach ($data as $value){
            
	    $count++;
	    
            $data_value = array();
            
            $no_upload++;
            
            if(!isset($value['sku']) || empty($value['sku'])) {
                $i++; $error_error++;
                $error_produst['missing_sku'] = $i;
                continue;
            } 
            
            $sku = $value['sku'];
            
            if(!isset($value['id_product']) || empty($value['id_product'])) {
                $error_error++;
                $error_produst[$sku] = 'Missing Product ID';
                continue;
            } 
            
            if(isset($value['minimum_price']) && !empty($value['minimum_price'])) {
                if(!is_numeric($value['minimum_price'])){
                    $error_error++;
                    $error_produst[$sku] = 'Minimum Price must have a decimal. Currently value : "' . $value['minimum_price'] . '" ';
                    continue;
                }
            } 
            
            if(isset($value['target_price']) && !empty($value['target_price'])) {
                if(!is_numeric($value['target_price'])){
                    $error_error++;
                    $error_produst[$sku] = 'Maximum Price must have a decimal. Currently value : "' . $value['target_price'] . '" ';
                    continue;
                }
            }
            
            $data_value['id_product'] = (int)$value['id_product'];
            $data_value['id_product_attribute'] = isset($value['id_product_attribute']) ? (int)$value['id_product_attribute'] : 0;          
            $data_value['id_shop'] = (int)$id_shop;            
            $data_value['id_country'] = (int)$id_country;
            $data_value['sku'] = $sku;
          
            if(isset($value['minimum_price']) && !empty($value['minimum_price']) && $value['minimum_price'] != 'NULL'){
                $data_value['minimum_price'] = $value['minimum_price'];
            } else {
		$data_value['minimum_price'] = null;
	    }
            if(isset($value['target_price']) && !empty($value['target_price']) && $value['target_price'] != 'NULL'){
                $data_value['target_price'] = $value['target_price'];
            } else {
		$data_value['target_price'] = null;
	    }
            /*if(isset($value['asin']) && !empty($value['asin']) && $value['asin'] != 'NULL'){
                $data_value['asin'] = $value['asin'];
            }else {
		$data_value['asin'] = null;
	    }
            if(isset($value['actual_price']) && !empty($value['actual_price']) && $value['actual_price'] != 'NULL'){
                $data_value['actual_price'] = $value['actual_price'];
            }else {
		$data_value['actual_price'] = null;
	    }
            if(isset($value['gap']) && !empty($value['gap']) && $value['gap'] != 'NULL'){
                $data_value['gap'] = $value['gap'];
            }else {
		$data_value['gap'] = null;
	    }*/
           
            $data_value['date_upd'] = $save_date; 

            $sql .= $this->replace_string($table, $data_value, true);     
	    
	    if($count < sizeof($data)){
		$sql .= ',';
	     } else {
		$sql .= ';' ;
	    }
	    
            $no_success++;
        }
	 
	if(strlen($sql)){
	    $exe_sql = 'REPLACE INTO '.$table.' (id_product,id_product_attribute,id_shop,id_country,sku,minimum_price,target_price,date_upd)'; 
	    $exe_sql .= ' VALUES ' . $sql ;
	    $exe = $this->db->product->db_exec($exe_sql,false, true, true);
	}
        
        if($exe){
            $this->update_amazon_status($id_country, $id_shop, $save_date, $table);
        }
        
        return array(
            'pass' => ($exe) ? true : false, 
            'has_error'=> empty($error_produst) ? false : true, 
            'error_product' => $error_produst,
            'no_upload' => $no_upload,
            'no_success' => $no_success,
            'no_error' => ($error_error == 0) ? '0' : $error_error
        );
        
    }
    
    public function get_feed_profile_price($id_shop, $id_mode, $id_profile = null) {

        $prices = $where = array();
        $_db = new Db($this->user, 'offers');
        $_db->from('profile_price');
        $where['id_shop'] = $id_shop;
        $where['id_mode'] = $id_mode;

        if (isset($id_profile) && !empty($id_profile)) {
            $where['id_profile_price'] = $id_profile;
        }

        $_db->where($where);
        $result = $_db->db_array_query($_db->query);

        if (isset($result) && !empty($result)) {
            foreach ($result as $price) {
                $prices[$price['id_profile_price']]['id_profile'] = $price['id_profile_price'];
                $prices[$price['id_profile_price']]['price_percentage'] = $price['price_percentage'];
                $prices[$price['id_profile_price']]['price_value'] = $price['price_value'];
                $prices[$price['id_profile_price']]['rounding'] = $price['rounding'];
                $prices[$price['id_profile_price']]['name'] = $price['name'];
            }
        }
        return $prices;
    }

    public function get_mapping_feature($id_country, $id_shop, $id_mode, $id_lang = null, $id_feature = null, $id_feature_value = null, $id_model = null, $product_type = null, $product_sub_type = null, $attribute_field = null) {

        $model = '';
        $content = array();
        $lang = $feature_group = $feature = $product_type_fields = $product_sub_type_fields = $attribute_field_fields = '';

        if (isset($id_lang) && !empty($id_lang)) {
            $lang = " AND f.id_lang = " . $id_lang . " AND fv.id_lang = " . $id_lang;
        }
        if (isset($id_model) && !empty($id_model)) {
            $model = " AND am.id_model = " . $id_model;
        }
        if (isset($product_type) && !empty($product_type)) {
            $product_type_fields = " AND amf.product_type = '" . $product_type . "' ";
        }
        if (isset($product_sub_type) && !empty($product_sub_type)) {
            $product_sub_type_fields = " AND amf.product_sub_type = '" . $product_sub_type . "' ";
        }
        if (isset($attribute_field) && !empty($attribute_field)) {
            $attribute_field_fields = " AND amf.attribute_field = '" . $attribute_field . "' ";
        }
        if (isset($id_feature_value) && !empty($id_feature_value)) {
            $feature = " AND fv.id_feature_value = " . $id_feature_value . " ";
        }
        if (isset($id_feature) && !empty($id_feature)) {
            if (is_array($id_feature)) {
                $feature_group = " AND f.id_feature IN (" . implode(',', $id_feature) . ") ";
                $feature = " AND fv.id_feature_value IN (SELECT id_feature_value FROM ".self::$p_pref."product_feature "
                        . "WHERE id_shop = " . $id_shop . " AND id_feature IN (" . implode(',', $id_feature) . ") GROUP BY id_feature_value) ";
            } else {
                $feature_group = " AND f.id_feature = " . $id_feature . " ";
            }
        }
        $sql = "SELECT 
                    f.id_feature as id_feature,
                    f.name as name,
                    fv.id_feature_value as id_feature_value,
                    fv.value as value,
                    amf.product_type as product_type,
                    amf.product_sub_type as product_sub_type,
                    amf.attribute_field as attribute_field,
                    amf.mapping as mapping,
                    CASE WHEN amf.is_color IS NOT NULL THEN amf.is_color ELSE 0 END AS is_color,
                    CASE WHEN amf.id_feature IS NOT NULL AND amf.id_feature_value IS NOT NULL THEN 'true' ELSE NULL END AS selected
                FROM ".self::$m_pref."amazon_model am 
                LEFT JOIN ".self::$m_pref."amazon_profile ap ON am.id_model = ap.id_model AND am.id_country = ap.id_country AND am.id_shop = ap.id_shop
                LEFT JOIN ".self::$m_pref."amazon_category_selected acs ON ap.id_profile = acs.id_profile AND ap.id_country = acs.id_country AND ap.id_shop = acs.id_shop
                LEFT JOIN ".self::$p_pref."product p ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop 
                LEFT JOIN ".self::$p_pref."product_feature pf ON p.id_product = pf.id_product AND p.id_shop = pf.id_shop
                LEFT JOIN ".self::$p_pref."feature f ON pf.id_feature = f.id_feature AND pf.id_shop = f.id_shop
                LEFT JOIN ".self::$p_pref."feature_value fv ON pf.id_feature_value = fv.id_feature_value AND pf.id_shop = fv.id_shop
                LEFT JOIN ".self::$m_pref."amazon_mapping_feature amf ON pf.id_feature = amf.id_feature AND pf.id_feature_value = amf.id_feature_value AND pf.id_shop = amf.id_shop AND am.id_country = amf.id_country
                WHERE am.id_country = $id_country 
                AND am.id_shop = $id_shop
                " . $model . "  
                " . $lang . "  
                " . $feature_group . "  
                " . $feature . "  
                " . $product_type_fields . " 
                " . $product_sub_type_fields . " 
                " . $attribute_field_fields . " 
                GROUP BY amf.product_type, amf.product_sub_type, amf.attribute_field, fv.id_feature_value, f.id_lang 
                ORDER BY f.name ASC";

        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value) {
            $content[$keys]['id_feature'] = $value['id_feature'];
            $content[$keys]['name'] = $value['name'];
            $content[$keys]['id_feature_value'] = $value['id_feature_value'];
            $content[$keys]['value'] = $value['value'];
            $content[$keys]['product_type'] = $value['product_type'];
            $content[$keys]['product_sub_type'] = $value['product_sub_type'];
            $content[$keys]['attribute_field'] = $value['attribute_field'];
            $content[$keys]['mapping'] = $value['mapping'];
            $content[$keys]['is_color'] = $value['is_color'];
            $content[$keys]['selected'] = $value['selected'];
        }
        return $content;
    }

    public function getMappingFeatureFeed($id_country, $id_shop, $product_type = null, $product_sub_type = null) {
        $data = array();
        $sql = "SELECT mapping, id_feature, id_feature_value, product_type, product_sub_type, attribute_field FROM ".self::$m_pref."amazon_mapping_feature 
                WHERE id_country = $id_country 
                AND id_shop = $id_shop  ";
        if (isset($product_type) && !empty($product_type)) {
            $sql .= " AND product_type = '$product_type' ";
        }
        if (isset($product_sub_type) && !empty($product_sub_type)) {
            $sql .= " AND product_sub_type = '$product_sub_type' ";
        }
        $sql .= " ; ";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $mapp) {
            $data[$mapp['id_feature']][$mapp['id_feature_value']][$mapp['attribute_field']] = $mapp['mapping'];
        }
        return $data;
    }

    public function get_mapping_attribute_group($id_country, $id_shop, $id_mode, $id_lang = null) {
        $content = array();
        $lang = '';
        if (isset($id_lang) && $id_lang != NULL)
            $lang = "AND agl.id_lang = " . $id_lang;
        $sql = "SELECT agl.id_attribute_group as id_attribute_group, agl.name as name,
                    CASE WHEN ama.id_mapping IS NOT NULL THEN ama.id_mapping ELSE NULL END AS id_mapping,
                    CASE WHEN ama.is_color IS NOT NULL THEN ama.is_color ELSE NULL END AS is_color
                FROM  ".self::$p_pref."attribute_group_lang agl
                LEFT OUTER JOIN ".self::$m_pref."amazon_mapping_attribute ama
                    ON ama.id_attribute_group = agl.id_attribute_group 
                    AND ama.id_shop = agl.id_shop               
                    AND ama.id_country = " . $id_country . "
                    AND ama.id_mode = " . $id_mode . "
                WHERE agl.id_shop = " . $id_shop . " " . $lang . "  
                GROUP BY agl.id_attribute_group
                ORDER BY agl.name ASC";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value) {
            $content[$keys]['id_attribute_group'] = $value['id_attribute_group'];
            $content[$keys]['name'] = $value['name'];
            $content[$keys]['id_mapping'] = $value['id_mapping'];
            $content[$keys]['is_color'] = $value['is_color'];
        }
        return $content;
    }
    
    public function get_mapping_attribute_groups($id_country, $id_shop) {
        $content = array();
        $sql = "SELECT agl.id_attribute_group as id_attribute_group, agl.name as name, agl.id_lang as id_lang
                    CASE WHEN ama.id_mapping IS NOT NULL THEN ama.id_mapping ELSE NULL END AS id_mapping,
                    CASE WHEN ama.is_color IS NOT NULL THEN ama.is_color ELSE NULL END AS is_color
                FROM  ".self::$p_pref."attribute_group_lang agl
                LEFT OUTER JOIN ".self::$m_pref."amazon_mapping_attribute ama
                    ON ama.id_attribute_group = agl.id_attribute_group
                    AND ama.id_shop = agl.id_shop
                    AND ama.id_country = " . $id_country . "                   
                WHERE agl.id_shop = " . $id_shop . " 
                GROUP BY agl.id_attribute_group
                ORDER BY agl.name ASC";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $value) {
            $keys = $value['id_lang'];
            $key = $value['id_attribute_group'];
            $content[$keys][$key]['id_attribute_group'] = $value['id_attribute_group'];
            $content[$keys][$key]['name'] = $value['name'];
            $content[$keys][$key]['id_mapping'] = $value['id_mapping'];
            $content[$keys][$key]['is_color'] = $value['is_color'];
        }
        return $content;
    }

    public function get_mapping_feature_group($id_country, $id_shop, $id_mode, $id_lang = null) {
        $content = array();
        $lang = '';
        if (isset($id_lang) && !empty($id_lang))
            $lang = ' AND ft.id_lang = ' . $id_lang;

        $sql = "SELECT  ft.id_feature as id_feature, ft.name as name,
                    CASE WHEN amf.id_mapping IS NOT NULL THEN amf.id_mapping  ELSE NULL END AS id_mapping,
                    CASE WHEN amf.is_color IS NOT NULL THEN amf.is_color ELSE NULL END AS is_color
                FROM ".self::$p_pref."feature ft 
                LEFT JOIN ".self::$m_pref."amazon_mapping_feature amf ON amf.id_feature = ft.id_feature AND amf.id_shop = ft.id_shop AND amf.id_country = $id_country AND amf.id_mode = $id_mode
                WHERE ft.id_shop = " . $id_shop . " " . $lang . " 
                GROUP BY ft.id_feature
                ORDER BY ft.name ASC";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value) {
            $content[$keys]['id_feature'] = $value['id_feature'];
            $content[$keys]['name'] = $value['name'];
            $content[$keys]['id_mapping'] = $value['id_mapping'];
            $content[$keys]['is_color'] = $value['is_color'];
        }
        return $content;
    }

    public function get_mapping_feature_groups($id_country, $id_shop) {
        $content = array();
        $sql = "SELECT  ft.id_feature as id_feature, ft.name as name, ft.id_lang as id_lang,
                    CASE WHEN amf.id_mapping IS NOT NULL THEN amf.id_mapping  ELSE NULL END AS id_mapping,
                    CASE WHEN amf.is_color IS NOT NULL THEN amf.is_color ELSE NULL END AS is_color
                FROM ".self::$p_pref."feature ft
                LEFT JOIN ".self::$m_pref."amazon_mapping_feature amf ON amf.id_feature = ft.id_feature AND amf.id_shop = ft.id_shop AND amf.id_country = $id_country 
                WHERE ft.id_shop = " . $id_shop . " 
                GROUP BY ft.id_feature
                ORDER BY ft.name ASC";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value) {
            $keys = $value['id_lang'];
            $key = $value['id_feature'];
            $content[$keys][$key]['id_feature'] = $value['id_feature'];
            $content[$keys][$key]['name'] = $value['name'];
            $content[$keys][$key]['id_mapping'] = $value['id_mapping'];
            $content[$keys][$key]['is_color'] = $value['is_color'];
        }
        return $content;
    }

    public function get_mapping_carrier($id_country, $id_shop, $id_mode, $id_carrier = null) {
        $content = array();
        $carrier = '';
        if (isset($id_carrier) && $id_carrier != NULL)
            $carrier = " AND id_carrier = " . $id_carrier . " ";
        $sql = "SELECT MAX(c.id_carrier) as id_carrier,c.id_carrier_ref as id_carrier_ref,c.id_tax as id_tax,c.name as name,amc.id_mapping as id_mapping,amc.mapping as mapping,
                amc.type as type,amc.other as other,
                CASE WHEN amc.id_carrier IS NOT NULL THEN 'true' ELSE null END AS selected 
                FROM ".self::$p_pref."carrier c
                LEFT JOIN ".self::$m_pref."amazon_mapping_carrier amc ON (amc.id_carrier = c.id_carrier_ref OR amc.id_carrier = c.id_carrier) AND amc.id_shop = c.id_shop
                WHERE c.id_shop = " . $id_shop . "  
                    " . $carrier . " 
                    AND amc.id_country = " . $id_country . "
                    AND amc.id_mode = " . $id_mode . " 
                    AND amc.id_shop = " . $id_shop . " 
                GROUP BY amc.id_mapping
                ORDER BY c.name ASC";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value)
            foreach ($value as $key => $val)
                if (!is_int($key))
                    $content[$keys][$key] = $val;
          
        return $content;
    }

    public function get_mapping_shipping_override($id_country, $id_shop) {
        $content = array();
        $sql = "SELECT amc.id_mapping as id_mapping,amc.mapping as mapping,amc.type as type,amc.other as other
                FROM  ".self::$m_pref."amazon_mapping_carrier amc 
                WHERE amc.id_country = " . $id_country . " AND amc.id_shop = " . $id_shop . " AND amc.type = 'shipping_override' ;";   
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $value)
        {
            $content[$value['other']]['id_mapping'] = $value['id_mapping'];
            $content[$value['other']]['mapping'] = $value['mapping'];
            $content[$value['other']]['type'] = $value['other'];
        }           
        return $content;
    }

    public function getMappingAttributeFeed($id_country, $id_shop, $product_type, $product_sub_type) {

        $data = array();
        $sql = "SELECT mapping,id_attribute_group,id_attribute,attribute_field FROM ".self::$m_pref."amazon_mapping_attribute
                WHERE id_country = " . $id_country . " 
                AND id_shop = " . $id_shop . " ";
        if (isset($product_type) && !empty($product_type)) {
            $sql .= " AND product_type = '" . $product_type . "' ";
        }
        if (isset($product_sub_type) && !empty($product_sub_type)) {
            $sql .= " AND product_sub_type = '" . $product_sub_type . "'  ";
        }
        $sql .= " ; ";

        $result = $this->db->product->db_query_str($sql);

        foreach ($result as $mapp) {
            $data[$mapp['id_attribute_group']][$mapp['id_attribute']][$mapp['attribute_field']] = $mapp['mapping'];
        }
        return $data;
    }

    public function get_valid_value_custom($region) {
        $data = $value = array();
        $region = Amazon_Tools::domainToId($region);
        $sql = "SELECT universe, product_type, attribute_field, valid_value FROM ".self::$m_pref."amazon_valid_values_custom WHERE region = '" . $region . "' ; ";
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $rows) {
            $custom_value = '';
            if (isset($data[$rows['universe']][$rows['product_type']][$rows['attribute_field']]))
                $custom_value = $data[$rows['universe']][$rows['product_type']][$rows['attribute_field']] . ', ';

            $data[$rows['universe']][$rows['product_type']][$rows['attribute_field']] = $custom_value . $rows['valid_value'];
        }
        return $data;
    }

    public function get_mapping_attribute($id_country, $id_shop, $id_mode, $id_lang = null, $id_attribute_group = null, $id_attribute = null, $id_model = null, $product_type = null, $product_sub_type = null, $attribute_field = null) {

        $content = array();
        $lang = $attribute_group = $attribute = $model = $product_type_fields = $product_sub_type_fields = $attribute_field_fields = '';

        if (isset($id_lang) && $id_lang != NULL) {
            $lang = " AND agl.id_lang = " . $id_lang . " AND al.id_lang = " . $id_lang;
        }
        if (isset($product_type) && !empty($product_type)) {
            $product_type_fields = " AND ama.product_type = '" . $product_type . "' ";
        }
        if (isset($product_sub_type) && !empty($product_sub_type)) {
            $product_sub_type_fields = " AND ama.product_sub_type = '" . $product_sub_type . "' ";
        }
        if (isset($attribute_field) && !empty($attribute_field)) {
            $attribute_field_fields = " AND ama.attribute_field = '" . $attribute_field . "' ";
        }
        if (isset($id_attribute_group) && $id_attribute_group != NULL) {
            if (is_array($id_attribute_group)) {
                $attribute_group = " AND ag.id_attribute_group IN (" . implode(',', $id_attribute_group) . ") ";
            } else {
                $attribute_group = " AND ag.id_attribute_group = " . $id_attribute_group . " ";
            }
        }
        if (isset($id_attribute) && $id_attribute != NULL) {
            $attribute = " AND al.id_attribute = " . $id_attribute . " ";
        }
        if (isset($id_model) && !empty($id_model)) {
            $model = " AND am.id_model = " . $id_model . " ";
        }

        $sql = "
            SELECT 
                agl.id_attribute_group as id_attribute_group, 
                agl.name as name, 
                al.id_attribute as id_attribute, 
                al.name as attribute, 
                ama.id_mapping as id_mapping, 
                ama.product_type as product_type, 
                ama.product_sub_type as product_sub_type, 
                ama.attribute_field as attribute_field, 
                ama.mapping as mapping, 
                CASE WHEN ama.is_color IS NOT NULL THEN ama.is_color ELSE ag.is_color_group END AS is_color, 
                CASE WHEN ama.id_attribute_group IS NOT NULL AND ama.id_attribute IS NOT NULL THEN 'true' ELSE NULL END AS selected 
            FROM ".self::$m_pref."amazon_model am  
            LEFT JOIN ".self::$m_pref."amazon_profile ap ON am.id_model = ap.id_model AND am.id_country = ap.id_country AND am.id_shop = ap.id_shop 
            LEFT JOIN ".self::$m_pref."amazon_category_selected acs ON ap.id_profile = acs.id_profile AND ap.id_country = acs.id_country AND ap.id_shop = acs.id_shop 
            LEFT JOIN ".self::$p_pref."product p ON p.id_category_default = acs.id_category AND p.id_shop = acs.id_shop  
            LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop  
            LEFT JOIN ".self::$p_pref."product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute AND pa.id_shop = pac.id_shop 
            LEFT JOIN ".self::$p_pref."attribute_group ag ON pac.id_attribute_group = ag.id_attribute_group AND pac.id_shop = ag.id_shop 
            LEFT JOIN ".self::$p_pref."attribute_group_lang agl ON pac.id_attribute_group = agl.id_attribute_group AND pac.id_shop = agl.id_shop 
            LEFT JOIN ".self::$p_pref."attribute_lang al ON pac.id_attribute = al.id_attribute AND pac.id_shop = al.id_shop 
            LEFT JOIN ".self::$m_pref."amazon_mapping_attribute ama ON pac.id_attribute_group = ama.id_attribute_group AND pac.id_attribute = ama.id_attribute AND am.id_country = ama.id_country AND am.id_shop = ama.id_shop 
            WHERE am.id_country = " . $id_country . " 
                AND am.id_shop = " . $id_shop . " 
                " . $model . " 
                " . $lang . " 
                " . $attribute_group . " 
                " . $attribute . "  
                " . $product_type_fields . " 
                " . $product_sub_type_fields . " 
                " . $attribute_field_fields . " 
            GROUP BY ama.product_type, ama.product_sub_type, ama.attribute_field, al.id_attribute, agl.id_lang            
            ORDER BY agl.name ASC";

        $result = $this->db->product->db_query_str($sql);

        foreach ($result as $keys => $value) {
            foreach ($value as $key => $val) {
                if (!is_int($key)) {
                    if (Amazon_Tools::is_serialized($val)) {
                        $content[$keys][$key] = unserialize($val);
                    } else {
                        $content[$keys][$key] = $val;
                    }
                }
            }
        }
        return $content;
    }

    public function get_feature_group_by_id($id_feature, $id_shop) {
        $content = $where = array();
       $this->db_from('feature', null , false);
        $where['id_feature'] = $id_feature;
        $where['id_shop'] = $id_shop;
        $this->db->product->where($where);
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $value) {
            $content = $value['name'];
        }
        return $content;
    }

    public function get_attribute_group_by_id($id_attribute_group, $id_shop, $id_lang) {
        $content = $where = array();
       $this->db_from('attribute_group_lang', null, false);
        $where['id_attribute_group'] = $id_attribute_group;
        $where['id_shop'] = $id_shop;
        $where['id_lang'] = $id_lang;
        $this->db->product->where($where);
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $value) {
            $content = $value['name'];
        }
        return $content;
    }

    public function get_amazon_model_by_id($id_model, $id_shop, $id_lang) {
        $content = $where = array();
       $this->db_from(self::$m_pref.'amazon_model');
        $where['id_model'] = $id_model;
        $where['id_shop'] = $id_shop;
        $this->db->product->where($where);
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $keys => $value) {
            $content[$keys]['product_category'] = $value['universe'];
            $content[$keys]['product_type'] = $value['product_type'];
            $content[$keys]['ProductSubtype'] = $value['sub_product_type'];
            $content[$keys]['VariationTheme'] = $value['variation_theme'];
            $content[$keys]['specific_options'] = $value['specific_options'];

            if (isset($value['variation_data']) && !empty($value['variation_data'])) {
                $variation_data = unserialize($value['variation_data']);
                $content[$keys]['variation_data'] = $variation_data;
                foreach ($variation_data as $vdk => $vdv) {
                    if (isset($vdv['attr']) && !empty($vdv['attr']))
                        $content[$keys]['amzn_attr'][$vdk] = $vdv['attr'];
                }
            }
            if (isset($value['recommended_data']) && !empty($value['recommended_data'])) {
                $recommended_data = unserialize($value['recommended_data']);

                $content[$keys]['recommended_data'] = $recommended_data;
                foreach ($recommended_data as $vdk => $vdv) {
                    if (isset($vdv['attr']) && !empty($vdv['attr'])) {
                        $content[$keys]['amzn_attr'][$vdk] = $vdv['attr'];
                        unset($content[$keys]['recommended_data'][$vdk]['attr']);
                    }
                }
            }
            if (isset($value['specific_fields']) && !empty($value['specific_fields'])) {
                $specific_fields = unserialize($value['specific_fields']);
                $content[$keys]['specific_fields'] = $specific_fields;
                foreach ($specific_fields as $vdk => $vdv) {
                    if (isset($vdv['attr']) && !empty($vdv['attr'])) {
                        $content[$keys]['amzn_attr'][$vdk] = $vdv['attr'];
                        unset($content[$keys]['specific_fields'][$vdk]['attr']);
                    }
                }
            }
        }
        return $content;
    }

    public function get_amazon_model($id_country = null, $id_mode = null, $id_shop = null, $id_model = null, $universe = true, $is_only_selected_category = false) {
        $mode = $shop = $country = $model = null;
        $content = $where = array();
       $this->db_from(self::$m_pref.'amazon_model');

        if (isset($id_mode) && !empty($id_mode)) {
            $mode = ' AND id_mode = ' . (int) $id_mode . ' ';
        }
        if (isset($id_shop) && !empty($id_shop)) {
            $shop = ' AND id_shop = ' . (int) $id_shop . ' ';
        }
        if (isset($id_country) && !empty($id_country)) {
            $country = ' AND id_country = ' . (int) $id_country . ' ';
        }
        if (isset($id_model) && !empty($id_model)) {
            $model = ' AND id_model = ' . (int) $id_model . ' ';
        }
        if ($is_only_selected_category) {
            $sql = "SELECT * FROM ".self::$m_pref."amazon_model 
                    WHERE id_model IN (
                        SELECT ap.id_model FROM ".self::$m_pref."amazon_category_selected acs 
                        LEFT JOIN ".self::$m_pref."amazon_profile ap ON ap.id_profile = acs.id_profile AND ap.id_shop = acs.id_shop
                        AND ap.id_country = acs.id_country 
                    ) $mode $country $shop  ORDER BY name; ";
        } else {
            $sql = "SELECT * FROM ".self::$m_pref."amazon_model WHERE 1 $mode $country $shop $model ORDER BY name; ";
        }
        
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $keys => $value) {
            foreach ($value as $key => $val) {
                if (!is_int($key)) {
                    if (!isset($universe) || empty($universe) || $universe == false) {
                        if ($key == "universe") {
                            $key = "product_category";
                        }
                    }
                    if (Amazon_Tools::is_serialized($val)) {
                        $content[$keys][$key] = unserialize($val);
                    } else {
                        $content[$keys][$key] = $val;
                    }
                }
            }
        }
        return $content;
    }

    public function get_amazon_profile($id_country = null, $id_mode = null, $id_shop = null, $id_profile = null) {

        $content = $where = array();
        if (isset($id_country) && !empty($id_country)) {
            $where['p.id_country'] = $id_country;
        }
        if (isset($id_mode) && !empty($id_mode)) {
            $where['p.id_mode'] = $id_mode;
        }
        if (isset($id_shop) && !empty($id_shop)) {
            $where['p.id_shop'] = $id_shop;
        }
        if (isset($id_profile) && !empty($id_profile)) {
            $where['p.id_profile'] = $id_profile;
        }

        $this->db->product->select(array('p.*', 'm.name as model_name'));
	$this->db_from(self::$m_pref.'amazon_profile p');
        $this->db_leftJoin(self::$m_pref.'amazon_model', 'm', '(p.id_model = m.id_model)');
        if (!empty($where)) {
            $this->db->product->where($where);
        }
        $this->db->product->orderBy('p.name');
        
        $result = $this->db->product->db_array_query($this->db->product->query);        
     
        foreach ($result as $keys => $value) {
            foreach ($value as $key => $val) {
                if(!isset($id_profile) || empty($id_profile)){
                    $keys = $value['id_profile'];
                }
                if (!is_int($key)) {
                    $key = str_replace(array('p.', 'm.'), '', $key);
                    if (Amazon_Tools::is_serialized($val)) {
                        $content[$keys][$key] = unserialize($val);                        
                    } else {
                        if($key == 'name') $val = str_replace (array("''"), array("'"), $val);
                        $content[$keys][$key] = $val;
                    }
                }
            }
        }
 
        return $content;
    }

    public function get_category_selected($id_country = null, $id_mode = null, $id_shop = null, $full = false, $id_profile=null) {

        $content = $where = array();
        if (isset($id_country) && !empty($id_country)) {
            $where['id_country'] = $id_country;
        }
        if (isset($id_mode) && !empty($id_mode)) {
            $where['id_mode'] = $id_mode;
        }
        if (isset($id_shop) && !empty($id_shop)) {
            $where['id_shop'] = $id_shop;
        }
        if (isset($id_profile) && !empty($id_profile)) {
            $where['id_profile'] = $id_profile;
        }

        $this->db_from(self::$m_pref.'amazon_category_selected');
        if (!empty($where)) {
            $this->db->product->where($where);
        }
        $result = $this->db->product->db_array_query($this->db->product->query);

        foreach ($result as $value) {
            if($full){
                $content[$value['id_category']]['id_category_selected'] =  $value['id_category_selected'];
                $content[$value['id_category']]['id_profile'] =  $value['id_profile'];
            } else {
                array_push($content, $value['id_category']);
            }
        }

        return $content;
    }

    public function get_id_profile_selected($id_category, $id_country = null, $id_mode = null, $id_shop = null) {

        $sql = "SELECT id_profile FROM ".self::$m_pref."amazon_category_selected 
                WHERE id_category = " . $id_category . "
                AND id_country = " . $id_country . "
                AND id_mode = " . $id_mode . " 
                AND id_shop = " . $id_shop . "; ";
        $result = $this->db->product->db_query_str($sql);

        if (isset($result[0]['id_profile']) && !empty($result[0]['id_profile'])) {
            return $result[0]['id_profile'];
        } else {
            return;
        }
    }

    public function get_profile_selected(/*$id_category,*/ $id_country = null, $id_mode = null, $id_shop = null, $mode = false) {

        $content = $where = array();
        //$where['id_category'] = $id_category;
        if (isset($id_country) && !empty($id_country)) {
            $where['id_country'] = $id_country;
        }
        if (isset($id_mode) && !empty($id_mode)) {
            $where['id_mode'] = $id_mode;
        }
        if (isset($id_shop) && !empty($id_shop)) {
            $where['id_shop'] = $id_shop;
        }

       $this->db_from(self::$m_pref.'amazon_category_selected');
        if (!empty($where)) {
            $this->db->product->where($where);
        }
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $value) {
            $id = $value['id_category'];
            $content[$id]['id_category_selected'] = $value['id_category_selected'];
            $content[$id]['id_category'] = $value['id_category'];
            $content[$id]['id_country'] = $value['id_country'];
            $content[$id]['id_shop'] = $value['id_shop'];
            $content[$id]['id_mode'] = $value['id_mode'];
            $content[$id]['id_profile'] = isset($value['id_profile']) ? $value['id_profile'] : (isset($value['id_profile_price']) ? $value['id_profile_price'] : null);
        }
        return $content;
    }

    public function delete_model($data) {
        if (!isset($data['id_model']) && empty($data['id_model']) && !isset($data['id_country']) && empty($data['id_country']) && !isset($data['id_shop']) && empty($data['id_shop']))
	{
            return false;
        }
        $table = self::$m_pref.'amazon_model';
        $where = array(
            "id_model" => array('operation' => "=", 'value' => $data['id_model']),
            "id_country" => array('operation' => "=", 'value' => $data['id_country']),
            "id_shop" => array('operation' => "=", 'value' => $data['id_shop']),
            "id_mode" => array('operation' => "=", 'value' => $data['id_mode']),
        );
        $sql = $this->delete_string($table, $where);
        return @$this->db->product->db_exec($sql);
    }

    public function delete_profile($data) {
        if (!isset($data['id_profile']) && empty($data['id_profile']) && !isset($data['id_country']) && empty($data['id_country']) && !isset($data['id_shop']) && empty($data['id_shop'])) {
            return false;
        }

        $table = self::$m_pref.'amazon_profile';
        $where = array(
            "id_profile" => array('operation' => "=", 'value' => $data['id_profile']),
            "id_country" => array('operation' => "=", 'value' => $data['id_country']),
            "id_shop" => array('operation' => "=", 'value' => $data['id_shop']),
            "id_mode" => array('operation' => "=", 'value' => $data['id_mode']),
        );
        $sql = $this->delete_string($table, $where);
        return @$this->db->product->db_exec($sql);
    }

    public function save_model($data, $id_shop, $id_country, $id_mode = null) {

        $table = self::$m_pref . 'amazon_model';
        $session_key = date('Y-m-d H:i:s');
        $sql = '';
        $model = array();

        if (isset($data) && !empty($data)) {
            foreach ($data as $value) {
                if (isset($value['name']) && !empty($value['name'])) {

                    $value['date_add'] = $session_key;
                    $value['id_country'] = $id_country;
                    $value['id_shop'] = $id_shop;
                    $value['id_mode'] = $id_mode;

                    foreach ($value as $k => $v) {

                        if (isset($value[$k])) {
                            if (is_array($value[$k])) {
                                foreach ($value[$k] as $variation_key => $variation_data) {
                                    $data = array();

                                    if (isset($variation_data['value']) && !empty($variation_data['value']))
                                        $data[$k] = explode("[", $variation_data['value']);

                                    if (isset($variation_data['CustomValue']) && $k == "specific_fields") {
                                        if (isset($value[$k][$variation_key]['value']))
                                            unset($value[$k][$variation_key]['value']);
                                    }

                                    if (empty($variation_data['value']))
                                        unset($value[$k][$variation_key]);

                                    if (isset($data[$k][1])) {
                                        $value[$k][$variation_key][$data[$k][0]] = str_replace("]", "", $data[$k][1]);
                                        unset($value[$k][$variation_key]['value']);
                                    }
                                }
                            } else {
                                if (empty($value[$k]))
                                    unset($value[$k]);
                                if (empty($value['variation_data'])) {
                                    unset($value['variation_data']);
                                    unset($value['variation_theme']);
                                }
                            }
                        }
                    }

		    $filter_data = $this->_filter_data($table, $value);		  
		    
                    if (isset($value['id_model']) && !empty($value['id_model'])) {
                        $where = array(
                            "id_model" => array('operation' => "=", 'value' => $value['id_model']),
                            "id_country" => array('operation' => "=", 'value' => $value['id_country']),
                        );
                        $sql .= $this->update_string($table, $filter_data, $where);
                        $model[$value['id_model']] = true;
                    } else {			
                        $sql .= $this->insert_string($table, $filter_data);
                    }
                }
            }
        }

        if (strlen($sql)) {

            if (!$this->db->product->db_exec($sql)) {
                return false;
            }
            if (count($model) > 0) {
                if (!$this->update_amazon_status($id_country, $id_shop, $session_key, 'models')) {
                    return 'status_fail';
                }
            }
        }
        return true;
    }

    public function duplicate_category_selected($data, $id_shop, $id_country) {

        $sql = $include_date = '';
        $date_add = date('Y-m-d H:i:s');
        $table = self::$m_pref ."amazon_category_selected";
        $list_id_category = $categories = array();

        //1.get category selected   
        foreach ($data['category'] as $val) {
            $list_id_category[] = $val['id_category'];
        }
       
        $sql_select = "SELECT id_country, id_category, id_profile FROM $table
                WHERE id_category IN (" .implode(", ", $list_id_category) . ")
                AND id_shop = " . $id_shop . "; ";

        $result = $this->db->product->db_query_str($sql_select);
        foreach ($result as $category) {
            $categories[$category['id_country']][$category['id_category']] = $category['id_profile'];
        }
        
        foreach ($categories as $country => $category){
            //if($country == $id_country)
                //continue;
            foreach ($list_id_category as $id_category){
                $val['id_country'] = $country;
                $val['id_shop'] = $id_shop;
                $val['id_mode'] = 1;
                $val['id_category'] = $id_category;
                $val['id_profile'] = isset($category[$id_category]) ? $category[$id_category] : null ;
                $val['date_add'] = $date_add;
                $filter_data = $this->_filter_data($table, $val);
                $sql .= $this->insert_string($table, $filter_data);
            }
        }
       
        if (strlen($sql)) {
            $include_date = " AND date_add < '" . $date_add . "'";
            if (!$this->db->product->db_exec($sql))
                return false;
        }

        $delete = "DELETE FROM ". $table . " "
                . "WHERE id_shop = " . $id_shop . " AND id_country != " . $id_country . $include_date . ";";
         
        return $this->db->product->db_exec($delete);
    }

    public function save_category_selected($data, $id_shop, $id_mode, $id_country, $wizard = false) {
        
        $sql = $include_date = '';
        $date_add = date('Y-m-d H:i:s');
        $table = self::$m_pref ."amazon_category_selected";

        if (isset($data['category']) && !empty($data['category'])) {
            foreach ($data['category'] as $val) {
                $val['id_country'] = $id_country;
                $val['id_shop'] = $id_shop;
                $val['id_mode'] = $id_mode;
                $val['date_add'] = $date_add;
                $filter_data = $this->_filter_data($table, $val);
                if (isset($val['id_category']) && !empty($val['id_category'])) {
                    $sql .= $this->insert_string($table, $filter_data);
                }
            }
            if (strlen($sql)) {
                $include_date = " AND date_add < '" . $date_add . "'";
                if (!$this->db->product->db_exec($sql))
                    return false;
            }
        }
       
        $delete = "DELETE FROM ". $table . " "
                . "WHERE id_shop = " . $id_shop . " AND id_mode = " . $id_mode . " AND id_country = " . $id_country . $include_date . ";";
        return @$this->db->product->db_exec($delete);
    }

    public function save_mapping_attribute($datas, $id_shop, $id_mode, $id_country) {

        $include_date = $string = '';
        $date_add = date('Y-m-d H:i:s');

        if (isset($datas['attribute']) && !empty($datas['attribute'])) {
            foreach ($datas['attribute'] as $key => $value) {
                $id_attribute_group = $key;
                $is_color = 0;
                foreach ($value as $ke => $val) {
                    $id_attribute = $ke;
                    foreach ($val as $k => $v) {
                        $product_type = $k;
                        foreach ($v as $kk => $vv) {
                            $product_sub_type = $kk;
                            foreach ($vv as $kkk => $vvv) {
                                $attribute_field = $kkk;
                                if ($attribute_field == 'Color') {
                                    $is_color = 1;
                                }
                                if (!empty($vvv)) {
                                    $string .= 'INSERT INTO '.self::$m_pref.'amazon_mapping_attribute (id_shop, id_mode, id_country, id_attribute_group, id_attribute, is_color, product_type, product_sub_type, attribute_field,mapping, date_add ) VALUES (' . (int) $id_shop . ',' . (int) $id_mode . ',' . (int) $id_country . ',' . (int) $id_attribute_group . ',' . (int) $id_attribute . ',' . $is_color . ',"' . $product_type . '","' . $product_sub_type . '","' . $attribute_field . '","' . $vvv . '","' . $date_add . '"); ';
                                }
                            }
                        }
                    }
                }
            }
            if (strlen($string)) {
                $include_date = " AND date_add < '" . $date_add . "'";
                //$this->db->product->db_trans_begin();
                if (!$this->db->product->db_exec($string)) {
                    return false;
                }
                //$this->db->product->db_trans_commit();
            }
        }

        $delete = "DELETE FROM ".self::$m_pref."amazon_mapping_attribute WHERE id_shop = " . $id_shop . " AND id_mode = " . $id_mode . " AND id_country = " . $id_country . $include_date . ";";
        return @$this->db->product->db_exec($delete);
    }

    public function save_mapping_feature($datas, $id_shop, $id_mode, $id_country) {

        $date_add = date('Y-m-d H:i:s');
        $string = $include_date = '';

        if (isset($datas['feature']) && !empty($datas['feature'])) {
            foreach ($datas['feature'] as $key => $value) {
                $id_feature = $key;
                $is_color = 0;
                foreach ($value as $ke => $val) {
                    $id_feature_value = $ke;
                    foreach ($val as $k => $v) {
                        $product_type = $k;
                        foreach ($v as $kk => $vv) {
                            $product_sub_type = $kk;
                            foreach ($vv as $kkk => $vvv) {
                                $attribute_field = $kkk;
                                if ($attribute_field == 'Color') {
                                    $is_color = 1;
                                }
                                if (!empty($vvv)) {
                                    $string .= 'INSERT INTO '.self::$m_pref.'amazon_mapping_feature (id_shop, id_mode, id_country, id_feature, id_feature_value, is_color, product_type, product_sub_type, attribute_field,mapping, date_add ) VALUES (' . (int) $id_shop . ',' . (int) $id_mode . ',' . (int) $id_country . ',' . (int) $id_feature . ',' . (int) $id_feature_value . ',' . $is_color . ',"' . $product_type . '","' . $product_sub_type . '","' . $attribute_field . '","' . $vvv . '","' . $date_add . '"); ';
                                }
                            }
                        }
                    }
                }
            }
            if (strlen($string)) {
                $include_date = " AND date_add < '" . $date_add . "'";               
                if (!$this->db->product->db_exec($string)) {
                    return false;
                }                
            }
        }

        $delete = "DELETE FROM ".self::$m_pref."amazon_mapping_feature WHERE id_shop = " . $id_shop . " AND id_mode = " . $id_mode . " AND id_country = " . $id_country . $include_date . ";";
        return @$this->db->product->db_exec($delete);
    }

    public function save_mapping_carrier($datas, $id_shop, $id_mode, $id_country) {

        require_once dirname(__FILE__) . '/../controllers/amazon.display.carrier.php';

        $sql = $include_date = '';
        $table = self::$m_pref . 'amazon_mapping_carrier';
        $date_add = date('Y-m-d H:i:s');
	$types = $carriers = array();

        // get carriers
        $list_carriers = $this->feedbiz->getCarriers($this->user, $id_shop);
        foreach ($list_carriers as $carrier){
            $carriers[$carrier['id_carrier']] = $carrier;
        }
        if (isset($datas['carrier']) && !empty($datas['carrier'])) {
            foreach ($datas['carrier'] as $carrier_key => $carrier_value) {
                $type = $carrier_key;                
                foreach ($carrier_value as $key => $val) {
                    $value = array();
                    $value['date_add'] = $date_add;
                    $value['id_country'] = (int)$id_country;
                    $value['id_shop'] = (int)$id_shop;
                    $value['id_mode'] = (int)$id_mode;
                    $value['type'] = $type;

                    if ($type == "incoming") {
                        $value['id_carrier'] = (int)$val['id_carrier'];   
                        $value['id_carrier_ref'] = isset($carriers[$value['id_carrier']]['id_carrier_ref']) ?
                            $carriers[$value['id_carrier']]['id_carrier_ref'] : $value['id_carrier'];
                        $value['mapping'] = $key;
                    } else if($type == 'outgoing') {
                        $carrier_codes = Amazon_Carrier::$carrier_codes;
                        $value['mapping'] = (isset($val['id_carrier']) && isset($carrier_codes[$val['id_carrier']])) ? $carrier_codes[$val['id_carrier']] : 'Other';
                        $value['id_carrier'] = (int)$key;
                        $value['id_carrier_ref'] = isset($carriers[$value['id_carrier']]['id_carrier_ref']) ?
                            $carriers[$value['id_carrier']]['id_carrier_ref'] : $value['id_carrier'];
                        if (isset($val['other']) && isset($val['id_carrier']) && $val['id_carrier'] == "Other") {
                            $value['other'] = $val['other'];
                        }
                    } else if($type == 'shipping_override') {
                        if($key == Amazon_Carrier::SHIPPING_STANDARD){
                            $ShippingStandard = Amazon_Carrier::getShippingMethods();
                            $value['mapping'] = isset($ShippingStandard[$val]) ? $ShippingStandard[$val] : $val;
                            $value['id_carrier'] = null;
                            $value['other'] = $key;
                        }
                        if($key == Amazon_Carrier::SHIPPING_EXPRESS){
                            $ShippingExpress = Amazon_Carrier::getShippingMethods($key);
                            $value['mapping'] = isset($ShippingExpress[$val]) ? $ShippingExpress[$val] : $val;
                            $value['id_carrier'] = null;
                            $value['other'] = $key;
                        }
                    }  else {
			$value['mapping'] = $val['id_carrier'];
			$value['id_carrier'] = (int)$key;
                        $value['id_carrier_ref'] = isset($carriers[$value['id_carrier']]['id_carrier_ref']) ?
                            $carriers[$value['id_carrier']]['id_carrier_ref'] : $value['id_carrier'];
		    }       
		    
                    if (!empty($val))
                        $sql .= $this->insert_string($table, $this->_filter_data($table, $value));
                }
		array_push($types, $type);
            }
            
	    if(!empty($types)) {
		$include_date .=  " AND type IN ('" . implode("', '", $types) . "')";
	    }
	    
            if (strlen($sql)) {
                $include_date .= " AND date_add < '" . $date_add . "'";
                if (!$this->db->product->db_exec($sql, true))
                    return false;
            }
        }

        $delete = "DELETE FROM ". $table . " WHERE id_shop = " . $id_shop . " AND id_mode = " . $id_mode . " AND id_country = " . $id_country . $include_date . ";";
        
        return @$this->db->product->db_exec($delete);
    }

    public function save_mapping_valid_value_custom($datas, $region) {
        if (!isset($region) || empty($region))
            return false;

        $custom_value = array();
        $region = Amazon_Tools::domainToId($region);
        $date_add = date('Y-m-d H:i:s');
        $string = $include_date = $delete = '';

        if (isset($datas['custom_value']) && !empty($datas['custom_value'])) {
            foreach ($datas['custom_value'] as $universe => $value) {
                foreach ($value as $product_type => $val) {
                    foreach ($val as $fields => $v) {
                        $attribite = explode(',', $v);
                        foreach ($attribite as $attr) {
                            if (!empty($attr)) {
                                $custom_value[$universe][$product_type][$fields][] = trim($attr);
                                $string .= "INSERT INTO ".self::$m_pref."amazon_valid_values_custom (region, universe, product_type, attribute_field, valid_value, date_upd) VALUES ('" . $region . "','" . $universe . "','" . $product_type . "','" . $fields . "','" . trim($attr) . "','$date_add'); ";
                            }
                        }
                    }
                    $include_date = " AND date_upd < '" . $date_add . "'";
                    $delete .= "DELETE FROM ".self::$m_pref."amazon_valid_values_custom WHERE region='$region' AND universe='$universe' AND product_type='$product_type' $include_date ; ";
                }
            }
            if (strlen($string)) {                
                if (!$this->db->product->db_exec($string)) {
                    return false;
                }               
            }
        }
        if (strlen($delete))
            @$this->db->product->db_exec($delete);
        return $custom_value;
    }

    public function get_amazon_submission_result_log($id_country, $id_shop, $submission_id = null, $batch_id = null, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false) {

        $table = self::$m_pref.'amazon_submission_result_log';
        $content = $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

        if (isset($submission_id) && !empty($submission_id)) {
            $where['id_submission_feed'] = $submission_id;
        }
        if (isset($batch_id) && !empty($batch_id)) {
            $where['batch_id'] = $batch_id;
        }
        if (isset($order_by) && !empty($order_by)) {
            $this->db->product->orderBy($order_by);
        }
        if (isset($start) && isset($limit)) {
            $this->db->product->limit($limit, $start);
        }
       $this->db_from($table);
        if (!empty($where)) {
            $this->db->product->where($where);
        }
        if ($num_rows) {

            $result = $this->db->product->db_sqlit_query($this->db->product->query_string($this->db->product->query));
            $row = $this->db->product->db_num_rows($result);
            return $row;
        } else {

            $result = $this->db->product->db_array_query($this->db->product->query);
            foreach ($result as $value) {
                $content['batch_id'] = $value['batch_id'];
                $content['date'] = $value['date_add'];
                $content['error'][$value['id_log']]['id_submission_feed'] = $value['id_submission_feed'];
                $content['error'][$value['id_log']]['sku'] = $value['sku'];
                $content['error'][$value['id_log']]['id_message'] = $value['id_message'];
                $content['error'][$value['id_log']]['result_code'] = $value['result_code'];
                $content['error'][$value['id_log']]['result_message_code'] = $value['result_message_code'];
                $content['error'][$value['id_log']]['result_description'] = $value['result_description'];
            }
            return $content;
        }
    }

    public function downloadLog($data) {
        $batch_id = date('Ymd_His');
        if (isset($data['batch_id']) && !empty($data['batch_id'])) {
            $batch_id = $data['batch_id'];
        }
        $list = array();
        $result = $this->get_amazon_submission_result_log($data['id_country'], $data['id_shop'], $data['id_submission_feed'], $data['batch_id']);
        $list['header']['SKU'] = "SKU";
        $list['header']['ResultMessageCode'] = "Error Code";
        $list['header']['ResultDescription'] = "Message";
        if (isset($result['error']) && !empty($result['error'])) {
            foreach ($result['error'] as $key => $log) {
                $list[$key]['SKU'] = $log['sku'];
                $list[$key]['ResultMessageCode'] = $log['result_message_code'];
                $list[$key]['ResultDescription'] = $log['result_description'];
            }
        }
        ob_start();
        ob_get_clean();
        $filename = 'amazon_report_' . $batch_id . '.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) {
            fputcsv($h, $data, ';');
        }
    }

    public function getAsinBySKU($id_country, $id_shop, $SKU) {
        $sql = "SELECT asin FROM ".self::$m_pref."amazon_report_inventory WHERE id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country." AND sku = '".$SKU."' ";
        $sql .= "UNION SELECT asin FROM ".self::$m_pref."amazon_inventory WHERE id_shop = ".(int)$id_shop." AND id_country = ".(int)$id_country." AND sku = '".$SKU."'; ";

        $result = $this->db->product->db_query_str($sql);

        if (isset($result[0]['asin']) && !empty($result[0]['asin'])) {
            return $result[0]['asin'];
        }
        return null;
    }

    public function getIdByAsin($id_country, $id_shop, $ASIN) {
        
        $sql = "SELECT sku FROM ".self::$m_pref."amazon_report_inventory WHERE id_country = $id_country AND id_shop = $id_shop AND asin = '$ASIN' ";
        $sql .= "UNION SELECT sku FROM ".self::$m_pref."amazon_inventory WHERE id_country = $id_country AND id_shop = $id_shop AND asin = '$ASIN'; ";

        $result = $this->db->product->db_sqlit_query($sql);
        $rows = $this->db->product->db_num_rows($result);
 
        if (isset($rows) && !empty($rows) && $rows > 0) {
            return true;
        }
        return false;
    }
    
    public function getIdByAsins($id_country, $id_shop, $ASINs) {
        
        $asin1 = explode("','", str_replace(" ", "", $ASINs));
        $asin2 = array();
        $sql = "SELECT asin,sku FROM ".self::$m_pref."amazon_report_inventory WHERE id_country = $id_country AND id_shop = $id_shop AND asin IN ('$ASINs') ";
        $sql .= "UNION SELECT asin,sku FROM ".self::$m_pref."amazon_inventory WHERE id_country = $id_country AND id_shop = $id_shop AND asin IN ('$ASINs') ";
        //$sql .= "UNION SELECT asin1 AS asin,sku FROM ".self::$m_pref."amazon_product_option WHERE id_country = $id_country AND id_shop = $id_shop AND asin1 IN ('$ASINs'); ";
        $result = $this->db->product->db_query_str($sql);
        
        foreach ($result as $data){
            $asin2[] = $data['asin'];            
        }
      
        return array_diff($asin1, $asin2);
    }
    
    // exclude deleted products
    public function getProductsInventory($id_country, $id_shop, $not_in_delete=true) {
        $products = array();
        $sql = "SELECT id_product, id_product_attribute,sku FROM ".self::$m_pref."amazon_inventory "
		. "WHERE id_country = $id_country AND id_shop = $id_shop " ;
        if($not_in_delete) {
            $sql .= "AND action_type NOT IN ('delete') ";
        }
      
        $result = $this->db->product->db_query_str($sql);
        
        foreach ($result as $data){
            $products[$data['sku']]['id_product'] = $data['id_product'];            
            $products[$data['sku']]['id_product_attribute'] = $data['sku'];            
            $products[$data['sku']]['sku'] = $data['id_product_attribute'];            
        }
      
        return $products;
    }
    
    public function getProductsByASINs($id_shop, $id_country, $ASINs = null, $id_lang = null){
        
        if (!isset($id_shop) || empty($id_shop) || !isset($id_country) || empty($id_country))
            return;
	
        $products = $history = array();        
        $_db = $this->db->product;            
        
        /* Get product by ASIN */
        $sql = "SELECT p.id_product AS id_product,
		p.id_category_default AS id_category_default,
		pa.id_product_attribute AS id_product_attribute,
                p.id_condition AS id_condition,
		p.id_currency AS id_currency,
		p.active AS active,
		p.on_sale AS on_sale,
		CASE 
                    WHEN pa.quantity IS NOT NULL THEN pa.quantity                         
                    WHEN p.quantity IS NOT NULL THEN p.quantity                        
		END AS quantity,
		CASE 
                    WHEN pa.price IS NOT NULL THEN pa.price                         
                    WHEN p.price IS NOT NULL THEN p.price                        
		END AS price,
		CASE 
                    WHEN pa.wholesale_price IS NOT NULL THEN pa.wholesale_price                         
                    WHEN p.wholesale_price IS NOT NULL THEN p.wholesale_price                        
		END AS wholesale_price,
		CASE 
                    WHEN pa.reference IS NOT NULL THEN pa.reference 
                    WHEN p.reference IS NOT NULL THEN p.reference 
		END AS sku,		
		/*ai.asin AS asin*/ 
		CASE
                    WHEN ari.asin IS NOT NULL THEN ari.asin
                    WHEN ai.asin IS NOT NULL THEN ai.asin 
		END AS asin
                FROM ".self::$m_pref."amazon_inventory ai  
                LEFT JOIN ".self::$m_pref."amazon_report_inventory ari ON ai.sku = ari.sku AND ai.id_shop = ari.id_shop  AND ai.id_country  = ari.id_country 
                LEFT JOIN ".self::$p_pref."product p ON p.id_product = ai.id_product AND p.id_shop = ai.id_shop 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop AND pa.id_product_attribute = ai.id_product_attribute
                WHERE p.id_shop = $id_shop AND ai.id_country = $id_country AND ai.id_shop = $id_shop 
                AND ( ai.asin IN ('$ASINs') OR ari.asin IN ('$ASINs') ) AND (p.date_upd IS NULL OR p.date_upd = '') 
                ORDER BY p.id_product,pa.id_product_attribute; "; 
	
	//file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/getProductsByASINs.txt', $sql);
	
        $result = $_db->db_query_str($sql);
        
        foreach ($result as $data) {
            
            if(!isset($history['id_product'][(int)$data['id_product']])){
                $list_id_product[] = (int)$data['id_product'];
            }
            if(!isset($history['id_product_attribute'][(int)$data['id_product_attribute']])){
                $list_id_product_attribute[] = (int)$data['id_product_attribute'];
            }
            
            $history['id_product'][(int)$data['id_product']] = true;
            $history['id_product_attribute'][(int)$data['id_product_attribute']] = true;
            
            $products[$data['asin']] = new stdClass();            
            $products[$data['asin']]->sku = $data['sku'];
            $products[$data['asin']]->id_product = (int)$data['id_product'];
            $products[$data['asin']]->id_product_attribute = (int)$data['id_product_attribute'];
            $products[$data['asin']]->id_category_default = (int)$data['id_category_default'];
            $products[$data['asin']]->quantity = (int)$data['quantity'];
            $products[$data['asin']]->price = (float)$data['price'];
            $products[$data['asin']]->wholesale_price = (float)$data['wholesale_price'];
            $products[$data['asin']]->id_condition = (int)$data['id_condition'];
            $products[$data['asin']]->id_currency = (int)$data['id_currency'];
            $products[$data['asin']]->active = (int)$data['active'];            
            $products[$data['asin']]->on_sale = (int)$data['on_sale'];            
        }
        
        if(!empty($products)) {
            
	    // get id_lang 
	    // $this->feedbiz->checkLanguage();
	    
            /* Get offers */
            $offers = $this->getOffersByIDs($id_shop, $list_id_product, $list_id_product_attribute);
            
            /* Marketplace Product Option */
	    $MarketplaceProductOption = new MarketplaceProductOption($this->user);
            $Marketplace_product_options = $MarketplaceProductOption->get_product_option_by_ids($list_id_product, $id_shop, $id_lang);
            
            /* Amazon Product Option */
	    $AmazonProductOption = new MarketplaceProductOption($this->user, null, null, null, null, $id_country, _ID_MARKETPLACE_AMZ_);
            $Amazon_product_options = $AmazonProductOption->get_product_option_by_ids($list_id_product, $id_shop);
            
            foreach ($products as $k => $p){
                
                $id_p = $p->id_product;
                $id_pa = $p->id_product_attribute;
                
                // Price Override
                if(isset($Amazon_product_options[$id_p][$id_pa]['price']) && !empty($Amazon_product_options[$id_p][$id_pa]['price'])){
                    $products[$k]->price = (float)$Amazon_product_options[$id_p][$id_pa]['price'];
                } else if(isset($Marketplace_product_options[$id_p][$id_pa]['price']) && !empty($Marketplace_product_options[$id_p][$id_pa]['price'])){
                    $products[$k]->price = (float)$Marketplace_product_options[$id_p][$id_pa]['price'];
                } else if(isset($offers[$id_p][$id_pa]['price']) && !empty($offers[$id_p][$id_pa]['price'])){
                    $products[$k]->price = (float)$offers[$id_p][$id_pa]['price'];
                }
                
                // disabled
                if(isset($Amazon_product_options[$id_p][$id_pa]['disable']) && $Amazon_product_options[$id_p][$id_pa]['disable'] == 1){
                    $products[$k]->disabled = true;
                } else if(isset($Marketplace_product_options[$id_p][$id_pa]['disable']) && $Marketplace_product_options[$id_p][$id_pa]['disable'] == 1){
                    $products[$k]->disabled = true;
                } 
                
                // fba
                if(isset($Amazon_product_options[$id_p][$id_pa]['fba']) && $Amazon_product_options[$id_p][$id_pa]['fba'] == 1){
                    $products[$k]->fba = true;
		    if(isset($Amazon_product_options[$id_p][$id_pa]['fba_value'])){
			$products[$k]->fba_value = $Amazon_product_options[$id_p][$id_pa]['fba_value'];
		    }
                } else if(isset($Marketplace_product_options[$id_p][$id_pa]['fba']) && $Marketplace_product_options[$id_p][$id_pa]['fba'] == 1){
                    $products[$k]->fba = true;
		    if(isset($Marketplace_product_options[$id_p][$id_pa]['fba_value'])){
			$products[$k]->fba_value = $Marketplace_product_options[$id_p][$id_pa]['fba_value'];
		    }
                } 
                if(isset($offers[$id_p][$id_pa]['quantity']) && !empty($offers[$id_p][$id_pa]['quantity'])){
                     $products[$k]->quantity = (int)$offers[$id_p][$id_pa]['quantity'];
                }
                if(isset($offers[$id_p][$id_pa]['wholesale_price']) && !empty($offers[$id_p][$id_pa]['wholesale_price'])){
                     $products[$k]->wholesale_price = (float)$offers[$id_p][$id_pa]['wholesale_price'];
                }
                if(isset($offers[$id_p][$id_pa]['id_condition']) && !empty($offers[$id_p][$id_pa]['id_condition'])){
                     $products[$k]->id_condition = (int)$offers[$id_p][$id_pa]['id_condition'];
                }
                if(isset($offers[$id_p][$id_pa]['id_currency']) && !empty($offers[$id_p][$id_pa]['id_currency'])){
                     $products[$k]->id_currency = (int)$offers[$id_p][$id_pa]['id_currency'];
                }
                if(isset($offers[$id_p][$id_pa]['on_sale']) && !empty($offers[$id_p][$id_pa]['on_sale'])){
                     $products[$k]->on_sale = (int)$offers[$id_p][$id_pa]['on_sale'];
                }
            }
        }
        
        return $products;        
    }
    
    public function getOffersByIDs($id_shop, $list_id_product, $list_id_product_attribute){
        
        if(!empty($list_id_product) || !empty($list_id_product_attribute)){
            
            $product_attribute = '';
            $offers = array();
            $_db_offer = $this->db->offer; 
            
            if(!empty($list_id_product_attribute)){
                $product_attribute = " AND pa.id_product_attribute IN (".  implode(", ", $list_id_product_attribute).")";
            }
            
            $sql = "SELECT p.id_product AS id_product, pa.id_product_attribute AS id_product_attribute,
                        CASE 
                            WHEN pa.quantity IS NOT NULL THEN pa.quantity                         
                            WHEN p.quantity IS NOT NULL THEN p.quantity                        
                        END AS quantity,
                        CASE 
                            WHEN pa.price IS NOT NULL THEN pa.price                         
                            WHEN p.price IS NOT NULL THEN p.price                        
                        END AS price,
                        CASE 
                            WHEN pa.wholesale_price IS NOT NULL THEN pa.wholesale_price                         
                            WHEN p.wholesale_price IS NOT NULL THEN p.wholesale_price                        
                        END AS wholesale_price,
                        p.id_condition AS id_condition, p.id_currency AS id_currency, p.on_sale AS on_sale
                    FROM ".self::$o_pref."product p 
                    LEFT JOIN ".self::$o_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop                 
                    WHERE p.id_shop = " . $id_shop . " AND p.id_product IN (".  implode(", ", $list_id_product).") $product_attribute; ";

            $result = $_db_offer->db_query_str($sql);
            
            foreach ($result as $data) {
                $offers[$data['id_product']][$data['id_product_attribute']]['id_product'] = (int)$data['id_product'];
                $offers[$data['id_product']][$data['id_product_attribute']]['id_product_attribute'] = (int)$data['id_product_attribute'];
                $offers[$data['id_product']][$data['id_product_attribute']]['quantity'] = (int)$data['quantity'];
                $offers[$data['id_product']][$data['id_product_attribute']]['price'] = (float)$data['price'];
                $offers[$data['id_product']][$data['id_product_attribute']]['wholesale_price'] = isset($data['wholesale_price']) ? (float)$data['wholesale_price'] : 0;
                $offers[$data['id_product']][$data['id_product_attribute']]['id_condition'] = (int)$data['id_condition'];
                $offers[$data['id_product']][$data['id_product_attribute']]['id_currency'] = (int)$data['id_currency'];
                $offers[$data['id_product']][$data['id_product_attribute']]['on_sale'] = (int)$data['on_sale'];
            }
        }
        return $offers;
    }
    
    public function getProductBySKU($id_shop, $SKU=null, $bulk=false, $join_offers=false, $include_offer_option=false, $id_country=null, $id_lang=null) {

        if (!isset($id_shop) || empty($id_shop)/*|| !isset($SKU) || empty($SKU)*/)
            return;

	$join = $join_select_qty1 = $join_select_qty2 = $join_select_ref = '';
        $products = array();
        $_db = $this->db->product;
	
	if($join_offers){
	    $join_select_qty1= " WHEN fa.id_product_attribute IS NOT NULL THEN fa.id_product_attribute ";
	    $join_select_qty2 = " WHEN fa.quantity IS NOT NULL THEN fa.quantity WHEN f.quantity IS NOT NULL THEN f.quantity ";
	    $join_select_ref = " WHEN fa.reference IS NOT NULL THEN fa.reference WHEN f.reference IS NOT NULL THEN f.reference   ";
	    $join = "LEFT JOIN ".self::$o_pref."product f ON f.id_product = p.id_product AND f.id_shop = p.id_shop 
		    LEFT JOIN ".self::$o_pref."product_attribute fa ON fa.id_product = f.id_product AND fa.id_shop = f.id_shop";
	}
	
        $sql = "SELECT p.id_product AS id_product, p.id_category_default AS id_category_default, 
		    CASE  
			$join_select_qty1
			WHEN pa.id_product_attribute IS NOT NULL THEN pa.id_product_attribute 
		    END AS id_product_attribute , 
		    CASE $join_select_qty2 
			WHEN pa.quantity IS NOT NULL THEN pa.quantity 
			WHEN p.quantity IS NOT NULL THEN p.quantity 
		    END AS quantity,
		    CASE $join_select_ref
			WHEN pa.reference IS NOT NULL THEN pa.reference 
			WHEN p.reference IS NOT NULL THEN p.reference 
		    END AS sku
                FROM ".self::$p_pref."product p 
                LEFT JOIN ".self::$p_pref."product_attribute pa ON pa.id_product = p.id_product AND pa.id_shop = p.id_shop 
		$join WHERE p.id_shop = " . $id_shop . " AND (p.date_upd IS NULL OR p.date_upd = '') ";

        //if($join_offers){
            //$sql .= " GROUP BY id_product, id_product_attribute, sku ";
        //}

        if (isset($SKU) && !empty($SKU)){
	    if(is_array($SKU)){
		$sql .= " AND (pa.reference IN ('".  implode("','", $SKU)."') OR p.reference IN ('".  implode("','", $SKU)."') ) ";
	    } else {
		if(!$bulk){
		    $sql .= " AND (pa.reference = CASE WHEN pa.reference = '".$SKU."' THEN '".$SKU."' 
			    END OR p.reference = CASE WHEN p.reference = '".$SKU."' THEN '".$SKU."' END)";
		}
	    }
        }
	
        $result = $_db->db_query_str($sql . " ; " );

        if(!$bulk){
            foreach ($result as $product) {
                $products['id_product'] = (int)$product['id_product'];
                $products['id_product_attribute'] = (int)$product['id_product_attribute'];
                $products['id_category_default'] = (int)$product['id_category_default'];
                $products['quantity'] = (int)$product['quantity'];
                $products['sku'] = $product['sku'];
            }
        } else {
            foreach ($result as $product) {
                $key = $product['sku'];
                $products[$key]['id_product'] = (int)$product['id_product'];
                $products[$key]['id_product_attribute'] = (int)$product['id_product_attribute'];
                $products[$key]['id_category_default'] = (int)$product['id_category_default'];
                $products[$key]['quantity'] = (int)$product['quantity'];
                $products[$key]['sku'] = $product['sku'];
		
		if($include_offer_option)
		    $list_id_product[] = $products[$key]['id_product'];
            }      	    
	    
	    if($include_offer_option) {

		/* Marketplace Product Option */
		$MarketplaceProductOption = new MarketplaceProductOption($this->user);
		$Marketplace_product_options = $MarketplaceProductOption->get_product_option_by_ids($list_id_product, $id_shop, $id_lang);

		/* Amazon Product Option */
		$AmazonProductOption = new MarketplaceProductOption($this->user, null, null, null, null, $id_country, _ID_MARKETPLACE_AMZ_);
		$Amazon_product_options = $AmazonProductOption->get_product_option_by_ids($list_id_product, $id_shop);

		foreach ($products as $k => $p){
		  
		    $id_p = (int)$p['id_product'];
		    $id_pa = (int)$p['id_product_attribute'];
                    
		    // disabled
		    if(isset($Amazon_product_options[$id_p][$id_pa]['disable']) && $Amazon_product_options[$id_p][$id_pa]['disable'] == 1){
			$products[$k]['disabled'] = true;
		    } else if(isset($Marketplace_product_options[$id_p][$id_pa]['disable']) && $Marketplace_product_options[$id_p][$id_pa]['disable'] == 1){
			$products[$k]['disabled'] = true;
		    } 

		    // fba
		    if(isset($Amazon_product_options[$id_p][$id_pa]['fba']) && $Amazon_product_options[$id_p][$id_pa]['fba'] == 1){
			$products[$k]['fba'] = true;
			$products[$k]['fba_value'] = isset($Amazon_product_options[$id_p][$id_pa]['fba_value']) ? $Amazon_product_options[$id_p][$id_pa]['fba_value'] : null;
		    } else if(isset($Marketplace_product_options[$id_p][$id_pa]['fba']) && $Marketplace_product_options[$id_p][$id_pa]['fba'] == 1){
			$products[$k]['fba'] = true;
			$products[$k]['fba_value'] = $Marketplace_product_options[$id_p][$id_pa]['fba_value'];
		    } 
		    
		    // force
		    if(isset($Amazon_product_options[$id_p][$id_pa]['force']) && !empty($Amazon_product_options[$id_p][$id_pa]['force']) ){
			 $products[$k]['force'] = (int) $Amazon_product_options[$id_p][$id_pa]['force'];
		    }
		}
            }

        }   

        return $products;
    }

    public function get_report_inventory($id_country, $id_shop, $sku = null) {

        $content = $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

        if (isset($sku) && !empty($sku)) {
            $where['sku'] = $sku;
        }
       $this->db_from('amazon_report_inventory');
        if (!empty($where)) {
            $this->db->product->where($where);
        }
        $this->db->product->groupBy('sku');
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $keys => $value) {
            $content[$keys]['sku'] = $value['sku'];
            $content[$keys]['asin'] = $value['asin'];
            $content[$keys]['price'] = $value['price'];
            $content[$keys]['quantity'] = $value['quantity'];
        }
        return $content;
    }

    public function get_submission_list_log($id_country, $id_shop, $batch_id = null, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false) {

        $content = $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

        if (isset($batch_id) && !empty($batch_id)) {
            $where['batch_id'] = $batch_id;
        }

       $this->db_from('amazon_submission_list_log');

        if (!empty($where)) {
            $this->db->product->where($where);
        }
        if (isset($search) && !empty($search)) {
            if (isset($search['all']) && !empty($search['all'])) {
                $val = $search['all'];
                $this->db->product->where_like(array('batch_id' => $val, 'action_type' => $val, 'feed_type' => $val, 'message' => $val, 'date_add' => $val), ' OR ');
            } else {
                $this->db->product->where_like($search);
            }
        }
        if (isset($order_by) && !empty($order_by)) {
            $this->db->product->orderBy($order_by);
        } else {
            $this->db->product->orderBy('date_add DESC');
        }
        if (isset($start) && isset($limit)) {
            $this->db->product->limit($limit, $start);
        }
        if ($num_rows) {

            $result = $this->db->product->db_sqlit_query($this->db->product->query_string($this->db->product->query));
            $row = $this->db->product->db_num_rows($result);
            return $row;
        } else {

            $result = $this->db->product->db_array_query($this->db->product->query);
            foreach ($result as $keys => $value) {

                $content[$keys]['batch_id'] = $value['batch_id'];
                $content[$keys]['action_type'] = $value['action_type'] == ucwords(AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE :
                        ($value['action_type'] == ucwords(AmazonDatabase::CREATE) ? AmazonDatabase::CREATION : ucwords($value['action_type']));
                $content[$keys]['feed_type'] = $value['feed_type'];
                $message[$keys] = unserialize(base64_decode($value['message']));

                if (isset($message[$keys]['FeedSunmissionId']))
                    $content[$keys]['feed_sunmission_id'] = $message[$keys]['FeedSunmissionId'];

                if (isset($message[$keys]['StatusCode']))
                    $content[$keys]['status_code'] = $message[$keys]['StatusCode'];

                if (isset($message[$keys]['MessagesProcessed']))
                    $content[$keys]['messages_processed'] = $message[$keys]['MessagesProcessed'];

                if (isset($message[$keys]['MessagesSuccessful']))
                    $content[$keys]['messages_successful'] = $message[$keys]['MessagesSuccessful'];

                if (isset($message[$keys]['MessagesWithError']))
                    $content[$keys]['messages_with_error'] = $message[$keys]['MessagesWithError'];

                if (isset($message[$keys]['MessagesWithWarning']))
                    $content[$keys]['messages_with_warning'] = $message[$keys]['MessagesWithWarning'];

                $content[$keys]['date_add'] = $value['date_add'];
            }
        }
        return $content;
    }

    public function get_request_report_log($id_country = null, $id_shop = null, $where_fields = array()) {

        $content = $where = array();

        if (isset($id_country) && !empty($id_country))
            $where['id_country'] = $id_country;

        if (isset($id_shop) && !empty($id_shop))
            $where['id_shop'] = $id_shop;

        if (isset($where_fields) && !empty($where_fields)) {

            foreach ($where_fields as $keys => $wheres) {
                if (is_array($wheres)) {
                    if (isset($wheres['value']) && !empty($wheres['value'])) {
                        if (isset($wheres['operation']) && !empty($wheres['operation'])) {
                            $this->db->product->where(array($keys => $wheres['value']), $wheres['operation']);
                        } else {
                            $this->db->product->where(array($keys => $wheres['value']));
                        }
                    }
                } else {
                    $where[$keys] = $wheres;
                }
            }
        }

       $this->db_from(self::$m_pref.'amazon_request_report_log');

        if (!empty($where)) {
            $this->db->product->where($where);
        }

        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $keys => $value) {
            $content[$keys]['requestReportTime'] = $value['requestReportTime'];
            $content[$keys]['reportTime'] = $value['reportTime'];
            $content[$keys]['requestId'] = $value['requestId'];
            $content[$keys]['reportProcessingStatus'] = $value['reportProcessingStatus'];
            $content[$keys]['reportId'] = $value['reportId'];
            $content[$keys]['step'] = $value['step'];
            $content[$keys]['title'] = $value['title'];
            $content[$keys]['message'] = $value['message'];
            $content[$keys]['flag'] = $value['flag'];
            $content[$keys]['date_add'] = $value['date_add'];
        }
        return $content;
    }

    public function get_validation_log($id_country, $id_shop, $action_process = null, $action_type = null, $batch_id = null, $start = null, $limit = null, $num_rows = false, $search = array(), $exclude_display_id_product=false,$ignore_case=false) {

        $content = $where = $where_like = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

        if (isset($batch_id) && !empty($batch_id)) {
            $where['batch_id'] = $batch_id;
        }
        if (isset($action_process) && !empty($action_process)) {
            $where['action_process'] = $action_process;
        }
        if (isset($action_type) && !empty($action_type)) {
            if ($action_type == "error") {
                $where_not_equal['action_type'] = "warning";
            } else {
                $where['action_type'] = $action_type;
            }
        }	
	
        if(!empty($search) && count($search) > 0){
            foreach ($search as $search_key => $search_value){
                if(!empty($search_value)){
                    $where_like[$search_key] = $search_value;
                }
            }
            $this->db->product->where_like($where_like);
        }        
		
	$this->db_from(self::$m_pref.'amazon_validation_log');
        if (!empty($where)) {
            $this->db->product->where($where);
        }
	
	// limit 1 month
	$query_where['date_add'] = date('Y-m-d', strtotime("-30 days"));
	$this->db->product->where($query_where, '>=');
	if($ignore_case){
            $where_not_equal['message'] = "m000001"; 
        }
        if (!empty($where_not_equal)) {
            $this->db->product->where_not_equal($where_not_equal);
        }
	
        if (isset($start) && isset($limit)) {
            $this->db->product->limit($limit, $start);
        }
	
        if ($num_rows) {	    
	    
	    $this->db->product->select(array('id_log'));
            $result = $this->db->product->db_sqlit_query($this->db->product->query_string($this->db->product->query));
            $row = $this->db->product->db_num_rows($result);
	    
	    unset($this->db->product->query);
	    
            return $row;
        } else {
	    $this->db->product->orderBy('date_add DESC');	
            $result = $this->db->product->db_array_query($this->db->product->query);
            
	    require_once dirname(__FILE__) . '/../../UserInfo/configuration.php';
	    $userInfo = new UserConfiguration();
	    $language =$userInfo->getUserDefaultLaguageByUserName($this->user);
	    
	    Amazon_Tools::load(_FEED_LANG_FILE_, $language); 
	    
	    if (file_exists(dirname(__FILE__) . '/../parameters/message.management.php')) {
		include(dirname(__FILE__) . '/../parameters/message.management.php');
	    }	    
	    
            foreach ($result as $keys => $value) {
		$id_product_attribute = null;
		
                $content[$keys]['batch_id'] = $value['batch_id'];
                if (isset($value['no_messages'])) {
                    $content[$keys]['no_messages'] = $value['no_messages'];
                }
                $content[$keys]['action_process'] = $value['action_process'];
                $content[$keys]['action_type'] = $value['action_type'];
		
		$msg = $value['message'];
		
		if(!empty($value['message']) && isset($value['id_message'])){
		    
		    $identify = explode('_', $value['id_message']);
		    
		    $id_product = $identify[0];
		    
		    if(isset($identify[1]))
			$id_product_attribute = $identify[1];
		    
		    $messages = explode(';', $value['message']);
		    
		    if(isset($messages[1])){
		    
			$message_tmp = '';
			$message_array_tmp = array();
			foreach ($messages as $key => $message){
			    if($key == 0)
			    {
				if(isset($message_management[$message]))
				{
				    $message_tmp = $message_management[$message];
				}
			    } else {
				array_push($message_array_tmp, $message) ;
			    }
			}
			
			if(!$exclude_display_id_product){
			    $msg = sprintf(Amazon_Tools::l('ProductID') . ' '. Amazon_Tools::l('ProductID(s)'), 
				    (isset($id_product_attribute) ? $id_product .'/'.$id_product_attribute : $id_product) );
			    $msg .= vsprintf(' - ' . Amazon_Tools::l($message_tmp), $message_array_tmp);
			} else {
			    $msg .= vsprintf(Amazon_Tools::l($message_tmp), $message_array_tmp);
			}
			
		    } else {
			
			if(isset($message_management[$messages[0]]))
			{
			    $msgs =  Amazon_Tools::l($message_management[$messages[0]]);
			} else {
			    $msgs = $messages[0];
			}
			
			if(!$exclude_display_id_product){
			    $msg = sprintf(Amazon_Tools::l('ProductID') . ' '. Amazon_Tools::l('ProductID(s)'), 
				(isset($id_product_attribute) ? $id_product .'/'.$id_product_attribute : $id_product) );
			    $msg .= ' - ' . $msgs;
			} else {
			   $msg = $msgs;
			}
			
		    }
		}
		
                $content[$keys]['message'] = $msg;
                $content[$keys]['date_add'] = $value['date_add'];
            }
        }
        return $content;
    }
    
    public function get_validation_logs($date = null, $action_process = null) {

        $content = $where = $where_like = array();

        if(!$this->db_table_exists( self::$m_pref.'amazon_validation_log')){
            return $content;
        }

        $sql = "SELECT id_country, id_shop, message, id_message, count(id_log) AS rows
                FROM amazon_validation_log WHERE 1 ";

        if (isset($action_process) && !empty($action_process)) {
            if(is_array($action_process))
                $action_process = implode ("', '", $action_process);
            $sql .= "AND action_process IN ('$action_process') ";
        }

        if (isset($date) && !empty($date)) {
            $sql .= "AND date_add like '%$date%' ";
        }
        $sql .= "GROUP BY message, id_country, id_shop ;  ";
         
        $result = $this->db->product->db_query_str($sql);
      
        require_once dirname(__FILE__) . '/../../UserInfo/configuration.php';
        $userInfo = new UserConfiguration();
        $language =$userInfo->getUserDefaultLaguageByUserName($this->user);

        Amazon_Tools::load(_FEED_LANG_FILE_, $language);

        if (file_exists(dirname(__FILE__) . '/../parameters/message.management.php')) {
            include(dirname(__FILE__) . '/../parameters/message.management.php');
        }

        $history = array();

        foreach ($result as $value) {

            $msg = $value['message'];
            $messages = explode(';', $msg);

            if(isset($message_management[$messages[0]]))
            {
                $msgs =  Amazon_Tools::l($message_management[$messages[0]]);
            } else {
                $msgs = $messages[0];
            }
            
            if(isset($msgs)){
                $msg = $msgs;
            }
            
            $keys = md5($msg);

            if(isset($history['message'][$keys])){
                if($value['rows'] > $history['message'][$keys]){
                    $content[$keys]['rows'] = $value['rows'];
                }
                continue;
            }

            $content[$keys]['id_shop'] = $value['id_shop'];
            $content[$keys]['id_country'] = $value['id_country'];
            $content[$keys]['rows'] = $value['rows'];
            $content[$keys]['message'] = $msg;

            $history['message'][$keys] = $value['rows'];
        }
        
        return $content;
    }
    
    public function get_amazon_status($id_country, $id_shop, $action_type = null, $is_cron = null, $id_status = null, $status = null) {

        $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

        if (isset($action_type) && !empty($action_type)) {
            $where['action_type'] = $action_type;
        }
        if (isset($status) && !empty($status)) {
            $where['status'] = $status;
        }
        if (isset($is_cron) && !empty($is_cron)) {
            $where['is_cron'] = 1;
        }
        if (isset($id_status) && !empty($id_status)) {
            $where['id_status'] = $id_status;
        }

       $this->db_from(self::$m_pref.'amazon_status');
        if (!empty($where)) {
            $this->db->product->where($where);
        }

        $result = $this->db->product->db_array_query($this->db->product->query);
        return $result;
    }

    public function update_amazon_status($id_country, $id_shop, $date_add, $details = null, $action_type = AmazonDatabase::SYNC) {

        $table_update_status = self::$m_pref . 'amazon_status';
	
	if(!$this->db_table_exists($table_update_status))	    
	    return false;
	
        $value_update_status = array(
            "id_country" => $id_country,
            "id_shop" => $id_shop,
            "action_type" => $action_type,
            "status" => 1,
            "is_cron" => 1,
            "date_upd" => $date_add,
            "detail" => $details
        );
        //delete before save new to avoid duplicate row
        $this->delete_amazon_status($id_country, $id_shop, $action_type);

        $sql_update_status = $this->insert_string($table_update_status, $this->_filter_data($table_update_status, $value_update_status));
        if (!$this->db->product->db_exec($sql_update_status)) {
            return false;
        }
        return true;
    }

    public function delete_amazon_status($id_country, $id_shop, $action_type) {
        $table = self::$m_pref.'amazon_status';
        $where = array(
            "id_country" => array('operation' => "=", 'value' => $id_country),
            "id_shop" => array('operation' => "=", 'value' => $id_shop),
            "action_type" => array('operation' => "=", 'value' => $action_type),
        );
        $sql = $this->delete_string($table, $where);
        return $this->db->product->db_exec($sql);
    }

    public function getNumProducts($id_country, $id_mode, $id_shop) {

        $row = 0;
        $categories = $this->get_category_selected($id_country, $id_mode, $id_shop);

        if (isset($categories) && !empty($categories)) {
            $category = implode(', ', $categories);
            $ws = new stdClass();
            $ws->id_country = $id_country;
            $ws->id_shop = $id_shop;
            $case = $this->_case_check_report_inventory($ws, true, false, true);
            $sql = 'SELECT sum(numbers) as num FROM (
                        SELECT CASE WHEN p.has_attribute THEN sum(p.has_attribute) ELSE count(p.id_product) END AS numbers
                        FROM '.self::$p_pref.'product p
                        LEFT JOIN '.self::$p_pref.'product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                        WHERE p.id_shop = ' . (int)$id_shop . '
                        AND p.id_category_default IN (' . $category . ')  ' . $case . '
                        GROUP BY p.id_product
                        ORDER BY p.id_product, pa.id_product_attribute 
                    ) AS numbers ;  ';
            $result = $this->db->product->db_query_str($sql);
            foreach ($result as $value) {
                $row = (int)$value['num'];
            }
            return $row;
        }
    }
    
    public function get_log_type($id_country, $id_shop) {
	
        $table = self::$m_pref . 'amazon_log';
        $content = $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;

	$this->db_from($table);       
        $this->db->product->groupBy('action_type');
        $result = $this->db->product->db_array_query($this->db->product->query);
	
        foreach ($result as $keys => $value) {	
	    
	    $content[$keys]['field'] = $value['action_type'];
            $content[$keys]['value'] = ($value['action_type'] == AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE :
                    (($value['action_type'] == AmazonDatabase::CREATE) ? AmazonDatabase::CREATION : $value['action_type']);
        }
	
        return $content;
    }
    
    public function get_log($id_country, $id_shop, $last_log = false, $cron = null, $batch_id = null, $page = 0, $query_option = array()) {

        $table = self::$m_pref . 'amazon_log';
        $content = $where = array();
        $where['id_country'] = $id_country;
        $where['id_shop'] = $id_shop;
        $limit_page = 5;

        if (isset($batch_id) && !empty($batch_id)) {
            $where['batch_id'] = $batch_id;
        }
        if (isset($cron) && $cron == true) {
            $where['is_cron'] = 1;
        }

       $this->db_from($table);
        if (!empty($where)) {
            $this->db->product->where($where);
        }

        if (isset($query_option) && !empty($query_option)) {
            $query_where = array();
            if (isset($query_option['date_from']) && !empty($query_option['date_from'])) {
                $query_where['date_upd'] = $query_option['date_from'] . ' 00:00:00';
                $this->db->product->where($query_where, '>=');
            }
            if (isset($query_option['date_to']) && !empty($query_option['date_to'])) {
                $query_where['date_upd'] = $query_option['date_to'] . ' 24:00:00';
                $this->db->product->where($query_where, '<=');
            }
            if (isset($query_option['action_type']) && !empty($query_option['action_type'])) {
                $action_type['action_type'] = $query_option['action_type'];
                $this->db->product->where_like($action_type);
            }
        }

        if (isset($cron) && $cron == false) {
            $this->db->product->where_not_equal(array('is_cron' => 1, 'action_type' => 'Import Orders'));
        }

        $this->db->product->orderBy('date_upd DESC');
        if ($last_log) {
            $this->db->product->limit(1);
        } else {
            if (isset($page) && !empty($page)) {
                $offset_page = $page * ($limit_page);
                $this->db->product->limit($limit_page, $offset_page);
            } else {
                if (empty($query_option))
                    $this->db->product->limit($limit_page);
            }
        }
	
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $keys => $value) {
            $content[$keys]['id_log'] = $value['id_log'];
            $content[$keys]['batch_id'] = $value['batch_id'];
            $content[$keys]['id_country'] = $value['id_country'];
            $content[$keys]['id_shop'] = $value['id_shop'];
            $content[$keys]['action_type'] = ($value['action_type'] == AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE :
                    (($value['action_type'] == AmazonDatabase::CREATE) ? AmazonDatabase::CREATION : $value['action_type']);
            $content[$keys]['no_send'] = $value['no_send'];
            $content[$keys]['no_skipped'] = $value['no_skipped'];
            $content[$keys]['no_process'] = $value['no_process'];
            $content[$keys]['no_success'] = $value['no_success'];
            $content[$keys]['no_error'] = $value['no_error'];
            $content[$keys]['no_warning'] = $value['no_warning'];
            $content[$keys]['is_cron'] = $value['is_cron'];
            $content[$keys]['date_upd_date'] = date('Y - m - d', strtotime($value['date_upd']));
            $content[$keys]['date_upd_time'] = date('H : i : s', strtotime($value['date_upd']));
            $content[$keys]['date_upd'] = $value['date_upd'];

            if (isset($value['detail']) && !empty($value['detail'])) {
                $content[$keys]['detail'] = unserialize(base64_decode($value['detail']));
            }
	   
        }
        return $content;
    }
    
    public function get_disabled_products($id_shop, $id_country, $list_sku) { // 17/02/2016 : get disabled product to check is first disable product
        	
        $disabled_products = array();
        $where = '';
	
        if(!empty($list_sku)) $where = " AND sku IN ('".implode ("', '", $list_sku)."') ";
                
        $sql = "SELECT sku FROM ".self::$m_pref."amazon_disabled_products WHERE id_shop = $id_shop AND id_country = $id_country $where ; ";
	
        $result = $this->db->product->db_array_query($sql);
	
        foreach ($result as $data ){
	    $disabled_products[$data['sku']] = true;
        }
	
        return $disabled_products;
    }
    
    public function get_product_repricings($id_shop, $id_country, $list_sku) {
        	
        $product_reprice = array();
        $where = '';
	
        if(!empty($list_sku)) $where = " AND sku IN ('".implode ("', '", $list_sku)."') ";
                
        $sql = "SELECT sku, reprice, repricing_mode FROM ".self::$m_pref."amazon_repricing_report "
		. "WHERE id_shop = $id_shop AND id_country = $id_country $where "
		. "AND (none_repricing IS NULL or none_repricing = 0) AND repricing_action NOT IN ('export') "
		. "ORDER BY date_upd ASC; ";
	
        $result = $this->db->product->db_array_query($sql);
        foreach ($result as $data ){
            if(isset($data['reprice']) && !empty($data['reprice']) && isset($data['sku'])){
                $product_reprice[$data['sku']]['reprice'] = $data['reprice'];
                $product_reprice[$data['sku']]['repricing_mode'] = $data['repricing_mode'];
            } 
        }
        return $product_reprice;
    }
    
    public function get_last_repricing_log($id_shop, $id_country) {

        $sql="SELECT date_add FROM ".self::$m_pref."amazon_repricing_log WHERE id_shop = $id_shop AND id_country = $id_country ORDER BY date_add DESC LIMIT 1; ";           
        $result = $this->db->product->db_array_query($sql);
        if(isset($result[0]['date_add'])){
            return $result[0]['date_add'];
        } else {
            return false;
        }
    }
    
    public function get_repricing_logs($id_shop, $id_country, $start = null, $limit = null, $order_by = null, $search = null, $numrow = false){
        
        // get product list
        $sql = "SELECT * FROM ".self::$m_pref."amazon_repricing_report "
             . "WHERE id_shop = ".(int)$id_shop." and id_country = ".(int)$id_country." and repricing_action NOT IN ('export') ";
                
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
                    . "sku like '%" . $search . "%' OR "
                    . "reprice like '%" . $search . "%' OR "
                    . "repricing_action like '%" . $search . "%' OR "
                    . "process_type like '%" . $search . "%' OR "
                    . "repricing_mode like '%" . $search . "%' OR "
                    /*. "repricing_date like '%" . $search . "%' "*/
                    . "date_upd like '%" . $search . "%' "
                    . ") ";
        }
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY $order_by ";
        }
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
        $sql .= " ; " ;
	
        if($numrow) {
            $result =  $this->db->product->db_sqlit_query($sql);
            $row =  $this->db->product->db_num_rows($result);
            return $row;
        }
        
        $result = $this->db->product->db_query_str($sql);
        return $result;
    }
    
    public function get_logs($date = null, $action_type = null, $id_shop = null) {

        $content = $where = array();
        if(!$this->db_table_exists( self::$m_pref . 'amazon_log')){
            return false;
        }
	
        // Order
        $sql = "SELECT site, count(id_orders) as no_success              
		FROM orders_orders WHERE date_add like '%$date%' AND id_marketplace = 2 
		GROUP BY site ORDER BY id_orders ASC ; ";           
       
        $result = $this->db->product->db_array_query($sql);
        
        foreach ($result as $value) {
            $content[$value['site']]['order']['no_success'] = $value['no_success'];
            if(!isset($content['total_order']['no_success']))
                $content['total_order']['no_success'] = 0;
            $content['total_order']['no_success'] = ($content['total_order']['no_success'] + $value['no_success']);
        }
        
        // Repricing
        $sql = "SELECT 
                id_country,
                action_type,                
                sum(no_send) as no_send                 
                FROM ".self::$m_pref."amazon_log WHERE date_upd like '%$date%' AND action_type LIKE '%repricing%'
                GROUP BY id_country, action_type
                ORDER BY id_country ASC, action_type ASC; ";    
        
        $result = $this->db->product->db_array_query($sql);
        
        foreach ($result as $value) {
            $content[$value['id_country']]['repricing']['no_success'] = $value['no_send'];
            if(!isset($content['total_repricing']['no_success']))
                $content['total_repricing']['no_success'] = 0;
            $content['total_repricing']['no_success'] = ($content['total_repricing']['no_success'] + $value['no_send']);
        }
        
        // if no order import unset content
        if((!isset($content['total_order']['no_success']) || $content['total_order']['no_success'] == 0) 
            && (!isset($content['total_repricing']['no_success']) || $content['total_repricing']['no_success'] == 0)) unset ($content);
            
        if (isset($action_type) && is_array($action_type)) {
            $action_type = implode ("', '", $action_type);           
        }
        
        $sql = "SELECT 
                id_country,
                action_type,
                sum(no_process) as no_process ,
                sum(no_success) as no_success ,
                sum(no_error) as no_error ,
                sum(no_warning) as no_warning 
                FROM ".self::$m_pref."amazon_log WHERE date_upd like '%$date%' AND action_type IN ('".$action_type."')
                GROUP BY id_country, action_type
                ORDER BY id_country ASC, action_type ASC";           
       
        $result = $this->db->product->db_array_query($sql);
       
        foreach ($result as $value) {
            
            // if no process unset
            if($value['no_process'] == 0)
                continue;
            
            $content[$value['id_country']][$value['action_type']]['no_process'] = $value['no_process'];
            $content[$value['id_country']][$value['action_type']]['no_success'] = $value['no_success'];
            $content[$value['id_country']][$value['action_type']]['no_error'] = $value['no_error'];
            $content[$value['id_country']][$value['action_type']]['no_warning'] = $value['no_warning'];
            
            // total
            if(!isset($content['total_'.$value['action_type']]['no_process']))
                $content['total_'.$value['action_type']]['no_process'] = 0;
            $content['total_'.$value['action_type']]['no_process'] = ($content['total_'.$value['action_type']]['no_process'] + $value['no_process']);
            
            if(!isset($content['total_'.$value['action_type']]['no_success']))
                $content['total_'.$value['action_type']]['no_success'] = 0;
            $content['total_'.$value['action_type']]['no_success'] = ($content['total_'.$value['action_type']]['no_success'] + $value['no_success']);
            
            if(!isset($content['total_'.$value['action_type']]['no_error']))
                $content['total_'.$value['action_type']]['no_error'] = 0;
            $content['total_'.$value['action_type']]['no_error'] = ($content['total_'.$value['action_type']]['no_error'] + $value['no_error']);
            
            if(!isset($content['total_'.$value['action_type']]['no_warning']))
                $content['total_'.$value['action_type']]['no_warning'] = 0;
            $content['total_'.$value['action_type']]['no_warning'] = ($content['total_'.$value['action_type']]['no_warning'] + $value['no_warning']);
        }
        if(isset($content)){ return $content;
        } else {  return false;  }
    }
    
    public function update_log($data, $batch_id = null) {
        $table = self::$m_pref.'amazon_log';
        $sql = '';
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            if (isset($batch_id) && !empty($batch_id)) {
                $where = array(
                    'batch_id' => $batch_id,
                    'id_country' => $data['id_country'],
                    'id_shop' => $data['id_shop']
                );
                $sql = $this->update_string($table, $filter_data, $where);
            } else {
                $sql = $this->insert_string($table, $filter_data);
            }
            if (!$this->exec_query($sql)) {
                return false;
            }
        }
        return true;
    }
    
    public function save_inventory($data, $id_country, $id_shop) {
	$table = self::$m_pref.'amazon_inventory';
        $sql = '';        
        if (isset($data) && !empty($data)) {
            foreach ($data as $d) {
                $d['id_country'] = (int)$id_country;
                $d['id_shop'] = (int)$id_shop;
                $d['date_add'] = date("Y-m-d H:i:s");
                $d['date_upd'] = date("Y-m-d H:i:s");
                $filter_data = $this->_filter_data($table, $d);
                $sql .= $this->replace_string($table, $filter_data);
            }
            if (!$this->exec_query($sql, true)) {
                return false;
            }
        }
        return true;
    }
    
    public function save_report_inventory($data, $id_country, $id_shop, $truncate = true) {
	$table = self::$m_pref.'amazon_report_inventory';
	if ($truncate) {
	    $this->truncate_table($table, $id_shop, $id_country);
        }
        $sql = '';        
        if (isset($data) && !empty($data)) {
            foreach ($data as $d) {
                $d['id_country'] = (int)$id_country;
                $d['id_shop'] = (int)$id_shop;
                $d['date_add'] = date("Y-m-d H:i:s");
                $d['date_upd'] = date("Y-m-d H:i:s");
                $filter_data = $this->_filter_data($table, $d);
                $sql .= $this->replace_string($table, $filter_data);
            }
            if (!$this->exec_query($sql, true)) {
                return false;
            }
        }
        return true;
    }
    
    public function save_fba_data($data, $id_shop, $id_country=null ) {
	
	$table = self::$m_pref.'amazon_fba_data';
        $sql = '';        
        if (isset($data) && !empty($data)) {
	    
	    $fba_data = $this->get_fba_data($id_shop, $id_country);
	    
            foreach ($data as $d) {
		
		if(isset($id_country) && !empty($id_country))
	            $d['id_country'] = (int)$id_country;
		
                $d['id_shop'] = (int)$id_shop;
                $d['date_add'] = date("Y-m-d H:i:s");
                $d['date_upd'] = date("Y-m-d H:i:s");
		
		if(isset($fba_data[$d['sku']]) && !empty($fba_data[$d['sku']]))
		{
		    unset($d['asin']);
		     $where = array(
			'sku' => array('operation' => '=', 'value' => $d['sku']),
			'id_product' => array('operation' => '=', 'value' => $d['id_product']),
			'id_product_attribute' => array('operation' => '=', 'value' => $d['id_product_attribute']),
			'id_shop' => array('operation' => '=', 'value' => (int)$id_shop),
		    ); 
		    
		     if(isset($id_country) && !empty($id_country))
			$where['id_country'] = array('operation' => '=', 'value' => (int)$id_country) ;

		    $sql .= $this->update_string($table, $d, $where);
		} else { 
		    $filter_data = $this->_filter_data($table, $d);		 
		    $sql .= $this->replace_string($table, $filter_data);
		}
            }
	    
	    unset($fba_data);
	    	    
            if (!$this->exec_query($sql, true)) {
                return false;
            }
        }
        return true;
    }
    
    public function save($table, $data, $id_country, $id_shop, $truncate = false) {
        $sql = '';
        if ($truncate) {
	    $this->truncate_table($table, $id_shop, $id_country);
        }
        if (isset($data) && !empty($data)) {
            foreach ($data as $d) {
                $d['id_country'] = (int)$id_country;
                $d['id_shop'] = (int)$id_shop;
                $d['date_add'] = date("Y-m-d H:i:s");
                $d['date_upd'] = date("Y-m-d H:i:s");
                $filter_data = $this->_filter_data($table, $d);
                $sql .= $this->replace_string($table, $filter_data);
            }
            
            if (!$this->exec_query($sql, true)) {
                return false;
            }
        }
        return true;
    }

    public function save_request_report($data, $truncate = true) {
        $table = self::$m_pref . 'amazon_request_report_log';
        if ($truncate) {
	    $this->truncate_table($table, $data['id_shop'], $data['id_country']);
        }
        $data = (array) $data;
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            $sql = $this->insert_string($table, $filter_data);
            return $this->db->product->db_exec($sql);
        }
        return null;
    }

    public function truncate_validation_log($id_shop, $id_country, $action_process = null, $action_type = null) {
        array_map('unlink', glob(getcwd() . '/users/' . $this->user . "/amazon/validation_error_logs.xml"));
        $process = $type = '';

        if (isset($action_process) && !empty($action_process)) {
            $process = ' AND action_process = "' . $action_process . '" ';
        }
        if (isset($action_type) && !empty($action_type)) {
            $type = ' AND action_type = "' . $action_type . '" ';
        }
        return $this->truncate_table(self::$m_pref.'amazon_validation_log', $id_shop, $id_country, $process.$type );
    }

    public function save_validation_log($data, $exec = true) {
	$table = self::$m_pref.'amazon_validation_log';
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            $sql = $this->insert_string($table, $filter_data);
            if ($exec) {
                return @$this->db->product->db_exec($sql);
            } else {
                return $sql;
            }
        }
        return null;
    }

    public function save_response_log($data) {
        $table = self::$m_pref.'amazon_submission_list_log';
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            $sql = $this->insert_string($table, $filter_data);
            return @$this->db->product->db_exec($sql, true);
        }
        return null;
    }

    public function save_submission_result_log($id_shop, $id_country, $data) {
        $sql = '';
        $table = 'amazon_submission_result_log';
        if (isset($data) && !empty($data)) {
            foreach ($data as $d) {
                $sql .= $this->insert_string($table, $d);
            }
            if (!$this->exec_query($sql, true)) {
                return false;
            }
        }
    }

    public function save_profiles($datas, $id_shop, $id_mode, $id_country, $advertising_groups=null) {
        
        $sql = '';
        $table = self::$m_pref.'amazon_profile';
        $date_add = date('Y-m-d H:i:s');
        $profile = $ad_groups = array();
        if(isset($advertising_groups)){
            foreach ($advertising_groups as $ad_group){
                $ad_groups[$ad_group['adGroupId']] = $ad_group;
            }
        }
        if (isset($datas['profile']) && !empty($datas['profile'])) {
            foreach ($datas['profile'] as $val) {
                $value = $val;
                $value['id_country'] = $id_country;
                $value['id_shop'] = $id_shop;
                $value['id_mode'] = $id_mode;
                $value['date_add'] = $date_add;
                $i=0;
                while(strpos($value['name'],"''")!==false){
                    $value['name'] = str_replace("''","'",$value['name']);
                    $i++;
                }
                    
                if (isset($val['sku_as_supplier_reference']) && !empty($val['sku_as_supplier_reference'])) {
                    unset($value['sku_as_supplier_reference']);
                    $value['sku_as_supplier_reference']['value'] = $val['sku_as_supplier_reference'];
                }
                if (isset($val['unconditionnaly']) && !empty($val['unconditionnaly'])) {
                    unset($value['unconditionnaly']);
                    $value['sku_as_supplier_reference']['unconditionnaly'] = $val['unconditionnaly'];
                }
                if (isset($val['key_product_feature']) && !empty($val['key_product_feature'])) {
                    if ($val['key_product_feature'] == 1) {
                        unset($value['key_product_features']['descriptions']);
                    } else if ($val['key_product_feature'] == 2) {
                        unset($value['key_product_features']['features']);
                    } else {
                        unset($value['key_product_features']);
                    }
                    $value['key_product_features']['key_product_feature'] = $val['key_product_feature'];
                } else {
                    unset($value['key_product_features']);
                }
                if (isset($val['price_rules']['value']) && !empty($val['price_rules']['value'])) {
                    foreach ($val['price_rules']['value'] as $price_rule_key => $price_rule) {
                        if (empty($price_rule['type']) || empty($price_rule['to']) || empty($price_rule['value'])) {
                            unset($value['price_rules']['value'][$price_rule_key]);
                        }
                    }
                }

                if (!isset($val['code_exemption']['chk']) || !$val['code_exemption']['chk'] || $val['code_exemption']['chk'] == 0) {
                    unset($value['code_exemption']);
                }

                if(isset($val['advertising']['adGroupId'])){
                    unset($value['advertising']['adGroupId']);
                    foreach ($val['advertising']['adGroupId'] as  $adGroupKey => $adGroupId){
                        if(isset($ad_groups[$adGroupId]['campaignId'])){
                            $value['advertising'][$adGroupKey]['campaignId'] = $ad_groups[$adGroupId]['campaignId'];
                            $value['advertising'][$adGroupKey]['adGroupId'] = $ad_groups[$adGroupId]['adGroupId'];
                        }
                    }
                }
             
                if (isset($value) && !empty($value)) {
                    if (isset($value['id_profile']) && !empty($value['id_profile'])) {
                        $sql .= $this->update_string($table, $this->_filter_data($table, $value), array('id_profile' => array('operation' => '=', 'value' => $value['id_profile'])));
                        $profile[$value['id_profile']] = true;
                    } else {
                        $sql .= $this->insert_string($table, $this->_filter_data($table, $value));
                    }
                }
            }
            
            if (strlen($sql)) {
                //$this->db->product->db_trans_begin();
                if (!$this->db->product->db_exec($sql)) {
                    return false;
                }
                if (count($profile) > 0) {
                    if (!$this->update_amazon_status($id_country, $id_shop, $date_add, 'profiles')) {
                        return 'status_fail';
                    }
                }
                //$this->db->product->db_trans_commit();
            }
        }
                    
        return true;
    }

    public function save_configuration_features($data, $id_shop, $id_country) {
        
        $sql = '';
        $table = self::$m_pref.'amazon_log_display';
        $feature_list = array('creation', 'import_orders', 'delete_products', 'price_rules', 'second_hand', 'code_examption', 'synchronization_field', 'sku_as_supplier_reference', 'sku_as_supplier_reference', 'repricing', 'shipping_override', 'fba', 'messaging', 'advertising', 'orders_cancelation');
            
        $this->truncate_table($table, $id_shop, $id_country, 'menu = "features"');
       
        foreach($data as $fields => $val) {
            if(in_array($fields, $feature_list)){
                $disabled = 1;
                if(!empty($val)){
                    $disabled = 0;
                }
                $features = array(
                    'field_name' => $fields,
                    'field_disabled' => $disabled,
                    'field_toggle' => (int)$val,
                    'id_shop' => (int)$id_shop,
                    'id_country' => (int)$id_country,
                    'menu' => 'features',
                    'other' => date('Y-m-d H:i:s'),
                );   
                $sql .= $this->insert_string($table, $features);
            }
        }         
        
        return $this->db->product->db_exec($sql);
    }
    
    public function get_configuration_features($id_country=null, $id_shop=null, $return_all=false) {
        $content = $where = array();
        $table =  self::$m_pref . 'amazon_log_display';
        if(!$this->db_table_exists($table))return $content;
        
	if(isset($id_country) && !empty($id_country))
	    $where['id_country'] = $id_country;
	
	if(isset($id_shop) && !empty($id_shop))
	    $where['id_shop'] = $id_shop;
	
        $where['menu'] = 'features';

       $this->db_from($table);
        if (!empty($where)){
            $this->db->product->where($where);
        }
       
        $result = $this->db->product->db_array_query($this->db->product->query);
        foreach ($result as $value) {
	    if(!$return_all) {
		$content[$value['field_name']] = $value['field_toggle'] ? 1:0;
	    } else {
		$content[$value['id_shop']][$value['id_country']][$value['field_name']] = $value['field_toggle'] ? 1:0;
	    }
        }
        return $content;
    }
    
    // update repricing status to product_update_log  // TO DO TRIGGER a reprice
    public function update_repricing_product_update_log($id_shop, $id_country, $sku=null){
        
	$sql = '';
        $product_update_type = 'repricing';
        $product_update_status = 'reprice';
        $table =  self::$m_pref . 'amazon_repricing_flag';
        $date = date('Y-m-d H:i:s');
        
        if(!$this->db_table_exists($table))
            return false;
	
       // 1. update product_update_log to current date :
	$sql .= " REPLACE INTO log_product_update_log ( id_product, id_product_attribute, id_shop, product_update_type,	product_update_status, date_add ) 
		  SELECT id_product, id_product_attribute, $id_shop, '$product_update_type', '$product_update_status', '$date' FROM ".self::$m_pref."amazon_inventory
		  WHERE id_shop = ". (int)$id_shop." AND id_country = ". (int)$id_country . " AND action_type != 'delete' ";
        if(isset($sku) && !empty($sku)){
            $sql .= " AND sku = '".$sku."' ";
        }
        $sql .= "; ";

        // 2. update amazon_repricing_flag, set flag = 1
        $sql .= " REPLACE INTO $table (id_product, id_product_attribute, sku, id_shop, id_country, flag, date_upd) "
                ."SELECT id_product, id_product_attribute, sku, id_shop, id_country, 1,'$date' FROM ".self::$m_pref."amazon_inventory
		  WHERE id_shop = ". (int)$id_shop." AND id_country = ". (int)$id_country . " AND action_type != 'delete' ";
	//$sql .= " UPDATE $table SET flag = 1 , date_upd = '$date' WHERE id_shop = ". (int)$id_shop." AND id_country = ". (int)$id_country . " ";
        if(isset($sku) && !empty($sku)){
            $sql .= " AND sku = '".$sku."' ";
        }
        $sql .= "; ";

        if($this->debug){
            echo '<pre>' . print_r($sql, true) . '</pre>';
            return true;
        } else {
            return $this->db->product->db_exec($sql);
        }
    }
    
    public function update_amazon_repricing_flag($id_shop, $id_country, $list_id_product, $flag = 0){
        
        $sql = '';
	$product_update_type = 'repricing';
	
	if($flag == 0) {
	    $product_update_status = 'reprice';
	} else {
	    $product_update_status = 'reprice';
	}
	
        $table =  self::$m_pref . 'amazon_repricing_flag';
        $date = date('Y-m-d H:i:s');
        
        if(!$this->db_table_exists($table) || empty($list_id_product))
            return false;
        
        foreach ($list_id_product as $product) {
	    
	    $field_prodcut_attribute = $id_prodcut_attribute = '';
            
            if(isset($product['id_product_attribute']) && !empty($product['id_product_attribute'])){
                $field_prodcut_attribute = ', id_product_attribute';
                $id_prodcut_attribute = ", ".(int)$product['id_product_attribute'];
            }
	    
	    // 1. update product_update_log to current date :
	    $sql .= " REPLACE INTO log_product_update_log ( id_product $field_prodcut_attribute, id_shop, product_update_type, product_update_status, date_add ) "
		   ." VALUES (".$product['id_product'].$id_prodcut_attribute.",".$id_shop.",'".$product_update_type."','".$product_update_status."','".$date."' ) ;";
	    
	    // 2. update amazon_repricing_flag, set flag = 0
            $sql .= " REPLACE INTO $table (id_product $field_prodcut_attribute, sku, id_shop, id_country, flag, date_upd) "
                    ."VALUES (".$product['id_product'].$id_prodcut_attribute.",'".$product['SKU']."',".$id_shop.",".$id_country.",".$flag.",'".$date."' ) ;";
        }       
        
        if($flag == 1){
           return $sql;
        }
        
        if(strlen($sql) > 10) {
            file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/update_amazon_repricing_flag.txt', print_r(array('date'=> date('Y-m-d H:i:s'), 'sql'=>$sql), true), FILE_APPEND);
            $exec = $this->db->product->db_exec($sql, true, true, true);
            return $exec;
        }
        return true;
    }
    
    public function save_repricing_report($id_shop, $id_country, $data, $repricing_action, $process_type) {
	
        $query = '';
	
        foreach ($data as $d) {
	    
	    if(!isset($d['no_price_export']) || empty($d['no_price_export']) || !$d['no_price_export']){		
		$log_data = array(
		    'sku' => $d['SKU'],
		    'id_country' => $id_country,
		    'id_shop' => $id_shop,
		    'repricing_date' => isset($d['date']) ? $d['date'] : '',
		    'repricing_timestamp' => isset($d['timestamp']) ? $d['timestamp'] : '',
		    'reprice' => isset($d['price']) ? $d['price'] : (isset($d['Price']) ? $d['Price'] : 0 ) ,
		    'none_repricing' => isset($d['none_repricing']) ? $d['none_repricing'] : null ,
		    'repricing_action' => $repricing_action ,
		    'process_type' => $process_type ,
		    'repricing_mode' => isset($d['repricing_mode']) ? $d['repricing_mode'] : 'NA' ,
		    'date_upd' => date("Y-m-d H:i:s"),
		);
		$query .= $this->replace_string(self::$m_pref.'amazon_repricing_report', $log_data);
	    }
        }        
	
	if(strlen($query)) {
	    return $this->db->product->db_exec($query,true,true,true);
	}
	
	return true;
    }
    
    public function get_amazon_repricing_flag($id_shop, $id_country, $list_id_product, $date){
	
        $content = $where = $inventories = $repricing_flag = array();            
        $table = self::$m_pref.'amazon_repricing_flag';
	
        if(!$this->db_table_exists($table) || empty($list_id_product))
            return $content;
	
	// 1. select flag 
        $sql_flag = "SELECT flag, sku FROM $table "
                . "WHERE id_product IN (".implode(', ',array_keys($list_id_product)).") "
                . "AND id_shop = $id_shop AND id_country = $id_country AND date_upd LIKE '%$date%' ; ";
	
        $result_flag = $this->db->product->db_array_query($sql_flag);
	
        foreach ($result_flag as $flag) {
            if(isset($flag['sku']))
		$repricing_flag[$flag['sku']] = (int)$flag['flag'];
        }

        if($this->debug){
            echo '<br/> get_amazon_repricing_flag <br/> <pre>' . print_r(count($repricing_flag), true) . '</pre>';
        }
        
	// 2. select all product in inventory
	$ws = new stdClass();
	$ws->id_country = $id_country;
	$ws->id_shop = $id_shop;
	
	$where_additional = array();
	$where_additional[0]['field'] = 'action_type';
	$where_additional[0]['operation'] = 'NOT IN';
	$where_additional[0]['value'] = '("delete")';
	
	$case = $this->_case_check_report_inventory($ws, false, true, true, false, $where_additional);

	$sql_inventory = "SELECT "
                . "p.id_product as id_product, "
                . "pa.id_product_attribute  as id_product_attribute, "
                . "CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS sku ";
	$sql_inventory .= "FROM ".self::$p_pref."product p ";
        $sql_inventory .= "LEFT JOIN ".self::$p_pref."product_attribute pa ON pa.id_product = p.id_product AND pa.id_shop = p.id_shop ";
        $sql_inventory .= "WHERE p.id_shop = " . $id_shop . $case . " " ;
	$sql_inventory .= "AND (p.date_upd IS NULL OR p.date_upd = '') AND p.id_product IN (".implode(', ',array_keys($list_id_product)).") ";
	$sql_inventory .= "GROUP BY p.id_product, pa.id_product_attribute ";

	$result_inventory = $this->db->product->db_array_query($sql_inventory);
        
        foreach ($result_inventory as $inventory) {
	    
	    $sku = $inventory['sku'];

            if(!isset($repricing_flag[$sku]) || $repricing_flag[$sku] == 0)
                continue;
            
            $content[$inventory['id_product']]['id_product'] = (int)$inventory['id_product'];
            $content[$inventory['id_product']]['id_product_attribute'] = (int)$inventory['id_product_attribute'];
            $content[$inventory['id_product']]['sku'] = $inventory['sku'];
	}
        
        return $content;        
    }
    
    public function set_error_notification($priority=null) {
        $row = array();
        $table = self::$m_pref.'amazon_submission_result_log';
        $marketplace = defined(_MARKETPLACE_NAME_) ? strtolower(_MARKETPLACE_NAME_) : 'amazon';

        // get prior notification
        if($this->db_table_exists($table)){

            // get notification rows only for today
            $notification_sql = "SELECT asrl.*, CASE WHEN ln.marketplace IS NOT NULL THEN ln.flag_unread ELSE NULL END AS flag_unread
                FROM $table asrl
                LEFT JOIN log_notifications ln ON ln.id_shop = asrl.id_shop AND ln.id_country = asrl.id_country AND ln.notification_identify = asrl.result_message_code
                AND ln.marketplace = '$marketplace'
                WHERE asrl.result_message_code IN ('".  implode("', '", array_keys($priority))."') AND asrl.result_code != 'Resolve' AND asrl.date_add LIKE '".date('Y-m-d')."%' 
                GROUP BY asrl.result_message_code ORDER BY asrl.date_add DESC ; ";
            $notification_result_list = $this->db->product->db_array_query($notification_sql);

            foreach ($notification_result_list as $nresult) {
                $row[$nresult['id_shop']][$nresult['id_country']][$nresult['result_message_code']] = $nresult;
            }
        }
        
        $insert = '';
        if($this->db_table_exists(self::$l_pref.'notifications')){
            foreach ($row as $id_shop => $notification1){
                foreach ($notification1 as $id_country => $notification2){
                    $this->truncate_table(self::$l_pref.'notifications', $id_shop, $id_country, "marketplace = '$marketplace'");
                    foreach ($notification2 as $message_code => $notification){
                        $notification_key = $marketplace.'_'.$id_shop.'_'.$id_country.'_'.$message_code;                        
                        $data = array(
                            'marketplace' => $marketplace,
                            'id_shop'=> $id_shop,
                            'id_country'=> $id_country,
                            'notification_identify' => $message_code,
                            'notification_date'=> $notification['date_add'],
                            'notification_link'=> $marketplace.'/error_resolutions/'.(isset($priority[$message_code]['error_type']) ?
                                            $priority[$message_code]['error_type'] : 'products').'/'.$id_country,
                            'flag_unread'=> (!isset($notification['flag_unread']) || $notification['flag_unread'] == 1) ? 1 : 0,
                            'notification_message'=> isset($priority[$message_code]['notification_summary']) ?
                                            $priority[$message_code]['notification_summary'] : $notification['result_description'],
                            'notification_key'=> $notification_key,
                            'date_upd'=> date('Y-m-d H:i:s')
                        );
                        $insert .= $this->replace_string(self::$l_pref.'notifications', $data);
                    }
                }
            }
        }
        if(strlen($insert)){
            $this->exec_query($insert);
        }
    }

    public function update_error_notification($id_shop, $id_country, $flag=0) {
        if($this->db_table_exists(self::$l_pref.'notifications')){
            $marketplace = defined(_MARKETPLACE_NAME_) ? strtolower(_MARKETPLACE_NAME_) : 'amazon';
            $update = array('flag_unread' => $flag, 'date_upd' => date('Y-m-d H:i:s'));
            $where = array();
            $where['marketplace'] = array('operation' => '=', 'value' => $marketplace);
            $where['id_shop'] = array('operation' => '=', 'value' => (int)$id_shop);
            $where['id_country'] = array('operation' => '=', 'value' => (int)$id_country);
            $where['notification_identify'] = array('operation' => '!=', 'value' => NULL);
            $sql = $this->update_string(self::$l_pref."notifications", $update, $where);

            return $this->exec_query($sql);
        }
    }

    public function get_error_resolutions_rows($id_shop=null, $id_country=null, $return_all=false, $priority=null) {
        $row = array();
	
	if($this->db_table_exists(self::$m_pref.'amazon_submission_result_log')){
	    
	    $country = $shop = '' ;
	    
	    if(isset($id_country) && !empty($id_country)){
		$country = "AND id_country = $id_country " ;
	    }
	    
	    if(isset($id_shop) && !empty($id_shop)) {
		$shop = "AND id_shop = $id_shop " ;
	    }

	    $list_id_sunmission_feed = array();
	    $sql_list_log = "SELECT * FROM ".self::$m_pref."amazon_submission_list_log WHERE 1 $country $shop ;" ;
	    $result_list_log = $this->db->product->db_array_query($sql_list_log);

             foreach ($result_list_log as $value) {
                $message = unserialize(base64_decode($value['message']));
                if(isset($message['FeedSunmissionId']) && !empty($message['FeedSunmissionId'])){
                    $type = isset($value['feed_type']) && in_array($value['feed_type'], AmazonErrorResolution::$feed_type['Create']) ? 'products' : 'offers' ;
                    $list_id_sunmission_feed[$type][$value['id_shop']][$value['id_country']][] = $message['FeedSunmissionId'];
                }
            }
            
	    foreach ($list_id_sunmission_feed as $type => $t_id_sunmission_feed) {
                foreach ($t_id_sunmission_feed as $shop_id => $l_id_sunmission_feed) {
                    foreach ($l_id_sunmission_feed as $country_id => $id_sunmission_feed) {

                        $sql = "SELECT *
                               FROM ".self::$m_pref."amazon_submission_result_log
                               WHERE id_submission_feed IN ('".  implode("', '", $id_sunmission_feed)."') AND result_code != 'Resolve' AND id_country = $country_id AND id_shop = $shop_id
                               GROUP BY result_message_code ; ";

                        $result = $this->db->product->db_sqlit_query($sql);
                        if(!$return_all){
                            $row = $this->db->product->db_num_rows($result);
                        } else {
                            $row[$shop_id][$country_id][$type] = $this->db->product->db_num_rows($result);
                        }
                    }
                }
	    }
        }
        return $row;            
    }
    
    /*Shipping Templates*/
    public function get_shipping_group_names($id_shop = null, $id_country = null) {
	
	$row = array();
	
	if($this->db_table_exists(self::$m_pref.'amazon_shipping_groups')){
	    
	    $country = $shop = '' ;
	    
	    if(isset($id_country) && !empty($id_country)){
		$country = "AND id_country = $id_country " ;
	    }
	    
	    if(isset($id_shop) && !empty($id_shop)) {
		$shop = "AND id_shop = $id_shop " ;
	    }

	    $sql = "SELECT * FROM ".self::$m_pref."amazon_shipping_groups WHERE 1 $country $shop ;" ;
	    $result = $this->db->product->db_array_query($sql);
	    
	    foreach ($result as $shipping_groups) {
		$row[$shipping_groups['group_key']] = $shipping_groups['group_name'];
	    }
	}
        
        return $row; 
    }

    /*Advertising*/
    public function getProductsAdvertising($id_shop, $id_country=null, $campaignId=null, $adGroupId=null){
        $sql_get_product_ads = "SELECT * "
                . "FROM ".self::$m_pref."amazon_advertising_products "
                . "WHERE id_shop = $id_shop " ;
        if(isset($id_country)){
            $sql_get_product_ads .= " AND id_country = $id_country ";
        }
        if(isset($campaignId)){
            $sql_get_product_ads .= " AND campaignId = '$campaignId' ";
        }
        if(isset($adGroupId)){
            $sql_get_product_ads .= " AND adGroupId = '$adGroupId' ";
        }
        $result_product_ads = $this->db->offer->db_query_str($sql_get_product_ads);
        $list_product_ads = array();
        foreach ($result_product_ads as $product_ads){
            //$list_product_ads[$product_ads['campaignId']][$product_ads['adGroupId']][$product_ads['sku']] = $product_ads;
            $list_product_ads[$product_ads['sku']] = $product_ads;
        }
        return $list_product_ads;
    }

    public function getProductsToCreateAd($id_shop, $id_country, $campaignId=null, $adGroupId=null){
        $not_in_list = $product_list = array();

        // get created product ads list
        $list_product_ads = $this->getProductsAdvertising($id_shop, null, $campaignId, $adGroupId);
        if(!empty($list_product_ads)) {
            $not_in_list = array_merge($not_in_list, array_keys($list_product_ads));
        }

        // get deleted product
        /*$sql_get_inventory = "SELECT id_product, id_product_attribute, sku FROM ".self::$m_pref."amazon_inventory "
                            . "WHERE id_country = $id_country AND id_shop = $id_shop AND action_type = 'delete'";
        $result_inventory = $this->db->offer->db_query_str($sql_get_inventory);
        foreach ($result_inventory as $inventory){
            //$not_in_list[] = $inventory['sku'];
            $not_in_list_id_product[] = $inventory['id_product'];
            $not_in_list_id_product_attribute[] = $inventory['id_product_attribute'];
        }   */

        // get profile in $campaignId & $adGroupId
//        $sql_get_profile = "SELECT id_category FROM ".self::$m_pref."amazon_profile ap"
//            . "LEFT JOIN ".self::$m_pref."amazon_category_selected acsw ON ap.id_profile = acsw.id_profile AND ap.id_shop = acsw.id_shop AND ap.id_country = acsw.id_country "
//            . "WHERE ap.id_country = $id_country AND ap.id_shop = $id_shop ";
//        $result_inventory = $this->db->offer->db_query_str($sql_get_profile);
//        foreach ($result_inventory as $inventory){
//            $not_in_list[] = $inventory['sku'];
//        }

        // get from product where advertising != 0
        $sql = "SELECT p.id_product AS id_product, pa.id_product_attribute AS id_product_attribute, acsw.id_category AS id_category, acsw.id_profile AS id_profile,
                CASE
                    WHEN pa.reference IS NOT NULL THEN pa.reference
                    WHEN p.reference IS NOT NULL THEN p.reference
                END AS sku 
                FROM ".self::$p_pref."product p
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".self::$m_pref."amazon_category_selected acsw ON p.id_category_default = acsw.id_category AND p.id_shop = acsw.id_shop
                LEFT JOIN ".self::$m_pref."amazon_inventory ai ON ai.id_product = p.id_product AND ai.id_shop = acsw.id_shop AND ai.id_country = acsw.id_country 
                WHERE p.id_shop = $id_shop AND acsw.id_country = $id_country AND acsw.id_shop = $id_shop AND ai.action_type != 'delete'
                AND p.active = 1 AND (p.date_upd IS NULL OR p.date_upd = '') ";

        if(!empty($not_in_list)) {
        $sql .= " AND ( CASE
                WHEN pa.reference IS NOT NULL THEN pa.reference NOT IN ('".  implode("','", $not_in_list)."')
                WHEN p.reference IS NOT NULL THEN p.reference NOT IN ('".  implode("','", $not_in_list)."')
                END ) ";
        }
        $sql .= " ORDER BY p.id_product, pa.id_product_attribute ASC ; ";
       
        $result = $this->db->product->db_query_str($sql);
        foreach ($result as $key => $data){
            if(!empty($data['sku'])){
                $product_list[$key]['id_product'] = $data['id_product'];
                $product_list[$key]['id_product_attribute'] = $data['id_product_attribute'];
                $product_list[$key]['sku'] = $data['sku'];
                $product_list[$key]['id_category'] = $data['id_category'];
                $product_list[$key]['id_profile'] = $data['id_profile'];
            }
        }
       
        return $product_list;
    }
    
    public function get_advertising_access_token($id_shop, $id_country){
        $table = self::$m_pref.'amazon_advertising';
        $expire_date = date('Y-m-d H:i:s', strtotime("-30 minutes"));
        $sql = "SELECT field_value FROM $table WHERE id_country = $id_country AND id_shop = $id_shop AND field_name = 'access_token' AND date_upd >= '$expire_date' ;" ;
        $result = $this->db->product->db_array_query($sql);
        foreach ($result as $value) {
            return $value['field_value'];
        }
    }

    public function get_advertising($id_shop, $id_country, $field_name=null, $field_value=null){
        
        // Alter amazon_profile
        if (!Db::fieldExists(self::$m_pref.'amazon_profile', 'advertising')) {
            if(!$this->db->product->db_exec('ALTER IGNORE TABLE ' . self::$m_pref.'amazon_profile ADD COLUMN advertising TEXT AFTER category ')){
                return (false);
            }
        }

        $row = array();
        $table = self::$m_pref.'amazon_advertising';
        $name = $values = '' ;      
        if(isset($field_name) && !empty($field_name)) {
            $name = "AND field_name = '$field_name' " ;
        }
        if(isset($field_value) && !empty($field_value)) {
            $values = "AND field_value = '$field_value' " ;
        }
        $sql = "SELECT * FROM $table WHERE id_country = $id_country  AND id_shop = $id_shop $name  $values;" ;
        $result = $this->db->product->db_array_query($sql);
        foreach ($result as $value) {
            $row[$value['field_name']] = $value['field_value'];
        }
        return $row; 
    }

    public function saveProductsAdvertising($data, $id_shop, $id_country=null){
        $save_data = array();
        $sql = '';
        $table = self::$m_pref.'amazon_advertising_products';
        if (isset($data) && !empty($data)) {
            $save_data['id_shop'] = $id_shop;
            $save_data['id_country'] = $id_country;
            $save_data['date_upd'] = date('Y-m-d H:i:s');
            foreach ($data as $field_value){
                $save_data = $field_value;
                $sql .= $this->replace_string($table, $save_data);
            }
        }

        return $this->exec_query($sql);
    }

    public function save_advertising($id_shop, $id_country, $data){

        $save_data = array();
        $sql = '';
        $table = self::$m_pref.'amazon_advertising';
        if (isset($data) && !empty($data)) {
            $save_data['id_shop'] = $id_shop;
            $save_data['id_country'] = $id_country;
            $save_data['date_upd'] = date('Y-m-d H:i:s');
            foreach ($data as $field_name => $field_value){
                $save_data['field_name'] = $field_name;
                $save_data['field_value'] = $field_value;
                $sql .= $this->replace_string($table, $save_data);
            }
        }
        
        return $this->exec_query($sql);
    }

    public function remove_advertising($id_shop, $id_country, $field_name=null, $field_value=null){
        $where = array(
            "id_shop" => array('operation' => "=", 'value' => $id_shop),
            "id_country" => array('operation' => "=", 'value' => $id_country),
        );
        if(isset($field_name) && !empty($field_name)) {
            $where['field_name'] =  array('operation' => "=", 'value' => $field_name) ;
        }
        if(isset($field_value) && !empty($field_value)) {
            $where['field_value'] = array('operation' => "=", 'value' => $field_value) ;
        }
        return $this->db->product->db_exec($this->delete_string(self::$m_pref.'amazon_advertising', $where));
    }

    public function log_advertising($id_shop, $id_country, $action, $message){
        $save_data = array();
        $sql = '';
        $table = self::$m_pref.'amazon_advertising_log';
        if (!empty($action) && !empty($message)) {
            $save_data['id_shop'] = $id_shop;
            $save_data['id_country'] = $id_country;
            $save_data['action'] = $action;
            $save_data['message'] = $message;
            $sql .= $this->replace_string($table, $save_data);
        }
        return $this->exec_query($sql);
    }

    public function removeDeletedProductsFromAmazon($id_shop){

        $exts = AmazonUserData::getAmazonCountriesByUsername($this->user, $id_shop, true);

        $product_sku_list = $product_active_list= $flagUpdateReprice = $amazon_inventory_list = array();
        
        //1. get deleted products
        $sql = "SELECT
                p.id_product AS id_product,
                pa.id_product_attribute AS id_product_attribute,
                CASE
                    WHEN pa.reference IS NOT NULL THEN pa.reference
                    WHEN p.reference IS NOT NULL THEN p.reference
                END AS sku,
                p.reference AS product_reference
                FROM ".self::$p_pref."product p
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                WHERE p.id_shop = " . $id_shop . " AND p.active = 0 AND ( p.date_upd != '' OR  p.date_upd IS NOT NULL) "
            . " UNION SELECT
                p.id_product AS id_product,
                CASE
                    WHEN pa.old_reference IS NOT NULL THEN pa.id_product_attribute
                    ELSE 0 
                END AS id_product_attribute,
                CASE
                    WHEN pa.old_reference IS NOT NULL THEN pa.old_reference
                    WHEN p.old_reference IS NOT NULL THEN p.old_reference
                END AS sku,
                CASE
                    WHEN pa.old_reference IS NOT NULL THEN pa.old_reference
                    WHEN p.old_reference IS NOT NULL THEN p.old_reference
                END AS product_reference
                FROM ".self::$p_pref."product p
                LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                WHERE p.id_shop = " . $id_shop . " AND (((p.old_reference != '' OR p.old_reference IS NOT NULL) AND p.flag_update_ref >= (NOW() - INTERVAL 2 DAY))
                OR ((pa.old_reference != '' OR pa.old_reference IS NOT NULL) AND pa.flag_update_ref >= (NOW() - INTERVAL 2 DAY)) ) ; ";
        
        $result = $this->db->product->db_query_str($sql);
        if (isset($result) && !empty($result)) {
            $product_list = $result;
            foreach ($result as $product) {
                if(isset($product['product_reference']))
                    $product_sku_list[] = $product['product_reference'];
            }
        }
    
        //2. get active products
        if(!empty($product_sku_list)) {
            $sql2 = "SELECT
                    p.id_product AS id_product,
                    pa.id_product_attribute AS id_product_attribute,
                    CASE
                        WHEN pa.reference IS NOT NULL THEN pa.reference
                        WHEN p.reference IS NOT NULL THEN p.reference
                    END AS sku
                    FROM ".self::$p_pref."product p
                    LEFT JOIN ".self::$p_pref."product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                    WHERE p.id_shop = " . $id_shop . " AND p.active = 1
                    AND (p.reference IN ('".implode("', '", $product_sku_list)."') OR pa.reference IN ('".implode("', '", $product_sku_list)."')) ; ";
            $result2 = $this->db->product->db_query_str($sql2);
            foreach ($result2 as $product_active) {
                $key = $product_active['sku'];
                $product_active_list[$key]['id_product'] = $product_active['id_product'];
                $product_active_list[$key]['id_product_attribute'] = $product_active['id_product_attribute'];
                $product_active_list[$key]['sku'] = $product_active['sku'];
            }
        }
       
        //3.skip when this sku aleady have new id_product
        if(!empty($product_sku_list)) {
            $sql3 = "SELECT t1.id_product, t1.id_product_attribute, t1.sku
                    FROM ".self::$m_pref."amazon_inventory t1
                    WHERE t1.id_shop = " . $id_shop . " AND t1.sku IN ('".implode("', '", $product_sku_list)."'); " ;
            $result3 = $this->db->product->db_query_str($sql3);
            foreach ($result3 as $amazon_inventory) {
                $key = (int)$amazon_inventory['id_product'];
                $key2 = (int)$amazon_inventory['id_product_attribute'];
                $key3 = $amazon_inventory['sku'];
                //$amazon_inventory_list[$key][$key2]['id_product'] = $amazon_inventory['id_product'];
                //$amazon_inventory_list[$key][$key2]['id_product_attribute'] = $amazon_inventory['id_product_attribute'];
                $amazon_inventory_list[$key][$key2][$key3] = $amazon_inventory['sku'];
            }
        }

        //4. update product id from amazon tablec
        $update_sql = $ImportLog = $br = '';
        if($this->debug){
            $br = "\r\n";
        }
       
        foreach ($product_list as $product){
            if(!isset($product['sku']) || empty($product['sku'])){
                continue;
            }
            $id_product = (int) $product['id_product'];
            $id_product_attribute = (int) $product['id_product_attribute'];
            $sku = $product['sku'];

            // if they have another id_product and it is active && the product have new id already
            if(isset($product_active_list[$sku]) && !empty($product_active_list[$sku])) {
                  
                $id_product_ative = (int)$product_active_list[$sku]['id_product'];
                $id_product_attribute_ative = (int)$product_active_list[$sku]['id_product_attribute'];

                if($this->debug){
                    echo '<pre>' . print_r('id_product_ative - '.$id_product_ative, true) . '</pre>' ;
                    echo '<pre>' . print_r('id_product - ' . $id_product, true) . '</pre>' ;
                    echo '<pre>' . print_r('id_product_attribute_ative - ' . $id_product_attribute_ative, true) . '</pre>' ;
                    echo '<pre>' . print_r('id_product_attribute - ' . $id_product_attribute, true) . '</pre>' ;
                    echo '<pre>' . print_r('sku - ' . $sku, true) . '</pre>' ;
                }

                // if the active id was not in record we update the old id_product to new id_product
                if((!isset($amazon_inventory_list[$id_product_ative]) || !isset($amazon_inventory_list[$id_product_ative][$id_product_attribute_ative]))
                    && isset($amazon_inventory_list[$id_product]) && $amazon_inventory_list[$id_product][$id_product_attribute]) {
                    
                    $update_sql .= "UPDATE ".self::$m_pref."amazon_inventory SET id_product = $id_product_ative, id_product_attribute = $id_product_attribute_ative "
                        . "WHERE id_shop = $id_shop "
                        . "AND id_product = $id_product "
                        . "AND id_product_attribute = $id_product_attribute "
                        . "AND sku = '$sku';".$br;
                    $update_sql .= "UPDATE ".self::$m_pref."amazon_product_option SET id_product = $id_product_ative, id_product_attribute = $id_product_attribute_ative "
                        . "WHERE id_shop = $id_shop "
                        . "AND id_product = $id_product "
                        . "AND id_product_attribute = $id_product_attribute "
                        . "AND sku = '$sku';".$br;
                    $update_sql .= "UPDATE ".self::$m_pref."amazon_repricing_product_strategies SET id_product = $id_product_ative, id_product_attribute = $id_product_attribute_ative "
                        . "WHERE id_shop = $id_shop "
                        . "AND id_product = $id_product "
                        . "AND id_product_attribute = $id_product_attribute "
                        . "AND sku = '$sku';".$br;
                    $update_sql .= "UPDATE ".self::$m_pref."amazon_repricing_flag SET id_product = $id_product_ative, id_product_attribute = $id_product_attribute_ative "
                        . "WHERE id_shop = $id_shop "
                        . "AND id_product = $id_product "
                        . "AND id_product_attribute = $id_product_attribute "
                        . "AND sku = '$sku';".$br;

                    // 3. flag update product all contry
                    foreach ($exts as $ext => $id_country){
                        // offer
                        $ImportLog .= ImportLog::update_flag_offers(_ID_MARKETPLACE_AMZ_, $ext, $id_shop, $id_product_ative, 1);
                        // reprice
                        $flagUpdateReprice[$sku] = array(
                                'SKU' => $sku,
                                'id_product' => (int)$id_product_ative,
                                'id_product_attribute' => (int)$id_product_attribute_ative,
                            );
                    }

                // if the active id was in record we remove the incorrect data
                } else {
                    if($this->debug){
                        echo '<pre>' . print_r($amazon_inventory_list[$id_product], true) . '</pre>' ;
                    }
                    if(isset($amazon_inventory_list[$id_product]) && isset($amazon_inventory_list[$id_product][$id_product_attribute]) && isset($amazon_inventory_list[$id_product][$id_product_attribute][$sku]) ) {                        
                        $update_sql .= "DELETE FROM ".self::$m_pref."amazon_inventory "
                                      ."WHERE id_shop = $id_shop AND id_product = $id_product AND id_product_attribute = $id_product_attribute AND sku = '$sku';".$br;
                        $update_sql .= "DELETE FROM ".self::$m_pref."amazon_product_option "
                                      ."WHERE id_shop = $id_shop AND id_product = $id_product AND id_product_attribute = $id_product_attribute AND sku = '$sku';".$br;
                        $update_sql .= "DELETE FROM ".self::$m_pref."amazon_repricing_product_strategies "
                                      ."WHERE id_shop = $id_shop AND id_product = $id_product AND id_product_attribute = $id_product_attribute AND sku = '$sku';".$br;
                        $update_sql .= "DELETE FROM ".self::$m_pref."amazon_repricing_flag "
                                      ."WHERE id_shop = $id_shop AND id_product = $id_product AND id_product_attribute = $id_product_attribute AND sku = '$sku';".$br;

                        //file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/sql_remove_amazon_product.txt',
                        //    print_r(array('date'=>date('Y-m-d H:i:s'), 'sql'=>$update_sql), true), FILE_APPEND);
                    }
                }
            } 
        }
       
        if($this->debug){           
            echo '</br><b>'. ('UpdateProduct'). '</b></br>';
            echo '<pre>'. ($update_sql);

            echo '</br><b>'. ('FlagUpdateOffer'). '</b></br>';
            echo '<pre>'. ($ImportLog);

            foreach ($exts as $ext => $id_country){
                $flagUpdateRepriceList = $this->update_amazon_repricing_flag($id_shop, $id_country, $flagUpdateReprice, 1);
                echo '</br><b>'. ('FlagUpdateReprice'). '</b></br>';
                echo '<pre>'. ($flagUpdateRepriceList);
            }
        } else {
            if(strlen($update_sql) > 10){
                if($this->exec_query($update_sql, true, true, true)){
                    if(strlen($ImportLog) > 10){
                        $this->exec_query($ImportLog, true, true, true);
                    }
                    if(count($flagUpdateReprice) > 0){
                        foreach ($exts as $ext => $id_country){
                            $flagUpdateRepriceList = $this->update_amazon_repricing_flag($id_shop, $id_country, $flagUpdateReprice, 1);
                            $this->exec_query($flagUpdateRepriceList, true, true, true);
                        }
                    }
                }
            }
        }
        // Remove missing product attribute
        /*$sql_remove_amazon_product_option = "DELETE ".self::$m_pref."amazon_product_option FROM ".self::$m_pref."amazon_product_option
            LEFT JOIN ".self::$p_pref."product_attribute ON ".self::$m_pref."amazon_product_option.id_product = ".self::$p_pref."product_attribute.id_product
            AND ".self::$m_pref."amazon_product_option.id_shop = ".self::$p_pref."product_attribute.id_shop
            AND ".self::$m_pref."amazon_product_option.id_product_attribute = ".self::$p_pref."product_attribute.id_product_attribute
            WHERE ".self::$p_pref."product_attribute.reference IS NULL";
        if($this->debug){
            var_dump('remove_amazon_product_option', $sql_remove_amazon_product_option);
        } else {
            file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/sql_remove_amazon_product_option.txt',
                print_r(array('date'=>date('Y-m-d H:i:s'), 'sql_remove_amazon_product_option'=>$sql_remove_amazon_product_option), true), FILE_APPEND);
            $this->db->exec_query($sql_remove_amazon_product_option, true, true, true);
        }
        $sql_remove_amazon_repricing_product_strategies = "DELETE ".self::$m_pref."amazon_repricing_product_strategies FROM ".self::$m_pref."amazon_repricing_product_strategies
            LEFT JOIN ".self::$p_pref."product_attribute ON ".self::$m_pref."amazon_repricing_product_strategies.id_product = ".self::$p_pref."product_attribute.id_product
            AND ".self::$m_pref."amazon_repricing_product_strategies.id_shop = ".self::$p_pref."product_attribute.id_shop
            AND ".self::$m_pref."amazon_repricing_product_strategies.id_product_attribute = ".self::$p_pref."product_attribute.id_product_attribute
            WHERE ".self::$p_pref."product_attribute.reference IS NULL";
        if($this->debug){
            var_dump('remove_amazon_repricing_product_strategies', $sql_remove_amazon_repricing_product_strategies);
        } else {
            file_put_contents('/var/www/backend/assets/apps/users/'.$this->user.'/amazon/sql_remove_amazon_repricing_product_strategies.txt',
                print_r(array('date'=>date('Y-m-d H:i:s'), 'sql_remove_amazon_repricing_product_strategies'=>$sql_remove_amazon_repricing_product_strategies), true),
                FILE_APPEND);
            $this->db->exec_query($sql_remove_amazon_repricing_product_strategies, true, true, true);
        }*/
    }

    public function truncate_table($table, $id_shop = null, $id_country = null, $where_fields = null, $no_prefix = true) {
        $where = '';
        if (isset($id_shop) && !empty($id_shop))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . ' id_shop = ' . $id_shop . ' ';

        if (isset($id_country) && !empty($id_country))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . 'id_country = ' . $id_country . ' ';

        if (isset($where_fields) && !empty($where_fields))
            $where .= ( ( strlen($where) > 3 ) ? ' AND ' : ' WHERE ') . $where_fields;

        return $this->db->product->truncate_table($table . $where . " ; ", $no_prefix);
    }
    
    public function replace_string($table, $data, $multi_value = false, $no_prefix = true) {
        if (isset($data) && !empty($data)) {
            return $this->db->product->replace_string($table, $data, $multi_value, $no_prefix);                   
        }
        return null;
    }
    
    public function update_string($table, $data, $where = array(), $limit = 0, $no_prefix = true) {
        if (isset($data) && !empty($data)) {
            //$filter_data = $this->_filter_data($table, $data);
            return $this->db->product->update_string($table, $data, $where, $limit, $no_prefix);
        }
        return null;
    }
    
    public function insert_string($table, $data, $no_prefix = true) {
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            return $this->db->product->insert_string($table, $filter_data, $no_prefix);
        }
        return null;
    }
    
    public function delete_string($table, $where = array(), $no_prefix = true) {
	return $this->db->product->delete_string($table, $where, $no_prefix);
    }

    public function exec_query($sql, $transaction=true, $multi_query=true, $direct=false, $commit=true, $asyn=false ) {
        return $this->db->product->db_exec($sql, $transaction , $multi_query, $direct, $commit, $asyn);        
    }

    public function _filter_data($table, $data) {
        $filtered_data = array();
        $columns = $this->_list_fields($table);
        if (is_array($data) && $columns!==FALSE) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data)) {
                    if (is_array($data[$column])) {
                        $filtered_data[$column] = Amazon_Tools::escape_str(serialize($data[$column]));
                    } else {
                        if ($data[$column] == '0') {
                            $filtered_data[$column] = $data[$column];
                        } else {
                            $filtered_data[$column] = Amazon_Tools::escape_str($data[$column]);
                        }
                    }
                } else {
                    $filtered_data[$column] = '';
                }
            }
        }
        return $filtered_data;
    }

    public function _list_fields($table) {
        return $this->db->product->get_all_column_name($table, true, true);
    }    
    
    public function db_table_exists($table, $no_prefix = true){
        return $this->db->product->db_table_exists($table, $no_prefix);
    }
    
    public function db_from($table, $alias = null, $no_prefix = true){
        return  $this->db->product->from($table, $alias, $no_prefix);
    }
    
    public function db_leftJoin($table, $alias = null, $on = null, $no_prefix = true){
        return  $this->db->product->leftJoin($table, $alias, $on, $no_prefix);
    }    
}