<?php

require_once 'amazon.config.php';
require_once 'amazon.validValues.php';
require_once 'amazon.field.settings.php';
require_once 'amazon.exception.php';

class AmazonForm {

    private $mainDom;
    private $mainXPath;
    private $baseDom;
    private $baseXPath;
    private $productDom;
    private $productXPath;
    private $productInstance;
    private $productName;
    private $smarty;
    private $recommendedPerUniverseFields;
    private $requiredFields;
    private $htmlFieldTemplates = array();
    public static $requireMfrPartNumber = array(
	'AutoAccessoryMisc',
        'AutoPart',
        'PowersportsPart',
        'PowersportsVehicle',
        'ProtectiveGear',
        'Helmet',
        'RidingApparel',
        'Tire',
        'Rims',
        'TireAndWheel',
        'Vehicle',
        'Motorcyclepart',
        'Motorcycleaccessory',
        'Ridinggloves',
        'Ridingboots',
        'PaperProducts',
        'PhoneAccessory',
        'Phone',
        'DigitalCamera',
        'Gifts_and_Occasions',
        'ApplianceAccessory',
        'Kitchen',
        'PersonalCareAppliances',
        'EntertainmentMemorabilia',
        'Musical_Instruments',
        'PetSuppliesMisc',
        'PersonalCareAppliances'
    );
    private $ignoredFields = array("ClothingType" => 1, "ClassificationData" => 1);
    private $ignoredFieldsForSpecificField = array("VariationTheme" => 1, "VariationData" => 1, "ColorMap" => 1, "SizeMap" => 1);
    private $ext;
    private static $universe_translate;
    private static $product_type_translate;
    private static $field_translation;
    private static $exception_field;
    private $models_settings;

    //public static $variation_theme_exception = array('SizeName'=>'Size', 'ColorName'=>'Color', 'SizeName-ColorName'=>'SizeColor') ; 
    
    /**
     * AmazonForm Constructor
     * @global type $smarty
     * @param type $product_universe
     */
    public function __construct($product_universe, $ext = null) {

        $this->productName = $product_universe;
        $this->productInstance = new stdClass();
        $this->mainDom = AmazonXSD::getMainDom();
        $this->mainXPath = AmazonXSD::getMainXPath();
        $this->baseDom = AmazonXSD::getBaseDom();
        $this->baseXPath = AmazonXSD::getBaseXPath();

        //Load Product XSD
        $this->productDom = new DOMDocument();
        $this->productDom->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . $product_universe . '.xsd'));
        $this->productDom->formatOutput = true;
        $this->productXPath = new DOMXPath($this->productDom);

        global $smarty;
        $this->smarty = $smarty;

        $this->ext = $ext;
        $this->getRecommendedPerUniverseFields($this->productName, $this->ext);
        self::$universe_translate = AmazonSettings::get_universes_translation($this->ext);
        self::$product_type_translate = AmazonSettings::get_product_type_translation($this->ext, $this->productName);
        self::$field_translation = AmazonSettings::get_field_translation($this->ext, $this->productName);
        self::$exception_field = isset(Amazon_Exception::$productTypeException[$this->productName]) ? Amazon_Exception::$productTypeException[$this->productName] : '';
    }

    // getrecommended from database 
    private function getRecommendedPerUniverseFields($universe, $ext = null) {

        $this->requiredFields = array(
            'ProductClothing' => array('Color', 'Size', 'Department'),
            'ClothingAccessories' => array('Color', 'Size', 'Department')
        );

        $this->recommendedPerUniverseFields = array(
            'Shoes' => array('Color', 'CollectionName', 'HeelType', 'HeelHeight', 'LeatherType', 'InnerMaterial', 'MaterialComposition', 'MaterialType', 'ModelYear', 'OuterMaterialType', 'Season', 'ShaftHeight', 'ShaftWidth', 'ShaftDiameter', 'ShoeClosureType', 'SoleMaterial', 'StyleKeywords', 'StyleName', 'BootOpeningCircumference'),
            'ProductClothing' => array('Department', 'StyleKeywords', 'Season', 'OuterMaterial', 'InnerMaterial', 'MaterialComposition', 'ModelYear', 'Color', 'CollectionName', 'IsAdultProduct', 'CareInstructions', 'SpecialSizeType'),
            'Books' => array('Author', 'Edition', 'Illustrator', 'Language', 'PublicationDate', 'SignedBy', 'Volume'),
            'Luggage' => array('Collection', 'Season', 'ModelYear', 'MaterialType', 'InnerMaterialType', 'ShellType', 'ClosureType'),
            'Home' => array('Material'),
            //'ClothingAccessories' => array('Color', 'Size', 'Department'),
	       //'Toys' => array('MinimumManufacturerAgeRecommended', 'MaximumManufacturerAgeRecommended', 'MinimumMerchantAgeRecommended', 'MaximumMerchantAgeRecommended'),
        );

        $this->recommendedPerTypeFields = array(
            'Watch' => array('BandColor', 'BandMaterial', 'BandLength', 'BandWidth', 'DialColor', 'MovementType', 'WaterResistantDepth', 'ModelYear', 'Season', 'TargetGender'),
            'Phone' => array('OperatingSystem', 'ScreenSize', 'TelephoneType'),
            'Speakers' => array('ConnectorType', 'NumberOfSpeakers', 'MaximumWattage'),
            'LightsAndFixtures' => array('BulbType', 'ColorTemperature', 'DisplayHeight', 'DisplayLength', 'DisplayWeight', 'DisplayWidth', 'Color', 'IncandescentEquivalentWattage', 'Wattage', 'PlugType'),
            'Pants' => array('InseamLength', 'WaistSize')
        );

        if (isset($ext) && !empty($ext)) {

            $valid_value = new ValidValue($ext);

            $attribute_lists = $valid_value->getAttributesForProductType($universe);
            
            foreach ($attribute_lists as $attribute_field => $value) {
                if (!isset($valid_value->AttributePrimaryFields[$attribute_field])) {
                    if (strlen($attribute_field) > 3) {
                        $reference = $this->getReferencedElement($attribute_field);
                        if (isset($reference) && !is_null($reference) && isset($universe)) {
                            if (!isset($this->recommendedPerUniverseFields[$universe])) {
                                $this->recommendedPerUniverseFields[$universe] = array();
                            }
                            array_push($this->recommendedPerUniverseFields[$universe], $attribute_field);
                            
                            if(self::isMandatory($reference)){
                                array_push($this->requiredFields[$universe], $attribute_field);
                            }
                        }
                    }
                }
            }
           
            // add recommeded field from ClassificationData for shoes, clothing accessory, shoes : 2016-11-23
            /*$classificationData_query = '//xsd:complexType/xsd:sequence/xsd:element[@name="ClassificationData"]/xsd:complexType/xsd:sequence/*';
            $classificationData_element = $this->productXPath->query($classificationData_query);
            if($classificationData_element->length > 0) {
                foreach ($classificationData_element as $element) {
                    $field_name = strval($this->getElementName($element));
                    if (!isset($this->recommendedFields[$universe])) {
                        $this->recommendedFields[$universe] = array();
                    }
                    array_push($this->recommendedFields[$universe], $field_name); 
                }
            }*/
       }
    }



    /**
     * If an XSD element has the "Ref" attribute, this function returns the
     * structure o the referenced element
     * @param type $element_name
     * @return type
     */
    private function getReferencedElement($element_name) {
        $reference = null;
        $reference_search = $this->productXPath->query('//xsd:element[@name="' . $element_name . '"]');       
        if ($reference_search && $reference_search->length > 0) {
            $reference = $reference_search->item(0);
        }
        return $reference;
    }

    /**
     * Get all amazon categories as <option> element,  
     * defined in XSD as included files
     * @return array
     */
    public static function getProductsUniverse($selected) {
        $_element = array();
        $cat = array();

        try {
            $x = AmazonXSD::getMainXPath();
            $xclude = array('AdditionalProductInformation.xsd', 'FBA.xsd', 'amzn-base.xsd', 'Amazon.xsd', 'MaterialHandling.xsd');
            $query = '//xsd:schema/xsd:include';
            $categories = $x->query($query);

            foreach ($categories as $category):
                $val = $category->getAttribute('schemaLocation');
                if (isset($val) && !in_array($val, $xclude)):
                    $cat[] = str_replace(".xsd", "", $val);
                endif;
            endforeach;

            //sort($cat, SORT_NATURAL | SORT_FLAG_CASE); //fails on PHP 5.3.28
            array_multisort($cat);
            $_element[] = '<option value="">Select an option</option>';
            $selected_universe = null;
            foreach ($cat as $c) {
                if ($selected == $c) {
                    $selected_universe = $c;
                }
                $displayName = self::getName($c, AmazonSettings::$universe);
                $_element[] = '<option value="' . $c . '" ' . ($selected == $c ? 'selected="selected"' : '') . '>' . $displayName . '</option>' . PHP_EOL;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $_element[] = '<option value="">' . $e->getMessage() . '</option>';
        }
        //set smarty data
        return $_element;
    }

    /**
     * Get Product types based on a category selected
     * @param type $selected value of current selected product type
     * @return string html code for product type options
     */
    public function getProductTypes($selected = null) {

        if (isset(AmazonXSD::$productTypeExceptions[$this->productName])) {
            $query = AmazonXSD::$productTypeExceptions[$this->productName];
        } elseif (isset(AmazonXSD::$productTypeSimpleType[$this->productName])) {
            $query = '//xsd:schema/xsd:element[@name="' . $this->productName . '"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]/xsd:simpleType/xsd:restriction/*';
        } else {
            $query = '//xsd:schema/xsd:element[@name="' . $this->productName . '"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]/xsd:complexType/xsd:choice/*';
        }
        
        $types = $this->productXPath->query($query);
        $html = '<option value="">Select an option</option>';
        $_elements = array();
        
        if ($types->length > 0)
            foreach ($types as $c):
                $displayName = self::getName($this->getElementName($c), AmazonSettings::$product_types);
                $value = $this->getElementName($c);
                $_elements[] = '<option value="' . $value . '" ' . ($selected == $value ? 'selected="selected"' : '') . '>' . $displayName . '</option>' . PHP_EOL;
            endforeach;

        array_multisort($_elements);
        foreach ($_elements as $el)
            $html .= $el;

        if (!isset($_elements) || empty($_elements))
            return;

        return $html;
    }

    /**
     * Get Product SubType, when it applies
     * @param type $selected
     * @return string
     */
    public function getProductSubtype($product_type, $selected = null) {

        $html = null;
        if (!$product_type) {
            return $html;
        }

        if (isset(AmazonXSD::$productSubtype[$this->productName])) {
            $query = AmazonXSD::$productSubtype[$this->productName];
        } else {
            return $html;
        }

        $sub_type = $this->productXPath->query($query);
        $_elements = array();
        foreach ($sub_type as $c) {
            $_elements[] = $this->getFieldGroupHtml($c, $selected, null, false, false, false, true, false, 'Product Sub type');
        }

        array_multisort($_elements);
        foreach ($_elements as $el)
            $html .= $el;

        return $html;
    }

    /**
     * Get MFR Part Number, when it applies
     * @param type $selected
     * @return string
     */
    public function getMfrPartNumber($product_type, $selected = null) {
        $html = null;
        if (!$product_type || !in_array($product_type, self::$requireMfrPartNumber)) {
            return $html;
        }

        $mfr_part_number = $this->mainXPath->query('//xsd:element[@name="MfrPartNumber"]');

        $_elements = array();
        foreach ($mfr_part_number as $c) {
            $mfr = $this->productDom->importNode($c->cloneNode(true), true);
            $_elements[] = $this->getFieldGroupHtml($mfr, $selected, null, false, false, false, true, false, 'Mfr Part Number');
        }

        foreach ($_elements as $el)
            $html .= $el;

        return $html;
    }

    /**
     * Get Product types based on a category selected
     * @param type $selected valur for current selected Variation Theme
     * @return string
     */
    public function getVariationTheme($product_type, $selected = null) {
                
        $html = null;
        if (!$product_type) {
            return $html;
        }

        if (isset(AmazonXSD::$variationThemeExceptions[$this->productName])) {
            $query = AmazonXSD::$variationThemeExceptions[$this->productName];
        } else {
            $query = '//xsd:schema/xsd:element[@name="' . $product_type . '"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationTheme"]';
        }

        $variation_theme_options = $this->productXPath->query($query);

        $_elements = array();
        foreach ($variation_theme_options as $c) {
            $name = $this->getElementName($c);   
            $title = $this->getTooltips($product_type, $name);            
            $_elements[] = $this->getFieldGroupHtml($c, $selected, null, true, false, false, true, false, false, $title);
        }
	
        array_multisort($_elements);
        foreach ($_elements as $el)
            $html .= $el;

        return $html;
    }

    /**
     * Get list of an element's variation data (mandatory items)
     * @param type $product_type
     * @return type
     */
    public function getVariationData($product_type, $variation_theme = null, $selected = array(), $returnKeys = false) {

        $_element = array();
        $variation_data = null;
        $recommended_data = null;		
		
        if (!$product_type) {
            return array("variation" => $variation_data, "recommended" => $recommended_data);
        }

        $additional_keys = array(); //only to store keys that will be ignored        
        $variationDataElements = array();
        $reference = $this->getReferencedElement($product_type, true);
	
        if (is_null($reference)) {
            //find selected variation
            $query = '//xsd:element[@name="' . $variation_theme . '"]';
            $element = $this->productXPath->query($query);
        } else {
            //find selected variation
            $query = './/xsd:element[@name="' . $variation_theme . '"]';	
            $element = $this->productXPath->query($query, $reference);
        }

        if ($element->length > 0) {
            $variationDataElements[$variation_theme] = $element->item(0);
        }
	
        //maybe it's a combination of more than one elements
        $names = array();

        if (preg_match_all('/[\w\d]+[^-^_^\s]/', $variation_theme, $names)) {
            foreach ($names[0] as $name) {

                if (isset($variationDataElements[$name]))
                    continue;
                if (is_null($reference)) {
                    //find selected variation
                    $query = '//xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query);
                } else {
                    //find selected variation
                    $query = './/xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query, $reference);
                }

                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                } else {
                    $element = $this->productXPath->query($query);
                    if ($element->length > 0) {
                        $variationDataElements[$name] = $element->item(0);
                    } else {
                        $this->getElementFromAdditionalPaths($name, $variationDataElements);
                    }
                }
            }
        }
     
        /*
         *               STARTS EXCEPTIONS FOR SIZE & COLOR
         */
        
        if(isset($name) && ($name == "Size" || $name == "SizeName")){
            $name = 'Size';
            if (is_null($reference)) {
                $query = '//xsd:element[@name="' . $name . '"]';
                $element = $this->productXPath->query($query);
            } else {
                $query = './/xsd:element[@name="' . $name . '"]';
                $element = $this->productXPath->query($query, $reference);
            }
            if ($element->length > 0) {
                $variationDataElements[$name] = $element->item(0);
            }  
            //$sizename = true;
        }
        
        if(isset($name) && ($name == "Color" || $name == "ColorName")){
            $name = "Color";
                if (is_null($reference)) {
                    $query = '//xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query);
                } else {
                    $query = './/xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query, $reference);
                }
                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                }
        }
        
        if ($variation_theme == "SizeColor" || $variation_theme == "Size-Color" || $variation_theme == "SizeName-ColorName") { //Size-Color //Size-Color
            foreach (array("Size", "Color") as $name) {

                if (is_null($reference)) {
                    //find selected variation
                    $query = '//xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query);
                } else {
                    //find selected variation
                    $query = './/xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query, $reference);
                }
                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                } else {
                    $this->getElementFromAdditionalPaths($name, $variationDataElements);
                }
            }

            //If Color is not set, but ColorMap/ColorName is set, ColorMap will be added
            if (!isset($variationDataElements["Color"])) {
                $name = "ColorName";
                if (is_null($reference)) {
                    $query = '//xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query);
                } else {
                    $query = './/xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query, $reference);
                }
                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                }
            }

            //If Color is not set, but ColorMap/ColorName is set, ColorMap will be added
            if (!isset($variationDataElements["Size"])) {
                $name = "SizeName";
                if (is_null($reference)) {
                    $query = '//xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query);
                } else {
                    $query = './/xsd:element[@name="' . $name . '"]';
                    $element = $this->productXPath->query($query, $reference);
                }
                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                }
            }
        }
        /* ENDS EXCEPTIONS FOR SIZE & COLOR*/


        // get field setting to find Recommended
        if (!isset($this->models_settings[$this->ext][$this->productName][$product_type])) {
            $this->models_settings[$this->ext][$this->productName][$product_type] = AmazonSettings::get_field_settting($this->ext, $this->productName, $product_type);
        }

        $recommended_elements = array();
        //Check 01 => recommended fields per Universe
        if (isset($this->recommendedPerUniverseFields[$this->productName])) {
            $recommended_elements = $this->recommendedPerUniverseFields[$this->productName];

            if(isset($this->requiredFields[$this->productName])) {
                array_merge($recommended_elements, $this->requiredFields[$this->productName]);
            }
        }

        //Check 02 => recommended fields per type
        if (isset($this->recommendedPerTypeFields[$product_type])) {
            foreach ($this->recommendedPerTypeFields[$product_type] as $recommendedPerTypeFields) {
                array_push($recommended_elements, $recommendedPerTypeFields);
            }
        }
 
        //Check 03=> require MRF part number (defined in Product.xsd )
        if (isset(self::$requireMfrPartNumber[$product_type])) {
            $variationDataElements['MfrPartNumber'] = 1;
        }	
        
        //Include additional elements defined in Check 01, Check 02 and Check 03
        foreach ($recommended_elements as $key => $name) {
            
            if (isset($variationDataElements[$name])) {
                unset($recommended_elements[$key]);
                continue;
            }
            if (is_null($reference)) {
                //find selected variation
                $query = '//xsd:element[@name="' . $name . '"]';
                $element = $this->productXPath->query($query);
            } else {
                //find selected variation
                $query = './/xsd:element[@name="' . $name . '"]';
                $element = $this->productXPath->query($query, $reference);
            }
            if ($element->length > 0) {
                $variationDataElements[$name] = $element->item(0);
            }            
        }
        
        //Check 04=> Additional fields per universe
        if (isset(AmazonXSD::$variationDataAdditionalElements[$this->productName])) {
            foreach (AmazonXSD::$variationDataAdditionalElements[$this->productName] as $name => $query) {
                $element = $this->productXPath->query($query); //element is searched on global product definition
                if ($element->length > 0) {
                    $variationDataElements[$name] = $element->item(0);
                }
            }
        }

        //Avoid to let "map" items, when color/size were already included
        if (isset($variationDataElements["Color"]) || isset($variationDataElements["ColorName"])) {
            $additional_keys[] = "ColorMap";
        }
        if (isset($variationDataElements["Size"]) || isset($variationDataElements["SizeName"])) {
            $additional_keys[] = "SizeMap";
        }
	
        //Obtain HTML for variation data fields
        foreach ($variationDataElements as $name => $c) {

            if (isset($this->ignoredFields[$name]))
                continue;
           
            $container = in_array($name, $recommended_elements) ? 'recommended_data' : 'variation_data';
            $values = isset($selected[$container]) ? $selected[$container] : array();
            $title = $this->getTooltips($product_type, $name);

            //Elements included in $htmlFieldTemplates with "html" attribute, will be displayed
            //Elements included in $htmlFieldTemplates without "html" are omitted, as they're included inside other element
            if (isset($this->htmlFieldTemplates[$name]) && $this->htmlFieldTemplates[$name]['html']) {

                foreach ($this->htmlFieldTemplates[$name]['params'] as $param) {
                    $this->smarty->assign(strtolower($param), in_array($param, array_keys($values)) ? $values[$param] : null);
                }

                $this->smarty->assign('container', $container);
                $this->smarty->assign(strtolower($name . '_options'), array());

                if (isset($this->htmlFieldTemplates[$name]['options'])) {
                    $attr_options = array();
                    foreach ($this->htmlFieldTemplates[$name]['options'] as $opt) {

                        $option = array();
                        $option["value"] = $opt;
                        $option["desc"] = $opt;
                        $option['selected'] = in_array($opt, $values) ? 'selected' : null;
                        $attr_options[] = $option;
                    }
                    $this->smarty->assign(strtolower($name . '_options'), $attr_options);
                }

                $_element[$name] = $this->smarty->fetch($this->htmlFieldTemplates[$name]['html']);
            } elseif (!isset($this->htmlFieldTemplates[$name])) {
                //Obtain real structure (when element references another element)
                $this->getReferencedStructure($c);

                //------------EXIST CHILDREN ELEMENTS------------//
                $query_children = './/xsd:complexType/xsd:sequence/xsd:element';
                $children = $this->productXPath->query($query_children, $c);

                if(isset($title['type']) && $title['type'] == AmazonSettings::MANDATORY)
                    $type = 'a';
                else if(isset($title['type']) && $title['type'] == AmazonSettings::RECOMMENDED)
                    $type = 'b';
                else
                    $type = 'c' ;

                //echo $this->productName;
                if ($children->length > 0) {
                    foreach ($children as $child) {   
                        $child_name = $this->getElementName($child);
                        $e_value = isset($values[$child_name]) ? $values[$child_name] : (isset($values['default']) ? $values['default'] : null) ;
                        if ($returnKeys){
                            $_element[$child_name] = $this->getFieldGroupHtml($child, $e_value, null, true, false, $container, true, true, false, $title);
                        } else {
                            $_element[$type][$child_name] = $this->getFieldGroupHtml($child, $e_value, null, true, false, $container, true, true, false, $title);
                        }
                    }
                } else {                   
                    //Element has no children elements
                    $e_value = isset($values[$name]) ? $values[$name] : (isset($values['default']) ? $values['default'] : null) ;
                    if ($returnKeys){
                        $_element[$name] = $this->getFieldGroupHtml($c, $e_value, null, true, false, $container, true, true, false, $title);
                    } else {
                        $_element[$type][$name] = $this->getFieldGroupHtml($c, $e_value, null, true, false, $container, true, true, false, $title);
                    }
                }
            }
        }
        
        //Returns all keys, to be ignored in other functions
        if ($returnKeys)
            return array_merge(array_keys($_element), $additional_keys);

        ksort($_element);
       
        foreach ($_element as $type => $field_data) {
            foreach ($field_data as $name => $option) {
                if (in_array($name, $recommended_elements)) {
                    $recommended_data .= $option;
                } else {
                    $variation_data .= $option;
                }
            }
        }
        
        //echo $variation_data; exit;
        return array("variation" => $variation_data, "recommended" => $recommended_data);
    }

    /**
     * Obtains <option> elements shown in "add" specific field option
     * @param type $product_type
     * @param type $selected_values
     * @param type $variation_theme
     * @return string
     */
    public function getSpecificOptions($product_type, $selected_values = array(), $variation_theme = null) {
        $html = '';
        $specific = array();
        if (!$product_type) {
            return $html;
        } else {
            $html = '<option value="">Select an option</option>';
        }
	
	if(isset(AmazonXSD::$productType[$product_type]) && !empty(AmazonXSD::$productType[$product_type]))
	{
	    $reference = $this->getReferencedElement(AmazonXSD::$productType[$product_type]);
	} else {
	    $reference = $this->getReferencedElement($product_type);
	}
	
        if (is_null($reference)) {
            //find selected variation
            $query = '//xsd:element[not(@name="VariationData")][not(@name="VariationTheme")]'; //[@minOccurs="0"]';
            $specific[] = $this->productXPath->query($query);
        } else {
            //find selected variation
            $query = './/xsd:element[not(@name="VariationData")][not(@name="VariationTheme")]'; //[@minOccurs="0"]';
            $specific[] = $this->productXPath->query($query, $reference);
        }

        $_elements = array();
        $non_included = $this->getVariationData($product_type, $variation_theme, $selected_values, true);

        foreach ($specific as $specific_fields) {
            foreach ($specific_fields as $c) {

                //Obtain real structure (when element references another element)
                $this->getReferencedStructure($c);
                //------------EXIST CHILDREN ELEMENTS------------//
                $query_children = './/xsd:complexType/xsd:sequence/xsd:element';
                $children = $this->productXPath->query($query_children, $c);

                //This includes children elements instead of name of container
                if ($children->length > 0) {
                    foreach ($children as $child) {
                        $this->getReferencedStructure($child);
                        $displayName = self::getName($this->getElementName($child), AmazonSettings::$attributes);
                        $value = $this->getElementName($child);

                        if ((in_array("Color", $non_included) && $value == "ColorMap") || (in_array("Size", $non_included) && $value == "SizeMap"))
                            continue;

                        if (isset($this->ignoredFields[$value]) || in_array($value, $non_included) || isset($this->ignoredFieldsForSpecificField[$value]))
                            continue;

                        if (isset($this->recommendedPerTypeFields[$product_type]) &&
                                in_array($value, $this->recommendedPerTypeFields[$product_type]))
                            continue;

                        if (isset($this->recommendedPerUniverseFields[$this->productName]) &&
                                in_array($value, $this->recommendedPerUniverseFields[$this->productName]))
                            continue;

                        if (isset(AmazonXSD::$variationDataAdditionalElements[$this->productName]) &&
                                array_key_exists($value, AmazonXSD::$variationDataAdditionalElements[$this->productName]))
                            continue;

                        $maxOccurs = $this->getElementMaxOccurs($child);
                        $max = $maxOccurs ? ' maxOccurs="' . $maxOccurs . '" ' : '';
                        $disabled = in_array($value, array_keys($selected_values)) && !$max ? ' disabled="disabled" ' : '';
                        $_elements[$value] = '<option value="' . $value . '" ' . $disabled . $max . '>' . $displayName . '</option>' . PHP_EOL;
                    }
                }else {
                    $displayName = self::getName($this->getElementName($c), AmazonSettings::$attributes);
                    $value = $this->getElementName($c);

                    if ((in_array("Color", $non_included) && $value == "ColorMap") || (in_array("Size", $non_included) && $value == "SizeMap"))
                        continue;

                    if (isset($this->ignoredFields[$value]) || in_array($value, $non_included) || isset($this->ignoredFieldsForSpecificField[$value]))
                        continue;

                    if (isset($this->recommendedPerTypeFields[$product_type]) &&
                            in_array($value, $this->recommendedPerTypeFields[$product_type]))
                        continue;

                    if (isset($this->recommendedPerUniverseFields[$this->productName]) &&
                            in_array($value, $this->recommendedPerUniverseFields[$this->productName]))
                        continue;

                    if (isset(AmazonXSD::$variationDataAdditionalElements[$this->productName]) &&
                            array_key_exists($value, AmazonXSD::$variationDataAdditionalElements[$this->productName]))
                        continue;

                    $maxOccurs = $this->getElementMaxOccurs($c);
                    $max = $maxOccurs ? ' maxOccurs="' . $maxOccurs . '" currentOccurs="0" ' : '';
                    $disabled = in_array($value, array_keys($selected_values)) && !$max ? ' disabled="disabled" ' : '';
                    $_elements[$value] = '<option value="' . $value . '" ' . $disabled . $max . '>' . $displayName . '</option>' . PHP_EOL;
                }
            }
        }

        //Verification of mfrPartNumber
        if (in_array($product_type, self::$requireMfrPartNumber)) {
            $mfr = "MfrPartNumber";
            $displayName = self::getName($mfr, AmazonSettings::$attributes);
            $value = $mfr;

            $max = 1;
            $disabled = in_array($value, array_keys($selected_values)) && !$max ? ' disabled="disabled" ' : '';
            $_elements[$value] = '<option value="' . $value . '" ' . $disabled . $max . '>' . $displayName . '</option>' . PHP_EOL;
        }

        array_multisort($_elements);

        foreach ($_elements as $el)
            $html .= $el;

        return $html;
    }

    /**
     * Get list of an element's specific fields (not mandatory items)
     * @param type $product_type
     * @param type $item indicates if search must be general, or for a single item
     * @param boolean $multiple indicates if element has maxOccurs > 0
     * @return array
     */
    public function getSpecificFields($product_type, $item = false, $value = null, $multiple = false) {
        
        $_element = array();
        if (!$product_type) {
            return $_element;
        }
        if ($item && $item == "MfrPartNumber") {
            return array($this->getMfrPartNumber($product_type));
        }
        if (isset($this->specificFieldsExceptions[$this->productName])) {
            $query = $this->specificFieldsExceptions[$this->productName];
        } else {
            if (!$item) {
                $query = './/xsd:element[not(@name="VariationData")]'; //[@minOccurs="0"]';
            } else {
                $query = './/xsd:element[@name="' . $item . '"]';
            }
        }
	if(isset(AmazonXSD::$productType[$product_type]) && !empty(AmazonXSD::$productType[$product_type]))
	{
	    $reference = $this->getReferencedElement(AmazonXSD::$productType[$product_type]);
	} else {
	    $reference = $this->getReferencedElement($product_type);
	}
        if (is_null($reference)) {
            if (strpos(".", $query) == 0) {
                $query = substr($query, 1);
            }
            $specific_fields = $this->productXPath->query($query);
        } else {
            $specific_fields = $this->productXPath->query($query, $reference);
        }

        if ($item && $specific_fields->length == 0) {
            //If element is "Ref"
            $query = './/xsd:element[@ref="' . $item . '"]';
            $main_reference = $this->getReferencedElement($product_type);

            if (is_null($main_reference)) {
                //find selected variation
                $query = '//xsd:element[@ref="' . $item . '"]';
                $reference = $this->productXPath->query($query);
            } else {
                //find selected variation
                $query = './/xsd:element[@ref="' . $item . '"]';
                $reference = $this->productXPath->query($query, $main_reference);
            }

            if ($reference->length > 0) {
                $specific_fields = $reference;
            } else {
                //Verify is element is defined as additional
                if (isset(AmazonXSD::$specificFieldsAdditions[$this->productName])) {
                    $query = AmazonXSD::$specificFieldsAdditions[$this->productName] . '[@ref="' . $item . '" or @name="' . $item . '"]';
                    $reference = $this->productXPath->query($query);
                    if ($reference->length > 0)
                        $specific_fields = $reference;
                }
            }
        }

        foreach ($specific_fields as $c) {

            $name = $this->getElementName($c);
            $title = $this->getTooltips($product_type, $name);

            if (!$item && in_array($name, array_keys($this->htmlFieldTemplates)))
                continue;

            if (isset($this->recommendedPerTypeFields[$product_type]) &&
                    in_array($name, $this->recommendedPerTypeFields[$product_type]))
                continue;

            if (isset($this->recommendedPerUniverseFields[$this->productName]) &&
                    in_array($name, $this->recommendedPerUniverseFields[$this->productName]))
                continue;

            if (is_array($value)) {
                $_element[$name] = $this->getFieldGroupHtml($c, isset($value['default']) ? $value['default'] : $value, null, true, true, 'specific_fields', true, true, false, $title);
            } else {
                $_element[$name] = $this->getFieldGroupHtml($c, isset($value['default']) ? $value['default'] : $value, null, true, true, 'specific_fields', true, $multiple, false, $title);
            }
        }

        array_multisort($_element);
        return $_element;
    }

    /**
     * Generates an HTML wrapper (label, input/select/checkbox
     * for an XSD DOMElement
     * @param DOMElement $element Based XSD Node
     * @param string $value current assigned value of the element
     * @param string $additionalStyle additional CSS included in the generated HTML
     * @param boolean $includeLabel indicates if <label> tag should be included contaning the display name of the element
     * @param boolean $removable_option indicates if a button option to "remove" this HTML element will be included
     * @param string $container if set, this element ID/Name will a sub array of the "container" name
     * @param boolean $enabled if set, indicates if HTML element will be enabled/disabled 
     * @param boolean $multiple indicates if element has maxOccurs > 0
     * @return string HTML code corresponding to the XSD element
     */
    private function getFieldGroupHtml(DOMElement $element, $value = null, $additionalStyle = null, $includeLabel = true, $removable_option = false, $container = false, $enabled = true, $multiple = false, $isMain = false, $title = null) {

        $name = $this->getElementName($element);
        $displayName = self::getName($name, AmazonSettings::$attributes);
        $this->getElementStructure($element);

        $label = $name;

        if ($container)
            $label = $container . "[" . $name . "]" . ($multiple ? '[]' : '');

        /*         * *****************************************************************
         *                 FieldGroupHTML.tpl template
         * ***************************************************************** */
        $input_html = $this->getInputFieldHtml($element, false, $value, $container, $enabled, $multiple, $isMain);

        $this->smarty->assign('additionalStyle', $additionalStyle);
        $this->smarty->assign('input_html', $input_html);
        $this->smarty->assign('includeLabel', $includeLabel);
        $this->smarty->assign('label', $label);
        $this->smarty->assign('displayName', $displayName);
        $this->smarty->assign('removable_option', $removable_option);
        $this->smarty->assign('name', $name);
        if ($isMain) {
            $this->smarty->assign('isMain', $isMain);
        } else {
            $this->smarty->assign('isMain', false);
        }

        $this->smarty->assign('displayTitle', 0);
        $this->smarty->assign('required', 0);
        $this->smarty->assign('recommended', 0);

        if (isset($title['html'])) {
            $this->smarty->assign('displayTitle', $title['html']);
        }
        if (isset($title['type'])) {
            if ($title['type'] == AmazonSettings::MANDATORY) {
                $this->smarty->assign('required', 1);
            }
            if ($title['type'] == AmazonSettings::RECOMMENDED) {
                $this->smarty->assign('recommended', 1);
            }
        }
        
        $html = $this->smarty->fetch('amazon/FieldGroupHTML.tpl');
	
        return $html;
    }

    /**
     * 
     * @global type $element_values
     * @param DOMElement $element Based XSD Node
     * @param boolean $isAttribute if set, indicates if element should be treated as an attribute
     * @param string $element_value current assigned value of the element
     * @param string $preffix
     * @param string $container if set, this element ID/Name will a sub array of the "container" name
     * @param boolean $enabled if set, indicates if HTML element will be enabled/disabled 
     * @param boolean $multiple indicates if element has maxOccurs > 0 (on attributes, this is not considered)
     * @return string
     */
    private function getInputFieldHtml(DOMElement $element, $isAttribute = false, $element_value = null, $container = false, $enabled = true, $multiple = false, $isMain = false) {

        global $amzn_attr_values;
        $html = '';

        $disabled = $enabled ? '' : ' disabled="disabled" ';

        $validation_type = $this->getElementType($element);

        if ($isAttribute) {
            $base_id = $this->getElementName($element);
            $id = $this->getElementName($element);
            $this->smarty->assign('isAttribute', true);
        } else {
            $id = $base_id = $this->getElementName($element);
            if ($container)
                $id = $base_id;
            if ($multiple)
                $id .= '';
            $this->smarty->assign('isAttribute', false);
        }

        if (is_array($element_value)) {
            if (isset($element_value[$id]))
                $value = $element_value[$id];
            else
                $value = $element_value;
        } else
            $value = $element_value;

        $cssClass = ' class="' . ($isAttribute ? 'attribute ' : '') . $validation_type . ' "';

        //Look for additional attributes, used on input validation
        $additional_attributes = $this->getElementValidationAttributes($element);

        //------------SELECT------------//
        /*  inputSelectHTML.tpl template */
        $query = 'xsd:simpleType/xsd:restriction/xsd:enumeration';
        $options_xml = $this->productXPath->query($query, $element);
	
        $this->smarty->assign('select_options', false);
        if ($options_xml->length > 0) {
	    
            $cssClass = ' class="' . ($isAttribute ? 'attribute ' : '') . $validation_type . ' search-select"';
            $this->smarty->assign('select_id', $id);
            $this->smarty->assign('select_cssClass', $cssClass);
            $this->smarty->assign('select_disabled', $disabled);
            $this->smarty->assign('select_selected_non', ($value ? '' : ' selected '));

            if ($isMain) {
                $this->smarty->assign('isMain', $isMain);
            } else {
                $this->smarty->assign('isMain', false);
            }

	    if(isset($value) && is_array($value)){
		
		$option = array();
		
		foreach ($value as $specific_fields_keys => $specific_fields) {
	    
		    
		    if (isset($specific_fields['value']) && is_array($specific_fields['value'])) {
			
			foreach ($specific_fields['value'] as $s => $c) {                       
			    $selected = isset($c['selected']) ? ' selected ' : '';
			    if (isset($c['id_attribute_group']) && !empty($c['id_attribute_group'])) {
				$option['attribute'][$s]["value"] = $c['id_attribute_group'];
				$option['attribute'][$s]["selected"] = $selected;
				$option['attribute'][$s]["desc"] = $c['name'];
			    } else if (isset($c['id_feature']) && !empty($c['id_feature'])) {
				$option['feature'][$s]["value"] = $c['id_feature'];
				$option['feature'][$s]["selected"] = $selected;
				$option['feature'][$s]["desc"] = $c['name'];
			    }
			}
		    } else {
			if (!empty($specific_fields)) {
			    $option["value"] = $specific_fields;
			}
		    }
		    //$options = $option;
		}

		$this->smarty->assign('specific_fields', $option);
		$html = $this->smarty->fetch('amazon/inputSelectSpecificField.tpl');
		
	    } else {
		
		$options = array();
		if (is_array($value))
		    foreach ($value as $v)
			if (!is_array($v))
			    $value = $v;
		    
		foreach ($options_xml as $c) {
		    
		    $option = array();
		    $cur_value = $this->getElementValue($c);

		    //$cur_value = isset(self::$variation_theme_exception[$cur_value]) ? self::$variation_theme_exception[$cur_value] : $cur_value;
		    $desc = $isAttribute ? $this->getElementValue($c) : self::getName($this->getElementValue($c), AmazonSettings::$attributes);
		    $selected = $value == $cur_value ? ' selected ' : '';

		    $option["value"] = $cur_value;
		    $option["selected"] = $selected;
		    $option["desc"] = $desc;
		    $options[] = $option;
		}

		$this->smarty->assign('select_options', $options);
		$html = $this->smarty->fetch('amazon/inputSelectHTML.tpl');
	    }
	    
            return $html;
	    
        } else if (is_array($value)) {

            //------------CHECKBOX------------//
            if ($element->getAttribute("type") == "xsd:boolean") {

                /* inputCheckboxHTML.tpl template */
                $checked = (isset($value['value']) && $value['value']) ? ' checked' : '';
                $this->smarty->assign('checkbox_id', $id);
                $this->smarty->assign('checkbox_cssClass', $cssClass);
                $this->smarty->assign('checkbox_checked', $checked);
                $this->smarty->assign('checkbox_disabled', $disabled);
                $html = $this->smarty->fetch('amazon/inputCheckboxHTML.tpl');
                return $html;
            }

            $cssClass = ' class="' . ($isAttribute ? 'attribute ' : '') . $validation_type . ' search-select"';
            $this->smarty->assign('select_id', $id);
            $this->smarty->assign('text_rel', $id);
            $this->smarty->assign('select_cssClass', $cssClass);
            $this->smarty->assign('select_disabled', $disabled);
            $this->smarty->assign('select_selected_non', ($value ? '' : ' selected '));

            if ($id != 'Color' && $id != 'Size')
                $this->smarty->assign('select_custom_value', true);
            else
                $this->smarty->assign('select_custom_value', false);

            $options = array();
            $option = array();

            foreach ($value as $specific_fields_keys => $specific_fields) {
                if (isset($specific_fields['value']) && is_array($specific_fields['value'])) {
                    foreach ($specific_fields['value'] as $s => $c) {                       
                        $selected = isset($c['selected']) ? ' selected ' : '';
                        if (isset($c['id_attribute_group']) && !empty($c['id_attribute_group'])) {
                            $option['attribute'][$s]["value"] = $c['id_attribute_group'];
                            $option['attribute'][$s]["selected"] = $selected;
                            $option['attribute'][$s]["desc"] = $c['name'];
                        } else if (isset($c['id_feature']) && !empty($c['id_feature'])) {
                            $option['feature'][$s]["value"] = $c['id_feature'];
                            $option['feature'][$s]["selected"] = $selected;
                            $option['feature'][$s]["desc"] = $c['name'];
                        }
                    }
                } else {
                    if (!empty($specific_fields)) {
                        $option["value"] = $specific_fields;
                    }
                }
            }

            $this->smarty->assign('specific_fields', $option);

            if ($isMain) {
                $this->smarty->assign('isMain', $isMain);
            } else {
                $this->smarty->assign('isMain', false);
            }

            $html = $this->smarty->fetch('amazon/inputSelectSpecificField.tpl');

            //------------ATTRIBUTES (ADDITIONAL FIELDS)------------//
            $attributes = $this->productXPath->query('xsd:complexType/xsd:simpleContent/xsd:restriction/xsd:attribute', $element);
            if ($attributes->length == 0) {
                $attributes = $this->productXPath->query('xsd:complexType/xsd:simpleContent/xsd:extension/xsd:attribute', $element);
            }

            if ($attributes->length > 0) {

                foreach ($attributes as $attribute) {
                    $this->getElementStructure($attribute);
                    $attr_name = $this->getElementName($attribute);
                    $attr_value = isset($amzn_attr_values[$base_id]['attr'][$attr_name]) ? $amzn_attr_values[$base_id]['attr'][$attr_name] : null;
                    $html .= $this->getInputFieldHtml($attribute, true, $attr_value, $base_id, $enabled);
                }
            }

            return $html;
        }

        //------------CHECKBOX------------//
        if ($element->getAttribute("type") == "xsd:boolean") {
            /* inputCheckboxHTML.tpl template */
            $checked = $value ? ' checked' : '';
            $this->smarty->assign('checkbox_id', $id);
            $this->smarty->assign('checkbox_cssClass', $cssClass);
            $this->smarty->assign('checkbox_checked', $checked);
            $this->smarty->assign('checkbox_disabled', $disabled);
            $html = $this->smarty->fetch('amazon/inputCheckboxHTML.tpl');
            return $html;
        }
        //------------TEXT------------//
        /* inputTextHTML.tpl template */
        $cssClass = ' class="' . ($isAttribute ? 'attribute ' : '') . $validation_type . ' form-control"';
        $this->smarty->assign('text_id', $id);
        $this->smarty->assign('text_value', $value);
        $this->smarty->assign('text_additional_attributes', $additional_attributes);
        $this->smarty->assign('text_cssClass', $cssClass);
        $this->smarty->assign('text_disabled', $disabled);
        if ($isMain) {
            $this->smarty->assign('isMain', $isMain);
        } else {
            $this->smarty->assign('isMain', false);
        }
        $html = $this->smarty->fetch('amazon/inputTextHTML.tpl');

        //------------ATTRIBUTES (ADDITIONAL FIELDS)------------//
        $attributes = $this->productXPath->query('xsd:complexType/xsd:simpleContent/xsd:restriction/xsd:attribute', $element);
        if ($attributes->length > 0) {
            foreach ($attributes as $attribute) {
                $this->getElementStructure($attribute);
                $attr_name = $this->getElementName($attribute);
                $attr_value = isset($amzn_attr_values[$base_id]) ?
                        (isset($amzn_attr_values[$base_id]['attr'][$attr_name]) ? $amzn_attr_values[$base_id]['attr'][$attr_name] : null) : null;
                $html .= $this->getInputFieldHtml($attribute, true, $attr_value, $base_id, $enabled);
            }
        }

        return $html;
    }

    /**
     * Based on element Restrictions, attributes are obtained to be validated on data input
     * @param type $element
     * @return type
     */
    private function getElementValidationAttributes($element) {
        $additional_attributes = '';

        $maxLength = $this->productXPath->query('xsd:simpleType/xsd:restriction/xsd:maxLength', $element);
        if ($maxLength->length > 0) {
            $additional_attributes .= ($maxLength->length > 0) ? ' amzn_maxLength="' . $this->getElementValue($maxLength->item(0)) . '" ' : "";
        }
        $minLength = $this->productXPath->query('xsd:simpleType/xsd:restriction/xsd:minLength', $element);
        if ($minLength->length > 0) {
            $additional_attributes .= ($minLength->length > 0) ? ' amzn_minLength="' . $this->getElementValue($minLength->item(0)) . '" ' : "";
        }
        $pattern = $this->productXPath->query('xsd:simpleType/xsd:restriction/xsd:pattern', $element);
        if ($pattern->length > 0) {
            $additional_attributes .= ($pattern->length > 0) ? ' amzn_pattern="' . $this->getElementValue($pattern->item(0)) . '" ' : "";
        }

        return $additional_attributes;
    }

    /**
     * Obtains Elements from additional paths, set as exception cases
     * @param type $name
     * @param array $container
     */
    private function getElementFromAdditionalPaths($name, array &$container) {
   
        if (isset(AmazonXSD::$variationDataAdditionalPath[$this->productName])) {
            //Search on additional paths
            $query = AmazonXSD::$variationDataAdditionalPath[$this->productName];
            $element_tmp = $this->productXPath->query($query);
            if ($element_tmp->length > 0) {
                foreach ($element_tmp as $c) {
                    $this->getElementStructure($c);

                    if ($c instanceof DOMElement) {
                        $n = $c->getAttribute("name");
                        if ($n == $name) {
                            $container[$name] = $c;
                            return;
                        } else {
                            $elements = $this->productXPath->query('.//xsd:element[@name="' . $name . '" or @ref="' . $name . '"]', $c);
                            if ($elements->length > 0) {
                                $container[$name] = $elements->item(0);
                            }
                        }
                    } else {
                        $elements = $this->productXPath->query('.//xsd:element[@name="' . $name . '" or @ref="' . $name . '"]', $c);
                        if ($elements->length > 0) {
                            $container[$name] = $elements->item(0);
                        }
                    }
                }
            }
        }
    }

    /**
     * Searches in XSD files related (Base or Product) for the XSD structure of current element
     * @param DOMElement $element
     */
    private function getElementStructure(DOMElement &$element) {
        Amazonxsd::getElementStructure($element, $this->productDom);
    }

    private function getReferencedStructure(DOMNode &$element) {
        AmazonXSD::getReferencedStructure($element, $this->productDom);
    }

    /**
     * Extract element name from DOM Element
     * @param DOMNode $element
     * @return string Name/Ref of the XSD element
     */
    public function getElementName(DOMNode $element) {
        $value = $element->getAttribute("ref");
        if ($value == "") {
            $value = $element->getAttribute("name");
        }
        if ($value == "") {
            $value = $this->getElementValue($element);
        }
        return $value;
    }

    /**
     * Get Value Attribute from an Element
     * @param DOMNode $element
     * @return string Value attribute of the element
     */
    public function getElementValue(DOMNode $element) {
        return $element->getAttribute("value");
    }

    /**
     * Get maxOccurs Attribute from an Element
     * @param DOMNode $element
     * @return string maxOccurs attribute of the element
     */
    public function getElementMaxOccurs(DOMNode $element) {
        return $element->getAttribute("maxOccurs");
    }

    /**
     * Taken from AmazonXSD
     * Get string for being displayed
     * @param type $str
     * @return type
     */
    public static function getName($str, $translate_type = null) {
    
        if (isset(self::$exception_field[$str])) {
            $exeption = self::$exception_field[$str];
        }

        $str = trim(str_replace("_", " ", preg_replace('/(?<!\ )[A-Z]/', ' $0', $str)));
        $key = (isset($exeption) && !empty($exeption)) ? Amazon_Tools::toKey($exeption) : Amazon_Tools::toKey($str);
        $name = Amazon_Tools::toKey($str);

        switch ($translate_type) {
            case AmazonSettings::$universe:
                $str = isset(self::$universe_translate[$key]) ? self::$universe_translate[$key] . " ($str)" : $str;
                break;
            case AmazonSettings::$product_types:
                $str = isset(self::$product_type_translate[$key]) ? self::$product_type_translate[$key] . " ($str)" : $str;
                break;
            case AmazonSettings::$attributes:               
                $str = isset(self::$field_translation[$key]) ? self::$field_translation[$key] : (isset(self::$field_translation[$name]) ? self::$field_translation[$name] : $str);
                break;
            default :
                $str = isset(self::$universe_translate[$key]) ? self::$universe_translate[$key] . " ($str)" :
                        (isset(self::$product_type_translate[$key]) ? self::$product_type_translate[$key] . " ($str)" :
                                (isset(self::$field_translation[$key]) ? self::$field_translation[$key] : $str));
                break;
        }
        return $str;
    }

    /**
     * Verifies if a field is mandatory
     * @param DOMElement $item
     * @return boolean
     */
    public static function isMandatory(DOMElement $item) {
        
        $min = $item->getAttribute("minOccurs");
        if ($min == "")
            return false;

        return intval($min) > 0;
    }

    /**
     * Extract element type from DOM Element, used on HTML validation
     * @param DOMNode $element
     * @return string Name/Ref of the XSD element
     */
    public function getElementType(DOMNode $element) {
        //01: Look for Type attribute
        $value = $element->getAttribute("type");

        if (!$value) {
            //01 Look for xsd:complexType/xsd:simpleContent/xsd:extension
            $extension = $this->productXPath->query(".//xsd:complexType/xsd:simpleContent/xsd:extension", $element);
            foreach ($extension as $ext) {
                $value = $ext->getAttribute("base");
                break;
            }
        }
        if (!$value) {
            //02 Look for xsd:complexType/xsd:simpleContent/xsd:restriction 
            $extension = $this->productXPath->query(".//xsd:complexType/xsd:simpleContent/xsd:restriction", $element);
            foreach ($extension as $ext) {
                $value = $ext->getAttribute("base");
            }
        }

        return str_replace("xsd:", "", $value);
    }

    public function getTooltips($product_type, $name) {

        $models = null;
        $str = trim(str_replace("_", " ", preg_replace('/(?<!\ )[A-Z]/', ' $0', $name)));
        $this->smarty->assign('original_name', $str);
        $this->smarty->assign('translation', false);
        $this->smarty->assign('description', false);
        $this->smarty->assign('tip', false);
        $this->smarty->assign('sample', false);

        if (isset(self::$exception_field[$name])) {
            $exeption = self::$exception_field[$name];
        }
        
        $key = (isset($exeption) && !empty($exeption)) ? Amazon_Tools::toKey($exeption) : Amazon_Tools::toKey($name);
        
        $models_settings = $this->models_settings[$this->ext][$this->productName][$product_type];

        if (isset($models_settings[$key]) || isset($models_settings[Amazon_Tools::toKey($name)])) {
            $models_setting = isset($models_settings[$key]) ? isset($models_settings[$key]) : $models_settings[Amazon_Tools::toKey($name)] ;
            $this->smarty->assign('translation', $models_setting['translation']);
            $this->smarty->assign('description', $models_setting['description']);
            $this->smarty->assign('tip', $models_setting['tip']);
            $this->smarty->assign('sample', $models_setting['sample']);

            if (isset($models_setting['type'])) {
                $models['type'] = $models_setting['type'];
            } 

            if(isset($this->requiredFields[$this->productName]) && in_array($name, $this->requiredFields[$this->productName])) {
                $models['type'] = 1 ;
            }
        }
        
        $models['html'] = htmlspecialchars($this->smarty->fetch('amazon/tooltips.tpl'), ENT_QUOTES);
        return $models;
    }

}