<?php

/**
 * This class obtains XML Structure from XSD and fills it with values stored in the Form
 */
class AmazonXmlProductData {
    
    private $_universe;
    private $_productType;
    private $_dom;
    private $_xPath;
    public static $_universe_tag_mapping = array('ProductClothing'=>'Clothing', 'SWVG'=>'SoftwareVideoGames');   
    private $productTypeExceptions =  array( 'ProductClothing'=> '//xsd:schema/xsd:element[@name="Clothing"]/xsd:complexType/xsd:sequence/xsd:element[@name="VariationData" or @name="ClassificationData"]', ///xsd:complexType/xsd:sequence/xsd:element[@name="ClothingType"]',
                                             'ClothingAccessories'=> '//xsd:schema/xsd:element[@name="ClothingAccessories"]',  //added 03-Nov-2014 Erick T.
                                             'Shoes'=> '//xsd:schema/xsd:element[@name="Shoes"]/xsd:complexType/xsd:sequence/xsd:element[@name="ClothingType" or @name="VariationData" or @name="ClassificationData"]',
                                             'SWVG'=> '//xsd:schema/xsd:element[@name="SoftwareVideoGames"]/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]',
                                             'Luggage'=> '//xsd:schema/xsd:element[@name="Luggage"]',
                                            );
    private $productClassificationDataType = array( 'ProductClothing'=> array('product_type' => 'ClothingType'), 'Shoes'=> array('product_type' => 'ClothingType'), 'Luggage'=> array('product_type' => 'ProductType') );
    private $productTypeFieldExceptions = array(
        'Sports' => 1
    );
    private static $_st_dom;
    private static $_st_xPath;
    private static $_st_baseDom;
    private static $_st_basexPath;
    //private static $_st_initialized = false;
    
    public function __construct($universe, $product_type) {
        $this->_universe = $universe;
        $this->_productType = in_array($this->_universe, array('ProductClothing','Shoes')) ? 'ClassificationData' : $product_type;        
        
        self::initProductData($this->_universe);
        
        $this->_dom = new DOMDocument();
        $this->_dom->formatOutput = true;
        foreach(self::$_st_dom->childNodes as $child)
            if($child instanceof DOMElement)
                $this->_dom->appendChild($this->_dom->importNode($child->cloneNode(true), true));
            
        $this->_xPath = new DOMXPath($this->_dom);
    }
    
    /**
     * Assign all needed values, to be kept in memory
     * @param type $universe
     * @return type
     */
    public static function initProductData($universe){
                
        $_st_dom = new DOMDocument();
        $_st_dom->formatOutput = true;
        $_st_dom->loadXML(AmazonXSD::cache(AmazonXSD::$definitionURL . $universe . '.xsd') ) ;
        $_st_xPath = new DOMXPath($_st_dom);
        
        self::$_st_baseDom = AmazonXSD::getBaseDom();
        self::$_st_basexPath = AmazonXSD::getBaseXPath();
        
        //Replace referenced elements to their corresponding structure
        $hasRef = true;
        
        while($hasRef){
            $query = '//xsd:element[@ref]';
            $references = $_st_xPath->query($query);
            if($references->length == 0){
                $hasRef = false;
            }else{
                //Obtains structure from all referenced elements
                foreach($references as $node){
                    if($node instanceof DOMElement){    
                        $new = self::replaceReferencedElements ($node, $_st_dom);
                        $node->parentNode->replaceChild($new, $node);
                    }
                }
                $hasRef = false;
            }
        }
        
        //Replace type elements, to their corresponding structures (Executed only ONCE)
        $query = '//xsd:element[@type]';
        $types = $_st_xPath->query($query);
        if($types->length > 0){
            //Obtains structure from all type elements (if possible)
            foreach($types as $node)
                if($node instanceof DOMElement)
                    self::replaceTypeElements ($node);
        }
        
        self::$_st_dom = $_st_dom;
        self::$_st_xPath = $_st_xPath;
        
    }
    
    /**
     * Obtains structure of an element, when this reffers another element, by "ref" attribute
     * CLONED from AmazonForm Class.
     * @param DOMElement $element
     * @return boolean
     */
    private static function replaceReferencedElements(DOMElement $element, DOMDocument &$productDom){
        return AmazonXSD::getReferencedStructure($element, $productDom);        
    }
    
    /**
     * Obtains structure of an element, when this refers another element, by "type" attribute
     * CLONED from AmazonForm Class.
     * @param DOMElement $element
     * @return boolean
     */
    private static function replaceTypeElements(DOMElement &$element){
        AmazonXSD::getElementStructure($element, $element->ownerDocument);
    }
    
    /**
     * Gets the XML structure for the selected product type, of curren universe
     * @param array $values array containing values that will be included in generated XML 
     * @return mixed false on any or missing data,  
     */
    public function getProductType($values){
        
        $query = isset($this->productTypeExceptions[$this->_universe]) ? 
                  $this->productTypeExceptions[$this->_universe] : 
                  '//xsd:schema/xsd:element[@name="' . $this->_universe . '"]'.
                  '/xsd:complexType/xsd:sequence/xsd:element[@name="ProductType"]';

        $structure = $this->_xPath->query($query);        
        
        $universe_tag = isset(self::$_universe_tag_mapping[$this->_universe]) ? self::$_universe_tag_mapping[$this->_universe]  : $this->_universe;
        
        $container = $this->_dom->createElement($universe_tag); 
  
        $this->createTypeStructure($structure, $container, $values);          
    
        if($container->getElementsByTagName($this->_productType)->length == 0 && !isset(AmazonXSD::$productTypeDuplicatedExceptions[$this->_universe])){ 
            //die("NO productType set 1");
            return false; 
        }

        if($container->getElementsByTagName($this->_productType)->length > 0){
            $parent = $container->getElementsByTagName($this->_productType)->item(0)->parentNode;

            $product_type_node = false;
            foreach($parent->childNodes as $child){
                if($child->nodeName == $this->_productType)
                    $product_type_node = $child->cloneNode (true);
                    continue;
            }
            if(!$product_type_node){
                return false;
            }
            if(isset(AmazonXSD::$productTypeDuplicatedExceptions[$this->_universe])){
                $container = $container->getElementsByTagName($this->_productType)->item(0)->cloneNode(true);
            } else {
                $new_parent = $parent->ownerDocument->createElement($parent->nodeName);
                $new_parent->appendChild($product_type_node);

                if($parent !== $new_parent){
                    if($parent->parentNode == NULL){
                        // this means the element has no parent node
                        $parent = $new_parent;
                    }else{
                        $parent->parentNode->replaceChild($new_parent, $parent);
                    }
                }
            }
        }        
        
        //Verify if UNIVERSE needs SUBTYPE
        if(isset(AmazonXSD::$productSubtype[$this->_universe])){
            $query2 = AmazonXSD::$productSubtype[$this->_universe];
            $structure2 = $this->_xPath->query($query2);
            $this->createTypeStructure($structure2, $container, $values, true);
        }
        
        //Additional fields per universe
        if(isset(AmazonXSD::$variationDataAdditionalElements[$this->_universe])){ 
            foreach(AmazonXSD::$variationDataAdditionalElements[$this->_universe] as $query4){
                $structure4 = $this->_xPath->query($query4);
                $this->createTypeStructure($structure4, $container, $values, true);
            }
        }
        //Verify if UNIVERSE needs specific additions
        if(isset(AmazonXSD::$specificFieldsAdditions[$this->_universe])){
            $query3 = AmazonXSD::$specificFieldsAdditions[$this->_universe];
            $structure3 = $this->_xPath->query($query3);
            $this->createTypeStructure($structure3, $container, $values, true, true /*prevents double inclusion*/);
        }

        //Verify if UNIVERSE needs recommended additions
        if(isset(AmazonXSD::$recommededFieldsAdditions[$this->_universe])){
            $query6 = AmazonXSD::$recommededFieldsAdditions[$this->_universe];
            $structure6 = $this->_xPath->query($query6);
            $this->createTypeStructure($structure6, $container, $values, true);
        }
        
        //Exception for size-color on CE universe
        //echo '<pre>' . print_r($container->getElementsByTagName($this->_productType), true) . '</pre>';
        if($container->nodeName == 'CE'){
            $var = $container->getElementsByTagName("VariationTheme");
            foreach($var as $v){
                if($v->nodeValue=='Size-Color'){
                    $structure5 = $this->_xPath->query('//xsd:schema/xsd:element[@name="CE"]/xsd:complexType/xsd:sequence/xsd:element[@name="Size"]');
                    $this->createTypeStructure($structure5, $container, $values, true);
                }
                break; //this should only happen once
            }
        }

       // print_r($container->textContent);
        return $container;
        
    }
    
    /**
     * Receives a list of Nodes corresponding to the XSD structure of an element
     * and returns the final XML structure for the corresponding XSD element
     * @param DOMNodeList $structure List of nodes to be considered in XML generation
     * @param DOMElement $root Parent node to which all children will be appended
     * @param array $values Values that will be included in XML elements
     * @return type
     */
    private function createTypeStructure(DOMNodeList &$structure, DOMElement &$root, $values, $preventDuplicate=false){
           
        if($structure->length == 0){            
            return;
        }
        
        if(isset($this->productClassificationDataType[$this->_universe])){
            foreach ($this->productClassificationDataType[$this->_universe] as $ClassificationDataType) {
                $values['recommended_data'][$ClassificationDataType] = isset($values['product_type']) ? $values['product_type'] : $this->_universe;
            }
        }     
        //var_dump($values['recommended_data']); exit;
        foreach($structure as $xsdElement){           
           
            if(! $xsdElement instanceof DOMElement){
                continue;
            }             
            
            //AgeRecommendation was previously added for "Toys", this prevents duplicate additions on first level
            //if dulication is found later, this change may be applied at all level of XML code, Not recommnended unless it is necessary
            if($preventDuplicate) {
                $name = $xsdElement->getAttribute("name");
         
                if($root->getElementsByTagName($name)->length > 0){
                    continue;
                }
            }
            $this->setElementStructure($xsdElement, $root, $values);
        }       
        //die; ////////////////////////////////////////////
    }
    
    /**
     * Obtain $node XSD structure, and converts it to XML structure 
     * @param DOMElement $node
     * @param DOMElement $parent
     * @param array $values
     */
    private function setElementStructure(DOMElement &$node, DOMElement &$parent, array $values=array()){
                
        $name = $node->getAttribute("name");

        $node_values = $values;

        if(isset($node_values['amzn_attr']) ){
            unset($node_values['amzn_attr']);
        }
        
        if($name){
            $val = $this->getFormValue($name, $node_values, true);
           
            if(!is_array($val)){
                $val = array($val);
            }
            //echo "<pre> PARENT : " . print_r($parent->ownerDocument->saveXML($parent), true) . '</pre>';
            foreach($val as $value){
                
                //////// avoid duplicate Color & Size //////
                if($name == 'Color' || $name == 'Size'){
                    $tag = $parent->nodeName;
                    if($tag == 'ClassificationData'){
                        continue;
                    }
                }
                
                //////// re-name attribute field //////
                //if(isset(self::$productTypeException[$this->_universe][$name])){
                //    $name = self::$productTypeException[$this->_universe][$name];
                //}
                
                $new = $parent->ownerDocument->createElement($name);
                foreach($node->childNodes as $child){
                    //echo "<pre> CHILD : " . print_r($child->ownerDocument->saveXML($child), true) . '</pre>';
                    if(!$child instanceof DOMElement){
                        continue;
                    }
                    
                    $this->setElementStructure($child,$new, $values);
                }

                if($value && $value !== true){
                    //$new->nodeValue = htmlspecialchars($value, ENT_NOQUOTES);
                    $new->nodeValue = str_replace(array('&','>','<'), array('&amp;', '&lt;', '&gt;'), $value);
                }

                //------------ATTRIBUTES------------//
                $attributes = $node->getElementsByTagName('attribute'); 
                $children_elements = $node->getElementsByTagName('element');
                $local_amzn_attr_values = isset($values['amzn_attr']) ? $values['amzn_attr'] : array();

                if(isset($local_amzn_attr_values[$name]) && 
                   $attributes->length > 0 && $children_elements->length == 0){

                    foreach($attributes as $attribute){
                        $attr_name = $attribute->getAttribute("name");
                        $attr_value =  (isset($local_amzn_attr_values[$name][$attr_name]) ? $local_amzn_attr_values[$name][$attr_name] : null);
                        if($attr_value){
                            $new->setAttribute($attr_name, $attr_value);
                        }
                    }
                }
                
                if($value || $new->hasChildNodes()){
                    $parent->appendChild($new);
                }
            }

        }elseif(!$name && $node->hasChildNodes()){
            foreach($node->childNodes as $child){
                if(!$child instanceof DOMElement){
                    continue;
                }
                $this->setElementStructure($child,$parent, $values);
            }
        }
        
    }
    
    /**
     * Given an element name and an array containing values in the form: name=>value
     * this function returns the value for current element name (if it exists)
     * @param string $name Name of the searched element
     * @param array $values Array containing values 
     * @param boolean $recursive if true, every child element in the values array is searched
     * @return string|boolean if value is found for corresponding element, value is returned. Else "false" is returned
     */
    private function getFormValue($name, array $values, $recursive=false){
      
        //$name = ($name=="ProductSubType" ? "your_subtype_tag" : $name ); //this is only used in Praew's version
        if($name == "ClothingType") $name = "product_type";
        if(isset($this->productTypeFieldExceptions[$this->_universe]) && $name == "ProductType") $name = "product_type";

        if(isset($values[$name])){
            return $values[$name] ? $values[$name] : false; //Returns true indicating parameter is selected, but has no value
        }
        
        //It's the productType
        //Added Exception for Product CLothing generation       
        if(isset($values["product_type"]) && ($values["product_type"] == $name || $name == 'ClassificationData')){
            return true;
        }
        if($recursive){
            foreach($values as $value){
                $val = false;
                if(is_array($value))
                    $val = $this->getFormValue($name, $value, $recursive);
               
                if($val)
                    return $val;
            }
        }
        return false;
    }
    
}