<?php

class FBA {
    
    const debug = false; 
    
    public function createFbaOrder($user, $id_shop, $site, $id_order, $force_send=false){
	
	// need to fastest use node
	require_once(dirname(__FILE__) . '/../../../../assets/apps/amazon_fba_order.php');
	
	//send to FBA	
	$amazon_order = new FbaOrderTasks( $user );	 
	$amazon_order->Dispatch(FbaOrderTasks::CREATE, $id_shop, $site, $id_order, $force_send);		
    }
    
    public function getFbaOrderInfo($user, $id_shop, $id_order){	
		
	require_once dirname(__FILE__) . '/../functions/amazon.fba.orders.php';
	require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
	require_once(dirname(__FILE__) . '/../classes/amazon.history.log.php');		
	
	// get order by id
	$amazon_order = new Amazon_Order($user, true);
	$order_data = $amazon_order->feedbiz_order->getOrdersByID($user, $id_order);
	$site = isset($order_data['site']) ? $order_data['site'] : null ;
	
	//$orders
	$info = array(
	    'user_name' => $user,
	    'id_shop' => $id_shop,
	    'site' => $site, 
	    'next_time' => 1440,
	    'owner' => 'system',
	    'action' => 'fba_order_cancel',
	);
	
	// 1. find Master Platform of region by  site
	$master_platform = AmazonUserData::getMasterFBA($user, $site, $id_shop);
	
	if((empty($master_platform)) || !$master_platform)
	{
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    // Set history log
	    AmazonHistoryLog::set($info, 'Missing Amazon Master Platform.');
	    return ;	    
	} 
	
	// 2. Reset site id
	$info['site'] = $master_platform['id_country'];
	$info['ext'] = $master_platform['ext'];
	$info['region'] = $master_platform['region'];

	// 3. Get userdata from database
	$data = AmazonUserData::get($info, false, true);
	
	if( (!isset($data) || empty($data)) ){
	    
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    
	    // Set history log
	    AmazonHistoryLog::set($info, 'Access Denided.');
	    return ;

	} else {
	    
	    //$info = $data;

	    $info['shop_name'] = $data['shop_name'];
	    $info['countries'] = $data['countries'];
	    $info['ext'] =  $data['ext'];
	    
	    // 4. check Only if FBA MultiChannel is active
	    /*if (!isset($data['fba_multichannel']) || !(bool)$data['fba_multichannel'])
	    {
		AmazonHistoryLog::set($info, 'fba multichannel is not allow.');
		return ;
	    }*/
	    
	    // 5. check Only if FBA MultiChannel auto is active
	    /*if (!isset($data['fba_multichannel_auto']) || !(bool)$data['fba_multichannel_auto'])
	    {
		AmazonHistoryLog::set($info, 'fba multichannel auto is not allow.');
		return ;
	    }*/   

	    // 6. Set history log
	    AmazonHistoryLog::set($info);

	    $users = json_decode(json_encode($data), false);
	    $cron = false; 
	    
	    $options = array();
	    $options['return_html'] = 1 ;
	    
	    global $result;
	    
	    // 8. fba_update
	    $amazon_stocks = new AmazonFBAOrder($users, $cron, FBA::debug);
	    $result = $amazon_stocks->Dispatch(AmazonFBAOrder::fba_info, $id_order, $options);
	    
	}
    }
    
    public function cancelFbaOrder($user, $id_shop, $id_order){	
		
	require_once dirname(__FILE__) . '/../functions/amazon.fba.orders.php';
	require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
	require_once(dirname(__FILE__) . '/../classes/amazon.history.log.php');		
	
	// get order by id
	$amazon_order = new Amazon_Order($user, true);
	$order_data = $amazon_order->feedbiz_order->getOrdersByID($user, $id_order);
	$site = $order_data['site'];
	
	//$orders
	$info = array(
	    'user_name' => $user,
	    'id_shop' => $id_shop,
	    'site' => $site, 
	    'next_time' => 1440,
	    'owner' => 'system',
	    'action' => 'fba_order_cancel',
	);
	
	// 1. find Master Platform of region by  site
	$master_platform = AmazonUserData::getMasterFBA($user, $site, $id_shop);
	
	if((empty($master_platform)) || !$master_platform)
	{
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    // Set history log
	    AmazonHistoryLog::set($info, 'Missing Amazon Master Platform.');
	    return ;	    
	} 
	
	// 2. Reset site id
	$info['site'] = $master_platform['id_country'];
	$info['ext'] = $master_platform['ext'];
	$info['region'] = $master_platform['region'];

	// 3. Get userdata from database
	$data = AmazonUserData::get($info, false, true);
	
	if( (!isset($data) || empty($data)) ){
	    
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    
	    // Set history log
	    AmazonHistoryLog::set($info, 'Access Denided.');
	    return ;

	} else {

	    // 4. check Only if FBA MultiChannel is active
	    //if (!isset($data['fba_multichannel']) || !(bool)$data['fba_multichannel'])
	    //{
	    //	AmazonHistoryLog::set($info, 'fba multichannel is not allow.');
	    //	return ;
	    //}
	    
	    // 5. check Only if FBA MultiChannel auto is active
	    //if (!isset($data['fba_multichannel_auto']) || !(bool)$data['fba_multichannel_auto'])
	    //{
	    //	AmazonHistoryLog::set($info, 'fba multichannel auto is not allow.');
	    //	    return ;
	    //}    

	    $info['shop_name'] = $data['shop_name'];
	    $info['countries'] = $data['countries'];
	    $info['ext'] =  $data['ext'];

	    // 6. Set history log
	    AmazonHistoryLog::set($info);

	    $users = json_decode(json_encode($data), false);
	    $cron = false; 
	    
	    $options = array();
	    $options['return_html'] = 1 ;
	    
	    global $result;
	    
	    // 8. fba_update
	    $amazon_stocks = new AmazonFBAOrder($users, $cron, FBA::debug);
	    $result = $amazon_stocks->Dispatch(AmazonFBAOrder::fba_cancel, $id_order, $options);

	}
    }
    
    public function updateFbaOrder($user, $id_shop, $site, $orders){	
		
	require_once dirname(__FILE__) . '/../functions/amazon.fba.orders.php';
	require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
	require_once(dirname(__FILE__) . '/../classes/amazon.history.log.php');		
	
	$info = array(
	    'user_name' => $user,
	    'id_shop' => $id_shop,
	    'site' => $site, 
	    'next_time' => 1440,
	    'owner' => 'system',
	    'action' => 'fba_order_update',
	);
	
	if(!isset($orders) || empty($orders))
	{
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    AmazonHistoryLog::set($info, 'Empty Orders.');
	    return ;
	}

	// 1. find Master Platform of region by  site
	$master_platform = AmazonUserData::getMasterFBA($user, $site, $id_shop);
	
	if((empty($master_platform)) || !$master_platform)
	{
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    // Set history log
	    AmazonHistoryLog::set($info, 'Missing Amazon Master Platform.');
	    return ;	    
	} 
	
	// 2. Reset site id
	$info['site'] = $master_platform['id_country'];
	$info['ext'] = $master_platform['ext'];
	$info['region'] = $master_platform['region'];

	// 3. Get userdata from database
	$data = AmazonUserData::get($info, false, true);
	
	if( (!isset($data) || empty($data)) ){
	    
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    
	    // Set history log
	    AmazonHistoryLog::set($info, 'Access Denided.');
	    return ;

	} else {

	    $info['shop_name'] = $data['shop_name'];
	    $info['countries'] = $data['countries'];
	    $info['ext'] =  $data['ext'];

	    // 4. check Only if FBA MultiChannel is active
	    if (!isset($data['fba_multichannel']) || !(bool)$data['fba_multichannel'])
	    {
		AmazonHistoryLog::set($info, 'fba multichannel is not allow.');
		return ;
	    }
	    
	    // 5. check Only if FBA MultiChannel auto is active
	    if (!isset($data['fba_multichannel_auto']) || !(bool)$data['fba_multichannel_auto'])
	    {
		AmazonHistoryLog::set($info, 'fba multichannel auto is not allow.');
		return ;
	    }    

	    // 6. Set history log
	    AmazonHistoryLog::set($info);

	    $users = json_decode(json_encode($data), false);
	    $cron = false; 
	    
	    $orders = array_keys($orders);
	    
	    // 8. fba_update
	    $amazon_stocks = new AmazonFBAOrder($users, $cron, FBA::debug);
	    $amazon_stocks->Dispatch(AmazonFBAOrder::fba_update, $orders);

	}
    }    
    
    
}