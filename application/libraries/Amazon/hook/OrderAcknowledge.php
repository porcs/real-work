<?php

class OrderAcknowledge {
    
    const debug = false;
	    
    public function updateAcknowledgeOrder($user, $id_shop, $site, $orders) {
	
	require_once dirname(__FILE__) . '/../functions/amazon.status.orders.php';
	require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
	require_once(dirname(__FILE__) . '/../classes/amazon.history.log.php');
	
	$info = array(
	    'user_name' => $user,
	    'id_shop' => $id_shop,
	    'site' => $site, 
	    'next_time' => 1440,
	    'owner' => 'system',
	    'action' => 'update_order_acknowledge',
	);	
	
	// 1. check empty orders
	if(!isset($orders) || empty($orders))
	{
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    AmazonHistoryLog::set($info, 'Empty Orders.');
	    return ;
	}	

	// 2. Get userdata from database
	$data = AmazonUserData::get($info, false, true);
	
	if( (!isset($data) || empty($data)) ){
	    
	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];
	    
	    // Set history log
	    AmazonHistoryLog::set($info, 'Access Denided.');
	    return ;

	} else {   
	    
	    $info['shop_name'] = $data['shop_name'];
	    $info['countries'] = $data['countries'];
	    $info['ext'] =  $data['ext'];

	    // 6. Set history log
	    AmazonHistoryLog::set($info);
	    
	    // 3. update order acknowledge 
	    $params = json_decode(json_encode($data), false);
	    $amazonOrders = new AmazonStatusOrders($params, OrderAcknowledge::debug, AmazonStatusOrders::AmazonOrderAcknowledgement);
	    $amazonOrders->updateAcknowledgeOrder($orders);    
	}
	
    }

    public function cancelOrder($user, $id_order, $id_marketplace, $id_shop, $site) { 

	// need to fastest use node
	require_once(dirname(__FILE__) . '/../../../../assets/apps/amazon_order_acknowledge.php');

	//send to FBA
	$amazon_order = new AmazonOrderAcknowledge( $user );
	$amazon_order->Dispatch(AmazonOrderAcknowledge::CANCELLED, $id_order, $id_marketplace, $id_shop, $site);
        
        /*$orders = new Orders($user, $id_order);

        // check is Amazon Order
        if(!isset($orders->id_marketplace) || $orders->id_marketplace <> 2) {
            return ;
        }

        // check is Cancelled order ?
        if(!isset($orders->order_status) || !in_array($orders->order_status, array("Canceled"))){
            return ;
        }
        
	require_once dirname(__FILE__) . '/../functions/amazon.status.orders.php';
	require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
	require_once(dirname(__FILE__) . '/../classes/amazon.history.log.php');

	$info = array(
	    'user_name' => $user,
	    'id_shop' => $orders->id_shop,
	    'site' => $orders->site,
	    'next_time' => 1440,
	    'owner' => 'system',
	    'action' => 'cancel_order_acknowledge',
	);

	// 1. Get userdata from database
	$data = AmazonUserData::get($info, false, true);
        
	if( (!isset($data) || empty($data)) ){

	    $info['ext'] =  $info['site'];
	    $info['countries'] = $info['site'];
	    $info['shop_name'] = $info['user_name'];

	    // Set history log
	    AmazonHistoryLog::set($info, 'Access Denided.');
	    return ;

	} else {

	    $info['shop_name'] = $data['shop_name'];
	    $info['countries'] = $data['countries'];
	    $info['ext'] =  $data['ext'];

	    // Set history log
	    AmazonHistoryLog::set($info);

	    // 2. update order acknowledge
	    $params = json_decode(json_encode($data), false);
	    $amazonOrders = new AmazonStatusOrders($params, OrderAcknowledge::debug, AmazonStatusOrders::AmazonOrderAcknowledgement);
	    $amazonOrders->cancelOrder(json_decode(json_encode($orders), true), $reason);
            
	}*/

    }
}