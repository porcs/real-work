<?php

class ProductManagement {

    public function removeDeltedProducts($user, $id_shop, $debug=false) {
	require_once(dirname(__FILE__) . '/../classes/amazon.database.php');

        $amazon_database = new AmazonDatabase($user, $debug);
        $amazon_database->removeDeletedProductsFromAmazon($id_shop);
    }

}