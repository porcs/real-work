<?php

require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once(dirname(__FILE__) . '/../classes/amazon.userdata.php');
require_once dirname(__FILE__) . '/../classes/amazon.database.php';

class AmazonMessageSummary {
    
    protected $user = null;
    protected $database = null;
    protected $feedbiz = null;

    public function __construct($user, $language = null, $debug = false) {        
        $this->user = $user;
        $this->debug = $debug;
        $this->database = new AmazonDatabase($this->user, $this->debug);  
        
        if(!isset($language)) $language = 'english';
        Amazon_Tools::load(_FEED_LANG_FILE_, $language);
    }
    
    public function getSummaryFeed($date){
              
        $feeds = array();
        $action_process = array('sync','create');
        $logs = $this->database->get_logs($date, $action_process);
        
        if(!empty($logs)){
            $feeds = $logs;
        }
        return $feeds;
    }
    
    public function getSummaryValidate($date){
        
        $action_process = array('sync','create');
        $logs = $this->database->get_validation_logs($date, $action_process);

        $main = array();
        $no_message = 0;

        foreach ($logs as $log){

            if(strpos($log['message'], 'are inactive') !== false){

                if(!isset($main['are_inactive']['number']))
                    $main['are_inactive']['number'] = 0;
                $main['are_inactive']['message'] = 'Product are inactive.';
                $main['are_inactive']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Minimum of quantity required') !== false){
                if(!isset($main['Minimum_of_quantity_required']['number']))
                    $main['Minimum_of_quantity_required']['number'] = 0;
                $main['Minimum_of_quantity_required']['message'] = Amazon_Tools::l('Product quantity required.');
                $main['Minimum_of_quantity_required']['number'] = $log['rows'];
            }
            if(strpos($log['message'], 'Duplicate entry for') !== false){
                if(!isset($main['Duplicate_entry']['number']))
                    $main['Duplicate_entry']['number'] = 0;
                $main['Duplicate_entry']['message'] = Amazon_Tools::l('Duplicate entry SKU.');
                $main['Duplicate_entry']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'are disabled') !== false){
                if(!isset($main['Disabled_product']['number']))
                    $main['Disabled_product']['number'] = 0;
                $main['Disabled_product']['message'] = Amazon_Tools::l('Product are disabled.');
                $main['Disabled_product']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Missing model in') !== false){
                if(!isset($main['Missing_model']['number']))
                    $main['Missing_model']['number'] = 0;
                $main['Missing_model']['message'] = Amazon_Tools::l('Missing model.');
                $main['Missing_model']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Missing profile setting') !== false){
                if(!isset($main['Missing_profile']['number']))
                    $main['Missing_profile']['number'] = 0;
                $main['Missing_profile']['message'] = Amazon_Tools::l('Missing profile setting.');
                $main['Missing_profile']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Has No') !== false){
                if(!isset($main['Has_No_s']['number']))
                    $main['Has_No_s']['number'] = 0;
                $main['Has_No_s']['message'] = Amazon_Tools::l('Product has no reference.');
                $main['Has_No_s']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Missing reference') !== false){
                if(!isset($main['Missing_reference']['number']))
                    $main['Missing_reference']['number'] = 0;
                $main['Missing_reference']['message'] = Amazon_Tools::l('Missing reference.');
                $main['Missing_reference']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'is incorrect EAN/UPC') !== false){
                if(!isset($main['is_incorrect_EAN_UPC']['number']))
                    $main['is_incorrect_EAN_UPC']['number'] = 0;
                $main['is_incorrect_EAN_UPC']['message'] = Amazon_Tools::l('Product is incorrect EAN/UPC.');
                $main['is_incorrect_EAN_UPC']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'Out of stock') !== false){
                if(!isset($main['out_of_stock']['number']))
                    $main['out_of_stock']['number'] = 0;
                $main['out_of_stock']['message'] = Amazon_Tools::l('Product are out of stock.');
                $main['out_of_stock']['number'] = $log['rows'];
            }       
            if(strpos($log['message'], 'length must be greater or equal to: 8') !== false){
                if(!isset($main['s_length_8_16']['number']))
                    $main['s_length_8_16']['number'] = 0;
                $main['s_length_8_16']['message'] = Amazon_Tools::l('Incorrect Standard Product ID length.');
                $main['s_length_8_16']['number'] = $log['rows'];
            }            
            if(strpos($log['message'], 'length must be greater or equal to: 1') !== false){
                if(!isset($main['s_length_1_40']['number']))
                    $main['s_length_1_40']['number'] = 0;
                $main['s_length_1_40']['message'] = Amazon_Tools::l('Incorrect SKU  length.');
                $main['s_length_1_40']['number'] = $log['rows'];
            } 
            if(strpos($log['message'], 'Please ensure that price of at least equal to or greater than 0.01') !== false){
                if(!isset($main['s_length_1_40']['number']))
                    $main['s_length_1_40']['number'] = 0;
                $main['s_length_1_40']['message'] = Amazon_Tools::l('Incorrect price, price less than 0.01.');
                $main['s_length_1_40']['number'] = $log['rows'];
            } 
            if(strpos($log['message'], "The parent SKU don't have child SKU") !== false){
                if(!isset($main['s_length_1_40']['number']))
                    $main['s_length_1_40']['number'] = 0;
                $main['s_length_1_40']['message'] = Amazon_Tools::l("The parent SKU don't have child SKU");
                $main['s_length_1_40']['number'] = $log['rows'];
            }
            
            $no_message = $no_message+$log['rows'];
        }
        
        $mains = $main;
        foreach ($mains as $key => $msg){
            $main[$key]['percentage'] = number_format((($msg['number'] / $no_message) * 100), 2);
        }
        
        return $main;
    }
    
     public function getSummaryInventory($id_country, $id_shop){
	 
	$Inventory = $this->database->getProductsInventory((int) $id_country, (int) $id_shop);
	return count($Inventory);
     }
     public function getSummaryNumErrorLastDay($id_country, $id_shop){

        $param = array(
            'date_from'=>date('Y-m-d',strtotime('yesterday')),
            'action_type'=>'create'
            );
	$log = $this->database->get_log((int) $id_country, (int) $id_shop,true,null,0,$param);
        $count = 0;
        if(!empty($log) && is_array($log)){
            foreach($log as $r){
                if(!empty($r['no_error'])){
                    $count += $r['no_error'];
                }
            }
        }
	return $count;
     }
     
}
