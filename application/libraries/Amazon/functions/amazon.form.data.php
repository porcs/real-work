<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'classes/amazon.config.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'classes/amazon.form.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'classes/amazon.xml.php');
require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'classes/amazon.xml.productData.php');


class AmazonFormData{
    private $product_category = "";
    private $ext = "";
    private $form;
    private $action;
    public function __construct() {
        
        $this->product_category = Amazon_Tools::getValue("product_category");
        $this->ext = Amazon_Tools::getValue("ext");
        
        if($this->product_category){
            $this->form = new AmazonForm($this->product_category, $this->ext);
        } 
        
        $this->action = Amazon_Tools::getValue("action");
    }
    
    public function dispatch(){
        
        if(!$this->product_category){
            die("No product selected");
        }
        
        if(!$this->action){ 
            die("No action selected");
        }
        $output = "";
        $product_type = Amazon_Tools::getValue("product_type", null);  
        $variation_theme = Amazon_Tools::getValue("VariationTheme", null);

        global $params;
        $mapping_specific_fields = $mapping_variation_data = null ;

        if(isset($params) && !empty($params)) {

            $country = isset($params->id_country) ? $params->id_country : null;
            $id_mode = isset($params->id_mode) ? $params->id_mode : null;
            $id_shop = isset($params->id_shop) ? $params->id_shop : null;

            $DisplayForm = new AmazonDisplayForm(null, null, $params);

            $mapping_attribute_recommended = $DisplayForm->get_mapping_group($country, $id_mode, $id_shop, 'recommended_data', false, true, true);
            $mapping_attribute_variation = $DisplayForm->get_mapping_group($country, $id_mode, $id_shop, 'variation_data', false, false, true);
            $mapping_attribute_specific = $DisplayForm->get_mapping_group($country, $id_mode, $id_shop, 'specific_fields', false, true, true);
            $variation = array_merge($mapping_attribute_recommended, $mapping_attribute_variation);

            if (isset($mapping_attribute_specific['specific_fields'])) {
                $mapping_specific_fields = $mapping_attribute_specific['specific_fields'];
            }
            if (isset($variation) && !empty($variation)){
                $mapping_variation_data = $variation;
            }
        }

        switch($this->action){
            case "product_types":
                $product_types = $this->form->getProductTypes();
                $output = json_encode(array(
                                            "product_types"=>$product_types,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )     
                                     );    
                break;
            case "product_subtype":
                $product_subtype = $this->form->getProductSubtype($product_type);
                $output = json_encode(array(
                                            "product_subtype"=>$product_subtype,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )     
                                     );
                break;
            case "mfr_part_number":
                $mfr_part_number = $this->form->getMfrPartNumber($product_type);
                $output = json_encode(array(
                                            "mfr_part_number"=>$mfr_part_number,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )     
                                     );
                break;
            case "variation_theme":
                $variation_theme = $this->form->getVariationTheme($product_type);
                $output = json_encode(array(
                                            "variation_theme"=>$variation_theme,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )     
                                     );
                break;
            case "variation_data":

                if(isset($mapping_variation_data) && !empty($mapping_variation_data)){
                    $selected = $mapping_variation_data;
                } else {
                    $selected = Amazon_Tools::getValue("selected", null);

                    if(isset($selected) && !empty($selected) && $selected != null) {
                        $selected = unserialize(base64_decode(str_replace(array('-', '_'), array('+', '/'), $selected)));
                    }
                }
                $data = $this->form->getVariationData($product_type, $variation_theme, $selected);
                
                $output = json_encode(array("variation_data"=>utf8_encode(utf8_decode($data["variation"])),
                                            "recommended_data"=> utf8_encode(utf8_decode($data["recommended"])),
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           ));
                break;
            case "specific_options":
                $variation_data = $this->form->getSpecificOptions($product_type, array(),$variation_theme);
                $output = json_encode(array("specific_options"=>utf8_encode(utf8_decode($variation_data)),
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )
                                     );
                break;
            case "specific_fields":
                $specific_html = "";
                $output = json_encode(array(
                                            "specific_fields"=>$specific_html,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )
                                     );
                break;
            case "add_specific_field": 
                
                $item = Amazon_Tools::getValue("item");
                $multiple = Amazon_Tools::getValue("multiple");
                
                if(isset($mapping_specific_fields) && !empty($mapping_specific_fields)){
                    $values = $mapping_specific_fields;
                } else {
                    $selected = Amazon_Tools::getValue("selected", null);

                    if(isset($selected) && !empty($selected) && $selected != null) {
                        $values = unserialize(base64_decode(str_replace(array('-', '_'), array('+', '/'), $selected)));
                    }
                }

                $specific_field = "";
                if(!$item)
                    break;
                
                $sf = $this->form->getSpecificFields($product_type, $item, $values, $multiple);
                
                foreach($sf as $option){ 
                    $specific_field.= $option;
                } 
                $output = json_encode(array("specific_fields"=>$specific_field,
                                            "memory_peak"=>  round(memory_get_peak_usage(true)/1024/1024,2)
                                           )
                                     );
                break;
            case "save_data":
                $filename = _AMAZON_DATA_SRC_;
                
                
                if (is_writable($filename)){
                    
                    $form_data = $_POST;
                
                    if(isset($form_data["action"]))
                        unset($form_data["action"]);
                    
                    $fh = fopen($filename, 'w');
                    fwrite($fh, base64_encode(serialize($form_data)));
                    fclose($fh);
                    echo "Data Saved!";
                }else{
                    die("you don't have write permission");
                }
                
                break;
                
            default:
                break;
        }
        
        echo $output;
    }
    
}

$formData = new AmazonFormData();
$formData->dispatch();
