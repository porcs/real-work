
<?php

require_once(dirname(__FILE__) . '/../classes/amazon.config.php');
require_once(dirname(__FILE__) . '/../classes/amazon.order.php');
require_once(dirname(__FILE__) . '/../classes/amazon.webservice.php');
require_once dirname(__FILE__) . '/../classes/amazon.xml.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.productData.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.validator.php';
require_once dirname(__FILE__) . '/../classes/amazon.remote.cart.php';
require_once(dirname(__FILE__) . '/../../FeedBiz/config/stock.php');
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class AmazonImportOrders 
{
    const AFN = 'AFN';
    const ACTION_PROCESS = 'Orders';    
    public $FBA = false;

    public function __construct($params, $debug = false, $cron = false)
    { 
        $this->params = $params;    
        
        if(isset($this->params->language))
            Amazon_Tools::load(_ORDER_LANG_FILE_, $this->params->language);
        
        $this->debug = $debug;

        if($this->debug){
            echo '<br/><b>Amazon'.$this->params->ext.' Started Order Query : </b><pre> '  . print_r($this->params, true) . '</pre>';
        }

        $this->cron = $cron;
        $this->option = $this->output = null; 
        $this->batchID = uniqid();
        $this->time = date('Y-m-d H:i:s');
        $this->params->time = $this->time;      
        $this->title = sprintf(Amazon_Tools::l("Amazon%s Started Order Query"), $this->params->ext) . ' '. Amazon_Tools::l('on') ;
        $this->operation = 'get_order' ;  
        $this->_amazonOrder = new Amazon_Order($this->params->user_name, false, $this->debug);
        
        if($this->debug == false) 
            $this->StartProcessMessage();
        
        $this->SetMessage(Amazon_Tools::l('Start Get Order List') . '...'); 
        if(isset($this->params->option))
        {
            $option = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;", urldecode($this->params->option)),null,'UTF-8');
            $this->option = json_decode($option);

            if($this->debug){
                echo '<br/><b>Query Options: </b><pre> '  . print_r($this->option, true) . '</pre>';
            }
        }

        $this->_amazonApi = $this->AmazonApi();

        /*if($this->params->user_name == 'u00000000000026'){
            $this->debug_url = '/var/www/backend/assets/apps/users/u00000000000026/debugs/amazon_'.$this->operation.'_'.$this->batchID.'('.$this->params->ext.').txt';
            file_put_contents($this->debug_url,
                print_r(array(
                    'date'              => date('Y-m-d H:i:s'),
                    'ext'               => $this->params->ext,
                    'id_country'        => isset($this->params->id_country) ? $this->params->id_country : '',
                    'debug'             => $this->debug,
                    'cron'              => $this->cron,
                    'option'            => $this->option,
                    'operation_type'    => $this->operation,
                ), true), FILE_APPEND);
        }*/
    }    
    
    public function Dispatch()
    {        
        $message  = array();
        $message['type'] = $this->title; 
        $message['date'] = date('Y-m-d', strtotime($this->time)); 
        $message['time'] = date('H:i:s', strtotime($this->time));         
        $message['country'] = $this->params->id_country;
        $message['all_message'] = Amazon_Tools::l('See all messages');
        $message['path_to_message'] = $this->batchID;
        
        $features = $this->_amazonOrder->get_configuration_features($this->params->id_country, $this->params->id_shop);
	
	if($this->debug){
	    echo 'Features : <pre>' . print_r($features, true) . '</pre>';
	}
	
        if((!isset($features['import_orders']) || empty($features['import_orders']) || $features['import_orders'] == 0)){
            $this->error = Amazon_Tools::l('inactivate_import_orders');
            if(isset($this->proc_rep)) {
                $message['error'] = $this->error; 
                $this->proc_rep->set_status_msg($message);
            }
            Amazon_Order::$errors[] = $this->error; 
            $this->ValidationError(Amazon_Order::$errors, 'error');
            $this->StopFeed(true);
        }        
	
	// Fulfillment by Amazon (FBA)
        $this->FBA = isset($features['fba']) ? (bool) $features['fba'] : false;
	
	if($this->debug){
	    echo 'Is FBA : ' .($this->FBA). '/n';
	}

        //1. Get Oeder List
        $this->SetMessage(sprintf(Amazon_Tools::l("Getting %s Order List"), strval($this->option->status)) . '...' );

	$data = $this->GetOrderList();

        if ( isset($data->Error) )
        {
            $error_message = array(
                'error_type' => $data->Error->Type,
                'error_code' => $data->Error->Code,
                'error_message' => $data->Error->Message,
                'request_id' => $data->RequestID
            );
            if(isset($this->proc_rep))
            {
                $message['error'] = '(#' .$error_message['error_code']. ') '. $error_message['error_message'];
                $this->proc_rep->set_status_msg($message);
            }
            $this->error = Amazon_Tools::l('Amazon Get Orders Error') . ': (#' .$error_message['error_code']. ') '. $error_message['error_message'];
            Amazon_Order::$errors[] = $this->error;
            $this->ValidationError(Amazon_Order::$errors, 'error');           
            $this->StopFeed();
        }

        if(!isset($data->Orders->Order) || empty($data->Orders->Order))
        {
            if(isset($this->proc_rep))
            {
                $date1 = date('Y-m-d', strtotime($this->option->dateto));
                $date2 = date('Y-m-d', strtotime($this->option->datefrom));
                $this->output = sprintf(Amazon_Tools::l('No pending order for %s to %s'), $date2, $date1);
                $message['warning'] = $this->output;
                $this->proc_rep->set_status_msg($message);
            }
            $this->StopFeed(true);
        }

        //2. List Order Item and Save
	$this->GetOrderItems($data);

        //3. Check Next Token
        if ( isset($data->NextToken) && !is_array($data->NextToken) )
        {
            $i = 0;
            $nextToken = $data->NextToken;
            while ( $nextToken )
            {
                if($this->debug == true)
		    echo '<pre> ' .($i++). ' :' .print_r(strval($nextToken), true) . '</pre>';

                $nextToken = $this->GetOrdersByNextToken(strval($nextToken)) ;
            }
        }

	if($this->debug){
	    echo 'Amazon_Order::$orders : <pre> ' . print_r(Amazon_Order::$orders, true). '</pre>';
	}

	// Manage stock
	$this->ordersStockMovement();

	// Cleanup
	$this->ordersCheckExpiredCarts();

        // Error
        if(isset(Amazon_Order::$errors) && !empty(Amazon_Order::$errors))
            $this->ValidationError(Amazon_Order::$errors, 'error');
        
        // Warnings cancelled
        if(isset(Amazon_Order::$warnings_cancelled) && !empty(Amazon_Order::$warnings_cancelled))
            $this->ValidationError(Amazon_Order::$warnings_cancelled, 'warnings_cancelled');

        // Warning
        if(isset(Amazon_Order::$warnings) && !empty(Amazon_Order::$warnings))
            $this->ValidationError(Amazon_Order::$warnings, 'warning');
        
        // Message
        if(isset(Amazon_Order::$messages) && !empty(Amazon_Order::$messages))
            $this->ValidationError(Amazon_Order::$messages, 'message');
        
        if(isset($this->proc_rep))
        {
            if(!empty(Amazon_Order::$errors)) 
            {
                $message['error'] = array_slice(Amazon_Order::$errors, 0, 3);
                $this->error = $message['error'];  
            }
            
            if(!empty(Amazon_Order::$warnings_cancelled))
            {
                $message['warning'] = array_slice(Amazon_Order::$warnings_cancelled, 0, 3);
                $this->output = $message['warning'];
            }

            if(!empty(Amazon_Order::$warnings))
            {
                $message['warning'] = array_slice(Amazon_Order::$warnings, 0, 3);
                $this->output = $message['warning'];
            }
            
            if(!empty(Amazon_Order::$messages)) 
            {
                $message['message'] = array_slice(Amazon_Order::$warnings, 0, 3);
                $this->output = $message['message'];
            }
            
            $message[AmazonImportOrders::ACTION_PROCESS]['message'] = Amazon_Tools::l('Success');
            $message[AmazonImportOrders::ACTION_PROCESS]['status'] = Amazon_Tools::l('Success');
            
            $this->proc_rep->set_status_msg($message);
        }        
	
        $this->StopFeed();
    }
    
    public function CheckStatus()
    {
        $result = $this->_amazonApi->ServiceStatus(AmazonImportOrders::ACTION_PROCESS, true);

        if (isset($result->GetServiceStatusResult->Status))
        {
            $status = strval($result->GetServiceStatusResult->Status);
            switch ($status)
            {
                case 'GREEN' :
                case 'GREEN_I' :
                    return (true);
            }
        }
        return (false);
    }
    
    public function GetOrderList()
    {
	if($this->debug){
	    echo '<pre>Options #1 : ' . print_r($this->option, true) . '</pre>';
	}
        
        $status = strval($this->option->status);
        $date1 = date('Y-m-d', strtotime($this->option->datefrom));
        $date2 = date('Y-m-d', strtotime($this->option->dateto));

	if ($this->FBA && $this->cron) { // only cron run
	    //$date1 = date('c', strtotime($date1.' -2 days'));
	}
	
	if($this->debug){
	    echo '<pre>After FBA : ' . print_r(array('status' => $status, 'datefrom' => $date1, 'dateto' => $date2), true) . '</pre>';
	}

        $region = null ;
        if(isset($this->params->id_region)){
            $region = $this->params->id_region;
        }

	$date = Amazon_Tools::query_date($date1, $date2, $region);

	if($this->debug){
	    echo '<pre>Date from Amazon_Tools : ' . print_r($date, true) . '</pre>';
	}

	if($date){
	    $date1 = $date['date1'];
	    $date2 = $date['date2'];
	}
        
        if (isset($this->option->recheck) && $this->option->recheck == 'recheck') {
            $status = 'Shipped';
            $this->FBA = self::AFN;
            $date1 = date('c', strtotime($date1.' -14 days'));
            $date2 = date('c', strtotime($date2.' -2 days'));

        } elseif (isset($this->option->recheck) && $this->option->recheck == 'doublecheck') {

            $status = 'Unshipped';
            $date1 = date('c', strtotime($date1.' -6 days'));
            $date2 = date('c', strtotime($date2.' -1 days'));

        }

	if($this->debug){
	    echo '<pre>Options #2 (before send) : ' . print_r(array('status' => $status, 'datefrom' => $date1, 'dateto' => $date2), true) . '</pre>';
	}
	
        $orders = $this->_amazonApi->GetUnshippedOrdersList($date1, $date2, $status, $this->FBA);

        /*if($this->params->user_name == 'u00000000000026'){
            file_put_contents($this->debug_url,
                print_r(array(
                    'GetOrderList' => array(
                        'status'    => $status,
                        'datefrom'  => $date1,
                        'dateto'    => $date2,
                        'FBA'    => $this->FBA,
                        'orders'    => $orders,
                    ),
                ), true), FILE_APPEND);
        }*/
        
        if (isset($orders) && !empty($orders)) 
	    return $orders;        
	
        return false;
    }
    
    public function GetOrderItems($data)
    {
        $Orders = $order_list = $canceled_orders = array();
        $no_process = $no_success = 0;
        
        foreach($data->Orders->Order as $key => $order)
        {
            // Check Imported Order
            $id_order       = $this->_amazonOrder->checkByMpId((string)$order->AmazonOrderId, $this->params->id_shop, $this->params->id_country, true);

            if($this->debug){
                echo '<br/><b>checkByMpId : </b><pre> '  . print_r($id_order, true) . '</pre>';
            }
            
	    //id_orders, status
            if (isset($id_order['id_orders']) && !empty($id_order['id_orders'])) {

                // if canceled order
                if(in_array((string)$order->OrderStatus , array('Canceled', 'Cancelled', 'canceled', 'cancelled'))) {

                    // update status to Cancelled MpMultichannelOrderStatus
                    $mp_status = constant('Amazon_Order::'.strtoupper((string)$order->OrderStatus));                    
                    $this->_amazonOrder->updateMpMultichannelOrderStatus($this->params->id_shop, $this->params->id_country, $id_order['id_orders'], $mp_status);
                    $canceled_orders[$id_order['id_orders']] = $id_order['id_orders'];
                    
                } else {

                    if(!isset($id_order['status']) || !($id_order['status']) ) {
                        $order_items    = $this->_amazonApi->GetOrderItems((string)$order->AmazonOrderId);
                        $Orders[$key] = new PlacedOrder($order, $order_items, $this->debug);
                        $order_list = $this->_amazonOrder->patchOrder($id_order['id_orders'], $Orders[$key], $this->params);
                    }
                }

            } else {

                if(!isset($id_order['status']) || !($id_order['status']) ) {
                    $no_process++;
                    $order_items    = $this->_amazonApi->GetOrderItems((string)$order->AmazonOrderId);
                    $Orders[$key] = new PlacedOrder($order, $order_items, $this->debug);

                    if($this->debug == true) {
                        echo '<pre>'. $key .print_r($Orders[$key], true) . '</pre>';
                    }

                    $order_list = $this->_amazonOrder->setOrder($Orders[$key], $this->params);
                    if(!isset($order_list)){                        
                        continue;
                    }
                    $no_success++;
                }
            }
        }

        // Update canceled orders
        if(!empty($canceled_orders)) {            
            $this->updateOrderCanceled($canceled_orders);
        }
        
        $this->no_process = isset($this->no_process) ? ($this->no_process + $no_process) : $no_process;
        $this->no_success = isset($this->no_success) ? ($this->no_success + $no_success) : $no_success;
    }

    public function GetOrdersByNextToken($token)
    {
	$order_list = array();
	
        if(!isset($token) || empty($token)) 
            return(false);
        
        $data = $this->_amazonApi->GetOrdersByNextToken($token);
        
        if ( isset($data->Error) ) 
            return(false);  
        
        if ( !isset($data->Orders->Order) || empty($data->Orders->Order)) 
            return(false);
        
        if ( isset($data->Orders) ) 
            $order_list = $this->GetOrderItems($data); 
        
        if ( isset($data->NextToken) )
            return ($data->NextToken);
        
        return false;
    } 
        
    private function StartProcessMessage()
    {
        $process_title = sprintf('%s, %s ...', 'Amazon', Amazon_Tools::l('Order Query Processing')); 
        $process_type = $this->operation . '_' .  $this->params->countries;
        $marketplace = strtolower(Amazon_Order::Amazon);
        $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, true );
        $this->proc_rep->set_process_type($process_type); 
        $this->proc_rep->set_popup_display(true);
        $this->proc_rep->set_marketplace($marketplace);
        
        if($this->proc_rep->has_other_running($process_type))
        {
            $this->StopFeed(true);
        }
    }
    
    private function StopFeed($exit=false)
    {   
        $this->log();
        
        $error = null;
        
        if(isset($this->error) && !empty($this->error))
            $error = $this->error;
	
        $json = json_encode( array('status' => isset($error) ? 'error' : 'success' , 'message' => $error ? $error : $this->output)) ;
        echo $json ;

        if(isset($this->proc_rep))
            $this->proc_rep->finish_task();
        if($exit)
            exit;
    }
    
    private function AmazonApi()
    {
        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
		
        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
                $auth['auth_token'] = trim($this->params->auth_token);
        }
		
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
	
        if (!$this->amazonApi = new Amazon_WebService($auth, $marketPlace, null, $this->debug))
        {
            $this->error = Amazon_Tools::l('Unable to connect Amazon');
            
            if(isset($this->proc_rep))
                $this->proc_rep->set_error_msg('Error : ' . $this->error);
            
            $this->StopFeed();
        }
        return $this->amazonApi;
    }
    
    private function SetMessage($msg)
    {
        if(isset($this->proc_rep))
        {
            $message = array(
                'type' => $this->title, 
                'date' => date('Y-m-d', strtotime($this->time)), 
                'time' => date('H:i:s', strtotime($this->time)),                    
                AmazonImportOrders::ACTION_PROCESS => array(
                    'message'=> (string)$msg
                    )
                ); 
            $this->proc_rep->set_status_msg($message);
        } 
    }    
    
    public function ValidationError($pxml, $type)
    {
        if($this->debug){
            echo '</br></br><b> Start Validation Error</b></br>';
        }

        $mail_error_message = array();
        $i=0;
        foreach ($pxml as $message) {
            $error_data = array(
                'batch_id' => $this->batchID,
                'id_country' => $this->params->id_country,
                'id_shop' => $this->params->id_shop,
                'action_process' => AmazonImportOrders::ACTION_PROCESS,
                'action_type' => ($type == "warnings_cancelled") ? 'warning' : $type,
                'message' => strval($message),
                'date_add' => date("Y-m-d H:i:s"),
            );            
            $log = $this->_amazonOrder->get_validation_log($message);
            if($this->_amazonOrder->save_validation_log($error_data, $this->params->id_shop, $this->params->id_country)){
                // check this order is save log already ?
                if(!$log || empty($log) || strpos($message, "#InvalidParameterValue")){
                    $mail_error_message[$i] = $error_data;
                    $mail_error_message[$i]['marketplace'] = _MARKETPLACE_NAME_;
                    $i++;
                } else {
                   if($this->debug){
                        echo '</br> - sent - ' . $message;
                    }
                }
            }
        }

        // mail notifications
        if(in_array($type, array('error', 'warning')) && !empty($mail_error_message)){
            $this->SendEmailResultValue($mail_error_message);
        }
    }

    private function SendEmailResultValue($mail_error_message) {

        require_once(dirname(__FILE__).'/../../Smarty/Smarty.class.php');
        require_once(dirname(__FILE__).'/../../Smarty.php');
        
        $smarty = new Smarty();
        $smarty->compile_dir = dirname(__FILE__)."/../../../views/templates_c";
        $smarty->template_dir = dirname(__FILE__)."/../../../views/templates";

        $base_url = _BASE_URL_;
        $style_img = 'border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;';
        $site_title = _SITE_TITLE_;
        $user_first_name = $this->params->user->firstname;
        $user_las_name = $this->params->user->lastname;
        $email_from = _ADMIN_EMAIL_;
        $marketplace_name = _MARKETPLACE_NAME_;
        $email_to = $this->params->user->email;
        $title = Amazon_Tools::l('Here is the error list');

        $data = array(
            'base_url' => $base_url,
            'logo' => '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="'.$site_title.'" style="'.$style_img.'" />',
            'title' => Amazon_Tools::l('Dear').' '.$user_first_name.' '.$user_las_name,
            'message' => $title.' : ',
            'Error_Detail' => Amazon_Tools::l('Error Detail'),
            'Error_Date' => Amazon_Tools::l('Error Date'),
            'message_detail' => $mail_error_message,
            'thanks' => Amazon_Tools::l('Thanks'),
            'footer1' => Amazon_Tools::l('This e-mail was sent to'),
            'footer2' => Amazon_Tools::l('Please note that you must use an email to access to Feed.biz management system.'),
            'footer3' => Amazon_Tools::l('All right reserved.'),
            'marketplace' => Amazon_Tools::l('Marketplace'),
        );

        $smarty->assign($data);
        $html = $smarty->fetch(dirname(__FILE__).'/../../../views/templates/email_template/amazon_import_order_error.tpl');

        if (strlen($html) > 50) {
            
            $message = $html;
            $subject = $site_title . ' : ' . ucwords($marketplace_name) . ' ' . Amazon_Tools::l('Import Order Error'); //Feed.biz : Amazon Import Order Error

            if($this->debug){
                //Amazon_Tools::send_mail($subject, $email_from, $site_title, $email_to, $message, 'text/html', 'praew@common-services.com');

                echo '<b>Send Mail Error</b></br>';
                echo 'Params : <pre>' . print_r(array('subject' => $subject, 'email_from' => $email_from, 'site_title' => $site_title, 'email_to' => $email_to), true) . '</pre>';
                echo 'Message : <pre>' . print_r($message, true) . '</pre>';
            } else {
                Amazon_Tools::send_mail($subject, $email_from, $site_title, $email_to, $message, 'text/html', 'praew@common-services.com');
            }
        }
    }
    
    private function log(){

        if($this->debug){
            echo '</br></br><b> Start Log</b></br>';
        }
        
        $date1 = isset($this->option->datefrom) ? date('Y-m-d', strtotime($this->option->datefrom)) : date('Y-m-d');
        $date2 = isset($this->option->dateto) ? date('Y-m-d', strtotime($this->option->dateto)) : date('Y-m-d');  
        $status = isset($this->option->status) ? $this->option->status : 'Unshipped';  
        
        $datail = array(
            'order_status' => $status,
            'order_range' => $date1 . ' - ' . $date2
        );
        
        $this->no_error = isset(Amazon_Order::$errors) ? count(Amazon_Order::$errors) : 0;
        $this->no_warning = isset(Amazon_Order::$warnings) ? count(Amazon_Order::$warnings) : 0;
        
        // Log 
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => 'Import Orders',
            "no_process"    => isset($this->no_process) ? $this->no_process : 0,
            "no_success"    => isset($this->no_success) ? $this->no_success : 0,
            "no_error"      => $this->no_error,
            "no_warning"    => $this->no_warning,                                 
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
        if(isset($datail) && !empty($datail)){
             $log['detail'] = base64_encode(serialize($datail));
        }       

        $this->_amazonOrder->update_log($log);
    }
    
    private function GetFeedbizConfigInfo(){
        $config_data = AmazonUserData::getShopInfo($this->params->id_customer);
        return $config_data;
    }
    
    private function updateOrderCanceled($orders){

        $info = $this->GetFeedbizConfigInfo();
	$allow_send_order_cacelled = isset($this->params->allow_send_order_cacelled) ? (bool) $this->params->allow_send_order_cacelled : false;

        if($this->debug){
            echo '<br/><b>Order Canceled : </b><pre> '  . print_r($orders, true) . '</pre>';
            echo '<br/><b>allow_send_order_cacelled : </b><pre> '  . print_r((bool)$allow_send_order_cacelled, true) . '</pre>';
        }

        // update mp_channel when status change
        return $this->_amazonOrder->feedbiz_order->updateOrderCanceled(
            $this->params->user_name,
            $this->params->user_code,
            $info['order_cancel'],
            $orders,
            $this->batchID,
            false,
            $this->debug,
            $allow_send_order_cacelled);
    }
    
    public function updateStockMovement($order_list, $action_type=null){
	
	// get stock movement url	    
	$info = $this->GetFeedbizConfigInfo();    
	$url = $info['url_stockmovement'];
	$user_code = $this->params->user_code;
	
	if($this->debug) {

	    echo '<pre> url = '  . print_r($url, true) . '</pre>';
	    echo '<pre> user_code = '  . print_r($user_code, true) . '</pre>';
	    echo '<pre> update StockMovement = '  . print_r($order_list, true) . '</pre>';

	} else {

	    $return = $this->_amazonOrder->feedbiz_order->stockMovement(
		    $this->params->user_name,
		    $url,
		    $user_code,
		    $this->params->id_shop,
		    $this->batchID,
		    Amazon_Order::$MarketplaceID,
		    $this->params->id_country,
		    $order_list,
		    strtolower(Amazon_Order::Amazon),
		    true,
                    $action_type                    
	    );

            /*if($this->params->user_name == 'u00000000000026'){
                file_put_contents($this->debug_url,
                    print_r(array(
                        'updateStockMovement' => array(
                            'action_type'    => $action_type,
                            'update_StockMovement'    => $order_list,
                            'return'    => $return,
                        ),
                    ), true), FILE_APPEND);
            }*/
	}

        // remove from Cart when send update stock fail : to avoid to send remove cart when we cannot connect to Shop // Use only for $action_type = add_cart
        if(isset($return['fail']['add_cart'])){
            $amazon_cart = new AmazonRemoteCart($this->params->user_name, $this->debug);
            foreach ($return['fail']['add_cart'] as $amazon_order_id => $items) {
                foreach ($items as $reference => $item) {
                    if ($amazon_cart->inCart($this->params->id_shop, $this->params->id_country, $amazon_order_id, $item['reference'])) {
                        $amazon_cart->removeFromCart($this->params->id_shop, $this->params->id_country, $amazon_order_id, $item['reference']);
                    }
                }
            }
        }

        // added to Cart when send update stock fail : to avoid to send remove cart when we cannot connect to Shop // Use only for $action_type = add_cart
        /*if(isset($return['fail']['remove_cart'])){
            $amazon_cart = new AmazonRemoteCart($this->params->user_name, $this->debug);
            foreach ($return['fail']['remove_cart'] as $amazon_order_id => $items) {
                if (!$amazon_cart->inCart($this->params->id_shop, $this->params->id_country, $amazon_order_id, $items['reference'])) {
                    $amazon_cart->addCart($this->params->id_shop, $this->params->id_country, $amazon_order_id, $items['reference'], $items['quantity']);
                }
            }
        }*/
        
        return true;
    }
    
    public function ordersStockMovement(){
        
	if(isset(Amazon_Order::$orders)) {
            
	    $amazon_fba = AmazonUserData::getFBA($this->params->id_customer, $this->params->ext, $this->params->id_shop);
	    $Stock = new StockMovement($this->params->user_name);
	    $amazon_cart = new AmazonRemoteCart($this->params->user_name, $this->debug);
	    	    
	    $id_shop = $this->params->id_shop;
	    $id_country = $this->params->id_country;
	    
	    $updateStockMovement = array();

	    foreach (Amazon_Order::$orders as $amazon_order_id => $orders) {		
		
		$order_id = isset($orders['MerchantOrderID']) ? $orders['MerchantOrderID'] : $amazon_order_id;
		
		if($this->debug) {
		    echo ' <br/> --------------------------------------------------------------- <br/>';
		    echo ' <br/> <b>Order</b> '. $order_id . ' : <b>Amazon</b> ('.$amazon_order_id.') <br/>';
		}
		
		if(isset($orders['items'])) {	
		    
		    $list_sku = array();
		    foreach ($orders['items'] as $key => $items) {
			$list_sku[] = $items['reference'];
		    }
		    
		    foreach ($orders['items'] as $key => $items) {
			
			if($items['id_product'] == 0)
			    continue;
			
			if($this->debug) {
			    echo '<pre> Order Status : ' . (isset($orders['OrderStatus']) ? $orders['OrderStatus'] : '') .'</pre>';
                        }

			// IF FBA do not decrease stock, so we should add stock log to aviod stock movement process // only FBA product
			if($this->FBA && isset($orders['FulfillmentChannel']) && trim($orders['FulfillmentChannel']) == "AFN")
			{
                            if(isset($orders['OrderStatus']) && $orders['OrderStatus'] == Amazon_Order::ORDER_PENDING){
                                if($this->debug) {
                                    // save to log is useless when order pending
                                    echo '<pre><b>Skipped adding stock to log on FBA Order Pending</b></pre>';
                                }
                                continue;
                            }

                            if(!isset($amazon_fba['fba_decrease_stock']) || $amazon_fba['fba_decrease_stock'] == 0) {
                                if($this->debug) {
                                    echo '<pre><b> add stock to log to aviod stock movement process when AFN </b></pre>';
                                } else {
                                    $Stock->stockMovementProcess($items, $id_shop, $items['time'], StockMovement::import_order, 'FBA', $order_id);
                                }
                            }              
                            
			} else {			   
			    
			    // Handle Remote Cart - reserve product for pending orders on Amazon			   
			    if (isset($orders['OrderStatus']) && $orders['OrderStatus'] == Amazon_Order::ORDER_PENDING)
			    {
				if (!$amazon_cart->inCart($id_shop, $id_country, $amazon_order_id, $items['reference']))
				{
				    //$timestamp = strtotime($orders['LastUpdateDate']);
				    $timestamp = strtotime("now");
				    $amazon_cart->addCart($id_shop, $id_country, $amazon_order_id, $items['reference'], $items['quantity'], $timestamp);
				    
				    // Decrease stock // $productQuantity - $quantity
                                    $orders['action_type'] = 'add_cart';
				    $updateStockMovement[$amazon_order_id] = $orders;
				} 
				
				$this->no_process--;
				$this->no_success--;
				
				continue; // Important, we do not proceed the order as it is in pending state.
			    } else {
				if ($amazon_cart->inCart($id_shop, $id_country, $amazon_order_id, $items['reference']))
				{
				    $amazon_cart->removeFromCart($id_shop, $id_country, $amazon_order_id, $items['reference']);
				    // Restore stock // $productQuantity + $quantity
                                    $orders['action_type'] = 'remove_cart';
				    $orders['items'][$key]['quantity'] = ( (int)$items['quantity'] ) * (-1);
				    $updateStockMovement[$amazon_order_id] = $orders;
				} 
			    }		
			}
		    }
		}
	    }

	    if(!empty($updateStockMovement)) {               
		$this->updateStockMovement($updateStockMovement);
	    }	    
	}	
    }
    
    public function ordersCheckExpiredCarts(){
	
	 // Remote Cart - cleanup
	$amazon_cart = new AmazonRemoteCart($this->params->user_name, $this->debug);
	$expireds = $amazon_cart->expiredCarts($this->params->id_shop, $this->params->id_country);
	
	$list_sku = $updateStockMovement = array();
	
	if($this->debug) 
	    echo '<pre> expireds = '  . print_r($expireds, true) . '</pre>';
		
	foreach($expireds as $expired)
	    $list_sku[] = $expired['reference'];
	
	$products = $this->_amazonOrder->getProductsBySKUs($list_sku, $this->params->id_shop, $this->params);
	
	foreach($expireds as $expired)
	{
	    $mp_order_id = $expired['mp_order_id'];
	    $sku = $expired['reference'];
	    $quantity = $expired['quantity'];
	    
	    if (!isset($products[$sku]))
		continue;
	    
	    if (!$product = $products[$sku])
		continue;
	    
	    $id_product = (int) $product['id_product'];
	    $id_product_attribute = isset($product['id_product_attribute']) ? (int) $product['id_product_attribute'] : 0;
	    
	    // Restore stock
	    $updateStockMovement[$mp_order_id]['items'][$sku] = array (
		'id_product' => $id_product ,
		'id_product_attribute' => $id_product_attribute ,
		'quantity' => ( (int)$quantity ) * (-1) ,
		'quantity_in_stock' => $product['quantity'] ,
		'reference' => $sku ,
		'time' => $this->time ,
	     );
		 
	    $amazon_cart->removeFromCart($this->params->id_shop, $this->params->id_country, $mp_order_id, $sku);
	}

	if(!empty($updateStockMovement)) {            
	    $this->updateStockMovement($updateStockMovement, 'remove_cart');
	}

    }    
}
