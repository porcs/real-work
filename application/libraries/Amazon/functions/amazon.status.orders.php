<?php

require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once(dirname(__FILE__) . '/../classes/amazon.order.php');
require_once(dirname(__FILE__) . '/../classes/amazon.webservice.php');
require_once dirname(__FILE__) . '/../classes/amazon.xml.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.productData.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.validator.php';
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class AmazonStatusOrders
{
    const ACTION_PROCESS = 'Update Order';
    const OrderFulfillment = 'Shipment' ;
    const OrderAcknowledgement = 'Acknowledgement' ;
    const AmazonOrderFulfillment = 'OrderFulfillment' ;
    const AmazonOrderAcknowledgement = 'OrderAcknowledgement' ; 
    const Dotted = '..';

    public $order_status;
    
    public function __construct($params, $debug=false, $feed_type=self::AmazonOrderFulfillment)
    {
        $this->message = new stdClass();
        $this->params = $params;
        
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
        
        $this->debug = $debug;
        $this->batchID = uniqid();
        $this->output = null; 
        $this->time = date('Y-m-d H:i:s');
        $this->params->time = $this->time;	
		$this->feed_type = $feed_type;	
        $this->type = constant('AmazonStatusOrders::'.$feed_type);	
        $this->title = sprintf(Amazon_Tools::l("Amazon_s_started_s_s_on"), $this->params->ext, Amazon_Tools::l(AmazonStatusOrders::ACTION_PROCESS), Amazon_Tools::l($this->type));
        $this->operation = 'order_' . strtolower($this->type);
	
        $this->_amazonOrder = new Amazon_Order($this->params->user_name, false, $this->debug);
        $this->_amazonApi = $this->AmazonApi();
        
        if($this->debug == false)  
	    $this->StartProcessMessage();        
        else 
	    echo '<pre>' .print_r($this->params, true) . '</pre>';
	
        AmazonXSD::initialize();
    }

    public function confirmOrder()
    {    	
        //1. Get Oeder List
        $this->SetMessage(Amazon_Tools::l('Getting_'.$this->operation.'_list')); 
        $orders = $this->_amazonOrder->getUpdateOrderLists($this->params->id_shop, $this->params->id_country);        
        $feed_type = self::AmazonOrderFulfillment ;
        
        if($this->debug)  {
	    echo '<pre>' . print_r(array('OrdersList' => $orders), true) . '</pre>';        
	    $this->FeedSubmission($feed_type, $orders);
	    exit;
	}
        
        if (!isset($orders) || empty($orders)) {
            if (isset($this->proc_rep)) {
                $message = array(
                    'type' => $this->title, 
                    'no_product' =>  Amazon_Tools::l('No_order_update')
                ); 
                $this->proc_rep->set_status_msg($message);
            }
        } else {
	    	// update shipped status to MpMultichannel
	    foreach($orders as $order){
		$this->_amazonOrder->updateMpMultichannelOrderStatus($this->params->id_shop, $this->params->id_country, $order['OrderID'], constant('Amazon_Order::'.'SHIPPED'));
	    }
            $this->FeedSubmission($feed_type, $orders);
        }
        $this->StopFeed();
    }
    
    public function updateAcknowledgeOrder($orders)
    {    		
        $feed_type = self::AmazonOrderAcknowledgement ;
        
        if($this->debug)  
	    echo '<b>updateAcknowledgeOrder</b> <pre>' . print_r(array('OrdersList' => $orders), true) . '</pre>';        
        
        if (!isset($orders) || empty($orders)) {
            if (isset($this->proc_rep)) {
                $message = array(
                    'type' => $this->title, 
                    'no_product' =>  Amazon_Tools::l('No_order_update')
                ); 
                $this->proc_rep->set_status_msg($message);
            }
        } else {
	    
	    // 1. update seller order id
	     $this->_amazonOrder->updateOrderAcknowledgementSellerID($this->params->id_shop, $this->params->id_country, $orders);
	    
	    // 2. get seller order id to send update 
	     $orders = $this->_amazonOrder->getOrderAcknowledgementToUpdate($this->params->id_shop, $this->params->id_country);
	    
	    if($this->debug)  
		echo '<pre>' . print_r(array('OrderAcknowledgementToUpdate' => $orders), true) . '</pre>'; 
	    
	    // submit order seller id
            $this->FeedSubmission($feed_type, $orders);
	    
        }
        
        $this->StopFeed();
    }

    public function cancelOrder($order_list)
    {
        $error = array();
        if(isset($order_list) && is_array($order_list)) {

            $features = $this->_amazonOrder->get_configuration_features($this->params->id_country, $this->params->id_shop);

            if(!isset($features['orders_cancelation']) || !$features['orders_cancelation']){

                $this->error = Amazon_Tools::l('Order cancelation is not allow, please activate the Confirm Order Cancellation Features.');
                if (isset($this->proc_rep)) {
                    $this->proc_rep->set_error_msg('Error : ' . $this->error);
                }

            } else {

                $orders = array();

                foreach ($order_list as $id_order) {

                    $order = new Orders($this->params->user_name, (int)$id_order);
                   
                    $reason = $order->comment;

                    $feed_type = self::AmazonOrderAcknowledgement ;
                    $this->order_status = $order->order_status ;

                    if($this->debug)
                        echo '<b>cancelOrder</b> <pre>' . print_r(array('OrdersList' => $order), true) . '</pre>';

                    if (!isset($order) || empty($order)) {

                        $this->error = Amazon_Tools::l('No_order_update');
                        if (isset($this->proc_rep)) {
                            $message = array(
                                'type' => $this->title,
                                'no_product' => $this->error
                            );
                            $this->proc_rep->set_status_msg($message);
                        }

                    } else {

                        $id_order = $order->id_orders;

                        if (!strlen($order->id_marketplace_order_ref)) {                            
                            $error[$id_order] = 'Missing marketplace order id: '.$id_order;
                            continue;
                        }

                        $multichannel = $this->_amazonOrder->getMpMultichannelByMpOrderId($order->id_marketplace_order_ref);

                        // check if AFN skip
                        if (isset($multichannel['mp_channel']) && $multichannel['mp_channel'] == Amazon_Order::AMAZON_FBA_AMAZON) {                           
                            $error[$id_order] = 'Skip AFN, order id:  '.$id_order;
                            continue;
                        }

                        // check if status is aleady cancel
                        if(isset($multichannel['mp_status']) && $multichannel['mp_status'] == Amazon_Order::CANCELLED){                            
                            $error[$id_order] = 'Skip cancelled, order id:  '.$id_order;
                            continue;
                        }

                        $orders[$order->id_orders]['status'] = 'Failure';
                        $orders[$order->id_orders]['AmazonOrderID'] = $order->id_marketplace_order_ref;
                        $orders[$order->id_orders]['MerchantOrderID'] = $order->invoices['seller_order_id'];
                        $orders[$order->id_orders]['items'] = array();

                        foreach ($order->item as $key => $item){
                            $orders[$order->id_orders]['items'][$key]['order_item_id'] = $item['id_marketplace_order_item_ref'];
                            $orders[$order->id_orders]['items'][$key]['sku'] = $item['reference'];
                            $orders[$order->id_orders]['items'][$key]['reason'] = $reason;
                        }
                    }
                }

                // submit order seller id
                if(!empty($orders))
                    $this->FeedSubmission($feed_type, $orders); //Failure
            }
        }

        // logs order
        if(!empty($error)){
            $logs = array();
            foreach ($error as $request_id => $output){
                $logs[] = array(
                        "id_shop" => $this->params->id_shop,
                        "id_marketplace" => Amazon_Order::$MarketplaceID,
                        "site" => $this->params->id_country,
                        "batch_id"  => $this->batchID,
                        "request_id" => $request_id,
                        "error_code" => 0,
                        "message" => $output,
                        "date_add" => date('Y-m-d H:i:s')
                    );
            }

            if($this->debug)
                echo '<b>Errir : </b> <pre>' . print_r($logs, true) . '</pre>';
            else
                $this->_amazonOrder->save_submission_result_log($logs);
        }

        $this->StopFeed();
    }
    
    private function FeedSubmission($feed_type, $orders){
	
            $this->productIndex = sizeof($orders);
            
            global $user_data;
            $user_data = $this->params;
            $user_data->batchID = $this->batchID;
            
            $xml = AmazonXml::getXmlRequest($feed_type, AmazonStatusOrders::ACTION_PROCESS, $orders, null, $this->debug);

            if($this->debug) {
                echo '<br/> ----------------------' . $feed_type . ' xml------------------------- <br/>';
                echo '<pre style="background: #ccc; padding: 10px;">'.htmlentities($xml['xml']).'</pre>';
                echo '<br/>';
		return;
	    }
	    
            if (isset($xml['xml']) && !empty($xml['xml']) && $xml != false)
            {
                //$this->_amazonApi->generate_xml($this->params->user_name, 'send_' . $feed_type, $xml['xml']);
                $pxml = new SimpleXMLElement($xml['xml']);
                if ($pxml->MessageType == "Error") {
                    $this->Log($pxml, $feed_type);
                } else {
                    
                    // Send XML to Amazon
                    $this->SetMessage(Amazon_Tools::l('Sending_'.$this->operation.'_lists')); 
                    $data = $this->_amazonApi->updateFeeds($xml['xml'], strtolower($feed_type));  
                                       
                    if (isset($data->Error) && is_object($data->Error)) {
                        //$this->_amazonApi->generate_xml($this->params->user_name, 'update_' . $feed_type . '_error_' . $this->batchID, $data->saveXML());  
                        $this->SetError(sprintf('%s : %s', Amazon_Tools::l("Error"), strval($data->Error->Message)));  
                    } else {
                        
                        if (!isset($data) || empty($data)) {
                            $this->SetError(Amazon_Tools::l('Empty_Feedsubmission_Id')); 
                        }
			
                        $SubmissionId = $data;
                        
                        //4. Get Feed Submission List from Amazon
                        if ($this->GetFeedSubmissionList($SubmissionId, $feed_type)) {
                            list($usec, $sec) = explode(" ", microtime());
                            $start_time = $current_time = $sec;
                            $limit_sec = 60 * (0.25);
                            $sleep_step = 10;
                            while ($current_time - $start_time <= $limit_sec) {
                                sleep($sleep_step);
                                list($usec, $sec) = explode(" ", microtime());
                                $current_time = $sec;
                            }

                            //5. Get Feed Submission Result from Amazon
                            $this->GetFeedSubmissionResult($SubmissionId, $feed_type, $xml['orders']);
                        }
                    }
                }
            }
    }
    
    private function GetFeedSubmissionList($SubmissionId, $feed_type) {

        $this->SetMessage(sprintf(Amazon_Tools::l('Getting_Submission_List_ID_s_s'), $SubmissionId, ' '));
        $FeedSubmissionListResult = $this->_amazonApi->getFeedSubmissionListResult($SubmissionId);

        if (isset($FeedSubmissionListResult->Error) && is_object($FeedSubmissionListResult->Error)) {
            //$this->_amazonApi->generate_xml($this->params->user_name, 'get_submission_list_' . $feed_type . '_error_' . $this->batchID, $FeedSubmissionListResult->saveXML());
            $this->SetError(sprintf(Amazon_Tools::l('Get_submission_List_Error_s_s_s'), strval($FeedSubmissionListResult->Error->Message), $SubmissionId, ' '));
        } else {
            if (isset($FeedSubmissionListResult) && !empty($FeedSubmissionListResult)) {
                return (true);
            } else {
                $this->GetFeedSubmissionList($SubmissionId, $feed_type);
            }
        }
    }

    private function GetFeedSubmissionResult($SubmissionId, $feed_type, $orders = null) {
        
        $this->message->$feed_type = new stdClass();
        $this->SetMessage(sprintf(Amazon_Tools::l('Getting_Submission_Result_ID_s_s'), $SubmissionId, ' '));
        $FeedSubmissionResult = $this->_amazonApi->getFeedSubmissionResult($SubmissionId);

        if (isset($FeedSubmissionResult->Error) && is_object($FeedSubmissionResult->Error)) {
            //$this->_amazonApi->generate_xml($this->params->user_name, 'get_submission_result_' . $feed_type . '_error_' . $this->batchID, $FeedSubmissionResult->saveXML());
            $this->error = strval($FeedSubmissionResult->Error->Message);
            if (isset($FeedSubmissionResult->Error->Code) && strval($FeedSubmissionResult->Error->Code) == "FeedProcessingResultNotReady") {
                if (isset($this->proc_rep)) {
                    $this->message->$feed_type->error = sprintf(Amazon_Tools::l('s_the_process_will_restart_in_s_minutes'),  $this->error, '5');
                    $this->proc_rep->set_status_msg($this->message);
                }

                list($usec, $sec) = explode(" ", microtime());
                $start_time = $current_time = $sec;
                $limit_sec = 60 * 5;
                $sleep_step = 10;
                while ($current_time - $start_time <= $limit_sec) {
                    sleep($sleep_step);
                    list($usec, $sec) = explode(" ", microtime());
                    $current_time = $sec;
                }

                $this->GetFeedSubmissionResult($SubmissionId, $feed_type, $orders);
            } else {
                $this->SetError(sprintf(Amazon_Tools::l('Get_submission_Result_Error_s_s'), $this->error, $SubmissionId, ' '));
            }
        } else {
            
            if($this->debug){
                echo '<pre>' . print_r(array('FeedSubmissionResult' => $FeedSubmissionResult), true) . '</pre>';
            }
            
            //Amazon Log Here 
            if (isset($FeedSubmissionResult->Message->ProcessingReport) && !empty($FeedSubmissionResult->Message->ProcessingReport)) {
                $FeedSubmissionResultInfo = array(
                    'FeedSunmissionId' => strval($FeedSubmissionResult->Message->ProcessingReport->DocumentTransactionID),
                    'StatusCode' => strval($FeedSubmissionResult->Message->ProcessingReport->StatusCode),
                    'MessagesProcessed' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesProcessed),
                    'MessagesSuccessful' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesSuccessful),
                    'MessagesWithError' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithError),
                    'MessagesWithWarning' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithWarning)
                );
                
                $this->no_process = strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesProcessed);
                $this->no_success = strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesSuccessful);
                $this->no_error = strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithError);
                $this->no_warning = strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithWarning);

                $log_data = array(
                    'batch_id' => $this->batchID,
                    'id_country' => $this->params->id_country,
                    'id_shop' => $this->params->id_shop,
                    'action_type' => ucwords(AmazonStatusOrders::ACTION_PROCESS),
                    'feed_type' => ucwords($feed_type),
                    'message' => base64_encode(serialize($FeedSubmissionResultInfo)),
                    'date_add' => date("Y-m-d H:i:s"),
                );

                $this->_amazonOrder->save_response_log($log_data);
                $success_message = "Success";
                
                // Success
                $this->message->type = $this->title;
                $this->message->date = date('Y-m-d', strtotime($this->time));
                $this->message->time = date('H:i:s', strtotime($this->time));
                $this->message->$feed_type->status = 'Success';
                $this->message->$feed_type->message = $success_message;
                $this->message->$feed_type->FeedSubmissionResult = $FeedSubmissionResultInfo;
                $this->message->$feed_type->Report = Amazon_Tools::l('See_result');
                $this->message->$feed_type->Country = $this->params->id_country;

                //Amazon Log Here 
                $Result = array();
                $no = 0;
		
		if($this->debug) 
		     echo '<pre>' . print_r(array('orders' => $orders), true) . '</pre>';
		
                //Error Result
                if (isset($FeedSubmissionResult->Message->ProcessingReport->Result)) {
                    foreach ($FeedSubmissionResult->Message->ProcessingReport->Result as $key => $result) {
                        if (isset($result->ResultCode) && strval($result->ResultCode) == "Error") {
                            $Result[$no] = array(
                                "id_shop" => $this->params->id_shop,
                                "id_marketplace" => Amazon_Order::$MarketplaceID,
                                "site" => $this->params->id_country,
                                "batch_id"  => $this->batchID,
                                "request_id" => isset($result->AdditionalInfo->AmazonOrderID) ? strval($result->AdditionalInfo->AmazonOrderID) : '',
                                "error_code" => strval($result->ResultMessageCode),
                                "message" => strval($result->ResultDescription),
                                "date_add" => date("c", time())
                            );
                            //remove unuse order
                            if(($key = array_search(strval($result->AdditionalInfo->AmazonOrderID), $orders)) !== false) {
                                unset($orders[$key]);
                            }
                            $no++;
                        }
                    }
                    
                    if($this->debug) echo '<pre>' . print_r(array('Result' => $Result), true) . '</pre>';
                     
                    $this->_amazonOrder->save_submission_result_log($Result);
                }
                
                if (isset($orders) && !empty($orders)){

                    if(isset($this->order_status) && in_array($this->order_status, array('Canceled', 'Cancelled', 'Cancel'))){ // for cancelled reason //update mp_status to 5
                        
                        if($feed_type == AmazonStatusOrders::AmazonOrderAcknowledgement){
                            foreach ($orders as $order) {
                                $mp_order = $this->_amazonOrder->getMpMultichannelByMpOrderId($order) ;
                                if(isset($mp_order['id_orders']))
                                    $this->_amazonOrder->updateMpMultichannelOrderStatus($this->params->id_shop, $this->params->id_country, $mp_order['id_orders'], Amazon_Order::CANCELED);
                            }
                        }

                    } else {

                        // update shipping status flag
                        if($feed_type == AmazonStatusOrders::AmazonOrderFulfillment) //update mp_status to 5
                            $this->_amazonOrder->update_order_shipping($orders);

                        // update mp multichannel acknowladge flag
                        if($feed_type == AmazonStatusOrders::AmazonOrderAcknowledgement)
                            $this->_amazonOrder->updateOrderAcknowledgementFlag($this->params->id_shop, $this->params->id_country, $orders, 1);

                    }
                }
                
            } else {
                $this->message->$feed_type->message = Amazon_Tools::l("No_message_from_Amazon.");
                $this->message->$feed_type->FeedSubmissionResult = '';
            }

            if (isset($this->proc_rep)) {
                $this->proc_rep->set_status_msg($this->message);
            }
            
            return $orders;
        }

        return(false);
    }
    
    public function Log($pxml, $type)
    {
        foreach ($pxml as $message)
        {
            $error_data = array(
                'batch_id' => $this->batchID,
                'id_country' => $this->params->id_country,
                'id_shop' => $this->params->id_shop,
                'action_process' => AmazonStatusOrders::ACTION_PROCESS,
                'action_type' => $type,
                'message' => strval($message),
                'date_add' => date("Y-m-d H:i:s"),
            );

            $this->_amazonOrder->save_validation_log($error_data, $this->params->id_shop, $this->params->id_country);
        }
        
        $this->error = Amazon_Tools::l('Validation_Error');
        $this->message->$type->error = $this->error;
        $this->message->country = $this->params->id_country;
        $this->message->all_message = Amazon_Tools::l('See_more_error_in_Logs');
        
        if (isset($this->proc_rep)) 
	    $this->proc_rep->set_status_msg($this->message);
	
        $this->StopFeed();
    }
    
    private function update_log($datail = null){
        
        // Log 
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => $this->feed_type,
            "no_send"       => isset($this->productIndex) ? (int)$this->productIndex : 0,
            "no_skipped"    => 0, 
            "no_process"    => isset($this->no_process) ? $this->no_process : 0,
            "no_success"    => isset($this->no_success) ? $this->no_success : 0,
            "no_error"      => isset($this->no_error) ? $this->no_error : 0,
            "no_warning"    => isset($this->no_warning) ? $this->no_warning : 0,     
            "is_cron"       => true,                
            "date_upd"      => $this->time
        );
        
        if(isset($datail) && !empty($datail))
             $log['detail'] = base64_encode(serialize($datail));
            
        $this->_amazonOrder->update_log($log);
    }
    
    
    private function AmazonApi()
    {
        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
        
        //MWSAuthToken
        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
            $auth['auth_token'] = trim($this->params->auth_token);
        }
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
        if (!$amazonApi = new Amazon_WebService($auth, $marketPlace, null, $this->debug))
        {
            $this->error = Amazon_Tools::l('Unable_to_connect_Amazon');
            
            if(isset($this->proc_rep))
                $this->proc_rep->set_error_msg('Error : ' . $this->error);
            
            $this->StopFeed();
        }
        return $amazonApi;
    }
    
    private function StartProcessMessage()
    {
        $process_title = $this->title; //sprintf(Amazon_Tools::l("Amazon_s_s_Processing"), AmazonStatusOrders::ACTION_PROCESS, constant('AmazonStatusOrders::'.$type));
        $process_type = $this->operation . '_' .  $this->params->countries;
        $marketplace = strtolower(Amazon_Order::Amazon);
		
        $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, true );
        $this->proc_rep->set_process_type($process_type); 
        $this->proc_rep->set_popup_display(true);
        $this->proc_rep->set_marketplace($marketplace);
        
        if($this->proc_rep->has_other_running($process_type)){
            $this->StopFeed();
        }
    }
    
    private function StopFeed()
    {   
        $error = null;
        if(isset($this->error) && !empty($this->error))
            $error = $this->error;
        
        $this->update_log(array('error'=>$error));
        
        $json = json_encode( array('status' => isset($error) ? 'error' : 'success' , 'message' => $error ? $error : $this->output)) ;
        echo $json ;

        if(isset($this->proc_rep))
            $this->proc_rep->finish_task();
        
        //exit;
    }
    
    private function SetMessage($msg)
    {
        if(isset($this->proc_rep))
        {
            $message = array(
                'type' => $this->title, 
                'date' => date('Y-m-d', strtotime($this->time)),
                'time' => date('H:i:s', strtotime($this->time)),
                self::AmazonOrderFulfillment => array(
                    'message'=> (string)$msg . self::Dotted
                    )
                ); 
            $this->proc_rep->set_status_msg($message);
        } 
    }   
    
    private function SetError($msg)
    {
        if(isset($this->proc_rep))
        {
            $this->error = $msg;
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) $this->proc_rep->set_error_msg($this->message);
            $this->StopFeed();
        } 
    }    
    
}
