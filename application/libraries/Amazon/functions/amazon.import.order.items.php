<?php

require_once(dirname(__FILE__) . '/../classes/amazon.config.php');
require_once(dirname(__FILE__) . '/../classes/amazon.order.php');
require_once(dirname(__FILE__) . '/../classes/amazon.webservice.php');
require_once dirname(__FILE__) . '/../classes/amazon.xml.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.productData.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.validator.php';
require_once dirname(__FILE__) . '/../classes/amazon.remote.cart.php';
require_once(dirname(__FILE__) . '/../../FeedBiz/config/stock.php');
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class AmazonImportOrderItems 
{
    //const ACTION_PROCESS = 'Orders';    
    //public $FBA = false;

    public function __construct($params, $debug = false, $cron = false)
    { 
        $this->params = $params;    
        
        //if(isset($this->params->language))
        //    Amazon_Tools::load(_ORDER_LANG_FILE_, $this->params->language);
        
        $this->debug = $debug;
        $this->cron = $cron;
        //$this->option = 
	$this->output = null; 
        $this->batchID = uniqid();
        $this->time = date('Y-m-d H:i:s');
        //$this->params->time = $this->time;      
        //$this->title = sprintf(Amazon_Tools::l("Amazon%s Started Order Query"), $this->params->ext) . ' '. Amazon_Tools::l('on') ;
        //$this->operation = 'get_order' ;  
        $this->_amazonOrder = new Amazon_Order($this->params->user_name, false, $this->debug);
        
        //if($this->debug == false) $this->StartProcessMessage();         
        //$this->SetMessage(Amazon_Tools::l('Start Get Order List') . '...'); 
       
        $this->_amazonApi = $this->AmazonApi();
        
    }    
    
    public function OrderMissingItems()
    {        
        /*$message  = array();
        $message['type'] = $this->title; 
        $message['date'] = date('Y-m-d', strtotime($this->time)); 
        $message['time'] = date('H:i:s', strtotime($this->time));         
        $message['country'] = $this->params->id_country;
        $message['all_message'] = Amazon_Tools::l('See all messages');
        $message['path_to_message'] = $this->batchID;*/
                
        //$this->SetMessage(sprintf(Amazon_Tools::l("Getting %s Order List"), strval($this->option->status)) . '...' );
	//
        //1. Get Oeder Missing Item List
	$orders = $this->_amazonOrder->getOrderMissingItems($this->params->id_shop);
	
	if(!isset($orders) || empty($orders))
	{
	    $this->StopFeed(true);
	}
	
	if($this->debug){
	    echo '<br/>orders : <pre> ' . print_r($orders, true). '</pre>';
	}
         	
	foreach ($orders as $order){
	    //2. List Order Item and Save
	    $this->GetOrderItems($order['id_order'], $order['id_marketplace_order_ref']);       
	    
	}
	
        // Error
        //if(isset(Amazon_Order::$errors) && !empty(Amazon_Order::$errors))
        //    $this->ValidationError(Amazon_Order::$errors, 'error');
        
        // Warning
        //if(isset(Amazon_Order::$warnings) && !empty(Amazon_Order::$warnings))
        //    $this->ValidationError(Amazon_Order::$warnings, 'warning');
        
        // Message
        //if(isset(Amazon_Order::$messages) && !empty(Amazon_Order::$messages))
        //    $this->ValidationError(Amazon_Order::$messages, 'message');
        
        /*if(isset($this->proc_rep))
        {
            if(!empty(Amazon_Order::$errors)) 
            {
                $message['error'] = array_slice(Amazon_Order::$errors, 0, 3);
                $this->error = $message['error'];  
            }
            
            if(!empty(Amazon_Order::$warnings)) 
            {
                $message['warning'] = array_slice(Amazon_Order::$warnings, 0, 3);
                $this->output = $message['warning'];
            }
            
            if(!empty(Amazon_Order::$messages)) 
            {
                $message['message'] = array_slice(Amazon_Order::$warnings, 0, 3);
                $this->output = $message['message'];
            }
            
            $message[AmazonImportOrders::ACTION_PROCESS]['message'] = Amazon_Tools::l('Success');
            $message[AmazonImportOrders::ACTION_PROCESS]['status'] = Amazon_Tools::l('Success');
            
            $this->proc_rep->set_status_msg($message);
        }*/        
	
        $this->StopFeed();
    }    
    
    public function GetOrderItems($id_order, $AmazonOrderId)
    {
	$order_items = $this->_amazonApi->GetOrderItems((string)$AmazonOrderId);  

	// save to order item
	if (isset($order_items) && !empty($order_items)) {
	    $this->_amazonOrder->saveOrderItems($this->params->id_shop, $id_order, $AmazonOrderId, $order_items, $this->params);
	} 
	
    }

    private function StartProcessMessage()
    {
        $process_title = sprintf('%s, %s ...', 'Amazon', Amazon_Tools::l('Order Query Processing')); 
        $process_type = $this->operation . '_' .  $this->params->countries;
        $marketplace = strtolower(Amazon_Order::Amazon);
        $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, true );
        $this->proc_rep->set_process_type($process_type); 
        $this->proc_rep->set_popup_display(true);
        $this->proc_rep->set_marketplace($marketplace);
        
        if($this->proc_rep->has_other_running($process_type))
        {
            $this->StopFeed(true);
        }
    }
    
    private function StopFeed($exit=false)
    {   
        $error = null;
        
        if(isset($this->error) && !empty($this->error))
            $error = $this->error;
	
        $json = json_encode( array('status' => isset($error) ? 'error' : 'success' , 'message' => $error ? $error : $this->output)) ;
        echo $json ;

        //if(isset($this->proc_rep))
        //    $this->proc_rep->finish_task();
        if($exit)
            exit;
    }
    
    private function AmazonApi()
    {
        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
		
        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
                $auth['auth_token'] = trim($this->params->auth_token);
        }
		
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
	
        if (!$this->amazonApi = new Amazon_WebService($auth, $marketPlace, null, $this->debug))
        {
            $this->error = Amazon_Tools::l('Unable to connect Amazon');
            
            if(isset($this->proc_rep))
                $this->proc_rep->set_error_msg('Error : ' . $this->error);
            
            $this->StopFeed();
        }
        return $this->amazonApi;
    }
    
    /*public function ValidationError($pxml, $type)
    {
        foreach ($pxml as $message)
        {
            $error_data = array(
                'batch_id' => $this->batchID,
                'id_country' => $this->params->id_country,
                'id_shop' => $this->params->id_shop,
                'action_process' => AmazonImportOrders::ACTION_PROCESS,
                'action_type' => $type,
                'message' => strval($message),
                'date_add' => date("Y-m-d H:i:s"),
            );            
            
            $this->_amazonOrder->save_validation_log($error_data, $this->params->id_shop, $this->params->id_country);
        }

    }*/
}
