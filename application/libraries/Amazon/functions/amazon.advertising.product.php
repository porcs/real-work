<?php

require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.advertising.class.php';
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

if(!defined("_MARKETPLACE_NAME_")) define('_MARKETPLACE_NAME_','amazon');

class AmazonAdvertisingProduct
{
    const PROCESS = 'Advertising';
    const OPERATION_TYPE = 'Product';
    public static $warnings = array();
    public static $log      = array();
    public static $allow_state = array("enabled", "paused", "archived");
    public $message;
    public $params;
    public $db;

    public function __construct($params, $cron=false, $debug=false)
    {
	$this->params = $params;	
	$this->debug = $debug;
	$this->cron = $cron;	
	// Load language
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
    }
    
    public function Dispatch()
    {
        // 1. set parameter
	$this->batchID = uniqid();
        $this->time = $this->params->time = date('Y-m-d H:i:s');
	$this->message = new stdClass();
	$this->message->batchID = $this->batchID;
	$this->message->type = sprintf(Amazon_Tools::l('AmazonS_s_started_on_s'), $this->params->ext, self::PROCESS . ' ' . self::OPERATION_TYPE, '');
	$this->message->title = self::PROCESS;
	$this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
	
	// 2. start popup process
        if (!$this->debug) {	   
            if(!$this->_startProcessMessage()){
		return (false);
            }
        }

	// 3. start amazon database
        $this->_amazonDatabase();
	
	// 4. check feature status
	$this->_checkFeatureStatus();

        // 5 . start
        $this->_init();
        
	// 6 . process
        $this->_createAdvertisingProduct();

        // 7. log validation error
        if (count(AmazonAdvertisingProduct::$warnings)) {
	    $errors = array();
	    $errors['warning'] = AmazonAdvertisingProduct::$warnings;
            $this->_validationError($errors);
        }
        
        // 8. stop popup process
	$this->_stopFeed();
    }

    private function _createAdvertisingProduct()
    {
        $profile_selected_ads = $skip_message = array();
        $skipped = 0;
        
        //1. get all campaign and group id
        $listAdGroups = $this->amazonAdvertising->listAdGroups();
        if(isset($listAdGroups['response']) && !empty($listAdGroups['response'])){

            // Profiles
            $profiles = $this->db->get_amazon_profile($this->params->id_country, $this->params->id_mode, $this->params->id_shop) ;

            // Product Options
	    $AmazonProductOption = new MarketplaceProductOption($this->params->user_name, null, null, null, null, $this->params->id_country, $this->params->id_marketplace);            

            /*foreach ($profiles as $profile){
                if(isset($profile['advertising'])){
                    foreach ($profile['advertising'] as $profile_advertising){
                        // 3. get category which choose this profile
                        $category = $this->db->get_category_selected($this->params->id_country, $this->params->id_mode, $this->params->id_shop, false, $profile['id_profile']);
                        $profile_selected_ads[$profile_advertising['campaignId']][$profile_advertising['adGroupId']] = implode(", ", $category);
                    }
                }
            }*/
         
            foreach ($listAdGroups['response'] as $ad_groups){

                $list_product_to_create = array();
                
                $state = "enabled"; //$ad_groups['state'];
                $adGroupId = $ad_groups['adGroupId'];
                $campaignId = $ad_groups['campaignId'];

                if(!in_array($ad_groups['state'], self::$allow_state)){ // skip if state not enabled
                    AmazonAdvertisingProduct::$warnings[] = "m000026;" . $adGroupId .";". $ad_groups['state'].";".implode(", ",self::$allow_state); 
                    continue;
                }

                //2. select product which not created with this campaignId and adGroupId // from table amazon_advertising_products
                $products = $this->db->getProductsToCreateAd($this->params->id_shop, $this->params->id_country, $campaignId, $adGroupId);

                if(isset($products) && !empty($products)){

                    // find offer options                    
                    $list_skus = $history = array();
                    foreach ($products as $product){
                        $list_skus[] = $product['sku'];
                    }
                    $ProductOption = $AmazonProductOption->get_product_options($this->params->id_shop, null, $list_skus);
                    
                    // list product to $list_product_to_create
                    $i = 0;
                    foreach ($products as $product){

                        if(isset($history[$product['sku']])){
                            $skip_message[$skip_id] = "m000002";
                            $skipped++;
                            continue;
                        }
                        
                        $id_product = (int)$product['id_product'];
                        $id_product_attribute = (int)$product['id_product_attribute'];
                        $skip_id = $id_product . '_' . $id_product_attribute;                      
                        
                        $options = isset($ProductOption[$id_product][$id_product_attribute]) ? $ProductOption[$id_product][$id_product_attribute] : null;
                        if(isset($options['disable']) && !empty($options['disable'])){ // skip disabled product
                            $skip_message[$skip_id] = "m000002";
                            $skipped++;
                            continue;
                        }                        
                        if(isset($options['advertising']) && $options['advertising'] == 0){ // skip don't allow advertising
                            $skip_message[$skip_id] = "m000027";
                            $skipped++;
                            continue;
                        }
                        // have ad groups override from offers options
                        if(isset($options['ad_groups']) && in_array($adGroupId, $options['ad_groups'])){
                            $list_product_to_create[$i]['campaignId'] = $adGroupId;
                            $list_product_to_create[$i]['adGroupId'] = $campaignId;
                            $list_product_to_create[$i]['sku'] = $product['sku'];
                            $list_product_to_create[$i]['state'] = $state;
                        } else {
                            // use default ad groups from profile
                            $profile = isset($profiles[$product['id_profile']]) ? $profiles[$product['id_profile']] : null ;
                            $advertisings = isset($profile['advertising']) && !empty($profile['advertising']) ? $profile['advertising'] : null ;
                            if(isset($advertisings)){
                                foreach ($advertisings as $advertising){ 
                                    // use the match campaign
                                    if($advertising['adGroupId'] == $adGroupId){ 
                                        $list_product_to_create[$i]['campaignId'] = $adGroupId;
                                        $list_product_to_create[$i]['adGroupId'] = $campaignId;
                                        $list_product_to_create[$i]['sku'] = $product['sku'];
                                        $list_product_to_create[$i]['state'] = $state;
                                    } 
                                }
                            } else {
                                $skip_message[$skip_id] = "m000028"; // missing ad group selected
                                $skipped++;
                                continue;
                            }
                        }
                        $i++;
                        $history[$product['sku']] = true;
                    }
                }else {
                    $this->error = Amazon_Tools::l('Sorry_no_items_found_process_completed_No_items_to_be_send');
                }
                
                //3. create product ads ;
                if(!empty($list_product_to_create)){
//                    if(!$this->debug){
                        $createProductAds = $this->amazonAdvertising->createProductAds($list_product_to_create);

                        echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                        echo 'createProductAds : <pre>' . print_r($createProductAds, true) . '</pre>';
                        
                        // log created product ads
                        if(isset($createProductAds['response'])){
                            //$this->db->saveProductsAdvertising($createProductAds['response'], $this->params->id_shop);
                        }
//                    } else {
                        echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                        echo 'list_product_to_create : <pre>' . print_r($list_product_to_create, true) . '</pre>';
//                    }
                }
            }
        } else {
            $this->error = Amazon_Tools::l('No_ad_group_to_be_create');
        }      
        // logs skipped
        if (isset($skip_message) && !empty($skip_message)) {
            $errors = array();
	    $errors['warning'] = $skip_message;
            $this->_validationError($errors, true);
        }
        // log warning
        if(isset($this->error) && !empty($this->error)){
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->_stopFeed();
        }
    }

    private function _init()
    {
        include dirname(__FILE__) . '/../parameters/api.settings.php';
        if(!isset($amazon_advertising_api)){
            $this->error = Amazon_Tools::l('Invalid_api_setting');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->_stopFeed();
        }

        $api = $amazon_advertising_api;
        if(!isset($api[$this->params->id_region])){
            $this->error = sprintf(Amazon_Tools::l('Invalid api setting on region : %s'), $this->params->id_region);
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->_stopFeed();
        }

        // get advertising data from db
        $advertising_data = $this->db->get_advertising($this->params->id_shop, $this->params->id_country);
        $ad_config = $api[$this->params->id_region];
        $ad_config['sandbox'] = true ;
        $ad_config['profileId'] = isset($advertising_data['profile_profileId']) ? $advertising_data['profile_profileId'] : null ;

        //we use refresh_token because the token will expire in 30 minute
        $ad_config['refreshToken'] = isset($advertising_data['refresh_token']) ? $advertising_data['refresh_token'] : null ; 

        //check expire token
        if(isset($advertising_data['access_token'])){
            $access_token = $this->db->get_advertising_access_token($this->params->id_shop, $this->params->id_country, 'access_token');
            $ad_config['accessToken'] = $access_token;
        }
	if (!$this->amazonAdvertising = new AmazonAdvertising($ad_config)) {

	    $this->error = isset($this->amazonAdvertising->message) ? $this->amazonAdvertising->message : Amazon_Tools::l('Unable_to_access_token');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->_stopFeed();
        }
    }

    private function _amazonDatabase()
    {
        if (!$this->db = new AmazonDatabase($this->params->user_name, $this->debug)) {
            $this->error = Amazon_Tools::l('Unable_to_login.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->_stopFeed();
        }
    }

    private function _startProcessMessage()
    {
        $process_title = self::PROCESS;
        $process_running = self::OPERATION_TYPE;
        $process_type = strtolower($process_title) . '_' . strtolower($process_running);
        $marketplace = strtolower(_MARKETPLACE_NAME_);
       
	$this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, false);
	$this->proc_rep->set_process_type($process_type);
	$this->proc_rep->set_popup_display(false);
	$this->proc_rep->set_marketplace($marketplace);
	    
        $process = $this->proc_rep->has_other_running($process_type);
	
        if ($process) {
            $this->message->error =sprintf(Amazon_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'),$process_running,'20');
            $this->proc_rep->set_error_msg($this->message);
            $this->proc_rep->finish_task();
	    
            echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, ' . self::OPERATION_TYPE . ' until old process done'));
	    return (false);    
        }       
	
	return (true);
    }

    private function _stopFeed($exit=true)
    {
        $error = null;
	
        if (isset($this->error) && !empty($this->error)){
            $error = $this->error;
            $this->_log(array('type' => self::OPERATION_TYPE,'error' => $error, 'date_add' => date('Y-m-d H:i:s')));

	    if ($this->debug){	   
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'Error : <pre>' . $this->error . '</pre>';
	    }
	    
        } else {
            $this->_log(array('type' => self::OPERATION_TYPE,'date_add' => date('Y-m-d H:i:s')));
        }
        
        if (isset($this->proc_rep)) 
            $this->proc_rep->finish_task();
	
	echo json_encode(array('status' => 'success', 'message' => $error));
	
	if($exit)
	    exit;
    }
    
    private function _log($datail = null)
    {
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => self::PROCESS,
            "no_skipped"    => isset($this->skipped) ? (int)$this->skipped : 0, 
            "no_process"    => isset($this->no_process) ? (int)$this->no_process : 0,
            "no_success"    => isset($this->no_success) ? (int)$this->no_success : 0,                      
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
        if(isset($datail) && !empty($datail))
             $log['detail'] = base64_encode(serialize($datail));
        
        if(!$this->debug){
            $this->db->update_log($log);
	} else {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'LOGS : <pre>' . print_r($log, true) . '</pre>';
	}
    }
    
    private function _validationError($errors, $is_skipped=false)
    {        
	$sql = '';	
        foreach ($errors as $type => $error) {
            if(!empty($error)) {
                foreach ($error as $key => $messages) {
                    $error_data = array(
                        'batch_id' => $this->batchID,
                        'id_country' => $this->params->id_country,
                        'id_shop' => $this->params->id_shop,
                        'action_process' => self::PROCESS . "_" . self::OPERATION_TYPE,
                        'action_type' => $type,
                        'message' => strval($messages),
                        'date_add' => date("Y-m-d H:i:s"),
                    );
                    if($is_skipped){
                        $error_data['id_message'] = $key;
                    }
                    $sql .= $this->db->save_validation_log($error_data, false);
                    unset($error_data);
                }
            }
        }
        unset($errors);
        if(strlen($sql) > 10)
        {
            if (!$this->debug){
                $this->db->exec_query($sql);
            } else {
                echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                echo 'SQL : <pre>' . $sql . '</pre>';
            }
        }
	unset($sql);
    }    
    
    private function _checkFeatureStatus()
    {
	$features = $this->db->get_configuration_features($this->params->id_country, $this->params->id_shop);
	if(!isset($features['advertising']) || empty($features['advertising']) || $features['advertising'] = 0) {
	    $this->error = Amazon_Tools::l('inactivate_advertising');
	    $this->message->error = $this->error;
	    $this->message->link = array('amazon', 'features', $this->params->id_country);
	    $this->message->link_label = Amazon_Tools::l('Go to features');
	    if (isset($this->proc_rep)) {
		$this->proc_rep->set_error_msg($this->message);
	    }
	    $this->_stopFeed();
	}
    }
    
    private function _setErrorMsg($msg, $function=null)
    {
	$this->error = Amazon_Tools::l($msg);
	$this->message->error = $this->error;

	if ($this->debug) {
	    printf("$function() ".$this->error."<br/>\n");
	}

	if (isset($this->proc_rep)) {
	    $this->proc_rep->set_error_msg($this->message);
	}
    }
    		    
    
}