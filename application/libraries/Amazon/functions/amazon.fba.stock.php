<?php

// cron every day
require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.webservice.php';
require_once dirname(__FILE__) . '/../classes/amazon.database.php';
require_once(dirname(__FILE__) . '/../../FeedBiz/config/stock.php');
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

if(!defined("_MARKETPLACE_NAME_"))define('_MARKETPLACE_NAME_','amazon');

class AmazonFBAStock
{
    const PROCESS = 'FBA Manager';
    const PROCESS_KEY = 'm';
    const FBA_STOCK_SWITCH = 1;
    const FBA_STOCK_SYNCH = 2;
    
    public static $tag = _MARKETPLACE_NAME_;
    public static $fba_stock_type = array(1=>'switching', 2=>'synchronize');
    public static $warnings = array();
    public static $log      = array();
    
    public $exts	    = array();
    public $log_message	    = array();
    public $update_fba	    = array();
    public $stock_movement;

    public function __construct($params, $cron=false, $debug=false)
    {
	$this->params = $params;	
	$this->debug = $debug;
	$this->cron = $cron;	
	
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
	
	$this->log_message['1'] = Amazon_Tools::l('Product became out of stock, switching to MFN (option)');
	$this->log_message['2'] = Amazon_Tools::l('Product in stock (FBA), switching to AFN');
	$this->log_message['3'] = Amazon_Tools::l('Product in stock (FBA), but not valued for FBA, switching to MFN');
        $this->log_message['4'] = Amazon_Tools::l('Product in stock (FBA), and it is in FBA, nothing todo');
    }

    public function Dispatch($forceUpdate=false, $anticipate=false, $days=1, $result=null) {
	
	// 1. set parameter
	$this->batchID = uniqid();
	$this->process = self::PROCESS;	
        $this->time = $this->params->time = date('Y-m-d H:i:s');
	
	$this->message = new stdClass();
	$this->message->batchID = $this->batchID;
	$this->message->type = sprintf(Amazon_Tools::l('AmazonS_s_started_on_s'), ''/*$this->params->ext*/, $this->process, '');
	$this->message->title = $this->process;
	$this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
		
	$action = isset($this->params->fba_stock_behaviour) ? $this->params->fba_stock_behaviour : '' ;
        $this->operation_type = isset(self::$fba_stock_type[$action]) ? self::$fba_stock_type[$action] : '' ;	
	$this->process_type = $action;
	
	// 2. start popup process
        if (!$this->debug) {
            $this->StartProcessMessage();
        }
	
	// 3. start amazon database
        $this->AmazonDatabase();
	
	// 4. start amazon webservice
        if(!isset($result) || empty($result) || !$result){
            $this->AmazonApi();
        }
	
	// 5. check feature status
	$this->checkFeatureStatus();
	
	// 6. check behavior
	$this->checkBehavior();
	
	// 7. get ext by region
	$this->getExtFBA();
	
	// 8. Manage Stocks
        switch ($action) {
	    case self::FBA_STOCK_SWITCH :
		$this->ManageStocks($forceUpdate, $anticipate, $days, $result);
                break;
	    case self::FBA_STOCK_SYNCH :
		$this->SynchStocks($forceUpdate, $anticipate, $days, $result);
                break;
        }
	
	// 9. stop popup process   
        $this->StopFeed();
    }
    
    public function SynchStocks($forceUpdate=false, $anticipate=false, $days=1, $result=null) /*$forceUpdate=false, $anticipate=false, */
    {        
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->inventory))
		$this->message->inventory = new stdClass ();

	    $this->message->inventory->message = Amazon_Tools::l('Listting Inventory') . "..";
	    $this->proc_rep->set_status_msg($this->message);
	}

	$updateProductOptions = $saveProductOptions = $updateProduct = $updateFlagUpdate = '';

        if(!isset($result) || empty($result)){
            $result = $this->api->ListInventoryByDate(date('Y-m-d', time() - (86400 * $days)));
        }

	$list_sku = array();
	foreach ($result as $res){
	    $list_sku[] = $res['SKU'];
	}

	if ($this->debug){
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'list_sku : <pre>' . print_r($list_sku, true) . '</pre>';
	}

        if ($result && is_array($result) && count($result)) {

	    $this->no_process = $this->no_success = $this->no_error = $this->skipped = 0;

	    // get products
	    $products = $this->db->getProductBySKU($this->params->id_shop, $list_sku, true, true);
            if ($this->debug){
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'products : <pre>' . print_r($products, true) . '</pre>';
	    }

             // get products options for all ext in region
            $AmazonProductOption = $ProductOptions = array();
            foreach ($this->exts as $ext => $id_country){
                $AmazonProductOption[$ext] = new MarketplaceProductOption($this->params->user_name,null,null,null,null, $id_country, $this->params->id_marketplace);
                $ProductOptions[$ext] = $AmazonProductOption[$ext]->get_product_options($this->params->id_shop, null, $list_sku);
            }
	    if ($this->debug){
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'Product Options : <pre>' . print_r($ProductOptions, true) . '</pre>';
	    }

            $this->stock_movement = new StockMovement($this->params->user_name);
            $stockmovement_url = $this->checkUrl('stockmovement_fba');

            $flagUpdateReprice = $missing_offers_options = array();
            
            foreach ($result as $SKU => $item) {

		$SKU = (string)$SKU;

		$message = '';
		$this->no_process++;

                if (!isset($products[$SKU]) || empty($products[$SKU])) {
                    if($this->debug){
                        echo '<br/>---------------------------------------------------------<br/>' ;
                        echo sprintf('%s - (%s)', Amazon_Tools::l('Unable to find product'), $SKU) . '<br/>';
                    }
                    AmazonFBAStock::$warnings[] = sprintf('%s - (%s)', Amazon_Tools::l('Unable to find product'), $SKU);
		    $this->skipped++;
                    continue;
                }

                // 1 find product data
                $product = $products[$SKU];
		$id_product = (int) $product['id_product'];
		$id_product_attribute = (int) $product['id_product_attribute'];

		if ($this->debug){
		    echo '---------------------------------------------------------';
		    echo '<pre>Id Product : '.$id_product.', ' ;
		    echo 'Id Product Attribute : '.$id_product_attribute.'</pre>' ;
		}

                // 2 find Quantity Considered
                /*if ($anticipate && array_key_exists('TotalSupplyQuantity', $item) && $item['TotalSupplyQuantity'] > $item['InStockSupplyQuantity']) {
                    $quantityConsidered = $item['TotalSupplyQuantity'];
                } else {*/
                    $quantityConsidered = $item['InStockSupplyQuantity'];
                //}

                // check if the product is Amazon-fulfilled listing
                $totalSupplyQuantity = $quantityConsidered;
                if (array_key_exists('TotalSupplyQuantity', $item)) {
                    $totalSupplyQuantity = $item['TotalSupplyQuantity'];
                }
                
		if ($this->debug){
		    echo 'Item : <pre>' . print_r($item, true) . '</pre>';
		    echo '<b>Quantity Considered : ' . print_r($quantityConsidered, true) . '</b><br/>';
		}

                // 3 consider each country
                foreach ($this->exts as $ext => $id_country){

                    if (!isset($ProductOptions[$ext][$id_product][$id_product_attribute]) || empty($ProductOptions[$ext][$id_product][$id_product_attribute])) { // dont have option
                        $missing_offers_options[$ext]  = array(
                            'id_product'            =>  $id_product,
                            'id_product_attribute'  =>  $id_product_attribute,
                            'sku'                   =>  $SKU,
                            'id_shop'               =>  (int)$this->params->id_shop,
                            'id_country'            =>  (int)$id_country,
                            'date_add'              =>  $this->time,
                        );
                        $saveProductOptions .= $AmazonProductOption[$ext]->save_offers_options($missing_offers_options[$ext], true);
                    } else {
                        // Product Options
                        $options = $ProductOptions[$ext][$id_product][$id_product_attribute] ;
                    }

                    $where = array(
                        'id_product'            =>  array('value' => $id_product, 'operation' => '='),
                        'id_product_attribute'  =>  array('value' => $id_product_attribute, 'operation' => '='),
                        'sku'                   =>  array('value' => $SKU, 'operation' => '='),
                        'id_shop'               =>  array('value' => (int)$this->params->id_shop, 'operation' => '='),
                        'id_country'            =>  array('value' => (int)$id_country, 'operation' => '='),
                    );

                    $offers_options = array(
                        //'id_product'            => $id_product,
                        //'id_product_attribute'  => $id_product_attribute,
                        //'sku'                   => $SKU,
                        //'id_shop'               => (int)$this->params->id_shop,
                        //'id_country'            => (int)$id_country,
                        'date_upd'		=> $this->time,
                    );

                    $flag_fba = 0;
                    $flag_fba_stock = 0;
                    
                    if ($quantityConsidered == 0) { // Became out of stock

                        if (isset($options['fba']) && $options['fba']) { // offer option have fba set to false

                            // Turns Product to MFN for all targets marketplaces
                            $offers_options['fba'] = ($totalSupplyQuantity > 0) ? 1 : 0;
                            $updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";

                            // Update table fba_data
                            $offers_options['fba_inventory'] = $quantityConsidered;
                            $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where);	// Update table fba_data
                            $updateFlagUpdate .= ImportLog::update_flag_offers($this->params->id_marketplace, $ext, $this->params->id_shop, $id_product, 1,false);

                            $flag_fba = 1;
                        } else {

                            if($totalSupplyQuantity > 0){
                            //if (!isset($options['fba']) || !$options['fba']) {
                                // Turns Product to MFN for all targets marketplaces
                                $offers_options['fba'] = 1;
                                $updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";

                                // Update table fba_data
                                $offers_options['fba_inventory'] = $quantityConsidered;
                                $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where);	// Update table fba_data
                                $updateFlagUpdate .= ImportLog::update_flag_offers($this->params->id_marketplace, $ext, $this->params->id_shop, $id_product, 1,false);

                                $flag_fba = 1;
                            //}
                            }
                        }

                        $message = Amazon_Tools::l('Product became out of stock');
                        AmazonFBAStock::$log[$SKU][$this->params->id_country]['fba_status'] =  1;

                    } else { // FBA - In Stock

                        if (!isset($options['fba']) || !$options['fba']) { // offer option don't have fba set to true

                            $offers_options['fba'] = 1;
                            $updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";

                            $offers_options['fba_inventory'] = $quantityConsidered;
                            $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where); // Update table fba_data
                            $updateFlagUpdate .= ImportLog::update_flag_offers($this->params->id_marketplace, $ext, $this->params->id_shop, $id_product, 1,false);
                            
                            $flag_fba = 1;

                        } else {

                            if($totalSupplyQuantity > 0){ // when have inventory and have FBA stock

                                $offers_options['fba_inventory'] = $quantityConsidered;
                                $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where);	// Update table fba_data
                                $updateFlagUpdate .= ImportLog::update_flag_offers($this->params->id_marketplace, $ext, $this->params->id_shop, $id_product, 1,false);

                                //$flag_fba = 1;
                            }
                        }

                        $message = Amazon_Tools::l('Product in stock (FBA)');
                        AmazonFBAStock::$log[$SKU][$this->params->id_country]['fba_status'] =  2;
                    }

                }

                $product_quantity = $product['quantity'];
                $product_quantity_fba = $quantityConsidered;

                if ($product_quantity < 0) {
                    $product_quantity = 0;
                }

                if ($product_quantity > $product_quantity_fba) {
                    $delta = ($product_quantity - $product_quantity_fba) * -1;
                } else {
                    $delta = $product_quantity_fba - $product_quantity;
                }

		if ($delta == 0) {
		    
		    $this->no_error++;
		    AmazonFBAStock::$log[$SKU][$this->params->id_country]['message'] = sprintf('%s - %s (%d)', $message, Amazon_Tools::l('Stock already up to date'), $product_quantity);

		} else {

                    ///// have stock movement /////
                    $flag_fba_stock = 1; // set flag update stock

                    //if($totalSupplyQuantity > 0){

                    //}

                    //if($flag_fba){

                        $this->no_success++;

                        // flag update reprice
                        $flagUpdateReprice[$SKU] = array(
                            'SKU' => $SKU,
                            'id_product' => $id_product,
                            'id_product_attribute' => $id_product_attribute,
                        );

                        // update quantity to products_product and offers_product
                        if(isset($id_product_attribute) && $id_product_attribute > 0){

                            // Update product combination offers : qty.
                            $update_offers = array(
                                'quantity' => $product_quantity_fba,
                            );
                            $where_offers = array(
                                'id_product' => array('operation' => '=', 'value' => (int) $id_product),
                                'id_product_attribute' => array('operation' => '=', 'value' => (int) $id_product_attribute),
                                'id_shop' => array('operation' => '=', 'value' => (int)$this->params->id_shop),
                            );

                            $updateProduct .= $this->db->update_string(AmazonDatabase::$o_pref.'product_attribute', $update_offers, $where_offers);
                            $updateProduct .= $this->db->update_string(AmazonDatabase::$p_pref.'product_attribute', $update_offers, $where_offers);

                        } else {

                            // Update offers : qty.
                            $update_offers = array(
                                'quantity' => $product_quantity_fba,
                            );
                            $where_offers = array(
                               'id_product' => array('operation' => '=', 'value' => (int) $id_product),
                               'id_shop' => array('operation' => '=', 'value' => (int)$this->params->id_shop),
                           );

                            $updateProduct .= $this->db->update_string(AmazonDatabase::$o_pref.'product', $update_offers, $where_offers);
                            $updateProduct .= $this->db->update_string(AmazonDatabase::$p_pref.'product', $update_offers, $where_offers);

                        }
                    //}

		}

		foreach($this->exts as $ext => $id_country){
                    
                    $UpdateFBAFlag = $this->UpdateFBAtoFlag(
                        $id_product,
                        $id_product_attribute,
                        $SKU,
                        $product_quantity_fba,
                        $id_country,
                        $product_quantity,
                        $delta,
                        $flag_fba,
                        $flag_fba_stock
                    );

                    if(!$this->debug){
                        $this->db->exec_query($UpdateFBAFlag);
                    } else {
                        echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                        echo 'SQL : <pre>' . $UpdateFBAFlag . '</pre>';
                    }

                    // Flag FBA to Update FBA to shop
                    /*if(isset($flag_fba) && $flag_fba) {
                        $updateProductOptions .= $this->UpdateFBAtoFlag($id_product, $id_product_attribute, $SKU, $product_quantity_fba, $id_country, $product_quantity, $delta); 
                    } 
                    // save missing offer options
                    if(!empty($missing_offers_options)) {
                        $missing_offers_options['id_country'] = $id_country ;
                        $saveProductOptions .= $AmazonProductOption->save_offers_options($missing_offers_options, true);
                    }*/
                }

                // Update stock movement to shop
                if($flag_fba_stock){
                    $update_product = array(
                        'id_product'  => $id_product,
                        'id_product_attribute'=> $id_product_attribute,
                        'quantity_fba' => $product_quantity_fba,
                        'quantity' => $product_quantity,
                        'sku' => $SKU,
                    );
                    $this->stock_movement_per_line($update_product, $stockmovement_url);
                }
            }
            //get all sql for update flag offers
            $updateFlagUpdate .= ImportLog::get_all_flag_offers();
            
	    // execute save new Product Option
            if(strlen($saveProductOptions) > 10)
	    {
                $save_query = "REPLACE INTO amazon_product_option (id_product,id_product_attribute,sku,id_shop,id_country,date_add) VALUES " . ltrim($saveProductOptions, ',');

                if (!$this->debug){
                    $this->db->exec_query($save_query, true, true, true);
                } else {
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'SQL : <pre>' . $save_query . '</pre>';
                }
            }

	    // execute update Product Options
	    if(!empty($updateProductOptions) && strlen($updateProductOptions) > 10) {
		if (!$this->debug) {
		    if(!$this->db->exec_query($updateProductOptions, true, true, true)) {
			AmazonFBAStock::$warnings[] = sprintf('%s', Amazon_Tools::l('Unable to update Offers Options.'));
		    } 
		} else {
		    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'Update Option SQL : <pre>' . $updateProductOptions . '</pre>';
		}
	    }

            // update qty
            if(!empty($updateProduct) && strlen($updateProduct) > 10 ) {
                if (!$this->debug) {
                    $this->db->exec_query($updateProduct, true, true, true);
                } else {
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                    echo 'Update Product Qty SQL : <pre>' . $updateProduct . '</pre>';
                }
            }

            // update Flag Update
            if(!empty($updateFlagUpdate) && count($updateFlagUpdate)) {
                if (!$this->debug) {
                    $this->db->exec_query($updateFlagUpdate, true, true, true);
                } else {
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                    echo 'Update Flag Update SQL : <pre>' . $updateFlagUpdate . '</pre>';
                }
            }

            // flag update reprice
            if(!empty($flagUpdateReprice) && count($flagUpdateReprice)) {
                if (!$this->debug) {
                    foreach($this->exts as $ext => $id_country) {
                        $flagUpdateRepriceList = $this->db->update_amazon_repricing_flag($this->params->id_shop, $id_country, $flagUpdateReprice, 1);
                        $this->db->exec_query($flagUpdateRepriceList);
                    }
                } else {
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                    echo 'Update Flag Update SQL : ';
                    foreach($this->exts as $ext => $id_country) {
                        $flagUpdateRepriceList = $this->db->update_amazon_repricing_flag($this->params->id_shop, $id_country, $flagUpdateReprice, 1);
                         echo '<pre>' . $flagUpdateRepriceList . '</pre><br/>';
                    }
                }
            }

	    if(isset($this->proc_rep)) {
		$this->message->inventory->status = 'Success';
		$this->message->inventory->message = Amazon_Tools::l("Success");
		$this->proc_rep->set_status_msg($this->message);
	    }

        } else {

	    // empty Inventory
	    if(isset($this->proc_rep)) {
		$this->message->inventory->status = 'Success';
		$this->message->inventory->no_product = Amazon_Tools::l("No_inventory");
		$this->proc_rep->set_status_msg($this->message);
	    }
	}
	
	// update stok to shop
	// $this->stockMovement();

	// save FBA Log
	$this->FBALogs();

	// save validate log
	$this->ValidationError();

	// Update FBA to Shop
	$this->UpdateFBAtoShop();
    }
    
    public function ManageStocks($forceUpdate=false, $anticipate=false, $days=1, $result=null)
    {   
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->inventory))
		$this->message->inventory = new stdClass ();
	    
	    $this->message->inventory->message = Amazon_Tools::l('Listting Inventory') . "..";
	    $this->proc_rep->set_status_msg($this->message);
	}
	    
	$updateProductOptions = $updateFbaData = $saveProductOptions = '';

        if(!isset($result) || empty($result)){
            $result = $this->api->ListInventoryByDate(date('Y-m-d', time() - (86400 * $days)));
        }
        
	if ($this->debug){	    
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo '<pre>DATE : '.date('Y-m-d', time() - (86400 * $days)).'</pre>' ;
	    echo 'ListInventoryByDate : <pre>' . print_r($result, true) . '</pre>';
	}

	$list_sku = array();
	foreach ($result as $res){
	    $list_sku[] = $res['SKU'];
	}
	
	if ($this->debug){	    
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'list_sku : <pre>' . print_r($list_sku, true) . '</pre>';
	}
	
        if ($result && is_array($result) && count($result)) {
	    
	    $this->no_process = $this->no_success = $this->no_error = $this->skipped = 0;	    
	    
	    // get products
	    $products = $this->db->getProductBySKU($this->params->id_shop, $list_sku, true, true);	    	   
	    if ($this->debug){	    
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'products : <pre>' . print_r($products, true) . '</pre>';
	    }
          
            // get products options for all ext in region
            $AmazonProductOption = $ProductOptions = array();
            foreach ($this->exts as $ext => $id_country){
                $AmazonProductOption[$ext] = new MarketplaceProductOption($this->params->user_name,null,null,null,null, $id_country, $this->params->id_marketplace);
                $ProductOptions[$ext] = $AmazonProductOption[$ext]->get_product_options($this->params->id_shop, null, $list_sku);
            }
	    if ($this->debug){	    
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'Product Options : <pre>' . print_r($ProductOptions, true) . '</pre>';
	    }
	    
	    // get fba_data
	    // $FbaData = $this->db->get_fba_data($this->params->id_shop, $this->params->id_country);
	    $flagUpdateReprice = $missing_offers_options = array();
            foreach ($result as $SKU => $item) {
		
		// Flag FBA = null
		$flag_fba = null;
		$SKU = (string)$SKU;
		
		$this->no_process++;
		
                if (!isset($products[$SKU]) || empty($products[$SKU])) {
                    AmazonFBAStock::$warnings[] = sprintf('%s - (%s)', Amazon_Tools::l('Unable to find product'), $SKU);
		    $this->skipped++;
                    continue;
                }		

                // 1 find product data
                $product = $products[$SKU];
		$id_product = (int) $product['id_product'];
		$id_product_attribute = (int) $product['id_product_attribute'];

		if ($this->debug){
		    echo '---------------------------------------------------------';
		    echo '<pre>Id Product : '.$id_product.', ' ;
		    echo 'Id Product Attribute : '.$id_product_attribute.'</pre>' ;
		}

                 // 2 find Quantity Considered
                if ($anticipate && array_key_exists('TotalSupplyQuantity', $item) && $item['TotalSupplyQuantity'] > $item['InStockSupplyQuantity']) {
                    $quantityConsidered = $item['TotalSupplyQuantity'];
                } else {
                    $quantityConsidered = $item['InStockSupplyQuantity'];
                }

		if ($this->debug){
		    echo 'Item : <pre>' . print_r($item, true) . '</pre>';
		    echo '<b>Quantity Considered : ' . print_r($quantityConsidered, true) . '</b><br/>';
		}
                
                // 3 consider each country
                foreach ($this->exts as $ext => $id_country){
                    if (!isset($ProductOptions[$ext][$id_product][$id_product_attribute]) || empty($ProductOptions[$ext][$id_product][$id_product_attribute])) { // dont have option
                        $missing_offers_options[$ext] = array(
                                'id_product'            =>  $id_product,
                                'id_product_attribute'  =>  $id_product_attribute,
                                'sku'                   =>  $SKU,
                                'id_shop'		=>  (int)$this->params->id_shop,
                                'id_country'		=>  (int)$id_country,
                                'date_add'		=>  $this->time,
                        );
                        $saveProductOptions .= $AmazonProductOption[$ext]->save_offers_options($missing_offers_options[$ext], true);
                    } else {
                        $options = $ProductOptions[$ext][$id_product][$id_product_attribute] ;
                    }

                    $where = array(
                        'id_product'            =>  array('value' => $id_product, 'operation' => '='),
                        'id_product_attribute'  =>  array('value' => $id_product_attribute, 'operation' => '='),
                        'sku'                   =>  array('value' => $SKU, 'operation' => '='),
                        'id_shop'		=>  array('value' => (int)$this->params->id_shop, 'operation' => '='),
                        'id_country'		=>  array('value' => (int)$id_country, 'operation' => '='),
                    );
		
                    $offers_options = array(
                        'id_product'            =>  $id_product,
                        'id_product_attribute'  =>  $id_product_attribute,
                        'sku'                   =>  $SKU,
                        'id_shop'               =>  (int)$this->params->id_shop,
                        'id_country'            =>  (int)$id_country,
                        'date_upd'              =>  $this->time,
                    );

                    if ($quantityConsidered == 0) { // Became out of stock

                        // Product is set as FBA in offer option
                        if ( (isset($options['fba']) && $options['fba'] == 1) /*|| $forceUpdate*/) { // offer option have fba

                            $offers_options['fba'] = 0;
                            $updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";
                            $offers_options['fba_inventory'] = $quantityConsidered;
                            $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where); // Update table fba_data

                            // Log the event
                            AmazonFBAStock::$log[$SKU][$id_country]['fba_status'] = '1';
                            $this->no_error++;

                            if ($this->debug){
                                echo "[$ext] <b>Product became out of stock, switching to MFN (option)</b><br/>";
                            }

                            // set Flag FBA = 0
                            $flag_fba = $offers_options['fba'];
                            if ($this->debug){
                                echo "[$ext] FBA Flag to Update Shop - <b>$flag_fba</b><br/>";
                            }

                        } else {  // offer option dont has fba
                            if ($this->debug){
                                echo "[$ext] <b>Product became out of stock, offer option dont has fba, nothing todo</b><br/>";
                            }
                        }

                    } else { // FBA - In Stock

                        // Product is not set as FBA, but Amazon have it in stock
                        if ((!isset($options['fba']) || !$options['fba'] /*|| $forceUpdate*/)) { // no fba, have value

                            $offers_options['fba'] = 1; // Turns Product to AFN for all targets marketplaces
                            $updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";
                            $offers_options['fba_inventory'] = $quantityConsidered;
                            $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where); // Update table fba_data

                            // Log the event
                            AmazonFBAStock::$log[$SKU][$id_country]['fba_status'] = '2';
                            $this->no_success++;

                            if ($this->debug){
                                echo "[$ext] no fba, /*have value*/ <b>Product in stock (FBA), switching to AFN</b><br/>";
                            }

                            // set Flag FBA = 1
                            $flag_fba = $offers_options['fba'];
                            if ($this->debug){
                                echo "[$ext] FBA Flag to Update Shop - <b>$flag_fba</b><br/>";
                            }

                        } else { //have fba, have value

                            // Update table fba_data
                            $offers_options['fba'] = 1; // Turns Product to AFN for all targets marketplaces
                            //$updateProductOptions .= $AmazonProductOption[$ext]->update_offers_options($offers_options, $where) . " ";
                            $offers_options['fba_inventory'] = $quantityConsidered;
                            $updateProductOptions .= $this->db->update_string('amazon_fba_data', $offers_options, $where);

                            // Log the event
                            AmazonFBAStock::$log[$SKU][$id_country]['fba_status'] = '4';

                            if ($this->debug){
                                echo "[$ext] have fba, have value - <b>Update fba_data to 1</b><br/>";
                                echo "[$ext] FBA Flag to Update Shop - <b>$flag_fba</b><br/>";
                            }
                        }
                    }
                
                    if(isset($flag_fba)) {
                        // flag update reprice
                        $flagUpdateReprice[$SKU] = array(
                            'SKU' => $SKU,
                            'id_product' => $id_product,
                            'id_product_attribute' => $id_product_attribute,
                        );
                    
                        // Flag FBA to Update FBA to shop
                        $fba_inventory = isset($offers_options['fba_inventory']) ? $offers_options['fba_inventory'] : null;
                        $updateProductOptions .= $this->UpdateFBAtoFlag($id_product, $id_product_attribute, $SKU, $fba_inventory, (int)$id_country);
                        $updateProductOptions .= ImportLog::update_flag_offers($this->params->id_marketplace, $ext, $this->params->id_shop, $id_product, 1,false);
                    }
                }
                $updateProductOptions .= ImportLog::get_all_flag_offers(); 
		unset($offers_options);
		unset($options);
            }
	    
	    if ($this->debug){	    
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'log : <pre>' . print_r(AmazonFBAStock::$log, true) . '</pre>';
	    }
            
	    // execute save new Product Option
            if(strlen($saveProductOptions) > 10)
	    {
                $save_query = "REPLACE INTO amazon_product_option (id_product,id_product_attribute,sku,id_shop,id_country,date_add) VALUES " . ltrim($saveProductOptions, ',');

                if (!$this->debug){
                    $this->db->exec_query($save_query, true, true, true);
                } else {
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'SQL : <pre>' . $save_query . '</pre>';
                }
            }
            
	    // execute update Product Options
	    if(strlen($updateProductOptions) > 10)
	    {
		if (!$this->debug){		    
		    if($update_exec = $this->db->exec_query($updateProductOptions, true, true, true)) {
			
			// flag update reprice
			if(!empty($flagUpdateReprice) && count($flagUpdateReprice) && (isset($this->repricing) && $this->repricing)){
                            foreach($this->exts as $ext => $id_country) {
                                $flagUpdateRepriceList = $this->db->update_amazon_repricing_flag($this->params->id_shop, $id_country, $flagUpdateReprice, 1);
                                $this->db->exec_query($flagUpdateRepriceList);
                            }
			}			
			$this->FBALogs();
		    }

		} else {

                    // flag update reprice
                    if(isset($this->repricing) && $this->repricing) {
                        foreach($this->exts as $ext => $id_country) {
                            $flagUpdateRepriceList = $this->db->update_amazon_repricing_flag($this->params->id_shop, $id_country, $flagUpdateReprice, 1);
                            echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                            echo 'SQL ('.$ext.'/'.$id_country.'): <pre>' . $flagUpdateRepriceList . '</pre>';
                        }
                    }                    
		    $this->FBALogs();		    
		    
		    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'SQL : <pre>' . $updateProductOptions . '</pre>';
		}
	    }

	    if(isset($this->proc_rep)) {
		$this->message->inventory->status = 'Success';
		$this->message->inventory->message = Amazon_Tools::l("Success");
		$this->proc_rep->set_status_msg($this->message);
	    }
	    
        } else {
	    
	    // empty Inventory
	    if(isset($this->proc_rep)) {
		$this->message->inventory->status = 'Error';
		$this->message->inventory->no_product = Amazon_Tools::l("No_inventory");
		$this->proc_rep->set_status_msg($this->message);
	    }
	}
	
	// save validate log
	$this->ValidationError();
	
	// Update FBA to Shop
	$this->UpdateFBAtoShop();
    }

    private function stock_movement_per_line($product, $stockmovement_url){

	if($stockmovement_url && isset($this->stock_movement)) {
            
            // Determine Software type and prepare URL
            $conj = strpos($stockmovement_url, '?') !== FALSE ? '&' : '?';
            $preparedURL = $stockmovement_url . $conj . sprintf('fbtoken=%s&fbproduct=%s&fbcombination=%s&fbstock_fba=%s&fbstock=%s&fbsku=%s', $this->params->user_code, (int) $product['id_product'], (int) $product['id_product_attribute'], (int) $product['quantity_fba'], (int) $product['quantity'], $product['sku']);

            if(!$this->debug){

                if(function_exists('fbiz_get_contents')){
                    $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                }else{
                    $return = @file_get_contents($preparedURL);
                }

                if(isset($return) && $return) {

                    $return_dom = new SimpleXMLElement($return);

                    $error_code = isset($return_dom->Status->Code) ? (int)$return_dom->Status->Code : '-1' ;

                    if(isset($return_dom->Status->Code) && $return_dom->Status->Code == 1)
                    {
                        if(isset($return_dom->StockFlag->Item))
                        {
                            foreach ($return_dom->StockFlag->Item as $items)
                            {
                                    $flag = isset($items['Flagged']) && $items['Flagged'] ? 0 : 1 ;
                                    $id_product = (int)$items['ProductId'];
                                    $id_product_attribute = (int)$items['ProductAttributeId'];
                                    $sku = strval($items['Reference']);
                                    $stock_fba = strval($items['StockFBA']);
                                    $stock = strval($items['Stock']);

                                    // set flagged
                                    $update_flag = array(
                                        'flag_stock'            => $flag,
                                        'date_upd'              => $this->time,
                                    );

                                    $where_flag = array(
                                        'id_product'		=> array('value' => $id_product, 'operation' => '='),
                                        'id_product_attribute'	=> array('value' => $id_product_attribute, 'operation' => '='),
                                        'sku'			=> array('value' => $sku, 'operation' => '='),
                                        'id_shop'               => array('value' => (int)$this->params->id_shop, 'operation' => '='),
                                        'process'               => array('value' => AmazonFBAStock::PROCESS_KEY, 'operation' => '='),
                                        'type'			=> array('value' => $this->process_type, 'operation' => '='),
                                    );

                                    $updatedStock = $this->db->update_string('amazon_fba_flag', $update_flag, $where_flag);
                                    $this->db->exec_query($updatedStock);

                                    if (isset($stock_fba) && $stock_fba == 0) {
                                        $message = Amazon_Tools::l('Product became out of stock');

                                    } else if(isset($stock_fba) && $stock_fba <> 0) {
                                        $message = Amazon_Tools::l('Product in stock (FBA)');
                                    }

                                    if($flag) {
                                        AmazonFBAStock::$log[$sku][$this->params->id_country]['message'] =
                                            sprintf('%s - %s (%d)', $message, Amazon_Tools::l('Stock Updated'), $stock_fba);
                                        // add log stock movement
                                        $data_offer = $sku;
                                        $data_offer['quantity'] = $stock_fba;
                                        $marketplace = strtolower(AmazonFBAStock::$tag);
                                        $this->stock_movement->stockMovementProcess(
                                            $data_offer,
                                            $this->params->id_shop,
                                            $this->time,
                                            str_replace(" " , "_", strtolower(self::PROCESS)),
                                            $marketplace
                                        );
                                    } else {
                                        AmazonFBAStock::$log[$sku][$this->params->id_country]['message'] =
                                            sprintf('%s(%s) - error code : %s', Amazon_Tools::l('Stock Update FAILED.'), $return_dom->Status->Code, $sku);
                                    }

                                    // keep log
                                    $data_insert = array(
                                        'id_shop' => (int)$this->params->id_shop,
                                        'id_marketplace' => _ID_MARKETPLACE_AMZ_,
                                        'site' => $this->params->id_country,
                                        'error_code' => $error_code,
                                        'id_product' => $id_product,
                                        'id_product_attribute' => $id_product_attribute,
                                        'message' => $message,
                                        'quantity' => (($stock - $stock_fba) * -1),
                                        'action_type' => self::PROCESS . ' - ' . $this->operation_type,
                                        'date_add' => date('Y-m-d H:i:s')
                                    );

                                    $offers_stock_mvt_logs = $this->db->insert_string('offers_stock_mvt_logs', $data_insert, true);
                                    $this->db->exec_query($offers_stock_mvt_logs);
                            }
                        }
                    }
                }
                
            } else {
                echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                echo 'URL : <pre>' . $preparedURL . '</pre>';
            }
            
        }

    }

    private function stockMovement() {

	$updatedStock = $offers_stock_mvt_logs = '';

	// 1. get shop update stock url
	$stockmovement_url = $this->checkUrl('stockmovement_fba');

	if($stockmovement_url) {

             $stock_movement = new StockMovement($this->params->user_name);

            // Determine Software type and prepare URL :/modules/feedbiz/functions/stockmovement_fba.php?fbtoken=xx&fbshop=x&fbsite=x
            $conj = strpos($stockmovement_url, '?') !== FALSE ? $stockmovement_url.'&' : $stockmovement_url.'?';
            $preparedURL = $conj.sprintf('fbtoken=%s&fbshop=%s&fbsite=%s', $this->params->user_code, $this->params->id_shop, $this->params->id_country);

            if (!$this->debug){
                if(function_exists('fbiz_get_contents')){
                    //fbiz_get_contents($preparedUrl,array('curl_method'=>1,'timeout'=>3600));
                    $return = fbiz_get_contents($preparedURL, array('curl_method'=>1,'timeout'=>(3600*4)));
                }else{
                    $return = @file_get_contents($preparedURL);
                }
            } else {
                echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                echo 'URL : <pre>' . $preparedURL . '</pre>';
            }

            if(isset($return) && $return) {

                $return_dom = new SimpleXMLElement($return);

                $error_code = isset($return_dom->Status->Code) ? (int)$return_dom->Status->Code : '-1' ;
                
                if(isset($return_dom->Status->Code) && $return_dom->Status->Code == 1)
		{
                    if(isset($return_dom->StockFlag))
		    {
			foreach ($return_dom->StockFlag as $stock_items)
			{
                            foreach ($stock_items as $items)
                            {
                                $flag = isset($items['Flagged']) && $items['Flagged'] ? 0 : 1 ;
                                $id_product = (int)$items['ProductId'];
                                $id_product_attribute = (int)$items['ProductAttributeId'];
                                $sku = strval($items['Reference']);
                                $stock_fba = strval($items['StockFBA']);
                                $stock = strval($items['Stock']);

                                // set flagged
                                $update_flag = array(
                                    'flag_stock'            => $flag,
                                    'date_upd'		=> $this->time,
                                );

                                $where_flag = array(
                                    'id_product'		=> array('value' => $id_product, 'operation' => '='),
                                    'id_product_attribute'	=> array('value' => $id_product_attribute, 'operation' => '='),
                                    'sku'			=> array('value' => $sku, 'operation' => '='),
                                    'id_shop'		=> array('value' => (int)$this->params->id_shop, 'operation' => '='),
                                    'process'		=> array('value' => AmazonFBAStock::PROCESS_KEY, 'operation' => '='),
                                    'type'			=> array('value' => $this->process_type, 'operation' => '='),
                                );

                                $updatedStock .= $this->db->update_string('amazon_fba_flag', $update_flag, $where_flag);

                                if (isset($stock_fba) && $stock_fba == 0) {
                                    $message = Amazon_Tools::l('Product became out of stock');

                                } else if(isset($stock_fba) && $stock_fba <> 0) {
                                    $message = Amazon_Tools::l('Product in stock (FBA)');
                                }

                                if($flag) {
                                    AmazonFBAStock::$log[$sku][$this->params->id_country]['message'] = sprintf('%s - %s (%d)', $message, Amazon_Tools::l('Stock Updated'), $stock_fba);
                                    // add log stock movement
                                    $data_offer = $sku;
                                    $data_offer['quantity'] = $stock_fba;
                                    $marketplace = strtolower(AmazonFBAStock::$tag);
                                    $stock_movement->stockMovementProcess($data_offer, $this->params->id_shop, $this->time, str_replace(" " , "_", strtolower(self::PROCESS)), $marketplace);
                                } else {
                                    AmazonFBAStock::$log[$sku][$this->params->id_country]['message'] = sprintf('%s(%s) - error code : %s', Amazon_Tools::l('Stock Update FAILED.'), $return_dom->Status->Code, $sku);
                                }

                                // keep log
                                $data_insert = array(
                                    'id_shop' => (int)$this->params->id_shop,
                                    'id_marketplace' => _ID_MARKETPLACE_AMZ_,
                                    'site' => $this->params->id_country,
                                    'error_code' => $error_code,
                                    'id_product' => $id_product,
                                    'id_product_attribute' => $id_product_attribute,
                                    'message' => $message,
                                    'quantity' => (($stock - $stock_fba) * -1),
                                    'action_type' => self::PROCESS . ' - ' . $this->operation_type,
                                    'date_add' => date('Y-m-d H:i:s')
                                );

                                $offers_stock_mvt_logs .= $this->db->insert_string('offers_stock_mvt_logs', $data_insert, true);
                            }
                        }
                    }
                }                
	    }
	}

	if (!$this->debug){
	    if(strlen($updatedStock) > 10) {
		if(!$this->db->exec_query($updatedStock, true, true, true)) {
		    AmazonFBAStock::$warnings[] = sprintf('%s', Amazon_Tools::l('Unable to update fba flag.'));
		} else {
                    if(strlen($offers_stock_mvt_logs) > 10) {
                        $this->db->exec_query($offers_stock_mvt_logs, true, true, true);
                    }
                }
	    }
	} else {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'SQL : <pre>' . $updatedStock . '</pre>';
	}

    }
    
    private function AmazonDatabase() {
        
        if (!$this->db = new AmazonDatabase($this->params->user_name, $this->debug)) {
            $this->error = Amazon_Tools::l('Unable_to_login.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->StopFeed();
        }
    }

    private function AmazonApi() {
        
        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
            $auth['auth_token'] = trim($this->params->auth_token);
        }        
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
        if (!$this->api = new Amazon_WebService($auth, $marketPlace, null, $this->debug)) {
            $this->error = Amazon_Tools::l('Unable_to_connect_Amazon.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)){
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->StopFeed();
        }
    }

    private function StartProcessMessage() {
        
        $process_running = $this->operation_type;
        $process_title = $this->process;
        $process_type = 'fba_manager_' . $this->operation_type /*. '_' . $this->params->countries*/;
        $marketplace = strtolower(AmazonFBAStock::$tag);
       
	$this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, false);
	$this->proc_rep->set_process_type($process_type);
	$this->proc_rep->set_popup_display(false);
	$this->proc_rep->set_marketplace($marketplace);
	    
        $process = $this->proc_rep->has_other_running($process_type);
	
        if ($process) {
            $this->message->error =sprintf(Amazon_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'),$process_running,'20');
            $this->proc_rep->set_error_msg($this->message);
            $this->proc_rep->finish_task();
            echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, ' . $this->operation_type . ' until old process done'));
            die();            
        }       
    }

    private function StopFeed() {
        
        $error = null;
	
        if (isset($this->error) && !empty($this->error)){
            $error = $this->error;
            $this->log(array('type' => $this->operation_type,'error' => $error, 'date_add' => date('Y-m-d H:i:s')));

	    if ($this->debug){	   
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'Error : <pre>' . $this->error . '</pre>';
	    }
	    
        } else {
            $this->log(array('type' => $this->operation_type,'date_add' => date('Y-m-d H:i:s')));    
        }
        
        if (isset($this->proc_rep)) 
            $this->proc_rep->finish_task();
	
	$json = json_encode( 
		array(  
		    'status' => isset($error) ? 'error' : 'success', 
		    'output' => 'FBA Manager '.$this->operation_type.' for user : ' . $this->params->user_name . ', country : ' . $this->params->ext) 
		) ;
        echo $json ;

        exit;        
    }
    
    private function log($detail = null){
        
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => self::PROCESS,
            "no_send"       => isset($this->productIndex) ? (int)$this->productIndex : 0,
            "no_skipped"    => isset($this->skipped) ? (int)$this->skipped : 0, 
            "no_process"    => isset($this->no_process) ? (int)$this->no_process : 0,
            "no_success"    => isset($this->no_success) ? (int)$this->no_success : 0,
            "no_error"      => isset($this->no_error) ? (int)$this->no_error : 0,
            "no_warning"    => isset($this->no_warning) ? (int)$this->no_warning : 0,                
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
        if(isset($detail) && !empty($detail))
             $log['detail'] = base64_encode(serialize($detail));
        
        if(!$this->debug){
            $this->db->update_log($log);
	} else {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'LOGS : <pre>' . print_r($log, true) . '</pre>';
	}
    }
    
    private function ValidationError() {
	
	$sql = '';	
	
	if (count(AmazonFBAStock::$warnings)) {
	    
	    $errors = array();
	    $errors['warning'] = AmazonFBAStock::$warnings;
	    
	    foreach ($errors as $type => $error)
	    {		
		if(!empty($error)) 
		{
		    foreach ($error as $messages)
		    {
			$error_data = array(
			    'batch_id' => $this->batchID,
			    'id_country' => $this->params->id_country,
			    'id_shop' => $this->params->id_shop,
			    'action_process' => $this->process,
			    'action_type' => $type,
			    'message' => strval($messages),
			    'date_add' => date("Y-m-d H:i:s"),
			);            

			$sql .= $this->db->save_validation_log($error_data, false);
			unset($error_data);
		    }
		}
	    }
	    
	    unset($errors);
	    
	    if(strlen($sql) > 10)
	    {
		if (!$this->debug){
		    $this->db->exec_query($sql);
		} else {
		    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'SQL : <pre>' . $sql . '</pre>';
		}
	    }
	}
	
	unset($sql);
    }
    
    private function UpdateFBAtoFlag($id_product, $id_product_attribute, $SKU, $quantity_fba, $id_country=null, $quantity=null, $delta=null, $flag=1, $flag_stock=null){
	
	$fba_flag = array(
	    'id_product'		=> $id_product,
	    'id_product_attribute'	=> $id_product_attribute,
	    'sku'			=> $SKU,
	    'id_shop'			=> (int)$this->params->id_shop, 
	    'id_country'		=> isset($id_country) && !empty($id_country) ? (int)$id_country : (int)$this->params->id_country,
	    'process'			=> AmazonFBAStock::PROCESS_KEY,
	    'type'			=> $this->process_type, //AmazonFBAStock::FBA_STOCK_SWITCH,
	    'quantity_fba'              => $quantity_fba,
            'quantity'                  => $quantity,
            'delta'                     => $delta,
	    'flag'			=> $flag,
	    'flag_stock'		=> $flag_stock, //($this->process_type == AmazonFBAStock::FBA_STOCK_SYNCH) ? 1 : null,
	    'date_upd'			=> $this->time,
	);

	return $this->db->replace_string('amazon_fba_flag', $fba_flag);
    }
    
    private function UpdateFBAtoShop() {
		
	$updateFlags = '';
	
	// 1. get shop update options url	    
	$update_option_url = $this->checkUrl('update_options'); 
	
	if($update_option_url)
	{
	    $conj = strpos($update_option_url, '?') !== FALSE ? $update_option_url.'&' : $update_option_url.'?';
	    $preparedURL = $conj.sprintf('fbtoken=%s&fbmarketplace=%s&fbshop=%s&fbsite=%s', 
		    $this->params->user_code, 
		    $this->params->id_marketplace, 
		    $this->params->id_shop, 
		    $this->params->id_country);
	    
	    if ($this->debug){	
		
		echo "UpdateFBAtoShop - <b>URL : $preparedURL</b><br/>";
		$return = false;
		
	    } else {
		
		// 2. notice Shop to get options update, Shop will get Offer Option from webservices, then return value to update
//		$return = file_get_contents($preparedURL);
                if(function_exists('fbiz_get_contents')){
                    $return = fbiz_get_contents($preparedURL, array('curl_method'=>1));
                }else{
                    $return = @file_get_contents($preparedURL);
                }
	    }

	    if($return) {

		$return_dom = new SimpleXMLElement($return);

		if(isset($return_dom->Status->Code) && $return_dom->Status->Code == 1) 
		{
		    if(isset($return_dom->OffersOptions))
		    {
			foreach ($return_dom->OffersOptions as $OffersOptions)
			{
			    foreach ($OffersOptions as $Offers) 
			    {
				// 3. recieved status from Shop
				$flag = isset($Offers['Flagged']) && $Offers['Flagged'] ? 0 : 1 ; // if flagged set flag in amazon_fba flag to zero to avoid duplicate send options
				$id_product = (int)$Offers['ProductId'];
				$id_product_attribute = (int)$Offers['ProductAttributeId'];
				$sku = strval($Offers['Reference']);

				// 4. Log flag
				$update_flag = array(
				    'flag'			=> $flag,
				    'date_upd'			=> $this->time,
				);

				$where_flag = array(
				    'id_product'		=> array('value' => $id_product, 'operation' => '='),
				    'id_product_attribute'	=> array('value' => $id_product_attribute, 'operation' => '='),
				    'sku'			=> array('value' => $sku, 'operation' => '='),
				    'id_shop'			=> array('value' => (int)$this->params->id_shop, 'operation' => '='), 
				    //'id_country'		=> array('value' => (int)$this->params->id_country, 'operation' => '='),
				    'process'			=> array('value' => AmazonFBAStock::PROCESS_KEY, 'operation' => '='), 
				    'type'			=> array('value' => $this->process_type, 'operation' => '='),
				);

				$updateFlags .= $this->db->update_string('amazon_fba_flag', $update_flag, $where_flag);
			    }
			}
		    }
		}
	    }
	}
	
	if(strlen($updateFlags) > 10)
	{
	    if (!$this->debug){
		$this->db->exec_query($updateFlags, true, true, true);
	    } else {
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'SQL : <pre>' . $updateFlags . '</pre>';
	    }
	}
	
    }   
    
    private function FBALogs() {
	$sql = '';
	$table = AmazonDatabase::$m_pref . 'amazon_fba_log';
			
	foreach (AmazonFBAStock::$log as $sku => $messages)
	{		
	    foreach ($messages as $id_country => $message)
	    {
		$error_data = array(
		    'batch_id'      => $this->batchID,
		    'id_country'    => $id_country, //$this->params->id_country,
		    'id_shop'       => $this->params->id_shop,
		    'process'       => AmazonFBAStock::PROCESS_KEY,
		    'type'          => $this->process_type,
		    'sku'           => $sku,
		    'fba_status'    => isset($message['fba_status']) ? strval($message['fba_status']) : null,
		    'message'       => isset($message['message']) ? strval($message['message']) : null,
		    'date_add'      => date("Y-m-d H:i:s"),
		);            

		$sql .= $this->db->insert_string($table, $error_data);
		unset($error_data);
	    }
	}
	
	if(strlen($sql) > 10)
	{
	    if (!$this->debug){
		$this->db->exec_query($sql);
	    } else {
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'SQL : <pre>' . $sql . '</pre>';
	    }
	}
	
	unset($sql);
    }
    
    private function checkFeatureStatus($id_country=null){
	
	if(!isset($id_country) || empty($id_country))
	    $country = $this->params->id_country;
	else
	    $country = $id_country;
		    
	$features = $this->db->get_configuration_features($country, $this->params->id_shop); //$this->params->id_country

        if(isset($features['repricing']) && !empty($features['repricing'])){
            $this->repricing = (bool) $features['repricing'];
        }

	if(!isset($id_country) || empty($id_country)) {
	    if(!isset($features['fba']) || empty($features['fba']) || $features['fba'] = 0)
	    {
		$this->error = Amazon_Tools::l('inactivate_fba');
		$this->message->error = $this->error;
		$this->message->link = array('amazon', 'features', $this->params->id_country);
		$this->message->link_label = Amazon_Tools::l('Go to features');

		if (isset($this->proc_rep)) {
		    $this->proc_rep->set_error_msg($this->message);
		}
		$this->StopFeed();
	    }           
	} else {
	    if(!isset($features['fba']) || empty($features['fba']) || $features['fba'] = 0)
	    {
		return (false);
	    }	    
	    return (true);
	}
    }
    
    private function checkBehavior() {
	
	if(!isset($this->params->fba_stock_behaviour) || empty($this->params->fba_stock_behaviour) || !isset(AmazonFBAStock::$fba_stock_type[$this->params->fba_stock_behaviour]))
	{
	    $this->error = Amazon_Tools::l('No FBA Behaviour defined, please configure priorly: FBA > Parameters tab.');
            $this->message->error = $this->error;
	    $this->message->link = array('amazon', 'fba', 'parameters', $this->params->id_country);
	    $this->message->link_label = Amazon_Tools::l('Go to fba parameters');
	    
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
	    
	    $this->StopFeed();
	}
	
    }
    
    private function checkUrl($type='stockmovement_fba') {
	
	$shop = AmazonUserData::getShopInfo($this->params->id_customer);
	$error = false;
	$url = '';
	if($this->debug){
            echo '<pre>' .print_r($shop, true);
        }
	switch ($type){
	    
	    case 'stockmovement_fba' :
		if(!isset($shop['url_stockmovementfba']) || empty($shop['url_stockmovementfba'])){
		    $error = true;
		} else {
		    $url = $shop['url_stockmovementfba'];
		}
		break;
		
	    case 'update_options' :
		if(!isset($shop['url_update_options']) || empty($shop['url_update_options'])){
		    $error = true;
		} else {
		    $url = $shop['url_update_options'];
		}
		break;
	    
	}
	
	if($error){

	    $this->error = Amazon_Tools::l('The specified URL for your shop is incorrect. '
		    . 'Please try to copy from feedbiz module URL connection provider and paste to verify box  on My shop > Configuration tab.');
	    $this->message->error = $this->error;
	    $this->message->link = array('my_shop', 'configuration');
	    $this->message->link_label = Amazon_Tools::l('Go to My shop Configuration');

	    if (isset($this->proc_rep)) {
		$this->proc_rep->set_error_msg($this->message);
	    }
	    
	    return false;  //$this->StopFeed();
	}
	
	return $url;
	
    }
    
    private function getExtFBA() {
	
	$this->exts = AmazonUserData::getExtFBA($this->params->id_customer, $this->params->id_shop, isset($this->params->region) ? $this->params->region : null);
	
	foreach ($this->exts as $ext => $id_country){
	    
	    if(!$this->checkFeatureStatus($id_country)){
		unset($this->exts[$ext]);
	    }
	    
	}	
	
    }
}