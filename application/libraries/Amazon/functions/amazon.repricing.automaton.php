<?php

require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.repricing.php';
require_once dirname(__FILE__) . '/../classes/amazon.strategies.php';
require_once(dirname(__FILE__) . '/../../eucurrency.php');

class AmazonRepricingAutomaton extends AmazonRepricing {

    const SUBSCRIBE = 1;
    const CANCEL = 2;
    const CHECK = 3;
    const PURGE = 4;

    const PRICE = 'Price';
    const REPRICE = 'reprice';
    const EXPORT = 'export';
    const MAX_MAX_EXECUTION_TIME = 120;
    const SYNC = 'sync';
    const REPRICE_PRODUCT_CREATION = 'RepriceProductCreation';
    const MAX_SEND_QUEUE = 200;

    protected $verbose = false;
    protected $script_start_time = null;
    protected $max_execution_time = null;
    protected $amazon_id_lang;
    protected $specials;
    protected $useTax;
    protected $awsKeyId = null;
    protected $awsSecretKey = null;
    protected $UrlQueueIn = null;
    protected $UrlQueueOut = null;
    protected $amazonApi = null;
    protected $fbaFormula = null;
    protected $params = null;
    protected $database = null;
    protected $feedbiz = null;
    protected $is_exit_process = true;

    private $smarty;

    public $action;

    public function __construct($params, $debug = false, $cron = false) {

        $this->debug = $debug;
        $this->params = $this->setUserData($params);
        $this->cron = $cron;
        $this->message = new stdClass();

        if($this->debug)
            $this->verbose = true;

	if(!isset($this->params->language))
	    Amazon_Tools::load(_FEED_LANG_FILE_, $params->language);
	else
	    Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
    }

    public function Dispatch($action, $options=null, $proc_rep=null, $is_exit_process=true) {

        $this->initAmazonMarketplaceAPI();
        $this->action = $action;
        $this->is_exit_process = $is_exit_process;

        if(isset($proc_rep) && !empty($proc_rep))
            $this->proc_rep = $proc_rep;

        switch ($action) {
            case AmazonRepricingAutomaton::CHECK:
                $this->CheckQueues(); break;
            case AmazonRepricingAutomaton::PURGE:
                $this->PurgeQueues($options); break;
            case AmazonRepricingAutomaton::SUBSCRIBE:
                $this->CheckService(); break;
            case AmazonRepricingAutomaton::CANCEL:
                $this->CancelService(); break;
            case AmazonDatabase::REPRICING_ANALYSIS:
                $this->Reprice(); break;
            case AmazonDatabase::REPRICING_EXPORT;
                $this->Export(); break;
            case AmazonRepricingAutomaton::REPRICE_PRODUCT_CREATION;
                return $this->RepriceProductCreation($options);
                //break;
        }

    }

    public function RepriceProductCreation($products){

        $this->strategy = new AmazonStrategy($this->params->user_name, $this->debug);
        $this->strategies = $this->strategy->get_strategies($this->params->id_shop, $this->params->id_country);
        $this->products_strategy = $this->strategy->get_products_strategies($this->params->id_shop, $this->params->id_country, $products);
        $this->setAwsSettings();
        return $this->getCompetitivePrice($products);
    }

    public function Export() {

        require_once dirname(__FILE__) . '/amazon.feeds.php';

        $this->id_shop = $this->params->id_shop;
        $productsUpdate = $moduleDataMessages = $skuObjects = $skipped = array();

        $this->setAwsSettings();
        $this->MaxExecutionTime();

	$this->params->repricing_type = AmazonDatabase::REPRICING_EXPORT;

        $amazon_feeds = new AmazonFeeds($this->params, AmazonDatabase::REPRICING, $this->debug, $this->cron);
        $batchID = isset($amazon_feeds->batchID) ? $amazon_feeds->batchID : uniqid();
        $operation_type = isset($amazon_feeds->operation_type) ? $amazon_feeds->operation_type : AmazonDatabase::REPRICING;

        $message_set = $this->RetrieveMessages($this->awsKeyId, $this->awsSecretKey, $this->UrlQueueOut, $this->script_start_time, $this->max_execution_time, $this->verbose);

        if (is_array($message_set) && count($message_set)) {

            if(isset($amazon_feeds->proc_rep)){
                $amazon_feeds->message->product->message = Amazon_Tools::l('Retrieve Messages Successfully, Messages Sets : ') . count($message_set) . ", Start Export Price..";
                $amazon_feeds->proc_rep->set_status_msg($amazon_feeds->message);
            }

            if ($this->verbose) {
                echo "<pre>\n";
                echo "Messages Sets:" . count($message_set);
                echo "</pre>\n";
            }

            foreach ($message_set as $messages) {

                // First Loop - Checking Notifications
                foreach ($messages as $message) {

                    if (!array_key_exists('ReceiptHandle', $message) || !array_key_exists('MessageId', $message) ||
                            !array_key_exists('Body', $message) || !array_key_exists('MD5OfBody', $message))
                        continue;

                    $md5 = $message['MD5OfBody'];

                    if (md5($message['Body']) != $md5){
                        continue;
                    }

                    if ($this->debug)
                        var_dump($message);

                    $ReceiptHandle = (string) $message['ReceiptHandle'];
                    $MessageId = (string) $message['MessageId'];
                    $message_content = &$message['Body'];

                    if (strpos($message_content, '{') !== 0){
                        continue;
                    }

                    $moduleData = json_decode($message_content);

                    if (!$moduleData instanceof StdClass)
                        continue;

                    if (!property_exists($moduleData, 'Data') || !isset($moduleData->Data) || empty($moduleData->Data))
                        continue;

                    $moduleData->ReceiptHandle = $ReceiptHandle;
                    $moduleData->MessageId = $MessageId;
                    $moduleDataMessages[] = $moduleData;
                }
            }

        } else {
            if ($this->verbose) {
                echo "<pre>\n";
                echo "No Messages pending...";
                echo "</pre>\n";
            }

            if(isset($amazon_feeds->proc_rep)){
                $amazon_feeds->message->no_product = Amazon_Tools::l('No Messages pending') . "..";
                $amazon_feeds->proc_rep->set_status_msg($amazon_feeds->message);
            }
            $amazon_feeds->output = 'No Messages pending...';
            $amazon_feeds->StopFeed();
        }

        if (is_array($moduleDataMessages) && count($moduleDataMessages)) {
            foreach ($moduleDataMessages as $moduleDataMessage) {
                $date = $moduleDataMessage->Date;
                $timestamp = strtotime($moduleDataMessage->Date);

                // Group by SKU, Date
                foreach ($moduleDataMessage->Data as $skuItem) {
                    $skuItem->date = $date;
                    $skuItem->timestamp = $timestamp;
                    $skuObjects[$skuItem->SKU][] = $skuItem; // preserve the items as unique (as the index is the SKU)
                }
            }

            if (is_array($skuObjects) && count($skuObjects)) {
                if ($this->verbose)
                    echo "<pre>Product Feeed:\n";

                foreach ($skuObjects as $skuObjectArray) {

                    $currentSkuObject = reset($skuObjectArray);

                    if (count($skuObjectArray) > 1) {
                        $timestamp = 0;

                        foreach ($skuObjectArray as $skuObject)
                            if ($skuObject->timestamp > $timestamp) // take the older
                                $currentSkuObject = $skuObject;
                    }

                    if ($this->verbose)
                        printf('%s: SKU: %s Price: %.02f MinPrice: %.02f MaxPrice %.02f' . "\n", $currentSkuObject->date, $currentSkuObject->SKU, $currentSkuObject->Price, $currentSkuObject->MinPrice, $currentSkuObject->MaxPrice);

                    $currentSkuObject->price = $currentSkuObject->Price; //unset($currentSkuObject->date); //unset($currentSkuObject->timestamp);
                    unset($currentSkuObject->Price);

                    // zero price
                    if($currentSkuObject->price <= 0){
                        $skipped[$currentSkuObject->SKU] = sprintf(Amazon_Tools::l('ProductID(s)') . ' - ' . Amazon_Tools::l('zero_price'), $currentSkuObject->SKU);
                        continue;
                    }

                    $productsUpdate['product'][] = (array) $currentSkuObject; // Revert To Arrray, Convert to an indexed array to be compatible with the AmazonWebService class format
                }

                $productsUpdate['productIndex'] = count($skuObjects);
                $productsUpdate['skipped'] = count($skipped);

                if ($this->verbose)
                    echo "</pre>\n";

            }

            // Log skipped product
            if(isset($amazon_feeds->amazonDatabase) && !empty($skipped)){
                $this->log_skipped($skipped, 'warning', $batchID, $operation_type, $amazon_feeds->amazonDatabase);
            }

            if (is_array($productsUpdate['product']) && count($productsUpdate['product'])) {

                if ($this->verbose) {
                    echo "<pre>\n";
                    echo "Preparing Feed Submission for" . count($productsUpdate['product']) . " offers\n";
                    echo "</pre>\n";
                }

                // Submit Product Feed to Amazon
                $feed = $amazon_feeds->FeedsData($productsUpdate, true);
            }

            if (isset($feed) && $feed && !$this->debug) { // if feed has been submitted we delete the previous queue

                $sqs = new AmazonSQS($this->awsKeyId, $this->awsSecretKey);
                foreach ($moduleDataMessages as $moduleDataMessage) {

                    $delete_result = $sqs->deleteMessage($this->UrlQueueOut, (string)$moduleDataMessage->ReceiptHandle);

                    if (!(is_array($delete_result) && array_key_exists('RequestId', $delete_result) && preg_match('/([a-z0-9]*-){4,}/', $delete_result['RequestId'])))
                    {
                        if ($this->debug)
                        {
                            printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                            var_dump($delete_result);
                        }
                        if ($this->verbose) {
                            echo "<pre>\n";
                            echo 'ERROR: Failed to delete message Id:' . $moduleDataMessage->MessageId;
                            echo "<pre>\n";
                        }
                    }
                }
            }

        }  else {

            $amazon_feeds->output = Amazon_Tools::l('No price to export');
            if(isset($amazon_feeds->proc_rep)){
                $amazon_feeds->message->no_product = $amazon_feeds->output;
                $amazon_feeds->proc_rep->set_status_msg($amazon_feeds->message);
            }
            $amazon_feeds->StopFeed();
        }

        exit;
    }

    public function Reprice() {

        require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

        $this->batchID = uniqid();

        if ($this->debug == false) {
            $this->StartProcessMessage('analysis');
        }

        if (isset($this->proc_rep)){
            $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Starting reprice') . "..";
            $this->proc_rep->set_status_msg($this->message);
        }

        $this->id_shop = $this->params->id_shop;
        $this->setAwsSettings();

        $this->database = new AmazonDatabase($this->params->user_name);
        $this->strategy = new AmazonStrategy($this->params->user_name, $this->debug);
        $this->currencies = new Eucurrency();
        $this->conditionMap = $this->database->feedbiz->getConditionMapping($this->params->user_name, $this->id_shop, _ID_MARKETPLACE_AMZ_);
        $this->profiles = $this->database->get_amazon_profile($this->params->id_country, null, $this->id_shop);
        $this->strategies = $this->strategy->get_strategies($this->id_shop, $this->params->id_country);
        $this->categories = $this->database->get_category_selected($this->params->id_country, null, $this->id_shop, $full = true);

	// check Amazon FBA
	$this->amazon_features = $this->database->get_configuration_features($this->params->id_country, $this->id_shop);

        // Currency
        $this->to_currency = $this->params->currency;
        $this->from_currency = $this->database->feedbiz->getCurrency($this->params->user_name, $this->id_shop);

        if (!is_array($this->categories) || !count($this->categories)) {
            $this->error = Amazon_Tools::l('Amazon categories are not yet configured');
            $this->StopFeed();
        }

        if (!is_array($this->profiles) || !count($this->profiles)) {
            $this->error = Amazon_Tools::l('Repricing tool requires profile configuration');
            $this->StopFeed();
        }

        if ($this->debug) {
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);
        }

        // FBA
        $FBA = isset($this->amazon_features['fba']) ? (bool) $this->amazon_features['fba'] : false;

	// get price formular from FBA master platform
	$FBA_formula = AmazonUserData::getFBA($this->params->id_customer, $this->params->ext, $this->params->id_shop);

	if($FBA) {

	    if (isset($FBA_formula['fba_formula']) || !empty($FBA_formula['fba_formula']))
		$this->fbaFormula = $FBA_formula['fba_formula'];
	    else
		$this->fbaFormula = null;

	    if (isset($FBA_formula['fba_estimated_fees']) || !empty($FBA_formula['fba_estimated_fees']))
		$this->fbaEstimatedFees = $FBA_formula['fba_estimated_fees'];
	    else
		$this->fbaEstimatedFees = null;

	    if (isset($FBA_formula['fba_fulfillment_fees']) || !empty($FBA_formula['fba_fulfillment_fees']))
		$this->fbaFulfillmentFees = $FBA_formula['fba_fulfillment_fees'];
	    else
		$this->fbaFulfillmentFees = null;

            if (isset($FBA_formula['default_fba_fees']) || !empty($FBA_formula['default_fba_fees']))
		$this->default_fba_fees = $FBA_formula['default_fba_fees'];
	    else
		$this->default_fba_fees = null;

	    if (isset($FBA_formula['default_fba_estimated_fees']) || !empty($FBA_formula['default_fba_estimated_fees']))
		$this->default_fbaEstimatedFees = $FBA_formula['default_fba_estimated_fees'];
	    else
		$this->default_fbaEstimatedFees = null;

	    if (isset($FBA_formula['default_fba_fulfillment_fees']) || !empty($FBA_formula['default_fba_fulfillment_fees']))
		$this->default_fbaFulfillmentFees = $FBA_formula['default_fba_fulfillment_fees'];
	    else
		$this->default_fbaFulfillmentFees = null;

	}

        // Max execution time is set and is reasonable, otherwise we determine it
        $this->MaxExecutionTime();

        if (isset($this->proc_rep)){
            $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Retrieve Messages') . "..";
            $this->proc_rep->set_status_msg($this->message);
        }

        $last_time = $this->database->get_last_repricing_log($this->params->id_shop, $this->params->id_country); // last repricing time

        /*if($this->debug){
            include USERDATA_PATH . $this->params->user_name . '/amazon/message_set_2016-11-07_06-40-08_JPY.php';
        } else {*/
            $message_set = $this->RetrieveMessages(
                    $this->awsKeyId,
                    $this->awsSecretKey,
                    $this->UrlQueueIn,
                    $this->script_start_time,
                    $this->max_execution_time,
                    $this->verbose,
                    $last_time
            );
        //}

        if(!$message_set){
            if (isset($this->proc_rep)){
                $this->message->{AmazonRepricingAutomaton::REPRICE}->status = 'Success';
                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Retrieve Messages Success') . ".";
                $this->proc_rep->set_status_msg($this->message);
            }
            file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',
                print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'site'=>$this->params->ext,'output'=>'empty $message_set'), true), FILE_APPEND);
            $this->StopFeed();
        }

        $message_count = 0;
        $notifications = $probeASINs = $skuItems = $report_logs = array();

        if (isset($message_set) && is_array($message_set) && count($message_set)) {

            // First Loop - Checking Notifications
            foreach ($message_set as $messages) {

                $message_count = $message_count + count($messages);
                foreach ($messages as $message) {

                    if (!array_key_exists('ReceiptHandle', $message) || !array_key_exists('MessageId', $message) || !array_key_exists('Body', $message) || !array_key_exists('MD5OfBody', $message)) {
                        continue;
                    }

                    $md5 = $message['MD5OfBody'];
                    if (md5($message['Body']) != $md5) {
                        if(!$this->debug) {
                            continue;
                        }
                    }

                    $ReceiptHandle = (string) $message['ReceiptHandle'];
                    $MessageId = (string) $message['MessageId'];
                    $notification_content = &$message['Body'];

                    if (strpos($notification_content, '<Notification>') !== 0){
                        file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'output'=>'missing <Notification>'), true), FILE_APPEND);
                        continue;
                    }

                    $notification = simplexml_load_string($notification_content);

                    if (!property_exists($notification, 'NotificationPayload') || !property_exists($notification->NotificationPayload, 'AnyOfferChangedNotification')){
                        file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'output'=>'missing NotificationPayload'), true), FILE_APPEND);
                        continue;
                    }

                    $notification->addChild('ReceiptHandle', $ReceiptHandle);
                    $notification->addChild('MessageId', $MessageId);
                    $notifications[] = $notification;

                    // List ASINs
                    $probeASINs[] = (string) $notification->NotificationPayload->AnyOfferChangedNotification->OfferChangeTrigger->ASIN;

                    #test FOXCHIP
                    /*$asin = (string) $notification->NotificationPayload->AnyOfferChangedNotification->OfferChangeTrigger->ASIN;
                    if($asin == 'B00MC7WHWI' || $asin == 'B016Z38IOA')
                    {
                        if(!$this->debug){
                            $this->debug_foxchip = $asin . '-' . date('Y-m-d').'_'.date('H_i_s');
                            file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/' . $this->params->user_name
                                    . '/amazon/'. $this->debug_foxchip . '.out', serialize($message_set));
                        }
                    }*/
                    #test FOXCHIP
                }
            }

            if ($this->verbose) {
                $count_notifications = count($notifications);
                echo "<pre>Starting Repricing for : $count_notifications\n";
                echo "</pre>\n";
            }

            $ASINs = implode("', '", $probeASINs);

	    if ($this->verbose) {
                echo "<pre>ASINs : ".  print_r($ASINs, true)."</pre>\n";
            }

            //Check ASIN
            $this->checkForASINs($ASINs);

            // get Products
            if(strlen($ASINs))
                $this->products = $this->database->getProductsByASINs((int)$this->id_shop, (int)$this->params->id_country, $ASINs, (int)$this->params->id_lang);

        }

        if(isset($this->products) && !empty($this->products)) {

	    if ($this->verbose) {
                echo "<pre>Products : ".  print_r($this->products, true)."</pre>\n";
            }

	    // FBA data
	    if($FBA && (isset($this->fbaEstimatedFees) || isset($this->fbaFulfillmentFees))){
		$sku = array();
		foreach ($this->products as $product){
		    if(isset($product->sku) && !empty($product->sku))
			$sku[] = $product->sku;
		}
		$this->Amazon_fba_data = $this->database->get_fba_data((int)$this->id_shop, (int)$this->params->id_country, $sku) ;

	    }

            $this->products_strategy = $this->strategy->get_products_strategies((int)$this->id_shop, (int)$this->params->id_country, $this->products);

            if (isset($this->proc_rep)){
                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Repricing') . "..";
                $this->proc_rep->set_status_msg($this->message);
            }

            $sqs = new AmazonSQS($this->awsKeyId, $this->awsSecretKey);

            // Forth Loop - Calculate
            foreach ($notifications as $notification) {

                $result = $this->repricing($notification);

                if (isset($result['skuItems']) && is_array($result['skuItems']) && count($result['skuItems'])) {
                    $skuItems = array_merge($skuItems, $result['skuItems']);
                }

                if (isset($result['report_logs']) && is_array($result['report_logs']) && count($result['report_logs'])) {
                    $report_logs = array_merge($report_logs, $result['report_logs']);
                }

                if (!$this->debug) {
                    $delete_result = $sqs->deleteMessage($this->UrlQueueIn, (string)$notification->ReceiptHandle);

                    if (!(is_array($delete_result) && array_key_exists('RequestId', $delete_result) && preg_match('/([a-z0-9]*-){4,}/', $delete_result['RequestId']))) {
                        if ($this->debug){
                            printf('%s(%d): Error'."\n", basename(__FILE__), __LINE__);
                            var_dump($delete_result);
                        }
                        if ($this->verbose){
                            echo "<pre>\n";
                            echo 'ERROR: Failed to delete message Id:'.$notification->MessageId;
                            echo "<pre>\n";
                        }
                    }
                }
            }
        }

        // log_reprice amazon_repricing_log
        $this->log_reprice($message_count, $skuItems, $this->database);

	if(count($report_logs)) {
	    $this->repricing_report($report_logs, $this->database, AmazonDatabase::REPRICING_ANALYSIS, AmazonDatabase::REPRICING);
	}

        if (is_array($skuItems) && count($skuItems)){

            if (isset($this->proc_rep)){
                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Sending to Queue') . "..";
                $this->proc_rep->set_status_msg($this->message);
            }
            $this->sendToQueue($skuItems, $report_logs);

        } else {
            if (isset($this->proc_rep)){
                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Nothing_to_send') ;
                $this->proc_rep->set_status_msg($this->message);
            }
            $this->StopFeed();
        }
    }

    public function sendToQueue($skuItems, $update_logs=null) {

        if ($this->verbose) {
            echo "<pre>\n";
            echo str_repeat('-', 160) . "\n";
            echo "Ready to send message to queue:" . $this->UrlQueueOut;
            echo "</pre>\n";
        }

        if (is_array($skuItems) && count($skuItems)) {
            if ($this->verbose) {
                echo "<pre>Amazon Price Feed:\n";
                var_dump($skuItems);
                echo "</pre>\n";
            }

	    $sqs = new AmazonSQS($this->awsKeyId, $this->awsSecretKey);

            if (!$this->UrlQueueOut) {
                $this->error = Amazon_Tools::l('Missing expected queue');
                $this->StopFeed();
                return (false);
            }

            $message = array(
                'Date' => date('c'),
                'From' => Amazon_Tools::encodeText($this->params->shopname, true),
                'Count' => count($skuItems),
                'Data' => $skuItems
            );

            $json_message = json_encode($message);

            if(!$this->debug){
                //if((int)$this->params->id_country == 6) {
                    //file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/sendToQueue_es.txt',
                        //print_r(array('Time'=>date('Y-m-d H:i:s'),'Type'=>$this->action,'UrlQueueOut'=>$this->UrlQueueOut,'message'=> ($json_message)),true), FILE_APPEND);
                //}
                $result = $sqs->sendMessage($this->UrlQueueOut, $json_message);
                if (!(is_array($result) && array_key_exists('RequestId', $result) && array_key_exists('MessageId', $result)
                    && preg_match('/([a-z0-9]*-){4,}/', $result['RequestId'])))
                {
                    if ($this->debug) {
                        printf('%s(%d): Error' . "\n", basename(__FILE__), __LINE__);
                        var_dump($result);
                    }
                    $this->error = Amazon_Tools::l('Failed to send message to the queue:') . $this->UrlQueueOut;
                    $this->StopFeed();
                    return (false);
                }

                if ($this->verbose) {
                    echo "<pre>\n";
                    echo "Message successfully sent to message queue, Id:" . $result['MessageId'] . "\n";
                    echo "</pre>\n";
                }

                if (isset($this->proc_rep)){
                    $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();
                    $this->message->{AmazonRepricingAutomaton::REPRICE}->status = 'Success';
                    $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Success. Message Queue Id: ').$result['MessageId'] ;
                    $this->proc_rep->set_status_msg($this->message);
                }

            }

        } else {
            if (isset($this->proc_rep)){
                $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();
                $this->message->{AmazonRepricingAutomaton::REPRICE}->status = 'Success';
                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Nothing_to_send') ;
                $this->proc_rep->set_status_msg($this->message);
            }
        }

        // 20161201 update flag
        $this->database->update_amazon_repricing_flag($this->params->id_shop, $this->params->id_country, $update_logs);

        if($this->action != AmazonRepricingAutomaton::REPRICE_PRODUCT_CREATION) {
            $this->StopFeed();
        }
        return (true);
    }

    public function repricing(&$notification) {

        $skuItems = $report_logs = array();
        $ASIN = (string) $notification->NotificationPayload->AnyOfferChangedNotification->OfferChangeTrigger->ASIN;
        $NotificationDateTime = (string) $notification->NotificationMetaData->PublishTime;
        $ItemCondition = (string) $notification->NotificationPayload->AnyOfferChangedNotification->OfferChangeTrigger->ItemCondition;
        $myMerchantId = $notification->NotificationMetaData->SellerId;
        $myOffer = $this->merchantOfferLookup($notification, $myMerchantId);

        if ($myOffer && $myOffer->IsBuyBoxWinner == 'true') {
            if ($this->verbose) {
                echo "<pre>\n";
                echo str_repeat('-', 160) . "\n";
                printf('Im Buybox winner for ASIN: %s' . "\n", $ASIN);
                echo "</pre>\n";
            }
            return(true);
        } elseif (!$myOffer) {
            if ($this->verbose) {
                echo "<pre>\n";
                echo str_repeat('-', 160) . "\n";
                printf('Unable to find my offer for ASIN: %s' . "\n", $ASIN);
                echo "</pre>\n";
            }
            return(false);
        }

        if ($this->verbose) {
            echo "<pre>\n";
            echo str_repeat('-', 160) . "\n";
            printf('Computing ASIN: %s, Condition: %s', $ASIN, $ItemCondition);
            echo "</pre>\n";
        }

        if ($this->debug) {
            echo "<pre>\n";
            printf('My Offer:' . "\n");
            echo print_r($myOffer, true);
            echo "</pre>\n";
        }

        $buyBoxOffers = $this->buyBoxOffersLookup($notification, $myMerchantId);
        $price = $reprice = null;

        if ($buyBoxOffers === true) {
            $price = null;
            if ($this->verbose) {
                echo "<pre>\n";
                printf('Current Price: %.2f - Our Offer has the Buybox', (float) $myOffer->ListingPrice->Amount);
                echo print_r($myOffer, true);
                echo "</pre>\n";
            }
            return(null); // We are the cheaper
        }

        if (isset($this->products[$ASIN]) && !empty($this->products[$ASIN]) && $this->products[$ASIN] instanceof StdClass) {

            $product_idenfier = $this->products[$ASIN];

            if (isset($product_idenfier->disable) && (bool) $product_idenfier->disable)
                return(false);

            $id_product = $product_idenfier->id_product;
            $id_product_attribute = isset($product_idenfier->id_product_attribute) ? $product_idenfier->id_product_attribute : 0;

            if ($this->verbose) {
                echo "<pre>Product idenfier : \n";
                echo print_r($product_idenfier, true);
                echo "</pre>\n";
            }

            // Check Condition
            $condition = !empty($product_idenfier->id_condition) && isset($this->conditionMap[$product_idenfier->id_condition]) && !empty($this->conditionMap[$product_idenfier->id_condition]) ? $this->conditionMap[$product_idenfier->id_condition]['condition_value'] : 'New';

            if (strtoupper($condition) != strtoupper($ItemCondition)){
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Mismatch conditions: Product-%s / Item Condition-%s', $condition, $ItemCondition);
                    echo "</pre>\n";
                }
                return(false);
            }

            if (!strlen($product_idenfier->sku) || !Amazon_Tools::ValidateSKU($product_idenfier->sku)) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Missing or wrong reference for id_product: %d/%d', $id_product, $id_product_attribute);
                    echo "</pre>\n";
                }
                return(false);
            }

            $SKU = $product_idenfier->sku;

            // Check is seleted category
            if (isset($this->categories[$product_idenfier->id_category_default])) {
                $id_category = (int) $product_idenfier->id_category_default;
            } else {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Product is not in selected categories: %s', $SKU);
                    echo "</pre>\n";
                }
                return(false);
            }

            $profile_id = null;
            $profile_name = null;

            // Check profile selected
            if (isset($this->categories[$product_idenfier->id_category_default]['id_profile'])) {
                $profile_id = $this->categories[$product_idenfier->id_category_default]['id_profile'];
            } else {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Category has no profile selected: ID:%s', $product_idenfier->id_category_default);
                    echo "</pre>\n";
                }
                return(false);
            }

            // If has Profile
            if (isset($this->profiles[$profile_id])) {

                $profile = $this->profiles[$profile_id];

                if (isset($profile['name'])) {
                    $profile_name = $profile['name'];
                }

                if (!empty($profile_name)) {
                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('Using profile [%s] ID: %s', $profile_name, $profile_id);
                        echo "</pre>\n";
                    }
                } else {
                    return(false);
                }
            } else {

                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Profil is not in profiles list [%s] id: %s', $profile_name, $profile_id);
                    echo "</pre>\n";
                }
                return(false);
            }

            //$this->strategies
            if (!isset($profile['category']['repricing']) || empty($profile['category']['repricing'])) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Repricing is not selected for profile: %s(%d)', $profile_name, $profile_id);
                    echo "</pre>\n";
                }
                return;
            }

            $repricing_id = $profile['category']['repricing'];
            $strategy = isset($this->strategies[$repricing_id]) ? $this->strategies[$repricing_id] : null;

            if (!$strategy) {
                if($this->verbose){
                    echo "<pre>\n";
                    printf('No repricing strategy available for this profile: %s(%d)', $profile_name, $profile_id);
                    echo "</pre>\n";
                }
                return(false);
            }

            if (isset($strategy['inactive']) && $strategy['inactive']) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Strategy %s is inactive', $strategy['name']);
                    echo "</pre>\n";
                }
                return(false);
            }

            /* PRICE */
            $price = $product_idenfier->price;
            $from_currency = isset($this->from_currency[$product_idenfier->id_currency]) ? $this->from_currency[$product_idenfier->id_currency] : null;

            if (!isset($from_currency['iso_code']) || empty($from_currency['iso_code'])) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Missing currency code for product: %d/%d', $id_product, $id_product_attribute);
                    echo "</pre>\n";
                }
                return(false);
            }

            // Price Rules
            if (isset($profile['price_rules']['value']) && !empty($profile['price_rules']['value']))
                $price = Amazon_Product::price_rule($profile['price_rules']['value'], $profile['price_rules']['rounding'], $price);

	    // convert currency for wholesale price
            $wholesale_price = $this->currencies->doDiffConvert($product_idenfier->wholesale_price, $from_currency['iso_code'], $this->to_currency);

	    // get product option fba value
            $fba = isset($product_idenfier->fba) ? (bool) $product_idenfier->fba : false;

            $value_added = 0;
	    $is_fba_calculate = false;

            if (isset($this->amazon_features['fba']) && $this->amazon_features['fba'] && $fba) {

                // Override FBA Value Added
                if (isset($product_idenfier->fba_value) && (float) $product_idenfier->fba_value > 0) {

		    $is_fba_calculate = true;
		    $ori_price = $price;
                    $price += (float) $product_idenfier->fba_value;
                    $value_added = (float) $product_idenfier->fba_value;

		    if ($this->verbose) {
			echo "<pre>\n";
			printf('FBA value add for product %d/%d: %d, added Price: %d, Original Price: %d', $id_product,
				$id_product_attribute,$value_added,$price,$ori_price);
			echo "</pre>\n";
		    }

                }
		// FBA Price Value Added
		elseif( (isset($this->Amazon_fba_data[$SKU]['estimated_fee']) || isset($this->Amazon_fba_data[$SKU]['fulfilment_fee']) ) ){

		    // Estimated fee
		    if( isset($this->Amazon_fba_data[$SKU]['estimated_fee']) && isset($this->fbaEstimatedFees) ) {

			$ori_price = $price;

			$estimated_fees = $this->Amazon_fba_data[$SKU]['estimated_fee'];
			$fba_estimated_fees = Amazon_Tools::Formula($estimated_fees, $this->fbaEstimatedFees) - $estimated_fees;

			$price += (float) $fba_estimated_fees;
			$value_added = $value_added + (float) $fba_estimated_fees;

			if ($this->verbose) {
			    echo "<pre>\n";
			    printf('FBA value add for product %d/%d: %d, estimated fees: %d, Original Price: %d', $id_product,
				    $id_product_attribute,$price,$estimated_fees,$ori_price);
			    echo "</pre>\n";
			}
		    }

		    //fba_fulfillment_fees
		    if( isset($this->Amazon_fba_data[$SKU]['fulfilment_fee']) && isset($this->fbaFulfillmentFees) ){

			$ori_price = $price;

			$fulfillment_fees = $this->Amazon_fba_data[$SKU]['fulfilment_fee'];
			$fba_fulfillment_fees = Amazon_Tools::Formula($fulfillment_fees, $this->fbaFulfillmentFees) - $fulfillment_fees;

			$price += (float) $fba_fulfillment_fees;
			$value_added = $value_added + (float) $fba_fulfillment_fees;

			if ($this->verbose) {
			    echo "<pre>\n";
			    printf('FBA value add for product %d/%d: %d, fulfillment fees: %d, Original Price: %d', $id_product,
				    $id_product_attribute,$price,$fulfillment_fees,$ori_price);
			    echo "</pre>\n";
			}

		    }

		    if($value_added <> 0){
			$is_fba_calculate = true;
		    }

		}
                elseif( isset($this->default_fba_fees) ){

                    $price  += (float) $this->default_fba_fees;
                    $value_added = (float) $this->default_fba_fees;

                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('FBA Defaul Fees : %s, added Price: %d, Original Price: %d', $this->default_fba_fees, $price, $ori_price);
                        echo "</pre>\n";
                    }
                }
		// FBA Default Price Value Added
		elseif( (isset($this->default_fbaEstimatedFees) || isset($this->default_fbaFulfillmentFees) ) ){

		    $ori_price = $price;

		    // Estimated fee
		    if(isset($this->default_fbaEstimatedFees) ) {

			$default_estimated_fees = $ori_price * ((float)$this->default_fbaEstimatedFees / 100);
			$price  += (float) $default_estimated_fees;
			$value_added = $value_added + (float) $default_estimated_fees;

			if ($this->verbose) {
			    echo "<pre>\n";
			    printf('FBA Defaul Estimated Fees : %s, added Price: %d, Original Price: %d', $this->default_fbaEstimatedFees, $price, $ori_price);
			    echo "</pre>\n";
			}
		    }

		    //fba_fulfillment_fees
		    if(isset($this->default_fbaFulfillmentFees) ){

			$default_fba_fulfillment_fees = $ori_price * ((float)$this->default_fbaFulfillmentFees / 100);
			$price  += (float) $default_fba_fulfillment_fees;
			$value_added = $value_added + (float) $default_fba_fulfillment_fees;

			if ($this->verbose) {
			    echo "<pre>\n";
			    printf('FBA Formula : %s, added Price: %d, Original Price: %d', $this->fbaFormula, $price, $ori_price);
			    echo "</pre>\n";
			}

		    }

		    if($value_added <> 0){
			$is_fba_calculate = true;
		    }

		}
		// FBA Formula
                elseif ($this->fbaFormula) {

		    $is_fba_calculate = true;
		    $ori_price = $price;
                    $price = Amazon_Tools::Formula($price, $this->fbaFormula);

		    if ($this->verbose) {
			echo "<pre>\n";
			printf('FBA Formula : %s, added Price: %d, Original Price: %d', $this->fbaFormula, $price, $ori_price);
			echo "</pre>\n";
		    }

                }

            } else {

                if (!isset($product_idenfier->quantity) || $product_idenfier->quantity <= 0) {
                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('No stock for product %d/%d, skipped....', $id_product, $id_product_attribute);
                        echo "</pre>\n";
                    }
                    return(false);
                }
            }

	    $stdPrice = $this->currencies->doDiffConvert($price, $from_currency['iso_code'], $this->to_currency);

	    // convert value added
	    if($value_added <> 0){
		$value_added = $this->currencies->doDiffConvert($value_added, $from_currency['iso_code'], $this->to_currency);
	    }

            $current_price = sprintf('%.02f', round($stdPrice, 2));
            $base_price = null;

            switch ($strategy['base']) {
                case AmazonRepricing::REPRICING_WHOLESALE_PRICE:
                    if (!isset($wholesale_price) || empty($wholesale_price)) {
                        if ($this->verbose) {
                            echo "<pre>\n";
                            printf('Missing wholesale price id_product:%d', $id_product);
                            echo "</pre>\n";
                        }
                        continue;
                    }
                    $base_price = $wholesale_price;
                    break;
                case AmazonRepricing::REPRICING_REGULAR_PRICE:
                    $base_price = $current_price;
                    break;
                default:
                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('Repricing base is not selected for strategy: %s', $strategy['name']);
                        echo "</pre>\n";
                    }
                    return(false);
            }

            if (!$base_price) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Missing base price for product: %d/%d', $id_product, $id_product_attribute);
                    echo "</pre>\n";
                }
            }

            if ($this->verbose) {
                echo "<pre>\n";
                echo "Product Data:\n";
                echo print_r($product_idenfier, true);
                echo "Repricing Strategy:\n";
                echo print_r($strategy, true);
                echo "</pre>\n";
            }

            $agressivity = max(1, (int) $strategy['aggressivity']);
            $limit = (int) $strategy['limit'];

            if ( ( isset($this->products_strategy[$id_product][$id_product_attribute]['minimum_price']) )
		    && ( isset($this->products_strategy[$id_product][$id_product_attribute]['target_price']) ) ) {

                $strategy_override = $this->products_strategy[$id_product][$id_product_attribute];

                $delta_min = 0;
                $delta_max = 0;

                $price_min = sprintf('%.02f', round($strategy_override['minimum_price'], 2));
                $price_max = sprintf('%.02f', round($strategy_override['target_price'], 2));

                $price_min = $this->currencies->doDiffConvert($price_min, $from_currency['iso_code'], $this->to_currency);
                $price_max = $this->currencies->doDiffConvert($price_max, $from_currency['iso_code'], $this->to_currency);

            } else {

                if (isset($strategy['manual']) && $strategy['manual']) // Manual
                {
                     if ($this->verbose) {
                        echo "<pre>\n";
                        echo "Product (".$id_product.") Have no strategy override and this strategy - ".$strategy['name']." is manual \n";
                        echo "</pre>\n";
                    }
                    return;
                }

                $delta_min = (int) $strategy['delta_min'];
                $delta_max = (int) $strategy['delta_max'];

                $price_min = sprintf('%.02f', round($current_price * (1 + ($delta_min / 100)), 2)); //93
                $price_max = sprintf('%.02f', round($current_price * (1 + ($delta_max / 100)), 2));
            }

            $price_max += $value_added;
            $price_min += $value_added;
            $base_price += $value_added;

            $base_price_limit = sprintf('%.02f', round($base_price * (1 + ($limit / 100)), 2));

            if (isset($strategy_override) && !empty($strategy_override) && $price_min > 0) // Overrides limit
                $base_price_limit = sprintf('%.02f', round($price_min, 2));

            if (!is_numeric($limit) || $base_price_limit <= 0)
                $base_price_limit = $price_min;

            $calculated = $this->getBestPrice($notification, $myOffer, $ASIN, $agressivity, $value_added);

            if (!$calculated) {
                $safe_price = sprintf('%.02f', max($base_price_limit, $current_price));
                $action = sprintf('No competition, skipping offer...', $current_price);
                $reprice = false;
            } elseif ($calculated <= $base_price_limit || $calculated <= $price_min) {
                $safe_price = sprintf('%.02f', max($base_price_limit, $price_min));
                $action = sprintf('Sending Price Min.: %.02f', $safe_price);
                $reprice = $safe_price;
            } elseif ($calculated >= $price_max) {
                $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                $action = sprintf('Sending Price Max.: %.02f', $safe_price);
                $reprice = $safe_price;
            } else {
                $safe_price = sprintf('%.02f', max($base_price_limit, $calculated));
                $action = 'Repriced';
                $reprice = $safe_price;
            }

            /*if (isset($profile['price_rules']['rounding']) && ($profile['price_rules']['rounding'] > -1 && $profile['price_rules']['rounding'] < 3 )) {
                $smart_price = sprintf('%.02f', round($reprice, $profile['price_rules']['rounding']));
		$price_min = sprintf('%.02f', round($price_min, $profile['price_rules']['rounding']));
		$price_max = sprintf('%.02f', round($price_max, $profile['price_rules']['rounding']));
	    } else {*/
                $smart_price = sprintf('%.02f', $reprice);
		$price_min = sprintf('%.02f', $price_min);
		$price_max = sprintf('%.02f', $price_max);
	    /*}*/

            if ($this->verbose) {
                $text_delta_min = $delta_min ? ($delta_min > 0 ? sprintf(' (+%02d&#37;)', $delta_min) : sprintf(' (%02d&#37;)', $delta_min)) : null;
                $text_delta_max = $delta_max ? ($delta_max > 0 ? sprintf(' (+%02d&#37;)', $delta_max) : sprintf(' (%02d&#37;)', $delta_max)) : null;

                echo "<pre>\n";
                echo "Action:<b>" . $action . "</b>\n";
                echo "Final Price:<b>" . $smart_price . "</b>\n";
                echo "Repriced:" . $reprice . "\n";
                echo "Calculated:" . $calculated . "\n";
                echo "Base Price:" . $current_price . "\n";
                echo "Range(min):" . $price_min . $text_delta_min . "\n";
                echo "Range(max):" . $price_max . $text_delta_max . "\n";
                echo "Base:" . $base_price . "\n";
                echo "Limit:" . $base_price_limit . ($limit > 0 ? sprintf(' (+%02d&#37;)', $limit) : sprintf(' (%02d&#37;)', $limit)) . "\n";
                echo "Strategy Source:" . 'Profile' . "\n";
                echo "Notification Date/Time:" . $NotificationDateTime . "\n";
                echo "</pre>\n";
            }

            if ($reprice) {

		if (isset($price_min) && (float) $smart_price) {
		    $price_min = min($price_min, $smart_price);
		}

		if (isset($price_max) && (float) $smart_price) {
		    $price_max = max($price_max, $smart_price);
		}

		$skuItems[$SKU]['SKU'] = $SKU;
                $skuItems[$SKU]['Price'] = sprintf('%.02f', $smart_price);
                $skuItems[$SKU]['MinPrice'] = sprintf('%.02f', $price_min);
                $skuItems[$SKU]['MaxPrice'] = sprintf('%.02f', $price_max);

		$report_logs[$SKU]['SKU'] = $SKU;
		$report_logs[$SKU]['Price'] = sprintf('%.02f', $smart_price);
                $report_logs[$SKU]['MinPrice'] = sprintf('%.02f', $price_min);
                $report_logs[$SKU]['MaxPrice'] = sprintf('%.02f', $price_max);
                $report_logs[$SKU]['action'] = $action;
		$report_logs[$SKU]['repricing_mode'] = $is_fba_calculate ? 'FBA' : 'MFN';

            }
        } else {

            if ($this->verbose) {
                echo "<pre>\n";
                echo "Product not found for ASIN:" . $ASIN . "\n";
                echo "</pre>\n";
            }
        }

        if (is_array($skuItems) && count($skuItems))
            return(array('skuItems' => $skuItems, 'report_logs' => $report_logs));
        else
            return(array());
    }

    public function getBestPrice($notification, $myOffer, $ASIN, $agressivity_param, $fba_value = 0) {

        $myMerchantId = $myOffer->SellerId;
        $offers = $notification->xpath('NotificationPayload/AnyOfferChangedNotification/Offers/Offer');

        if (!is_array($offers) || !count($offers))
            return(false);

        $summary = &$notification->NotificationPayload->AnyOfferChangedNotification->Summary;

        $agressivities = array();

        for ($i = 10, $rindex = 10; $i < 110; $i += 10, $rindex--) {
            $calculated_agressivity = round($agressivity_param / $rindex, 2);
            $agressivities[$i] = $calculated_agressivity;
        }

        $imFulfilledByAmazon = $myOffer->IsFulfilledByAmazon == 'true' ? true : false;
        $imFeaturedMerchant = $myOffer->IsFeaturedMerchant == 'true' ? true : false;
        $myFeedbackRating = $myOffer->SellerFeedbackRating ? (int) $myOffer->SellerFeedbackRating->attributes()->SellerPositiveFeedbackRating : null;
        $myFeedbackCount = $myOffer->SellerFeedbackRating ? (int) $myOffer->SellerFeedbackRating->attributes()->FeedbackCount : null;
        $iShipFrom = $myOffer->ShipsFrom ? $myOffer->ShipsFrom->attributes()->Country : null;
        $myOfferAvailabilityType = $myOffer->ShippingTime && $myOffer->ShippingTime->attributes()->availabilityType;
        $myShipping = (float) $myOffer->Shipping->Amount;
        $myPrice = (float) $myOffer->ListingPrice->Amount + $fba_value;
        $myCondition = (string) $myOffer->SubCondition;

        $bestSellerFeedBackCount = 0;
        $bestSellerFeedBackRating = 0;
        $bestSellerPrice = 0;
        $bestSellerAvailabilityType = null;
        $bestSellerShipsFrom = null;

        $calculated_price = null;
        $cheaper_price = 0;
        $cheaper_shipping = 0;
        $bestOffer = null;

        if ($this->debug) {
            echo "<pre>Notification:" . print_r($notification) . "\n";
            echo "<pre>OfferCount:" . print_r($summary->NumberOfOffers->OfferCount, true) . "\n";
            echo "<pre>All Offers:\n";
            print_r($offers);
            echo "</pre>\n";
        }

        foreach ($summary->LowestPrices->LowestPrice as $lowestPriceItem) {
            if ((string) $lowestPriceItem->attributes()->SubCondition != $myCondition)
                continue;

            $currentPrice = (float) $lowestPriceItem->ListingPrice->Amount + (float) $lowestPriceItem->Shipping->Amount;

            if ($bestSellerPrice < $currentPrice) {
                $bestSellerPrice = $currentPrice;

                $cheaper_price = (float) $lowestPriceItem->ListingPrice->Amount;
                $cheaper_shipping = (float) $lowestPriceItem->Shipping->Amount;
            }
        }

        foreach ($offers as $offer) {
            if ($this->debug) {
                echo "<pre>Competition on Offer:\n";
                print_r($offer);
                echo "</pre>\n";
            }
            $merchantId = (string) $offer->SellerId;
            $sellerPrice = (float) $offer->ListingPrice->Amount;
            $sellerShipping = (float) $offer->Shipping->Amount;

            $sellerFeedBackRating = $offer->SellerFeedbackRating && (int) $offer->SellerFeedbackRating->attributes()->SellerPositiveFeedbackRating;
            $sellerFeedBackCount = $offer->SellerFeedbackRating && (int) $offer->SellerFeedbackRating->attributes()->FeedbackCount;
            $sellerAvailabilityType = $offer->ShippingTime && $offer->ShippingTime->attributes()->availabilityType;
            $sellerShipsFrom = $offer->ShipsFrom ? strtolower($offer->ShipsFrom->attributes()->Country) : null;

            if ($merchantId == $myMerchantId)
                continue;

            if ($sellerPrice + $sellerShipping < $bestSellerPrice)
                $bestSellerPrice = $sellerPrice + $sellerShipping;

            if ($sellerFeedBackRating > $bestSellerFeedBackRating)
                $bestSellerFeedBackRating = $sellerFeedBackRating;

            if ($sellerFeedBackCount > $bestSellerFeedBackCount)
                $bestSellerFeedBackCount = $sellerFeedBackCount;

            if ($sellerAvailabilityType == 'NOW')
                $bestSellerAvailabilityType = $sellerAvailabilityType;

            if ($sellerShipsFrom == $this->params->iso_code && $sellerShipsFrom != $iShipFrom)
                $bestSellerShipsFrom = $sellerShipsFrom;

            if ($offer->IsBuyBoxWinner == 'true') {
                $bestOffer = $offer;
            }
            if ($sellerPrice + $sellerShipping < $cheaper_price + $cheaper_shipping || $cheaper_price + $cheaper_shipping == 0) {
                if (!$bestOffer)
                    $bestOffer = $offer;

                $cheaper_price = $sellerPrice;
                $cheaper_shipping = $sellerShipping;
            }
        }
        if (!$bestOffer)
            $bestOffer = $offers[0];

        if ($this->debug) {
            echo "<pre>Best Offer:\n";
            print_r($bestOffer);
            echo "</pre>\n";

            echo "<pre>\n";
            printf('Cheaper Price: %.02f' . "\n", $cheaper_price);
            echo "</pre>\n";
        }

        $score = 0;

        if ($bestOffer) {
            $sellerPrice = (float) $bestOffer->ListingPrice->Amount;
            $sellerShipping = (float) $bestOffer->Shipping->Amount;

            $sellerFeedBackRating = $bestOffer->SellerFeedbackRating ? (int) $bestOffer->SellerFeedbackRating->attributes()->SellerPositiveFeedbackRating : null;
            $sellerFeedBackCount = $bestOffer->SellerFeedbackRating ? (int) $bestOffer->SellerFeedbackRating->attributes()->FeedbackCount : null;
            $sellerAvailabilityType = $bestOffer->ShippingTime ? $bestOffer->ShippingTime->attributes()->availabilityType : null;
            $sellerShipsFrom = $bestOffer->ShipsFrom ? strtolower($bestOffer->ShipsFrom->attributes()->Country) : null;

            if ($bestOffer->IsBuyBoxWinner == 'true')
                $score += 20;

            if ($bestOffer->IsFulfilledByAmazon == 'true')
                $score += 20;

            if ($bestOffer->IsFeaturedMerchant == 'true')
                $score += 10;

            if ($sellerFeedBackRating && $sellerFeedBackRating < $myFeedbackRating)
                $score -= 10;

            if ($sellerFeedBackCount && $sellerFeedBackCount < $myFeedbackCount)
                $score -= 10;

            if ($sellerAvailabilityType && $sellerAvailabilityType == 'NOW')
                $score += 10;

            if ($sellerPrice && $sellerPrice + $sellerShipping < $myPrice + $myShipping)
                $score += 10;

            if ($sellerShipping == 0)
                $score += 10;

            if ($sellerShipsFrom == $this->params->iso_code && $sellerShipsFrom != $iShipFrom)
                $score += 10;
        }

        $myscore = 100;
        $myscore -=!$imFulfilledByAmazon ? 30 : 0;
        $myscore -=!$imFeaturedMerchant ? 20 : 0;
        $myscore -= $bestSellerPrice && $bestSellerPrice < $myPrice + $myShipping ? 10 : 0;
        $myscore -= $bestSellerFeedBackRating && $myFeedbackRating < $bestSellerFeedBackRating ? 10 : 0;
        $myscore -= $bestSellerFeedBackCount && $myFeedbackCount < $bestSellerFeedBackCount ? 10 : 0;
        $myscore -= $myOfferAvailabilityType != 'NOW' && $bestSellerAvailabilityType == 'NOW' ? 10 : 0;
        $myscore -= $myShipping > 0 && !$imFulfilledByAmazon ? 10 : 0;
        $myscore -= $bestSellerShipsFrom == $this->params->iso_code && $bestSellerShipsFrom != $iShipFrom ? 10 : 0;

        $base_score = $score;
        $base_price = $cheaper_price;
        $base_shipping = $cheaper_shipping;

        if ($this->debug) {
            echo "<pre>\n";
            printf('Base Price: %.02f, Base Shipping: %.02f' . "\n", $base_price, $base_shipping);
            echo "</pre>\n";
        }

        if ($base_price) {
            // Adjust to minimum
            $score_gap = $base_score - $myscore;
            $agressivity_level = 0;

            if ($myscore < $base_score) {
                // Adjust agressivly
                foreach ($agressivities as $agressivity_level => $agressivity) {
                    if ($score_gap <= $agressivity_level)
                        break;
                }
            }
            else { // Minimum Agressivity
                $agressivity = reset($agressivities);
                $agressivity_level = key($agressivities);
            }
            $agressivity /= 100;

            $shipping_diff = $myShipping - $base_shipping;

            $raw_price = ($base_price - $shipping_diff);
            $calculated_price = sprintf('%.02f', ($raw_price / (1 + $agressivity)));

            if ($this->verbose) {
                echo "<pre>";
                printf('ASIN: %s, agressivity: %d' . "\n", $ASIN, $agressivity_param);
                if ($this->debug)
                    print_r($agressivities);
                printf('Competition on Price: %.02f, Shipping: %.02f, Price+Shipping: %.02f, Score: %d' . "\n", $base_price, $base_shipping, $base_price + $base_shipping, $base_score);
                printf('My Price: %.02f Shipping: %.02f, Price+Shipping: %.02f, My Score: %d, Agressivity: Level: %d / Rate: %.04f, Calculated: %.02f' . "\n", $myPrice, $myShipping, $myPrice + $myShipping, $myscore, $agressivity_level, $agressivity, $calculated_price);
                printf('<b>Competition Result: %.02f against %.02f</b>' . "\n", $calculated_price + $myShipping, $base_price + $base_shipping);
                echo "</pre>\n";
            }
        }
        return($calculated_price);
    }

    public function merchantOfferLookup(&$notification, $merchantId) {
        $offers = &$notification->NotificationPayload->AnyOfferChangedNotification->Offers;

        if (property_exists($offers, 'Offer') && count($offers->Offer)) {
            foreach ($offers->Offer as $offer) {
                if ((string) $offer->SellerId == (string) $merchantId)
                    return($offer);
            }
        }
        return(false);
    }

    public function buyBoxOffersLookup(&$notification, $merchantId) {

        $offers = &$notification->NotificationPayload->AnyOfferChangedNotification->Offers;
        $buyBoxEligibleOffers = &$notification->NotificationPayload->AnyOfferChangedNotification->Summary->BuyBoxEligibleOffers;
        $returnedBuyBoxOffers = array();

        if (!count($buyBoxEligibleOffers) || !property_exists($buyBoxEligibleOffers, 'OfferCount') || !count($buyBoxEligibleOffers->OfferCount))
            return(false);

        if (property_exists($offers, 'Offer') && count($offers->Offer) && property_exists($buyBoxEligibleOffers, 'OfferCount') && count($buyBoxEligibleOffers->OfferCount)) {
            foreach ((array) $buyBoxEligibleOffers->OfferCount as $key => $offerCount) {
                if (!is_numeric($key))
                    continue;

                $idOffer = (int) $offerCount;
                $offers_array = (array) $offers;

                if (!isset($offers_array['Offer'][$idOffer]))
                    continue;

                $targetOffer = $offers_array['Offer'][$idOffer];

                if ((string) $targetOffer->SellerId == (string) $merchantId && $targetOffer->IsBuyBoxWinner == 'true')
                    return(true); //We are the buybox winner

                $returnedBuyBoxOffers[] = $targetOffer;
            }
            if (count($returnedBuyBoxOffers))
                return($returnedBuyBoxOffers);
        }
        return(null);
    }

    public function getCompetitivePrice($products) {

	$this->database = new AmazonDatabase($this->params->user_name);
        $history = $skuItems = $report_logs = array();
        $loop_start_time = microtime(true);

        if (!empty($products)) {

            // get default shipping price //2016-10-17
            $defaut_override_shipping = isset($this->params->repricing_defaut_override_shipping) ? $this->params->repricing_defaut_override_shipping : null;

            if ($this->verbose) {
                echo "<pre><b>GetCompetitivePricingForSKU Product :</b> \n ";
                echo print_r($products, true);
                echo "</pre>\n";
            }

            $i = 0;
            $slices = array_chunk($products, 20); //Maximum request quota = 20 item / time

            if (count($slices)) {
                foreach ($slices as $slice){
                    $i++;
                    $loop_average = (microtime(true) - $loop_start_time) / ($i + 1);
                    $total_elapsed = microtime(true) - $this->script_start_time;
                    $max_estimated = (($loop_start_time - $this->script_start_time) + $loop_average * $i * 1.4);

                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('Loop average: %.02f, Max estimated: %.02f, Total Elapsed: %.02f' . "\n", $loop_average, $max_estimated, $total_elapsed);
                        echo "</pre>\n";
                    }

                    if ($this->max_execution_time && ($max_estimated >= $this->max_execution_time || $total_elapsed >= $this->max_execution_time)) {
                        if ($this->verbose) {
                            echo "<pre>\n";
                            printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__,
                                'Warning: time allowed is about to be reached, loop aborted', $this->max_execution_time, $max_estimated, $total_elapsed);
                            echo "</pre>\n";
                        }
                        $this->error = printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__,
                            Amazon_Tools::l('Warning: time allowed is about to be reached, loop aborted'), $this->max_execution_time, $max_estimated, $total_elapsed);
                        $this->StopFeed();
                         return false;
                    }

                    $result = $this->amazonApi->GetCompetitivePricingForSKU($slice);
                    if ($this->verbose) {
                        echo "<pre><b>GetCompetitivePricingForSKU Return :</b> \n ";
                        echo print_r($result, true);
                        echo "</pre>\n";
                    }

                    /*11/05/2016*/
                    $products_list = array();
                    foreach ($products as $k => $p){
                        $products_list[$k."-"] = $p;
                    }

                    if ($result instanceof SimpleXMLElement) {

                        // get my price
                        $myPriceForSKU = $this->getMyPriceForSKU($slice);

                        foreach ($result as $getCompetitivePricing) {
                            $getCompetitivePricing->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Products/2011-10-01');
                            $xpath_identifier = $getCompetitivePricing->xpath('xmlns:Product/xmlns:Identifiers/xmlns:SKUIdentifier/xmlns:SellerSKU/text()');
                            $xpath_offer = $getCompetitivePricing->xpath('xmlns:Product/xmlns:CompetitivePricing');

                            if (is_array($xpath_offer))
                                $competitive = reset($xpath_offer);

                            // if GetCompetitivePricingForSKU is Error
                            if (!isset($competitive) || !is_object($competitive) || !property_exists($competitive, 'CompetitivePrices') || !is_array($xpath_identifier)){
                                //if ($this->verbose) {
                                if($getCompetitivePricing['SellerSKU']){

                                    $action = sprintf('No competition');
                                    $reprice = false;
                                    $SellerSKU = trim((string)$getCompetitivePricing['SellerSKU'])."-";

                                    if(!isset($products_list[$SellerSKU]['SKU']))
                                        continue;

                                    if(!isset($products_list[$SellerSKU]['repricing']))
                                        continue;

                                    if(!isset($products_list[$SellerSKU]['originalPrice']))
                                        continue;
                                    
                                    $current_price = $products_list[$SellerSKU]['originalPrice'];
                                    if(isset($products_list[$SellerSKU]['MinPrice']) && isset($products_list[$SellerSKU]['MaxPrice']) ){
                                        $price_min = sprintf('%.02f', round($products_list[$SellerSKU]['MinPrice'], 2));
                                        $price_max = sprintf('%.02f', round($products_list[$SellerSKU]['MaxPrice'], 2));
                                    } else {
                                        $strategy = isset($this->strategies[$products_list[$SellerSKU]['repricing']]) ?
                                            $this->strategies[$products_list[$SellerSKU]['repricing']] : null;
                                        $delta_min = (int) $strategy['delta_min'];
                                        $delta_max = (int) $strategy['delta_max'];
                                        $price_min = sprintf('%.02f', round($current_price * (1 + ($delta_min / 100)), 2));
                                        $price_max = sprintf('%.02f', round($current_price * (1 + ($delta_max / 100)), 2));
                                    }
                                    $last_reprice = isset($products_list[$SellerSKU]['reprice']) ? sprintf('%.02f', round($products_list[$SellerSKU]['reprice'], 2)) : 0;
                                    $current_reprice_mode = isset($products_list[$SellerSKU]['repricing_mode']) ? $products_list[$SellerSKU]['repricing_mode'] : null;
                                    $last_reprice_mode = isset($products_list[$SellerSKU]['last_repricing_mode']) ? $products_list[$SellerSKU]['last_repricing_mode'] : null;
                                    $base_price_limit = $price_min;

                                    if(isset($last_reprice) && $last_reprice > 0){// use lasted reprice when don't have competition
                                        // check mode
                                        // if same use last
                                        if(isset($last_reprice_mode) && isset($current_reprice_mode) && $last_reprice_mode == $current_reprice_mode) {
                                            // if last reprice less then min price : use min price
                                            if($last_reprice <= $price_min){
                                                $safe_price = sprintf('%.02f', max($base_price_limit, $price_min));
                                                $reprice = $safe_price ;
                                                $action = sprintf('F01'); //Get Competitive Pricing Fail (use Min)
                                            // if last reprice grater then max price : use min price
                                            } else if($last_reprice >= $price_max){
                                                $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                                                $reprice = $safe_price;
                                                $action = sprintf('F02'); //Get Competitive Pricing Fail (use Max)
                                            } else {
                                                $safe_price = sprintf('%.02f', max($base_price_limit, $last_reprice));
                                                $reprice = $safe_price;
                                                $action = sprintf('F03'); //Get Competitive Pricing Fail (use last reprice)
                                            }
                                        //if difference use max
                                        } else {
                                            $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                                            $reprice = $safe_price;
                                            $action = sprintf('F04'); //Get Competitive Pricing Fail (use Max - Mode changed)
                                        }
                                    } else { //for new product use max
                                        $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                                        $reprice = $safe_price;
                                        $action = sprintf('F05'); //Get Competitive Pricing Fail (use Max - no last reprice
                                    }

                                    /*if(isset($last_reprice) && $last_reprice > 0){// use lasted reprice when don't have competition
                                        // check mode
                                        // if same use last
                                        if(isset($last_reprice_mode) && isset($current_reprice_mode) && $last_reprice_mode == $current_reprice_mode) {
                                            // if last reprice less then min price : use min price
                                            if($last_reprice < $price_min){
                                                $reprice = sprintf('%.02f', max($last_reprice, $price_min));
                                                $action = sprintf('F01'); //Get Competitive Pricing Fail (use Min)
                                            // if last reprice grater then max price : use max price
                                            } else if($last_reprice > $price_max){
                                                $reprice = sprintf('%.02f', max($last_reprice, $price_max));
                                                $action = sprintf('F02'); //Get Competitive Pricing Fail (use Max)
                                            } else {
                                                $reprice = sprintf('%.02f', $last_reprice);
                                                $action = sprintf('F03'); //Get Competitive Pricing Fail (use last reprice)
                                            }
                                        //if difference use max
                                        } else {
                                            $reprice = sprintf('%.02f', $price_max);
                                            $action = sprintf('F04'); //Get Competitive Pricing Fail (use Max - Mode changed)
                                        }
                                    } else { //for new product original
                                        //$price_max = max($current_price, $price_max);
                                        $reprice = sprintf('%.02f', $price_max);
                                        $action = sprintf('F05'); //Get Competitive Pricing Fail (use Max - no last reprice
                                    }*/

                                    if ($reprice) {

                                        $price_min = sprintf('%.02f', min($reprice, $price_min));
                                        $price_max = sprintf('%.02f', max($reprice, $price_max));

                                        $results_skuItems = array(
                                           $products_list[$SellerSKU]['SKU'] => array(
                                                'SKU' => $products_list[$SellerSKU]['SKU'],
                                                'Price' => $reprice,
                                                'MinPrice' => $price_min,
                                                'MaxPrice' => $price_max,
                                            )
                                        );
                                        $results_report_logs = array(
                                            $products_list[$SellerSKU]['SKU'] => array(
                                                'SKU' => $products_list[$SellerSKU]['SKU'],
                                                'id_product' => $products_list[$SellerSKU]['id_product'],
                                                'id_product_attribute' => isset($products_list[$SellerSKU]['id_product_attribute']) ?
                                                                        $products_list[$SellerSKU]['id_product_attribute'] : 0,
                                                'Price' => $reprice,
                                                'MinPrice' =>  $price_min,
                                                'MaxPrice' => $price_max,
                                                'action' =>  $action,
                                                'repricing_mode' => (isset($products_list[$SellerSKU]['FBA']) && $products_list[$SellerSKU]['FBA']) ? 'FBA' : 'MFN'
                                            )
                                        );
                                    }

                                    if (isset($results_skuItems) && count($results_skuItems)) {
                                        $skuItems = array_merge($skuItems, $results_skuItems);
                                    }
                                    if (isset($results_report_logs) && count($results_report_logs)) {
                                        $report_logs = array_merge($report_logs, $results_report_logs);
                                    }

                                    if ($this->verbose) {
                                        echo "<pre>\n";
                                        echo "Action:<b>" . $action . "</b>\n";
                                        echo "Price:<b>" . $current_price . "</b>\n";
                                        echo "Repriced:" . $reprice . "\n";
                                        echo "Range(min):" . $price_min . "\n";
                                        echo "Range(max):" . $price_max . "\n";
                                        echo "</pre>\n";
                                    }
                                }
                                //} else {
                                //    continue;
                                //}
                            } else {

                                $SellerSKU = (string)reset($xpath_identifier);
                                //$SKU = $SellerSKU;
                                $SellerSKU = trim($SellerSKU)."-";

                                if(!isset($history['CompetitivePricing'][$SellerSKU])){

                                    $history['CompetitivePricing'][$SellerSKU] = true;
                                    $CompetitivePrices = $competitive->CompetitivePrices;

                                    if ($this->verbose) {
                                        echo "<pre><b>SellerSKU :</b>";
                                        var_dump($SellerSKU);
                                        echo "</pre>\n";
                                    }

                                    if(isset($products_list[$SellerSKU]) && !empty($products_list[$SellerSKU])){

                                        // if have getMyPriceBySKU use the listing  & shipping Amount to calculate
                                        if(!isset($products_list[$SellerSKU]['ShippingOverride'])){
                                            if(isset($myPriceForSKU[$SellerSKU]['shippingPrice']) && !empty($myPriceForSKU[$SellerSKU]['shippingPrice'])) {
                                                $products_list[$SellerSKU]['ShippingOverride'] = $myPriceForSKU[$SellerSKU]['shippingPrice'] ;
                                            } else {
                                                $products_list[$SellerSKU]['ShippingOverride'] = $defaut_override_shipping;
                                            }
                                        }

                                        if ($this->verbose) {
                                            echo "<pre><b>Shipping Override :</b>";
                                            var_dump($products_list[$SellerSKU]['ShippingOverride']);
                                            echo "</pre>\n";
                                        }

                                        $results = $this->repricingCompetitivePrice($CompetitivePrices, $products_list[$SellerSKU]);

                                        if (is_array($results['skuItems']) && count($results['skuItems'])) {
                                            $skuItems = array_merge($skuItems, $results['skuItems']);
                                        }
                                        if (isset($results['report_logs']) && is_array($results['report_logs']) && count($results['report_logs'])) {
                                            $report_logs = array_merge($report_logs, $results['report_logs']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $sent_to_queue = false;
        if (is_array($skuItems)){
            $sent_to_queue = $this->sendToQueue($skuItems, $report_logs);
        }

	if(isset($report_logs) && $sent_to_queue){
	    $this->repricing_report($report_logs, $this->database, 'update_option', AmazonDatabase::REPRICING);
	}

        if($sent_to_queue) {
            return $report_logs;
        } else {
            return false;
        }
    }

    public function checkProcessRunning($action = 'analysis', $process_time=1){
        $process_type = AmazonDatabase::REPRICING . '_'.$action.'_' . $this->params->countries;
        $process = $this->proc_rep->has_other_running($process_type);
        if ($process) {
            if($process_time > 5){
                return (false);
            }
            sleep(1*60);
            $process_time++;
            $this->checkProcessRunning($action, $process_time);
        }
        return (true);
    }

    public function repricingCompetitivePrice($Competitives, $products){

        if(isset($products['parent']) && $products['parent']){
            file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/repricingCompetitivePrice_parent.txt',print_r(array($products['SKU']),true), FILE_APPEND);
            return;
        }

        if(!isset($products['price']) || $products['price'] <= 0){
            file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/repricingCompetitivePrice_price.txt',print_r(array($products['SKU']),true), FILE_APPEND);
            return;
        }

        if(!isset($products['originalPrice']) || $products['originalPrice'] <= 0){
            file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/repricingCompetitivePrice_originalPrice.txt',print_r(array($products['SKU']),true), FILE_APPEND);
            return;
        }

        if(!isset($products['repricing']) || empty($products['repricing'])){
            file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/repricingCompetitivePrice_repricing.txt',print_r(array($products['SKU']),true), FILE_APPEND);
            return;
        }

        $skuItems = $skuItemsNoCompetition = $report_logs = array();
        $reprice = null;
        $id_product = $products['id_product'];
        $id_product_attribute = isset($products['id_product_attribute']) ? $products['id_product_attribute'] : 0;

        $price = $products['originalPrice'];
        $value_added = $fba_value_added = 0;

        // Override FBA Value Added
        if (isset($products['fba_value_added']) && !empty($products['fba_value_added'])) {

            $value_added = (float) $products['fba_value_added'];
            //$fba_value_added = (float) $products['fba_value_added'];

            if ($this->verbose) {
                echo "<pre>\n";
                printf('FBA value add for product %d/%d: %d', $id_product, $id_product_attribute, (float) $products['fba_value_added']);
                echo "</pre>\n";
            }

        } else {

            if (!isset($products['quantity']) || $products['quantity'] <= 0) {
                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('No stock for product %d/%d, skipped....', $id_product, $id_product_attribute);
                    echo "</pre>\n";
                }
                return(false);
            }
        }

	// Product price is include FBA already
        $base_price = null;
        $current_price = sprintf('%.02f', round($price, 2));
        $last_reprice = isset($products['reprice']) ? sprintf('%.02f', round($products['reprice'], 2)) : 0;
        $current_reprice_mode = isset($products['repricing_mode']) ? $products['repricing_mode'] : null;
        $last_reprice_mode = isset($products['last_repricing_mode']) ? $products['last_repricing_mode'] : null;

        if ($this->verbose) {
            echo "<pre>\n";
            echo "current_price:<b>" . $current_price . "</b>\n";
            echo "last_reprice:<b>" . $last_reprice . "</b>\n";
            echo "current_reprice_mode:" . $current_reprice_mode . "\n";
            echo "last_reprice_mode:" . $last_reprice_mode . "\n";
            echo "</pre>\n";
        }

        $strategy = isset($this->strategies[$products['repricing']]) ? $this->strategies[$products['repricing']] : null;

        if(isset($strategy['base'])){
            switch ($strategy['base']) {
                case AmazonRepricing::REPRICING_WHOLESALE_PRICE:
                    if (!isset($products['wholesale_price']) || empty($products['wholesale_price'])) {
                        if ($this->verbose) {
                            echo "<pre>\n";
                            printf('Missing wholesale price id_product:%d', $id_product);
                            echo "</pre>\n";
                        }
                        continue;
                    }
                    $base_price = $products['wholesale_price'];
                    break;
                case AmazonRepricing::REPRICING_REGULAR_PRICE:
                    $base_price = $current_price;
                    break;
                default:
                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('Repricing base is not selected for strategy: %s', $strategy['name']);
                        echo "</pre>\n";
                    }
                    return(false);
            }
        }

        if (!$base_price) {
            if ($this->verbose) {
                echo "<pre>\n";
                printf('Missing base price for product: %d/%d', $id_product, $id_product_attribute);
                echo "</pre>\n";
            }
            return(false);
        }

        $agressivity = max(1, (int) $strategy['aggressivity']);
        $limit = (int) $strategy['limit'];

	if(isset($products['MinPrice']) && isset($products['MaxPrice']) ){

	    $strategy_override = true;

	    $delta_min = 0;
            $delta_max = 0;

            $price_min = sprintf('%.02f', round($products['MinPrice'], 2));
            $price_max = sprintf('%.02f', round($products['MaxPrice'], 2));

	} else {
            $delta_min = (int) $strategy['delta_min'];
            $delta_max = (int) $strategy['delta_max'];

            $price_min = sprintf('%.02f', round($current_price * (1 + ($delta_min / 100)), 2));
            $price_max = sprintf('%.02f', round($current_price * (1 + ($delta_max / 100)), 2));
        }

        $price_max += $value_added;
        $price_min += $value_added;
        $base_price += $value_added;

        $base_price_limit = sprintf('%.02f', round($base_price * (1 + ($limit / 100)), 2));

        if (isset($strategy_override) && $strategy_override && $price_min > 0) // Overrides limit
	    $base_price_limit = sprintf('%.02f', round($price_min, 2));

        if (!is_numeric($limit) || $base_price_limit <= 0)
            $base_price_limit = $price_min;

        $belongsToMe = false;
        if(isset($Competitives->CompetitivePrice)){
            $belongsToMe = (string)$Competitives->CompetitivePrice->attributes()->belongsToRequester == 'false' ? false :  true;
        }

        if(!$belongsToMe) {
            $calculated = $this->getBestCompititvePrice($Competitives, $products, $agressivity, $fba_value_added);
        } else {
            $calculated = sprintf('%.02f', max($base_price_limit, $price_max));
        }

        if (!$calculated) {

            $safe_price = sprintf('%.02f', max($base_price_limit, $current_price));
            $action = sprintf('No competition, skipping offer...', $current_price);
            $reprice = false;

	    // if last repricing != current_price
	    //if($current_price != $last_reprice) {
            if(isset($last_reprice) && $last_reprice > 0){// use lasted reprice when don't have competition
                // check mode
                // if same use last
                if(isset($last_reprice_mode) && $last_reprice_mode == $current_reprice_mode) {
                    // if last reprice less then min price : use min price
                    if($last_reprice <= $price_min){
                        $safe_price = sprintf('%.02f', max($base_price_limit, $price_min));
                        $reprice = $safe_price ; /*sprintf('%.02f', max($last_reprice, $safe_price));*/
                    // if last reprice grater then max price : use min price
                    } else if($last_reprice >= $price_max){
                        $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                        $reprice = $safe_price;
                    } else {
                        $safe_price = sprintf('%.02f', max($base_price_limit, $last_reprice));
                        $reprice = $safe_price;
                    }
                //if difference use max
                } else {
                    $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
                    $reprice = $safe_price;
                }
            } else { //for new product use max
                $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
		$reprice = $safe_price;
	    }

        } elseif ($calculated <= $base_price_limit || $calculated <= $price_min) {
            $safe_price = sprintf('%.02f', max($base_price_limit, $price_min));
            $action = sprintf('Sending Price Min.: %.02f', $safe_price);
            $reprice = $safe_price;
        } elseif ($calculated >= $price_max) {
            $safe_price = sprintf('%.02f', max($base_price_limit, $price_max));
            $action = sprintf('Sending Price Max.: %.02f', $safe_price);
            $reprice = $safe_price;
        } else {
            $safe_price = sprintf('%.02f', max($base_price_limit, $calculated));
            $action = 'Repriced';
            $reprice = $safe_price;
        }

        $smart_price = sprintf('%.02f', $reprice);
        $price_min = sprintf('%.02f', $price_min);
        $price_max = sprintf('%.02f', $price_max);

        if ($this->verbose) {
            $text_delta_min = $delta_min ? ($delta_min > 0 ? sprintf(' (+%02d&#37;)', $delta_min) : sprintf(' (%02d&#37;)', $delta_min)) : null;
            $text_delta_max = $delta_max ? ($delta_max > 0 ? sprintf(' (+%02d&#37;)', $delta_max) : sprintf(' (%02d&#37;)', $delta_max)) : null;
            echo "<pre>\n";
            echo "Action:<b>" . $action . "</b>\n";
            echo "Final Price:<b>" . $smart_price . "</b>\n";
            echo "Repriced:" . $reprice . "\n";
            echo "Calculated:" . $calculated . "\n";
            echo "Base Price:" . $current_price . "\n";
            echo "Range(min):" . $price_min . $text_delta_min . "\n";
            echo "Range(max):" . $price_max . $text_delta_max . "\n";
            echo "Base:" . $base_price . "\n";
            echo "Limit:" . $base_price_limit . ($limit > 0 ? sprintf(' (+%02d&#37;)', $limit) : sprintf(' (%02d&#37;)', $limit)) . "\n";
            echo "Strategy Source:" . 'Profile' . "\n";
            echo "belongsToRequester:" . $belongsToMe . "\n";
            echo "</pre>\n";
        }

        /*if((int)$this->params->id_country == 2 && $products['SKU'] == "3700936106131") {
            $details = array(
                "Date Time" => date('Y-m-d H:i:s') ,
                "Action" => $action ,
                "Final Price:" =>$smart_price ,
                "Repriced" =>$reprice  ,
                "Calculated" =>$calculated ,
                "Base Price" =>$current_price ,
                "Range(min)" =>$price_min . $text_delta_min  ,
                "Range(max)" =>$price_max . $text_delta_max  ,
                "Base" => $base_price  ,
                "Limit" =>$base_price_limit . ($limit > 0 ? sprintf(' (+%02d&#37;)', $limit) : sprintf(' (%02d&#37;)', $limit)) ,
                "belongsToRequester" =>$belongsToMe
            );
            file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/amazon/reprice_'.$products['SKU'].'.txt',print_r(array('Type'=>$this->action,'UrlQueueOut'=>$this->UrlQueueOut,'details'=> $details),true), FILE_APPEND);
        }*/

        if ($reprice) {

	    if (isset($price_min) && (float) $smart_price) {
		$price_min = min($price_min, $smart_price);
	    }

	    if (isset($price_max) && (float) $smart_price) {
		$price_max = max($price_max, $smart_price);
	    }

            $skuItems[$products['SKU']]['SKU'] = $products['SKU'];
            $skuItems[$products['SKU']]['Price'] = sprintf('%.02f', $smart_price);
	    $skuItems[$products['SKU']]['MinPrice'] = sprintf('%.02f', $price_min);
	    $skuItems[$products['SKU']]['MaxPrice'] = sprintf('%.02f', $price_max);

	    $report_logs[$products['SKU']]['SKU'] = $products['SKU'];
	    $report_logs[$products['SKU']]['id_product'] = $id_product;
	    $report_logs[$products['SKU']]['id_product_attribute'] = $id_product_attribute;
	    $report_logs[$products['SKU']]['Price'] = sprintf('%.02f', $smart_price);
	    $report_logs[$products['SKU']]['MinPrice'] = sprintf('%.02f', $price_min);
	    $report_logs[$products['SKU']]['MaxPrice'] = sprintf('%.02f', $price_max);
	    $report_logs[$products['SKU']]['action'] = $action;
	    $report_logs[$products['SKU']]['repricing_mode'] = (isset($products['FBA']) && $products['FBA']) ? 'FBA' : 'MFN';
        }

        return(array('skuItems' => $skuItems, 'report_logs' => $report_logs));
    }

    public function getBestCompititvePrice($Competitives, $products, $agressivity_param, $value_added = 0){

        $agressivities = array();

        for ($i = 10, $rindex = 10; $i < 110; $i += 10, $rindex--) {
            $calculated_agressivity = round($agressivity_param / $rindex, 2);
            $agressivities[$i] = $calculated_agressivity;
        }

        //$myShipping = 0;
	$myShipping = isset($products['ShippingOverride']) ? (float)$products['ShippingOverride'] : 0;
        $myPrice = (float)$products['price'] + $value_added;
        $myCondition = isset($products['condition']) ? (string)$products['condition'] : null;

        $imFulfilledByAmazon = isset($products['FBA']) && isset($products['fba_value_added']) ? true : false;
        $imFeaturedMerchant = $imFulfilledByAmazon ? false : true;

        $bestSellerPrice = 0;
        $calculated_price = null;
        $cheaper_price = 0;
        $cheaper_shipping = 0;
        $bestOffer = null;

        foreach ($Competitives as $CompetitivePrices) {

            foreach ($CompetitivePrices as $CompetitivePrice) {

                $ItemCondition = (string) $CompetitivePrice['condition'];
                $sellerPrice = (float) $CompetitivePrice->Price->ListingPrice->Amount;
                $sellerShipping = (float) $CompetitivePrice->Price->Shipping->Amount;

                if (strtoupper($myCondition) != strtoupper($ItemCondition)){
                   continue;
                }

                if ($sellerPrice + $sellerShipping < $bestSellerPrice){
                   $bestSellerPrice = $sellerPrice + $sellerShipping;
                }

                if ($sellerPrice + $sellerShipping < $cheaper_price + $cheaper_shipping || $cheaper_price + $cheaper_shipping == 0) {
                   if (!$bestOffer) {
                       $bestOffer = $CompetitivePrice;
                   }

                   $cheaper_price = $sellerPrice;
                   $cheaper_shipping = $sellerShipping;
               }
            }
        }

        if (!$bestOffer){
            $bestOffer = $CompetitivePrices[0];
        }

        if ($this->debug) {
            echo "<pre>Best Offer:\n";
            print_r($bestOffer);
            echo "</pre>\n";
            echo "<pre>\n";
            printf('Cheaper Price: %.02f' . "\n", $cheaper_price);
            echo "</pre>\n";
        }

        $score = 100;

        if ($bestOffer) {
            $sellerPrice = isset($bestOffer->Price->ListingPrice->Amount) ? (float) $bestOffer->Price->ListingPrice->Amount : 0;
            $sellerShipping = isset($bestOffer->Price->Shipping->Amount) ? (float) $bestOffer->Price->Shipping->Amount : 0;

            if ($sellerPrice && $sellerPrice + $sellerShipping < $myPrice + $myShipping)
                $score -= 10;

            if ($sellerShipping == 0)
                $score -= 10;

        }

        $myscore = 100;

        $myscore -= !$imFulfilledByAmazon ? 30 : 0;
        $myscore -= !$imFeaturedMerchant ? 20 : 0;
        $myscore -= $bestSellerPrice && $bestSellerPrice < $myPrice + $myShipping ? 10 : 0;
        $myscore -= $myShipping > 0 && !$imFulfilledByAmazon ? 10 : 0;

        $base_score = $score;
        $base_price = $cheaper_price;
        $base_shipping = $cheaper_shipping;

        if ($this->debug) {
            echo "<pre>\n";
            printf('Base Price: %.02f, Base Shipping: %.02f' . "\n", $base_price, $base_shipping);
            echo "</pre>\n";
        }

        if ($base_price) {
            // Adjust to minimum
            $score_gap = $base_score - $myscore;
            $agressivity_level = 0;

            if ($myscore < $base_score) {
                // Adjust agressivly
                foreach ($agressivities as $agressivity_level => $agressivity) {
                    if ($score_gap <= $agressivity_level){
                        break;
                    }
                }
            }
            else { // Minimum Agressivity
                $agressivity = reset($agressivities);
                $agressivity_level = key($agressivities);
            }
            $agressivity /= 100;

            $shipping_diff = $myShipping - $base_shipping;
            $raw_price = ($base_price - $shipping_diff);
            $calculated_price = sprintf('%.02f', ($raw_price / (1 + $agressivity)));

            if ($this->verbose) {
                echo "<pre>";
                printf('SKU: %s, agressivity: %d' . "\n", $products['SKU'], $agressivity_param);
                if ($this->debug)
                    print_r($agressivities);
                printf('Competition on Price: %.02f, Shipping: %.02f, Price+Shipping: %.02f, Score: %d' . "\n", $base_price, $base_shipping, $base_price + $base_shipping, $base_score);
                printf('My Price: %.02f Shipping: %.02f, Price+Shipping: %.02f, My Score: %d, Agressivity: Level: %d / Rate: %.04f, Calculated: %.02f' . "\n", $myPrice, $myShipping, $myPrice + $myShipping, $myscore, $agressivity_level, $agressivity, $calculated_price);
                printf('<b>Competition Result: %.02f against %.02f</b>' . "\n", $calculated_price + $myShipping, $base_price + $base_shipping);
                echo "</pre>\n";
            }
        }
        return($calculated_price);

    }

    public function getMyPriceForSKU($SKUs) {

        // Third Loop - Grab SKU from Product API
        $probeCount = $i = 0;
        $amazonItems = $ASINerrors = array();
        //$loop_start_time = microtime(true);
        //$slices = array_chunk($SKUs, 20);
        //if (count($slices)) {
            //foreach ($slices as $slice){
                /*$i++;
                $loop_average = (microtime(true) - $loop_start_time) / ($i + 1);
                $total_elapsed = microtime(true) - $this->script_start_time;
                $max_estimated = (($loop_start_time - $this->script_start_time) + $loop_average * $i * 1.4);

                if ($this->verbose) {
                    echo "<pre>\n";
                    printf('Loop average: %.02f, Max estimated: %.02f, Total Elapsed: %.02f' . "\n", $loop_average, $max_estimated, $total_elapsed);
                    echo "</pre>\n";
                }

                if ($this->max_execution_time && ($max_estimated >= $this->max_execution_time || $total_elapsed >= $this->max_execution_time)) {
                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__,
                            'Warning: time allowed is about to be reached, loop aborted', $this->max_execution_time, $max_estimated, $total_elapsed);
                        echo "</pre>\n";
                    }
                    $this->error = printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__,
                        Amazon_Tools::l('Warning: time allowed is about to be reached, loop aborted'), $this->max_execution_time, $max_estimated, $total_elapsed);
                    $this->StopFeed();
                }*/

                $result = $this->amazonApi->GetMyPriceForSKU($SKUs);
                //$result = new SimpleXMLElement(file_get_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/GetMyPriceForSKU.xml'));

                if ($result instanceof SimpleXMLElement) {

                    foreach ($result as $getMyPriceItem) {
                        $probeCount++;
                        $getMyPriceItem->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Products/2011-10-01');
                        $xpath_offer = $getMyPriceItem->xpath('xmlns:Product/xmlns:Offers');

                        if (is_array($xpath_offer))
                            $offer = reset($xpath_offer);

                        if (!isset($offer) || !is_object($offer) || !property_exists($offer, 'Offer'))
                            continue;

                        $SellerSKU = (string) $offer->Offer->SellerSKU;

                        if (!strlen($SellerSKU) || !Amazon_Tools::ValidateSKU($SellerSKU))
                            continue;

                        $SellerSKU = $SellerSKU . "-";

                        $landedPrice = isset($offer->Offer->BuyingPrice->LandedPrice->Amount) ? (string) $offer->Offer->BuyingPrice->LandedPrice->Amount : 0;
                        $listingPrice = isset($offer->Offer->BuyingPrice->ListingPrice->Amount) ? (string) $offer->Offer->BuyingPrice->ListingPrice->Amount : 0;
                        $shippingPrice = isset($offer->Offer->BuyingPrice->Shipping->Amount) ? (string) $offer->Offer->BuyingPrice->Shipping->Amount : 0;

                        if($listingPrice > 0) {
                            //$amazonItems[$SellerSKU]['sku'] = $SellerSKU;
                            $amazonItems[$SellerSKU]['listingPrice'] = $listingPrice;
                            $amazonItems[$SellerSKU]['shippingPrice'] = $shippingPrice;
                        }
                    }
                }
            //}
        //}

        if ($this->verbose) {
            echo "<pre>\n";
            echo "getMyPriceBySKU :\n";
            var_dump($amazonItems);
            echo "</pre>\n";
        }

        return $amazonItems ;
    }

    public function checkForASINs($ASINs) {

        // Third Loop - Grab SKU from Product API and fill the database
        $probeCount = 0;
        $ASINresolutions = $ASINerrors = $history = array();
        $loop_start_time = microtime(true);

        // IF don't have ASIN
        $missing_asins = $this->database->getIdByAsins($this->params->id_country, $this->params->id_shop, $ASINs);

        if (!empty($missing_asins)) {
	     file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'missing_asins'=>$missing_asins), true), FILE_APPEND);

			// GET INVENTORY to check exits product
			$ProductsInventory = $this->database->getProductsInventory($this->params->id_country, $this->params->id_shop);

	    //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'ProductsInventory'=>$ProductsInventory), true), FILE_APPEND);
            $i = 0;
            $slices = array_chunk($missing_asins, 20);

            if (count($slices)) {
                foreach ($slices as $slice){
                    $i++;
                    $loop_average = (microtime(true) - $loop_start_time) / ($i + 1);
                    $total_elapsed = microtime(true) - $this->script_start_time;
                    $max_estimated = (($loop_start_time - $this->script_start_time) + $loop_average * $i * 1.4);

                    if ($this->verbose) {
                        echo "<pre>\n";
                        printf('Loop average: %.02f, Max estimated: %.02f, Total Elapsed: %.02f' . "\n", $loop_average, $max_estimated, $total_elapsed);
                        echo "</pre>\n";
                    }

                    if ($this->max_execution_time && ($max_estimated >= $this->max_execution_time || $total_elapsed >= $this->max_execution_time)) {
                        if ($this->verbose) {
                            echo "<pre>\n";
                            printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__, 'Warning: time allowed is about to be reached, loop aborted', $this->max_execution_time, $max_estimated, $total_elapsed);
                            echo "</pre>\n";
                        }
                        $this->error = printf('%s(%d): %s (%d/%d/%d)', basename(__FILE__), __LINE__, Amazon_Tools::l('Warning: time allowed is about to be reached, loop aborted'), $this->max_execution_time, $max_estimated, $total_elapsed);

                        file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'out put'=>$this->error), true), FILE_APPEND);
                        $this->StopFeed();
                    }

                    $result = $this->amazonApi->GetMyPriceForASIN($slice);
                    //$result = new SimpleXMLElement(file_get_contents(dirname(__FILE__).'/../orderImport/getPriceByASIN.xml'));

                    if ($result instanceof SimpleXMLElement) {

                        $amazonItems = array();

                        foreach ($result as $getMyPriceItem) {
                            $probeCount++;
                            $getMyPriceItem->registerXPathNamespace('xmlns', 'http://mws.amazonservices.com/schema/Products/2011-10-01');
                            $xpath_identifier = $getMyPriceItem->xpath('xmlns:Product/xmlns:Identifiers/xmlns:MarketplaceASIN/xmlns:ASIN/text()');
                            $xpath_offer = $getMyPriceItem->xpath('xmlns:Product/xmlns:Offers');

                            if (is_array($xpath_offer))
                                $offer = reset($xpath_offer);

                            if (!isset($offer) || !is_object($offer) || !property_exists($offer, 'Offer') || !is_array($xpath_identifier))
                                continue;

                            $ASIN = (string) reset($xpath_identifier);
                            $SellerSKU = (string) $offer->Offer->SellerSKU;

                            if (!strlen($SellerSKU) || !Amazon_Tools::ValidateSKU($SellerSKU))
                                continue;

                            $price = isset($offer->Offer->RegularPrice->Amount) ? (string) $offer->Offer->RegularPrice->Amount : 0;
                            $ASINresolutions[$ASIN] = $SellerSKU;
                            $amazonItems[$SellerSKU]['sku'] = $SellerSKU;
                            $amazonItems[$SellerSKU]['asin'] = $ASIN;
                            $amazonItems[$SellerSKU]['price'] = $price;
                        }
						// if exits product update asin to amazon_report_inventiry
                        if (!empty($amazonItems) && isset($ProductsInventory[$SellerSKU]) && !empty($ProductsInventory[$SellerSKU])) {
                            if (!$this->database->save('amazon_report_inventory', $amazonItems, $this->params->id_country, $this->params->id_shop, false)) {
                                $ASINerrors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to save report'));
                                file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/RetrieveMessages.txt',  print_r(array('line'=>__LINE__,'date'=>date('Y-m-d H:i:s'),'ASINerrors'=>$ASINerrors), true), FILE_APPEND);
                            }
                        }
                    }
                }
            }
        }
        if (count($ASINresolutions) && $this->verbose) {
            echo "<pre>\n";
            echo "SKU/ASIN solved:\n";
            var_dump($ASINresolutions);
            echo "</pre>\n";
        }
    }

    public function CheckService() {
        // Retrieve or Create our queues
        if (!($queueUrls = $this->createQueues($this->awsKeyId, $this->awsSecretKey, $this->params->iso_code))) {
            $this->error = sprintf(Amazon_Tools::l('Error: Unable to list or create queues - %s - Queues: %s'), $this->params->iso_code, nl2br(print_r($queueUrls, true)));
            $this->StopFeed();
            exit;
        }

        if ($queueUrls && $this->debug) {
            echo "<pre>\n" . printf('%s(#%d): Amazon - Available Queues: %s', basename(__FILE__), __LINE__, print_r($queueUrls, true)) . "</pre>\n";
        }

        if (!is_array($queueUrls) || !array_key_exists(AmazonRepricing::INPUT_QUEUE, $queueUrls) || !array_key_exists(AmazonRepricing::OUTPUT_QUEUE, $queueUrls)) {
            $this->error = sprintf(Amazon_Tools::l('Error: Missing expected queues - %s '), nl2br(print_r($queueUrls, true)));
            $this->StopFeed();
            exit;
        }

        $target_inqueue_url = $queueUrls[AmazonRepricing::INPUT_QUEUE];
        $target_inqueue_name = $this->getQueueName($this->params->iso_code, AmazonRepricing::INPUT_QUEUE);
        $target_outqueue_url = $queueUrls[AmazonRepricing::OUTPUT_QUEUE];
        $registered_destinations = $this->listRegisteredDestinations($this->amazonApi);
        $registered_queue = false;

        // Queue is already registered, checking if this is our own.
        if (is_array($registered_destinations) && count($registered_destinations)) {
            foreach ($registered_destinations as $queue_name => $queue_url) {
                if (strpos($queue_url, AmazonSQS::ENDPOINT_US_EAST) === false)
                    continue;

                if ($target_inqueue_name == $queue_name)
                    $registered_queue = true;
            }
            if ($registered_queue && $this->debug) {
                echo "<pre>\n";
                printf('%s(#%d): Amazon - Queue already registered: %s', basename(__FILE__), __LINE__, $target_inqueue_name);
                echo "</pre>\n";
            }
        }

        // Our Queue is not yet registered, register
        if (!$registered_queue) {
            $registration_result = $this->registerDestination($this->amazonApi, $target_inqueue_url);
            if ($registration_result == false) {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed to register: INPUT_QUEUE - %s '), $target_inqueue_url);
                $this->StopFeed();
                exit;
            }
        }

        // Queue is existing or Created, testing queue
        $testQueueResult = $this->testQueue($this->amazonApi, $target_inqueue_url);
        $awsAccountId = null;
        if (isset($testQueueResult->Error) && isset($testQueueResult->Error->Code)) {
            $errorMsg = (string) $testQueueResult->Error->Message;
            $errorCode = (string) $testQueueResult->Error->Code;
            $pass = false;

            // Catch the case: User has no permission to write to the queue:
            // SQS queue 'https://sqs.us-east-1.amazonaws.com/xx/AmazonPrestashopIQ-fr' does not exist or AWS Account '456465' is not authorized to it
            if ($errorCode == 'DependencyFatalException') {
                if (preg_match("/AWS\sAccount\s'([0-9]*)'\sis\snot\sauthorized\sto\sit/", $errorMsg, $result) == 1) {
                    $awsAccountId = (string) $result[1];
                    $pass = true;

                    if ($this->debug) {
                        echo "<pre>\n";
                        printf('%s(#%d): Amazon - AWS Account ID caught: %s', basename(__FILE__), __LINE__, $awsAccountId);
                        echo "</pre>\n";
                    }
                }
            }
            if (!$pass) {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed to test queue, Amazon returned: %s '), $errorMsg);
                $this->StopFeed();
            }
        }

        // Granting Permissions:
        // We got an account ID, we need to give the permission from AWS to MWS to write into the Queue
        if ($awsAccountId) {
            if (!$this->setQueuePermission($this->awsKeyId, $this->awsSecretKey, $target_inqueue_url, $awsAccountId, 'SendMessage')) {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed to grant permission to queue: %s '), $target_inqueue_url);
                $this->StopFeed();
            }
            if (!$this->setQueuePermission($this->awsKeyId, $this->awsSecretKey, $target_outqueue_url, $awsAccountId, 'SendMessage')) {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed to grant permission to queue: %s '), $target_outqueue_url);
                $this->StopFeed();
            }
            if (!$this->setQueuePermission($this->awsKeyId, $this->awsSecretKey, $target_outqueue_url, $awsAccountId, 'ReceiveMessage')) {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed to grant permission to queue: %s '), $target_outqueue_url);
                $this->StopFeed();
            }
        }

        // Check if our Subscribption exists
        if (!$this->checkSubscription($this->amazonApi, $target_inqueue_url)) {
            // Create subscribption to that queue
            if ($this->createSubscription($this->amazonApi, $target_inqueue_url)) {
                $this->message = Amazon_Tools::l('Repricing service has been configured and activated with success');
                $this->StopFeed();
            } else {
                $this->error = sprintf(Amazon_Tools::l('Error: Failed create subscription:  %s '), $target_inqueue_url);
                $this->StopFeed();
            }
        } else {
            $this->message = Amazon_Tools::l('Repricing service is already configured and well activated');
            $this->StopFeed();
        }
    }

    public function CancelService() {
        $pass = true;

        // Retrieve or Create our queues
        if (!($queueUrls = $this->listQueues($this->awsKeyId, $this->awsSecretKey/*, $this->params->iso_code*/))) {
            if (!isset($queueUrls['Queues'])) {
                $this->error = sprintf(Amazon_Tools::l('Error: Unable to list or create queues - %s - Queues: %s'), $this->params->iso_code, nl2br(print_r($queueUrls, true)));
                $this->StopFeed();
            }
        }

        if (isset($queueUrls['Queues']) && $this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): Amazon - Available Queues: %s', basename(__FILE__), __LINE__, print_r($queueUrls, true));
            echo "</pre>\n";
        }

        $target_queue_url = null;
        $target_queue_name = null;

        // Filter Target Queues
        if (isset($queueUrls['Queues'])) {
            foreach ($queueUrls['Queues'] as $queue_url) {
                $url = trim(dirname(dirname($queue_url)));
                $queue = trim(basename($queue_url));

                if (strpos(AmazonSQS::ENDPOINT_US_EAST, $url) === false)
                    continue;

                if ($queue == $this->getQueueName($this->params->iso_code, AmazonRepricing::INPUT_QUEUE)) {
                    $target_queue_url = $queue_url;
                    $target_queue_name = $queue;
                    break;
                }
            }
        }
        $registered_destinations = $this->listRegisteredDestinations($this->amazonApi);
        $registered_queue = false;

        // Queue is already registered, checking if this is our own.
        if (is_array($registered_destinations) && count($registered_destinations)) {
            foreach ($registered_destinations as $queue_name => $queue_url) {
                if (strpos($queue_url, AmazonSQS::ENDPOINT_US_EAST) === false)
                    continue;

                if ($target_queue_name == $queue_name)
                    $registered_queue = true;
            }
            if ($registered_queue && $this->debug) {
                echo "<pre>\n";
                printf('%s(#%d): Amazon - Queue well registered: %s', basename(__FILE__), __LINE__, $target_queue_name);
                echo "</pre>\n";
            }
        }

        // Check if our Subscribption exists
        if ($target_queue_url && $this->checkSubscription($this->amazonApi, $target_queue_url)) {
            // Create subscribption to that queue
            if (!$this->deleteSubscription($this->amazonApi, $target_queue_url))
                $pass = false;
        }

        // Deregister Destinations
        if ($registered_queue) {
            if (!$this->deregisterDestination($this->amazonApi, $target_queue_url))
                $pass = false;
        }

        if (!$pass) {
            $this->error = Amazon_Tools::l('Failed to unsubscribe the service');
            $this->StopFeed();
        } elseif ($registered_queue && $target_queue_url) {
            $this->message = Amazon_Tools::l('Repricing service unsubscribed with success');
            $this->StopFeed();
        } else {
            $this->message = Amazon_Tools::l('Repricing service was already unsubscribed');
            $this->StopFeed();
        }
    }

    public function CheckQueues() {

        $this->setAwsSettings();

        // Retrieve or Create our queues
        if (!($queueUrls = $this->listQueues($this->awsKeyId, $this->awsSecretKey/*, $this->params->iso_code*/))) {
            if (!isset($queueUrls['Queues'])) {
                $this->error = sprintf(Amazon_Tools::l('Error: Unable to list or create queues - %s - Queues: %s'), $this->params->iso_code, nl2br(print_r($queueUrls, true)));
                $this->StopFeed();
            }
        }

        if (isset($queueUrls['Queues']) && $this->debug) {
            echo "<pre>\n";
            printf('%s(#%d): Amazon - Available Queues: %s', basename(__FILE__), __LINE__, print_r($queueUrls, true));
            echo "</pre>\n";
        }

        $target_in_queue_url = null;
        $target_in_queue_name = null;
        $target_out_queue_url = null;
        $target_out_queue_name = null;

        // Filter Target Queues
        if (isset($queueUrls['Queues'])) {
            foreach ($queueUrls['Queues'] as $queue_url) {
                $url = trim(dirname(dirname($queue_url)));
                $queue = trim(basename($queue_url));

                if (strpos(AmazonSQS::ENDPOINT_US_EAST, $url) === false)
                    continue;

                if ($queue == $this->getQueueName($this->params->iso_code, AmazonRepricing::INPUT_QUEUE)) {
                    $target_in_queue_url = $queue_url;
                    $target_in_queue_name = $queue;
                } elseif ($queue == $this->getQueueName($this->params->iso_code, AmazonRepricing::OUTPUT_QUEUE)) {
                    $target_out_queue_url = $queue_url;
                    $target_out_queue_name = $queue;
                }
            }
        }

        $sqs = new AmazonSQS($this->awsKeyId, $this->awsSecretKey);

        if ($target_in_queue_url) {
            $nMessagesIn = $this->countMessages($sqs, $target_in_queue_url);

            if (!is_int($nMessagesIn) && is_bool($nMessagesIn)) {
                $this->error = sprintf(Amazon_Tools::l('ERROR, unable to getQueueAttributes for queue %s'), $target_in_queue_url);
                $this->StopFeed();
            }
        } else {
            $nMessagesIn = 0;
        }

        if ($target_out_queue_url) {
            $nMessagesOut = $this->countMessages($sqs, $target_out_queue_url);

            if (!is_int($nMessagesOut) && is_bool($nMessagesOut)) {
                $this->error = sprintf(Amazon_Tools::l('ERROR, unable to getQueueAttributes for queue %s'), $target_out_queue_url);
                $this->StopFeed();
            }
        } else {
            $nMessagesOut = 0;
        }

        $view_params = array();
        $view_params[AmazonRepricing::INPUT_QUEUE] = array();
        $view_params[AmazonRepricing::INPUT_QUEUE]['name'] = $target_in_queue_name;
        $view_params[AmazonRepricing::INPUT_QUEUE]['url'] = $target_in_queue_url;
        $view_params[AmazonRepricing::INPUT_QUEUE]['count'] = $nMessagesIn;

        $view_params[AmazonRepricing::OUTPUT_QUEUE] = array();
        $view_params[AmazonRepricing::OUTPUT_QUEUE]['name'] = $target_out_queue_name;
        $view_params[AmazonRepricing::OUTPUT_QUEUE]['url'] = $target_out_queue_url;
        $view_params[AmazonRepricing::OUTPUT_QUEUE]['count'] = $nMessagesOut;

        if ($target_in_queue_url) {

            if (!isset($this->smarty) || !$this->smarty) {
                global $smarty;
                $this->smarty = $smarty;
            }
            $this->smarty->assign("Repricing_PURGE", AmazonRepricingAutomaton::PURGE);
            $this->smarty->assign('data', $view_params);
            $this->message = $this->smarty->fetch('amazon/RepricingQueues.tpl');
            $this->StopFeed();
        } else {
            $this->error = sprintf(Amazon_Tools::l('Unable to list queues for:  %s'), $this->params->iso_code);
            $this->StopFeed();
        }
    }

    public function PurgeQueues($options) {

        $queues = $options;

        if (!is_array($queues) || !count($queues)) {
            $this->error = Amazon_Tools::l('Please select at least one queue');
            $this->StopFeed();
        }

        $this->setAwsSettings();

        $sqs = new AmazonSQS($this->awsKeyId, $this->awsSecretKey);

        $queues_purged = array();

        foreach ($queues as $queue_name => $url) {

            $result = $sqs->purgeQueue($url);
            if (!(is_array($result) && array_key_exists('RequestId', $result) && preg_match('/([a-z0-9]*-){4,}/', $result['RequestId']))) {
                if ($this->debug) {
                    printf('%s(%d): Error' . "\n", basename(__FILE__), __LINE__);
                    var_dump($result);
                }
                $this->error = sprintf(Amazon_Tools::l('ERROR: Failed to delete queue: %s'), $queue_name);
                $this->StopFeed();
            }
            $queues_purged[$queue_name] = $queue_name;
        }

        if (is_array($queues_purged) && count($queues_purged)) {
            $this->message = Amazon_Tools::l('Queues successfully purged');
            $this->queues = $queues_purged;
            $this->StopFeed();
        } else {
            $this->error = Amazon_Tools::l('ERROR: An unexpected error occured during purge');
            $this->StopFeed();
        }
    }

    public function setAwsSettings() {

        /*if($this->debug){
            echo "<pre>setAwsSettings : check params</pre>";
            echo "<pre>: " . print_r($this->params, true) . "</pre>";
        }*/

        if (!isset($this->awsKeyId) || !isset($this->awsSecretKey))
            die('Repricing tool is not yet configured');

        if (empty($this->awsKeyId) || !preg_match('/([0-9A-Z]{12,})/', $this->awsKeyId))
            die('Wrong AWS Key ID');

        if (empty($this->awsSecretKey) || strlen($this->awsSecretKey) < 10)
            die('Wrong AWS Secret Key');

        $queues = $this->listQueues($this->awsKeyId, $this->awsSecretKey/*, $this->params->iso_code*/);

        if (!is_array($queues) || !count($queues)) {
            $this->error = Amazon_Tools::l('Unable to retrieve queues from Amazon AWS, please verify your AWS configuration');
            $this->StopFeed();
        }

        $input_queue = null;
        $output_queue = null;
        if (isset($queues['Queues'])) {
            foreach ($queues['Queues'] as $queue_url) {
                $url = trim(dirname(dirname($queue_url)));
                $queue = trim(basename($queue_url));

                if (strpos(AmazonSQS::ENDPOINT_US_EAST, $url) === false)
                    continue;

                if ($queue == $this->getQueueName($this->params->iso_code, AmazonRepricing::INPUT_QUEUE)) {
                    $input_queue = $queue_url;
                } elseif ($queue == $this->getQueueName($this->params->iso_code, AmazonRepricing::OUTPUT_QUEUE)) {
                    $output_queue = $queue_url;
                }
            }
        }

        if (!strlen($input_queue) || !strlen($output_queue)) {
            $this->error = sprintf(Amazon_Tools::l('Unable to find queues for:  %s'), $this->params->iso_code);
            //$this->error = $queues['Queues'];
            $this->StopFeed();
        }

        $this->UrlQueueIn = $input_queue;
        $this->UrlQueueOut = $output_queue;
    }

    private function setUserData($params) {

        $data['user_id'] = isset($params->id_user) ? $params->id_user : (isset($params->id_customer) ? $params->id_customer: null);
        $data['ext'] = $params->ext;
        $data['id_shop'] = $params->id_shop;
        $data['id_marketplace'] = (defined("_ID_MARKETPLACE_AMZ_")) ? _ID_MARKETPLACE_AMZ_ : 2;
        $AmazonUserData = AmazonUserData::get($data, true, true);

        if (empty($AmazonUserData) || !$AmazonUserData) {
            $this->error = Amazon_Tools::l('No API Connection established due to API connection keys missing or inactive. Please go to Configurations '
		    . '> Parameters Tab to set API keys.');
            $this->StopFeed();
	    die();
        }

        if(isset($params->shop_name) && !empty($params->shop_name)){
            $AmazonUserData['shopname'] = $params->shop_name;
        } else {
           $this->database = new AmazonDatabase($params->user_name);
           $shop_default = $this->database->feedbiz->getDefaultShop($params->user_name);
           $AmazonUserData['shopname'] = $shop_default['name'];
        }
        // change iso_code to ext
        $AmazonUserData['iso_code'] = Amazon_Tools::iso_codeToId($params->ext);

        if($this->debug && isset($params->asin)){
            $AmazonUserData['asin'] = $params->asin;
        }

        $user_data = json_encode($AmazonUserData);

        return json_decode($user_data);
    }

    private function initAmazonMarketplaceAPI() {

        $this->awsKeyId = isset($this->params->repricing_aws_key_id) ? trim($this->params->repricing_aws_key_id) : null;
        $this->awsSecretKey = isset($this->params->repricing_aws_secret_key) ? trim($this->params->repricing_aws_secret_key) : null;

	if($this->debug){
	    echo "<pre>awsKeyId : " . print_r($this->awsKeyId, true) . "</pre>";
	    echo "<pre>awsSecretKey : " . print_r($this->awsSecretKey, true) . "</pre>";
	}

        if (!strlen($this->awsKeyId) || !strlen($this->awsSecretKey)) {
            $this->error = Amazon_Tools::l('Failure: Both of AWS Key Id and AWS Secret Key must be filled');
            $this->StopFeed();
            exit;
        }
        $this->amazonApi = $this->AmazonApi();
    }

    private function AmazonApi() {

        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
        if (isset($this->params->auth_token) && !empty($this->params->auth_token)) {
            $auth['auth_token'] = trim($this->params->auth_token);
        }
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
        if (!$amazonApi = new Amazon_WebService($auth, $marketPlace, null, false)) {
            $this->error = Amazon_Tools::l('Unable_to_connect_Amazon');
            $this->StopFeed();
        }
        return $amazonApi;
    }

    private function StopFeed() {

        $error = null;
        if (isset($this->error) && !empty($this->error)) {
            $error = $this->error;
            if (isset($this->proc_rep)){
                $this->message->error = $this->error;
                $this->proc_rep->set_error_msg($this->message);
            }
        }

        $output = array('status' => strlen($error) ? 'error' : 'success', 'message' => $error ? $error : (isset($this->proc_rep) ? 'success' : $this->message) );

        if (isset($this->queues) && !empty($this->queues)) {
            $output['queues'] = $this->queues;
        }

        if (isset($this->proc_rep))
            $this->proc_rep->finish_task();

        echo json_encode($output);

	if(isset($this->is_exit_process) && $this->is_exit_process)
	    exit;
    }

    private function log_skipped($messages, $feed_type, $batchID, $operation_type, $database) {

        $history = array();
        foreach ($messages as $key => $message) {
            $error_data = array(
                'batch_id' => $batchID,
                'id_country' => $this->params->id_country,
                'id_shop' => $this->params->id_shop,
                'action_process' => $operation_type,
                'action_type' => $feed_type,
                'message' => strval($message),
                'id_message' => $key,
                'date_add' => date("Y-m-d H:i:s"),
            );

            if (!array_search(strval($message), $history))
                $database->save_validation_log($error_data);

            $history[] = strval($message);
        }
    }

    private function log_reprice($message_count, $sku, $database) {

        $log_data = array(
            'id_shop' => $this->params->id_shop,
            'id_country' => $this->params->id_country,
            'message_count' => $message_count,
            'sku' => $sku,
            'date_add' => date("Y-m-d H:i:s"),
        );

        $query = $database->insert_string('amazon_repricing_log', $log_data);

        #test FOXCHIP
        /*if(isset($this->debug_foxchip) && $this->debug_foxchip){
            $log_data = array(
                'id_shop' => $this->params->id_shop,
                'id_country' => $this->params->id_country,
                'message_count' => $message_count,
                'sku' => $this->debug_foxchip,
                'date_add' => date("Y-m-d H:i:s"),
            );

            $query .= $database->insert_string('amazon_repricing_log', $log_data);
        }*/
        #test FOXCHIP
        if(strlen($query))
            $database->exec_query($query);
    }

    private function repricing_report($data, $database, $repricing_action, $process_type) {

	if(!$this->debug) {
	    $database->save_repricing_report($this->params->id_shop, $this->params->id_country, $data, $repricing_action, $process_type);
	} else {
	    echo '<pre>repricing_report : ' . print_r($data, true) . '</pre>';
	}
    }

    private function update_flag($data, $database) {

        $query = '';

        foreach ($data as $d) {
	    if(isset($d['id_product']))
		$query .= ImportLog::update_flag_offers($this->params->id_marketplace, $this->params->ext, $this->params->id_shop, $d['id_product'], 1);
        }

	if(!$this->debug) {
	    if(strlen($query)) {
		$database->exec_query($query);
	    }
	} else {
	    echo '<pre>update_flag : ' . print_r($query, true) . '</pre>';
	}
    }

    private function MaxExecutionTime(){
        $server_max_execution_time = (int) @ini_get('max_execution_time');
        if (is_numeric($server_max_execution_time) && $server_max_execution_time < 5 * 60) {
            $this->max_execution_time = (int) $server_max_execution_time;
        } else {
            $this->max_execution_time = (int) self::MAX_MAX_EXECUTION_TIME;
        }
        $this->script_start_time = microtime(true);
    }

    private function StartProcessMessage($action = '') {
        if(!$this->verbose){
            $process_running = AmazonDatabase::REPRICING . ' - ' . $action;

            if(strlen($action) > 3)  $action = '_' . $action;

            $process_title = sprintf("Amazon %s, %s ..", strtolower($process_running), Amazon_Tools::l('processing'));
            $process_type = AmazonDatabase::REPRICING . $action .'_' . $this->params->countries;
            $marketplace = 'amazon';

            $this->message = new stdClass();
            $this->time = $this->params->time = date('Y-m-d H:i:s');

            $this->message->batchID = $this->batchID;
            $this->message->title = $process_title;
            $this->message->date = date('Y-m-d', strtotime($this->time));
            $this->message->time = date('H:i:s', strtotime($this->time));
            $this->message->type = sprintf(Amazon_Tools::l('AmazonS_s_started_on_s'), $this->params->ext, $process_running, '');
            $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();

            $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, true);
            $this->proc_rep->set_process_type($process_type);
            $this->proc_rep->set_popup_display(true);
            $this->proc_rep->set_marketplace($marketplace);
            $process = $this->proc_rep->has_other_running($process_type);

            if ($process) {
                $this->message->error =sprintf(Amazon_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'),$process_running,'20');
                $this->proc_rep->set_error_msg($this->message);

                $this->proc_rep->finish_task();
                echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, ' . $process_running . ' until old process done'));
                die();
            }
        }
    }
}
