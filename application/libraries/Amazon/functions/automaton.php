<?php
require_once(dirname(__FILE__) . '/../classes/amazon.webservice.php');
require_once(dirname(__FILE__) . '/../classes/amazon.database.php');
require_once dirname(__FILE__) . '/../classes/amazon.config.php';

class AmazonAutomatonProcess
{
    public $requestReportTime ; 
    public $reportTime ; 
    public $requestId ; 
    public $reportProcessingStatus ; 
    public $reportId ;
    public $step ;
    public $title ;
    public $message ;
    public $flag ;
    public $marketplace ;
    public $resubmitTimer ;
    public $completedDate ;
    public $loader ;
    public $hide ;
    public $processTimer ;
    public $inventory ;
}

class AmazonAutomaton /*extends Amazon*/
{
    private $debug = true ;
    
    static protected $errors = array();
    static protected $log = array();
    static protected $xml = null ;
    
    protected $import = null ;
    protected $ws = null ;
    protected $file_inventory = null ;
    protected $amazon_id_lang = null ;
    protected $marketplaceId  = null ;
    protected $region = null ;
    
    static protected $process = null ;
    static protected $products = array() ;
    
    const MERCHANT_OPEN_LISTINGS_DATA = 'open_listings_data' ;
    const FULFILLMENT_FEES_DATA = 'fulfillment_fees_data' ;
    const AFN_INVENTORY_DATA = 'afn_inventory_data' ; // 20/04/2016
    const MERCHANT_ACTIVE_LISTINGS_DATA = 'active_listings_data';
    const CONFIRM   = 'confirm' ;
    const UPDATE    = 'update' ;
    const CREATE    = 'create' ;
    const DELETE    = 'delete' ;
    
    const ACTION_WIZARD_MATCHING_MODE  = 1 ;
    const ACTION_WIZARD_CREATION_MODE  = 2 ;
    const MATCHING_MODE  = 'matching-wizard' ;
    const CREATION_MODE  = 'creation-wizard' ;
    const FULFILLMENT_FEES  = 'fulfillment_fees' ;
    
    const STEP_REPORT_REQUEST = 1 ;
    const STEP_GET_REPORT_REQUEST_LIST = 2 ;
    const STEP_GET_REPORT = 3 ;
    const STEP_PROCESS_REPORT = 4 ;
    const STEP_POST_PROCESS = 5 ;
    
    const ONE_DAY  = 86400 ;
    const ONE_HOUR = 3600 ;
    const TWO_HOURS = 7200 ;

    const LIMIT_TIME =  5400;
    const ONE_MINUTE = 60 ;
    const TWO_MINUTES = 120 ;
    const THREE_MINUTES = 180 ;
    const FIVE_MINUTES = 300 ;
    const NOW = 1 ;
    
    const EXPIRE = 14400; //4 hours - active_listings_data // 14/01/2016 
    
    protected static $steps = array(
        self::STEP_REPORT_REQUEST => 'REPORT_REQUEST',
        self::STEP_GET_REPORT_REQUEST_LIST => 'GET_REPORT_REQUEST_LIST',
        self::STEP_GET_REPORT => 'GET_REPORT',
        self::STEP_PROCESS_REPORT => 'STEP_PROCESS_REPORT',
        self::STEP_POST_PROCESS => 'STEP_POST_PROCESS',
    ) ;
    
    public function __construct($auth, $from, $marketPlaces = null, $debug = false, $cron = false)
    {        
        $this->debug = $debug;    
        $this->cron = $cron;  
	
        $this->batchID = isset($auth['batchID']) ? $auth['batchID'] : uniqid(); 
        $this->time = isset($auth['time']) ? $auth['time'] : date('Y-m-d H:i:s');

        $this->params = $auth;
		 
        if(!$this->debug) 
	    ob_start() ;
		
        Amazon_Tools::load(_REPORT_LANG_FILE_, $auth['language']);  
        
        if (!($this->ws = new Amazon_WebService($auth, $from, $marketPlaces, $this->debug)))
        {
            self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to init Amazon Service')) ; 
            die;
        }
        
        if ( $this->debug )
        {
            echo '<pre>' . print_r( basename(__FILE__) . '(#' . __LINE__ . ') : WS', true ) . '</pre>';
            echo '<pre>' . print_r($this->ws, true) . '</pre>';
        }
        
        if (!($this->db = new AmazonDatabase($this->ws->user, true)))
        {
            self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to init Amazon Service')) ; 
            die;
        }
        
        $this->initImportDirectory() ;

        if ( $this->debug )
        {
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);            
        }   
    }
        
    public function Dispatch($step, $status = null, $products = null, $mode = null, $page = 1, $datefrom = null, $dateto = null)
    {
        if ($this->debug)
        {
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);
        }       
	
        switch ($step)
        {
            case 'creation-wizard' :
            case 'matching-wizard' :
                $this->StartWizard($status);
                break;            
	    
            case 'match-products' :               
                return $this->MatchProducts($mode, $page);
		
            case 'confirm-products' :
                return $this->ConfirmProducts($products);
		
            case AmazonAutomaton::FULFILLMENT_FEES :
                return $this->FulfillmentFees($status, $datefrom, $dateto);
		
            case AmazonAutomaton::MERCHANT_ACTIVE_LISTINGS_DATA :
                return $this->ShippingGroupNames($status);

            case AmazonAutomaton::AFN_INVENTORY_DATA :
                return $this->FulfillmentInventory($status);
        }
        
        return (self::$process);
    }
       
    /*Shipping Group*/
    public function ShippingGroupNames($status)
    {        
	$fileid = floor((time() % (86400 * 365)) / self::EXPIRE); // file id valid for 4 hours

        $this->file_inventory = sprintf('%s%s_%s_%s_%s.raw', $this->import, self::MERCHANT_ACTIVE_LISTINGS_DATA, $this->ws->mid, $this->ws->region, $fileid);
	
	
	self::$process = new AmazonAutomatonProcess() ;                
        self::$process->flag = false ;
        self::$process->marketplace = sprintf('Amazon %s', strtoupper($this->region)) ;
	
        $title  = sprintf(Amazon_Tools::l('Amazon%s get shipping groups started on %s'), $this->ws->region, date('Y-m-d H:i:s')) ;
	
        return $this->WizardProcess($status, $title, AmazonAutomaton::MERCHANT_ACTIVE_LISTINGS_DATA) ;
	
	/********
	if (file_exists($this->file_inventory) && filesize($this->file_inventory) && filemtime($this->file_inventory) > time() - self::EXPIRE )
	{
	    
	    $message = sprintf(Amazon_Tools::l('Using existings file: "%s" - Expires: %s'), 
		    basename($this->file_inventory), date('Y-m-d H:i:s', filemtime($this->file_inventory) + self::EXPIRE));
	    
	    self::$process->message = $message;

	    // Inventory Exists, and downloaded, process the report
	    $group_names = $this->ShippingProcessInventory();
	    
	    $continue = false;
	}
	elseif (file_exists($this->file_inventory) && !filesize($this->file_inventory))
	{
	    // Inventory Exists, but has not been downloaded

	    // Check Timestamp
	    // 1 - if timestamp more than 2 minutes; get report
	    // 2 - if less ; ask to wait

	    $request_time = filemtime($this->file_inventory);
	    $now = time();
	    $elapsed = $now - $request_time;

	    if ($this->debug)
	    {
		echo "<pre>\n";
		printf('%s(#%d): %s - Request Time: "%s", elapsed: %d', basename(__FILE__), __LINE__, __FUNCTION__, date('c', $request_time), $elapsed);
		echo "</pre>\n";
	    }

	    if ($elapsed > 60 * 60)
	    {
		$error = Amazon_Tools::l('Delay to download report is expired');
		$debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
		self::$process->errors = $error;

		if ($this->debug)
		{
		    echo "<pre>\n";
		    echo $debug;
		    printf('%s(#%d): %s - ERROR: Request Time: "%s", elapsed: %d - delay expired', basename(__FILE__), __LINE__, __FUNCTION__, date('c', $request_time), $elapsed);
		    echo "</pre>\n";
		}
		unlink($this->file_inventory);
		$continue = false;
		$pass = false;
	    }
	    elseif ($elapsed < 60 * 2)
	    {
		$continue = true;
		$pass = true;

		self::$messages[] = $message = Amazon_Tools::l('Waiting a while for the report to be ready for download');
		$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);

		if ($this->debug)
		{
		    echo "<pre>\n";
		    print $debug;
		    echo "</pre>\n";
		}
	    }
	    else
	    {
		$reportRequestId = $this->reportRequestList();

		if ($reportRequestId)
		{
		    if ($this->getReport($reportRequestId))
		    {
			self::$messages[] = $message = sprintf('%s (%s)', Amazon_Tools::l('Downloading Report ID'), $reportRequestId);
			$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);

			if ($this->debug)
			{
			    echo "<pre>\n";
			    print $debug;
			    echo "</pre>\n";
			}

			$continue = true;
			$pass = true;
		    }
		    else
		    {
			$error = Amazon_Tools::l('Failed to download the Report');
			$debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
			self::$process->errors = $error;

			if ($this->debug)
			{
			    echo "<pre>\n";
			    print $debug;
			    echo "</pre>\n";
			}
			$continue = false;
			$pass = false;
		    }
		}
		else
		{
		    self::$messages[] = sprintf('%s (%s)', Amazon_Tools::l('Waiting for the report to be available... this operation could take time'), $reportRequestId);

		    if ($this->debug)
		    {
			echo "<pre>\n";
			printf('%s(#%d): %s - A report has been already requested and there is not any available report yet', basename(__FILE__), __LINE__, __FUNCTION__);
			echo "</pre>\n";
		    }
		    touch($this->file_inventory);
		    $continue = true;
		    $pass = true;
		}
	    }
	}
	else
	{
	    // File doesn't exist
	    // 1 - Create the file
	    // 2 - Request the Report

	    if (!Amazon_Tools::is_dir_writeable($this->import))
	    {
		$error = sprintf('"%s" %s', $this->import, Amazon_Tools::l('is not a writable directory, please check directory permissions'));
		$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
		self::$process->errors = $error;

		if ($this->debug)
		{
		    echo "<pre>\n";
		    echo "Error:$debug\n";
		    echo "</pre>\n";
		}
		$continue = false;
		$pass = false;
	    }
	    
	    if (file_put_contents($this->file_inventory, null) === false)
	    {
		$error = sprintf('%s: "%s"', $this->import, Amazon_Tools::l('failed to create file'), $this->file_inventory);
		$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
		self::$process->errors = $error;

		if ($this->debug)
		{
		    echo "<pre>\n";
		    echo "Error:$debug\n";
		    echo "</pre>\n";
		}
		$continue = false;
		$pass = false;
	    }

	    if ($reportRequestId = $this->reportRequest(self::MERCHANT_ACTIVE_LISTINGS_DATA))
	    {
		touch($this->file_inventory);

		self::$messages[] = sprintf(Amazon_Tools::l('Report has been requested (%s), please wait a while'), $reportRequestId);

		if ($this->debug)
		{
		    echo "<pre>\n";
		    printf('%s(#%d): %s - Report Request ID: "%s"', basename(__FILE__), __LINE__, __FUNCTION__, $reportRequestId);
		    echo "</pre>\n";
		}
		$continue = true;
		$pass = true;
	    }
	    else
	    {
		$error = Amazon_Tools::l('Request Report failed, please review your module configuration');
		$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
		self::$process->errors = $error;

		if ($this->debug)
		{
		    echo "<pre>\n";
		    print $debug;
		    echo "</pre>\n";
		}
		$continue = false;
		$pass = false;
	    }
	}
	/********/
    }
    
    protected function ShippingProcessInventory()
    {
        if ($this->debug)
        {
            printf('ProcessInventory()'."<br />\n");
        }

        if (($result = file_get_contents($this->file_inventory)) === false)
        {
            $error = Amazon_Tools::l('Unable to read input file');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
	    
            return (false);
        }

        if ($result == null || empty($result))
        {
            $error = Amazon_Tools::l('Inventory is empty !');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        $lines = explode("\n", $result);

        if (!is_array($lines) || !count($lines))
        {
            $error = Amazon_Tools::l('Inventory is empty !');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        if ($this->debug)
        {
            echo "<pre>\n";
            echo str_repeat('-', 160)."\n";
            printf('Inventory: %s products'."<br />\n", count($lines));
            echo "</pre>\n";
        }

        $header = reset($lines);

        if (!strlen($header))
        {
            $error = Amazon_Tools::l('No header, file might be corrupted');
            $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        $columns = explode("\t", $header);

        // Header, display to the user he doesn't have merchant shipping group
        if (!in_array('merchant-shipping-group', $columns))
        {
            $error = Amazon_Tools::l('No merchant shipping groups detected, it seems your are not using shipping templates');
            $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }
	
        $columns_keys = array_flip($columns);
        $merchant_shipping_group_key = $columns_keys['merchant-shipping-group'];

        $count = 0;
        $group_names = $history_groupnames = array();
	$update_shipping_group = '';
	
        foreach ($lines as $line)
        {
            if (empty($line))
                continue;

            if ($count++ < 1)
                continue;

            $result = explode("\t", $line);

            if (count($result) < $merchant_shipping_group_key + 1)
                continue;

            $group_key = Amazon_Tools::toKey($result[$merchant_shipping_group_key]);
            $group_names = utf8_encode($result[$merchant_shipping_group_key]);
	    
	    if(!isset($history_groupnames[$group_key])){
		
		$amazon_shipping_group = array(
		    "id_shop" => $this->ws->id_shop,
		    "id_country" => $this->ws->id_country,
		    "group_key" => $group_key,
		    "group_name" => $group_names,
		    'date_upd' => date('Y-m-d H:i:s'),
		);
	    
		$update_shipping_group .= $this->db->replace_string(AmazonDatabase::$m_pref.'amazon_shipping_groups', $amazon_shipping_group );
	    }
	    
            $history_groupnames[$group_key] = utf8_encode($result[$merchant_shipping_group_key]);
	    
        }

//	    if (is_array($group_names) && count($group_names))
//	    {
//		/*$configured_group_names = unserialize(AmazonConfiguration::get('shipping_groups'));
//
//		if (is_array($configured_group_names) && count($configured_group_names))
//		{
//		    unset($configured_group_names[$this->region]);
//		    $configured_group_names[$this->region] = $group_names;
//		}
//		else
//		{
//		    $configured_group_names = array();
//		    $configured_group_names[$this->region] = $group_names;
//		}*/
//		
//		//AmazonConfiguration::updateValue('shipping_groups', serialize($configured_group_names));
//		
//		// Update to amazon_shipping_group
//		
//		self::$messages[] = $message = sprintf('%d %s', count($group_names), Amazon_Tools::l('shipping groups have been retrieve with success'));
//		$debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);
//
//		if ($this->debug)
//		{
//		    echo "<pre>\n";
//		    print $debug;
//		    echo "</pre>\n";
//		}
//
//		$pass = true;
//	    }
//	    else
//	    {
//		$configured_group_names = unserialize(AmazonConfiguration::get('shipping_groups'));
//
//		if (is_array($configured_group_names) && count($configured_group_names))
//		{
//		    //unset($configured_group_names[$this->region]);
//		}
//
//		AmazonConfiguration::updateValue('shipping_groups', serialize($configured_group_names));
//
//		$error = Amazon_Tools::l('Not any existing shipping groups have been found from the inventory');
//		$debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
//		self::$process->errors = $error;
//
//		if ($this->debug)
//		{
//		    echo "<pre>\n";
//		    echo $debug;
//		    echo "</pre>\n";
//		}
//	    }
	
	if(strlen($update_shipping_group) > 10)
	{
//	    $update_sql .= 'REPLACE INTO '. AmazonDatabase::$m_pref.'amazon_shipping_groups (id_shop, id_country, group_key, group_name, date_upd) VALUES '.implode(', ', $values_stringified) . '; ' 
//		    $update_shipping_group;
	    
	    if(!$this->db->exec_query($update_shipping_group, false, true, true, true))
            {
		$error = Amazon_Tools::l('Unable to save shipping group');
		$debug = sprintf('%s(#%d): %s - Unable to save shipping group', basename(__FILE__), __LINE__, __FUNCTION__);
                self::$process->errors = $error ; 
		
		if ($this->debug)
		{
		    echo "<pre>\n";
		    echo "Error:$debug\n";
		    echo "</pre>\n";
		}

                return(false);
            }
	    
	} else {
	    
	    $error = Amazon_Tools::l('Not any group name found');
            $debug = sprintf('%s(#%d): %s - Not any group name found', basename(__FILE__), __LINE__, __FUNCTION__);
	    
            self::$process->errors = $error;

            if ($this->debug)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
	    
            return (false);
        }

        if ($this->debug)
        {
            echo "<pre>\n";
            printf('Processed Items: %s'."<br />\n", print_r($update_shipping_group, true));
            echo "</pre>\n";
        }

        return (true);
    }
    
    /*Shipping Group*/
    
    public function FulfillmentInventory($status)
    {
	$this->file_inventory = sprintf('%s%s_%s_%s.raw', $this->import, self::AFN_INVENTORY_DATA, $this->ws->mid, $this->ws->region) ;

	self::$process = new AmazonAutomatonProcess() ;
        self::$process->flag = false ;
        self::$process->marketplace = sprintf('Amazon %s', ''/*strtoupper($this->region)*/) ;
        self::$process->step  = $status;
        
        $title  = sprintf(Amazon_Tools::l('Amazon%s FBA - get inventory started on %s'), ' ' /*. $this->ws->region*/, date('Y-m-d H:i:s')) ;

        return $this->WizardProcess($status, $title, AmazonAutomaton::AFN_INVENTORY_DATA) ;

    }

    public function FulfillmentFees($status, $datefrom, $dateto)
    {        
	$this->file_inventory = sprintf('%s%s_%s_%s.raw', $this->import, self::FULFILLMENT_FEES_DATA, $this->ws->mid, $this->ws->region) ; 
	
	self::$process = new AmazonAutomatonProcess() ;                
        self::$process->flag = false ;
        self::$process->marketplace = sprintf('Amazon %s', ''/*strtoupper($this->region)*/) ;
	
        $title  = sprintf(Amazon_Tools::l('Amazon%s FBA - get data started on %s'), ' ' /*. $this->ws->region*/, date('Y-m-d H:i:s')) ;
	
        return $this->WizardProcess($status, $title, AmazonAutomaton::FULFILLMENT_FEES, $datefrom, $dateto) ;
	
    }
    
    /*
     * Product REPORT Wizard
     */
    public function StartWizard($status)
    {
	$this->file_inventory = sprintf('%s%s_%s_%s.raw', $this->import, self::MERCHANT_OPEN_LISTINGS_DATA, $this->ws->mid, $this->ws->region) ; 
	
        $amazon_synch = $this->db->get_request_report_log($this->ws->id_country, $this->ws->id_shop) ;
	
	if($this->debug) echo '<pre> get_request_report_log :' . print_r($amazon_synch, true) . '</pre>';
	    
        if ( ! is_array($amazon_synch) )
            $amazon_synch = array() ;
        
        if ( isset($amazon_synch) )
        {
            self::$process = $amazon_synch ;
            
            if ( ! self::$process instanceOf AmazonAutomatonProcess )
                self::$process = new AmazonAutomatonProcess() ;            
        }
        else
        {
            self::$process = new AmazonAutomatonProcess() ;
        }
            
        if ( self::$process->requestReportTime && self::isExpired(self::$process->requestReportTime, self::ONE_HOUR) )
        {
            self::$process = new AmazonAutomatonProcess() ;
        }
        
        self::$process->flag = false ;
        self::$process->marketplace = sprintf('Amazon %s', strtoupper($this->region)) ;
	
        $title  = sprintf('%s %s', Amazon_Tools::l('Amazon Marketplace Automaton started on'), date('Y-m-d H:i:s')) ;
	
        return $this->WizardProcess($status, $title) ;
    }   

    public function WizardProcess($status, $title, $type=null, $datefrom=null, $dateto=null)
    {
        self::$process->title = $title ;
        
	if($this->debug) 
	    echo '<pre> Process step :' . print_r(isset(self::$steps[self::$process->step]) ? self::$steps[self::$process->step] : self::$process->step, true) . '</pre>';
	
        switch( self::$process->step )
        {                        
            case self::STEP_PROCESS_REPORT :
		
		if($this->debug) 
		    echo '<pre> Process step : STEP_PROCESS_REPORT , type = ' . $type .' ';
		
		switch ($type){
		    
		    case AmazonAutomaton::MERCHANT_ACTIVE_LISTINGS_DATA :
			if($this->debug) 
			    echo '<pre> ShippingProcessInventory </pre>';		    
			$process = $this->ShippingProcessInventory();
			break;

                    case AmazonAutomaton::AFN_INVENTORY_DATA :                        
			if($this->debug)
			    echo '<pre> processFulfillmentInventory </pre>';

                        self::$process->resubmitTimer = self::NOW ;
                        self::$process->completedDate = time() ;
                        self::$process->message = Amazon_Tools::l('Inventory processing completed.') ;
                        self::$process->hide = 0 ;
                        self::$process->loader = 1 ;
                        self::$process->step = self::STEP_POST_PROCESS ;
                        
			return $this->processFulfillmentInventory();
			break;
                        
		    case AmazonAutomaton::FULFILLMENT_FEES :
			if($this->debug) 
			    echo '<pre> processFulfillmentData </pre>';		    
			$process = $this->processFulfillmentData();
			break;
			
		    default :
			if($this->debug) 
			    echo '<pre> ProcessInventory </pre>';
			$process = $this->ProcessInventory();
		    break;
		}		
		
		if($this->debug) 
		    echo '<pre>'. print_r($process, true) . '</pre>';
		
                if ( !$process )
                {
                    self::$process = new AmazonAutomatonProcess() ;
                    self::$process->message = Amazon_Tools::l('Inventory processing not complete, the process will restart in 1 hours') ;
                    self::$process->resubmitTimer = self::ONE_HOUR ;
                    self::$process->loader = 1 ;
                    self::$process->hide = 1 ;
                }
                else
                {
                    self::$process->resubmitTimer = self::NOW ;
                    self::$process->completedDate = time() ;
                    self::$process->message = Amazon_Tools::l('Inventory processing completed.') ;
                    self::$process->hide = 0 ;
                    self::$process->loader = 1 ;
                    self::$process->step = self::STEP_POST_PROCESS ;
                }
                break ;
                
            case self::STEP_GET_REPORT ;
                
                if ( $this->Steps(self::STEP_GET_REPORT, $type) )
                {
                    self::$process->step = self::STEP_PROCESS_REPORT ;
                    self::$process->resubmitTimer = self::NOW ;
                    self::$process->message = Amazon_Tools::l('File downloaded successfully, wait for processing') ;
                }
                else
                {
                    self::$process = new AmazonAutomatonProcess() ;
                    self::$process->resubmitTimer = self::TWO_HOURS ;
                    self::$process->message = Amazon_Tools::l('Unable to find any inventory, the process will restart in 2 hours') ;
                }
                self::$process->loader = 1 ;
                self::$process->hide = 0 ;
                break ;
                
            case self::STEP_GET_REPORT_REQUEST_LIST ;
		
                if ( $this->Steps(self::STEP_GET_REPORT_REQUEST_LIST, $type) )
                {
                    if ( self::$process->reportId )
                    {
                        self::$process->message = Amazon_Tools::l('The inventory is ready, wait for processing') ;
                        self::$process->step = self::STEP_GET_REPORT ; 
                    }
                    else
                    {
                        self::$process->message = Amazon_Tools::l('Wait for the inventory, it should take a while, up to one hour') ;
                    }
                }
                else
                {
                    $message = Amazon_Tools::l('Unable to find any inventory, the process will restart in 2 hours') ; 
		    self::$process->step = self::STEP_POST_PROCESS ; 
                    self::$process->message = $message ;
                }
                self::$process->loader = 1 ;
                self::$process->hide = 0 ;
                break ;
                
            case self::STEP_REPORT_REQUEST ; 
            default :
                
                self::$process->step = self::STEP_REPORT_REQUEST ;
                self::$process->message = Amazon_Tools::l('Requesting an inventory to Amazon') ;
                
                if ( $status ) 
                {
                    self::$process->hide = 0 ;
                    self::$process->loader = 0 ;
                    break ;
                }                
                if ( file_exists($this->file_inventory) && !self::isExpired(filemtime($this->file_inventory), self::ONE_HOUR) ) 
                {
		    self::$process->step = self::STEP_PROCESS_REPORT ;   
		    self::$process->message = Amazon_Tools::l('Inventory file exists already and is not expired, reprocessing this feed') ;
		    self::$process->resubmitTimer = self::NOW ;
		    self::$process->loader = 1 ;                    
		    self::$process->hide = 0 ;
		    self::$process->exists = true ;		    
                }
                else
                {                    
                    if ( $this->Steps(self::STEP_REPORT_REQUEST, $type, $datefrom, $dateto) )
                    {
                        if ( self::$process->requestId )
                        {
                            self::$process->step = self::STEP_GET_REPORT_REQUEST_LIST ;   
                            self::$process->message = Amazon_Tools::l('Request has been accepted, wait for processing') ;
                            self::$process->resubmitTimer = self::ONE_MINUTE ;
                            self::$process->loader = 1 ;
                        }
                        else
                        {
                            self::$process->message = Amazon_Tools::l('Request failed, the request will be resubmitted in a while') ;
                            self::$process->resubmitTimer = self::TWO_HOURS ;
                        }
                    }
                }
                break ;
        }  
       
        $amazon_synch = self::$process ;
        $amazon_synch->id_shop = $this->ws->id_shop;
        $amazon_synch->id_country = $this->ws->id_country;
        $amazon_synch->date_add = date('Y-m-d H:i:s');
	
	if(!isset($type) || empty($type))
	    $this->db->save_request_report((array)$amazon_synch);
      
        return $amazon_synch;
    }     
    
    //confirm product
    public function ConfirmProducts($products)
    {
        $products_result = $product_data = array() ;
        
        //sleep(2) ;        
        if ( is_array($products) && count($products) )
        {
            foreach($products as $reference => $product)
            {
                if ( !isset($product['id_product']) || empty($product['id_product']) )
                    continue ;
                elseif ( !isset($product['asin']) || empty($product['asin']) )
                    continue ;
                
                $products_result[$reference] = $product ;   
                 
                $product_data[$reference]['id_product'] = $product['id_product'];                
                $product_data[$reference]['sku'] = $reference;
                $product_data[$reference]['asin'] = $product['asin'];
                $product_data[$reference]['action_type'] = AmazonAutomaton::CONFIRM;
                
                if(isset($product['id_product_attribute']) && !empty($product['id_product_attribute']))
                    $product_data[$reference]['id_product_attribute'] = $product['id_product_attribute']; 
            }
        }
        
        if ( is_array($products_result) && count($products_result) )
        {
            self::$products = $products_result ;
            
            if(!$this->db->save_inventory($product_data, $this->ws->id_country, $this->ws->id_shop))
            {
                return(false);
            }
        }
        else
            self::$products = null ;
       
        return self::$products;
    }
    
    //MatchProducts
    public function MatchProducts($mode = null, $page = 1)
    {
        $type = $this->ws->synchronization_field;
        
        $products = $this->db->getProductToSync($this->ws, $mode, $page) ; 
        
        if ( $this->debug )
        {
            echo '<pre>' . print_r( basename(__FILE__) . '(#' . __LINE__ . ') : Products', true ) . '</pre>';
            echo '<pre>' . print_r($products, true) . '</pre>';
        }
        
        $productCount = 0 ;
        $productList = array() ;
        $productUniq = array() ;
        $productTypeValueUniq = array() ;
        $params = array() ;

        // mark as to be matched
        if ( is_array($products) && count($products) )
        {            
            foreach($products as $key => $product)
            {
                $key = preg_replace("/[^a-zA-Z0-9]+/", "_", $key);
                
                if ( !isset($product[$type]) || empty($product[$type]) )
                {
                    if (isset($product['ean13']) && !empty($product['ean13'])) {
                        $type = 'ean13';
                    } elseif (isset($product['upc']) && !empty($product['upc'])) {
                        $type = 'upc';
                    } elseif (isset($product['jan']) && !empty($product['jan'])) {
                        $type = 'jan';
                    } else {
                        continue ;
                    }
                }
                
                if ( $this->debug )
                {
                    echo '<pre>' . print_r( basename(__FILE__) . '(#' . __LINE__ . ') : Type : ' . $type , true ) . '</pre>';
                }
                if ( isset($productUniq[$key]) || isset($productTypeValueUniq[ $product[$type] ]) )
                {
                    unset($products[$key]) ;
                    continue ;
                }
                $productUniq[$key] = true ;
                $productTypeValueUniq[ $product[$type] ] = true ;
                
                if ( isset($products[$key]['checked']) && $products[$key]['checked'] )
                {
                    unset($products[$key]) ;
                    continue ;
                }  
            }

            foreach($products as $key => $product)
            {
                if ( !isset($product[$type]) || empty($product[$type]) )
                {
                    if (isset($product['ean13']) && !empty($product['ean13'])) {
                        $type = 'ean13';
                    } elseif (isset($product['upc']) && !empty($product['upc'])) {
                        $type = 'upc';
                    } elseif (isset($product['jan']) && !empty($product['jan'])) {
                        $type = 'jan';
                    } else {
                        continue ;
                    }
                }

                $products[$key]['checked'] = true ;
                $products[$key]['amazon'] = array() ;
                $products[$key]['amazon']['name'] = null ;
                $products[$key]['amazon']['image_url'] = null ;
                $products[$key]['amazon']['asin'] = null ;                

                $productList[] = $key ;
                
                if(!isset($params[$type]))$productCount = 0;
                
                $params[$type]['Action'] = 'GetMatchingProductForId' ;
                $params[$type]['MarketplaceId'] = $this->ws->mpid ;  
                $params[$type]['IdType'] = ($type == "ean13") ? 'EAN' : ( ($type == "upc") ? 'UPC' : ( ($type == "jan") ? 'JAN' : '') );
                $params[$type]['IdList.Id.' . ($productCount + 1)] = ($type == "ean13") ? sprintf('%013s', $product[$type]) : $product[$type] ;
				
                if(isset($this->ws->auth_token) && !empty($this->ws->auth_token)){
                        $params[$type]['MWSAuthToken'] = $this->ws->auth_token;
                }
		
                if ( ++$productCount >= 5 )
                    break;
            }
        }
 
        if ( $this->debug )
        {
            echo '<pre>' . print_r( basename(__FILE__) . '(#' . __LINE__ . ') : Params', true ) . '</pre>';
            echo '<pre>' . print_r($params, true) . '</pre>';
        }

        if ( $productCount > 0 )
        {
            //sleep(1) ; // anti-throttling - 1 second is efficient because recovery time is 5 products per second (1 query)
            foreach ($params as $type => $param)
            {
                $xml = $this->ws->simpleCallWS($param, 'Products') ;

                if ( $this->debug )
                {
                    echo '<pre>' . print_r( basename(__FILE__) . '(#' . __LINE__ . ') : XML', true ) . '</pre>';
                    echo '<pre>' . print_r($xml, true) . '</pre>';
                }

                if ( ! $xml instanceOf SimpleXMLElement OR isset($xlm->Error) OR ! isset($xml->GetMatchingProductForIdResult) )
                {
                    if ( isset($xml->Error->Message) )
                        $error_msg = (string)$xml->Error->Message;
                    elseif ( ! isset($xml->GetMatchingProductForIdResult) || ! $xml->GetMatchingProductForIdResult instanceOf SimpleXMLElement ) 
                        $error_msg = 'XML GetMatchingProductForIdResult was expected' ;
                    else
                        $error_msg = 'XML was expected' ;

                    self::$process->errors = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, Amazon_Tools::l('Failed'), $error_msg) ; 
                    self::$products = null ;
                    return(false) ;
                } 

                foreach($xml->GetMatchingProductForIdResult as $result)
                {
                    $attributes = $result->attributes() ;
                    $Status = (string)$attributes->status ;

                    if ( $Status != 'Success' )
                        continue ;

                    $ProductId = (string)$attributes->Id ;

                    foreach($products as $key => $product)
                    {
                        // product matched
                        if ( $product[$type] == $ProductId )
                        {
                            if(isset($result->Products->Product->AttributeSets))
                            {
                                $Item = $result->Products->Product->AttributeSets->children('ns2', true) ;
                                $Identifiers = $result->Products->Product->Identifiers ;

                                $products[$key]['amazon'] = array() ;

                                if ( isset($Item->ItemAttributes->SmallImage->URL) )
                                    $products[$key]['amazon']['image_url'] = trim((string)$Item->ItemAttributes->SmallImage->URL) ; 
                                else
                                    $products[$key]['amazon']['image_url'] = '' ;

                                if ( isset($Item->ItemAttributes->Brand) )
                                    $products[$key]['amazon']['brand'] = self::encodeText(trim((string)$Item->ItemAttributes->Brand)) ;
                                else
                                    $products[$key]['amazon']['brand'] = '' ;

                                if ( preg_replace('/[^A-Za-z0-9]/', '', $products[$key]['amazon']['brand']) != preg_replace('/[^A-Za-z0-9]/', '', $products[$key]['manufacturer']) )
                                    $products[$key]['brand_mismatch'] = true ;    
                                else
                                    $products[$key]['brand_mismatch'] = false ;    

                                if ( isset($Item->ItemAttributes->Title) )
                                    $products[$key]['amazon']['name'] = self::encodeText(trim((string)$Item->ItemAttributes->Title)) ;
                                else
                                    $products[$key]['amazon']['name'] = '' ;

                                if ( isset($Identifiers->MarketplaceASIN->ASIN) )
                                {
                                    $products[$key]['amazon']['asin'] = trim((string)$Identifiers->MarketplaceASIN->ASIN) ;
                                    $products[$key]['matched'] = true ;
                                }
                                else
                                    $products[$key]['amazon']['asin'] = '' ;
                            }
                            break ;
                        }
                    }
                }
                sleep(1) ; // anti-throttling - 1 second is efficient because recovery time is 5 products per second (1 query)
            }
            
            // Mark unmatched as processed, but unmatched
            foreach($productList as $key)
                if ( ! isset($products[$key]['matched']) || ! $products[$key]['matched'] )
                {
                    $products[$key]['matched'] = false ;
                }
        }
        
        return($products) ;
    }
    
    /*
     * PART 1 - Get Asynchronously the Inventory Report From Amazon
     */
    protected function Steps($step, $type=null, $datefrom=null, $dateto=null)
    {
        $pass = false ;
        
        self::$process->resubmitTimer = null ;
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
        
        switch($step)
        {
            // Request Report Generation 
            case self::STEP_REPORT_REQUEST :
               
                if ( ! $this->reportRequest($type, $datefrom, $dateto) )
                {
                    self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
                    return(false);
                }
                $pass = true ;            
                break ;
                
            // Wait for the Report Id
            case self::STEP_GET_REPORT_REQUEST_LIST :
                
                if ( ! $this->reportRequestList($type) )
                {
                    self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
                    return(false);
                }
                switch(self::$process->reportProcessingStatus)
                {
                    case '_SUBMITTED_' :
                    case '_IN_PROGRESS_' :
                        self::$process->resubmitTimer = self::ONE_MINUTE ;
                        $pass = true ;
                        return(true) ;
                    case '_CANCELLED_' :
                    case '_DONE_NO_DATA_' :
                        //self::$process = new AmazonAutomatonProcess() ;
                        $pass = false; 
			return (false); //exit ;
                    case '_DONE_' : 
                        self::$process->resubmitTimer = self::NOW ;
                        self::$process->message = Amazon_Tools::l('File downloaded successfully, wait for processing') ;
                        $pass = true ;
                        break ;                    
                }
                break ;   
                
            // Download Report
            // 
            case self::STEP_GET_REPORT :
                
                if ( ! $this->getReport($type) )
                {
                    self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
                    return(false);
                }
                $pass = true ;            
                break ;
                
        }
        return($pass) ;
    }
    
    protected function reportRequest($type=null, $datefrom=null, $dateto=null)
    {          
    
        $params = array() ;
        $params['Action'] = 'RequestReport' ;
        $params['Version'] = '2009-01-01' ;
        $params['MarketplaceIdList.Id.1'] = $this->ws->mpid ;

	if(isset($this->ws->auth_token) && !empty($this->ws->auth_token)){
		$params['MWSAuthToken'] = $this->ws->auth_token;
	}
		
	switch ($type) {
	    
	    case self::MERCHANT_ACTIVE_LISTINGS_DATA:
		$params['ReportType'] = '_GET_MERCHANT_LISTINGS_DATA_' ;
		break;
	    
            case self::AFN_INVENTORY_DATA :
                $params['ReportType'] = '_GET_AFN_INVENTORY_DATA_' ;
                break;

            case self::FULFILLMENT_FEES :
                $params['ReportType'] = '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_' ;
				
		$date1 = date('Y-m-d', strtotime($datefrom));
		$date2 = date('Y-m-d', strtotime($dateto));
		
		$date = Amazon_Tools::query_date($date1, $date2);
	
		if($date){
		    $date1 = $date['date1'];
		    $date2 = $date['date2'];
		}  

                $params['StartDate'] = $date1 ;
                $params['EndDate'] = $date2 ;
                break;

	    default : 
                $params['ReportType'] = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_' ;
                break;
        }
	
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);        
        
        $xml = $this->ws->simpleCallWS($params, 'Reports') ;
        
        if ( ! $xml instanceOf SimpleXMLElement )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
            return(false) ;
        }     
        
        self::$xml = $this->debugXML($xml) ;
        
        if ( ! isset($xml->RequestReportResult->ReportRequestInfo->ReportProcessingStatus) || ! isset($xml->RequestReportResult->ReportRequestInfo->ReportRequestId) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
            return(false) ;
        }
        if ( $xml->RequestReportResult->ReportRequestInfo->ReportProcessingStatus == '_SUBMITTED_' )
        {
            self::$process->requestReportTime = time() ;
            self::$process->requestId = (string)$xml->RequestReportResult->ReportRequestInfo->ReportRequestId ;   
            return(true) ;
        }
        else
            return(false) ;
    }
     
    protected function reportRequestList($type=null)
    {
        $params = array() ;
        $params['Version'] = '2009-01-01' ;
        $params['Action'] = 'GetReportRequestList' ;
        $params['ReportRequestIdList.Id.1'] = self::$process->requestId ;

	if(isset($this->ws->auth_token) && !empty($this->ws->auth_token)){
		$params['MWSAuthToken'] = $this->ws->auth_token;
	}
	
	switch ($type) {
	    case self::MERCHANT_ACTIVE_LISTINGS_DATA :
		$params['ReportRequestList.Type.1'] = '_GET_MERCHANT_LISTINGS_DATA_' ;
                break; 
            case self::AFN_INVENTORY_DATA :
                $params['ReportRequestList.Type.1'] = '_GET_AFN_INVENTORY_DATA_' ;
                break;           
            case self::FULFILLMENT_FEES :
                $params['ReportRequestList.Type.1'] = '_GET_FBA_ESTIMATED_FBA_FEES_TXT_DATA_' ;
                break; 
	    default : 
                $params['ReportRequestList.Type.1'] = '_GET_FLAT_FILE_OPEN_LISTINGS_DATA_' ;
                break;
        }
				
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
        
        $xml = $this->ws->simpleCallWS($params, 'Reports') ;
        
        if ( ! $xml instanceOf SimpleXMLElement )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
            return(false) ;
        }     
        
        self::$xml = $this->debugXML($xml) ;
        
        if ( ! isset($xml->GetReportRequestListResult->ReportRequestInfo->ReportProcessingStatus) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Failed')) ; 
            return(false) ;
        }
        
        self::$process->reportProcessingStatus = (string)$xml->GetReportRequestListResult->ReportRequestInfo->ReportProcessingStatus ;
        
        if ( isset($xml->GetReportRequestListResult->ReportRequestInfo->GeneratedReportId) && (int)$xml->GetReportRequestListResult->ReportRequestInfo->GeneratedReportId )
        {
            self::$process->reportTime = time() ;
            self::$process->reportId = (string)$xml->GetReportRequestListResult->ReportRequestInfo->GeneratedReportId ;
        }
        return(true) ;
    }

    /*
     * Returns raw result ; CSV Tab separated file
     */
    protected function getReport($type=null)
    {
        $params = array() ;
        $params['Version'] = '2009-01-01' ;
        $params['Action'] = 'GetReport' ;
        $params['ReportId'] = self::$process->reportId ; 
		
        if(isset($this->ws->auth_token) && !empty($this->ws->auth_token)){
            $params['MWSAuthToken'] = $this->ws->auth_token;
        }
		
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
        
        $result = $this->ws->simpleCallWS($params, 'Reports', false) ;
        
        if ( empty($result) )
        {
	    $empty_result = '';
	    switch ($type) {
		case self::FULFILLMENT_FEES :
		    $empty_result = Amazon_Tools::l('Fulfillment data is empty (1)') ;
		    break;           
		default : 
		    $empty_result = Amazon_Tools::l('Inventory is empty (1)') ;
		    break;
	    }
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, $empty_result) ; 
            return(false) ;            
        }
        
        if ( file_put_contents($this->file_inventory, $result) === false )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to write to output file'), $this->file_inventory) ; 
            return(false) ;                        
        }
    
        return(true) ;
    }    
    
    /*
     * PART 2 - Manage the Inventory
     */
    protected function processInventory()
    {
	if ($this->debug && isset(self::$process->exists) && self::$process->exists) 
	{
	    return(true);
	}
	 
	if($this->debug) 
	    echo '<pre> function processInventory </pre>';	
	
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
	
        if ( ($result = file_get_contents($this->file_inventory)) === false )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to read input file'), $this->file_inventory) ; 
            return(false) ;                         
        }
	
	
        if ( $result == null or empty($result) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Inventory is empty (2)')) ; 
            return(false) ;            
        }
        
        $lines = explode("\n", $result) ;
 
        if ( ! is_array($lines) || ! count($lines) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Inventory is empty (3)')) ; 
            return(false) ;     
        }

        $amazonItems = $quantity_mismatch = array() ;
	$updateFlag = $update_amazon_inventory = '';
        $count = 0 ;
        
        if(!empty($lines)) {	    
	    
            $products = $this->db->getProductBySKU($this->ws->id_shop, null, true, true, true ,$this->ws->id_country, $this->ws->id_lang);
            $products_inventory = $this->db->getProductsInventory($this->ws->id_shop, $this->ws->id_country, false);
            
            foreach($lines as $line)
            {
		$ASIN = null;
		$SKU = null;
		
                if ( empty($line) ) continue ;
                if ( $count++ < 1 ) continue ;

                $result = explode("\t", $line) ;
                if ( count(array_keys($result)) < 4 ) continue ;

                list($SKU, $ASIN, $Price, $Qty) = $result ;

                $ASIN = trim($ASIN) ;
                $SKU = trim($SKU) ;

                if ( Amazon_Tools::ValidateSKU($SKU) && Amazon_Tools::ValidateASIN($ASIN) )
                {
                    //check product sku 
		    //ProcessInventory - SKU/Reference not found in your database
                    if ( !isset($products[$SKU]) || empty($products[$SKU]) )
                    {
			if($this->debug)
			    var_dump('Product : ' . $SKU . ' SKU/Reference not found in your database');
                        $count--; 
			continue ;
                    }	    
		    
		    // ProcessInventory - not found in your database
		    if( !isset($products[$SKU]['id_product']) || empty($products[$SKU]['id_product']))
		    {
			if($this->debug)
			    var_dump('Product ID : ' . $products[$SKU]['id_product'] . ' not found in your database');
			
                        $count--; 
			continue ;
                    }		    
		    
		    if ( isset($products[$SKU]['fba'])  && (bool)$products[$SKU]['fba'] )
		    {
			if($this->debug)
			    var_dump($products[$SKU] . ' fba');
			
			$count--; 
			continue;
		    }
		    
		    if ( isset($products[$SKU]['disable'])  && (bool)$products[$SKU]['disable'])
		    {
			if($this->debug)
			    var_dump($products[$SKU] . ' disable');
			
			$count--; 
			continue;
		    }
		    
		    $quantity = $products[$SKU]['quantity'];

		    if ( isset($products[$SKU]['force']) )
		    {
			$quantity = $products[$SKU]['force'];
		    }
		    
		    if ($Qty <= 0 && $quantity <= 0)
			continue;		    
		     
		    if ((int)$Qty != (int)$quantity)
		    {
			$quantity_mismatch[$SKU] = array('id_product' => $products[$SKU]['id_product'], 'amazon' => $Qty, 'feedbiz' => $quantity);
		    }
		    
                    $amazonItems[$SKU]['sku'] = $SKU ; 
                    $amazonItems[$SKU]['asin'] = $ASIN ; 
                    $amazonItems[$SKU]['price'] = (float)$Price ; 
                    $amazonItems[$SKU]['quantity'] = (int)$Qty ; 
                }
		
		#update ASIN to amazon_inventory
		$amazon_inventory_where = array(
		    "sku" => array('operation' => "=", 'value' => $SKU),
		    "id_shop" => array('operation' => "=", 'value' => $this->ws->id_shop),
		    "id_country" => array('operation' => "=", 'value' => $this->ws->id_country),
		);

                $id_product = (int) $products[$SKU]['id_product'];
                $id_product_attribute = (int) $products[$SKU]['id_product_attribute'];

                // if have in amazon_inventory update
                if(isset($products_inventory[$SKU]) && !empty($products_inventory[$SKU])) {
                    $amazon_inventory_data = array(
                        'asin' => $ASIN,
                        'date_upd' => date('Y-m-d H:i:s'),
                    );
                    $update_amazon_inventory .= $this->db->update_string(AmazonDatabase::$m_pref.'amazon_inventory', $amazon_inventory_data, $amazon_inventory_where );
                    
                // if doesn't exits insert
                } else {
                    $amazon_inventory_data = array(
                        "id_product" => $id_product,
                        "id_product_attribute" => $id_product_attribute,
                        "sku" => $SKU,
                        "id_shop" => $this->ws->id_shop,
                        "id_country" => $this->ws->id_country,
                        'asin' => $ASIN,
                        'action_type' => 'send', // the product already exist on Amazon.
                        'date_add' => date('Y-m-d H:i:s'),
                    );
                    $update_amazon_inventory .= $this->db->replace_string(AmazonDatabase::$m_pref.'amazon_inventory', $amazon_inventory_data);
                }
            }	   
	        
	    if (count($quantity_mismatch))
	    {
		$report = $log_id = array();
		
		foreach($quantity_mismatch as $SKU => $report_array)
		{
		    if (!$this->debug && !isset($log_id[$report_array['id_product']]))
		    {
			$updateFlag .= ImportLog::update_flag_offers(_ID_MARKETPLACE_AMZ_, $this->ws->ext, $this->ws->id_shop, $report_array['id_product'], 1);
			$log_id[$report_array['id_product']] = true;
		    }

		    $report[$SKU] = sprintf('Mismatching quantities report: SKU %s, feedbiz %s, amazon %s', $SKU, $report_array['feedbiz'], $report_array['amazon']);
		}
		
		if(strlen($updateFlag) > 10)
		{
		    if (!$this->debug){
			$this->db->exec_query($updateFlag);
		    } else {
			echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
			echo 'SQL : <pre>' . $updateFlag . '</pre>';
		    }
		}
		
		$this->ValidationError('listings data', $report); 
	    }
	    
	    $this->log('listings data', count($quantity_mismatch)); 
	    
	    if(strlen($update_amazon_inventory) > 10)
	    {
		$this->db->exec_query($update_amazon_inventory);
	    }
	    
        } else {
            self::$process->message = Amazon_Tools::l('Inventory is empty (4)') ; 
            return; 
        }
	
        if($this->debug) echo '<pre> Items : '. print_r ($amazonItems, true).'</pre>';
	
        if (!empty($amazonItems))
        {
            if(!$this->db->save_report_inventory($amazonItems, $this->ws->id_country, $this->ws->id_shop))
            {
                self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to save report')) ; 
                return(false);
            }	    
	    
            self::$process->inventory = $count;
            self::$process->flag = true;
        } else {           
	    self::$process->inventory = 0;
            self::$process->flag = true;
        }
        
        return(true);
    }
    protected function processFulfillmentInventory()
    {
	if($this->debug)
	    echo '<pre> function processFulfillmentInventory </pre>';

        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
        if ( ($result = file_get_contents($this->file_inventory)) === false )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to read input file'), $this->file_inventory) ;
            return(false) ;
        }
        
//	if($this->debug) echo '<pre> Result : '.$result.'</pre>';

        if ( $result == null or empty($result) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Fulfillment data is empty (2)')) ;
            return(false) ;
        }

        $lines = explode("\n", $result) ;

        if ( ! is_array($lines) || ! count($lines) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Fulfillment data is empty (3)')) ;
            return(false) ;
        }

        $amazonItems = array() ;
        $count = 0 ;

        if(!empty($lines)) {

            // get products
            $products = $this->db->getProductBySKU($this->ws->id_shop, null, true);
            
//	    if($this->debug) echo '<pre> products : '. print_r ($products, true).'</pre>';

            foreach($lines as $line)
            {
                if ( empty($line) ) continue ;
                if ( $count++ < 1 ) continue ;

                $result = explode("\t", $line) ;

                if ( count(array_keys($result)) < 4 ) continue ;

                list($sku, $fnsku, $asin, $condition_type, $Warehouse_Condition_code, $Quantity_Available) = $result ;

                $asin = trim($asin) ;
                $sku = trim($sku) ;
                $Quantity_Available = trim($Quantity_Available) ;

                if ( Amazon_Tools::ValidateSKU($sku) && Amazon_Tools::ValidateASIN($asin) )
                {
                    //check product sku
                    if ( !isset($products[$sku]['id_product']) || empty($products[$sku]['id_product']) )
                    {
                        $count--;
			continue ;
                    }

                    // UNSELLABLE
                    if( isset($Warehouse_Condition_code) && (trim($Warehouse_Condition_code) == "UNSELLABLE"))
                    {
                        $count--;
			continue ;
                    }

                    $amazonItems[$sku]['SKU'] = $sku ;
                    $amazonItems[$sku]['InStockSupplyQuantity'] = $Quantity_Available ;
                    $amazonItems[$sku]['TotalSupplyQuantity'] = $Quantity_Available ;
                }
            }
        } else {
            self::$process->message = Amazon_Tools::l('Fulfillment data is empty (4)') ;
            return;
        }

//        if($this->debug) echo '<pre> Items : '. print_r ($amazonItems, true).'</pre>';
        return($amazonItems);
    }

    protected function processFulfillmentData()
    {
	if($this->debug) 
	    echo '<pre> function processFulfillmentData </pre>';	
	
        $step_title = Amazon_Tools::l(self::$steps[self::$process->step]);
        if ( ($result = file_get_contents($this->file_inventory)) === false )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to read input file'), $this->file_inventory) ; 
            return(false) ;                         
        }	
	
	if($this->debug) echo '<pre> Result : '.$result.'</pre>';
	
        if ( $result == null or empty($result) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Fulfillment data is empty (2)')) ; 
            return(false) ;            
        }
        
        $lines = explode("\n", $result) ;

        if ( ! is_array($lines) || ! count($lines) )
        {
            self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Fulfillment data is empty (3)')) ; 
            return(false) ;     
        }

        $amazonItems = array() ;
        $count = 0 ;
        
        if(!empty($lines)) {
            
            // get products
            $products = $this->db->getProductBySKU($this->ws->id_shop, null, true);            
	    
	    if($this->debug) echo '<pre> products : '. print_r ($products, true).'</pre>';
	    
            foreach($lines as $line)
            {
		//if($this->debug) echo '<pre> line : '. print_r ($line, true).'</pre>';
		
                if ( empty($line) ) continue ;
                if ( $count++ < 1 ) continue ;

                $result = explode("\t", $line) ;
		    
		//if($this->debug) echo '<pre> result : '. print_r ($result, true).'</pre>';
		
                if ( count(array_keys($result)) < 4 ) continue ;
		
                list($sku, $fnsku, $asin, $product_name, $product_group, $brand, $fulfilled_by, $has_local_inventory, $your_price, $sales_price, $longest_side, $median_side	, $shortest_side, $length_and_girth, $unit_of_dimension, $item_package_weight, $unit_of_weight, $product_size_weight_band, $currency, $estimated_fee, $estimated_referral_fee_per_unit, $estimated_variable_closing_fee, $expected_domestic_fulfilment_fee_per_unit, $expected_efn_fulfilment_fee_per_unit_uk, $expected_efn_fulfilment_fee_per_unit_de, $expected_efn_fulfilment_fee_per_unit_fr, $expected_efn_fulfilment_fee_per_unit_it, $expected_efn_fulfilment_fee_per_unit_es) = $result ;

                $asin = trim($asin) ;
                $sku = trim($sku) ;
		
		//if($this->debug) echo '<pre> asin : '. print_r ($asin, true).'</pre>';
		//if($this->debug) echo '<pre> sku : '. print_r ($sku, true).'</pre>';
		
                if ( Amazon_Tools::ValidateSKU($sku) && Amazon_Tools::ValidateASIN($asin) )
                {
		    //if($this->debug) echo '<pre> count : '. print_r ($count, true).'</pre>';
		    
                    //check product sku
                    if ( !isset($products[$sku]['id_product']) || empty($products[$sku]['id_product']) )
                    {			
                        $count--; 
			continue ;
                    }

                    $amazonItems[$sku]['id_product'] = $products[$sku]['id_product'] ; 
                    $amazonItems[$sku]['id_product_attribute'] = isset($products[$sku]['id_product_attribute']) ? $products[$sku]['id_product_attribute'] : 0 ; 
                    $amazonItems[$sku]['sku'] = $sku ; 
                    $amazonItems[$sku]['asin'] = $asin ; 
                    $amazonItems[$sku]['fulfilled_by'] = $fulfilled_by ; 
                    $amazonItems[$sku]['local_inventory'] = $products[$sku]['quantity'] ; 
                    $amazonItems[$sku]['your_price'] = (float)$your_price ; 
                    $amazonItems[$sku]['sales_price'] = (float)$sales_price ; 
                    $amazonItems[$sku]['currency'] = $currency ; 
                    $amazonItems[$sku]['estimated_fee'] = (float)$estimated_referral_fee_per_unit ; 
                    $amazonItems[$sku]['fulfilment_fee'] = (float)$expected_domestic_fulfilment_fee_per_unit ; 	

		    //if($this->debug) echo '<pre> amazonItems : '. print_r ($amazonItems, true).'</pre>';
                }
            }
        } else {
            self::$process->message = Amazon_Tools::l('Fulfillment data is empty (4)') ; 
            return; 
        }
	 
        if($this->debug) echo '<pre> Items : '. print_r ($amazonItems, true).'</pre>';
	
        if (!empty($amazonItems))
        {
            if(!$this->db->save_fba_data($amazonItems, $this->ws->id_shop, $this->ws->id_country))
            {
                self::$process->errors = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, $step_title, Amazon_Tools::l('Unable to save data')) ; 
                return(false);
            }
            self::$process->inventory = $count;
            self::$process->flag = true;
        } else {           
	    self::$process->inventory = 0;
            self::$process->flag = true;
        }
       
        return(true);
    }

    protected static function isExpired($timestamp, $expiration = 3600)
    {
        $now = time() ;
        
        if ( ! $timestamp )
            return(true) ;
        
        if ( $now - $timestamp > $expiration )
            return(true) ;
        
        return(false) ;
    }
    
    public function debugXML($xml)
    {
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;

        $output  = "<pre>" ;
        $output .= htmlspecialchars($dom->saveXML()) ;
        $output .= "</pre>" ; 
        
        //save xml file
        if($this->debug)
        {
            $filename =  self::$steps[self::$process->step];
            $this->ws->generate_xml($this->ws->user,  'report/' . $filename, $dom->saveXML());
        }
        
        return($output) ;
    }   
     
    public static function encodeText($text)
    {
        return(str_replace( array('&','"', "'", '\\', '<', '>'), '', $text));
    }
    
    private function initImportDirectory()
    {
        $output_dir = USERDATA_PATH. $this->ws->user . '/amazon';

        // Check rights
        if (!is_dir($output_dir) && !mkdir($output_dir))
        {
            self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to create path')) ;
            return false;
        }
        
        $this->import = $output_dir . '/report/' ;

        if ( ! is_dir($this->import) )
        {
            if ( ! @mkdir($this->import) )
            {
                self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to create import directory')) ;
                die;
            }
            
            @chmod($this->import, 0777) ;
        }
    }    
    
    private function log($action_type='listings data', $skipped=0){
        
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->ws->id_country,
            "id_shop"       => (int)$this->ws->id_shop,
            "action_type"   => $action_type,
            "no_process"    => $skipped, 
            "no_send"	    => 0, 
            "no_skipped"    => 0, 
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
	if($skipped > 0)
	    $log['detail'] = base64_encode(serialize(array('error' => $skipped . ' unconsistencies automatically fixed')));
	
        if(!$this->debug){
            $this->db->update_log($log);
	} else {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'LOGS : <pre>' . print_r($log, true) . '</pre>';
	}
    }
    
    private function ValidationError($process, $message) {
	
	$sql = '';	

	foreach ($message as $error)
	{		
	    $error_data = array(
		'batch_id' => $this->batchID,
		'id_country' => $this->ws->id_country,
		'id_shop' => $this->ws->id_shop,
		'action_process' => $process,
		'action_type' => 'warning',
		'message' => strval($error),
		'date_add' => date("Y-m-d H:i:s"),
	    );            

	    $sql .= $this->db->save_validation_log($error_data, false);
	    unset($error_data);
	}

	if(strlen($sql) > 10)
	{
	    if (!$this->debug){
		$this->db->exec_query($sql);
	    } else {
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'SQL : <pre>' . $sql . '</pre>';
	    }
	}
	
	unset($sql);
    }
}