<?php

require_once(dirname(__FILE__) . '/../../FeedBiz.php');
require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.productData.php';
require_once dirname(__FILE__) . '/../classes/amazon.xml.validator.php';
require_once dirname(__FILE__) . '/../classes/amazon.webservice.php';
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');
require_once dirname(__FILE__) . '/../classes/amazon.error.resolution.php';
require_once dirname(__FILE__) . '/amazon.repricing.automaton.php';

class AmazonFeeds {

    const Amazon = 'amazon';
    const PRODUCT = 'product';
    const INVENTORY = 'inventory';
    const PRICE = 'price';
    const IMAGE = 'image';
    const RELATIONSHIP = 'relationship';
    const SHIPPING = 'shipping_override';
    const MAX_SEND_PRODUCTS = 1999;
    
    public $message;
    private $user;
    private $skipped;
    private $productIndex;
    private $process;
    private $is_from_export_reprice;

    private $debug_url;
    
    public function __construct($params, $operation_type, $debug = false, $cron = false) {               
                
        $this->message = new stdClass();
        $this->params = $params;
        $this->debug = $debug;
        $this->cron = $cron;
        $this->option = new stdClass();
        $this->operation_type = $operation_type;
        $this->process = $operation_type;
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
          
        if ($this->operation_type == AmazonDatabase::SYNC){
            $this->process = AmazonDatabase::SYNCHRONIZE;
        } else if ($this->operation_type == AmazonDatabase::CREATE) {
            $this->process = AmazonDatabase::CREATE;
        } else if ($this->operation_type == AmazonDatabase::DELETE) {
            $this->process = AmazonDatabase::DELETE;
        } else {
            $this->process = $this->operation_type . " " . strtolower(Amazon_Tools::l('products'));
        }
                
        $this->batchID = uniqid();
        $this->time = $this->params->time = date('Y-m-d H:i:s');
        $this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
        $this->title = sprintf('%s %s', Amazon_Tools::l('Amazon_Marketplace_Automaton_started_on'), $this->time);
        
        $this->message_type = sprintf(Amazon_Tools::l('AmazonS_s_started_on_s'), $this->params->ext, $this->process, '');

        if (isset($this->params->mode) && $this->params->mode == AmazonDatabase::SYNC)
            $this->message_type = sprintf(Amazon_Tools::l('Amazon_s_synchronize_started_on_s'), $this->params->ext, '');

        if (isset($this->params->user) && !empty($this->params->user)) {
            $this->user = $this->params->user;
            unset($this->params->user);
        }
        if (isset($this->params->mode) && ($this->operation_type == AmazonDatabase::CREATE || $this->operation_type == AmazonDatabase::DELETE)) {
            $mode = html_entity_decode(preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;", urldecode($this->params->mode)), null, 'UTF-8');
            $this->option = json_decode($mode);
            
            if($this->operation_type == AmazonDatabase::DELETE) {
                $this->params->mode = AmazonDatabase::DELETE;
            } else {
                unset($this->params->mode);
            }
        }        
        
        if ($this->debug == false) {
            $this->StartProcessMessage();
        }
        
        $this->amazonDatabase = $this->AmazonDatabase();
        $this->amazonApi = $this->AmazonApi();
        
        AmazonXSD::initialize();

        /*if($this->params->user_name == 'u00000000000026'){
            $this->debug_url = dirname(__FILE__).'/../../../../assets/apps/users/u00000000000026/debugs/amazon_'.$operation_type.'_'.$this->batchID.'('.$this->params->countries.').txt';
            file_put_contents($this->debug_url,
                print_r(array(
                    'date'              => date('Y-m-d H:i:s'),
                    'ext'               => $this->params->ext,
                    'repricing_type'    => isset($this->params->repricing_type) ? $this->params->repricing_type : '',
                    'region'            => isset($this->params->region) ? $this->params->region : '',
                    'id_country'        => isset($this->params->id_country) ? $this->params->id_country : '',
                    'currency'          => isset($this->params->currency) ? $this->params->currency : '',
                    'debug'             => $this->debug,
                    'cron'              => $this->cron,
                    'option'            => $this->option,
                    'operation_type'    => $this->operation_type,
                    'process'           => $this->process,                    
                ), true), FILE_APPEND);
        }*/
    }

    public function FeedsData($products=null, $is_from_export_reprice=false) {
                       
	$this->is_from_export_reprice = $is_from_export_reprice; //17-03-2016 //use for save reprice report log : to know why we update price ;
	
        if (isset($this->proc_rep)) {
            
            if(!isset($this->message->product))
                $this->message->product = new stdClass();
            
            $this->message->batchID = $this->batchID;
            $this->message->type = $this->message_type;
            $this->message->title = $this->title;
            $this->message->product->message = Amazon_Tools::l('Preparing_products') . "..";
            $this->proc_rep->set_status_msg($this->message);
        }
		
        // check feature status
        $this->checkStatusFeatures();   
                    
        if($this->cron && ($this->operation_type != AmazonDatabase::CREATE) && ($this->operation_type != AmazonDatabase::REPRICING)) {
            
            $this->option->cron =  true;
            
            // CHECK UPDATE TYPE  
            $status = $this->amazonDatabase->get_amazon_status($this->params->id_country, $this->params->id_shop, $this->params->mode, true, null, 1);

            if (isset($status) && count($status) > 0 && !empty($status)) {
                
                # Delete status before set mode
                if(!$this->debug) {
                    $this->amazonDatabase->delete_amazon_status($this->params->id_country, $this->params->id_shop, $this->params->mode);
                }
                
                # change parameter value
                $this->params->update_type = 'all'; 
            }
            
        } elseif($this->cron && ($this->operation_type == AmazonDatabase::CREATE)) { // checking for 'recreate'

            $status = $this->amazonDatabase->get_amazon_status($this->params->id_country, $this->params->id_shop, 'recreate', true );

            if (isset($status) && count($status) > 0 && !empty($status)) {

                # Delete recreate
                if(!$this->debug) {
                    $this->amazonDatabase->delete_amazon_status($this->params->id_country, $this->params->id_shop, 'recreate');
                }

            }
        }
        
        global $user_data;
        $user_data = $this->params;
        $user_data->batchID = $this->batchID;
        $user_data->operation_type = $this->operation_type;
        $user_data->cron = $this->cron;
        $this->error = $this->output = null;

        /************************** Status **********************/
        $allow_quantity = $allow_price = $allow_image = $allow_relationship = $allow_product = true;
        $allow_shipping_override = false;

        // dont send quantity
        if ((isset($this->params->synchronize_quantity) && !$this->params->synchronize_quantity) /* || $this->params->id_mode == 1 */) {
            $allow_quantity = false;
        }
        // dont send price
        if ((isset($this->params->synchronize_price) && !$this->params->synchronize_price) && ($this->operation_type != AmazonDatabase::DELETE)) {
            $allow_price = false;
            if($this->operation_type == AmazonDatabase::CREATE) {
                $allow_price = true;
            }

            // IF update all offers and not cron job
            if ($this->operation_type == AmazonDatabase::SYNC && !isset($this->option->cron) && (!isset($this->params->update_type) || empty($this->params->update_type) )) {
                $allow_price = true;
            }
        }
        // creattion
        if ($this->operation_type == AmazonDatabase::CREATE) {
            if ((isset($this->option->send_image) && !$this->option->send_image)) {
                $allow_image = false;
            }
        }
        // delete
        if ($this->operation_type == AmazonDatabase::DELETE) {
            $allow_quantity = $allow_price = $allow_relationship = $allow_image = false;
        }
        // send relationship
        if ($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_relation_ship) && $this->option->send_relation_ship) {
            $allow_quantity = $allow_price = $allow_image = $allow_product = $allow_shipping_override = false;
        }
        // send only image
        if ($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_only_image) && $this->option->send_only_image) {
            $allow_product = $allow_quantity = $allow_price =  $allow_relationship = $allow_shipping_override = false;
        }

        // update offer ; if SYNC and update_type = offer ; we will send only offer on both cron and not cron
        if ($this->operation_type == AmazonDatabase::SYNC /*&& isset($this->option->cron)*/ && isset($this->params->update_type) && $this->params->update_type == 'offer') {
            $allow_product = $allow_image = $allow_relationship = false;
        }

        // repricing
        if ($this->operation_type == AmazonDatabase::REPRICING) {
            $allow_product = $allow_quantity = $allow_image = $allow_relationship = false;
            $allow_price = true;
        }

        // shipping override
        if(isset($this->shipping_override) && $this->shipping_override){
            if($this->operation_type == AmazonDatabase::CREATE || (isset($this->option->cron) && isset($this->params->update_type) && $this->params->update_type != 'offer') ){
                if(!isset($this->option->send_relation_ship) && !isset($this->option->send_only_image)){ // if not send relation_ship only and only_image
                    $allow_shipping_override = true;
                }
            }
            // send shipping override only
            if((isset($this->option->send_shipping_override) && $this->option->send_shipping_override)){
                $allow_quantity = $allow_price = $allow_image = $allow_relationship = $allow_product = false;
                $allow_shipping_override = true;
            }
            // checke carrier mapping.
            $ShippingOverride = $this->amazonDatabase->get_mapping_shipping_override($this->params->id_country, $this->params->id_shop);

            if($allow_shipping_override) {
                if(!isset($ShippingOverride) || empty($ShippingOverride)){
                    $this->error = $this->message->error = Amazon_Tools::l("shipping_override_missing_mapping_carrier");

                    if (isset($this->proc_rep))
                        $this->proc_rep->set_error_msg($this->message);

                    $this->StopFeed();

                }
            }
        }

        // send products only
        if (isset($this->option->send_products) && $this->option->send_products) {
            $allow_quantity = $allow_price = $allow_image = $allow_relationship = $allow_shipping_override = false;
        }
        
        if($this->debug){
            echo "<br/>Allow quantity : ".(bool)$allow_quantity ."<br/>";
            echo "<br/>Allow price : ".(bool)$allow_price ."<br/>";
            echo "<br/>Allow image : ".(bool)$allow_image ."<br/>";
            echo "<br/>Allow relationship : ".(bool)$allow_relationship ."<br/>";
            echo "<br/>Allow product : ".(bool)$allow_product ."<br/>";
            echo "<br/>Allow shipping : ".(bool)$allow_shipping_override ."<br/>";
        }             
        /******************** Status ******************/
        
        //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
        //    print_r(array('allow_quantity'=>(int)$allow_quantity,'allow_price'=>(int)$allow_price,'allow_image'=>(int)$allow_image,'allow_relationship'=>(int)$allow_relationship,'allow_product'=>(int)$allow_product,'allow_shipping_override'=>(int)$allow_shipping_override, ), true), FILE_APPEND);

        if(!isset($products) || empty($products)){
            $products = $this->amazonDatabase->getProductsFromFeedbizDatabase($this->params, $this->operation_type, $this->option); 
        }   	

        $skipped_message = array();
        
        if(isset($products['skipped']) && $products['skipped'] > 0) {
            $this->skipped = (string)$products['skipped'];
            $skipped_message['no_skipped'] = sprintf("%s %s", $this->skipped, Amazon_Tools::l('skipped_items'));
        }       
        
        if(isset($products['productIndex']) && $products['productIndex'] > 0) {
            $this->productIndex = (string)$products['productIndex'];
            $skipped_message['no_send'] = sprintf("%s %s", $this->productIndex, sprintf(Amazon_Tools::l('items_to_xx_with_Amazon'), $this->process));
        }        

        if (isset($this->proc_rep) && !empty($skipped_message)) {
            $this->message->result = $skipped_message;
            $this->proc_rep->set_status_msg($this->message);
        }      
        
        // set message object
        if ($allow_quantity){
            $this->message->{AmazonFeeds::INVENTORY} = new stdClass();
        }
        if ($allow_price){
            $this->message->{AmazonFeeds::PRICE} = new stdClass();
        }
        if ($allow_image){
            $this->message->{AmazonFeeds::IMAGE} = new stdClass();
        }
        if ($allow_relationship){
            $this->message->{AmazonFeeds::RELATIONSHIP} = new stdClass();
        }
        if ($allow_shipping_override){
            $this->message->{AmazonFeeds::SHIPPING} = new stdClass();
        }

        // start set repricing message
        if ($this->operation_type == AmazonDatabase::CREATE && isset($this->repricing) && $this->repricing &&
            (!isset($this->option->send_only_image) && !isset($this->option->send_relation_ship) && !isset($this->option->send_shipping_override))){
            $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();
        }
        if ($this->operation_type == AmazonDatabase::SYNC && isset($this->repricing) && $this->repricing && isset($products["repricing"]) && !empty($products["repricing"])){
            $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();
        }
        if ($this->operation_type == AmazonDatabase::SYNC && isset($this->repricing) && $this->repricing && (!isset($this->cron) || !$this->cron || $this->cron == false) ){
            $this->message->{AmazonRepricingAutomaton::REPRICE} = new stdClass();
        }

        if (!isset($products['product']) || empty($products['product'])) {
            $this->error = Amazon_Tools::l('Sorry_no_items_found_process_completed_No_items_to_be_send') ;
            
            if (isset($this->proc_rep)) {
                
                if(!isset($this->message->no_product))
                    $this->message->no_product = new stdClass();
                
                $this->message->no_product = $this->error;
                $this->proc_rep->set_status_msg($this->message);
            }
            
        } else {
              
	    // split to 1000
	    $nProducts = count($products['product']);
        
	    if($this->debug){
		echo "<br/>Count Products : $nProducts <br/>";
	    }
	    
	    $full_products = $products['product'];
	    
	    $fetch = true;
	    $ofset = 0;
	    
	    if($allow_product && $this->operation_type != AmazonDatabase::DELETE) {
		$limit = self::MAX_SEND_PRODUCTS; //$nProducts; 
	    } else {
		$limit = $nProducts; 
	    }
	    
	    while ($fetch) {
		
		if($ofset != 0){
		    
		    if (isset($this->proc_rep)) {
			
			$number_of_sent = $limit+$ofset;
			
			if ($allow_product) {
			    $this->message->product = new stdClass();
			    $this->message->product->message = Amazon_Tools::l('validating_product') . " [$ofset to $number_of_sent of $nProducts] ..";
			} else {
			    unset($this->message->product);
			}
			$this->proc_rep->set_status_msg($this->message);
		    }

		} else {

		    if (isset($this->proc_rep)) {
			if ($allow_product) {
			    $this->message->product->message = Amazon_Tools::l('Products_preparing_successful_start_validation') . " [$ofset - $limit] ..";
			} else {
			    unset($this->message->product);
			}
			$this->proc_rep->set_status_msg($this->message);
		    }
		}

		if ($this->debug) {
		    echo "<br/>Ofset : $ofset <br/>";
		    echo "Limit : $limit <br/>";
		}
		
		if($allow_product) {
		    $products['product'] = array_slice($full_products, $ofset, $limit);
		} else {
		    $products['product'] = $full_products;
		    $fetch = false;
		}   

		//////Send Products//////
		if($allow_product) {
		    $products = $this->FeedsProcess($products, AmazonFeeds::PRODUCT);
		}

		if ($this->debug == true) {
		    if ($allow_quantity)
			$this->FeedsProcess($products, AmazonFeeds::INVENTORY);
		    if ($allow_price)
			$this->FeedsProcess($products, AmazonFeeds::PRICE);
		    if ($allow_image)
			$this->FeedsProcess($products, AmazonFeeds::IMAGE);
		    //		    if ($allow_relationship)
//			$this->FeedsProcess($products, AmazonFeeds::RELATIONSHIP);
                    if ($allow_relationship && isset($products['relationship']) && !empty($products['relationship'])) {
                        if($nProducts <= ($ofset+$limit)){ // send relationship only last loop avoid duplicate
                            $this->FeedsProcess($products, AmazonFeeds::RELATIONSHIP);
                        }
                    }
		    if ($allow_shipping_override)
			$this->FeedsProcess($products, AmazonFeeds::SHIPPING);

		    echo '<br/><br/>';
		    echo ' ----------------------products-------------------------';
		    echo '<pre>' . print_r($products, true) . '</pre>';
		    echo ' --------------------end products-------------------------'; 
		    echo '<br/>finish - ' . date('H:i:s') . '<br/>';  
		    //exit;
		}

		if($this->debug == false){
		    if (isset($products["product"]) && !empty($products["product"]) && sizeof($products["product"]) > 0) {

			//////Send Inventory//////
			if ($allow_quantity) {
			    if (!$this->FeedsProcess($products, AmazonFeeds::INVENTORY))
				$this->NoneProducts(AmazonFeeds::INVENTORY);
			}
			//////Send Price//////
			if ($allow_price) {
			    if (!$this->FeedsProcess($products, AmazonFeeds::PRICE))
				$this->NoneProducts(AmazonFeeds::PRICE);
			}
			//////Send Images//////
			if ($allow_image) {
			    if (!$this->FeedsProcess($products, AmazonFeeds::IMAGE))
				$this->NoneProducts(AmazonFeeds::IMAGE);
			}
			//////Send Relationship//////
			if ($allow_relationship && isset($products['relationship']) && !empty($products['relationship'])) {
                            if($nProducts <= ($ofset+$limit)){ // send relationship only last loop avoid duplicate
                                if (!$this->FeedsProcess($products, AmazonFeeds::RELATIONSHIP)) {
                                    $this->NoneProducts(AmazonFeeds::RELATIONSHIP);
                                } 
                            }
			}
			//////Send Shipping Override//////
			if ($allow_shipping_override) {
                            
                            if(isset($products['override']) && !empty($products['override'])){
                                if (!$this->FeedsProcess($products, AmazonFeeds::SHIPPING)) {
                                    $this->NoneProducts(AmazonFeeds::SHIPPING);
                                }
                            }
			}

		    // Send Relationship Only
		    } else if($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_relation_ship) && $this->option->send_relation_ship) { 

			if ($allow_relationship) {
			    if (isset($this->proc_rep)) {
				$this->message->product->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
			    }
			    if (!$this->FeedsProcess($products, AmazonFeeds::RELATIONSHIP)){
				$this->NoneProducts(AmazonFeeds::RELATIONSHIP);
			    }
			}

		    } else {

			if (isset($this->params->mode) && $this->params->mode == AmazonDatabase::SYNC) {
			    $success_message = sprintf("%s %s. %s", AmazonDatabase::SYNCHRONIZE, Amazon_Tools::l("completely"), Amazon_Tools::l("See_result_in_Report_tab"));
			    $this->message->product->status = 'Success';
			    $this->message->product->message = $success_message;
			    $this->proc_rep->set_status_msg($this->message);
			}

			if ($allow_quantity)
			    $this->message->{AmazonFeeds::INVENTORY}->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
			if ($allow_price)
			    $this->message->{AmazonFeeds::PRICE}->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
			if ($allow_image)
			    $this->message->{AmazonFeeds::IMAGE}->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
			if ($allow_relationship)
			    $this->message->{AmazonFeeds::RELATIONSHIP}->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
			if ($allow_shipping_override)
			    $this->message->{AmazonFeeds::SHIPPING}->no_product = sprintf("%s", Amazon_Tools::l("Nothing_to_send"));
		    }
		}
		
		$ofset = $ofset+$limit;
		
		if($nProducts < $ofset){
		    $fetch = false;
		}
		
		unset($products["product"]);
	    }
	    
	    // set product back ;
	    $products["product"] = $full_products;
	    
	    if($this->debug){
		echo "<br/>After Count Products :". count($products["product"])." <br/>";
	    }
        }
	
        if ($this->debug == false && $this->cron == false) {        
            $this->SendEmailResultValue();
            //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
            //    print_r(array('SendEmailResultValue'=>true), true), FILE_APPEND);
        }
        
        if($this->operation_type == AmazonDatabase::REPRICING){ // If repricing we need to delete message before exit; see functions/amazon.repricing.automaton.php
            //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
            //    print_r(array('REPRICING'=>true), true), FILE_APPEND);
            return $this->StopFeed();
        } else {
            
            if($this->operation_type == AmazonDatabase::CREATE && isset($products["product"]) && !empty($products["product"]) && isset($this->repricing) && $this->repricing 
            && (!isset($this->option->send_only_image) && !isset($this->option->send_relation_ship) && !isset($this->option->send_shipping_override)) ){
                // not reprice on override/image/relation
                
                // Repricing
                $repricing = new AmazonRepricingAutomaton($this->params, $this->debug);
                $proc_rep = null;
                if(isset($this->proc_rep)){
                    $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Start repring') . "..";
                    $this->proc_rep->set_status_msg($this->message);
                    $proc_rep = $this->proc_rep;
                }
                 
                $is_exit_repricing_process = false;
                $update_logs = $repricing->Dispatch(AmazonRepricingAutomaton::REPRICE_PRODUCT_CREATION, $products["product"], $proc_rep, $is_exit_repricing_process);

                // update amazon_repricing_flag , set flag = 0 // next feed will not get this repricing
                /*if(!$this->debug && $update_logs){
                    $this->amazonDatabase->update_amazon_repricing_flag($this->params->id_shop, $this->params->id_country, $update_logs);
                } else {
                    echo "<br/>update_logs :<pre>". print_r($update_logs, true)."</pre> <br/>";
                }*/

                //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
                //    print_r(array('CREATE'=>true,'repricing'=>$this->repricing), true), FILE_APPEND);
            } 
            // Reprice updated product // instock //active
            else if($this->operation_type == AmazonDatabase::SYNC && isset($this->repricing) && $this->repricing /*&& isset($this->cron) && $this->cron == true*/){  

                if(isset($this->cron) && $this->cron == true)/*!isset($this->cron) || !$this->cron || $this->cron == false)*/{
                    
                } else {
                    $products["repricing"] = $products["product"];
                }

                $fetch = true;
                $ofset = 0;
                
                $full_products = isset($products["repricing"]) ? $products["repricing"] : array();
                $nProducts = count($full_products) ? count($full_products) : false ;
                
                if($this->debug){
                    echo "<br/>Count Full Products Reprice :". $nProducts." <br/>";
                }

                $limit = 1000; //self::MAX_SEND_PRODUCTS; //$nProducts;

                if($nProducts){
                    while ($fetch) {

                        $products['repricing'] = array_slice($full_products, $ofset, $limit);

                        if($this->debug && isset($products["repricing"])){
                            echo "<br/>Count Products Reprice :". count($products["repricing"])." <br/>";
                        }

                        if(isset($products["repricing"]) && !empty($products["repricing"])) { // $products["repricing"] return from amazon.product.php

                            $repricing = new AmazonRepricingAutomaton($this->params, $this->debug);
                            $proc_rep = null;

                            if(isset($this->proc_rep)){
                                $this->message->{AmazonRepricingAutomaton::REPRICE}->message = Amazon_Tools::l('Start repring') . "[$ofset - $limit]" . " ..";
                                $this->proc_rep->set_status_msg($this->message);
                                $proc_rep = $this->proc_rep;
                            }

                            $is_exit_repricing_process = false;
                            $update_logs = $repricing->Dispatch(AmazonRepricingAutomaton::REPRICE_PRODUCT_CREATION, $products["repricing"], $proc_rep, $is_exit_repricing_process);
                            // update amazon_repricing_flag , set flag = 0 // next feed will not get this repricing
                            /*if(!$this->debug && $update_logs){
                                $this->amazonDatabase->update_amazon_repricing_flag($this->params->id_shop, $this->params->id_country, $update_logs);
                            } else {
                                echo "<br/>update_logs :<pre>". print_r($update_logs, true)."</pre> <br/>";
                            }*/
                        }

                        $ofset = $ofset+$limit;

                        if($nProducts < $ofset){
                            $fetch = false;
                        }
                    }
                }

                //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
                //    print_r(array('SYNC'=>true,'repricing'=>$this->repricing), true), FILE_APPEND);
            }
            
            $this->StopFeed();
        }            
    }

    private function FeedsProcess($products, $feed_type) {        
    
        //1. Get XML
        $xml = AmazonXml::getXmlRequest(ucwords($feed_type), $this->operation_type, $products, $this->amazonDatabase, $this->debug);

        //No data
        if (!isset($xml) || empty($xml) || $xml == "" || $xml == false) {
            if ($this->debug == true) {
                echo '<br/> NoneProducts <br/>';
                echo $feed_type;
                echo '<br/>';
            }
            if($feed_type == "Product"){
                $this->UpdateFlag($products['product'], $feed_type);
            }
            $this->NoneProducts($feed_type);
            return(false);
        }
        
        $pxml = new SimpleXMLElement($xml);
        
        if ($this->debug == true) {
            echo '<br/> ----------------------' . $feed_type . ' xml------------------------- <br/>';
            echo '<pre style="background: #ccc; padding: 10px;">'.htmlentities($xml).'</pre>';
            echo '<br/>';

            return $products;
        }
        
        if ($this->operation_type != AmazonDatabase::REPRICING)
            $this->amazonDatabase->truncate_validation_log($this->params->id_shop, $this->params->id_country, $this->operation_type, $feed_type);

        if ($pxml->MessageType == "Error") {
            
            $this->UpdateFlag($products['product'], $feed_type);
            return $this->ValidationError($pxml, $feed_type, $products);
            
        } else {
                        
            if (isset($this->proc_rep)) {
                $this->message->$feed_type->message =  sprintf(Amazon_Tools::l("Sending_s_feed_to_Amazon_s"), $feed_type, '.. ');
                $this->proc_rep->set_status_msg($this->message);
            }

            //2. Send XML to Amazon
            $data = $this->amazonApi->updateFeeds($xml, $feed_type);
            
            if (isset($data->Error) && is_object($data->Error)) {

                $this->UpdateFlag($products['product'], $feed_type);

                $this->amazonApi->generate_xml($this->params->user_name, 'update_' . $feed_type . '_error_' . $this->batchID, $data->saveXML());
                $this->error = strval($data->Error->Message);
                $this->message->error = sprintf('%s %s, %s : %s', Amazon_Tools::l('Update'), ucwords($feed_type), Amazon_Tools::l("Error"), $this->error); 

                if (isset($this->proc_rep))
                    $this->proc_rep->set_error_msg($this->message);

                $this->StopFeed();
            } else {

                if (!isset($data) || empty($data) || !$data) {

                    sleep(10); //sleep 10 sec.
                    
                    $data = $this->amazonApi->updateFeeds($xml, $feed_type); // send again 

                    // if can't send feed set error
                    if (!isset($data) || empty($data)) {     

                        $this->UpdateFlag($products['product'], $feed_type);
                        $this->error = Amazon_Tools::l('Empty Feedsubmission Id') . " ($feed_type)";
                        $this->message->error = sprintf('%s %s, %s : %s', Amazon_Tools::l('Update'), ucwords($feed_type), Amazon_Tools::l("Error"), $this->error); 
                        if (isset($this->proc_rep))
                            $this->proc_rep->set_error_msg($this->message);

                        $this->StopFeed();
                    }
                }

                $SubmissionId = $data;

                /*if(in_array($feed_type, array("Inventory", "inventory")) && $this->params->user_name == 'u00000000000026'){
                    file_put_contents($this->debug_url,
                        print_r(array(                    
                            'feed_type'         => $feed_type,
                            'products'          => $products,
                            'xml'               => htmlentities($xml)
                        ), true), FILE_APPEND);
                }*/

                //3. Get Feed Submission List from Amazon
                if ($this->GetFeedSubmissionList($SubmissionId, $feed_type, $products)) {

                    sleep(10);
                    //4. Get Feed Submission Result from Amazon
                    return $this->GetFeedSubmissionResult($SubmissionId, $feed_type, $products);
                }
            }
        }
    }

    private function GetFeedSubmissionList($SubmissionId, $feed_type, $products = null) {
        
        if (isset($this->proc_rep)) {
            $this->message->$feed_type->message = sprintf(Amazon_Tools::l('Getting_Submission_List_ID_s_s'), $SubmissionId, '..');
            $this->proc_rep->set_status_msg($this->message);
        } 
            
        $FeedSubmissionListResult = $this->amazonApi->getFeedSubmissionListResult($SubmissionId, $this->amazonDatabase, $this->params, $this->time, $feed_type);

        if (isset($FeedSubmissionListResult->Error) && is_object($FeedSubmissionListResult->Error)) {
            $this->amazonApi->generate_xml($this->params->user_name, 'get_submission_list_' . $feed_type . '_error_' . $this->batchID, $FeedSubmissionListResult->saveXML());
            $this->error = strval($FeedSubmissionListResult->Error->Message);
            $this->message->error = sprintf(Amazon_Tools::l('Get_submission_List_Error_s_s_s'), $this->error, $feed_type, '..');
            if (isset($this->proc_rep))
                $this->proc_rep->set_error_msg($this->message);
            
            if(isset($products['product']))
                $this->UpdateFlag($products['product'], $feed_type);
            
            $this->StopFeed();
        } else {
            if (isset($FeedSubmissionListResult) && !empty($FeedSubmissionListResult)) {
                return (true);
            } else {
                $this->GetFeedSubmissionList($SubmissionId, $feed_type);
            }
        }
    }

    private function GetFeedSubmissionResult($SubmissionId, $feed_type, $products = null) {

        if (isset($this->proc_rep)) {
            $this->message->$feed_type->message = sprintf(Amazon_Tools::l('Getting_Submission_Result_ID_s_s'), $SubmissionId, '..');
            $this->proc_rep->set_status_msg($this->message);
        }

        $FeedSubmissionResult = $this->amazonApi->getFeedSubmissionResult($SubmissionId);
        
        if (isset($FeedSubmissionResult->Error) && is_object($FeedSubmissionResult->Error)) {
            $this->error = strval($FeedSubmissionResult->Error->Message);
            if (isset($FeedSubmissionResult->Error->Code) && strval($FeedSubmissionResult->Error->Code) == "FeedProcessingResultNotReady") {
                if (isset($this->proc_rep)) {
                    $this->message->$feed_type->error = sprintf(Amazon_Tools::l('s_the_process_will_restart_in_s_minutes'),  $this->error, '5');
                    $this->proc_rep->set_status_msg($this->message);
                }
                sleep(60*5);
                $this->GetFeedSubmissionResult($SubmissionId, $feed_type, $products);
            } else {
                if (isset($this->proc_rep)) {
                    $this->message->error = sprintf(Amazon_Tools::l('Get_submission_Result_Error_s_s'),  $this->error, $feed_type);
                    $this->proc_rep->set_error_msg($this->message);
                }
                
                if(isset($products['product']))
                    $this->UpdateFlag($products['product'], $feed_type);
                
                $this->StopFeed();
            }
        } else {
            unset($this->message->error);

            //Amazon Log Here 
            if (isset($FeedSubmissionResult->Message->ProcessingReport) && !empty($FeedSubmissionResult->Message->ProcessingReport)) {
                $FeedSubmissionResultInfo = array(
                    'FeedSunmissionId' => strval($FeedSubmissionResult->Message->ProcessingReport->DocumentTransactionID),
                    'StatusCode' => strval($FeedSubmissionResult->Message->ProcessingReport->StatusCode),
                    'MessagesProcessed' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesProcessed),
                    'MessagesSuccessful' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesSuccessful),
                    'MessagesWithError' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithError),
                    'MessagesWithWarning' => strval($FeedSubmissionResult->Message->ProcessingReport->ProcessingSummary->MessagesWithWarning)
                );

                if($feed_type == AmazonFeeds::PRODUCT || $feed_type == AmazonFeeds::INVENTORY){
                    $this->no_process = $FeedSubmissionResultInfo['MessagesProcessed'];
                    $this->no_success = $FeedSubmissionResultInfo['MessagesSuccessful'];
                    $this->no_error = $FeedSubmissionResultInfo['MessagesWithError'];
                    $this->no_warning = $FeedSubmissionResultInfo['MessagesWithWarning'];
                }
                
                $log_data = array(
                    'batch_id' => $this->batchID,
                    'id_country' => $this->params->id_country,
                    'id_shop' => $this->params->id_shop,
                    'action_type' => ucwords($this->operation_type),
                    'feed_type' => ucwords($feed_type),
                    'message' => base64_encode(serialize($FeedSubmissionResultInfo)),
                    'date_add' => date("Y-m-d H:i:s"),
                );

                $this->amazonDatabase->save_response_log($log_data);
                $success_message = "Success";

                $this->message->$feed_type->status = 'Success';
                $this->message->$feed_type->message = $success_message;
                $this->message->$feed_type->FeedSubmissionResult = $FeedSubmissionResultInfo;

                //Amazon Log Here 
                $Result = $FlagProduct = $FlagRemoveAttribute = array();
                $no = 0;
                $update_product = '';
                $trigger_update_product = false;
                
                //Error Result
                if (isset($FeedSubmissionResult->Message->ProcessingReport->Result)) {
                    
                    $amazon_error_resolution = new AmazonErrorResolution($this->params->user_name);                    
                    foreach ($FeedSubmissionResult->Message->ProcessingReport->Result as $key => $result) {
                        if (isset($result->ResultCode) /*&& strval($result->ResultCode) == "Error"*/) {
                            $Result[$no]['batch_id'] = $this->batchID;
                            $Result[$no]['id_country'] = $this->params->id_country;
                            $Result[$no]['id_shop'] = $this->params->id_shop;
                            $Result[$no]['sku'] = isset($result->AdditionalInfo->SKU) ? strval($result->AdditionalInfo->SKU) : '';
                            $Result[$no]['id_message'] = strval($result->MessageID);
                            $Result[$no]['id_submission_feed'] = $SubmissionId;
                            $Result[$no]['result_code'] = strval($result->ResultCode);
                            $Result[$no]['result_message_code'] = strval($result->ResultMessageCode);
                            $Result[$no]['result_description'] = strval($result->ResultDescription);
                            $Result[$no]['date_add'] = date("Y-m-d H:i:s");

                            $Flagged = true;

                            if(strval($result->ResultCode) == "Error"){

                                $id_product = isset($products["product"][$Result[$no]['sku']]['id_product']) ?
                                        $products["product"][$Result[$no]['sku']]['id_product'] : null;
                                $id_product_attribute = isset($products["product"][$Result[$no]['sku']]['id_product_attribute']) ?
                                        $products["product"][$Result[$no]['sku']]['id_product_attribute'] : 0;

                                // if error code = 13013 (This SKU does not exist in the Amazon catalog)
                                if(strval($result->ResultMessageCode) == "13013") {

                                    if(isset($id_product)) {
                                        // 1. flag_product_update                                        
                                        $update_product .= $this->amazonDatabase->updateProductFlag(
                                            $this->params->id_shop,
                                            $this->params->id_country,
                                            $id_product,
                                            'update_product',
                                            1,
                                            $id_product_attribute
                                        );

                                        // 2. update action to error ; both sync & create
                                        $update_product.= $this->amazonDatabase->updateMatchProductToSync(
                                                $Result[$no]['sku'],
                                                $this->params->id_country,
                                                $this->params->id_shop,
                                                'error',
                                                $id_product,
                                                $id_product_attribute
                                        );
                                        // 3. trigger update product
                                        // $trigger_update_product = true;
                                        // 4. don't allow other update
                                        //$Flagged = false;
                                    }

                                // if error code = 5000
                                } else if($feed_type == AmazonFeeds::PRODUCT && strval($result->ResultMessageCode) == "5000") {

                                    if(isset($id_product)) {
                                        // 1. flag_product_update
                                        $update_product .= $this->amazonDatabase->updateProductFlag(
                                            $this->params->id_shop,
                                            $this->params->id_country,
                                            $id_product,
                                            'update_product',
                                            1,
                                            $id_product_attribute
                                        );
                                        $update_product .= $amazon_error_resolution->flag_product_update(
                                            $this->params->id_shop,
                                            $this->params->id_country,
                                            $Result[$no]['sku'],
                                            $id_product,
                                            $id_product_attribute,
                                            5000
                                        );
                                        // 2. epdate action to error ; both sync & create
                                        $update_product.= $this->amazonDatabase->updateMatchProductToSync(
                                                $Result[$no]['sku'],
                                                $this->params->id_country,
                                                $this->params->id_shop,
                                                'error',
                                                $id_product,
                                                $id_product_attribute
                                        );
                                        // 3. trigger update product
                                        // $trigger_update_product = true;
                                        // 4. don't allow other update
                                        //$Flagged = false;
                                    }
                                } 
                            }
                            
                            if(isset($products["product"][$Result[$no]['sku']]) && $Flagged) {
                                $FlagProduct[$Result[$no]['sku']] = $products["product"][$Result[$no]['sku']];
                            }

                            if ($feed_type == AmazonFeeds::PRODUCT && isset($products) && !empty($products["product"])) {   
                                if(strval($result->ResultCode) == "Error" && strval($result->ResultMessageCode) != "8026") {
                                    $products["product"][$Result[$no]['sku']] = array();
                                    unset($products["product"][$Result[$no]['sku']]);
                                }
                            }                            
                            $no++;
                        }
                    }
                    
                    // Update Flag on error products
                    if(isset($FlagProduct) && !empty($FlagProduct)) {
                        $this->UpdateFlag($FlagProduct, $feed_type);
                    }

                    // Trigger update product
                    if(!$this->debug && $trigger_update_product){
                        $this->amazonDatabase->update_amazon_status(
                            $this->params->id_country,
                            $this->params->id_shop,
                            $this->time,
                            'feed_'.$feed_type,
                            'update_product'
                        );
                    }

                    /*if(in_array($feed_type, array("Inventory", "inventory")) && $this->params->user_name == 'u00000000000026'){
                        file_put_contents($this->debug_url,
                            print_r(array(
                                'submission_result'  => $Result
                            ), true), FILE_APPEND);
                    }*/

                    $this->amazonDatabase->save_submission_result_log($this->params->id_shop, $this->params->id_country, $Result);
                }

                // flag update product
                if (!$this->debug && isset($update_product) && !empty($update_product)){
                    $this->amazonDatabase->exec_query($update_product);                   
                }

                //delete report file 
                $ext = str_replace(array(".", ".."), "_", ltrim($this->params->ext, "."));
                $this->amazonApi->unlink_file($this->params->user_name, 'open_listings_data_' . $this->params->merchant_id . '_' . $ext . '.raw', 'report');
            } else {
                $this->message->$feed_type->message = Amazon_Tools::l("No_message_from_Amazon.");
                $this->message->$feed_type->FeedSubmissionResult = '';
            }

            if (isset($this->proc_rep)) {
                $this->proc_rep->set_status_msg($this->message);
            }
            
	    // log price report
	    if ($feed_type == AmazonFeeds::PRICE && isset($products) && !empty($products["product"]) ) {
		
		$repricing_action = AmazonDatabase::REPRICING_EXPORT;
		
		if($this->is_from_export_reprice){		    
		    $process_type = AmazonDatabase::REPRICING;		    
		} else {
		    $process_type = $this->operation_type;
		}
		
		$this->amazonDatabase->save_repricing_report($this->params->id_shop, $this->params->id_country, $products["product"], $repricing_action, $process_type);
	    }
	    
            	    
            return $products;
        }

        return(false);
    }
        
    private function UpdateFlag($products, $feed_type){

        $ImportLog = $ImportLog_exec = $update_product = $update_product_exec = '';
        
        if($this->operation_type == AmazonDatabase::SYNC /*&& $this->cron*/) {
            if (isset($products) && !empty($products) && sizeof($products) > 0) {
                foreach ($products as $key => $value) {
                    $id_marketplace = isset($this->params->id_marketplace) ? $this->params->id_marketplace : _ID_MARKETPLACE_AMZ_;
                    $ImportLog = ImportLog::update_flag_offers($id_marketplace, $this->params->ext, $this->params->id_shop, $value['id_product'], 1);
                }
                if (isset($ImportLog) && !empty($ImportLog))
                    $ImportLog_exec = $this->amazonDatabase->db->offer->db_exec($ImportLog);
            }
        }

	if(isset($products) && $feed_type == AmazonFeeds::PRODUCT) { // set status only product feed
            foreach ($products as $key => $result) {
                if (isset($result['SKU']) && !empty($result['SKU'])) {
                    $action = 'error';
                    if(isset($this->operation_type) && $this->operation_type == AmazonDatabase::CREATE){
                        $action = 'delete';
                    }
                    $id_product = (isset($result['id_product']) && !empty($result['id_product'])) ? $result['id_product'] : null;
                    $id_product_attribute = (isset($result['id_product_attribute']) && !empty($result['id_product_attribute'])) ? $result['id_product_attribute'] : null;
                    $update_product .= $this->amazonDatabase->updateMatchProductToSync(
                        $result['SKU'], $this->params->id_country, $this->params->id_shop, $action, $id_product ,$id_product_attribute);
                }
            }
        }
        
        if (!$this->debug && strlen($update_product)) {
            $update_product_exec = $this->amazonDatabase->exec_query($update_product);
        } else {
            echo "<br/><b>UpdateFlag Query : </b><pre>".print_r($update_product, true)."</pre>" ;
        }

        /*if(in_array($feed_type, array("Inventory", "inventory")) && $this->params->user_name == 'u00000000000026'){
            file_put_contents($this->debug_url,
                print_r(array(
                    'UpdateFlag'  => array(
                        'products' => $products,
                        'ImportLog' => $ImportLog,
                        'ImportLog_exec' => $ImportLog_exec,
                        'update_product' => $update_product,
                        'update_product_exec' => $update_product_exec,
                    )
                ), true), FILE_APPEND);
        }*/

    }

    private function ValidationError($pxml, $feed_type) {
        
        $history = array();
        $update_product = '';

        foreach ($pxml->Message as $message) {
            
            $ListSKU = array();
            $ID = strval($message->Error['ID']);

            if($feed_type == AmazonFeeds::PRODUCT) { // set status only product feed
                if(isset($message->Error->SKU) && !empty($message->Error->SKU)){
                    foreach ($message->Error->SKU as $SKU){
                        if(!isset($history[strval($SKU)])){
                            $action = 'error';
                            if(isset($this->operation_type) && $this->operation_type == AmazonDatabase::CREATE){
                                $action = 'delete';
                            }
                            $update_product.= $this->amazonDatabase->updateMatchProductToSync(strval($SKU), $this->params->id_country, $this->params->id_shop, $action);
                        }
                        $history[strval($SKU)] = true;
                        array_push($ListSKU, strval($SKU));
                    }
                }
            }
            
            if(isset($message->Error->ErrorMessage) && !empty($message->Error->ErrorMessage)){
                
                $ErrorMessage = Amazon_Tools::spacify_str(strval($message->Universe)) . ', ' . strval($message->Error->ErrorMessage);
                if(!empty($ListSKU)) {$ErrorMessage .= ' [SKU : ' . implode(', ', $ListSKU) . ']'; }
                
                $this->error_data = array(
                    'batch_id' => $this->batchID,
                    'id_country' => $this->params->id_country,
                    'id_shop' => $this->params->id_shop,
                    'action_process' => $this->operation_type,
                    'action_type' => $feed_type,
                    'message' => $ErrorMessage,
                    'id_message' => $ID,
                    'date_add' => date("Y-m-d H:i:s"),
                );
            }
            
            if(isset($this->error_data))
                $this->amazonDatabase->save_validation_log($this->error_data/* , $this->params->id_shop, $this->params->id_country */);

        }
        
        if (!$this->debug && isset($update_product) && !empty($update_product)) {
            $this->amazonDatabase->exec_query($update_product);
        } else {
            echo "<br/><b>ValidationError Query : </b><pre>".print_r($update_product, true)."</pre>" ;
        }
        
        $this->error = Amazon_Tools::l('Validation_Error');
        $this->message->$feed_type->error = $this->error;
        $this->message->country = $this->params->id_country;
        $this->message->all_message = Amazon_Tools::l('See_more_error_in_Logs');
        
        if (isset($this->proc_rep))
            $this->proc_rep->set_status_msg($this->message);

        if ($this->debug == false) {
            $this->SendEmailResultValue(true);
        }

        return false;
    }

    private function AmazonDatabase() {
        
        if (!$this->amazonDatabase = new AmazonDatabase($this->params->user_name, $this->debug)) {
            $this->error = Amazon_Tools::l('Unable_to_login.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep))
                $this->proc_rep->set_error_msg($this->message);

            $this->StopFeed();
        }
        return $this->amazonDatabase;
    }

    private function AmazonApi() {
        
        $auth = array(
            'MerchantID' => trim($this->params->merchant_id),
            'AWSAccessKeyID' => trim($this->params->aws_id),
            'SecretKey' => trim($this->params->secret_key),
            'lang' => ltrim($this->params->ext, "."),
        );
        if(isset($this->params->auth_token) && !empty($this->params->auth_token)){
            $auth['auth_token'] = trim($this->params->auth_token);
        }        
        $marketPlace = array(
            'Currency' => $this->params->currency,
            'Country' => ltrim($this->params->ext, "."),
        );
        if (!$this->amazonApi = new Amazon_WebService($auth, $marketPlace, null, false)) {
            $this->error = Amazon_Tools::l('Unable_to_connect_Amazon.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep))
                $this->proc_rep->set_error_msg($this->message);

            $this->StopFeed();
        }
        return $this->amazonApi;
    }

    private function NoneProducts($feed_type) {
        
        if (isset($this->proc_rep)) {
            $FeedSubmissionResultInfo = array('MessagesProcessed' => 0);
            $feed_message =  Amazon_Tools::l("Nothing_to_send");

            if (isset($this->params->mode) && $this->params->mode == AmazonDatabase::SYNC)
                $feed_message = sprintf("%s %s, %s", AmazonDatabase::SYNCHRONIZE, Amazon_Tools::l("completely"), Amazon_Tools::l("See_result_in_Report_tab"));

            $this->message->$feed_type->status = 'Success';
            $this->message->$feed_type->no_product = $feed_message;
            $this->message->$feed_type->FeedSubmissionResult = $FeedSubmissionResultInfo;

            $this->proc_rep->set_status_msg($this->message);
        }
    }

    private function StartProcessMessage() {
        
        $process_running = ($this->operation_type == AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE :
                          (($this->operation_type == AmazonDatabase::CREATE) ? AmazonDatabase::CREATION : $this->operation_type . ' ' . Amazon_Tools::l('products'));

        $process_title = sprintf("%s %s, %s ..", ucwords(AmazonFeeds::Amazon), strtolower($process_running), Amazon_Tools::l('processing'));
        $process_type = $this->operation_type . (isset($this->params->update_type) ?  '_' . $this->params->update_type : '' ) . '_' . $this->params->countries;

        $marketplace = AmazonFeeds::Amazon;
        
        $popup = false;
        
        if (isset($this->params->mode) && $this->params->mode == AmazonDatabase::SYNC) {
            $popup = true;
        }
        if($this->operation_type == AmazonDatabase::DELETE || $this->operation_type == AmazonDatabase::REPRICING) {
            $popup = true;
        }
        if($this->operation_type == AmazonDatabase::REPRICING && isset($this->params->repricing_type)) {
            $process_type .= '_'.$this->params->repricing_type;
        }
        if($this->cron) {
            $popup = true;
        }            
        if($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_relation_ship) && $this->option->send_relation_ship){
            $process_type = 'relations_' . $this->params->countries;
            $popup = true;
        }
        if($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_only_image) && $this->option->send_only_image){
            $process_type = 'image_' . $this->params->countries;
            $popup = true;
        }
        if($this->operation_type == AmazonDatabase::CREATE && isset($this->option->send_shipping_override) && $this->option->send_only_image){
            $process_type = 'shipping_override_' . $this->params->countries;
            $popup = true;
        }
        if ($popup) {
            $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, true);
            $this->proc_rep->set_process_type($process_type);
            $this->proc_rep->set_popup_display(true);
            $this->proc_rep->set_marketplace($marketplace);
        } else {
            $this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, false);
            $this->proc_rep->set_process_type($process_type);
            $this->proc_rep->set_popup_display(false);
            $this->proc_rep->set_marketplace($marketplace);
        }

        $process = $this->proc_rep->has_other_running($process_type);
        if ($process) {
            $this->message->error =sprintf(Amazon_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'),$process_running,'20');
            $this->proc_rep->set_error_msg($this->message);

            $this->proc_rep->finish_task();
            echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, ' . $this->operation_type . ' until old process done'));
            die();            
        }
        
        if (isset($this->proc_rep) && ($this->operation_type == AmazonDatabase::REPRICING)) {

            if(!isset($this->message->product))
                $this->message->product = new stdClass();

            $this->message->batchID = $this->batchID;
            $this->message->type = $this->message_type;
            $this->message->title = $this->title;
            $this->message->product->message = Amazon_Tools::l('Retrieve_Messages') . "..";
            $this->proc_rep->set_status_msg($this->message);
        }
    }

    public function StopFeed() {

        //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/AmazonFeeds.txt',
        //            print_r(array('StopFeed'=>true), true), FILE_APPEND);
        
        $error = null;
        if (isset($this->error) && !empty($this->error)){
            $error = $this->error;
            $this->log(array('error' => $error, 'date_add' => date('Y-m-d H:i:s')));
        } else {
            $this->log(array('date_add' => date('Y-m-d H:i:s')));    
        }
        
        if($this->operation_type != AmazonDatabase::REPRICING) { // If repricing we need to delete message before exit; see functions/amazon.repricing.automaton.php 
            $json = json_encode(array('status' => strlen($error) ? 'error' : 'success', 'message' => $error ? $error : $this->output));
            echo $json;
        }

        if (isset($this->proc_rep))
            $this->proc_rep->finish_task();
        
        if($this->operation_type == AmazonDatabase::REPRICING){ // If repricing we need to delete message before exit; see functions/amazon.repricing.automaton.php
            return true;
        } else {
            exit;
        }
    }
    
    private function log($datail = null){
        
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => $this->operation_type,
            "no_send"       => isset($this->productIndex) ? (int)$this->productIndex : 0,
            "no_skipped"    => isset($this->skipped) ? (int)$this->skipped : 0, 
            "no_process"    => isset($this->no_process) ? (int)$this->no_process : 0,
            "no_success"    => isset($this->no_success) ? (int)$this->no_success : 0,
            "no_error"      => isset($this->no_error) ? (int)$this->no_error : 0,
            "no_warning"    => isset($this->no_warning) ? (int)$this->no_warning : 0,                
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
        if(isset($datail) && !empty($datail))
             $log['detail'] = base64_encode(serialize($datail));

        //file_put_contents(dirname(__FILE__) . '/../../../../assets/apps/users/'.$this->params->user_name.'/amazon/feed_log.txt',
        //    print_r(array('Time'=>date('Y-m-d H:i:s'),'operation_type'=>$this->operation_type,'log'=>$log), true), FILE_APPEND);
        
        if($this->debug == false)
            $this->amazonDatabase->update_log($log);
    }
            
    private function SendEmailResultValue($only_validation_log=false) {
        
        $site_title = _SITE_TITLE_;
        if (function_exists('base_url')) {
            $base_url = base_url();
        } else {
            $base_url = _BASE_URL_;
        }

        $user = $this->user;
        $email_from = _ADMIN_EMAIL_;
        $email_to = $user->email;

        $process_running = ($this->operation_type == AmazonDatabase::SYNC) ? AmazonDatabase::SYNCHRONIZE : (($this->operation_type == AmazonDatabase::CREATE) ? AmazonDatabase::CREATION : $this->operation_type);

        $feed = array();
        $feed['skipped'] = $this->skipped;
        $feed['productIndex'] = $this->productIndex;
        $feed['action'] = $this->process;
        if (isset($this->skipped) && $this->skipped > 0){
            $skipped_number = 1;
            $skipped_data = $this->amazonDatabase->get_validation_log($this->params->id_country, $this->params->id_shop, $this->operation_type, 'warning', $this->batchID, null, null, false, array(), false);

            if (isset($skipped_data) && !empty($skipped_data)) {
                foreach ($skipped_data as $k => $d) {
                    if($skipped_number <= 5){
                        $feed['log_skipped'][$k]['message'] = $d['message'];
                        $feed['log_skipped'][$k]['date_add'] = $d['date_add']; 
                    }
                    $skipped_number++;
                }
                if($skipped_number > 5) {
                    $feed['log_skipped']['view_more'] = htmlspecialchars($base_url.'amazon/logs/'.$this->params->id_country.'/0/'.$this->operation_type.'/'.'warning'.'/'.$this->batchID); 
                }
            }   
        }
        
        if (isset($this->error) && !empty($this->error)) {
            $validation_number = 0;
            $feed['error'] = $this->error;        
            $data = $this->amazonDatabase->get_validation_log($this->params->id_country, $this->params->id_shop, $this->operation_type, 'product', $this->batchID, null, null, false, array(), true);

            if (isset($data) && !empty($data) && $validation_number <=4) {
                foreach ($data as $k => $d) {
                    $validation_number++;
                    $feed['log_data'][$k]['message'] = $d['message'];
                    $feed['log_data'][$k]['date_add'] = $d['date_add'];                    
                }
                if($validation_number >= 5) {
                    $feed['log_data']['view_more'] = htmlspecialchars($base_url.'amazon/logs/'.$this->params->id_country.'/0/'.$this->operation_type.'/'.'error'.'/'.$this->batchID); 
                }
            }                 
        } else {
            $data = $this->amazonDatabase->get_submission_list_log($this->params->id_country, $this->params->id_shop, $this->batchID);            
            if (isset($data) && !empty($data)) {
                foreach ($data as $d) {
                    $feed['action_type'] = $d['action_type'];
                    $feed['data'][$d['feed_type']]  = $d;
                    if ($d['messages_with_error'] > 0) {
                        $feed['data'][$d['feed_type']]['link'] = $base_url.'/'.strtolower(AmazonFeeds::Amazon).'/downloadLog/'.$this->params->id_country.'/'.$this->params->id_shop.'/'.$this->batchID.'/'.$d['feed_sunmission_id'];
                    }
                }
            } else {
                $feed['error'] = sprintf(Amazon_Tools::l('Feed_submission_error'));
            }
        }

        require_once(dirname(__FILE__).'/../../Smarty/Smarty.class.php');
        require_once(dirname(__FILE__).'/../../Smarty.php');

        $smarty = new Smarty();
        $smarty->compile_dir = dirname(__FILE__)."/../../../views/templates_c";
        $smarty->template_dir = dirname(__FILE__)."/../../../views/templates";
        
        $base_url = _BASE_URL_;
        $style_img = 'border: 0 !important; display: block !important; outline: none !important; padding: 5px 5px;';
        $data = array(
            'base_url' => $base_url,
            'logo' => '<img src="'.$base_url.'/nblk/images/feedbiz_logob.png" alt="'.$site_title.'" style="'.$style_img.'" />',
            'title' => Amazon_Tools::l('Dear').' '.$user->firstname.' '.$user->lastname,
            'head1' => $this->message_type . ': ' . $this->time,
            'feed' => $feed,
            'thanks' => Amazon_Tools::l('Thanks'),
            'footer1' => Amazon_Tools::l('This e-mail was sent to'),
            'footer2' => Amazon_Tools::l('Please note that you must use an email to access to Feed.biz management system.'),
            'footer3' => Amazon_Tools::l('All right reserved.'),
            'marketplace' => Amazon_Tools::l('Marketplace'),
            'l_Statistics' => Amazon_Tools::l('Statistics'),
            'l_Items_to' => Amazon_Tools::l('Items to'),
            'l_with_Amazon' => Amazon_Tools::l('with Amazon'),
            'l_Skipped_items' => Amazon_Tools::l('Skipped items'),
            'l_View_more_on_web_site' => Amazon_Tools::l('View more on web site'),
            'l_Result' => Amazon_Tools::l('Result'),
            'l_Feed' => Amazon_Tools::l('Feed'),
            'l_Feed_Submission_Id' => Amazon_Tools::l('Feed Submission Id'),
            'l_Feed_Submission_date' => Amazon_Tools::l('Feed Submission date'),
            'l_Messages_Processed' => Amazon_Tools::l('Messages Processed'),
            'l_Messages_Successful' => Amazon_Tools::l('Messages Successful'),
            'l_Messages_With_Warning' => Amazon_Tools::l('Messages With Warning'),
            'l_Messages_With_Error' => Amazon_Tools::l('Messages With Error'),
            'l_Download_Error_Message' => Amazon_Tools::l('Download Error Message'),
        );
        
        $smarty->assign($data);

        if($only_validation_log){
            $message = $smarty->fetch(dirname(__FILE__).'/../../../views/templates/email_template/amazon_validation_error.tpl');
            $subject = $site_title . ' - ' . ucwords(AmazonFeeds::Amazon) . ' Validation Error.';
        } else {
            $message = $smarty->fetch(dirname(__FILE__).'/../../../views/templates/email_template/amazon_feedsubmissionresult.tpl');
            $subject = $site_title . ' - ' . ucwords(AmazonFeeds::Amazon) . ' ' . $process_running . ' result.';
        }

        $mail = Amazon_Tools::send_mail($subject, $email_from, $site_title, $email_to, $message, 'text/html', array('praew@common-services.com'));

        if(isset($mail['pass']) && !$mail['pass'] && isset($mail['output'])){
            if (isset($this->proc_rep)) {
                $this->message->error = $mail['output'];
                $this->proc_rep->set_status_msg($this->message);
            }
            $this->StopFeed();
        }       
    }
    
    public function checkStatusFeatures(){
            
        $features = $this->amazonDatabase->get_configuration_features($this->params->id_country, $this->params->id_shop);

        if($this->operation_type == AmazonDatabase::CREATE && (!isset($features['creation']) || empty($features['creation']) || $features['creation'] == 0)){
                $this->error = Amazon_Tools::l('inactivate_create_products');
                $this->message->error = $this->error; 

                if (isset($this->proc_rep))
                    $this->proc_rep->set_error_msg($this->message);

                $this->StopFeed(); 
        }
        if($this->operation_type == AmazonDatabase::DELETE && (!isset($features['delete_products']) || empty($features['delete_products']) || $features['delete_products'] == 0)){
                $this->error = Amazon_Tools::l('inactivate_delete_products');
                $this->message->error = $this->error; 

                if (isset($this->proc_rep))
                    $this->proc_rep->set_error_msg($this->message);

                $this->StopFeed(); 
        }
        if($this->operation_type == AmazonDatabase::REPRICING && (!isset($features['repricing']) || empty($features['repricing']) || $features['repricing'] == 0)){
                $this->error = Amazon_Tools::l('inactivate_repricing');
                $this->message->error = $this->error; 

                if (isset($this->proc_rep))
                    $this->proc_rep->set_error_msg($this->message);

                $this->StopFeed(); 
        }               
        if(isset($features['repricing']) && $features['repricing'] == 1){
                $this->repricing = true;
        }
        if(isset($features['shipping_override']) && $features['shipping_override'] == 1){
                $this->shipping_override = true;
        }
    }
}