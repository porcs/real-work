<?php

// cron every day
require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.webservice.php';
require_once(dirname(__FILE__) . '/../classes/amazon.multichannel.php');
require_once(dirname(__FILE__) . '/../../FeedBiz/config/stock.php');
require_once(dirname(__FILE__) . '/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

if(!defined("_MARKETPLACE_NAME_"))define('_MARKETPLACE_NAME_','amazon');

class AmazonFBAOrder
{
    const PROCESS = 'FBA Order';
    const PROCESS_KEY = 'o';
    
    const ORDER_STATE_STANDARD = 'STD';
    const ORDER_STATE_PREORDER = 'PRE';

    const AFN = 'AFN';
    const MFN = 'MFN';
    const MAFN = 'MAFN';
    
    const fba_create = 1;
    const fba_info = 2;
    const fba_cancel = 3;
    const fba_status = 4;
    const fba_update = 5;
    
    const FBAOrder = 'fba_order';
    
    public static $tag = _MARKETPLACE_NAME_;
    public static $fba_action_type = array( 
		    self::fba_create =>'create', 
		    self::fba_info=>'info', 
		    self::fba_cancel=>'cancel',
		    self::fba_status=>'update status',
		    self::fba_update=>'update'
		);
    public static $warnings = array();
    public static $log      = array();
    
    public $exts	    = array();

    public function __construct($params, $cron=false, $debug=false)
    {
	$this->params = $params;	
	$this->debug = $debug;
	$this->cron = $cron;	
	
        Amazon_Tools::load(_FEED_LANG_FILE_, $this->params->language);
    }
    
    public function Dispatch($action, $values=null, $options=null)
    {
        // 1. set parameter
	$this->batchID = uniqid();
	$this->process = self::PROCESS;	
        $this->time = $this->params->time = date('Y-m-d H:i:s');
		
        $this->operation_type = isset(self::$fba_action_type[$action]) ? self::$fba_action_type[$action] : '' ;	
	$this->process_type = $action;
	
	$this->message = new stdClass();
	$this->message->batchID = $this->batchID;
	$this->message->type = sprintf(Amazon_Tools::l('AmazonS_s_started_on_s'), $this->params->ext, $this->process . ' ' . $this->operation_type, '');
	$this->message->title = $this->process;
	$this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
	
	// 2. start popup process
        if (!$this->debug && !(isset($options['return_html']) && $options['return_html'] == 1) ) {	   
            if(!$this->StartProcessMessage())
		return (false);
        }

	// 3. start amazon database
        if(!$this->AmazonDatabase())
	    return (false);
	
	// 5. check feature status
	if(!$this->checkFeatureStatus())
	    return (false);
	
	$exit = true;
	
	// 6 . action type
        switch ($action) {
            case self::fba_create :
                $this->CreateFulfillmentOrders($values, $options);
                break;
            case self::fba_info :		
                $result = $this->GetFulfillmentOrder($values, isset($options['return_html']) && $options['return_html'] == 1 ? true : false);
		if(isset($options['return_html']) && $options['return_html'] == 1)
		    return $result;
                break;
            case self::fba_cancel :
                $this->CancelFulfillmentOrder($values);		
                break;
            case self::fba_status :
                $this->FulfillmentOrderStatuses();
                break;
	    case self::fba_update :
                $this->UpdateFulfillmentOrders($values);
		$exit = false;
                break;
        }
	
        // 7. stop popup process
	$this->StopFeed($exit);
    }
    
    public function UpdateFulfillmentOrders($id_orders)
    {       
        if(!$this->Init()){
	    return (false);
	}		
	
	$this->no_process = $this->no_success = $this->skipped = 0 ;
		
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->{AmazonFBAOrder::FBAOrder}))
		$this->message->{AmazonFBAOrder::FBAOrder} = new stdClass ();
	    
	    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Start update Fulfillment Order') . "...");
	    $this->proc_rep->set_status_msg($this->message);
	}
	
	// 1. get order to Update fba order
	$orders = $this->amazonMultiChannel->getOrdersToUpdateFBA($this->params->id_shop, $id_orders, AmazonMultiChannel::AMAZON_FBA_MULTICHANNEL);
		
	if($this->debug){
	    echo '<br/> AmazonFBAOrder -> UpdateFulfillmentOrders() <br/>  <b>Orders - :</b> <pre>' . print_r($orders, true);
	}
	
	if ( !$orders || !count($orders) || empty($orders) ) {
	    
           if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = Amazon_Tools::l("No Fulfillment Orders to update to Amazon");
		$this->proc_rep->set_status_msg($this->message);
	    }
            return (true);
	    
        } else {
	    
	    $RequestId = array();
	    $list_order = '';
	    
	    // 3. CreateFulfillmentOrder
	    foreach ($orders as $order) 
	    {	    
		$list_order .= $order['seller_order_id'] . ', ';
		if(isset($this->proc_rep)) {
		    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Update Fulfillment Order %s'), $list_order . ".." ) ;
		    $this->proc_rep->set_status_msg($this->message);
		}

		$this->no_process++;
		if(!$AmazonOrder = $this->amazonMultiChannel->UpdateFulfillmentOrder($order)){
		    $this->skipped++;
		} else {
		    $RequestId[] = $AmazonOrder;
		    $this->no_success++;	
		}
	    }		
	
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Update fulfillment order success, RequestId: %s'), implode(", ", $RequestId));
		$this->proc_rep->set_status_msg($this->message);
	    }
	    
	    if(isset(AmazonMultiChannel::$errors) && !empty(AmazonMultiChannel::$errors)){
		AmazonFBAOrder::$warnings = AmazonMultiChannel::$errors;
	    }
	}
	
	// 4. save validate log
	$this->ValidationError();
    }
    
    public function CreateFulfillmentOrders($id_order=null, $options=null)
    {     
	// Only if FBA MultiChannel is active
	if (!isset($this->params->fba_multichannel) || !(bool)$this->params->fba_multichannel)
	{
	    $this->message->error = Amazon_Tools::l('fba multichannel is not allow.');
	    
	    if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
	    
	    return (false);
	}
	
	// If not force send and Only if FBA MultiChannel auto is active
	if ( (!isset($options['force_send']) || !$options['force_send']) && (!isset($this->params->fba_multichannel_auto) || !(bool)$this->params->fba_multichannel_auto))
	{
	    $this->message->error = Amazon_Tools::l('fba multichannel auto is not allow.');
	    $this->message->link = array('my_shop','multichannel_orders');
	    $this->message->link_label = Amazon_Tools::l('view orders');
	    
	    if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
	    
	    return (false);
	}    
    
        if(!$this->Init()){
	    return (false);
	}		
	
	$this->no_process = $this->no_success = $this->skipped = 0 ;
		
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->{AmazonFBAOrder::FBAOrder}))
		$this->message->{AmazonFBAOrder::FBAOrder} = new stdClass ();
	    
	    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Start create Fulfillment Order %s'),isset($id_order)?', Order id : '.$id_order:'')."..";
	    $this->proc_rep->set_status_msg($this->message);
	}
	
	// 1. get order to create fba order
	$orders = $this->amazonMultiChannel->getOrdersToCreateFBA($this->params->id_shop, $id_order);
        
	if($this->debug){
	    echo '<br/> AmazonFBAOrder -> CreateFulfillmentOrders() <br/>  <b>Orders - :</b> <pre>' . print_r($orders, true);
	}
	
	// 2. check available Products 	
        if (!($order = $this->amazonMultiChannel->isEligible($orders, $this->params->id_shop, $this->params->id_country, $this->params->id_marketplace))) {
	    
            if ($this->debug) {
                printf('CreateFulfillmentOrder() is not eligible'."<br/>\n");
            }

	    if (isset($id_order) && isset($orders[$id_order]['seller_order_id'])) { // if isset seller order id 
                $error_msg = printf(Amazon_Tools::l('Create Fulfillment Order, order id: %d (seller order id : %d), is not eligible'),
                                                $id_order, $orders[$id_order]['seller_order_id']);
            } else if (isset($id_order)) {
                $error_msg = sprintf(Amazon_Tools::l('Create Fulfillment Order, order id: %d, is not eligible'), $id_order);
            } else {
                $error_msg = sprintf(Amazon_Tools::l('Create Fulfillment Order is not eligible'));
            }

            AmazonFBAOrder::$warnings[] = $error_msg; // log

            if(isset($this->proc_rep)) {
                $this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
                $this->message->{AmazonFBAOrder::FBAOrder}->error = $error_msg;
                $this->message->{AmazonFBAOrder::FBAOrder}->link = array('amazon','logs',$this->params->id_country);
                $this->message->{AmazonFBAOrder::FBAOrder}->link_label = Amazon_Tools::l('view error message');
                $this->proc_rep->set_status_msg($this->message);
            }
	    
        } else {
	
	    // check Not already ordered, shipped or canceled
	    if(isset($id_order)) {

		$this->amazonMultiChannel->getMpStatus($id_order);

		if (strlen($this->amazonMultiChannel->marketPlaceChannelStatus)) {
		    if ($this->debug) {
			printf('CreateFulfillmentOrder() has already a FBA/Multichannel state : '. $id_order ."<br/>\n");
		    }
		    AmazonFBAOrder::$warnings[] = sprintf(Amazon_Tools::l('Create Fulfillment Order, has already a FBA/Multichannel state : '), $id_order);
		    return (false);
		}

	    } else {
		foreach ($orders as $order){

		    $this->amazonMultiChannel->getMpStatus($order['id_orders']);

		    if (strlen($this->amazonMultiChannel->marketPlaceChannelStatus)) {
			if ($this->debug) {
			    printf('CreateFulfillmentOrder() has already a FBA/Multichannel state: '. $order['id_orders'] ."<br/>\n");
			}
			AmazonFBAOrder::$warnings[] = sprintf(Amazon_Tools::l('Create Fulfillment Order, has already a FBA/Multichannel state : '), $order['id_orders']);
			return (false);
		    }
		}
	    }
	    
	    $RequestId = array();
	    
	    // 3. CreateFulfillmentOrder
	    foreach ($order as $o)
	    {	 
		if(isset($this->proc_rep)) {
		    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Create Fulfillment Order %s'),
			    (isset($order['seller_order_id']) ? $o['seller_order_id'] : $o['id_orders']) .".." ) ;
		    $this->proc_rep->set_status_msg($this->message);
		}
		
		$this->no_process++;
		if(!$AmazonOrder = $this->amazonMultiChannel->CreateFulfillmentOrder($o, $this->params)){
		    $this->skipped++;
		} else {
		    $RequestId[] = $AmazonOrder['Response'];
		    $this->no_success++;	
		}
	    }		
	    
	    if(!empty($RequestId)) {
		if(isset($this->proc_rep)) {
		    $this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Create fulfillment order success, RequestId: %s'), implode(", ", $RequestId));
		    $this->proc_rep->set_status_msg($this->message);
		}
	    } else {
		if(isset($this->proc_rep)) {
		    $this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		    $this->message->{AmazonFBAOrder::FBAOrder}->error = sprintf(Amazon_Tools::l('Create fulfillment order unsuccess.'));
		    $this->message->{AmazonFBAOrder::FBAOrder}->link = array('amazon','logs',$this->params->id_country);
		    $this->message->{AmazonFBAOrder::FBAOrder}->link_label = Amazon_Tools::l('view error message');
		    $this->proc_rep->set_status_msg($this->message);
		}
	    }
	    
	    if(isset(AmazonMultiChannel::$errors) && !empty(AmazonMultiChannel::$errors)){
		AmazonFBAOrder::$warnings = AmazonMultiChannel::$errors;
	    }

	}
	
	// 4. save validate log
	$this->ValidationError();
	
    }
   
    public function GetFulfillmentOrder($id_order, $return_html=false)
    {
	
	if(!$this->Init()){
	    return (false);
	}		

	$this->no_process = $this->no_success = $this->skipped = 0 ;
		
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->{AmazonFBAOrder::FBAOrder}))
		$this->message->{AmazonFBAOrder::FBAOrder} = new stdClass ();
	    
	    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Start Get Fulfillment Order %s'),isset($id_order)?', Order id : '.$id_order:'')."..";
	    $this->proc_rep->set_status_msg($this->message);
	}
	
	if (!isset($id_order) || empty($id_order)) {
	
	    AmazonFBAOrder::$warnings[] = Amazon_Tools::l('Missing mandatory parameter, id_order'); // log
	    $this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
	    $this->message->{AmazonFBAOrder::FBAOrder}->error = Amazon_Tools::l('Missing mandatory parameter, id_order');
	    $this->message->{AmazonFBAOrder::FBAOrder}->link = array('amazon','logs',$this->params->id_country);
	    $this->message->{AmazonFBAOrder::FBAOrder}->link_label = Amazon_Tools::l('view error message');
	    $this->proc_rep->set_status_msg($this->message);
	    
        } else {
	    	    
	    if (!$result = $this->amazonMultiChannel->GetFulfillmentOrder($id_order)) {
		if ($this->debug) {
		    printf('GetFulfillmentOrder() failed and returns: %s'."<br/>\n", nl2br(print_r($result)));
		}
	    } 
	    /*if (isset($result->Error)) {

		$errorMessage .= Amazon_Tools::l('Error from Amazon:');

		if (isset($result->Error->Type)) {
		    $errorMessage .= sprintf(Amazon_Tools::l('Type') .': %s<br />', $result->Error->Type);
		}
		if (isset($result->Error->Code)) {
		    $errorMessage .= sprintf(Amazon_Tools::l('Code') .': %s<br />', $result->Error->Code);
		}
		if (isset($result->Error->Message)) {
		    $errorMessage .= sprintf(Amazon_Tools::l('Message') .': %s<br />', $result->Error->Message);
		}

		AmazonFBAOrder::$warnings[] = $errorMessage;

	    }   */		

	    if(isset(AmazonMultiChannel::$errors) && !empty(AmazonMultiChannel::$errors)){
		AmazonFBAOrder::$warnings = AmazonMultiChannel::$errors;		
		$result['error'] = AmazonFBAOrder::$warnings;
	    }
	    
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Get fulfillment order success'));
		$this->proc_rep->set_status_msg($this->message);
	    }

	}
	
	if($return_html) 
	{
	    if(isset($result)){
		return $result;
	    } else {
		return array('error' => sprintf(Amazon_Tools::l('GetFulfillmentOrder() failed and returns: %s'), isset($id_order)?', Order id : '.$id_order:''));
	    }
	}
	
	// 4. save validate log
	$this->ValidationError(); 
	
    }

    public function CancelFulfillmentOrder($id_order,  $return_html=false)
    {
        if(!$this->Init()){
	    return (false);
	}		
	
	$this->no_process = $this->no_success = $this->skipped = 0 ;
		
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->{AmazonFBAOrder::FBAOrder}))
		$this->message->{AmazonFBAOrder::FBAOrder} = new stdClass ();
	    
	    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Start Cancel Fulfillment Order %s'),isset($id_order)?', Order id : '.$id_order:'')."..";
	    $this->proc_rep->set_status_msg($this->message);
	}
	
	if (!isset($id_order) || empty($id_order)) {
	
	    AmazonFBAOrder::$warnings[] = Amazon_Tools::l('Missing mandatory parameter, id_order'); // log
	    $this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
	    $this->message->{AmazonFBAOrder::FBAOrder}->error = Amazon_Tools::l('Missing mandatory parameter, id_order');
	    $this->message->{AmazonFBAOrder::FBAOrder}->link = array('amazon','logs',$this->params->id_country);
	    $this->message->{AmazonFBAOrder::FBAOrder}->link_label = Amazon_Tools::l('view error message');
	    $this->proc_rep->set_status_msg($this->message);
	    
        } else {

	    if (!($result = $this->amazonMultiChannel->CancelFulfillmentOrder($id_order))) {
		if ($this->debug) {
		    printf('GetFulfillmentOrder() failed and returns: %s'."<br/>\n", nl2br(print_r($result)));
		}		
	    } else {
		if (isset($result->Error)) {
		    
		    $errorMessage = Amazon_Tools::l('Error from Amazon:');

		    if (isset($result->Error->Type)) {
			$errorMessage .= sprintf(Amazon_Tools::l('Type') .': %s<br />', $result->Error->Type);
		    }
		    if (isset($result->Error->Code)) {
			$errorMessage .= sprintf(Amazon_Tools::l('Code') .': %s<br />', $result->Error->Code);
		    }
		    if (isset($result->Error->Message)) {
			$errorMessage .= sprintf(Amazon_Tools::l('Message') .': %s<br />', $result->Error->Message);
		    }
		    
		    AmazonFBAOrder::$warnings[] = $errorMessage;
		    $result['error'] = $errorMessage;
		} 
	    }

	    if(isset(AmazonMultiChannel::$errors) && !empty(AmazonMultiChannel::$errors)){
		AmazonFBAOrder::$warnings = AmazonMultiChannel::$errors;
	    }
	    
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Cancel fulfillment order success'));
		$this->proc_rep->set_status_msg($this->message);
	    }

	}
	
	if($return_html) 
	{
	    if(isset($result)){
		return $result;
	    } else {
		return array('error' => sprintf(Amazon_Tools::l('GetFulfillmentOrder() failed and returns: %s'), isset($id_order)?', Order id : '.$id_order:''));
	    }
	}
	
	// 4. save validate log
	$this->ValidationError();
	
    }

    public function FulfillmentOrderStatuses()
    {       
        if(!$this->Init()){
	    return (false);
	}
	
	$this->no_process = $this->no_success = $this->skipped =0;
		
	if(isset($this->proc_rep)) {
	    if(!isset($this->message->{AmazonFBAOrder::FBAOrder}))
		$this->message->{AmazonFBAOrder::FBAOrder} = new stdClass ();
	    
	    $this->message->{AmazonFBAOrder::FBAOrder}->message = sprintf(Amazon_Tools::l('Listting_S_S'), $this->process, $this->operation_type) . "..";
	    $this->proc_rep->set_status_msg($this->message);
	}
	
        $pending_state = Amazon_Order::UNSHIPPED; //(int)Configuration::get('AMAZON_FBA_MULTICHANNEL_STATE');
        $sent_state = Amazon_Order::SHIPPED; //(int)Configuration::get('AMAZON_FBA_MULTICHANNEL_SENT');      
	
        $result = $this->amazonMultiChannel->ordersByStatus(array($pending_state/*, $sent_state*/));
	
	if ($this->debug) {
	    printf('ordersByStatus() returns: %s'."<br/>\n", nl2br(print_r($result)));
	}
	
        if (!$result || !count($result)) {
	    
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = Amazon_Tools::l("No orders to update staus");
		$this->proc_rep->set_status_msg($this->message);
	    }
	    //ordersByStatus returned nothing
            return (true);
        }
	
	$lastorder_date = $result[0]['date_add'];
        $dateStart = date('Y-m-d H:i:s', strtotime("$lastorder_date - 4 hours"));

        // Merge as array('id_order' => ..)    
        $orders = $updateOrder = $canceled_orders = array();

        foreach ($result as $entry) {
	    if(isset($entry['id_orders']) && !empty($entry['id_orders']))
		$orders[$entry['id_orders']] = $entry;
        }
	
        $result = $this->amazonMultiChannel->ListAllFulfillmentOrders($dateStart);
	
	if ($this->debug) {
	    printf('ListAllFulfillmentOrders() returns: %s'."<br/>\n", nl2br(print_r($result)));
	}
	
        if (!$result) {
	    $this->set_error_msg('Impossible to retrieve the order from Amazon', __FUNCTION__);
	    return (false);
        }
	
        if (!is_array($result)) {
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = Amazon_Tools::l("No Fulfillment Orders from Amazon");
		$this->proc_rep->set_status_msg($this->message);
	    }
	    //$this->set_error_msg('ListAllFulfillmentOrders returns nothing', __FUNCTION__);
            return (true);
        }

        // Merge orders informations
        foreach ($result as $fulfillmentOrderss) {
            foreach ($fulfillmentOrderss as $fulfillmentOrders) {
                foreach ($fulfillmentOrders as $fulfillmentOrder) {
                    if (isset($fulfillmentOrder->SellerFulfillmentOrderId) && 
			    $fulfillmentOrder->SellerFulfillmentOrderId && 
			    isset($orders[(string)$fulfillmentOrder->SellerFulfillmentOrderId])) {
                        $orders[(string)$fulfillmentOrder->SellerFulfillmentOrderId]['FulfillmentOrderStatus'] = (string)$fulfillmentOrder->FulfillmentOrderStatus;
                        $orders[(string)$fulfillmentOrder->SellerFulfillmentOrderId]['StatusUpdatedDateTime'] = (string)$fulfillmentOrder->StatusUpdatedDateTime;
                    }
                }
            }
        }
	
	if ($this->debug) {
	    printf('Orders: %s'."<br/>\n", nl2br(print_r($orders)));
	}
	
        foreach ($orders as $key => $order) {
	    
	    $this->no_process++;
	    	   
            $id_order = (int)$order['id_orders'];
            $seller_order_id = $order['seller_order_id'];

            if (!isset($order['FulfillmentOrderStatus'])) {
                // not listed
		AmazonFBAOrder::$warnings[] = sprintf(Amazon_Tools::l('Missing Fulfillment Order Status, order id: %d'), $id_order);
		$this->skipped++;
                continue;
            }
	    	    
            if (!$this->amazonMultiChannel->getMpStatus($id_order)) {
		
		AmazonFBAOrder::$warnings[] = sprintf(Amazon_Tools::l('Unable to load order id: %d'), $id_order);
		
                if ($this->debug) {
                    sprintf('FulfillmentOrderStatuses() unable to load order id: %d'."<br/>\n", $id_order);
                }
		
		$this->skipped++;
                continue;
            }
	    
            if ($order['mp_channel_status'] != $order['FulfillmentOrderStatus']) {
		
                $this->amazonMultiChannel->updateMpChannelStatus($id_order, $order['FulfillmentOrderStatus']);               
                $orders[$key]['NewFulfillmentOrderStatus'] = $order['FulfillmentOrderStatus'];
		
            }
	
            switch (strtolower($order['FulfillmentOrderStatus'])) {
		
                case AmazonMultiChannel::AMAZON_FBA_STATUS_PROCESSING :
                case AmazonMultiChannel::AMAZON_FBA_STATUS_COMPLETE :
                case AmazonMultiChannel::AMAZON_FBA_STATUS_COMPLETEPARTIALLED :
		case AmazonMultiChannel::AMAZON_FBA_STATUS_RECEIVED :                 
		    	
		    if (!$result = $this->amazonMultiChannel->GetFulfillmentOrder($id_order, true)) {	
			
			AmazonFBAOrder::$warnings[] =  sprintf(Amazon_Tools::l('Impossible to retrieve the order from Amazon order id: %d'), $seller_order_id);		
			$this->skipped++;
			
			if ($this->debug) {
			    sprintf('Impossible to retrieve the order from Amazon order id: %d', $seller_order_id);
			}
			
			continue;
		    }	
		     
		    if ($this->debug) {
			sprintf('GetFulfillmentOrder order id: %d, %s'."<br/>\n", $id_order, nl2br(print_r($result)));
		    }

                    if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentStatus) && $result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentStatus == 'SHIPPED') {
			
                        if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->TrackingNumber)) {
                            $trackingNumber = $result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->TrackingNumber;
                        } else {
                            $trackingNumber = null;
                        }

                        if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode)) {
                            $carrierCode = $result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->CarrierCode;
                        } else {
                            $carrierCode = null;
                        }
			
			if (isset($result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->EstimatedArrivalDateTime)) {
			    $EstimatedArrivalDateTime = $result->GetFulfillmentOrderResult->FulfillmentShipment->member->FulfillmentShipmentPackage->member->EstimatedArrivalDateTime;
			    $EstimatedArrivalDateTime =date('Y-m-d H:i:s', strtotime($EstimatedArrivalDateTime));
			} else {
                            $EstimatedArrivalDateTime = null;
                        }
			
                        if ($carrierCode && $trackingNumber && $EstimatedArrivalDateTime) {
			    
			    $this->no_success++;

			    $updateOrder [$id_order] = array(
				'OrderID' => $id_order,				    
				'SellerOrderId' => $seller_order_id,				    
				'CarrierName' => (string)$carrierCode,
				'ShippingNumber' => (string)$trackingNumber,
				'ShippingDate' => (string)$EstimatedArrivalDateTime,
				//'id_shop' => $this->params->id_shop,
				//'id_marketplace' => $this->params->id_marketplace,
				//'site' => $this->params->id_country,				
			    );	
			   
                        } else {
			    AmazonFBAOrder::$warnings[] =  sprintf(Amazon_Tools::l('Missing Carrier Code or Tracking Number, order id: %s'), $seller_order_id);
			    $this->skipped++;
			    continue;
			}
			
			// If SHIPPED Update mp_status => 4
			if(!$this->debug)
			    $this->amazonMultiChannel->updateMpStatus($id_order, $sent_state);
                    }
                    break;
		    
		case AmazonMultiChannel::AMAZON_FBA_STATUS_CANCELLED:
	
			$canceled_orders[$id_order] = $id_order;
			
		    break;
            }
        }
	
	if ($this->debug) {
	    sprintf('Update Order Tracking List: %s'."<br/>\n", nl2br(print_r($updateOrder)));
	    sprintf('Update Order Canceled List: %s'."<br/>\n", nl2br(print_r($canceled_orders)));
	}
	
	if(!empty($updateOrder) || !empty($canceled_orders)){
	    
	    // update tracking
	    if(!empty($updateOrder) )
	    {
		$this->updateOrderTracking($updateOrder);
	    }
	    
	    // Update canceled orders
	    if(!empty($canceled_orders)) 
	    {			    
		$this->updateOrderCanceled($canceled_orders);
	    }
	    
	    if(isset($this->proc_rep)) {
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = Amazon_Tools::l("Update Status Success");
		$this->proc_rep->set_status_msg($this->message);
	    }
	    
	} else {
	    
	    // empty updateOrder
	    if(isset($this->proc_rep)) {					
		$this->message->{AmazonFBAOrder::FBAOrder}->status = 'Success';
		$this->message->{AmazonFBAOrder::FBAOrder}->message = Amazon_Tools::l("No_order_update");
		$this->proc_rep->set_status_msg($this->message);
	    }
	    
	}		
	
	// save validate log
	$this->ValidationError();

    }
    
    public function Init()
    {       
	//$this->amazon_id_lang = (int) $this->params->id_lang;
	$this->amazonMultiChannel = new AmazonMultiChannel($this->params->user_name, $this->debug);
	
	if (!$this->amazonMultiChannel->AmazonApi($this->params)) {	   
	    $this->set_error_msg('Unable_to_connect_Amazon', __FUNCTION__);
            return (false);
        }
	
        return (true);
    }

    private function AmazonDatabase() {
        
        if (!$this->db = new AmazonDatabase($this->params->user_name, $this->debug)) {
            $this->error = Amazon_Tools::l('Unable_to_login.');
            $this->message->error = $this->error;
            if (isset($this->proc_rep)) {
                $this->proc_rep->set_error_msg($this->message);
	    }
            $this->StopFeed();
	    
	    return (false);
        }
	
	return (true);
    }

    private function StartProcessMessage() {
        
        $process_running = $this->operation_type;
        $process_title = $this->process;
        $process_type = 'fba_order_' . $this->operation_type . '_' . $this->params->countries;
        $marketplace = strtolower(AmazonFBAOrder::$tag);
       
	$this->proc_rep = new report_process($this->params->user_name, $this->batchID, $process_title, true, false, false);
	$this->proc_rep->set_process_type($process_type);
	$this->proc_rep->set_popup_display(false);
	$this->proc_rep->set_marketplace($marketplace);
	    
        $process = $this->proc_rep->has_other_running($process_type);
	
        if ($process) {
            $this->message->error =sprintf(Amazon_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'),$process_running,'20');
            $this->proc_rep->set_error_msg($this->message);
            $this->proc_rep->finish_task();
	    
            echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, ' . $this->operation_type . ' until old process done'));
	    return (false);    
        }       
	
	return (true);
    }

    private function StopFeed($exit=false) {
        
        $error = null;
	
        if (isset($this->error) && !empty($this->error)){
            $error = $this->error;
            $this->log(array('type' => $this->operation_type,'error' => $error, 'date_add' => date('Y-m-d H:i:s')));

	    if ($this->debug){	   
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'Error : <pre>' . $this->error . '</pre>';
	    }
	    
        } else {
            $this->log(array('type' => $this->operation_type,'date_add' => date('Y-m-d H:i:s')));    
        }
        
        if (isset($this->proc_rep)) 
            $this->proc_rep->finish_task();
	
	echo json_encode(array('status' => 'success', 'message' => $error));
	
	if($exit)
	    exit;
    }
    
    private function log($datail = null){
        
        $log = array(
            "batch_id"      => $this->batchID,
            "id_country"    => (int)$this->params->id_country,
            "id_shop"       => (int)$this->params->id_shop,
            "action_type"   => self::PROCESS,
            "no_skipped"    => isset($this->skipped) ? (int)$this->skipped : 0, 
            "no_process"    => isset($this->no_process) ? (int)$this->no_process : 0,
            "no_success"    => isset($this->no_success) ? (int)$this->no_success : 0,                      
            "is_cron"       => $this->cron ? true : false,                
            "date_upd"      => $this->time
        );
        
        if(isset($datail) && !empty($datail))
             $log['detail'] = base64_encode(serialize($datail));
        
        if(!$this->debug){
            $this->db->update_log($log);
	} else {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'LOGS : <pre>' . print_r($log, true) . '</pre>';
	}
    }
    
    private function ValidationError() {
	
	$sql = '';	
	
	if (count(AmazonFBAOrder::$warnings)) {
	    
	    $errors = array();
	    $errors['warning'] = AmazonFBAOrder::$warnings;
	    
	    foreach ($errors as $type => $error)
	    {		
		if(!empty($error)) 
		{
		    foreach ($error as $messages)
		    {
			$error_data = array(
			    'batch_id' => $this->batchID,
			    'id_country' => $this->params->id_country,
			    'id_shop' => $this->params->id_shop,
			    'action_process' => $this->process,
			    'action_type' => $type,
			    'message' => strval($messages),
			    'date_add' => date("Y-m-d H:i:s"),
			);            

			$sql .= $this->db->save_validation_log($error_data, false);
			unset($error_data);
		    }
		}
	    }
	    
	    unset($errors);
	    
	    if(strlen($sql) > 10)
	    {
		if (!$this->debug){
		    $this->db->exec_query($sql);
		} else {
		    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		    echo 'SQL : <pre>' . $sql . '</pre>';
		}
	    }
	}
	
	unset($sql);
    }    
    
    private function checkFeatureStatus($id_country=null){
	
	if(!isset($id_country) || empty($id_country))
	    $country = $this->params->id_country;
	else
	    $country = $id_country;
		    
	$features = $this->db->get_configuration_features($country, $this->params->id_shop); //$this->params->id_country
	
	if(!isset($features['fba']) || empty($features['fba']) || $features['fba'] = 0)
	{
	    $this->error = Amazon_Tools::l('inactivate_fba');
	    $this->message->error = $this->error;
	    $this->message->link = array('amazon', 'features', $this->params->id_country);
	    $this->message->link_label = Amazon_Tools::l('Go to features');

	    if (isset($this->proc_rep)) {
		$this->proc_rep->set_error_msg($this->message);
	    }
	    $this->StopFeed();
	    return (false);
	}
	
	return (true);
    }
    
    private function set_error_msg($msg, $function=null){
	
	$this->error = Amazon_Tools::l($msg);
	$this->message->error = $this->error;

	if ($this->debug) {
	    printf("$function() ".$this->error."<br/>\n");
	}

	if (isset($this->proc_rep)) {
	    $this->proc_rep->set_error_msg($this->message);
	}
    }
    		    
    private function GetFeedbizConfigInfo(){
        $config_data = AmazonUserData::getShopInfo($this->params->id_customer);
        return $config_data;
    }
    
    private function updateOrderCanceled($orders){	
	
        $info = $this->GetFeedbizConfigInfo();  
	
	if ($this->debug) {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'info : <pre>' . print_r($info, true) . '</pre>';
	}
	
	$by_seller_order_id = true;
	
        return $this->amazonMultiChannel->feedbiz_order->updateOrderCanceled(
		$this->params->user_name, 
		$this->params->user_code, 
		$info['order_cancel'], 
		$orders, 
		$this->batchID, 
		$by_seller_order_id, 
		$this->debug
	    );        
    }
    
    private function updateOrderTracking($orders){	
	
        $info = $this->GetFeedbizConfigInfo();  
	
	if ($this->debug) {
	    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
	    echo 'info : <pre>' . print_r($info, true) . '</pre>';
	}
	
	//$this->amazonMultiChannel->feedbiz_order->updateOrderTrackingByIdOrders($this->params->user_name, $orders);
        return $this->amazonMultiChannel->feedbiz_order->updateOrderTrackingByIdOrders(
		$this->params->user_name, 
		$orders, 
		$this->params->user_code, 
		$info['order_status'], 
		$this->batchID, 
		$this->debug
	    );        
    }
    
}