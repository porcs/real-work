<?php

require_once(dirname(__FILE__) . '/../classes/amazon.webservice.php');
require_once(dirname(__FILE__) . '/../classes/amazon.database.php');
require_once dirname(__FILE__) . '/../classes/amazon.config.php';

class AmazonShippingConfig
{
    const MERCHANT_ACTIVE_LISTINGS_DATA = 'active_listings_data';
    const EXPIRE = 14400; //4 hours

    public $import = null;
    public $ws = null;
    public $marketplaceId = null;
    public $region = null;
    public $file_inventory = null;
    public $merchantId = null;

    public static $errors = array();
    public static $messages = array();
    
    public function __construct($params, $debug=false, $cron=false)
    {
        $this->cron = $cron;
        $this->debug = $debug;
        $this->params = $params;
        $this->batchID = uniqid();
        $this->time = date('Y-m-d H:i:s');
        
        Amazon_Tools::load(_REPORT_LANG_FILE_, $this->params->language); 
        
        $this->message_type = sprintf('%s %s', Amazon_Tools::l('Amazon get shipping groups started on'), '') ;
        $this->message = new stdClass();
        $this->message->date = date('Y-m-d', strtotime($this->time));
        $this->message->time = date('H:i:s', strtotime($this->time));
	
        if(!$this->debug){ 
	    $this->startProcess(); 	    
	}
	
        $this->automaton = $this->AmazonAutomaton();
        
        $this->initImportDirectory() ;

        if ( $this->debug )
        {
            @ini_set('display_errors', 'on');
            @error_reporting(E_ALL | E_STRICT);            
        }   
    }
    
    public function dispatch($action='groups')
    {
        switch ($action)
        {
            case 'groups':
                $this->getShippingGroupNames();
                break;            
        }

    }
    
    
    private function initImportDirectory()
    {
        $directory = dirname(__FILE__) . '/../../../../' ;
        $output_dir = $directory . 'assets/apps/users/'. $this->ws->user . '/amazon';
        
        // Check rights
        if (!is_dir($output_dir) && !mkdir($output_dir))
        {
            self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to create path')) ;
            return false;
        }
        
        $this->import = $output_dir . '/report/' ;

        if ( ! is_dir($this->import) )
        {
            if ( ! @mkdir($this->import) )
            {
                self::$process->errors = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, Amazon_Tools::l('Unable to create import directory')) ;
                die;
            }
            
            @chmod($this->import, 0777) ;
        }
    }    
    
    public function setFileInventory()
    {
        $fileid = floor((time() % (86400 * 365)) / self::EXPIRE); // file id valid for 4 hours

        $this->file_inventory = sprintf('%s%s_%s_%s_%s.raw', $this->import, self::MERCHANT_ACTIVE_LISTINGS_DATA, $this->merchantId, $this->region, $fileid);

        return;
    }

    public function getShippingGroupNames()
    {
        ob_start();

        $pass = false;
        $continue = false;
        $group_names = array();

        if ($this->Init())
        {
            if (file_exists($this->file_inventory) && filesize($this->file_inventory) && filemtime($this->file_inventory) > time() - self::EXPIRE )
            {
                self::$messages[] = $message = sprintf($this->l('Using existings file: "%s" - Expires: %s') , basename($this->file_inventory), date('Y-m-d H:i:s', filemtime($this->file_inventory) + self::EXPIRE));

                // Inventory Exists, and downloaded, process the report
                $group_names = $this->ProcessInventory();

                if (is_array($group_names) && count($group_names))
                {
                    $configured_group_names = unserialize(AmazonConfiguration::get('shipping_groups'));

                    if (is_array($configured_group_names) && count($configured_group_names))
                    {
                        unset($configured_group_names[$this->region]);
                        $configured_group_names[$this->region] = $group_names;
                    }
                    else
                    {
                        $configured_group_names = array();
                        $configured_group_names[$this->region] = $group_names;
                    }
                    AmazonConfiguration::updateValue('shipping_groups', serialize($configured_group_names));

                    self::$messages[] = $message = sprintf('%d %s', count($group_names), $this->l('shipping groups have been retrieve with success'));
                    $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        print $debug;
                        echo "</pre>\n";
                    }

                    $pass = true;
                }
                else
                {
                    $configured_group_names = unserialize(AmazonConfiguration::get('shipping_groups'));

                    if (is_array($configured_group_names) && count($configured_group_names))
                    {
                        //unset($configured_group_names[$this->region]);
                    }

                    AmazonConfiguration::updateValue('shipping_groups', serialize($configured_group_names));

                    $error = $this->l('Not any existing shipping groups have been found from the inventory');
                    $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
                    self::$errors[] = $error;

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        echo $debug;
                        echo "</pre>\n";
                    }
                }
                $continue = false;
            }
            elseif (file_exists($this->file_inventory) && !filesize($this->file_inventory))
            {
                // Inventory Exists, but has not been downloaded

                // Check Timestamp
                // 1 - if timestamp more than 2 minutes; get report
                // 2 - if less ; ask to wait

                $request_time = filemtime($this->file_inventory);
                $now = time();
                $elapsed = $now - $request_time;

                if (Amazon::$debug_mode)
                {
                    echo "<pre>\n";
                    printf('%s(#%d): %s - Request Time: "%s", elapsed: %d', basename(__FILE__), __LINE__, __FUNCTION__, date('c', $request_time), $elapsed);
                    echo "</pre>\n";
                }

                if ($elapsed > 60 * 60)
                {
                    $error = $this->l('Delay to download report is expired');
                    $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
                    self::$errors[] = $error;

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        echo $debug;
                        printf('%s(#%d): %s - ERROR: Request Time: "%s", elapsed: %d - delay expired', basename(__FILE__), __LINE__, __FUNCTION__, date('c', $request_time), $elapsed);
                        echo "</pre>\n";
                    }
                    unlink($this->file_inventory);
                    $continue = false;
                    $pass = false;
                }
                elseif ($elapsed < 60 * 2)
                {
                    $continue = true;
                    $pass = true;

                    self::$messages[] = $message = $this->l('Waiting a while for the report to be ready for download');
                    $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        print $debug;
                        echo "</pre>\n";
                    }
                }
                else
                {
                    $reportRequestId = $this->reportRequestList();

                    if ($reportRequestId)
                    {
                        if ($this->getReport($reportRequestId))
                        {
                            self::$messages[] = $message = sprintf('%s (%s)', $this->l('Downloading Report ID'), $reportRequestId);
                            $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $message);

                            if (Amazon::$debug_mode)
                            {
                                echo "<pre>\n";
                                print $debug;
                                echo "</pre>\n";
                            }

                            $continue = true;
                            $pass = true;
                        }
                        else
                        {
                            $error = $this->l('Failed to download the Report');
                            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
                            self::$errors[] = $error;

                            if (Amazon::$debug_mode)
                            {
                                echo "<pre>\n";
                                print $debug;
                                echo "</pre>\n";
                            }
                            $continue = false;
                            $pass = false;
                        }
                    }
                    else
                    {
                        self::$messages[] = sprintf('%s (%s)', $this->l('Waiting for the report to be available... this operation could take time'), $reportRequestId);

                        if (Amazon::$debug_mode)
                        {
                            echo "<pre>\n";
                            printf('%s(#%d): %s - A report has been already requested and there is not any available report yet', basename(__FILE__), __LINE__, __FUNCTION__);
                            echo "</pre>\n";
                        }
                        touch($this->file_inventory);
                        $continue = true;
                        $pass = true;
                    }
                }
            }
            else
            {
                // File doesn't exist
                // 1 - Create the file
                // 2 - Request the Report

                if (!AmazonTools::is_dir_writeable($this->import))
                {
                    $error = sprintf('"%s" %s', $this->import, $this->l('is not a writable directory, please check directory permissions'));
                    $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
                    self::$errors[] = $error;

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        echo "Error:$debug\n";
                        echo "</pre>\n";
                    }
                    $continue = false;
                    $pass = false;
                }
                if (file_put_contents($this->file_inventory, null) === false)
                {
                    $error = sprintf('%s: "%s"', $this->import, $this->l('failed to create file'), $this->file_inventory);
                    $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
                    self::$errors[] = $error;

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        echo "Error:$debug\n";
                        echo "</pre>\n";
                    }
                    $continue = false;
                    $pass = false;
                }

                if ($reportRequestId = $this->reportRequest())
                {
                    touch($this->file_inventory);

                    self::$messages[] = sprintf($this->l('Report has been requested (%s), please wait a while'), $reportRequestId);

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        printf('%s(#%d): %s - Report Request ID: "%s"', basename(__FILE__), __LINE__, __FUNCTION__, $reportRequestId);
                        echo "</pre>\n";
                    }
                    $continue = true;
                    $pass = true;
                }
                else
                {
                    $error = $this->l('Request Report failed, please review your module configuration');
                    $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
                    self::$errors[] = $error;

                    if (Amazon::$debug_mode)
                    {
                        echo "<pre>\n";
                        print $debug;
                        echo "</pre>\n";
                    }
                    $continue = false;
                    $pass = false;
                }
            }

        }
        $result =
            array(
                'error' => (count(self::$errors) ? true : false),
                'errors' => self::$errors,
                'message' => count(self::$messages) ? true : false,
                'messages' => self::$messages,
                'groups' => count($group_names) ? $group_names : null,
                'continue' => $continue,
                'pass' => $pass,
                'debug' => Amazon::$debug_mode,
                'output' => ob_get_clean()
            );

        $json = Tools::jsonEncode($result);

        if ($callback = Tools::getValue('callback'))
        {
            if ($callback == '?')
            {
                $callback = 'jsonp_'.time();
            }
            echo (string)$callback.'('.$json.')';
            die;
        }
        else
        {
            echo '<pre>'."\n";
            print_r($result);
            echo '</pre>'."\n";
            die;
        }
    }

    public function Init()
    {
        $lang = Tools::getValue('lang');
        $id_lang = (int)Tools::getValue('id_lang');
        $token = Tools::getValue('instant_token');

        if (!$token || $token != Configuration::get('AMAZON_INSTANT_TOKEN', null, 0, 0))
        {
            print 'Wrong token';
            die;
        }

        $marketPlaceRegion = unserialize(AmazonTools::decode(Configuration::get('AMAZON_REGION')));
        $marketPlaceIds = unserialize(AmazonTools::decode(Configuration::get('AMAZON_MARKETPLACE_ID')));

        if (!is_array($marketPlaceRegion) || !count($marketPlaceRegion))
        {
            $error  = $this->l('Module is not configured yet');
            self::$errors = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$error\n";
                echo "</pre>\n";
            }
            return (false);
        }

        $marketLang2Region = array_flip($marketPlaceRegion);

        if (Tools::strlen($lang))
        {
            if (!isset($marketLang2Region[$lang]) || !$marketLang2Region[$lang])
            {
                die('No selected language, nothing to do...');
            }

            $id_lang = $marketLang2Region[$lang];
        }
        elseif (!empty($id_lang) && is_numeric($id_lang))
        {
            if (!isset($marketPlaceRegion[$id_lang]) || !$marketPlaceRegion[$id_lang])
            {
                die('No selected language, nothing to do...');
            }
        }

        $amazon = AmazonTools::selectPlatforms($id_lang, 0);

        $this->marketplaceId = trim($marketPlaceIds[$id_lang]);
        $this->region = trim($amazon['params']['Country']);
        $this->merchantId = trim($amazon['auth']['MerchantID']);

        $this->cleanupImportDirectory();

        $this->setFileInventory();

        if (Amazon::$debug_mode)
        {
            echo "<pre>\n";
            printf('Webservice Params: %s'."<br />\n", print_r($amazon, true));
            echo "</pre>\n";
        }
        $this->ws = new AmazonWebService($amazon['auth'], $amazon['params'], null, Amazon::$debug_mode);

        $marketPlaceIds = unserialize(AmazonTools::decode(Configuration::get('AMAZON_MARKETPLACE_ID')));

        if (!isset($marketPlaceIds[$id_lang]) || !$marketPlaceIds[$id_lang])
        {
            $lang = new Language($id_lang);
            $error = sprintf('%s(#%d): %s "%s"', basename(__FILE__), __LINE__, $this->l('Marketplace is not yet configured for'), $lang->name);

            self::$errors = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$error\n";
                echo "</pre>\n";
            }
            return (false);
        }
        return(true);
    }

    protected function ProcessInventory()
    {
        if (Amazon::$debug_mode)
        {
            printf('ProcessInventory()'."<br />\n");
        }

        if (($result = file_get_contents($this->file_inventory)) === false)
        {
            $error = $this->l('Unable to read input file');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        if ($result == null or empty($result))
        {
            $error = $this->l('Inventory is empty !');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        $lines = explode("\n", $result);

        if (!is_array($lines) || !count($lines))
        {
            $error = $this->l('Inventory is empty !');
            $debug = sprintf('%s(#%d): %s - %s (%s)', basename(__FILE__), __LINE__, __FUNCTION__, $error, $this->file_inventory);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        if (Amazon::$debug_mode)
        {
            echo "<pre>\n";
            echo str_repeat('-', 160)."\n";
            printf('Inventory: %s products'."<br />\n", count($lines));
            echo "</pre>\n";
        }

        $header = reset($lines);

        if (!Tools::strlen($header))
        {
            $error = $this->l('No header, file might be corrupted');
            $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }

        $columns = explode("\t", $header);

        // Header, display to the user he doesn't have merchant shipping group
        if (!in_array('merchant-shipping-group', $columns))
        {
            $error = $this->l('No merchant shipping groups detected, it seems your are not using shipping templates');
            $debug = sprintf('%s(#%d): %s - %s', basename(__FILE__), __LINE__, __FUNCTION__, $error);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$debug\n";
                echo "</pre>\n";
            }
            return (false);
        }
        $columns_keys = array_flip($columns);
        $merchant_shipping_group_key = $columns_keys['merchant-shipping-group'];

        $count = 0;
        $group_names = array();

        foreach ($lines as $line)
        {
            if (empty($line))
                continue;

            if ($count++ < 1)
                continue;

            $result = explode("\t", $line);

            if (count($result) < $merchant_shipping_group_key + 1)
                continue;

            $group_key = AmazonTools::toKey($result[$merchant_shipping_group_key]);
            $group_names[$group_key] = utf8_encode($result[$merchant_shipping_group_key]);
        }

        if (!is_array($group_names) || !count($group_names))
        {
            $error = sprintf('%s(#%d): %s - Not any group name found', basename(__FILE__), __LINE__, __FUNCTION__);
            self::$errors[] = $error;

            if (Amazon::$debug_mode)
            {
                echo "<pre>\n";
                echo "Error:$error\n";
                echo "</pre>\n";
            }
            return (false);
        }

        if (Amazon::$debug_mode)
        {
            echo "<pre>\n";
            printf('Processed Items: %s'."<br />\n", print_r($group_names, true));
            echo "</pre>\n";
        }

        return ($group_names);
    }

    private function filelistImportDirectory()
    {
        // Generic function sorting files by date
        $output_dir = sprintf('%s/', rtrim($this->import, '/'));

        if (!is_dir($output_dir))
        {
            return null;
        }

        $files = glob($output_dir.self::MERCHANT_ACTIVE_LISTINGS_DATA.'*.raw');

        if (!is_array($files) || !count($files))
        {
            return null;
        }

        // Sort by date
        foreach ($files as $key => $file)
        {
            $files[filemtime($file)] = $file;
            unset($files[$key]);
        }
        ksort($files);

        return $files;
    }

    private function cleanupImportDirectory()
    {
        // Cleanup oldest files
        $files = $this->filelistImportDirectory();
        $now = time();

        if (is_array($files) && count($files))
        {
            foreach ($files as $timestamp => $file)
            {
                if ($now - $timestamp > 86400 * 30)
                {
                    unlink($file);
                }
            }
        }
    }


    protected function reportRequest()
    {
        $params = array();
        $params['Action'] = 'RequestReport';
        $params['ReportType'] = '_GET_MERCHANT_LISTINGS_DATA_';
        $params['Version'] = '2009-01-01';
        $params['MarketplaceIdList.Id.1'] = $this->marketplaceId;

        if (Amazon::$debug_mode)
        {
            printf('reportRequest()'."<br />\n");
        }

        $xml = $this->ws->simpleCallWS($params, 'Reports');

        if (!$xml instanceof SimpleXMLElement or isset($xml->Error))
        {
            printf('%s(#%d): %s - reportRequest Failed', basename(__FILE__), __LINE__);

            return (false);
        }

        if (Amazon::$debug_mode)
        {
            echo  $this->debugXML($xml);
        }

        if (!isset($xml->RequestReportResult->ReportRequestInfo->ReportProcessingStatus) || !isset($xml->RequestReportResult->ReportRequestInfo->ReportRequestId))
        {

            printf('%s(#%d): %s - reportRequest Failed', basename(__FILE__), __LINE__);

            return (false);
        }

        if ($xml->RequestReportResult->ReportRequestInfo->ReportProcessingStatus == '_SUBMITTED_')
        {
            $requestId = (string)$xml->RequestReportResult->ReportRequestInfo->ReportRequestId;
            return ($requestId);
        }
        else
        {
            return (false);
        }
    }

    protected function reportRequestList()
    {
        $params = array();
        $params['Version'] = '2009-01-01';
        $params['Action'] = 'GetReportRequestList';
        $params['ReportRequestList.Type.1'] = '_GET_MERCHANT_LISTINGS_DATA_';
        $params['ReportProcessingStatusList.Status.1'] = '_DONE_';
        $params['RequestedFromDate'] = gmdate('Y-m-d\TH:i:s\Z', strtotime('now -1 hour'));

        if (Amazon::$debug_mode)
        {
            printf('reportRequestList()'."<br />\n");
        }

        $xml = $this->ws->simpleCallWS($params, 'Reports');

        if (!$xml instanceof SimpleXMLElement or isset($xml->Error))
        {
            printf('%s(#%d): reportRequestList Failed', basename(__FILE__), __LINE__);
            return (false);
        }

        if (Amazon::$debug_mode)
        {
            printf('reportRequestList() - report'."<br />\n");
            echo $this->debugXML($xml);
        }

        $xml->registerXPathNamespace('xmlns', 'http://mws.amazonaws.com/doc/2009-01-01/');

        $xpath_result = $xml->xpath('//xmlns:GetReportRequestListResponse/xmlns:GetReportRequestListResult/xmlns:ReportRequestInfo');

        if (Amazon::$debug_mode)
        {
            echo "<pre>Reports:\n";
            printf('%s(#%d): reportRequestList result: %s', basename(__FILE__), __LINE__, nl2br(print_r($xpath_result, true)));
            echo "</pre>\n";
        }

        if (is_array($xpath_result) && !count($xpath_result))
        {
            return(false);
        }
        else
        {
            // the report is available, take the first one :
            $report_data = reset($xpath_result);

            if ($report_data instanceof SimpleXMLElement)
            {
                if (Amazon::$debug_mode)
                {
                    echo "<pre>Selected Report:\n";
                    var_dump($report_data);
                    echo "</pre>\n";
                }
                if (isset($report_data->GeneratedReportId) && $report_data->GeneratedReportId)
                {
                    return((string)$report_data->GeneratedReportId);
                }
            }
            else return(false);
        }

        return (false);
    }

    protected function getReport($reportRequestId)
    {
        $params = array();
        $params['Version'] = '2009-01-01';
        $params['Action'] = 'GetReport';
        $params['ReportId'] = $reportRequestId;

        if (Amazon::$debug_mode)
        {
            printf('getReport()'."<br />\n");
        }

        $result = $this->ws->simpleCallWS($params, 'Reports', false);

        if ($result instanceof SimpleXMLElement)
        {
            printf('%s(#%d): getReport - An error occur', basename(__FILE__), __LINE__);
            var_dump($result);
            die;
        }

        if (empty($result))
        {
            printf('%s(#%d): getReport - Inventory is empty', basename(__FILE__), __LINE__);
            return (false);
        }

        if (file_put_contents($this->file_inventory, $result) === false)
        {
            printf('%s(#%d): getReport - Unable to write to output file: %s', basename(__FILE__), __LINE__, $this->file_inventory);
            return (false);
        }

        return (true);
    }

    public function debugXML($xml)
    {
        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;

        $output = '<pre>';
        $output .= htmlspecialchars($dom->saveXML());
        $output .= '</pre>';

        return ($output);
    }
}

$amazonShippingConfig = new AmazonShippingConfig();
$amazonShippingConfig->dispatch();
