<?php

require_once dirname(__FILE__) . '/../classes/amazon.config.php';
require_once dirname(__FILE__) . '/../classes/amazon.form.php';
require_once dirname(__FILE__) . '/../classes/amazon.validValues.php';
require_once dirname(__FILE__) . '/../classes/amazon.field.settings.php';
require_once dirname(__FILE__) . '/../classes/amazon.tools.php';
require_once(dirname(__FILE__) . '/../../FeedBiz.php');


class AmazonDisplayForm {

    const TAG = 'Amazon';

    public $excludeAttributeFromMapping = array('Width' => 1, 'Height' => 1, 'Weight' => 1, 'Depth' => 1);
    public $amazon_model;
    public $valid_value;
    public $feedbiz;
    public $params;
    public $ext_id;
    public $mapping_attribute_groups;
    public $mapping_feature_groups;

    public function __construct($amazon=null, $feedbiz=null, $params=null) {

        if(isset($params->user_name))
            $user = $params->user_name;
        
        // load library
        if(isset($feedbiz)){
            $this->feedbiz = $feedbiz;
        } else {
            if(isset($user))
                $this->feedbiz = new FeedBiz(array($user));
        }
        // amazon
        if(isset($feedbiz)){
            $this->amazon_model = $amazon;
        } else {
            if(isset($user))
                $this->amazon_model = new AmazonDatabase($user);
        }
        // params
        $this->params = $params;

        // Valid Value
        $this->valid_value = new ValidValue($this->params->ext);
        
        AmazonXSD::initialize();
    }

    public function displayModelsForm($content, $country=null, $id_shop=null) {
	
        //set global values, in case it's needed when rendering attributes
        global $amzn_attr_values;
        global $recommended_values;
        global $element_values;

        $data = $amzn_attr_values = $m_name = $form = array();
        $i = 0;

        if(isset($country) && isset($id_shop)){
            $this->mapping_attribute_groups = $this->amazon_model->get_mapping_attribute_groups($country, $id_shop);
            $this->mapping_feature_groups = $this->amazon_model->get_mapping_feature_groups($country, $id_shop);
        }

        $productsUniverse=$productsUniverseName=$productTypes=$productTypesName=$productSubtype=$productSubtypeName=$mfrPartNumber=$variationTheme=$variation_datas=array();

        foreach ($content as $m) {
	   
            $mapping_variation_data = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'variation_data', false, true);
            $mapping_recommended_data = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'recommended_data', false, true);
            $mapping_specific_fields = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'specific_fields', false, true);

            $key = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace(" ", "_", $m['name']));
            if (isset($m_name[$m['name']]) && $m_name[$m['name']] == true)
                $key = $key . '_' . $i++;

            $data['model'][$key] = $m;

            $selected_universe = isset($m["universe"]) ? $m["universe"] : false;

            if ($selected_universe) {

                if(!isset($form[$selected_universe]))
                    $form[$selected_universe] = new AmazonForm($selected_universe, $this->params->ext);

                $element_values = isset($m["variation_data"]) ? $m["variation_data"] : array();

                if (!empty($element_values))
                    foreach ($element_values as $ele_key => $ele_value) {
                        if (isset($ele_value['attr']))
                            $amzn_attr_values[$ele_key] = $ele_value;
                    }

                $recommended_values = isset($m["recommended_data"]) ? $m["recommended_data"] : array();

                // Products Universe
                if(!isset($productsUniverse[$selected_universe]))
                    $productsUniverse[$selected_universe] = $form[$selected_universe]->getProductsUniverse($selected_universe);

                $data['model'][$key]['universe'] = $this->_getStringValueFromArray($productsUniverse[$selected_universe]);

                // Products Universe Name
                if(!isset($productsUniverseName[$selected_universe]))
                    $productsUniverseName[$selected_universe] = $form[$selected_universe]->getName($selected_universe, AmazonSettings::$universe);
                
                $data['model'][$key]['selected_universe'] = $productsUniverseName[$selected_universe];

                // Product types
                $selected_product_type = isset($m["product_type"]) ? $m["product_type"] : null;

                if(!isset($productTypes[$selected_universe][$selected_product_type]))
                    $productTypes[$selected_universe][$selected_product_type] = $form[$selected_universe]->getProductTypes($selected_product_type);

                $data['model'][$key]['product_type_options'] = $productTypes[$selected_universe][$selected_product_type];

                // Product types Name
                if(!isset($productTypesName[$selected_universe][$selected_product_type]))
                    $productTypesName[$selected_universe][$selected_product_type] = $form[$selected_universe]->getName($selected_product_type, AmazonSettings::$product_types);
                
                $data['model'][$key]['selected_product_type'] = $productTypesName[$selected_universe][$selected_product_type];

                // Product subtypes
                $selected_product_subtype = isset($m["sub_product_type"]) ? $m["sub_product_type"] : null;

                if(!isset($productSubtype[$selected_universe][$selected_product_type][$selected_product_subtype]))
                    $productSubtype[$selected_universe][$selected_product_type][$selected_product_subtype] =
                        $form[$selected_universe]->getProductSubtype($selected_product_type, $selected_product_subtype);
                
                $data['model'][$key]['product_subtype'] = $productSubtype[$selected_universe][$selected_product_type][$selected_product_subtype];

                // Product subtypes Name
                if(!isset($productSubtypeName[$selected_universe][$selected_product_type][$selected_product_subtype]))
                    $productSubtypeName[$selected_universe][$selected_product_type][$selected_product_subtype] =
                        $form[$selected_universe]->getName($selected_product_subtype);
                
                $data['model'][$key]['selected_product_subtype'] = $productSubtypeName[$selected_universe][$selected_product_type][$selected_product_subtype];

                // MfrPartNumber
                $mfr_part_number = isset($m["mfr_part_number"]) ? $m["mfr_part_number"] : null;

                if(!isset($mfrPartNumber[$selected_universe][$mfr_part_number]))
                    $mfrPartNumber[$selected_universe][$mfr_part_number] = $form[$selected_universe]->getMfrPartNumber($selected_product_type, $mfr_part_number);
                
                $data['model'][$key]['mfr_part_number'] = $mfrPartNumber[$selected_universe][$mfr_part_number];

                // Variation theme options
                $selected_variation_theme = isset($m["variation_theme"]) ? $m["variation_theme"] : null;

                if(!isset($variationTheme[$selected_universe][$selected_product_type][$selected_variation_theme]))
                    $variationTheme[$selected_universe][$selected_product_type][$selected_variation_theme] =
                        $form[$selected_universe]->getVariationTheme($selected_product_type, $selected_variation_theme);
                
                $data['model'][$key]['variation_theme'] = $variationTheme[$selected_universe][$selected_product_type][$selected_variation_theme];

                if ($selected_product_type) {
		
                    //Variation
                    if (isset($m['variation_data'])) {
                        $attr = array();
                        if (isset($mapping_variation_data['variation_data']['Color']['value']))
                            unset($mapping_variation_data['variation_data']['Color']['value']);

                        if (isset($mapping_variation_data['variation_data']['Size']['value']))
                            unset($mapping_variation_data['variation_data']['Size']['value']);
			
                        foreach ($m['variation_data'] as $variation_data_key => $variation_data) {
                            $attr = $this->get_mapping_value($mapping_variation_data['variation_data'], $variation_data);
                            $m['variation_data'][$variation_data_key] = $attr;
			    
                            if (isset($attr['CustomValue']) && !empty($attr['CustomValue'])) {
                                
                                $validValue = $this->get_valid_value($selected_universe, $variation_data_key, $selected_product_type, $attr['CustomValue']);
                                
                                if (isset($validValue) && !empty($validValue)) {
                                    $m['variation_data'][$variation_data_key]['CustomValue'] = $validValue;
                                }
                            }
                        }

                        $m['variation_data']['default'] = $mapping_variation_data['variation_data'];
                    }

                    //Recommended Data
                    if (isset($recommended_values) && is_array($recommended_values) && isset($mapping_recommended_data['recommended_data'])) {

                        $recommended = array();
                        foreach ($recommended_values as $recommended_key => $recommended_value) {

                            $recommended = $this->get_mapping_value($mapping_recommended_data['recommended_data'], $recommended_value);

                            if (isset($recommended_value['attr'])) {
                                $amzn_attr_values[$recommended_key] = $recommended_value;
                            }

                            $m['recommended_data'][$recommended_key] = $recommended;

                            if (isset($recommended['CustomValue']) && !empty($recommended['CustomValue'])) {

                                $validValue = $this->get_valid_value($selected_universe, $recommended_key, $selected_product_type, $recommended['CustomValue']);

                                if (isset($validValue) && !empty($validValue)) {
                                    $m['recommended_data'][$recommended_key]['CustomValue'] = $validValue;
                                }
                            }
                        }

                        $m['recommended_data']['default'] = $mapping_recommended_data['recommended_data'];
                    }

                    $VariationData = $form[$selected_universe]->getVariationData($selected_product_type, $selected_variation_theme, $m);
                   
                    $data['model'][$key]['recommended_data'] = $VariationData["recommended"];
                    $data['model'][$key]['variation_data'] = $VariationData["variation"];

                    // Specific Options
                    $data['model'][$key]['specific_options'] =  $form[$selected_universe]->getSpecificOptions($selected_product_type, (isset($m["specific_fields"]) ? $m["specific_fields"] : array()), $selected_variation_theme);

                    if (isset($m["specific_fields"])) {
                        $sf = $map = array();
                        $specific_html = "";
                        foreach ($m["specific_fields"] as $item => $item_value) {
                            if (isset($item_value['attr']))
                                $amzn_attr_values[$item] = $item_value;

                            if (isset($mapping_specific_fields) && !empty($mapping_specific_fields)) {
                                $map = $this->get_mapping_value($mapping_specific_fields['specific_fields'], $item_value);

                                if (isset($map['CustomValue']) && !empty($map['CustomValue'])) {
                                    $validValue = $this->get_valid_value($selected_universe, $item, $selected_product_type, $map['CustomValue']);
                                    if (isset($validValue) && !empty($validValue)) {
                                        $map['CustomValue'] = $validValue;
                                    }
                                }
                            }

                            $sf[] = $form[$selected_universe]->getSpecificFields($selected_product_type, $item, $map);
                        }

                        foreach ($sf as $item) {
                            foreach ($item as $name => $item_html) {
                                $specific_html .= $item_html;
                            }
                        }
                        $data['model'][$key]['specific_fields'] = $specific_html;
                    }
                }
                $data['model'][$key]['memory_usage'] = $m["memory_usage"] ? round($m["memory_usage"], 2) : "--";
            }
            $m_name[$m['name']] = true;
        }

        return $value = array('data' => $data, 'amzn_attr_values' => $amzn_attr_values);
    }

    public function duplicateForm($id_model) {
        global $amzn_attr_values;
        $data = $amzn_attr_values = array();

        $ms = $this->amazon_model->get_amazon_model(null, $this->params->id_mode, $this->params->id_shop, $id_model);
	
        foreach ($ms as $m) {
            $mapping_variation_data = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'variation_data', false, true);
            $mapping_recommended_data = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'recommended_data', false, true);
            $mapping_specific_fields = $this->get_mapping_group($m['id_country'], $m['id_mode'], $m['id_shop'], 'specific_fields', false, true);

            $selected_universe = isset($m["universe"]) ? $m["universe"] : false;

            if ($selected_universe) {
                $form = new AmazonForm($selected_universe, $this->params->ext);

                $data['universe'] = $this->_getStringValueFromArray($form->getProductsUniverse($selected_universe));
                $selected_product_type = isset($m["product_type"]) ? $m["product_type"] : null;
                $data['product_types'] = $form->getProductTypes($selected_product_type);
                $selected_variation_theme = isset($m["variation_theme"]) ? $m["variation_theme"] : null;
                $data['variation_theme'] = $form->getVariationTheme($selected_product_type, $selected_variation_theme);
                $selected_product_subtype = isset($m["sub_product_type"]) ? $m["sub_product_type"] : null;
                $data['product_subtype'] = $form->getProductSubtype($selected_product_type, $selected_product_subtype);
                $recommended_values = isset($m["recommended_data"]) ? $m["recommended_data"] : array();

                $element_values = isset($m["variation_data"]) ? $m["variation_data"] : array();

                if (!empty($element_values))
                    foreach ($element_values as $ele_key => $ele_value) {
                        if (isset($ele_value['attr']))
                            $amzn_attr_values[$ele_key] = $ele_value;
                    }
		    
                if ($selected_product_type) {
		    
                    /*//Variation
                    if (isset($m['variation_data'])) {
                        $attr = array();
                        if (isset($mapping_variation_data['variation_data']['Color']['value']))
                            unset($mapping_variation_data['variation_data']['Color']['value']);

                        if (isset($mapping_variation_data['variation_data']['Size']['value']))
                            unset($mapping_variation_data['variation_data']['Size']['value']);

                        foreach ($m['variation_data'] as $variation_data_key => $variation_data) {
                            $attr = $this->get_mapping_value($mapping_variation_data['variation_data'], $variation_data);
                            $m['variation_data'][$variation_data_key] = $attr;
                        }
                        $m['variation_data']['default'] = $mapping_variation_data['variation_data'];
                    }

                    //Recommended Data
                    if (isset($recommended_values) && is_array($recommended_values)) {

                        $recommended = array();
                        foreach ($recommended_values as $recommended_key => $recommended_value) {
                            $recommended = $this->get_mapping_value($mapping_recommended_data['recommended_data'], $recommended_value);
			 
			    
                            if (isset($recommended_value['attr']))
                                $amzn_attr_values[$recommended_key] = $recommended_value;

                            $m['recommended_data'][$recommended_key] = $recommended;
                        }
                        $m['recommended_data']['default'] = $mapping_recommended_data['recommended_data'];
                    }*/
		    
                    //Variation
                    if (isset($m['variation_data'])) {
                        $attr = array();
                        if (isset($mapping_variation_data['variation_data']['Color']['value']))
                            unset($mapping_variation_data['variation_data']['Color']['value']);

                        if (isset($mapping_variation_data['variation_data']['Size']['value']))
                            unset($mapping_variation_data['variation_data']['Size']['value']);
			
                        foreach ($m['variation_data'] as $variation_data_key => $variation_data) {
                            $attr = $this->get_mapping_value($mapping_variation_data['variation_data'], $variation_data);
                            $m['variation_data'][$variation_data_key] = $attr;
			    
                            if (isset($attr['CustomValue']) && !empty($attr['CustomValue'])) {
                                $validValue = $this->get_valid_value($selected_universe, $variation_data_key, $selected_product_type, $attr['CustomValue']);
                                if (isset($validValue) && !empty($validValue)) {
                                    $m['variation_data'][$variation_data_key]['CustomValue'] = $validValue;
                                }
                            }
                        }

                        $m['variation_data']['default'] = $mapping_variation_data['variation_data'];
                    }

                    //Recommended Data
                    if (isset($recommended_values) && is_array($recommended_values)) {
			
                        $recommended = array();
                        foreach ($recommended_values as $recommended_key => $recommended_value) {
                            $recommended = $this->get_mapping_value($mapping_recommended_data['recommended_data'], $recommended_value);
			    
                            if (isset($recommended_value['attr'])) {
                                $amzn_attr_values[$recommended_key] = $recommended_value;
                            }
			    
                            $m['recommended_data'][$recommended_key] = $recommended;                            
                            
                            if (isset($recommended['CustomValue']) && !empty($recommended['CustomValue'])) {

                                $validValue = $this->get_valid_value($selected_universe, $recommended_key, $selected_product_type, $recommended['CustomValue']);
                                
                                if (isset($validValue) && !empty($validValue)) {
                                    $m['recommended_data'][$recommended_key]['CustomValue'] = $validValue;
                                }
                            }
                        }

                        $m['recommended_data']['default'] = $mapping_recommended_data['recommended_data'];
                    }
		    
                    $VariationData = $form->getVariationData($selected_product_type, $selected_variation_theme, $m);
                    $data['recommended_data'] = $VariationData["recommended"];
                    $data['variation_data'] = $m['variation_data'] ? $VariationData["variation"] : null;
                    $data['specific_options'] = $form->getSpecificOptions($selected_product_type, (isset($m["specific_fields"]) ? $m["specific_fields"] : array()), $selected_variation_theme);
                    $specific_html = "";
		    
                    if (isset($m["specific_fields"])) {
                        $sf = $map = array();
                        foreach ($m["specific_fields"] as $item => $item_value) {
                            if (isset($item_value['attr']))
                                $amzn_attr_values[$item] = $item_value;

                            if (isset($mapping_specific_fields) && !empty($mapping_specific_fields))
                                $map = $this->get_mapping_value($mapping_specific_fields['specific_fields'], $item_value);

                            $sf[] = $form->getSpecificFields($selected_product_type, $item, $map);
                        }
                        foreach ($sf as $item) {

                            foreach ($item as $name => $item_html) {
                                $specific_html .= $item_html;
                            }
                        }
                    }
		    
                    $data['specific_fields'] = strlen($specific_html) ? $specific_html : null;
                    $data['duplicate'] = strlen($data['specific_fields']) ? true : false;
                }
                $data['memory_peak'] = isset($m["memory_usage"]) ? $m["memory_usage"] : '--';
            }
        }

        return $data;
    }

    public function displayMappingCustomValueForm($id_country) {
        // Check table custom mapping        
        $main_array = $exeption = array();
        $models = $this->amazon_model->get_amazon_model($id_country, $this->params->id_mode, $this->params->id_shop, null, true, true);
        $universe_translate = AmazonSettings::get_universes_translation($this->params->ext);
        $custom_value = $this->amazon_model->get_valid_value_custom($this->params->ext);

        foreach ($models as $model) {

            $type = array();
            $universe = $model['universe'];
            $product_type = isset($model['product_type']) && !empty($model['product_type']) ? $model['product_type'] : '0';

            if (!isset($exeption[$universe])) {
                if(isset(Amazon_Exception::$productTypeException[$universe]))
                    $exeption[$universe] = Amazon_Exception::$productTypeException[$universe];
            }

            $product_type_translate = AmazonSettings::get_product_type_translation($this->params->ext, $universe);
            $fields_translate = AmazonSettings::get_field_translation($this->params->ext, $universe);

            $universe_trans = isset($universe_translate[Amazon_Tools::toKey($universe)]) ? $universe_translate[Amazon_Tools::toKey($universe)] : '';
            $product_type_trans = isset($product_type_translate[Amazon_Tools::toKey($product_type)]) ? $product_type_translate[Amazon_Tools::toKey($product_type)] : '';

            if (isset($model['variation_data'])) {
                array_push($type, $model['variation_data']);
            }

            if (isset($model['recommended_data'])) {
                array_push($type, $model['recommended_data']);
            }

            if (isset($model['specific_fields'])) {
                array_push($type, $model['specific_fields']);
            }

            foreach ($type as $product_attribute) {
                foreach ($product_attribute as $attribute => $value) {

                    $main_array[$universe]['name'] = trim(str_replace("_", " ", preg_replace('/(?<!\ )[A-Z]/', ' $0', $universe)));
                    $main_array[$universe]['translate'] = $universe_trans;
                    $main_array[$universe]['product_type'][$product_type]['name'] = trim(str_replace("_", " ", preg_replace('/(?<!\ )[A-Z]/', ' $0', $product_type)));
                    $main_array[$universe]['product_type'][$product_type]['translate'] = $product_type_trans;

                    $key = (isset($exeption[$universe][$attribute]) && !empty($exeption[$universe][$attribute])) ?
                            Amazon_Tools::toKey($exeption[$universe][$attribute]) : Amazon_Tools::toKey($attribute);

                    // Attribute
                    $main_array[$universe]['product_type'][$product_type]['attribute'][$attribute]['name'] = trim(str_replace("_", " ", preg_replace('/(?<!\ )[A-Z]/', ' $0', $attribute)));
                    // Translate
                    $fields_trans = isset($fields_translate[Amazon_Tools::toKey($key)]) ? $fields_translate[Amazon_Tools::toKey($key)] : '';
                    $main_array[$universe]['product_type'][$product_type]['attribute'][$attribute]['translate'] = $fields_trans;

                    // Values
                    if (isset($custom_value[$universe][$product_type][$attribute]))
                        $main_array[$universe]['product_type'][$product_type]['attribute'][$attribute]['custom_value'] = $custom_value[$universe][$product_type][$attribute];
                }
            }
        }

        return $main_array;
    }

    public function displayMappingForm($id_country) {
        $attribute = $feature = $attr = $feat = array();
        $models = $this->amazon_model->get_amazon_model($id_country, $this->params->id_mode, $this->params->id_shop, null, true, true);

        $validValueCustom = $this->amazon_model->get_valid_value_custom($this->params->ext);      
        $universe_translate = AmazonSettings::get_universes_translation($this->params->ext);
        
        foreach ($models as $model) {

            $universe = $model['universe'];
            $id_model = $model['id_model'];

            $product_type_translate = AmazonSettings::get_product_type_translation($this->params->ext, $model['universe']);
            
            $product_type = isset($model['product_type']) && !empty($model['product_type']) ? $model['product_type'] : '0';

            $universer_trans = isset($universe_translate[Amazon_Tools::toKey($universe)]) ? $universe_translate[Amazon_Tools::toKey($universe)] : $universe;
            $product_type_trans = isset($product_type_translate[Amazon_Tools::toKey($product_type)]) ? $product_type_translate[Amazon_Tools::toKey($product_type)] : $product_type;

            $attribute[$universe][$product_type]['universer_trans'] = $universer_trans;
            $attribute[$universe][$product_type]['product_type_trans'] = $product_type_trans;

            if (isset($validValueCustom[$universe][$product_type]))
                $attribute[$universe][$product_type]['custom_value'] = $validValueCustom[$universe][$product_type];

            if (isset($model['variation_data'])) {
                $variation_data = $this->mapping_valid_values($model['variation_data'], $universe, $id_country, $product_type, $id_model);
                $attribute =  Amazon_Tools::array_merge_recursive_distinct($attribute, $variation_data['attribute']);
                $feature = Amazon_Tools::array_merge_recursive_distinct($feature, $variation_data['feature']);
            }

            if (isset($model['recommended_data'])) {
                $recommended_data = $this->mapping_valid_values($model['recommended_data'], $universe, $id_country, $product_type, $id_model);
                $attribute = Amazon_Tools::array_merge_recursive_distinct($attribute, $recommended_data['attribute']);
                $feature = Amazon_Tools::array_merge_recursive_distinct($feature, $recommended_data['feature']);
            }

            if (isset($model['specific_fields'])) {
                $specific_fields = $this->mapping_valid_values($model['specific_fields'], $universe, $id_country, $product_type, $id_model);
                $attribute = Amazon_Tools::array_merge_recursive_distinct($attribute, $specific_fields['attribute']);
                $feature = Amazon_Tools::array_merge_recursive_distinct($feature, $specific_fields['feature']);
            }
        }

        $main_array = Amazon_Tools::array_merge_recursive_distinct($attribute, $feature);
        ksort($main_array);
        //echo json_encode($main_array);
        //exit;
        return(array('attribute' => $main_array));
    }    

    public function mapping_valid_values($model_data, $universe, $id_country, $product_type, $id_model) {
        
        $attribute = $feature =  array();
	
        foreach ($model_data as $key_variation => $attr_variation) {
            $is_color = ($key_variation == 'Color') ? true : false;

            $validValue = $this->get_valid_value($universe, $key_variation, $product_type, null, $is_color);	   

            if (isset($validValue) && !empty($validValue) || (isset($attr_variation['attr']['unitOfMeasure']) && !empty($attr_variation['attr']['unitOfMeasure']))) {

                $is_number = true;
               
                if ((isset($attr_variation['attribute']) && (int) $attr_variation['attribute'])) {
                    $attribute[$universe][$product_type]['fields'][$key_variation][$attr_variation['attribute']]
                        = $this->get_mapping_attribute($id_country, $attr_variation['attribute'], null, $is_color, $id_model, $is_number, $key_variation, $universe, $product_type);

                    unset($attribute[$universe][$product_type]['fields'][$key_variation]['mapping_attribute_flag']);

                    if (!empty($attribute[$universe][$product_type]['fields'][$key_variation])) {
                        $attribute[$universe][$product_type]['fields'][$key_variation]['valid_value'] = $validValue;
                    } else {
                        unset($attribute[$universe][$product_type]['fields'][$key_variation]);
                    }
                }
                if (isset($attr_variation['feature']) && (int) $attr_variation['feature']) {
                    $feature[$universe][$product_type]['fields'][$key_variation][$attr_variation['feature']]
                        = $this->get_mapping_feature($id_country, $attr_variation['feature'], null, $is_color, $id_model, $is_number, $key_variation, $universe, $product_type);

                    unset($feature[$universe][$product_type]['fields'][$key_variation]['mapping_feature_flag']);

                    if (!empty($feature[$universe][$product_type]['fields'][$key_variation])) {
                        $feature[$universe][$product_type]['fields'][$key_variation]['valid_value'] = $validValue;
                    } else {
                        unset($feature[$universe][$product_type]['fields'][$key_variation]);
                    }
                }
               
            } else {

                if (isset($attr_variation['attr']['unitOfMeasure']) && !empty($attr_variation['attr']['unitOfMeasure'])) {
                    $is_number = true;
                } else {
                    $is_number = false;
                }

                if (isset($attr_variation['attribute']) && (int) $attr_variation['attribute']) {
                    $get_mapping_attribute = $this->get_mapping_attribute($id_country, $attr_variation['attribute'], null, $is_color, $id_model, $is_number, $key_variation, $universe, $product_type);
                     
                    if (isset($get_mapping_attribute['mapped']) && $get_mapping_attribute['mapped']) {
                        $attribute[$universe][$product_type]['fields'][$key_variation][$attr_variation['attribute']] = $get_mapping_attribute;
                    } else {
                        if ($key_variation == 'Color') {
                            $attribute[$universe][$product_type]['fields'][$key_variation]['none_valid_value'] = $get_mapping_attribute;
                        } else {
                            $attribute[$universe][$product_type]['fields'][$key_variation]['none_valid_value'] = $get_mapping_attribute;
                        }
                    }
                }
                if (isset($attr_variation['feature']) && (int) $attr_variation['feature']) {
                    $get_mapping_feature = $this->get_mapping_feature($id_country, $attr_variation['feature'], null, $is_color, $id_model, $is_number, $key_variation, $universe, $product_type);
                    
                    if (isset($get_mapping_feature['mapped']) && $get_mapping_feature['mapped']) {
                        $feature[$universe][$product_type]['fields'][$key_variation][$attr_variation['feature']] = $get_mapping_feature;
                    } else {
                        if ($key_variation == 'Color') {
                            $feature[$universe][$product_type]['fields'][$key_variation]['none_valid_value'] = $get_mapping_feature;
                        } else {
                            $feature[$universe][$product_type]['fields'][$key_variation]['none_valid_value'] = $get_mapping_feature;
                        }
                    }
                }
            }
        }
       
        return array('attribute' => $attribute, 'feature' => $feature);
    }

    public function get_valid_value($product_type, $attribute_field, $product_sub_type, $value = null, $is_color = false) {

        $validValue = $this->valid_value->get_valid_value($product_type, $attribute_field, $product_sub_type);

        $is_valid_value = false;

        if(!isset($value) || empty($value)) {
            $is_valid_value = true;
        }

        if (isset($validValue) && !empty($validValue)) {
            $valid = array();

            foreach ($validValue as $valid_key => $valid_val) {

                if (isset($value) && $value === $valid_val['value']) {
                    $valid[$valid_key]['selected'] = 'selected';
                    $is_valid_value = true;
                }

                $valid[$valid_key]['desc'] = $valid_val['desc'];
                $valid[$valid_key]['value'] = $valid_val['value'];

                if ($is_color) {
                    $valid[$valid_key]['desc'] = $this->_getColorMap($valid_val['desc']);
                }
            }

            if(!$is_valid_value){
                return false;
            }

            return $valid;
        }
    }    

    public function get_mapping_value($mapping_data, $m_data) {
        $map = $mapping_data;

        foreach ($map as $keys => $fields) {
            if (isset($fields['value']))
                foreach ($fields['value'] as $key => $field) {
                    if (isset($m_data['attribute']) && $keys == "attribute") {
                        if ($field['id_attribute_group'] == $m_data['attribute'])
                            $map[$keys]['value'][$key]['selected'] = 1;
                    }
                    else if (isset($m_data['feature']) && $keys == "feature") {
                        if ($field['id_feature'] == $m_data['feature'])
                            $map[$keys]['value'][$key]['selected'] = 1;
                    }
                    else if (!isset($m_data['feature']) && !isset($m_data['attribute'])) {
                        if (is_array($m_data)) {
                            foreach ($m_data as $m_key => $m_value) {
                                if (!is_array($m_value))
                                    $map[$m_key] = $m_value;
                            }
                        }
                    }
                }
        }

        return $map;
    }

    public function get_mapping_group($country, $id_mode, $id_shop, $heading, $split_color = false, $include_feature = false, $default = false) {
        $mapping = array();
        $lang = $this->feedbiz->checkLanguage($this->params->iso_code, $id_shop);

        if(isset($this->mapping_attribute_groups[$lang['id_lang']])){
            $mapping_attribute = $this->mapping_attribute_groups[$lang['id_lang']];
        } else {
            $mapping_attribute = $this->amazon_model->get_mapping_attribute_group($country, $id_shop, $id_mode, $lang['id_lang']);
        }
       
        foreach ($mapping_attribute as $akey => $attribute) {
            if ($default == true) {
                $mapping[$heading]['default']['attribute']['value'][$akey] = $attribute;
                $mapping[$heading]['default']['attribute']['value'][$akey]['id_shop'] = $id_shop;
                $mapping[$heading]['default']['attribute']['value'][$akey]['id_mode'] = $id_mode;
            } else {
                $mapping[$heading]['attribute']['value'][$akey] = $attribute;
                $mapping[$heading]['attribute']['value'][$akey]['id_shop'] = $id_shop;
                $mapping[$heading]['attribute']['value'][$akey]['id_mode'] = $id_mode;
            }

            if ($split_color == true) {
                $mapping[$heading]['Color']['value'][$akey] = $attribute;
                $mapping[$heading]['Color']['value'][$akey]['id_shop'] = $id_shop;
                $mapping[$heading]['Color']['value'][$akey]['id_mode'] = $id_mode;
                $mapping[$heading]['Size']['value'][$akey] = $attribute;
                $mapping[$heading]['Size']['value'][$akey]['id_shop'] = $id_shop;
                $mapping[$heading]['Size']['value'][$akey]['id_mode'] = $id_mode;
            }
        }

        if ($include_feature == true) {

            if(isset($this->mapping_feature_groups[$lang['id_lang']])) {
                $mapping_feature = $this->mapping_feature_groups[$lang['id_lang']];
            } else {
                $mapping_feature = $this->amazon_model->get_mapping_feature_group($country, $id_shop, $id_mode, $lang['id_lang']);
            }

            foreach ($mapping_feature as $fkey => $feature) {
                if ($default == true) {
                    $mapping[$heading]['default']['feature']['value'][$fkey] = $feature;
                    $mapping[$heading]['default']['feature']['value'][$fkey]['id_shop'] = $id_shop;
                    $mapping[$heading]['default']['feature']['value'][$fkey]['id_mode'] = $id_mode;
                } else {
                    $mapping[$heading]['feature']['value'][$fkey] = $feature;
                    $mapping[$heading]['feature']['value'][$fkey]['id_shop'] = $id_shop;
                    $mapping[$heading]['feature']['value'][$fkey]['id_mode'] = $id_mode;
                }
            }
        }
     
        return $mapping;
    }

    public function get_mapping_carrier($country) {
        
        require_once dirname(__FILE__) . '/amazon.display.carrier.php';
        $data = array();
        $mapping_carrier_flag = false;

        //get lang id        
        $carrier = $this->amazon_model->get_mapping_carrier($country, $this->params->id_shop, $this->params->id_mode);
        
        //var_dump($carrier); exit;
        if (isset($carrier) && !empty($carrier)) {
            foreach ($carrier as $k => $mapping_data) {
                if ($mapping_data['type'] == "outgoing" || $mapping_data['type'] == "fba" ) {
                    $key = $mapping_data['id_carrier'];
                } else {
                    $key = str_replace(array(", ", " ", "/"), "_", $mapping_data['mapping']);
                }

                if (isset($mapping_data['selected']) && !empty($mapping_data['selected']) && $mapping_data['selected'] == true)
                    $mapping_carrier_flag = true;

                $data[$mapping_data['type']][$key]['id_carrier'] = $mapping_data['id_carrier'];
                $data[$mapping_data['type']][$key]['id_carrier_ref'] = isset($mapping_data['id_carrier_ref']) ? $mapping_data['id_carrier_ref'] : $mapping_data['id_carrier'];
                $data[$mapping_data['type']][$key]['name'] = $mapping_data['name'];
                $data[$mapping_data['type']][$key]['id_tax'] = $mapping_data['id_tax'];
                $data[$mapping_data['type']][$key]['mapping'] = $mapping_data['mapping'];
                if (isset($mapping_data['other']) && !empty($mapping_data['other']))
                    $data[$mapping_data['type']][$key]['other'] = $mapping_data['other'];
                $data[$mapping_data['type']][$key]['selected'] = $mapping_data['selected'];
            }
            ksort($data);
        }
        $data['mapping_carrier_flag'] = $mapping_carrier_flag;

        return $data;
    }

    public function get_mapping_attribute($country, $id_attribute_group = null, $id_attribute = null, $is_color = false, $id_model = null, $is_number = false, $attribute_field = null, $product_type = null, $product_sub_type = null) {
        
        $data = $is_attribute_color = $is_attribute_color = array();
        $mapping_attribute_flag = false;

        $lang = $this->feedbiz->checkLanguage($this->params->iso_code, $this->params->id_shop);

        $attributes = $this->amazon_model->get_mapping_attribute($country, $this->params->id_shop, $this->params->id_mode, $lang['id_lang'], $id_attribute_group, $id_attribute, $id_model);
        if (isset($attributes) && !empty($attributes)) {
            foreach ($attributes as $mapping_data) {
                if (!isset($this->excludeAttributeFromMapping[$mapping_data['name']])) {
                    $product_type = $mapping_data['product_type'];
                    $product_sub_type = $mapping_data['product_sub_type'];
                    $attribute = $mapping_data['attribute_field'];

                    $data['name'] = $mapping_data['name'];
                    $data['translate'] = $mapping_data['name'];

                    if (isset($attribute_field))
                        $data['field'] = Amazon_Tools::spacify_str($attribute_field);

                    $data['type'] = 'attribute';
                    $data['id_group'] = $mapping_data['id_attribute_group'];
                    $data['is_color'] = $is_color;

                    if ($is_number && is_numeric($mapping_data['attribute'])) {
                        $data['attribute'][$mapping_data['id_attribute']]['valid_value'] = true;
                        $data['attribute'][$mapping_data['id_attribute']]['value'] = $mapping_data['attribute'];
                    } else {

                        if (isset($mapping_data['selected']) && !empty($mapping_data['selected']) && $mapping_data['selected'] == true) {
                            $mapping_attribute_flag = true;
                            $is_attribute_selected = true;
                        }

                        $data['selected'] = isset($is_attribute_selected) ? 1 : 0;
                        $data['attribute'][$mapping_data['id_attribute']]['id_attribute'] = $mapping_data['id_attribute'];
                        $data['attribute'][$mapping_data['id_attribute']]['value'] = $mapping_data['attribute'];
                        if ($is_color)
                            $data['attribute'][$mapping_data['id_attribute']]['value'] = array(
                                'color' => $mapping_data['attribute'],
                                'colorMap' => $this->_getColorMap($mapping_data['attribute'])
                            );

                        if (isset($attribute_field) && ($attribute == $attribute_field)) {
                            if (isset($mapping_data['mapping']) && !empty($mapping_data['mapping']))
                                $data['mapped'] = true;
                            $data['attribute'][$mapping_data['id_attribute']]['mapping'][$product_type][$product_sub_type][$attribute_field] = $mapping_data['mapping'];
                            $data['attribute'][$mapping_data['id_attribute']]['selected'] = $mapping_data['selected'];
                        }
                    }
                }
            }

            ksort($data['attribute']);
        }

        $data['mapping_attribute_flag'] = $mapping_attribute_flag;
       
        return $data;
    }

    public function get_mapping_feature($country, $id_feature=null, $id_feature_value=null, $is_color=null, $id_model=null, $is_number=false, $attribute_field=null, $universe=null, $product_type=null) {
        
        $data = $is_feature_selected = array();
        $mapping_feature_flag = false;
        $lang = $this->feedbiz->checkLanguage($this->params->iso_code, $this->params->id_shop);
        $feature = $this->amazon_model->get_mapping_feature($country, $this->params->id_shop, $this->params->id_mode, $lang['id_lang'], $id_feature, $id_feature_value, $id_model);

        if (isset($feature) && !empty($feature)) {
            foreach ($feature as $feature_data) {
                if (!isset($this->excludeAttributeFromMapping[$feature_data['name']])) {
                    $attribute = $feature_data['attribute_field'];
                    $product_type = $feature_data['product_type'];
                    $product_sub_type = $feature_data['product_sub_type'];

                    $data['name'] = $feature_data['name'];

                    if (isset($attribute_field))
                        $data['field'] = Amazon_Tools::spacify_str($attribute_field);

                    $data['type'] = 'feature';
                    $data['id_group'] = $feature_data['id_feature'];
                    $data['is_color'] = $is_color;

                    if ($is_number && is_numeric($feature_data['value'])) {
                        $data['attribute'][$feature_data['id_feature_value']]['valid_value'] = true;
                        $data['attribute'][$feature_data['id_feature_value']]['value'] = $feature_data['value'];
                    } else {

                        if (isset($feature_data['selected']) && !empty($feature_data['selected']) && $feature_data['selected'] == true) {
                            $mapping_feature_flag = true;
                            $is_feature_selected = true;
                        }
                        $data['selected'] = isset($is_feature_selected) ? 1 : 0;

                        if (isset($feature_data['id_feature_value']) && !empty($feature_data['id_feature_value'])) {
                            $data['attribute'][$feature_data['id_feature_value']]['id_attribute'] = $feature_data['id_feature_value'];
                            $data['attribute'][$feature_data['id_feature_value']]['value'] = $feature_data['value'];

                            if ($is_color) {
                                $data['attribute'][$feature_data['id_feature_value']]['value'] = array(
                                    'color' => $feature_data['value'],
                                    'colorMap' => $this->_getColorMap($feature_data['value'])
                                );
                            }

                            if (isset($attribute_field) && ($attribute == $attribute_field)) {
                                if (isset($feature_data['mapping']) && !empty($feature_data['mapping']))
                                    $data['mapped'] = true;

                                $data['attribute'][$feature_data['id_feature_value']]['mapping'][$product_type][$product_sub_type][$attribute] = $feature_data['mapping'];
                                $data['attribute'][$feature_data['id_feature_value']]['selected'] = $feature_data['selected'];
                            }
                        }
                    }
                }
            }

            if (isset($data['attribute']))
                ksort($data['attribute']);
        }

        if (isset($data) && !empty($data))
            $data['mapping_feature_flag'] = $mapping_feature_flag;

        return $data;
    }

    private function _getStringValueFromArray($array) {
        if (!is_array($array))
            return;

        $html = '';
        foreach ($array as $arr)
            $html .= $arr;

        return $html;
    }

    private function _getColorMap($color) {

        if (is_array($color)) {
            $array_name = $color;
        } else {
            $array_name = array($color);
        }

        $colors = $this->valid_value->get_mapping_colors($array_name);

        foreach ($colors as $k => $c) {
            if (!is_array($color)) {
                if (isset($c->color_name))
                    return $c->color_name;
                else
                    return null;
            }
        }
    }

}
