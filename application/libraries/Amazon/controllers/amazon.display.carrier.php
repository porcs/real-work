<?php

class Amazon_Carrier
{
    const SHIPPING_STANDARD = 'standard';
    const SHIPPING_EXPRESS = 'express';
               
    public static $carrier_fba = array(
        'Standard',
        'Expedited',
        'Priority'
    );
     
    public static function display()
    {
        $carrier = Amazon_Carrier::getShippingMethods();       
        return $carrier;
    }

    public static $carrier_codes = array(        
        'AFL_Fedex' => 'AFL/Fedex',
        'Aramex' => 'Aramex',
        'Blue_Package' => 'Blue Package',
        'BlueDart' => 'BlueDart',
        'Canada_Post' => 'Canada Post',
        'Chronopost' => 'Chronopost',
        'City_Link' => 'City Link',
        'Delhivery' => 'Delhivery',
        'Deutsche_Post' => 'Deutsche Post',
        'DHL_Global_Mail' => 'DHL Global Mail',
        'DHL' => 'DHL',
        'DPD' => 'DPD',
        'DTDC' => 'DTDC',
        'Fastway' => 'Fastway',
        'FedEx_SmartPost' => 'FedEx SmartPost',
        'FEDEX_JP' => 'FEDEX_JP',
        'FedEx' => 'FedEx',
        'First_Flight' => 'First Flight',
        'GLS' => 'GLS',
        'GO' => 'GO!',
        'Hermes_Logistik_Gruppe' => 'Hermes Logistik Gruppe',
        'India_Post' => 'India Post',
        'JP_EXPRESS' => 'JP_EXPRESS',
        'La_Poste' => 'La Poste',
        'Lasership' => 'Lasership',
        'Newgistics' => 'Newgistics',
        'NipponExpress' => 'NipponExpress',
        'NITTSU' => 'NITTSU',
        'OnTrac' => 'OnTrac',
        'OSM' => 'OSM',
        'Overnite_Express' => 'Overnite Express',
        'Parcelforce' => 'Parcelforce',
        'Parcelnet' => 'Parcelnet',
        'Poste_Italiane' => 'Poste Italiane',
        'Professional' => 'Professional',
        'Royal_Mail' => 'Royal Mail',
        'SAGAWA' => 'SAGAWA',
        'SagawaExpress' => 'SagawaExpress',
        'SDA' => 'SDA',
        'Smartmail' => 'Smartmail',
        'Streamlite' => 'Streamlite',
        'Target' => 'Target',
        'TNT' => 'TNT',
        'UPS_Mail_Innovations' => 'UPS Mail Innovations',
        'UPS' => 'UPS',
        'UPSMI' => 'UPSMI', 
        'USPS' => 'USPS',
        'YAMATO' => 'YAMATO',
        'YamatoTransport' => 'YamatoTransport',
        
        /* 15/07/2015 */
        'Yodel' => 'Yodel',
        'Endopack' => 'Endopack',
        'MRW' => 'MRW',
        'Nacex' => 'Nacex',
        'Seur' => 'Seur',
        'Chrono_Express' => 'Chrono Express',
        'Correos' => 'Correos',
        //'Other' => 'Other'
    );
    
    public static function getShippingMethods($type = null) {        
        
        if(!isset($type) || empty($type)) 
            $types = array(self::SHIPPING_STANDARD, self::SHIPPING_EXPRESS);
        else
            $types = array($type);
        
        $shipping_methods = array();
        $datadir = dirname(dirname(__FILE__)) . '/valid_value/carriers/';
       
        foreach ($types as $t) {

            $filename = sprintf('amazon_%s_carriers.ini', $t);
            $file = $datadir . $filename;
            if (file_exists($file) && is_readable($file)) {
                $content_array = file($file);

                if (is_array($content_array) && count($content_array)) {
                    foreach ($content_array as $line) {
                        $line = trim($line);
                        if (!preg_match('/^[A-Za-z0-9\s]*/', $line))
                            continue;

                        $shipping_method = trim(stripslashes($line));
                        $key = str_replace(array(' ', '/', '-', ','), array('_', '_', '_', ''), $shipping_method);
                        $shipping_methods[$key] = $shipping_method;
                    }
                    $shipping_methods = array_unique($shipping_methods);
                }
            }

        }
        
        return($shipping_methods);
    }

}
