<?php

$message_management = array();
//amazon.product.php
$message_management['m000001'] = 'are_inactive';
$message_management['m000002'] = 'Disabled_product';
$message_management['m000003'] = 'Missing_model_in_s_profile_setting';
$message_management['m000004'] = 'Missing_profile_setting';
$message_management['m000005'] = 'Has_No_s';
$message_management['m000006'] = 'Condition_S_not_allow';
$message_management['m000007'] = 'Missing Condition';
$message_management['m000008'] = 'Missing Mapping Condition';
$message_management['m000009'] = 'Missing %s';
$message_management['m000010'] = 's_is_incorrect_EAN_UPC';
$message_management['m000011'] = 'out_of_stock';
$message_management['m000012'] = 'Duplicate_entry_for_s_Previously_used_by_Product_ID_s_This_is_not_allowed';
$message_management['m000013'] = 's_length_must_be_greater_or_equal_to_8_and_less_or_equal_to_16_Currently_value_s';
$message_management['m000014'] = 's_length_must_be_greater_or_equal_to_1_and_less_or_equal_to_40_Currently_value_s';
$message_management['m000015'] = 'Minimum of quantity required to be in stock to export the product: %s Current stock: %s';
$message_management['m000016'] = 'This error occurs when the available inventory you declare for a product is greater than 99,999,999. Current value : %s';
$message_management['m000017'] = '0.00 price (standard or sales) will not be accepted. Please ensure that price of at least equal to or greater than 0.01. Current value : %s';
$message_management['m000018'] = 'Repricing failed due to condition missing';
$message_management['m000019'] = 'Repricing failed due to mapping condition missing';
$message_management['m000020'] = 'Product_S_has_several_profiles_in_serveral_categories';
$message_management['m000021'] = 'Product_S_has_no_category';
$message_management['m000022'] = 'Product_S_is_not_in_selected_categories_D';
$message_management['m000023'] = 'Disabled_product_global';
$message_management['m000024'] = 'The product data provided was insufficient for creating a variation (parent/child) relationship. A required value [%s] is missing for [%s] variation theme  on profile: [%s]';
$message_management['m000025'] = 'The parent SKU don\'t have child SKU';
$message_management['m000026'] = 'Skipped Ad group : %s, are %s. Allow only : %s';
$message_management['m000027'] = 'The product was not allow to create advertise';
$message_management['m000028'] = 'missing ad group selected';