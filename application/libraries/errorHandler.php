<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class errorHandler
{    
    public function set_error_handler() 
    {
        register_shutdown_function( "check_for_fatal" );
        set_error_handler( "log_error" );
        set_exception_handler( "log_exception" );
        ini_set( "display_errors", "off" );
        error_reporting( E_ALL|E_STRICT );
    } 
    
    public function get_error_handle($content)
    {
        ob_start() ;
        ob_get_clean() ;
        $file = '';
        
        $un_seri = unserialize(base64_decode($content));
                
        if(isset($un_seri['class']))
            $file  .= $un_seri['class'] . '/';

        if(isset($un_seri['file']))
            $file  .= $un_seri['file'];

        header("Cache-Control: no-cache, must-revalidate"); 
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); 

        header("Content-type: plain/text");
        header("Content-Disposition: attachment; filename=" . $un_seri['file'] );

        $dir_to_logs = dirname(__FILE__) . "/../logs/";
        $file_to_logs = $dir_to_logs . $file;
        
        echo file_get_contents($file_to_logs);
    }
}

/**
* Error handler, passes flow over the exception logger with new ErrorException.
*/
function log_error( $num, $str, $file, $line, $context = null )
{
    log_exception( new ErrorException( $str, 0, $num, $file, $line ) );
}

/**
* Uncaught exception handler.
*/
function log_exception( Exception $e )
{
    global $econfig;

    if ( $econfig["debug"] == true )
    {
        print "<div style='text-align: center;'>";
        print "<h2 style='color: rgb(190, 50, 50);'>Exception Occured:</h2>";
        print "<table style='width: 800px; display: inline-block;'>";
        print "<tr style='background-color:rgb(230,230,230);'><th style='width: 80px;'>Type</th><td>" . get_class( $e ) . "</td></tr>";
        print "<tr style='background-color:rgb(240,240,240);'><th>Message</th><td>{$e->getMessage()}</td></tr>";
        print "<tr style='background-color:rgb(230,230,230);'><th>File</th><td>{$e->getFile()}</td></tr>";
        print "<tr style='background-color:rgb(240,240,240);'><th>Line</th><td>{$e->getLine()}</td></tr>";
        print "</table></div>";
    }
    else
    {
        $ci =& get_instance();
        $trace = $e->getTrace();
        $class = strtolower($trace[1]['class']);
        $function = ($trace[1]['function']);
        $error_page = base_url() . 'errors/display/' . $function;

        //echo '<pre>' . print_r($error_page, true) .'</pre>';
        $message = "Time: " . date( 'H:i:s' ) . "; Type: " . get_class( $e ) . "; Message: {$e->getMessage()}; File: {$e->getFile()}; Line: {$e->getLine()};";

        $dir_to_logs = dirname(__FILE__) . "/../logs/" . $class . "/";
        $file_to_logs = $dir_to_logs . "log-".date('Y-m-d').".log";

        if(!is_dir($dir_to_logs))
            mkdir($dir_to_logs);

        file_put_contents( $file_to_logs, $message . PHP_EOL, FILE_APPEND );

        //Email to admin
        $site_title = 'Feed.biz';
        $base_url = 'sismiracles.com';
        $image = $base_url . '/assets/images/feedbiz_logob.png'; 
        $logo = '<img src="' . $image . '" alt="Feed.biz" />';
        $email_from = $ci->config->item('admin_email');
        $email_to = $ci->config->item('send_error_email');

        $ci->db->select('user_code');
        $ci->db->from('users');
        $ci->db->where('id', '1');

        $user = $ci->db->get()->row();

        $data = array(
            'class' => $class,
            'file'  => "log-".date('Y-m-d').".log"
        );
        $datas = base64_encode(serialize($data));
        /*$email_data = array(
            'class'          => $class,
            'logo'           => $logo, 
            'file'           => '<a href="' . base_url() . 'errors/getErrorHandler/' . $user->user_code. '/' . $datas . '">' . 
                                'logs/' . $class . '/log-'. date('Y-m-d') . '.log' . 
                                '</a>'
        );*/

        $email_templates = 'templates/email_template/errHandler.tpl' ;
        //$email_templates = 'templates/email/english/errHandler.tpl.php' ;
        //$message = $ci->load->view($email_templates, $email_data, true);
        $this->ci->smarty->assign('class',  $class);
        $this->ci->smarty->assign('logo',  $logo);
        $this->ci->smarty->assign('file',  '<a href="' . base_url() . 'errors/getErrorHandler/' . $user->user_code. '/' . $datas . '">' . 
                                'logs/' . $class . '/log-'. date('Y-m-d') . '.log' . 
                                '</a>');
        $message = $this->ci->smarty->fetch($email_templates);

        require_once dirname(__FILE__) .'/Swift/swift_required.php'; 
        $subject =  $site_title . ' - Error handler on ' . $class;

        try
        { 
            $transport = Swift_SmtpTransport::newInstance();  
            $mailer = Swift_Mailer::newInstance($transport);   
            $swift_message = Swift_Message::newInstance();
            $swift_message->setSubject($subject)
                    ->setFrom( array($email_from => $site_title) )
                    ->setTo($email_to)
                    ->setBody($message, 'text/html'); 
            if ($mailer->send($swift_message) == false)
            {
                echo 'exception_handler_error';
                exit;
            } 

        }
        catch(Exception $ex)
        {
            echo '<pre>' . print_r($ex, true) . '</pre>'; exit;
        }          

    }
    //header('Location: '.$error_page. '');
    //exit();
}

/**
* Checks for a fatal error, work around for set_error_handler not working on fatal errors.
*/
function check_for_fatal()
{
    $error = error_get_last();
    if ( $error["type"] == E_ERROR )
        log_error( $error["type"], $error["message"], $error["file"], $error["line"] );
}