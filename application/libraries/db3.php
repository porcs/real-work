<?php
//for Sqlite3
Class Db
{
    /**
    * @var array list of data to build the query
    */
    public $query = array(
        'select'    =>  array(),
        'from'      => 	'',
        'join'      => 	array(),
        'where'     => 	array(),
        'group'     =>  array(),
        'having'    =>  array(),
        'order'     => 	array(),
        'limit'     => 	array('offset' => 0, 'limits' => 0),
    );
    public $db_status;
    protected $conn_id;
        
    public function __construct($user, $database,$check_exist=true)
    {
        $path = dirname(__FILE__) .  '/../../assets/apps/users/' . $user . '/' . $database. '.db3';
        $this->conn_id = $this->db_connect($path,$check_exist);
          
        if(!isset($this->conn_id) || empty($this->conn_id))
            $this->db_status = false;
    }
    
    public function db_connect($location,$check_exist=true) 
    { 
        if($check_exist && !file_exists($location)) return false;
        $conn_id = new SQLite3($location);
        
        if ( !$conn_id || empty($conn_id) )
            return false;

        return $conn_id;
             
    } 
    
    public function select($fields)
    {
        if (!empty($fields))
        {
            foreach ($fields as $value) 
                $select[] =  $value;

            $query_string = implode(', ', $select) ;

            $this->query['select'][] = $query_string;
        }
        
        return $this;
    }
        
    public function from ($table, $alias = null)
    {
        if (!empty($table))
            $this->query['from'] = $table . ($alias ? ' '.$alias : '');
        return $this;
    }
    
    public function where($restriction, $operation = "=")
    {
        
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'{$value}'";
                
             $where[] =  $key . " $operation " . $value;

        } 
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode(' AND ', $where) ;

//        $query_string = ' WHERE ' . implode(' AND ', $where) ;
        $this->query['where']=array();
        $this->query['where'][] = $query_string;
        
        return $this;
    }
    
    public function where_not_equal($restriction)
    {
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'{$value}'";
                
            $where[] =  $key . ' != ' . $value;
        }

        if(is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_like($restriction, $operation = null)
    {
        $where = array();
        
        if(isset($operation)) {
            $operation = ' OR ';            
        } else {
            $operation = ' AND ';
        }
        
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'%{$value}%'";
                
            $where[] =  $key . ' like ' . $value  ;

        }

       if(is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode($operation, $where) ;
        else
            $query_string = ' WHERE ' . implode($operation, $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_select($restriction, $operation = 'IN')
    {
        foreach ($restriction as $key => $value) 
            $where[] =  $key . ' ' . $operation . ' ' . $value;
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;
        
        return $this;
    }
    
    public function join($join)
    {
        if (!empty($join))
            $this->query['join'][] = $join;

        return $this;
    }
    
    public function leftJoin($table, $alias = null, $on = null)
    {
        return $this->join('LEFT JOIN ' . $table . ' ' . ($alias ? $alias : '') . ($on ? ' ON '. $on : ''));
    }
    
    public function innerJoin($table, $alias = null, $on = null)
    {
        return $this->join('INNER JOIN ' . $table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function outerJoin($table, $alias = null, $on = null)
    {
        return $this->join('OUTER JOIN ' . $table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function orderBy($fields)
    {
        if (!empty($fields))
            $this->query['order'][] = $fields;

        return $this;
    }
    
    public function groupBy($fields)
    {
        if (!empty($fields))
            $this->query['group'][] = $fields;

        return $this;
    }
    
    public function limit($limit, $offset = 0)
    {
        $offset = (int)$offset;
        if ($offset < 0)
            $offset = 0;

        $this->query['limit'] = array(
            'offset' => $offset,
            'limits' => (int)$limit,
        );
        return $this;
    }
    
    public function query_string($arr_query) 
    { 
        $query_string = '';
        
        if(empty($arr_query['select']))
        {
            $query_string .= 'SELECT * FROM ';
        }
        else
        {
            $sel_list = array(); 
            foreach ($arr_query['select'] as $select_txt)
            {
                $select_txt = explode(',',$select_txt);
                foreach($select_txt as $select){
                    if(strpos($select,'.') !== false && strpos($select,'as') === false && strpos($select,'*') === false  ){
                        $select = $select.' as "'.trim($select).'"'; 
                        $sel_list[] = $select ; 
                    }elseif(strpos($select,'.') !== false && strpos($select,'*') !== false  && strpos($select,'as') === false){
                        $alias = explode('.',$select);
                        $alias = trim($alias[0]);
                        $tab_list = array();
                        $tab_list[]  = $arr_query['from'];
                        foreach ($arr_query['join'] as $join)
                        {
                            $tab_list[] = $join;
                        }
                        $table_name = '';
                        

                        foreach($tab_list as $table){
                            $temp = explode(' ',trim($table));
                            $found = false;
                            $temp = str_replace(array(' as ','  '),' ',$temp);
                             
                            foreach($temp as $k => $v){
                                 if($v != '' && trim($v) == $alias){
                                    $found = true;
                                    $table_name=trim($temp[$k-1]);
                                  
                                    break;
                                }
                            }
                            if(!$found){$table_name='';}
                            else{break;}
                        } 
                       
                       $tablesquery = $this->conn_id->query("PRAGMA table_info($table_name);");
                         while ($table = $tablesquery->fetchArray(SQLITE3_ASSOC)) {
                            $sel_list[] = $alias.'.'.$table['name'].' as "'.$alias.'.'.$table['name'].'"' ;
                         }
                    }else{
                        $sel_list[] = $select ; 
                    } 
                }
            }
           
            $query_string .= " SELECT ".implode(', ',$sel_list)." FROM ";
//             echo '<pre>';
//                        echo  ' - '.$query_string.'<br>';
//                         print_r($sel_list);
//                         echo '</pre>';
        }
         
        if(!isset($arr_query['from']) || empty($arr_query['from']))
            return false;
        if(strpos(trim($arr_query['from']),' ')!==false){
            $tb = explode(' ',trim($arr_query['from'])); 
            $tb = $tb[0];
        }else{
            $tb = $arr_query['from'];
        } 
        if(!$this->db_table_exists($tb)) return false; 
        
        $query_string .= $arr_query['from'];
        
        if(isset($arr_query['join']) && !empty($arr_query['join']))
            foreach ($arr_query['join'] as $join)
            {
                $query_string .= ' ' . $join;
            }
        
        if(isset($arr_query['where']) && !empty($arr_query['where']))
            foreach ($arr_query['where'] as $where)
            {
                $query_string .= $where;
            }
        
        if(isset($arr_query['group']) && !empty($arr_query['group']))
        {
            $groups = array();
            foreach ($arr_query['group'] as $group)
                $groups[] =  $group;
            
            $query_string .= ' GROUP BY ' . implode(', ', $groups) ;
        }
        
        if(isset($arr_query['order']) && !empty($arr_query['order']))
        {
            $orders = array();
            foreach ($arr_query['order'] as $order)
                $orders[] =  $order;
            
            $query_string .= ' ORDER BY ' . implode(', ', $orders) ;
        }
        
        if(isset($arr_query['limit']) && $arr_query['limit']['limits'] != 0)
            $query_string .= ' LIMIT ' . $arr_query['limit']['limits'];
        
        if(isset($arr_query['limit']) && isset($arr_query['limit']['offset']) && $arr_query['limit']['offset'] != 0)
            $query_string .= ' OFFSET ' . $arr_query['limit']['offset'] ;
        
        $query_string .= '; ';
        //echo '<pre>' . print_r($query_string, true) . '</pre>';
        
        return $query_string;
    } 
    
    public function insert_string($table, $data)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified))
                {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }
                else
                    $keys[] = "$key";
                
                if($value === '' || is_null($value))
                    $value = 'NULL';
                else if(is_int($value))
                    $value = (int)$value;
                else if(is_float($value))
                    $value = (float)$value;
                else
                    $value = "'" . $this->escape_str($value) . "'";
                 
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }

        $sql = 'INSERT INTO '.$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
        return (string)$sql;
    }
    
    public function replace_string($table, $data)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified))
                {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }
                else
                    $keys[] = "$key";
                
                if($value === '' || is_null($value))
                    $value = 'NULL';
                else if(is_int($value))
                    $value = (int)$value;
                else if(is_float($value))
                    $value = (float)$value;
                else
                    $value = "'" . $this->escape_str($value) . "'";
                 
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }

        $sql = 'REPLACE INTO '.$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
        return (string)$sql;
    }
    
    public function update_string($table, $data, $where = array(), $limit = 0)
    {
        if (!$data)
            return false;

        $sql = 'UPDATE '.$table.' SET ';
        
        foreach ($data as $key => $value) {
            //$sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";             
            if($value === '' || is_null($value)) {
                $sql .= "$key = NULL,";
            } else if(is_int($value)) {
                $sql .= "$key = ".(int)$value.",";
            } else if(is_float($value)) {
                $sql .= "$key = ".(float)$value.",";
            } else {
                $sql .= "$key = '" . $this->escape_str($value) . "',";
            }
        }
        $sql = rtrim($sql, ',');
        
        if (!empty($where))
        {
            $sql .= ' WHERE ';
            
            $sql_where = array();
            foreach ($where as $key => $value)
            {
                $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];
            }
            
            $sql .= implode(' AND ', $sql_where);
        }
        
        if ($limit)
                $sql .= ' LIMIT '.(int)$limit;
        
        return (string)$sql . '; ';
    }
    
    public function delete_string($table, $where = array())
    {
        if (!$table || !$where)
            return false;
        
        $sql = ' WHERE ';
        $sql_where = array();
        
        foreach ($where as $key => $value)
            $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];

        $sql .= implode(' AND ', $sql_where);
        
        $query =  "DELETE FROM " . $table . $sql . " ; ";
        
        return $query;
    }
    
    public function db_num_rows($result) 
    {
        if(!isset($result) || empty($result) || !$result)
            return 0;
        
        if(is_object($result) && get_class($result) == 'SQLite3Result'){ 
            $result = $this->db_sqlite_fetch_all($result); 
            unset($this->query);
            return sizeof($result);
        }else if(is_string($result)){
            $out = $this->db_num_rows_str($result);
            unset($this->query);
            return $out;
        }else{
            unset($this->query);
            return 0 ;
        }
    }
    public function db_num_rows_str($sql) 
    {
        if(!isset($sql) || empty($sql) || !$sql)
            return 0;
        
        $sql_row = "select count(*) from ( $sql ) as cnt";
         $this->newrelic_report($sql_row,__LINE__);
        $out = $this->conn_id->querySingle($sql_row); 
        return  $out ;
    }
    
    // 
    public function db_last_insert_rowid() 
    {
        $exec =  $this->conn_id->lastInsertRowID();
        return $exec;
    }
    
    public function db_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
        $this->newrelic_report($query,__LINE__);
        $sql = $this->query_string($query);  
        $exec = $this->conn_id->query($sql);
        unset($this->query);
        
        return $exec;
    } 
    
    
    public function db_query_fetch($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $sql = $this->query_string($query); 
        
        $this->newrelic_report($sql,__LINE__);
        $exec=$this->db_sqlit_query($sql); 
        $result = $this->db_sqlite_fetch_all($exec); 
        unset($this->query); 
        
        return $result;  
    } 
    
    public function db_sqlite_fetch_all(&$result,$type=SQLITE3_BOTH){
        $i=0;
        $out=array(); 
        while($res = $result->fetchArray($type)){ 
            
            foreach($res as $k=>$v){
                $out[$i][$k] = $v;
            }
            $i++; 
         } 
         return $out;
    }
    
    public function db_query_string_fetch($sql) 
    { 
        if(!isset($sql) || empty($sql) || !$sql)
            return FALSE;
        $this->newrelic_report($sql,__LINE__);
        $exec=$this->conn_id->query($sql);
        if(!$exec) return array();
        $result = $this->db_sqlite_fetch_all($exec,SQLITE3_ASSOC); 
        
        unset($this->query);
        
        return $result;
    } 
    
    public function db_sqlit_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
//        $exec = sqlite_query($this->conn_id, $query); 
        $this->newrelic_report($query,__LINE__);
        $exec = $this->conn_id->query($query);
        return $exec;
    } 
    
    public function db_query_str($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
        
//        $exec = @sqlite_array_query($this->conn_id, $query); 
        $result = null;
        $this->newrelic_report($query,__LINE__);
        $exec=@$this->conn_id->query($query);
        if($exec) 
        $result = $this->db_sqlite_fetch_all($exec); 
        
        return $result ? $result : array();
    } 
    
    public function db_array_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query || empty($this->conn_id) || !$this->conn_id)
            return array();
            
        $sql = $this->query_string($query); 
//        $exec = @sqlite_array_query($this->conn_id, $sql); 
//        debug_print_backtrace();
//        echo $sql.'<br>';
        if(trim($sql) == '') return array();
         $this->newrelic_report($sql,__LINE__);
        $exec=$this->conn_id->query($sql);
        
        $result = $this->db_sqlite_fetch_all($exec); 
        unset($this->query);
        
        return $result ? $result : array() ;
    } 
    
    public function db_exec($query) 
    { 
        if(!$query || empty($query) || !isset($query))
            return FALSE;
        
//        $exec = sqlite_exec($this->conn_id, $query); 
        $query_src = $query;
        
        do{
            $query = strtolower(str_replace('  ',' ',trim($query)));
        } while(strpos($query,'  '));
        if(strpos($query,'insert into')!==false
                || strpos($query,'create table')!==false
                || strpos($query,'begin') !==false
                || strpos($query,'commit') !==false
                || strpos($query,'replace into') !==false
                || strpos($query,'delete from') !==false
                ||(strpos($query,'update')!==false && strpos($query,'set')!==false)){
            $this->newrelic_report($query_src,__LINE__);
            $exec = $this->conn_id->exec($query_src);
            
        }else if((strpos($query,'select')!==false && strpos($query,'from')!==false)){
            $this->newrelic_report($query_src,__LINE__);
            $exec = $this->db_query_str($query_src);  
        }else{
            $this->newrelic_report($query_src,__LINE__);
            $exec = $this->conn_id->exec($query_src);
        }
        if(!$exec)
            return FALSE;
            
        return $exec;
    } 
    
    public function db_close() 
    { 
//        return sqlite_close($this->conn_id);
        return $this->conn_id->close();
    }
    
    public function db_trans_begin()
    {
//        sqlite_exec($this->conn_id, 'BEGIN');
        $this->conn_id->exec('BEGIN');
        return TRUE;
    }
    
    public function db_trans_commit()
    {
//        sqlite_exec($this->conn_id, 'COMMIT');
        $this->conn_id->exec('COMMIT');
        return TRUE;
    }
    
    public function db_table_exists($table = '')
    {
        $sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='".trim($table)."'";
        if(empty($this->conn_id))return false;
//        $exec = @sqlite_array_query($this->conn_id, $sql); 
//        return empty($exec)?false:true;
        return $this->db_num_rows_str($sql); 
    }
    
    public function truncate_table($table)
    {
//        if(!sqlite_exec($this->conn_id, "DELETE FROM " . $table . "; "))
        if(!$this->conn_id->exec("DELETE FROM " . $table . "; "))
            return false;
        
        return true;
    }
    
    public function escape_str($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = $this->escape_str($val);
             return $str;
         }

//         $str = sqlite_escape_string($str);
         $ste = $this->conn_id->escapeString($str);
         return $str;
    }
    public static function escape_string($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = self::escape_string($val);
             return $str;
         }

         $str = sqlite_escape_string($str);
         return $str;
    }
    
    public function newrelic_report($sql,$line=0){
        if(function_exists('newrelic_add_debug')){
            newrelic_add_debug('Issue query('.$line.')',$sql);
        }
    }
}