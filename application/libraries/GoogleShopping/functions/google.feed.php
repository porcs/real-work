<?php

require_once(dirname(__FILE__) . '/../../FeedBiz.php');
require_once(dirname(__FILE__) . '/../classes/google.config.php');
require_once(dirname(__FILE__) . '/../classes/google.configuration.php');
require_once(dirname(__FILE__) . '/../classes/google.product.php');
require_once(dirname(__FILE__) . '/../classes/google.attributes.php');
require_once(dirname(__FILE__) . '/../../../../libraries/forceutf8/src/ForceUTF8/Encoding.php');
use \ForceUTF8\Encoding;

class GoogleFeed {
    
    const LENGTH_TITLE = 150;
    const LENGTH_DESCRIPTION = 5000;
    
    public $ProductUniverse = array();
    
    public function __construct($user, $debug=false, $export_file=false)
    {
	$this->feedbiz = new FeedBiz(array($user));
	
	$this->debug = $debug;
	$this->user = $user;
	$this->marketplace_prefix = _PREFIX_;
	$this->marketplace_name = _MARKETPLACE_NAME_;

        $this->batch_id = uniqid();
        $this->action_type = $export_file ? 'export' : 'feed' ;
    }    
    
    public function getGoogleTypes($iso_code) {
	if(!isset($this->ProductUniversePool[$iso_code])){
            $this->ProductUniverse = GoogleAttributes::getProductUniverse($iso_code);
            $this->ProductUniversePool[$iso_code] = $this->ProductUniverse;
        }else{
             $this->ProductUniverse = $this->ProductUniversePool[$iso_code];
        }

	   
    }
        
    /**
     * The processing function. in order to properly form an array of 
     * combinations (and merging into groups by specified rules from profile) 
     * before creating the XML
     *
     * @param array $product product data
     * @param array $profile profile data(mapping fields)
     * @param string $iso_code language code
     * @return array return array of names or FALSE
     */
    function prepareCombinations($product, $profile, $iso_code/*, $conditions*/) {

        $combinations = array();
        
        if ( empty($product) ) 
	{
		return $combinations;
        }
	
	$this->getGoogleTypes($iso_code);
        
        if (isset($product['combination']) && !empty($product['combination'])) {
            
	    //if product have combinations then prepare by each combination
	    foreach ($product['combination'] as $id_combination => &$comb) {

		if (isset($profile['item_mapping'])) {

                    $custom_label_key = 0;

		    foreach ($profile['item_mapping'] as $key_map_item => $map_item) {
                        
                            $node_val = null;
                            
			    //itterate by item mapping of profile
			    if (!isset($map_item['type']))
				continue;

			    //preparing items by their type
			    switch ($map_item['type']) {

				case "text":
				    if(isset($map_item['value']) && !empty($map_item['value']))
					$comb[$key_map_item] = htmlspecialchars($map_item['value']);
				    break;

				case "array_text":

				    //if type is product_type, get google types names by their id and iso_code
				    if(isset($map_item['value']) && !empty($map_item['value']))
					
					if ($key_map_item == 'product_type') 
					{
					    $types = unserialize($map_item['value']);
					    
					    foreach($types as $type)
					    {
						if(isset($this->ProductUniverse[$type]))
						    $comb[$key_map_item][] = $this->ProductUniverse[$type];
					    }
					}

					break;

				case "item_field":

                                    $key_name = isset($map_item['value']) ? strtolower($map_item['value']) : null ;
                                    
                                    if(isset($map_item['attr_type']) && !empty($map_item['attr_type']))
                                    {
                                        // Attribute Fields
                                        if(strtolower($map_item['attr_type']) == 'attribute')
                                        {
                                            if(isset($profile['group']) && $profile['group'] == 'Yes' ){

                                                if(isset($map_item['field_name']) && in_array($map_item['field_name'], $profile['grouping']))
                                                    $comb['group'] = ucfirst ($map_item['field_name']);
                                            }

                                            if(isset($key_name) && isset($comb['attributes'][$key_name]['value']))
                                            {
                                                if(isset($comb['attributes'][$key_name]['name'][$iso_code]))
                                                    $attr_name = $comb['attributes'][$key_name]['name'][$iso_code];
                                                foreach ($comb['attributes'][$key_name]['value'] as $attr_value)
                                                {
                                                    if(isset($attr_value['name'][$iso_code]))
                                                    {
                                                        if(($key_map_item)=='color'){
                                                            $comb[$key_map_item] = $attr_value['name'][$iso_code];
                                                        }else{
                                                            $comb[$key_map_item] = /*$attr_name .': '.*/$attr_value['name'][$iso_code];
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // Feature Fields
                                        if(strtolower($map_item['attr_type']) == 'feature')
                                        {
                                            if(isset($key_name) && isset($product['feature'][$key_name]['value']))
                                            {
                                                $attr_name = $attr_value = '';
                                                if(isset($product['feature'][$key_name]['name']))
                                                    $attr_name = $product['feature'][$key_name]['name'];

                                                if(isset($product['feature'][$key_name]['value'][$iso_code]))
                                                    $attr_value = $product['feature'][$key_name]['value'][$iso_code];

                                                if(!empty($attr_value))
                                                    $comb[$key_map_item] = /*$attr_name .': '.*/$attr_value;
                                            }
                                        }
                                    } else {
                                        
                                        // Supplier Reference
                                        if ($key_name == 'supplier reference') {
                                            if(isset($product['id_supplier']) && isset($product['supplier'][$product['id_supplier']]['supplier_reference'])) {
                                                $node_val = $product['supplier'][$product['id_supplier']]['supplier_reference'];
                                            }
                                        }

                                        // Category
                                        if ($key_name == 'category') {
                                            if(isset($product['id_category_default']) && isset($product['category'][$product['id_category_default']]['name'][$iso_code])) {
                                                $node_val = $product['category'][$product['id_category_default']]['name'][$iso_code];
                                            }
                                        }

                                        // Manufacturer
                                        if ($key_name == 'manufacturer') {                                            
                                            if(isset($product['id_manufacturer']) && isset($product[$key_name][$product['id_manufacturer']])) {
                                                $node_val = $product[$key_name][$product['id_manufacturer']];
                                            }
                                        }

                                        // Title
                                         if ($key_name == 'meta title') {
                                            if(isset($product['name'][$iso_code])) {
                                                $node_val = $product['name'][$iso_code];
                                            }
                                        }

                                        // Description
                                         if ($key_name == 'meta description') {
                                            if(isset($product['description'][$iso_code])) {
                                                $node_val = $product['description'][$iso_code];
                                            }
                                        }

                                        // weight
                                         if ($key_name == 'weight') {
                                            if(isset($product['weight']['value'])) {
                                                $node_val = $product['weight']['value'] . (isset($product['weight']['unit']) ? ' '.$product['weight']['unit'] : '');
                                            }
                                        }

                                        // sale date
                                         if ($key_name == 'sale date') {
                                            if(isset($product['sale'])) {
                                                foreach ($product['sale'] as $sale) {
                                                    if(isset($sale['date_from']) && isset($sale['date_to'])){
                                                        $node_val = $sale['date_from'] .'/'.$sale['date_to'];
                                                    }
                                                }
                                            }
                                        }
                                        if(isset($product[$key_name])){
                                            if(is_array($product[$key_name])){
                                                if(isset($product[$key_name][$iso_code])){
                                                    $node_val = $product[$key_name][$iso_code];
                                                }
                                            } else {
                                                $node_val = $product[$key_name];
                                            }
                                        }

                                        if (!isset($node_val) || is_array($node_val) || (empty($node_val)))
                                            break;

                                        if (strpos($key_map_item, 'custom_label') !== FALSE){
                                            $key_map_item = 'custom_label_'.$custom_label_key;
                                            $custom_label_key++;
                                        }

                                        $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                                    }
                                    break;
				     
				default:
				    break;
			    }

			    $comb['parent_id'] = $product['reference'];
			}
		    
		}                

		//if shipping is checked then busting all existing carries
		if (isset($profile['shipping'])) {

		    if ($profile['shipping'] == "Yes") {

			foreach ($product['carrier'] as $key => $value) {

			    $comb['shipping_array'][] = array("g:service" => $value['name'], "g:price" => $value['price']);
			}
		    }
		}

		$combinations[$id_combination] = $comb;
	    }
            
	    unset($comb);
           
	} else {

	    //if product doesn't have combinations we doing the same staff but without busting combinations
	    $comb = $product;
            
            $custom_label_key = 0;
	    foreach ($profile['item_mapping'] as $key_map_item => $map_item) {

                    if (!$map_item['type'])
                        continue;
                    
                    //preparing items by their type
                    switch ($map_item['type']) {
                        case "text":
                            $comb[$key_map_item] = htmlspecialchars($map_item['value']);
                            break;
                        case "array_text":
                            //if type is product_type, get google types names by their id and iso_code
                            if ($key_map_item == 'product_type') {
                                $types = unserialize($map_item['value']);
                                $history = array();
                                foreach($types as $type)
                                {
                                    if(isset($this->ProductUniverse[$type]['name']) && !isset($history[$this->ProductUniverse[$type]['id']])){
                                        $comb[$key_map_item][] = $this->ProductUniverse[$type]['name'];
                                        $history[$this->ProductUniverse[$type]['id']] = true;
                                    }
                                }
                            }
                            break;
                        case "item_field":
                           
                            //getting item field value
                            $key_name = isset($map_item['value']) ? strtolower($map_item['value']) : null;

                            if(isset($map_item['attr_type']) && !empty($map_item['attr_type']))
                            {
                                // Feature Fields
                                if(strtolower($map_item['attr_type']) == 'feature')
                                {
                                    if(isset($key_name) && isset($product['feature'][$key_name]['value']))
                                    {
                                        $attr_name = $attr_value = '';
                                        if(isset($product['feature'][$key_name]['name']))
                                            $attr_name = $product['feature'][$key_name]['name'];

                                        if(isset($product['feature'][$key_name]['value'][$iso_code]))
                                            $attr_value = $product['feature'][$key_name]['value'][$iso_code];

                                        if(!empty($attr_value))
                                            $comb[$key_map_item] = /*$attr_name .': '.*/$attr_value;
                                    }
                                }
                            } else {
                                // Supplier Reference
                                if ($key_name == 'supplier reference') {
                                    if(isset($product['id_supplier']) && isset($product['supplier'][$product['id_supplier']]['supplier_reference'])) {
                                        $node_val = $product['supplier'][$product['id_supplier']]['supplier_reference'];
                                    }
                                }

                                // Category
                                if ($key_name == 'category') {
                                    if(isset($product['id_category_default']) && isset($product['category'][$product['id_category_default']]['name'][$iso_code])) {
                                        $node_val = $product['category'][$product['id_category_default']]['name'][$iso_code];
                                    }
                                }
                                
                                // Manufacturer
                                if ($key_name == 'manufacturer') {
                                    if(isset($product['id_manufacturer']) && isset($product[$key_name][$product['id_manufacturer']])) {
                                        $node_val = $product[$key_name][$product['id_manufacturer']];
                                    }
                                }

                                // Title
                                 if ($key_name == 'meta title') {
                                    if(isset($product['name'][$iso_code])) {
                                        $node_val = $product['name'][$iso_code];
                                    }
                                }

                                // Description
                                 if ($key_name == 'meta description') {
                                    if(isset($product['description'][$iso_code])) {
                                        $node_val = $product['description'][$iso_code];
                                    }
                                }

                                // weight
                                 if ($key_name == 'weight') {
                                    if(isset($product['weight']['value'])) {
                                        $node_val = $product['weight']['value'] . (isset($product['weight']['unit']) ? ' '.$product['weight']['unit'] : '');
                                    }
                                }

                                // sale date
                                 if ($key_name == 'sale date') {
                                    if(isset($product['sale'])) {
                                        foreach ($product['sale'] as $sale) {
                                            if(isset($sale['date_from']) && isset($sale['date_to'])){
                                                $node_val = $sale['date_from'] .'/'.$sale['date_to'];
                                            }
                                        }
                                    }
                                }
                                
                                if(isset($product[$key_name])){
                                    if(is_array($product[$key_name])){
                                        if(isset($product[$key_name][$iso_code])){
                                            $node_val = $product[$key_name][$iso_code];
                                        }
                                    } else {
                                        $node_val = $product[$key_name];
                                    }
                                }
                                /*if ($key_name == 'quantity') {
                                    if ($product[$key_name] == 0) {
                                        $node_val = 0;
                                    } else if ($product[$key_name] < $this->out_of_stock) {
                                        $node_val = 0;
                                    } else {
                                        $node_val = 1;
                                    }
                                    $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                                }*/

                                if (!isset($node_val) || is_array($node_val) || (empty($node_val)))
                                    break;

                                if (strpos($key_map_item, 'custom_label') !== FALSE){
                                    $key_map_item = 'custom_label_'.$custom_label_key;
                                    $custom_label_key++;
                                }

                                $comb[$key_map_item] = htmlspecialchars(strip_tags($node_val));
                            }

                            break;

                        default:                            
                            break;
                    }
	    }

	    //if shipping is checked then busting all existing carries
	    if (isset($profile['shipping']))
		if ($profile['shipping'] == "Yes")
		    foreach ($product['carrier'] as $key => $value) {
			$comb['shipping_array'][] = array("g:service" => $value['name'], "g:price" => $value['price']);
		    }
          
	    $combinations[] = $comb;
	}

	return $combinations;
    }

    /**
     * Helper function to group combinations by a certain field 
     *
     * @param array $combinations array of combinations 
     * @param string $field field key for grouping

     * @return array An array of grouped combinations
     */
    function combinationsGroupingByField($combinations, $field) {

        foreach ($combinations as $comb_group => $combinations_array) {
	    
		foreach ($combinations_array as $key => $comb) {
		    
			if (isset($comb[$field])) {
			    $combinations[$comb_group . $comb[$field]][] = $comb; //add new
			    unset($combinations[$comb_group][$key]);
			}
		}
		if (empty($combinations[$comb_group]))
			unset($combinations[$comb_group]);
        }

        return $combinations;
    }

    /**
     * The main function for generating XML
     * 
     * Takes to cycle all items from DB in the specified rules and generates them XML
     * 
     * https://support.google.com/merchants/answer/188494?hl=en
     * @param string $user user ID
     * @param int $id_shop shop ID
     * @param string $lang language id
     * @param string $mode user ID
     * @param array $iso_code language code
     * @param string $profiles array of profiles
     * @return output Show xml on page
     */
    //function getXmlFeedByArray($profile, $user, $id_shop, $mode = null, $lang = null, $iso_code = "fr", $profile_id) {
    //function getXmlFeedByArray($user, $id_shop, $lang, $mode = null/*, $iso_code, $profiles, $conditions, $categories*/) {
    
    public function getXmlFeedByArray($id_shop, $id_country, $id_customer) {
	    
	    $user = $this->user;

	    if(!$this->debug){
                header('Content-Type: application/xml; charset=utf-8');
            }
            
	    $GoogleProduct = new GoogleProduct($user);
	    $GoogleConfiguration = new GoogleConfiguration($user);
	    $UserInfo = new UserConfiguration();
	    $ShopInfo = $UserInfo->getShopInfo($id_customer);

	    $ShopDetail = $ShopLink = '';
	    $ShopName = isset($ShopInfo['name']) ? $ShopInfo['name'] : 'FeedBiz';
           
	    if(isset($ShopInfo['base_url']) && !empty($ShopInfo['base_url']))
	    {
		    $Link = explode('/modules', $ShopInfo['base_url']);
		    $ShopLink = $Link[0];
	    }

	    $xml = new DOMDocument("1.0", "UTF-8"); // Create new DOM document.
	    $rss = $xml->createElement("rss"); //create "RSS" element
	    $rss_node = $xml->appendChild($rss); //add RSS element to XML node
	    $rss_node->setAttribute("version", "2.0"); //set RSS version
	    $rss_node->setAttribute("xmlns:g", "http://base.google.com/ns/1.0"); //set attributes

	    $channel = $xml->createElement("channel"); //create "channel" element under "RSS" element
	    $channel_node = $rss_node->appendChild($channel);

	    //add general elements under "channel" node
	    if(!empty($ShopName))
	    {
		    $channel_node->appendChild($xml->createElement("title", $ShopName));
	    }

	    if(!empty($ShopDetail))
	    {
		    $channel_node->appendChild($xml->createElement("description", $ShopDetail));
	    }

	    //description get from google_shopping_configuration
	    if(!empty($ShopLink))
	    {
		    $channel_node->appendChild($xml->createElement("link", $ShopLink));
	    }

            $channel_node->appendChild($xml->createElement("language", 'en'));
            
	    $params = $GoogleConfiguration->getUserConfigurations($id_shop, $id_country);

	    if(empty($params))
	    {
                    if($this->debug)
                    {
                        echo ('<br/>--------<br/>');
                        echo ('<b>No Google Configuration</b>');
                    } else {
                        // log
                        $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, 0, 0, 0, 0, 0, 0, null, array('error'=>'Missing_parameter'));
                        echo $xml->saveXML();
                    }
		    return;
	    }

	    if(!isset($params['active']) || empty($params['active']) || $params['active'] == 0)
	    {
                    if($this->debug)
                    {
                        echo ('<br/>--------<br/>');
                        echo ('<b>Google are inactive</b>');
                    } else {
                        // log
                        $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, 0, 0, 0, 0, 0, 0, null, array('error'=>'inactive'));
                        echo $xml->saveXML();
                    }
		    return;
	    }

	    // Language
            if(isset($params['iso_code']) && !empty($params['iso_code']))
	    {
		    $iso_code = $params['iso_code'];
	    }

            $lang_default = $this->feedbiz->getLanguageDefault($id_shop);

            if ( !empty($lang_default) )
            {
                    foreach ($lang_default as $lang)
                            $language_code = $lang['iso_code'];
            }
            
	    if(!empty($language_code))
	    {
                    // remove default;
                    $matchingElements = $xml->getElementsByTagName('language');
                    $toDelete = $matchingElements->item(0);
                    $toDelete->parentNode->removeChild($toDelete);

                    // assign new Node Value
                    $channel_node->appendChild($xml->createElement("language", $language_code));  //language
	    }
            
	    if($this->debug)
	    {
		    //var_dump(array('params', $params)); // don't debug it can hack
	    }

	    $products = $GoogleProduct->exportProducts($id_shop, $id_country);

	    if(empty($products))
	    {
                    if($this->debug)
                    {
                        echo ('<br/>--------<br/>');
                        echo ('<b>No products</b>');
                    } else {
                        // log
                        $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, 0, 0, 0, 0, 0, 0, null, array('error'=>'No_products'));
                        echo $xml->saveXML();
                    }
		    return;
	    }
//	    if($this->debug)
//	    {
//		    var_dump(array('products', $products));
//	    }

	    $page = null;
	    $mode = 2;
	    $addtional = array();

	    //get category selected
	    $Categories = new Category($user, 'offers');
	    $addtional['category'] = $Categories->getSelectedCategoryID($id_shop, $mode);	

	    //get exclude Manufacturers
	    $addtional['manufacturer'] = $this->feedbiz->getExcludeManufacturers($user, $id_shop, $mode);

	    //get exclude Suppliers
	    $addtional['supplier'] = $this->feedbiz->getExcludeSuppliers($user, $id_shop, $mode);

	    //get Carriers Selected
	    $addtional['carrier'] = $this->feedbiz->getSelectedCarriers($user, $id_shop, $mode, true);

	    //limit page
	    $addtional['page'] = $page;

	    //get product name
	    $addtional['product_category'] = true;
	    $addtional['product_category_name'] = true;

	    $product_list = new Product($user);
	    $data = $product_list->exportProductsArray($id_shop, $mode, $params['id_lang'], $addtional, $products);
            
	    // if nothing to send
	    if(empty($data))
	    {
                    if($this->debug)
                    {
                        echo ('<br/>--------<br/>');
                        echo ('<b>No products</b>');
                    } else {
                        // log
                        $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, 0, 0, 0, 0, 0, 0, null, array('error'=>'No_products'));
                        echo $xml->saveXML();
                    }
		    return;
	    }	 

	    //get currencies 
	    $currencies = array();
	    foreach ($this->feedbiz->getCurrency($user, $id_shop) as $cur) 
	    {
		    $currencies[mb_strtolower($cur['name'])] = $cur['iso_code'];
	    }

	    // get category
	    $categories = $GoogleConfiguration->getSelectedCategories($id_shop, $id_country);
	    
	    if (empty($categories))
	    {
                    if($this->debug)
                    {
                            echo ('<br/>--------<br/>');
                            echo ('<b>Missing category</b>');
                    } else {
                        // log
                        $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, 0, 0, 0, 0, 0, 0, null, array('error'=>'Missing_category'));
                        echo $xml->saveXML();
                    }
		    return;
	    }
	    
	    // get profile
	    $profiles = $GoogleConfiguration->getProfiles($id_shop, $id_country);		
	    
	    // get model 
	    $models = $GoogleConfiguration->getModels($id_shop, $id_country);
	     
	    // get condition
	    $conditions = $this->feedbiz->getConditionMapping($user, $id_shop, $id_country);

	    // get google fields
	    $GoogleFields = GoogleAttributes::getGoogleFields();

            $skipped_logs = array();
            $no_send = $no_skipped = $no_process = $no_success = $no_error = $no_warning = 0;

            //start busting products
            foreach ($data as $key => $product) 
	    {                 
		    if(!isset($product['id_product']))
		    {
                            // log
                            if(isset($product['reference']))
                                $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $product['reference'], $this->batch_id, 'Missing_id_product');
                            
                            $no_skipped++;
			    continue;
		    }
		    
		    $id_product = (int) $product['id_product'];
		    $category = $categories[$product['id_category_default']];
		    $id_profile = $category['id_profile'];
                    $profile = isset($profiles[$id_profile]) ? $profiles[$id_profile] : null ;

                    $skip_reference =isset($product['reference']) ? $product['reference'] : 'ID:'.$id_product ;
                    
		    if(!isset($profile)) 
		    {	
                            // log
                            $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'Missing_profile_mapping');
                            $no_skipped++;
			    continue;
		    }
		    
		    $model = isset($models[$profile['id_model']]) ? $models[$profile['id_model']] : null;
		    
		    if(!isset($model)) 
		    {	
                            // log
                            $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'Missing_model_setting');
                            $no_skipped++;
			    continue;
		    }
		    
		    $this->out_of_stock = isset($profile['out_of_stock']) ? (int) $profile['out_of_stock'] : 0;
		    
		    $attribute = $this->setProductAttribute($model, $GoogleFields/*, $product, $iso_code*/) ;
		    
		    $profile_data = array_merge($model, $profile);
		     
		    // item mapping
		    if(isset($attribute['item_mapping']))
		    {
			    $profile_data['item_mapping'] = $attribute['item_mapping'];  
		    }
		    
		    // Group
		    if(isset($attribute['group']))
		    {
			    $profile_data['group'] = !empty($attribute['group']) ? 'Yes' : 'No';  
			    $profile_data['grouping'] = $attribute['group'];  
		    }
		    
		    // shipping
		    $profile_data['shipping'] = isset($params['export_shipping']) && $params['export_shipping'] == 1 ? 'Yes': 'No';  

		    // Currency
		    if(isset($product['currency']))
                            foreach ($product['currency'] as $cur)
                                    $currency = mb_strtolower ($cur['name']) ;
				
		    // GTIN field
		    if (isset($profile_data['synchronization_field']) && !empty($profile_data['synchronization_field'])) {
                            switch ($profile_data['synchronization_field']) {
                                    case 'ean13' :
                                            $ps_code = 'ean13';
                                            break;
                                    case 'upc' :
                                            $ps_code = 'upc';
                                            break;
                                    case 'reference' :
                                            $ps_code = 'reference';
                                            break;
                                    case 'both' :
                                            $sync_both_mode = true;
                                            break;
                            }
		    }
                    
		    // Reference
		    $reference = '';
		    if (isset($product['reference']) && !empty($product['reference'])) {
                            $reference = $product['reference'];
		    } else if (isset($product[$ps_code]) && !empty($product[$ps_code])) {
                            $reference = $product[$ps_code];
		    } 

		    if(empty($reference))
		    {
                            // log
                            $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, 'ID:'.$id_product, $this->batch_id, 'Missing_reference');
                            $no_skipped++;
			    continue;
		    }

		    // Manufacturer
		    if (isset($product['id_manufacturer']) && isset($product['manufacturer'][$product['id_manufacturer']])) {
                            $manufacturer = $product['manufacturer'][$product['id_manufacturer']];
		    }

		    // Title Format
		    $product_name = isset($product['name'][$language_code]) ? $product['name'][$language_code] : '';
		    $master_name = trim(mb_substr($product_name, 0, self::LENGTH_TITLE));

		    // Description
		    $descriptions = '';
		    if (isset($product['description'][$language_code]) && !empty($product['description'][$language_code])) {

                            $product_description = $product['description'][$language_code];

                            if (isset($profile_data['html_description']) && $profile_data['html_description']) {
                                    $description = trim($product_description);
                            } else {
                                    $description = MarketplacesTools::clean_strip_tags(($product_description));
                            }
		    }

		    if (isset($description)) {
                        $descriptions = Encoding::fixUTF8(mb_substr( utf8_decode($description)  , 0, self::LENGTH_DESCRIPTION));
		    }
		    
		    // Condition 
		    $mapping_condition = 'new';
                     
		    if (isset($product['id_condition']) && !empty($product['id_condition']) && isset($conditions[$product['id_condition']]['condition_value'])) {
                            $mapping_condition = $conditions[$product['id_condition']]['condition_value'];
		    } 
		    
		    //Images
		    $additional_image_link = isset($profile_data['specific_fields']['additional_image_link']['value']) ? 
					    (bool) $profile_data['specific_fields']['additional_image_link']['value'] : false ;

		    $combinations = $this->prepareCombinations($product, $profile_data, $language_code/*, $conditions*/);

		    foreach ($combinations as $key => $comb) {
                   
                            $id_product_attribute = isset($comb['id_product_attribute']) ? (int) $comb['id_product_attribute'] : 0 ;
                            
                            if (isset($comb['reference']) && !empty($comb['reference'])) {
                                    $reference = $comb['reference'];
                            } else if (isset($comb[$ps_code]) && !empty($comb[$ps_code])) {
                                    $reference = $comb[$ps_code];
                            }

                            $skip_reference = $reference;

                            // Both (EAN/UPC)
                            if (isset($sync_both_mode) && $sync_both_mode) {
                                    $ps_code = 'ean13';
                                    if (isset($comb['ean13']) && !empty($comb['ean13'])) {
                                            $ps_code = 'ean13';
                                    } elseif (isset($comb['upc']) && !empty($comb['upc'])) {
                                            $ps_code = 'upc';
                                    }
                            }

                            if (!isset($comb[$ps_code]) || empty($comb[$ps_code])) {
                                    // log
                                    if(isset($ps_code)){
                                        $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'Missing_'.$ps_code);
                                    }else {
                                        $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'Missing_GTIN_field');
                                    }
                                    
                                    $no_skipped++;
                                    continue;
                            }
                            
                            // availability : check with quantity if instock 'in stock' else 'out of stock'
                            if(isset($comb['quantity'])){
                                    $quantity  = $comb['quantity'];
                            } else {
                                    $quantity  = $product['quantity'];
                            }

                            if(isset($params['export_instock_only']) && $params['export_instock_only'] && $quantity < 1){
                                    // log
                                    $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'out_of_stock');
                                    $no_skipped++;
                                    continue;
                            }


                            // Image
                            $image_link = $additional_image_links = array();

                            if(!isset($comb['images']) && isset($product['image'])){
                                    $comb['images'] = $product['image'];
                            }
                            
                            if(isset($comb['images']))
                            {
                                $first_image = null;
                                foreach ($comb['images'] as $k => $images)
                                { 
                                    if(!isset($images['url']) && !empty($images['image_url'])) {
                                        $images['url'] = $images['image_url'];
                                    }
                                    if(!isset($images['type']) && !empty($images['image_type'])) {
                                        $images['type'] = $images['image_type'];
                                    }

                                    if(isset($images['url']) && !empty($images['url'])) {
                                        if(isset($images['type']) && $images['type'] == 'default')
                                        {
                                            $image_link[] = $images['url'];                                            
                                        } else {
                                            if(empty($first_image) && !empty($images['url'])){
                                                $first_image=$images['url'];
                                            }
                                            if($additional_image_link)
                                            {
                                                $additional_image_links[] = $images['url'];                                             
                                            }
                                        }
                                    } 
                                }
                                if(empty($image_link)){
                                    $image_link[] = $first_image;
                                }
                            } else {
                                // Log
                                $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $product['reference'], $this->batch_id, 'Missing_image');
                                $no_skipped++;
                                continue;
                            }

                            // required Brand                          
                            if(!isset($comb['brand']) || empty($comb['brand']) || !$comb['brand']){
                                    $skipped_logs[] = $GoogleConfiguration->setLogProductsSkip($id_shop, $skip_reference, $this->batch_id, 'missing_brand');
                                    $no_skipped++;
                                    continue;
                            }

                            $item_node = $channel_node->appendChild($xml->createElement("item")); //create a new node called "item"

                            if (isset($comb["parent_id"]) && isset($comb['group']) ){
                                    $item_node->appendChild($xml->createElement("g:item_group_id", $comb["parent_id"]/* . "-" . $id_product_attribute*/));
                            }

                            /** Specific Field **/
                            //
                            // ID
                            $identifier = $id_product . ( ($id_product_attribute > 0) ? '-' . $id_product_attribute : '' ) ;
                            $item_node->appendChild($xml->createElement("g:id", $identifier));

                            // gtin EAN check profile GTIN field : Global Trade Item Numbers (GTINs)
                            // <g:gtin>3234567890126</g:gtin>
                            $item_node->appendChild($xml->createElement("g:gtin", $comb[$ps_code]));

                            // Title with format title
                            $name_attributes = '';
                            if (isset($profile_data['title_format'])) {
                                    switch ($profile['title_format']) {

                                            case 1 : //1 :  Standard Title, Attributes
                                                    $name = $master_name;
                                                    break;

                                            case 2 : //2 :Manufacturer, Title, Attributes
                                                    if (!isset($manufacturer) || empty($manufacturer)) {
                                                        $name = $master_name;
                                                        break;
                                                    }
                                                    $master_name = trim(mb_substr(sprintf('%s - %s', $manufacturer, $product_name), 0, self::LENGTH_TITLE));
                                                    $name = $master_name;
                                                    break;

                                            case 3 : //3: Manufacturer, Title, Reference, Attributes
                                                    if (!isset($manufacturer) || empty($manufacturer)) {
                                                        $name = $master_name;
                                                        break;
                                                    }
                                                    $master_name = trim(mb_substr(sprintf('%s - %s - %s', $manufacturer, $product_name, trim($reference)), 0, self::LENGTH_TITLE));
                                                    $name = $master_name;
                                                    break;
                                            default :
                                                    $name = $master_name;
                                                    break;
                                    }
                            }

                            // Attribute Name
                            if (isset($comb['attributes'])) {

                                // Name
                                $attr_name = array();

                                foreach ($comb['attributes'] as $key => $attribute) {

                                        if (isset($attribute['value'])) {
                                                foreach ($attribute['value'] as $value) {
                                                        if(isset($attribute['name'][$iso_code]) && isset($value['name'][$iso_code])){
                                                                $attr_name[$key] = $attribute['name'][$iso_code] . ' ' . $value['name'][$iso_code];
                                                        } else {
                                                                $attr_name[$key] = current($attribute['name']) . ' ' . current($value['name']);
                                                        }
                                                }
                                        }
                                }

                                if (isset($attr_name) && !empty($attr_name)) {
                                        $name_attributes = ' - ' . implode(", ", $attr_name);
                                }
                            }

                            $title = $name . $name_attributes;
                            $title_tag = $item_node->appendChild($xml->createElement('title'));
                            $title_content = $xml->createCDATASection($title);
                            $title_tag->appendChild($title_content);

                            // description with html description
                            $desc = $item_node->appendChild($xml->createElement('description'));
                            $desc_content = $xml->createCDATASection($descriptions);
                            $desc->appendChild($desc_content);

                            // product link
                            // <g:link>http://www.example.com/clothing/sports/product?id=CLO1029384&amp;src=gshopping&amp;popup=false</g:link>
                            if(isset($comb['link']) && !empty($comb['link'])){
                                    $item_node->appendChild($xml->createElement("g:link", $comb['link']));
                            }

                            // condition
                            if($mapping_condition){
                                    $item_node->appendChild($xml->createElement("g:condition", $mapping_condition));
                            }

                            if($quantity > $this->out_of_stock){
                                    $availability = 'in stock';
                            } else {
                                    $availability = 'out of stock';
                            }

                            if(isset($availability)){
                                    $item_node->appendChild($xml->createElement("g:availability", $availability));
                            }

                            // availability_date : product date <g:availability_date>2014-12-25T13:00-0800</g:availability_date>
                            if(isset($comb['available_date']) && !empty($comb['available_date'])){
                                    $available_date = date('c', MarketplacesTools::ceil_time(strtotime($comb['available_date'])));
                                    $item_node->appendChild($xml->createElement("g:availability_date", $available_date));
                            } else {
                                    if(isset($product['available_date']) && !empty($product['available_date'])){
                                            $available_date = date('c', MarketplacesTools::ceil_time(strtotime($product['available_date'])));
                                            $item_node->appendChild($xml->createElement("g:availability_date", $available_date));
                                    }
                            }

                            // Price
                            //$price = round($comb['price'], 2) . ' ' . (isset($currencies[$currency]) ? $currencies[$currency] : '');
                            if(isset($comb['global_override'])&&$comb['global_override'] && isset($comb['original_price'])){
                                $price = $comb['original_price'];
                            }else{
                                $price = $comb['price'];
                            }
                            $price = round($price, 2) . ' ' . (isset($currencies[$currency]) ? $currencies[$currency] : '');
                            $item_node->appendChild($xml->createElement("g:price", $price));
                            // var_dump($product);
                            // sale <g:sale_price>15.00 USD</g:sale_price>
                            if(isset($product['sale'][$id_product_attribute])){
                                // Sale
                                $sale = $product['sale'][$id_product_attribute];

                                if(!empty($sale) && $price > 0) {

                                    $date_from = isset($sale['date_from']) ? date('c', MarketplacesTools::ceil_time(strtotime($sale['date_from']))) : null;
                                    $date_to = isset($sale['date_to']) ? date('c', MarketplacesTools::ceil_time(strtotime($sale['date_to']))) : null;

                                    if($sale['reduction_type'] == 'amount'){
                                        $sale_price = $price - $sale['reduction'] ;
                                    } else {
                                        $sale_price = ($price - ( $price * ($sale['reduction']/100) ) );
                                    }

                                    if($sale_price > 0)
                                    {
                                        $sale_price = round($sale_price, 2) . ' ' . (isset($currencies[$currency]) ? $currencies[$currency] : '');
                                        $item_node->appendChild($xml->createElement("g:sale_price", $sale_price));

                                        // sale date <g:sale_price_effective_date>2011-03-01T13:00-0800/2011-03-11T15:30-0800</g:sale_price_effective_date>
                                        // ISO 8601
                                        if(isset($date_from) && isset($date_to))
                                        {
                                            if(MarketplacesTools::diff_date($date_from, $date_to) > 0){
                                                $sale_date = $date_from.'/'.$date_to ;
                                                $item_node->appendChild($xml->createElement("g:sale_price_effective_date", $sale_date));
                                            }
                                        }

                                    }
                                }
                            }

                            // image link additional
                            //<g:image_link>http://www.example.com/image1.jpg</g:image_link>
                            //<g:additional_image_link>http://www.example.com/image1.jpg</g:additional_image_link>                            
                            if(isset($image_link)) {
                                    foreach ($image_link as $images) {
                                            $item_node->appendChild($xml->createElement("g:image_link", $images));
                                    }
                            }
                            if(isset($additional_image_links)) {
                                    foreach ($additional_image_links as $images) {
                                            $item_node->appendChild($xml->createElement("g:additional_image_link", $images));
                                    }
                            }
                            // unit_pricing_measure <g:unit_pricing_measure>225g</g:unit_pricing_measure>
                            // (e.g. if price is 3 EUR, ‘unit pricing measure’ is 150ml, and ‘unit pricing base measure’ is 100ml the unit price would be 2 EUR / 100ml).

                            //<g:unit_pricing_base_measure>100g</g:unit_pricing_base_measure>

                            /*shipping*/
                            //<g:shipping_weight>3 kg</g:shipping_weight>
                            if(isset($comb['weight']) && isset($comb['weight']['unit'])) {
                                    $product_weight = floatval($comb['weight']['value']);
                                    $product_unit = strtoupper($comb['weight']['unit']);
                                    $item_node->appendChild($xml->createElement("g:shipping_weight", "$product_weight $product_unit"));
                            }
                            //g:shipping_height>12 in</g:shipping_height>
                            if(isset($comb['height']) && isset($comb['height']['unit'])) {
                                    $product_weight = floatval($comb['height']['value']);
                                    $product_unit = strtoupper($comb['height']['unit']);
                                    $item_node->appendChild($xml->createElement("g:shipping_height", "$product_weight $product_unit"));
                            }
                            //<g:shipping_length>20 in</g:shipping_length>
                            if(isset($comb['depth']) && isset($comb['depth']['unit'])) {
                                    $product_weight = floatval($comb['depth']['value']);
                                    $product_unit = strtoupper($comb['depth']['unit']);
                                    $item_node->appendChild($xml->createElement("g:shipping_length", "$product_weight $product_unit"));
                            }
                            //<g:shipping_width>12 in</g:shipping_width>
                            if(isset($comb['width']) && isset($comb['width']['unit'])) {
                                    $product_weight = floatval($comb['width']['value']);
                                    $product_unit = strtoupper($comb['width']['unit']);
                                    $item_node->appendChild($xml->createElement("g:shipping_width", "$product_weight $product_unit"));
                            }

                            /** Custom Field **/
                            foreach ($profile_data['item_mapping'] as $key_map_item => $map_item) {

                                    if($map_item['tag'] == "g:additional_image_link")
                                            continue;

                                    if($map_item['tag'] == "g:identifier_exists" || $map_item['tag'] == "g:adult" || $map_item['tag'] == "g:is_bundle")
                                            $comb[$key_map_item] = ($comb[$key_map_item] == 1) ? "TRUE" : "FALSE" ;

                                    if (isset($comb[$key_map_item])) {

                                            if (!is_array($comb[$key_map_item])) {

                                                    if (strlen($comb[$key_map_item]) < 50) {
                                                            $item_node->appendChild($xml->createElement($map_item['tag'], $comb[$key_map_item]));
                                                    } else {
                                                            //if lenght of item bigger than 50 - wrap data in CDATA
                                                            $node = $item_node->appendChild($xml->createElement($map_item['tag']));
                                                            $cdata_content = $xml->createCDATASection($comb[$key_map_item]);
                                                            $cdata_node = $node->appendChild($cdata_content);
                                                    }

                                            } else {

                                                    //section for multiplie nodes like product_type
                                                    foreach ($comb[$key_map_item] as $item) {
                                                        if(is_array($item) && isset($item['name'])){
                                                                $node = $item_node->appendChild($xml->createElement($map_item['tag']));
                                                                $cdata_content = $xml->createCDATASection($item['name']);
                                                                $cdata_node = $node->appendChild($cdata_content);
                                                        } else {
                                                                $node = $item_node->appendChild($xml->createElement($map_item['tag']));
                                                                $cdata_content = $xml->createCDATASection($item);
                                                                $cdata_node = $node->appendChild($cdata_content);
                                                        }
                                                    }
                                            }
                                    }
                            }

                            if (isset($comb['shipping_array'])) { //if shipping checkbox enabled

                                    foreach ($comb['shipping_array'] as $shipping) {

                                            $shipping_node = $item_node->appendChild($xml->createElement("shipping")); //create a new node called "shipping

                                            foreach ($shipping as $tag => $ship_value) {
                                                    $shipping_node->appendChild($xml->createElement($tag));
                                                    $shipping_cdata_content = $xml->createCDATASection($ship_value);
                                                    $shipping_node->appendChild($shipping_cdata_content);
                                            }
                                    }
                            }

                            $no_send++;
		    }
            }

            
            // save logs
            if(!empty($skipped_logs)){
                if($this->debug){
                    echo '<br/> ---------------------- Skipped ------------------------- <br/>';
                    echo '<pre>'.print_r($skipped_logs, true).'</pre>';
                    echo '<br/>';
                } else {
                    $GoogleConfiguration->saveLogProductsSkip($skipped_logs);
                }
            }
            
	    // Set HTTP Response Content Type
            if($this->debug){
                    echo '<br/> ---------------------- Xml ------------------------- <br/>';
                    echo '<pre style="background: #ccc; padding: 10px;">'.htmlentities($xml->saveXML()).'</pre>';
                    echo '<br/>';
            } else {
                    header('Content-Type: application/xml; charset=utf-8');
                    echo $xml->saveXML();
            }

            $GoogleConfiguration->setLogs($id_shop, $id_country, $this->batch_id, $this->action_type, $no_send, $no_skipped, $no_process, $no_success, $no_error,$no_warning,null);
    }
    
    public function setProductAttribute($model, $GoogleFields/*, $product, $iso_code*/)
    {
	    $item_mapping = $grouping = array();   
	    
	    foreach ($model as $Fields => $Value)
	    {
		    switch ($Fields) {

			    case 'product_universe' : 
				    if ( isset($GoogleFields[$Fields]) ) 
				    {
					
					    $item_mapping['google_product_category'] = array(
						    'type'      => 'text',
						    'value'     => $Value,
						    'tag'       => $GoogleFields[$Fields]['tag'],
					    );
				    }
			    break;

			    case 'product_type' : 
				    if ( is_array($Value) && isset($GoogleFields[$Fields]) ) 
				    {
					    $item_mapping['product_type'] = array(
						    'type'      => 'array_text',
						    'value'     => serialize($Value),
						    'tag'       => $GoogleFields[$Fields]['tag'],
					    );
				    }
			    break;

			    case 'variation_theme' :
                                if (is_array($Value))
                                {
                                    foreach ($Value as $item_key => $item )
                                    {
                                            if ( isset($GoogleFields[$item_key]) && !empty($item) )
					    {
						if ( !empty($item['grouping']) )
						{
							$grouping[$item_key] = true;
						}

						$item_mapping[$item_key] = array(
							'type'      => $this->get_google_types($item),
							'value'     => $item['value'],
							'tag'       => $GoogleFields[$item_key]['tag'],
							'attr_type' => isset($item['attr_type']) ? $item['attr_type'] : 'attribute',
							'field_name' => $item_key,
						);
					    }                                            
                                    }
                                }
                             break;
                             
			    case 'specific_fields' :				
			    case 'custom_attributes' :

				if(is_array($Value))
				{
                                    $custom_label_key = 0;
				    foreach ( $Value as $item_key => $item ) 
				    {
					    
					    /*if ( $item_key == 'mobile_link' && !empty($item['value']) && isset($GoogleFields[$item_key]['tag'])) 
					    {
						    $item_mapping[$item_key] = array(
							    'type'      => 'item_field',
							    'value'     => 'link',
							    'tag'       => $GoogleFields[$item_key]['tag'],
						    );
					    }*/

					    /*if ( $item_key == 'additional_image_link' && !empty($item['value']) && isset($GoogleFields[$item_key]['tag']) ) 
					    {
						    $item_mapping[$item_key] = array(
							    'type'      => 'item_field',
							    'value'     => 'images',
							    'tag'       => $GoogleFields[$item_key]['tag'],
						    );
					    }*/

					    if ( isset($GoogleFields[$item_key]) && !empty($item) ) 
					    {	
						$item_mapping[$item_key] = array(
							'type'      => $this->get_google_types($item),
							'value'     => $item['value'],
							'tag'       => $GoogleFields[$item_key]['tag'],
							'attr_type' => isset($item['attr_type']) ? $item['attr_type'] : '',
						);
                                              
					    }
				    }
				}
			    break;
			    
			    default :
				
//				var_dump($model);
//				$item_mapping['id'] = array(
//				    'type'      => 'item_field',
//				    'value'     => $product['id_product'],
//				    'tag'       => 'g:id',
//				);
//				$item_mapping['title'] = array(
//				    'type'      => 'item_field',
//				    'value'     => isset($product['name'][$iso_code]) ? $product['name'][$iso_code] : '',
//				    'tag'       => 'title',
//				);
//				
//				
//				$item_mapping['description'] = array(
//				    'type'      => 'item_field',
//				    'value'     => isset($product['name'][$iso_code]) ? $product['name'][$iso_code] : '',
//				    'tag'       => 'description',
//				);
//				$item_mapping['link'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'link',
//				    'tag'       => 'link',
//				);
//				$item_mapping['image_link'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'images',
//				    'tag'       => 'g:image_link',
//				);
//				$item_mapping['condition'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'condition',
//				    'tag'       => 'g:condition',
//				);
//				$item_mapping['price'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'price',
//				    'tag'       => 'g:price',
//				);
//				$item_mapping['availability'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'active',
//				    'tag'       => 'g:availability',
//				);
//				$item_mapping['availability_date'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'available_date',
//				    'tag'       => 'g:availability_date',
//				);
//				$item_mapping['unit_pricing_measure'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'unit_price',
//				    'tag'       => 'g:unit_pricing_measure',
//				);
//				$item_mapping['unit_pricing_base_measure'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'unity',
//				    'tag'       => 'g:unit_pricing_base_measure',
//				);
//				$item_mapping['brand'] = array(
//				    'type'      => 'item_field',
//				    'value'     => 'manufacturer',
//				    'tag'       => 'g:brand',
//				);
			    break;
		    }
		  
//		    $profile['data'][$profile['id_model']] = array(
//                        'name'      => $profile['name'],
//                        'group'     => !empty($grouping) ? 'Yes' : 'No',
//                        'shipping'  => !empty($this->data['configuration']['export_shipping']) ? 'Yes': 'No',
//                    );
//                    if ( !empty($grouping) ) {
//                        $profile['data'][$profile['id_model']]['grouping'] = $grouping;
//                    }
//                    if ( !empty($item_mapping) ) {
//                        $profile['data'][$profile['id_model']]['item_mapping'] = $item_mapping;
//                    }
			    
		    /*if(is_array($Value)) 
		    {
			    foreach ($Value as $Field => $Val)
			    {
				    if(isset($GoogleFields[$Field]))
				     {
					     $value = array();
					     $value['tag'] = $GoogleFields[$Field]['tag'];
					     $value['value'] = $Val;
					     $attribute_data[$Fields] = $value;

				     } 
			    }
			
		    } else {
			
			    if(isset($GoogleFields[$Fields]))
			    {
				    $value = array();
				    $value['tag'] = $GoogleFields[$Fields]['tag'];
				    $value['value'] = $Value;
				    $attribute_data[$Fields] = $value;

			    } 
		    
		    }*/
		
	    }
            
	    return array('item_mapping' => $item_mapping, 'group' => $grouping);
	    
    }
    
    public function get_google_types($value = 'text') 
    {	
	    $type = 'item_field';
	    
	    if(isset($value['hidden']))
	    {
		    switch ($value['hidden']) 
		    {
			    case 'select' :
				    $type = 'item_field';
			    break;

			    case 'select|select' :
				    $type = 'item_field';
			    break;

			    case 'input' :
				    $type = 'text';
			    break;

			    case 'select|input' :
				    $type = 'text';
			    break;

			    default :
				    $type = 'text';
			    break;
		    }
	    }
	    
	    return $type;
    }

}
