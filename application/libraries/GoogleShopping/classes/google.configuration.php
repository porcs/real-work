<?php

require_once(dirname(__FILE__) . '/google.config.php');
require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.configuration.php');

class GoogleConfiguration extends MarketplacesConfiguration
{
    const _PREFIX = _PREFIX_;

    public function __construct($user) {		
	
	    parent::__construct($user, self::_PREFIX);
	    
    }
    
    public function getUserConfigurations($id_shop, $id_country)
    {
	    $mysql_db = new ci_db_connect();  
	    $user_name = $mysql_db->escape_str($this->user);
	   
	    $sql = "SELECT * FROM ".$this->marketplace."_configurations ac 
		    LEFT JOIN users u ON (u.id = ac.id_customer)
		    LEFT JOIN languages l ON (l.id = u.user_default_language)
		    WHERE ac.active = 1 and ac.user_name = '".$user_name."' and ac.id_country = ".(int)$id_country." and ac.id_shop = ".(int)$id_shop." ; ";
            $query = $mysql_db->select_query($sql);
            while($row = $mysql_db->fetch($query)){
                return $row;
            }
    }
    
}