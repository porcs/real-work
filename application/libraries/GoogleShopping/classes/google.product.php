<?php

require_once(dirname(__FILE__) . '/google.config.php');
require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.database.php');

class GoogleProduct extends MarketplacesDatabase
{
    const _PREFIX = _PREFIX_;

    public function __construct($user) {		
	
	    parent::__construct($user, self::_PREFIX);
	    
    }
    
    public function exportProducts($id_shop, $id_country)
    {
	    $products = array();
	    $sql = "SELECT p.id_product AS id_product
		    FROM ".$this->product."product p
		    LEFT JOIN ".$this->prefix."categories_selected acsw ON p.id_category_default = acsw.id_category AND p.id_shop = acsw.id_shop 
		    WHERE p.id_shop = " . (int)$id_shop . " AND acsw.id_country = " . (int)$id_country . " AND acsw.id_shop = " . (int)$id_shop . "  
		    ORDER BY p.id_product ASC ; ";

	    $rows = $this->db->db_array_query($sql);  
	    
	    foreach ($rows as $row)
	    {
		array_push($products, $row['id_product']);
	    }
	    
	    return $products;
    }
    
}