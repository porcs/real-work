<?php

require_once(dirname(__FILE__) . '/google.config.php');
require_once realpath(dirname(__FILE__)).'/../../../../libraries/ci_db_connect.php';

class GoogleAttributes {

    const _PREFIX = 'google';        

    public static function getGoogleProductCategory($lang, $id_universe=null) {
	    
	    $_db = new ci_db_connect(); 
	    $universes = array();
	    
	    $query = "SELECT * FROM google_category_lang WHERE lang = '$lang' ";
            
            if(isset($id_universe) && !empty($id_universe))
                   $query .= " AND id = $id_universe ";

            $results = $_db->select_query($query);
            
	    while($item = $_db->fetch($results)){ 
		    $universes[$item['id']]['id'] = (int) $item['id'];
		    $universes[$item['id']]['text'] = htmlspecialchars($item['text']);
                    //$universe = explode("&gt;", htmlspecialchars($item['text']));
                    //$universes[$item['id']]['universe'] = trim($universe[0]);
	    } 

	    return $universes;
    }
    
    public static function getProductUniverse($iso_code) {	
	    
	    $_db = new ci_db_connect(); 
	    $universes = array();
	    
	    $query = "SELECT * FROM google_category_lang WHERE lang = '$iso_code' ; ";
            $results = $_db->select_query($query);
	    while($item = $_db->fetch($results)){ 
	
		    $universes[$item['id']]['id'] = (int) $item['id'];
		    $universes[$item['id']]['name'] = htmlspecialchars($item['text']);
	    } 
	  
	    return $universes;
    }
    
    public static function getProductType($iso_code, $id_universe=null) {
	    
	    $_db = new ci_db_connect(); 
	    $type = array();
            $universe_parent = '';

            if(isset($id_universe) && !empty($id_universe)){

                $universe = GoogleAttributes::getGoogleProductCategory($iso_code, $id_universe);

                foreach ($universe as $item){
//                    $text = explode("&gt;", $item['text']);
                    $universe_parent = htmlspecialchars_decode(trim($item['text']));
                }
            }

	    $query = "SELECT * FROM google_category_lang WHERE lang = '$iso_code' ";
	    
	    if(!empty($universe_parent))
		    $query .= " AND text LIKE '$universe_parent%' ";
          
	    $results = $_db->select_query($query);
	    while($item = $_db->fetch($results)){ 
	
		    $type[$item['id']]['id'] = (int) $item['id'];
		    $type[$item['id']]['name'] = htmlspecialchars($item['text']);
	    } 

	    return $type;
    }    
    
    public static function getAttributesFields() {	
	
	    $attributes = array();

	    $attributes['specific_fields'] = array(
		    'mpn' => array("required" => 1, "tag" => 'g:mpn'),
		    'brand' => array("required" => 1, "tag" => 'g:brand'),
		    'identifier_exists' => array("required" => 0, "tag" => 'g:identifier_exists', 'values' => array("TRUE", "FALSE")),
		    //'mobile_link' => array("required" => 0, "tag" => 'g:mobile_link', 'values' => array("TRUE", "FALSE")),
		    'additional_image_link' => array("required" => 0, "tag" => 'g:additional_image_link', 'values' => array("TRUE", "FALSE")),
		    'is_bundle' => array("required" => 0, "tag" => 'g:is_bundle', 'values' => array("TRUE", "FALSE")),
		    'adult' => array("required" => 0, "tag" => 'g:adult', 'values' => array("TRUE", "FALSE")),
		    'adwords_redirect' => array("required" => 0, "tag" => 'g:adwords_redirect'),
		    'expiration_date' => array("required" => 0, "tag" => 'g:expiration_date'),
		    'energy_efficiency_class' => array("required" => 0, "tag" => 'g:energy_efficiency_class', 'values' => array("G","F","E","D","C","B","A","A+","A++","A+++")),
		    'shipping_weight' => array("required" => 0, "tag" => 'g:shipping_weight'),
		    'shipping_label' => array("required" => 0, "tag" => 'g:shipping_label'),
		    //'sale_price_effective_date' => array("required" => 0, "tag" => 'g:sale_price_effective_date'), //2011-03-01T13:00-0800/2011-03-11T15:30-0800
		    //'unit_pricing_measure' => array("required" => 0, "tag" => 'g:unit_pricing_measure'),
		    //'unit_pricing_base_measure' => array("required" => 0, "tag" => 'g:unit_pricing_base_measure'),
		    //'gtin' => array("required" => 1, "tag" => 'g:gtin'), // show <input/>
		    //'availability' => array("required" => 1, "tag" => 'g:availability', 'values' => array("in stock", "out of stock", "preorder")),
		    //'availability_date' => array("required" => 0, "tag" => 'g:availability_date'), //2011-03-01T13:00-0800            
		    //'multipack' => array("required" => 0, "tag" => 'g:multipack'),
	    );

	    $attributes['variation_theme'] = array(
		    'color' => array("required" => 0, "tag" => 'g:color'),
		    'gender' => array("required" => 0, "tag" => 'g:gender', 'values' => array("male", "female", "unisex")),
		    'age_group' => array("required" => 0, "tag" => 'g:age_group', 'values' => array("newborn", "infant", "toddler", "kids", "adult")),
		    'material' => array("required" => 0, "tag" => 'g:material'),
		    'pattern' => array("required" => 0, "tag" => 'g:pattern'),
		    'size' => array("required" => 0, "tag" => 'g:size'),
		    'size_type' => array("required" => 0, "tag" => 'g:size_type', 'values' => array("regular", "petite", "plus", "big and tall", "maternity")),
		    'size_system' => array("required" => 0, "tag" => 'g:size_system', 'values' => array("US", "UK", "EU", "DE", "FR", "JP", "CN (China)", "IT", "BR", "MEX", "AU")),
	    );

	    $attributes['custom_attributes'] = array(
		    'custom_label_0' => array("required" => 0, "tag" => 'g:custom_label_0'),
		    'custom_label_1' => array("required" => 0, "tag" => 'g:custom_label_1'),
		    'custom_label_2' => array("required" => 0, "tag" => 'g:custom_label_2'),
		    'custom_label_3' => array("required" => 0, "tag" => 'g:custom_label_3'),
		    'custom_label_4' => array("required" => 0, "tag" => 'g:custom_label_4'),
	    );

	    $attributes['additional_attributes'] =  array(
		    "Supplier Reference", 
		    "Reference", 
		    "Category", 
		    "Manufacturer", 
		    //"Unity",
		    "Meta Title", 
		    "Meta Description", 
		    "Weight",
		    "Sale Date"
	    );

	    return $attributes;
    }        
    
    public static function getGoogleFields() {
	
	    return array(
		    'product_universe'		=> array('tag' => 'g:google_product_category'),
		    'product_type'		=> array('tag' => 'g:product_type'),
		    'google_product_category'   => array('tag' => 'g:google_product_category'),
		    'product_type'              => array('tag' => 'g:product_type'),
		    //'mobile_link'               => array('tag' => 'g:mobile_link'),
		    'additional_image_link'     => array('tag' => 'g:additional_image_link'),
		    'mpn'                       => array('tag' => 'g:mpn'),
		    'brand'                     => array('tag' => 'g:brand'),
		    'identifier_exists'         => array('tag' => 'g:identifier_exists'),
		    'color'                     => array('tag' => 'g:color'),
		    'gender'                    => array('tag' => 'g:gender'),
		    'age_group'                 => array('tag' => 'g:age_group'),
		    'material'                  => array('tag' => 'g:material'),
		    'pattern'                   => array('tag' => 'g:pattern'),
		    'size'                      => array('tag' => 'g:size'),
		    'size_type'                 => array('tag' => 'g:size_type'),
		    'size_system'               => array('tag' => 'g:size_system'),
		    'custom_label_0'            => array('tag' => 'g:custom_label_0'),
		    'custom_label_1'            => array('tag' => 'g:custom_label_1'),
		    'custom_label_2'            => array('tag' => 'g:custom_label_2'),
		    'custom_label_3'            => array('tag' => 'g:custom_label_3'),
		    'custom_label_4'            => array('tag' => 'g:custom_label_4'),
		    'multipack'                 => array('tag' => 'g:multipack'),
		    'is_bundle'                 => array('tag' => 'g:is_bundle'),
		    'adult'                     => array('tag' => 'g:adult'),
		    'adwords_redirect'          => array('tag' => 'g:adwords_redirect'),
		    'expiration_date'           => array('tag' => 'g:expiration_date'),
		    'energy_efficiency_class'   => array('tag' => 'g:energy_efficiency_class'),
	    );
		    //'id'                        => array('tag' => 'g:id'),
		    //'title'                     => array('tag' => 'title'),
		    //'description'               => array('tag' => 'description'),
		    //'link'                      => array('tag' => 'link'),
		    //'image_link'                => array('tag' => 'g:image_link'),
		    //'condition'                 => array('tag' => 'g:condition'),
		    //'gtin'                      => array('tag' => 'g:gtin'),
		    //'availability'              => array('tag' => 'g:availability'),
		    //'availability_date'         => array('tag' => 'g:availability_date'),
		    //'price'                     => array('tag' => 'g:price'),
		    //'sale_price'                => array('tag' => 'g:sale_price'),
		    //'sale_price_effective_date' => array('tag' => 'g:sale_price_effective_date'),
		    //'unit_pricing_measure'      => array('tag' => 'g:unit_pricing_measure'),
		    //'unit_pricing_base_measure' => array('tag' => 'g:unit_pricing_base_measure'),
		    //'shipping_weight'           => array('tag' => 'g:shipping_weight'),
		    //'shipping_length'           => array('tag' => 'g:shipping_length'),
		    //'shipping_width'            => array('tag' => 'g:shipping_width'),
		    //'shipping_height'           => array('tag' => 'g:shipping_height'),
		    //'shipping_label'            => array('tag' => 'g:shipping_label'),
		    //'multipack'			=> array('tag' => 'g:multipack'),
	
    }

}
