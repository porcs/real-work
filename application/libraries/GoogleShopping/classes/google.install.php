<?php

require_once(dirname(__FILE__) . '/google.spider.php');
require_once(dirname(__FILE__) . '/google.config.php');
require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.install.php');
require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

class GoogleInstall extends MarketplacesInstall
{
    const _PREFIX = _PREFIX_;
    public $debug;

    public function __construct($user=null/*, $debug=false*/) {
	    $this->defineAdditionalField();
            if(!empty($user)){
                parent::__construct($user, self::_PREFIX);
            }
//            $this->debug = $debug;
            $this->main_db = new ci_db_connect();

    }

    public function defineAdditionalField(){

	    $this->addtional_main_tbl_field[self::_PREFIX."_configurations"] =
			    array( 'field' => array(
					   'title' => array('type' => 'text'),
					   'description' => array('type' => 'text'),
					   'link' => array('type' => 'text'),
					   'export_shipping' => array('type' => 'tinyint', 'size' => 1),
					   'export_instock_only' => array('type' => 'tinyint', 'size' => 1),
				   ),

			    );

	    $this->addtional_user_tbl_field[self::_PREFIX."_models"] =
			    array(
				 'field' => array(
					    'custom_attributes' => array('type' => 'text'),
				),
			    );

	    $this->addtional_user_tbl_field[self::_PREFIX."_profiles"] =
			    array(
				 'field'=>array(
					    'export_shipping' => array('type' => 'tinyint','size' => 1),
				),
			    );
    }

    public function getAdditionalTableField(){
        return $this->addtional_main_tbl_field;
    }

    public function getCategoryLanguages(){

	    return $this->saveCategoryLanguages(GoogleSpider::getLocales());

    }

    /**
     * Downloads in the database category for all the above language
     *
     * @param array $locales all locales for import in DB
     * @return null
     */
    public function saveCategoryLanguages($locales) {

            if($this->debug){
                    var_dump(array('function'=>__FUNCTION__, 'line'=>__LINE__, 'lable'=>'locales', 'content'=>$locales));
            }

	    $url = "http://www.google.com/basepages/producttype/taxonomy-with-ids.";

            $this->main_db->truncate_table('google_category_lang');
	    $this->main_db->select_query("SET autocommit=0;");

	    foreach ($locales as $lang => $locale) {

		    $handle = GoogleSpider::downloadFile($url . $locale . ".txt");

                    if($this->debug){
                            var_dump(array('function'=>__FUNCTION__, 'line'=>__LINE__, 'lable'=>'downloadFile', 'content'=>$url . $locale . ".txt"));
                    }

		    fgets($handle, 1000);

		    while (($data = fgets($handle, 1000)) !== FALSE) {

			    $m = explode("-", $data);
			    $m = array_map("trim", $m);

			    $insert['lang'] = $lang;
			    $insert['id'] = $m[0];
			    $insert['text'] = isset($m[1])?$m[1]:'';

                            $result = $this->main_db->replace_db('google_category_lang', $insert);

                            if($this->debug){
                                    var_dump(array('function'=>__FUNCTION__, 'line'=>__LINE__, 'lable'=>'sql', 'content'=>$result));
                            }
		    }

		    $meta_data = stream_get_meta_data($handle);
		    $filename = $meta_data["uri"];
		    fclose($handle);

                    if(file_exists($filename))
                            unlink($filename);
	    }

    }

    /**
     * Get taxonomy name by language, for example: category from google
     *
     * @param string $table reference table with taxonomy from DB
     * @param string $iso_code taxonomy language code
     * @return array array of all taxonomy names for this language code
     */
    public function getTaxonomyByLanguage($table, $iso_code) {

	    $this->main_db->select(array('id', "text"));
	    $this->main_db->from($table, "a", true);
	    $arr_where['a.lang'] = $iso_code;
	    $this->main_db->where($arr_where);
	    $tax_arr = $this->main_db->db_array_query($this->main_db->query);

	    $taxonomy = array();
	    
	    foreach ($tax_arr as $tax) {
		    $taxonomy[$tax['id']] = $tax['text'];
	    }

	    return $taxonomy;
    }
}