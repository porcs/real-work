<?php

require_once(dirname(__FILE__) . '/google.install.php');

if(!isset($config['base_url']))
    include(dirname(__FILE__) . '/../../../config/config.php');

// Report all PHP errors
error_reporting(E_ALL);
ini_set('display_errors', '1');

// Set time zone
date_default_timezone_set('UTC'); 

if(!defined('_BASE_URL_'))
    define('_BASE_URL_', $config['base_url']);

if(!defined('_MARKETPLACE_NAME_'))
    define('_MARKETPLACE_NAME_', 'Google Shopping');

define('_PREFIX_', 'google');