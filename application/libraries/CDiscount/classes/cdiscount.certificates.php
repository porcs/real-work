<?php

class CdiscountCertificates
{
    /**
     * Official URL for curl certs
     */
    const URL = 'https://curl.haxx.se/ca/cacert.pem';

    /**
     * CDiscount PEM
     */
    const CDISCOUNT = 'cdiscount.crt';

    /**
     * Has to be in the file
     */
    const PURPOSE = 'Bundle of CA Root Certificates';
    /**
     * File must have a SHA key, unfortunately, we can't parse it
     */
    const HEADER_REGEX = '## SHA[0-9]{1,3}: ([0-9a-f]{40})';
    /**
     * End of file
     */
    const TRAILER_REGEX = '-----END CERTIFICATE-----';

    /**
     * Expiration
     */
    const EXPIRES = 2592000; //1 month

    /**
     * Directory
     */
    const DIR_CERT = 'cert';
    /**
     * Default cert file which has not to be removed
     */
    const FILE_DEFAULT = 'cacert.pem';

    /**
     * Returns null in case of huge trouble
     * @return null|false|string
     */
    public static function getCertificate($debug=false)
    {
        $fileid = floor((time() % (86400 * 365)) / self::EXPIRES); // file is valid till self::EXPIRES

        $cert_dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.self::DIR_CERT;
        $cert_file = $cert_dir.DIRECTORY_SEPARATOR.'cacert.'.$fileid.'.pem';

        if ($debug) {
            echo "<pre>\n";
            printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
            printf('cert_file: %s'."\n", print_r($cert_file, true));
            echo "</pre>";
        }

        if (!is_dir($cert_dir)) {
            mkdir($cert_dir);
            if (!is_dir($cert_dir)) {
                if ($debug) {
                    echo "<pre>\n";
                    printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                    printf('unable to create cert directory: %s'."\n", print_r($cert_dir, true));
                    echo "</pre>";
                }
                return(false);
            }
        }
        if (!is_readable($cert_dir) || (file_exists($cert_file) && !is_readable($cert_file))) {
            if ($debug) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('unable to read cert file: %s'."\n", print_r($cert_file, true));
                echo "</pre>";
            }
            return(false);
        }
        if (!is_writeable($cert_dir)) {
            if ($debug) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('unable to create cert directory: %s'."\n", print_r($cert_dir, true));
                echo "</pre>";
            }
            if (file_exists($cert_file)) {
                return ($cert_file);
            } else {
                $default_certificate = self::getDefaultCertificatePath();

                if (file_exists($default_certificate) && is_readable($default_certificate)) {
                    return($default_certificate);
                } else {
                    return(false);
                }
            }
        }

        self::cleanup();

        if (file_exists($cert_file)) {
            if ($debug) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('return cert file: %s'."\n", print_r($cert_file, true));
                echo "</pre>";
            }

            return($cert_file);
        } else {
            $cert_content = CdiscountTools::file_get_contents(self::URL, false, null, 30, self::getDefaultCertificatePath(), $debug);

            if ($debug) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('download cert file: %s'."\n", print_r($cert_file, true));
                echo "</pre>";
            }

            $purpose = preg_match('/'.self::PURPOSE.'/i', $cert_content);
            $sha_check = preg_match('/'.self::HEADER_REGEX.'/i', $cert_content);
            $eof_check = preg_match('/'.self::TRAILER_REGEX.'/i', $cert_content);

            if ($purpose && $sha_check && $eof_check) {
                if ($debug) {
                    echo "<pre>\n";
                    printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                    printf('download ok: %s'."\n", print_r($cert_file, true));
                    echo "</pre>";
                }

                $cdiscount_pem = $cert_dir.DIRECTORY_SEPARATOR.self::CDISCOUNT;
                $cdiscount_pem_content = null;

                if (is_readable($cert_dir) && file_exists($cdiscount_pem) && is_readable($cdiscount_pem)) {
                    $cdiscount_pem_content = file_get_contents($cdiscount_pem);
                } elseif ($debug) {
                    echo "<pre>\n";
                    printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                    printf('unable to read cert file: %s'."\n", print_r($cdiscount_pem, true));
                    echo "</pre>";
                }
                if (strlen($cdiscount_pem_content)) {
                    $cert_content .= "\n".$cdiscount_pem_content;
                }

                if (file_put_contents($cert_file, $cert_content) !== false) {
                    return($cert_file);
                }
            } else {
                if ($debug) {
                    echo "<pre>\n";
                    printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                    printf('download failed: %s/%s/%s'."\n", $purpose, $sha_check, $eof_check);
                    printf('content: %s'."\n", print_r($cert_content, true));
                    echo "</pre>";
                }
            }
        }
        return(self::getDefaultCertificatePath());
    }

    /**
     * returns null if the user has deleted the file
     * @return null|string
     */
    public static function getDefaultCertificatePath()
    {
        $default_cert_file = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.self::DIR_CERT.DIRECTORY_SEPARATOR.self::FILE_DEFAULT;

        if (file_exists($default_cert_file) && filesize($default_cert_file) && is_readable($default_cert_file)) {
            return($default_cert_file);
        } else {
            return(null);
        }
    }

    /**
     * delete old certificates
     * @return null
     */
    private static function cleanup()
    {
        $now = time();
        $default_certificate_file = self::getDefaultCertificatePath();

        $cert_dir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.self::DIR_CERT;

        if (!is_dir($cert_dir)) {
            return null;
        }

        $files = glob($cert_dir.'*.pem');

        if (!is_array($files) || !count($files)) {
            return null;
        }

        foreach ($files as $file) {
            if (basename($file) == $default_certificate_file) {
                continue;
            }
            if (filemtime($file) < $now - self::EXPIRES) {
                unlink($file);
            }
        }
    }
}
