<?php

require_once(dirname(__FILE__) . '/cdiscount.config.php');
require_once(dirname(__FILE__) . '/../../Marketplaces/classes/marketplaces.install.php');
require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

class CDiscountInstall extends MarketplacesInstall
{
    const _PREFIX = _PREFIX_;
    public $debug;

    public function __construct($user=null/*, $debug=false*/) {
	    $this->defineAdditionalField();
            if(!empty($user)){
                parent::__construct($user, self::_PREFIX);
            }
//            $this->debug = $debug;
            $this->main_db = new ci_db_connect();

    }

    public function defineAdditionalField(){

	    $this->addtional_main_tbl_field[self::_PREFIX."_configurations"] =
			    array( 'field' => array(
					   'token' => array('type' => 'text'),
					   'token_validity' => array('type' => 'text'),
				   ),

			    ); 
    }

    public function getAdditionalTableField(){
        return $this->addtional_main_tbl_field;
    }
 
}