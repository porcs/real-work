<?php

require_once(dirname(__FILE__).'/cdiscount.webservice.php');
require_once(dirname(__FILE__).'/cdiscount.config.php');

class CDiscountModel extends CDiscountWebservice
{
    const MARKETPLACE = _MARKETPLACE_NAME_;

    const MODELS = 1;
    public static $model_file           = null;
    public static $model_list_file      = null;
    private static $_first_dummy_element = 999;
    private static $_xml_model_data      = null;
    private static $type                 = self::MODELS;
    private static $confkey              = 'CDISCOUNT_MODELS';
    private static $tablekey             = 'models';


    public function __construct($username, $password, $prod = true, $debug = false, $demo = false)
    {
        parent::__construct($username, $password, $prod, $debug, $demo);
    }

    public static function toKey($str)
    {
        $str = str_replace(array('-', ',', '.', '/', '+', '.', ':', ';', '>', '<', '?', '(', ')', '!', '"', "'"), array('_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'), $str);
        $str = strtolower(preg_replace('/[^A-Za-z0-9_]/', '', $str));

        return $str;
    }

    public static function setType($type = null)
    {
        switch ($type) {
            case self::MODELS:
                self::$type = self::MODELS;
                self::$confkey = parent::KEY.'_MODELS';
                self::$tablekey = 'models';
                break;
            default:
                die(sprintf('%s/%d: Missing type', basename(__FILE__), __LINE__));
        }
    }

    public static function getAllModelsFileInfo($args = array())
    {
        self::setPath();

        if (!file_exists(self::$model_file)) {
            return (false);
        }

        if (filesize(self::$model_file) < 1024 * 1024) {
            return (false);
        }

        return (filemtime(self::$model_file));
    }

    public static function setPath()
    {
        self::$model_file = dirname(__FILE__).DS.'..'.DS.parent::XML_DIRECTORY.DS.'allModelsList.xml.gz';
        self::$model_list_file = dirname(__FILE__).DS.'..'.DS.parent::XML_DIRECTORY.DS.'allModelsNamesList.xml.gz';
    }

    public static function model2Public($model_id)
    {
        static $cached = array();

        if (isset($cached[$model_id])) {
            return ($cached[$model_id]);
        }

        self::setPath();

        $model2gender = self::modelProperty2Values($model_id, 'Type de public');

        if ($model2gender === null) {
            return (null);
        }
        if (!$model2gender) {
            return (false);
        }

        $cached[$model_id] = array_keys($model2gender);

        return ($cached[$model_id]);
    }

    private static function modelProperty2Values($model_id, $property)
    {
        $properties_values = array();

        self::setPath();
        self::loadModel();

        $result = self::$_xml_model_data;

        if (!$result) {
            return (false);
        }

        $result->registerXPathNamespace('m', 'http://www.cdiscount.com');

        $xPath = '//m:ModelList/m:ProductModel[m:ModelId="'.$model_id.'"]/m:Definition/m:ListProperties/*[a:Key="'.$property.'"]/a:Value/a:string';

        $model_values_xml = $result->xpath($xPath);

        if (!is_array($model_values_xml) || !count($model_values_xml)) {
            return (false);
        }

        foreach ($model_values_xml as $model_value_xml) {
            $properties_values[htmlspecialchars((string)$model_value_xml)] = true;
        }

        return ($properties_values);
    }

    public static function isLoaded()
    {
        return self::$_xml_model_data instanceof SimpleXMLElement;
    }

    public static function supportData()
    {
        $message = null;

        if (!file_exists(self::$model_file)) {
            $message .= 'Missing file: '.self::$model_file.parent::LF;
        } else {
            $message .= sprintf('File: %s', self::$model_file).parent::LF;
            $message .= sprintf('Size: %s', $filesize = filesize(self::$model_file)).parent::LF;
            $message .= sprintf('Date: %s', date('c', filemtime(self::$model_file))).parent::LF;
            $message .= sprintf('Read-only: %s', is_writable(self::$model_file) ? 'No' : 'Yes').parent::LF;
            $message .= sprintf('Permissions: %s', substr(sprintf('%o', fileperms(self::$model_file)), -4, 4)).parent::LF;
        }
        return($message);
    }

    public static function loadModel()
    {
        if (self::isLoaded()) {
            return (true);
        }

        if (Configuration::get(parent::KEY.'_MININUM')) {
            return (false);
        }

        if (!file_exists(self::$model_file)) {
            self::$_xml_model_data = null;

            return (false);
        }

        if (filesize(self::$model_file) < 1024 * 1024) {
            self::$_xml_model_data = null;
            displayError(sprintf('%s(#%d): FATAL - file is incomplete - please contact support'."\n", basename(__FILE__), __LINE__));

            return (false);
        }

        if (self::$_xml_model_data == null) {
            if (defined('LIBXML_PARSEHUGE')) {
                $flags = LIBXML_ERR_NONE | LIBXML_PARSEHUGE;
            } else {
                $flags = LIBXML_ERR_NONE;
            }

            if ((self::$_xml_model_data = simplexml_load_file('compress.zlib://'.self::$model_file, null, $flags)) == false) {
                self::$_xml_model_data = null;
                displayError(sprintf('%s(#%d): FATAL - unable to load model XML file - please contact support'."\n", basename(__FILE__), __LINE__));

                return (false);
            }

            if (!self::$_xml_model_data instanceof SimpleXMLElement) {
                self::$_xml_model_data = null;
                displayError(sprintf('%s(#%d): FATAL - unable to load model XML file - please contact support'."\n", basename(__FILE__), __LINE__));

                return (false);
            }
        }

        return (true);
    }

    public static function model2Gender($model_id)
    {
        static $cached = array();

        if (isset($cached[$model_id])) {
            return ($cached[$model_id]);
        }

        self::setPath();

        $model2gender = self::modelProperty2Values($model_id, 'Genre');

        if ($model2gender === null) {
            return (null);
        }
        if (!$model2gender) {
            return (false);
        }

        $cached[$model_id] = array_keys($model2gender);

        return ($cached[$model_id]);
    }

    /**
     *
     * @param type $args
     * @return boolean
     */
    public static function getAllModelsListData($args = array())
    {
        self::setPath();

        if (Configuration::get(parent::KEY.'_MININUM')) {
            return (false);
        }

        //Check if file already exists
        $dir = dirname(self::$model_file);

        if (!is_dir($dir)) {
            if (!mkdir($dir)) {
                return (false);
            }
        }

        if (!is_writable($dir)) {
            @chmod($dir, parent::PERMISSIONS_DIRECTORY);
        }

        $output_file = self::$model_file;

        if (!file_exists($output_file)) {
            return (false);
        }

        if ((time() - filectime($output_file)) < ((60 * 60 * 24 * 30) + rand(86400, 86400 * 7))) {
            return (CDiscountTools::file_get_contents('compress.zlib://'.$output_file));
        }

        return false;
    }

    /**
     *
     * @param type $args
     * @param type $format
     * @param type $source
     * @return type
     */
    public static function getAllModelListNames($args = array(), $format = null, $source = null)
    {
        static $names_list = null;

        if ($names_list === null) {
            self::setPath();

            if ($source != null && is_array($source)) {
                $names = $source;
                foreach ($names as $key => $name) {
                    //$names_list[$key] = self::getStringFormatted($name,$format);
                    $names_list[$key] = $name;
                }
            } else {
                self::loadModel();

                $result = self::$_xml_model_data;

                if (!$result) {
                    return (false);
                }

                $result->registerXPathNamespace('xmlns', 'http://www.cdiscount.com');
                $xPath = '//xmlns:ModelList/xmlns:ProductModel';

                $modelTreeResult = $result->xpath($xPath);

                if (is_array($modelTreeResult) && count($modelTreeResult)) {
                    foreach ($modelTreeResult as $modelResult) {
                        $model_name = (string)$modelResult->Name;
                        if (!preg_match('/.*_MK$/', $model_name)) {
                            continue;
                        }
                        $model_id = (string)$modelResult->ModelId;
                        $model_name = (string)$modelResult->Name;
                        $names_list[$model_id] = $model_name;
                    }
                }
            }
        }

        return ($names_list);
    }

    /**
     * Get Models as an array containing needed informarmation for its representation as HTML field
     * @param type $args
     * @return type
     */
    public static function getAllModelListFields($args = array())
    {
        static $fields = null;
        
        if ($fields === null) {
            $fields = array(self::$_first_dummy_element => null);
            $model_name = null;
            $model_list = array();
            $models = array();

            if (isset($args['model_name'])) {
                $model_name = $args['model_name'];
            }

            if (isset($args['model_list'])) {
                $model_list = $args['model_list'];
            }

            if ($model_name != null) {
                //
                $models = array(self::getModel($model_name));
            } elseif ($model_list != null) {
                foreach ($model_list as $model) {
                    $models[] = self::getModel($model);
                }
            }
            if (!is_array($models) || !count($models)) {
                return (false);
            }

            foreach ($models as $model_dom) {
                if (!$model_dom instanceof DOMDocument) {
                    continue;
                }

                $field_properties = $model_dom->getElementsByTagName('KeyValueOfstringArrayOfstringty7Ep6D1');
                $product_model_name = $model_dom->getElementsByTagName('Name')->item(0)->textContent;
                $product_model_id = $model_dom->getElementsByTagName('ModelId')->item(0)->textContent;
                $model = array();
                foreach ($field_properties as $field_property) {
                    $field_xml_name = $field_property->getElementsByTagName('Key')->item(0)->textContent;
                    $field_field = self::getStringFormatted($field_xml_name, 'HtmlFormField');
                    $field_id = self::getStringFormatted($field_xml_name, 'HtmlFieldName');
                    $field_name = self::getStringFormatted($field_xml_name, 'DisplayName');
                    $field_mandatory = self::isMandatoryField($field_xml_name);
                    $field_display = self::isDisplayedField($field_xml_name);
                    $field_type = self::getFieldType($field_xml_name);
                    $field_size = '60';

                    if ($field_type) {
                        $model[] = array(
                        'xml_tag' => $field_xml_name,
                        'html_id' => $field_id,
                        'name' => $field_field,
                        'mandatory' => $field_mandatory,
                        'display' => $field_display,
                        'size' => $field_size,
                        'type' => $field_type
                        );
                    } else {
                        $model[] = array(
                        'xml_tag' => $field_xml_name,
                        'html_id' => $field_id,
                        'field' => $field_field,
                        'name' => $field_field,
                        'mandatory' => $field_mandatory,
                        'display' => $field_display,
                        'size' => $field_size
                        );
                    }
                }

                $fields[$product_model_id] = $model;
            }

            unset($fields[self::$_first_dummy_element]);
        }

        return $fields;
    }

    /**
     * Returns model variation details
     * @param type $model_name
     * @return array
     */
    public static function getModelVariationValues($model_name)
    {
        $attributes = array();

        self::setPath();
        self::loadModel();

        $simpleXml = self::$_xml_model_data;

        if (!$simpleXml) {
            return (false);
        }

        $simpleXml->registerXPathNamespace('m', 'http://www.cdiscount.com');
        $simpleXml->registerXPathNamespace('a', 'http://schemas.microsoft.com/2003/10/Serialization/Arrays');

        $variations = $simpleXml->xpath('//m:ProductModel/m:Name[. = "'.$model_name.'"]/../m:VariationValueList/m:VariationDescription');

        if (is_array($variations) && count($variations)) {
            foreach ($variations as $variation) {
                if (!$variation instanceof SimpleXMLElement) {
                    continue;
                }

                if ((string)$variation->VariantValueId && strlen((string)$variation->VariantValueDescription)) {
                    $variation_key = self::toKey((string)$variation->VariantValueDescription);
                    $attributes[$variation_key] = trim((string)$variation->VariantValueDescription);
                }
            }
        }

        return $attributes;
    }

    /**
     * Returns model variation details
     * @param type $model_name
     * @return array
     */
    public static function getModelVariationFlag($model_name)
    {
        static $variations_flags = array();

        if (array_key_exists($model_name, $variations_flags) && count($variations_flags[$model_name])) {
            return ($variations_flags[$model_name]);
        }

        self::setPath();
        self::loadModel();

        $simpleXml = self::$_xml_model_data;

        if (!$simpleXml instanceof SimpleXMLElement) {
            return (false);
        }

        $simpleXml->registerXPathNamespace('m', 'http://www.cdiscount.com');

        $variations = $simpleXml->xpath('//m:ProductModel/m:Name[. = "'.$model_name.'"]/parent::*/m:VariationValueList[1]/m:VariationDescription[1]');

        return $variations_flags[$model_name] = is_array($variations) && count($variations);
    }

    /**
     * Returns model values
     * @param type $model_name
     * @return array
     */
    public static function getModelValues($model_name)
    {
        static $attributes = array();

        if (array_key_exists($model_name, $attributes) && count($attributes[$model_name])) {
            return ($attributes[$model_name]);
        }

        self::setPath();
        self::loadModel();

        $simpleXml = self::$_xml_model_data;

        if (!$simpleXml) {
            return (false);
        }

        $simpleXml->registerXPathNamespace('m', 'http://www.cdiscount.com');
        $simpleXml->registerXPathNamespace('a', 'http://schemas.microsoft.com/2003/10/Serialization/Arrays');

        $definitions = $simpleXml->xpath('//m:ProductModel/m:Name[. = "'.$model_name.'"]/../m:Definition/m:ListProperties/a:*');

        if (is_array($definitions) && count($definitions)) {
            foreach ($definitions as $definition) {
                $strip_namespace = str_replace('<a:', '<', $definition->saveXML());
                $strip_namespace = str_replace('</a:', '</', $strip_namespace);

                if (!strlen($strip_namespace)) {
                    continue;
                }

                $new_element = simplexml_load_string($strip_namespace);

                if ($new_element instanceof SimpleXMLElement) {
                    if ($new_element->Value->string) {
                        $attribute_name = (string)$new_element->Key;
                        $attribute_name_key = self::toKey((string)$new_element->Key);

                        $attributes[$model_name][$attribute_name_key] = array();
                        $attributes[$model_name][$attribute_name_key]['title'] = $attribute_name;
                        $attributes[$model_name][$attribute_name_key]['values'] = array();

                        foreach ($new_element->Value->string as $attribute) {
                            $attribute_item = (string)$attribute;
                            $index = self::toKey($attribute_item);

                            $attributes[$model_name][$attribute_name_key]['values'][$index] = $attribute_item;
                        }
                    }
                }
            }

            return $attributes[$model_name];
        }

        return (false);
    }


    /**
     * Returns model structure
     * @param type $model_name
     * @return \DOMDocument|boolean
     */
    public static function getModel($model_name)
    {
        self::setPath();
        self::loadModel();

        $simpleXml = self::$_xml_model_data;

        if (!$simpleXml) {
            return (false);
        }

        $simpleXml->registerXPathNamespace('m', 'http://www.cdiscount.com');

        $productModel = $simpleXml->xpath('//m:ProductModel/m:Name[. = "'.$model_name.'"]/parent::*');

        if ($productModel && count($productModel) > 0) {
            $productXmlStructure = (string)$productModel[0]->ProductXmlStructure;

            if (empty($productXmlStructure)) {
                $productModel[0]->ProductXmlStructure = null;
            }
            //Creates DOM instance of XML Structure
            $output = new DOMDocument();
            $output->loadXML($productModel[0]->asXML());
            $output->formatOutput = true;
            $structureElement = $output->getElementsByTagName('ProductXmlStructure')->item(0);

            //Finds Node to be replaced (ProductXmlStructure)
            $dom = new DOMDocument();
            $dom->loadXML($productXmlStructure);
            $dom->formatOutput = true;
            $nodeList = $dom->getElementsByTagName('Product');

            foreach ($nodeList as $node) {
                $importedNode = $output->importNode($node->cloneNode(true), true);
                $structureElement->appendChild($importedNode);
            }

            return $output;
        }

        return false;
    }

    /**
     * Returns a String formatted
     * @param type $str
     * @param type $format
     * @return type
     */
    public static function getStringFormatted($str, $format = null)
    {
        if ($format == null) {
            return $str;
        }

        if ($format == 'CSV') {
            $str = str_ireplace('-', '_', $str);
            $str = strtolower(str_ireplace('  ', ' ', $str));
            $str = strtolower($str.'.csv');
            $str = preg_replace('/\s+/', '', $str);
        } elseif ($format == 'HtmlFieldName') {
            //
            $str = str_replace('&', ':$:', htmlentities($str, null, 'UTF-8'));
        } elseif ($format == 'HtmlFormField') {
            $str = htmlentities($str, null, 'UTF-8');
        } elseif ($format == 'decoded') {
            $str = html_entity_decode(str_replace(':$:', '&', $str), ENT_COMPAT, 'UTF-8');
        }

        return $str;
    }

    private static function isMandatoryField($field_name = null)
    {
        if ($field_name == null) {
            return false;
        }

        return false;
    }

    private static function isDisplayedField($field_name = null)
    {
        if (in_array($field_name, array('Genre', 'Type de public', 'Marque'))) {
            return false;
        }

        return true;
    }

    private static function getFieldType($field_name = null)
    {
        if ($field_name == 'Genre') {
            return 'gender';
        }

        return false;
    }

    /**
     * Save All Model List to an XML File allModelsNamesList.xml
     * @param type $data
     * @return boolean
     */
    public static function saveAllModelList($data)
    {
        self::setPath(); // self::$model_file

        if (file_exists(self::$model_file) && !($data)) {
            return (false);
        }

        if (!is_writable(self::$model_file)) {
            @chmod(self::$model_file, parent::PERMISSIONS_STD_FILE);
        }

        if ($data instanceof SimpleXMLElement) {
            $simple = dom_import_simplexml($data);
            $dom = new DOMDocument();
            $dom_element = $dom->importNode($simple, true);
            $dom_element = $dom->appendChild($dom_element);
            $dom->formatOutput = true;
            if (!$dom->save('compress.zlib://'.self::$model_file)) {
                return false;
            }
            $dom = null;

            $data->registerXPathNamespace('m', 'http://www.cdiscount.com');
            $xPath = '//s:Envelope/s:Body/m:GetAllModelListResponse/m:GetAllModelListResult/m:ModelList/m:ProductModel/m:Name';
            $only_models_names = $data->xpath($xPath);

            $dom_models_names = new DOMDocument();
            $namesListElement = $dom_models_names->createElement('NamesList');
            foreach ($only_models_names as $model_name) {
                $namesListElement->appendChild($dom_models_names->importNode(dom_import_simplexml($model_name), true));
            }

            $dom_models_names->appendChild($namesListElement);
            $dom_models_names->formatOutput = true;

            if (!$dom_models_names->save('compress.zlib://'.self::$model_list_file)) {
                return false;
            }

            //Nulllify variables
            $dom = null;
            $dom_models_names = null;
            $only_models_names = null;
            $data = null;

            //$dom=NULL;
            return (true);
        }

        return ($data);
    }

    /**
     * Extracts all modules list from Web Service response
     * @param type $xml_response
     */
    public static function parseAllModelList($xml_response)
    {
        $pos = stripos($xml_response, '<s:Envelope');

        if ($pos) {
            $xml_response = substr($xml_response, $pos); //TODO: DO NOT SUBSTITUTE BY substr !!!

            $pos = stripos($xml_response, '</s:Envelope');

            if ($pos != false) {
                $xml = simplexml_load_string($xml_response);

                if ($xml instanceof SimpleXMLElement) {
                    $xml->registerXPathNamespace('m', 'http://www.cdiscount.com');
                    $xPath = '//s:Envelope/s:Body/m:GetAllModelListResponse/m:GetAllModelListResult/m:ModelList';
                    $productModels = $xml->xpath($xPath);

                    if ($productModels[0] instanceof SimpleXMLElement && isset($productModels[0]->ProductModel)) {
                        return $productModels[0];
                    } else {
                        self::printErrorMessage($xml);
                    }
                } else {
                    printf('%s/%d: API returned unexpected content: %s', basename(__FILE__), __LINE__, $xml_response);
                }
            } else {
                printf('%s/%d: API returned unexpected content: %s', basename(__FILE__), __LINE__, $xml_response);
            }
        } else {
            printf('%s/%d: API returned unexpected content: %s', basename(__FILE__), __LINE__, $xml_response);
        }

        return false;
    }

    public static function printErrorMessage($xml)
    {
        $pass = false;
        $xml->registerXPathNamespace('mns', 'http://schemas.datacontract.org/2004/07/Cdiscount.Framework.Core.Communication.Messages');

        $xPath = '//*[mns:ErrorMessage or mns:ErrorList]';
        $xpath_result = $xml->xpath($xPath);

        if (is_array($xpath_result) && count($xpath_result)) {
            echo "<pre>";
            foreach ($xpath_result as $item) {
                $pass = true;
                if (isset($item->ErrorMessage)) {
                    printf('<b style="color:red">Error: %s</b>'."\n", (string)$item->ErrorMessage);
                }
                if (isset($item->SellerLogin)) {
                    printf('SellerLogin: %s'."\n", (string)$item->SellerLogin);
                }
                if (isset($item->TokenId)) {
                    printf('TokenId: %s'."\n", (string)$item->TokenId);
                }

                if (isset($item->ErrorList)) {
                    foreach ($item->ErrorList->Error as $error_item) {
                        printf('Error - Type: %s Message: %s'."\n", (string)$error_item->ErrorType, (string)$error_item->Message);
                    }
                }
            }
            echo "</pre>";
        }
    }

    public static function getIdFeatureByTitle($id_product, $id_lang, $title)
    {
        $sql
            = '
            SELECT md5(fl.`name`) as md5, fl.`id_feature` as id_feature, fl.`name`, fvl.`value`, fp.id_feature_value FROM `'._DB_PREFIX_.'feature_product`  fp
            LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` fvl on (fvl.id_feature_value = fp.id_feature_value and fvl.id_lang = '.(int)$id_lang.')
            LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl on (fl.id_feature = fp.id_feature and fl.id_lang = '.(int)$id_lang.')
            WHERE fp.id_product =  '.(int)$id_product.'
            HAVING md5 = md5("'.pSQL($title).'")';

        $query = Db::getInstance()->ExecuteS($sql);

        if ($query) {
            return ($query[0]);
        } else {
            return (false);
        }
    }
}
