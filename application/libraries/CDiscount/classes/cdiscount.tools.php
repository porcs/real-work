<?php

class CDiscountTools 
{
    public static $_order_states
        = array(
            1 => 'CancelledByCustomer',
            2 => 'WaitingForSellerAcceptation',
            3 => 'AcceptedBySeller',
            4 => 'PaymentInProgress',
            5 => 'WaitingForShipmentAcceptation',
            6 => 'Shipped',
            7 => 'RefusedBySeller',
            8 => 'AutomaticCancellation',
            9 => 'PaymentRefused',
            10 => 'ShipmentRefusedBySeller',
            11 => 'None',
            12 => 'RefusedNoShipment',
            13 => 'AvailableOnStore',
            14 => 'NonPickedUpByCustomer',
            15 => 'PickedUp'
        );

    public static function toKey($str)
    {
        $str = str_replace(
            array('-', ',', '.', '/', '+', '.', ':', ';', '>', '<', '?', '(', ')', '!'),
            array('_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm'), $str);
        $str = Tools::strtolower(preg_replace('/[^A-Za-z0-9_]/', '', $str));

        return $str;
    }

    public static function encode($configuration)
    {
        return base64_encode($configuration);
    }

    public static function decode($configuration)
    {
        return base64_decode($configuration);
    }

    public static function priceRule($price, $rule)
    {
        // Integrity check
        if (!isset($rule['rule']) || !isset($rule['rule']['from']) || !isset($rule['rule']['to'])) {
            return ((float)$price);
        }

        if (!is_array($rule['rule']) || !is_array($rule['rule']['from']) || !is_array($rule['rule']['to'])) {
            return ((float)$price);
        }

        if ($rule['type'] == 'percent' && !(isset($rule['rule']['percent']) || !is_array($rule['rule']['percent']) || !max($rule['rule']['percent']))) {
            return ((float)$price);
        }
        if ($rule['type'] == 'value' && !(isset($rule['rule']['value']) || !is_array($rule['rule']['value']) || !max($rule['rule']['value']))) {
            return ((float)$price);
        }

        $index = null;

        if (is_array($rule['rule']['to']) && is_array($rule['rule']['from']) && max($rule['rule']['to']) >= $price) {
            foreach ($rule['rule']['from'] as $key => $val1) {
                if ((int)$price >= (int)$val1 && (int)$price <= (int)$rule['rule']['to'][$key]) {
                    $index = $key;
                }
            }
        }

        if ($index === null) {
            return ((float)$price);
        }

        if ($rule['type'] == 'value') {
            $price += (float)$rule['rule']['value'][$index];
        } elseif ($rule['type'] == 'percent') {
            $price += $price * ((float)$rule['rule']['percent'][$index] / 100);
        }

        return ((float)$price);
    }

    public static function currentToken($cdiscount_token, $cdiscount_token_validity)
    {
        static $token = null;
        static $validity = null;

        $now = time();

        if ($token == null) {
            $token = $cdiscount_token;
            $validity = $cdiscount_token_validity;
        }

        if (!preg_match('/[0-9a-f]{32}/i', $token)) {
            return (false);
        }

        if ($now < $validity && $token) {
            return ($token);
        }

        return (false);
    }

    public static function auth($cdiscount_token, $cdiscount_token_validity, $force_username = null, $force_password = null, $force_token = null, $debug)
    {
        $now = time();

        $token = $cdiscount_token;
        $validity = $cdiscount_token_validity;

        if ($force_token) {
            $token = null;
        }

        if ($now < $validity && $token) {
            if ($debug) {
                printf('%s/%s: getToken() - using valid token: %s', basename(__FILE__), __LINE__, $token);
            }

            return ($token);
        }

        require_once(dirname(__FILE__).'/cdiscount.webservice.php');
        
        $username = $force_username;
        $password = $force_password;

        $marketplace = new CDiscountWebservice($params, $debug);

        $ret = $marketplace->getToken($debug);

        if (isset($ret['Error']) && $ret['Error']) {
            return (false);
        }

        if (!$force_token) {
            Configuration::updateValue(parent::KEY.'_TOKEN', $ret['Token']);
            Configuration::updateValue(parent::KEY.'_TOKEN_VALIDITY', $ret['Validity']);
        }

        return ($ret['Token']);
    }


    public static function displayDate($date, $id_lang = null, $full = false, $separator = '-')
    {
        if (version_compare(_PS_VERSION_, '1.5', '>=')) {
            $id_lang = null;

            return (Tools::displayDate($date, $id_lang, $full));
        } else {
            return (Tools::displayDate($date, $id_lang, $full, $separator));
        }
    }


    public static function orderStateToId($orderState)
    {
        $states = array_flip(self::$_order_states);

        return ($states[$orderState]);
    }


    public static function orderIdToState($orderStateId)
    {
        return (isset(self::$_order_states[$orderStateId]) ? self::$_order_states[$orderStateId] : 'Unknown');
    }


    public static function oldest()
    {
        $sql
            = '
            SELECT MIN(`date_add`) as date_min FROM `'._DB_PREFIX_.'product`;';
        if (($rq = Db::getInstance()->ExecuteS($sql)) && is_array($rq)) {
            $result = array_shift($rq);

            return ($result['date_min']);
        } else {
            return (false);
        }
    }


    public static function getFriendlyUrl($text)
    {
        $text = htmlentities($text);
        $text = preg_replace(array('/&szlig;/', '/&(..)lig;/', '/&([aouAOU])uml;/', '/&(.)[^;]*;/'), array(
            'ss',
            '$1',
            '$1'.'e',
            '$1'
        ), $text);
        $text = preg_replace('/[\x00-\x1F\x21-\x2B\x3A-\x3F\x5B-\x60\x7B-\x7F]/', '', $text); // remove non printable
        $text = preg_replace('/[ \t]+/', '-', $text);
        $text = str_replace(array('_', ',', '.', '/', '+', '?', '&', '='), '-', $text);

        return Tools::strtolower(trim($text));
    }

    public static function getProductImages($id_product, $id_product_attribute, $id_lang)
    {
        $product = new Product($id_product);

        if (($cover = Product::getCover($id_product))) {
            $id_image_cover = (int)$cover['id_image'];
        } else {
            $id_image_cover = null;
        }

        if ((int)$id_product_attribute) {
            $images = $product->getCombinationImages($id_lang);
            $id_images = array();

            if (is_array($images) && count($images)) {
                if (isset($images[$id_product_attribute])) {
                    foreach ($images[$id_product_attribute] as $image) {
                        if ($id_image_cover && $image['id_image'] == $id_image_cover) {
                            array_splice($id_images, 0, 0, array($image['id_image']));
                        } else {
                            $id_images[] = $image['id_image'];
                        }
                    }
                } else {
                    $id_images = false;
                }
            } else {
                $images = $product->getImages($id_lang);
                if (is_array($images) && count($images)) {
                    foreach ($images as $key => $image) {
                        if ($id_image_cover && $image['id_image'] == $id_image_cover) {
                            array_splice($id_images, 0, 0, array($image['id_image']));
                        } else {
                            $id_images[] = $image['id_image'];
                        }
                    }
                } else {
                    $id_images = false;
                }
            }
        } else {
            $images = $product->getImages($id_lang);
            $id_images = array();

            if (is_array($images) && count($images)) {
                foreach ($images as $image) {
                    if ($id_image_cover && $image['id_image'] == $id_image_cover) {
                        array_splice($id_images, 0, 0, array($image['id_image']));
                    } else {
                        $id_images[] = $image['id_image'];
                    }
                }
            } else {
                $id_images = false;
            }
        }
        $images = array();

        if ($id_images) {
            foreach ($id_images as $id_image) {
                $images[] = self::getImageUrl($id_image, $id_product);
            }
        }

        return ($images);
    }

    /**
     * @param $id_image
     * @param $productid
     *
     * @return bool|string
     */
    public static function getImageUrl($id_image, $productid)
    {
        $image_type = null;
        $ext = 'jpg';

        #image url
        if (version_compare(_PS_VERSION_, '1.4', '>=')) {
            $image_obj = new Image($id_image);

            // PS > 1.4.3
            if (method_exists($image_obj, 'getExistingImgPath')) {
                $img_path = $image_obj->getExistingImgPath();
                $imageurl = $img_path;
            } else {
                $imageurl = $productid.'-'.$id_image;
            }
        } else {
            $imageurl = $productid.'-'.$id_image;
        }

        if (method_exists('ImageType', 'getFormatedName')) {
            $image_type = Configuration::get(parent::KEY.'_IMAGE_TYPE');
        }

        if (Tools::strlen($image_type)) {
            $imageurl = sprintf('%s-%s.%s', $imageurl, $image_type, $ext);
        } else {
            $imageurl = sprintf('%s.%s', $imageurl, $ext);
        }


        return $imageurl;
    }

    /*  XML 2 ARRAY
      Event handler called by the expat library when an element's end tag is encountered.
     */

    public static function xml2array($contents, $get_attributes = 1, $priority = 'tag')
    {
        $attributes = $value = null;
        if (!$contents) {
            return array();
        }

        if (!function_exists('xml_parser_create')) {
            //print "'xml_parser_create()' function not found!";
            return array();
        }

        //Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, 'UTF-8'); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values) {
            return;
        } //Hmm...

        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array; //Refference
        //Go through the tags.
        $repeated_tag_index = array(); //Multiple tags with same name will be turned into an array
        foreach ($xml_values as $data) {
            unset($attributes, $value); //Remove existing values, or there will be trouble
            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data); //We could use the array by itself, but this cooler.

            $result = array();
            $attributes_data = array();

            if (isset($value)) {
                if ($priority == 'tag') {
                    $result = $value;
                } else {
                    $result['value'] = $value;
                } //Put the value in a assoc array if we are in the 'Attribute' mode
            }

            //Set the attributes too.
            if (isset($attributes) && $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag') {
                        $attributes_data[$attr] = $val;
                    } else {
                        $result['attr'][$attr] = $val;
                    } //Set all the attributes in a array called 'attr'
                }
            }

            //See tag status and do the needed.
            if ($type == 'open') {
                //The starting of the tag '<tag>'
                $parent[$level - 1] = &$current;
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                    $current[$tag] = $result;
                    if ($attributes_data) {
                        $current[$tag.'_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag.'_'.$level] = 1;

                    $current = &$current[$tag];
                } else { //There was another element with the same tag name
                    if (isset($current[$tag][0])) {
                        //If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                        $repeated_tag_index[$tag.'_'.$level]++;
                    } else {
                        //This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        ); //This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag.'_'.$level] = 2;

                        if (isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag.'_'.$level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }
            } elseif ($type == 'complete') { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if (!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if ($priority == 'tag' && $attributes_data) {
                        $current[$tag.'_attr'] = $attributes_data;
                    }
                } else { //If taken, put all things inside a list(array)
                    if (isset($current[$tag][0]) && is_array($current[$tag])) {
                        //If it is already an array...
                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

                        if ($priority == 'tag' && $get_attributes && $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level].'_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag.'_'.$level]++;
                    } else { //If it is not an array...
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        ); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag.'_'.$level] = 1;
                        if ($priority == 'tag' && $get_attributes) {
                            if (isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                                $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                                unset($current[$tag.'_attr']);
                            }

                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag.'_'.$level].'_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                    }
                }
            } elseif ($type == 'close') {
                //End of tag '</tag>'
                $current = &$parent[$level - 1];
            }
        }

        return ($xml_array);
    }

    /*Source : http://www.edmondscommerce.co.uk/php/ean13-barcode-check-digit-with-php/
    Many thanks ;)*/

    /**
     * @param $code
     *
     * @return bool
     */
    public static function eanUpcCheck($code)
    {
        if (!is_numeric($code) || Tools::strlen($code) < 12) {
            return (false);
        }
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', Tools::substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1}
        + $digits{3}
        + $digits{5}
        + $digits{7}
        + $digits{9}
        + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0}
        + $digits{2}
        + $digits{4}
        + $digits{6}
        + $digits{8}
        + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = (int)$next_ten - $total_sum;
        $last_digit = (int)Tools::substr($code, Tools::strlen($code) - 1, 1);

        return ((int)$last_digit == (int)$check_digit);
    }
    
    public static function cPath($id_category, $id_lang)
    {
        $c = new Category($id_category);

        if (!isset($category)) {
            $category = '';
        }

        if ($c->id_parent && $c->id_parent != 1) {
            $category .= self::cPath($c->id_parent, $id_lang).' > ';
        }

        if (is_array($c->name)) {
            if (isset($c->name[$id_lang])) {
                $category .= $c->name[$id_lang];
            } else {
                $category .= $c->name[0];
            }
        } else {
            $category .= $c->name;
        }

        return (rtrim($category, ' > '));
    }

    public static function getHttpHost($http = false, $entities = false, $ignore_port = false)
    {
        if (method_exists('Tools', 'getHttpHost')) {
            return (Tools::getHttpHost($http, $entities, $ignore_port));
        } else {
            $host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
            if ($entities) {
                $host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
            }
            if ($http) {
                $host = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$host;
            }

            return $host;
        }
    }

    public static function trimArray($Input)
    {
        if (!is_array($Input)) {
            return trim($Input);
        }

        return array_map(array(__CLASS__, 'trimArray'), $Input);
    }

    public static function isDirWriteable($path)
    {
        $path = rtrim($path, '/\\');

        $testfile = sprintf('%s%stestfile_%s.chk', $path, DIRECTORY_SEPARATOR, uniqid());
        $timestamp = time();

        if (@file_put_contents($testfile, $timestamp)) {
            $result = trim(CDiscountTools::file_get_contents($testfile));
            @unlink($testfile);

            if ((int)$result == (int)$timestamp) {
                return (true);
            }
        }

        return (false);
    }

    public static function ucfirst($str)
    {
        if (method_exists('Tools', 'ucfirst')) {
            return Tools::ucfirst($str);
        } else {
            return Tools::strtoupper(Tools::substr($str, 0, 1)).Tools::substr($str, 1);
        }
    }

    public static function ucwords($str)
    {
        if (method_exists('Tools', 'ucwords')) {
            return Tools::ucwords($str);
        }
        if (function_exists('mb_convert_case')) {
            return mb_convert_case($str, MB_CASE_TITLE);
        }

        return ucwords(Tools::strtolower($str));
    }

    public static function moduleIsInstalled($moduleName)
    {
        if (method_exists('Module', 'isInstalled')) {
            return (Module::isInstalled($moduleName));
        } else {
            Db::getInstance()->ExecuteS('SELECT `id_module` FROM `'._DB_PREFIX_.'module` WHERE `name` = \''.pSQL($moduleName).'\'');

            return (bool)Db::getInstance()->NumRows();
        }
    }

    public static function arrayFilterRecursive($input)
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = self::arrayFilterRecursive($value);
            }
        }

        return array_filter($input);
    }

    public static function validateSKU($SKU)
    {
        return ($SKU != null && Tools::strlen($SKU) && preg_match('/[0-9A-Za-z\/_ @\?\>=\<;:\.\-,\+\*\)\(\'\&%\$#" !\^\~\}\{\[\]]{1,64}/', $SKU) && preg_match('/[^ ]$/', $SKU) && preg_match('/^[^ ]/', $SKU));
    }

    public static function copy($source, $destination, $stream_context = null)
    {
        if (method_exists('Tools', 'copy')) {
            if (is_null($stream_context) && !preg_match('/^https?:\/\//', $source)) {
                return @copy($source, $destination);
            } //TODO: Validation - PS1.4 compat
            return @file_put_contents($destination, CDiscountTools::file_get_contents($source, false, $stream_context));//TODO: Validation - PS1.4 compat
        } else {
            return @copy($source, $destination);
        }
    }

    public static function smartRounding($price)
    {
        // Smart Price
        $plain = floor($price);
        $decimals = $price - $plain;
        $decimal_part = (int)((string)$decimals * 100); // https://www.google.fr/search?hl=fr&output=search&sclient=psy-ab&q=php+floor+bug&btnG=&gws_rd=ssl

        if (!$decimals || ($decimal_part % 10) == 0) {
            $rounded = $decimal_part;
        } else {
            $rounded = sprintf('%02d', ((number_format(round($decimals, 1) - 0.1, 2, '.', '') * 100) - 1) + 10);
        }

        $smart_price = sprintf('%d.%02d', $plain, max(0, $rounded));

        return ($smart_price);
    }


    public static function fieldExists($table, $field)
    {
        static $field_exists = array();
        $fields = array();

        if (isset($field_exists[$table.$field])) {
            return $field_exists[$table.$field];
        }

        // Check if exists
        //
        $query = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'.$table.'`');

        if (!is_array($query) || !count($query)) {
            return (null);
        }

        foreach ($query as $row) {
            $fields[$row['Field']] = 1;
        }

        if (isset($fields[$field])) {
            $field_exists[$table.$field] = true;
        } else {
            $field_exists[$table.$field] = false;
        }

        return $field_exists[$table.$field];
    }

    public static function tableExists($table)
    {
        static $table_exists = array();
        static $show_tables_content = null;

        if (isset($table_exists[$table])) {
            return $table_exists[$table];
        }

        // Check if exists
        //
        if ($show_tables_content === null) {
            $tables = array();
            $query_result = Db::getInstance()->ExecuteS('SHOW TABLES');

            if (!is_array($query_result) || !count($query_result)) {
                return (null);
            }

            $show_tables_content = $query_result;
        }

        foreach ($show_tables_content as $rows) {
            foreach ($rows as $table_check) {
                $tables[$table_check] = 1;
            }
        }

        if (isset($tables[$table])) {
            $table_exists[$table] = true;
        } else {
            $table_exists[$table] = false;
        }

        return $table_exists[$table];
    }

    //http://php.net/manual/fr/function.glob.php#106595
    /**
     * @param $pattern
     * @param int $flags
     *
     * @return array
     */
    public static function globRecursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);

        if (is_array($files) && count($files)) {
            $dirs = glob(dirname($pattern).'/*', GLOB_ONLYDIR | GLOB_NOSORT);

            if (is_array($dirs) && count($dirs)) {
                foreach ($dirs as $dir) {
                    $other_files = self::globRecursive($dir.'/'.basename($pattern), $flags);

                    if (is_array($other_files) && count($other_files)) {
                        $files = array_merge($files, $other_files);
                    }
                }
            }
        }

        return $files;
    }

    /**
     * @param $url
     * @param bool $use_include_path
     * @param null $stream_context
     * @param int $curl_timeout
     *
     * @return bool|mixed
     */
    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 30, $certificate = null, $debug=false)
    {
        if (function_exists('curl_init') && preg_match('/^https?:\/\//', $url)) {
            $curl = curl_init();
            $cert = strlen($certificate) ? $certificate : CdiscountCertificates::getCertificate();

            if (defined('CURLOPT_IPRESOLVE') && defined('CURL_IPRESOLVE_V4')) {
                curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            }
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($curl, CURLOPT_CAINFO, $cert);
            
            if ($stream_context != null) {
                $opts = stream_context_get_options($stream_context);
                if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post') {
                    curl_setopt($curl, CURLOPT_POST, true);
                    if (isset($opts['http']['content'])) {
                        parse_str($opts['http']['content'], $post_data);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
                    }
                }
            }
            $content = curl_exec($curl);

            if ($debug) {
                echo "<pre>\n";
                printf('%s - %s::%s()/#%d'."\n", basename(__FILE__), __CLASS__, __FUNCTION__, __LINE__);
                printf('timeout: %s'."\n", print_r($curl_timeout, true));
                printf('cert file: %s'."\n", print_r($cert, true));
                printf('curl error: %s (%d)'."\n", curl_error($curl), curl_errno($curl));
                printf('curl info: %s'."\n", print_r(curl_getinfo($curl), true));
                echo "</pre>";
            }
            curl_close($curl);


            return $content;
        } elseif (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)) {
            if ($stream_context == null && preg_match('/^https?:\/\//', $url)) {
                if (preg_match('/^https:\/\//', $url)) {
                    $contextOptions = array(
                        'ssl' => array(
                        'verify_peer'   => true,
                        'cafile'        => Tools::strlen($certificate) ? $certificate : CdiscountCertificates::getCertificate()
                        )
                    );
                    $stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)), $contextOptions);
                } else {
                    $contextOptions = array();
                    $stream_context = null;
                }
            }

            if (CDiscount::$debug_mode) {
                return file_get_contents($url, $use_include_path, is_resource($stream_context) ? $stream_context : null);//TODO Validation: http://forge.prestashop.com/browse/PSCSX-7758
            } else {
                return @file_get_contents($url, $use_include_path, is_resource($stream_context) ? $stream_context : null);//TODO Validation: http://forge.prestashop.com/browse/PSCSX-7758
            }
        } else {
            return false;
        }
    }

    public static function isTlsAvailable($curl_tls_constant)
    {
        $php_version = phpversion();
        $php_version_check = version_compare($php_version, '7', '>=');//too many problem with TLSv_x with PHP <7
        return(defined($curl_tls_constant) && $php_version_check);
    }
}
