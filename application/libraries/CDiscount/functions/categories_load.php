<?php

require_once(dirname(__FILE__).'/../classes/cdiscount.tools.php');
require_once(dirname(__FILE__).'/../classes/cdiscount.categories.php');
require_once(dirname(__FILE__).'/../classes/cdiscount.configuration.php');

class CDiscountCategoriesLoad
{
    private $json  = false;
    private $data  = false;
    private $error = false;
    public $debug;
    public $user;
    public $params;

    public function __construct($user, $debug=false)
    {
        $this->debug = $debug;
        $this->user = $user;
    }

    public function dispatch($action)
    {
        // get user data
        $this->fetch_params();
        
        switch ($action) {
            case 'universes':
                $this->universes();
                break;
            case 'categories':
                $this->categories();
                break;
            case 'category':
                ob_start();
                $this->json = true;
                if (!$this->universe2Category()) {
                    printf('%s/%s: universe2Category() failed'.self::LF, basename(__FILE__), __LINE__);
                }
                break;
            default :

        }
        
        if ($this->json) {            // jquery

            $callback = Tools::getValue('callback');

            if (!$callback) {
                $callback = 'jsonp_'.time();
            }

            $output = ob_get_clean();

            $data = array('data' => $this->data, 'error' => $this->error, 'output' => $output);

            die((string)$callback.'('.Tools::jsonEncode($data).')');
        }
    }
    
    public function fetch_params(){

        $cdiscountConfiguration = new CDiscountConfiguration($this->user);
        $params = $cdiscountConfiguration->getUserConfigurations();

        var_dump($params); exit;
        //$this->params =
        
    }



    public function universes()
    {
        CDiscountCategories::setPath();

        if (!($result = $this->loadAllCategoryTree())) {
            if ($this->debug) {
                printf('%s/%s: loadAllCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
            }

            return (false);
        }
        $rootCategories = $result->xpath('/*/CategoryTree/ChildrenCategoryList/*/Name');

        if (is_array($rootCategories) && count($rootCategories)) {
            if (file_put_contents(CDiscountCategories::$universes_file, null) === false) {
                // clear file

                if ($this->debug) {
                    printf('%s/%s: Unable to write to output file'.self::LF, basename(__FILE__), __LINE__);
                }

                return (false);
            }

            foreach ($rootCategories as $key => $category) {
                file_put_contents(CDiscountCategories::$universes_file, $category.self::LF, FILE_APPEND);
            }
        } else {
            if (file_exists(CDiscountCategories::$universes_file)) {
                unlink(CDiscountCategories::$universes_file);
            }
        }
        $this->checkAllCategory();
    }

    public function loadAllCategoryTree()
    {
        $marketplace = new CDiscountCategories( $this->debug);
        $marketplace->token = CDiscountTools::auth('AllData', 'pa$$word', true);

        $filecheck = CDiscountCategories::getCategoriesTimestamp();

        if ($this->debug) {
            printf('%s/%s: Timestamp: %d - %s'.self::LF, basename(__FILE__), __LINE__, $filecheck, CDiscountTools::displayDate(date('Y-m-d H:i:s', $filecheck), $this->id_lang, true));
        }

        if (!$filecheck || $filecheck < (time() - 86400 * 15) || (bool)Tools::getValue('force')) {
            if ($this->ps16x) {
                $class_error = 'alert alert-danger';
            } else {
                $class_error = parent::MODULE.'-error';
            }

            if (!$marketplace->token) {
                die('<div class="'.$class_error.'">'.$this->l('Authentication process has failed').' '.parent::NAME.'</div>');
            }

            if ($this->debug) {
                printf('%s/%s: Load through Web/Service'.self::LF, basename(__FILE__), __LINE__);
            }

            $result = $marketplace->GetAllAllowedCategoryTree();

            if (isset($result->ErrorList->Error)) {
                if ($this->debug) {
                    printf('%s/%s: GetAllAllowedCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
                }

                return (false);
            }
            if ($result instanceof SimpleXMLElement && isset($result->CategoryTree)) {
                if (!$result->saveXML('compress.zlib://'.CDiscountCategories::$category_file)) {
                    if ($this->debug) {
                        printf('%s/%s: GetAllAllowedCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
                    }

                    return (false);
                }
            }
        } else {
            if ($this->debug) {
                printf('%s/%s: Load from File: %s'.self::LF, basename(__FILE__), __LINE__, CDiscountCategories::$category_file);
            }

            if (!file_exists(CDiscountCategories::$category_file)) {
                if ($this->debug) {
                    printf('%s/%s: Missing file: %s'.self::LF, basename(__FILE__), __LINE__, CDiscountCategories::$category_file);
                }

                return (false);
            }

            if (!($file_contents = CDiscountTools::file_get_contents('compress.zlib://'.CDiscountCategories::$category_file))) {
                if ($this->debug) {
                    printf('%s/%s: Unable to read file: %s'.self::LF, basename(__FILE__), __LINE__, CDiscountCategories::$category_file);
                }

                return (false);
            }

            if (!($result = simplexml_load_string($file_contents))) {
                if ($this->debug) {
                    printf('%s/%s: GetAllAllowedCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
                }

                return (false);
            }
        }

        return ($result);
    }

    public function l($string, $specific = false, $id_lang = null)
    {
        return (parent::l($string, basename(__FILE__, '.php'), $id_lang));
    }

    public function checkAllCategory()
    {
        $fileInfo = CDiscountCategories::getCategoriesTimestamp();

        if ($fileInfo == false) {
            $Info
                = '<div class="'.parent::MODULE.'-error">'.
                $this->l('The categories file is wrong or missing, please contact us').
                '</div>';
        } else {
            if (Tools::getValue('force')) {
                $date = date('Y-m-d H:i:s');
            } else {
                $date = date('Y-m-d H:i:s', $fileInfo);
            }

            if ($this->ps16x) {
                $class = 'alert alert-info';
            } else {
                $class = 'cd-info-level-info';
            }

            $Info
                = '<div class="'.$class.'" style="display:block">'.
                $this->l('Categories File, last updated on').' : '.CDiscountTools::displayDate($date, $this->id_lang, true).
                '</div>';
        }
        echo $Info;
    }

    public function categories()
    {
        CDiscountCategories::setPath();

        if (!($result = $this->loadAllowedCategoryTree())) {
            if ($this->debug) {
                printf('%s/%s: loadAllCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
            }

            return (false);
        }
        if (!$result instanceof SimpleXMLElement) {
            unlink(CDiscountCategories::$allowed_category_file);
        } else {
            $Categories = $result->xpath('//ChildrenCategoryList/CategoryTree/Code');

            if (!is_array($Categories) || !count($Categories)) {
                unlink(CDiscountCategories::$allowed_category_file);
            }
        }

        $this->checkAllowedCategory();
    }

    public function loadAllowedCategoryTree()
    {
        $force_load = false;
        $username = Configuration::get(parent::KEY.'_USERNAME');
        $password = Configuration::get(parent::KEY.'_PASSWORD');
        $debug = Configuration::get(parent::KEY.'_DEBUG');
        $production = !(Configuration::get(parent::KEY.'_PREPRODUCTION') ? true : false);

        $marketplace = new CDiscountCategories($username, $password, $production, $this->debug);
        $marketplace->token = CDiscountTools::auth();

        $filecheck = CDiscountCategories::getAllowedCategoriesTimestamp();

        if ($this->debug) {
            printf('%s/%s: Timestamp: %d - %s'.self::LF, basename(__FILE__), __LINE__, $filecheck, CDiscountTools::displayDate(date('Y-m-d H:i:s', $filecheck), $this->id_lang));
        }

        if (!$filecheck || $filecheck < (time() - 86400 * 15) || (bool)Tools::getValue('force')) {
            if ($this->ps16x) {
                $class_error = 'alert alert-danger';
            } else {
                $class_error = parent::MODULE.'-error';
            }

            if (!$marketplace->token) {
                die('<div class="'.$class_error.'">'.$this->l('Authentication process has failed').' '.parent::NAME.'</div>');
            }

            if ($this->debug) {
                printf('%s/%s: Load through Web/Service'.self::LF, basename(__FILE__), __LINE__);
            }

            $result = $marketplace->GetAllowedCategoryTree();

            if ($result instanceof SimpleXMLElement && isset($result->CategoryTree)) {
                // Fix trailing whitespace issue
                $output = str_replace(' </Name>', '</Name>', $result->asXML());

                if (!file_put_contents('compress.zlib://'.CDiscountCategories::$allowed_category_file, $output)) {
                    if ($this->debug) {
                        printf('%s/%s: LoadAllowedCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
                    }

                    $force_load = true;
                }
            } else {
                $force_load = true;
            }
        } else {
            $force_load = true;
        }

        if ($force_load) {
            if ($this->debug) {
                printf('%s/%s: Load from File: %s'.self::LF, basename(__FILE__), __LINE__, CDiscountCategories::$allowed_category_file);
            }

            if (!($result = simplexml_load_file('compress.zlib://'.CDiscountCategories::$allowed_category_file))) {
                if ($this->debug) {
                    printf('%s/%s: LoadAlowedlCategoryTree() failed'.self::LF, basename(__FILE__), __LINE__);
                }

                return (false);
            }
        }

        return ($result);
    }

    public function checkAllowedCategory()
    {
        $fileInfo = CDiscountCategories::getAllowedCategoriesTimestamp();

        if ($fileInfo == false) {
            $Info
                = '<div class="'.parent::MODULE.'-error">'.
                $this->l('The categories file is wrong or missing, please contact us').
                '</div>';
        } else {
            if (Tools::getValue('force')) {
                $date = date('Y-m-d H:i:s');
            } else {
                $date = date('Y-m-d H:i:s', $fileInfo);
            }

            if ($this->ps16x) {
                $class = 'alert alert-info';
            } else {
                $class = 'cd-info-level-info';
            }
            $Info
                = '<div class="'.$class.'" style="display:block">'.
                $this->l('Categories File, last updated on').' : '.CDiscountTools::displayDate($date, $this->id_lang, true).
                '</div>';
        }
        echo $Info;
    }

    public function universe2Category()
    {
        CDiscountCategories::setPath();

        if (!($result = $this->LoadAllowedCategoryTree())) {
            if ($this->debug) {
                printf('%s/%s: universe2Category() failed'.self::LF, basename(__FILE__), __LINE__);
            }
            $this->error = true;

            return (false);
        }

        $category = trim(Tools::getValue('category'));

        if (empty($category)) {
            $this->error = false;
            $this->data = '';

            return (false);
        }
        $options = null;

        $categories_array = CDiscountCategories::universeToCategories($category);

        if (is_array($categories_array) && count($categories_array)) {
            asort($categories_array);

            if (!count($categories_array)) {
                // No Categories
                $options .= '<OPTION disabled>'.parent::l('No categories available').'</OPTION>'.self::LF;
            } else {
                asort($categories_array);

                foreach ($categories_array as $code => $category) {
                    $options .= '<OPTION value="'.$code.'">'.$category.'</OPTION>'.self::LF;
                }
            }
        }
        echo $options;
        $this->data = $options;
    }
}