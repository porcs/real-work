<?php

require_once 'hook.database.php';
require_once 'config.php';

class MiraklValue {

    private $get_config;
    private $hook_database;
    private $config;

    public function __construct($get_config) {
        $this->get_config = $get_config;
        $this->hook_database = new HookDatabase($this->get_config['user_name']);
        $this->config = new Config();
    }

    public function getValue() {
        $count = 0;
        $total = 0;
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();

        if (isset($endpoints[$this->get_config['sub_marketplace']]) && !empty($endpoints[$this->get_config['sub_marketplace']])) {
            $result_endpoints = $endpoints[$this->get_config['sub_marketplace']];

            //$version_api = $this->config->getVersionAPI($result_endpoints['endpoint'], $this->get_config['api_key']);

            $results = $this->config->getValuesList($result_endpoints['endpoint'], $this->get_config['api_key']);
            $sub_marketplace = $result_endpoints['sub_marketplace'];

//            echo '<pre>';
//            print_r($results);
//            exit;

            /* version api 3.22 can get label
             * version api 3.23 change label, can read label in (label_translations[0]-> value)
             */

            //ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 900);
            if (isset($results->values_lists)) {
                foreach ($results->values_lists as $list) {

                    $data = array(
                        'sub_marketplace' => $sub_marketplace,
                        'code' => isset($list->code) ? $this->config->replace_quote((string) $list->code) : '',
                        'label' => isset($list->label) ? $this->config->replace_quote((string) $list->label) : ''
                    );

                    $result = $this->setValue($data, $list->values);

                    if (isset($result) && !empty($result)) {
                        $id_field[] = $result;
                        $count = $count + 1;
                    }
                    
                    $total = $total + 1;
                }

                if (isset($id_field) && !empty($id_field)) {
                    $this->deleteValue($sub_marketplace, $id_field);
                }
            }
        }

        print_r('get value list config '.$total.' / '.$count);
    }

    private function setValue($data = array(), $values_list) {

        if (empty($values_list)) {
            return false;
        }

        $id_config_value = '';
        $id_config_value_list = '';
        $sub_marketplace = $data['sub_marketplace'];
        $code = $data['code'];

        $id_config_value = $this->hook_database->getIdConfigValue($sub_marketplace, $code);

        if (isset($id_config_value) && !empty($id_config_value)) {
            $this->hook_database->updValue($id_config_value, $data);
        } else {
            $id_config_value = $this->hook_database->addValue($data);
        }
//        
//        if ($id_config_value != '1450') {
//            return false;
//        }

        $is_upd = false;
        $is_ins = false;

        $ins_arr = array();
        $upd_arr = array();

        $len = 0;
        $len2 = 0;

        // detail
        if (isset($id_config_value) && !empty($id_config_value)) {

            $res_value_list = $this->hook_database->getIdConfigValueListByIdConfigValue($id_config_value);

            $count_values_list = count($values_list); // count begin index is 1
            $count_loop = 0;
            foreach ($values_list as $index => $value_list) {

                $code = $this->config->replace_quote(($value_list->code));
                $label = isset($value_list->label_translations[0]->value) ? $this->config->replace_quote($value_list->label_translations[0]->value) : $this->config->replace_quote($value_list->label);

                $indb = false;
                if (!empty($res_value_list)) {
                    foreach ($res_value_list as $id_config_value_list => $res_list) {
                        if ($code == $this->config->replace_quote($res_list)) {
                            $upd_arr[] = "WHEN id_config_value_list = $id_config_value_list THEN '$label' ";
                            $len2 = $len2 + 1;
                            $indb = true;
                            break;
                        }
                    }
                }

                if ($len2 >= 1000) {
                    $sql_upd = "UPDATE mirakl_config_value_list SET label = CASE ";
                    $sql_upd.= implode(" ", $upd_arr);
                    $sql_upd.= " ELSE label END; ";
                    if ($this->hook_database->sql_query_exec($sql_upd) != 1) {
                        die('This is an error update within muit.'.$sql_upd);
                    }

                    $len2 = 0;
                    $upd_arr = array();
                }


                if (!$indb) {
                    $is_ins = true;
                    $ins_arr[] = " ('$id_config_value', '$code', '$label') ";

                    if ($len >= 1000) {

                        $sql_ins = "INSERT INTO mirakl_config_value_list (id_config_value, code, label) VALUES ";
                        $sql_ins.= implode(",", $ins_arr);
                        $sql_ins.= " ; ";
                        if ($this->hook_database->sql_query_exec($sql_ins) != 1) {
                            die('This is an error insert within muit.'.$sql_ins);
                        }

                        $len = 0;
                        $ins_arr = array();
                    }
                }

                $len = $len + 1;


                $count_loop = $count_loop + 1;
            }
        } else {
            die('This is value an error.');
        }

        if (!empty($ins_arr)) {
            $sql_ins = "INSERT INTO mirakl_config_value_list (id_config_value, code, label) VALUES ";
            $sql_ins.= implode(",", $ins_arr);
            $sql_ins.= " ; ";

            // print_r($sql_ins); exit;
            if ($this->hook_database->sql_query_exec($sql_ins) != 1) {
                die('This is an error insert.'.$sql_ins);
            }
        }

        if (!empty($upd_arr)) {
            $sql_upd = "UPDATE mirakl_config_value_list SET label = CASE ";
            $sql_upd.= implode(" ", $upd_arr);
            $sql_upd.= " ELSE label END; ";
            // print_r($sql_upd); exit;
            if ($this->hook_database->sql_query_exec($sql_upd) != 1) {
                die('This is an error update.'.$sql_upd);
            }
        }
        
        if($count_values_list != $count_loop){
            die('This is an error value list is not count mapping');
        }

        return $id_config_value;

    }

    private function deleteValue($sub_marketplace, $id_field = array()) {
        $this->hook_database->delValue($sub_marketplace, $id_field);
    }

}

// end definition
