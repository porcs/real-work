<?php

require_once 'config.php';
require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__).'/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');
require_once(dirname(__FILE__).'/../classes/mirakl.parameter.php');
require_once(dirname(__FILE__).'/../classes/mirakl.tools.php');

class MiraklCondition {

    private $mysql_db;

    public function __construct() {
        $this->mysql_db = new ci_db_connect();
        $this->config = new Config();
    }

    public function getCondition($get_config = array()) { // actually $get_config not need.
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();
        
        if(isset($endpoints) && !empty($endpoints)){
            foreach ($endpoints as $sub_marketplace => $value) {
                if(isset($value['condition']) && !empty($value['condition'])){
                    $this->setCondition($value['condition'], MiraklParameter::MARKETPLACE_ID, $sub_marketplace);
                }
            }
        }
    }

    private function setCondition($conditions, $id_marketplace, $sub_marketplace) {

        $table = 'marketplace_condition';
        $id_conditions = array();
        $conditions_fields = explode(',', $conditions);

        if (isset($conditions_fields) && is_array($conditions_fields)) {
            foreach ($conditions_fields as $conditions_field) {
                $condition_set = explode(':', $conditions_field);
                if (count($condition_set) == 2) {
                    $condition_text = Mirakl_Tools::toKey($condition_set[0]);
                    $condition_value = $condition_set[1];

                    $sql = "SELECT * FROM marketplace_condition WHERE id_marketplace = $id_marketplace AND sub_marketplace = $sub_marketplace  AND condition_text = '$condition_text' ";
                    $query = $this->mysql_db->select_query($sql);

                    if ($this->mysql_db->count_rows($query) == 0) {
                        $data = array();
                        $data['condition_value'] = $condition_value;
                        $data['condition_text'] = $condition_text;
                        $data['status'] = 'enable';
                        $data['id_marketplace'] = $id_marketplace;
                        $data['sub_marketplace'] = $sub_marketplace;

                        if ($this->mysql_db->add_db($table, $data, false)) {
                            $id_conditions[] = $this->mysql_db->insert_id();
                        }
                    } else {
                        $rows = $this->mysql_db->fetch($query);
                        $data = array();
                        $id_condition = $rows[0];
                        $data['condition_value'] = $condition_value;
                        $data['condition_text'] = $condition_text;
                        $data['status'] = 'enable';

                        $where = "id = $id_condition AND id_marketplace = $id_marketplace AND sub_marketplace = $sub_marketplace ";
                        $this->mysql_db->update($table, $data, $where);
                        $id_conditions[] = $id_condition;
                    }
                }
            }

            if (!empty($id_conditions)) {
                $where_delete = implode(', ', $id_conditions);
                $sql_delete = "DELETE FROM $table WHERE id_marketplace = $id_marketplace AND sub_marketplace = $sub_marketplace  AND id NOT IN ($where_delete)  ";
                $this->mysql_db->select_query($sql_delete);
            }
        }
    }

}

// end definition
