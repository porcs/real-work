<?php

require_once 'hook.database.php';
require_once 'config.php';

class MiraklAttribute {

    private $get_config;
    private $hook_database;
    private $config;

    public function __construct($get_config) {
        $this->get_config = $get_config;
        $this->hook_database = new HookDatabase($this->get_config['user_name']);
        $this->config = new Config();
    }

    public function getAttribute() {
        $count = 0;
        $total = 0;
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();

        if (isset($endpoints[$this->get_config['sub_marketplace']]) && !empty($endpoints[$this->get_config['sub_marketplace']])) {
            $result_endpoints = $endpoints[$this->get_config['sub_marketplace']];

            $results = $this->config->getProductsAttbiutes($result_endpoints['endpoint'], $this->get_config['api_key']);
            $sub_marketplace = $result_endpoints['sub_marketplace'];

            //echo '<pre>'; print_r($results); exit;
            
            ini_set('max_execution_time', 800);

            if (!empty($results)) {
                foreach ($results->attributes as $attribute) {
                    $data = array(
                        'sub_marketplace' => $sub_marketplace,
                        'code' => isset($attribute->code) ? (string) $attribute->code : '',
                        'type' => isset($attribute->type) ? (string) $attribute->type : '',
                        'default_value' => isset($attribute->default_value) ? (string) $attribute->default_value : '',
                        'description' => isset($attribute->description) ? $this->config->replace_quote((string) $attribute->description) : '',
                        'example' => isset($attribute->example) ? $this->config->replace_quote((string) ($attribute->example)) : '',
                        'hierarchy_code' => isset($attribute->hierarchy_code) ? (string) ($attribute->hierarchy_code) : '',
                        'label' => isset($attribute->label) ? $this->config->replace_quote((string) $attribute->label) : '',
                        'required' => isset($attribute->required) && ($attribute->required == true) ? 'Y' : 'N',
                        'transformations' => isset($attribute->transformations) ? (string) ($attribute->transformations) : '',
                        'type_parameter' => isset($attribute->type_parameter) ? $this->config->replace_quote((string) $attribute->type_parameter) : '',
                        'validations' => isset($attribute->validations) ? (string) ($attribute->validations) : '',
                        'values_list' => isset($attribute->values_list) ? $this->config->replace_quote((string) $attribute->values_list) : '',
                        'variant' => isset($attribute->variant) && ($attribute->variant == true) ? 'Y' : 'N',
                    );

                    $result = $this->setAttribute($data);

                    if (isset($result) && !empty($result)) {
                        $id_field[] = $result;
                        $count = $count + 1;
                    }

                    $total = $total + 1;
                }

                if (isset($id_field) && !empty($id_field)) {
                    $this->deleteAttribute($sub_marketplace, $id_field);
                }
            }
        }

        print_r('get attribute config '.$total.' / '.$count);
    }

    private function setAttribute($data = array()) {

        $id_config_attribute = '';
        $sub_marketplace = $data['sub_marketplace'];
        $code = $data['code'];
        $hierarchy_code = isset($data['hierarchy_code']) ? $data['hierarchy_code'] : '';

        $id_config_attribute = $this->hook_database->getIdConfigAttribute($sub_marketplace, $code, $hierarchy_code);

        if (isset($id_config_attribute) && !empty($id_config_attribute)) {
            $this->hook_database->updAttribute($id_config_attribute, $data);
        } else {
            $id_config_attribute = $this->hook_database->addAttribute($data);
        }

        return $id_config_attribute;
    }

    private function deleteAttribute($sub_marketplace, $id_field = array()) {
        $this->hook_database->delAttribute($sub_marketplace, $id_field);
    }

}

// end definition
