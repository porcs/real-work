<?php

require_once dirname(__FILE__).'/../../db.php';

class HookDatabase {

    private $connector;
    private $user;
    private $prefix_mirakl;

    public function __construct($user, $database = 'products') {
        $this->connector = new Db($user, $database);
        $this->user = $user;

        //set table prefix for mirakl
        $this->prefix_mirakl = MiraklParameter::PREFIX_MIRAKL;
    }

    private function sql_query($sql = '', $prefix = '', $current = false, $key = '') {

        if (empty($sql)) {
            return false;
        }

        $sql = str_replace("::PREFIX::", $prefix, $sql);
        $result = $this->connector->db_query_str($sql);

        if (empty($result)) {
            return false;
        }

        if ($current) {
            if (!empty($key)) {
                return $result[0][$key];
            } else {
                return $result[0];
            }
        }

        return $result;
    }

    private function sql_query_effect($sql = '', $last_id = false) {

        if (empty($sql)) {
            return false;
        }

        $this->connector->db_query_result_str($sql);

        if ($last_id) {
            return $this->connector->db_last_insert_rowid();
        }

        $affected = $this->connector->db_changes();

        if (!empty($affected)) {
            return true;
        }

        return false;
    }

    public function sql_query_exec($sql = '') {
        // for list many sql text eg.insert
        if (empty($sql)) {
            return false;
        }

//        $s = "INSERT INTO mirakl_config_value_list (id_config_value, code, label) VALUES ('3', 'N/A', '') , ('3', 'NC', '') , ('3', 'Lubrifiants à base de silicone', '') ;";
//        
////        print_r($sql);
////        
////        exit;
////        
//       
//        print_r($this->connector->db_exec($sql));  exit;
//        
//     //   $e = new Exception;
//        
//        echo '<pre>'; print_r($e);
//        
//        
//        ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//        
//        
//        exit;
//        
//         print_r($sql); exit;


        return $this->connector->db_exec($sql);
    }

    /* carrier */

    public function getIdCarrier($sub_marketplace, $code) {
        $table = $this->prefix_mirakl.'config_carrier';

        $sql = "SELECT id_config_carrier FROM $table WHERE sub_marketplace = $sub_marketplace AND code = '$code' ";
        return $this->sql_query($sql, false, true, 'id_config_carrier');
    }

    public function addCarrier($data) {
        $table = $this->prefix_mirakl.'config_carrier';

        $sql = "INSERT INTO $table (sub_marketplace, code, label, tracking_url) VALUES ('{$data['sub_marketplace']}', '{$data['code']}', '{$data['label']}', '{$data['tracking_url']}') ; ";
        return $this->sql_query_effect($sql, true);
    }

    public function updCarrier($id_config_carrier, $data) {
        $table = $this->prefix_mirakl.'config_carrier';

        $sql = "UPDATE $table "
                ."SET label = '{$data['label']}', tracking_url = '{$data['tracking_url']}' "
                ."WHERE id_config_carrier = $id_config_carrier ";
        return $this->sql_query_effect($sql);
    }

    public function delCarrier($sub_marketplace, $id_field) {
        $table = $this->prefix_mirakl.'config_carrier';

        $id_field = implode(",", $id_field);
        $sql = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_config_carrier NOT IN ($id_field) ; ";
        return $this->sql_query_effect($sql);
    }

    /* end carrier */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* hierarchy */
    public function getIdHierarchy($sub_marketplace, $code, $level) {
        $table = $this->prefix_mirakl.'config_hierarchy';

        $sql = "SELECT id_config_hierarchy FROM $table WHERE sub_marketplace = $sub_marketplace AND level = $level AND code = '$code' ";
        return $this->sql_query($sql, false, true, 'id_config_hierarchy');
    }

    public function addHierarchy($data) {
        $table = $this->prefix_mirakl.'config_hierarchy';

        $sql = "INSERT INTO $table (sub_marketplace, code, label, level, parent_code) VALUES ('{$data['sub_marketplace']}', '{$data['code']}', '{$data['label']}', '{$data['level']}', '{$data['parent_code']}' ) ; ";
        return $this->sql_query_effect($sql, true);
    }

    public function updHierarchy($id_config_hierarchy, $data) {
        $table = $this->prefix_mirakl.'config_hierarchy';

        $sql = "UPDATE $table "
                ."SET label = '{$data['label']}', parent_code = '{$data['parent_code']}' "
                ."WHERE id_config_hierarchy = $id_config_hierarchy ";
        return $this->sql_query_effect($sql);
    }

    public function delHierarchy($sub_marketplace, $id_field) {
        $table = $this->prefix_mirakl.'config_hierarchy';

        $id_field = implode(",", $id_field);
        $sql = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_config_hierarchy NOT IN ($id_field) ; ";
        return $this->sql_query_effect($sql);
    }

    /* end hierarchy */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* attribute */
    public function getIdConfigAttribute($sub_marketplace, $code, $hierarchy_code) {
        $table = $this->prefix_mirakl.'config_attribute';

        $sql = "SELECT id_config_attribute FROM $table WHERE sub_marketplace = $sub_marketplace AND code = '$code' ";
        if (!empty($hierarchy_code)) {
            $sql.= "AND hierarchy_code = '$hierarchy_code' ";
        }
        return $this->sql_query($sql, false, true, 'id_config_attribute');
    }

    public function addAttribute($data) {
        $table = $this->prefix_mirakl.'config_attribute';

        $sql = "INSERT INTO $table (sub_marketplace, code, type, default_value, description, example, hierarchy_code, label, required, transformations, type_parameter, validations, values_list, variant) "
                ."VALUES ('{$data['sub_marketplace']}', '{$data['code']}', '{$data['type']}', '{$data['default_value']}', '{$data['description']}', '{$data['example']}', '{$data['hierarchy_code']}', '{$data['label']}', '{$data['required']}', '{$data['transformations']}', '{$data['type_parameter']}', '{$data['validations']}', '{$data['values_list']}', '{$data['variant']}' ) ; ";

        /* for debug */
//                if($data['code'] == 'canap_type'){
//                    echo '<pre>'; print_r($sql); exit;
//                }

        return $this->sql_query_effect($sql, true);
    }

    public function updAttribute($id_config_attribute, $data) {
        $table = $this->prefix_mirakl.'config_attribute';

        $sql = "UPDATE $table "
                ."SET type = '{$data['type']}', default_value = '{$data['default_value']}', description = '{$data['description']}', example = '{$data['example']}', hierarchy_code = '{$data['hierarchy_code']}', label = '{$data['label']}', required = '{$data['required']}' "
                .", transformations = '{$data['transformations']}', type_parameter = '{$data['type_parameter']}', validations = '{$data['validations']}', values_list = '{$data['values_list']}', variant = '{$data['variant']}'       "
                ."WHERE id_config_attribute = $id_config_attribute ";
        return $this->sql_query_effect($sql);
    }

    public function delAttribute($sub_marketplace, $id_field) {
        $table = $this->prefix_mirakl.'config_attribute';

        $id_field = implode(",", $id_field);
        $sql = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_config_attribute NOT IN ($id_field) ; ";
        return $this->sql_query_effect($sql);
    }

    /* end attribute */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* value list */
    public function getIdConfigValue($sub_marketplace, $code) {
        $table = $this->prefix_mirakl.'config_value';
        $sql = "SELECT id_config_value FROM $table WHERE sub_marketplace = $sub_marketplace AND BINARY code = '$code' ; ";
        return $this->sql_query($sql, false, true, 'id_config_value');
    }

    public function addValue($data) {
        $table = $this->prefix_mirakl.'config_value';

        $sql = "INSERT INTO $table (sub_marketplace, code, label) VALUES ('{$data['sub_marketplace']}', '{$data['code']}', '{$data['label']}' ) ; ";
        return $this->sql_query_effect($sql, true);
    }

    public function updValue($id_config_value, $data) {
        $table = $this->prefix_mirakl.'config_value';

        $sql = "UPDATE $table "
                ."SET code = '{$data['code']}', label = '{$data['label']}' "
                ."WHERE id_config_value = $id_config_value ";
        return $this->sql_query_effect($sql);
    }

//    public function getIdConfigValueList($id_config_value, $code) {
//        $table = $this->prefix_mirakl.'config_value_list';
//        $sql = "SELECT id_config_value_list FROM $table WHERE id_config_value = $id_config_value AND code = '$code' ; ";
//        return $this->sql_query($sql, false, true, 'id_config_value_list');
//    }

    public function getIdConfigValueListByIdConfigValue($id_config_value) {
        $data = array();
        $table = $this->prefix_mirakl.'config_value_list';
        $sql = "SELECT  id_config_value_list, code FROM $table WHERE id_config_value = $id_config_value ; ";
        $res = $this->sql_query($sql, false);

        if (isset($res) && !empty($res)) {
            foreach ($res as $re) {
                $data[$re['id_config_value_list']] = $re['code'];
            }
        }

        return $data;
    }

    public function addValueList($data) {
        $table = $this->prefix_mirakl.'config_value_list';

        $sql = "INSERT INTO $table (id_config_value, code, label) VALUES ('{$data['id_config_value']}', '{$data['code']}', '{$data['label']}' ) ; ";
        return $this->sql_query_effect($sql, true);
    }

    public function updValueList($id_config_value_list, $data) {
        $table = $this->prefix_mirakl.'config_value_list';

        $sql = "UPDATE $table "
                ."SET code = '{$data['code']}', label = '{$data['label']}' "
                ."WHERE id_config_value_list = $id_config_value_list ";
        return $this->sql_query_effect($sql);
    }

    public function delValue($sub_marketplace, $id_field) {
        $table = $this->prefix_mirakl.'config_value';
        $table_detail = $this->prefix_mirakl.'config_value_list';

        $id_field = implode(",", $id_field);
        $sql = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_config_value NOT IN ($id_field) ; ";
        $this->sql_query_effect($sql);


        $sql_detail = "DELETE D FROM $table_detail D "
                ."LEFT JOIN $table M ON D.id_config_value = M.id_config_value "
                ."WHERE IFNULL(M.sub_marketplace,'') = '' AND "
                ."D.id_config_value NOT IN ( SELECT V.id_config_value FROM $table V WHERE sub_marketplace = $sub_marketplace ) ; ";
        return $this->sql_query_effect($sql_detail);
    }

    /* end attribute */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* additional_field */
    public function getIdConfigAdditionalField($sub_marketplace, $code) {
        $table = $this->prefix_mirakl.'config_additional_field';
        $sql = "SELECT id_additional_field FROM $table WHERE sub_marketplace = $sub_marketplace AND code = '$code' ";
        return $this->sql_query($sql, false, true, 'id_additional_field');
    }

    public function addAdditionalField($data) {
        $table = $this->prefix_mirakl.'config_additional_field';

        $sql = "INSERT INTO $table (sub_marketplace, enable, code, entity, label, required, type) "
                ."VALUES ('{$data['sub_marketplace']}', '{$data['enable']}', '{$data['code']}', '{$data['entity']}', '{$data['label']}', '{$data['required']}', '{$data['type']}' ) ; ";

        return $this->sql_query_effect($sql, true);
    }

    public function updAdditionalField($id_additional_field, $data) {
        $table = $this->prefix_mirakl.'config_additional_field';

        $sql = "UPDATE $table "
                ."SET entity = '{$data['entity']}', label = '{$data['label']}', required = '{$data['required']}', type = '{$data['type']}' "
                ."WHERE id_additional_field = $id_additional_field ; ";

        return $this->sql_query_effect($sql);
    }

    public function delAdditionalField($sub_marketplace, $id_field) {
        $table = $this->prefix_mirakl.'config_additional_field';

        $id_field = implode(",", $id_field);
        $sql = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_additional_field NOT IN ($id_field) ; ";

        return $this->sql_query_effect($sql);
    }

    /* end additional_field */

    public function get($id_config_value, $code) {
        $table = $this->prefix_mirakl.'config_value_list';
        $sql = "SELECT * FROM $table WHERE id_config_value = $id_config_value ; ";
        return $this->sql_query($sql, false);
    }

    public function add($data) {
        $table = $this->prefix_mirakl.'config_value_list';

        $sql = "INSERT INTO $table (id_config_value, code, label) VALUES ('{$data['id_config_value']}', '{$data['code']}', '{$data['label']}' ) ; ";
        return $sql;
        return $this->sql_query_effect($sql, true);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

// end definition