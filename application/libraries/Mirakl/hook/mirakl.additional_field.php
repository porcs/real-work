<?php

require_once 'hook.database.php';
require_once 'config.php';

class MiraklAdditional {

    private $get_config;
    private $hook_database;
    private $config;

    public function __construct($get_config) {
        $this->get_config = $get_config;
        $this->hook_database = new HookDatabase($this->get_config['user_name']);
        $this->config = new Config();
    }

    public function getAdditional() {

        $count = 0;
        $total = 0;
        $id_field = array();
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();

        if (isset($endpoints[$this->get_config['sub_marketplace']]) && !empty($endpoints[$this->get_config['sub_marketplace']])) {
            $result_endpoints = $endpoints[$this->get_config['sub_marketplace']];

            $results = $this->config->getAdditionalField($result_endpoints['endpoint'], $this->get_config['api_key']);
            $sub_marketplace = $result_endpoints['sub_marketplace'];

            if (!empty($results)) {

                if (isset($results->additional_fields) && !empty($results->additional_fields)) {
                    foreach ($results->additional_fields as $additional) {
                        $data = array(
                            'sub_marketplace' => $sub_marketplace,
                            'enable' => 1,
                            'code' => isset($additional->code) ? (string) $additional->code : '',
                            'entity' => isset($additional->entity) ? (string) $additional->entity : '',
                            'label' => isset($additional->label) ? $this->config->replace_quote((string) $additional->label) : '',
                            'required' => isset($additional->required) && ($additional->required == true) ? 'Y' : 'N',
                            'type' => isset($additional->type) ? (string) $additional->type : '',
                        );

                        $result = $this->setAdditionalField($data);

                        if (isset($result) && !empty($result)) {
                            $id_field[] = $result;
                            $count = $count + 1;
                        }
                        
                        $total = $total + 1;
                    }

                    if (isset($id_field) && !empty($id_field)) {
                        $this->deleteAdditionalField($sub_marketplace, $id_field);
                    }
                }
            }
        }
        print_r('get additional field config '.$total.' / '.$count);
    }

    private function setAdditionalField($data = array()) {

      
        $id_additional_field = '';
        $sub_marketplace = $data['sub_marketplace'];
        $code = $data['code'];
        $id_additional_field = $this->hook_database->getIdConfigAdditionalField($sub_marketplace, $code);

        if (isset($id_additional_field) && !empty($id_additional_field)) {
            $this->hook_database->updAdditionalField($id_additional_field, $data);
        } else {
            $id_additional_field = $this->hook_database->addAdditionalField($data);
                   
        }

        return $id_additional_field;
    }

    private function deleteAdditionalField($sub_marketplace, $id_field = array()) {
        $this->hook_database->delAdditionalField($sub_marketplace, $id_field);
    }

}
// end definition
