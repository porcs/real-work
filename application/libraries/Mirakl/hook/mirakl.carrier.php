<?php
require_once 'hook.database.php';
require_once 'config.php';

class MiraklCarrier {

    private $get_config;
    private $hook_database;
    private $config;

    public function __construct($get_config) {
        $this->get_config = $get_config;
        $this->hook_database = new HookDatabase($this->get_config['user_name']);
        $this->config = new Config();
    }

    public function getCarrier() {
        $count = 0;
        $total = 0;
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();

        if (isset($endpoints[$this->get_config['sub_marketplace']]) && !empty($endpoints[$this->get_config['sub_marketplace']])) {
            $result_endpoints = $endpoints[$this->get_config['sub_marketplace']];
            
            $results = $this->config->getCarriers($result_endpoints['endpoint'], $this->get_config['api_key']);
            $sub_marketplace = $result_endpoints['sub_marketplace'];
            
            if (!empty($results)) {
                foreach ($results->carriers as $carrier) {
                    $data = array(
                        'sub_marketplace' => $sub_marketplace,
                        'code' => isset($carrier->code) ? (string) $carrier->code : '',
                        'label' => isset($carrier->label) ?  $this->config->replace_quote((string) $carrier->label) : '',
                        'tracking_url' => isset($carrier->tracking_url) ? (string) $carrier->tracking_url : '',
                    );

                    $result = $this->setCarrier($data);

                    if (isset($result) && !empty($result)) {
                        $id_field[] = $result;
                        $count = $count + 1;
                    }
                    
                    $total = $total + 1;
                }

                if (isset($id_field) && !empty($id_field)) {
                    $this->deleteCarrier($sub_marketplace, $id_field);
                }
            }
        }

        print_r('get carrier config '.$total.' / '.$count);
    }

    private function setCarrier($data = array()) {

        $id_config_carrier = '';
        $sub_marketplace = $data['sub_marketplace'];
        $code = $data['code'];
        
        $id_config_carrier = $this->hook_database->getIdCarrier($sub_marketplace, $code);

        if(isset($id_config_carrier) && !empty($id_config_carrier)){
            $this->hook_database->updCarrier($id_config_carrier, $data);
        }else{
            $id_config_carrier = $this->hook_database->addCarrier($data);
        }

        return $id_config_carrier;
    }

    private function deleteCarrier($sub_marketplace, $id_field = array()) {
        $this->hook_database->delCarrier($sub_marketplace, $id_field);
    }

}

// end definition

