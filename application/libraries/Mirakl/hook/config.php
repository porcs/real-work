<?php

class Config {

    private $base_dir;
    private $mysql_db;

    public function __construct() {
        $this->mysql_db = new ci_db_connect();
        $this->base_dir = realpath(dirname(__FILE__).'/../settings/');
    }

    public function getApi($marketplace) {

        $sql = "SELECT * FROM offer_sub_marketplace ";
        $query = $this->mysql_db->select_query($sql);

        while ($rows = $this->mysql_db->fetch($query)) {
            $marketplace_db[strtolower($rows[1])] = $rows[0]; // id_marketplace
        }

        $base_dir = $this->base_dir.'/'.$marketplace;
        $ini_file = $base_dir.'/'.$marketplace.'.ini';

        if (!file_exists($ini_file)) {
            die('file not exist');
        }

        $marketplace_ini = parse_ini_file($ini_file);


        $data = array();
        $data['name'] = $marketplace_ini['name'];
        $data['endpoint'] = $marketplace_ini['endpoint'];

        return $data;
    }

    public function getEndpoints() {
        $marketplace_db = array();
        $endpoints = array();

        $sql = "SELECT * FROM offer_sub_marketplace WHERE id_marketplace = 6 ";
        $query = $this->mysql_db->select_query($sql);

        while ($rows = $this->mysql_db->fetch($query)) {
            $marketplace_db[strtolower($rows['name_marketplace'])] = $rows['id_offer_sub_marketplace']; // key: name marketplace, value: id_marketplace
        }

//        Example $marketplace_db
//            Array
//            (
//                [darty] => 1
//                [carrefour] => 2
//                [auchan] => 3
//                [comptoirsante] => 4
//                [delamaison] => 5
//                [elcorteingles] => 6
//                [menlook] => 7
//                [naturedecouvertes] => 8
//                [privalia] => 9
//                [rueducommerce] => 10
//                [thebeautyst] => 11
//                [truffaut] => 12
//                [mirakl] => 13
//            )

        $config_dir = dirname(__FILE__).'/config.ini';
        $config_ini = parse_ini_file($config_dir);

        foreach ($marketplace_db as $marketplace => $sub_marketplace) {

            $file_marketplace = $marketplace."_marketplace";
            $file_endpoint = $marketplace."_endpoint";
            $file_condition = $marketplace."_conditions";

            if (isset($config_ini[$file_endpoint])) {


                $endpoints[$sub_marketplace]['marketplace'] = $marketplace;
                $endpoints[$sub_marketplace]['sub_marketplace'] = $sub_marketplace;
                $endpoints[$sub_marketplace]['endpoint'] = $config_ini[$file_endpoint];
                $endpoints[$sub_marketplace]['condition'] = isset($config_ini[$file_condition]) ? $config_ini[$file_condition] : '';
            }
        }
        //echo '<pre>'; print_r($endpoints); exit;
        return $endpoints;
    }

    public function getVersionAPI($endpoint, $api_key) {
        $service_method = 'GET';
        $service_child = 'version/';
        $service_url = $endpoint.$service_child."?api_key=".$api_key;

        $file = @file_get_contents($service_url);

        if ($file == false) {
            return 0;
        }

        return json_decode($file)->version;
    }

    public function getHierarchies($endpoint, $api_key) {
        $service_method = 'GET';
        $service_child = 'hierarchies/';
        $service_url = $endpoint.$service_child."?api_key=".$api_key;

        $file = @file_get_contents($service_url);

        if ($file == false) {
            return 0;
        }

        if ($this->isJson($file)) {
            $obj = json_decode($file);
        } else {
            $obj = simplexml_load_string($file);
        }

        return $obj;
    }

    public function getCarriers($endpoint, $api_key) {
        $service_method = 'GET';
        $service_child = 'shipping/carriers/';
        $service_url = $endpoint.$service_child."?api_key=".$api_key;

        $file = @file_get_contents($service_url);

        if ($file == false) {
            return 0;
        }

        if ($this->isJson($file)) {
            $obj = json_decode($file);
        } else {
            $obj = simplexml_load_string($file);
        }

        return $obj;
    }

    public function getProductsAttbiutes($endpoint, $api_key) {
        $service_method = 'GET';
        $service_child = 'products/attributes';
        $service_url = $endpoint.$service_child."?api_key=".$api_key;

        $file = @file_get_contents($service_url);

        if ($file == false) {
            return 0;
        }

        if ($this->isJson($file)) {
            $obj = json_decode($file);
        } else {
            $obj = simplexml_load_string($file);
        }

        return $obj;
    }

    public function getAdditionalField($endpoint, $api_key) {
        $service_method = 'GET';
        $service_child = 'additional_fields';
        $service_url = $endpoint.$service_child."?api_key=".$api_key;

        $file = @file_get_contents($service_url);

        if ($file == false) {
            return 0;
        }

        if ($this->isJson($file)) {
            $obj = json_decode($file);
        } else {
            $obj = simplexml_load_string($file);
        }

        return $obj;
    }

    public function getValuesList($endpoint, $api_key, $values_list = '') {
        $service_method = 'GET';

        if (!empty($values_list)) { // old version
            $service_child = 'products/attributes/values_list/';
            $service_code = 'P31';
            $values_list = str_replace(' ', '%20', $values_list);
            $service_url = $endpoint.$service_child.$values_list."?api_key=".$api_key;
        } else {  // new version
            $service_child = 'values_lists';
            $service_code = 'VL11';
            $service_url = $endpoint.$service_child."?api_key=".$api_key;
        }

        // https://moa-recette.mirakl.net/api/products/attributes/values_list/COLOR?api_key=c140c926-e4ae-46a2-86d9-cf1d8dbb2e3c
        //  https://moa-recette.mirakl.net/api/products/attributes/values_list/vat?api_key=c140c926-e4ae-46a2-86d9-cf1d8dbb2e3c
        //print_r($service_url); exit;
        $file = @file_get_contents($service_url);
        //print_r($file); exit;
        if ($file == false) {
            return 0;
        }

        if ($this->isJson($file)) {
            $obj = json_decode($file);
        } else {
            $obj = simplexml_load_string($file);
        }

        return $obj;
    }

    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public function replace_quote($str) {
        return str_replace("'", "\'", $str);
    }

}

// end defintion
