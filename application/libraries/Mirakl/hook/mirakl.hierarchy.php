<?php

require_once 'hook.database.php';
require_once 'config.php';

class MiraklHierarchy {

    private $get_config;
    private $hook_database;
    private $config;

    public function __construct($get_config) {
        $this->get_config = $get_config;
        $this->hook_database = new HookDatabase($this->get_config['user_name']);
        $this->config = new Config();
    }

    public function getHierarchy() {
        $count = 0;
        $total = 0;
        $endpoints = array();
        $endpoints = $this->config->getEndpoints();

        if (isset($endpoints[$this->get_config['sub_marketplace']]) && !empty($endpoints[$this->get_config['sub_marketplace']])) {
            $result_endpoints = $endpoints[$this->get_config['sub_marketplace']];

            //$version_api = $this->config->getVersionAPI($result_endpoints['endpoint'], $this->get_config['api_key']);

            $results = $this->config->getHierarchies($result_endpoints['endpoint'], $this->get_config['api_key']);
            $sub_marketplace = $result_endpoints['sub_marketplace'];

//            echo '<pre>';
//            print_r($results);
//            exit;

            /* version api 3.22 can get label
             * version api 3.23 change label, can read label in (label_translations -> label_translation -> value)
             */

            if (!empty($results)) {
                foreach ($results->hierarchies->hierarchy as $hierarchy) {

                    $data = array(
                        'sub_marketplace' => $sub_marketplace,
                        'code' => isset($hierarchy->code) ? (string) $hierarchy->code : '',
                        'label' => isset($hierarchy->label) ? $this->config->replace_quote((string) $hierarchy->label) : '', // ?   isset($hierarchy->label_translations->label_translation->value) ? $this->config->replace_quote((string) $hierarchy->label_translations->label_translation->value) : '',
                        'level' => isset($hierarchy->level) ? (int) $hierarchy->level : 0,
                        'parent_code' => isset($hierarchy->parent_code) ? (string) ($hierarchy->parent_code) : ''
                    );

                    $result = $this->setHierarchy($data);

                    if (isset($result) && !empty($result)) {
                        $id_field[] = $result;
                        $count = $count + 1;
                    }

                    $total = $total + 1;
                }

                if (isset($id_field) && !empty($id_field)) {
                    $this->deleteHierarchy($sub_marketplace, $id_field);
                }
            }
        }

        print_r('get hierarchy config '.$total.' / '.$count);
    }

    private function setHierarchy($data = array()) {

        $id_config_hierarchy = '';
        $sub_marketplace = $data['sub_marketplace'];
        $code = $data['code'];
        $level = $data['level'];

        $id_config_hierarchy = $this->hook_database->getIdHierarchy($sub_marketplace, $code, $level);

        if (isset($id_config_hierarchy) && !empty($id_config_hierarchy)) {
            $this->hook_database->updHierarchy($id_config_hierarchy, $data);
        } else {
            $id_config_hierarchy = $this->hook_database->addHierarchy($data);
        }

        return $id_config_hierarchy;
    }

    private function deleteHierarchy($sub_marketplace, $id_field = array()) {
        $this->hook_database->delHierarchy($sub_marketplace, $id_field);
    }

}

// end definition
