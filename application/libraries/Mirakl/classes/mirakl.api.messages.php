<?php

require_once dirname(__FILE__).'/mirakl.webservice.class.php';

class MiraklApiMessages extends MiraklWebservice {

    public function __construct($marketplace_params) {
        parent::__construct($marketplace_params);
        $this->service = "messages";
    }

// end func

    public function messages($filters = array()) {
        /*         * *****************************************************************************************
         * Descripton:    List messages for orders and offers
         * QUERY PARAMETERS:
          customer_id - [optional] Returns messages concerned by this "customer_id".
          start_date - [optional] Creation date for filtering. Format: "yyyy-MM-dd'T'HH:mm:ss"
          end_date - [optional] Creation date for filtering. Format: "yyyy-MM-dd'T'HH:mm:ss"
          offer_id - [optional] The identifier of the offer.
          order_id - [optional] The identifier of the order.
          read - [optional] Ignored, will be removed in next version.
          archived - [optional] Will be removed in next version.
          received - [optional] "ALL", "FALSE" or "TRUE" (default). If "TRUE" ("FALSE") returns only messages received by (sent to) shop.
          visible - [optional] "ALL", "TRUE" (default) or "FALSE". If "TRUE" ("FALSE") returns only the visible (not visible) messages.
          shop_id - This parameter should be used when your user has access to multiple shops. If not mentioned, the shop_id from your default shop will be used.
         *    Example: GET /api/messages?customer_id=…&start_date=…&end_date=…&offer_id=…&order_id=…&read=…&archived=…&received=…&visible=…
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = null;
        $this->service_code = 'M01';

//        if (!isset($filters['customer_id']) || empty($filters['customer_id'])) {
//            return $this->errors($code = 10, $cause = 'CUSTOMER_ID_MISSING');
//        }
//
//        if (!isset($filters['start_date']) || empty($filters['start_date'])) {
//            return $this->errors($code = 10, $cause = 'START_DATE_MISSING');
//        }

        $params = '';
        if (isset($filters['customer_id']) && !empty($filters['customer_id']))
            $params .= "&customer_id={$filters['customer_id']}";
        if (isset($filters['start_date']) && !empty($filters['start_date']))
            $params .= "&start_date={$filters['start_date']}";
        if (isset($filters['end_date']) && !empty($filters['end_date']))
            $params .= "&end_date={$filters['end_date']}";
        if (isset($filters['offer_id']) && !empty($filters['offer_id']))
            $params .= "&offer_id={$filters['offer_id']}";
        if (isset($filters['order_id']) && !empty($filters['order_id']))
            $params .= "&order_id={$filters['order_id']}";
        if (isset($filters['received']) && !empty($filters['received']))
            $params .= "&received={$filters['received']}";
        if (isset($filters['visible']) && !empty($filters['visible']))
            $params .= "&visible={$filters['visible']}";

        $this->service_url = $this->endpoint.$this->service.'/?api_key='.$this->api_key.$params;
       //$this->service_url = "https://drt-prod.mirakl.net/api/messages/?api_key=3c67b2c5-ca6a-4800-b931-f2533ccfe5b5"; // exit;

        return $this->parse($this->get());
    }

// end func
}

// end class definition