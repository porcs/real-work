<?php

class Mirakl_Tools {

    public $lang;

    const DEF_LANG_FILE = 'mirakl_lang.php';

    public static function xml2array($contents, $get_attributes = 1, $priority = 'tag') {
        /*  XML 2 ARRAY
          Event handler called by the expat library when an element's end tag is encountered.
         */

        if (!$contents)
            return array();

        if (!function_exists('xml_parser_create')) {
            //print "'xml_parser_create()' function not found!";
            return array();
        }

        //Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
        # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values)
            return; // Hmm...













            
//Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array; // Refference
        //Go through the tags.
        $repeated_tag_index = array(); //Multiple tags with same name will be turned into an array

        foreach ($xml_values as $data) {
            unset($attributes, $value); //Remove existing values, or there will be trouble
            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data); //We could use the array by itself, but this cooler.

            $result = array();
            $attributes_data = array();

            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
            }

            //Set the attributes too.
            if (isset($attributes) && $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }

            //See tag status and do the needed.
            if ($type == 'open') {
                //The starting of the tag '<tag>'
                $parent[$level - 1] = &$current;
                if (!is_array($current) || (!in_array($tag, array_keys($current)))) {
                    //Insert New tag
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag.'_attr'] = $attributes_data;

                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    $current = &$current[$tag];
                }
                else {
                    //There was another element with the same tag name
                    if (isset($current[$tag][0])) {
                        //If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                        $repeated_tag_index[$tag.'_'.$level] ++;
                    } else {//This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array($current[$tag], $result); //This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag.'_'.$level] = 2;

                        if (isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }
                    }
                    $last_item_index = $repeated_tag_index[$tag.'_'.$level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }
            } elseif ($type == 'complete') { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                // New Key
                if (!isset($current[$tag])) {
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if ($priority == 'tag' && $attributes_data)
                        $current[$tag.'_attr'] = $attributes_data;
                }
                //If taken, put all things inside a list(array)
                else {
                    if (isset($current[$tag][0]) && is_array($current[$tag])) {
                        //If it is already an array...
                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

                        if ($priority == 'tag' && $get_attributes && $attributes_data)
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level].'_attr'] = $attributes_data;

                        $repeated_tag_index[$tag.'_'.$level] ++;
                    }
                    else { //If it is not an array...
                        $current[$tag] = array($current[$tag], $result); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag.'_'.$level] = 1;

                        if ($priority == 'tag' && $get_attributes) {
                            if (isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                                $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                                unset($current[$tag.'_attr']);
                            }

                            if ($attributes_data)
                                $current[$tag][$repeated_tag_index[$tag.'_'.$level].'_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag.'_'.$level] ++; //0 and 1 index is already taken
                    }
                }
            }
            //End of tag '</tag>'
            elseif ($type == 'close')
                $current = &$parent[$level - 1];
        }

        return $xml_array;
    }

    public static function displayDate($date) {
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 5) {

        if ($stream_context == null && preg_match('/^https?:\/\//', $url)) {
            $stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)));
        }
        if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url)) {
            return @file_get_contents($url, $use_include_path, $stream_context);
        } elseif (function_exists('curl_init')) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            if ($stream_context != null) {
                $opts = stream_context_get_options($stream_context);
                if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post') {
                    curl_setopt($curl, CURLOPT_POST, true);
                    if (isset($opts['http']['content'])) {
                        parse_str($opts['http']['content'], $datas);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
                    }
                }
            }
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        } else {
            return false;
        }
    }

    public static function getValue($key, $default_value = false) {
        if (!isset($key) || empty($key) || !is_string($key))
            return false;
        $ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));

        if (is_string($ret) === true)
            $ret = urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret)));
        return !is_string($ret) ? $ret : stripslashes($ret);
    }

    public static function strip_html($html) {
        $text = strip_tags($html, '<br>');
        $text = preg_replace('#<br\s{0,}/{0,}>#i', "\n", $text); // br to newline
        $text = preg_replace('#[\n]+#i', "\n", $text); // multiple-return
        $text = preg_replace('#^[\n\r\s]+#i', "", $text); // trim
        $text = str_replace("\n", "<br />\n", $text); // newline to br        
        return(trim($text));
    }

    public static function remove_html_tag($html) {
        $text = strip_tags($html, '<br>');
        $text = preg_replace('#<br\s{0,}/{0,}>#i', "\n", $text); // br to newline
        $text = preg_replace('#[\n]+#i', "\n", $text); // multiple-return
        $text = preg_replace('#^[\n\r\s]+#i', "", $text); // trim
        $text = str_replace("\n", "", $text); // newline to br        
        return(trim($text));
    }

    public static function isUSMarketplaceId($marketplaceID) {
        return( trim($marketplaceID) == 'ATVPDKIKX0DER' );
    }

    public static function EAN_UPC_Check($code) {
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;
        return ((int) $code == (int) ($digits.$check_digit));
    }

    public static function load($language) {

        $langfile = self::DEF_LANG_FILE; //'mirakl_lang.php';
        $language = strtolower($language);



        if (file_exists(dirname(__FILE__).'/../../../'.'language/'.$language.'/'.$langfile)) {
            include(dirname(__FILE__).'/../../../'.'language/'.$language.'/'.$langfile);
        } else {
            return;
        }

        if (!isset($lang) || empty($lang)) {
            return;
        }

        global $l;
        $l = $lang;
    }

    public static function isLocal() {
        //return !checkdnsrr($_SERVER['SERVER_NAME'], 'NS');
        if (isset($_SERVER['SERVER_ADDR'])) { // node not exists SERVER_ADDR
            $ips = explode(".", $_SERVER['SERVER_ADDR']);
            return $ips[0] == '127' ? true : false;
        }
        return false;
    }

    public static function l($line) {

        global $l;

        if (!isset($l[$line]) && !self::isLocal()) {
            self::line($line);
        }

        return (empty($line) || !isset($l[$line])) ? $line : $l[$line];
    }

    private static function line($line) {

        global $l, $loaded_lang_file;
        if (!is_numeric($line) && !is_string($line)) {
            return $line;
        }if ((empty($line) || !isset($l[$line]))) {
            $file = self::DEF_LANG_FILE;
            if (!empty($line)) {
                if (function_exists('get_instance')) {
                    $ci = get_instance();

                    $result = $ci->db->query("SHOW TABLES LIKE 'crowdin_missing_word'");
                    //$tableExists = mysql_num_rows($result) > 0;
                    if ($result->num_rows > 0) {
                        $session_key = date("Y-m-d H:i:s");
                        $data = array('id_key' => md5($line.$file), 'missing_word' => $line, 'target_file' => $file);
                        $insert_query = $ci->db->insert_string('crowdin_missing_word', $data);
                        $insert_query = str_replace('INSERT INTO', 'REPLACE INTO', $insert_query);
                        $ci->db->query($insert_query);
                    }
                } else {
                    $crowdin = '/var/www/backend/script/crowdin/config.php';
                    if (file_exists($crowdin)) {
                        include $crowdin;
                        $line = mysqli_escape_string($dbx, $line);
                        $sql = "replace into crowdin_missing_word (id_key,missing_word,target_file) values ('".md5($line.$file)."','".$line."','".$file."')";
                        mysqli_query($dbx, $sql);
                    }
                }
            }
            return $line;
        } else {
            return $l[$line];
        }
    }

    // --------------------------------------------------------------------
//    /**
//     * Fetch a single line of text from the language array
//     *
//     * @access	public
//     * @param	string	$line	the language line
//     * @return	string
//     */
//    function check_file_line($file, $line) {
//        return isset($this->language_by_file[$file][$line]);
//    }
//
//    public static function line($line = '', $file = '') {
//        
////        if (!is_string($line))
////            return '';
////        if (!empty($file)) {
////            $value = ($line == '' OR ! isset($this->language_by_file[$file]) OR ! isset($this->language_by_file[$file][$line])) ? FALSE : $this->language[$line];
////        } else {
////            $value = ($line == '' OR ! isset($this->language[$line])) ? FALSE : $this->language[$line];
////        }
//
//        // Because killer robots like unicorns!
//        //if ($value === FALSE) {
//            $url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
//            $ref = '';
//            log_message('error', 'Could not find the language line "'.$line.'" @ '.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
//            if (isset($_SERVER["HTTP_REFERER"])) {
//                $ref = $_SERVER["HTTP_HOST"].$_SERVER["HTTP_REFERER"];
//                log_message('error', 'Referer From : '.$_SERVER["HTTP_HOST"].$_SERVER["HTTP_REFERER"]);
//            }
//
//            if (!empty($line) && !empty($url)) {
//                $ci = get_instance();
//                $session_key = date("Y-m-d H:i:s");
//                $data = array('id_key' => md5($line.$url), 'missing_word' => $line, 'url' => $url, 'ref' => $ref, 'last_update' => $session_key);
//                if (!empty($file)) {
//                    $data['target_file'] = $file.'_lang.php';
////                             $value = $line;
//                }
//                $value = $line;
//                $insert_query = $ci->db->insert_string('crowdin_missing_word', $data);
//                $insert_query = str_replace('INSERT INTO', 'REPLACE INTO', $insert_query);
//                $ci->db->query($insert_query);
//            }
//       // } else {
////                    $url =  $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
////                    $url = str_replace('//','/',$url);
////                    $ref='';
////                    if(isset($_SERVER["HTTP_REFERER"])){
////                        $ref =  $_SERVER["HTTP_REFERER"];
////                    }
////                        $ci = get_instance();
////                        $session_key = date("Y-m-d H:i:s");
////                        $data = array('id_key'=>md5($line.$url), 'key_full'=>$line,'value'=>$value,'url'=>$url,'ref'=>$ref,'last_update'=>$session_key);
////                        $insert_query = $ci->db->insert_string('crowdin_check_use_word', $data);
////                        $insert_query = str_replace('INSERT INTO','REPLACE INTO',$insert_query);
////                        $ci->db->query($insert_query);
//      //  }
//
//        return $value;
//    }
//    


    public static function rstr($value) {
        $str = $value;
        if (!empty($value)) {
            if (is_array($value)) {
                foreach ($value as $key => $val) {
                    $str[$key] = Amazon_Tools::rstr($val);
                }
            } else if (is_int($value)) {
                $str = trim($value);
            } else if (is_float($value)) {
                $str = trim(number_format($value, 2));
            } else if (is_string($value)) {
                $str = trim(htmlspecialchars($value, ENT_QUOTES, 'UTF-8'));
            } else {
                $str = trim(htmlspecialchars($value, ENT_QUOTES, 'UTF-8'));
            }
        }
        return $str;
    }

    public static function ValidateASIN($ASIN) {
        return( $ASIN != null && strlen($ASIN) && preg_match('/[A-Z0-9]{10}/', $ASIN) );
    }

    public static function ValidateSKU($SKU) {
        return( $SKU != null && strlen($SKU) && preg_match('/[\x00-\xFF]{1,40}/', $SKU) && preg_match('/[^ ]$/', $SKU) );
    }

    public static function EAN_UPC_isPrivate($code) {
        return(in_array(substr(sprintf('%013s', $code), 0, 1), array('2')));
    }

    public static function split_words($string, $max = 1) {
        $words = preg_split('/\s/', $string);
        $lines = array();
        $line = '';

        foreach ($words as $k => $word) {
            $length = strlen($line.' '.$word);
            //echo '<pre>' . print_r($word, true) . '</pre>';
            if (strpos($word, '.')) {
                if (!empty($line)) {
                    $lines[] = trim($line).' '.$word;
                }
                $line = '';
            } else if ($length <= $max) {
                $line .= ' '.$word;
            } else if ($length > $max) {
                if (!empty($line)) {
                    $lines[] = trim($line);
                }
                $line = $word;
            } else {
                $lines[] = trim($line).' '.$word;
                $line = '';
            }
        }
        if (empty($lines) && strlen($line))
            $lines[] = ($line = trim($line)) ? $line : $word;


        return $lines;
    }

    public static function getMarketplaceTags($tags, $lang) {
        $tags_array = array();
        if (!isset($tags[$lang]) || empty($tags[$lang]))
            return;

        $tag_s = $tags[$lang];
        if (isset($tag_s) && !empty($tag_s)) {
            $tags_array = explode(PHP_EOL, wordwrap(trim(Amazon_Tools::encodeText($tag_s)), 50, PHP_EOL, FALSE));
            if (isset($tags_array) && is_array($tags_array) && !empty($tags_array))
                return (array_slice($tags_array, 0, 5));
        }
        return $tags_array;
    }

    public static function encodeText($string, $verySafe = false) {
        if ($verySafe) {
            $string = str_replace('’', "'", $string);
            $string = @utf8_encode(utf8_decode($string));
            $string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
            $string = str_replace('&#39;', "'", $string);
        }

        return (trim($string));
    }

    public static function stripInvalidXml($value) {
        $ret = '';
        $current = null;
        if (empty($value))
            return $ret;

        $length = strlen($value); //TODO: Multibyte dance, do not replace by Tools::strlen !
        for ($i = 0; $i < $length; $i++) {
            $current = ord($value{$i});
            if (($current == 0x9) || ($current == 0xA) || ($current == 0xD) || (($current >= 0x20) && ($current <= 0xD7FF)) || (($current >= 0xE000) && ($current <= 0xFFFD)) || (($current >= 0x10000) && ($current <= 0x10FFFF)))
                $ret .= chr($current);
            else
                $ret .= ' ';
        }

        return $ret;
    }

    public static function escape_str($str) {
        if (isset($str) && !empty($str)) {
            if (is_array($str)) {
                foreach ($str as $key => $val)
                    $str[$key] = $this->escape_str($val);
                return $str;
            }

            $str = sqlite_escape_string($str);
            return $str;
        }

        return '';
    }

    public static function spacify_str($world, $glue = ' ') {
        return preg_replace('/([a-z0-9])([A-Z])/', "$1$glue$2", $world);
    }

    public static function ceil_time($time) {
        date_default_timezone_set('UTC');
        $date = strtotime(date('Y-m-d', $time));
        $chk_date = strtotime(date('Y-m-d', $time + 43200));
        if ($date == $chk_date) {
            $out = $date;
        } else {
            $out = $chk_date;
        }
        return $out;
    }

    public static function floor_time($time) {
        date_default_timezone_set('UTC');
        $date = strtotime(date('Y-m-d', $time));
        $chk_date = strtotime(date('Y-m-d', $time + 43200));
        if ($date == $chk_date) {
            $out = $date;
        } else {
            $out = $date;
        }
        return $out;
    }

    /* Valid Value */ // 25-06-2015

    public static function ucfirst($str) {
        return strtoupper(substr($str, 0, 1)).substr($str, 1);
    }

    public static function toKey($str) {

        $str = str_replace(
                array('-', ',', '.', '/', '+', '.', ':', ';', '>', '<', '?', '(', ')', '!'), array('_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm'), $str);

        return strtolower(preg_replace('/[^A-Za-z0-9_]/', '', $str));
    }

    public static function domainToId($domain) {

        $domain = ltrim($domain, ".");
        switch ($domain) {
            case 'co.uk' :
                return ('uk');
            case 'com' :
                return ('us');
            default:
                return ($domain);
        }
    }

    public static function is_serialized($value, &$result = null) {
        if (!is_string($value)) {
            return false;
        }
        if ($value === 'b:0;') {
            $result = false;
            return true;
        }
        $length = strlen($value);
        $end = '';
        if (isset($value[0])) {
            switch ($value[0]) {
                case 's': if ($value[$length - 2] !== '"')
                        return false;
                case 'b':
                case 'i':
                case 'd': $end .= ';';
                case 'a':
                case 'O': $end .= '}';
                    if ($value[1] !== ':')
                        return false;
                    switch ($value[2]) {
                        case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9: break;
                        default: return false;
                    }
                case 'N': $end .= ';';
                    if ($value[$length - 1] !== $end[0]) {
                        return false;
                    }
                    break;
                default:
                    return false;
            }
        }
        if (($result = @unserialize($value)) === false) {
            $result = null;
            return false;
        }
        return true;
    }

    public static function cleanNonUnicodeSupport($pattern) {
        if (!defined('PREG_BAD_UTF8_OFFSET'))
            return $pattern;
        return preg_replace('/\\\[px]\{[a-z]{1,2}\}|(\/[a-z]*)u([a-z]*)$/i', "$1$2", $pattern);
    }

    public static function isEmail($email) {
        return !empty($email) && preg_match(self::cleanNonUnicodeSupport('/^[a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]+[.a-z\p{L}0-9!#$%&\'*+\/=?^`{}|~_-]*@[a-z\p{L}0-9]+(?:[.]?[_a-z\p{L}0-9-])*\.[a-z\p{L}0-9]+$/ui'), $email);
    }

    public static function strtolower($str) {
        if (is_array($str))
            return false;
        if (function_exists('mb_strtolower'))
            return mb_strtolower($str, 'utf-8');
        return strtolower($str);
    }

    public static function jsonEncode($data) {
        if (function_exists('json_encode')) {
            return json_encode($data);
        } else {
            include_once(_PS_TOOL_DIR_.'json/json.php');
            $pear_json = new Services_JSON();
            return $pear_json->encode($data);
        }
    }

    public static function strlen($str, $encoding = 'UTF-8') {
        if (is_array($str))
            return false;
        $str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
        if (function_exists('mb_strlen'))
            return mb_strlen($str, $encoding);
        return strlen($str);
    }

    public static function eanupcCheck($code) {
        // Source : http://www.edmondscommerce.co.uk/php/ean13-barcode-check-digit-with-php/
        // Many thanks ;)
        //first change digits to a string so that we can access individual numbers
        $digits = sprintf('%012s', self::substr(sprintf('%013s', $code), 0, 12));
        // 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
        $even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
        // 2. Multiply this result by 3.
        $even_sum_three = $even_sum * 3;
        // 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
        $odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
        // 4. Sum the results of steps 2 and 3.
        $total_sum = $even_sum_three + $odd_sum;
        // 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
        $next_ten = (ceil($total_sum / 10)) * 10;
        $check_digit = $next_ten - $total_sum;

        return ((int) $code == (int) ($digits.$check_digit));
    }

    public static function substr($str, $start, $length = false, $encoding = 'utf-8') {
        if (is_array($str))
            return false;
        if (function_exists('mb_substr'))
            return mb_substr($str, (int) $start, ($length === false ? self::strlen($str) : (int) $length), $encoding);
        return substr($str, $start, ($length === false ? self::strlen($str) : (int) $length));
    }

    public static function formatDescription($html, $decription_html = false) {
        $text = self::stripInvalidXml($html);

        if ($decription_html) {
            $text = iconv('UTF-8', 'UTF-8//TRANSLIT', $text);
            $text = str_replace('&#39;', "'", $text);
            $text = str_replace('&', '&amp;', $text);
        } else {
            $text = str_replace('</p>', "\n</p>", $html);
            $text = str_replace('</li>', "\n</li>", $text);
            $text = str_replace('<br', "\n<br", $text);
            $text = strip_tags($text);

            $text = iconv('UTF-8', 'UTF-8//TRANSLIT', $text);
            $text = str_replace('&#39;', "'", $text);

            $text = mb_convert_encoding($text, 'HTML-ENTITIES');
            $text = str_replace('&nbsp;', ' ', $text);
            $text = html_entity_decode($text, ENT_NOQUOTES, 'UTF-8');
            $text = str_replace('&', '&amp;', $text);

            $text = preg_replace('#\s+[\n|\r]+$#i', '', $text); // empty
            $text = preg_replace('#[\n|\r]+#i', "\n", $text); // multiple-return
            $text = preg_replace('#\n+#i', "\n", $text); // multiple-return
            $text = preg_replace('#^[\n\r\s]#i', '', $text);
        }

        return trim($text);
    }

    public static function price_rule($price_rule, $rounding, $price) {
        foreach ($price_rule as $rule_key => $rule_value) {
            if ($price <= $rule_value['to'] && $price >= $rule_value['from']) {

                $value = (float) $rule_value['value'];

                if ($rule_value['type'] == "percent") {
                    $price = $price + ( ($price * ($value) ) / 100 );
                } else if ($rule_value['type'] == "value") {
                    $price = $price + ($value);
                }
            }
        }

        if ($rounding == 0) {
            return $price;
        } else {
            return round($price, $rounding);
        }
    }

    public static function strtoupper($str) {
        if (is_array($str))
            return false;
        if (function_exists('mb_strtoupper'))
            return mb_strtoupper($str, 'utf-8');
        return strtoupper($str);
    }

    // http://wpscholar.com/blog/filter-multidimensional-array-php/
    public static function array_filter_recursive(array $array, callable $callback = null) {
        $array = is_callable($callback) ? array_filter($array, $callback) : array_filter($array);
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = call_user_func(array(__CLASS__, __FUNCTION__), $value, $callback);
            }
        }
        return $array;
    }

}
