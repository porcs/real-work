<?php

require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__).'/../../../../assets/apps/FeedbizImport/log/RecordProcess.php');

class MiraklHistoryLog {

    const ACTION_TYPE_EXPORT_PRODUCT = 'export_products';
    const ACTION_TYPE_EXPORT_OFFER = 'export_offers';
    const ACTION_TYPE_DELETE_PRODUCT = 'delete_products';
    const ACTION_TYPE_ACCEPT_ORDER = 'accept_orders';
    const ACTION_TYPE_IMPORT_ORDER = 'import_orders';
    const ACTION_TYPE_UPDATE_TRACKING = 'update_tracking';
    
    public static function getConfig($type){
        /* additional | attribute | condition | hierarchy | value */
        return 'config_'.$type;
    }

    public static function set($data) {
        $mysql_db = new ci_db_connect();
        $type = $data['action'];
        $type = $type."_mirakl_{$data['sub_marketplace']}_{$data['ext']}";
        $batch_id = 'check exist other process of '.$data['countries'].' running';

        $other = array(
            'next_time' => strtotime("+{$data['next_time']} minutes"),
            'transaction' => $data['transaction']
        );

        $data_history = array(
            'user_id' => $data['user_id'],
            'user_name' => $data['owner'],
            'history_action' => $type,
            'history_date_time' => date('Y-m-d H:i:s'),
            'history_table_name' => $data['shop_name'],
            'history_data' => json_encode($other),
        );

        $mysql_db->add_db('histories', $data_history);
        $batch_id = 'check exist mirakl running';
    }

    public static function startPopup($popup_data) {

        if($popup_data['action'] == self::ACTION_TYPE_IMPORT_ORDER){
            $action_name = 'import - orders';
            $action_type = 'orders';
        }else if($popup_data['action'] == self::ACTION_TYPE_EXPORT_OFFER){
            $action_name = 'export - offers';
            $action_type = 'offers';
        }else if($popup_data['action'] == self::ACTION_TYPE_EXPORT_PRODUCT){
            $action_name = 'export - products';
            $action_type = 'product';
        }else if($popup_data['action'] == self::ACTION_TYPE_DELETE_PRODUCT){
            $action_name = 'delete - products';
            $action_type = 'product';
        }else if($popup_data['action'] == self::ACTION_TYPE_UPDATE_TRACKING){
            $action_name = 'update status - orders';
            $action_type = 'orders';
        }else if($popup_data['action'] == self::ACTION_TYPE_ACCEPT_ORDER){
            $action_name = 'accept - orders';
            $action_type = 'orders';
        }else{
            die('No declare action type');
        }
        
        $process_name = sprintf(Mirakl_Tools::l('SS_s_started_on_s'), ucfirst($popup_data['marketplace']), $popup_data['ext'], $action_name, '..');
        $process_title = $action_name. ' processing..';
        $process_type = $action_name.$popup_data['country'];

        $message = new stdClass();
        $message->batch_id = $popup_data['batch_id']; 
        $message->title = $process_title;
        $message->date = date('Y-m-d', strtotime($popup_data['date_time']));
        $message->time = date('H:i:s', strtotime($popup_data['date_time']));
        $message->type = $process_name;
        $message->$action_type = new stdClass(); //$message->orders = new stdClass(); eg, orders, offers, products

        $proc_rep = new report_process($popup_data['user_name'], $popup_data['batch_id'], $process_title, true, false, true);
        $proc_rep->set_process_type($process_type); // need into date and time $message['type']
        $proc_rep->set_popup_display(true);
        $proc_rep->set_marketplace($popup_data['marketplace']);
        $process = $proc_rep->has_other_running($process_type);
        
        if ($process) {
            $this->message->error = sprintf(Mirakl_Tools::l('s_products_are_running_you_can_not_run_this_process_again_just_wait_withing_s_minutes'), $process_running, '20');
            $this->proc_rep->set_error_msg($this->message);
            $this->proc_rep->finish_task();
            echo json_encode(array('status' => 'error', 'process' => $process_running, 'message' => 'process are running, '.$this->operation_type.' until old process done'));
            die();
        }
        
        $popup_return = array('message' => $message, 'proc_rep' => $proc_rep);
        return $popup_return;
    }
}

// end definition
