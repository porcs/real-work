<?php

require_once dirname(__FILE__).'/mirakl.webservice.class.php';

class MiraklApiOffers extends MiraklWebservice {

    public function __construct($parameters) {
        parent::__construct($parameters);
        $this->service = "offers";
    }

// end func

    public function imports($params = array()) {

        /*         * *****************************************************************************************
         * Descripton:    List messages for orders and offers
         * ==========================================================================================
         * Params:    @file - [required] Import file to upload. Use multipart/form-data with name 'file'.
         * @import_mode - [required] [NORMAL, PARTIAL_UPDATE, REPLACE]
         * @with_products - [optional] This file also contains product information. Default : false
         * ***************************************************************************************** */
        $this->service_method = 'POST';
        $this->service_child = 'imports';
        $this->service_code = 'OF01';

        if (!$params['file'] || !file_exists($params['file'])) {
            return $this->errors($code = 10, $cause = 'SOURCE_FILE_MISSING');
        }

        if (!isset($params['import_mode']))
            $params['import_mode'] = '';

        $upload_params = array();
        //$upload_params['file'] = "@{$params['file']}";
        if (version_compare(phpversion(), '5.4.7', '<')) {
            $upload_params['file'] = "@{$params['file']}";
        } else {
            $upload_params['file'] = new CurlFile($params['file'], 'text/csv');
        }
        $upload_params['import_mode'] = urlencode(MiraklHelperQuery::params($this->service.'/imports', 'import_mode', $params['import_mode']));
        if (isset($params['with_products']) && !empty($params['with_products']))
            $upload_params['with_products'] = $params['with_products'];

        $this->service_url = $this->endpoint."{$this->service}/{$this->service_child}/?api_key=".$this->api_key;

        return $this->parse($this->post($request_type = 'csv', $upload_params));
    }

// end func

    public function imports_info($import_id = 0) {
        /*         * *****************************************************************************************
         * Descripton:    Get import information and stats
         * ==========================================================================================
         * Params:    @import - [required] The identifier of the import
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = 'imports';
        $this->service_code = 'OF02';

        if (!$import_id) {
            return $this->errors($code = 10, $cause = 'IMPORT_ID_MISSING');
        }

        //$this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$this->service_child}/{$import_id}?api_key=".$this->api_key;
        $this->service_url = $this->endpoint."{$this->service}/{$this->service_child}/{$import_id}?api_key=".$this->api_key;

        //return $this->parse($this->get());
        return $this->get();
    }

// end func

    public function error_report($import_id = 0, $filename = '') {
        /*         * *****************************************************************************************
         * Descripton:    Get error report file for an import
         * ==========================================================================================
         * Params:    @import - [required] The identifier of the import
         *            @$filename - destination file to save error report
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = 'imports';
        $this->service_code = 'OF03';

        if (!$import_id) {
            return $this->errors($code = 10, $cause = 'IMPORT_ID_MISSING');
        }

        if (empty($filename)) {
            return $this->errors($code = 10, $cause = 'FILE_MISSING');
        }

        //$this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$this->service_child}/{$import_id}/error_report/?api_key=".$this->api_key;
        $this->service_url = $this->endpoint."{$this->service}/{$this->service_child}/{$import_id}/error_report/?api_key=".$this->api_key;
        return $this->parse($this->get($filename));
    }

// end func

    public function get_api_error_report($import_id = 0) {
        $this->service_method = 'GET';
        $this->service_child = 'imports';
        $this->service_code = 'OF03';

        if (!$import_id) {
            return $this->errors($code = 10, $cause = 'IMPORT_ID_MISSING');
        }


        //$this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$this->service_child}/{$import_id}/error_report/?api_key=".$this->api_key;
        $this->service_url = $this->endpoint."{$this->service}/{$this->service_child}/{$import_id}/error_report/?api_key=".$this->api_key;
        return $this->service_url;
    }

// end func

    public function offers($filters = array()) {
        /*         * *****************************************************************************************
         * Descripton:    List messages for orders and offers
         * ==========================================================================================
         * Params:    @offer_state_codes - [optional] List of offer state codes.
         * @sku - [optional] Offer's sku.
         * @product_id - [optional] Product's sku.
         * @favorite - [optional] filter only the favorite offers. (default false)
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = null;
        $this->service_code = 'OF21';

        $params = '';
        if (isset($filters['offer_state_codes']) && !empty($filters['offer_state_codes']))
            $params .= "&offer_state_codes={$filters['offer_state_codes']}";
        if (isset($filters['sku']) && !empty($filters['sku']))
            $params .= "&sku={$filters['sku']}";
        if (isset($filters['product_id']) && !empty($filters['product_id']))
            $params .= "&product_id={$filters['product_id']}";
        if (isset($filters['favorite']) && !empty($filters['favorite']))
            $params .= "&favorite={$filters['favorite']}";

        //$this->service_url = self::MIRAKL_API_ENDPOINT.$this->service."/?api_key=".$this->api_key.$params;

        $this->service_url = $this->endpoint."{$this->service}/?api_key=".$this->api_key;

        return $this->parse($this->get());
    }

// end func

    public function offer($offer_id = 0) {
        /*         * *****************************************************************************************
         * Descripton:    Get information of an offer
         * ==========================================================================================
         * Params:    @offer - [required] The identifier of the offer
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = null;
        $this->service_code = 'OF22';

        if (!$offer_id) {
            return $this->errors($code = 10, $cause = 'OFFER_ID_MISSING');
        }

        $this->service_url = self::MIRAKL_API_ENDPOINT.$this->service."/{$offer_id}/?api_key=".$this->api_key;

        return $this->parse($this->get());
    }

// end func

    public function offer_post($params = array()) {
        /*         * *****************************************************************************************
         * Descripton:    Update offers
         * ==========================================================================================
         * Params:     {
         * "offers": [{
         * "available_ended": Date,
         * "available_started": Date,
         * "description": String,
         * "discount": {
         * "end_date": Date,
         * "price": Number,
         * "start_date": Date
         * },
         * "internal_description": String,
         * "logistic_class": String,
         * "min_quantity_alert": Number,
         * "offer_additional_fields": [{
         * "code": String,
         * "value": String
         * }],
         * "price": Number,dg
         * "price_additional_info": String,
         * "product_id": String,
         * "product_id_type": String,
         * "quantity": Number,
         * "shop_sku": String,
         * "state_code": String,
         * "update_delete": String
         * }]
         * }
         * ***************************************************************************************** */
        $this->service_method = 'POST';
        $this->service_child = null;
        $this->service_code = 'OF24';

        //$this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/?api_key=".$this->api_key;
        $this->service_url = $this->endpoint."{$this->service}/?api_key=".$this->api_key;
        //print_r($this->service_url); exit;

        return $this->parse($this->post($request_type = 'json', $params));
    }

// end func

    public function messages($offer_id = 0, $filters = array()) {
        /*         * *****************************************************************************************
         * Descripton:    List messages of an offer
         * ==========================================================================================
         * Params:    @customer_id - [optional] customer identifier.
         * @archived - [optional] "ALL", "FALSE" (default) or "TRUE". If TRUE (FALSE) returns only archived (not archived) messages of the messages received.
         * @received - [optional] "ALL" (default), "FALSE" or "TRUE". If TRUE (FALSE) returns only messages received by (sent to) shop.
         * @visible - [optional] "ALL", "TRUE" (default) or "FALSE". If "TRUE" ("FALSE") returns only the visible (not visible) messages.
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = "messages";
        $this->service_code = 'OF41';

        if (!$offer_id) {
            return $this->errors($code = 10, $cause = 'OFFER_ID_MISSING');
        }

        $params = '';
        if (isset($filters['customer_id']) && !empty($filters['customer_id']))
            $params .= "&customer_id={$filters['customer_id']}";
        if (isset($filters['archived']) && !empty($filters['archived']))
            $params .= "&archived=".urlencode(MiraklHelperQuery::params($this->service.'/messages', 'archived', $filters['archived']));
        if (isset($filters['received']) && !empty($filters['received']))
            $params .= "&received=".urlencode(MiraklHelperQuery::params($this->service.'/messages', 'received', $filters['received']));
        if (isset($filters['visible']) && !empty($filters['visible']))
            $params .= "&visible=".urlencode(MiraklHelperQuery::params($this->service.'/messages', 'visible', $filters['visible']));
        if (isset($filters['sort']) && !empty($filters['sort']))
            $params .= "&sort=".(MiraklHelperQuery::params($this->service, $filters['sort']));
        if (isset($filters['order']) && !empty($filters['order']))
            $params .= "&order=".MiraklHelperQuery::order($filters['order']);
        if (isset($filters['max']) && !empty($filters['max']))
            $params .= "&max=".MiraklHelperQuery::page_max($filters['max']);
        if (isset($filters['offset']) && !empty($filters['offset']))
            $params .= "&offset=".MiraklHelperQuery::page_offset($filters['offset']);


        $this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$offer_id}/{$this->service_child}/?api_key=".$this->api_key.$params;

        return $this->parse($this->get());
    }

// end func

    public function message_post($offer_id = 0, $message_id = 0, $params = array()) {
        /*         * *****************************************************************************************
         * Descripton:    List messages of an offer
         * ==========================================================================================
         * Params:    @offer_id - [required] The identifier of the offer
         * @message_id - [required] The identifier of the message
         * @body        - [required] Body of the message
         * @visible    - [optional] boolean - Publicity of the message
         *             {
         * "body": String,
         * "visible": Boolean
         * }
         * ***************************************************************************************** */
        $this->service_method = 'POST';
        $this->service_child = "messages";
        $this->service_code = 'OF43';

        if (empty($offer_id)) {
            return $this->errors($code = 10, $cause = 'OFFER_ID_MISSING');
        }

        if (!$message_id) {
            return $this->errors($code = 10, $cause = 'MESSAGE_ID_MISSING');
        }

        $this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$offer_id}/{$this->service_child}/{$message_id}/?api_key=".$this->api_key;

        return $this->parse($this->post($request_type = 'json', $params));
    }

// end func

    public function states() {
        /*         * *****************************************************************************************
         * Descripton:    Get the list of offer states
         *                List of offer states representations without pagination
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = "states";
        $this->service_code = 'OF61';
        $this->service_url = self::MIRAKL_API_ENDPOINT."{$this->service}/{$this->service_child}/?api_key=".$this->api_key;

        return $this->parse($this->get());
    }

// end func
}

// end class definition