<?php

require_once dirname(__FILE__).'/mirakl.database.php';
require_once dirname(__FILE__).'/mirakl.lib.interface.php';
require_once dirname(__FILE__).'/mirakl.config.class.php';

class MiraklTask extends Popup {

    private $mirakl_database;
    private $user_name;
    private $iso_code;
    public $skipped = array(); // // Format skipped[] = ([0] = refercnce_type, [1] = reference, [2] = log_key, [3] = log_message, [4] = log_message)
    public $count_process = 0;
    public $count_send = 0;
    public $count_success = 0;
    public $count_error = 0;
    public $count_skipped = 0;
    public $count_warning = 0;
    public $separator = ';';
    public $download = false;
    public $config;
    public $cron;
    public $batch_id;
    public $date_time;
    public $proc_rep;
    public $message;
    public $reference_report = '';
    public $S; // status success
    public $E; // status error

    public function __construct($user_name) {
        $this->user_name = $user_name;
        $this->mirakl_database = new MiraklDatabase($this->user_name);
        $this->S = MiraklParameter::STATUS_SUCCESS;
        $this->E = MiraklParameter::STATUS_ERROR;
    }

    public function getBatchId() {
        return uniqid();
    }

    public function getDateTime() {
        return date('Y-m-d H:i:s');
    }

    public function getPathMirakl() {
        return USERDATA_PATH.$this->user_name.'/mirakl';
    }

    public function lang($str) {
        return Mirakl_Tools::l($str);
    }

    public function getDatabase() {
        return $this->mirakl_database;
    }

    public function getFileName($marketplace, $ext) {
        if (strpos($ext, ".") === 0) {
            $e = explode(".", $ext);
            return strtolower($marketplace).'-'.$e[1];
        }
        return strtolower($marketplace);
    }

    public function setLang($iso_code) {
        $this->iso_code = $iso_code;
    }

    public function getLang($text) {
        if (!is_array($text)) {
            return $text;
        }
        return isset($text[$this->iso_code]) ? $text[$this->iso_code] : reset($text);
    }

    public function is_required($required, $value) {
        return $required == 'Y' && empty($value); // error return true
    }

    public function is_type($type, $value) {
        // error return true

        if (empty($value)) {
            return false;
        }

        if ($type == 'DECIMAL') {
            $value = is_numeric($value) ? (float) $value : '';
            return !is_float($value);
        }

        if ($type == 'INTEGER') {

//            $value = (int) $value; /* convert string to int eg. "2 ans" = 2 */
//            if($value === 0){
//                return true;
//            }

            $value = is_numeric($value) ? (int) $value : '';
            return !is_int($value);
        }

        return false;
    }

    public function is_validation($validation, $value) {
        // error return true

        if (empty($value)) {
            return false;
        }

        if (!empty($validation)) {
            $valid_many = explode(",", $validation);

            foreach ($valid_many as $valid) {

                $valids = explode("|", $valid);
                $valid_key = $valids[0];
                $valid_value = $valids[1];
                $value_lenght = strlen($value);

                if ($valid_key == 'MIN_LENGTH' && $value_lenght < $valid_value) {
                    return true;
                } else if ($valid_key == 'MAX_LENGTH' && $value_lenght > $valid_value) {
                    return true;
                } else if ($valid_key == 'LENGTH' && $value_lenght != $valid_value) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getNotification($notification) {

        foreach ($notification as $valid_type => $valid_value) {
            if ($valid_type == 'required') {
                $message_key = "Attribute %s are required ";
                $message_value = implode(", ", $valid_value);
                break;
            } else if ($valid_type == 'type') {
                $message_key = "Attribute %s type worng ";
                $message_value = implode(", ", $valid_value);
                break;
            } else if ($valid_type == 'valid') {
                $message_key = "Attribute %s are validation ";
                $message_value = implode(", ", $valid_value);
                break;
            } else {
                $message_key = '';
                $message_value = '';
            }
        }

        return array('notify_key' => $message_key, 'notify_value' => $message_value);
    }

    public function getField($shop_attribute, $data, $debug = false) {

        // getAttributeFieldShop
        $field_value = array();
        $field_value['id'] = ''; // for feild shop
        $field_value['code'] = '';
        $field_value['is_reference'] = false;
        $field_value['type'] = '';

        $data_field = explode('-', $shop_attribute); // eg. S-category
        $field_type = isset($data_field[0]) ? $data_field[0] : '';
        $field_shop = isset($data_field[1]) ? $data_field[1] : '';


        $field_value['type'] = $field_type;
        
        switch ($field_type) {
            case MiraklParameter::FIELD_ATTRIBUTE :
                $field_value['id'] = isset($data['attributes'][$field_shop]['id_attribute']) ? $data['attributes'][$field_shop]['id_attribute'] : '';
                $field_value['code'] = isset($data['attributes'][$field_shop]['attribute_name']) ? $data['attributes'][$field_shop]['attribute_name'] : '';
                break;
            case MiraklParameter::FIELD_FEATURE :


                $field_value['id'] = isset($data['features'][$field_shop]['id_feature_value']) ? $data['features'][$field_shop]['id_feature_value'] : '';
                $field_value['code'] = isset($data['features'][$field_shop]['feature_value']) ? $data['features'][$field_shop]['feature_value'] : '';

                break;
            default :


//                if ($debug) {
//                    echo '<pre>';
//                    print_r($field_shop);
//                    exit;
//                }

                switch ($field_shop) {
                    case 'category' :
                        $field_value['code'] = isset($data['category_mapping_model']) && !empty($data['category_mapping_model']) ? $data['category_mapping_model'] : $data['category_default'];
                        break;
                    case 'reference' :
                        $field_value['code'] = $data['reference'];
                        /* get sku on the marketplace for get key report */
                        $field_value['is_reference'] = true;
                        break;
                    case 'title' :
                        $field_value['code'] = $data['product_name'];
                        break;
                    case 'description' :
                        $field_value['code'] = $data['description'];
                        break;
                    case 'ean' :
                        $field_value['code'] = $data['ean13'];
                        break;
                    case 'upc' :
                        $field_value['code'] = $data['upc'];
                        break;
                    case 'manufacturer' :
                        $field_value['code'] = $data['manufacturer'];
                        break;
                    case 'supplier' :
                        $field_value['code'] = $data['supplier'];
                        break;
                    case 'weight' :
                        $field_value['code'] = $data['weight'];
                        break;
                    case 'internal_description' :
                        $field_value['code'] = $data['internal_description'];
                        break;
                    case 'default_image' :
                        $field_value['code'] = '';
                        if (isset($data['images']) && !empty($data['images'])) {
                            foreach ($data['images'] as $index => $image) {
                                if ($image['image_type'] == 'default') {
                                    $field_value['code'] = $image['image_url'];
                                    break;
                                }
                            }
                        }
                        break;
                    case 'additional_image' :
                        $count_additional_image = 0;
                        $field_value['code'] = '';
                        if (isset($data['images_normal']) && !empty($data['images_normal'])) {
                            foreach ($data['images_normal'] as $index => $images_normal) {
                                if ($index == $count_additional_image) {
                                    $field_value['code'] = isset($images_normal['image_url']) ? $images_normal['image_url'] : ''; //exit;
                                    break;
                                }
                            }
                        }

                        $count_additional_image = $count_additional_image + 1;
                        break;
                    case 'vat' :
                        $field_value['code'] = $data['tax'];
                        break;
                }

                break;
        }


        return $field_value;
    }

}

class Popup {

    private $action;
    private $batch_id;
    private $date_time;
    private $user_name;
    private $ext;
    private $country;
    private $marketplace;
    public $popup = array();

    const ACTION_PRODUCT_CREATE = 1;
    const ACTION_PRODUCT_UPDATE = 2;
    const ACTION_PRODUCT_DELETE = 3;
    const ACTION_ORDER_ACCEPT = 4;
    const ACTION_ORDER_IMPORT = 5;
    const ACTION_ORDER_TRACKING = 6;

    public function __construct() {
        
    }

    public function startPopup($context) {

        /* $acts = array() : [0] = action_name, [1] = action_type */
        switch ($context['action']) {
            case self::ACTION_PRODUCT_CREATE : $acts = array('product', 'export - products');
                break;
            case self::ACTION_PRODUCT_UPDATE : $acts = array('offers', 'export - offers');
                break;
            case self::ACTION_PRODUCT_DELETE : $acts = array('product', 'delete - products');
                break;
            case self::ACTION_ORDER_ACCEPT : $acts = array('orders', 'accept - orders');
                break;
            case self::ACTION_ORDER_IMPORT : $acts = array('orders', 'import - orders');
                break;
            case self::ACTION_ORDER_TRACKING : $acts = array('orders', 'update status - orders');
                break;
            default : $acts = array('product', 'No declare action type');
        }

        $process_name = sprintf(Mirakl_Tools::l('SS_s_started_on_s'), ucfirst($context['marketplace']), $context['ext'], $acts[1], '..');
        $process_title = $acts[1].' processing..';
        $process_type = $acts[1].$context['country'];

        $message = new stdClass();
        $message->batch_id = $context['batch_id'];
        $message->title = $process_title;
        $message->date = date('Y-m-d', strtotime($context['date_time']));
        $message->time = date('H:i:s', strtotime($context['date_time']));
        $message->type = $process_name;
        $message->$acts[0] = new stdClass();

        $proc_rep = new report_process($context['user_name'], $context['batch_id'], $process_title, true, false, true);
        $proc_rep->set_process_type($process_type);
        $proc_rep->set_popup_display(true);
        $proc_rep->set_marketplace($context['marketplace']);
        $process = $proc_rep->has_other_running($process_type);

        if ($process) {
            // log debug
        }

        $this->popup = array('message' => $message, 'proc_rep' => $proc_rep);
        return $this->popup;
    }

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class LogReport implements iLogger {

    public function __construct($context) {
        
    }

    public function alert($message, array $context = array()) {
        
    }

    public function critical($message, array $context = array()) {
        
    }

    public function debug($message, array $context = array()) {
        
    }

    public function emergency($message, array $context = array()) {
        
    }

    public function error($message, array $context = array()) {
        
    }

    public function info($message, array $context = array()) {
        
    }

//    public function log($level, $message, array $context = array()) {
//        
//    }

    public function notice($message, array $context = array()) {
        
    }

    public function warning($message, array $context = array()) {
        
    }

    public function log($level, $message, array $context = array()) {
        
    }

}
