<?php

require_once dirname(__FILE__).'/mirakl.tools.php';

class MiraklConfig {

    // Dropbox ini path
    const DROPBOX_PUBLIC_FOLDER = 'https://dl.dropboxusercontent.com/u/53469716/mirakl';

    public static function marketplaceParams($name = '') {

        if (empty($name)) {
            return false;
        }

        $name = strtolower($name);
        $force_logo_update = false;

        $base_dir = realpath(dirname(__FILE__).'/../settings/'.$name);
        $ini_file = $base_dir.'/'.$name.'.ini';

        if (!file_exists($ini_file)) {
            return false;
//            self::marketplaceInit($name);
//            $force_logo_update = true;
        }

        // Default is Mirakl
        $marketplace_ini = array();
        $marketplace_ini['module'] = 'mirakl';
        $marketplace_ini['name'] = 'Mirakl';
        $marketplace_ini['display_name'] = 'Mirakl';
        $marketplace_ini['endpoint'] = 'https://moa-recette.mirakl.net/api/';
        $marketplace_ini['email'] = 'support@mirakl.com';

        if (!file_exists($ini_file)) {
            printf('marketplace.ini doesn\'t exist');
            return false;
        }

        if (!($marketplace_ini = parse_ini_file($ini_file))) {
            printf('failed to parse marketplace.ini');
            return false;
        }

        $module_logo = $base_dir.'/'.$name.'_app.png';
        $logo_file = $base_dir.'/'.$name.'_large.png';
        $logo64_file = $base_dir.'/'.$name.'_app-64px.png';
        $logo32_file = $base_dir.'/'.$name.'_app-32px.png';
        
        $marketplace_ini['logo'] = $name.'/'.$name.'_app-32px.png';
        
        if (array_key_exists('logo_57', $marketplace_ini)) {
            if (self::getLogo($module_logo, $marketplace_ini['logo_57'], $force_logo_update)) {
                $marketplace_ini['logo_57'] = 'marketplace/'.basename($logo_file);
            } else {
                $marketplace_ini['logo_57'] = 'logo.png';
            }
        }

//        if (array_key_exists('logo', $marketplace_ini)) {
//            if (self::getLogo($logo_file, $marketplace_ini['logo'], $force_logo_update)) {
//                $marketplace_ini['logo'] = 'marketplace/'.basename($logo_file);
//            } else {
//                $marketplace_ini['logo'] = 'logo.png';
//            }
//        }

        if (array_key_exists('logo_32', $marketplace_ini)) {
            if (self::getLogo($logo32_file, $marketplace_ini['logo_32'], $force_logo_update)) {
                $marketplace_ini['logo_32'] = 'marketplace/'.basename($logo32_file);
            } else {
                $marketplace_ini['logo_32'] = 'logo32px.png';
            }
        }

        if (array_key_exists('logo_64', $marketplace_ini)) {
            if (self::getLogo($logo64_file, $marketplace_ini['logo_64'], $force_logo_update)) {
                $marketplace_ini['logo_64'] = 'marketplace/'.basename($logo64_file);
            } else {
                $marketplace_ini['logo_64'] = 'logo64px.png';
            }
        }

        if (!isset($marketplace_ini['name'])) {
            $marketplace_ini['name'] = 'Mirakl';
        } else {
            $marketplace_ini['name'] = Mirakl_Tools::ucfirst(Mirakl_Tools::toKey($marketplace_ini['name']));
        }

        $marketplace_ini['module'] = Mirakl_Tools::strtolower($marketplace_ini['name']);

        if (!isset($marketplace_ini['display_name'])) {
            $marketplace_ini['display_name'] = 'Mirakl';
        }

        $marketplace_ini['marketplace'] = Mirakl_Tools::toKey(Mirakl_Tools::strtolower($marketplace_ini['name']));
        $marketplace_ini['marketplace_key'] = Mirakl_Tools::strtoupper(Mirakl_Tools::toKey($marketplace_ini['name']));
        $marketplace_ini['marketplace_logo'] = file_exists($logo_file) ? 'marketplace/'.basename($logo_file) : basename($mirakl_logo_file);

        // Optionnal/Marketplace custom fields ;
        if (array_key_exists('optionnal_fields', $marketplace_ini)) {
            $marketplace_ini['fields'] = self::parseOptionalFields($marketplace_ini);
        } else {
            $marketplace_ini['fields'] = array();
        }

        // Optionnal/Marketplace custom fields ;
        if (array_key_exists('exclude', $marketplace_ini)) {
            $marketplace_ini['exclude'] = self::parseExcludeFields($marketplace_ini);
        } else {
            $marketplace_ini['exclude'] = array();
        }

        // Marketplace Options ;
        if (array_key_exists('options', $marketplace_ini)) {
            $marketplace_ini['options'] = self::parseOptionsFields($marketplace_ini);
        } else {
            $marketplace_ini['options'] = array();
        }

        // Marketplace Additionnal Fields ;
        if (array_key_exists('additionnal_fields', $marketplace_ini)) {
            $marketplace_ini['additionnals'] = self::parseAdditionnalFields($marketplace_ini);
        } else {
            $marketplace_ini['additionnals'] = array();
        }

        // Marketplace Conditions ;
        if (array_key_exists('conditions', $marketplace_ini)) {
            $marketplace_ini['conditions'] = self::parseConditions($marketplace_ini);
        } else {
            $marketplace_ini['conditions'] = array();
        }

        //echo '<pre>'; print_r($marketplace_ini); exit;
        return ($marketplace_ini);
    }

    public static function getLogo($local, $remote, $force = false) {
        if (!file_exists($local) || $force) {
            if ($image = Mirakl_Tools::file_get_contents($remote, false, null, 30)) {
                if (!file_put_contents($local, $image)) {
                    return (false);
                }
            } else {
                return (false);
            }
        }
        return ($local);
    }

    public static function parseOptionalFields($marketplace_ini = array()) {

        if (empty($marketplace_ini)) {
            return false;
        }

        $fields = array();
        $additionnal_fields = explode(',', $marketplace_ini['optionnal_fields']);

        if (is_array($additionnal_fields) && count($additionnal_fields)) {
            foreach ($additionnal_fields as $additionnal_field) {
                if (array_key_exists($additionnal_field, $marketplace_ini)) {
                    $params = explode(',', $marketplace_ini[$additionnal_field]);
                    if (is_array($params) && count($params) == 3) {
                        $field = array();
                        $field['mirakl'] = $params[0];
                        $field['prestashop'] = $params[1];
                        $field['default'] = $params[2];

                        $fields[] = $field;
                    }
                }
            }
        }
        return ($fields);
    }

    public static function parseExcludeFields($marketplace_ini = array()) {

        if (empty($marketplace_ini)) {
            return false;
        }

        $fields = array();
        $exclude_fields = explode(',', $marketplace_ini['exclude']);

        if (is_array($exclude_fields) && count($exclude_fields)) {
            foreach ($exclude_fields as $exclude_field) {

                if (strpos($exclude_field, '[')) {
                    preg_match('#([^\[]+)\[(\w+)\]#i', $exclude_field, $result);

                    if (is_array($result) && count($result) > 2) {
                        if (stripos($result[2], 'offers') !== false) {
                            $exclude_field = $result[1];
                        } elseif (stripos($result[2], 'products') !== false) {
                            $exclude_field = $result[1];
                        } else {
                            continue;
                        }
                    }
                }
                $fields[$exclude_field] = true;
            }
        }

        return ($fields);
    }

//    public static function parseExcludeFields($marketplace_ini) {
//        $fields = array();
//        $exclude_fields = explode(',', $marketplace_ini['exclude']);
//
//        if (is_array($exclude_fields) && count($exclude_fields)) {
//            foreach ($exclude_fields as $exclude_field) {
//                $fields[$exclude_field] = true;
//            }
//        }
//        return ($fields);
//    }

    public static function parseOptionsFields($marketplace_ini = array()) {

        if (empty($marketplace_ini)) {
            return false;
        }

        $fields = array();
        $options_fields = explode(',', $marketplace_ini['options']);

        if (is_array($options_fields) && count($options_fields)) {
            foreach ($options_fields as $option_field) {
                $fields[$option_field] = true;
            }
        }

        return ($fields);
    }

    public static function parseAdditionnalFields($marketplace_ini = array()) {
        
        if (empty($marketplace_ini)) {
            return false;
        }
        
        $fields = array();
        $additionnal_fields = explode(',', $marketplace_ini['additionnal_fields']);

        if (is_array($additionnal_fields) && count($additionnal_fields)) {
            foreach ($additionnal_fields as $additionnal_field) {
                if (array_key_exists($additionnal_field, $marketplace_ini)) {
                    $params = explode(',', $marketplace_ini[$additionnal_field]);
                    if (is_array($params)) {
                        $field = array();
                        $field['mirakl'] = $params[0];
                        $field['prestashop'] = isset($params[1]) ? $params[1] : null;
                        $field['default'] = isset($params[2]) ? $params[2] : null; //isset($params[2]) ? $params[1] : null; // issue
                        $field['required'] = isset($params[3]) ? (bool) $params[3] : false;

                        $fields[] = $field;
                    }
                }
            }
        }
        return ($fields);
    }

    public static function parseConditions($marketplace_ini = array()) {
        
        if (empty($marketplace_ini)) {
            return false;
        }
        
        $conditions = array();
        $conditions_text = preg_replace('/\s/', '', $marketplace_ini['conditions']);

        if (!Mirakl_Tools::strlen($conditions_text)) {
            return(false);
        }
        $conditions_fields = explode(',', $conditions_text);
        if (is_array($conditions_fields) && count($conditions_fields)) {
            foreach ($conditions_fields as $conditions_field) {
                $condition_set = explode(':', $conditions_field);

                if (count($condition_set) == 2) {
                    $state = Mirakl_Tools::toKey($condition_set[0]);
                    $code = $condition_set[1];
                    $conditions[$state] = $code;
                }
            }
        }
        return ($conditions);
    }

    // Get Mirakl ini // 2016-01-22
    public function marketplaceInit($name = '') {

        require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
        require_once(dirname(__FILE__).'/mirakl.database.php');
        $id_marketplace = 6;
        $ci = new ci_db_connect();
        $sql = "SELECT * FROM offer_sub_marketplace  ORDER BY id_offer_sub_marketplace;";
        $query = $ci->select_query($sql);

        $marketplace_db = array();
        while ($rows = $ci->fetch($query)) {
            $marketplace_db[strtolower($rows[1])] = $rows[0]; // id_marketplace
        }

        if (!empty($name)) {
            $marketplaces = array($name);
        } else {
            // get All Marketplace in Mirakl
            $marketplaces = array(
                'auchan',
                'comptoirsante',
                'darty',
                'delamaison',
                'elcorteingles',
                'menlook',
                'naturedecouvertes',
                'privalia',
                'rueducommerce',
                'thebeautyst',
                'truffaut'
            );
        }

        foreach ($marketplaces as $name) {
            // Get ini file
            $ini_file = sprintf('%s%s%s.ini?dl=1', self::DROPBOX_PUBLIC_FOLDER, '/ini/', $name);
            $file_headers = @get_headers($ini_file);

            if (isset($file_headers[0]) && $file_headers[0] != 'HTTP/1.1 404 Not Found') {

                if (!($ini_content = file_get_contents($ini_file))) {
                    printf('Failed to download: '.$ini_file);
                    return;
                }

                $file_setttings = dirname(__FILE__).'/../settings/';

                if (!file_exists($file_setttings)) {
                    mkdir($file_setttings, 0777);
                    chmod($file_setttings, 0777);
                }

                $output_ini_file = $file_setttings.$name.'/'.$name.'.ini';

                if (!$this->initImportDirectory($name)) {
                    printf('Failed to write: '.$output_ini_file);
                    continue;
                }

                if (!file_put_contents($output_ini_file, $ini_content)) {
                    printf('Failed to write: '.$output_ini_file);
                }
            }

            // Get logo file
            $file_type = array(
                '_app-32px',
                '_app-57px',
                '_app-64px',
                '_app',
                '_large',
            );

            foreach ($file_type as $type) {
                $logo_file = sprintf('%s%s%s%s.png?dl=1', self::DROPBOX_PUBLIC_FOLDER, '/logo/', $name, $type);
                $file_headers = @get_headers($logo_file);

                if (isset($file_headers[0]) && $file_headers[0] != 'HTTP/1.1 404 Not Found') {

                    if (!($logo_content = file_get_contents($logo_file))) {
                        printf('Failed to download: '.$logo_file);
                        return;
                    }

                    $output_logo_file = $file_setttings.$name.'/'.$name.$type.'.png';

                    if (!$this->initImportDirectory($name)) {
                        printf('Failed to write: '.$output_logo_file);
                        continue;
                    }

                    if (!file_put_contents($output_logo_file, $logo_content)) {
                        printf('Failed to write: '.$output_logo_file);
                    }
                }
            }

            $ini_file = $file_setttings.$name.'/'.$name.'.ini';

        }
    }

    private function initImportDirectory($folder, $file = null) {

        $this->import = dirname(__FILE__).'/../settings/'.$folder.'/';

        if (!is_dir($this->import)) {
            if (!@mkdir($this->import)) {
                $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to create '.$folder.' directory');
                return false;
            }
        }

        if (isset($file) && strlen($file)) {
            $this->import = $this->import.$file.'/';
            if (!is_dir($this->import)) {
                if (!@mkdir($this->import)) {
                    $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to create '.$file.' directory');
                    return false;
                }
            }
        }

        @chmod($this->import, 0777);

        if (file_put_contents($this->import.'.htaccess', "deny from all\n") === false) {
            $this->error = sprintf('%s(#%d): %s', basename(__FILE__), __LINE__, 'Unable to write into '.$folder.' directory');
            return false;
        }

        return true;
    }

}

//$function = new MiraklConfig();
//$function->marketplaceInit();
