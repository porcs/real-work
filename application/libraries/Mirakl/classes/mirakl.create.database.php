<?php

class MiraklCreateDatabase {

    private $db;

    public function __construct($db) {
        $this->db = $db;

        //set table prefix for order :: orders_
        $this->prefix_order = MiraklParameter::PREFIX_ORDER;

        //set table prefix for mirakl
        $this->prefix_mirakl = MiraklParameter::PREFIX_MIRAKL;
    }

    public function create_table() {

        if (!$this->check_table_exists($this->prefix_mirakl.'log_report')) {
            $this->create_mirakl_log_report_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'log_report_detail')) {
            $this->create_mirakl_log_report_detail_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'log')) {
            $this->create_mirakl_log_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'log_detail')) {
            $this->create_mirakl_log_detail_table();
        }

        /* Profiles */
        if (!$this->check_table_exists($this->prefix_mirakl.'profile')) {
            $this->create_mirakl_profile_table();
        }

        /* Categories */
        if (!$this->check_table_exists($this->prefix_mirakl.'category_selected')) {
            $this->create_mirakl_category_selected_table();
        }

        /* Mappings */
        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_carrier')) {
            $this->create_mirakl_mapping_carrier_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'status')) {
            $this->create_mirakl_status_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'products')) {
            $this->create_mirakl_products_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_model')) {
            $this->create_mirakl_mapping_model_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_model_attribute')) {
            $this->create_mirakl_mapping_model_attribute_table();
        }

//        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_model_value')) {
//            $this->create_mirakl_mapping_model_value_table();
//        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_field')) {
            $this->create_mirakl_mapping_field_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_field_attribute')) {
            $this->create_mirakl_mapping_field_attribute_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_field_value')) {
            $this->create_mirakl_mapping_field_value_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'mapping_attribute_list')) {
            $this->create_mirakl_mapping_attribute_list_table();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_additional_field')) {
            $this->create_mirakl_config_additional_field();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_carrier')) {
            $this->create_mirakl_config_carrier();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_hierarchy')) {
            $this->create_mirakl_config_hierarchy();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_attribute')) {
            $this->create_mirakl_config_attribute();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_value')) {
            $this->create_mirakl_config_value();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'config_value_list')) {
            $this->create_mirakl_config_value_list();
        }
    }

    public function create_table_order() {
        if (!$this->check_table_exists($this->prefix_mirakl.'order_accept')) {
            $this->create_order_accept();
        }

        if (!$this->check_table_exists($this->prefix_mirakl.'order_accept_item')) {
            $this->create_order_accept_item();
        }

        if (!$this->check_table_exists($this->prefix_order.'order_sub_marketplace')) {
            $this->create_order_sub_marketplace();
        }
    }

    private function check_table_exists($table_name) {
        if ($this->db->db_table_exists($table_name, true)) {
            return true;
        }

        return false;
    }

    private function create_order_accept() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'order_accept (
                id_order_accept INTEGER NOT NULL auto_increment,
                sub_marketplace INTEGER NOT NULL,
                id_country      INTEGER NOT NULL,
                id_shop         INTEGER NOT NULL,
                order_id        VARCHAR(50) NOT NULL,
                commercial_id   VARCHAR(50) NOT NULL,
                order_state     VARCHAR(50) NOT NULL,
                customer_name   VARCHAR(255) NOT NULL,
                shipping_price  DECIMAL(15,2),
                total_price     DECIMAL(15,2),
                tracking_no     VARCHAR(50),
                invoice_no      VARCHAR(50),
                order_date      DATETIME,
                flag            VARCHAR(2),
                status          VARCHAR(50),
                date_add        DATETIME NOT NULL,
                PRIMARY KEY (id_order_accept ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_order_accept_item() {
        // (price_unit * quantity) + shipping_price = total_price

        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'order_accept_item (
                id_order_accept_item    INTEGER NOT NULL auto_increment,
                id_order_accept         INTEGER NOT NULL,
                id_product              INTEGER,
                offer_id                VARCHAR(50) NOT NULL,
                offer_sku               VARCHAR(50) NOT NULL,
                order_line_id           VARCHAR(50) NOT NULL,
                order_line_index        INTEGER NOT NULL,
                order_line_state        VARCHAR(50) NOT NULL,
                price                   DECIMAL(15,2) NOT NULL,
                price_unit              DECIMAL(15,2) NOT NULL,
                quantity                INTEGER NOT NULL,
                shipping_price          DECIMAL(15,2) NOT NULL,
                product_sku             VARCHAR(50),
                product_title           VARCHAR(255),
                status                  VARCHAR(50),
                date_add                DATETIME NOT NULL,
                PRIMARY KEY (id_order_accept_item ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_order_sub_marketplace() {
        $sql = 'CREATE TABLE '.$this->prefix_order.'order_sub_marketplace (
                id_order_sub_marketplace    INTEGER NOT NULL auto_increment,
                id_orders                   INTEGER NOT NULL,
                id_country                  INTEGER NOT NULL,
                id_shop                     INTEGER NOT NULL,
                sub_marketplace             INTEGER NOT NULL,
                date_add                    DATETIME NOT NULL,
                PRIMARY KEY (id_order_sub_marketplace ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_status_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'status (
                id_status       INTEGER NOT NULL auto_increment,
                sub_marketplace INTEGER NOT NULL,
                id_country      INTEGER NOT NULL,
                id_shop         INTEGER NOT NULL,
                action_type     varchar(32) NOT NULL,
                status          TINYINT,
                is_cron         TINYINT,
                detail          LONGTEXT,
                marketplace     VARCHAR(255) NOT NULL,
                date_upd        DATETIME,
                PRIMARY KEY (id_status ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_products_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'products (
                id_product          INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                id_country          INTEGER NOT NULL,
                id_shop             INTEGER NOT NULL,
                category_code       VARCHAR(32),
                category_label      VARCHAR(128),
                product_id          VARCHAR(32) NOT NULL,
                product_id_type     VARCHAR(32) NOT NULL,
                product_sku         VARCHAR(128),
                product_title       VARCHAR(128),
                date_add            DATETIME,
                date_upd            DATETIME,
                PRIMARY KEY (id_product ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_log_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'log (
                id_log              INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                id_country          INTEGER NOT NULL,
                id_shop             INTEGER NOT NULL,
                batch_id            VARCHAR(32) NOT NULL,
                feed_type           VARCHAR(32) NOT NULL,
                count_process       INTEGER,
                count_send          INTEGER,
                count_success       INTEGER,
                count_error         INTEGER,
                count_skipped       INTEGER,
                count_warning       INTEGER,                
                is_cron             TINYINT,  
                log_status          TINYINT,
                log_key             VARCHAR(255),
                log_value           VARCHAR(255),
                date_add            DATETIME,
                PRIMARY KEY (id_log ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_log_detail_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'log_detail (
                    id_log_detail       INTEGER NOT NULL auto_increment,
                    id_log              INTEGER NOT NULL,
                    reference_type      VARCHAR(32),
                    reference           VARCHAR(32),
                    log_key             VARCHAR(255),
                    log_value           VARCHAR(255),
                    date_add            DATETIME,
                    PRIMARY KEY (id_log_detail ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_log_report_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'log_report (
                id_log_report   INTEGER NOT NULL auto_increment,
                sub_marketplace INTEGER NOT NULL,
                id_country      INTEGER NOT NULL,
                id_shop         INTEGER NOT NULL,
                batch_id        VARCHAR(32) NOT NULL,
                feed_type       VARCHAR(32) NOT NULL,
                detail          TEXT,
                date_add        DATETIME,
                PRIMARY KEY (id_log_report ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_log_report_detail_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'log_report_detail (
                id_log_report_detail    INTEGER NOT NULL auto_increment,
                id_log_report           INTEGER NOT NULL,
                reference               VARCHAR(32),
                log_error               VARCHAR(255),
                log_warning             VARCHAR(255),
                date_add                DATETIME,
                PRIMARY KEY (id_log_report_detail ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_category_selected_table() {
        $sql = ' CREATE TABLE '.$this->prefix_mirakl.'category_selected (
                id_category_selected    INTEGER NOT NULL auto_increment,   
                sub_marketplace         INTEGER NOT NULL,
                id_country              INTEGER NOT NULL,
                id_shop                 INTEGER NOT NULL,
                id_category             INTEGER,
                id_mode                 INTEGER,
                id_profile              INTEGER,
                marketplace             VARCHAR(255),
                date_add                DATETIME,
                PRIMARY KEY (id_category_selected ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_carrier_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_carrier (
                id_mapping_carrier      INTEGER NOT NULL auto_increment,
                sub_marketplace         INTEGER NOT NULL,
                id_country              INTEGER NOT NULL,
                id_shop                 INTEGER NOT NULL,
                id_config_carrier       INTEGER,
                id_carrier              INTEGER,
                date_add                DATETIME,
                PRIMARY KEY (id_mapping_carrier ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_profile_table() {
        $sql = ' CREATE TABLE '.$this->prefix_mirakl.'profile (
                id_profile              INTEGER NOT NULL auto_increment,
                sub_marketplace         INTEGER NOT NULL,
                id_country              INTEGER NOT NULL,
                id_shop                 INTEGER NOT NULL,
                id_mode                 INTEGER,
                id_model                INTEGER,
                is_default              TINYINT,
                name                    VARCHAR(255),
                price_rules             LONGTEXT,
                shipping_type           VARCHAR(16),
                shipping                float(20,6),
                warranty                VARCHAR(8),
                combinations_long_attr  tinyint,
                min_quantity_alert      INTEGER,
                logistic_class          TEXT,
                state_code              VARCHAR(255),
                synchronization_field   TEXT,
                title_format            TINYINT,
                html_description        TINYINT,
                description_field       TINYINT,
                product_format          TINYINT,
                no_image                TINYINT,
                note                    LONGTEXT,
                other                   LONGTEXT,
                marketplace             VARCHAR(255),
                date_add                DATETIME,
                PRIMARY KEY (id_profile ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_model_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_model (
                id_model            INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                id_country          INTEGER NOT NULL,
                id_shop             INTEGER NOT NULL,
                model_name          VARCHAR(64) NOT NULL,
                hierarchy_code      VARCHAR(255) NOT NULL,
                date_add            DATETIME,
                date_upd            DATETIME,
                PRIMARY KEY (id_model ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_model_attribute_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_model_attribute (
                id_model_attribute          INTEGER NOT NULL auto_increment,
                id_model                    INTEGER NOT NULL,
                id_config_attribute         INTEGER,
                shop_attribute              VARCHAR(32),
                default_value               VARCHAR(128)
                PRIMARY KEY (id_model_attribute ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

//    private function create_mirakl_mapping_model_value_table() {
//        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_model_value (
//                id_model_value             INTEGER NOT NULL auto_increment,
//                id_model_attribute         INTEGER NOT NULL,
//                id_config_value_list       INTEGER,
//                shop_attribute_value       VARCHAR(32),
//                PRIMARY KEY (id_model_value ASC)
//                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';
//
//        if ($this->db->db_exec($sql, false))
//            return true;
//
//        return false;
//    }

    private function create_mirakl_mapping_field_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_field (
                id_field                INTEGER NOT NULL auto_increment,
                sub_marketplace         INTEGER NOT NULL,
                id_shop                 INTEGER NOT NULL,
                id_config_attribute     INTEGER,
                shop_attribute          VARCHAR(32),
                date_add                DATETIME,
                PRIMARY KEY (id_field ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_field_attribute_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_field_attribute (
                id_field_attribute      INTEGER NOT NULL auto_increment,
                sub_marketplace         INTEGER NOT NULL,
                id_shop                 INTEGER NOT NULL,
                id_config_attribute     INTEGER,
                shop_attribute          VARCHAR(32),
                date_add                DATETIME,
                PRIMARY KEY (id_field_attribute ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_field_value_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_field_value (
                id_field_value             INTEGER NOT NULL auto_increment,
                id_field_attribute         INTEGER NOT NULL,
                id_config_value_list       INTEGER,
                shop_attribute_value       VARCHAR(32),
                PRIMARY KEY (id_field_value ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_mapping_attribute_list_table() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'mapping_attribute_list (
                id_attribute_list          INTEGER NOT NULL auto_increment,
                sub_marketplace            INTEGER NOT NULL,
                id_country                 INTEGER NOT NULL,
                id_shop                    INTEGER NOT NULL,
                id_config_attribute        INTEGER NOT NULL,
                shop_attribute_value       VARCHAR(32),
                id_config_value_list       INTEGER,
                PRIMARY KEY (id_attribute_list ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_additional_field() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_additional_field (
                id_additional_field   INTEGER NOT NULL auto_increment,
                sub_marketplace       INTEGER NOT NULL,
                enable                VARCHAR(2) NOT NULL,
                code                  VARCHAR(32) NOT NULL,
                entity                VARCHAR(20),
                label                 VARCHAR(128) NOT NULL,
                required              VARCHAR(2) NOT NULL,
                type                  VARCHAR(20),
                PRIMARY KEY (id_additional_field ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_carrier() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_carrier (
                id_config_carrier   INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                code                VARCHAR(64) NOT NULL,
                label               VARCHAR(128) NOT NULL,
                tracking_url        VARCHAR(255),
                PRIMARY KEY (id_config_carrier ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_hierarchy() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_hierarchy (
                id_config_hierarchy INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                code                VARCHAR(64) NOT NULL,
                label               VARCHAR(128) NOT NULL,
                level               INTEGER NOT NULL,
                parent_code         VARCHAR(64),
                PRIMARY KEY (id_config_hierarchy ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_attribute() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_attribute (
                id_config_attribute INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                code                VARCHAR(64) NOT NULL,
                type                VARCHAR(20) NOT NULL,
                default_value       VARCHAR(64),
                description         TEXT,
                example             VARCHAR(128),
                hierarchy_code      VARCHAR(32),
                label               VARCHAR(128),
                required            VARCHAR(2) NOT NULL,
                transformations     VARCHAR(32),
                type_parameter      VARCHAR(64),
                validations         VARCHAR(32),
                values_list         VARCHAR(64),
                variant             VARCHAR(2) NOT NULL,
                PRIMARY KEY (id_config_attribute ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_value() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_value (
                id_config_value     INTEGER NOT NULL auto_increment,
                sub_marketplace     INTEGER NOT NULL,
                code                VARCHAR(64) NOT NULL,
                label               VARCHAR(128),
                PRIMARY KEY (id_config_value ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

    private function create_mirakl_config_value_list() {
        $sql = 'CREATE TABLE '.$this->prefix_mirakl.'config_value_list (
                id_config_value_list    INTEGER NOT NULL auto_increment,
                id_config_value         INTEGER NOT NULL,
                code                    VARCHAR(64) NOT NULL,
                label                   VARCHAR(128),
                PRIMARY KEY (id_config_value_list ASC)
                ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci; ';

        if ($this->db->db_exec($sql, false))
            return true;

        return false;
    }

}

// end definition
