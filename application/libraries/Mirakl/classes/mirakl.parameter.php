<?php

class MiraklParameter {

    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;
    const LOG_STATUS_SUCCESS = 1;
    const LOG_STATUS_ERROR = 0;
    const TAG_MIRAKL_UP = 'MIRAKL';
    const TAG_MIRAKL_LW = 'mirakl';
    const TAG_MIRAKL_UC = 'Mirakl';
    const PREFIX_PRODUCT = 'products_';
    const PREFIX_OFFER = 'offers_';
    const PREFIX_ORDER = 'orders_';
    const PREFIX_MIRAKL = 'mirakl_';
    const MARKETPLACE_ID = 6;
    const TAG_GENERAL_UC = 'General';
    const SUPPLIER_REFERENCE = 1;
    const REFERENCE = 2;
    const CATEGORY = 3;
    const MANUFACTURER = 4;
    const UNITY = 5;
    const META_TITLE = 6;
    const META_DESCRIPTION = 7;
    const WEIGHT = 8;
    const BASE_SETTING = 'application/libraries/Mirakl/settings/';
    
    const OFFER_NORMAL = 'NORMAL';
    const OFFER_REPLACE = 'REPLACE';
    const OFFER_NO_EXPIRE = false;
    const OFFER_DELETE = 'delete';
    const OFFER_UPDATE = 'update';

    public static $shop_fields = array(
        self::SUPPLIER_REFERENCE,
        self::REFERENCE,
        self::CATEGORY,
        self::MANUFACTURER,
        self::UNITY,
        self::META_TITLE,
        self::META_DESCRIPTION,
        self::WEIGHT
    );

    /* if change or relate any about it, Should place key "getAttributeFieldShop" or "$field_type" for search and edit. */

    public function getAttributeFieldShop() {
        $feed_attribute = array(
            array('code' => 'S-category', 'label' => 'Category', 'type' => ''),
            array('code' => 'S-title', 'label' => 'Product Title', 'type' => ''),
            array('code' => 'S-description', 'label' => 'Description', 'type' => ''),
            array('code' => 'S-reference', 'label' => 'Reference', 'type' => ''),
            array('code' => 'S-ean', 'label' => 'EAN13', 'type' => ''),
            array('code' => 'S-upc', 'label' => 'UPC', 'type' => ''),
            array('code' => 'S-manufacturer', 'label' => 'Manufacturer', 'type' => 'VL'),
            array('code' => 'S-supplier', 'label' => 'Supplier', 'type' => 'VL'),
            array('code' => 'S-weight', 'label' => 'Weight', 'type' => 'VL'),
            array('code' => 'S-internal_description', 'label' => 'Internal Description', 'type' => ''),
            array('code' => 'S-default_image', 'label' => 'Default Image', 'type' => ''),
            array('code' => 'S-additional_image', 'label' => 'Additional Image', 'type' => ''),
            array('code' => 'S-vat', 'label' => 'Vat', 'type' => 'VL'),
        );
        return $feed_attribute;
    }

    /* if user don't know to mapping can try this */
    /* if change or relate any about it, In mirakl create product func getField() "default" */
    /* Note: Can not use "-" in code */

//    public function getAttributeFieldExtra() {
//        $feed_attribute = array(
//            array('code' => 'E-default', 'label' => 'Default', 'type' => '')
//        );
//        return $feed_attribute;
//    }

    const SYNC_FIELD_EAN = 'EAN';
    const SYNC_FIELD_UPC = 'UPC';
    const SYNC_FIELD_SKU = 'SKU';
//    const TAG_GENERAL = 'General'; // ucfirst only!
//    const ID_GENERAL = 6;
//    
//    const CREATE = 'create';
    const FIELD_DESCRIPTION_LONG = 1;
    const FIELD_DESCRIPTION_SHORT = 2;
    const FIELD_DESCRIPTION_BOTH = 3;
//    const FIELD_DESCRIPTION_FEATURES = 2;
//

    const DEFAULT_VALUE = 'D-9999';
    const FEED_TYPE_PRODUCT = 'PRODUCT';
    const FEED_TYPE_OFFER = 'OFFER';
    const FEED_TYPE_DELETE = 'DELETE';
    const FEED_TYPE_ACCEPT = 'ACCEPT';
    const FEED_TYPE_IMPORT = 'IMPORT';
    const FEED_TYPE_UPDATE = 'UPDATE';
    const FEED_TYPE_ORDER = 'ORDER'; // export order
    const ACTION_TYPE_PRODUCT = 'CREATE PRODUCTS';
    const ACTION_TYPE_DELETE = 'DELETE PRODUCTS';
    const ACTION_TYPE_OFFER = 'UPDATE OFFERS';
    const ACTION_TYPE_ACCEPT = 'ACCEPT ORDERS';
    const ACTION_TYPE_IMPORT = 'IMPORT ORDERS';
    const ACTION_TYPE_UPDATE = 'UPDATE ORDERS';
    const ACTION_TYPE_ORDER = 'EXPORT ORDERS';
//    const NAME_NAME = 1;
    const NAME_NAME_ATTRIBUTES = 2;
    const NAME_BRAND_NAME_ATTRIBUTES = 3;
    const NAME_NAME_BRAND_ATTRIBUTES = 4;
    const ORDER_IMPORT_STATUS_IMPORT = 1;
    const ORDER_IMPORT_STATUS_ALL = 2;
    const TRASH_DOMAIN = 'mirakl.mp.common-services.com';
    const TYPE_MODEL_ATTRIBUTE_LIST = 'AL';
    const TYPE_MODEL_ATTRIBUTE_HIERARCHY = 'AH';
    const FIELD_ATTRIBUTE = 'A';
    const FIELD_FEATURE = 'F';
    const FIELD_SHOP = 'S';
    const FIELD_EXTRA = 'E';
    const FIELD_TYPE_AT = 'AT'; // attribute
    const FIELD_TYPE_AL = 'AL'; // attribute with list
    const FIELD_TYPE_AH = 'AH'; // attribute with hierarchy

}
