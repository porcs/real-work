<?php

require_once dirname(__FILE__).'/../../db.php';
require_once dirname(__FILE__).'/../../FeedBiz.php';
require_once dirname(__FILE__).'/mirakl.webservice.class.php';
require_once dirname(__FILE__).'/mirakl.tools.php';

class MiraklDatabase {

    private $connector;
    private $user;
    private $debug;
    private $prefix_product;
    private $prefix_offer;
    private $prefix_order;
    private $prefix_mirakl;

    public function __construct($user, $database = 'products', $debug = false) {
        $this->connector = new Db($user, $database);
        $this->user = $user;
        $this->debug = $debug;

        //set table prefix for product :: products_
        $this->prefix_product = MiraklParameter::PREFIX_PRODUCT;

        //set table prefix for offer :: offers_
        $this->prefix_offer = MiraklParameter::PREFIX_OFFER;

        //set table prefix for order :: orders_
        $this->prefix_order = MiraklParameter::PREFIX_ORDER;

        //set table prefix for mirakl
        $this->prefix_mirakl = MiraklParameter::PREFIX_MIRAKL;
    }

    public function getDatabase() { // use db engin global.
        return $this->connector;
    }

    public function test() {
        //db_query query from object
        //db_query_fetch from object
        //
        //db_query_string_fetch     <- similar
        //db_query_str  <- similar ------
        //db_sqlit_query  return object query db but not fetch
        //db_query_result_str  similar
        //db_array_query check array
        //db_exec many cherck
        $sql = "SELECT * FROM products_product";
        $results = $this->db->product->db_array_query($sql);
        echo '<pre>';
        print_r($this->prefix_order);
        exit;
    }

    private function sql_query($sql = '', $prefix = '', $current = false, $key = '') {

        if (empty($sql)) {
            return false;
        }

        $sql = str_replace("::PREFIX::", $prefix, $sql);
        $result = $this->connector->db_query_str($sql);

        if (empty($result)) {
            return false;
        }

        if ($current) {
            if (!empty($key)) {
                return $result[0][$key];
            } else {
                return $result[0];
            }
        }

        return $result;
    }

    private function sql_query_effect($sql = '', $last_id = false) {

        if (empty($sql)) {
            return false;
        }

        $this->connector->db_query_result_str($sql);

        if ($last_id) {
            return $this->connector->db_last_insert_rowid();
        }

        $affected = $this->connector->db_changes();

        if (!empty($affected)) {
            return true;
        }

        return false;
    }

    private function sql_query_exec($sql = '') {
        // for list many sql text eg.insert
        if (empty($sql)) {
            return false;
        }

        return $this->connector->db_exec($sql);
    }

    private function sql_text($sql = '', $prefix = '') {
        // for test PREFIX
        $sql = str_replace("::PREFIX::", $prefix, $sql);
        return $sql;
    }

    private function sql_num_rows($sql, $prefix = '') {
        $sql = str_replace("::PREFIX::", $prefix, $sql);
        $query = $this->connector->db_sqlit_query($sql);
        $num_row = $this->connector->db_num_rows($query);
        return (int) $num_row;
    }

    private function _filter_data($table, $data) {
        $filtered_data = array();
        $columns = $this->_list_fields($table);
        if (is_array($data)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data)) {
                    if (is_array($data[$column])) {
                        $filtered_data[$column] = Mirakl_Tools::escape_str(serialize($data[$column]));
                    } else {
                        if ($data[$column] == '0') {
                            $filtered_data[$column] = $data[$column];
                        } else {
                            $filtered_data[$column] = Mirakl_Tools::escape_str($data[$column]);
                        }
                    }
                } else {
                    $filtered_data[$column] = '';
                }
            }
        }
        return $filtered_data;
    }

    private function _list_fields($table, $db_type = null) {
        return $this->connector->get_all_column_name($table, true, true);
    }

    private function insert_string($table, $data, $nf_prefix = true) {
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            return $this->connector->insert_string($table, $filter_data, $nf_prefix);
        }
        return null;
    }

    private function update_string($table, $data, $where = array(), $filter = true) {
        if (isset($data) && !empty($data)) {

            if ($filter) {
                $filter_data = $this->_filter_data($table, $data);
            } else {
                $filter_data = $data;
            }

            return $this->connector->update_string($table, $filter_data, $where, 0, true);
        }
        return null;
    }

    private function delete_string($table, $where = array(), $nf_prefix = true) {
        return $this->connector->delete_string($table, $where, $nf_prefix);
    }

    private function replace_quote($str) {
        return str_replace("'", " ", $str);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function exec_query($sql) {
        return $this->sql_query_exec($sql);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getMiraklCategorySelect($sub_marketplace, $id_country, $id_shop) {
        $data = array();
        $sql = "SELECT * FROM ::PREFIX::category_selected "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $id_category = $result['id_category'];

                $data[$id_category]['id_category_selected'] = $result['id_category_selected'];
                $data[$id_category]['id_category'] = $result['id_category'];
                $data[$id_category]['id_country'] = $result['id_country'];
                $data[$id_category]['id_shop'] = $result['id_shop'];
                $data[$id_category]['id_profile'] = isset($result['id_profile']) ? $result['id_profile'] : '';
            }
        }
        return $data;
    }

    public function getSettingCategory($id_shop) {
        $data = array();
        $sql = "SELECT C.id_category, C.id_parent, C.is_root_category, CL.id_lang, CL.name  "
                ."FROM ::PREFIX::category C "
                ."LEFT JOIN ::PREFIX::category_lang CL ON C.id_category = CL.id_category "
                ."WHERE C.id_shop = $id_shop ORDER BY C.id_category ";
        $results = $this->sql_query($sql, $this->prefix_product);

        if (isset($results) && !empty($results)) {
            foreach ($results as $key => $result) {
                $id_category = $result['id_category'];

                $data[$id_category]['id_parent'] = isset($result['id_parent']) ? $result['id_parent'] : ''; // set array id_category -> id_parent => value in array
                $data[$id_category]['name'] = isset($result['name']) ? $result['name'] : '';  // name in product_catagory_lang
            }
        }
        return $data;
    }

    public function getProductCategoryByProduct($id_shop, $product_id) {
        $data = array();
        $product_id = implode(", ", $product_id);

        $sql = "SELECT id_product, id_category "
                ."FROM ::PREFIX::product_category "
                ."WHERE id_shop = $id_shop AND id_product IN ($product_id) "
                ."ORDER BY id_product, id_category ";

        $results = $this->sql_query($sql, $this->prefix_product);

        if (isset($results) && !empty($results)) {
            foreach ($results as $key => $result) {
                $id_product = $result['id_product'];

                $data[$id_product][$key] = isset($result['id_category']) ? $result['id_category'] : ''; // set id_product -> id_categorories key => value in array
            }
        }
        return $data;
    }

    public function getSettingCarrier($id_shop) {

        $sql = "SELECT * "
                ."FROM ::PREFIX::carrier "
                ."WHERE id_shop = $id_shop "
                ."ORDER BY id_carrier ";

        $results = $this->sql_query($sql, $this->prefix_product);
        return $results;
    }

    public function getProductIdInCategory($id_shop, $id_categories) {

        if (!empty($id_categories)) {
            $id_categories = implode(", ", $id_categories);
        }

        $sql = "SELECT P.id_product "
                ."FROM ::PREFIX::product_category PC "
                ."INNER JOIN ::PREFIX::product P ON PC.id_product = P.id_product "
                ."WHERE P.active = 1 AND P.id_shop = $id_shop AND IFNULL(P.id_category_default,'') IN ($id_categories) " //PC.id_category IN ($id_categories) "
                ."GROUP BY P.id_product "
                ."ORDER BY P.id_product ";
        $results = $this->sql_query($sql, $this->prefix_product);
        return $results;
    }

    public function getProductIdInCategoryByFlag($id_shop, $category, $ext, $sub_marketplace, $id_marketplace, $all = false) {
        $whereflag = '';

        if (!empty($category)) {
            $categories = implode(", ", $category);
        }

        $sql = "SELECT P.id_product "
                ."FROM ::PREFIX::product_category PC "
                ."INNER JOIN ::PREFIX::product P ON PC.id_product = P.id_product ";

        if (!$all) {
            $sql.= "LEFT JOIN {$this->prefix_offer}flag_update FU ON P.id_product = FU.id_product AND FU.id_site = '$ext' AND FU.sub_marketplace = '$sub_marketplace' AND P.id_shop = FU.id_shop AND FU.id_marketplace = $id_marketplace ";
            $whereflag = " AND (FU.flag IS NULL OR FU.flag = 1) ";
        }

        //$sql.= "WHERE P.id_shop = $id_shop AND PC.id_category IN ($categories) $whereflag "
        $sql.= "WHERE P.id_shop = $id_shop AND IFNULL(P.id_category_default,'') IN ($categories) $whereflag "
                ."GROUP BY P.id_product "
                ."ORDER BY P.id_product ";
        //$this->log_message($sql, 'sql_export_offer');
        $results = $this->sql_query($sql, $this->prefix_product);
        return $results;
    }

    public function getProductAttribute($id_shop, $id_lang) {

        $data_attribute = array();
        $data_group = array();

        $sql_group = "SELECT AG.id_attribute_group, AG.is_color_group, name "
                ."FROM ::PREFIX::attribute_group AG "
                ."INNER JOIN ::PREFIX::attribute_group_lang AGL ON AG.id_attribute_group = AGL.id_attribute_group "
                ."WHERE AG.id_shop = $id_shop AND AGL.id_lang = $id_lang ";
        $results_group = $this->sql_query($sql_group, $this->prefix_product);

        $sql_attribute = "SELECT A.id_attribute_group, A.id_attribute, AL.name "
                ."FROM ::PREFIX::attribute A "
                ."INNER JOIN ::PREFIX::attribute_lang AL ON A.id_attribute = AL.id_attribute "
                ."WHERE A.id_shop = $id_shop AND AL.id_lang = $id_lang ";
        $results_attribute = $this->sql_query($sql_attribute, $this->prefix_product);

        if (isset($results_attribute) && !empty($results_attribute)) {
            foreach ($results_attribute as $result_attribute) {
                $id_attribute_group = $result_attribute['id_attribute_group'];
                $id_attribute = $result_attribute['id_attribute'];
                $name_attribute = $result_attribute['name'];
                $data_attribute[$id_attribute_group][$id_attribute] = $name_attribute;
            }
        }

        if (isset($results_group) && !empty($results_group)) {
            foreach ($results_group as $result_group) {
                $id_attribute_group = $result_group['id_attribute_group'];
                $is_color_group = $result_group['is_color_group'];
                $name_group = $result_group['name'];

                if (isset($data_attribute[$id_attribute_group]) && !empty($data_attribute[$id_attribute_group])) {
                    $data_group[$id_attribute_group]['name'] = $name_group;
                    $data_group[$id_attribute_group]['value'] = $data_attribute[$id_attribute_group];
                    $data_group[$id_attribute_group]['is_color_group'] = $is_color_group;
                }
            }
        }

        return $data_group;
    }

    public function getProductFeature($id_shop, $id_lang) {
        $data_feature = array();

        $sql = "SELECT F.id_feature, F.name, FV.id_feature_value, FV.value  "
                ."FROM ::PREFIX::feature F "
                ."INNER JOIN ::PREFIX::feature_value FV ON F.id_feature = FV.id_feature "
                ."WHERE F.id_shop= $id_shop AND F.id_lang = $id_lang ";

        $features = $this->sql_query($sql, $this->prefix_product);

        if (isset($features) && !empty($features)) {
            foreach ($features as $feature) {
                $id_feature = $feature['id_feature'];
                $name = $feature['name'];
                $id_feature_value = $feature['id_feature_value'];
                $value = $feature['value'];

                $data_feature[$id_feature]['name'] = $name;
                $data_feature[$id_feature]['value'][$id_feature_value] = $value;
            }
        }

        //echo '<pre>'; print_r($data_feature); exit;
        return $data_feature;
    }

//    public function getProductAvailable($id_shop) {
//        $sql = "SELECT P.id_product, IFNULL(PA.reference, P.reference) as 'reference' "
//                ."FROM ::PREFIX::product P "
//                ."LEFT JOIN ::PREFIX::product_attribute PA ON P.id_product = PA.id_product AND P.id_shop = PA.id_shop "
//                ."LEFT JOIN {$this->prefix_offer}product F ON P.id_product = F.id_product AND P.id_shop = F.id_shop "
//                ."LEFT JOIN {$this->prefix_offer}product_attribute FA ON F.id_product = FA.id_product AND P.id_shop = F.id_shop "
//                ."LEFT JOIN marketplace_product_option MPO ON IFNULL(PA.reference, P.reference) = MPO.sku "
//                ."WHERE P.id_shop = $id_shop "
//                ."AND P.active = 1 "
//                ."AND IFNULL(MPO.c_disable, 0) != 1 "
//                ."AND (IFNULL(P.quantity, 0) > 0 OR IFNULL(PA.quantity, 0) > 0 OR IFNULL(F.quantity, 0) > 0 OR IFNULL(FA.quantity, 0) > 0) "
//                ."ORDER BY P.id_product ";
//
//        $results = $this->sql_query($sql, $this->prefix_product);
//        return $results;
//    }

    public function getProductAvailable($id_shop, $id_lang) {
        $sql = "SELECT P.id_product, IFNULL(PA.reference, P.reference) as 'reference', P.active, IFNULL(MPO.c_disable, 0) 'disable', "
                ."(IFNULL(P.quantity,0) + IFNULL(PA.quantity, 0) + IFNULL(F.quantity, 0) + IFNULL(FA.quantity, 0)) sum_quantity "
                ."FROM ::PREFIX::product P "
                ."LEFT JOIN ::PREFIX::product_attribute PA ON P.id_product = PA.id_product AND P.id_shop = PA.id_shop "
                ."LEFT JOIN {$this->prefix_offer}product F ON P.id_product = F.id_product AND P.id_shop = F.id_shop "
                ."LEFT JOIN {$this->prefix_offer}product_attribute FA ON F.id_product = FA.id_product AND P.id_shop = F.id_shop "
                ."LEFT JOIN marketplace_product_option MPO ON P.id_product = MPO.id_product AND P.id_shop = MPO.id_shop AND MPO.id_lang = $id_lang "
                ."WHERE P.id_shop = $id_shop "
                ."GROUP BY P.id_product, PA.id_product_attribute "
//                ."AND P.active = 1 "
//                ."AND IFNULL(MPO.c_disable, 0) != 1 "
//                ."AND (IFNULL(P.quantity, 0) > 0 OR IFNULL(PA.quantity, 0) > 0 OR IFNULL(F.quantity, 0) > 0 OR IFNULL(FA.quantity, 0) > 0) "
                ."ORDER BY P.id_product ";

        $results = $this->sql_query($sql, $this->prefix_product);
        return $results;
    }

    public function getProductOrAttributeBySku($id_shop, $reference) {
        $sql = "SELECT IF (PA.id_product, concat(PA.id_product, \"_\", PA.id_product_attribute), P.id_product) AS 'id_product' "
                ."FROM ::PREFIX::product P "
                ."LEFT JOIN ::PREFIX::product_attribute PA ON P.id_product = PA.id_product "
                ."WHERE (PA.reference = '$reference' OR P.reference = '$reference') AND P.id_shop = $id_shop "
                ." LIMIT 1 ";

        $result = $this->sql_query($sql, $this->prefix_product, true, 'id_product');
        return $result;
    }

    public function getProductQuantity($id_shop, $id_product, $id_product_attribute) {
        if (!empty($id_product_attribute)) {
            $sql = "SELECT PA.quantity "
                    ."FROM ::PREFIX::product P "
                    ."LEFT JOIN ::PREFIX::product_attribute PA ON (P.id_product = PA.id_product) "
                    ."WHERE PA.id_product_attribute = $id_product_attribute AND P.id_product = $id_product ";
        } else {
            $sql = "SELECT P.quantity FROM ::PREFIX::product P WHERE P.id_product = $id_product ";
        }

        $sql.= "AND P.id_shop = $id_shop ";
        $result = $this->sql_query($sql, $this->prefix_product, true, 'quantity');
        return $result;
    }

    public function getProductTax($list_id_product = array()) {
        $list_id_product = implode(",", $list_id_product);
        $sql = "SELECT P.id_product, PT.rate, PT.type "
                ."FROM ::PREFIX::product P "
                ."LEFT JOIN ::PREFIX::tax PT ON P.id_tax = PT.id_tax "
                ."WHERE P.id_product IN ($list_id_product) "
                ."ORDER BY P.id_product ";

        $results = $this->sql_query($sql, $this->prefix_product);

        $data = array_reduce($results, function($result, $value) {
            $result[$value['id_product']] = $value;
            return $result;
        });

        return $data;
    }

    public function getProductBySkuMirakl($id_shop, $sku, $params) {
        $details = array();
        $condition_sku = "AND (
                    PA.reference = CASE WHEN PA.reference = '$sku' THEN '$sku' END
                    OR PA.sku = CASE WHEN PA.sku = '$sku' THEN '$sku' END
                    OR PA.ean13 = CASE WHEN PA.ean13 = '$sku' THEN '$sku' END
                    OR PA.upc = CASE WHEN PA.upc = '$sku' THEN '$sku' END
                    OR P.reference = CASE WHEN P.reference = '$sku' THEN '$sku' END
                    OR P.sku = CASE WHEN P.sku = '$sku' THEN '$sku' END
                    OR P.ean13 = CASE WHEN P.ean13 = '$sku' THEN '$sku' END
                    OR P.upc = CASE WHEN P.upc = '$sku' THEN '$sku' END
                )";

        $sql = "SELECT P.id_product AS id_product, PA.id_product_attribute AS id_product_attribute "
                ."FROM ::PREFIX::product P "
                ."LEFT JOIN ::PREFIX::product_attribute PA ON P.id_product = PA.id_product AND P.id_shop = PA.id_shop "
                ."WHERE P.id_shop = $id_shop $condition_sku ";

        $results = $this->sql_query($sql, $this->prefix_product);

        if (isset($results) && !empty($results)) {
            foreach ($results as $product) {
                $details = new Product($params['user_name'], $product['id_product'], $params['id_lang'], $params['id_mode'], $id_shop);
                if (isset($product['id_product_attribute'])) {
                    $details->id_product_attribute = $product['id_product_attribute'];
                }
                return $details;
            }
        }

        return false;
    }

    public function getMiraklProfile($sub_marketplace, $id_country, $id_shop) {
        $content = array();

        $sql = "SELECT P.*, M.model_name "
                ."FROM ::PREFIX::profile P "
                ."LEFT JOIN ::PREFIX::mapping_model M ON P.id_model = M.id_model "
                ."WHERE P.sub_marketplace = $sub_marketplace AND P.id_country = $id_country AND P.id_shop = $id_shop "
                ."ORDER BY P.name ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (!empty($results)) {
            foreach ($results as $keys => $value) {
                foreach ($value as $key => $val) {
                    if (!isset($id_profile) || empty($id_profile)) {
                        $keys = $value['id_profile'];
                    }

                    if (!is_int($key)) {
                        $key = str_replace(array('p.', 'm.'), '', $key);
                        if ($key == 'price_rules' || $key == 'other') {
                            $content[$keys][$key] = unserialize($val);
                        } else {
                            $content[$keys][$key] = $val;
                        }
                    }
                }
            }
        }

        return $content;
    }

    public function getMiraklConfigHierarchy($sub_marketplace, $is_level = false) {

//        $sql = "SELECT id_config_hierarchy, code, label, level, parent_code "
//                ."FROM ::PREFIX::config_hierarchy WHERE sub_marketplace = $sub_marketplace ";
//
//        if ($is_level) {
//            $sql.= "AND level = 1";
//        }
        if ($is_level) {
//            $sql = "SELECT M.id_config_hierarchy, M.code, M.label, M.level, M.parent_code, count(D.parent_code) count_sub_hierarchy, M.sub_marketplace "
//                    ."FROM mirakl_config_hierarchy M "
//                    ."LEFT JOIN mirakl_config_hierarchy D ON M.code = D.parent_code "
//                    ."GROUP BY D.parent_code, M.code "
//                    ."HAVING M.level = 1 AND M.sub_marketplace = $sub_marketplace "
//                    ."ORDER BY M.id_config_hierarchy ";

            $sql = "SELECT M.id_config_hierarchy, M.code, M.label, M.level, M.parent_code, count(D.parent_code) count_sub_hierarchy, "
                    ."( SELECT count(A.hierarchy_code) FROM ::PREFIX::config_attribute A WHERE A.sub_marketplace = $sub_marketplace AND A.hierarchy_code != '' AND M.code = A.hierarchy_code) count_attribute "
                    ."FROM mirakl_config_hierarchy M "
                    ."LEFT JOIN ::PREFIX::config_hierarchy D ON M.code = D.parent_code "
                    ."WHERE M.sub_marketplace = $sub_marketplace AND M.level = 1 "
                    ."GROUP BY D.parent_code, M.code "
                    ."ORDER BY M.id_config_hierarchy ";
        } else {
            $sql = "SELECT id_config_hierarchy, code, label, level, parent_code "
                    ."FROM ::PREFIX::config_hierarchy WHERE sub_marketplace = $sub_marketplace ";
        }

        return $this->sql_query($sql, $this->prefix_mirakl);
    }

    public function getMiraklSubHierarchy($id_config_hierarchy, $sub_marketplace) {
//        $sql = "SELECT * FROM ::PREFIX::config_hierarchy "
//                ."WHERE sub_marketplace = $sub_marketplace "
//                ."AND parent_code IN (SELECT code FROM ::PREFIX::config_hierarchy WHERE id_config_hierarchy = $id_config_hierarchy AND sub_marketplace = $sub_marketplace ) ";

        $sql = "SELECT M.id_config_hierarchy, M.code, M.label, M.level, M.parent_code, count(D.parent_code) count_sub_hierarchy,"
                ."( SELECT count(A.hierarchy_code) FROM ::PREFIX::config_attribute A WHERE A.sub_marketplace = $sub_marketplace AND A.hierarchy_code != '' AND M.code = A.hierarchy_code) count_attribute "
                ."FROM ::PREFIX::config_hierarchy M "
                ."LEFT JOIN ::PREFIX::config_hierarchy D ON M.code = D.parent_code "
                ."WHERE M.sub_marketplace = $sub_marketplace AND M.parent_code IN (SELECT code FROM ::PREFIX::config_hierarchy WHERE id_config_hierarchy = $id_config_hierarchy AND sub_marketplace = $sub_marketplace ) "
                ."GROUP BY D.parent_code, M.code "
                ."ORDER BY M.id_config_hierarchy ";

        //print_r($sql);
        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getMiraklMappingModel($sub_marketplace, $id_country, $id_shop, $id_lang) {

        $data = array();
        $set_shop_attribute = array();

        $sql_model = "SELECT id_model, model_name, hierarchy_code FROM ::PREFIX::mapping_model WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";
        $results_model = $this->sql_query($sql_model, $this->prefix_mirakl);

        if (isset($results_model) && !empty($results_model)) {
            foreach ($results_model as $result_model) {

                $id_model = $result_model['id_model'];
                $model_name = $result_model['model_name'];
                $hierarchy_code = implode(", ", unserialize($result_model['hierarchy_code']));

//                $sql_hierarchy = "SELECT M.id_config_hierarchy, M.code, M.label, M.level, M.parent_code "
//                        ."FROM ::PREFIX::config_hierarchy M WHERE M.sub_marketplace = $sub_marketplace AND M.id_config_hierarchy IN ($hierarchy_code) ";
//                $results_hierarchy = $this->sql_query($sql_hierarchy, $this->prefix_mirakl);

                $sql_hierarchy = "SELECT M.id_config_hierarchy, M.code, M.label, M.level, M.parent_code, count(D.parent_code) count_sub_hierarchy, "
                        ."( SELECT count(A.hierarchy_code) FROM ::PREFIX::config_attribute A WHERE A.sub_marketplace = $sub_marketplace AND A.hierarchy_code != '' AND M.code = A.hierarchy_code) count_attribute "
                        ."FROM ::PREFIX::config_hierarchy M "
                        ."LEFT JOIN ::PREFIX::config_hierarchy D ON M.code = D.parent_code "
                        ."WHERE M.sub_marketplace = $sub_marketplace AND M.id_config_hierarchy IN ($hierarchy_code) "
                        ."GROUP BY D.parent_code, M.code "
                        ."ORDER BY M.id_config_hierarchy ";
                $results_hierarchy = $this->sql_query($sql_hierarchy, $this->prefix_mirakl);

                if (isset($results_hierarchy) && !empty($results_hierarchy)) {
                    foreach ($results_hierarchy as $index => $hierarchy) {
                        if ($index != 0) {
                            $h = "SELECT * FROM ::PREFIX::config_hierarchy WHERE sub_marketplace = $sub_marketplace AND parent_code IN ('{$hierarchy['parent_code']}')";
                            $results_h = $this->sql_query($h, $this->prefix_mirakl);
                            $results_hierarchy[$index]['hierarchy_code_level'] = $results_h;
                        }
                    }
                }

                $data[$id_model]['id_model'] = $id_model;
                $data[$id_model]['model_name'] = $model_name;
                $data[$id_model]['hierarchy_code'] = $results_hierarchy;

                $sql_detail = "SELECT MA.id_model_attribute, MA.id_config_attribute, CA.code, CA.label, CA.description, CA.type, "
                        ."CA.example, CA.required, MA.shop_attribute, CA.type_parameter, MA.default_value, IFNULL(MV.id_model_value, '') id_model_value, "
                        ."IFNULL(MV.id_config_value_list, '') id_config_value_list, IFNULL(MV.shop_attribute_value, '') shop_attribute_value, "
                        ."IFNULL(CV.id_config_value, '') id_config_value, IFNULL(CV. CODE, '') code_list,IFNULL(CV.label, '') label_list "
                        ."FROM ::PREFIX::mapping_model_attribute MA "
                        ."LEFT JOIN ::PREFIX::mapping_model_value MV ON MA.id_model_attribute = MV.id_model_attribute "
                        ."LEFT JOIN ::PREFIX::config_attribute CA ON MA.id_config_attribute = CA.id_config_attribute "
                        //."LEFT JOIN ::PREFIX::config_value_list CV ON MV.id_config_value_list = CV.id_config_value_list "
                        ."LEFT JOIN ::PREFIX::config_value_list CV ON MA.shop_attribute = CV.id_config_value_list "
                        ."WHERE MA.id_model = $id_model "
                        ."ORDER BY CA.required DESC, MA.id_model_attribute, MV.id_model_value ";
                $results_detail = $this->sql_query($sql_detail, $this->prefix_mirakl);

                //echo '<pre>'; print_r($results_detail); exit;


                if (isset($results_detail) && !empty($results_detail)) {
                    foreach ($results_detail as $detail) {

                        $id_model_attribute = $detail['id_model_attribute'];
                        $id_config_attribute = $detail['id_config_attribute'];
                        $shop_attribute = $detail['shop_attribute'];
                        $id_model_value = $detail['id_model_value'];


                        if (!empty($shop_attribute) && !in_array($shop_attribute, $set_shop_attribute)) {
                            $shop_field = $this->getValueByFieldShop($id_shop, $id_lang, array(), $shop_attribute);
                            $set_shop_attribute[$shop_attribute] = $shop_attribute;
                        }

                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['code'] = $detail['code'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['label'] = $detail['label'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['type'] = $detail['type'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['attribute'] = isset($shop_field['results']) ? $shop_field['results'] : '';
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['is_color_group'] = isset($shop_field['is_color_group']) ? $shop_field['is_color_group'] : '';
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['description'] = $detail['description'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['example'] = $detail['example'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['required'] = $detail['required'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['shop_attribute'] = $detail['shop_attribute'];
                        $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['default_value'] = $detail['default_value'];

                        if (!empty($id_model_value)) {
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['id_model_value'] = $detail['id_model_value'];
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['id_config_value_list'] = $detail['id_config_value_list'];
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['shop_attribute_value'] = $detail['shop_attribute_value'];
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['id_config_value'] = $detail['id_config_value'];
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['code_list'] = $detail['code_list'];
                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['label_list'] = $detail['label_list'];

                            //$data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'][$id_model_value]['value_list'][] = $detail['label_list'];
                        }

                        if ($detail['type'] == 'LIST') {

                            $sql_value = " SELECT * "
                                    ."FROM ::PREFIX::config_value V "
                                    ."INNER JOIN ::PREFIX::config_value_list VL ON V.id_config_value = VL.id_config_value "
                                    ."WHERE BINARY V.code = '{$detail['type_parameter']}' "; // BINARY is case sentitive
                            $results_value = $this->sql_query($sql_value, $this->prefix_mirakl);

                            //echo '<pre>'; print_r($results_value); exit;
                            //$results[$index]['value_list'] = isset($results_value) ? $results_value : '';
                            //$s = "SELECT * FROM ::PREFIX::config_value_list WHERE id_config_value =  {$detail['id_config_value']} ";
                            //echo '<pre>'; print_r($detail); exit;
                            //$data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'] = $this->sql_query($s, $this->prefix_mirakl);

                            $data[$id_model]['attribute_with_hierarchy'][$id_config_attribute]['value_list'] = isset($results_value) ? $results_value : '';
                        }
                    }
                }
            }
        }
        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getAttributeWithHierarchy($id_config_hierarchy_array, $sub_marketplace) {

        $data = array();
        $id_config_hierarchy_array = implode(", ", $id_config_hierarchy_array);

        $sql = "SELECT A.* "
                ."FROM ::PREFIX::config_attribute A "
                ."WHERE hierarchy_code IN "
                ."( SELECT code FROM ::PREFIX::config_hierarchy WHERE sub_marketplace = $sub_marketplace AND id_config_hierarchy IN ($id_config_hierarchy_array) ) "
                ."ORDER BY A.required DESC, A.id_config_attribute ";

        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) { // gte value list
            foreach ($results as $index => $result) {
                if (!empty($result['type_parameter'])) {
                    $sql_value = " SELECT * "
                            ."FROM ::PREFIX::config_value V "
                            ."INNER JOIN ::PREFIX::config_value_list VL ON V.id_config_value = VL.id_config_value "
                            ."WHERE BINARY V.code = '{$result['type_parameter']}' "; // BINARY is case sentitive
                    $results_value = $this->sql_query($sql_value, $this->prefix_mirakl);

                    $results[$index]['value_list'] = isset($results_value) ? $results_value : '';
                }
            }
        }
        //print_r($results); exit;
        return $results;
    }

    public function deleteProfile($data) {

        $table = $this->prefix_mirakl.'profile';
        $id_profile = isset($data['id_profile']) ? $data['id_profile'] : '';

        if (empty($id_profile)) {
            return false;
        }

        $where = array(
            "id_profile" => array('operation' => "=", 'value' => $id_profile),
            "sub_marketplace" => array('operation' => "=", 'value' => $data['sub_marketplace']),
            "id_country" => array('operation' => "=", 'value' => $data['id_country']),
            "id_shop" => array('operation' => "=", 'value' => $data['id_shop'])
        );

        $sql = $this->delete_string($table, $where);
        return $this->sql_query_effect($sql);
    }

    public function deleteModel($data) {

        /* delete model 
         * 1. delete model (mirakl_mapping_model, mirakl_mapping_model_attribute, mirakl_mapping_model_value)
         * 2. update profile id_model is '' as mapping model
         * 
         */

        $sql_delete = '';
        $flag_model = false;
        $table_model = $this->prefix_mirakl.'mapping_model';
        $table_model_attribute = $this->prefix_mirakl.'mapping_model_attribute';
        $table_model_value = $this->prefix_mirakl.'mapping_model_value';

        $id_model = isset($data['id_model']) ? $data['id_model'] : '';

        if (empty($id_model)) {
            return false;
        }

        $sub_marketplace = $data['sub_marketplace'];
        $id_shop = $data['id_shop'];
        $id_country = $data['id_country'];

        ///////////////////////////////////////////////////////////////////////////////////////
        // del model
        $where = array(
            "id_model" => array('operation' => "=", 'value' => $id_model),
            "sub_marketplace" => array('operation' => "=", 'value' => $sub_marketplace),
            "id_shop" => array('operation' => "=", 'value' => $id_shop),
            "id_country" => array('operation' => "=", 'value' => $id_country),
        );

        $sql_delete = $this->delete_string($table_model, $where);
        $flag_model = $this->sql_query_effect($sql_delete);

        if (!$flag_model) {
            return false;
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        // del model value need before model attribute !
        $sql = "SELECT id_model_attribute FROM $table_model_attribute WHERE id_model = $id_model ";
        $id_model_attribute = $this->sql_query($sql);

        if (isset($id_model_attribute) && !empty($id_model_attribute)) {

            $model_attribute = array_map(function($params) {
                return $params['id_model_attribute'];
            }, $id_model_attribute);

            $model_attribute = implode(",", $model_attribute);

            $sql_delete = "DELETE FROM $table_model_value WHERE id_model_attribute IN ($model_attribute) ; ";
            $this->sql_query_effect($sql_delete);
        }
        ///////////////////////////////////////////////////////////////////////////////////////
        // del model attribute
        $where = array(
            "id_model" => array('operation' => "=", 'value' => $id_model),
        );

        $sql_delete = $this->delete_string($table_model_attribute, $where);
        $this->sql_query_effect($sql_delete);
        ///////////////////////////////////////////////////////////////////////////////////////
        // upd profile
        $sql = "UPDATE {$this->prefix_mirakl}profile SET id_model = '' WHERE id_model = $id_model ; ";
        $this->sql_query_effect($sql);
        ///////////////////////////////////////////////////////////////////////////////////////

        return $flag_model;
    }

    public function saveProfile($data) {     // to use
        //$sql = '';
        $table = $this->prefix_mirakl.'profile';
        $sub_marketplace = $data['sub_marketplace'];
        $id_country = $data['id_country'];
        $id_shop = $data['id_shop'];
        $marketplace = $data['marketplace'];
        $date_add = date('Y-m-d H:i:s');

        if (isset($data['profile']) && !empty($data['profile'])) {
            foreach ($data['profile'] as $profile) {

                $value = array();
                $value = $profile;
                $value['sub_marketplace'] = $sub_marketplace;
                $value['id_country'] = $id_country;
                $value['id_shop'] = $id_shop;
                $value['marketplace'] = $marketplace;
                $value['date_add'] = $date_add;

                if (isset($profile['product_format'])) {
                    $value['product_format'] = $profile['product_format'];
                }

                if (isset($profile['no_image'])) {
                    $value['no_image'] = $profile['no_image'];
                }

                if (isset($profile['price_rules']['value']) && !empty($profile['price_rules']['value'])) { // into
                    foreach ($profile['price_rules']['value'] as $price_rule_key => $price_rule) {
                        if (empty($price_rule['type']) || empty($price_rule['to']) || empty($price_rule['value'])) {
                            unset($value['price_rules']['value'][$price_rule_key]); // not into
                        }
                    }
                }

                if (isset($profile['combinations_long_attr'])) {
                    $value['combinations_long_attr'] = 1;
                } else {
                    $value['combinations_long_attr'] = 0;
                }

                if (isset($profile['html_description'])) {
                    $value['html_description'] = 1;
                } else {
                    $value['html_description'] = 0;
                }

                if (isset($profile['additionnals']) && !empty($profile['additionnals'])) {
                    foreach ($profile['additionnals'] as $additionnal_code => $additionnal) {
                        if ($additionnal['option'] != MiraklParameter::DEFAULT_VALUE) {
                            $value['additionnals'][$additionnal_code]['text'] = '';
                        }
                    }
                    $additionnals = Mirakl_Tools::escape_str(serialize($value['additionnals']));
                    $value['other'] = $additionnals;
                } else {
                    $value['other'] = '';
                }

                //////////////////////////////////////////////

                if (isset($value['id_profile']) && !empty($value['id_profile'])) {
                    $where = array(
                        'id_profile' => array('operation' => '=', 'value' => $value['id_profile'])
                    );

                    $sql = $this->update_string($table, $value, $where);
                } else {
                    $sql = $this->insert_string($table, $value);
                }

                $this->sql_query_effect($sql);
            }
        }
        return true;
    }

    public function saveMiraklCategorySelect($data) {
        //echo '<pre>'; print_r($data); exit;
        $sql = $include_date = '';
        $table = $this->prefix_mirakl.'category_selected';
        $sub_marketplace = $data['sub_marketplace'];
        $id_country = $data['id_country'];
        $id_shop = $data['id_shop'];
        $marketplace = $data['marketplace'];
        $date_add = date('Y-m-d H:i:s');

        if (isset($data['category']) && !empty($data['category'])) {
            foreach ($data['category'] as $value) {
                $value['sub_marketplace'] = $sub_marketplace;
                $value['marketplace'] = $marketplace;
                $value['id_country'] = $id_country;
                $value['id_shop'] = $id_shop;
                $value['date_add'] = $date_add;

                if (isset($value['id_category']) && !empty($value['id_category'])) {
                    $sql .= $this->insert_string($table, $value);
                }
            }

            if (!empty($sql)) {
                $include_date = " AND date_add < '".$date_add."'";
                $this->sql_query_exec($sql);
            }
        }

        $sql_delete = "DELETE FROM $table WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country  AND id_shop =  $id_shop $include_date ;";
        return $this->sql_query_effect($sql_delete);
    }

    public function getProductCarrier($id_shop) {
        $sql = "SELECT id_carrier, name, is_default "
                ."FROM ::PREFIX::carrier "
                ."WHERE id_shop = $id_shop ";

        $results = $this->sql_query($sql, $this->prefix_product);
        return $results;
    }

    public function getMiraklConfigCarrier($sub_marketplace, $id_country, $id_shop) { // use in (mirak.php, mirakl.orders.import.php)
        $data = array();

        $sql = "SELECT MCC.id_config_carrier, MCC.`code`, MCC.label, MCC.tracking_url, MMC.id_carrier "
                ."FROM ::PREFIX::config_carrier MCC "
                ."LEFT JOIN ::PREFIX::mapping_carrier MMC ON MCC.id_config_carrier = MMC.id_config_carrier AND MMC.sub_marketplace = $sub_marketplace AND MMC.id_country = $id_country AND MMC.id_shop = $id_shop "
                ."WHERE MCC.sub_marketplace = $sub_marketplace ";

        $results = $this->sql_query($sql, $this->prefix_mirakl);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // index carrier 0 set default only !importance !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $sql_default = "SELECT id_carrier "
                ."FROM ::PREFIX::mapping_carrier "
                ."WHERE id_config_carrier = 0 AND sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop  ";
        $results_default = $this->sql_query($sql_default, $this->prefix_mirakl, true, 'id_carrier');

        if (isset($results_default) && empty($results_default)) {
            $results_default = 0; // id_carrier
        }
        $default = array('id_config_carrier' => 0, 'code' => 'default', 'label' => 'default', 'tracking_url' => '', 'id_carrier' => $results_default);
        $data[0] = $default;
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $index = 1;
        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $data[$index]['id_config_carrier'] = $result['id_config_carrier'];
                $data[$index]['code'] = $result['code'];
                $data[$index]['label'] = $result['label'];
                $data[$index]['tracking_url'] = $result['tracking_url'];
                $data[$index]['id_carrier'] = $result['id_carrier'];

                $index = $index + 1;
            }
        }

        return $data;
    }

    public function saveMiraklCarrier($data) {
        $table = $this->prefix_mirakl.'mapping_carrier';

        $id_mapping_carrier = '';
        $id_field = array();
        $id_field_upd = array();
        $sub_marketplace = $data['sub_marketplace'];
        $id_country = $data['id_country'];
        $id_shop = $data['id_shop'];

        $condition = "sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";

        $sql_update = "UPDATE $table SET id_carrier = CASE id_mapping_carrier ";
        $sql_upd = "";

        if (isset($data['mapping_carrier']) && !empty($data['mapping_carrier'])) {
            foreach ($data['mapping_carrier'] as $mapping_carrier) {

                $id_config_carrier = !empty($mapping_carrier['id_config_carrier']) ? $mapping_carrier['id_config_carrier'] : 0;
                $id_carrier = !empty($mapping_carrier['id_carrier']) ? $mapping_carrier['id_carrier'] : 0;
                $date_add = date("Y-m-d H:i:s");

                $sql_query = "SELECT id_mapping_carrier FROM $table WHERE $condition AND id_config_carrier = $id_config_carrier ";
                $id_mapping_carrier = $this->sql_query($sql_query, false, true, 'id_mapping_carrier');

                if (isset($id_mapping_carrier) && !empty($id_mapping_carrier)) {
                    // update

                    $sql_upd .= " WHEN $id_mapping_carrier THEN $id_carrier ";
                    $id_field_upd[] = $id_mapping_carrier;
                } else {
                    // insert
                    $sql_ins = "INSERT INTO $table (sub_marketplace, id_country, id_shop, id_config_carrier, id_carrier, date_add) "
                            ."VALUES ($sub_marketplace, $id_country, $id_shop, $id_config_carrier, $id_carrier, '$date_add' ) ; ";
                    $id_mapping_carrier = $this->sql_query_effect($sql_ins, true);
                }

                $id_field[] = $id_mapping_carrier;
            }
        }

        if (!empty($sql_upd) && !empty($id_field_upd)) {
            // query update
            $id_field_upd = implode(",", $id_field_upd);
            $sql_update.= $sql_upd .= "END ";
            $sql_update.= "WHERE $condition AND id_mapping_carrier IN ($id_field_upd) ";
            $this->sql_query_effect($sql_update);
        }

        if (isset($id_field) && !empty($id_field)) {
            // delete
            $id_field = implode(",", $id_field);
            $sql_del = "DELETE FROM $table WHERE $condition AND id_mapping_carrier NOT IN ($id_field) ; ";
            $this->sql_query_effect($sql_del);
        }

        return !empty($id_mapping_carrier) ? true : false;
    }

    public function getMiraklLogLast($sub_marketplace, $id_country, $id_shop) {
        /* default */
        $never = Mirakl_Tools::l('Never');

        /*  NU normal update, CU cron update */
        $data = array(
            'NU' => array('id_log' => '', 'is_cron' => '', 'date_add_date' => $never, 'date_add_time' => $never),
            'CU' => array('id_log' => '', 'is_cron' => '', 'date_add_date' => $never, 'date_add_time' => $never)
        );
        $table = $this->prefix_mirakl.'log';

        $sql = "(SELECT id_log, is_cron, DATE_FORMAT(date_add, '%Y - %m - %d') 'date_add_date', DATE_FORMAT(date_add,'%H : %i : %s') 'date_add_time' "
                ."FROM $table WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND is_cron = 0 ORDER BY date_add DESC LIMIT 1) "
                ."UNION"
                ."(SELECT id_log, is_cron, DATE_FORMAT(date_add, '%Y - %m - %d') 'date_add_date', DATE_FORMAT(date_add,'%H : %i : %s') 'date_add_time'"
                ."FROM $table WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND is_cron = 1 ORDER BY date_add DESC LIMIT 1) ";
        $results = $this->sql_query($sql);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $is_cron = $result['is_cron'];
                if ($is_cron == 1) {
                    $data['NU'] = $result;
                } else {
                    $data['CU'] = $result;
                }
            }
        }

        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getMiraklLog($sub_marketplace, $id_country, $id_shop, $page = 0, $filter = array()) {

        if (empty($sub_marketplace) || empty($id_country) || empty($id_shop)) {
            return false;
        }

        $table = $this->prefix_mirakl.'log';
        $data = array();
        $option_condition = '';
        $limit_page = 5;


        $sql = "SELECT id_log, batch_id, feed_type, "
                ."count_process, count_send, count_success, count_error, count_skipped, count_warning, "
                ."is_cron, log_status, log_key, log_value, "
                ."DATE_FORMAT(date_add, '%Y - %m - %d') 'date_add_date', DATE_FORMAT(date_add,'%H : %i : %s') 'date_add_time', date_add "
                ."FROM $table "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";




//        if (isset($cron) && $cron == true) {
//            $sql.= "AND is_cron = 1 ";
//        }
//
//        if (isset($cron) && $cron == false) {
//            $sql.= "AND is_cron != 1 ";
//        }
        // print_r($sql); exit;
//        if (isset($batch_id) && !empty($batch_id)) { // no into
//            $sql.= "AND batch_id = '$batch_id' ";
//        }

        if (isset($filter) && !empty($filter)) {
            if (isset($filter['date_from']) && !empty($filter['date_from'])) {
                $date_from = "'".$filter['date_from']."'";
                $option_condition .= "AND date(date_add) >= $date_from ";
            }
            if (isset($filter['date_to']) && !empty($filter['date_to'])) {
                $date_to = "'".$filter['date_to']."'";
                $option_condition .= "AND date(date_add) <= $date_to ";
            }
            if (isset($filter['feed_type']) && !empty($filter['feed_type'])) {
                $feed_type = $filter['feed_type'];
                $option_condition .= "AND feed_type = '$feed_type' ";
            }
        }

        if (!empty($option_condition)) {
            $sql.= $option_condition;
        }

        $sql.= "ORDER BY date_add DESC ";

//        if ($last_log) {
//            //$sql.= "LIMIT 1 ";
//        } else {
        if (isset($page) && !empty($page)) {
            $offset_page = $page * ($limit_page);
            $sql .= "LIMIT $offset_page, $limit_page ";
        } else {
            if (empty($filter)) {
                $sql .= "LIMIT $limit_page ";
            }
        }
        //  }


        $results = $this->sql_query($sql);

        if (isset($results) && !empty($results)) {
            foreach ($results as $i => $r) {

                $feed_type = $r['feed_type'];
                $c_process = $r['count_process'];
                $c_skipped = $r['count_skipped'];
                $c_send = $r['count_send'];
                $c_success = $r['count_success'];
                $c_error = $r['count_error'];
                $c_warning = $r['count_warning'];
                $log_status = $r['log_status'];
                $log_message = isset($r['log_key']) && !empty($r['log_key']) ? sprintf(Mirakl_Tools::l($r['log_key']), $r['log_value']) : '';

                $data[$i]['id_log'] = $r['id_log'];
                $data[$i]['batch_id'] = $r['batch_id'];
                $data[$i]['feed_type'] = $feed_type;
                $data[$i]['cron'] = ($r['is_cron'] == 1) ? ' ('.Mirakl_Tools::l('cron').')' : '';
                $data[$i]['date_add_date'] = $r['date_add_date'];
                $data[$i]['date_add_time'] = $r['date_add_time'];


                // color success #00ce88
                // color error #f74f63

                if ($feed_type == MiraklParameter::FEED_TYPE_ACCEPT) {

                    if ($log_status == MiraklParameter::STATUS_ERROR) {
                        $data[$i]['result'] = $log_message;
                        $data[$i]['log_status'] = '<span class="logRow logError"><i class="fa fa-warning red"></i> Error</span>';
                    } else {
                        $log_message.= ' : Process '.$c_process.' Skipped '.$c_skipped.'';
                        $log_message.= 'To accept : Send '.$c_send.' Success '.$c_success.' Error '.$c_error;
                        $data[$i]['result'] = $log_message;
                        $data[$i]['log_status'] = '<span class="logRow logSuccess"><i class="fa fa-check green"></i> Success</span>';
                    }
                } else if ($feed_type == MiraklParameter::FEED_TYPE_OFFER) {
                    $data[$i]['result'] = $log_message;
                    $data[$i]['log_status'] = "<p class='logRow logSuccess'><i class='fa fa-check green'></i> Success ($c_success)</p> <p class='logRow logError'><i class='fa fa-warning'></i> Error ($c_skipped)</p> ";
                } else if ($feed_type == MiraklParameter::FEED_TYPE_PRODUCT) {
                    $data[$i]['result'] = $log_message;
                    $data[$i]['log_status'] = "<p class='logRow logSuccess'><i class='fa fa-check green'></i> Success ($c_success)</p> <p class='logRow logError'><i class='fa fa-warning'></i> Error ($c_skipped)</p> ";
                } else if ($feed_type == MiraklParameter::FEED_TYPE_DELETE) {
                    $data[$i]['result'] = $log_message;
                    $data[$i]['log_status'] = "<p class='logRow logSuccess'><i class='fa fa-check green'></i> Success ($c_success)</p> <p class='logRow logError'><i class='fa fa-warning'></i> Error ($c_skipped)</p> ";
                } else if ($feed_type == MiraklParameter::FEED_TYPE_UPDATE) {
                    $data[$i]['result'] = $log_message;
                    $data[$i]['log_status'] = "<p class='logRow logSuccess'><i class='fa fa-check green'></i> Success ($c_success)</p> <p class='logRow logError'><i class='fa fa-warning'></i> Error ($c_skipped)</p> ";
                }else if ($feed_type == MiraklParameter::FEED_TYPE_IMPORT) {
                    $data[$i]['result'] = $log_message;
                    $data[$i]['log_status'] = "<p class='logRow logSuccess'><i class='fa fa-check green'></i> Success ($c_success)</p> <p class='logRow logError'><i class='fa fa-warning'></i> Error ($c_skipped)</p> ";
                }
            }
        }

//         echo '<pre>';
//                print_r($data);
//                exit;
//        $logs = array();
//        if (isset($results) && !empty($results)) {
//            foreach ($results as $index => $log) {
//                $logs[$index]['message'] = sprintf($this->lang->line($log['log_key']), $log['log_value']);
//                //$logs[$index]['message'] = sprintf(Mirakl_Tools::l($log['log_key']), $log['log_value']);
//            }
//        }


        return $data;
    }

    public function getMiraklLogReport($sub_marketplace, $id_country, $id_shop, $batch_id = null, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false) {
        $data = array();

        $sql = "SELECT R.batch_id, R.feed_type, R.detail, R.date_add "
                ."FROM ::PREFIX::log_report R "
                ."WHERE R.sub_marketplace = $sub_marketplace AND R.id_country = $id_country AND R.id_shop = $id_shop ";

        if (isset($batch_id) && !empty($batch_id)) {
            $sql.= "AND R.batch_id = '$batch_id' ";
        }

        if (isset($search['all']) && !empty($search['all'])) {
            $search = $search['all'];
            $sql.= "AND R.batch_id LIKE %'$search'% ";
        }

        if (isset($order_by) && !empty($order_by)) {
            $sql.= "ORDER BY R.$order_by ";
        } else {
            $sql.= "ORDER BY R.date_add DESC ";
        }

        if (isset($start) && isset($limit)) {
            $sql.= "LIMIT $start , $limit ";
        }

        if ($num_rows) {
            $num_row = $this->sql_num_rows($sql, $this->prefix_mirakl);
            return $num_row;
        }

        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (!empty($results)) {
            foreach ($results as $key => $result) {
                $data[$key]['batch_id'] = $result['batch_id'];
                $data[$key]['feed_type'] = ucfirst(strtolower($result['feed_type']));

                $detail = unserialize($result['detail']);

                $data[$key]['detail']['ReportImportId'] = $detail['ReportImportId'];
                $data[$key]['detail']['ReportProcess'] = $detail['ReportProcess'];
                $data[$key]['detail']['ReportSuccess'] = $detail['ReportSuccess'];
                $data[$key]['detail']['ReportWarning'] = $detail['ReportWarning'];
                $data[$key]['detail']['ReportError'] = $detail['ReportError'];
                $data[$key]['detail']['ReportMode'] = ($detail['ReportMode'] != '-') ? ucfirst(strtolower($detail['ReportMode'])) : '-';
                $data[$key]['detail']['ReportStatus'] = $detail['ReportStatus'];
                $data[$key]['detail']['ReportDateCreate'] = date("Y-m-d H:i:s", strtotime($detail['ReportDateCreate']));

                $count_insert = "<span style='color:Green;'>(".$detail['ReportOfferInsert'].")</span>";
                $count_update = "<span style='color:Blue;'>(".$detail['ReportOfferUpdate'].")</span>";
                $count_delete = "<span style='color:red;'>(".$detail['ReportOfferDelete'].")</span>";

                if ($result['feed_type'] == MiraklParameter::FEED_TYPE_OFFER) {
                    $data[$key]['detail']['ReportMessage'] = "Insert $count_insert Update $count_update Delete $count_delete ";
                } else {
                    $data[$key]['detail']['ReportMessage'] = "-";
                }
            }
        }

        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getMiraklLogReportDetail($sub_marketplace, $id_country, $id_shop, $batch_id, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false) {

        $sql = "SELECT R.batch_id, R.feed_type, RD.id_log_report, RD.reference, RD.log_error, RD.log_warning, RD.date_add "
                ."FROM ::PREFIX::log_report R "
                ."INNER JOIN ::PREFIX::log_report_detail RD ON R.id_log_report = RD.id_log_report "
                ."WHERE R.sub_marketplace = $sub_marketplace AND R.id_country = $id_country AND R.id_shop = $id_shop ";

        if (isset($batch_id) && !empty($batch_id)) {
            $sql.= "AND batch_id = '$batch_id' ";
        }

        if (isset($order_by) && !empty($order_by)) {
            $sql.= "ORDER BY RD.$order_by ";
        }

        if (isset($start) && isset($limit)) {
            $sql.= "LIMIT $start , $limit ";
        }

        if ($num_rows) {
            $num_row = $this->sql_num_rows($sql, $this->prefix_mirakl);
            return $num_row;
        }

        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getMiraklLogDetail($sub_marketplace, $id_country, $id_shop, $batch_id = null, $start = null, $limit = null, $num_rows = false, $ignore_case = false, $search = '') {

        if ($num_rows) {
            $select = " 1 ";
        } else {
            $select = "L.batch_id, L.feed_type, LD.id_log, LD.reference_type, LD.reference, LD.log_key, LD.log_value, LD.date_add ";
        }

        $sql = "SELECT $select "
                ."FROM ::PREFIX::log L "
                ."INNER JOIN ::PREFIX::log_detail LD ON L.id_log = LD.id_log "
                ."WHERE L.sub_marketplace = $sub_marketplace AND L.id_country = $id_country AND L.id_shop = $id_shop ";

        //if (isset($batch_id) && !empty($batch_id)) {
        $sql.= "AND L.batch_id = '$batch_id' ";
        //}

        if (!empty($search)) {
            $sql.= "AND ( L.batch_id LIKE '%$search%' OR LD.reference_type LIKE '%$search%' OR LD.reference LIKE '%$search%' OR CONCAT(LD.log_key, LD.log_value) LIKE '%$search%' ) ";
        }

        if ($ignore_case) {
            $sql.= "AND LD.message NOT LIKE '%*The product does not exis%' ";
        }

        if (isset($start) && isset($limit)) {
            $sql.= "LIMIT $start , $limit ";
        }

        if ($num_rows) {
            $num_row = $this->sql_num_rows($sql, $this->prefix_mirakl);
            return $num_row;
        }
        //print_r($sql); exit;
        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function updateMiraklLog($log_data, $log_detail = array()) {

        $db_insert = '';
        $id_log = '';
        $sql = '';
        $sql_detail = '';
        $db_value = array();
        $table_log = $this->prefix_mirakl.'log';
        $table_log_detail = $this->prefix_mirakl.'log_detail';

        $sql = $this->insert_string($table_log, $log_data);
        $id_log = $this->sql_query_effect($sql, true);

        // This ins log detail insert 2 set cause many insert

        if (!empty($id_log) && !empty($log_detail)) {
            $db_insert = "INSERT INTO $table_log_detail (id_log, reference_type, reference, log_key, log_value, date_add) VALUES ";
            foreach ($log_detail as $value) {

                $log_value = str_replace("'", " ", $value['3']);
                $date = date("Y-m-d H:i:s");
                $db_value[] = " ( '{$id_log}', '{$value['0']}', '{$value['1']}', '{$value['2']}', '$log_value', '{$date}' ) ";

                if (sizeof($db_value) >= 2500) {
                    $sql_detail = $db_insert.implode(",", $db_value).'; ';
                    $this->sql_query_exec($sql_detail); // insert set 1
                    $db_value = array();
                }
            } // end loop

            if (!empty($db_value)) {
                $sql_detail = $db_insert.implode(",", $db_value).'; ';
            }
        }

        if (!empty($sql_detail)) {
            $this->sql_query_exec($sql_detail); // insert set 2
        }

        return $id_log;
    }

    public function saveMiraklLogReport($report_data, $report_detail = array()) {
        $id_log_report = '';
        $sql = '';
        $sql_detail = '';
        $table = $this->prefix_mirakl.'log_report';
        $table_detail = $this->prefix_mirakl.'log_report_detail';
        $sql = $this->insert_string($table, $report_data);
        $id_log_report = $this->sql_query_effect($sql, true);

        if (!empty($id_log_report) && !empty($report_detail)) {
            foreach ($report_detail as $value) {
                $detail = array();
                $detail['id_log_report'] = $id_log_report;
                $detail['reference'] = $value['rererence'];
                $detail['log_error'] = $value['error'];
                $detail['log_warning'] = $value['warning'];
                $detail['date_add'] = date("Y-m-d H:i:s");
                $sql_detail.= $this->insert_string($table_detail, $detail);
            }
        }

        if (!empty($sql_detail)) {
            $this->sql_query_exec($sql_detail);
        }

        return $id_log_report;
    }

    public function getSettingCurrencyIdByIsoCode($iso_code, $id_shop) {

        if (!isset($iso_code) || !isset($id_shop)) {
            return false;
        }

        $iso_code = strtoupper($iso_code);

        $sql = "SELECT id_currency "
                ."FROM ::PREFIX::currency "
                ."WHERE iso_code = '$iso_code' AND id_shop = $id_shop ";

        $result = $this->sql_query($sql, $this->prefix_product, true, 'id_currency');
        return $result;
    }

    public function getMiraklProduct($sub_marketplace, $id_country, $id_shop) {
        $sql = "SELECT * "
                ."FROM ::PREFIX::products "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";

        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getSettingCondition($id_shop) {
        $data = array();
        $sql = "SELECT id_condition, name "
                ."FROM ::PREFIX::conditions "
                ."WHERE id_shop = $id_shop ";

        $results = $this->sql_query($sql, $this->prefix_product);

        if (!empty($results)) {
            foreach ($results as $result) {
                $data[$result['id_condition']] = $result['name'];
            }
        }

        return $data;
    }

    public function getSettingMappingCondition($id_shop, $id_marketplace) {
        $data = array();
        $sql = "SELECT name, condition_value "
                ."FROM ::PREFIX::mapping_condition "
                ."WHERE id_shop = $id_shop AND id_marketplace = $id_marketplace ";

        $results = $this->sql_query($sql, $this->prefix_product);

        if (!empty($results)) {
            foreach ($results as $result) {
                $data[$result['name']] = $result['condition_value'];
            }
        }

        return $data;
    }

    public function getMiraklMappingValues($sub_marketplace, $id_country, $id_shop, $mirakl_value_list = array()) {
        $data = array();
        $sql = "SELECT 	M.id_mapping_value, M.id_model, M.hierarchy_code, D.id_mapping_value_detail, D.id_attribute_group, D.id_value_list, D.id_attribute, D.field_marketplace "
                ."FROM ::PREFIX::mapping_values M "
                ."INNER JOIN ::PREFIX::mapping_values_detail D ON M.id_mapping_value = D.id_mapping_value "
                ."WHERE M.sub_marketplace = $sub_marketplace AND M.id_country = $id_country AND M.id_shop = $id_shop AND  D.id_attribute != '' "
                ."ORDER BY M.id_model, D.id_mapping_value_detail ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $key => $result) {
                $id_model = $result['id_model'];
                $id_attribute_group = $result['id_attribute_group'];
                $id_attribute = $result['id_attribute'];
                $id_value_list = $result['id_value_list'];
                $field_marketplace = $result['field_marketplace'];

                $data[$id_model][$id_attribute_group]['field_marketplace'] = $field_marketplace;
                $data[$id_model][$id_attribute_group]['hierarchy_code'] = $result['hierarchy_code'];
                $data[$id_model][$id_attribute_group]['attribute'][$id_attribute]['id_attribute'] = $id_attribute;
                $data[$id_model][$id_attribute_group]['attribute'][$id_attribute]['id_value_list'] = $id_value_list;

                $data[$id_model][$id_attribute_group]['attribute'][$id_attribute]['list_code'] = isset($mirakl_value_list[$id_value_list]['list_code']) ? $mirakl_value_list[$id_value_list]['list_code'] : '';
                $data[$id_model][$id_attribute_group]['attribute'][$id_attribute]['list_label'] = isset($mirakl_value_list[$id_value_list]['list_label']) ? $mirakl_value_list[$id_value_list]['list_label'] : '';
            }
        }
        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function log_message($data = '', $file_name) {
        $file_dir = USERDATA_PATH.$this->user.'/mirakl/logs/';
        $file = $file_dir.$file_name.'.txt';

        if (!is_dir($file_dir)) {
            return 0;
        }

        $get_data = date("Y-m-d H:i:s")." --> ";

        if (is_array($data)) {
            $get_data.= serialize($data);
        } else {
            $get_data = $data;
        }

        $get_data.= "\r\n";

        file_put_contents($file, $get_data, FILE_APPEND);

        if (file_exists($file)) {
            $perms = (int) substr(sprintf('%o', fileperms($file)), -4);
            if ($perms != 777 && $perms != 666) {
                chmod($file, 0777);
            }
        }
    }

    public function getMiraklAdditionalField($sub_marketplace, $enable = 1) {
        $sql = "SELECT * "
                ."FROM ::PREFIX::config_additional_field "
                ."WHERE sub_marketplace = $sub_marketplace AND enable = $enable "
                ."ORDER BY id_additional_field ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getMiraklMappingModelKeyId($sub_marketplace, $id_country, $id_shop) {

        $data = array();
        $sql = "SELECT id_model, model_name, hierarchy_code FROM ::PREFIX::mapping_model WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ORDER BY id_model ";
        $models = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($models) && !empty($models)) {
            foreach ($models as $model) {
                $id_model = $model['id_model'];

                $data[$id_model]['model_name'] = $model['model_name'];

                /* hierarchy */
                $hierarchy_code = unserialize($model['hierarchy_code']);
                $hierarchy_code = implode(",", $hierarchy_code);
                $sql_hierarhy = "SELECT code, label, level, parent_code FROM ::PREFIX::config_hierarchy WHERE sub_marketplace = $sub_marketplace AND id_config_hierarchy IN ($hierarchy_code) ";
                $hierarchy = $this->sql_query($sql_hierarhy, $this->prefix_mirakl);
                $data[$id_model]['hierarchy'] = $hierarchy;

                $sql_detail = "SELECT MA.id_model_attribute, MA.id_config_attribute, MA.shop_attribute, MA.default_value, "
                        ."MV.id_model_value, MV.id_config_value_list, MV.shop_attribute_value, "
                        ."CA.code, CA.label, CA.type, CA.hierarchy_code, CA.required, CA.type_parameter, CA.validations,"
                        ."CV.id_config_value, CV.code 'code_list', CV.label 'label_list' "
                        ."FROM ::PREFIX::mapping_model_attribute MA "
                        ."LEFT JOIN ::PREFIX::mapping_model_value MV ON MA.id_model_attribute = MV.id_model_attribute "
                        ."LEFT JOIN ::PREFIX::config_attribute CA ON MA.id_config_attribute = CA.id_config_attribute "
                        //."LEFT JOIN ::PREFIX::config_value_list CV ON MV.id_config_value_list = CV.id_config_value_list "
                        ."LEFT JOIN ::PREFIX::config_value_list CV ON MA.shop_attribute = CV.id_config_value_list "
                        ."WHERE MA.id_model = $id_model "
                        ."ORDER BY MA.id_model_attribute, MV.id_model_value ";

                $model_detail = $this->sql_query($sql_detail, $this->prefix_mirakl);

                //echo '<pre>'; print_r($model_detail); exit;

                if (isset($model_detail) && !empty($model_detail)) {
                    foreach ($model_detail as $detail) {
                        $id_model_attribute = $detail['id_model_attribute'];
                        $id_model_value = $detail['id_model_value'];

                        $data[$id_model]['mapping'][$id_model_attribute]['id_config_attribute'] = $detail['id_config_attribute'];
                        $data[$id_model]['mapping'][$id_model_attribute]['shop_attribute'] = $detail['shop_attribute'];
                        $data[$id_model]['mapping'][$id_model_attribute]['default_value'] = $detail['default_value'];
                        $data[$id_model]['mapping'][$id_model_attribute]['code'] = $detail['code'];
                        $data[$id_model]['mapping'][$id_model_attribute]['label'] = $detail['label'];
                        $data[$id_model]['mapping'][$id_model_attribute]['type'] = $detail['type'];
                        $data[$id_model]['mapping'][$id_model_attribute]['hierarchy_code'] = $detail['hierarchy_code'];
                        $data[$id_model]['mapping'][$id_model_attribute]['required'] = $detail['required'];
                        $data[$id_model]['mapping'][$id_model_attribute]['type_parameter'] = $detail['type_parameter'];
                        $data[$id_model]['mapping'][$id_model_attribute]['validations'] = $detail['validations'];

                        if (empty($id_model_value)) {
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'] = array();
                        } else {
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'][$id_model_value]['id_config_value_list'] = $detail['id_config_value_list'];
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'][$id_model_value]['shop_attribute_value'] = $detail['shop_attribute_value'];
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'][$id_model_value]['id_config_value'] = $detail['id_config_value'];
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'][$id_model_value]['code_list'] = $detail['code_list'];
                            $data[$id_model]['mapping'][$id_model_attribute]['value_list'][$id_model_value]['label_list'] = $detail['label_list'];
                        }

                        if (!empty($detail['code_list'])) {
                            $data[$id_model]['mapping'][$id_model_attribute]['code_list'] = $detail['code_list'];
                        }
                    }
                }
            }
        }
        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getMiraklMappingAttributeList($sub_marketplace, $id_country, $id_shop) {
        $data = array();

        $sql = "SELECT AL.id_attribute_list, AL.id_config_attribute, AL.shop_attribute_value, "
                ."VL.id_config_value_list, VL.code, VL.label "
                ."FROM ::PREFIX::mapping_attribute_list AL "
                ."INNER JOIN ::PREFIX::config_value_list VL ON AL.id_config_value_list = VL.id_config_value_list "
                ."WHERE AL.sub_marketplace = $sub_marketplace AND AL.id_country = $id_country AND AL.id_shop = $id_shop "
                ."ORDER BY id_attribute_list ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $shop_attribute_value = $result['shop_attribute_value'];
                $data[$shop_attribute_value] = $result;
            }
        }

        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getValueByFieldShop($id_shop, $id_lang, $value, $field_shop = array(), $attribute_list = false, $sub_marketplace = '', $id_country = '') {
        // getAttributeFieldShop

        $sql = '';
        $field_value = '';


        $get_field = isset($value['field_shop']) && !empty($value['field_shop']) ? $value['field_shop'] : $field_shop;



        $data_field = explode('-', $get_field);
        $field_type = isset($data_field[0]) ? $data_field[0] : '';
        $field_shop = isset($data_field[1]) ? $data_field[1] : '';
        $is_color_group = isset($data_field[2]) ? $data_field[2] : '';

        /* attribute */
        if ($field_type == MiraklParameter::FIELD_ATTRIBUTE) {

            if ($attribute_list) {
                $sql = "SELECT A.id_attribute field_shop_code, A.name field_shop_label, G.name 'field_shop_name', AL.id_config_value_list "
                        ."FROM {$this->prefix_product}attribute_group_lang G "
                        ."INNER JOIN {$this->prefix_product}attribute_lang A ON G.id_attribute_group = A.id_attribute_group "
                        ."LEFT JOIN {$this->prefix_mirakl}mapping_attribute_list AL ON AL.sub_marketplace = $sub_marketplace AND AL.id_country = $id_country AND A.id_attribute = SUBSTRING_INDEX(AL.shop_attribute_value, '-', -1) AND SUBSTRING_INDEX(AL.shop_attribute_value, '-', 1) = '$field_type' "
                        ."WHERE G.id_shop = $id_shop AND G.id_lang = $id_lang AND G.id_attribute_group = $field_shop ";
            } else {
                $sql = "SELECT id_attribute field_shop_code, name field_shop_label FROM {$this->prefix_product}attribute_lang WHERE id_shop = $id_shop AND id_lang = $id_lang AND id_attribute_group = $field_shop ";
            }
        } else if ($field_type == MiraklParameter::FIELD_FEATURE) {
            /* feature */

            if ($attribute_list) {
                $sql = "SELECT FV.id_feature_value field_shop_code, FV.value field_shop_label, F.name 'field_shop_name', AL.id_config_value_list "
                        ."FROM {$this->prefix_product}feature F "
                        ."INNER JOIN {$this->prefix_product}feature_value FV ON F.id_feature = FV.id_feature "
                        ."LEFT JOIN {$this->prefix_mirakl}mapping_attribute_list AL ON AL.sub_marketplace = $sub_marketplace AND AL.id_country = $id_country AND FV.id_feature_value = SUBSTRING_INDEX(AL.shop_attribute_value, '-', -1) AND SUBSTRING_INDEX(AL.shop_attribute_value, '-', 1) = '$field_type' "
                        ."WHERE F.id_shop = $id_shop AND F.id_lang = $id_lang AND F.id_feature = $field_shop ";
            } else {
                $sql = "SELECT id_feature_value field_shop_code, value field_shop_label FROM {$this->prefix_product}feature_value WHERE id_shop = $id_shop AND id_lang = $id_lang AND id_feature = $field_shop ";
            }
        } else if ($field_type == MiraklParameter::FIELD_EXTRA) {
            //$sql = "SELECT 'E-default' field_shop_code, 'Default' field_shop_label ";
        } else {
            /* field shop */
            if ($field_shop == 'category') {
                //$sql = "SELECT id_category field_shop_code, name field_shop_label FROM {$this->prefix_product}category_lang WHERE id_shop = $id_shop AND id_lang = $id_lang ";
            } else if ($field_shop == 'title') {
                //$sql = "SELECT id_product field_shop_code, name field_shop_label FROM {$this->prefix_product}product_lang WHERE id_shop = $id_shop AND id_lang = $id_lang ";
            } else if ($field_shop == 'description') {
                //$sql = "SELECT id_product field_shop_code, description field_shop_label FROM {$this->prefix_product}product_lang WHERE id_shop = $id_shop AND id_lang = $id_lang ";
            } else if ($field_shop == 'reference') {
//                $sql = "SELECT id_product, reference, ean, upc "
//                        ."FROM {$this->prefix_product}product P "
//                        ."INNER JOIN {$this->prefix_product}product_attribute PA ON P.id_product = PA.id_product "
//                        ." WHERE P.id_shop = $id_shop ";
            } else if ($field_shop == 'manufacturer') {
                $sql = "SELECT name field_shop_code, name field_shop_label FROM {$this->prefix_product}manufacturer WHERE id_shop = $id_shop ";
            } else if ($field_shop == 'supplier') {
                $sql = "SELECT name field_shop_code, name field_shop_label FROM {$this->prefix_product}supplier WHERE id_shop = $id_shop ";
            } else if ($field_shop == 'weight') {
                $sql = "SELECT weight field_shop_code, weight field_shop_label FROM {$this->prefix_product}product P WHERE P.id_shop = $id_shop AND IFNULL(P.weight,'') != '' "
                        ."UNION "
                        ."SELECT weight field_shop_code, weight field_shop_label FROM {$this->prefix_product}product_attribute PA WHERE PA.id_shop = $id_shop AND IFNULL(PA.weight,'') != '' ";
            } else if ($field_shop == 'vat') {
                $sql = "SELECT T.rate field_shop_code, TL.name field_shop_label "
                        ."FROM {$this->prefix_product}tax T "
                        ."INNER JOIN {$this->prefix_product}tax_lang TL ON T.id_tax = TL.id_tax AND TL.id_lang = $id_lang "
                        ."WHERE T.id_shop = $id_shop ";
            }
        }

        $results = $this->sql_query($sql);
        $name = isset($results[0]['field_shop_name']) ? $results[0]['field_shop_name'] : '';
        return array('results' => $results, 'is_color_group' => $is_color_group, 'query' => $sql, 'field_shop_name' => $name);
    }

    public function saveMiraklProduct($sub_marketplace, $id_country, $id_shop, $products) {

        $table = $this->prefix_mirakl.'products';
        $date = date('Y-m-d H:i:s');

        $data_ins = array();
        $data_ins['sub_marketplace'] = $sub_marketplace;
        $data_ins['id_country'] = $id_country;
        $data_ins['id_shop'] = $id_shop;
        $data_ins['product_id'] = $products['product_id'];
        $data_ins['product_id_type'] = $products['product_id_type'];
        $data_ins['date_add'] = $date;

        $sql_query = "SELECT 1 FROM $table "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_Shop = $id_shop "
                ."AND product_id = '{$products['product_id']}' AND product_id_type = '{$products['product_id_type']}' ";
        $num_row = $this->sql_num_rows($sql_query);

        if ($num_row == 0) {
            $sql_insert = $this->insert_string($table, $data_ins);
            $this->sql_query_effect($sql_insert);
        }

        $data = array();
        $data['category_code'] = $products['category_code'];
        $data['category_label'] = $products['category_label'];
        $data['product_sku'] = $products['product_sku'];
        $data['product_title'] = $products['product_title'];
        $data['date_upd'] = $date;

        $where = array(
            'sub_marketplace' => array('operation' => '=', 'value' => $sub_marketplace),
            'id_country' => array('operation' => '=', 'value' => $id_country),
            'id_Shop' => array('operation' => '=', 'value' => $id_shop),
            'product_id' => array('operation' => '=', 'value' => $products['product_id']),
            'product_id_type' => array('operation' => '=', 'value' => $products['product_id_type']),
        );

        $sql_update = $this->update_string($table, $data, $where, false);
        $this->sql_query_effect($sql_update);
    }

    public function saveModel($value) {

        $flag_error = 'N';
        $sql_model = '';
        $sql_model_attribute = '';
        $sql_model_value = '';
        $sql_delete = '';

        $table_model = $this->prefix_mirakl.'mapping_model';
        $table_model_attribute = $this->prefix_mirakl.'mapping_model_attribute';
        $table_model_value = $this->prefix_mirakl.'mapping_model_value';

        $sub_marketplace = $value['sub_marketplace'];
        $id_country = $value['id_country'];
        $id_shop = $value['id_shop'];

        $id_model_value_array = array();
        $id_model_attribute_array = array();

        if (isset($value['model']) && !empty($value['model'])) {
            foreach ($value['model'] as $key_name => $model) {

                $set_hierarchy_code = array(); // for clear data in hierarchy_code

                if (!isset($model['model_name']) || empty($model['model_name'])) {
                    $flag_error = 'Y';
                    continue;
                }

                if (!isset($model['hierarchy_code'][0]) || empty($model['hierarchy_code'][0])) {
                    $flag_error = 'Y';
                    continue;
                }

                $id_model = isset($model['id_model']) && !empty($model['id_model']) ? $model['id_model'] : '';
                $model_name = isset($model['model_name']) && !empty($model['model_name']) ? $model['model_name'] : $key_name;

                /////////////////////////////////////////////////////////////////////////////////
                // In hierarchy code, if last level not choose hierarchy.
                if (isset($model['hierarchy_code'][1])) {
                    foreach ($model['hierarchy_code'] as $level => $hierarchy) {
                        if (!empty($hierarchy)) {
                            $set_hierarchy_code[$level] = $hierarchy;
                        }
                    }
                } else {
                    $set_hierarchy_code[0] = $model['hierarchy_code'][0];
                }
                ///////////////////////////////////////////////////////////////////////////////////

                /* model master */
                if (isset($id_model) && !empty($id_model)) {

                    $where = array(
                        "id_model" => array('operation' => "=", 'value' => $id_model)
                    );

                    $data = array();
                    $data['model_name'] = $model_name;
                    $data['hierarchy_code'] = serialize($set_hierarchy_code);
                    $data['date_upd'] = date('Y-m-d H:i:s');

                    $sql_model = $this->update_string($table_model, $data, $where, false);
                    $this->sql_query_effect($sql_model);
                } else {
                    // ins model
                    $data = array();
                    $data['sub_marketplace'] = $sub_marketplace;
                    $data['id_country'] = $id_country;
                    $data['id_shop'] = $id_shop;
                    $data['model_name'] = $model_name;
                    $data['hierarchy_code'] = serialize($set_hierarchy_code);
                    $data['date_add'] = date('Y-m-d H:i:s');
                    $data['date_upd'] = date('Y-m-d H:i:s');
                    $sql_model = $this->insert_string($table_model, $data);
                    $id_model = $this->sql_query_effect($sql_model, true);
                }

                /* model attribute */
                if (!empty($id_model) && isset($model['attribute_with_hierarchy']) && !empty($model['attribute_with_hierarchy'])) {
                    foreach ($model['attribute_with_hierarchy'] as $id_config_attribute => $attribute_data) {

                        $data = array();
                        $data['id_model'] = $id_model;
                        $data['id_config_attribute'] = $id_config_attribute;
                        $data['shop_attribute'] = $attribute_data['shop_attribute'];
                        $data['default_value'] = isset($attribute_data['default_value']) ? $attribute_data['default_value'] : '';

                        $sql_model_attribute = "SELECT id_model_attribute FROM $table_model_attribute WHERE id_model = $id_model AND id_config_attribute = $id_config_attribute ";
                        $id_model_attribute = $this->sql_query($sql_model_attribute, '', true, 'id_model_attribute');

                        if (isset($id_model_attribute) && !empty($id_model_attribute)) {

                            $where = array(
                                "id_model_attribute" => array('operation' => "=", 'value' => $id_model_attribute)
                            );

                            $sql_model_attribute = $this->update_string($table_model_attribute, $data, $where, false);
                            $this->sql_query_effect($sql_model_attribute);
                        } else {
                            // ins model attribute
                            $sql_model_attribute = $this->insert_string($table_model_attribute, $data);
                            $id_model_attribute = $this->sql_query_effect($sql_model_attribute, true);
                        }

                        /* model value */
                        if (!empty($id_model_attribute) && isset($attribute_data['attribute_value']) && !empty($attribute_data['attribute_value'])) {
                            foreach ($attribute_data['attribute_value'] as $id_config_value_list => $shop_attribute_value) {

                                $data = array();
                                $data['id_model_attribute'] = $id_model_attribute;
                                $data['id_config_value_list'] = $id_config_value_list;
                                $data['shop_attribute_value'] = $shop_attribute_value;

                                $sql_model_value = "SELECT id_model_value FROM $table_model_value WHERE id_model_attribute = $id_model_attribute AND id_config_value_list = $id_config_value_list ";
                                $id_model_value = $this->sql_query($sql_model_value, '', true, 'id_model_value');

                                if (isset($id_model_value) && !empty($id_model_value)) {

                                    $where = array(
                                        "id_model_value" => array('operation' => "=", 'value' => $id_model_value)
                                    );

                                    $sql_model_value = $this->update_string($table_model_value, $data, $where, false);
                                    $this->sql_query_effect($sql_model_value);
                                } else {
                                    // ins model value
                                    $sql_model_value = $this->insert_string($table_model_value, $data);
                                    $id_model_value = $this->sql_query_effect($sql_model_value, true);
                                }
                                $id_model_value_array[] = $id_model_value; // for delete not in
                            }
                        }
                        $id_model_attribute_array[] = $id_model_attribute; // for delete not in
                    }

                    if (!empty($id_model_attribute_array)) {
                        $id_model_attribute_array = implode(", ", $id_model_attribute_array);

                        // delete model attribute
                        $sql_delete = "DELETE FROM $table_model_attribute WHERE id_model = $id_model AND id_model_attribute NOT IN ($id_model_attribute_array) ; ";
                        $this->sql_query_effect($sql_delete);

                        // delete model value
                        if (!empty($id_model_value_array)) {
                            $id_model_value_array = implode(", ", $id_model_value_array);

                            $sql_delete = "DELETE FROM $table_model_value WHERE id_model_attribute IN ($id_model_attribute_array) AND id_model_value NOT IN ($id_model_value_array) ; ";
                            $this->sql_query_effect($sql_delete);
                        }

                        // clear data
                        $id_model_attribute_array = array();
                        $id_model_value_array = array();
                    }
                }
            }
        } else {
            $flag_error = 'Y'; // model data in model
        }

        return $flag_error == 'N' ? true : false;
    }

    public function getMiraklMappingField($sub_marketplace, $id_shop) {
        $results = array();
        $sql = "SELECT F.id_field, F.id_config_attribute, F.shop_attribute, "
                ."A.code, A.label, A.type, A.required, A.validations "
                ."FROM ::PREFIX::mapping_field F "
                ."LEFT JOIN ::PREFIX::config_attribute A ON F.id_config_attribute = A.id_config_attribute AND A.sub_marketplace = $sub_marketplace "
                ."WHERE F.sub_marketplace = $sub_marketplace AND F.id_shop = $id_shop "
                ."ORDER BY F.id_field ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getMiraklMappingFieldList($sub_marketplace, $id_shop) {
        $data = array();
        $sql = "SELECT FA.id_field_attribute, FA.id_config_attribute, FA.shop_attribute, "
                ."FV.id_field_value, CV.id_config_value_list, FV.shop_attribute_value, "
                ."CA.code, CA.label, CA.type, CA.hierarchy_code, CA.required, CA.type_parameter, CA.validations, "
                ."CV.id_config_value, CV.code 'code_list', CV.label 'label_list' "
                ."FROM ::PREFIX::mapping_field_attribute FA "
                ."LEFT JOIN ::PREFIX::mapping_field_value FV ON FA.id_field_attribute = FV.id_field_attribute "
                ."LEFT JOIN ::PREFIX::config_attribute CA ON FA.id_config_attribute = CA.id_config_attribute "
                ."LEFT JOIN ::PREFIX::config_value_list CV ON CV.id_config_value_list = FA.shop_attribute "
                ."WHERE FA.sub_marketplace = $sub_marketplace AND FA.id_shop = $id_shop "
                ."ORDER BY FA.id_field_attribute, CV.id_config_value_list ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $id_field_attribute = $result['id_field_attribute'];
                $id_field_value = $result['id_field_value'];

                $data[$id_field_attribute]['id_config_attribute'] = $result['id_config_attribute'];
                $data[$id_field_attribute]['shop_attribute'] = $result['shop_attribute'];
                $data[$id_field_attribute]['code'] = $result['code'];
                $data[$id_field_attribute]['label'] = $result['label'];
                $data[$id_field_attribute]['type'] = $result['type'];
                $data[$id_field_attribute]['required'] = $result['required'];
                $data[$id_field_attribute]['type_parameter'] = $result['type_parameter'];
                $data[$id_field_attribute]['validations'] = $result['validations'];

                if (!empty($id_field_value)) {
                    $data[$id_field_attribute]['value_list'][$id_field_value]['id_config_value_list'] = $result['id_config_value_list'];
                    $data[$id_field_attribute]['value_list'][$id_field_value]['shop_attribute_value'] = $result['shop_attribute_value'];
                    $data[$id_field_attribute]['value_list'][$id_field_value]['id_config_value'] = $result['id_config_value'];
                    $data[$id_field_attribute]['value_list'][$id_field_value]['code_list'] = $result['code_list'];
                    $data[$id_field_attribute]['value_list'][$id_field_value]['label_list'] = $result['label_list'];
                }

                if ($result['shop_attribute'] == $result['id_config_value_list']) {
                    $data[$id_field_attribute]['code_list'] = $result['code_list'];
                }
            }
        }
        return $data;
    }

    public function getMiraklMappingAttributeWithList($sub_marketplace, $id_shop) {
        $data = array();
        $field_type = MiraklParameter::FIELD_TYPE_AL;
        $sql = "SELECT A.id_mapping_attribute, A.field_marketplace, A.field_shop, AD.id_mapping_attribute_detail, AD.id_value_list, AD.id_shop_value "
                ."FROM ::PREFIX::mapping_attribute A "
                ."INNER JOIN ::PREFIX::mapping_attribute_detail AD ON A.id_mapping_attribute = AD.id_mapping_attribute "
                ."WHERE A.field_type = '$field_type' ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $id_mapping_attribute = $result['id_mapping_attribute'];
                $id_mapping_attribute_detail = $result['id_mapping_attribute_detail'];

                $data[$id_mapping_attribute]['field_marketplace'] = $result['field_marketplace'];
                $data[$id_mapping_attribute]['field_shop'] = $result['field_shop'];

                $data[$id_mapping_attribute]['values_list_detail'][$id_mapping_attribute_detail]['id_value_list'] = $result['id_value_list'];
                $data[$id_mapping_attribute]['values_list_detail'][$id_mapping_attribute_detail]['id_shop_value'] = $result['id_shop_value'];
            }
        }
        return $data;
    }

    public function getMiraklMappingModelMarketplaceCode($sub_marketplace, $id_country, $id_shop) {
        exit;
        $hierarchy_code = array();
        $field_marketplace = array();

        $sql = "SELECT M.id_model, M.hierarchy_code, D.id_model_detail, D.field_marketplace "
                ."FROM ::PREFIX::mapping_models M "
                ."LEFT JOIN ::PREFIX::mapping_models_detail D ON M.id_model = D.id_model "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND field_shop != '' "
                ."ORDER BY name, hierarchy_code, D.id_model_detail ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {

                /* unique hierarchy_code */
                $hierarchies = unserialize($result['hierarchy_code']);
                if (isset($hierarchies) && !empty($hierarchies)) {
                    foreach ($hierarchies as $hierarchy) {
                        $hierarchy_code[$hierarchy] = $hierarchy;
                    }
                }

                /* unique field_marketplace */
                $field_marketplace[$result['field_marketplace']] = $result['field_marketplace'];
            }
        }

        return array('hierarchy_code' => $hierarchy_code, 'field_marketplace' => $field_marketplace);
    }

    public function getMiraklModelMapping($sub_marketplace, $id_country, $id_shop, $id_lang, $mirakl_hierarchy, $mirakl_attribute, $mirakl_value_list) {
        exit;
        $data = array();
        $data_attribute = array();

        $sql_attribute = "SELECT AG.id_attribute_group, AGL.name 'name_attribute_group', A.id_attribute, AL.name 'name_attribute', AG.is_color_group "
                ."FROM ::PREFIX::attribute_group AG "
                ."INNER JOIN ::PREFIX::attribute_group_lang AGL ON AG.id_attribute_group = AGL.id_attribute_group "
                ."LEFT JOIN ::PREFIX::attribute A ON AG.id_attribute_group = A.id_attribute_group AND AG.id_shop = A.id_shop "
                ."LEFT JOIN ::PREFIX::attribute_lang AL ON A.id_attribute = AL.id_attribute AND AGL.id_lang = AL.id_lang "
                ."WHERE AG.id_shop = $id_shop AND AGL.id_lang = $id_lang ";
        $results_attribute = $this->sql_query($sql_attribute, $this->prefix_product);

        if (isset($results_attribute) && !empty($results_attribute)) {
            foreach ($results_attribute as $attribute) {
                $id_attribute_group = $attribute['id_attribute_group'];
                $id_attribute = $attribute['id_attribute'];

                $data_attribute[$id_attribute_group]['id_attribute_group'] = $id_attribute_group;
                $data_attribute[$id_attribute_group]['name_group'] = $attribute['name_attribute_group'];
                $data_attribute[$id_attribute_group]['is_color_group'] = $attribute['is_color_group'];
                $data_attribute[$id_attribute_group]['attribute'][$id_attribute]['name_attribute'] = $attribute['name_attribute'];
            }
        }

        $sql_mapping_value = "SELECT * "
                ."FROM ::PREFIX::mapping_values V "
                ."INNER JOIN ::PREFIX::mapping_values_detail VD ON V.id_mapping_value = VD.id_mapping_value "
                ."WHERE V.sub_marketplace = $sub_marketplace AND V.id_country = $id_country AND id_shop = $id_shop ";
        $result_mapping_value = $this->sql_query($sql_mapping_value, $this->prefix_mirakl);
        $result_mapping_data = array(); // [id_value_list][id_model][id_attribute_group]

        if (isset($result_mapping_value) && !empty($result_mapping_value)) {
            foreach ($result_mapping_value as $mapping_value) {
                $id_value_list = $mapping_value['id_value_list'];
                $id_model = $mapping_value['id_model'];
                $id_attribute_group = $mapping_value['id_attribute_group'];
                $result_mapping_data[$id_value_list][$id_model][$id_attribute_group] = $mapping_value;
            }
        }

        $sql = "SELECT M.id_model, M.`name`, M.hierarchy_code, D.id_model_detail, D.field_marketplace, D.field_shop, D.type "
                ."FROM ::PREFIX::mapping_models M "
                ."LEFT JOIN ::PREFIX::mapping_models_detail D ON M.id_model = D.id_model "
                ."INNER JOIN ::PREFIX::profile MP ON M.id_model = MP.id_model "
                ."INNER JOIN mirakl_category_selected MCS ON MP.id_profile = MCS.id_profile "
                ."WHERE M.sub_marketplace = $sub_marketplace AND M.id_country = $id_country AND M.id_shop = $id_shop AND field_shop != '' "
                ."ORDER BY M.name, D.id_model_detail ";
        $results_model = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results_model) && !empty($results_model)) {
            foreach ($results_model as $model) {

                $id_model = $model['id_model'];
                $id_model_detail = $model['id_model_detail'];
                //
                $field_split = explode('-', $model['field_shop']);
                $type = $field_split[0];
                $id_attribute_group = $field_split[1];
                //
                $field_marketplace = $model['field_marketplace'];
                $mirakl_attribute_label = isset($mirakl_attribute[$field_marketplace]) ? $mirakl_attribute[$field_marketplace]['label'] : '';
                $value = isset($mirakl_attribute[$field_marketplace]) ? $mirakl_attribute[$field_marketplace]['values_list'] : '';
                //
                $hierarchy_code = unserialize($model['hierarchy_code']);
                //    print_r($hierarchy_code); exit;

                $hierarchy_data = array();
                if (isset($hierarchy_code) && !empty($hierarchy_code)) {
                    foreach ($hierarchy_code as $key => $hierarchy) {
                        $hierarchy_data[$hierarchy] = isset($mirakl_hierarchy[$hierarchy]) ? $mirakl_hierarchy[$hierarchy] : ''; // code, label
                    }
                }

                $data[$id_model]['model_name'] = $model['name'];
                $data[$id_model]['hierarchy_code'] = $hierarchy_data;

                $data[$id_model]['model_detail'][$id_model_detail]['type'] = $model['type'];
                $data[$id_model]['model_detail'][$id_model_detail]['field_marketplace'] = $field_marketplace;
                $data[$id_model]['model_detail'][$id_model_detail]['label'] = $mirakl_attribute_label;
                $data[$id_model]['model_detail'][$id_model_detail]['value'] = $value;
                $data[$id_model]['model_detail'][$id_model_detail]['field_attribute'] = $data_attribute[$id_attribute_group];

                $value_list = array();

                if (isset($mirakl_value_list[$value]['list_detail']) && !empty($mirakl_value_list[$value]['list_detail'])) {
                    foreach ($mirakl_value_list[$value]['list_detail'] as $id_value_list => $data_list) {

                        $value_list[$id_value_list]['list_code'] = $data_list['list_code'];
                        $value_list[$id_value_list]['list_label'] = $data_list['list_label'];
                        $value_list[$id_value_list]['attribute_mapping'] = isset($result_mapping_data[$id_value_list][$id_model][$id_attribute_group]['id_attribute']) ? $result_mapping_data[$id_value_list][$id_model][$id_attribute_group]['id_attribute'] : '';
                    }
                }

                $data[$id_model]['model_detail'][$id_model_detail]['value_list'] = $value_list;
            }
        }

        return $data;
    }

    public function getShopAttribute($id_shop, $id_lang) {

        $field = array();
        $field_shop = array();
        $field_attribute = array();
        $field_feature = array();

        /* field shop */
        $field_shop = MiraklParameter::getAttributeFieldShop();

        /* field extra */
        //$field_extra = MiraklParameter::getAttributeFieldExtra();

        /* field attribute */
        $result_attribute_group = $this->getProductAttribute($id_shop, $id_lang);
        if (isset($result_attribute_group) && !empty($result_attribute_group)) {
            foreach ($result_attribute_group as $id_attribute_group => $attribute_group) {
                $field_attribute[] = array('code' => 'A-'.$id_attribute_group.'-'.$attribute_group['is_color_group'], 'label' => $attribute_group['name']);
            }
        }

        /* field feature */
        $result_feature = $this->getProductFeature($id_shop, $id_lang);
        if (isset($result_feature) && !empty($result_feature)) {
            foreach ($result_feature as $id_feature => $feature) {
                $field_feature[] = array('code' => 'F-'.$id_feature, 'label' => $feature['name']);
            }
        }

        /* field merge */
        $field['S'] = $field_shop;
        //$field['E'] = $field_extra;
        $field['A'] = $field_attribute;
        $field['F'] = $field_feature;

        //echo '<pre>'; print_r($field); exit; 
        return $field;
    }

    public function saveMappingField($sub_marketplace, $id_shop, $fields) {

        $sql_field = '';
        $sql_delete = '';
        $id_field = '';
        $id_field_array = array();
        $table_field = $this->prefix_mirakl.'mapping_field';

        foreach ($fields as $key => $field) {

            $sql_field = "SELECT id_field FROM $table_field WHERE sub_marketplace = $sub_marketplace AND id_shop = $id_shop  AND id_config_attribute = '{$field['id_config_attribute']}' ; ";
            $id_field = $this->sql_query($sql_field, '', true, 'id_field');

            if (isset($id_field) && !empty($id_field)) {

                $where = array(
                    'id_field' => array('operation' => '=', 'value' => $id_field),
                    'sub_marketplace' => array('operation' => "=", 'value' => $sub_marketplace),
                    'id_shop' => array('operation' => "=", 'value' => $id_shop),
                );
                $data = array();
                $data['shop_attribute'] = $field['shop_attribute'];
                $data['id_config_attribute'] = $field['id_config_attribute'];
                $sql_field = $this->update_string($table_field, $data, $where, false);
                $this->sql_query_effect($sql_field);
            } else {
                $data = array();
                $data['sub_marketplace'] = $sub_marketplace;
                $data['id_shop'] = $id_shop;
                $data['id_config_attribute'] = $field['id_config_attribute'];
                $data['shop_attribute'] = $field['shop_attribute'];
                $data['date_add'] = date('Y-m-d H:i:s');
                $sql_field = $this->insert_string($table_field, $data);
                $id_field = $this->sql_query_effect($sql_field, true);
            }

            $id_field_array[] = $id_field;
        }

        // delete not into field
        if (!empty($id_field_array)) {
            $id_field_array = implode(", ", $id_field_array);
            $sql_delete = "DELETE FROM $table_field WHERE sub_marketplace = $sub_marketplace AND id_shop = $id_shop AND id_field NOT IN ($id_field_array) ; ";
            $this->sql_query_effect($sql_delete);
        }

        return !empty($id_field) ? true : false;
    }

    public function saveMappingAttributeWithList($sub_marketplace, $id_shop, $attribute_with_list) {

        $id_field_attribute = '';
        $sql_field_attribute = '';
        $sql_delete = '';

        $id_field_attribute_array = array();
        $id_field_value = array();

        $table_field_attribute = $this->prefix_mirakl.'mapping_field_attribute';
        $table_field_value = $this->prefix_mirakl.'mapping_field_value';

        foreach ($attribute_with_list as $attribute) {

            $sql_field_attribute = "SELECT id_field_attribute FROM $table_field_attribute WHERE sub_marketplace = $sub_marketplace AND id_shop = $id_shop  AND id_config_attribute = '{$attribute['id_config_attribute']}' ";
            $id_field_attribute = $this->sql_query($sql_field_attribute, '', true, 'id_field_attribute');

            if (isset($id_field_attribute) && !empty($id_field_attribute)) {
                $where = array(
                    'id_field_attribute' => array('operation' => '=', 'value' => $id_field_attribute),
                    'sub_marketplace' => array('operation' => "=", 'value' => $sub_marketplace),
                    'id_shop' => array('operation' => "=", 'value' => $id_shop),
                );
                $data = array();
                $data['id_config_attribute'] = $attribute['id_config_attribute'];
                $data['shop_attribute'] = $attribute['shop_attribute'];
                $sql_field_attribute = $this->update_string($table_field_attribute, $data, $where, false);
                $this->sql_query_effect($sql_field_attribute);
            } else {
                $data = array();
                $data['sub_marketplace'] = $sub_marketplace;
                $data['id_shop'] = $id_shop;
                $data['id_config_attribute'] = $attribute['id_config_attribute'];
                $data['shop_attribute'] = $attribute['shop_attribute'];
                $data['date_add'] = date('Y-m-d H:i:s');
                $sql_field_attribute = $this->insert_string($table_field_attribute, $data);
                $id_field_attribute = $this->sql_query_effect($sql_field_attribute, true);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // field value
            if (!empty($id_field_attribute) && isset($attribute['field_value']) && !empty($attribute['field_value'])) {

                $id_field_value = '';
                $sql_field_value = '';

                foreach ($attribute['field_value'] as $id_config_value_list => $shop_attribute_value) {

                    $sql_field_value = "SELECT id_field_value FROM $table_field_value WHERE id_field_attribute = $id_field_attribute AND id_config_value_list = $id_config_value_list ; ";
                    $id_field_value = $this->sql_query($sql_field_value, '', true, 'id_field_value');

                    if (isset($id_field_value) && !empty($id_field_value)) {
                        $where = array(
                            'id_field_attribute' => array('operation' => '=', 'value' => $id_field_attribute),
                            'id_field_value' => array('operation' => '=', 'value' => $id_field_value),
                        );
                        $data = array();
                        $data['id_config_value_list'] = $id_config_value_list;
                        $data['shop_attribute_value'] = $shop_attribute_value;
                        $sql_field_value = $this->update_string($table_field_value, $data, $where, false);
                        $this->sql_query_effect($sql_field_value);
                    } else {
                        $data = array();
                        $data['id_field_attribute'] = $id_field_attribute;
                        $data['id_config_value_list'] = $id_config_value_list;
                        $data['shop_attribute_value'] = $shop_attribute_value;
                        $sql_field_value = $this->insert_string($table_field_value, $data);
                        $id_field_value = $this->sql_query_effect($sql_field_value, true);
                    }

                    $id_field_value_array[] = $id_field_value;
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $id_field_attribute_array[] = $id_field_attribute;
        }

        if (!empty($id_field_attribute_array)) {
            $id_field_attribute_array = implode(", ", $id_field_attribute_array);

            // delete field attribute
            $sql_delete = "DELETE FROM $table_field_attribute WHERE sub_marketplace = $sub_marketplace AND id_shop = $id_shop AND id_field_attribute NOT IN ($id_field_attribute_array) ; ";

            $this->sql_query_effect($sql_delete);

            // delete field value
            if (!empty($id_field_value_array)) {
                $id_field_value_array = implode(", ", $id_field_value_array);

                $sql_delete = "DELETE FROM $table_field_value WHERE id_field_attribute IN ($id_field_attribute_array) AND id_field_value NOT IN ($id_field_value_array) ; ";
                $this->sql_query_effect($sql_delete);
            }
        }

        return $id_field_attribute;
    }

    public function saveMappingAttributeList($sub_marketplace, $id_country, $id_shop, $attribute_list) {

//        $id_field_attribute = '';
//        $sql_field_attribute = '';
//        $sql_delete = '';
//
//        $id_field_attribute_array = array();
        $id_attribute_list_array = array();

        $table_attribute_list = $this->prefix_mirakl.'mapping_attribute_list';
        //$table_field_value = $this->prefix_mirakl.'mapping_field_value';
        //echo '<pre>'; print_r($attribute_list); exit;

        foreach ($attribute_list as $id_config_attribute => $attributes) {

            foreach ($attributes['shop_attribute_value'] as $shop_attribute => $id_config_value_list) {

                $sql_field_attribute = "SELECT id_attribute_list FROM $table_attribute_list WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND id_config_attribute = $id_config_attribute AND shop_attribute_value = '$shop_attribute' ; ";
                $id_attribute_list = $this->sql_query($sql_field_attribute, '', true, 'id_attribute_list');


                if (isset($id_attribute_list) && !empty($id_attribute_list)) {
                    $where = array(
                        'id_attribute_list' => array('operation' => '=', 'value' => $id_attribute_list),
                        'sub_marketplace' => array('operation' => "=", 'value' => $sub_marketplace),
                        'id_country' => array('operation' => "=", 'value' => $id_country),
                        'id_shop' => array('operation' => "=", 'value' => $id_shop),
                    );

                    $data = array();
                    $data['id_config_attribute'] = $id_config_attribute;
                    //$data['shop_attribute_value'] = $shop_attribute;
                    $data['id_config_value_list'] = $id_config_value_list;
                    $sql_field_attribute = $this->update_string($table_attribute_list, $data, $where, false);
                    $this->sql_query_effect($sql_field_attribute);
                } else {
                    $data = array();
                    $data['sub_marketplace'] = $sub_marketplace;
                    $data['id_country'] = $id_country;
                    $data['id_shop'] = $id_shop;
                    $data['id_config_attribute'] = $id_config_attribute;
                    $data['shop_attribute_value'] = $shop_attribute;
                    $data['id_config_value_list'] = $id_config_value_list;

                    $sql_attribute_list = $this->insert_string($table_attribute_list, $data);
                    $id_attribute_list = $this->sql_query_effect($sql_attribute_list, true);
                }

                $id_attribute_list_array[] = $id_attribute_list;
            }
        }

        if (!empty($id_attribute_list_array)) {
            $id_attribute_list_array = implode(", ", $id_attribute_list_array);
            // delete field attribute
            $sql_delete = "DELETE FROM $table_attribute_list WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND id_attribute_list NOT IN ($id_attribute_list_array) ; ";
            $this->sql_query_effect($sql_delete);
        }

        return true;
    }

//    public function saveMappingValue($sub_marketplace, $id_country, $id_shop, $value_mapping) {
//
//        $sql = '';
//        $sql_detail = '';
//        $table = $this->prefix_mirakl.'mapping_values';
//        $table_detail = $this->prefix_mirakl.'mapping_values_detail';
//        $date = date('Y-m-d H:i:s');
//
//        $data = array();
//        $data['id_shop'] = $id_shop;
//        $data['sub_marketplace'] = $sub_marketplace;
//        $data['id_country'] = $id_country;
//        $data['date_add'] = $date;
//
//        foreach ($value_mapping as $mapping) {
//
//            $id_model = $mapping['id_model'];
//            $sql_query = "SELECT id_mapping_value FROM $table WHERE id_model = $id_model ";
//            $id_mapping_value = $this->sql_query($sql_query, '', true, 'id_mapping_value');
//
//            if (empty($id_mapping_value)) {
//                $data['id_model'] = $id_model;
//                $data['hierarchy_code'] = '';
//                $sql = $this->insert_string($table, $data);
//                $id_mapping_value = $this->sql_query_effect($sql, true);
//            }
//
//            if (isset($mapping['value_detail']) && !empty($mapping['value_detail'])) {
//                foreach ($mapping['value_detail'] as $id_model_detail => $value_detail) {
//
//                    if (isset($value_detail['mapping']) && !empty($value_detail['mapping'])) {
//                        foreach ($value_detail['mapping'] as $id_value_list => $id_attribute) {
//                            $data_detail = array();
//                            $data_detail['id_mapping_value'] = $id_mapping_value;
//                            $data_detail['id_model_detail'] = $id_model_detail;
//                            $data_detail['id_attribute_group'] = $value_detail['id_attribute_group'];
//                            $data_detail['id_value_list'] = $id_value_list;
//                            $data_detail['id_attribute'] = $id_attribute;
//                            $data_detail['field_marketplace'] = $value_detail['field_marketplace'];
//                            $data_detail['date_add'] = $date;
//
//                            $sql_query = "SELECT id_mapping_value_detail FROM $table_detail WHERE id_mapping_value = $id_mapping_value AND id_model_detail = $id_model_detail AND id_value_list = $id_value_list ";
//                            $id_mapping_value_detail = $this->sql_query($sql_query, '', true, 'id_mapping_value_detail');
//
//                            if (isset($id_mapping_value_detail) && !empty($id_mapping_value_detail)) {
//                                $where = array(
//                                    'id_mapping_value_detail' => array('operation' => '=', 'value' => $id_mapping_value_detail)
//                                );
////                                $sql = $this->update_string($table_detail, $data_detail, $where, false);
////                                $sql_detail = $this->update_string($table_detail, $data_detail, $where);
////                                $this->sql_query_effect($sql);
//
//                                $sql_detail = $this->update_string($table_detail, $data_detail, $where, false);
//                                // echo '<pre>'; print_r($sql_detail); exit;
//                                $this->sql_query_effect($sql_detail);
//                            } else {
//                                $sql_detail = $this->insert_string($table_detail, $data_detail);
//
//                                $id_mapping_value_detail = $this->sql_query_effect($sql_detail, true);
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        return $id_mapping_value;
//    }

    public function getMiraklAttributeNoHierarchy($sub_marketplace) {
//        $sql = "SELECT CA.*, IFNULL(F.shop_attribute, '') shop_attribute "
//                ."FROM ::PREFIX::config_attribute CA "
//                ."LEFT JOIN ::PREFIX::mapping_field F ON CA.id_config_attribute = F.id_config_attribute "
//                ."WHERE CA.sub_marketplace = $sub_marketplace AND CA.hierarchy_code = '' AND CA.type_parameter = '' "
//                ."ORDER BY CA.required DESC ";

        $sql = "SELECT CA.*, IFNULL(F.shop_attribute, '') shop_attribute "
                ."FROM ::PREFIX::config_attribute CA "
                ."LEFT JOIN ::PREFIX::mapping_field F ON CA.id_config_attribute = F.id_config_attribute "
                ."WHERE CA.sub_marketplace = $sub_marketplace AND CA.hierarchy_code = '' AND CA.id_config_attribute NOT IN "
                ."(SELECT id_config_attribute FROM mirakl_config_attribute SUB_CA INNER JOIN mirakl_config_value SUB_V ON SUB_CA.type_parameter IN (SUB_V.code) WHERE SUB_CA.hierarchy_code = '' AND SUB_CA.sub_marketplace = $sub_marketplace ) "
                ."ORDER BY CA.required DESC ";

        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function getMiraklAttributeNoHierarchyHaveValueList($sub_marketplace, $id_shop, $id_lang) {
        $data = array();

        $sql = "SELECT CA.id_config_attribute, CA.code 'code_attribute', CA.type, CA.default_value, CA.description, CA.example, CA.label 'label_attribute', CA.required, CA.transformations, CA.type_parameter, CA.validations, CA.variant, "
                ."FA.id_field_attribute, FA.shop_attribute, "
                ."V.id_config_value, V.code 'code_value', V.label 'label_value', "
                ."VL.id_config_value_list, VL.code 'code_value_list', VL.label 'label_value_list', "
                ."FV.id_field_value, FV.shop_attribute_value "
                ."FROM ::PREFIX::config_attribute CA "
                ."LEFT JOIN ::PREFIX::mapping_field_attribute FA ON CA.id_config_attribute = IFNULL(FA.id_config_attribute, '') "
                ."LEFT JOIN ::PREFIX::config_value V ON (CA.type_parameter = V.code OR CA.values_list = V.code) AND V.sub_marketplace = $sub_marketplace "
                ."LEFT JOIN mirakl_config_value_list VL ON V.id_config_value = VL.id_config_value "
                ."LEFT JOIN mirakl_mapping_field_value FV ON VL.id_config_value_list = FV.id_config_value_list "
                ."WHERE CA.sub_marketplace = $sub_marketplace  AND IFNULL(CA.hierarchy_code, '') = '' AND (CA.type_parameter != '' OR CA.values_list != '') AND type = 'LIST' "
                ."ORDER BY CA.required DESC, CA.id_config_attribute, V.id_config_value, VL.id_config_value_list ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $id_config_attribute = $result['id_config_attribute'];
                $id_field_attribute = $result['id_field_attribute'];
                $id_config_value = $result['id_config_value'];
                $id_config_value_list = $result['id_config_value_list'];
                $id_field_value = $result['id_field_value'];

                $data[$id_config_attribute]['code_attribute'] = $result['code_attribute'];
                $data[$id_config_attribute]['type'] = $result['type'];
                $data[$id_config_attribute]['default_value'] = $result['default_value'];
                $data[$id_config_attribute]['description'] = $result['description'];
                $data[$id_config_attribute]['example'] = $result['example'];
                $data[$id_config_attribute]['label_attribute'] = $result['label_attribute'];
                $data[$id_config_attribute]['required'] = $result['required'];
                $data[$id_config_attribute]['transformations'] = $result['transformations'];
                $data[$id_config_attribute]['type_parameter'] = $result['type_parameter'];
                $data[$id_config_attribute]['validations'] = $result['validations'];
                $data[$id_config_attribute]['variant'] = $result['variant'];

                $data[$id_config_attribute]['id_field_attribute'] = $id_field_attribute;
                $data[$id_config_attribute]['shop_attribute'] = $result['shop_attribute'];

                $data[$id_config_attribute]['id_config_value'] = $id_config_value;
                $data[$id_config_attribute]['code_value'] = $this->replace_quote($result['code_value']);
                $data[$id_config_attribute]['label_value'] = $this->replace_quote($result['label_value']);

                $data[$id_config_attribute]['value_list'][$id_config_value_list]['code_value_list'] = $this->replace_quote($result['code_value_list']);
                $data[$id_config_attribute]['value_list'][$id_config_value_list]['label_value_list'] = $this->replace_quote($result['label_value_list']);

                $data[$id_config_attribute]['value_list'][$id_config_value_list]['id_field_value'] = $id_field_value;
                $data[$id_config_attribute]['value_list'][$id_config_value_list]['shop_attribute_value'] = $result['shop_attribute_value'];

                if (!empty($result['shop_attribute'])) {
                    $shop_field = $this->getValueByFieldShop($id_shop, $id_lang, array(), $result['shop_attribute']);
                    $data[$id_config_attribute]['shop_attribute_value'] = $shop_field['results'];
                }
            }
        }
        //echo '<pre>'; print_r($data); exit;
        return $data;
    }

    public function getMiraklMapping($value) {
        $data = array();

        $sql = "SELECT VL.id_config_value_list, VL.id_config_value, VL.code, VL.label, MFV.id_field_value, MFV.shop_attribute_value "
                ."FROM ::PREFIX::config_value_list VL "
                ."LEFT JOIN ::PREFIX::mapping_field_value MFV ON VL.id_config_value_list = MFV.id_config_value_list   "
                ."WHERE VL.id_config_value = {$value['id_config_value']} AND VL.id_config_value_list > {$value['id_config_value_list']}  "
                ."LIMIT 30 ";
        $results = $this->sql_query($sql, $this->prefix_mirakl);

        return $results;
    }

    public function getMiraklMappingModelAttribute($sub_marketplace, $id_country, $id_shop, $id_lang) {
        $data = array();
        $set = array();

        $sql = "SELECT CA.id_config_attribute, CA.code 'code_attribute', CA.label 'label_attribute', CA.description, CA.example, CA.type_parameter, "
                ."MA.id_model_attribute, MA.id_model, MA.shop_attribute, "
                ."V.id_config_value, V.code 'code_value', V.label 'label_value', "
                ."VL.id_config_value_list, VL.code 'code_value_list', VL.label 'label_value_list' "
                ."FROM ::PREFIX::config_attribute CA "
                ."INNER JOIN ::PREFIX::mapping_model M ON M.sub_marketplace = $sub_marketplace AND M.id_country = $id_country AND M.id_shop = $id_shop "
                ."INNER JOIN ::PREFIX::mapping_model_attribute MA ON M.id_model = MA.id_model AND CA.id_config_attribute = MA.id_config_attribute "
                ."INNER JOIN ::PREFIX::config_value V ON BINARY V.code = CA.type_parameter "
                ."INNER JOIN ::PREFIX::config_value_list VL ON V.id_config_value = VL.id_config_value "
                ."WHERE CA.sub_marketplace = $sub_marketplace AND MA.shop_attribute != '' AND CA.type = 'LIST'  "
                ."ORDER BY MA.shop_attribute ";
        
        $results = $this->sql_query($sql, $this->prefix_mirakl);
        $id2 = '';
        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {

                $id_config_attribute = $result['id_config_attribute'];
                $id_config_value = $result['id_config_value'];
                $id_config_value_list = $result['id_config_value_list'];
                $shop_attribute = $result['shop_attribute'];
                $data_field = explode('-', $shop_attribute);
                $field_type = isset($data_field[0]) ? $data_field[0] : '';
                $field_shop = isset($data_field[1]) ? $data_field[1] : '';

                $data[$field_type][$shop_attribute]['id_config_attribute'] = $id_config_attribute;
                $data[$field_type][$shop_attribute]['code_attribute'] = $result['code_attribute'];
                $data[$field_type][$shop_attribute]['label_attribute'] = $result['label_attribute'];
                $data[$field_type][$shop_attribute]['description'] = $result['description'];
                $data[$field_type][$shop_attribute]['example'] = $result['example'];
                $data[$field_type][$shop_attribute]['type_parameter'] = $result['type_parameter'];

                $data[$field_type][$shop_attribute]['field_type'] = $field_type;
                $data[$field_type][$shop_attribute]['field_shop'] = $field_shop;

                $data[$field_type][$shop_attribute]['code_value'] = $result['code_value'];
                $data[$field_type][$shop_attribute]['label_value'] = $result['label_value'];

                $data[$field_type][$shop_attribute]['value_list'][$id_config_value_list]['code_value_list'] = $result['code_value_list'];
                $data[$field_type][$shop_attribute]['value_list'][$id_config_value_list]['label_value_list'] = $result['label_value_list'];

                $fields = $this->getValueByFieldShop($id_shop, $id_lang, array(), $shop_attribute, true, $sub_marketplace, $id_country);
                $data[$field_type][$shop_attribute]['field_shop_name'] = $fields['field_shop_name'];
                $data[$field_type][$shop_attribute]['field_shop_list'] = $fields['results'];
                
                //$set[$field_type][$shop_attribute] = $id_config_attribute;

            }
        } else {
            $sql_delete = "DELETE FROM {$this->prefix_mirakl}mapping_attribute_list WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ; ";
            $this->sql_query_effect($sql_delete);
        }

//echo '<pre>'; print_r($data); exit;
        return $data;
    }

}

//end definition