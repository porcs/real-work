<?php

require_once dirname(__FILE__).'/../classes/mirakl.parameter.php';
require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');

class MiraklScheme {

    private $mysql_db;
    private $id_marketplace;

    public function __construct() {
        $this->mysql_db = new ci_db_connect();
        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;
    }

    private function mysql_query($sql, $current = false, $key = '') {
        $result = $this->mysql_db->db_array_query($sql);

        if ($current) {
            if (!empty($key)) {
                return $result[0][$key];
            } else {
                return isset($result[0]) && !empty($result[0]) ? $result[0] : reset($result);
            }
        }

        return $result;
    }

    private function mysql_replace($table, $data, $sql_text = false) {
        $result = $this->mysql_db->replace_db($table, $data, $sql_text);
        return $result;
    }

    public function getShopInfo($id_user) {
        $data = array();
        $sql = "SELECT * FROM configuration WHERE id_customer = $id_user AND name IN ('FEED_SHOP_INFO') ";
        $results = $this->mysql_query($sql, true);

        if (isset($results) && !empty($results)) {
            $data = unserialize(base64_decode($results['value']));
        }

        return $data;
    }

    public function getMiraklConfiguration($id_user, $sub_marketplace, $id_country, $id_shop) {

        if (empty($id_user) || empty($sub_marketplace) || empty($id_country) || empty($id_shop)) {
            return false;
        }

        $sql = "SELECT MC.*, REPLACE(MC.countries, ' ', '_') AS 'countries', FSM.name_marketplace AS 'marketplace', FSM.id_marketplace, IFNULL(L.language_name,'english') AS 'language' "
                ."FROM mirakl_configuration MC "
                ."LEFT JOIN users U ON MC.id_customer = U.id "
                ."LEFT JOIN offer_sub_marketplace FSM ON MC.sub_marketplace = FSM.Id_offer_sub_marketplace AND FSM.id_marketplace = {$this->id_marketplace} "
                ."LEFT JOIN languages L ON L.id = U.user_default_language "
                ."WHERE MC.id_customer = $id_user AND MC.sub_marketplace = $sub_marketplace AND MC.id_country = $id_country AND MC.id_shop = $id_shop ";

        $results = $this->mysql_query($sql, true);
        return $results;
    }

    public function getApiByUserName($user_name, $sub_marketplace, $id_country) {
        $sql = "SELECT api_key FROM mirakl_configuration WHERE user_name = '$user_name' AND sub_marketplace = $sub_marketplace AND id_country = $id_country ";
        $query = $this->mysql_db->select_query($sql);
        $results = $this->mysql_db->fetch($query);
        return isset($results['api_key']) ? $results['api_key'] : '';
    }

    public function getApiByUserId($id_user, $sub_marketplace, $id_country, $id_shop) {
        $sql = "SELECT api_key, auto_accept, logo, active "
                ."FROM mirakl_configuration "
                ."WHERE id_customer = $id_user AND sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ";

        $results = $this->mysql_query($sql, true);
        return $results;
    }

    public function getMiraklAdditionalField($sub_marketplace, $enable = 1) {
        $sql = "SELECT * "
                ."FROM mirakl_additional_field "
                ."WHERE sub_marketplace = $sub_marketplace AND enable =  $enable ";

        $results = $this->mysql_query($sql);
        return $results;
    }

    public function getLanguageCode($id_country) {
        $sql = "SELECT language_code FROM offer_sub_packages "
                ."WHERE id_offer_sub_pkg = $id_country ";

        $result = $this->mysql_query($sql, true, 'language_code');
        return $result;
    }

    public function saveMiraklConfiguration($data) { // must use in save parameter
        $result = $this->mysql_replace('mirakl_configuration', $data);
        return $result;
    }

    public function getMiraklHierarchy($sub_marketplace, $code = array(), $key_code = false, $id_user = '') {
        $data = array();

        $sql = "SELECT id_hierarchy, code, label "
                ."FROM mirakl_hierarchy WHERE 1 ";

        if (!$key_code) {
            $sql.= "AND level = 1 ";
        }

        if (!empty($id_user)) {
            $sql.= "AND id_user = $id_user ";
        }

        if (!empty($code)) {
            if (is_array($code)) {
                $code = implode("', '", $code);
                $sql.= "AND code IN ('$code') ";
            }
        }

        $sql.= "AND sub_marketplace = $sub_marketplace ORDER BY label ";

        $results = $this->mysql_query($sql);

        if ($key_code) {
            foreach ($results as $result) {
                $code = $result['code'];
                $data[$code] = $result['label'];
            }
        }

        return $key_code ? $data : $results;
    }

    public function getMiraklSubHierarchy($id_user, $hierarchy_code) {
        $data = array();
        $sql = "SELECT * FROM mirakl_hierarchy WHERE id_user = $id_user AND parent_code = '$hierarchy_code' ";
        $results = $this->mysql_db->db_array_query($sql);

        if (isset($results) && !empty($results)) {
            foreach ($results as $key => $result) {
                $data['parent_code'] = $result['parent_code'];
                $data['level'] = $result['level'];

                $data['option'][$key]['id_hierarchy'] = $result['id_hierarchy'];
                $data['option'][$key]['code'] = $result['code'];
                $data['option'][$key]['label'] = $result['label'];
            }
        }

        return $data;
    }

    public function getMiraklAttributeNoHierarchy($id_user, $sub_marketplace) {
        $sql = "SELECT * FROM mirakl_attribute WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND hierarchy_code = '' AND values_list = '' ORDER BY required DESC ";
        $results = $this->mysql_db->db_array_query($sql);
        return $results;
    }

//    public function getMiraklAttributeNoHierarchyHaveValueList($id_user, $sub_marketplace) {
//        $data = array();
//        $sql = "SELECT * FROM mirakl_attribute WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND hierarchy_code = '' AND values_list != '' ";
//        $results_attribute = $this->mysql_query($sql);
//
//        // get in values_list for where in mirakl_value
//        $values_list = array_map(function($params) {
//            return $params['values_list'];
//        }, $results_attribute);
//
//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        if (isset($values_list) && !empty($values_list)) {
//            $values_list = implode("','", $values_list);
//            $sql_value = "SELECT V.id_value, V.code, V.label, VL.id_value_list, VL.code 'code_list', VL.label 'label_list' "
//                    ."FROM mirakl_value V "
//                    ."INNER JOIN mirakl_value_list VL ON V.id_value = VL.id_value "
//                    ."WHERE V.id_user = $id_user AND V.sub_marketplace = $sub_marketplace AND V.code IN ('$values_list') ";
//            $results_value = $this->mysql_query($sql_value);
//
//            // get data in mirakl_value and set key = code, value = mirakl_value
//            $results_key_value = array_reduce($results_value, function ($result, $value) {
//                $result[$value['code']][$value['id_value_list']] = $value;
//                return $result;
//            });
//        }
//
//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        if (isset($results_attribute) && !empty($results_attribute)) {
//            foreach ($results_attribute as $key => $result_attribute) {
//                $data[$key]['id_attribute'] = $result_attribute['id_attribute'];
//                $data[$key]['code'] = str_replace(' ', '_', $result_attribute['code']);
//                $data[$key]['type'] = $result_attribute['type'];
//                $data[$key]['default_value'] = $result_attribute['default_value'];
//                $data[$key]['description'] = $result_attribute['description'];
//                $data[$key]['example'] = $result_attribute['example'];
//                $data[$key]['hierarchy_code'] = $result_attribute['hierarchy_code'];
//                $data[$key]['label'] = $result_attribute['label'];
//                $data[$key]['required'] = $result_attribute['required'];
//                $data[$key]['transformations'] = $result_attribute['transformations'];
//                $data[$key]['type_parameter'] = $result_attribute['type_parameter'];
//                $data[$key]['validations'] = $result_attribute['validations'];
//                $data[$key]['values_list'] = $result_attribute['values_list'];
//                $data[$key]['variant'] = $result_attribute['variant'];
//                $data[$key]['values_list_detail'] = isset($results_key_value[$result_attribute['values_list']]) ? $results_key_value[$result_attribute['values_list']] : '';
//            }
//        }
//
//        return $data;
//    }

    public function getMiraklValueList($sub_marketplace, $code = array(), $id_user) {
        $data = array();

        $sql = "SELECT M.id_value, M.code, M.label, ML.id_value_list, ML.code 'list_code', ML.label 'list_label' "
                ."FROM mirakl_value M "
                ."INNER JOIN mirakl_value_list ML ON M.id_value = ML.id_value "
                ."WHERE M.id_user = $id_user AND M.sub_marketplace = $sub_marketplace ";

        if (!empty($code)) {
            $code_value = array();
            $code = implode("', '", $code);
            $sql_attribute = "SELECT id_attribute, code, values_list FROM mirakl_attribute WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace  AND code IN ('$code') ; ";
            $results_attribute = $this->mysql_query($sql_attribute);

            if (isset($results_attribute) && !empty($results_attribute)) {
                foreach ($results_attribute as $attribute) {
                    if (!empty($attribute['values_list'])) {
                        $code_value[] = $attribute['values_list'];
                    }
                }
            }

            $code_value = implode("', '", $code_value);

            $sql.= "AND M.code IN ('$code_value') ";
        }

        $results = $this->mysql_query($sql);

        if (!empty($results)) {
            if (!empty($code)) {
                foreach ($results as $result) {
                    $code = $result['code'];
                    $id_value_list = $result['id_value_list'];

                    $data[$code]['label'] = $result['label'];
                    $data[$code]['list_detail'][$id_value_list]['list_code'] = $result['list_code'];
                    $data[$code]['list_detail'][$id_value_list]['list_label'] = $result['list_label'];
                }
            } else {
                foreach ($results as $result) {
                    $id_value_list = $result['id_value_list'];

                    $data[$id_value_list]['list_code'] = $result['list_code'];
                    $data[$id_value_list]['list_label'] = $result['list_label'];
                }
            }
        }
        return $data;
    }

    public function getMiraklAttribute($sub_marketplace, $code, $id_user) {
        $data = array();
        $code = implode("', '", $code);
        $sql = "SELECT * FROM mirakl_attribute WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND code IN ('$code') ";
        $results = $this->mysql_query($sql);

        foreach ($results as $key => $result) {
            $code = str_replace(' ', '_', $result['code']);

            $data[$code]['id_attribute'] = $result['id_attribute'];
            $data[$code]['label'] = $result['label'];
            $data[$code]['values_list'] = $result['values_list'];
        }

        return $data;
    }

    public function getAttributeByHierarchy($id_user, $hierarchy_array = array()) {

        $data = array();
        $hierarchy_code = array();

        if (isset($hierarchy_array) && empty($hierarchy_array)) {
            return $data;
        }

        foreach ($hierarchy_array as $hierarchy) {
            if (empty($hierarchy) || $hierarchy == '') { // if value == '' break;
                break;
            }
            $hierarchy_code[] = $hierarchy;
        }

        if (!empty($hierarchy_code)) {
            $hierarchy_code = implode("','", $hierarchy_code);
        } else {
            return $data;
        }

        $sql = "SELECT A.* FROM mirakl_hierarchy H "
                ."INNER JOIN mirakl_attribute A ON H.code = A.hierarchy_code "
                ."WHERE H.id_user = $id_user AND A.id_user = $id_user AND A.hierarchy_code IN ('$hierarchy_code') AND (H.parent_code = '{$hierarchy_array[0]}' OR H.code = '{$hierarchy_array[0]}') ";

        $results = $this->mysql_db->db_array_query($sql);

        return $results;
    }

    public function getMiraklAttributeByFieldMarketplace($id_user, $sub_marketplace, $field_marketplace) {
        $sql = "SELECT * FROM mirakl_attribute "
                ."WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND code = '$field_marketplace' ";
        $results = $this->mysql_query($sql, true);

        return $results;
    }

    public function getMiraklAttributeRequire($id_user, $sub_marketplace) {
        $data = array();
        $sql = "SELECT * FROM mirakl_attribute "
                ."WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND hierarchy_code != '' AND required = 'Y'";

        $results = $this->mysql_query($sql);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $code = $result['code'];
                $hierarchy_code = $result['hierarchy_code'];

                $data[$hierarchy_code][$code] = $result['required'];
            }
        }

        return $data;
    }

    public function getMiraklFieldRequire($id_user, $sub_marketplace) {
        $data = array();
        $sql = "SELECT * FROM mirakl_attribute "
                ."WHERE id_user = $id_user AND sub_marketplace = $sub_marketplace AND hierarchy_code = '' AND required = 'Y'";

        $results = $this->mysql_query($sql);

        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                $data[$result['code']] = $result['label'];
            }
        }

        return $data;
    }

}

//end definition