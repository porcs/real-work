<?php

require_once(dirname(__FILE__).'/mirakl.tools.php');
require_once(dirname(__FILE__).'/../../FeedBiz_Orders.php');

class MiraklOrder {

    private $connector;
    private $id_marketplace;
    private $user;
    private $debug;
    private $prefix_order;
    private $prefix_mirakl;

    public function __construct($user, $database = 'products', $debug = false) {
        $this->connector = new Db($user, $database);
        $this->user = $user;
        $this->debug = $debug;

        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;

        //set table prefix for product :: products_
        $this->prefix_product = MiraklParameter::PREFIX_PRODUCT;

        //set table prefix for order :: orders_
        $this->prefix_order = MiraklParameter::PREFIX_ORDER;

        //set table prefix for mirakl
        $this->prefix_mirakl = MiraklParameter::PREFIX_MIRAKL;
    }

    private function sql_query($sql = '', $prefix = '', $current = false, $key = '') {

        if (empty($sql)) {
            return false;
        }

        $sql = str_replace("::PREFIX::", $prefix, $sql);
        $result = $this->connector->db_query_str($sql);

        if (empty($result)) {
            return false;
        }

        if ($current) {
            if (!empty($key)) {
                return $result[0][$key];
            } else {
                return $result[0];
            }
        }

        return $result;
    }

    private function sql_query_effect($sql = '', $last_id = false) {

        if (empty($sql)) {
            return false;
        }

        $this->connector->db_query_result_str($sql);

        if ($last_id) {
            return $this->connector->db_last_insert_rowid();
        }

        $affected = $this->connector->db_changes();

        if (!empty($affected)) {
            return true;
        }

        return false;
    }

    private function sql_query_exec($sql = '') {
        // for list many sql text eg.insert
        if (empty($sql)) {
            return false;
        }

        return $this->connector->db_exec($sql);
    }

    private function sql_text($sql = '', $prefix = '') {
        $sql = str_replace("::PREFIX::", $prefix, $sql);
        return $sql;
    }

    private function sql_num_rows($sql, $prefix = '') {
        $sql = str_replace("::PREFIX::", $prefix, $sql);
        $query = $this->connector->db_sqlit_query($sql);
        $num_row = $this->connector->db_num_rows($query);
        return (int) $num_row;
    }

    private function _filter_data($table, $data) {
        $filtered_data = array();
        $columns = $this->_list_fields($table);
        if (is_array($data)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data)) {
                    if (is_array($data[$column])) {
                        $filtered_data[$column] = Mirakl_Tools::escape_str(serialize($data[$column]));
                    } else {
                        if ($data[$column] == '0') {
                            $filtered_data[$column] = $data[$column];
                        } else {
                            $filtered_data[$column] = Mirakl_Tools::escape_str($data[$column]);
                        }
                    }
                } else {
                    $filtered_data[$column] = '';
                }
            }
        }
        return $filtered_data;
    }

    private function _list_fields($table, $db_type = null) {
        return $this->connector->get_all_column_name($table, true, true);
    }

    private function insert_string($table, $data, $nf_prefix = true) {
        if (isset($data) && !empty($data)) {
            $filter_data = $this->_filter_data($table, $data);
            return $this->connector->insert_string($table, $filter_data, $nf_prefix);
        }
        return null;
    }

    private function update_string($table, $data, $where = array(), $filter = true) {
        if (isset($data) && !empty($data)) {

            if ($filter) {
                $filter_data = $this->_filter_data($table, $data);
            } else {
                $filter_data = $data;
            }

            return $this->connector->update_string($table, $filter_data, $where, 0, true);
        }
        return null;
    }

    private function delete_string($table, $where = array(), $nf_prefix = true) {
        return $this->connector->delete_string($table, $where, $nf_prefix);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function insert_string_order($table, $data) {
        $table = $this->prefix_order.$table;
        return $this->insert_string($table, $data);
    }

    private function getIdOrder($table, $check, $conditions = array()) {

        if (empty($check)) {
            return false;
        }

        $condition = '';

        if (!empty($conditions)) {
            $condition.= "AND id_shop = {$conditions['id_shop']} AND id_marketplace = {$conditions['id_marketplace']} AND site = '{$conditions['id_country']}' ";
        }

        if (isset($check['id_orders']) && !empty($check['id_orders'])) {
            $condition.= "AND id_orders = {$check['id_orders']} ";
        }

        if (isset($check['id_marketplace_order_ref']) && !empty($check['id_marketplace_order_ref'])) {
            $condition.= "AND id_marketplace_order_ref = '{$check['id_marketplace_order_ref']}' ";
        }

        if (isset($check['id_marketplace']) && !empty($check['id_marketplace'])) {
            $condition.= "AND id_marketplace = '{$check['id_marketplace']}' ";
        }

        $sql = "SELECT id_orders FROM $table WHERE 1 $condition ";

        $result = $this->sql_query($sql, '', true, 'id_orders');

        return $result;
    }

    private function getIdOrderItem($table, $check) {

        if (empty($check)) {
            return false;
        }

        $condition = "AND id_orders = {$check['id_orders']} ";

        if (isset($check['id_product']) && !empty($check['id_product'])) {
            $condition.= "AND id_product = {$check['id_product']} ";
        }

        if (isset($check['id_order_items']) && !empty($check['id_order_items'])) {
            $condition.= "AND id_order_items = {$check['id_order_items']} ";
        }

        $sql = "SELECT id_order_items FROM $table WHERE 1 $condition ";

        $result = $this->sql_query($sql, '', true, 'id_order_items');

        return $result;
    }

    private function getIdOrdersAccept($table, $check) {

        if (empty($table) || empty($check)) {
            return false;
        }

        $condition = "";
        $condition.= "AND order_id = '{$check['order_id']}' ";
        $condition.= "AND sub_marketplace = {$check['sub_marketplace']} ";
        $condition.= "AND id_country = {$check['id_country']} ";
        $condition.= "AND id_shop = {$check['id_shop']} ";

        $sql = "SELECT id_order_accept FROM $table WHERE 1 $condition ";
        $result = $this->sql_query($sql, '', true, 'id_order_accept');
        return $result;
    }

    private function getIdOrdersAcceptItem($table_detail, $check) {

        if (empty($table_detail) || empty($check)) {
            return false;
        }

        $condition = "";
        $condition.= "AND id_order_accept = '{$check['id_order_accept']}' ";
        $condition.= "AND order_line_index = '{$check['order_line_index']}' ";

        $sql = "SELECT id_order_accept_item FROM $table_detail WHERE 1 $condition ";
        $result = $this->sql_query($sql, '', true, 'id_order_accept_item');
        return $result;
    }

    private function saveOrderSubMarketplace($ins, $subdata) {

        $table = $this->prefix_order.'order_sub_marketplace';

        if ($ins) {
            $sql = $this->insert_string($table, $subdata);
            return $this->sql_query_effect($sql, true);
        } else {
            $where = array(
                'id_orders' => array('operation' => '=', 'value' => $subdata['id_orders'])
            );
            $sql_update = $this->update_string($table, $subdata, $where);
            return $this->sql_query_effect($sql_update);
        }
    }

    public function checkOrderStatus($order_id, $conditions = array()) {

        $sub_marketplace = $conditions['sub_marketplace'];
        $id_country = $conditions['id_country'];
        $id_shop = $conditions['id_shop'];
        $id_marketplace = $conditions['id_marketplace'];

        $sql = "SELECT 1 "
                ."FROM ::PREFIX::orders O "
                ."INNER JOIN {$this->prefix_order}order_sub_marketplace OS ON O.id_orders = OS.id_orders "
                ."WHERE O.id_marketplace_order_ref = '$order_id' AND O.status = 1 AND O.id_shop = $id_shop AND O.id_marketplace = $id_marketplace "
                ."AND OS.sub_marketplace = $sub_marketplace AND OS.id_country = $id_country ";

        $num_row = $this->sql_num_rows($sql, $this->prefix_order);
        return $num_row;
    }

    public function checkOrderProductAttribute($data) {
        $product = isset($data['id_order_items']) ? $data['id_order_items'] : '';
        $attr_group = isset($data['id_attribute_group']) ? $data['id_attribute_group'] : '';
        $attr = isset($data['id_attribute']) ? $data['id_attribute'] : '';
        $id_shop = isset($data['id_shop']) ? $data['id_shop'] : '';


        $sql = "SELECT 1 FROM ::PREFIX::order_items_attribute "
                ."WHERE id_order_items = $product AND id_attribute_group $attr_group AND id_attribute = $attr AND id_shop = $id_shop ";

        $num_row = $this->sql_num_rows($sql, $this->prefix_order);
        return $num_row > 0 ? true : false;
    }

    public function saveOrder($otable, $data, $conditions = array()) { //save_update_order_import($table, $data, $option = array()) {
        $table = $this->prefix_order.$otable;
        $ins = 1;

        $check = array();
        $check['id_orders'] = isset($data['id_orders']) ? $data['id_orders'] : '';
        $check['id_marketplace_order_ref'] = isset($data['id_marketplace_order_ref']) ? $data['id_marketplace_order_ref'] : '';
        $check['id_marketplace'] = isset($data['id_marketplace']) ? $data['id_marketplace'] : '';

        $id_order = $this->getIdOrder($table, $check, $conditions);
        if (isset($id_order) && !empty($id_order)) {
            $where = array();
            $where['id_orders'] = array('operation' => '=', 'value' => $id_order);

            if (!empty($check['id_marketplace'])) {
                $where['id_marketplace'] = array('operation' => '=', 'value' => $check['id_marketplace']);
            }

            $ins = 0;
            $sql_update = $this->update_string($table, $data, $where);
            $status = $this->sql_query_effect($sql_update);
        } else {
            $sql = $this->insert_string($table, $data);
            $id_order = $this->sql_query_effect($sql, true);

            $status = 1;
            $ins = 1;

            if ($table == $this->prefix_order.'orders') {
                $subdata = array();
                $subdata['id_orders'] = $id_order;
                $subdata['id_country'] = $conditions['id_country'];
                $subdata['id_shop'] = $conditions['id_shop'];
                $subdata['sub_marketplace'] = $conditions['sub_marketplace'];
                $subdata['date_add'] = $conditions['date_add'];
                $this->saveOrderSubMarketplace($ins, $subdata);
            }
        }

        return array('id_order' => $id_order, 'ins' => $ins, 'status' => $status);
    }

    public function saveOrderLine($otable, $data) {// save or update order items
        if (!isset($data['id_orders']) && empty($data['id_orders'])) {
            return '';
        }

        $ins = 1;
        $table = $this->prefix_order.$otable;
        $id_orders = $data['id_orders'];
        $id_product = isset($data['id_product']) ? $data['id_product'] : '';
        $id_order_items = isset($data['id_order_items']) ? $data['id_order_items'] : '';

        $check = array();
        $check['id_orders'] = $id_orders;
        $check['id_product'] = $id_product;
        $check['id_order_items'] = $id_order_items;

        $id_order_items = '';
        $id_order_items = $this->getIdOrderItem($table, $check);

        if (isset($id_order_items) && !empty($id_order_items)) {
            $where = array();
            $where['id_orders'] = array('operation' => '=', 'value' => $id_orders);

            if (!empty($check['id_product'])) {
                $where['id_product'] = array('operation' => '=', 'value' => $check['id_product']);
            }

            if (!empty($check['id_order_items'])) {
                $where['id_order_items'] = array('operation' => '=', 'value' => $check['id_order_items']);
            }

            $ins = 0;
            $sql_update = $this->update_string($table, $data, $where);
            $status = $this->sql_query_effect($sql_update);
        } else {
            $sql = $this->insert_string($table, $data);
            $id_order_items = $this->sql_query_effect($sql, true);
            $status = 1;
            $ins = 1;
        }

        return array('id_order_items' => $id_order_items, 'ins' => $ins, 'status' => $status);
    }

    public function updateOrder($otable, $data) {
        
        $table = $this->prefix_order.$otable;
        $value = $data;
        $id_order = isset($data['id_orders']) ? $data['id_orders'] : '';
        $id_marketplace = isset($data['id_marketplace']) ? $data['id_marketplace'] : '';
        
        if($id_order == '' || $id_marketplace == ''){
            return false;
        }
        
        unset($value['id_orders']);
        unset($value['id_marketplace']);
                
        $where = array();
        $where['id_orders'] = array('operation' => '=', 'value' => $id_order);
        $where['id_marketplace'] = array('operation' => '=', 'value' => $id_marketplace);

        $sql_update = $this->update_string($table, $value, $where, false);
        $status = $this->sql_query_effect($sql_update);
        return ($status == 1) ? true : false;
    }

    public function getSqlUpdateStatusOrder($id_shop, $id_marketplace, $id_order, $comment) {
        $table = $this->prefix_order.'orders';

        $data = array(
            'status' => 0,
            'comment' => $comment
        );

        $where = array(
            'id_orders' => array('operation' => '=', 'value' => $id_order),
            'id_marketplace' => array('operation' => '=', 'value' => $id_marketplace),
            'id_shop' => array('operation' => '=', 'value' => $id_shop)
        );

        $sql = $this->update_string($table, $data, $where, false);
        return $sql;
    }

    public function getMiraklOrderAcceptList($sub_marketplace, $id_country, $id_shop, $start = null, $limit = null, $order_by = null, $search = null, $num_rows = false) {

        $sql = "SELECT * "
                ."FROM ::PREFIX::order_accept "
                ."WHERE sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop AND flag = 'Y' ";

        if (isset($order_by) && !empty($order_by)) {
            $sql.= "ORDER BY $order_by ";
        }

        if (isset($start) && isset($limit)) {
            $sql.= "LIMIT $start , $limit ";
        }

        if ($num_rows) {
            $num_row = $this->sql_num_rows($sql, $this->prefix_mirakl);
            return $num_row;
        }

        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function saveOrderAccept($data) {

        $ins = 1;
        $table = $this->prefix_mirakl.'order_accept';
        $table_detail = $this->prefix_mirakl.'order_accept_item';

        $check = array();
        $check['order_id'] = $data['order_id'];
        $check['sub_marketplace'] = $data['sub_marketplace'];
        $check['id_country'] = $data['id_country'];
        $check['id_shop'] = $data['id_shop'];

        $id_order_accept = $this->getIdOrdersAccept($table, $check);

        if (isset($id_order_accept) && !empty($id_order_accept)) {
            $where = array(
                'id_order_accept' => array('operation' => '=', 'value' => $id_order_accept)
            );

            $sql_update = $this->update_string($table, $data, $where);
            $status = $this->sql_query_effect($sql_update);
            $ins = 0;
        } else {

            $sql = $this->insert_string($table, $data);
            $id_order_accept = $this->sql_query_effect($sql, true);
            $status = 1;
            $ins = 1;
        }

        // save accept detail
        $this->saveOrderAcceptDetail($table_detail, $id_order_accept, $data['detail']);

        return $id_order_accept;
    }

    private function saveOrderAcceptDetail($table_detail, $id_order_accept, $details) {
        $ins = 1;
        $check = array();
        foreach ($details as $detail) {

            $check['id_order_accept'] = $id_order_accept;
            $check['order_line_index'] = $detail['order_line_index'];

            $id_order_accept_item = $this->getIdOrdersAcceptItem($table_detail, $check);

            // add id_order_accept in detail
            $detail['id_order_accept'] = $id_order_accept;

            if (isset($id_order_accept_item) && !empty($id_order_accept_item)) {
                $where = array(
                    'id_order_accept_item' => array('operation' => '=', 'value' => $id_order_accept_item)
                );

                $sql_update = $this->update_string($table_detail, $detail, $where);
                $status = $this->sql_query_effect($sql_update);
                $ins = 0;
            } else {
                $sql = $this->insert_string($table_detail, $detail);
                $id_order_accept_item = $this->sql_query_effect($sql, true);
                $status = 1;
                $ins = 1;
            }
        }
    }

    public function getOrderList($sub_marketplace, $id_country, $id_shop, $start = null, $limit = null, $order_by = null, $search = null, $status, $num_rows = false) {

        $sql = "SELECT O.*, OB.address_name, OS.tracking_number, OI.invoice_no   "
                ."FROM ::PREFIX::orders O "
                ."LEFT JOIN {$this->prefix_order}order_sub_marketplace SM ON O.id_orders = SM.id_orders "
                ."LEFT JOIN ::PREFIX::order_buyer OB ON O.id_orders = OB.id_orders "
                ."LEFT JOIN ::PREFIX::order_shipping OS ON O.id_orders = OS.id_orders "
                ."LEFT JOIN ::PREFIX::order_invoice OI ON O.id_orders = OI.id_orders  "
                ."WHERE O.id_shop = $id_shop AND O.id_marketplace = {$this->id_marketplace} AND O.site = '$id_country' AND SM.sub_marketplace = $sub_marketplace ";

        if (isset($search) && !empty($search)) {
            $sql .= "AND ( "
                    ."O.id_orders like '%".$search."%' OR "
                    ."O.id_marketplace_order_ref like '%".$search."%' OR "
                    ."OB.address_name like '%".$search."%' OR "
                    ."O.payment_method like '%".$search."%' OR "
                    ."O.total_paid like '%".$search."%' OR "
                    ."O.order_status like '%".$search."%' OR "
                    ."O.order_date like '%".$search."%' OR "
                    ."O.shipping_date like '%".$search."%' OR "
                    ."OI.invoice_no like '%".$search."%' OR "
                    ."OS.tracking_number like '%".$search."%' "
                    .") ";
        }

        if (isset($status) && !empty($status)) {
            if ($status == 'error') {
                $sql .= "AND (O.status = 'error' OR O.status IS NULL) ";
            } else {
                $sql .= "AND O.status = '$status' ";
            }
        }

        if (isset($order_by) && !empty($order_by)) {
            $sql .= "ORDER BY $order_by ";
        }

        if (isset($start) && isset($limit)) {
            $sql .= "LIMIT $limit OFFSET $start ";
        }

        $sql .= "; ";

        if ($num_rows) {
            $num_row = $this->sql_num_rows($sql, $this->prefix_order);
            return $num_row;
        }

        $results = $this->sql_query($sql, $this->prefix_order);
        return $results;
    }

    public function getOrderShipping($sub_marketplace, $id_country, $id_shop, $id_marketplace) {
        $orders = array();

        $sql = "SELECT O.id_orders, O.id_marketplace_order_ref, OS.tracking_number, OS.id_carrier, OS.shipment_service, C.name, MMC.id_mapping_carrier, MCC.code, MCC.label "
                ."FROM ::PREFIX::orders O "
                ."LEFT JOIN ::PREFIX::order_shipping OS ON O.id_orders = OS.id_orders "
                ."LEFT JOIN {$this->prefix_order}order_sub_marketplace SM ON O.id_orders = SM.id_orders AND O.id_shop = SM.id_shop AND SM.id_country = O.id_shop AND SM.sub_marketplace = $sub_marketplace "
                ."LEFT JOIN {$this->prefix_product}carrier C ON OS.id_carrier = C.id_carrier AND O.id_shop = C.id_shop "
                ."LEFT JOIN {$this->prefix_mirakl}mapping_carrier MMC ON OS.id_carrier = MMC.id_carrier "
                ."LEFT JOIN {$this->prefix_mirakl}config_carrier MCC ON MMC.id_config_carrier = MCC.id_config_carrier "
                ."LEFT JOIN {$this->prefix_mirakl}order_accept MOA ON O.id_marketplace_order_ref = MOA.order_id AND MOA.status NOT IN('CANCELED') AND MOA.sub_marketplace = $sub_marketplace AND O.site = MOA.id_country AND O.id_shop = MOA.id_shop  "
                ."WHERE O.id_shop = $id_shop AND O.id_marketplace = $id_marketplace AND O.site = $id_country "
                ."AND IFNULL(OS.tracking_number, '') != '' "
                ."AND (O.order_status = 'SHIPPING' OR O.order_status = 'SHIPPED' OR O.order_status = 'RECEIVED') "
                ."AND (O.shipping_status IS NULL OR O.shipping_status = '' OR IFNULL(MOA.tracking_no, '') = '' ) "
                ."AND IFNULL(O.id_marketplace_order_ref, '') != '' "
                ."ORDER BY O.id_orders ";

        $results = $this->sql_query($sql, $this->prefix_order);
        return $results;
    }

    public function getAcceptOrder($sub_marketplace, $id_country, $id_shop) {

        $sql = "SELECT * "
                ."FROM ::PREFIX::order_accept "
                ."WHERE IFNULL(tracking_no,'')  = '' AND id_shop = $id_shop AND sub_marketplace = $sub_marketplace AND id_country = $id_country AND order_state != 'CANCELED'  ";

        $results = $this->sql_query($sql, $this->prefix_mirakl);
        return $results;
    }

    public function updateOrderAcceptStatus($sub_marketplace, $id_country, $id_shop, $condition = array()) {

        $flag = '';
        $id_order_accept = $condition['id_order_accept'];
        $order_id = $condition['order_id'];
        $order_state = $condition['order_state'];
        $tracking_no = $condition['tracking_no'];
        $order_line_state = $condition['line_state'];

        $sql = "UPDATE {$this->prefix_mirakl}order_accept "
                ."SET order_state = '$order_state', tracking_no = '$tracking_no' "
                ."WHERE order_id = '$order_id' AND sub_marketplace = $sub_marketplace AND id_country = $id_country AND id_shop = $id_shop ; ";

        $flag = $this->sql_query_exec($sql);

        if ($flag && !empty($order_line_state)) {
            foreach ($order_line_state as $index => $state) {

                $sql_line = "UPDATE {$this->prefix_mirakl}order_accept_item "
                        ."SET order_line_state = '$state' "
                        ."WHERE id_order_accept = $id_order_accept AND order_line_index = $index ; ";

                $flag = $this->sql_query_exec($sql_line);
            }
        }
        return $flag;
    }

    public function updateOrderStatusShipping($id_marketplace_order_ref, $id_shop, $id_marketplace, $id_country) {

        if (!isset($id_marketplace_order_ref) || !isset($id_shop) || !isset($id_marketplace) || !isset($id_country)) {
            return false;
        }

        $sql = "UPDATE {$this->prefix_order}orders "
                ."SET shipping_status = 1 "
                ."WHERE id_marketplace_order_ref = '$id_marketplace_order_ref' AND id_shop = $id_shop AND site = $id_country AND id_marketplace = $id_marketplace ; ";

        $result = $this->sql_query_exec($sql);
        return $result;
    }

    public function getOrderBuyerNameEmail($order_id, $sub_marketplace, $id_country) { // get email to message
        $sql = "SELECT B.name, B.email "
                ."FROM ::PREFIX::order_buyer B "
                ."INNER JOIN ::PREFIX::orders O ON B.id_orders = O.id_orders "
                ."LEFT JOIN {$this->prefix_order}order_sub_marketplace OS ON B.id_orders = OS.id_orders AND OS.sub_marketplace = $sub_marketplace AND OS.id_country = $id_country "
                ."WHERE O.id_marketplace = $this->id_marketplace AND O.id_marketplace_order_ref = '$order_id'  ";

        $results = $this->sql_query($sql, $this->prefix_order, true);
        return $results;
    }

    public function updateFlagOrderAccept($id_order_accept) {
        $flag = '';
        $sql = "UPDATE {$this->prefix_mirakl}order_accept "
                ."SET flag = 'Y' "
                ."WHERE id_order_accept = $id_order_accept ";

        $flag = $this->sql_query_exec($sql);
        return $flag;
    }

}

// end definition