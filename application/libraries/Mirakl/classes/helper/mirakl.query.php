<?php

class MiraklHelperQuery
{

	public static function page_max($value = 10)
	{
		$value = intval($value);
		if ($value > 100) $value = 100;
		else if ($value < 1) $value = 10;

		return $value;
	}

	public static function page_offset($value = 0)
	{
		$value = intval($value);
		if ($value < 0) $value = 0;

		return $value;
	}

	public static function order($value = 'asc')
	{
		$valid_orders = array('asc', 'desc');
		$value = trim(strtolower($value));
		if (!in_array($value, $valid_orders)) return $valid_orders[0];
		else return $value;
	}

	public static function params($service = '', $key = '', $value = '')
	{
		$api = array();
		if ($key != 'sort') $value = strtoupper($value);

		//additional_fields
		$api['additional_fields']['entities'] = array('SHOP', 'OFFER', 'ORDER_LINE');

		//invoices
		$api['invoices']['sort'] = array('dateCreated');

		//messages
		$api['messages']['sort'] = array('dateCreated');
		$api['messages']['archived'] = array('FALSE', 'ALL', 'TRUE');
		$api['messages']['received'] = array('FALSE', 'ALL', 'TRUE');
		$api['messages']['visible'] = array('FALSE', 'ALL', 'TRUE');

		//offers
		$api['offers/imports']['import_mode'] = array('NORMAL', 'PARTIAL_UPDATE', 'REPLACE');
		$api['offers/messages']['archived'] = array('FALSE', 'ALL', 'TRUE');
		$api['offers/messages']['received'] = array('FALSE', 'ALL', 'TRUE');
		$api['offers/messages']['visible'] = array('FALSE', 'ALL', 'TRUE');
		$api['offers']['sort'] = array('totalPrice', 'price', 'productTitle');
		$api['offers/messages']['sort'] = array('dateCreated');
		$api['offers/states']['sort'] = array('sortIndex');

		//orders
		$api['orders']['sort'] = array('dateCreated');
		$api['orders/messages']['sort'] = array('dateCreated');
		$api['orders/messages']['archived'] = array('FALSE', 'TRUE', 'ALL');
		$api['orders/messages']['received'] = array('ALL', 'FALSE', 'TRUE');

		//products
		$api['products']['sort'] = array('productSku');
		$api['products/offers']['premium'] = array('ALL', 'FALSE', 'TRUE');
		$api['products/offers']['all_offers'] = array('FALSE', 'TRUE');
		$api['products/offers']['all_channels'] = array('FALSE', 'TRUE');
		$api['products/offers']['sort'] = array('bestPrice', 'bestEvaluation');

		//reasons
		$api['reasons/incident_open']['sort'] = array('sortIndex');
		$api['reasons/incident_close']['sort'] = array('sortIndex');
		$api['reasons/refund']['sort'] = array('sortIndex');
		$api['reasons/messaging']['sort'] = array('sortIndex');

		//shipping
		$api['shipping/carriers']['sort'] = array('sortIndex');

		if (isset($api[$service][$key]))
		{
			if (!in_array($value, $api[$service][$key]))
			{
				return $api[$service][$key][0];
			}
			else
			{
				return $value;
			}
		}
		else
		{
			return '';
		}
	}
}
 
