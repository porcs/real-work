<?php

require_once dirname(__FILE__).'/helper/mirakl.array2xml.php';
require_once dirname(__FILE__).'/helper/mirakl.xml2array.php';
require_once dirname(__FILE__).'/helper/mirakl.query.php';
require_once dirname(__FILE__).'/helper/mirakl.errors.php';

class MiraklWebservice
{	
	protected $config = array();
	protected $service_url = null;
	protected $service_method = null;
	protected $service = null;
	protected $service_code = null;
	protected $service_child = null;
	protected $errors = null;
	protected $valid_http_responses = array('200', '201', '204');

	//taken from config class;
	protected $error_debug = false;
	protected $api_key = '';

	/**
	 * Initializes global values used in Mirakl Module
	 */
	public function __construct($marketplace_params)
	{
		$this->api_key = $marketplace_params['api_key'];
		//$this->endpoint = isset($marketplace_params['endpoint_'.$marketplace]) ? $marketplace_params['endpoint_'.$marketplace] : '';
                $this->endpoint = isset($marketplace_params['endpoint']) ? $marketplace_params['endpoint'] : '';
		$this->error_debug = isset($marketplace_params['debug']) && $marketplace_params['debug'] ? true : false;
                
		$this->config['output'] = 'xml';
		//$this->config['if_xml_output_raw'] = true;
	}


	public function get($filename = '', $request_type = 'json')
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/libraries/cacert.pem');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json'));

		if ($request_type == 'xml')
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', 'Accept: application/xml'));
		}
		else if ($request_type == 'json')
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json'));
		}

		if (!empty($filename))
		{
			$fp = fopen($filename, 'w');
			curl_setopt($ch, CURLOPT_FILE, $fp);
		}
		curl_setopt($ch, CURLOPT_URL, $this->service_url);
		$data = curl_exec($ch);
                
                
        
		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if ($this->error_debug)
		{
			echo "<pre>";
			echo $this->service_url;
			echo nl2br(print_r(curl_getinfo($ch), true));
			echo "</pre>";

			echo "<pre>";
			echo $data;
			echo "</pre>";
		}

		//display errors
		$cErr = curl_errno($ch);
		if ($cErr != '')
		{
			$err = 'cURL ERROR: '.curl_errno($ch).': '.$cErr.'<br>';
			foreach (curl_getinfo($ch) as $k => $v)
			{
				$err .= "$k: $v<br>";
			}
			curl_close($ch);
			if (isset($fp))
				fclose($fp);

			return $this->errors($http_status, $err, $parse = false);
		}
		curl_close($ch);
		if (isset($fp))
			fclose($fp);

		if (!in_array($http_status, $this->valid_http_responses))
		{
			return $this->errors($http_status, $data, $parse = false);
		}
		else
		{
			if (strlen($data) < 2)
			{
				return $this->errors($http_status, 'REQUEST_SUCCESS');
			}
		}
                
		return $data;
	}

// end func

	public function put($request_type = 'json', $params = array())
	{
		return $this->_post_put('put', $request_type, $params);
	}

// end func	

	public function post($request_type = 'json', $params = array())
	{
		return $this->_post_put('post', $request_type, $params);
	}

// end func

	protected function parse($json)
	{
		if (is_array($json))
			$json = json_encode($json);
		$response_type = $this->_response_type($json);
		if ($response_type == 'invalid')
		{
			return $json;
		}

		/*
		if ($response_type == 'xml')
		{
			if ($this->config['if_xml_output_raw'])
			{
				return $json;
			}
			$array = MiraklHelperXML2Array::createArray($json);
			$json = json_encode($array);
		}
		*/

		if (!isset($this->config['output']) || $this->config['output'] == 'json')
		{
			return $json;
		}
		else if ($this->config['output'] == 'array')
		{
			return $this->json2array($json);
		}
		else if ($this->config['output'] == 'xml')
		{
			$array = $this->json2array($json);
			$xml = MiraklHelperArray2XML::createXML('mirakl_'.strtolower(str_replace('/', '_', $this->service)), $array);

			return $xml->saveXML();
		}
		else
		{
			return $json;
		}
	}

// end func

	protected function json2array($json, $assoc = true)
	{
		return json_decode($json, $assoc);
	}

// end func 

	protected function errors($code = 0, $reason = '', $parse = true)
	{
		$info = array();
		$info['error_code'] = $code;
		$info['service'] = ucwords($this->service);
		$info['service_child'] = ucwords($this->service_child);
		$info['service_code'] = $this->service_code;
		$info['service_method'] = $this->service_method;
		$info['error'] = $reason;
                
		$errors = MiraklHelperErrors::explain($info);
		if ($code != 200)
			$this->service = 'Errors';

		return ($parse) ? $this->parse($errors) : $errors;
	}

// end func 

	private function _post_put($type = 'post', $request_type = 'json', $params = array())
	{

		if ($request_type == 'csv')
		{
			$headers = array('Accept: application/json', 'Content-Type: multipart/form-data');
			//$params['type']='text/csv';
		}
		else if ($request_type == 'json')
		{
			$headers = array('Accept: application/json', 'Content-Type: application/json');
			$params = json_encode($params);
		}
                
                //echo '<pre>'; print_r($params); exit;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->service_url);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_VERBOSE, 1); // TRUE to output verbose information
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/libraries/cacert.pem');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		//$f = fopen(dirname(__FILE__).'/request.txt', 'w');
		//curl_setopt($ch, CURLOPT_STDERR, $f);

		if ($type == 'put')
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		else
			curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

		if ($this->error_debug)
		{
			printf('%s(#%d): Webservice url: %s query: %s', basename(__FILE__), __LINE__, $this->service_url, nl2br(print_r($params, true)));
		}

		$data = curl_exec($ch);
                //print_r($data); exit;
                //print_r($data); exit;
//		$http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//
//		//display errors
//		$cErr = curl_errno($ch);
//		if ($cErr != '')
//		{
//			$err = 'cURL ERROR: '.$cErr.': '.curl_error($ch)."\r\n";
//			foreach (curl_getinfo($ch) as $k => $v)
//			{
//				if (is_array($v))
//					$err .= "$k: ".print_r($v, true)."\r\n";
//				else
//					$err .= "$k: $v"."\r\n";
//			}
//			curl_close($ch);
//
//			return $this->errors($http_status, $err, false);
//		}
//
//		curl_close($ch);
//		//fclose($f);
//		if (!in_array($http_status, $this->valid_http_responses))
//		{
//			return $this->errors($http_status, $data, false);
//		}
		return $data;
	}

// end func

	private function _response_type($data = '')
	{
		$data = trim($data);
		if (isset($data[0]) && $data[0] == '<')
		{
			return 'xml';
		}
		else if (isset($data[0]) && ($data[0] == '{' || $data[0] == '['))
		{
			return 'json';
		}
		else
		{
			return 'invalid';
		}
	}

// end func
}
