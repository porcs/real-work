<?php

require_once dirname(__FILE__).'/mirakl.webservice.class.php';

class MiraklApiAccount extends MiraklWebservice {

    public function __construct($marketplace_params) {
        parent::__construct($marketplace_params);
        $this->service = "account";
    }

// end func

    public function account() {
        /*         * *****************************************************************************************
         * Descripton: Get shop information
         * ***************************************************************************************** */
        $this->service_method = 'GET';
        $this->service_child = null;
        $this->service_code = 'A01';
        $this->service_url = $this->endpoint.$this->service.'/?api_key='.$this->api_key;
        return $this->get(null, 'xml');
    }

// end func
}

// end class definition