<?php

require_once dirname(__FILE__).'/../../FeedBiz.php';
require_once dirname(__FILE__).'/../classes/mirakl.webservice.class.php';
require_once dirname(__FILE__).'/../classes/mirakl.database.php';
require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';
require_once dirname(__FILE__).'/../classes/mirakl.api.orders.php';
require_once dirname(__FILE__).'/../../../../assets/apps/FeedbizImport/log/RecordProcess.php';
require_once dirname(__FILE__).'/../classes/mirakl.tools.php';
require_once dirname(__FILE__).'../../../FeedBiz/products/Product.php';
require_once dirname(__FILE__).'/../classes/mirakl.order.php';
require_once BASEPATH.'/../libraries/Hooks.php';

class MiraklOrderImport {

    private $debug;
    private $cron;
    private $get_config;
    private $batch_id;
    private $date_time;
    private $id_marketplace;
    private $process;
    private $success;
    private $mirakl_database;
    private $mirakl_order;
    private $proc_rep;
    private $message;
    private $skipped = array(); // Format skipped[] = ([0] = refercnce_type, [1] = reference, [2] = log_key, [3] = log_message, [4] = log_message)

    public function __construct($get_config, $debug = false, $cron = false) {
        $this->debug = false;
        $this->cron = $cron;
        $this->get_config = $get_config;
        $this->batch_id = uniqid();
        $this->date_time = date('Y-m-d H:i:s');
        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;

        $this->process = 0;
        $this->success = 0;

        Mirakl_Tools::load($this->get_config->language);
        $this->mirakl_database = new MiraklDatabase($this->get_config->user_name);
        $this->mirakl_order = new MiraklOrder($this->get_config->user_name);

        if (!$this->debug) {
            $this->startPopup();
        }
    }

    private function startPopup() {
        $popup_data = array();
        $popup_data['action'] = MiraklHistoryLog::ACTION_TYPE_IMPORT_ORDER;
        $popup_data['batch_id'] = $this->batch_id;
        $popup_data['date_time'] = $this->date_time;
        $popup_data['user_name'] = $this->get_config->user_name;
        $popup_data['ext'] = $this->get_config->ext;
        $popup_data['country'] = $this->get_config->countries;
        $popup_data['marketplace'] = $this->get_config->marketplace;

        $get_popup = MiraklHistoryLog::startPopup($popup_data);
        $this->proc_rep = $get_popup['proc_rep'];
        $this->message = $get_popup['message'];
    }

    private function endTask($status, $message, $set_message = false, $error = true, $optional = array()) {
        $log_date_time = date('Y-m-d H:i:s');

        if ($status == MiraklParameter::STATUS_ERROR) {
            if (isset($optional['log_key']) && isset($optional['log_value'])) {
                $this->updateLog(MiraklParameter::LOG_STATUS_ERROR, $optional['log_key'], $optional['log_value']);
            } else {
                $this->updateLog(MiraklParameter::LOG_STATUS_ERROR, $message);
            }
        } else {
            $this->updateLog(MiraklParameter::LOG_STATUS_SUCCESS);
        }

        if (isset($this->proc_rep) && $set_message) {

            if ($error) { // case error! into this.
                $this->message->no_product = $message;
            } else {
                $this->message->orders->status = 'Success';
                $this->message->orders->message = $message;
            }

            $this->proc_rep->set_status_msg($this->message);
        }

        if (isset($this->proc_rep)) {
            $this->proc_rep->finish_task();
        }

        $return_value = array(
            'status' => $status,
            'message' => $message
        );

        return $return_value;
    }

    private function lang($str) {
        return Mirakl_Tools::l($str);
    }

    private function status($flag) {
        return ($flag == 'E') ? MiraklParameter::STATUS_ERROR : MiraklParameter::STATUS_SUCCESS;
    }

    public function importOrder($params = array()) {

        if (empty($params)) {
            return $this->endTask($this->status('E'), $this->lang('No parameters in this order'), true);
        }

        // set init params
        $date_start = date('Y-m-d', strtotime($params['date_start']));
        $import_status = !empty($params['import_status']) ? $params['import_status'] : MiraklParameter::ORDER_IMPORT_STATUS_IMPORT;

        $result = array();
        $total_count = 0;

        if (isset($this->proc_rep)) {
            $this->message->orders->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        $mirakl_params = MiraklConfig::marketplaceParams($this->get_config->marketplace);
        $mirakl_params['debug'] = $this->debug;
        $mirakl_params['api_key'] = $this->get_config->api_key;

        $mirakl = new MiraklApiOrders($mirakl_params);

        if ($import_status == MiraklParameter::ORDER_IMPORT_STATUS_ALL) {
            $params = array(
                'order_state_codes' => null,
                'start_date' => $date_start
            );
        } else {
            $params = array(
                'order_state_codes' => MiraklApiOrders::STATUS_SHIPPING.','.MiraklApiOrders::STATUS_SHIPPED.','.MiraklApiOrders::STATUS_RECEIVED,
                'start_date' => $date_start
            );
        }

        $this->mirakl_database->log_message($params, 'params_import_order');
        //$orders = file_get_contents('https://mp.thebeautyst.com/api/orders/?api_key=4284ed2c-cf0b-4b90-8794-6aa8cdacae37&order_state_codes=SHIPPING,SHIPPED,RECEIVED'); // for debug
        $response = $mirakl->orders($params);

        if (empty($response)) {
            return $this->endTask($this->status('E'), $this->lang('No response from web services'), true);
        }

        $xml = simplexml_load_string($response);

        if (isset($xml->error_code)) {
            return $this->endTask($this->status('E'), $this->lang('Web services has an error'), true);
        }

        $result = $xml->xpath('//mirakl_orders/orders');
        $xcount = $xml->xpath('//mirakl_orders/total_count');

        if (!empty($xcount)) {
            $total_count = (int) $xcount[0];
        } else {
            return $this->endTask($this->status('E'), $this->lang('Web services has an error'), true);
        }

        if ($total_count == 0) {
            return $this->endTask($this->status('E'), $this->lang('No new an order to import'), true);
        }

        //echo '<pre>'; print_r($result); exit;
        return $this->startImportOrder($result);
    }

    private function startImportOrder($orders) {
        $total_quantity = $total_shipping = $total_amount = $total_tax_rate = $total_discount = $total_shipping_tax = 0;
        $gift_amount = 0;
        $sql = $gift_message = $sql_status = '';
        $hook = new Hooks();

        $carriers = $this->mirakl_database->getMiraklConfigCarrier($this->get_config->sub_marketplace, $this->get_config->id_country, $this->get_config->id_shop);



        if (empty($carriers)) {
            return $this->endTask($this->status('E'), $this->lang('missing carrier'), true);
        }

        foreach ($orders as $order) {

            $this->process = $this->process + 1;

            if (!isset($order->order_id) || empty($order->order_id)) {
                $this->skipped[] = array('order', 'n/a', 'missing order id', '', $this->lang('missing order id'));
                continue;
            }

            if (!isset($order->commercial_id) || empty($order->commercial_id)) {
                $this->skipped[] = array('order', 'n/a', 'missing commercial id', '', $this->lang('missing commercial id'));
                continue;
            }

            $order_id = (string) $order->order_id;
            $commercial_id = (string) $order->commercial_id;

            if (!isset($order->customer) || empty($order->customer->customer_id)) {
                $this->skipped[] = array('order', $commercial_id, 'missing customer id', '', $this->lang('missing customer id'));
                continue;
            }

            if (!isset($order->order_lines) || empty($order->order_lines)) {
                $this->skipped[] = array('order', $commercial_id, 'missing product', '', $this->lang('missing product'));
                continue;
            }

            $today_date = date("Y-m-d H:i:s");
            $order_date = date('Y-m-d H:i:s', strtotime($order->created_date));
            $shipping_date = !empty($order->order_lines->shipped_date) ? date('Y-m-d H:i:s', strtotime($order->order_lines->shipped_date)) : '';
            $customer_firstname = ucwords(Mirakl_Tools::strtolower($order->customer->firstname));
            $customer_lastname = ucwords(Mirakl_Tools::strtolower($order->customer->lastname));
            $customer_email = sprintf('%s-%s@%s', $order->customer->customer_id, Mirakl_Tools::substr(Mirakl_Tools::toKey(strtolower($this->get_config->marketplace)), 0, 16), MiraklParameter::TRASH_DOMAIN);
            $customer_phone = preg_replace('/[^0-9]/', '', isset($order->customer->shipping_address->phone) ? $order->customer->shipping_address->phone : (isset($order->customer->billing_address->phone) ? $order->customer->billing_address->phone : null));
            $sales_channel = isset($this->get_config->marketplace) ? $this->get_config->marketplace : ''; //self::Mirakl;
            $total_paid = isset($order->total_price) && ($order->total_price > 0) ? (float) $order->total_price : 0;
            $total_commission = isset($order->total_commission) && ($order->total_commission > 0) ? (float) $order->total_commission : 0;
            $id_currency = (int) $this->mirakl_database->getSettingCurrencyIdByIsoCode($order->currency_iso_code, $this->get_config->id_shop);
            $order_state = isset($order->order_state) ? (string) $order->order_state : '';

            // condition
            $condition = array();
            $condition['id_shop'] = $this->get_config->id_shop;
            $condition['id_country'] = $this->get_config->id_country;
            $condition['sub_marketplace'] = $this->get_config->sub_marketplace;
            $condition['id_marketplace'] = $this->id_marketplace;
            $condition['date_add'] = $today_date;

            $status_order = $this->mirakl_order->checkOrderStatus($order_id, $condition); // check order is status = 1 has invoice

            if ($status_order > 0) {
                $this->skipped[] = array('order', $commercial_id, 'an order had processed', '', $this->lang('an order had processed'));
                continue;
            }

            // Customer individual account
            if (!isset($customer_email) || !Mirakl_Tools::isEmail($customer_email)) {
                $this->skipped[] = array('order', $commercial_id, 'invalid email address %s', $customer_email, sprintf($this->lang('invalid email address %s'), $customer_email));
                continue;
            }

            //Save Order//    
            $sorder = array();
            $sorder['id_marketplace_order_ref'] = $order_id;
            $sorder['sales_channel'] = $sales_channel;
            $sorder['id_shop'] = (int) $this->get_config->id_shop;
            $sorder['site'] = $this->get_config->id_country;
            $sorder['id_marketplace'] = $this->id_marketplace;
            $sorder['id_lang'] = (int) $this->get_config->id_lang;
            $sorder['id_region'] = $this->get_config->id_region;
            $sorder['id_currency'] = isset($order->currency_iso_code) ? (string) $order->currency_iso_code : null;
            $sorder['order_type'] = '';
            $sorder['order_status'] = $order_state;
            $sorder['payment_method'] = (string) $order->paymentType;
            $sorder['purchase_date'] = $order_date;
            $sorder['order_date'] = $order_date;
            $sorder['shipping_date'] = $shipping_date;
            $sorder['total_paid'] = $total_paid;
            $sorder['commission'] = $total_commission;
            $sorder['date_add'] = $today_date;
            $sorder['marketplace_order_number'] = $commercial_id;

            if ($order_state == MiraklApiOrders::STATUS_SHIPPED || $order_state == MiraklApiOrders::STATUS_RECEIVED) {
                $sorder['shipping_status'] = true;
            }

            $flag_order = $this->mirakl_order->saveOrder('orders', $sorder, $condition);

            if (empty($flag_order['id_order']) || $flag_order['status'] == 0) {
                $this->skipped[] = array('order', $commercial_id, 'unable to save order', '', $this->lang('unable to save order'));
                continue;
            }

            $id_order = $flag_order['id_order'];

            // Begin Line
            foreach ($order->order_lines as $key => $order_lines) {

                $offer_sku = (string) $order_lines->offer_sku;
                $offer_id = (string) $order_lines->offer_id;
                $order_line_state = (string) $order_lines->order_line_state;

                switch ($order_line_state) {
                    case MiraklApiOrders::STATUS_SHIPPING:
                    case MiraklApiOrders::STATUS_SHIPPED:
                    case MiraklApiOrders::STATUS_RECEIVED:
                        break;
                    default:
                        $this->skipped[] = array('order', $commercial_id, 'product sku %s status can not import', $offer_sku, sprintf($this->lang('product sku %s status can not import'), $offer_sku));
                        continue;
                }

                $option = array(
                    'user_name' => $this->get_config->user_name,
                    'id_lang' => $this->get_config->id_lang,
                    'id_mode' => 1
                );

                // Product
                $product = $this->mirakl_database->getProductBySkuMirakl($this->get_config->id_shop, $offer_sku, $option);

                if (empty($product)) {
                    $this->skipped[] = array('order', $commercial_id, 'product %s missing offer sku', $offer_sku, sprintf($this->lang('product %s missing offer sku'), $offer_sku));
                    $comment = "Could not import product $offer_sku";
                    $sql_status.= $this->mirakl_order->getSqlUpdateStatusOrder($this->get_config->id_shop, $this->id_marketplace, $id_order, $comment);
                    continue;
                }

                $order_item['id_orders'] = $id_order;
                $order_item['id_marketplace_order_item_ref'] = $offer_id; // id_item
                $order_item['id_shop'] = $this->get_config->id_shop;
                $order_item['id_product'] = $product->id_product;
                $order_item['reference'] = $offer_sku;
                $order_item['id_marketplace_product'] = (string) $order_lines->product_sku;
                $order_item['id_product_attribute'] = isset($product->id_product_attribute) ? $product->id_product_attribute : null;
                $order_item['product_name'] = (string) $order_lines->product_title;
                $order_item['quantity'] = (int) $order_lines->quantity;
                $total_quantity = $total_quantity + (int) $order_item['quantity'];

                // Product quantity
                if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute])) {
                    $quantity_in_stock = $product->combination[$product->id_product_attribute]['quantity'];
                } else {
                    $quantity_in_stock = $product->quantity;
                }

                $order_item['quantity_in_stock'] = $quantity_in_stock;

                // Product weight
                if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['weight']['value'])) {
                    $product_weight = $product->combination[$product->id_product_attribute]['weight']['value'];
                } else {
                    $product_weight = isset($product->weight['value']) ? $product->weight['value'] : null;
                }

                $order_item['product_weight'] = $product_weight;

                // Product price
                if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['price'])) {
                    $product_price = $product->combination[$product->id_product_attribute]['price'];
                } else {
                    $product_price = $product->price;
                }

                $order_item['product_price'] = $product_price;

                // Shipping 
                $order_item['shipping_price'] = (float) $order_lines->shipping_price;
                $total_shipping = $total_shipping + (float) $order_item['shipping_price'];

                // Tax
                $order_item['tax_rate'] = 0;
                $total_tax_rate = $total_tax_rate + (float) $order_item['tax_rate'];
                $total_shipping_tax = $total_shipping_tax + (float) (isset($order_lines->TaxesInformation->ShippingTaxAmount) ? $order_lines->TaxesInformation->ShippingTaxAmount : 0);

                // Before VAT
                $order_item['unit_price_tax_excl'] = ((float) $order_lines->total_price != 0 || (int) $order_item['quantity'] != 0) ? ( (float) $order_lines->total_price / (int) $order_item['quantity'] ) : 0;
                $order_item['total_price_tax_excl'] = (float) $order_lines->total_price;
                $order_item['total_shipping_price_tax_excl'] = (float) $order_lines->shipping_price - $order_item['tax_rate'];

                // After VAT
                $order_item['unit_price_tax_incl'] = (float) $order_lines->price_unit; //( $order_item['unit_price_tax_excl'] + $order_item['tax_rate'] );
                $order_item['total_price_tax_incl'] = (float) $order_lines->price_unit + $order_item['tax_rate']; //( $order_item['total_price_tax_excl'] + $order_item['tax_rate'] );
                $order_item['total_shipping_price_tax_incl'] = (float) $order_lines->shipping_price;

                // Condition
                $order_item['id_condition'] = $product->id_condition;

                // Promotion
                $order_item['promotion'] = isset($order_lines->PromotionId) ? $order_lines->PromotionId : '';

                //Gift Message
                $giftMessage = isset($order_lines->GiftMessageText) ? $order_lines->GiftMessageText : '';
                $order_item['message'] = $giftMessage;
                $message = $giftMessage;
                $gift_message = $giftMessage;
                $gift_amount = $gift_amount + (float) (isset($order_lines->GiftWrapPrice) ? $order_lines->GiftWrapPrice : 0);

                // Discount
                if (isset($order_lines->ShippingDiscount) && $order_lines->ShippingDiscount > 0) {
                    $total_discount = ( $total_discount + (float) $order->ShippingDiscount );
                }

                if (isset($order_lines->PromotionDiscount) && $order_lines->PromotionDiscount > 0) {
                    $total_discount = ( $total_discount + (float) $order->PromotionDiscount );
                }

                // Total Amount
                $total_amount = (float) $total_amount + (float) $order_lines->price_unit;

                // insert or update into order item.
                $flag_order_items = $this->mirakl_order->saveOrderLine('order_items', $order_item);

                if (empty($flag_order_items['id_order_items']) || $flag_order_items['status'] == 0) {
                    $this->skipped[] = array('order', $commercial_id, 'unable to save order item', '', $this->lang('unable to save order item'));
                }

                $id_order_items = $flag_order_items['id_order_items'];

                /////////////////////////////////////////////////////////////////////////////////////////

                if (isset($product->id_product_attribute) && isset($product->combination[$product->id_product_attribute]['attributes'])) {
                    foreach ($product->combination[$product->id_product_attribute]['attributes'] as $attr_key => $attr_value) {
                        $order_item_attr['id_order_items'] = $id_order_items;
                        $order_item_attr['id_attribute_group'] = $attr_key;

                        foreach ($product->combination[$product->id_product_attribute]['attributes'][$attr_key]['value'] as $id_attribute => $attr_val) {
                            $order_item_attr['id_attribute'] = $id_attribute;
                        }
                        $order_item_attr['id_shop'] = (int) $this->get_config->id_shop;
                        $arrt_exists = $this->mirakl_order->checkOrderProductAttribute($order_item_attr);

                        if (!$arrt_exists) {
                            $sql .= $this->mirakl_order->insert_string_order('order_items_attribute', $order_item_attr);
                        }
                    }
                }

                // payment
                $payment = array();
                $payment['id_orders'] = $id_order;
                $payment['id_order_items'] = $id_order_items;
                $payment['payment_method'] = isset($order->paymentType) ? (string) $order->paymentType : null;
                $payment['payment_status'] = (string) $order->payment_workflow; //$order_lines->payment_workflow; //(isset($order->order_state) ? $order->order_state : null);
                $payment['id_currency'] = $id_currency; //$this->id_currency;
                $payment['Amount'] = (float) $order_lines->total_price;
                $this->mirakl_order->saveOrderLine('order_payment', $payment);
            } // End loop line
            // Buyer
            $buyer = array();
            $buyer['id_orders'] = $id_order;
            $buyer['site'] = $this->get_config->id_country; //$site;
            $buyer['id_buyer_ref'] = $order_id; //(string) $order->commercial_id;
            $buyer['email'] = $customer_email; //(string) $order->BuyerEmail;
            $buyer['name'] = $customer_firstname.' '.$customer_lastname;
            $buyer['id_address_ref'] = (string) $order->customer->customer_id;
            $buyer['address_name'] = $customer_firstname.' '.$customer_lastname;
            $buyer['address1'] = (string) $order->customer->shipping_address->street_1;
            $buyer['address2'] = !empty($order->customer->shipping_address->street_2) ? $order->customer->shipping_address->street_2 : null;
            $buyer['city'] = (string) $order->customer->shipping_address->city;
            $buyer['state_region'] = ''; // NJ (string) $order->Address->StateOrRegion;
            $buyer['postal_code'] = (string) $order->customer->shipping_address->zip_code;
            $buyer['country_code'] = isset($order->shipping_zone_code) ? (string) $order->shipping_zone_code : '';
            $buyer['country_name'] = isset($order->shipping_zone_label) ? (string) $order->shipping_zone_label : ''; //(string) $order->customer->shipping_address->country;
            $buyer['phone'] = $customer_phone;
            $this->mirakl_order->saveOrder('order_buyer', $buyer);

            $shipping_carrier_code = '';
            if (isset($order->shipping_carrier_code) && !empty($order->shipping_carrier_code)) {
                $shipping_carrier_code = (string) $order->shipping_carrier_code;
            }

            $id_carrier = '';
            if (!empty($shipping_carrier_code)) {
                foreach ($carriers as $carrier) {
                    /* carrier['id_config_carrier'] = 0 is default, go to skip */
                    if ($carrier['id_config_carrier'] != 0 && $carrier['code'] == $shipping_carrier_code) {
                        $id_carrier = $carrier['id_carrier'];
                        break;
                    }
                }
            }

            if (empty($id_carrier)) {
                $id_carrier = $carriers[0]['id_carrier']; // is default
            }

            if (empty($id_carrier)) {
                $this->skipped[] = array('order', $commercial_id, 'missing mapping carrier', '', $this->lang('missing mapping carrier'));
                continue;
            }

            //Shipping        
            $shipping = array();
            $shipping['id_orders'] = $id_order;
            $shipping['shipment_service'] = (string) (!empty($order->shipping_company) ? $order->shipping_company : '');
            $shipping['weight'] = 0;
            $shipping['tracking_number'] = (string) (!empty($order->shipping_tracking) ? $order->shipping_tracking : '');
            $shipping['shipping_services_cost'] = (string) (!empty($order->shipping_price) ? $order->shipping_price : '');
            $shipping['shipping_services_level'] = '';
            $shipping['id_carrier'] = $id_carrier;
            $this->mirakl_order->saveOrder('order_shipping', $shipping);

            // Invoice
            $invoice = array();
            $invoice['id_orders'] = $id_order;
            $invoice['total_discount_tax_incl'] = $total_discount;
            $invoice['total_paid_tax_incl'] = $total_paid;
            $invoice['total_products'] = $total_quantity;
            $invoice['total_shipping_tax_incl'] = (float) $order->shipping_price; //$total_shipping;
            $invoice['date_add'] = $today_date;
            $this->mirakl_order->saveOrder('order_invoice', $invoice);

            // Taxes
            $taxes['id_orders'] = $id_order;
            $taxes['tax_amount'] = $total_tax_rate;
            $taxes['tax_on_shipping_amount'] = $total_shipping_tax;
            $this->mirakl_order->saveOrder('order_taxes', $taxes);

            // Update Order
            $uorder = array();
            $uorder['id_orders'] = $id_order;
            $uorder['id_marketplace'] = $this->id_marketplace;
            $uorder['total_shipping'] = $total_shipping;
            $uorder['total_discount'] = $total_discount;
            $uorder['total_amount'] = $total_amount;
            $uorder['gift_message'] = $gift_message;
            $uorder['gift_amount'] = $gift_amount;
            $uorder['comment'] = '';

            if (!empty($sorder['shipping_date']) && !empty($shipping['tracking_number'])) {
                $uorder['status'] = 1;
            }

            $flag_update_order = $this->mirakl_order->updateOrder('orders', $uorder);

            if (!$flag_update_order) {
                // error should test again!

                $this->skipped[] = array('order', $commercial_id, 'unable to update order', '', $this->lang('unable to update order'));

                $eorder = array();
                $eorder['id_orders'] = $id_order;
                $eorder['id_marketplace'] = $this->id_marketplace;
                $eorder['status'] = 'error';
                $eorder['comment'] = 'unable to update order';
                $this->mirakl_order->updateOrder('orders', $eorder);
            } else {

                if (!empty($id_order)) {
                    $hook->_call_hook('save_orders', array(
                        'user_name' => $this->get_config->user_name,
                        'id_shop' => $this->get_config->id_shop,
                        'site' => $this->get_config->id_country,
                        'id_order' => $id_order
                    ));
                }

                $this->success = $this->success + 1;
                $id_order = '';
            }
        } // end foreach orders ------------------------------------------------------------------------------------------------------

        //echo '<pre>'; print_r($this->skipped); exit;
        
        if (!empty($sql_status) || $sql_status != '') {
            $this->mirakl_database->exec_query($sql_status);
        }

        return $this->endTask($this->status('S'), $this->lang('This order has imported'), true, false);
    }

    private function updateLog($log_status, $log_key = '', $log_value = '') {

        $log_data = array(
            'sub_marketplace' => (int) $this->get_config->sub_marketplace,
            'id_country' => (int) $this->get_config->id_country,
            'id_shop' => (int) $this->get_config->id_shop,
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_IMPORT,
            'count_process' => $this->process,
            'count_send' => 0,
            'count_success' => isset($this->success) ? $this->success : 0,
            'count_error' => 0,
            'count_skipped' => isset($this->skipped) ? count($this->skipped) : 0,
            'count_warning' => 0,
            'is_cron' => $this->cron ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s')
        );
        
        $id_log = $this->mirakl_database->updateMiraklLog($log_data, $this->skipped);
        return $id_log;
    }

}

// end defintion