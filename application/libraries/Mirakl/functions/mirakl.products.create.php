<?php

require_once dirname(__FILE__).'/../classes/mirakl.lib.abstract.php';
require_once dirname(__FILE__).'/../../eucurrency.php';

class MiraklProductCreate extends MiraklTask implements iProduct {

    private $mapping_field;
    private $mapping_field_list;
    private $mapping_model;
    private $mapping_attribute_list;
    private $reports = array();
    private $reports_detail = array();
    private $path_download_product;
    private $path_export_create;
    private $file_create;
    private $file_error;
    private $set_time = 10; // secound
    private $set_max_loop = 15; // secound
    private $product_tax;

    public function __construct($config, $cron = false) {
        parent::__construct($config['user_name']);

        $this->config = $config;
        $this->cron = $cron;
        $this->batch_id = $this->getBatchId();
        $this->date_time = $this->getDateTime();

        $this->path_download_product = $this->getPathMirakl().'/download/products';
        $this->path_export_create = $this->getPathMirakl().'/export/create';
        $this->file_create = $this->getFileName($config['marketplace'], $config['ext']).'-product-create.csv';
        $this->file_error = '/products-import-error-report-';

        Mirakl_Tools::load($config['language']);
        $this->setLang($config['iso_code']);

        if (!isset($config['download'])) {
            $this->setPopup();
        }
    }

    private function setPopup() {
        $context = array();
        $context['action'] = self::ACTION_PRODUCT_CREATE;
        $context['batch_id'] = $this->batch_id;
        $context['date_time'] = $this->date_time;
        $context['user_name'] = $this->config['user_name'];
        $context['ext'] = $this->config['ext'];
        $context['country'] = $this->config['countries'];
        $context['marketplace'] = $this->config['marketplace'];

        $popup = $this->startPopup($context);
        $this->proc_rep = $popup['proc_rep'];
        $this->message = $popup['message'];
    }

    private function stopTask($message, $log_key = '', $log_value = null) {

        if (empty($log_key)) {
            $message = $this->lang($message);
            $log_key = $message;
        }

        $this->setLog(MiraklParameter::LOG_STATUS_ERROR, $log_key, $log_value);

        if (isset($this->proc_rep)) {
            $this->message->no_product = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_ERROR, 'message' => $message);
    }

    private function finishTask($message) {

        $this->setLog(MiraklParameter::LOG_STATUS_SUCCESS);

        if (isset($this->proc_rep)) {
            $this->message->product->status = 'Success';
            $this->message->product->message = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_SUCCESS, 'message' => $message);
    }

    public function processProduct($params = array(), $download = false) {

        $to_mirakl = $data = array();
        $currencies = new Eucurrency();
        $this->download = $download;

        /* set init params */
        $in_stock = ($params['in_stock'] == 1) ? true : false;
        $send_mirakl = ($params['send_mirakl'] == 1) ? true : false;

        if (isset($this->proc_rep)) {
            $this->message->product->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        /* get selected Categories */
        $default_categories = $this->getDatabase()->getMiraklCategorySelect($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);

        if (empty($default_categories)) {
            return $this->stopTask('You must configure the categories to create');
        }

        /* get profile */
        $default_profiles = $this->getDatabase()->getMiraklProfile($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);

        if (empty($default_profiles)) {
            return $this->stopTask('You must configure the profiles to create');
        }
        
        
        $id_categories = array_map(function($params) {
            return $params['id_category'];
        }, $default_categories);

        $default_profiles2categories = array_map(function($params) {
            return $params['id_profile'];
        }, $default_categories);

        unset($default_categories);

        /* get product in category selected */
        $products = $this->getDatabase()->getProductIdInCategory($this->config['id_shop'], $id_categories);
        
        unset($id_categories);

        if (empty($products)) {
            return $this->stopTask('No product to create');
        }

        $list_id_product = array_map(function($params) {
            return $params['id_product'];
        }, $products);

        $this->product_tax = $this->getDatabase()->getProductTax($list_id_product);


        /* set limit memory */
        ini_set('memory_limit', '-1');

        /* get product data by list id product */
        $objProduct = new Product($this->config['user_name']);
        $list_products = $objProduct->exportProductsArray($this->config['id_shop'], null, $this->config['id_lang'], null, $list_id_product);
        
        unset($objProduct);
        unset($list_id_product);

        $conditions = $this->getDatabase()->getSettingCondition($this->config['id_shop']);
        $conditions_mapping = $this->getDatabase()->getSettingMappingCondition($this->config['id_shop'], MiraklParameter::MARKETPLACE_ID);

        /* mapping */
        $this->mapping_field = $this->getDatabase()->getMiraklMappingField($this->config['sub_marketplace'], $this->config['id_shop']);
        $this->mapping_field_list = $this->getDatabase()->getMiraklMappingFieldList($this->config['sub_marketplace'], $this->config['id_shop']);
        $this->mapping_model = $this->getDatabase()->getMiraklMappingModelKeyId($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);
        $this->mapping_attribute_list = $this->getDatabase()->getMiraklMappingAttributeList($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);

        if (empty($this->mapping_field)) {
            return $this->stopTask('You must configure the mapping field to create');
        }

        if (empty($this->mapping_model)) {
            return $this->stopTask('You must configure the mapping model to create');
        }

        /* check field require */
        $mapping_field_required = array_reduce($this->mapping_field, function($result, $value) {
            if ($value['required'] == 'Y' && empty($value['shop_attribute'])) {
                $result[] = $value['label'];
            }
            return $result;
        });
        
        if (!empty($mapping_field_required)) {
            $mapping_field_required = implode(", ", $mapping_field_required);
            return $this->stopTask(sprintf($this->lang('You must mapping field %s are required'), $mapping_field_required), 'You must mapping field %s are required', $mapping_field_required);
        }

        /* check field list require */
        if (!empty($this->mapping_field_list)) {
            $mapping_field_list_required = array_reduce($this->mapping_field_list, function($result, $value) {
                if ($value['required'] == 'Y' && empty($value['shop_attribute'])) {
                    $result[] = $value['label'];
                }
                return $result;
            });

            if (!empty($mapping_field_list_required)) {
                $mapping_field_list_required = implode(", ", $mapping_field_list_required);
                return $this->stopTask(sprintf($this->lang('You must mapping field list %s are required'), $mapping_field_list_required), 'You must mapping field list %s are required', $mapping_field_list_required);
            }
        }

        /* start product */
        foreach ($products as $position => $product) {

            // check id_product
            if (!isset($product['id_product']) || empty($product['id_product'])) {
                continue;
            }

            $id_product = $product['id_product'];
            $this->count_process = $this->count_process + 1;

            if (!isset($list_products[$id_product])) { // how happened ?
                $this->skipped[] = array('id product', $id_product, 'missing list product', '', $this->lang('missing list product'));
                continue;
            }

            $details = $list_products[$id_product];

            // check id_category
            if (!isset($details['id_category_default']) || empty($details['id_category_default'])) {
                $this->skipped[] = array('id product', $id_product, 'missing id category', '', $this->lang('missing id category'));
                continue;
            }

            // id_category 
            $id_category = $details['id_category_default'];

            // get id profile
            $id_profile = isset($default_profiles2categories[$id_category]) ? (int) $default_profiles2categories[$id_category] : '';

            if (isset($id_profile) && !empty($id_profile)) {
                $profiles = isset($default_profiles[$id_profile]) ? $default_profiles[$id_profile] : null;
            }

            // check $profiles
            if (!isset($profiles) || empty($profiles)) {
                $this->skipped[] = array('id product', $id_product, 'missing profile', '', $this->lang('missing profile'));
                continue;
            }

            // set configuration
            $profile_long_attr = isset($profiles['combinations_long_attr']) && $profiles['combinations_long_attr'] ? true : false;
            $profile_price_rule = isset($profiles['price_rule']) ? $profiles['price_rule'] : '';
            $rounding = isset($profiles['rounding']) ? (int) $profiles['rounding'] : 2;
            $decription_field = isset($profiles['description_field']) ? $profiles['description_field'] : '';
            $decription_html = isset($profiles['html_description']) && $profiles['html_description'] ? true : false;
            $logistic_class = isset($profiles['logistic_class']) ? $profiles['logistic_class'] : '';
            $synchronization_field = isset($profiles['synchronization_field']) ? $profiles['synchronization_field'] : '';
            $warranty = isset($profiles['warranty']) ? $profiles['warranty'] : '';
            $id_model = isset($profiles['id_model']) && !empty($profiles['id_model']) ? $profiles['id_model'] : '';
            $product_format = isset($profiles['product_format']) && !empty($profiles['product_format']) ? $profiles['product_format'] : '';
            $no_image = isset($profiles['no_image']) && !empty($profiles['no_image']) ? $profiles['no_image'] : 0;

            $data['no_image'] = $no_image;

            $reference = $details['reference'];
            $ean13 = $details['ean13'];
            $key_parent = !empty($reference) ? $reference : (!empty($ean13) ? $ean13 : $id_product);

            if (isset($details['currency']) && !empty($details['currency'])) {
                foreach ($details['currency'] as $currency) {
                    $from_currency = $currency['iso_code'];
                }
            }

            $model = isset($this->mapping_model[$id_model]) ? $this->mapping_model[$id_model] : '';

            if (isset($model['hierarchy']) && !empty($model['hierarchy'])) {
                $category_mapping_model = end($model['hierarchy']);
                $data['category_mapping_model'] = $category_mapping_model['code'];
            } else {
                $this->skipped[] = array('reference', $key_parent, 'You must chosen model in the profile %s', $profiles['name'], sprintf($this->lang('You must chosen the model in the profile %s'), $profiles['name']));
                continue;
            }

            // features
            $features = array();
            if (isset($details['feature']) && !empty($details['feature'])) {
                foreach ($details['feature'] as $id_feature => $feature) {
                    $features[$id_feature]['feature_name'] = $feature['name'];
                    $features[$id_feature]['id_feature_value'] = $feature['id_value'];
                    $features[$id_feature]['feature_value'] = isset($feature['value']) ? $this->getLang($feature['value']) : '';
                }
            }
            $data['features'] = $features;

            // product type
            if (!in_array($synchronization_field, array(MiraklParameter::SYNC_FIELD_EAN, MiraklParameter::SYNC_FIELD_UPC, MiraklParameter::SYNC_FIELD_SKU))) {
                $synchronization_field = MiraklParameter::SYNC_FIELD_EAN; // default product type = 'EAN'
            }
            $data['product_type'] = $synchronization_field;

            //name
            $product_name = isset($details['name']) ? $this->getLang($details['name']) : '';
            $product_name = Mirakl_Tools::encodeText($product_name, true);

            // Description
            $description_long = isset($details['description']) ? $this->getLang($details['description']) : '';
            $description_short = isset($details['description_short']) ? $this->getLang($details['description_short']) : '';

            switch ($decription_field) {
                case MiraklParameter::FIELD_DESCRIPTION_LONG :
                    $description = Mirakl_Tools::formatDescription($description_long, $decription_html);
                    break;
                case MiraklParameter::FIELD_DESCRIPTION_SHORT :
                    $description = Mirakl_Tools::formatDescription($description_short, $decription_html);
                    break;
                case MiraklParameter::FIELD_DESCRIPTION_BOTH :
                    $description = Mirakl_Tools::formatDescription($description_long, $decription_html);
                    $description .= $decription_html ? nl2br("\n") : "\n";
                    $description .= Mirakl_Tools::formatDescription($description_short, $decription_html);
                    break;
                default: $description = '';
            }
            $data['description'] = Mirakl_Tools::encodeText($description, true);

            //Images
            $data['images'] = isset($details['image']) ? $details['image'] : array();

            //Image normal
            $data['images_normal'] = array();
            if (isset($details['image']) && !empty($details['image'])) {
                $index = 0;
                foreach ($details['image'] as $img_value) {
                    if ($img_value['image_type'] == 'normal') {
                        $data['images_normal'][$index] = $img_value;
                        $index = $index + 1;
                    }
                }
            }

            // Manufacturer
            $manufacturer = isset($details['manufacturer'][$details['id_manufacturer']]) ? $details['manufacturer'][$details['id_manufacturer']] : '';
            $data['manufacturer'] = $manufacturer;

            //supplier
            if (isset($details['supplier'])) {
                $supplier = reset($details['supplier']); // into first array
                $supplier = isset($supplier['name']) && !empty($supplier['name']) ? $supplier['name'] : '';
            } else {
                $supplier = '';
            }
            $data['supplier'] = $supplier;

            // mapping condition
            $id_condition = isset($details['id_condition']) && !empty($details['id_condition']) ? $details['id_condition'] : '';
            $name_condition = isset($conditions[$id_condition]) && !empty($conditions[$id_condition]) ? $conditions[$id_condition] : '';
            $condition = isset($conditions_mapping[$name_condition]) && !empty($conditions_mapping[$name_condition]) ? $conditions_mapping[$name_condition] : 11; // 11 hard code !!!!!!!!!
            $data['state'] = $condition;

            $data['tax'] = isset($this->product_tax[$id_product]['rate']) ? $this->product_tax[$id_product]['rate'] : '';


            $data['logistic_class'] = $logistic_class;
            $data['warranty'] = !empty($warranty) ? $warranty : 0;
            $data['favorite'] = '';
            $data['ecotax'] = 0;
            $data['memtax'] = 0;

            // get attributes
            $has_attribute = isset($details['has_attribute']) && $details['has_attribute'] > 0 ? true : false;

            // check have combination if not exists is set parent.
            $combinations = array();
            if (!isset($details['combination']) && empty($details['combination']) && !$has_attribute) {
                $combinations[0]['reference'] = $reference;
                $combinations[0]['ean13'] = $ean13;
                $combinations[0]['upc'] = $details['upc'];
                $combinations[0]['id_product_attribute'] = 0;
                $combinations[0]['attributes'] = '';
                $combinations[0]['price'] = $details['price'];
                $combinations[0]['quantity'] = $details['quantity'];
            } else {
                $combinations = $details['combination'];
            }

            // Grouping Combinations
            asort($combinations);

            foreach ($combinations as $combination) {

                // key_chlid is array index key 0 into skipped
                $reference = $combination['reference'];
                $ean13 = $combination['ean13'];
                $key_chlid = !empty($reference) ? $reference : (!empty($ean13) ? $ean13 : $id_product);

                $attributes = array();
                $attributes_description = array();
                if (isset($combination['attributes']) && !empty($combination['attributes'])) {
                    foreach ($combination['attributes'] as $id_attribute_group => $attribute) {
                        $id_attribute = key($attribute['value']);

                        $attributes[$id_attribute_group]['attribute_group_name'] = isset($attribute['name']) ? $this->getLang($attribute['name']) : '';
                        $attributes[$id_attribute_group]['id_attribute'] = $id_attribute;
                        $attributes[$id_attribute_group]['attribute_name'] = isset($attribute['value'][$id_attribute]['name']) ? $this->getLang($attribute['value'][$id_attribute]['name']) : '';

                        // attributes_description is define
                        $attributes_description[] = isset($attribute['name']) ? '('.$this->getLang($attribute['name']).' '.$this->getLang($attribute['value'][$id_attribute]['name']).')' : '';
                    }
                }

                $data['attributes'] = $attributes;

                $attribute_description = !empty($attributes_description) ? implode(", ", $attributes_description) : '';
                $attribute_description = Mirakl_Tools::encodeText($attribute_description, true);

                if (!empty($attribute_description)) { // set key attribute in log.
                    $key_attribute = $attribute_description;
                } else {
                    $key_attribute = $product_name; // not have attribute, set product name to key log.
                }

                switch ($product_format) {
                    case MiraklParameter::NAME_NAME_ATTRIBUTES: // Name and Attributes
                        if (!empty($attribute_description)) {
                            $name = rtrim(sprintf('%s (%s)', $product_name, rtrim($attribute_description, ' - ')), ' - ');
                            $name = trim(rtrim($name, '-'));
                            $product_name = $name;
                        } else {
                            $product_name = $product_name;
                        }
                        break;
                    case MiraklParameter::NAME_BRAND_NAME_ATTRIBUTES: // Brand, Name and Attributes
                        if (!empty($attribute_description)) {
                            if (!empty($manufacturer)) {
                                $name = rtrim(sprintf('%s - %s (%s)', $manufacturer, $product_name, rtrim($attribute_description, ' - ')), ' - ');
                            } else {
                                $name = rtrim(sprintf('%s (%s)', $product_name, rtrim($attribute_description, ' - ')), ' - ');
                            }
                            $name = trim(rtrim($name, '-'));
                            $product_name = $name;
                        } else {
                            $product_name = sprintf('%s - %s', $manufacturer, $product_name);
                        }
                        break;
                    case MiraklParameter::NAME_NAME_BRAND_ATTRIBUTES: // Name, Brand and Attributes
                        if (!empty($attribute_description)) {
                            if (!empty($manufacturer)) {
                                $name = rtrim(sprintf('%s - %s - (%s)', $product_name, $manufacturer, rtrim($attribute_description, ' - ')), ' - ');
                            } else {
                                $name = rtrim(sprintf('%s - (%s)', $product_name, rtrim($attribute_description, ' - ')), ' - ');
                            }
                            $name = trim(rtrim($name, '-'));
                            $product_name = $name;
                        } else {
                            $product_name = sprintf('%s - %s', $product_name, $manufacturer);
                        }
                        break;
                    default: // ame Only
                        $product_name = $product_name;
                        break;
                }

                $data['product_name'] = $product_name;
                $data['internal_description'] = sprintf('Product Creation Attempt for %s, %s', $product_name, date('Y-m-d H:i:s'));

                // check image
                if (isset($data['images'][1]) && empty($data['images'][1]) && $no_image) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s missing image', $key_attribute, sprintf($this->lang("product %s missing image"), $key_attribute));
                    continue;
                }

                // check reference
                if (empty($reference)) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s missing reference', $key_attribute, sprintf($this->lang("product %s missing reference"), $key_attribute));
                    continue;
                }
                $data['reference'] = $reference;

                // check ean13
                if (empty($ean13)) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s missing ean13', $key_attribute, sprintf($this->lang('product %s missing ean13'), $key_attribute));
                    continue;
                }

                // check wrong ean13
                if (!Mirakl_Tools::eanupcCheck($ean13)) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s wrong ean13', $key_attribute, sprintf($this->lang('product %s wrong ean13'), $key_attribute));
                    //continue; //debug for test
                }
                $data['ean13'] = $ean13;

                if ($ean13 && isset($history_ean13[$ean13])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product ean13 %s duplicate', $key_attribute, sprintf($this->lang('product ean13 %s duplicate'), $key_attribute));
                    continue;
                }
                $history_ean13[$ean13] = true;

                if (isset($sku_history[$reference])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product reference %s duplicate', $key_attribute, sprintf($this->lang('product reference %s duplicate'), $key_attribute));
                    continue;
                }
                $sku_history[$reference] = true;

                $data['upc'] = isset($combination['upc']) ? $combination['upc'] : '';

                // check out of stock
                if ($in_stock && (int) $combination['quantity'] < 1) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s out of stock', $key_attribute, sprintf($this->lang('product %s out of stock'), $key_attribute));
                    continue;
                }

                $data['quantity'] = ($details['active'] == 0) ? 0 : $combination['quantity'];

                // Price		   
                $price = $combination['price'];

                // price rule
                if (isset($profile_price_rule) && !empty($profile_price_rule)) {
                    $price = Mirakl_Tools::price_rule($profile_price_rule, $rounding, $price);
                }

                // convert price
                $price = $currencies->doDiffConvert($price, $from_currency, $this->config['currency']);
                $data['price'] = (float) round($price, $rounding);

                //weight
                $data['weight'] = isset($combination['weight']['value']) && !empty($combination['weight']['value']) ? $combination['weight']['value'] : '';

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // set data into mirakl

                $notification = array();

                foreach ($this->mapping_field as $mapping_field) {

                    $id_field = $mapping_field['id_field'];
                    $shop_attribute = $mapping_field['shop_attribute'];
                    $field = $mapping_field['code'];
                    $label = $mapping_field['label'];
                    $required = $mapping_field['required'];
                    $type = $mapping_field['type'];
                    $validation = $mapping_field['validations'];

                    $fields = $this->getField($shop_attribute, $data); // array in code
                    $field_value = isset($fields['code']) ? $fields['code'] : '';

                    if ($this->is_required($required, $field_value)) {
                        $notification['required'][] = $label;
                    }

                    if ($type == 'INTEGER' && $field_value != 0) {

                        $field_value = (int) $field_value; /* convert string to int eg. "2 ans" = 2 */

                        if ($field_value === 0) { /* is 0 error */
                            $notification['type'][] = $label;
                        }
                    }

                    if ($this->is_type($type, $field_value)) {
                        $notification['type'][] = $label;
                    }

                    if ($this->is_validation($validation, $field_value)) {
                        $notification['valid'][] = $label." must ".$validation;
                    }

                    /* referecne to report */
                    if ($fields['is_reference'] && empty($this->reference_report)) {
                        $this->reference_report = $field;
                    }

                    $to_mirakl[$position][$field] = $field_value;
                }

                if (!empty($notification)) {
                    unset($to_mirakl[$position]);
                    $notify = $this->getNotification($notification);

                    if (!empty($notify['notify_key']) && !empty($notify['notify_value'])) {
                        $this->skipped[] = array('reference', $key_chlid, $notify['notify_key'], $notify['notify_value'], sprintf($this->lang($notify['notify_key']), $notify['notify_value']));
                        continue;
                    }
                }


                $notification = array();

                if (!empty($this->mapping_field_list)) {
                    foreach ($this->mapping_field_list as $id_field_attribute => $mapping_field_list) {

                        $field = $mapping_field_list['code'];
                        $label = $mapping_field_list['label'];
                        $required = $mapping_field_list['required'];
                        $type = $mapping_field_list['type'];
                        $validation = $mapping_field_list['validations'];

                        if (isset($mapping_field_list['code_list']) && !empty($mapping_field_list['code_list'])) {
                            $field_value = $mapping_field_list['code_list'];
                        } else {
                            $field_value = '';

//                            $field_value = $this->getField($mapping_field_list, $data);
//
//                            if (isset($mapping_field_list['value_list']) && !empty($mapping_field_list['value_list'])) {
//                                $field_value = $this->getFieldList($mapping_field_list['value_list'], $field_value);
//                            } else {
//                                $field_value = $field_value['code']; // no into
//                            }
                        }

                        if ($this->is_required($required, $field_value)) {
                            $notification['required'][] = $label;
                        }

                        if ($this->is_type($type, $field_value)) {
                            $notification['type'][] = $label;
                        }

                        if ($this->is_validation($validation, $field_value)) {
                            $notification['valid'][] = $label." must ".$validation;
                        }

                        $to_mirakl[$position][$field] = $field_value;
                    }
                }

                if (!empty($notification)) {
                    unset($to_mirakl[$position]);
                    $notify = $this->getNotification($notification);

                    if (!empty($notify['notify_key']) && !empty($notify['notify_value'])) {
                        $this->skipped[] = array('reference', $key_chlid, $notify['notify_key'], $notify['notify_value'], sprintf($this->lang($notify['notify_key']), $notify['notify_value']));
                        continue;
                    }
                }

                

                $notification = array();
                if (isset($model['mapping']) && !empty($model['mapping'])) {
                    
                    foreach ($model['mapping'] as $id_model_attribute => $model_mapping) {

                        $shop_attribute = $model_mapping['shop_attribute'];
                        $field = $model_mapping['code'];
                        $label = $model_mapping['label'];
                        $required = $model_mapping['required'];
                        $type = $model_mapping['type'];
                        $validation = $model_mapping['validations'];

                        if (isset($model_mapping['code_list']) && !empty($model_mapping['code_list'])) {
                            $field_value = $model_mapping['code_list'];
                        } else {

                            $fields = $this->getField($shop_attribute, $data);

                            if (isset($model_mapping['value_list']) && !empty($model_mapping['value_list'])) {
                                //echo '<pre>'; print_r($model['mapping']); exit;
                                print_r('into getFieldList');
                                exit;
                                $field_value = $this->getFieldList($model_mapping['value_list'], $fields);
                            } else {
                                
                                $t = $fields['type'].'-'.$fields['id'];
                                
                                if(isset($this->mapping_attribute_list[$t]['code']) && !empty($this->mapping_attribute_list[$t]['code'])){
                                    $field_value = $this->mapping_attribute_list[$t]['code'];
                                }else{
                                    $field_value = $fields['code'];
                                }
                                //echo '<pre>'; print_r($this->mapping_attribute_list); exit;
                            }
                        }
                        
                        

                        // default value from model
                        if (empty($field_value) && !empty($model_mapping['default_value'])) {
                            $field_value = $model_mapping['default_value'];
                        }

                        if ($this->is_required($required, $field_value)) {
                            $notification['required'][] = $label;
                        }

                        if ($this->is_type($type, $field_value)) {
                            $notification['type'][] = $label;
                        }

                        if ($this->is_validation($validation, $field_value)) {
                            $notification['valid'][] = $label." must ".$validation;
                        }

                        $to_mirakl[$position][$field] = $field_value;
                    }
                }

                if (!empty($notification)) {
                    unset($to_mirakl[$position]);
                    $notify = $this->getNotification($notification);

                    if (!empty($notify['notify_key']) && !empty($notify['notify_value'])) {
                        $this->skipped[] = array('reference', $key_chlid, $notify['notify_key'], $notify['notify_value'], sprintf($this->lang($notify['notify_key']), $notify['notify_value']));
                        continue;
                    }
                }
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        } // end loop product
        
        //echo '<pre>'; print_r($to_mirakl); exit;
  
        if (empty($to_mirakl) || !is_array($to_mirakl)) {
            return $this->stopTask('No product create into marketplace');
        }

        // create csv file
        $export = $this->exportProduct($to_mirakl);

        if ($export['count'] == 0) {
            return $this->stopTask('Product create failed');
        }

        //$send_mirakl = 0; // now, product no send exit;
        if ($send_mirakl) {
            $import_id = $this->uploadFile($export['filename']);
            $this->setLogReport();
            $count = count($to_mirakl);
        } else {
            $count = $export['count'];
        }

        $this->count_success = $count;

        // distory variable
        unset($default_profiles);
        unset($default_profiles2categories);
        unset($products);
        unset($list_products);
        unset($to_mirakl);
        unset($data);
        unset($this->mapping_field);
        unset($this->mapping_field_list);
        unset($this->mapping_model);

        // finish task
        return $this->finishTask(sprintf($this->lang('Products have created %s product'), $count));
    }

    public function exportProduct($data = array()) {

        $count_row_csv = 0;
        $keys = array();
        $file = $this->path_export_create.'/'.$this->file_create;

        if (!($fp = fopen($file, 'w+'))) {
            return $this->stopTask('Unable to write in file');
        }

        $data = Mirakl_Tools::array_filter_recursive($data);

        /* get key into title */
        foreach ($data as $CSV) {
            $keys = array_unique(array_merge($keys, array_keys($CSV)));
        }
        fputcsv($fp, $keys, $this->separator);

        foreach ($data as $CSV) {
            $count_row_csv++;
            fputcsv($fp, array_merge(array_fill_keys($keys, null), $CSV), $this->separator);
        }
        fclose($fp);

        if (file_exists($file)) {
            $perms = (int) substr(sprintf('%o', fileperms($file)), -4);
            if ($perms != 777 && $perms != 666) {
                chmod($file, 0777);
            }
        }

        // download product will return file name and exit.
        if ($this->download && file_exists($file)) {
            echo $this->file_create;
            exit;
        }

        return array('count' => $count_row_csv, 'filename' => $file);
    }

    public function uploadFile($filename = '') {

        require_once dirname(__FILE__).'/../classes/mirakl.api.products.php';
        require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];


        $api_products = new MiraklApiProducts($mirakl_params);
        $response = $api_products->imports_upload($filename);
        $xml = simplexml_load_string($response);

        $import_id = '';
        if (isset($xml->import_id) && !empty($xml->import_id)) {
            $import_id = (int) $xml->import_id;

            $this->reports = $this->getReport($import_id);
            $this->reports_detail = $this->getReportDetail($import_id);
        }

        return $import_id;
    }

    public function getReport($import_id = '') {

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $api_products = new MiraklApiProducts($mirakl_params);

        $msg = '';
        $count_check = 0;
        do {
            sleep($this->set_time); // set time delay
            $msg = $api_products->imports($import_id);
            $xml = simplexml_load_string($msg);
            $count_check = $count_check + 1;
        } while ($xml->import_status != 'COMPLETE' && $xml->import_status != 'SENT' && $count_check <= $this->set_max_loop);
        //TRANSFORMATION_WAITING 
        //SENT

        if (((int) $xml->transform_lines_in_error) > 0 || ((int) $xml->transform_lines_with_warning) > 0) { // have error or warning ?
            $api_error_report = $api_products->get_api_transformation_error_report($import_id);
            $src = fopen($api_error_report, 'r');
            $file_error = $this->path_download_product.$this->file_error.$import_id.'.csv';
            $dest1 = fopen($file_error, 'w');
            stream_copy_to_stream($src, $dest1);
        }

        if (!($count_check <= $this->set_max_loop)) {
            $this->error = Mirakl_Tools::l('Incorrect status');
            if (isset($this->proc_rep)) {
                $this->message->no_product = $this->error;
                $this->proc_rep->set_status_msg($this->message);
            }
        }

        return $xml;
    }

    public function getReportDetail($import_id = '') {
        $file = $this->path_download_product.$this->file_error.$import_id.'.csv';

        if (!file_exists($file)) {
            return '';
        }

        $datas = array();
        if (($handle = fopen($file, "r")) === FALSE) {
            return;
        }
        $report_text = array();
        $index = 0;
        while (($cols = fgetcsv($handle, 3000, ";")) !== FALSE) {

            if ($index == 0) { // title csv.
                $key_sku = array_search($this->reference_report, $cols) ? array_search($this->reference_report, $cols) : 0; // get key sku.
                $key_error = array_search('errors', $cols) ? array_search('errors', $cols) : array_search(end($cols), $cols); // get ket errors.
                $key_warning = array_search('warnings', $cols) ? array_search('warnings', $cols) : ''; // get ket errors.
            } else {
                $report_text[$index - 1]['rererence'] = isset($cols[$key_sku]) ? $cols[$key_sku] : '';
                $report_text[$index - 1]['error'] = isset($cols[$key_error]) ? $cols[$key_error] : '';
                $report_text[$index - 1]['warning'] = isset($cols[$key_warning]) ? $cols[$key_warning] : '';
            }
            $index++;
        }

        return $report_text;
    }

    public function setLog($log_status, $log_key = null, $log_value = null) {
        $log_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_PRODUCT,
            'count_process' => $this->count_process,
            'count_send' => $this->count_send,
            'count_success' => $this->count_success,
            'count_error' => $this->count_error,
            'count_skipped' => count($this->skipped),
            'count_warning' => $this->count_warning,
            'is_cron' => ($this->cron) ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s')
        );
        //echo '<pre>'; print_r($this->skipped); exit;
        $id_log = $this->getDatabase()->updateMiraklLog($log_data, $this->skipped);
        return $id_log;
    }

    public function setLogReport() {

        if (empty($this->reports)) {
            return false;
        }

        $reports = (object) $this->reports;
        $report_detail = !empty($this->reports_detail) ? $this->reports_detail : array();
        
        unset($this->reports);
        unset($this->reports_detail);

        $detail = array();
        $detail['ReportImportId'] = isset($reports->import_id) ? (int) $reports->import_id : 0; // Number
        $detail['ReportProcess'] = isset($reports->transform_lines_read) ? (int) $reports->transform_lines_read : 0; // Number
        $detail['ReportSuccess'] = isset($reports->transform_lines_in_succes) ? (int) $reports->transform_lines_in_succes : 0; // Number
        $detail['ReportError'] = isset($reports->transform_lines_in_error) ? (int) $reports->transform_lines_in_error : 0; // Number
        $detail['ReportWarning'] = isset($reports->transform_lines_with_warning) ? (int) $reports->transform_lines_with_warning : 0; // Number
        $detail['ReportMode'] = '-'; //  Products not have mode
        $detail['ReportStatus'] = isset($reports->import_status) ? (string) $reports->import_status : '';
        $detail['ReportOfferDelete'] = ''; //  Products not have delete
        $detail['ReportOfferInsert'] = ''; //  Products not have insert
        $detail['ReportOfferUpdate'] = ''; //  Products not have update
        $detail['ReportHasError'] = isset($reports->has_error_report) ? (string) $reports->has_error_report : ''; // Boolean
        $detail['ReportDateCreate'] = isset($reports->date_created) ? (string) $reports->date_created : ''; // Date

        $report_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_PRODUCT,
            'detail' => serialize($detail),
            'date_add' => date('Y-m-d H:i:s'),
        );
        //echo '<pre>'; print_r($detail); exit;
        $id_log_report = $this->getDatabase()->saveMiraklLogReport($report_data, $report_detail);
    }

    public function downloadFile($filename = '') {
        $link_download = $this->path_export_create.'/'.$filename;
        $text = file_get_contents($link_download);
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.$this->file_create.'"');
        echo $text;
    }

//    private function getFieldList($value_list, $field_value) {
//
//        if (isset($field_value['id']) && !empty($field_value['id'])) {
//            $field = $field_value['id'];
//        } else if (isset($field_value['code']) && !empty($field_value['code'])) {
//            $field = $field_value['code'];
//        } else {
//            return '';
//        }
//
//        $data = '';
//        foreach ($value_list as $list) {
//            if ($field == $list['shop_attribute_value']) {
//                $data = $list['code_list'];
//            }
//        }
//        return $data;
//    }
}

// end defintion