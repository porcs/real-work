<?php

require_once dirname(__FILE__).'/../classes/mirakl.order.php';
require_once dirname(__FILE__).'/../classes/mirakl.api.orders.php';
require_once dirname(__FILE__).'/../classes/mirakl.lib.abstract.php';

class MiraklOrderAccept extends MiraklTask implements iOrder {

    private $mirakl_order;
    private $order_id;
    private $is_lookup;
    private $is_accept;

    public function __construct($config, $cron = false) {
        parent::__construct($config['user_name']);
        $this->config = $config;
        $this->cron = $cron;
        $this->batch_id = $this->getBatchId();
        $this->date_time = $this->getDateTime();

        Mirakl_Tools::load($config['language']);
        $this->mirakl_order = new MiraklOrder($this->config['user_name']);

        if (0) {
            exit;
            $this->setPopup();
        }
    }

    private function setPopup() {
        $context = array();
        $context['action'] = self::ACTION_ORDER_ACCEPT;
        $context['batch_id'] = $this->batch_id;
        $context['date_time'] = $this->date_time;
        $context['user_name'] = $this->config['user_name'];
        $context['ext'] = $this->config['ext'];
        $context['country'] = $this->config['countries'];
        $context['marketplace'] = $this->config['marketplace'];

        $popup = $this->startPopup($context);
        $this->proc_rep = $popup['proc_rep'];
        $this->message = $popup['message'];
    }

    private function stopTask($message, $log_key = '', $log_value = null) {

        if (empty($log_key)) {
            $message = $this->lang($message);
            $log_key = $message;
        }

        $this->setLog(MiraklParameter::LOG_STATUS_ERROR, $log_key, $log_value);

        if (isset($this->proc_rep)) {
            $this->message->no_product = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_ERROR, 'message' => $message);
    }

    private function finishTask($message) {

        $this->setLog(MiraklParameter::LOG_STATUS_SUCCESS);

        if (isset($this->proc_rep)) {
            $this->message->product->status = 'Success';
            $this->message->product->message = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_SUCCESS, 'message' => $message);
    }

    public function processOrder($params = array(), $download = false) {

        /* set init params */
        $this->order_id = $params['order_id'];
        $this->is_lookup = $params['is_lookup'];
        $this->is_accept = ($this->config['auto_accept'] == 1) ? true : false;

        if (isset($this->proc_rep)) {
            $this->message->product->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        $orders = $this->getOrder($this->order_id);

        if (isset($orders['S']) && $orders['S'] == $this->E) {
            return $this->stopTask($orders['M']);
        }

        // $status = 0;
        $data = array();
        $full = array();
        $info = array();

        foreach ($orders as $i => $order) {
            /* (*1 order) = (1 task) and many product */
            $res = $this->setOrder($order);
            $full[$i] = $res;

            if (isset($res['M']) && !empty($res['M'])) {
                $message = $res['M'];
                $this->setLog($this->E, $message[2], isset($message[3]) ? $message[3] : null);
            } else if (empty($res['detail'])) {
                $this->skipped = $res['skipped'];
                $this->count_process = count($res['skipped']);
                $this->setLog($this->E, 'An order %s is not product', $res['order_id']);
            } else if (!empty($res['skipped'])) {
                $this->skipped = $res['skipped'];
                $this->count_process = count($res['skipped']) + count($res['detail']);
                $this->setLog($this->E, 'An order %s is has skipped', $res['order_id']);
            } else {
                $info[$i] = $res;
            }
        }

        
        if(empty($info) && !empty($this->order_id) && !$this->is_lookup){
            return array('status' => $this->E, 'message' => sprintf($this->lang('An order %s is not product'), $this->order_id)); 
        }
        


        foreach ($info as $i => $data) {

            if ($this->is_accept && $this->cron) {
                $response = $this->acceptOrderConfirm($data);
                $this->count_process = count($data['detail']);
                $this->count_send = $this->count_process;
                $this->setLog($response[0], $response[1], $response[2]);
                $full[$i]['response'] = $response;
            }

            if ($this->is_accept && $this->is_lookup) {
                
                $response = $this->acceptOrderConfirm($data);
                $this->count_process = count($data['detail']);
                $this->count_send = $this->count_process;
                $this->setLog($response[0], $response[1], $response[2]);
                $full[$i]['response'] = $response;
                
            }

            if (!empty($this->order_id) && !$this->is_lookup) {
                
                $response = $this->acceptOrderConfirm($data);
                $this->count_process = count($data['detail']);
                $this->count_send = $this->count_process;
                $this->setLog($response[0], $response[1], $response[2]);
                $full[$i]['response'] = $response;
                
                return array('status' => $response[0], 'message' => $response[1]); 
            }



//            if ($this->cron || $this->is_lookup) {
//                
//            }
//
//
//
//            if ($this->is_accept && $this->is_lookup) {
//
//                $response = $this->acceptOrderConfirm($data);
//
//                if ($response[0] == $this->E) {
//                    $this->count_success = 0;
//                    $this->count_error = $this->count_send;
//
//                    foreach ($data['detail'] as $detail) {
//                        $skipped = array('reference', $detail['offer_sku'], 'Can not accept', '');
//                        $data['skipped'][] = $skipped;
//                    }
//
//                    $this->skipped = $data['skipped'];
//                    $this->setLog($this->E, $response[1], $response[2]);
//                } else {
//                    $this->count_success = $this->count_send;
//                    $this->count_error = 0;
//                    $this->skipped = $data['skipped'];
//                    $this->setLog($this->S, $response[1], $response[2]);
//                }
//                continue;
//            }
//
//
//
//            if (!empty($this->order_id) && !$this->is_lookup) {
//                $response = $this->acceptOrderConfirm($id_order_accept, $data);
//                $skipped[] = array('order', $order_id, $response[1], $response[2]);
//                return array('F' => 'R', 'S' => $response[0], 'D' => $data, 'SP' => $skipped);
//            }
        }

//        } else {
////            if ($this->is_lookup) {
////                return $this->lookupOrder($data);
////            }
//        }
////        echo '<pre>';
//        print_r($full);
//        exit;
        return array('status' => $this->S, 'message' => '', 'optional' => $full);

        return $full;
    }

    private function setOrder($order) {

        // $this->skipped[] = array('id product', $id_product, 'missing profile', '', $this->lang('missing profile'));

        $skipped = array();
        $order = (array) $order; /* convert to array */

        if (!isset($order['order_id']) || empty($order['order_id'])) { //exit;
            $skipped = array('order', 'N/A', 'missing order id', '');
            return array('F' => 'N', 'S' => $this->E, 'D' => '', 'M' => $skipped);
        }

        if (!isset($order['commercial_id']) || empty($order['commercial_id'])) {
            $skipped = array('order', 'N/A', 'missing commercial id', '');
            return array('F' => 'N', 'S' => $this->E, 'D' => '', 'M' => $skipped);
        }

        $order_id = $order['order_id'];
        $commercial_id = $order['commercial_id'];

        $customer = array();
        if (isset($order['customer'])) {
            $customer = (array) $order['customer']; /* convert to array */
        }

        if (!isset($customer['customer_id'])) {
            $skipped = array('order', $order_id, 'An order %s missing customer', $order_id);
            return array('F' => 'N', 'S' => $this->E, 'D' => '', 'M' => $skipped);
        }

        $order_lines = array();
        if (isset($order['order_lines'])) {
            $order_lines = (array) $order['order_lines']; /* convert to array */
        }

        if (empty($order_lines)) {
            $skipped = array('order', $order_id, 'An order %s missing product', $order_id);
            return array('F' => 'N', 'S' => $this->E, 'D' => '', 'M' => $skipped);
        }

        /* if detail is 1 array 1 dim, if detail than more 1 array 2 dim */
        $is_more = isset($order_lines[0]) ? true : false;

        /* set format to array 2 dim */
        $details = array();
        if (!$is_more) {
            $details[0] = $order_lines; // array
        } else {
            $details = $order_lines; // object
        }

//        if ($order_id == '1443191243009-D') { // for debug
//            return '';
//        }

        $data = array();
        $data['order_id'] = $order_id;
        $data['commercial_id'] = $commercial_id;
        $data['order_state'] = $order['order_state'];
        $data['customer_name'] = $customer['firstname'].' '.$customer['lastname'];
        $data['shipping_price'] = sprintf('%.02f', $order['shipping_price']);
        $data['total_price'] = sprintf('%.02f', $order['total_price']);
        $data['tracking_no'] = '';
        $data['invoice_no'] = '';
        $data['order_date'] = date('Y-m-d H:i:s', strtotime($order['created_date']));
        $data['flag'] = 'N'; // N : no accept, Y : accept already.
        $data['status'] = 'complete';
        $data['export'] = ($this->config['auto_accept'] == 1) ? '1' : '0';
        $data['sub_marketplace'] = $this->config['sub_marketplace'];
        $data['id_country'] = $this->config['id_country'];
        $data['id_shop'] = $this->config['id_shop'];
        $data['date_add'] = date("Y-m-d H:i:s");
        $data['detail'] = array();
        $data['skipped'] = array();
        $data['errors'] = array();

        foreach ($details as $index => $detail) {

            if ($is_more) {
                $detail = (array) $detail; /* convert to array */
            }

            $offer_sku = $detail['offer_sku'];
            $product_sku = $detail['product_sku'];
            $id_product_attribute = $this->getDatabase()->getProductOrAttributeBySku($this->config['id_shop'], $offer_sku);

            if (empty($id_product_attribute)) {
                $skipped[] = array('order', $order_id, 'product %s missing offer sku', $offer_sku, sprintf($this->lang('product %s missing offer sku'), $offer_sku));
                continue;
            }

            $split = explode('_', $id_product_attribute);
            $id_product = $split[0];
            $id_product_attribute = isset($split[1]) ? $split[1] : '';

            $quantity = (int) $this->getDatabase()->getProductQuantity($this->config['id_shop'], $id_product, $id_product_attribute);
            $status = (!$quantity) ? 'outofstock' : 'instock';

            if (!$quantity) {
                $skipped[] = array('order', $order_id, 'product %s out of stock', $offer_sku, sprintf($this->lang('product %s out of stock'), $offer_sku));
                continue;
            }

            $data['detail'][$index]['order_id'] = $order_id;
            $data['detail'][$index]['id_product'] = $id_product;
            $data['detail'][$index]['offer_id'] = $detail['offer_id'];
            $data['detail'][$index]['offer_sku'] = $offer_sku;
            $data['detail'][$index]['order_line_id'] = $detail['order_line_id'];
            $data['detail'][$index]['order_line_index'] = $detail['order_line_index'];
            $data['detail'][$index]['order_line_state'] = $detail['order_line_state'];
            $data['detail'][$index]['price'] = sprintf('%.02f', $detail['price']);
            $data['detail'][$index]['price_unit'] = $detail['price_unit'];
            $data['detail'][$index]['quantity'] = $detail['quantity'];
            $data['detail'][$index]['shipping_price'] = sprintf('%.02f', $detail['shipping_price']);
            $data['detail'][$index]['product_sku'] = $product_sku;
            $data['detail'][$index]['product_title'] = $detail['product_title'];
            $data['detail'][$index]['status'] = $status;
            $data['detail'][$index]['date_add'] = $this->date_time;
        } // end loop line
//        if (empty($data['detail'])) {
//            $skipped[] = array('order', $order_id, 'An order %s is not product', $order_id);
//            return array('F' => 'N', 'S' => $this->E, 'D' => '', 'SP' => $skipped);
//        }


        if (!empty($skipped)) {
            $data['skipped'] = $skipped;
        }

        $id_order_accept = $this->mirakl_order->saveOrderAccept($data);
        $data['id_order_accept'] = $id_order_accept;


        return $data;

        //echo '<pre>'; print_r($data); exit;

        /* return
         * [F] = flag (N | L | R) is 'N no, L lookup, R response'
         * [S] = status (0 | 1) Error, Success
         * [D] = (array) data
         * [SP] = (array) skipped
         * 
         */


        if ($this->is_accept && $this->cron) {
            $response = $this->acceptOrderConfirm($id_order_accept, $data);
            $skipped[] = array('order', $order_id, $response[1], $response[2]);
            return array('F' => 'R', 'S' => $response[0], 'D' => $data, 'SP' => $skipped);
        }

        if ($this->is_accept && $this->is_lookup) {
            $response = $this->acceptOrderConfirm($id_order_accept, $data);
            $skipped[] = array('order', $order_id, $response[1], $response[2]);
            return array('F' => 'R', 'S' => $response[0], 'D' => $data, 'SP' => $skipped);
        }

        if ($this->is_lookup) {
            return array('F' => 'L', 'S' => $this->S, 'D' => $data, 'SP' => $skipped);
        }

        if (!empty($this->order_id) && !$this->is_lookup) {
            $response = $this->acceptOrderConfirm($id_order_accept, $data);
            $skipped[] = array('order', $order_id, $response[1], $response[2]);
            return array('F' => 'R', 'S' => $response[0], 'D' => $data, 'SP' => $skipped);
        }
    }

    public function lookupOrder($data) {

        $return_value = array(
            'status' => MiraklParameter::STATUS_SUCCESS,
            'message' => '',
            'optional' => $data
        );

        return $return_value;
    }

    public function getOrder($order_id = '') {

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];
        $mirakl = new MiraklApiOrders($mirakl_params);

        $parameter = array(
            //'order_state_codes' => 'CLOSED', // for test exit;
            'order_state_codes' => MiraklApiOrders::STATUS_CANCELED, // for test exit;
            //'order_state_codes' => 'REFUSED'  // for test exit;
            //'order_state_codes' => MiraklApiOrders::STATUS_RECEIVED, // for test exit;
            //'order_state_codes' => MiraklApiOrders::STATUS_WAITING_ACCEPTANCE, // use really
            'order_ids' => $order_id
        );

        //log message
        //$this->mirakl_database->log_message($parameter, 'accept');
//print_r($parameter); exit;
        $response = $mirakl->orders($parameter);

        //echo '<pre>'; print_r($response); exit;

        if (empty($response)) {
            return array('F' => 'R', 'S' => $this->E, 'M' => 'No response from web services');
        }

        $xml = simplexml_load_string($response);
        //$xml = file_get_contents('https://mp.thebeautyst.com/api/orders/?api_key=4284ed2c-cf0b-4b90-8794-6aa8cdacae37&order_state_codes=WAITING_ACCEPTANCE');
        //echo '<pre>'; print_r($xml); exit;
        $result = $xml->xpath('//mirakl_orders/orders');
        $xcount = $xml->xpath('//mirakl_orders/total_count');

        if (empty($xcount)) {
            $total_count = 0;
        } else {
            $total_count = (int) $xcount[0];
        }

        if (!$total_count || empty($result)) {
            return array('F' => 'R', 'S' => $this->E, 'M' => 'No new an order to accept');
        }

        return $result;
    }

    public function acceptOrderConfirm($data) { // accept order for update status from waiting.
        $order_id = $data['order_id'];
        $id_order_accept = $data['id_order_accept'];

        $order_info = array();
        foreach ($data['detail'] as $detail) {
            $order_info['order_lines'][] = array(
                'accepted' => true,
                'id' => $detail['order_line_id']
            );
        }


        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];
        $mirakl = new MiraklApiOrders($mirakl_params);

        $response = $mirakl->accept($order_id, $order_info);

        //log message
        //$this->mirakl_database->log_message($response, 'accept_return');

        if (Mirakl_Tools::strlen($response)) { // error
            return array($this->E, 'An order %s is an error', $order_id);
            //return array('status' => MiraklParameter::STATUS_ERROR, 'message' => sprintf($this->lang('An order %s is an error'), $order_id));
        } else {
            $flag_update = $this->mirakl_order->updateFlagOrderAccept($id_order_accept);
            return array($this->S, 'An order %s have accepted success', $order_id);
            //return array('status' => MiraklParameter::STATUS_SUCCESS, 'message' => sprintf($this->lang('An order %s have accepted success'), $order_id));
        }
    }

    public function setLog($log_status, $log_key = null, $log_value = null) {
        $log_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->getBatchId(),
            'feed_type' => MiraklParameter::FEED_TYPE_ACCEPT,
            'count_process' => $this->count_process,
            'count_send' => $this->count_send,
            'count_success' => $this->count_success,
            'count_error' => $this->count_error,
            'count_skipped' => count($this->skipped),
            'count_warning' => $this->count_warning,
            'is_cron' => ($this->cron) ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s')
        );
        //echo '<pre>'; print_r($log_data); exit;
        $id_log = $this->getDatabase()->updateMiraklLog($log_data, $this->skipped);
        $this->skipped = array();
        $this->count_skipped = 0;
        $this->count_process = 0;
        $this->count_send = 0;
        $this->count_success = 0;
        $this->count_error = 0;
        $this->count_warning = 0;
        return $id_log;
    }

}

// end definition
