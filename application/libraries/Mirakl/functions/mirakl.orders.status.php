<?php
require_once dirname(__FILE__).'/../classes/mirakl.order.php';
require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';
require_once dirname(__FILE__).'/../classes/mirakl.api.orders.php';
require_once dirname(__FILE__).'/../classes/mirakl.database.php';
require_once dirname(__FILE__).'/../../../../assets/apps/FeedbizImport/log/RecordProcess.php';

class MiraklOrderStatus {

    private $mirakl_order;
    private $mirakl_database;
    private $skipped = array(); // Format skipped[] = ([0] = refercnce_type, [1] = reference, [2] = log_key, [3] = log_message, [4] = log_message)
    private $process = 0;
    private $success = 0;

    public function __construct($get_config, $debug = false, $cron = false) {
        $this->debug = false;
        $this->cron = $cron;
        $this->get_config = $get_config;
        $this->batch_id = uniqid();
        $this->date_time = date('Y-m-d H:i:s');
        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;

        Mirakl_Tools::load($this->get_config->language);
        $this->mirakl_database = new MiraklDatabase($this->get_config->user_name);
        $this->mirakl_order = new MiraklOrder($this->get_config->user_name);

        if (!$this->debug) {
            $this->startPopup();
        }
    }

    private function startPopup() {
        $popup_data = array();
        $popup_data['action'] = MiraklHistoryLog::ACTION_TYPE_UPDATE_TRACKING;
        $popup_data['batch_id'] = $this->batch_id;
        $popup_data['date_time'] = $this->date_time;
        $popup_data['user_name'] = $this->get_config->user_name;
        $popup_data['ext'] = $this->get_config->ext;
        $popup_data['country'] = $this->get_config->countries;
        $popup_data['marketplace'] = $this->get_config->marketplace;

        $get_popup = MiraklHistoryLog::startPopup($popup_data);
        $this->proc_rep = $get_popup['proc_rep'];
        $this->message = $get_popup['message'];
    }

    private function endTask($status, $message, $set_message = false, $error = true, $optional = array()) {
        $log_date_time = date('Y-m-d H:i:s');

        if ($status == MiraklParameter::STATUS_ERROR) {
            if (isset($optional['log_key']) && isset($optional['log_value'])) {
                $this->updateLog(MiraklParameter::LOG_STATUS_ERROR, $optional['log_key'], $optional['log_value']);
            } else {
                $this->updateLog(MiraklParameter::LOG_STATUS_ERROR, $message);
            }
        } else {
            $this->updateLog(MiraklParameter::LOG_STATUS_SUCCESS);
        }

        if (isset($this->proc_rep) && $set_message) {

            if ($error) { // case error! into this.
                $this->message->no_product = $message;
            } else {
                $this->message->orders->status = 'Success';
                $this->message->orders->message = $message;
            }

            $this->proc_rep->set_status_msg($this->message);
        }

        if (isset($this->proc_rep)) {
            $this->proc_rep->finish_task();
        }

        $return_value = array(
            'status' => $status,
            'message' => $message
        );

        return $return_value;
    }

    private function lang($str) {
        return Mirakl_Tools::l($str);
    }

    private function status($flag) {
        return ($flag == 'E') ? MiraklParameter::STATUS_ERROR : MiraklParameter::STATUS_SUCCESS;
    }

    public function statusOrder() {

        if (isset($this->proc_rep)) {
            $this->message->orders->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        $mirakl_params = MiraklConfig::marketplaceParams($this->get_config->marketplace);
        $mirakl_params['debug'] = $this->debug;
        $mirakl_params['api_key'] = $this->get_config->api_key;

        $mirakl = new MiraklApiOrders($mirakl_params);

        // for update tracking
        $orders_tracking = $this->mirakl_order->getOrderShipping($this->get_config->sub_marketplace, $this->get_config->id_country, $this->get_config->id_shop, $this->id_marketplace);
        
        // get mirakl order accept
        $accept_order = $this->mirakl_order->getAcceptOrder($this->get_config->sub_marketplace, $this->get_config->id_country, $this->get_config->id_shop);

        if (!empty($accept_order)) {
            
            ///////////////////////////////////////////////////////////////////////
            // set filters order_ids gor get api order
            $accept_order_str = '';
            
            $accept_order_arr = array_map(function($params){
                return $params['order_id'];
            }, $accept_order);
            
            if(isset($accept_order_arr) && !empty($accept_order_arr)){
                $accept_order_str = implode(",", $accept_order_arr);
            }
            
            $filters = array(
                'order_ids' => $accept_order_str
            );
            ///////////////////////////////////////////////////////////////////////
            
            // gte api order
            $api_order = $mirakl->orders($filters);
            $xml_order = simplexml_load_string($api_order);

            if (isset($xml_order->orders) && !empty($xml_order->orders)) {
                foreach ($accept_order as $accept) {
                    foreach ($xml_order->orders as $api_order) {
                        
                        $api_order_id = (string) $api_order->order_id;
                        
                        if ($accept['order_id'] == $api_order_id) {

                            $order_state = (string) $api_order->order_state;
                            $shipping_tracking = isset($api_order->shipping_tracking) ? (string) $api_order->shipping_tracking : '';

                            foreach ($api_order->order_lines as $key => $order_line) {
                                $order_line_state[(string) $order_line->order_line_index] = (string) $order_line->order_line_state;
                            }
                            // echo '<pre>'; print_r($accept); exit;
                            $condition = array();
                            $condition['id_order_accept'] = $accept['id_order_accept'];
                            $condition['order_id'] = $accept['order_id'];
                            $condition['order_state'] = $order_state;
                            $condition['tracking_no'] = $shipping_tracking;
                            $condition['line_state'] = $order_line_state;

                            $this->mirakl_order->updateOrderAcceptStatus($this->get_config->sub_marketplace, $this->get_config->id_country, $this->get_config->id_shop, $condition);
                        }
                    }
                }
            }
        }

        if (empty($orders_tracking)) {
            return $this->endTask($this->status('S'), $this->lang('No update status an order'), true);
        }

        
        $order_ship = array(); // get $id_marketplace_order_ref for ship
        foreach ($orders_tracking as $order_tracking) {
            
            $this->process = $this->process + 1;
            $id_marketplace_order_ref = $order_tracking['id_marketplace_order_ref'];
            //sleep(2);
            $order = array();
            $order['carrier_code'] = isset($order_tracking['code']) && !empty($order_tracking['code']) ?  $order_tracking['code'] : '';
            $order['carrier_name'] = !empty($order_tracking['shipment_service']) ? $order_tracking['shipment_service'] : $order_tracking['name'];
            $order['carrier_url'] = '';
            $order['tracking_number'] = isset($order_tracking['tracking_number']) ? $order_tracking['tracking_number'] : '';

            // put update tracking on the marketplace
            $response = $mirakl->tracking($id_marketplace_order_ref, $order);

            if (empty($response)) { // if empty is succeslly
                $this->mirakl_order->updateOrderStatusShipping($id_marketplace_order_ref, $this->get_config->id_shop, $this->id_marketplace, $this->get_config->id_country);
                $order_ship[] = $id_marketplace_order_ref;
                $this->success = $this->success + 1;
            } else {
                $xml = simplexml_load_string($response);

                if (isset($xml->status) && isset($xml->message)) {
                    $msg = (string) $xml->message;
                    $this->skipped[] = array('order', $id_marketplace_order_ref, $msg, '', $msg);
                } else {
                    $this->skipped[] = array('order', $id_marketplace_order_ref, 'Error', '', 'Error');
                }
            }
        }


        // Valid the shipment of the order which is in "SHIPPING" state
        if (isset($order_ship) && !empty($order_ship)) {

            $this->mirakl_database->log_message($order_ship, 'before_order_ship');
            $result_ship = array();
            foreach ($order_ship as $oid) {
                //sleep(2);

                $result_ship = $mirakl->ship($oid); // In put ship not keep into log.
                
                if(empty($result_ship)){ // check success or error.
                    $this->mirakl_database->log_message('success_'.$oid, 'after_order_ship');
                }else{
                    $this->mirakl_database->log_message('error_'.$oid.'_'.$result_ship, 'after_order_ship');
                }
            }
        }
        
        
        return $this->endTask($this->status('E'), $this->lang('Update status an order'), true, false);
    }

    private function updateLog($log_status, $log_key = '', $log_value = '') {

        $log_data = array(
            'sub_marketplace' => (int) $this->get_config->sub_marketplace,
            'id_country' => (int) $this->get_config->id_country,
            'id_shop' => (int) $this->get_config->id_shop,
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_UPDATE,
            'count_process' => $this->process,
            'count_send' => 0,
            'count_success' => $this->success,
            'count_error' => 0,
            'count_skipped' => isset($this->skipped) ? count($this->skipped) : 0,
            'count_warning' => 0,
            'is_cron' => $this->cron ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s')
        );

        $id_log = $this->mirakl_database->updateMiraklLog($log_data, $this->skipped);
        
        return $id_log;
    }

}

// end definition   