<?php

require_once dirname(__FILE__).'/../../FeedBiz.php';
require_once dirname(__FILE__).'/../classes/mirakl.webservice.class.php';
require_once dirname(__FILE__).'/../classes/mirakl.order.php';
require_once dirname(__FILE__).'/../classes/mirakl.tools.php';
require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';
require_once dirname(__FILE__).'/../classes/mirakl.parameter.php';
require_once dirname(__FILE__).'/../classes/mirakl.scheme.php';
require_once dirname(__FILE__).'/../classes/mirakl.api.messages.php';
require_once dirname(__FILE__).'/../../../../assets/apps/FeedbizImport/log/RecordProcess.php';

class MiraklMessages {

    private $mirakl_order;
    private $user_name;
    public $debug;

    public function __construct($user_name, $debug = false) {
        $this->user_name = $user_name;
        $this->mirakl_order = new MiraklOrder($this->user_name);
        $this->mirakl_scheme = new MiraklScheme();
        $this->debug = $debug;
    }

    public function getIdMarketplace() {
        return MiraklParameter::MARKETPLACE_ID;
    }

    public function getMessage($params) {

        // init parms
        $sub_marketplace = $params['sub_marketplace'];
        $id_country = $params['id_country'];
        $marketplace = $params['marketplace'];

        $params_api = array(
            'start_date' => $params['start_date'] // yyyy-mm-dd'T'HH:mm:ss'Z'
        );

//      const ATOM = "Y-m-d\TH:i:sP";
//	const COOKIE = "l, d-M-Y H:i:s T";
//	const ISO8601 = "Y-m-d\TH:i:sO";
//	const RFC822 = "D, d M y H:i:s O";
//	const RFC850 = "l, d-M-y H:i:s T";
//	const RFC1036 = "D, d M y H:i:s O";
//	const RFC1123 = "D, d M Y H:i:s O";
//	const RFC2822 = "D, d M Y H:i:s O";
//	const RFC3339 = "Y-m-d\TH:i:sP";
//	const RSS = "D, d M Y H:i:s O";
//	const W3C = "Y-m-d\TH:i:sP";

        $api_key = $this->mirakl_scheme->getApiByUserName($this->user_name, $sub_marketplace, $id_country);

        if ($this->debug) {
            echo ('Marketplace : <pre>'.print_r($marketplace, true).'</pre>');
            echo ('Mirakl API : <pre>'.print_r($api_key, true).'</pre>');
        }

        if (empty($api_key)) {
            return array();
        }

        $mirakl_params = MiraklConfig::marketplaceParams($marketplace);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $api_key;

        if ($this->debug) {
            echo ('Mirakl Params : <pre>'.print_r($mirakl_params, true).'</pre>');
        }

        $message = new MiraklApiMessages($mirakl_params);
        $response = $message->messages($params_api);

        if ($this->debug) {
            echo ('Mirakl Response : <pre>'.print_r($response, true).'</pre>');
        }

        $xml = simplexml_load_string($response);

        $total_count = (int) $xml->total_count;
        $results = $xml->xpath('messages');

        if ($this->debug) {
            echo ('total count : <pre>'.print_r($total_count, true).'</pre>');
            echo ('Mirakl messages : <pre>'.print_r($results, true).'</pre>');
        }

        if ($total_count <= 0) {
            return array();
        }

        $messages = array();
        foreach ($results as $key => $result) {
            $form_type = (string) $result->from_type;
            $order_id = (string) $result->order_id;
            
            $buyer = $this->mirakl_order->getOrderBuyerNameEmail($order_id, $sub_marketplace, $id_country);

            if ($form_type == 'CUSTOMER' && !empty($buyer)) {
                $key = ($key + 1).'';
                $messages[$key]['subject'] = (string) $result->subject;
                $messages[$key]['from'] = $buyer['name'].' <'.$buyer['email'].'>';;
                $messages[$key]['date'] = (string) date('Y-m-d H:i:s', strtotime($result->date_created));
                $messages[$key]['seen'] = (string) $result->read;
                $messages[$key]['message_id'] = (string) $result->id;
                $messages[$key]['body'] = (string) $result->body;
                $messages[$key]['mp_order_id'] = $order_id;
            }
        }

        if ($this->debug) {
            echo ('Mirakl messages : <pre>'.print_r($messages, true).'</pre>');
        }
       
        return $messages;
    }

}
