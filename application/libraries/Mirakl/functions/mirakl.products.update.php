<?php

require_once dirname(__FILE__).'/../classes/mirakl.lib.abstract.php';
require_once dirname(__FILE__).'/../../eucurrency.php';

class MiraklProductUpdate extends MiraklTask implements iProduct {

    private $reports = array();
    private $reports_detail = array();
    private $path_download_offer;
    private $path_export_update;
    private $file_update;
    private $file_error;
    private $operation;
    private $id_products = array(); // for update flag
    private $id_marketplace;
    private $set_time = 10; // secound
    private $set_max_loop = 15; // secound

    public function __construct($config, $cron = false) {
        parent::__construct($config['user_name']);

        $this->config = $config;
        $this->cron = $cron;
        $this->batch_id = $this->getBatchId();
        $this->date_time = $this->getDateTime();

        $this->path_download_offer = $this->getPathMirakl().'/download/offers';
        $this->path_export_update = $this->getPathMirakl().'/export/update';
        $this->file_update = $this->getFileName($config['marketplace'], $config['ext']).'-product-update.csv';
        $this->file_error = '/offers-import-error-report-';

        Mirakl_Tools::load($config['language']);
        $this->setLang($config['iso_code']);
        $this->id_marketplace = MiraklParameter::MARKETPLACE_ID;

        if (!isset($config['download'])) {
            $this->setPopup();
        }
    }

    private function setPopup() {
        $context = array();
        $context['action'] = self::ACTION_PRODUCT_UPDATE;
        $context['batch_id'] = $this->batch_id;
        $context['date_time'] = $this->date_time;
        $context['user_name'] = $this->config['user_name'];
        $context['ext'] = $this->config['ext'];
        $context['country'] = $this->config['countries'];
        $context['marketplace'] = $this->config['marketplace'];

        $popup = $this->startPopup($context);
        $this->proc_rep = $popup['proc_rep'];
        $this->message = $popup['message'];
    }

    private function stopTask($message, $log_key = '', $log_value = null) {

        if (empty($log_key)) {
            $message = $this->lang($message);
            $log_key = $message;
        }

        $this->setLog(MiraklParameter::LOG_STATUS_ERROR, $log_key, $log_value);

        if (isset($this->proc_rep)) {
            $this->message->no_product = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_ERROR, 'message' => $message);
    }

    private function finishTask($message) {

        $this->setLog(MiraklParameter::LOG_STATUS_SUCCESS);

        if (isset($this->proc_rep)) {
            $this->message->product->status = 'Success';
            $this->message->product->message = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_SUCCESS, 'message' => $message);
    }

    public function processProduct($params = array(), $download = false) {

        $to_mirakl = $data = array();
        $currencies = new Eucurrency();
        $this->download = $download;

        /* set init params */
        $export_all = ($params['export_all'] == 1) ? true : false;
        $this->operation = ($params['purge'] == 1) ? MiraklParameter::OFFER_REPLACE : MiraklParameter::OFFER_NORMAL;
        $send_mirakl = ($params['send_mirakl'] == 1) ? true : false;

        if (isset($this->proc_rep)) {
            $this->message->offers->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        /* get selected Categories */
        $default_categories = $this->getDatabase()->getMiraklCategorySelect($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);

        if (empty($default_categories)) {
            return $this->stopTask('You must configure the categories to update');
        }

        /* get profile */
        $default_profiles = $this->getDatabase()->getMiraklProfile($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);

        if (empty($default_profiles)) {
            return $this->stopTask('You must configure the profiles to update');
        }

        $id_categories = array_map(function($params) {
            return $params['id_category'];
        }, $default_categories);

        $default_profiles2categories = array_map(function($params) {
            return $params['id_profile'];
        }, $default_categories);

        unset($default_categories);

        /* get product in category selected */
        $products = $this->getDatabase()->getProductIdInCategoryByFlag($this->config['id_shop'], $id_categories, $this->config['id_country'], $this->config['sub_marketplace'], $this->id_marketplace, $export_all);

        if (empty($products)) {
            return $this->stopTask('No product to update');
        }

        $products_conditions = $this->getDatabase()->getSettingCondition($this->config['id_shop']);
        $products_mapping_condition = $this->getDatabase()->getSettingMappingCondition($this->config['id_shop'], $this->id_marketplace);

        $list_id_product = array_map(function($params) {
            return $params['id_product'];
        }, $products);

        // set limit memory
        ini_set('memory_limit', '-1');

        // get product data by list id product
        $objProduct = new Product($this->config['user_name']);
        $list_products = $objProduct->exportProductsArray($this->config['id_shop'], null, $this->config['id_lang'], null, $list_id_product);

        foreach ($products as $position => $product) {

            // check id_product
            if (!isset($product['id_product']) || empty($product['id_product'])) {
                continue;
            }

            $id_product = $product['id_product'];
            $this->count_process = $this->count_process + 1;

            if (!isset($list_products[$id_product])) { // how happened ?
                $this->skipped[] = array('id product', $id_product, 'missing list product', '', $this->lang('missing list product'));
                continue;
            }

            $details = $list_products[$id_product];

            // check id_category
            if (!isset($details['id_category_default']) || empty($details['id_category_default'])) {
                $this->skipped[] = array('id product', $id_product, 'missing id category', '', $this->lang('missing id category'));
                continue;
            }

            // id_category 
            $id_category = $details['id_category_default'];

            // get id profile
            $id_profile = isset($default_profiles2categories[$id_category]) ? (int) $default_profiles2categories[$id_category] : '';

            if (isset($id_profile) && !empty($id_profile)) {
                $profiles = isset($default_profiles[$id_profile]) ? $default_profiles[$id_profile] : null;
            }

            // check $profiles
            if (!isset($profiles) || empty($profiles)) {
                $this->skipped[] = array('id product', $id_product, 'missing profile', '', $this->lang('missing profile'));
                continue;
            }

            // set configuration
            $profile_long_attr = isset($profiles['combinations_long_attr']) && $profiles['combinations_long_attr'] ? true : false;
            $profile_price_rule = isset($profiles['price_rule']) ? $profiles['price_rule'] : '';
            $rounding = isset($profiles['rounding']) ? (int) $profiles['rounding'] : 2;
            $min_quantity_alert = isset($profiles['min_quantity_alert']) && !empty($profiles['min_quantity_alert']) ? $profiles['min_quantity_alert'] : '';
            $logistic_class = isset($profiles['logistic_class']) ? $profiles['logistic_class'] : '';
            $synchronization_field = isset($profiles['synchronization_field']) ? $profiles['synchronization_field'] : '';
            $additionnal = isset($profiles['other']) ? $profiles['other'] : '';

            $data['additionnal'] = $additionnal;

            foreach ($details['currency'] as $currency) {
                $from_currency = $currency['iso_code'];
            }

            // reference
            $data['parent_reference'] = isset($details['reference']) && !empty($details['reference']) ? $details['reference'] : '';

            // features
            $data['product_features'] = isset($details['feature']) && !empty($details['feature']) ? $details['feature'] : '';

            // product type
            if (!in_array($synchronization_field, array(MiraklParameter::SYNC_FIELD_EAN, MiraklParameter::SYNC_FIELD_UPC, MiraklParameter::SYNC_FIELD_SKU))) {
                $synchronization_field = MiraklParameter::SYNC_FIELD_EAN;
            }
            $data['product_type'] = $synchronization_field;

            // The name field is too long (128 chars max).
            $product_name = isset($details['name']) ? $this->getLang($details['name']) : '';
            $product_name = Mirakl_Tools::encodeText($product_name, true);

            $data['product_name'] = $product_name;
            $data['internal_description'] = sprintf('Offer Update for %s, %s', $product_name, date('Y-m-d H:i:s'));

            // Manufacturer
            $data['brand'] = isset($details['manufacturer'][$details['id_manufacturer']]) ? $details['manufacturer'][$details['id_manufacturer']] : '';

            //supplier
            if (isset($details['supplier'])) {
                $supplier = reset($details['supplier']); // into first array
                $supplier = isset($supplier['supplier_reference']) && !empty($supplier['supplier_reference']) ? $supplier['supplier_reference'] : '';
            } else {
                $supplier = '';
            }
            $data['supplier'] = $supplier;

            // mapping condition
            $id_condition = isset($details['id_condition']) && !empty($details['id_condition']) ? $details['id_condition'] : '';
            $name_condition = isset($products_conditions[$id_condition]) && !empty($products_conditions[$id_condition]) ? $products_conditions[$id_condition] : '';
            $condition = isset($products_mapping_condition[$name_condition]) && !empty($products_mapping_condition[$name_condition]) ? $products_mapping_condition[$name_condition] : 11; // 11 hard code !!!!!!!!!
            $data['state'] = $condition;

            $data['logistic_class'] = $logistic_class;
            $data['min_quantity_alert'] = $min_quantity_alert;
            $data['id_product'] = $id_product; // get for update flag to send.
            // get attributes
            $has_attribute = isset($details['has_attribute']) && $details['has_attribute'] > 0 ? true : false;

            // check have combination if not exists is set parent.
            $combinations = array();
            if (!isset($details['combination']) && empty($details['combination']) && !$has_attribute) {
                $combinations[0]['reference'] = $details['reference'];
                $combinations[0]['ean13'] = $details['ean13'];
                $combinations[0]['id_product_attribute'] = 0;
                $combinations[0]['attributes'] = '';
                $combinations[0]['price'] = $details['price'];
                $combinations[0]['quantity'] = $details['quantity'];
            } else {
                $combinations = $details['combination'];
            }

            // Grouping Combinations
            asort($combinations);

            foreach ($combinations as $combination) {

                $reference = $combination['reference'];
                $ean13 = $combination['ean13'];
                $key_chlid = !empty($reference) ? $reference : (!empty($ean13) ? $ean13 : $id_product);

                $attributes = array();
                $attributes_description = array();
                if (isset($combination['attributes']) && !empty($combination['attributes'])) {
                    foreach ($combination['attributes'] as $id_attribute_group => $attribute) {
                        $id_attribute = key($attribute['value']);

                        $attributes[$id_attribute_group]['attribute_group_name'] = isset($attribute['name']) ? $this->getLang($attribute['name']) : '';
                        $attributes[$id_attribute_group]['id_attribute'] = $id_attribute;
                        $attributes[$id_attribute_group]['attribute_name'] = isset($attribute['value'][$id_attribute]['name']) ? $this->getLang($attribute['value'][$id_attribute]['name']) : '';

                        // attributes_description is define
                        $attributes_description[] = isset($attribute['name']) ? '('.$this->getLang($attribute['name']).' '.$this->getLang($attribute['value'][$id_attribute]['name']).')' : '';
                    }
                }

                $data['attributes'] = $attributes;

                $attribute_description = !empty($attributes_description) ? implode(", ", $attributes_description) : '';
                $attribute_description = Mirakl_Tools::encodeText($attribute_description, true);

                if (!empty($attribute_description)) { // set key attribute in log.
                    $key_attribute = $attribute_description;
                } else {
                    $key_attribute = $product_name; // not have attribute, set product name to key log.
                }

                // check reference
                if (!isset($combination['reference'])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s missing reference', $key_attribute, sprintf($this->lang("product %s missing reference"), $key_attribute));
                    continue;
                }
                $data['reference'] = $combination['reference'];

                // check ean13
                if (!isset($combination['ean13'])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s missing ean13', $key_attribute, sprintf($this->lang("product %s missing reference"), $key_attribute));
                    continue;
                }

                if (!Mirakl_Tools::eanupcCheck($combination['ean13'])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product %s wrong ean13', $key_attribute, sprintf($this->lang('product %s wrong ean13'), $key_attribute));
                    //continue; //debug for test
                }
                $data['ean13'] = $combination['ean13'];

                if ($combination['ean13'] && isset($history_ean13[$combination['ean13']])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product ean13 %s duplicate', $key_attribute, sprintf($this->lang('product ean13 %s duplicate'), $key_attribute));
                    continue;
                }
                $history_ean13[$combination['ean13']] = true;

                if ($combination['reference'] && isset($sku_history[$combination['reference']])) {
                    $this->skipped[] = array('reference', $key_chlid, 'product reference %s duplicate', $key_attribute, sprintf($this->lang('product reference %s duplicate'), $key_attribute));
                    continue;
                }
                $sku_history[$combination['reference']] = true;

                $data['quantity'] = ($details['active'] == 0) ? 0 : $combination['quantity'];

                // Price		   
                $price = $combination['price'];
                // price rule
                if (isset($profile_price_rule) && !empty($profile_price_rule)) {
                    $price = Mirakl_Tools::price_rule($profile_price_rule, $rounding, $price);
                }

                // convert price
                $price = $currencies->doDiffConvert($price, $from_currency, $this->config['currency']);
                $data['price'] = (float) round($price, $rounding);

                //weight
                $data['weight'] = isset($combination['weight']['value']) && !empty($combination['weight']['value']) ? $combination['weight']['value'] : '';

                $to_mirakl[$position]['sku'] = $data['reference']; // id have combi reference else parent reference
                $to_mirakl[$position]['product-id'] = sprintf('%013s', trim($data['ean13'])); // id have combi ean13 else parent ean13 // sprintf('%013s', $product_id);
                $to_mirakl[$position]['product-id-type'] = $data['product_type'];
                $to_mirakl[$position]['description'] = ''; //$data['description'];
                $to_mirakl[$position]['internal-description'] = $data['internal_description'];
                $to_mirakl[$position]['price'] = sprintf('%.02f', $data['price']);
                $to_mirakl[$position]['price-additional-info'] = '';
                $to_mirakl[$position]['quantity'] = $data['quantity'];
                $to_mirakl[$position]['min-quantity-alert'] = $data['min_quantity_alert'];
                $to_mirakl[$position]['state'] = $data['state'];

                if (MiraklParameter::OFFER_NO_EXPIRE) {
                    $to_mirakl[$position]['available-start-date'] = '';
                    $to_mirakl[$position]['available-end-date'] = '';
                } else {
                    $to_mirakl[$position]['available-start-date'] = date('Y-m-d', strtotime('now'));
                    $to_mirakl[$position]['available-end-date'] = date('Y-m-d', strtotime('now + 30 day'));
                }

                $to_mirakl[$position]['discount-price'] = '';
                $to_mirakl[$position]['discount-start-date'] = '';
                $to_mirakl[$position]['discount-end-date'] = '';
                $to_mirakl[$position]['discount-ranges'] = '';
                $to_mirakl[$position]['leadtime-to-ship'] = '';
                $to_mirakl[$position]['update-delete'] = ($data['quantity'] <= 0) ? MiraklParameter::OFFER_DELETE : MiraklParameter::OFFER_UPDATE;

                if (!empty($data['logistic_class'])) {
                    $to_mirakl[$position]['logistic-class'] = $data['logistic_class'];
                }

                if (isset($data['additionnal']) && !empty($data['additionnal']) && is_array($data['additionnal'])) {
                    foreach ($data['additionnal'] as $field_name => $addition) {

                        if ($addition['option'] == MiraklParameter::DEFAULT_VALUE) {
                            $to_mirakl[$position][$field_name] = isset($addition['text']) ? Mirakl_Tools::encodeText($addition['text'], true) : '';
                        } else {

                            $selected_field = explode('-', $additionnal[$field_name]['option']);
                            $field_type = isset($selected_field[0]) ? $selected_field[0] : '';
                            $field_id = isset($selected_field[1]) ? $selected_field[1] : '';

                            switch ($field_type) {
                                case 'a'://attributes
                                    if (isset($data['attribute']) && !empty($data['attribute']) && array_key_exists($field_id, $data['attribute'])) {
                                        $to_mirakl[$position][$field_name] = $data['attribute'][$field_id]['attribute_name'];
                                    }
                                    break;
                                case 'f'://features
                                    if (isset($data['product_features']) && isset($data['product_features'][$field_id]) && isset($data['product_features'][$field_id]['value']) && isset($data['product_features'][$field_id]['value'][$this->lang])) {
                                        $to_mirakl[$position][$field_name] = $data['product_features'][$field_id]['value'][$this->lang];
                                    } else {
                                        $to_mirakl[$position][$field_name] = '';
                                    }
                                    break;
                                case 'p'://prestashop field
                                    switch ($field_id) {
                                        case MiraklParameter::REFERENCE:
                                            $value = $data['reference']; //$reference;
                                            break;
                                        case MiraklParameter::SUPPLIER_REFERENCE:
                                            $value = $data['supplier']; //$supplier_reference;
                                            break;
                                        case MiraklParameter::MANUFACTURER:
                                            $value = $data['brand']; //Manufacturer::getNameById($details->id_manufacturer);
                                            break;
                                        case MiraklParameter::CATEGORY:
                                            $value = $category_name;
                                            break;
//                                case MiraklParameter::META_TITLE:
//                                    $value = $combination['meta_title'];
//                                    break;
//                                case MiraklParameter::META_DESCRIPTION:
//                                    $value = $combination['meta_description'];
//                                    break;
//                                case MiraklParameter::UNITY:
//                                    $value = $combination['unity'];
//                                    break;
                                        case MiraklParameter::WEIGHT:
                                            $value = sprintf('%.02f', $data['weight']);
                                            break;
                                        default:
                                            $value = null;
                                            break;
                                    }
                                    if (Mirakl_Tools::strlen($value)) {
                                        $to_mirakl[$position][$field_name] = $value;
                                    }
                                    break;
                            }
                        }
                    }
                }

                if (!$this->download) {
                    $product_flag = isset($to_mirakl[$position]['product-id']) ? $to_mirakl[$position]['product-id'] : $data['reference'];
                    $this->id_products[$product_flag] = $data['id_product'];
                }
            }
        } // end loop products

        if ($send_mirakl) {
            $to_mirakl = $this->checkExistsProducts($to_mirakl);
        }

        if (empty($to_mirakl) || !is_array($to_mirakl)) {
            return $this->stopTask('No product update into marketplace');
        }

        // create csv file
        $export = $this->exportProduct($to_mirakl);

        if ($export['count'] == 0) {
            return $this->stopTask('Product update failed');
        }

        //$send_mirakl = 0; // now, product no send exit;
        if ($send_mirakl) {
            $import_id = $this->uploadFile($export['filename']);
            $this->setLogReport();
            $this->updateFlagOffer($to_mirakl); // update flag offers, params pass by reference
            $count = count($to_mirakl);
        } else {
            $count = $export['count'];
        }

        $this->count_success = $count;

        // distory variable
        unset($default_categories);
        unset($default_profiles);
        unset($default_profiles2categories);
        unset($products);
        unset($list_id_product);
        unset($list_products);
        unset($combinations);
        unset($data);
        unset($to_mirakl);

        // finish task
        return $this->finishTask(sprintf($this->lang('Products have created %s offers'), $count));
    }

    public function exportProduct($data = array()) {

        $count_row_csv = 0;
        $keys = array();
        $file = $this->path_export_update.'/'.$this->file_update;

        if (!($fp = fopen($file, 'w+'))) {
            return $this->stopTask('Unable to write in file');
        }

        /* get key into title */
        foreach ($data as $CSV) {
            $keys = array_unique(array_merge($keys, array_keys($CSV)));
        }
        fputcsv($fp, $keys, $this->separator);

        foreach ($data as $CSV) {
            $count_row_csv = $count_row_csv + 1;
            fputcsv($fp, array_merge(array_fill_keys($keys, null), $CSV), $this->separator);
        }
        fclose($fp);

        if (file_exists($file)) {
            $perms = (int) substr(sprintf('%o', fileperms($file)), -4);
            if ($perms != 777 && $perms != 666) {
                chmod($file, 0777);
            }
        }

        // download offer will return file name and exit.
        if ($this->download && file_exists($file)) {
            echo $this->file_update;
            exit;
        }

        return array('count' => $count_row_csv, 'filename' => $file);
    }

    public function uploadFile($filename = '') {

        require_once dirname(__FILE__).'/../classes/mirakl.api.offers.php';
        require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $api_offers = new MiraklApiOffers($mirakl_params);
        $params = array('file' => $filename, 'import_mode' => $this->operation);
        $response = $api_offers->imports($params);
        $xml = simplexml_load_string($response);

        $import_id = '';
        if (isset($xml->import_id) && !empty($xml->import_id)) {
            $import_id = (int) $xml->import_id;

            $this->reports = $this->getReport($import_id);
            $this->reports_detail = $this->getReportDetail($import_id);
        }

        return $import_id;
    }

    public function getReport($import_id = '') {

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $offers = new MiraklApiOffers($mirakl_params);
        $msg = '';
        $count_check = 0;
        do {
            sleep($this->set_time); // set time delay
            $msg = $offers->imports_info($import_id);
            $msg = json_decode($msg);
            $count_check = $count_check + 1;
        } while ($msg->status != 'COMPLETE' && $count_check <= $this->set_max_loop); // the first time send offer status = WAITING, if status = 'COMPLETE' it work!

        if (!empty($msg) && ($msg->has_error_report) && ($msg->lines_in_error != 0)) { // have error ?
            $api_error_report = $offers->get_api_error_report($msg->import_id);

            $src = fopen($api_error_report, 'r');
            $dest1 = fopen($this->path_download_offer.$this->file_error.$msg->import_id.'.csv', 'w');

            stream_copy_to_stream($src, $dest1); // copy erro file from mirakl into feedbiz 
        }

        if (!($count_check <= $this->set_max_loop)) {
            $this->error = Mirakl_Tools::l('Incorrect status');
            if (isset($this->proc_rep)) {
                $this->message->no_product = $this->error;
                $this->proc_rep->set_status_msg($this->message);
            }
        }

        return $msg;
    }

    public function getReportDetail($import_id = '') {
        $file = $this->path_download_offer.$this->file_error.$import_id.'.csv';

        if (!file_exists($file)) {
            return;
        }

        $datas = array();
        if (($handle = fopen($file, "r")) === FALSE) {
            return;
        }

        $index = 0;
        while (($cols = fgetcsv($handle, 3000, ";")) !== FALSE) {

            if ($index == 0) { // title csv.
                $key_sku = array_search('sku', $cols) ? array_search('sku', $cols) : 0; // get key sku.
                $key_error = array_search('errors', $cols) ? array_search('errors', $cols) : array_search(end($cols), $cols); // get ket errors.
                $key_warning = array_search('warnings', $cols) ? array_search('warnings', $cols) : ''; // get ket errors.
            } else {
                $report_text[$index - 1]['rererence'] = isset($cols[$key_sku]) ? $cols[$key_sku] : '';
                $report_text[$index - 1]['error'] = isset($cols[$key_error]) ? $cols[$key_error] : '';
                $report_text[$index - 1]['warning'] = isset($cols[$key_warning]) ? $cols[$key_warning] : '';
            }
            $index++;
        }

        //echo '<pre>'; print_r($report_text); exit;
        return $report_text;
    }

    public function setLog($log_status, $log_key = null, $log_value = null) {
        $log_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_OFFER,
            'count_process' => $this->count_process,
            'count_send' => $this->count_send,
            'count_success' => $this->count_success,
            'count_error' => $this->count_error,
            'count_skipped' => count($this->skipped),
            'count_warning' => $this->count_warning,
            'is_cron' => ($this->cron) ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s'),
        );

        $id_log = $this->getDatabase()->updateMiraklLog($log_data, $this->skipped);
        return $id_log;
    }

    public function setLogReport() {
        if (empty($this->reports)) {
            return false;
        }

        $reports = (object) $this->reports;
        $report_detail = isset($this->reports_detail) && !empty($this->reports_detail) ? $this->reports_detail : array();

        $detail = array();
        $detail['ReportImportId'] = isset($reports->import_id) ? (int) $reports->import_id : 0; // Number
        $detail['ReportProcess'] = isset($reports->lines_read) ? (int) $reports->lines_read : 0; // Number
        $detail['ReportSuccess'] = isset($reports->lines_in_success) ? (int) $reports->lines_in_success : 0; // Number
        $detail['ReportError'] = isset($reports->lines_in_error) ? (int) $reports->lines_in_error : 0; // Number
        $detail['ReportWarning'] = 0;
        $detail['ReportMode'] = isset($reports->mode) ? (string) $reports->mode : ''; //  "NORMAL" | "PARTIAL_UPDATE" | "REPLACE",
        $detail['ReportStatus'] = isset($reports->status) ? (string) $reports->status : ''; // "WAITING_SYNCHRONIZATION_PRODUCT" | "WAITING" | "RUNNING" | "COMPLETE" | "FAILED" | "QUEUED"
        $detail['ReportOfferDelete'] = isset($reports->offer_deleted) ? (int) $reports->offer_deleted : 0; // Number
        $detail['ReportOfferInsert'] = isset($reports->offer_inserted) ? (int) $reports->offer_inserted : 0; // Number
        $detail['ReportOfferUpdate'] = isset($reports->offer_updated) ? (int) $reports->offer_updated : 0; // Number
        $detail['ReportHasError'] = isset($reports->has_error_report) ? (string) $reports->has_error_report : ''; // Boolean
        $detail['ReportDateCreate'] = isset($reports->date_created) ? (string) $reports->date_created : ''; // Date

        $report_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_OFFER,
            'detail' => serialize($detail),
            'date_add' => date('Y-m-d H:i:s'),
        );

        $id_log_report = $this->getDatabase()->saveMiraklLogReport($report_data, $report_detail);
    }

    public function downloadFile($filename = '') {
        $link_download = $this->path_export_update.'/'.$filename;
        $text = file_get_contents($link_download);
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.$this->file_update.'"');
        echo $text;
    }

    private function updateFlagOffer(&$to_mirakl) {
        $id_products = array(); // for get id_products as check exists in mirakl server.
        foreach ($to_mirakl as $data) {
            $id_products[] = isset($this->id_products[$data['product-id']]) ? $this->id_products[$data['product-id']] : '';
        }

        if (!empty($id_products)) { // !empty $id_products and send = true for update flag send.
            $ImportLog = '';
            $ImportLog .= ImportLog::update_flag_offers_general($this->id_marketplace, $this->config['id_country'], $this->config['sub_marketplace'], $this->config['id_shop'], $id_products);
            if (isset($ImportLog) && !empty($ImportLog)) {
                $result = $this->getDatabase()->exec_query($ImportLog);
            }
        }
    }

    private function checkExistsProducts($to_mirakl) {

        if (empty($to_mirakl)) {
            return $to_mirakl;
        }

        $reference_product = array();
        $reference_product_package = array();
        $results = array();
        $no_products_db = array();
        $error = '';

        $get_products_db = $this->getDatabase()->getMiraklProduct($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop']);


        if (!empty($get_products_db)) {
            foreach ($to_mirakl as $data) {
                $vetify = false;

                foreach ($get_products_db as $products_db) {

                    $product_id = $products_db['product_id'];
                    $product_id_type = $products_db['product_id_type'];

                    if ($data['product-id'] == $product_id && $data['product-id-type'] == $product_id_type) { // if exists
                        $vetify = true;
                        break;
                    }
                }

                if (!$vetify) { // not have product in db.
                    $no_products_db[] = $data;
                } else {
                    $results[] = $data; // have product in db.
                }
            }
        } else {
            $no_products_db = $to_mirakl;
        }

        if (empty($no_products_db)) { // true if all product have in db.
            return $results;
        }

        $max_ref = 100;
        foreach ($no_products_db as $setdata) {
            $reference_product[] = $setdata['product-id-type'].'|'.$setdata['product-id'];
            $max_ref--;
            if ($max_ref == 0) {
                $reference_product_package[] = $reference_product;
                $max_ref = 100;
                unset($reference_product);
            }
        }

        if (!empty($reference_product)) {
            $reference_product_package[] = $reference_product;
        }

        require_once(dirname(__FILE__).'/../classes/mirakl.api.products.php');
        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $api = new MiraklApiProducts($mirakl_params);

        $api_products = array();
        set_time_limit(480);
        foreach ($reference_product_package as $k => $package) {

            $reference = 'product_references=';
            $reference.= implode(',', $package);

            /* Note: this resource return 100 products maximum */
            /* example:  https://moa-recette.mirakl.net/api/products/?api_key=c140c926-e4ae-46a2-86d9-cf1d8dbb2e3c&product_references=EAN|3700372700111,EAN|3700372700444 */
            /* example : https://drt-prod.mirakl.net/api/products/?api_key=3c67b2c5-ca6a-4800-b931-f2533ccfe5b5&product_references=EAN|0849803075118 */

            $response = $api->products_reference($reference);
            $xml = simplexml_load_string($response);

            foreach ($xml->products as $data) {
                $api_products[] = $data;
            }

            unset($response);
            unset($xml);
            unset($reference);
        }
        unset($reference_product_package);

        foreach ($no_products_db as $data) {
            $vetify = false; // check a have products.

            foreach ($api_products as $data_api) {
                $product_id = (string) $data_api->product_id;
                $product_id_type = (string) $data_api->product_id_type;
                if ($data['product-id'] == $product_id && $data['product-id-type'] == $product_id_type) { // if exists
                    $this->getDatabase()->saveMiraklProduct($this->config['sub_marketplace'], $this->config['id_country'], $this->config['id_shop'], (array) $data_api);
                    $vetify = true;
                    break;
                }
            }

            if (!$vetify) {
                $error = sprintf($this->lang('The product does not exist on the %s'), $this->config['marketplace']);
                $this->skipped[] = array('reference', $data['product-id'], 'The product does not exist on the %s', $this->config['marketplace'], $error);
            } else {
                $results[$data['sku']] = $data;
            }
        }
        //echo '<pre>'; print_r($results); exit;
        return $results;
    }

}

//end definition
