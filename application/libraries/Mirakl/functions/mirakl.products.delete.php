<?php

require_once dirname(__FILE__).'/../classes/mirakl.api.offers.php';
require_once dirname(__FILE__).'/../classes/mirakl.lib.abstract.php';

class MiraklDeleteProducts extends MiraklTask implements iProduct {

    private $path_export_delete;
    private $file_create;
    private $reports;
    private $reports_detail;
    private $set_time = 10; // secound
    private $set_max_loop = 15; // secound

    public function __construct($config, $cron = false) {
        parent::__construct($config['user_name']);

        $this->config = $config;
        $this->cron = $cron;
        $this->batch_id = $this->getBatchId();
        $this->date_time = $this->getDateTime();

        $this->path_export_delete = $this->getPathMirakl().'/export/delete';
        $this->file_create = $this->getFileName($config['marketplace'], $config['ext']).'-product-delete.csv';

        Mirakl_Tools::load($config['language']);
        $this->setLang($config['iso_code']);

        if (!isset($config['download'])) {
            $this->setPopup();
        }
    }

    private function setPopup() {
        $context = array();
        $context['action'] = self::ACTION_PRODUCT_DELETE;
        $context['batch_id'] = $this->batch_id;
        $context['date_time'] = $this->date_time;
        $context['user_name'] = $this->config['user_name'];
        $context['ext'] = $this->config['ext'];
        $context['country'] = $this->config['countries'];
        $context['marketplace'] = $this->config['marketplace'];

        $popup = $this->startPopup($context);
        $this->proc_rep = $popup['proc_rep'];
        $this->message = $popup['message'];
    }

    private function stopTask($message, $log_key = '', $log_value = null) {

        if (empty($log_key)) {
            $message = $this->lang($message);
            $log_key = $message;
        }

        $this->setLog(MiraklParameter::LOG_STATUS_ERROR, $log_key, $log_value);

        if (isset($this->proc_rep)) {
            $this->message->no_product = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_ERROR, 'message' => $message);
    }

    private function finishTask($message) {

        $this->setLog(MiraklParameter::LOG_STATUS_SUCCESS);

        if (isset($this->proc_rep)) {
            $this->message->product->status = 'Success';
            $this->message->product->message = $message;
            $this->proc_rep->set_status_msg($this->message);
            $this->proc_rep->finish_task();
        }

        return array('status' => MiraklParameter::LOG_STATUS_SUCCESS, 'message' => $message);
    }

    public function downloadFile($filename = '') {
        $link_download = $this->path_export_delete.'/'.$filename;
        $text = file_get_contents($link_download);
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="'.$this->file_create.'"');
        echo $text;
    }

    public function processProduct($params = array(), $download = false) {

        $to_mirakl = array();
        $offers = array();
        $reasons = array();
        $this->download = $download;

        /* set init params */
        $reason_delete = ($params['reason_delete'] == 1) ? true : false;
        $send_mirakl = ($params['send_mirakl'] == 1) ? true : false;

        if (isset($this->proc_rep)) {
            $this->message->product->message = $this->lang('Begin processing');
            $this->proc_rep->set_status_msg($this->message);
        }

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $this->api_product = new MiraklApiOffers($mirakl_params);
        $response = $this->api_product->offers();
        $xml = simplexml_load_string($response);



        if (((int) $xml->total_count) <= 0) {
            return $this->stopTask('No offer to delete on the marketplace');
        }

        $results = $xml->xpath('//offers');
        $products_active = $this->getDatabase()->getProductAvailable($this->config['id_shop'], $this->config['id_lang']);
        //echo '<pre>'; print_r($products_active); exit;

        /* Reason to deleted
         * 1. In product on mirakl has quantity is 0.
         * 2. In db marketplace_product_option is c_disable != 1 "
         * 3. In product on feedbiz has quantity is 0.
         * 4. In product on mirakl has but In product feedbiz not has.
         * 
         */

        $reasons[0] = Mirakl_Tools::l('The product do not exists in the shop');
        $reasons[1] = sprintf(Mirakl_Tools::l('The product on the %s is 0 quantity'), $this->config['marketplace']);
        $reasons[2] = Mirakl_Tools::l('The product inactive in the shop');
        $reasons[3] = Mirakl_Tools::l('The product disable in the shop');
        $reasons[4] = Mirakl_Tools::l('The product is 0 quantity in the shop');

        foreach ($results as $position => $api) {
            $vertify = false;
            $reason = $reasons[0]; /* reason */

            $api_reference = (string) $api->shop_sku; // reference
            $api_quantity = (string) $api->quantity;


            foreach ($products_active as $active) {
                if ($api_reference == $active['reference']) {

                    if ($active['active'] != 1) { // delete
                        $vertify = false;
                        $reason = $reasons[2]; /* reason */
                    } else if ($active['disable'] != 0) { // delete
                        $vertify = false;
                        $reason = $reasons[3]; /* reason */
                    } else if ($active['sum_quantity'] <= 0) {
                        $vertify = false;
                        $reason = $reasons[4]; /* reason */
                    } else {
                        $vertify = true; // true not delete
                        break;
                    }
                }
            }

            if ($api_quantity == 0) {
                $reason = $reasons[1]; /* reason */
                $vertify = false;
            }

            if (!$vertify) { // delete
                $to_mirakl[$position]['sku'] = $api_reference;
                $to_mirakl[$position]['update-delete'] = 'delete';

                if ($reason_delete) {
                    $to_mirakl[$position]['reason'] = $reason;
                }
            }
        }

        if (empty($to_mirakl) || !is_array($to_mirakl)) {
            return $this->stopTask('No offer to delete in the shop');
        }

        // create csv file
        $export = $this->exportProduct($to_mirakl);

        if ($export['count'] == 0) {
            return $this->stopTask('Product delete failed');
        }

        //$send_mirakl = 1; // now, product delete sent to mirakl alway.
        $send_mirakl = 0;
        if ($send_mirakl) {
            $import_id = $this->uploadFile($export['filename']);
            $this->setLogReport();
            $this->updateFlagOffer($to_mirakl); // update flag offers, params pass by reference
            $count = count($to_mirakl);
        } else {
            //$count = $export['count'];
        }

        $this->count_success = $count;

        // finish task
        return $this->finishTask(sprintf($this->lang('Products have deleted %s product'), $count));
    }

    public function exportProduct($data = array()) {

        $count_row_csv = 0;
        $keys = array();
        $file = $this->path_export_delete.'/'.$this->file_create;

        if (!($fp = fopen($file, 'w+'))) {
            return $this->stopTask('Unable to write in file');
        }

        /* get key into title */
        foreach ($data as $CSV) {
            $keys = array_unique(array_merge($keys, array_keys($CSV)));
        }
        fputcsv($fp, $keys, $this->separator);

        foreach ($data as $CSV) {
            $count_row_csv++;
            fputcsv($fp, array_merge(array_fill_keys($keys, null), $CSV), $this->separator);
        }
        fclose($fp);

        if (file_exists($file)) {
            $perms = (int) substr(sprintf('%o', fileperms($file)), -4);
            if ($perms != 777 && $perms != 666) {
                chmod($file, 0777);
            }
        }

        // download product will return file name and exit.
        if ($this->download && file_exists($file)) {
            echo $this->file_create;
            exit;
        }

        return array('count' => $count_row_csv, 'filename' => $file);
    }

    public function uploadFile($filename = '') {

        require_once dirname(__FILE__).'/../classes/mirakl.api.offers.php';
        require_once dirname(__FILE__).'/../classes/mirakl.config.class.php';

        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $api_offers = new MiraklApiOffers($mirakl_params);
        $params = array('file' => $filename, 'import_mode' => MiraklParameter::OFFER_NORMAL);
        $response = $api_offers->imports($params);
        $xml = simplexml_load_string($response);

        $import_id = '';
        if (isset($xml->import_id) && !empty($xml->import_id)) {
            $import_id = (int) $xml->import_id;

            $this->reports = $this->getReport($import_id);

            /* delete not have error */
            //$this->reports_detail = $this->getReportDetail($import_id);
        }

        return $import_id;
    }

    public function getReport($import_id = '') {
        $mirakl_params = MiraklConfig::marketplaceParams($this->config['marketplace']);
        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $this->config['api_key'];

        $offers = new MiraklApiOffers($mirakl_params);
        $msg = '';
        $count_check = 0;
        do {
            sleep($this->set_time); // set time delay
            $msg = $offers->imports_info($import_id);
            $msg = json_decode($msg);
            $count_check = $count_check + 1;
        } while ($msg->status != 'COMPLETE' && $count_check <= $this->set_max_loop); // the first time send offer status = WAITING, if status = 'COMPLETE' it work!

        /* delete not have error */
//        if (!empty($msg) && ($msg->has_error_report) && ($msg->lines_in_error != 0)) { // have error ?
//            $api_error_report = $offers->get_api_error_report($msg->import_id);
//
//            $src = fopen($api_error_report, 'r');
//            $dest1 = fopen($this->path_download_offer.$this->file_error.$msg->import_id.'.csv', 'w');
//
//            stream_copy_to_stream($src, $dest1); // copy erro file from mirakl into feedbiz 
//        }

        if (!($count_check <= $this->set_max_loop)) {
            $this->error = Mirakl_Tools::l('Incorrect status');
            if (isset($this->proc_rep)) {
                $this->message->no_product = $this->error;
                $this->proc_rep->set_status_msg($this->message);
            }
        }

        return $msg;
    }

    public function getReportDetail($import_id = '') {
//        $file = $this->path_download_offer.$this->file_error.$import_id.'.csv';
//
//        if (!file_exists($file)) {
//            return;
//        }
//
//        $datas = array();
//        if (($handle = fopen($file, "r")) === FALSE) {
//            return;
//        }
//
//        $index = 0;
//        while (($cols = fgetcsv($handle, 3000, ";")) !== FALSE) {
//
//            if ($index == 0) { // title csv.
//                $key_sku = array_search('sku', $cols) ? array_search('sku', $cols) : 0; // get key sku.
//                $key_error = array_search('errors', $cols) ? array_search('errors', $cols) : array_search(end($cols), $cols); // get ket errors.
//                $key_warning = array_search('warnings', $cols) ? array_search('warnings', $cols) : ''; // get ket errors.
//            } else {
//                $report_text[$index - 1]['rererence'] = isset($cols[$key_sku]) ? $cols[$key_sku] : '';
//                $report_text[$index - 1]['error'] = isset($cols[$key_error]) ? $cols[$key_error] : '';
//                $report_text[$index - 1]['warning'] = isset($cols[$key_warning]) ? $cols[$key_warning] : '';
//            }
//            $index++;
//        }
//
//        //echo '<pre>'; print_r($report_text); exit;
//        return $report_text;
    }

    public function setLogReport() {
        if (empty($this->reports)) {
            return false;
        }

        $reports = (object) $this->reports;
        $report_detail = isset($this->reports_detail) && !empty($this->reports_detail) ? $this->reports_detail : array();

        $detail = array();
        $detail['ReportImportId'] = isset($reports->import_id) ? (int) $reports->import_id : 0; // Number
        $detail['ReportProcess'] = isset($reports->lines_read) ? (int) $reports->lines_read : 0; // Number
        $detail['ReportSuccess'] = isset($reports->lines_in_success) ? (int) $reports->lines_in_success : 0; // Number
        $detail['ReportError'] = isset($reports->lines_in_error) ? (int) $reports->lines_in_error : 0; // Number
        $detail['ReportWarning'] = 0;
        $detail['ReportMode'] = isset($reports->mode) ? (string) $reports->mode : ''; //  "NORMAL" | "PARTIAL_UPDATE" | "REPLACE",
        $detail['ReportStatus'] = isset($reports->status) ? (string) $reports->status : ''; // "WAITING_SYNCHRONIZATION_PRODUCT" | "WAITING" | "RUNNING" | "COMPLETE" | "FAILED" | "QUEUED"
        $detail['ReportOfferDelete'] = isset($reports->offer_deleted) ? (int) $reports->offer_deleted : 0; // Number
        $detail['ReportOfferInsert'] = isset($reports->offer_inserted) ? (int) $reports->offer_inserted : 0; // Number
        $detail['ReportOfferUpdate'] = isset($reports->offer_updated) ? (int) $reports->offer_updated : 0; // Number
        $detail['ReportHasError'] = isset($reports->has_error_report) ? (string) $reports->has_error_report : ''; // Boolean
        $detail['ReportDateCreate'] = isset($reports->date_created) ? (string) $reports->date_created : ''; // Date

        $report_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_DELETE,
            'detail' => serialize($detail),
            'date_add' => date('Y-m-d H:i:s'),
        );

        $id_log_report = $this->getDatabase()->saveMiraklLogReport($report_data, $report_detail);
    }

    public function setLog($log_status, $log_key = null, $log_value = null) {
        $log_data = array(
            'sub_marketplace' => $this->config['sub_marketplace'],
            'id_country' => $this->config['id_country'],
            'id_shop' => $this->config['id_shop'],
            'batch_id' => $this->batch_id,
            'feed_type' => MiraklParameter::FEED_TYPE_DELETE,
            'count_process' => $this->count_process,
            'count_send' => $this->count_send,
            'count_success' => $this->count_success,
            'count_error' => $this->count_error,
            'count_skipped' => count($this->skipped),
            'count_warning' => $this->count_warning,
            'is_cron' => ($this->cron) ? 1 : 0,
            'log_status' => $log_status,
            'log_key' => $log_key,
            'log_value' => $log_value,
            'date_add' => date('Y-m-d H:i:s'),
        );

        $id_log = $this->getDatabase()->updateMiraklLog($log_data, $this->skipped);
        return $id_log;
    }

}

//end definition
