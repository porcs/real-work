<?php
require_once dirname(__FILE__) . '/../classes/mirakl.api.account.php';
require_once dirname(__FILE__) . '/../classes/mirakl.config.class.php';
require_once dirname(__FILE__) . '/../classes/mirakl.tools.php';

class MiraklAccount
{
    public function __construct($language) {
        Mirakl_Tools::load($language);
    }

    public function verify($marketplace, $api_key) {
        
        if (!isset($marketplace) || empty($marketplace)) {
            return array('code' => MiraklParameter::STATUS_ERROR, 'msg' => Mirakl_Tools::l('Missing marketplace'));
        }

        if (!isset($api_key) || empty($api_key)) {
            return array('code' => MiraklParameter::STATUS_ERROR, 'msg' => Mirakl_Tools::l('Please provide an api key'));
        }

        $mirakl_params = MiraklConfig::marketplaceParams($marketplace);
        
        $logo = base_url().MiraklParameter::BASE_SETTING.'/'.$mirakl_params['logo'];


        if (empty($mirakl_params) || !is_array($mirakl_params)) {
            return array('code' => MiraklParameter::STATUS_ERROR, 'msg' => sprintf(Mirakl_Tools::l('Please provide an api key %s !'), $marketplace));
        }
        
        if(!isset($mirakl_params['endpoint']) || empty($mirakl_params['endpoint'])){
            return array('code' => MiraklParameter::STATUS_ERROR, 'msg' => sprintf(Mirakl_Tools::l('Please provide an endpoint to %s !'), $marketplace));
        }

        $mirakl_params['debug'] = false;
        $mirakl_params['api_key'] = $api_key;

        $mirakl = new MiraklApiAccount($mirakl_params);
        $response = $mirakl->account();

        if (is_array($response) && isset($response['error_code'])) { // wrong api key
            if ($response['error_code'] == 404) {
                return array("code" => MiraklParameter::STATUS_ERROR, "msg" => sprintf(Mirakl_Tools::l("Failed to connect to %s"), $marketplace));
            } else {
                return array("code" => MiraklParameter::STATUS_ERROR, "msg" => Mirakl_Tools::l($response['error']));
            }
        }

        if (!empty($response)) {
            $result = simplexml_load_string($response);
        } else {
            $result = null;
        }
        
        //echo '<pre>'; print_r(); exit;

        if (isset($result->shop_name)) {
          //  $logo = isset($mirakl_params['logo_32']) ? $mirakl_params['logo_32'] : '';
            return array("code" => MiraklParameter::STATUS_SUCCESS, "msg" => sprintf(Mirakl_Tools::l("Connection to %s successful. Store: %s  Status: %s"), $marketplace, $result->shop_name, $result->shop_state), 'logo' => $logo);
        } else {
            return array("code" => MiraklParameter::STATUS_ERROR, "msg" => sprintf(Mirakl_Tools::l("Failed to connect to %s"), $marketplace));
        }
    }
}