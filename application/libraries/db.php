<?php

if(!defined('DB_VER')){
    include dirname(__FILE__).'/../config/config.php';
}
if(defined('DB_VER') &&  DB_VER == 'DB3'){
    include dirname(__FILE__).'/db3.php';
}elseif(defined('DB_VER') &&  DB_VER == 'mysql'){
    
    include dirname(__FILE__).'/db_mysqli.php';
}else{ 
    include dirname(__FILE__).'/db2.php';
}