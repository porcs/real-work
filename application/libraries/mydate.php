<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Mydate
{
    private $ci;
    
    function __construct()
    {
        $this->ci =& get_instance();
    }    
    
    public function getBillingDate($user_id,$lastPayment)
    {
        $this->ci->load->model('paypal_model','model');
        $this->ci->load->library('authentication');
        
        $user_data = (array)$this->ci->authentication->user($user_id)->row();
        if(is_array($user_data))
        {            
            /*
            * Get Payment Profile 
            */            
            $firstPayment = $this->ci->model->getLastBilling($user_data['id'],'ASC');
            /*
            * End Get 
            */

            if(is_array($firstPayment) && is_array($lastPayment))
            {
                $currentTime = time(); // timestamp of current date
                $dueDay = date('j',strtotime($firstPayment['bill_round'])); // first round of billing day ex: 20                
                $tmpNextMonth = $this->get_x_months_to_the_future(strtotime($lastPayment['bill_created_date'])); // Get Next Month timestamp
                $maxDaysOfMonth = cal_days_in_month(CAL_GREGORIAN, date('n',$tmpNextMonth), date('Y',$tmpNextMonth)); // get max days of the month
                $payDay = $dueDay >= $maxDaysOfMonth?$maxDaysOfMonth:$dueDay; // Set Next Month Pay Day
                $lastPayment = strtotime($lastPayment['bill_created_date']);
                $nextTime = strtotime(date('Y-m',$tmpNextMonth).'-'.$payDay);

                $returnArr = array(
                    'last' => $lastPayment,
                    'next' => $nextTime,
                    'current' => $currentTime,
                    'diff' => $this->getDateDiff($currentTime, $nextTime),
                );
                return $returnArr;
            }

        }
        else
        {
            return null;
        }
    }
    
    public function get_x_months_to_the_future( $base_time = null, $months = 1 )
    {
        if (is_null($base_time))
            $base_time = time();
    
        $x_months_to_the_future = strtotime( "+" . $months . " months", $base_time );
    
        $month_before = (int) date( "m", $base_time ) + 12 * (int) date( "Y", $base_time );
        $month_after = (int) date( "m", $x_months_to_the_future ) + 12 * (int) date( "Y", $x_months_to_the_future );
    
        if ($month_after > $months + $month_before)
            $x_months_to_the_future = strtotime( date("Ym01His", $x_months_to_the_future) . " -1 day" );
    
        return $x_months_to_the_future;
    }
    
    public function getDateDiff($startTimestamp, $endTimestamp)
    {
        $startDate = new DateTime(date('Y-m-d',$startTimestamp));
        $endDate = new DateTime(date('Y-m-d',$endTimestamp));
        
        return $startDate->diff($endDate);
    }
    
    public function getDateLastMonth($initMonth,$length=6)
    {
        /* set month length */
        $length -= 1;
        if($length < 1) 
            $length = 1;
        $length *= -1;
        /* end */
        $arrMonth = array();
        $arrMonth['end'] = $initMonth;
        $arrMonth['start'] = strtotime('first day of '.$length.' Month',$initMonth);
        return $arrMonth;
    }
    
    public function getDatePeriod($startDate,$endDate)
    {
        $arrTimestamp = array();
        while($startDate < $endDate)
        {
            $arrTimestamp[] = $startDate;
            $startDate = strtotime("+1 month",$startDate);
        }
        return $arrTimestamp;
    }
    
    public function generateRandomString($length = 10)
                {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
    
    public function convertCurrency($amount, $from, $to)
    {
        $url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";        
        try
        {
            $data = file_get_contents($url);
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }        
        
        preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
        $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
        return round($converted, 3);
    }
    
    public function moneySymbol($type='USD')
    {
        switch(strtolower($type))
        {
            case 'usd':
            case 'cad':
            case 'aud':
                return '&dollar;';
            case 'eur':
                return '&euro;';
            case 'thb':
                return '&#3647;';
            case 'jpy':
                return '&yen;';
            case 'gbp':
                return '&pound;';
            default:
                return $type;
        }
    }
    
    public function floatFormat($money)
    {
        return sprintf('%.02f',$money);
    }
    
    public function blindString($strlen)
    {
        $str = '';
        for($i=1;$i<=$strlen;$i++)
            $str .= '*';
        return $str;
    }
    
    public function alphanumeric_rand($num_require=8)
    {
        $alphanumeric = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',0,1,2,3,4,5,6,7,8,9);
        if($num_require > sizeof($alphanumeric))
        {
            echo "Error alphanumeric_rand(\$num_require) : \$num_require must less than " . sizeof($alphanumeric) . ", $num_require given";
            return;
        }
        $rand_key = array_rand($alphanumeric , $num_require);
        $randomstring = '';
        for($i=0;$i<sizeof($rand_key);$i++) $randomstring .= $alphanumeric[$rand_key[$i]];
        return $randomstring;
    }
    
    public function debugging($arr)
    {
        print('<pre>');
        print_r($arr);
        print('</pre>');
    }
}