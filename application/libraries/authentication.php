<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

if(!function_exists('do_hash')){
	include DIR_SERVER.'/system/helpers/security_helper.php';
}

class Authentication {

    private $ci;
    public $manual_activation;
    public $db_table;
    public $identity;
    public $_ion_where = array();
    public $_ion_select = array();
    public $_ion_like = array();
    public $_ion_limit = NULL;
    public $_ion_offset = NULL;
    public $_ion_order_by = NULL;
    public $_ion_order = NULL;
    public $activation_code;
    public $forgotten_password_code;
    public $logo;

    public function __construct() {
        $this->ci = & get_instance();
        $this->db_table = 'users';
        $this->identity = 'user_email';
        $this->db_group_table = 'groups';
        $this->users_groups_name = 'Users';
        $this->site_title = "Feed.biz";
        $this->forgot_password_expiration = 0;
        $this->manual_activation = TRUE;
        $this->language = $this->ci->session->userdata('language') ? $this->ci->session->userdata('language') : $this->ci->config->item('language');
        $this->ci->lang->load("user", $this->language);

        //Controller
        $this->login_controller = 'users/login/';
        $this->activate_controller = 'users/activate/';
        $this->reset_password_controller = 'users/reset_password/';
        
        //Logo
        $b_url = base_url(); 
        $im = ($b_url . '/nblk/images/feedbiz_logob.png');
        $this->logo = '<img src="' . ($im) . '" alt="feed.biz"  style="border: none" />';

        //Email
        $this->email_templates =  'email_template/'; //'email/' . $this->language . '/';     
        $this->email_activate = 'activate.tpl';
        $this->email_forgot_password = 'forgot_password.tpl';
        $this->email_forgot_password_complete = 'new_password.tpl';       
        $this->email_activation_subject = $this->ci->lang->line('email_activation_subject');
        $this->email_forgot_password_subject = $this->ci->lang->line('email_forgot_password_subject');
        $this->email_config = array(
            'useragent' => "Common-services",
            'mailpath' => '', 
            'protocol' => "mail",
            'smtp_host' => $this->ci->config->item('base_url'), 
            'smtp_port' => "25",
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'wordwrap' => False,
            'send_multipart' => 1,
            'multipart' => 'mixed',
        );
        $this->ci->load->library('email');

        if (isset($this->email_config) && is_array($this->email_config))
            $this->ci->email->initialize($this->email_config);

        $this->getGeneratedOTP();
    }

    /* forgotten password feature @return mixed  boolian / array */
    public function forgotten_password($identity) {
        $data = array();
        if (empty($identity)) {
            $data['error'] = 'error_email_empty';
            return $data;
        }

        // get identity for that email
        $email_user = $this->where('user_email', strtolower($identity))->users()->row();

        if (isset($email_user->user_status) && $email_user->user_status != 1) {
            $active_data = array(
                'user_activation_code' => NULL,
                'user_status' => 1
            );
            $this->ci->db->update($this->db_table, $active_data, array('user_email' => $identity));
        }

        if (empty($email_user)) {
            $data['error'] = 'forgot_password_email_not_found';
            return $data;
        }

        //All some more randomness
        $activation_code_part = "";
        if (function_exists("openssl_random_pseudo_bytes")) {
            $activation_code_part = openssl_random_pseudo_bytes(128);
        }

        for ($i = 0; $i < 1024; $i++) {
            $activation_code_part = sha1($activation_code_part . mt_rand() . microtime());
        }

        $this->forgotten_password_code = sha1($activation_code_part);

        $update = array(
            'user_forgotten_password_code' => $this->forgotten_password_code,
            'user_forgotten_password_time' => time()
        );

        $this->ci->db->update($this->db_table, $update, array($this->identity => $identity));

        $return = $this->ci->db->affected_rows() == 1;

        if ($return) {
            // Get user information
            $user = $this->where($this->identity, $identity)->where('user_status', 1)->users()->row();  //changed to get_user_by_identity from email

            if ($user) {              
                
                $this->ci->smarty->assign('sitename', sprintf($this->site_title));
                $this->ci->smarty->assign('identity', $this->identity);
                $this->ci->smarty->assign('logo', $this->logo);
                $this->ci->smarty->assign('forgot_password_link',  site_url( $this->reset_password_controller . $user->user_forgotten_password_code));
                $message = $this->ci->smarty->fetch($this->email_templates . $this->email_forgot_password);
                
                require_once APPPATH . 'libraries/Swift/swift_required.php';
                $subject = $this->site_title . ' - ' . $this->email_forgot_password_subject;
                $email = $user->user_email;
                
                try {
                    
                    $transport = Swift_SmtpTransport::newInstance();
                    $mailer = Swift_Mailer::newInstance($transport);
                    $swift_message = Swift_Message::newInstance();
                    
                    $swift_message->setSubject($subject)                           
                            ->setFrom(array($this->ci->config->item('admin_email') => $this->site_title))
                            ->setTo($email)
                            ->setBody($message, 'text/html');
                    
                    if ($mailer->send($swift_message) == false) {
                        $data['error'] = 'forgot_password_cant_send_mail';
                        return $data;
                    }
                } catch (Exception $ex) {
                    $data['error'] = 'forgot_password_cant_send_mail';
                    return $data;
                }
                
                if(!isset($data['error'])){
                     $data['message'] = 'forgot_password_successful';
                    return $data;
                }

            } else {
                $data['error'] = 'forgot_password_unsuccessful';
                return $data;
            }
        } else {
            $data['error'] = 'forgot_password_unsuccessful';
            return $data;
        }
    }

    /* forgotten_password_complete @return void */
    public function forgotten_password_complete($code) {
        $data = array();
        if (empty($code)) {
            $data['error'] = 'post_forgotten_password_empty_code';
            return $data;
        }

        $identity = $this->identity;
        $profile = $this->where('user_forgotten_password_code', $code)->users()->row(); //pass the code to profile
        if (!$profile) {
            $data['error'] = 'password_change_unsuccessful';
            return $data;
        }

        if ($profile) {

            if ($this->forgot_password_expiration > 0) {
                //Make sure it isn't expired
                $expiration = $this->forgot_password_expiration;
                if (time() - $profile->forgotten_password_time > $expiration) {
                    //it has expired
                    $data['error'] = 'forgot_password_expired';
                    return $data;
                }
            }

            $password = $this->salt();

            $data = array(
                'user_password' => $this->hash_password($password, $salt),
                'user_forgotten_password_code' => NULL,
                'user_status' => 1,
            );

            $this->ci->db->update($this->db_table, $data, array('user_forgotten_password_code' => $code));
        }

        if (!$password) {
            $data['error'] = 'post_forgotten_password_complete_unsuccessful';
            return $data;
        }

        $new_password = $password;

        if ($new_password) {    
                
                $this->ci->smarty->assign('sitename', sprintf($this->site_title));
                $this->ci->smarty->assign('identity', $profile->{$identity});
                $this->ci->smarty->assign('logo', $this->logo);
                $this->ci->smarty->assign('new_password',  $new_password);
                $message = $this->ci->smarty->fetch($this->email_templates . $this->email_forgot_password_complete);                
            
                $this->ci->email->clear();
                $this->ci->email->from($this->ci->config->item('admin_email'), $this->site_title);
                $this->ci->email->to($profile->email);
                $this->ci->email->subject($this->site_title . ' - ' . $this->ci->lang->line('email_new_password_subject'));
                $this->ci->email->message($message);
                if ($this->ci->email->send()) {
                    $this->set_message('password_change_successful');
                    $this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_successful'));
                    return TRUE;
                } else {
                    $this->set_error('password_change_unsuccessful');
                    $this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
                    return FALSE;
                }
                
        }
        
        $this->ion_auth_model->trigger_events(array('post_password_change', 'password_change_unsuccessful'));
        return FALSE;
    }

    /* forgotten_password_check @return void */
    public function forgotten_password_check($code) {
        
        $profile = $this->where('user_forgotten_password_code', $code)->users()->row(); //pass the code to profile
        
        if (!is_object($profile)) {
            
            $data['error'] = 'forgotten_password_code_wrong';
            return $data;
            
        } else {
            
            if ($this->forgot_password_expiration > 0) {
                $expiration = $this->forgot_password_expiration;
                if (time() - $profile->forgotten_password_time > $expiration) {
                    $this->clear_forgotten_password_code($code);
                    $data['error'] = 'password_change_unsuccessful';
                    return $data;
                }
            }
            
            return $profile;
        }
    }

    /* reset password @return bool */
    public function reset_password($identity, $new) {
        
        $data = array();
        if (!$this->identity_check($identity)) {
            $data['error'] = 'post_change_password_unsuccessful';
            return $data;
        }

        $this->ci->db->select(array('id', 'user_password', 'user_email'));
        $this->ci->db->where($this->identity, $identity);
        $this->ci->db->limit(1);
        $query = $this->ci->db->get($this->db_table);

        if ($query->num_rows() !== 1) {
            $data['error'] = 'password_change_unsuccessful';
            return $data;
        }

        $result = $query->row(); 
        $new = $this->hash_password($new, $result->user_email);

        $update_data = array(
            'user_password' => $new,
            'user_forgotten_password_code' => NULL,
            'user_forgotten_password_time' => NULL,
        );

        $this->ci->db->update($this->db_table, $update_data, array($this->identity => $identity));
        $return = $this->ci->db->affected_rows() == 1;
        if ($return)
            $data['message'] = 'password_change_successful';
        else
            $data['error'] = 'password_change_unsuccessful';

        return $data;
    }

    public function clear_forgotten_password_code($code) {
        if (empty($code))
            return FALSE;

        $this->ci->db->where('user_forgotten_password_code', $code);
        if ($this->ci->db->count_all_results($this->db_table) > 0) {
            $data = array(
                'user_forgotten_password_code' => NULL,
                'user_forgotten_password_time' => NULL
            );

            $this->ci->db->update($this->db_table, $data, array('user_forgotten_password_code' => $code));
            return TRUE;
        }
        return FALSE;
    }

    public function unserialize_authen($authen) {
        $arr_data = array();
        $arr_key = array();
        $response = unserialize(base64_decode($authen));

        if (isset($response['auth']))
            foreach ($response['auth'] as $auth_key => $auth_info) {
                if (is_array($auth_info)) {
                    foreach ($auth_info as $key => $auth_info_item) {
                        if (!is_array($auth_info_item)) {
                            if (array_search($key, $arr_key) == false && ( $arr_key != 'image' || $arr_key != 'picture' )) {
                                if (isset($response['auth']['provider']) && $response['auth']['provider'] == 'Twitter') {
                                    if ($key == 'screen_name') {
                                        $key = 'user_name';
                                    }

                                    if ($key == 'name') {
                                        $name = explode(" ", $auth_info_item);
                                        $arr_data['user_first_name'] = $name[0];
                                        if (isset($name[1]))
                                            $arr_data['user_first_name'] = $name[1];

                                        $key = "full_name";
                                    }
                                }
                                if ($arr_key != 'id') {
                                    $vowels = array("-", " ", ".");
                                    if ($key == 'last_name')
                                        $key = 'las_name';
                                    if ($key == 'name')
                                        $key = "full_name";
                                    $key = str_replace($vowels, "_", $key);
                                    $arr_data['user_' . $key] = $auth_info_item;
                                }
                            }
                            array_push($arr_key, $key);
                        }
                    }
                }
                else {
                    if ($auth_key == 'uid') {
                        $arr_data['user_name'] = $auth_info;
                        $auth_key = 'auth_id';
                    }
                    $arr_data['user_' . $auth_key] = $auth_info;
                }
            } else
            return false;

        //google-img
        if (isset($response['auth']['provider']) && $response['auth']['provider'] == 'GooglePlus') {
            $arr_data['user_image'] = '<img src="https://plus.google.com/s2/photos/profile/' . $response['auth']['uid'] . '?sz=100" />';
        } else {
            if (isset($response['auth']['info']['image']))
                $arr_data['user_image'] = '<img src="' . $response['auth']['info']['image'] . '"' . ' />';
        }
        
        return $arr_data;
    }

    /* login @return id */
    public function login_with_opauth() {
        
        if ($this->ci->uri->segment(3) == '') {
            
            $ci_config = $this->ci->config->item('opauth_config');
            $arr_data['strategies'] = array_keys($ci_config['Strategy']);
            $arr_data['path'] = $ci_config['path'];
            return $arr_data;
            
        } else {
            
            //Run login
            $config_data = $this->ci->config->item('opauth_config');
            $this->ci->load->library('Opauth/Opauth', $config_data, true);
            $this->ci->opauth->run();
            
        }
    }

    /* check_opauth_login  @return mixed */
    public function get_opauth_id($auth_id) {
        
        $this->ci->db->select('id');
        $this->ci->db->where(array('user_auth_id' => $auth_id));
        $id = $this->ci->db->get($this->db_table)->row();

        if ($id)
            return $id->id;
        else
            return FALSE;
    }

    /* check_username_login_id @return mixed */
    public function get_username_id($username) {
        
        $this->ci->db->select('id');
        $this->ci->db->where(array('user_name' => $username));
        $id = $this->ci->db->get($this->db_table)->row();

        if ($id)
            return $id->id;
        else
            return FALSE;
    }

    public function get_user_by_id($id) {
        
        $this->ci->db->select('*');
        $this->ci->db->where(array('id' => $id));
        return $this->ci->db->get($this->db_table)->result_array();
    }

    /* save_opauth_data  @return bool */
    public function save_opauth_data($data) {
        
        $insert_data = array(
            'user_ip_address' => $this->ci->input->ip_address(),
            'user_created_on' => date('Y-m-d H:i:s', time()),
            'user_status' => 1,
            'user_code' => md5(serialize($data) . mt_rand(0, 999999999)),
            'group_id' => $this->get_user_group(),
            'user_default_language' => $this->getLanguageIDByName($this->ci->config->item('language')),
        );

        $user_data = array_merge($this->_filter_data($this->db_table, $data), $insert_data);
        $this->ci->db->insert($this->db_table, $user_data);
        $id = $this->ci->db->insert_id();
        if (isset($id) && !empty($id)) {
            $mode = array(
                'id_customer' => $id,
                'name' => 'FEED_MODE',
                'config_date' => date('Y-m-d H:i:s', time()),
                'value' => base64_encode(serialize(array('mode' => '1')))
            );

            $this->ci->db->where(array('id_customer' => $mode['id_customer'], 'name' => $mode['name']));
            $this->ci->db->get('configuration');
            if ($this->ci->db->affected_rows() == 0)
                $this->ci->db->insert('configuration', $mode);
        }else {
            return false;
        }
        return $id;
    }

    /* login @return id */
    public function logout() {
        $this->removeOTP();
    }

    public function login($identity, $password, $loin_as_forum = null) {
        
        $data = array();
        $pass = false;
        
        if (empty($identity)) {
            $data['error'] = 'error_login_empty';
            return $data;
        }
        
        if (!isset($loin_as_forum) && empty($password)) {
            $data['error'] = 'error_login_empty';
            return $data;
        }
        
        if ($loin_as_forum) {
            $compare_user = $identity;
            if($loin_as_forum != $compare_user){
                $data['error'] = 'error_login_empty';
                return $data;
            }
        }

        $result = $this->ci->db->query('SELECT `id`, `user_name`, `user_first_name`, `user_las_name`,  `user_password`, `user_email`, `user_status` , `user_activation_code`, `user_default_language`, `token_assist` FROM `' . $this->db_table . '` WHERE `' . $this->identity . '` = ?', array($identity));

        if (!$result->num_rows()) {
            $data['error'] = 'error_login_mismatch';
            return $data;
        }

        $userdata = $result->row();
        
        if(!$loin_as_forum){            

            $compare = $this->hash_password(trim($password), trim($userdata->user_email));
            if($compare == $userdata->user_password){
                $pass = true;
            }
        } else {
            $pass = true;
        }
        
        if ($userdata->user_status == 0 ) {
            if ($userdata->user_activation_code != '') {
                $data['error'] = ('please_activate_account_by_email_click');
            } else {
                $data['error'] = ('error_account_not_active');
            }
            return $data;
        }

        if ($pass && ($this->ci->session->userdata('aptempt') < 3)) {
            $session_data['logged'] = TRUE;
            $this->ci->session->set_userdata($session_data);
            $this->ci->session->set_flashdata('message', 'message_returning_user');
            $this->update_last_login($userdata->id);
            $this->generateNewOTP($userdata->id);

            return array(
                'id' => $userdata->id,
                'user_name' => $userdata->user_name,
                'user_first_name' => $userdata->user_first_name,
                'user_las_name' => $userdata->user_las_name,
                'user_default_language' => $userdata->user_default_language,
                'token_assist' => $userdata->token_assist,
                'manager_status'=>$userdata->user_status==2?true:false,
            );
        } else {
            // Reset counter
            if (time() - $this->ci->session->userdata('last_try') > 180) {
                $session_data['last_try'] = time();
                $session_data['aptempt'] = 0;
                $this->ci->session->set_userdata($session_data);
            }
            if (($session_data['aptempt'] = $this->ci->session->userdata('aptempt') + 1) >= 3) {
                $session_data['last_try'] = time();
                $this->ci->session->set_userdata($session_data);

                $data['error'] = 'error_login_maxtries';
                return $data;
            }

            $data['error'] = 'error_login_mismatch';
            return $data;
        }
    }
    
    /* logged_in @return bool */
    public function logged_in() {
        
        if (!$this->ci->session->userdata('id'))
            return FALSE;
        
        $user = $this->user($this->ci->session->userdata['id'])->num_rows();
        
        if ($user > 0)
            return (bool) $this->ci->session->userdata('logged');
        
        return FALSE;
        
    }

    /* update_last_login  @return bool */
    public function update_last_login($id) {

        $this->ci->db->update($this->db_table, array('user_last_login' => date('Y-m-d H:i:s', time())), array('id' => $id));
        return $this->ci->db->affected_rows() == 1;
    }

    /**
     * register
     * @return array
     * $identity // A database column which is used to login with "email" / "username"
     * $manual_activation // Manual Activation for registration TRUE / FALSE
     * */
    public function register($email, $password, $additional_data = array(), $username = null, $referral = null) {
        
        if ($this->identity == 'user_email' && $this->email_check($email)) {
            $return['error'] = 'account_creation_duplicate_email';
            return $return;
        }

        $ip_address = $this->ci->input->ip_address();
        $hash_password = $this->hash_password($password, $email);

        $data = array(
            'user_name' => $username,
            'user_password' => $hash_password,
            'user_email' => $email,
            'user_code' => md5($email . $username . $hash_password . mt_rand(0, 999999999)),
            'user_ip_address' => $ip_address,
            'user_created_on' => date('Y-m-d H:i:s', time()),
            'user_status' => ($this->manual_activation == false ? 1 : 0),
            'group_id' => $this->get_user_group(),
            'user_referral' => $referral,
            'user_default_language' => $this->getLanguageIDByName($this->ci->config->item('language')),
        );

        $user_data = $data;
        $id = '';

        if (is_array($additional_data) && !empty($additional_data))
            $user_data = array_merge($this->_filter_data($this->db_table, $additional_data), $data);

        if ($this->ci->db->insert($this->db_table, $user_data)) {
            $id = $this->ci->db->insert_id();
            $username = 'u' . str_pad($id, 14, '0', STR_PAD_LEFT);
            $this->ci->db->update($this->db_table, array('user_name' => $username), array('id' => $id));
            $mode = array(
                'id_customer' => $id,
                'name' => 'FEED_MODE',
                'config_date' => date('Y-m-d H:i:s', time()),
                'value' => base64_encode(serialize(array('mode' => '1')))
            );
            if (!$this->ci->db->insert('configuration', $mode))
                return FALSE;
        }
        if ($id) {
            if ($this->manual_activation) {
                $deactivate = $this->deactivate($id);

                if ($deactivate) {
                    $activation_code = $this->activation_code;
                } else {
                    $return['error'] = 'deactivate_unsuccessful';
                    return $return;
                }

                $this->ci->smarty->assign('sitename', $this->site_title);
                $this->ci->smarty->assign('identity', $email);
                $this->ci->smarty->assign('id', $id);
                $this->ci->smarty->assign('logo',$this->logo);
                $this->ci->smarty->assign('link',site_url($this->activate_controller . $id . '/' . $activation_code));
                $message = $this->ci->smarty->fetch($this->email_templates . $this->email_activate);

                require_once APPPATH . 'libraries/Swift/swift_required.php';
                $subject = $this->site_title . ' - ' . $this->email_activation_subject;

                try {
                    $transport = Swift_SmtpTransport::newInstance();
                    $mailer = Swift_Mailer::newInstance($transport);
                    $swift_message = Swift_Message::newInstance();
                    $swift_message->setSubject($subject)
                            ->setFrom(array($this->ci->config->item('admin_email') => $this->site_title))
                            ->setTo($email)
                            ->setBody($message, 'text/html');
                    if ($mailer->send($swift_message) == false) {
                        $return['error'] = 'account_can_not_send_activate_email';
                        return $return;
                    }
                } catch (Exception $ex) {
                    
                }
            }
        }

        $return['id'] = (int) $id;
        $return['user_name'] = $username;
        $return['activation'] = $this->manual_activation;
        return $return;
    }

    /* update @return bool */
    public function update($id, array $data) {
        
        $return = array();
        $user = $this->user($id)->row();

        $this->ci->db->trans_begin();
        $current_password = (isset($data['current_password']) || !empty($data['current_password'])) ? $data['current_password'] : null;

        // Filter the data passed
        $data = $this->_filter_data($this->db_table, $data);

        if (array_key_exists('user_password', $data) || array_key_exists('user_email', $data)) {
            if (array_key_exists('user_password', $data)) {
                if (!empty($data['user_password'])) {
                    if (empty($data['user_email'])) {
                        $return['error'] = 'error_email_empty';
                        return $return;
                    }

                    if ($this->new_password_check($id)) {
                        
                        $data['user_password'] = $this->hash_password($data['user_password'], $data['user_email']);
                        
                    } else {
                        
                        if (empty($current_password) || $current_password == '') {
                            $return['error'] = 'error_current_password_empty';
                            return $return;
                        }

                        if (!$this->password_check($id, $current_password, $data['user_email'])) {
                            $return['error'] = 'current_password_mismat';
                            return $return;
                        }

                        $data['user_password'] = $this->hash_password($data['user_password'], $data['user_email']);
                    }
                } else {
                    unset($data['user_password']);
                }
            }
        }

        $this->ci->db->update($this->db_table, $data, array('id' => $user->id));

        if ($this->ci->db->trans_status() === FALSE) {
            $this->ci->db->trans_rollback();
            $return['error'] = 'update_unsuccessful';
            return $return;
        }

        $this->ci->db->trans_commit();
        $return['message'] = 'update_successful';

        return $return;
    }

    //activate the user
    function activate($id, $code = false) {
        $data = array();

        if ($code !== false) {
            $query = $this->ci->db->select('user_email,user_activation_code')
                    ->where('id', $id)
                    ->limit(1)
                    ->get($this->db_table);

            $result = $query->row();

            if ($query->num_rows() !== 1) {
                $data['error'] = 'activate_unsuccessful';
                return $data;
            } elseif ($result->user_activation_code != '' && $result->user_activation_code != $code) {
                $data['error'] = 'activate_unsuccessful';
                return $data;
            } elseif ($result->user_activation_code == '') {
                $data['error'] = 'already_activated';
                return $data;
            } else {

                $identity = $result->user_email;

                $data = array(
                    'user_activation_code' => NULL,
                    'user_status' => 1
                );

                $this->ci->db->update($this->db_table, $data, array('user_email' => $identity));
                $this->generateNewOTP($id);
            }
        } else {
            $data = array(
                'user_activation_code' => NULL,
                'user_status' => 0
            );
            $this->ci->db->update($this->db_table, $data, array('id' => $id));
        }

        $return = $this->ci->db->affected_rows() == 1;

        if ($return) {
            $data['message'] = 'activate_successful';
        } else {
            $data['error'] = 'activate_unsuccessful';
        }

        return $data;
    }

    /* Deactivate @return boolean */
    public function deactivate($id = NULL) {
        if (!isset($id)) {
            $data['error'] = 'deactivate_unsuccessful';
            return $data;
        }

        $activation_code = sha1(md5(microtime()));
        $this->activation_code = $activation_code;

        $data = array(
            'user_activation_code' => $activation_code,
            'user_status' => 0,
        );

        $this->ci->db->update($this->db_table, $data, array('id' => $id));
        $return = $this->ci->db->affected_rows() == 1;

        if ($return)
            return TRUE;
        else
            return FALSE;

        return FALSE;
    }

    /* Generates a random salt value. @return void */
    public function salt() {
        return substr(md5(uniqid(rand(), true)), 0, $this->salt_length);
    }

    /* Generates a random salt value for forgotten passwords or any other keys. Uses SHA1.  @return void  @author Mathew */
    public function hash_code($password, $salt) {
        return $this->hash_password($password, $salt);
    }

    /* Hashes the password to be stored in the database.  @return void */
    public function hash_password($password, $email) {
        if (empty($password))
            return FALSE;
//         $hash = crypt($password, $email);
        $hash = do_hash($password.$email);
        if (strlen($hash) >= 13)
            return $hash;
        else
            return null;
    }

    /* Checks email  @return bool */
    public function email_check($email = '') {
        if (empty($email))
            return FALSE;
        return $this->ci->db->where('user_email', $email)->count_all_results($this->db_table) > 0;
    }

    /* Checks username @return bool */
    public function username_check($username = '') {
        if (empty($username))
            return FALSE;
        return $this->ci->db->where('user_name', $username)->count_all_results($this->db_table) > 0;
    }

    /* check new password @return boolean */
    public function new_password_check($id) {
        $this->ci->db->select('user_password');
        $oldpassword = $this->ci->db->where(array('id' => $id))->get($this->db_table)->row();
        if ($oldpassword->user_password == null)
            return true;
        else
            return false;
    }

    /* check password  @return boolean */
    public function password_check($id, $password, $email) {
        $this->ci->db->select('user_password');
        $oldpassword = $this->ci->db->where(array('id' => $id))->get($this->db_table)->row();
        if ($oldpassword->user_password) {
            $haspassword = $this->hash_password($password, $email);
            if ($oldpassword->user_password === $haspassword)
                return true;
            else
                return false;
        }
        return false;
    }

    /* Identity check  @return bool  @author Mathew */
    public function identity_check($identity = '') {
        if (empty($identity))
            return FALSE;
        return $this->ci->db->where($this->identity, $identity)->count_all_results($this->db_table) > 0;
    }

    public function _filter_data($table, $data) {
        $filtered_data = array();
        $columns = $this->ci->db->list_fields($table);

        if (is_array($data)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data))
                    $filtered_data[$column] = $data[$column];
            }
        }
        return $filtered_data;
    }

    /* get_user_group @return group_id */
    public function get_user_group() {
        $this->ci->db->select(array($this->db_group_table . '.id'));
        $this->ci->db->limit(1);
        $this->ci->db->where($this->db_group_table . '.group_name', $this->users_groups_name);
        $group_id = $this->ci->db->get($this->db_group_table)->row();
        return $group_id->id;
    }

    /* user @return object */
    public function user($id) {
        $this->limit(1);
        $this->where($this->db_table . '.id', $id);
        $this->users();
        return $this;
    }

    /* users @return object Users */
    public function users() {
        if (isset($this->_ion_select) && !empty($this->_ion_select)) {
            foreach ($this->_ion_select as $select) {
                $this->db->select($select);
            }
            $this->_ion_select = array();
        } else {
            //default selects
            $this->ci->db->select(array(
                $this->db_table . '.*',
                $this->db_table . '.id as id'
            ));
        }

        //run each where that was passed
        if (isset($this->_ion_where) && !empty($this->_ion_where)) {
            foreach ($this->_ion_where as $where) {
                $this->ci->db->where($where);
            }
            $this->_ion_where = array();
        }

        if (isset($this->_ion_like) && !empty($this->_ion_like)) {
            foreach ($this->_ion_like as $like) {
                $this->ci->db->or_like($like);
            }
            $this->_ion_like = array();
        }

        if (isset($this->_ion_limit) && isset($this->_ion_offset)) {
            $this->ci->db->limit($this->_ion_limit, $this->_ion_offset);
            $this->_ion_limit = NULL;
            $this->_ion_offset = NULL;
        } else if (isset($this->_ion_limit)) {
            $this->ci->db->limit($this->_ion_limit);
            $this->_ion_limit = NULL;
        }
        //set the order
        if (isset($this->_ion_order_by) && isset($this->_ion_order)) {
            $this->ci->db->order_by($this->_ion_order_by, $this->_ion_order);
            $this->_ion_order = NULL;
            $this->_ion_order_by = NULL;
        }
        $this->response = $this->ci->db->get($this->db_table);
        return $this;
    }

    public function delete_user($id) {
        $user = $this->user($id)->num_rows();
        if ($user > 0) {
            $this->ci->db->where('id', $id);
            if ($this->ci->db->delete('users'))
                return true;
        }
        return false;
    }

    public function limit($limit) {
        $this->_ion_limit = $limit;
        return $this;
    }

    public function offset($offset) {
        $this->_ion_offset = $offset;
        return $this;
    }

    public function where($where, $value = NULL) {
        if (!is_array($where)) {
            $where = array($where => $value);
        }
        array_push($this->_ion_where, $where);
        return $this;
    }

    public function like($like, $value = NULL, $position = 'both') {
        if (!is_array($like)) {
            $like = array($like => array(
                    'value' => $value,
                    'position' => $position,
            ));
        }
        array_push($this->_ion_like, $like);
        return $this;
    }

    public function select($select) {
        $this->_ion_select[] = $select;
        return $this;
    }

    public function order_by($by, $order = 'desc') {
        $this->_ion_order_by = $by;
        $this->_ion_order = $order;
        return $this;
    }

    public function row() {
        $row = $this->response->row();
        $this->response->free_result();
        return $row;
    }

    public function row_array() {
        $row = $this->response->row_array();
        $this->response->free_result();
        return $row;
    }

    public function result() {
        $result = $this->response->result();
        $this->response->free_result();
        return $result;
    }

    public function result_array() {
        $result = $this->response->result_array();
        $this->response->free_result();
        return $result;
    }

    public function num_rows() {
        $result = $this->response->num_rows();
        $this->response->free_result();
        return $result;
    }

    public function checkAuthen($ref = false) {
        $data = array();
        if ($this->logged_in()) {
            $data['userData'] = (array) $this->user($this->ci->session->userdata['id'])->row();
            
            if($this->ci->session->userdata('original_redirect')) {
            	$this->ci->session->unset_userdata('original_redirect');
                redirect($this->ci->session->userdata('original_redirect'), 'refresh');
            }
            
            return $data;
        } else {
            
            $_SERVER['QUERY_STRING'] = $_SERVER['QUERY_STRING'] == '' ? '' : '?' . $_SERVER['QUERY_STRING'];
            $uri = current_url() . $_SERVER['QUERY_STRING'];
            $this->ci->session->set_userdata('error', 'session_error');
            $this->ci->session->set_userdata('original_redirect', $uri);
            redirect('users/login', 'refresh');
        }
        return $data;
    }

    public function setMarkRedirect() {
        if ($this->logged_in()) {
            $_SERVER['QUERY_STRING'] = $_SERVER['QUERY_STRING'] == '' ? '' : '?' . $_SERVER['QUERY_STRING'];
            $uri = current_url() . $_SERVER['QUERY_STRING'];
            $this->ci->session->set_userdata('original_redirect', $uri);
        }
    }

    public function validateAjaxToken() {
        $ajax_request = strpos($_SERVER['HTTP_ACCEPT'], 'json') !== FALSE;
        $token = isset($_SERVER['HTTP_X_AUTH_TOKEN']) ? $_SERVER['HTTP_X_AUTH_TOKEN'] : '';
        $error_message = '';

        if (empty($token)) {
            $error_message = 'Ajax authentication fail.';
        } else {
            if ($this->logged_in()) {
                $user_id = $this->ci->session->userdata('id');
                $sql = "select request_id,token,status,user_code from token where request_id = '$user_id' and status != 0 and source = 'login' ";
                $r = $this->ci->db->query($sql);
                if (!$r->num_rows()) {
                    $error_message = 'Ajax authentication fail(tk).';
                } else {
                    $out = $r->row();
                    $tokenDB = $out->token;
                }

                $mtime = $this->ci->session->userdata('mtime');
                if (empty($mtime)) {
                    $error_message = 'Ajax authentication fail(ss).';
                }
                $mtime = explode('|', $mtime);
                $otp = md5($mtime[0] . '|' . $tokenDB . '|' . $mtime[1]);
                if ($otp == $token) {
                    return true;
                } else {
                    $session_id = $this->ci->session->userdata('session_id');
                    if($out->status >1 && $out->status !=9  && $out->user_code != $session_id){
                        $error_message = "Your account has logged in by another session";
                    }else if($out->status==9  && $out->user_code != $session_id){
                        $error_message = "Your account has logged in by Feed.biz Admin";
                    }else{
                        $error_message = 'Ajax authentication fail(ot).';
                    }
                }
            }
        }

        if ($ajax_request) {
            $err = array('error' => $error_message,
                'txt' => $error_message
            );
            die(json_encode($err));
        } else {
            die($error_message);
        }
    }

    public function getGeneratedOTP() {
        if ($this->logged_in()) {
            $user_id = $this->ci->session->userdata('id');
            $sql = "select request_id,token from token where request_id = '$user_id' and status != 0 and source = 'login'";
            $r = $this->ci->db->query($sql);
            if (!$r->num_rows()) {
                $token = $this->generateNewOTP($user_id);
            } else {
                $out = $r->row();
                $token = $out->token;
            }
            $mtime = $this->ci->session->userdata('mtime');
            if (empty($mtime)) {
                $this->ci->smarty->assign("ajax_token", '');
                return false;
            }
            $mtime = explode('|', $mtime);
            $otp = md5($mtime[0] . '|' . $token . '|' . $mtime[1]);
            $this->ci->smarty->assign("ajax_token", $otp);
            return $otp;
        } else {
            $this->ci->smarty->assign("ajax_token", '');
            return '';
        }
    }

    public function generateNewOTP($user_id,$admin=false) {
        $another_login = $this->hasOtherLogin($user_id);
        $sql = "select request_id,status from token where request_id = '$user_id' ";
        $r = $this->ci->db->query($sql);
        $mtime = microtime() . '|' . microtime(true) . '|' . time(); 
        $session_id = $this->ci->session->userdata('session_id');
        $rand = $session_id;// mt_rand(0, 999999999);
        $token = md5($user_id . $mtime . $rand . $mtime . $user_id);
        $date_add = date('Y-m-d H:i:s', time());
            $this->ci->session->set_userdata(array(
                'mtime'=>$mtime, 
            ));
        if($another_login){
            $out = $r->row();
            if($admin){
                $status = 9 ;
            }else{
            $status = $out->status+1; 
            }
        }else{
            $status = 1;
        }
        if (!$r->num_rows()) {
            $this->ci->db->insert('token', array('token' => $token, 'request_id' => $user_id, 'user_code' => $rand, 'status' => $status, 'source' => 'login', 'date_add' => $date_add));
        } else {
            $this->ci->db->update('token', array('token' => $token, 'status' => $status, 'user_code' => $rand, 'date_add' => $date_add), array('request_id' => $user_id, 'source' => 'login'));
        }
        return $token;
    }
    
    public function hasOtherLogin($user_id){
        $session_table = $this->ci->config->item('sess_table_name');
        $session_id = $this->ci->session->userdata('session_id');
        $sql = "select t.request_id from token t inner join $session_table s on s.session_id != '".$session_id."' where request_id = '$user_id' and source = 'login' and t.status >= 0  ";
        $r = $this->ci->db->query($sql);
        if ($r->num_rows()) {
            return true;
        }
        return false;
    }

    public function removeOTP() {
        if ($this->logged_in()) {
            $user_id = $this->ci->session->userdata('id');
            $this->ci->session->unset_userdata('mtime');
            $sql = "select request_id,status from token where request_id = '$user_id' ";
            $r = $this->ci->db->query($sql);
            if ($r->num_rows()>0) {
                $out = $r->row();
                $status = $out->status-1;
                if($status>0){
                    $status = 1;
                    $input = array( 'status' => $status );
                }else{
                    $input = array( 'status' => $status ,'token'=>'');
                }
                $this->ci->db->update('token', array( 'status' => $status ), array('request_id' => $user_id, 'source' => 'login')); 
            } 
        }
    }
    
    public function getLanguageNameByID($id){
        $lang = $this->ci->db->query('SELECT `language_name` FROM `languages` WHERE `id` = ?', array($id));
        $language = $lang->row();
        if(isset($language->language_name))
            return $language->language_name;
    }
    
    public function getLanguageIDByName($name){
        $lang = $this->ci->db->query('SELECT `id` FROM `languages` WHERE `language_name` = ?', array($name));
        $language = $lang->row();
        if(isset($language->id))
            return $language->id;
    }
    
}
