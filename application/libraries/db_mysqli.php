<?php
$backend_db_connector=null;
$single_conn = true;
$global_query=$last_query='';
$tb_exists = array();
$all_cols = array();
$db_exists = array();
$last_db_conn_name = '';
Class Db
{
    /**
    * @var array list of data to build the query
    */
    private $shell_script_create_db = true;
    public $query = array(
        'select'    =>  array(),
        'from'      => 	'',
        'join'      => 	array(),
        'where'     => 	array(),
        'group'     =>  array(),
        'having'    =>  array(),
        'order'     => 	array(),
        'limit'     => 	array('offset' => 0, 'limits' => 0),
    );
    public $db_status;
    protected $conn_id;
    public $exist_db;
    public $prefix_db = 'fb_';
    public $prefix_table;
    public $database = '';
    protected $db_key = 'backend';
    protected $db_config = array(); 
    public $debug = false;
    public $db_exists = false;
    public $db_encode = 'utf8mb4';
    public $utf8_ver = 50500; 
    public function __construct($user, $database='',$check_exist=true,$single_connect = null)
    {
        global  $single_conn;

        if($single_connect!==null){
            $single_conn=$single_connect;
        }

        $this->database = $this->prefix_db.strtolower($user);
        if(trim($database)==''){
            $this->prefix_table = $database;
        }else{
            $this->prefix_table = $database.'_';
        }
        $clear_conn = true;
	
        if($database=='log'){
            $clear_conn = false;
        }
	
        $this->db_connect($this->database,$check_exist,$clear_conn);
         
    }
    
    public function db_connect($db_name,$check=true,$clear_conn=true) 
    {  
        global $backend_db_connector,$single_conn,$last_db_conn_name;
        if ( ! defined('BASEPATH')) define ('BASEPATH',DIR_SERVER);
        include dirname(__FILE__).'/../config/database.php';
        if(!isset($db[$this->db_key])) return false;
        $this->db_config = $db[$this->db_key];
        $re_new_connect=false;
        if($last_db_conn_name!=$db_name){
            $last_db_conn_name = $db_name;
            $re_new_connect=true;
            $backend_db_connector=null;
        }
        if(!$re_new_connect){
            if($backend_db_connector!=null && !empty($backend_db_connector) && $single_conn && $clear_conn){
                $this->conn_id = $backend_db_connector;
                $this->db_status = isset($this->conn_id) && !empty($this->conn_id);
                if($this->conn_id){
                    if(mysqli_get_server_version($this->conn_id) < $this->utf8_ver){
                        $this->db_encode = 'utf8';
                    }
                }
                return true;
            }
        }
        if(!empty($this->conn_id)){
            $this->db_close();
        }
        
        $this->conn_id = mysqli_connect($this->db_config['hostname'], $this->db_config['username'], $this->db_config['password']) or $this->_error();
        if($this->conn_id){
            if(mysqli_get_server_version($this->conn_id) < $this->utf8_ver){
                $this->db_encode = 'utf8';
            }
        }
        $backend_db_connector = $this->conn_id;
        if(!$this->db_exists || $this->database !=$db_name){ 
            $db_exists = $this->checkExistsDB($db_name);
            $this->db_exists = $db_exists;
            $this->database = $db_name;
        }else{
            $db_exists = $this->db_exists;
        }
        if($check==true && !$db_exists){$this->exist_db = false; $this->conn_id = null; return false;}
        
        if(!$db_exists){
            $this->createNewDB($db_name); 
        }
        
        $this->exist_db = true;
        $this->db = mysqli_select_db($this->conn_id, $this->database) or $this->_error();
          
        if ( !$this->conn_id || empty($this->conn_id) )
            return false;
        if($this->conn_id){
            mysqli_set_charset($this->conn_id,$this->db_encode);
            mysqli_query($this->conn_id, 'SET NAMES '.$this->db_encode);
        }
        $this->db_status = isset($this->conn_id) && !empty($this->conn_id);
        return true;
             
    } 
    
    public function select($fields)
    {
        if (!empty($fields))
        {
            foreach ($fields as $value) 
                $select[] =  $value;

            $query_string = implode(', ', $select) ;

            $this->query['select'][] = $query_string;
        }
        
        return $this;
    }
        
    public function from ($table, $alias = null,$no_prefix = false)
    {
        $table = (!$no_prefix?$this->prefix_table:'').$table;
        if (!empty($table))
            $this->query['from'] = $table . ($alias ? ' '.$alias : '');
        return $this;
    }
    
    public function where($restriction, $operation = "=", $where_operation = 'AND')
    {
        $where = array();
        foreach ($restriction as $key => $value) 
        {
            if($operation == 'IN'){
                $where[] =  $key . " $operation " . $value;
            } else {
                
                if($value === '' || is_null($value)){
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
		    
		    if($operation != "=")
			$value = "{$value}";
		    else
			$value = "'{$value}'";
                }
                $where[] =  $key . " $operation " . $value;
            }
        }
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = " $where_operation " . implode(" $where_operation ", $where) ;
        else
            $query_string = ' WHERE ' . implode(" $where_operation ", $where) ;
        //$query_string = ' WHERE ' . implode(' AND ', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_not_equal($restriction)
    {
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'{$value}'";
                
            $where[] =  $key . ' != ' . $value;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_like($restriction, $operation = null)
    {
        $where = array();
        
        if(isset($operation)) {
            $operation = ' OR ';            
        } else {
            $operation = ' AND ';
        }
            
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'%{$value}%'";

            $where[] =  $key . ' like ' . $value  ;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND (' . implode($operation, $where).')' ;
        else
            $query_string = ' WHERE (' . implode($operation, $where).')'  ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_select($restriction, $operation = 'IN')
    {
        foreach ($restriction as $key => $value) 
            $where[] =  $key . ' ' . $operation . ' ' . $value;
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;
        
        return $this;
    }
    
    public function join($join)
    {
        if (!empty($join))
            $this->query['join'][] = $join;

        return $this;
    }
    
    public function leftJoin($table, $alias = null, $on = null, $no_prefix = false)	    
    {
        return $this->join('LEFT JOIN ' . (!$no_prefix ? $this->prefix_table : '').$table . ' ' . ($alias ? $alias : '') . ($on ? ' ON '. $on : ''));
    }
    
    public function innerJoin($table, $alias = null, $on = null, $no_prefix = false)
    {
        return $this->join('INNER JOIN ' . (!$no_prefix ? $this->prefix_table : '').$table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function outerJoin($table, $alias = null, $on = null, $no_prefix = false)
    {
        return $this->join('OUTER JOIN ' . (!$no_prefix ? $this->prefix_table : '').$table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function orderBy($fields)
    {
        if (!empty($fields))
            $this->query['order'][] = $fields;

        return $this;
    }
    
    public function groupBy($fields)
    {
        if (!empty($fields))
            $this->query['group'][] = $fields;

        return $this;
    }
    
    public function limit($limit, $offset = 0)
    {
        $offset = (int)$offset;
        if ($offset < 0)
            $offset = 0;

        $this->query['limit'] = array(
            'offset' => $offset,
            'limits' => (int)$limit,
        );
        return $this;
    }
    
    public function query_string($arr_query) 
    { 
        $query_string = '';
        
        if(empty($arr_query['select']))
        {
            $query_string .= 'SELECT * FROM ';
        }
        else
        {
            foreach ($arr_query['select'] as $select)
            {
                $query_string .= 'SELECT ' . $select . ' FROM ';
            }
        }
        
        if(!isset($arr_query['from']) || empty($arr_query['from']))
            return false;
        
        $query_string .= $arr_query['from'];
        
        if(isset($arr_query['join']) && !empty($arr_query['join']))
            foreach ($arr_query['join'] as $join)
            {
                $query_string .= ' ' . $join;
            }
        
        if(isset($arr_query['where']) && !empty($arr_query['where']))
            foreach ($arr_query['where'] as $where)
            {
                $query_string .= $where;
            }
        
        if(isset($arr_query['group']) && !empty($arr_query['group']))
        {
            $groups = array();
            foreach ($arr_query['group'] as $group)
                $groups[] =  $group;
            
            $query_string .= ' GROUP BY ' . implode(', ', $groups) ;
        }
        
        if(isset($arr_query['order']) && !empty($arr_query['order']))
        {
            $orders = array();
            foreach ($arr_query['order'] as $order)
                $orders[] =  $order;
            
            $query_string .= ' ORDER BY ' . implode(', ', $orders) ;
        }
        
        if(isset($arr_query['limit']) && $arr_query['limit']['limits'] != 0)
            $query_string .= ' LIMIT ' . $arr_query['limit']['limits'];
        
        if(isset($arr_query['limit']) && isset($arr_query['limit']['offset']) && $arr_query['limit']['offset'] != 0)
            $query_string .= ' OFFSET ' . $arr_query['limit']['offset'] ;
        
        $query_string .= '; ';
        //echo '<pre>' . print_r($query_string, true) . '</pre>'; 
        
        return $query_string;
    } 
    
    public function insert_string($table, $data, $no_prefix = false)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified)) {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                } else {
                    $keys[] = "$key";
                }
                
                if($value === '' || is_null($value)) {
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
                    $value = "'" . $this->escape_str($value) . "'";
                }
                 
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }

        $sql = 'INSERT INTO '.(!$no_prefix ? $this->prefix_table : '').$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
        return (string)$sql;
    }
    
    public function replace_string($table, $data, $multi_value = false, $no_prefix = false)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified)) {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }  else {
                    $keys[] = "$key";
                }
                
                if($value === '' || is_null($value)) {
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
                    $value = "'" . $this->escape_str($value) . "'";
                }
                
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }
	
	if($multi_value){
	    $sql = implode(',', $values_stringified);
	} else  {
	    $sql = 'REPLACE INTO '.(!$no_prefix ? $this->prefix_table : '').$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';	    
	}
	    
        return (string)$sql;
    }
    
    public function update_string($table, $data, $where = array(), $limit = 0, $no_prefix = false)
    {
        if (!$data)
            return false;

        $sql = 'UPDATE '.(!$no_prefix ? $this->prefix_table : '').$table.' SET ';
        
        foreach ($data as $key => $value) {
            //$sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";             
            if($value === '' || is_null($value)) {
                $sql .= " $key = NULL,";
            } else if(is_int($value)) {
                $sql .= " $key = ".(int)$value.",";
            } else if(is_float($value)) {
                $sql .= " $key = ".(float)$value.",";
            } else {
                $sql .= " $key = '" . $this->escape_str($value) . "',";
            }
        }
        
        $sql = rtrim($sql, ',');
        
        if (!empty($where))
        {
            $sql .= ' WHERE ';
            
            $sql_where = array();
            foreach ($where as $key => $value)
            {                
                if(is_int($value['value'])) {
                    if(empty($value['operation'])){
                        $value['operation']= '=';
                    }
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (int)$value['value'];
                } else if(is_float($value['value'])) {
                    if(empty($value['operation'])){
                        $value['operation']= '=';
                    }
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (float)$value['value'];
                } else if(is_array($value['value'])){
                    if(empty($value['operation'])){
                        $value['operation']= 'in ';
                    }
                    $sql_where[] = $key . " " . $value['operation'] . " ('" . implode("','", $value['value']) . "') ";
                } else {
                    if(!empty($value['operation']) && isset($value['value'])){
                        $sql_where[] = $key . " " . $value['operation'] . " '" . $this->escape_str($value['value']) . "' ";
                    }
                }
            }
            
            $sql .= implode(' AND ', $sql_where);
        }
        
        if ($limit)
                $sql .= ' LIMIT '.(int)$limit;
        
        return (string)$sql . '; ';
    }
    
    public function delete_string($table, $where = array(), $no_prefix = false)
    {
        if (!$table || !$where)
            return false;
        
        $sql = ' WHERE ';
        $sql_where = array();
        
        foreach ($where as $key => $value)
	{
            // $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];
	    if(is_int($value['value'])) {
		$sql_where[] = $key . ' ' . $value['operation'] . ' ' . (int)$value['value'];
	    } else if(is_float($value['value'])) {
		$sql_where[] = $key . ' ' . $value['operation'] . ' ' . (float)$value['value'];
	    } else if(is_array($value['value'])){
		$sql_where[] = $key . " " . $value['operation'] . " ('" . implode("','", $value['value']) . "') ";
	    } else {
		$sql_where[] = $key . " " . $value['operation'] . " '" . $this->escape_str($value['value']) . "' ";
	    }
	}

        $sql .= implode(' AND ', $sql_where);
        
        $query =  "DELETE FROM " . (!$no_prefix ? $this->prefix_table : '').$table . $sql . " ; ";
        
        return $query;
    }
    
    public function db_num_rows($result) 
    {
        if(!isset($result) || empty($result) || !$result)
            return FALSE;
        
        $exec = mysqli_num_rows($result);
        unset($this->query);
        
        return $exec;
    }
    
    // 
    public function db_last_insert_rowid() 
    {
        $exec = mysqli_insert_id($this->conn_id);
        return $exec;
    }
    
    public function db_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
        
            $this->db_connect($this->database,false);
            
        $sql = $this->query_string($query);
        DB::keep_log($sql);
        $exec = mysqli_query($this->conn_id, $sql); 
        unset($this->query);
        if(!$exec)$this->newrelic_report($sql,__LINE__);
        return $exec;
    } 
    
    
    public function db_query_fetch($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
        
            $this->db_connect($this->database,false);
        
        $sql = $this->query_string($query);
         
//        $exec = mysqli_query($this->conn_id, $sql); 
//        if(!$exec)$this->newrelic_report($sql,__LINE__);
//        $result = sqlite_fetch_all($exec, SQLITE_ASSOC);
        $result = array();
        DB::keep_log($sql);
        $q = mysqli_query($this->conn_id, $sql);
        if(is_bool($q) && !$q){
            
            $e = new Exception; 
            $trace = $e->getTraceAsString();
            file_put_contents('/var/www/backend/assets/apps/fifo_dir/sql_error.txt',print_r(array(date('c'),$sql,$trace),true),FILE_APPEND);
            
        }
        while($o = mysqli_fetch_assoc($q)){
            $result[] = $o;
        }
        
        unset($this->query);
        
        return $result;
    } 
    
    public function db_query_string_fetch($sql) 
    { 
        if(!isset($sql) || empty($sql) || !$sql)
            return FALSE;
        
        
        
//        $exec = mysqli_query($this->conn_id, $sql);  
//        if(!$exec)$this->newrelic_report($sql,__LINE__);
//        $result = sqlite_fetch_all($exec, SQLITE_ASSOC);
//        if(empty($this->conn_id)){
            $this->db_connect($this->database,false);
//        }
        $result = array();
        DB::keep_log($sql);
        $q = mysqli_query($this->conn_id, $sql);
        if(is_bool($q) && !$q){return array();}
        while($o = mysqli_fetch_assoc($q)){
            $result[] = $o;
        }
        
        unset($this->query);
        
        return $result;
    } 

    public function db_changes()
    {
    	$result = mysqli_affected_rows($this->conn_id);
    	return $result;
    }
    
    
    public function db_sqlit_query($query) 
    { 
        
            $this->db_connect($this->database,false);
        
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
        DB::keep_log($query);
        $exec = mysqli_query($this->conn_id, $query); 
        if(!$exec)$this->newrelic_report($query,__LINE__);
//        if(!$exec){ echo '<pre>';debug_print_backtrace(); echo '</pre>';}
        return $exec;
    } 
    public function db_query_result_str($query) {
        
        $this->db_connect($this->database,false);
        
        if(!isset($query) || empty($query) || !$query  || empty($this->conn_id))
            return FALSE;
        DB::keep_log($query);
        $exec = mysqli_query($this->conn_id, $query); 
        if(!$exec)$this->newrelic_report($query,__LINE__);
//        if(!$exec){ echo '<pre>';debug_print_backtrace(); echo '</pre>';}
        return $exec;
    }
    
    public function db_query_str($query) 
    { 
        
            $this->db_connect($this->database,false);
        
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
//        $exec = @sqlite_array_query($this->conn_id, $query); 
        $exec = array();
        DB::keep_log($query);
        $q = mysqli_query($this->conn_id, $query);
        if($q!==false)
        while($o = mysqli_fetch_assoc($q)){
            $exec[] = $o;
        }
        
        return $exec ? $exec : array();
    } 
    
    public function db_array_query($query,$debug=false) 
    { 
        if(!isset($query) || empty($query) || !$query || empty($this->conn_id) || !$this->conn_id)
            return array();
            
        $sql = is_array($query) ? $this->query_string($query) : $query; 
        if($debug){
            echo $sql.'<br>';
        echo print_r($query,true); 
        
        }
//        $exec = @sqlite_array_query($this->conn_id, $sql); 
        $exec = array();
        
            $this->db_connect($this->database,false);
        DB::keep_log($sql);
        $q = mysqli_query($this->conn_id, $sql);
        if($q)
        while($o = mysqli_fetch_assoc($q)){
            
            $exec[] = $o;
        }
        
        if(!$q)$this->newrelic_report($sql,__LINE__);
         
        unset($this->query);
         
        return $exec ? $exec : array() ;
    } 
    
    public function db_exec($query,$transaction = true,$multi_query=true,$direct=false,$commit=true,$asyn=false)  
    {
        global $global_query,$last_query;
        $query = trim($query);
        if(!$query || empty($query) || !isset($query) || empty($this->conn_id) )
            return FALSE;
//        $this->newrelic_report($query,__LINE__);   
        $last_query=$query;
        $added = false;
        $exec = true;
        if(!$multi_query){
            $transaction = $multi_query;
        }
        
        if($transaction&& $multi_query===true){ //$query = "BEGIN;".$query."COMMIT;";
//             $query = " START TRANSACTION;".$query."COMMIT;";
//             mysqli_autocommit($this->conn_id,FALSE);
//             mysqli_begin_transaction($this->conn_id);
//             $multi_query = true;
        }
        if($multi_query === true){
            $i=1;
//            if(!$added)$query = " START TRANSACTION;".$query."COMMIT;";
//            if($this->last_query_result){
//                $this->db_close();
//                $this->db_connect($this->database,false);
//            }
            

            if($direct){
                $global_query.=$query;
                 
                if($commit){
                $this->run_mysql_command($global_query,$asyn);
                $global_query='';
                }
                 
//                do {
//                    /* store first result set */
////                    if ($result = mysqli_store_result($this->conn_id)) { 
////                        mysqli_free_result($result);
////                        if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',print_r($result,true)."\n",FILE_APPEND);
////                    }else{
////                        mysqli_use_result($this->conn_id);
//                    
//                        $micro_date = microtime();
//                        $date_array = explode(" ",$micro_date);
//                        $date = date("Y-m-d H:i:s",$date_array[1]); 
//                        if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\nDate: $date:" . $date_array[0]."\n",FILE_APPEND); 
//                        if(!mysqli_more_results($this->conn_id))break;
////                    }
//                    /* print divider */
//                    
//                } while (mysqli_next_result($this->conn_id));
                
                
                 $micro_date = microtime(); $date_array = explode(" ",$micro_date); $date = date("Y-m-d H:i:s",$date_array[1]).' '.$date_array[0];//show micro time
                if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\nNew connect Date: $date:" . $date_array[0]."\n",FILE_APPEND);
                
                
            }else{
//                do{
//                    
//                    $num_proc=$this->get_num_process(); 
//                    $micro_date = microtime(); $date_array = explode(" ",$micro_date); $date = date("Y-m-d H:i:s",$date_array[1]);
//                    
//                    if($num_proc>=70){
//                        usleep(200000);
//                        if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\nWait '$num_proc' process pool Date: $date:" . $date_array[0]."\n",FILE_APPEND);
//                    }
//                    
//                }while($num_proc>=70);
//                $micro_date = microtime(); $date_array = explode(" ",$micro_date); $date = date("Y-m-d H:i:s",$date_array[1]); 
//                if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\n\n\nStart Query Date: $date:" . $date_array[0]."\n".$query,FILE_APPEND);
//                $exec = mysqli_multi_query ($this->conn_id, $query);
                DB::keep_log($query);
                $exec = mysqli_multi_query ($this->conn_id, $query); 
                if ($exec)
                {
                  do
                    {
                    // Store first result set
                    if ($result=mysqli_store_result($this->conn_id)) {
                      // Fetch one and one row
//                      while ($row=mysqli_fetch_row($result))
//                        {
//                        printf("%s\n",$row[0]);
//                        }
                      // Free result set
                      mysqli_free_result($result);
                      }
                      if(!mysqli_more_results($this->conn_id))break;
                    }
                  while (mysqli_next_result($this->conn_id));
                }
                 
//                if($exec){
//                $this->last_query_result = $exec;
//                $n =substr_count($query,';');
//                if($n>0)usleep($n*5000);
//                $this->db_close();
//                $this->db_connect($this->database,false);
//                }
            }
        }else{
              
               $this->db_connect($this->database,false);
            
            $i=2;
            DB::keep_log($query);
//            $query = " START TRANSACTION;".$query."COMMIT;";
            $exec = mysqli_query ($this->conn_id, $query); 
//            if(!$exec){
//                echo '<pre>';
//                print_r(mysqli_error($this->conn_id));
//                echo '</pre>';exit;
//            }
        }
        if($transaction){
//            mysqli_commit ($this->conn_id);
        }
//        echo "\n case ";
//        echo $i.'x'.$query;
//        echo "\n";
//        echo 'test res : ';
//        print_r($exec);
//        echo "\n";
//        echo mysqli_error($this->conn_id);
//        echo "\n";
//        $e = new Exception; 
//        echo $e->getTraceAsString() ;
//        echo "\n\n"; 
        
//        exit;
        $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("Y-m-d H:i:s",$date_array[1]); 
        if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\nEND FN Date: $date:" . $date_array[0]."\n",FILE_APPEND); 
        $last_query='';
        if(!$exec){ 
//            echo json_encode($query); 
            $this->newrelic_report($query,__LINE__); 
            return  false;
        }
            
        return $exec;
    } 
    
    public function db_close() 
    { global $backend_db_connector,$single_conn;
         $backend_db_connector = null;
         $out=null;
         if(!empty($this->conn_id)){
            $out = mysqli_close($this->conn_id);
            unset($this->conn_id);
         }else{
             if($backend_db_connector!=null && !empty($backend_db_connector) && $single_conn){
                $this->conn_id = $backend_db_connector;
                $out = mysqli_close($this->conn_id);
                unset($this->conn_id);
                $backend_db_connector=null;
             }
         }
         unset($this->tb_exists);
         return $out;
    }
    
    public function db_trans_begin()
    {
//        mysqli_query($this->conn_id, 'START TRANSACTION');
        return TRUE;
    }
    
    public function db_trans_commit()
    {
//        mysqli_query($this->conn_id, 'COMMIT');
        return TRUE;
    }
    
    public function db_table_exists($table = '', $no_prefix=false)
    { 
        global $argv;
        $table = (!$no_prefix?$this->prefix_table:'').$table;
        if(isset($this->tb_exists[$table]) && $this->tb_exists[$table]){
            return true;
        }
        $sql = "SHOW TABLES LIKE '{$table}';";
        
      
        
        $this->db_connect($this->database,false);
        
        if(empty($this->conn_id)){
           
            return false;
        }
        DB::keep_log($sql);
        $chk = mysqli_query($this->conn_id, $sql);
        if($chk)
        $chk = mysqli_num_rows($chk);
        if(!$chk){ 
             
//            $msg = "Not Found table : $table @ ".date('c')."\n".$sql."\n";
//            if(!empty($argv)) $msg.= implode(' ',$argv);
//            $e = new Exception;
//            $trace = $e->getTraceAsString();
//            $msg .= "\n".$trace."\n---\n";
//            file_put_contents(DIR_SERVER.'/assets/apps/fifo_dir/table_not_found.txt' , $msg,FILE_APPEND);
            return false;
        }else{
            $this->tb_exists[$table]=true;
        }
         
        return $chk>0?true:false;
    }
    
    public function truncate_table($table, $no_prefix = false)
    {
         
        $this->db_connect($this->database,false);
        DB::keep_log("DELETE FROM " . (!$no_prefix ? $this->prefix_table : '').$table . "; ");
        if(!mysqli_query($this->conn_id, "DELETE FROM " . (!$no_prefix ? $this->prefix_table : '').$table . "; "))
            return false;
        
        return true;
    }
    
    public function escape_str($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = $this->escape_str($val);
             return $str;
         }

         $str = mysqli_real_escape_string ($this->conn_id,$str);
         return $str;
    }
    
    public static function escape_string($str)
    {   global $backend_db_connector;
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = self::escape_string($val);
             return $str;
         }
         
         

//         $str = sqlite_escape_string($str);
        if (function_exists('mysqli_real_escape_string'))
        {
           if(!empty($backend_db_connector)){
                $str = mysqli_real_escape_string($backend_db_connector,$str);
           }else{
               $str = addslashes($str);
           }
        }
        else
        {
                $str = addslashes($str);
        }
         return $str;
    }
    
    public function newrelic_report($sql,$line=0){
        if(function_exists('newrelic_add_debug')){
            newrelic_add_debug('Issue query('.$line.')',$sql); 
        }
        
        if(function_exists('log_message')){
            $e = new Exception;
            log_message('error','Issue query : '.$sql) ;  
//            log_message('error',$e->getTraceAsString()) ;
        }
    }
    
    public function checkExistsDB($db_name){
        global $db_exists;
        if(isset($db_exists[$db_name])) return $db_exists[$db_name];
        if($this->conn_id){
            $sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$db_name'";
//            echo $sql.'<br>';
            DB::keep_log($sql);
            $q = mysqli_query($this->conn_id, $sql);
            $o = mysqli_num_rows($q);
            $out = $o>0?true:false;
            $db_exists[$db_name] = $out; 
            return $out;
        }
        return false;
    }
    
    public function createNewDB($db_name){
        if($this->conn_id && $db_name !=''){
            $sql = "CREATE DATABASE IF NOT EXISTS {$db_name}; GRANT ALL PRIVILEGES ON {$db_name}.* TO '{$this->db_config['username']}'@'%' WITH GRANT OPTION; ";

            if($this->shell_script_create_db){
                if($this->db_config['hostname']=='localhost'){
                    $pass='';
                    
                    if($this->db_config['password']!='') $pass=" -p{$this->db_config['password']} ";
                    $cmd = $this->db_config['local_mysql_path'].' -u '.$this->db_config['username'].' '.$pass.' -e "'.$sql.'"';
                }else{
                    $cmd = 'mysql -u '.$this->db_config['username'].' -p'.$this->db_config['password'].' -h '.$this->db_config['hostname'].' -e "'.$sql.'"'; 
                }
                exec($cmd);
            }else{
                DB::keep_log($sql);
                $q = mysqli_query($this->conn_id, $sql); 
            }
            return  true ;
        }
        return false;
    }
    public function get_all_column_name($table_name, $except_primary=false, $no_prefix=false){
	
        global $all_cols;
	
        if(trim($table_name) == '')
	    return array();
	
        $this->db_connect($this->database,false);
	
        if(!$this->db_table_exists($table_name, $no_prefix)){
            return false;            
        }
	
	$tb = (!$no_prefix) ? "{$this->prefix_table}$table_name" : "$table_name";
	
        if(isset($all_cols[$tb]) && !empty($all_cols[$tb])){
            return $all_cols[$tb];
        }
        DB::keep_log("SHOW COLUMNS FROM $tb  ;");
        $res = mysqli_query($this->conn_id, "SHOW COLUMNS FROM $tb  ;");
        $out = array();
	
        if(!is_bool($res)) {
	    while($o = mysqli_fetch_assoc($res)){
		 if($except_primary && $o['Key']=='PRI' && $o['Extra'] == 'auto_increment'){
			continue;
		    }
		$out[] = $o['Field'];
	    }
	}
	
        if(!empty($out)){
            $all_cols[$tb]=$out;
        }
	
        return $out;
    }
    public function put_array_tb_alias($columns){
        $cols=array();
        foreach($columns as $v){
            $v=trim($v);
            $cols[] = $v." as '{$v}'";
        } 
        return $cols;
    }
    public function get_column_tb_alias($table_name,$alias='',$sign='"', $no_prefix = false){
        
        if(trim($table_name) == '')return array();
        $sql = "SHOW COLUMNS FROM " . (!$no_prefix ? $this->prefix_table : '') . "$table_name  ";
        $this->db_connect($this->database,false);
        DB::keep_log($sql);
        $res = mysqli_query($this->conn_id,$sql);
        $out = array();
        while($o = mysqli_fetch_assoc($res)){
            if($alias!=''){
                $o['Field'] = $o['Field']." as {$sign}{$alias}.{$o['Field']}{$sign}";
            }
            $out[] = $o['Field'];
        }
        return $out;
    }
    
    public function _error() {
        if($this->conn_id){
            $err = mysqli_error($this->conn_id);
            $this->error[] = $err;
            return $err;
        }
    }
    public function run_mysql_command($sql,$asyn=true){
        $out='';
        if(!empty($sql)){
            $fname = 'msql_tmp_'.time().uniqid().'.sql';
            $dir = DIR_SERVER.'/assets/apps/fifo_dir/';
            $tmp_mysql_command = $dir.$fname;
            $e = new Exception; 
            $padd_top = "SET CHARACTER SET {$this->db_encode};\n SET NAMES {$this->db_encode};\n";
            $padd = "\n/*".$this->database."\n\n".$e->getTraceAsString().'*/' ;
            file_put_contents($tmp_mysql_command, $padd_top.$sql.$padd);
            if(isset($this->runned_before)){
                usleep($this->runned_before*100000);
            }
            
            if($asyn){
                $this->runned_before=10;
            }else{
                $this->runned_before=1;
            }
            if($this->db_config['hostname']=='localhost'){
                    $pass='';
                    
                    if($this->db_config['password']!='') $pass=" -p{$this->db_config['password']} ";
                    $cmd = $this->db_config['local_mysql_path'].' -u '.$this->db_config['username'].' '.$pass.' '.$this->database.'<  "'.$tmp_mysql_command.'"';
                    $out = exec($cmd);
            } else {
                
                 set_time_limit ( 3600 ); ini_set ( 'memory_limit', '2048M' ); 
                
                    $cmd = 'mysql -u '.$this->db_config['username'].' -p'.$this->db_config['password'].' -h '.$this->db_config['hostname'].' '.$this->database.'< '.$tmp_mysql_command; 
                    if($asyn){
                        $out = exec('bash -c "exec nohup setsid '.$cmd.' > /dev/null 2>&1 &  "');
                    }else{
                        $out = exec($cmd);
                    }
            }
           
            
            $this->remove_tmp_direct_sql($tmp_mysql_command);
            if($this->debug)file_put_contents('/var/www/html/backend/assets/apps/test.txt',"\nEnd of command  Date: $date:" . $date_array[0]."\n",FILE_APPEND);
            DB::keep_log($sql);
//            exec("rm {$dir}*.sql");
        }
        return empty($out)?true:false;
    }
    public function put_global_query($sql){
        global $global_query;
        $global_query.=$sql;
        return true;
    }
    public function commit_global_query($asyn=true){
        global $global_query;
        $global_query = trim($global_query);
        if(empty($global_command))return false;
        $this->run_mysql_command($global_command,$asyn);
        $global_query='';
        return true;
    }
    public function remove_tmp_direct_sql($path_file){
        if(defined('KEEP_SQL_DIRECT'))return;
//        $dir = DIR_SERVER.'/assets/apps/fifo_dir/';
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
            exec("rm {$path_file} ");
        }else{
            if(file_exists($path_file))
            unlink($path_file);
        }
        
    }
    public function get_num_process(){
         
        $this->db_connect($this->database,false);
//        $result = mysqli_query($this->conn_id,"SHOW PROCESSLIST");
        $c=0;
//        while ($row=mysqli_fetch_array($result)) {
//            $process_id=$row["Id"];
//            if ($row["Time"] > 300 && $row['Command'] == 'Sleep') {
//              $sql="KILL $process_id";
//              mysqli_query($this->conn_id,$sql);
//            }
//           $c++;
//        } 
        return $c;
    }
    
    public static function fieldExists($table, $field)
    {
        global $backend_db_connector;
        if(empty($backend_db_connector))return false;
        static $field_exists = array();
        $fields = array();

        if (isset($field_exists[$table . $field])) {
            return $field_exists[$table . $field];
        }

        $sql = 'SHOW COLUMNS FROM  `' . $table  . '`';

        // Check if exists
        DB::keep_log($sql);
        $q = mysqli_query($backend_db_connector,$sql);
        $query=array();
        if($q){
            while($d = mysqli_fetch_array($q)){
                $query[]=$d;
            }
        }

        if (!is_array($query) || !count($query)) {
            return false;
        }

        foreach ($query as $row) {
            $fields[$row['Field']] = 1;
        }

        if (isset($fields[$field])) {
            $field_exists[$table . $field] = true;
        } else {
            $field_exists[$table . $field] = false;
        }

        return $field_exists[$table . $field];
    }
    public static function keep_log($sql){
        if(defined('_KEEP_LAST_SQL_') && _KEEP_ALL_USER_SQL_){
            $e = new Exception;
            $trace = $e->getTraceAsString();
            file_put_contents(DIR_SERVER.'/assets/apps/fifo_dir/last_sql_log.txt',print_r(array(date('c'),$sql,$trace),true));
        }
        if(defined('_KEEP_ALL_USER_SQL_') && _KEEP_ALL_USER_SQL_){
            $e = new Exception;
            $trace = $e->getTraceAsString();
            file_put_contents(DIR_SERVER.'/assets/apps/fifo_dir/all_sql_log.txt',print_r(array(date('c'),$sql,$trace),true),FILE_APPEND);
        }
    }
}