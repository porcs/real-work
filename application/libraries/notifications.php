<?php

require_once(dirname(__FILE__).'/tools.php');
require_once(dirname(__FILE__).'/db.php');

class Notifications {

    private static $prefix;

    public function __construct($user_name) {
        foreach ($user_name as $username)
            $this->db = new Db($username, 'log');

        if(isset($this->db->prefix_table) && !empty($this->db->prefix_table))
	    self::$prefix = $this->db->prefix_table;
    }
    
    public function getNotifications($marketplace=null, $id_shop=null, $id_country=null, $identify=null){
        $notification = $count = array();
        $where = '';

        if(isset($marketplace))
            $where .= " AND marketplace = '$marketplace' ";
        if(isset($id_shop))
            $where .= " AND id_shop = '$id_shop' ";
        if(isset($id_country))
            $where .= " AND id_country = '$id_country' ";
        if(isset($identify))
            $where .= " AND notification_identify = '$identify' ";
        
        $sql = "SELECT * FROM ".self::$prefix."notifications WHERE date_upd >= (NOW() - INTERVAL 7 DAY) $where ORDER BY flag_unread DESC, notification_date DESC LIMIT 20;" ;
       
        $result = $this->db->db_query_str($sql);
        foreach ($result as $key => $row){
            $keys = $row['id_shop'];
            $notification[$keys][$key]['marketplace'] = $row['marketplace'];
            $notification[$keys][$key]['id_shop'] = $row['id_shop'];
            $notification[$keys][$key]['id_country'] = $row['id_country'];
            $notification[$keys][$key]['notification_identify'] = $row['notification_identify'];
            $notification[$keys][$key]['notification_date'] = time_elapsed_string(strtotime($row['notification_date']));
            $notification[$keys][$key]['notification_link'] = $row['notification_link'];
            $notification[$keys][$key]['flag_unread'] = $row['flag_unread'];
            $notification[$keys][$key]['notification_message'] = $row['notification_message'];

            if(!isset($count[$keys]))
                $count[$keys] = 0;
            
            if($row['flag_unread'] == 1)
                $count[$keys]++;

            $notification[$keys]['count'] = $count[$keys];
        }
        
        return $notification;
    }

    public function flagUnread($marketplace, $id_shop, $id_country, $identify, $flag=0){
        $redirect = null;
        $update = array('flag_unread' => $flag, 'date_upd' => date('Y-m-d H:i:s'));
        $where = array();
        $where['marketplace'] = array('operation' => '=', 'value' => $marketplace);
        $where['id_shop'] = array('operation' => '=', 'value' => (int)$id_shop);
        $where['id_country'] = array('operation' => '=', 'value' => (int)$id_country);
        $where['notification_identify'] = array('operation' => '=', 'value' => $identify);
        $sql = $this->db->update_string("notifications", $update, $where, true);

        if($this->db->db_exec($sql)){
            $result = $this->getNotifications($marketplace, $id_shop, $id_country, $identify);
            if(isset($result[$id_shop][0]['notification_link'])){
                $redirect = $result[$id_shop][0]['notification_link'];
            }
        }
        return $redirect;
    }
} 