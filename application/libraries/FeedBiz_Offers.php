<?php

require_once(dirname(__FILE__) . '/FeedBiz/config/offers.php');

class FeedBiz_Offers extends ObjectModel {

    public $lang;
    
    public function __construct($user, $id = null, $id_lang = null) {
        foreach ($user as $id_user){
            parent::__construct($id_user, (int) $id, $id_lang);
            $this->user=$id_user;
        }
        
    }

    public function getShops($user) {
        $shops = new Shop($user);
        return $shops->getShops();
    }

    public function getDefaultShop($user) {
        $shops = new Shop($user);
        return $shops->getDefaultShop();
    }

    public function checkShops($user, $id_shop) {
        $shops = new Shop($user);
        return $shops->checkShops($id_shop);
    }

    public function setDefaultShop($user, $data) {
        $shops = new Shop($user);
        return $shops->setDefaultShop($data);
    }

    public function getProfilePrice($user, $id_shop, $id_mode) {
        $prices = new Price($user);
        $price = $prices->getProfilePrice($id_shop, $id_mode);

        if (!$price)
            return FALSE;

        return $price;
    }

    public function deleteProfilePrice($user, $id_profile_price, $id_shop, $id_mode) {
        $prices = new Price($user);

        if (!$prices->deleteProfilePrice($id_profile_price, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function saveProfilePrice($user, $data) {
        $prices = new Price($user);

        if (!$prices->saveProfilePrice($data))
            return FALSE;

        return true;
    }

    public function getProfile($user, $id_shop, $id_mode) {
        $profiles = new Profile($user);
        $profile = $profiles->getProfile($id_shop, $id_mode);

        if (!$profile)
            return FALSE;

        return $profile;
    }

    public function deleteProfile($user, $id_profile, $id_shop, $id_mode) {
        $profiles = new Profile($user);

        if (!$profiles->deleteProfile($id_profile, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function deleteProfileItem($user, $id_rule, $id_shop, $id_mode) {
        $rules = new Profile($user);

        if (!$rules->deleteProfileItem($id_rule, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function saveProfile($user, $data) {
        
        $profiles = new Profile($user);

        if (!$profiles->saveProfile($data))
            return FALSE;

        return true;
    }

    public function getRules($user, $id_shop, $id_mode) {
        $rules = new Rule($user);
        $rule = $rules->getRule($id_shop, $id_mode);

        if (!$rule)
            return FALSE;

        return $rule;
    }

    public function saveRules($user, $data) {
        $rules = new Rule($user);

        if (!$rules->saveRules($data))
            return FALSE;

        return true;
    }

    public function deleteRules($user, $id_rule, $id_shop, $id_mode) {
        $rules = new Rule($user);

        if (!$rules->deleteRules($id_rule, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function deleteRulesItem($user, $id_rule, $id_shop, $id_mode) {
        $rules = new Rule($user);

        if (!$rules->deleteRulesItem($id_rule, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function deleteRulesItemPriceRange($user, $id_rule, $id_shop, $id_mode) {
        $rules = new Rule($user);

        if (!$rules->deletePriceRangeItem($id_rule, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function getSuppliers($user, $id_shop) {
        $suppliers = new Supplier($user);
        $supplier = $suppliers->getSuppliers($id_shop);

        if (!$supplier)
            return FALSE;

        return $supplier;
    }

    public function getManufacturers($user, $id_shop) {
        $manufacturers = new Manufacturer($user);
        $manufacturer = $manufacturers->getManufacturers($id_shop);

        if (!$manufacturer)
            return FALSE;

        return $manufacturer;
    }

    public function saveCategory($user, $data, $id_shop, $id_mode) {
        $categories = new Category($user);

        if (!$categories->saveSelectedCategory($data, $id_shop, $id_mode))
            return FALSE;

        return true;
    }

    public function getRootCategories($user, $id_shop) {
        $categories = new Category($user, 'products');
        $root = $categories->getRootCategories($id_shop);
        $name = $categories->getCategoryNameByID($root, $id_shop);

        return array('id' => $root, 'name' => $name);
    }
    
    public function getAllRootCategories($user, $id_shop,$id_lang=null) {
        $categories = new Category($user, 'products'); 
        $root = $categories->getAllRootCategories($id_shop);
        $out= array();
        if(is_array($root)){
            foreach($root as $r){
               $name = $categories->getCategoryNameByID($r, $id_shop,$id_lang); 
               $out[] =  array('id' => $r, 'name' => $name);
            }
            
        }else{
            $name = $categories->getCategoryNameByID($root, $id_shop,$id_lang);
            $out[] =  array('id' => $root, 'name' => $name);
        }

        return $out;
    }

    public function getCategories($user, $id_shop, $id_lang = null) {
        if (!isset($user) || empty($user))
            return false;

        $categories = new Category($user);

        $root = $categories->getRootCategories($id_shop);
        $sub_category = $categories->getSubCategories($root, $id_shop, $id_lang);

        return $sub_category;
    }

    public function getSelectedCategoriesProfileID($user, $id_category, $id_mode = null, $id_shop = null) {
        if(!isset($this->obj_categories_offer)){
        $this->obj_categories_offer = new Category($user);
        }
        $category = $this->obj_categories_offer->getSelectedCategoriesProfileID($id_category, $id_mode, $id_shop);

        return $category;
    }

    public function getSelectedCategories($user, $id_parent, $id_shop, $id_lang = null) {
        if(!isset($this->obj_categories)){
        $this->obj_categories = new Category($user, 'products');
        }
        $category = $this->obj_categories->getSelectedCategory($id_parent, $id_shop, $id_lang);

        return $category;
    }

    public function truncate_table($user, $table) {
        $db = new Db($user, ObjectModel::$database);
        $truncate = $db->truncate_table($table);

        if (!$truncate)
            return FALSE;

        return true;
    }

    public function get_token($user) {
        $otp = new OTP($user);
        $token = $otp->get_token();

        if (!$token)
            return FALSE;

        return $token;
    }

    public function exportOfferList($user, $id_mode = null, $page = 1) {
        if (!isset($user) || empty($user))
            return false;

        $data = array();
        $product_list = new Product($user);

        foreach ($product_list->exportOffers($id_mode, $page) as $key => $product) {
            if (!isset($product->id_product) || empty($product->id_product))
                $product = new Product($user, (int) $product['id_product']);

            if (isset($product) || !empty($product)) {
                $data['product'][$product->id_product]['id_product'] = $product->id_product;
                $data['product'][$product->id_product]['sku'] = isset($product->sku) ? $product->sku : '';
                $data['product'][$product->id_product]['ean13'] = isset($product->ean13) ? $product->ean13 : '';
                $data['product'][$product->id_product]['upc'] = isset($product->upc) ? $product->upc : '';
                $data['product'][$product->id_product]['id_supplier'] = isset($product->id_supplier) ? $product->id_supplier : '';
                $data['product'][$product->id_product]['id_manufacturer'] = isset($product->id_manufacturer) ? $product->id_manufacturer : '';
                $data['product'][$product->id_product]['active'] = isset($product->active) ? $product->active : '';
                $data['product'][$product->id_product]['on_sale'] = isset($product->on_sale) ? $product->on_sale : '';
                $data['product'][$product->id_product]['reference'] = isset($product->reference) ? $product->reference : '';
                $data['product'][$product->id_product]['quantity'] = isset($product->quantity) ? $product->quantity : '';
                $data['product'][$product->id_product]['condition'] = isset($product->condition) ? $product->condition : '';
                $data['product'][$product->id_product]['price'] = isset($product->price) ? $product->price : '';
                $data['product'][$product->id_product]['supplier'] = isset($product->supplier) ? $product->supplier : '';
                $data['product'][$product->id_product]['manufacturer'] = isset($product->manufacturer) ? $product->manufacturer : '';
                $data['product'][$product->id_product]['category'] = isset($product->category) ? $product->category : '';
                $data['product'][$product->id_product]['id_category_default'] = isset($product->id_category_default) ? $product->id_category_default : '';
                $data['product'][$product->id_product]['carrier'] = isset($product->carrier) ? $product->carrier : '';
                $data['product'][$product->id_product]['shipping_cost'] = isset($product->shipping_cost) ? $product->shipping_cost : '';
                $data['product'][$product->id_product]['combination'] = isset($product->combination) ? $product->combination : '';

                if (isset($product->on_sale) && $product->on_sale == 1)
                    $data['product'][$product->id_product]['sale'] = $product->sale;


                if (isset($product->currency) && !empty($product->currency)) {
                    foreach ($product->currency as $currency)
                        if (isset($currency['name']) || !empty($currency['name']))
                            $data['product'][$product->id_product]['currency'] = strtolower($currency['name']);
                } else
                    $data['product'][$product->id_product]['currency'] = '';
            }
        }

        return $data;
    }
    
    public function checkLanguage($iso_code, $id_shop) {
        $language = Language::getLanguages($id_shop,$this->user );

        foreach ($language as $l) {
            if ($l['iso_code'] == $iso_code)
                return $l;
        }

        foreach (Language::getLanguageDefault($id_shop,$this->user ) as $default_lang)
            return $default_lang;
    }

}
