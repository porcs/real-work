<?php

Class Db
{
    /**
    * @var array list of data to build the query
    */
    public $query = array(
        'select'    =>  array(),
        'from'      => 	'',
        'join'      => 	array(),
        'where'     => 	array(),
        'group'     =>  array(),
        'having'    =>  array(),
        'order'     => 	array(),
        'limit'     => 	array('offset' => 0, 'limits' => 0),
    );
    public $db_status;
    protected $conn_id;
        
    public function __construct($user, $database,$check_exist=true)
    {
        $path = dirname(__FILE__) .  '/../../assets/apps/users/' . $user . '/' . $database. '.db';
        $this->conn_id = $this->db_connect($path,$check_exist);
          
        $this->db_status = isset($this->conn_id) && !empty($this->conn_id);
    }
    
    public function db_connect($location,$check=true) 
    { 
        if($check==true && !file_exists($location)) return false;
        $conn_id = @sqlite_open($location, 0666, $error);
        
        if ( !$conn_id || empty($conn_id) )
            return false;

        return $conn_id;
             
    } 
    
    public function select($fields)
    {
        if (!empty($fields))
        {
            foreach ($fields as $value) 
                $select[] =  $value;

            $query_string = implode(', ', $select) ;

            $this->query['select'][] = $query_string;
        }
        
        return $this;
    }
        
    public function from ($table, $alias = null)
    {
        if (!empty($table))
            $this->query['from'] = $table . ($alias ? ' '.$alias : '');
        return $this;
    }
    
    public function where($restriction, $operation = "=", $where_operation = 'AND')
    {
        $where = array();
        foreach ($restriction as $key => $value) 
        {
            if($operation == 'IN'){
                $where[] =  $key . " $operation " . $value;
            } else {
                
                if($value === '' || is_null($value)){
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
                    $value = "'{$value}'";
                }
                $where[] =  $key . " $operation " . $value;
            }
        }
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = " $where_operation " . implode(" $where_operation ", $where) ;
        else
            $query_string = ' WHERE ' . implode(" $where_operation ", $where) ;
        //$query_string = ' WHERE ' . implode(' AND ', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_not_equal($restriction)
    {
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'{$value}'";
                
            $where[] =  $key . ' != ' . $value;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_like($restriction, $operation = null)
    {
        $where = array();
        
        if(isset($operation)) {
            $operation = ' OR ';            
        } else {
            $operation = ' AND ';
        }
            
        foreach ($restriction as $key => $value) 
        {
            if($value === '' || is_null($value))
                $value = 'NULL';
            else if(is_int($value))
                $value = (int)$value;
            else if(is_float($value))
                $value = (float)$value;
            else
                $value = "'%{$value}%'";

            $where[] =  $key . ' like ' . $value  ;
        }

        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode($operation, $where) ;
        else
            $query_string = ' WHERE ' . implode($operation, $where) ;
        
        $this->query['where'][] = $query_string;

        return $this;
    }
    
    public function where_select($restriction, $operation = 'IN')
    {
        foreach ($restriction as $key => $value) 
            $where[] =  $key . ' ' . $operation . ' ' . $value;
        
        if(isset($this->query['where']) && is_array($this->query['where']) && !empty($this->query['where']))
            $query_string = ' AND ' . implode(' AND ', $where) ;
        else
            $query_string = ' WHERE ' . implode('', $where) ;
        
        $this->query['where'][] = $query_string;
        
        return $this;
    }
    
    public function join($join)
    {
        if (!empty($join))
            $this->query['join'][] = $join;

        return $this;
    }
    
    public function leftJoin($table, $alias = null, $on = null)
    {
        return $this->join('LEFT JOIN ' . $table . ' ' . ($alias ? $alias : '') . ($on ? ' ON '. $on : ''));
    }
    
    public function innerJoin($table, $alias = null, $on = null)
    {
        return $this->join('INNER JOIN ' . $table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function outerJoin($table, $alias = null, $on = null)
    {
        return $this->join('OUTER JOIN ' . $table .' '.($alias ?  $alias : '').($on ? ' ON ' . $on : ''));
    }
    
    public function orderBy($fields)
    {
        if (!empty($fields))
            $this->query['order'][] = $fields;

        return $this;
    }
    
    public function groupBy($fields)
    {
        if (!empty($fields))
            $this->query['group'][] = $fields;

        return $this;
    }
    
    public function limit($limit, $offset = 0)
    {
        $offset = (int)$offset;
        if ($offset < 0)
            $offset = 0;

        $this->query['limit'] = array(
            'offset' => $offset,
            'limits' => (int)$limit,
        );
        return $this;
    }
    
    public function query_string($arr_query) 
    { 
        $query_string = '';
        
        if(empty($arr_query['select']))
        {
            $query_string .= 'SELECT * FROM ';
        }
        else
        {
            foreach ($arr_query['select'] as $select)
            {
                $query_string .= 'SELECT ' . $select . ' FROM ';
            }
        }
        
        if(!isset($arr_query['from']) || empty($arr_query['from']))
            return false;
        
        $query_string .= $arr_query['from'];
        
        if(isset($arr_query['join']) && !empty($arr_query['join']))
            foreach ($arr_query['join'] as $join)
            {
                $query_string .= ' ' . $join;
            }
        
        if(isset($arr_query['where']) && !empty($arr_query['where']))
            foreach ($arr_query['where'] as $where)
            {
                $query_string .= $where;
            }
        
        if(isset($arr_query['group']) && !empty($arr_query['group']))
        {
            $groups = array();
            foreach ($arr_query['group'] as $group)
                $groups[] =  $group;
            
            $query_string .= ' GROUP BY ' . implode(', ', $groups) ;
        }
        
        if(isset($arr_query['order']) && !empty($arr_query['order']))
        {
            $orders = array();
            foreach ($arr_query['order'] as $order)
                $orders[] =  $order;
            
            $query_string .= ' ORDER BY ' . implode(', ', $orders) ;
        }
        
        if(isset($arr_query['limit']) && $arr_query['limit']['limits'] != 0)
            $query_string .= ' LIMIT ' . $arr_query['limit']['limits'];
        
        if(isset($arr_query['limit']) && isset($arr_query['limit']['offset']) && $arr_query['limit']['offset'] != 0)
            $query_string .= ' OFFSET ' . $arr_query['limit']['offset'] ;
        
        $query_string .= '; ';
        //echo '<pre>' . print_r($query_string, true) . '</pre>'; 
        
        return $query_string;
    } 
    
    public function insert_string($table, $data)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified)) {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                } else {
                    $keys[] = "$key";
                }
                
                if($value === '' || is_null($value)) {
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
                    $value = "'" . $this->escape_str($value) . "'";
                }
                 
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }

        $sql = 'INSERT INTO '.$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
        return (string)$sql;
    }
    
    public function replace_string($table, $data)
    {
        if (!$data)
            return false;

        // Check if $data is a list of row
        $current = current($data);
        if (!is_array($current) || isset($current['type']))
            $data = array($data);

        $keys = array();
        $values_stringified = array();
        foreach ($data as $row_data)
        {
            $values = array();
            foreach ($row_data as $key => $value)
            {
                if (isset($keys_stringified)) {
                    // Check if row array mapping are the same
                    if (!in_array("$key", $keys))
                         return false;
                }  else {
                    $keys[] = "$key";
                }
                
                if($value === '' || is_null($value)) {
                    $value = 'NULL';
                } else if(is_int($value)) {
                    $value = (int)$value;
                } else if(is_float($value)) {
                    $value = (float)$value;
                } else {
                    $value = "'" . $this->escape_str($value) . "'";
                }
                
                $values[] = $value;
            }
            $keys_stringified = implode(', ', $keys);
            $values_stringified[] = '('.implode(', ', $values).')';
        }

        $sql = 'REPLACE INTO '.$table.' ('.$keys_stringified.') VALUES '.implode(', ', $values_stringified) . '; ';
        return (string)$sql;
    }
    
    public function update_string($table, $data, $where = array(), $limit = 0)
    {
        if (!$data)
            return false;

        $sql = 'UPDATE '.$table.' SET ';
        
        foreach ($data as $key => $value) {
            //$sql .= ($value === '' || is_null($value)) ? "$key = NULL," : "$key = '{$value}',";             
            if($value === '' || is_null($value)) {
                $sql .= " $key = NULL,";
            } else if(is_int($value)) {
                $sql .= " $key = ".(int)$value.",";
            } else if(is_float($value)) {
                $sql .= " $key = ".(float)$value.",";
            } else {
                $sql .= " $key = '" . $this->escape_str($value) . "',";
            }
        }
        
        $sql = rtrim($sql, ',');
        
        if (!empty($where))
        {
            $sql .= ' WHERE ';
            
            $sql_where = array();
            foreach ($where as $key => $value)
            {                
                if(is_int($value['value'])) {
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (int)$value['value'];
                } else if(is_float($value['value'])) {
                    $sql_where[] = $key . ' ' . $value['operation'] . ' ' . (float)$value['value'];
                } else if(is_array($value['value'])){
                    $sql_where[] = $key . " " . $value['operation'] . " ('" . implode("','", $value['value']) . "') ";
                } else {
                    $sql_where[] = $key . " " . $value['operation'] . " '" . $this->escape_str($value['value']) . "' ";
                }
            }
            
            $sql .= implode(' AND ', $sql_where);
        }
        
        if ($limit)
                $sql .= ' LIMIT '.(int)$limit;
        
        return (string)$sql . '; ';
    }
    
    public function delete_string($table, $where = array())
    {
        if (!$table || !$where)
            return false;
        
        $sql = ' WHERE ';
        $sql_where = array();
        
        foreach ($where as $key => $value)
            $sql_where[] = $key . ' ' . $value['operation'] . ' ' . $value['value'];

        $sql .= implode(' AND ', $sql_where);
        
        $query =  "DELETE FROM " . $table . $sql . " ; ";
        
        return $query;
    }
    
    public function db_num_rows($result) 
    {
        if(!isset($result) || empty($result) || !$result)
            return FALSE;
        
        $exec = sqlite_num_rows($result);
        unset($this->query);
        
        return $exec;
    }
    
    // 
    public function db_last_insert_rowid() 
    {
        $exec = sqlite_last_insert_rowid($this->conn_id);
        return $exec;
    }
    
    public function db_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $sql = $this->query_string($query);
        
        $exec = sqlite_query($this->conn_id, $sql); 
        unset($this->query);
        if(!$exec)$this->newrelic_report($sql,__LINE__);
        return $exec;
    } 
    
    
    public function db_query_fetch($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $sql = $this->query_string($query);
         
        $exec = sqlite_query($this->conn_id, $sql); 
        if(!$exec)$this->newrelic_report($sql,__LINE__);
        $result = sqlite_fetch_all($exec, SQLITE_ASSOC);
        
        unset($this->query);
        
        return $result;
    } 
    
    public function db_query_string_fetch($sql) 
    { 
        if(!isset($sql) || empty($sql) || !$sql)
            return FALSE;
        
        
        
        $exec = sqlite_query($this->conn_id, $sql);  
        if(!$exec)$this->newrelic_report($sql,__LINE__);
        $result = sqlite_fetch_all($exec, SQLITE_ASSOC);
        
        unset($this->query);
        
        return $result;
    } 

    public function db_changes()
    {
    	$result = sqlite_changes($this->conn_id);
    	return $result;
    }
    
    
    public function db_sqlit_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE; 
        $exec = sqlite_query($this->conn_id, $query); 
        if(!$exec)$this->newrelic_report($query,__LINE__);
//        if(!$exec){ echo '<pre>';debug_print_backtrace(); echo '</pre>';}
        return $exec;
    } 
    
    public function db_query_str($query) 
    { 
        if(!isset($query) || empty($query) || !$query)
            return FALSE;
            
        $exec = @sqlite_array_query($this->conn_id, $query); 
        
        return $exec ? $exec : array();
    } 
    
    public function db_array_query($query) 
    { 
        if(!isset($query) || empty($query) || !$query || empty($this->conn_id) || !$this->conn_id)
            return array();
            
        $sql = is_array($query) ? $this->query_string($query) : $query; 
         
        $exec = @sqlite_array_query($this->conn_id, $sql); 
        $test = @sqlite_query($this->conn_id, $sql); 
        if(!$test)$this->newrelic_report($sql,__LINE__);
         
        unset($this->query);
         
        return $exec ? $exec : array() ;
    } 
    
    public function db_exec($query) 
    { 
        if(!$query || empty($query) || !isset($query))
            return FALSE;
//        $this->newrelic_report($query,__LINE__);      
        $exec = sqlite_exec($this->conn_id, $query); 
        if(!$exec){
            $this->newrelic_report($query,__LINE__); 
            return FALSE;
        }
            
        return $exec;
    } 
    
    public function db_close() 
    { 
        return sqlite_close($this->conn_id);
    }
    
    public function db_trans_begin()
    {
        sqlite_exec($this->conn_id, 'BEGIN');
        return TRUE;
    }
    
    public function db_trans_commit()
    {
        sqlite_exec($this->conn_id, 'COMMIT');
        return TRUE;
    }
    
    public function db_table_exists($table = '')
    {
        $sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='".$table."'";
        if(empty($this->conn_id))return false;
        $exec = @sqlite_array_query($this->conn_id, $sql); 
        return empty($exec)?false:true;
        
    }
    
    public function truncate_table($table)
    {
        if(!sqlite_exec($this->conn_id, "DELETE FROM " . $table . "; "))
            return false;
        
        return true;
    }
    
    public function escape_str($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = $this->escape_str($val);
             return $str;
         }

         $str = sqlite_escape_string($str);
         return $str;
    }
    
    public static function escape_string($str)
    {
         if (is_array($str))
         {
             foreach ($str as $key => $val)
                 $str[$key] = self::escape_string($val);
             return $str;
         }

         $str = sqlite_escape_string($str);
         return $str;
    }
    
    public function newrelic_report($sql,$line=0){
        if(function_exists('newrelic_add_debug')){
            newrelic_add_debug('Issue query('.$line.')',$sql); 
        }
        
        if(function_exists('log_message')){
            $e = new Exception;
            log_message('error','Issue query : '.$sql) ;  
//            log_message('error',$e->getTraceAsString()) ;
        }
    }
}