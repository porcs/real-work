<?php

class Shop extends ObjectModel
{
    
    public static $definition = array(
        'table'     => 'shop',
        'primary' => array('id_shop'),
        'fields'    => array(
            'id_shop'       =>  array('type' => 'INTEGER', 'required' => true,  'primary_key' => true),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'is_default'    =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'active'        =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'description'   =>  array('type' => 'text', 'required' => false),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    public function getShops()
    {
        $shops = array();
        
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->orderBy('name');
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $shop)
            foreach ($shop as $skey => $svalue)
                foreach (self::$definition['fields'] as $fkey => $fvalue)
                    if($skey == $fkey)
                        $shops[$key][$fkey] = $shop[$fkey];
                    
        return $shops;
    }
    
    public function checkShops($id_shop)
    {
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->where(array('id_shop' => (int)$id_shop));
        $result = $_db->db_query($_db->query);
        if($_db->db_num_rows($result) > 0)
            return TRUE;
        
        return FALSE;
    }
    
    public static function getDefaultShop()
    {
        $shops = array();
        
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->where(array('is_default' => 1));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $shop)
        {
            foreach ($shop as $skey => $svalue)
            {
                foreach (self::$definition['fields'] as $fkey => $fvalue)
                {
                    if($skey == $fkey)
                        $shops[$fkey] = $shop[$fkey];
                }
            }
        }
                    
        return $shops;
    }
    
    public function setDefaultShop($data)
    {
        $update['is_default'] = 1;
        $where['id_shop'] = array('operation' => '=', 'value' => (int)$data['id_shop']);
        
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->where(array('id_shop' => (int)$data['id_shop']));
        $result = $_db->db_query($_db->query);
        
        if($_db->db_num_rows($result) > 0)
        {
            $sql = $_db->update_string('shop', $update, $where);

            if($_db->db_exec($sql))
            {
                $update2['is_default'] = 0;
                $where2['id_shop'] = array('operation' => '!=', 'value' => (int)$data['id_shop']);

                $sql2 = $_db->update_string('shop', $update2, $where2);

                if($_db->db_exec($sql2))
                {
                    $sql3 = $_db->db_query_str('SELECT name FROM '.$_db->prefix_table.'shop WHERE id_shop = "' . (int)$data['id_shop'] . '"');
                    if($sql3)
                        foreach ($sql3 as $shop)
                            return $shop["name"];
                }   
            }
        }
        else
        {
            $update2['is_default'] = 0;

            $sql2 = $_db->update_string('shop', $update2, '');
            if($_db->db_exec($sql2))
            {
                return TRUE;
            }
        }
        return FALSE;
    }
}