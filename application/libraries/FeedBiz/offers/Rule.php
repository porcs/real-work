<?php

class Rule extends ObjectModel
{
     public static $definition = array(
        'table'     => 'rule',
        'primary'   => array('id_rule'),
        'fields'    => array(
            'id_rule'           =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'name'              =>  array('type' => 'varchar', 'required' => true ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
     
    public static function getRule($id_shop, $id_mode)
    {
        $rules = array();
        $rule_name = array();
        $i = 0;
        
        $_db = ObjectModel::$db;
        $_db->from(self::$definition['table']);
        $_db->where(array('id_shop' => $id_shop, 'id_mode' => $id_mode));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $rule)
        {
            $name = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace (" ", "_", $rule['name']));
                if(isset($rule_name[$rule['name']]) && $rule_name[$rule['name']] == true)
                    $name = $name . '_' . $i++;
                
            $rules[$name]['id_rule']         = (int)$rule['id_rule'];
            $rules[$name]['name']            = (string)$rule['name'];
            
            $_db->from('rule_item');
            $_db->where(array('id_rule' => $rule['id_rule'], 'id_shop' => $id_shop));
            $result2 = $_db->db_array_query($_db->query);
            
            foreach ($result2 as $key_item => $rule_item)
            {
                $rules[$name]['rule'][$key_item]['id_rule_item']        = (int)$rule_item['id_rule_item'];
                
                if(isset($rule_item['id_supplier']) && !empty($rule_item['id_supplier']))
                    $rules[$name]['rule'][$key_item]['id_supplier']         = (int)$rule_item['id_supplier'];

                if(isset($rule_item['id_manufacturer']) && !empty($rule_item['id_manufacturer']))
                    $rules[$name]['rule'][$key_item]['id_manufacturer']     = (int)$rule_item['id_manufacturer'];
                
                if(isset($rule_item['action']) && $rule_item['action'] != NULL)
                    $rules[$name]['rule'][$key_item]['action']              = (int)$rule_item['action'];
                
                if(isset($rule_item['id_rule_item']) && !empty($rule_item['id_rule_item']))
                {
                    $_db->from('rule_item_price_range');
                    $_db->where(array('id_rule_item' => $rule_item['id_rule_item'], 'id_shop' => $id_shop));
                    $result3 = $_db->db_array_query($_db->query);

                    foreach ($result3 as $key_item_price_range => $rule_item_price_range)
                    {
                        $rules[$name]['rule'][$key_item]['price_range'][$key_item_price_range]['id_rule_item_price_range']=(int)$rule_item_price_range['id_rule_item_price_range'];

                        if(isset($rule_item_price_range['price_range_from']) && $rule_item_price_range['price_range_from'] != NULL)
                            $rules[$name]['rule'][$key_item]['price_range'][$key_item_price_range]['price_range_from']  = (int)$rule_item_price_range['price_range_from'];

                        if(isset($rule_item_price_range['price_range_to']) && $rule_item_price_range['price_range_to'] != NULL)
                            $rules[$name]['rule'][$key_item]['price_range'][$key_item_price_range]['price_range_to']  = (int)$rule_item_price_range['price_range_to'];
                    }
                }
            }
            
            $rule_name[$rule['name']] = true;
        }
        
        return $rules;
    }
    
    public static function saveRules($datas)
    {
        $db = ObjectModel::$db;
        $sql = '';
        foreach ($datas as $data)
        {   
            if((isset($data["name"]) && !empty($data["name"]) && $data["name"] != NULL))
            {   
                $insert_data = array(
                    'name' => $data["name"], 
                    'id_shop' => $data["id_shop"], 
                    'id_mode' => $data["id_mode"], 
                    'date_add' => $data["date_add"]
                );
                
                if(isset($data["id_rule"]) && !empty($data["id_rule"]))
                {
                    $sql = $db->update_string(self::$definition['table'], $insert_data, array('id_rule' => array('value' => $data["id_rule"] , 'operation' => '=')));
                    
                    if(!$sql)
                        continue;
                    else
                    {
                        if(!$db->db_exec($sql,false,false))
                            continue;
                        else
                        {
                            $insert_rule = array();
                            if(isset($data["rule"]) && !empty($data['rule']))
                            {
                                foreach ($data["rule"] as $key => $rule)
                                { 
                                    $insert_rule[$key]['id_shop'] = (int)$data["id_shop"];
                                    $insert_rule[$key]['id_mode'] = (int)$data["id_mode"];
                                    $insert_rule[$key]['id_supplier'] = (isset($rule['id_supplier']) && !empty($rule['id_supplier'])) ? 
                                            (int)$rule['id_supplier'] : '';
                                    $insert_rule[$key]['id_manufacturer'] = (isset($rule['id_manufacturer']) && !empty($rule['id_manufacturer'])) ? 
                                            (int)$rule['id_manufacturer'] : '';
                                    $insert_rule[$key]['action'] = (isset($rule['action']) && $rule['action'] != null) ? 
                                            (int)$rule['action'] : null;
                                    
                                    if( (isset($data['id_rule']) && !empty($data['id_rule'])) && (isset($rule['id_rule_item']) && !empty($rule['id_rule_item'])) )
                                    {
                                        $where = array(
                                            'id_rule' => array(
                                                'value' => $data["id_rule"], 
                                                'operation' => '='
                                            ),
                                            'id_rule_item' => array(
                                                'value' => $rule["id_rule_item"] , 
                                                'operation' => '='
                                            )
                                        );
                                        
                                        $sql_item = $db->update_string('rule_item', $insert_rule[$key], $where);
                                        
                                        if(!$db->db_exec($sql_item,false,false))
                                               continue;
                                        
                                        if(isset($rule["price_range"]) && !empty($rule["price_range"]))
                                        {
                                            $insert_rule_price_range = array();
                                            foreach ($rule["price_range"] as $key2 => $rule2)
                                            {        
                                                //echo '<pre>' . print_r($rule2, true) . '</pre>';
                                                $insert_rule_price_range[$key2]['id_shop'] = (int)$data["id_shop"];
                                                $insert_rule_price_range[$key2]['id_mode'] = (int)$data["id_mode"];
                                                $insert_rule_price_range[$key2]['price_range_from'] = (isset($rule2['price_range_from']) && $rule2['price_range_from'] != null ) ? 
                                                        (int)$rule2['price_range_from'] : null;
                                                $insert_rule_price_range[$key2]['price_range_to'] = (isset($rule2['price_range_to']) && $rule2['price_range_to'] != null ) ? 
                                                        (int)$rule2['price_range_to'] : null;

                                                if( (isset($rule['id_rule_item']) && !empty($rule['id_rule_item']))
                                                    && (isset($rule2['id_rule_item_price_range']) && !empty($rule2['id_rule_item_price_range'])) )
                                                {
                                                    $where2 = array(
                                                        'id_rule_item' => array(
                                                            'value' => $rule["id_rule_item"] , 
                                                            'operation' => '='
                                                        ),
                                                        'id_rule_item_price_range' => array(
                                                            'value' => $rule2["id_rule_item_price_range"] , 
                                                            'operation' => '='
                                                        )
                                                    );

                                                    $sql_item_price_range = $db->update_string('rule_item_price_range', $insert_rule_price_range[$key2], $where2);
                                                    if(!$db->db_exec($sql_item_price_range,false,false))
                                                           continue;
                                                }
                                                else
                                                {
                                                    $insert_rule_price_range[$key2]["id_rule_item"] = $rule['id_rule_item'];
                                                    $sql_item_price_range = $db->insert_string('rule_item_price_range', $insert_rule_price_range[$key2]);

                                                    if(!$db->db_exec($sql_item_price_range,false,false))
                                                        continue;
                                                }
                                            }
                                        }
                                        
                                    }
                                    else
                                    {
                                        $insert_rule[$key]["id_rule"] = $data["id_rule"];
                                        $sql_item = $db->insert_string('rule_item', $insert_rule[$key]);

                                        if(!$db->db_exec($sql_item,false,false))
                                            continue;
                                        
                                        if(isset($rule["price_range"]) && !empty($rule["price_range"]))
                                        {
                                            $id  = $db->db_last_insert_rowid();
                                            $insert_rule_price_range = array();
                                            foreach ($rule["price_range"] as $key2 => $rule2)
                                            { 
                                                $insert_rule_price_range[$key2]['id_shop'] = (int)$data["id_shop"];
                                                $insert_rule_price_range[$key2]['id_mode'] = (int)$data["id_mode"];
                                                $insert_rule_price_range[$key2]['price_range_from'] = (isset($rule2['price_range_from']) && $rule2['price_range_from'] != null ) ? 
                                                        (int)$rule2['price_range_from'] : null;
                                                $insert_rule_price_range[$key2]['price_range_to'] = (isset($rule2['price_range_to']) && $rule2['price_range_to'] != null ) ? 
                                                        (int)$rule2['price_range_to'] : null;
                                                $insert_rule_price_range[$key2]["id_rule_item"] = $id;
                                                $sql_item_price_range = $db->insert_string('rule_item_price_range', $insert_rule_price_range[$key2]);

                                                if(!$db->db_exec($sql_item_price_range,false,false))
                                                    continue;
                                            }
                                        }
                                    }
                                //echo '<pre>' . print_r($sql_item, true) . '</pre>';
                            } //exit;
                            }
                        }
                     }
                }
                else 
                {
                    $sql = $db->insert_string(self::$definition['table'], $insert_data);
                
                    if(!$sql)
                        continue;
                    else
                    {
                       if(!$db->db_exec($sql,false,false))
                           continue;
                       else
                       {
                            $insert_rule = array();
                            $id  = $db->db_last_insert_rowid();
                            if(isset($data["rule"]))
                            foreach ($data["rule"] as $key => $rule)
                            { 
                                if( (isset($rule['id_supplier']) || !empty($rule['id_supplier'])) 
                                            || (isset($rule['id_manufacturer']) || !empty($rule['id_manufacturer']))
                                            || (isset($rule['price_range_from']) || !empty($rule['price_range_from']))
                                            || (isset($rule['action']) || !empty($rule['action'])))
                                {

                                    $insert_rule[$key]["id_rule"] = $id;
                                    $insert_rule[$key]['id_shop'] = (int)$data["id_shop"];
                                    $insert_rule[$key]['id_mode'] = (int)$data["id_mode"];
                                    $insert_rule[$key]['id_supplier'] = (isset($rule['id_supplier']) && !empty($rule['id_supplier'])) ? 
                                            (int)$rule['id_supplier'] : '';
                                    $insert_rule[$key]['id_manufacturer'] = (isset($rule['id_manufacturer']) && !empty($rule['id_manufacturer'])) ? 
                                            (int)$rule['id_manufacturer'] : '';
                                    $insert_rule[$key]['action'] = (isset($rule['action']) && $rule['action'] != null) ? 
                                            (int)$rule['action'] : null;
                                    
                                    $sql_item = $db->insert_string('rule_item', $insert_rule[$key]);

                                    if(!$db->db_exec($sql_item,false,false))
                                        continue;
                                    
                                    if(isset($rule["price_range"]) && !empty($rule["price_range"]))
                                    {
                                        $id2  = $db->db_last_insert_rowid();
                                        $insert_rule_price_range = array();
                                        foreach ($rule["price_range"] as $key2 => $rule2)
                                        {        
                                            $insert_rule_price_range[$key2]['id_shop'] = (int)$data["id_shop"];
                                            $insert_rule_price_range[$key2]['id_mode'] = (int)$data["id_mode"];
                                            $insert_rule_price_range[$key2]['price_range_from'] = (isset($rule2['price_range_from']) && $rule2['price_range_from'] != null ) ? 
                                                    (int)$rule2['price_range_from'] : null;
                                            $insert_rule_price_range[$key2]['price_range_to'] = (isset($rule2['price_range_to']) && $rule2['price_range_to'] != null ) ? 
                                                    (int)$rule2['price_range_to'] : null;

                                            $insert_rule_price_range[$key2]["id_rule_item"] = $id2;
                                            $sql_item_price_range = $db->insert_string('rule_item_price_range', $insert_rule_price_range[$key2]);

                                            if(!$db->db_exec($sql_item_price_range,false,false))
                                                continue;
                                        }
                                    }
                                    
                                }
                            }
                       }
                    }
                }
                
            }
        }
        
        return true;
    }
    
    public static function deleteRules($id_rule, $id_shop, $id_mode)
    {
        $db = ObjectModel::$db;
        
        //rule
        $sql = $db->delete_string(self::$definition['table'],  
                array(
                    'id_rule' => array(
                        'value' => $id_rule , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '='
                        )
                    )
                );
        
        //select id_rule_item
        $db->from('rule_item');
        $db->where(array('id_rule' => $id_rule));
        
        $result = $db->db_array_query($db->query);
        
        //rule_item_price_range
        if(isset($result) && !empty($result))
            foreach ($result as $rule_item)
            {
                $sql .= $db->delete_string('rule_item_price_range', 
                array(
                    'id_rule_item' => array(
                        'value' => $rule_item['id_rule_item'] , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '='
                        ),
                    )
                );
            }
                
        //rule_item
        $sql .= $db->delete_string('rule_item', 
                array(
                    'id_rule' => array(
                        'value' => $id_rule , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '='
                        ),
                    )
                );
        
        $sql .= $db->delete_string('profile_item', 
                array(
                    'id_rule' => array(
                        'value' => $id_rule , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '='
                        )
                    )
                );
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
    
    public static function deleteRulesItem($id_rule_item, $id_shop, $id_mode)
    {
        $db = ObjectModel::$db;
        
        $sql = $db->delete_string('rule_item', 
                array(
                    'id_rule_item' => array(
                        'value' => $id_rule_item , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '='
                        )
                    )
                );
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
    
    public static function deletePriceRangeItem($id_rule_item_price_range, $id_shop, $id_mode)
    {
        $db = ObjectModel::$db;
        
        $sql = $db->delete_string('rule_item_price_range', 
                array(
                    'id_rule_item_price_range' => array(
                        'value' => $id_rule_item_price_range , 
                        'operation' => '='
                        ),
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='
                        ),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '=')
                    )
                );
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
}