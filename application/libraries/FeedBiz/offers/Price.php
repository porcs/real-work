<?php

class Price extends ObjectModel
{
    public static $definition = array(
        'table'     => 'profile_price',
        'fields'    => array(
            'id_profile_price'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 3 ),
            'price_percentage'  =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'price_value'       =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'rounding'          =>  array('type' => 'int', 'required' => false, 'size' => 1 ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public static function getProfilePrice($id_shop, $id_mode)
    {
        $prices = array();
        $price_name = array();
        $i = 1;
        $_db = ObjectModel::$db;
        $_db->from(self::$definition['table']);
        $_db->where(array('id_shop' => $id_shop, 'id_mode' => $id_mode));
        $result = $_db->db_array_query($_db->query);
        
        if(isset($result) && !empty($result))
        {
            foreach ($result as $price)
            {
                $name = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace (" ", "_", $price['name']));
                if(isset($price_name[$price['name']]) && $price_name[$price['name']] == true)
                    $name = $name . '_' . $i++;
                
                $prices[$name]['id_profile_price'] = $price['id_profile_price'];
                $prices[$name]['price_percentage'] = $price['price_percentage'];
                $prices[$name]['price_value'] = $price['price_value'];
                $prices[$name]['rounding'] = $price['rounding'];
                $prices[$name]['name'] = $price['name'];
                $price_name[$price['name']] = true;
            }
        }
        
        return $prices;
    }
    
    public static function saveProfilePrice($datas)
    {
        $db = ObjectModel::$db;
        foreach ($datas as $data)
        {   
            if( (isset($data["name"]) && !empty($data["name"]) && $data["name"] != NULL) 
                && (isset($data["price_percentage"]) || isset($data["price_value"])) )
            {
                if(isset($data["id_profile_price"]) && !empty($data["id_profile_price"])) {
                    $sql = $db->update_string(self::$definition['table'], $data, array('id_profile_price' => array('value' => $data["id_profile_price"] , 'operation' => '=')));
                } else {
                    $sql = $db->insert_string(self::$definition['table'], $data);
                }
                if($sql == false) return false;
                if(!$db->db_exec($sql, true))  return false;
            }
        }
        return true;
    }
    
    public static function deleteProfilePrice($id_profile_price, $id_shop, $id_mode)
    {
        $db = ObjectModel::$db;
        
        $sql = $db->delete_string(self::$definition['table'], 
                array(
                    'id_profile_price' => array(
                        'value' => $id_profile_price , 
                        'operation' => '='), 
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '=')
                    )
                );
        
        //update_string
        $sql .= $db->update_string(
                    'profile', 
                    array(
                        'id_profile_price' => null
                    ),
                    array(
                    'id_profile_price' => array(
                        'value' => $id_profile_price , 
                        'operation' => '='), 
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '=')
                    )                
                );
        
        $sql .= $db->update_string(
                    'rule_item', 
                    array(
                        'action' => null
                    ),
                    array(
                    'action' => array(
                        'value' => $id_profile_price , 
                        'operation' => '='), 
                    'id_shop' => array(
                        'value' => $id_shop , 
                        'operation' => '='),
                    'id_mode' => array(
                        'value' => $id_mode , 
                        'operation' => '=')
                    )                
                );
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
}