<?php

class ProductAttribute extends ObjectModel
{   
    public static $definition = array(
        'table'     => 'product_attribute',
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'reference'             =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'sku'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'ean13'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 13, 'default_null' => true ),
            'price'                 =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'wholesale_price'       =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'price_type'            =>  array('type' => 'tinyint', 'required' => false, 'default_null' => true ),
            'quantity'              =>  array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0' ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),       
    );          
}