<?php

class Conditions extends ObjectModel
{
    public static $definition = array(
        'table'     => 'conditions',
        'fields'    => array(
            'id_condition'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public static function getConditionNameByID($id_condition, $id_shop) 
    {
        $condition = '';
        if(!isset($id_condition) || empty($id_condition))
            return NULL;
        
        $_db = ObjectModel::$db;
        $_db->from('conditions');
        $_db->where( array('id_condition' => (int)$id_condition, 'id_shop' => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $cond)
            $condition =  $cond['name'];
        
        return $condition;
    }
   
    public static function getConditions($id_shop) 
    {
        $condition = array();
        $_db = ObjectModel::$db;
        $_db->from('conditions');
        $_db->where( array('id_shop' => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $cond)
            $condition[$cond['id_condition']] =  $cond['name'];
        
        return $condition;
    }
       
}