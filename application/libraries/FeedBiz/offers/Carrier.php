<?php

class Carrier extends ObjectModel
{
    public $id_carrier;
    public $id_tax;
    public $name;
    
    public static $definition = array(
        'table'     => 'carrier',
        'primary'   => array('id_carrier'),
        'fields'    => array(
            'id_carrier'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_tax'   =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'tmpcode'           =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'date_add'      =>  array('type' => 'date', 'required' => true ),
        ),
    );

    public static function getCarriersByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $carriers = array();
        $_db = ObjectModel::$db;
        $_db->select(array('pc.id_carrier_default', 'c.name', 'c.id_tax'));
        $_db->from('product pc');
        $_db->leftJoin('carrier', 'c', '(pc.id_carrier_default = c.id_carrier AND pc.id_shop = c.id_shop)');
        $_db->where(array('pc.id_product' => (int)$id_product, 'pc.id_shop' => (int)$id_shop));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $carrier)
        {
            $carriers[$carrier['id_carrier_default']]['name'] = (string)$carrier['name'];

            $tax_rate = Tax::getTaxesRate($carrier['id_tax'], $id_shop);
            if(isset($tax_rate) && !empty($tax_rate))
                $carriers[$carrier['id_carrier_default']]['tax'] = $tax_rate;
        }
        
        return $carriers;
    }
    
    public static function getCarriers($id_shop)
    {
        $carriers = array();
        
        $_db = ObjectModel::$db;
        $_db->from('carrier');
        $_db->where(array('id_shop' => (int)$id_shop));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $carrier)
        {
            $carriers[$carrier['id_carrier']]['name'] = (string)$carrier['name'];

            $tax_rate = Tax::getTaxesRate($carrier['id_tax']);
            $carriers[$carrier['id_carrier']]['tax'][$carrier['id_tax']] = $tax_rate;
        }
        
        return $carriers;
    }
    
}