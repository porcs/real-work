<?php

class Category extends ObjectModel {

    public static $definition = array(
        'table' => 'category',
        'fields' => array(
            'id_category' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_parent' => array('type' => 'int', 'required' => false, 'size' => 10),
            'is_root_category' => array('type' => 'tinyint', 'required' => true, 'size' => 1),
            'date_add' => array('type' => 'datetime', 'required' => true),
        ),
    );

    public function __construct($user, $database = null) {
        parent::__construct($user, null, null, $database);
        $this->db = new stdClass();
        $this->db->product = new Db(ObjectModel::$user, 'products');
    }

    public static function getCategoryByProductID($id_product, $id_shop) {
        if (!isset($id_product) || empty($id_product))
            return NULL;

        $categories = array();
        $_db = ObjectModel::$db;
        $_db->select(array('c.id_category', 'c.id_parent', 'c.is_root_category'));
        $_db->from('product pc');
        $_db->leftJoin('category', 'c', '(pc.id_category_default = c.id_category AND pc.id_shop = c.id_shop)');
        $_db->where(array('pc.id_product' => (int) $id_product, 'c.id_shop' => (int) $id_shop));

        $result = $_db->db_array_query($_db->query);

        foreach ($result as $category) {
            $categories[$category['id_category']]['name'] = Category::getCategoryNameByID($category['id_category'], $id_shop);
            $parent = Category::getParentsByCatagoryID($category['id_parent'], $id_shop);
            $categories[$category['id_category']]['parent'] = $parent;
        }

        return $categories;
    }

    //Return array Category Name
    public static function getCategoryNameByID($id_category, $id_shop, $id_lang = null) {
        if (!isset($id_category) || empty($id_category))
            return NULL;

        $categories = array();

        $_db = ObjectModel::$db;
        $_db->select(array('id_category', 'name', 'id_lang'));
        $_db->from('category_lang');
        $arr_where['id_category'] = (int) $id_category;
        $arr_where['id_shop'] = (int) $id_shop;

        if (isset($id_lang) && !empty($id_lang))
            $arr_where['id_lang'] = (int) $id_lang['id_lang'];

        $_db->where($arr_where);

        $result = $_db->db_array_query($_db->query);

        foreach ($result as $category) {
            $categories = $category['name'];
        }

        return $categories;
    }

    //Return array Category Parent
    public static function getParentsByCatagoryID($id_category, $id_shop) {
        if (!isset($id_category) || empty($id_category))
            return NULL;

        $categories = array();

        $_db = ObjectModel::$db;
        $_db->from('category');
        $_db->where(array('id_category' => (int) $id_category, 'id_shop' => (int) $id_shop));
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $category) {
            $categories[$id_category]['id'] = $category['id_category'];
            $categories[$id_category]['name'] = Category::getCategoryNameByID($category['id_category'], $id_shop);

            if ($category['is_root_category'] == 0)
                $categories[$id_category]['parent'] = Category::getParentsByCatagoryID($category['id_parent'], $id_shop);
        }

        return $categories;
    }

    //Return array Categories
    public static function getCategories($id_shop) {
        $categories = array();

        $_db = ObjectModel::$db;
        $_db->from('category');
        $_db->where(array('id_shop' => (int) $id_shop));
        $_db->orderBy('name');
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $category) {
            $categories[$category['id_category']]['id_profile'] = $category['id_profile'];
            $categories[$category['id_category']]['name'] = Category::getCategoryNameByID($category['id_category'], $id_shop);
            $categories[$category['id_category']]['id_parent'] = $category['id_parent'];
            $categories[$category['id_category']]['is_root_category'] = $category['is_root_category'];
        }

        return $result;
    }

    public function getSubCategories($id_parent, $id_shop) {
        $_db = ObjectModel::$db;
        if (!isset($this->arr_cat_by_parent)) {
            $sql = "select c.id_category  ,c.id_parent from products_category c where c.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);

            foreach ($out as $v) {
                if ($v['id_parent'] == $v['id_category']) {
                    continue;
                }
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] = $v;
            }
        }
//        $sql = "SELECT * FROM {$_db->prefix_table}category c 
//                LEFT JOIN {$_db->prefix_table}category_selected cs ON c.id_category = cs.id_category AND c.id_shop = cs.id_shop
//                WHERE c.id_parent = " . (int)$id_parent . " 
//                AND c.id_category <> " . (int)$id_parent . " 
//                AND c.id_shop = " . (int)$id_shop . "
//                ORDER BY c.id_category";
//        $result = $_db->db_sqlit_query($sql);
//        return $_db->db_num_rows($result) ;
        if (isset($this->arr_cat_by_parent[$id_parent])) {
            return sizeof($this->arr_cat_by_parent[$id_parent]);
        } else {
            return 0;
        }
    }

    public function getRootCategories($id_shop) {
        $root = '';

        $_db = ObjectModel::$db;
        $_db->select(array('id_category'));
        $_db->from('category');
        $arr_where['is_root_category'] = 1;
        $arr_where['id_shop'] = (int) $id_shop;
        $_db->where($arr_where);
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $category)
            $root = $category['id_category'];

        return $root;
    }

    public function getAllRootCategories($id_shop) {
        $root = '';
        $_db = ObjectModel::$db;
        $_db->select(array('id_category', 'id_parent'));
        $_db->from('category');
        $arr_where['id_shop'] = (int) $id_shop;
        $_db->where($arr_where);
        $result = $_db->db_array_query($_db->query);
        $all_parent = array();
        $all_category = array();
        foreach ($result as $category) {
            $all_parent[$category['id_parent']][$category['id_category']] = $category['id_category'];
            $all_category[$category['id_category']] = $category;
        }
        foreach ($all_parent as $par_id => $id) {
            if (!isset($all_category[$par_id])) {
                foreach ($all_parent[$par_id] as $c_id => $id) {
                    $root[] = $c_id;
                }
            }
        }

        return $root;
    }

    public function getSelectedCategory($id_parent, $id_shop, $id_lang = null) {
        $categories = array();
        $l = '';

        $_db = ObjectModel::$db;

        if (!isset($this->default_lang_id)) {
            $sql = "select l.id_lang,l.is_default from products_language l where l.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            foreach ($out as $v) {
                if ($v['is_default'] == 1) {
                    $this->default_lang_id = $v['id_lang'];
                }
            }
        }
        if (empty($this->default_lang_id)) {
            $this->default_lang_id = 0;
        }

        if (!isset($this->arr_cat_lang)) {
            $this->arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name,cl.id_lang from products_category_lang cl where '{$id_shop}'= cl.id_shop  ";

            $result = $_db->db_query_str($sql_cat_lang);
            foreach ($result as $v) {
                $this->arr_cat_lang[$v['id_lang']][$v['id_category']] = $v['name'];
            }
        }
        if (!isset($this->arr_cat_by_parent)) {
            $sql = "select c.id_category  ,c.id_parent from products_category c where c.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);

            foreach ($out as $v) {
                if ($v['id_parent'] == $v['id_category']) {
                    continue;
                }
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] = $v;
            }
        }

        if (isset($id_lang) && !empty($id_lang))
            $l = ' AND cl.id_lang = ' . $id_lang . ' ';
        else
            $l = ' AND l.is_default = 1 ';

//        $sql = "SELECT c.id_category as id_category, c.id_parent as id_parent, cl.name as name  
//                FROM {$_db->prefix_table}category c
//                LEFT JOIN {$_db->prefix_table}category c2 ON c.id_category = c2.id_parent AND c.id_shop = c2.id_shop 
//                LEFT JOIN {$_db->prefix_table}category_lang cl ON c.id_category = cl.id_category AND c.id_shop = cl.id_shop  
//                LEFT JOIN {$_db->prefix_table}language l ON cl.id_lang = l.id_lang AND c.id_shop = cl.id_shop  
//                WHERE c.id_parent = " . $id_parent . " 
//                AND c.id_category <> " . $id_parent . " 
//                AND c.id_shop = " . $id_shop . "
//                " . $l . "
//                GROUP BY c.id_category 
//                ORDER BY c.id_category ASC";
//        $result = $_db->db_query_str($sql);
        $result = isset($this->arr_cat_by_parent[$id_parent]) ? $this->arr_cat_by_parent[$id_parent] : array();

        if (isset($id_lang) && !empty($id_lang))
            $l_id = $id_lang;
        else
            $l_id = $this->default_lang_id;
        $keys = 0;
        foreach ($result as $c_id => $value) {

            $categories[$keys]['id_category'] = $c_id;
            $categories[$keys]['parent'] = $id_parent;
            $categories[$keys]['name'] = isset($this->arr_cat_lang[$l_id][$c_id]) ? $this->arr_cat_lang[$l_id][$c_id] : ''; //$val;
//            foreach ( $value as $key => $val)
//            {
//                if( !is_int($key))
//                {
//                    $key = str_replace(array("c.","cs.","c2."), "", $key);
//                    if($key == "id_category")
//                        $categories[$keys]['id_category'] = $val;
//                    
//                    if($key == "id_parent")
//                        $categories[$keys]['parent'] = $val;
//                    
//                    if($key == "name")
//                        $categories[$keys]['name'] = $val;
//                }
//            }  

            $categories[$keys]['child'] = $this->getSubCategories($value['id_category'], $id_shop);

            if (isset($categories[$keys]['child']) && !empty($categories[$keys]['child']) && $categories[$keys]['child'] > 0)
                $categories[$keys]['type'] = "folder";
            else
                $categories[$keys]['type'] = "item";
            $keys++;
        }

        return $categories;
    }

    public function getSelectedCategoriesProfileID($id_category, $id_mode = null, $id_shop = null) {
        $_db = ObjectModel::$db;
        $content = array();
        $mode = '';
        $shop = '';

        if (!isset($this->arr_cat_selected)) {
            $sql = "select * from {$_db->prefix_table}category_selected  where  id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);

            foreach ($out as $v) {
                $this->arr_cat_selected[$v['id_category']] = $v;
                $this->arr_cat_selected_mode[$v['id_mode']][$v['id_category']] = $v;
            }
        }

//        if(isset($id_mode) && !empty($id_mode))
//            $mode = " AND id_mode = " . $id_mode . " ";
//        
//        if(isset($id_shop) && !empty($id_shop))
//            $shop = " AND id_shop = " . $id_shop . " ";
//        
//        if(!empty($where))
//            $_db->where($where);
//        
//        $sql = "SELECT * FROM {$_db->prefix_table}category_selected
//                WHERE id_category = " . $id_category . " 
//                " . $mode . $shop . "; ";
//        
//        $result = $_db->db_query_str($sql);
        if (isset($id_mode) && !empty($id_mode)) {
            $result = isset($this->arr_cat_selected_mode[$id_mode][$id_category]) ? $this->arr_cat_selected_mode[$id_mode][$id_category] : array();
        } else {
            $result = isset($this->arr_cat_selected[$id_category]) ? $this->arr_cat_selected[$id_category] : array();
        }

        foreach ($result as $keys => $value) {
            if (!is_array($value)) {
                return $result;
            }
            foreach ($value as $key => $val) {
                if (!is_int($key))
                    $content[$key] = $val;
            }
        }

        return $content;
    }

    public function getParents($id, $id_shop) {
        $parents = $this->getParentsByCatagoryID($id, $id_shop);

        $ref = array();

        if (isset($parents) && !empty($parents)) {
            foreach ($parents as $dep) {
                if (isset($dep['parent']) && !empty($dep['parent'])) {
                    $item_id = $dep['id'];

                    foreach ($dep['parent'] as $parent) {
                        $id_parent = $parent['id'];

                        if (isset($parent['parent']))
                            $ref[$item_id] = $this->getParents($id_parent, $id_shop);

                        if (!isset($ref[$id_parent]))  //Child
                            $ref[$id_parent] = array('id' => $item_id, 'name' => $dep['name'], 'parent' => $id_parent);

                        $ref[$item_id]['children'][$id_parent] = $ref[$id_parent];
                    }
                }
            }
        }

        if (isset($ref[$id]))
            return $ref[$id];

        return;
    }

    public static function saveSelectedCategory($datas, $id_shop, $id_mode) {
        $db = ObjectModel::$db;
        $sql_trunc = $db->truncate_table('category_selected WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode);

        if ($sql_trunc) {
            foreach ($datas as $data) {
                if (isset($data["id_category"]) && !empty($data["id_category"])) {
                    $sql = $db->replace_string('category_selected', $data);

                    if ($sql == false)
                        return false;

                    if (!$db->db_exec($sql, true))
                        return false;
                }
            }
        }
        return true;
    }

}
