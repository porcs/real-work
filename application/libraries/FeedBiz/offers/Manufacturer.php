<?php

class Manufacturer extends ObjectModel
{
    public static $definition = array(
        'table'     => 'manufacturer',
        'fields'    => array(
            'id_manufacturer'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'tmpcode'       =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
   
    public static function getManufacturerByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
         
        $_db = ObjectModel::$db;

        $_db->select(array('m.id_manufacturer', 'm.name'));
        $_db->from('product p');
        $_db->leftJoin('manufacturer', 'm', '(p.id_manufacturer = m.id_manufacturer AND p.id_shop = m.id_shop)');
        $_db->where( array("p.id_product"  => (int)$id_product, "p.id_shop"  => (int)$id_shop) );

        $result = $_db->db_array_query($_db->query);

        $manufacturer = array();
        foreach ($result as $values)
        {
            if(isset($values['id_manufacturer']) || !empty($values['id_manufacturer']))
            {
                $manufacturer_name = $values['name'];
                $manufacturer[$values['id_manufacturer']] = $manufacturer_name;
            }
            else 
                return null;
        }
        return $manufacturer ;
    }
    
    public static function getManufacturers($id_shop)
    {
        $_db = ObjectModel::$db;
        $_db->from('manufacturer');
        $_db->where(array('id_shop' => (int)$id_shop));

        $result = $_db->db_array_query($_db->query);

        $manufacturer = array();
        
        foreach ($result as $values)
        {
            if(isset($values['id_manufacturer']) || !empty($values['id_manufacturer']))
                $manufacturer[$values['id_manufacturer']] = $values['name'];
            else 
                return null;
        }
        
        return $manufacturer ;
    }
}