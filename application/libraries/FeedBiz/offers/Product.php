<?php

class Product extends ObjectModel
{
    //Limit per page
    private $limit_page = 300;
    
    /** @var integer Product id */
    public $id_shop;
    
    /** @var integer Product id */
    public $id_product;
        
    /** @var integer Supplier id */
    public $id_supplier;
    
    /** @var integer Manufacturer id */
    public $id_manufacturer;
    
    /** @var integer default Category id */
    public $id_category_default;
        
    /** @var boolean on_sale */
    public $on_sale = false;
    
    /** @var string Reference */
    public $reference;
    
    /** @var string SKU */
    public $sku;
    
    /** @var string Ean-13 barcode */
    public $ean13;
    
    /** @var string Upc barcode */
    public $upc;
        
    /** @var float Ecotax */
    public $ecotax = 0;
    
    /** @var integer Quantity available */
    public $quantity = 0;
    
    /** @var float Price in euros */
    public $price = 0;
    public $wholesale_price = 0;
    
    /** @var int Current id */
    public $id_currency = 0;
       
    /** @var boolean Product statuts */
    public $active = true;
   
    /** @var enum Product condition (new, used, refurbished) */
    public $id_condition;
    
    /** @var int Tax id */
    public $id_tax;
    
    /** @var int Number of Attribute */
    public $has_attribute;
    
    /** @var string Object creation date */
    public $date_add;
    
    /** @var array Supplier id */
    public $supplier;
    
    /** @var array Manufacturer */
    public $manufacturer;
        
    /** @var array Category */
    public  $category;
    
    /** @var array Currency */
    public $currency;
    
    /** @var string Condition Name */
    public $condition;
    
    /** @var array Tax */
    public $tax;
       
    /** @var array Carrier */
    public $carrier;
    
    public $shipping_cost;
    
    /* @see ObjectModel::$definition */
    public static $definition = array(
        'table'     => 'product',
        'primary'   => array('id_product'),
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'               =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_supplier'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_manufacturer'       =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_category_default'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_currency'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'on_sale'               =>  array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0' ),
            'reference'             =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'sku'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 64 ),
            'ean13'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 13 ),
            'upc'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 12 ),
            'ecotax'                =>  array('type' => 'decimal', 'required' => false, 'size' => '17,6', 'default' => '0.000000' ),
            'quantity'              =>  array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0' ),
            'price'                 =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'wholesale_price'       =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'id_condition'          =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true  ),
            'id_tax'                =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'shipping_cost'         =>  array('type' => 'decimal', 'required' => false, 'size' => 10 ),
            'has_attribute'         =>  array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'  ),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
            'date_upd'              =>  array('type' => 'datetime', 'required' => false ),
            
        ),
        'table_link' => array( 
            '0' => array( 'name' => 'product_attribute', 'lang' => false ),
            '1' => array( 'name' => 'product_sale', 'lang' => false ),
        ),
       
    );
    
    public function __construct( $user, $id_product = null, $id_lang = null )
    {
        parent::__construct( $user, (int)$id_product, $id_lang);
        if ($this->id)
        {
            $this->supplier = Supplier::getSupplierByProductId((int)$this->id, (int)$this->id_shop);
            $this->manufacturer = Manufacturer::getManufacturerByProductId((int)$this->id, (int)$this->id_shop);
            $this->carrier = Carrier::getCarriersByProductId((int)$this->id, (int)$this->id_shop);
            
            if ($this->id_category_default)
                $this->category = Category::getCategoryByProductID((int)$this->id, (int)$this->id_shop);
            
            $this->tax = Tax::getTaxesRateByProductId((int)$this->id, (int)$this->id_shop);
            $this->currency = Currency::getCurrencyById((int)$this->id_currency, (int)$this->id_shop);
            $this->condition = Conditions::getConditionNameByID((int)$this->id_condition, (int)$this->id_shop);
            
            if ($this->on_sale != 0)
                $this->sale = ProductSale::getProductSaleByProductId((int)$this->id, (int)$this->id_shop);
        }
        
        $combinations = Product::getProductCombinationByProductId((int)$this->id, (int)$this->id_shop);
        if($this->has_attribute)
        {
            $product_combination = array();
            if(isset($combinations) && !empty($combinations))
            {
                foreach ($combinations as $combination)
                {   
                    $product_combination[$combination['id_product_attribute']] = $combination;
                }
                $this->combination = $product_combination;
            }
        }
    }
        
    public function exportOffers($id_mode = null, $page = 1)
    {
        $start = $limit = 1;
        $query = $category = $l = $id_shop = '';
        $data_list = $product_list = $history = array();
        
        $_db = ObjectModel::$db;
        $prefix = isset($_db->prefix_table) ? $_db->prefix_table : 'offers';
        
        if(isset($page) && !empty($page))
        {
            $start_page = (($this->limit_page * ($page - $start)) + 1);
            $start =  $start_page == 1 ? 0 : $start_page ;
        }
        
        if(isset($this->id_shop) && !empty($this->id_shop))
            $id_shop = ' AND cs.id_shop = ' . $this->id_shop ;
        
        if(isset($id_mode) && !empty($id_mode))
            $mode = ' AND cs.id_mode = ' . $id_mode ;
        
        if( isset($id_mode) && $id_mode != 1 )
        { 
            $query = 'SELECT 
                p.id_product as id_product,
                ri.id_supplier as id_supplier, 
                ri.id_manufacturer as id_manufacturer,
                ripr.price_range_from as price_range_from, 
                ripr.price_range_to as price_range_to, 
                p.id_supplier as product_id_supplier,
                p.id_manufacturer as product_id_manufacturer,
                p.price as product_price,
                r.name as rule_name,
                ri.action as rule_item_action,
                pp.price_percentage as profile_price_percentage,
                pp.rounding as profile_price_rounding,
                pp.price_value as profile_price_value
            FROM product p
            LEFT JOIN '.$prefix.'category_selected cs ON p.id_category_default = cs.id_category AND p.id_shop = cs.id_shop
            LEFT JOIN '.$prefix.'profile pr ON cs.id_profile = pr.id_profile AND cs.id_shop = pr.id_shop
            LEFT JOIN '.$prefix.'profile_item pri ON pr.id_profile = pri.id_profile AND pr.id_shop = pri.id_shop
            LEFT JOIN '.$prefix.'rule r ON pri.id_rule = r.id_rule AND pri.id_shop = r.id_shop
            LEFT JOIN '.$prefix.'rule_item ri ON ri.id_rule = r.id_rule AND r.id_shop = ri.id_shop
            LEFT JOIN '.$prefix.'rule_item_price_range ripr ON ri.id_rule_item = ripr.id_rule_item AND ri.id_shop = ripr.id_shop
            LEFT JOIN '.$prefix.'profile_price pp ON (ri."action" = pp.id_profile_price AND ri.id_shop = pp.id_shop) OR 
                (pr.id_profile_price = pp.id_profile_price AND pr.id_shop = pp.id_shop)
            WHERE p.id_category_default 
                IN ( 
                    SELECT cs.id_category 
                    FROM '.$prefix.'category_selected cs
                    LEFT JOIN '.$prefix.'profile p ON cs.id_profile = p.id_profile AND cs.id_shop = p.id_shop AND cs.id_mode = p.id_mode
                    WHERE cs.id_profile != 0 ' . $id_shop . ' ' . $mode . ' 
                )
            AND cs.id_profile != 0 ' . $id_shop . ' ' . $mode . ' ' ;            
            $query .= ' LIMIT ' . $this->limit_page .  ' OFFSET ' . $start  . ' ;';
            $result = $_db->db_query_str($query);

            if(isset($result) && !empty($result))
            {
                foreach ($result as $data)
                {
                    $id_product = $data['id_product'];
                    //supplier
                    if(!empty($data['id_supplier']) || $data['id_supplier'] != NULL)
                    {
                        //manufacturer
                        if(!empty($data['id_manufacturer']) || $data['id_manufacturer'] != NULL)
                        {      
                            //price range
                            if((!empty($data['price_range_from']) || $data['price_range_from'] != NULL) 
                            && (!empty($data['price_range_to']) || $data['price_range_to'] != NULL))
                            {
                                //supplier & manufacturer & price range
                                if(($data['id_supplier'] == $data['product_id_supplier']) 
                                    && ($data['id_manufacturer'] == $data['product_id_manufacturer'])
                                    && ( $data['product_price'] >= $data['price_range_from'] && $data['product_price'] <= $data['price_range_to']))
                                {
                                    $data_list[$id_product]['case']  = $data['rule_name'] ;
                                }
                            }
                            else 
                            {   
                                //supplier & manufacturer & !price range
                                if(($data['id_supplier'] == $data['product_id_supplier']) && ($data['id_manufacturer'] == $data['product_id_manufacturer']))
                                {
                                    $data_list[$id_product]['case']  = $data['rule_name'] ;

                                }
                            }
                        }           
                        //supplier & !manufacturer            
                        else 
                        {
                             if(($data['id_supplier'] == $data['product_id_supplier']))
                             {
                                $data_list[$id_product]['case']  = $data['rule_name'] ;
                             }
                        }               
                    }      

                    //!supplier
                    else 
                    {
                        //manufacturer
                        if(!empty($data['id_manufacturer']) || $data['id_manufacturer'] != NULL)
                        {    
                            //check price range
                            if((!empty($data['price_range_from']) || $data['price_range_from'] != NULL) 
                            && (!empty($data['price_range_to']) || $data['price_range_to'] != NULL))
                            {
                                //!supplier & manufacturer & price range
                                if( ($data['id_manufacturer'] == $data['product_id_manufacturer'])
                                    && $data['product_price'] >= $data['price_range_from'] && $data['product_price'] <= $data['price_range_to'])
                                {
                                    $data_list[$id_product]['case']  = $data['rule_name'] ;
                                }
                            }
                            else 
                            {   
                                //!supplier & manufacturer & !price range
                                if($data['id_manufacturer'] == $data['product_id_manufacturer'])
                                {
                                    $data_list[$id_product]['case']  = $data['rule_name'] ;
                                }
                            }
                        }           
                        //!manufacturer            
                        else 
                        {
                             //price range
                            if((!empty($data['price_range_from']) || $data['price_range_from'] != NULL) 
                            && (!empty($data['price_range_to']) || $data['price_range_to'] != NULL))
                            {
                                 //!supplier & !manufacturer & price range
                                if( $data['product_price'] >= $data['price_range_from'] && $data['product_price'] <= $data['price_range_to'])
                                {
                                    $data_list[$id_product]['case']  = $data['rule_name'] ;
                                }
                                else
                                {
                                    //!supplier & !manufacturer & !price range
                                    $data_list['case']  = $data['rule_name'] ;
                                }
                            }
                        }               
                    } 

                    if( $data['rule_item_action'] != 0 || is_null($data['rule_item_action']) )
                    {
                        if(isset($data_list[$id_product]['case']))
                        {
                            unset($data_list[$id_product]['case']);

                            if(isset($data['profile_price_percentage']) && !empty($data['profile_price_percentage']))
                            {
                                $data_list[$id_product]['price'] = $data['product_price'];
                                $data_list[$id_product]['price_formula'] = $data['profile_price_percentage'] . ' (%)';
                                $calculate_price = round($data['product_price']+($data['product_price']*($data['profile_price_percentage']/100)),$data['profile_price_rounding']);
                                $data_list[$id_product]['rounding'] = $data['profile_price_rounding'];

                                if(isset($data['profile_price_value']) && !empty($data['profile_price_value']))
                                    $calculate_price = $calculate_price + round($data['profile_price_value']);

                                $data_list[$id_product]['price_overide'] = $calculate_price;
                            }
                            else
                            {
                                 if(isset($data['profile_price_value']) && !empty($data['profile_price_value']))
                                    $data_list[$id_product]['price_overide'] = $data['product_price'] + round($data['profile_price_value']);
                            }
                        }

                    }

                    if( isset($data_list[$id_product]['case']) && !empty($data_list[$id_product]['case']) )
                    {
                        if($data['rule_item_action'] == 0)
                            $history[$id_product] = true;

                        unset($product_list[$id_product]);
                        continue;
                    }
                    else 
                    {
                        if( (isset($id_product) && !empty($id_product)) && !isset($history[$id_product]))
                        {
                            $datas = new Product($this->user, (int)$id_product);
                            $product_list[$id_product] = $datas;

                            if(isset($data_list[$id_product]['price_overide']) && !empty($data_list[$id_product]['price_overide']))
                            {
                                $product_list[$id_product]->price = $data['product_price'];
                                $product_list[$id_product]->price_overide = $data_list[$id_product]['price_overide'];
                            }
                        }
                    }
                } // end foreach $result.
            } // end $result.
        }
        else 
        {
            $query = 'SELECT 
                        p.id_product as id_product,
                        p.id_manufacturer as product_id_manufacturer,
                        p.price as product_price,
                        pp.price_percentage as profile_price_percentage,
                        pp.rounding as profile_price_rounding,
                        pp.price_value as profile_price_value
                    FROM product p
                    LEFT JOIN '.$prefix.'category_selected cs ON p.id_category_default = cs.id_category AND p.id_shop = cs.id_shop
                    LEFT JOIN '.$prefix.'profile_price pp ON (cs.id_profile = pp.id_profile_price AND cs.id_shop = pp.id_shop AND cs.id_mode = pp.id_mode)
                    WHERE p.id_category_default 
                        IN ( 
                             SELECT cs.id_category 
                             FROM '.$prefix.'category_selected cs
                             LEFT JOIN '.$prefix.'profile_price p ON cs.id_profile = p.id_profile_price AND cs.id_shop = p.id_shop AND cs.id_mode = p.id_mode
                                WHERE cs.id_profile != 0 ' . $id_shop . ' ' . $mode . ' 
                        )
                    AND cs.id_profile != 0 ' . $id_shop . ' ' . $mode . ' ' ;
            $query .= ' LIMIT ' . $this->limit_page .  ' OFFSET ' . $start  . ' ;';
            
            $result = $_db->db_query_str($query);

            if(isset($result) && !empty($result))
            {
                foreach ($result as $data)
                {
                    $id_product = $data['id_product'];

                    if(isset($data['profile_price_percentage']) && !empty($data['profile_price_percentage']))
                    {
                        $data_list[$id_product]['price'] = $data['product_price'];
                        $data_list[$id_product]['price_formula'] = $data['profile_price_percentage'] . ' (%)';
                        $calculate_price = round($data['product_price'] + ($data['product_price'] * ($data['profile_price_percentage']/100) ) , $data['profile_price_rounding']);
                        $data_list[$id_product]['rounding'] = $data['profile_price_rounding'];

                        if(isset($data['profile_price_value']) && !empty($data['profile_price_value']))
                            $calculate_price = $calculate_price + round($data['profile_price_value']);

                        $data_list[$id_product]['price_overide'] = $calculate_price;
                    }
                    else
                    {
                         if(isset($data['profile_price_value']) && !empty($data['profile_price_value']))
                            $data_list[$id_product]['price_overide'] = $data['product_price'] + round($data['profile_price_value']);
                    }
                   
                    if( (isset($id_product) && !empty($id_product)) && !isset($history[$id_product]))
                    {
                        $datas = new Product($this->user, (int)$id_product);
                        $product_list[$id_product] = $datas;

                        if(isset($data_list[$id_product]['price_overide']) && !empty($data_list[$id_product]['price_overide']))
                        {
                            $product_list[$id_product]->price = $data['product_price'];
                            $product_list[$id_product]->price_overide = $data_list[$id_product]['price_overide'];
                        }
                    }
                } // end foreach $result.
            } // end $result.
        }
        
       // echo $query;
        return $product_list;
    }
    
    public function getProductCombinationByProductId($id_product, $id_sop)
    {
         if(!isset($id_product) || empty($id_product))
            return NULL;
         
        $_db = ObjectModel::$db;
        
        $_db->from('product_attribute');
        $_db->where( array('id_product' => (int)$id_product, 'id_shop' => (int)$id_sop) );
        $result = $_db->db_array_query($_db->query);
        
        $attributes = array();
        
        foreach ($result as $attribute)
        {
            $attributes[$attribute['id_product_attribute']]['id_product_attribute'] = (int)$attribute['id_product_attribute'];
            $attributes[$attribute['id_product_attribute']]['reference'] = $attribute['reference'];
            $attributes[$attribute['id_product_attribute']]['ean13'] = $attribute['ean13'];
            $attributes[$attribute['id_product_attribute']]['upc'] = $attribute['upc'];
            $attributes[$attribute['id_product_attribute']]['quantity'] = (int)$attribute['quantity'];
            $attributes[$attribute['id_product_attribute']]['price'] = (float)$attribute['price'];
            
            if(isset($attribute['price_type']))
                $attributes[$attribute['id_product_attribute']]['price_type'] = $attribute['price_type'];
            
        }
        
        return $attributes;
    }
    
}