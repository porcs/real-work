<?php

class Tax extends ObjectModel
{
   
    public static $definition = array(
        'table'     => 'tax',
        'fields'    => array(
            'id_tax'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'rate'          =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'type'          =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
            'id_currency'   =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_tax'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
                'date_add'  =>  array('type' => 'datetime', 'required' => true ),
            ),
        ),
    );
    
    public static function getTaxesRate($id_tax, $id_shop)
    {
        if(!isset($id_tax) || empty($id_tax))
            return NULL;
        
        $_db = ObjectModel::$db;
        $_db->from('tax');
        $_db->where( array("id_tax" => (int)$id_tax, "id_shop" => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        $rate = null;
        foreach ($result as $value)
            $rate = (float)$value['rate'];

        return $rate;
    }
    
    public static function getTaxesRateByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $_db = ObjectModel::$db;
        $_db->select(array('t.id_tax as id_tax', 't.rate as rate'));
        $_db->from('product p');
        $_db->leftJoin('tax', 't' , '(p.id_tax = t.id_tax)');
        $_db->where( array("p.id_product" => (int)$id_product, "p.id_shop" => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        $rate = array();
        foreach ($result as $value)
            $rate[$value['id_tax']] = (float)$value['rate'];
        
        return $rate;
    }
    
    public static function getTaxes($id_shop)
    {
        $_db = ObjectModel::$db;
        $_db->select(array('t.id_tax as "t.id_tax"', 't.rate as "t.rate"', 't.type as "t.type"', 'tl.id_lang as "tl.id_lang"', 'tl.name as "tl.name"'));
        $_db->from('tax t');
        $_db->leftJoin('tax_lang', 'tl', '(t.id_tax = tl.id_tax)');
        $_db->where( array("t.id_shop" => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages();
        
        $taxs = null;
        foreach ($result as $value)
        {
            $taxs[$value['t.id_tax']]['rate'] = (float)$value['t.rate'];
            $taxs[$value['t.id_tax']]['type'] = $value['t.type'];

            foreach ($lang as $l)
                if($value['tl.id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $taxs[$value['t.id_tax']]['name'][$iso_code] = $value['tl.name'];
        }
        
        return $taxs;
    }
    
}