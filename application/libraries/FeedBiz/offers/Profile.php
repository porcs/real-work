<?php

class Profile extends ObjectModel
{
    public static $definition = array(
        'table'     => 'profile',
        'primary'   => array('id_profile'),
        'fields'    => array(
            'id_profile'    =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'id_shop'       =>  array('type' => 'INTEGER' ),
            'name'          =>  array('type' => 'varchar', 'required' => true ),
            'id_rule'       =>  array('type' => 'INTEGER', 'required' => false),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public static function getProfile($id_shop, $id_mode)
    {
        $profiles = array();
        $profile_name = array();
        $i = 0;
        
        $_db = ObjectModel::$db;
        $_db->from(self::$definition['table']);
        $_db->where(array('id_shop' => $id_shop, 'id_mode' => $id_mode));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $profile)
        {
            
            $name = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace (" ", "_", $profile['name']));
            if(isset($profile_name[$profile['name']]) && $profile_name[$profile['name']] == true)
                $name = $name . '_' . $i++;

            $key = $name;
            $profiles[$key]['id_profile'] = $profile['id_profile'];
            $profiles[$key]['name'] = $profile['name'];
            $profiles[$key]['is_default'] = (int)$profile['is_default'];
            $profiles[$key]['id_profile_price'] = $profile['id_profile_price'];
            
            $_db->from('profile_item');
            $_db->where(array('id_profile' => $profile['id_profile']));
            $result2 = $_db->db_array_query($_db->query);
            
            //item
            foreach ($result2 as $key_item => $profile_item)
            {
                $profiles[$key]['id_rule'][$profile_item['id_rule']]['id_profile_item']      = (int)$profile_item['id_profile_item'];
                $profiles[$key]['id_rule'][$profile_item['id_rule']]['id_rule']      = (int)$profile_item['id_rule'];
            }
            
            $profile_name[$profile['name']] = true;
                
        }
        
        return $profiles;
    }
    
    public static function saveProfile($datas)
    {
        $db = ObjectModel::$db;     
        
        foreach ($datas as $data)
        {              
            if((isset($data["name"]) && !empty($data["name"]) && $data["name"] != NULL))
            {
                if(isset($data["is_default"]) && !empty($data["is_default"]))
                    $is_dafault = $data["is_default"];
                else 
                    $is_dafault = 0;
                    
                $insert_data = array(
                    'name'              => $data["name"], 
                    'is_default'        => $is_dafault, 
                    'id_profile_price'  => isset($data["id_profile_price"]) ? $data["id_profile_price"] : null, 
                    'id_shop'           => (int)$data["id_shop"], 
                    'id_mode'           => (int)$data["id_mode"], 
                    'date_add'          => $data["date_add"]
                );
                
                if(isset($data["id_profile"]) && !empty($data["id_profile"]))
                {
                    $sql = $db->update_string(self::$definition['table'], $insert_data, array(
                        'id_profile' => array(
                            'value' => $data["id_profile"] , 
                            'operation' => '='),
                        'id_shop' => array(
                            'value' => $data["id_shop"] , 
                            'operation' => '='),
                        'id_mode' => array(
                            'value' => $data["id_mode"] , 
                            'operation' => '=')
                            )
                        );
                
                    if(!$sql)
                        continue;
                    else
                    {
                        if(!$db->db_exec($sql,false,false))
                            continue;
                        else
                        {
                            $insert_rule = array();
                            if(isset($data["rule"]) && !empty($data['rule']))
                                foreach ($data["rule"] as $key => $rule)
                                { 
                                    if(isset($rule['id_profile_item']) && !empty($rule['id_profile_item']))
                                    {
                                        $where = array(
                                            'id_profile' => array(
                                                'value' => $data["id_profile"], 
                                                'operation' => '='
                                            ),
                                            'id_profile_item' => array(
                                                'value' => $rule["id_profile_item"] , 
                                                'operation' => '='
                                            ),
                                            'id_shop' => array(
                                                'value' => (int)$data["id_shop"] , 
                                                'operation' => '='
                                            ),
                                            'id_mode' => array(
                                                'value' => $data["id_mode"] , 
                                                'operation' => '='
                                                )
                                        );
                                        
                                        $insert_rule[$key] = array(
                                            'id_rule'   => (int)$rule['id_rule'], 
                                            'id_shop'   => (int)$data["id_shop"], 
                                            'id_mode'   => (int)$data["id_mode"] 
                                        );
                                        
                                        $sql_item = $db->update_string('profile_item', $insert_rule[$key], $where);
                                        if(!$db->db_exec($sql_item,false,false))
                                               continue;

                                    }
                                    else
                                    {
                                        if(isset($rule) && !empty($rule))
                                        {
                                            $insert_rule[$key] = array(
                                                'id_rule'       => (int)$rule['id_rule'],
                                                'id_profile'    => (int)$data["id_profile"],
                                                'id_shop'       => (int)$data["id_shop"],
                                                'id_mode'       => (int)$data["id_mode"] 
                                            );
                                            
                                            $sql_item = $db->insert_string('profile_item', $insert_rule[$key]);

                                            if(!$db->db_exec($sql_item,false,false))
                                                continue;
                                        }
                                    }
                            } 
                        }
                     }
                }
                else
                {
                    if( isset($data['name']) && !empty($data['name']) )
                    {
                        $sql = $db->insert_string(self::$definition['table'], $insert_data);
                        
                        if(!$db->db_exec($sql,false,false))
                            continue;
                        else
                        {
                            $id  = $db->db_last_insert_rowid();
                            $insert_rule = array();
                            
                            if( isset($data["rule"]) && !empty($data["rule"]) )
                               foreach ($data["rule"] as $key => $rule)
                               { 
                                   if( (isset($rule['id_rule']) && !empty($rule['id_rule'])) )
                                   {
                                       $insert_rule[$key] = array(
                                            'id_rule'       => (int)$rule['id_rule'],
                                            'id_profile'    => $id,
                                            'id_shop'       => (int)$data["id_shop"],
                                            'id_mode'       => (int)$data["id_mode"] 
                                        );
                                       
                                       $sql_item = $db->insert_string('profile_item', $insert_rule[$key]);

                                       if(!$db->db_exec($sql_item,false,false))
                                           continue;
                                   }
                               }                            
                        }
                    }
                }
            }
        }    
            
        return true;
    }
    
    public static function deleteProfile($id_profile, $id_shop)
    {
        $db = ObjectModel::$db;
        
        $sql = $db->delete_string(self::$definition['table'], array('id_profile' => array('value' => $id_profile , 'operation' => '=')));
        
        $sql .= $db->delete_string('profile_item', array(
            'id_profile' => array(
                'value' => $id_profile , 
                'operation' => '='
                ),
            'id_shop' => array(
                'value' => $id_shop , 
                'operation' => '='
                ),
            ));
        
        $sql .= $db->delete_string('category_selected', array(
            'id_profile' => array(
                'value' => $id_profile , 
                'operation' => '='
                ),
            'id_shop' => array(
                'value' => $id_shop , 
                'operation' => '='
                ),
            ));
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
 
    public static function deleteProfileItem($id_profile_item, $id_shop)
    {
        $db = ObjectModel::$db;
        
        $sql = $db->delete_string('profile_item', array(
            'id_profile_item' => array(
                'value' => $id_profile_item , 
                'operation' => '='
                ),
            'id_shop' => array(
                'value' => $id_shop , 
                'operation' => '='
                ),
            )
        );
        
        if($sql == false)
            return false;

        if(!$db->db_exec($sql))
            return false;
            
        return true;
    }
    
}