<?php

class Currency extends ObjectModel
{   
    public static $definition = array(
        'table'     => 'currency',
        'fields'    => array(
            'id_currency'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'iso_code'      =>  array('type' => 'varchar', 'required' => true, 'size' => 3 ),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'is_default'    =>  array('type' => 'tinyint', 'required' => true, 'size' => 1),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
			
        ),
    );
    
    public static function getCurrencyById($id_currency, $id_shop)
    {
        if(!isset($id_currency) || empty($id_currency))
            return NULL;
        
        $currency = array();
        
        $_db = ObjectModel::$db;
        $_db->from('currency');
        $_db->where( array('id_currency' => (int)$id_currency, 'id_shop' => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $cur)
        {
            $currency[$cur['id_currency']]['name'] = $cur['name'];
            $currency[$cur['id_currency']]['iso_code'] = $cur['iso_code'];
        }
        
        return $currency;
          
    }    
    
}