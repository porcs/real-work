<?php

class Language extends ObjectModel
{

    public static $definition = array(
        'table'     => 'language',
        'primary'   => array('id_lang'),
        'fields'    => array(
            'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'      =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
            'iso_code'  =>  array('type' => 'varchar', 'required' => true, 'size' => 2),
            'is_default'    =>  array('type' => 'tinyint', 'required' => true, 'size' => 1 ),
        ),
    );
    
    public static function getLanguages($id_shop, $user)
    {
        $languages = array();
        
        $_db = new Db($user, 'products');
        $_db->from('language');
        $_db->where(array('id_shop' => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $language)
        {
            $languages[$language['id_lang']]['id_lang'] = $language['id_lang'];
            $languages[$language['id_lang']]['name'] = $language['name'];
            $languages[$language['id_lang']]['iso_code'] = $language['iso_code'];
        }
            
        return $languages;
    }
    
    public static function getLanguageDefault($id_shop, $user)
    {
        $languages = array();
        
        $_db = new Db($user, 'products');
        $_db->from('language');
        $_db->where(array('is_default' => 1, 'id_shop' => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $language)
        {
            $languages[$language['id_lang']]['id_lang'] = $language['id_lang'];
            $languages[$language['id_lang']]['name'] = $language['name'];
            $languages[$language['id_lang']]['iso_code'] = $language['iso_code'];
        }
            
        return $languages;
    }
}