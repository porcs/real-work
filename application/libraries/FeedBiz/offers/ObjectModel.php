<?php

abstract class ObjectModel
{
    /*@var array Contain object definition*/
    protected $id;
    protected static $user;
    public static $definition = array();

    /*@var array Contain current object definition*/
    protected $def;
    protected static $db = false;
    protected $_db = null;
    public static $database = 'offers';
    
    /* Build object */
    public function __construct($user, $id = null,  $id_lang = null, $database = null )
    {
        if (!isset($user) && !strlen($user))
            return (false);
        
        $this->user = ObjectModel::$user = $user;
        
        $class = get_class($this);
        if (!$class)
            return (false);
        
        $this->def = $class::$definition;
            
        if(isset($database) && $database != null && !empty($database))
            $database_name = $database;
        else
            $database_name = ObjectModel::$database;
                
        ObjectModel::$db = new Db($user, $database_name );
        $this->_db = ObjectModel::$db;
        
        if(!$this->_db)
            return (false);
        
        $shop = Shop::getDefaultShop();
        
        if(isset($shop) && !empty($shop))
            $this->id_shop = $shop['id_shop'];
        
        if ($id)
        {
            $this->id = (int)$id;
           
            if(isset($this->def) && is_array($this->def) && count($this->def))
            {
                if (!(isset($this->def['table']) && strlen($this->def['table'])))
                    return (false);
                
                $this->_db->from($this->def['table'], 'a');
                
                if (!(isset($this->def['primary'][0]) && strlen($this->def['primary'][0])))
                    return (false);
                
                $this->_db->where( array( "a." . $this->def['primary'][0] =>  $this->id, "a.id_shop"  => (int)$this->id_shop) );

                // Get lang informations
                if ($id_lang)
                {
                    $this->id_lang = (int)$id_lang;
                    if(!(isset($this->def['lang']) && is_array($this->def['lang'])))
                        return (false);
                        
                    $this->_db->leftJoin($this->def['table'].'_lang','b','a.'.$this->def['primary'][0].' = b.'.$this->def['primary'][0].' AND b.id_lang = '.(int)$id_lang);
                }
                
                $object_datas = $this->_db->db_array_query($this->_db->query);
                
                if (is_array($object_datas) && count($object_datas))
                {                    
                    foreach ($object_datas as $key_object => $value_object) 
                    {                        
                        if ($id_lang && isset($this->def['lang']) && $this->def['lang'])
                        {
                            $this->_db->from( $this->def['table'] . '_lang' );
                            $this->_db->where( array( $this->def['primary'][0] => (int)$id, "id_shop"  => (int)$this->id_shop) );
                            
                            $object_datas_lang = $this->_db->db_array_query($this->_db->query);
                            
                            if (is_array($object_datas_lang) && count($object_datas_lang))
                            {
                                $lang = Language::getLanguages((int)$this->id_shop);
                                
                                foreach ($object_datas_lang as $row)
                                {
                                    foreach ($row as $key => $value)
                                    {
                                        foreach ($lang as $l)
                                        {
                                            if($row['id_lang'] == $l['id_lang'])
                                            {
                                                $iso_code = $l['iso_code'];
                                            }
                                        }
                                            
                                        if (array_key_exists($key, $this) && $key != $this->def['primary'][0])
                                        {
                                            if (!isset($object_datas[$key]) || !is_array($object_datas[$key]) || !count($object_datas[$key]))
                                            {
                                                if(isset($iso_code) && !empty($iso_code))
                                                {
                                                    $object_datas[$key_object][$key][$iso_code] = $value;
                                                }
                                                else 
                                                {
                                                    $object_datas[$key_object][$key] = $value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }    
                    }
                }
                 
                if (is_array($object_datas) && count($object_datas))
                {
                    foreach ($object_datas as $values)
                    {
                        foreach ($values as $key=>$value)
                        {
                            $key = str_replace(array("a.","b."), "", $key);
                            if (array_key_exists($key, $this))
                            {
                                $this->{$key} = $value;
                            }
                        }
                    }
                }
                
                //set type
                foreach ($this->def['fields'] as $fields_key => $fields )
                {
                    if($fields['type'] == 'int' || $fields['type'] == 'tinyint' || $fields['type'] == 'INTEGER')
                        settype($this->{$fields_key}, "integer");

                    if($fields['type'] == 'decimal')
                        settype($this->{$fields_key}, "float");
                }
            }
        }
        
    }
    
    public function get_tmpcode($table, $column, $id)
    {
        $tmp = '';
        $this->_db->select(array('tmpcode'));
        $this->_db->from($table);
        $this->_db->where(array($column => $id));
        
        $result = $this->_db->db_array_query($this->_db->query);
        
        if (isset($result) && !empty($result))
            foreach ($result as $value)
                $tmp = $value['tmpcode'];
        
        return $tmp;
    }
 
}
