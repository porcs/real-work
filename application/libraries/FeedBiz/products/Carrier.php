<?php

class Carrier extends ObjectModel
{
    public $id_carrier;
    public $id_tax;
    public $name;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'carrier',
        'primary'   => array('id_carrier'),
        'fields'    => array(
            'id_carrier'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_carrier_ref'    =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'id_tax'            =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'tmpcode'           =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );

    public static function getCarriersByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $carriers = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('pc.id_carrier', 'c.name', 'c.id_tax', 'pc.price'));
        $_db->from('product_carrier pc');
        $_db->leftJoin('carrier', 'c', '(pc.id_carrier = c.id_carrier) AND (pc.id_shop = c.id_shop)');
        $_db->where(array('pc.id_product' => (int)$id_product, 'pc.id_shop' => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $carrier)
        {
            $carriers[$carrier['id_carrier']]['name'] = (string)$carrier['name'];
            $carriers[$carrier['id_carrier']]['price'] = (float)$carrier['price'];

            $tax_rate = Tax::getTaxesRate($carrier['id_tax'], (int)$id_shop);
            $carriers[$carrier['id_carrier']]['tax'][$carrier['id_tax']] = $tax_rate;
        }
        
        return $carriers;
    }
    
    public static function getCarriers($id_shop,$key_id_carrier = false)
    {
        $carriers = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('max(id_carrier) as id_carrier' , 'max(name) as name', 'id_tax', 'id_carrier_ref'));
        $_db->from('carrier');
        $_db->groupBy('id_carrier_ref');
        $_db->orderBy('name');
        $_db->where(array('id_shop' => (int)$id_shop));
	
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $carrier)
        {
            if($key_id_carrier){
                $id_carreir = $carrier['id_carrier'];
            }else{
                if(isset($carrier['id_carrier_ref']) && !empty($carrier['id_carrier_ref']))
                    $id_carreir = $carrier['id_carrier_ref'];
                else 
                    $id_carreir = $carrier['id_carrier'];
            }
            
            $carriers[$id_carreir]['name'] = (string)$carrier['name'];
            $carriers[$id_carreir]['id_carrier'] = (int)$carrier['id_carrier'];
            $carriers[$id_carreir]['id_carrier_ref'] = (int)$carrier['id_carrier_ref'];
            
            if(isset($carrier['id_tax']) && !empty($carrier['id_tax']))
            {
                $tax_rate = Tax::getTaxesRate($carrier['id_tax']);
                $carriers[$id_carreir]['tax'][$carrier['id_tax']] = $tax_rate;
            }
        }
        
        return $carriers;
    }
    
    public static function getCarriersSelected($id_shop, $id_mode = null)
    {
        $carriers = array();
        $mode = '';
        if(isset($id_mode) && !empty($id_mode))
             $mode = ' AND id_mode = ' . $id_mode . ' ';
        
        $_db = ObjectModel::$db;
        $result = $_db->db_query_str("SELECT * FROM ".$_db->prefix_table."carrier_selected WHERE id_shop = " . $id_shop . " $mode ; ");
        
        foreach ($result as $keys => $carrier)
        {
            foreach ($carrier as $key => $c)
                if(!is_int($key))
                    $carriers[$carrier['id_carrier']][$key] = $c;
        }       
      
        return $carriers;
    }
    
    public static function getSelectedCarriers($id_shop, $id_mode = null)
    {
        $carriers = array();
        
        $all_carriers = self::getCarriers($id_shop);
        $selected_carriers = self::getCarriersSelected($id_shop, $id_mode);
       
        $arr_selected_carrier = array();
        
        foreach ($all_carriers as $key => $carrier)
        {
            $carriers[$key]['id_carrier'] = $carrier['id_carrier'];
            $carriers[$key]['id_carrier_ref'] = $carrier['id_carrier_ref'];
            $carriers[$key]['name'] = $carrier['name'];
            $carriers[$key]['selected'] = isset($selected_carriers[$carrier['id_carrier']]) ? true : false;// $carrier['selected'];

            if($carriers[$key]['selected'] == 'true')
              $arr_selected_carrier[$key] = true;   
        }

        if(empty($arr_selected_carrier))
            $carriers['no_selected'] = true;
        
        return $carriers;
    }
    
    public static function saveSelectedCarriers($datas, $id_shop, $id_mode)
    {
        
        $db = ObjectModel::$db;
        
        // get carrier 
        $carriers = self::getCarriers($id_shop,true);
       
        $sql_trunc = $db->truncate_table('carrier_selected WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode);
         
        if(!$sql_trunc)  {
            $sql = "CREATE TABLE ".$db->prefix_table."carrier_selected (
            id_carrier int(10) NOT NULL, 
            id_shop int(10) NOT NULL, 
            id_mode int(10) NOT NULL, 
            session_key date NOT NULL, 
            PRIMARY KEY (id_carrier, id_shop, id_mode)); ";
            $db->db_exec($sql,true);
        }
        $sql = '';
        foreach ($datas as $data) {
                
                if(isset($carriers[$data["id_carrier"]]) && !empty($carriers[$data["id_carrier"]]) ) {
                    $data["id_carrier"] =  $carriers[$data["id_carrier"]]['id_carrier']; //   var_dump($carriers[$data["id_carrier"]]['id_carrier']); exit;
                }
                
                if(isset($data["id_carrier"]) && !empty($data["id_carrier"]) && $data["id_carrier"] != NULL) {
                    $sql .= $db->replace_string('carrier_selected', $data);
                }
            }      
        if($sql == '')
            return true;
        
        $db->db_trans_begin();
        $exec = $db->db_exec($sql);
        $db->db_trans_commit();
            
        if(!$exec)
            return false;
        
        return true;
    }
    
}