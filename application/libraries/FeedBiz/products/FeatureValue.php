<?php

class FeatureValue extends ObjectModel
{
    public $id_feature;
    public $id_feature_value;
    public $id_lang;
    public $value;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'feature_value',
        'primary'   => array('id_feature'),
        'fields'    => array(
            'id_feature'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_feature_value'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_lang'   =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'value'  =>  array('type' => 'varchar', 'required' => true, 'size' => 255),
        ),
    );
    
    public static function getFeatureValuesById($id_feature, $id_shop, $id_lang = null)
    {
        if(!isset($id_feature) || empty($id_feature))
            return NULL;
        
        $features = array();
        
        $_db = ObjectModel::$db;
        $_db->from('feature_value');
        $arr_where['id_feature'] = (int)$id_feature;
        $arr_where['id_shop'] = (int)$id_shop;
        
        if($id_lang)
            $arr_where['id_lang'] = (int)$id_lang;
        
        $_db->where($arr_where);
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        
        foreach ($result as $feature)
        {
            foreach ($lang as $l)
                if($feature['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $features[$feature['id_feature_value']][$iso_code] = (string)$feature['value'];

        }

        return  $features;
    }
    
}