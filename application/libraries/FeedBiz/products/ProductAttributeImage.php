<?php

class ProductAttributeImage extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute_image',
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_image'              =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            
        ),
    );
    
}