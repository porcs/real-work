<?php

require_once(dirname(__FILE__) . '/ObjectModel.php');

class OTP extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'token',
        'fields'    => array(
            'token'         =>  array('type' => 'varchar', 'required' => true, 'size' => 255),
            'feed_source'   =>  array('type' => 'varchar', 'required' => false, 'size' => 255),
            'status'        =>  array('type' => 'int', 'required' => true, 'size' => 1 ),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
    );

    public function get_token($feed_source = null)
    {
        $source = '';
        if(isset($feed_source) && !empty($feed_source))
            $source = " AND feed_source = '" . $feed_source . "' ";
        
        $_db = ObjectModel::$db;
        $sql = $_db->db_query_str("SELECT ".$_db->prefix_table."token FROM token WHERE status = 1" . $source );
        
        if($sql)
            foreach ($sql as $result)
                return $result['token'];
        
        return;
    }
    
    //get_offer_token
    public function get_offer_token($user, $feed_source = null)
    {
        $source = '';
        if(isset($feed_source) && !empty($feed_source))
            $source = " AND feed_source = '" . $feed_source . "' ";
        
        $_db = new Db($user, 'offers');
        
        $sql = $_db->db_query_str("SELECT ".$_db->prefix_table."token FROM token WHERE status = 1" . $source );
        
        if($sql)
            foreach ($sql as $result)
                return $result['token'];
        
        return;
    }
}

