<?php

class Log extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'log',
        'primary'   => array('id_log', 'id_shop'),
        'fields'    => array(
            'id_log'            =>  array('type' => 'INTEGER', 'required' => false, 'primary_key' => true),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'severity'          =>  array('type' => 'tinyint', 'required' => false, 'size' => 1),
            'error_code'        =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'message'           =>  array('type' => 'text'),
            'object_id'         =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public function get_market_log($id_shop,$market='amazon'){
	
        $_db = ObjectModel::$db;
        
        $sql = '';
        $out = array();
        switch ($market){
            case 'amazon':
                    if(!$_db->db_table_exists('amazon_submission_result_log')) return $out;
                    $sql = 'select id_country,batch_id,date_add from '.$_db->prefix_table.'amazon_submission_result_log where id_shop = '.$id_shop.' order by date_add desc limit 1 ';
                break;
            case 'ebay':
                    if(!$_db->db_table_exists('ebay_statistics_log')) return $out;
                    $sql = 'select id_site,type,id_country,batch_id,date_add from '.$_db->prefix_table.'ebay_statistics_log where id_shop = '.$id_shop.' order by date_add desc limit 1 ';
                break;
        }
            
        $out = $_db->db_query_str($sql);
        return empty($out)?array():$out;
            
    } 

}