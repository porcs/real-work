<?php

require_once(dirname(__FILE__).'/../../UserInfo/configuration.php');

class MarketplaceProductOption extends ObjectModel
{   
    private static $table_definition = '_product_option';    
    
    public $table;
    public $marketplace; 
    public $use_site = false;   
    
    public static $tables = array(
	    'Main'  => 'marketplace',
    );
    
    public static $fields = array(
	
	    'keys'  => array(
		'id_product'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ),
                'id_product_attribute'	    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'sku'			    =>  array('type' => 'varchar', 'size' => 64, 'require' => '' ),
                'id_shop'		    =>  array('type' => 'int', 'size' => 11, 'require' => 'NOT NULL' ), 
		'id_country'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
		'id_lang'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
	    ),
	
	    'main'  => array(
                'c_force'		    =>  array('type' => 'int', 'size' => 11, 'require' => '' ),
                'no_price_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'no_quantity_export'	    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'latency'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'c_disable'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
                'price'			    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ), 
		'shipping'		    =>  array('type' => 'decimal', 'size' => '20,6', 'require' => '' ),
                'shipping_type'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),
		'text'			    =>  array('type' => 'varchar', 'size' => 256, 'require' => '' ),		
		'flag_update'		    =>  array('type' => 'tinyint', 'size' => 4, 'require' => '' ),		
	    ),	
	    
	    'amazon'  => array(
		'fba'                       =>  array('type' => 'tinyint', 'size' => 4, 'input_type' => 'checkbox'),
		'fba_value'                 =>  array('type' => 'decimal', 'size' => '20,6', 'input_type' => 'text'),
		'asin1'                     =>  array('type' => 'varchar', 'size' => 16, 'input_type' => 'text' ),
		'asin2'                     =>  array('type' => 'varchar', 'size' => 16, 'input_type' => 'text' ),
		'asin3'                     =>  array('type' => 'varchar', 'size' => 16, 'input_type' => 'text' ),
		'bullet_point1'             =>  array('type' => 'varchar', 'size' => 500, 'input_type' => 'text' ),
		'bullet_point2'             =>  array('type' => 'varchar', 'size' => 500, 'input_type' => 'text' ),
		'bullet_point3'             =>  array('type' => 'varchar', 'size' => 500, 'input_type' => 'text' ),
		'bullet_point4'             =>  array('type' => 'varchar', 'size' => 500, 'input_type' => 'text' ),
		'bullet_point5'             =>  array('type' => 'varchar', 'size' => 500, 'input_type' => 'text' ),
		'browsenode'		    =>  array('type' => 'varchar', 'size' => 256, 'input_type' => 'text' ),
		'gift_wrap'                 =>  array('type' => 'tinyint', 'size' => 4, 'input_type' => 'checkbox' ),
		'gift_message'              =>  array('type' => 'tinyint', 'size' => 4, 'input_type' => 'checkbox' ),
		'shipping_group'            =>  array('type' => 'varchar', 'size' => 256, 'input_type' => 'select' ),
		//'advertising'               =>  array('type' => 'tinyint', 'size' => 2, 'input_type' => 'checkbox', 'set_zero' => true ),
		//'ad_groups'                 =>  array('type' => 'text', 'input_type' => 'text' ),
	    ),
	
    );
    
    public function __construct($user, $id = null, $id_lang = null, $database = null, $shop = null, $site = null, $Marketplace = null) {
        parent::__construct($user, $id, $id_lang, $database, $shop);
	if(!isset($Marketplace) || empty($Marketplace)){
	    $this->marketplace = 'marketplace';   
	} else {
	    $this->marketplace = strtolower(ObjectModel::get_marketplace_by_id($Marketplace));  
	}	
	if(isset($site) && !empty($site)){
	    $this->id_country = $site;   
	    $this->use_site = true;
	}	
        $this->table = $this->marketplace . self::$table_definition;
    }
    
    public function checke_table_exit() {
        $_db = ObjectModel::$db;
	if(!$_db->db_table_exists($this->table, true)){
	    $this->create_product_option($this->marketplace);  
	} else {
            $this->check_field_exists();
        }
    }

    private function check_field_exists() {
        $_db = ObjectModel::$db;
        $marketplace = $this->marketplace;
        if($this->marketplace == 'marketplace') {
            $marketplace = 'main';
        }
        $sql = array();
        if(isset(self::$fields[$marketplace])) {
            foreach (self::$fields[$marketplace] as $fields => $values){
                if (!Db::fieldExists($this->table, $fields)) {
                    $type = $size = $after = '';
                    if(isset($values['type']) && !empty($values['type'])){
                        $type = $values['type'];
                    }
                    if(isset($values['size']) && !empty($values['size'])){
                        $size = '(' . $values['size'] . ')';
                    }
                    if(isset($last_field) && !empty($last_field)){
                        $after = 'AFTER ' . $last_field;
                    }
                    $sql[] = " ADD COLUMN $fields $type$size $after";
                }
                $last_field = $fields;
            }
        }
        if(!empty($sql)){
            $addColumns = implode (", ", $sql);
        }
        if(isset($addColumns)) {
           $_db->db_exec('ALTER IGNORE TABLE ' . $this->table . ' ' . $addColumns);
        }
    }
    
    private function create_product_option($marketplace) {
	
	$site_primary_key = $fields = '';
        $_db = ObjectModel::$db;
	$table = $this->table;
	
	if($this->marketplace != 'marketplace') {
	    $site_primary_key .= "id_country";
	} else {
	    $site_primary_key .= "id_lang";
	}
	 
	foreach (self::$fields['keys'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	 
	foreach (self::$fields['main'] as $main_fields => $main_values){
	    $fields .= "$main_fields  ".$main_values['type']." (".$main_values['size'].") ".$main_values['require'].", ";
	}
	
	if($this->marketplace != 'main'){
	    if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
		    $fields .= "$marketplace_fields  ".$marketplace_values['type']." (".$marketplace_values['size']."), ";
		}
	    }
	}
        $query = "CREATE TABLE ".$table." ( ".
		$fields
		."date_add datetime,
                date_upd datetime,
                PRIMARY KEY (id_product, id_product_attribute, id_shop, $site_primary_key)
              ) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci ;";
       
        if($_db->db_exec($query,false)){
            return true;
        }
        return false;	
    }    
    
    public function getProducts($id_shop, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null)
    {        
        $product_list = array();              
        $_db = ObjectModel::$db;
       
	$select = 'mpo.*, p.id_product as id_product, pa.id_product_attribute as id_product_attribute, p.reference as parent_sku,  
		   CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference ';
	
	if( isset($num_row) && !empty($num_row) ) {
	    $select = 'count(p.id_product) as rows';
	}
	
        $sql = "SELECT $select FROM {$_db->prefix_table}product p
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".$this->table." mpo ON p.id_product = mpo.id_product 
		    AND CASE WHEN pa.id_product_attribute IS NOT NULL THEN mpo.id_product_attribute = pa.id_product_attribute ELSE mpo.id_product_attribute = 0  END 
		    AND p.id_shop = mpo.id_shop ";
        
		if($this->use_site) {
		     $sql .= "AND mpo.id_country = " . $this->id_country . " ";
		} else {
		    if(!isset($id_lang) || empty($id_lang)){
			$id_lang = $this->get_id_language($id_shop);		
		    }
		    $sql .= "AND mpo.id_lang = " . (int)$id_lang . " ";
		}     
        
	$sql .= "WHERE p.id_shop = $id_shop AND (p.date_upd IS NULL OR p.date_upd = '') " ;
        if(isset($search['all']) && !empty($search['all'])){
            $sql .= "AND ( "
                    . "p.id_product LIKE '%" . $search['all'] . "%' OR "
                    . "pa.id_product_attribute LIKE '%" . $search['all'] . "%' OR "
                    . "p.reference LIKE '%" . $search['all'] . "%' OR "
                    . "pa.reference LIKE '%" . $search['all'] . "%' "
                    . ") ";
        }        
        
        if(isset($search['id_category']) && !empty($search['id_category'])){
            $sql .= "AND ( "
                    . "p.id_category_default = " . $search['id_category'] . ""
                    . ") ";
        }
        
	if(isset($search['date_created']) && !empty($search['date_created'])){
            $sql .= "AND ( "
                    . "p.date_created between '" . date('Y-m-d', strtotime($search['date_created']['date_from'])) 
		    . "' and '" . date('Y-m-d', strtotime($search['date_created']['date_to'])) . "' "
                    . ") ";
        }
	
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY $order_by ";
        } else {
            $sql .= "ORDER BY p.id_product asc, pa.reference asc ";
        }
        
        if(isset($limit)){
            $sql .= "LIMIT $limit ";
        }
	
        if(isset($start)){
            $sql .= "OFFSET $start ";
        }
        
        $sql .= " ; " ;
	
        if( isset($num_row) && !empty($num_row) ) {
            
	    $result = $_db->db_query_str($sql);   
            return isset($result[0]['rows']) ? $result[0]['rows'] : 0;
            
        } else {
	    
            $result = $_db->db_query_str($sql);    
	    
	    $language = Language::getLanguageDefault($id_shop);
            $def_lang_id = 0;
	    foreach ($language as $lang){
		$iso_code = $lang['iso_code'];
		$default_iso_code = $lang['iso_code'];
                $def_lang_id = $lang['id_lang'];
	    }
		
	    if($this->use_site){
		$countries = ObjectModel::get_countries();
		foreach ($countries as $country)
		    if($country['id'] == $this->id_country)
			$iso_code = $country['iso_code'];
	    }
            $p_id=array();
            foreach ($result as $key => $data){
                $p_id[$data['id_product']] = $data['id_product'];
            }
	    $p_lang = $this->exportProductDefLanguageList($p_id,$id_shop);
            foreach ($result as $key => $data){
                if(!empty($data['reference'])){
                    $product_list[$key]['key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $data['reference']);
                    $product_list[$key]['parent_sku'] = !empty($data['parent_sku']) ? $data['parent_sku'] : $data['reference'];
                    $product_list[$key]['parent_sku_key'] = preg_replace("/[^a-zA-Z0-9]+/", "_", $product_list[$key]['parent_sku']);
                    $product_list[$key]['id_product'] = $data['id_product'];
                    $product_list[$key]['id_product_attribute'] = $data['id_product_attribute'];
                    $product_list[$key]['reference'] = $data['reference'];
                    $product_list[$key]['force'] = $data['c_force'];
                    $product_list[$key]['no_price_export'] = $data['no_price_export'];
                    $product_list[$key]['no_quantity_export'] = $data['no_quantity_export'];
                    $product_list[$key]['latency'] = $data['latency'];
                    $product_list[$key]['disable'] = $data['c_disable'];
                    $product_list[$key]['price'] = isset($data['price']) ? number_format($data['price'], 2) : null;
                    $product_list[$key]['shipping'] = isset($data['shipping']) ? number_format($data['shipping'], 2) : null;
                    $product_list[$key]['shipping_type'] = $data['shipping_type'];
                    $product_list[$key]['text'] = $data['text'];
                    $product_list[$key]['date_add'] = $data['date_add'];
                    $product_list[$key]['date_upd'] = $data['date_upd'];
                    $product_list[$key]['p_name'] = isset($p_lang[$data['id_product']])?$p_lang[$data['id_product']]['name']:'';
		    
		    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
			foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			    if(isset($data[$marketplace_fields])) {
                                if(is_serialized($data[$marketplace_fields])){
                                    $product_list[$key][$marketplace_fields] = unserialize($data[$marketplace_fields]);
                                } else {
                                    $product_list[$key][$marketplace_fields] = isset($marketplace_values['type']) && $marketplace_values['type'] == 'decimal' ?
                                    sprintf('%.02f', round($data[$marketplace_fields], 2)) : $data[$marketplace_fields];
                                }
			    }
			}
		    }		    
                } else {
		    
		     if(!empty($data['id_product'])){
			
			$name = '';
			if( isset($data['id_product_attribute']) ) {
			    
			    $attributes = ProductAttribute::getProductAttributeById($data['id_product_attribute'], $id_shop);

			    foreach ($attributes as $attribute)
			    {
				if(strlen($name) > 2)
				     $name .= ', ';
				
				if(isset($attribute['name'][$iso_code])){
				    $name .= $attribute['name'][$iso_code] ;
				} else if(isset($attribute['name'][$default_iso_code])){
				    $name .= $attribute['name'][$default_iso_code] ;
				}
				
				if(isset($attribute['value'])){
				    foreach ($attribute['value'] as $values) {
					if(isset($values['name'][$iso_code])) {
					    $name .=  ': ' . $values['name'][$iso_code] ;
					} else if(isset($values['name'][$default_iso_code])){
					    $name .=  ': ' . $values['name'][$default_iso_code] ;
					}
				    }
				}
			    }
			}
			
			$product_list[$key]['key'] = $data['id_product'] . '_' . $data['id_product_attribute'];
			$product_list[$key]['parent_sku'] = 'Product ID ' .$data['id_product'];
			$product_list[$key]['parent_sku_key'] = $data['id_product'];
			$product_list[$key]['id_product'] = $data['id_product'];
			$product_list[$key]['id_product_attribute'] = $data['id_product_attribute'];
			$product_list[$key]['reference'] = isset($data['id_product_attribute']) ? 'Attribute ID '.$data['id_product_attribute']:$product_list[$key]['parent_sku'] ;
			$product_list[$key]['name'] = (strlen($name) > 2) ? $name : $product_list[$key]['reference'] ;
			$product_list[$key]['force'] = $data['c_force'];
			$product_list[$key]['no_price_export'] = $data['no_price_export'];
			$product_list[$key]['no_quantity_export'] = $data['no_quantity_export'];
			$product_list[$key]['latency'] = $data['latency'];
			$product_list[$key]['disable'] = $data['c_disable'];
			$product_list[$key]['price'] = isset($data['price']) ? number_format($data['price'], 2) : null;
			$product_list[$key]['shipping'] = isset($data['shipping']) ? number_format($data['shipping'], 2) : null;
			$product_list[$key]['shipping_type'] = $data['shipping_type'];
			$product_list[$key]['text'] = $data['text'];
			$product_list[$key]['date_add'] = $data['date_add'];
			$product_list[$key]['date_upd'] = $data['date_upd'];
			$product_list[$key]['remark_missing_reference'] = true;
			
			if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
			    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
				if(isset($data[$marketplace_fields])) {
                                    if(is_serialized($data[$marketplace_fields])){
                                        $product_list[$key][$marketplace_fields] = unserialize($data[$marketplace_fields]);
                                    } else {
                                        $product_list[$key][$marketplace_fields] = isset($marketplace_values['type']) && $marketplace_values['type'] == 'decimal' ?
                                        sprintf('%.02f', round($data[$marketplace_fields], 2)) : $data[$marketplace_fields];
                                    }
				}
			    }
			}
		    }
		    
		}
            }


            
            return $product_list;
        }
    }

    public function getAllRootCategories($id_shop,$is_selected=false)
    {
        $root = '';
        $_db = ObjectModel::$db;
        $_db->select(array('id_category','id_parent','is_root_category'));
        $_db->from('category');
        $arr_where['id_shop'] = (int)$id_shop;
        $_db->where( $arr_where );
        $result = $_db->db_array_query($_db->query);
        $all_parent = array();
        $all_category = array();
        foreach ($result as $category){
            $all_parent[$category['id_parent']][$category['id_category']] = $category['id_category'];
            $all_category[$category['id_category']] = $category;
        }
        if($is_selected){
            $this->getSelectedCategoriesOffers($id_shop);
        }
        foreach($all_parent as $par_id => $id){
            if(!isset($all_category[$par_id])){
                foreach($all_parent[$par_id] as $c_id => $id){
                    if($is_selected&& !isset($this->arr_cat_selected[$c_id]) && isset($all_category[$c_id]['is_root_category'])&& $all_category[$c_id]['is_root_category']==0){continue;}
                    $root[] = $c_id;
                }
            }
        }
        return $root;
    }
    
    public function getCategories($id_shop, $parent = null, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null)
    {        
        $product_list =  array();              
        $_db = ObjectModel::$db; 
	
	if(!isset($id_lang) || empty($id_lang)){
	    $id_lang = $this->get_id_language($id_shop);		
	}     
     	
        if(!isset($parent) || empty($parent)){
            $categories = new Category(ObjectModel::$user);
//            $parent = $categories->getRootCategories($id_shop);
            $parent = $this->getAllRootCategories($id_shop);
        } 
        if(!isset($this->arr_cat_lang)){
            $this->arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name from {$_db->prefix_table}category_lang cl where '{$id_shop}'= cl.id_shop AND cl.id_lang = '$id_lang'";
            
            $result = $_db->db_query_str($sql_cat_lang);
            foreach($result as $v){
                $this->arr_cat_lang[$v['id_category']] = $v['name'];
            }            
        } 
        
        if(!isset($this->arr_mpo)){
            $this->arr_mpo = array();
            $this->arr_mpo_country = array();
            $sql_mpo = "SELECT mpo.id_country,mpo.c_force,mpo.no_price_export,mpo.no_quantity_export,mpo.latency,mpo.price,mpo.shipping,mpo.shipping_type,mpo.c_disable,mpo.text, "
                    . " p.id_category_default from ".$this->table." mpo , {$_db->prefix_table}product p "
			. "where p.id_product = mpo.id_product AND '{$id_shop}'= mpo.id_shop AND '{$id_shop}'= p.id_shop";
            $result = $_db->db_query_str($sql_mpo); 
            foreach($result as $v){
                $this->arr_mpo[$v['id_category_default']] = $v;
		
		if($this->use_site){
                    if(isset($v['id_country']) && isset($v['id_category_default'])) 
		    $this->arr_mpo_country[$v['id_country']][$v['id_category_default']] = $v;
                }
            } 
        }
        
        $sql = "SELECT  c.id_category AS id_category  , c.id_parent AS id_parent  
                FROM {$_db->prefix_table}category c 
                ";
        if(isset($search) && !empty($search)){
            $sql .=" LEFT JOIN {$_db->prefix_table}category_lang cl ON c.id_category = cl.id_category AND c.id_shop = cl.id_shop AND cl.id_lang = $id_lang ";
        }
                   
        $sql .= "WHERE c.id_shop = $id_shop  ";
        if( isset($num_row) && !empty($num_row) ) {
//            $sql .= " AND c.id_parent in ('".implode("','",$parent)."')  ";
        }
        
        if(isset($search) && !empty($search)){
            $sql .= "AND ( "
                    . "cl.name LIKE '%" . $search . "%' OR "
                    . "c.id_category LIKE '%" . $search . "%' "
                    . ") ";
        }        
        
        $sql .= " GROUP BY c.id_category ";
        if(isset($order_by) && !empty($order_by)){
            $sql .= "ORDER BY c.id_parent ASC, $order_by ";
        } else {
            $sql .= "ORDER BY c.id_category ASC ";
        }
        
        if(isset($start) && isset($limit)){
            $sql .= "LIMIT $limit OFFSET $start ";
        }
        
        $sql .= " ; " ;
             
        if( isset($num_row) && !empty($num_row) ) {
            
            $result = $_db->db_sqlit_query($sql);
            $row = $_db->db_num_rows($result);
            return $row;
            
        } else {
            $result=array();
            if(!isset($this->arr_cat_by_parent)){
                $this->arr_cat_by_parent = array();
                $sql_cat = $sql; 
                $result = $_db->db_query_str($sql_cat);
                foreach($result as $v){
                    $this->arr_cat_by_parent[$v['id_parent']][] = $v;
                } 
            }else{
                if(empty($parent)){
                    $result = $_db->db_query_str($sql);
                }
            } 
                
            if(!empty($parent)){ 
                if(is_array($parent))
                foreach($parent as $p_id =>$v){
                    $result[$p_id] = isset($this->arr_cat_by_parent[$p_id])?$this->arr_cat_by_parent[$p_id]:array();
                }else{
                    $result[$parent] = isset($this->arr_cat_by_parent[$parent])?$this->arr_cat_by_parent[$parent]:array();
                }
            }
            foreach($result as $parent_id => $res){
                foreach ($res as $key => $data){

                    if(!empty($data['id_category'])){

                        $cat_name = isset($this->arr_cat_lang[$data['id_category']])?$this->arr_cat_lang[$data['id_category']]:'';
                        $product_list[$key]['id_parent']            = $data['id_parent'];
                        $product_list[$key]['category']             = $cat_name;//$data['category'];
                        $product_list[$key]['id_category']          = $data['id_category'];

                        if($this->use_site){
                            $data = isset($this->arr_mpo_country[$this->id_country][$data['id_category']])?$this->arr_mpo_country[$this->id_country][$data['id_category']]:null;
                        }else{
                            $data = isset($this->arr_mpo[$data['id_category']])?$this->arr_mpo[$data['id_category']]:null;
                        }

                        $product_list[$key]['force']                = !empty($data['c_force']) ? $data['c_force'] : '';
                        $product_list[$key]['no_price_export']      = !empty($data['no_price_export']) ? $data['no_price_export'] : '';
                        $product_list[$key]['no_quantity_export']   = !empty($data['no_quantity_export']) ? $data['no_quantity_export'] : '';
                        $product_list[$key]['latency']              = !empty($data['latency']) ? $data['latency'] : '';
                        $product_list[$key]['price']                = !empty($data['price']) ? $data['price'] : '';
                        $product_list[$key]['shipping']             = !empty($data['shipping']) ? $data['shipping'] : '';
                        $product_list[$key]['shipping_type']        = !empty($data['shipping_type']) ? $data['shipping_type'] : '';
                        $product_list[$key]['disable']              = !empty($data['c_disable']) ? $data['c_disable'] : '';
                        $product_list[$key]['text']                 = !empty($data['text']) ? $data['text'] : '';

                        if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
                            foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
                                if(isset($data[$marketplace_fields])) {
                                    if(is_serialized($data[$marketplace_fields])){
                                        $product_list[$key][$marketplace_fields] = unserialize($data[$marketplace_fields]);
                                    } else {
                                        $product_list[$key][$marketplace_fields] = $data[$marketplace_fields];
                                    }
                                }
                                /*if(isset($marketplace_values['set_zero']) && $marketplace_values['set_zero']){
                                        if(!isset($data[$marketplace_fields]) || empty($data[$marketplace_fields])) {
                                            $product_list[$key][$marketplace_fields] = 0;
                                        }
                                }*/
                            }
                         }

                        $child = $this->getCategories($id_shop, $product_list[$key]['id_category'], $start, $limit, $order_by, $search, $num_row);
                        if(!empty($child['category'])){
                            $product_list[$key]['children'] = $child['category'];
                        }
                    }
                }
            }

            return array('category' => $product_list);
        }
    }    
    
    public function save_marketplace_category_option($data, $id_shop, $id_lang = null)
    {             
        $_db = ObjectModel::$db;
        $parent_value = $child_value = array();
        $sql_child = '';        
        $table_name = $this->table;
        
        foreach ($data as $value){
            var_dump($value);
            if(isset($value['id_category']) && !empty($value['id_category'])) { 
                   
                if($this->use_site){
                    $parent_value['id_country'] = $this->id_country;
                } else {
		    if(!isset($id_lang) || empty($id_lang)){
			$id_lang = $this->get_id_language($id_shop);		
		    } 
		    $parent_value['id_lang'] = $id_lang ? $id_lang : null;
		}
		
                if(!isset($value['force_chk']) || !$value['force_chk']){
		    $parent_value['c_force'] = null;
		} else {
		    $parent_value['c_force'] = (isset($value['force']) || ($value['force'] === 0)) ? $value['force'] : '';
		}
                $parent_value['no_price_export'] = (isset($value['no_price_export']) && !empty($value['no_price_export'])) ? $value['no_price_export'] : 0;
                $parent_value['no_quantity_export'] = (isset($value['no_quantity_export']) && !empty($value['no_quantity_export'])) ? $value['no_quantity_export'] : 0; 
                
                if(isset($value['latency']) && !empty($value['latency']))
                    $parent_value['latency'] = $value['latency']; 
                
                $parent_value['c_disable'] = (isset($value['disable']) || !empty($value['disable'])) ? $value['disable'] : 0;
                
                if(isset($value['price']) || ($value['price'] === 0))
                    $parent_value['price'] = $value['price'];
                
                if(isset($value['shipping']) || ($value['shipping'] === 0))
                    $parent_value['shipping'] = $value['shipping'];
                
                if(isset($value['shipping_type']) && !empty($value['shipping_type'])){
                    $parent_value['shipping_type'] = (isset($value['shipping_type']) && !empty($value['shipping_type'])) ? $value['shipping_type'] : 0;
                }
                
                if(isset($value['text']) && !empty($value['text'])){
                    $parent_value['text'] = $value['text'];
                }

		if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			if(isset($value[$marketplace_fields])) {
			    if(!empty($value[$marketplace_fields])){
                                if(is_array($value[$marketplace_fields])){
                                    $parent_value[$marketplace_fields] = serialize($value[$marketplace_fields]);
                                } else {
                                    $parent_value[$marketplace_fields] = $value[$marketplace_fields];
                                }
                            } else {
				$parent_value[$marketplace_fields] = null;
                            }
			}
                        if(isset($marketplace_values['set_zero']) && $marketplace_values['set_zero']){
                                if(!isset($value[$marketplace_fields]) || empty($value[$marketplace_fields])) {
                                    $parent_value[$marketplace_fields] = 0;
                                }
                        }
		    }
		}		
                $parent_value['id_shop'] = $id_shop;     
                $parent_value['date_upd'] = date('Y-m-d H:i:s');                                            
            
                //Get Product in Category
                $products = $this->get_product_by_id_category($value['id_category'], $id_shop);
                
                if(isset($products) && !empty($products)){

                    foreach ($products as $product){
                        
                        if(!isset($product['sku']) || empty($product['sku'])){
                            continue;
                        }
                        
                        $child_value = array_merge($product, $parent_value); 
			
			// update flag
			$child_value['flag_update'] = 1;
			
                        $child_value['id_shop'] = $id_shop;   
                        $child_value['date_upd'] = date('Y-m-d H:i:s');
                
                        $sql_chk = "SELECT * FROM $table_name "
                            . "WHERE id_product = ".(int)$child_value['id_product']." "                            
                            . "AND id_shop = ".(int)$child_value['id_shop']." " ;
                        if(isset($child_value['sku'])){
                            $sql_chk .= "AND sku = '".$child_value['sku']."' ";
                        }
                        if($this->use_site){
                            $sql_chk .= "AND id_country = ".(int)$this->id_country." ";
                        }else{
			    $sql_chk .= "AND id_lang = ".(int)$child_value['id_lang']." ";
			}
                        if(isset($child_value['id_product_attribute']) && !empty($child_value['id_product_attribute'])) {
                            $sql_chk .=  "AND id_product_attribute = ".(int)$child_value['id_product_attribute']." ";
                        }
                        
                        $result = $_db->db_sqlit_query($sql_chk);

                        if($_db->db_num_rows($result) <= 0){			   
                            $sql_child .= $this->insert_string($table_name, $child_value);
                        } else {
			    
                             $where = array(
                                'id_product' => array('operation' => '=', 'value' => (int)$child_value['id_product']),                            
                                'sku' => array('operation' => '=', 'value' => $child_value['sku']),
                                'id_shop' => array('operation' => '=', 'value' => (int)$child_value['id_shop']),
                            );
			     
                            if($this->use_site){
                                $where['id_country'] = array('operation' => '=', 'value' => (int)$this->id_country);
                            }else{
                                $where['id_lang'] = array('operation' => '=', 'value' => (int)$child_value['id_lang']);
			    }
                            if(isset($child_value['id_product_attribute']) && !empty($child_value['id_product_attribute'])){
                                $where['id_product_attribute'] = array('operation' => '=', 'value' => (int)$child_value['id_product_attribute']);
                            }                 

                            $sql_child .= $this->update_string($table_name, $child_value, $where);
                        }
                    }
                   
                    if(!$_db->db_exec($sql_child)){
                        return false;
                    } else {
                        $this->update_amazon_status($id_shop);
                    } 
                }
            }
        }
        return true;
    }
    
    public function save_marketplace_product_option ($data, $id_shop, $id_lang = null)
    {       
        $_db = ObjectModel::$db;
        $parent_value = $child_value = array();
        $table_name = $this->table;
        
        $disabled = 'disabled';
        $sql_child = '';
       
        foreach ($data as $key => $value){
            
            $select_id_product_attribute = '';
               
            if(isset($value['id_product']) && !empty($value['id_product']) && isset($value['parent_sku']) && !empty($value['parent_sku'])) { 
                                  
                if($this->use_site){
                    $parent_value['id_country'] = $this->id_country;
                } else {
		    if(!isset($id_lang) || empty($id_lang)){
			$id_lang = $this->get_id_language($id_shop);		
		    }
		    $parent_value['id_lang'] = $id_lang ? $id_lang : null;
		}
		
                $parent_value['id_product'] = $value['id_product'];
                $parent_value['sku'] = $value['parent_sku'];
		  if(!isset($value['force_chk']) || !$value['force_chk']){
		    $parent_value['c_force'] = null;
		} else {
		    $parent_value['c_force'] = (isset($value['force']) || ($value['force'] === 0)) ? $value['force'] : '';
		}
		
                $parent_value['no_price_export'] = (isset($value['no_price_export']) && $value['no_price_export'] != $disabled) ? $value['no_price_export'] : 0;
                $parent_value['no_quantity_export'] = (isset($value['no_quantity_export']) && $value['no_quantity_export'] != $disabled) ? $value['no_quantity_export'] : 0; 
              		
                if(isset($value['latency']) && $value['latency'] != $disabled){
                    $parent_value['latency'] = $value['latency']; 
                }
                $parent_value['c_disable'] = (isset($value['disable']) && $value['disable'] != $disabled) ? $value['disable'] : 0; 
                
                if(isset($value['price']) && $value['price'] != $disabled){
                    $parent_value['price'] = $value['price'];
                }
                
                if(isset($value['text']) && $value['text'] != $disabled){
                    $parent_value['text'] = $value['text'];
                }
              
                if(isset($value['shipping']) && $value['shipping'] != $disabled){
                    $parent_value['shipping'] = $value['shipping'];
                }
                
                if(isset($value['shipping_type']) && $value['shipping_type'] != $disabled){
                    $parent_value['shipping_type'] = (isset($value['shipping_type']) && $value['shipping_type'] != $disabled) ? $value['shipping_type'] : 0;
                }               
		if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			if(isset($value[$marketplace_fields]) && !empty($value[$marketplace_fields])) {
                                if(is_array($value[$marketplace_fields])){
                                    foreach ($value[$marketplace_fields] as $k => $val){
                                        if(empty($val)){
                                            unset($value[$marketplace_fields][$k]);
                                        }
                                    }
                                    if(!empty($value[$marketplace_fields])) {
                                        $parent_value[$marketplace_fields] = serialize($value[$marketplace_fields]);
                                    } else {
                                        $parent_value[$marketplace_fields] = null;
                                    }
                                } else {
                                    $parent_value[$marketplace_fields] = $value[$marketplace_fields];
                                }

			} else {
				$parent_value[$marketplace_fields] = null;
			}
                        if(isset($marketplace_values['set_zero']) && $marketplace_values['set_zero']){
                                if(!isset($value[$marketplace_fields]) || empty($value[$marketplace_fields])) {
                                    $parent_value[$marketplace_fields] = 0;
                                }
                        }
		    }
		}
                $parent_value['id_shop'] = $id_shop;  
                $parent_value['date_upd'] = date('Y-m-d H:i:s'); 
                $filter_value = $parent_value;             
                if(isset($parent_value['id_product_attribute']) && !empty($parent_value['id_product_attribute'])) {
                    $select_id_product_attribute = "AND id_product_attribute = ".(int)$parent_value['id_product_attribute'];
                }
                
            }            
          
            if(isset($value['child']) && !empty($value['child'])){

                // If has child loop child with id_product_attribute / sku
                foreach ($value['child'] as $child_key => $child){
                    
                    if(!empty($parent_value)) {
                        unset($parent_value['asin1']);
                        $child_value = $parent_value;
                    } else {
                        $child_value = $child;
			
			if($this->use_site){
			    $child_value['id_country'] = $this->id_country;
			} else {
			    $child_value['id_lang'] = $id_lang ? $id_lang : null;
			}
                        $child_value['id_shop'] = $id_shop;  
                        $child_value['date_upd'] = date('Y-m-d H:i:s');
                    }   
                    
                    if(isset($child['id_product_attribute']) && !empty($child['id_product_attribute'])) {
                        $child_value['id_product_attribute'] = (int)$child['id_product_attribute'];
                    } else {
			$child_value['id_product_attribute'] = 0;
		    }
                    
                    $child_value['sku'] = $child['sku'];                     
                    unset($child_value['parent_sku']);
                     
                    if(isset($data[$key]['child'][$child_key]['asin1'])){
                        $child_value['asin1'] = $data[$key]['child'][$child_key]['asin1'];
                    }

		    // update flag
		    $child_value['flag_update'] = 1;
		    
                    $filter_value = $child_value; 
                    
                    $sql_chk = "SELECT * FROM $table_name "
                        . "WHERE id_product = ".(int)$child_value['id_product']." "                            
                        . "AND sku = '".$child_value['sku']."' "
                        . "AND id_shop = ".(int)$child_value['id_shop']." " ;
                    
		    if($this->use_site){
                        $sql_chk .=  "AND id_country = ".(int)$this->id_country." ";
                    } else {
                        $sql_chk .=  "AND id_lang = ".(int)$child_value['id_lang']." ";
		    }
		    
                    if(isset($child_value['id_product_attribute'])) {
                        $sql_chk .=  "AND id_product_attribute = ".(int)$child_value['id_product_attribute']." ";
                    }
                  
                    $result = $_db->db_sqlit_query($sql_chk);
                    
                    if($_db->db_num_rows($result) <= 0){
                        
                        $sql_child .= $this->insert_string($table_name, $filter_value);
                        
                    } else {
                        
                         $where = array(
                            'id_product' => array('operation' => '=', 'value' => (int)$child_value['id_product']),                            
                            'sku' => array('operation' => '=', 'value' => $child_value['sku']),
                            'id_shop' => array('operation' => '=', 'value' => (int)$child_value['id_shop']),
                        );
                        if(isset($this->id_country) && !empty($this->id_country)){
                            $where['id_country'] = array('operation' => '=', 'value' => (int)$this->id_country);
                        } else {
                            $where['id_lang'] = array('operation' => '=', 'value' => (int)$child_value['id_lang']);
			}
			
                        if(isset($child_value['id_product_attribute'])){
                            $where['id_product_attribute'] = array('operation' => '=', 'value' => (int)$child_value['id_product_attribute']);
                        }                 
                        
                        $sql_child .= $this->update_string($table_name, $filter_value, $where);
                    }	    
		    
                }
            }
        }
	
        if(!empty($sql_child)){
            $exec = $_db->db_exec($sql_child);
            if($exec){
                $this->update_amazon_status($id_shop);
            } else {
                return false;
            } 
        }
	
	// send update offer options
	$this->update_offer_options_to_shop($id_shop);
	
        return true;
        
    }
    
    public function update_offer_options_to_shop($id_shop){
		
	$updateFlags = '';
	
	$UserConfiguration = new UserConfiguration();
	
	// 1. get user data by user_name
	$user = $UserConfiguration->getUserByUsername(ObjectModel::$user);
		
	if(!isset($user['id_customer']) || empty($user['id_customer']))
	    return false;
	
	// 2. get shop update options url	    
        $shopUrl = $UserConfiguration->getShopInfo((int) $user['id_customer']);    
	
	if(!isset($shopUrl['url_update_options']) || empty($shopUrl['url_update_options'])){
	    return false;
	} 
	
	$update_option_url = $shopUrl['url_update_options'];
	
	if($update_option_url)
	{
	    // 3. get id_marketplace by name
	    $id_marketplace = ObjectModel::get_marketplace_by_name($this->marketplace);
	    
	    $conj = strpos($update_option_url, '?') !== FALSE ? $update_option_url.'&' : $update_option_url.'?';

	    if(!isset($id_marketplace) || empty($id_marketplace)){
		//return false;
                $preparedURL = $conj.sprintf('fbtoken=%s&fbshop=%s',
		    $user['user_code'],
		    $id_shop);
            } else {
                $preparedURL = $conj.sprintf('fbtoken=%s&fbmarketplace=%s&fbshop=%s&fbsite=%s',
		    $user['user_code'],
		    $id_marketplace,
		    $id_shop,
		    ($this->use_site) ? $this->id_country : null );
            }
	    
	    $return = file_get_contents($preparedURL);	   

	    if($return) {

		$return_dom = new SimpleXMLElement($return);
                //file_put_contents(dirname(__FILE__).'/../../../../assets/apps/users/'.ObjectModel::$user.'/amazon/return_dom.xml', $return_dom->saveXML());
		if(isset($return_dom->Status->Code) && $return_dom->Status->Code == 1) 
		{
		    if(isset($return_dom->OffersOptions))
		    {
			foreach ($return_dom->OffersOptions as $OffersOptions)
			{
			    foreach ($OffersOptions as $Offers) 
			    {
				// 4. recieved status from Shop
				$flag = isset($Offers['Flagged']) && $Offers['Flagged'] ? 0 : 1 ; 
				$id_product = (int)$Offers['ProductId'];
				$id_product_attribute = (int)$Offers['ProductAttributeId'];
				$sku = strval($Offers['Reference']);

				// 5. Log flag
				$update_flag = array(
				    'flag_update'		=> $flag,
				);

				$where_flag = array(
				    'id_product'		=> array('value' => $id_product, 'operation' => '='),
				    'id_product_attribute'	=> array('value' => $id_product_attribute, 'operation' => '='),
				    'sku'			=> array('value' => $sku, 'operation' => '='),
				    'id_shop'			=> array('value' => (int)$id_shop, 'operation' => '='), 
				);
				
				if($this->use_site){
				    $where_flag['id_country']	= array('value' => (int)$this->id_country, 'operation' => '=');
				}

				$updateFlags .= $this->update_string($this->table, $update_flag, $where_flag);
			    }
			}
		    }
		}
	    }
	}
	
	if(strlen($updateFlags) > 10)
	{
	    $_db = ObjectModel::$db;
	    $_db->db_exec($updateFlags);           
	}
	
    }

    public function reset_marketplace_product_option($id_product, $id_shop, $id_lang = null){
        
        $_db = ObjectModel::$db;
        $table_name = $this->table;
	
        $sql = "DELETE FROM $table_name WHERE id_product = $id_product AND id_shop =  $id_shop ";    
	
        if($this->use_site){
            $sql .= " AND id_country = ".$this->id_country." "; 
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= " AND id_lang = ".$id_lang." "; 
	}
	
        if($_db->db_exec($sql." ; ")){
            return true;
        }
        
        return false;
    }
    
    public function reset_marketplace_category_option($id_category, $id_shop, $id_lang = null){
        
        $_db = ObjectModel::$db;
        $table_name = $this->table;       
        $sql = '';
        
        $products = $this->get_product_by_id_category($id_category, $id_shop);    
	
        if(isset($products) && !empty($products)){   
            foreach ($products as $product){
                $sql .= "DELETE FROM $table_name WHERE id_product = ". $product['id_product']." AND id_shop =  $id_shop "; 
                if($this->use_site){
                    $sql .= " AND id_country = ".$this->id_country." "; 
                } else {
		    if(!isset($id_lang) || empty($id_lang)){
			$id_lang = $this->get_id_language($id_shop);		
		    }
		    $sql .= " AND id_lang = ".$id_lang." "; 
		}
                $sql .= " ; "; 
            }
        }
        
        $exec = $_db->db_exec($sql);       
        
        if($exec){
            return true;
        }        
        return false;
    }
    
    // return 1 row
    public function get_product_option_by_sku($sku, $id_shop, $id_lang = null)
    {    
        $data = $history = array();
        
        $_db = ObjectModel::$db;
        $table_name = $this->table;
        
        $list = implode("','", (array)$sku);
        $sql = "SELECT * FROM $table_name "
                . "WHERE sku IN ('".$list."') "
                . "AND id_shop = ".(int)$id_shop." " ;
        
        if($this->use_site){
             $sql .= "AND id_country = " . $this->id_country . " ";
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= "AND id_lang = ".(int)$id_lang." "; 
	}
        
        $result = $_db->db_query_str($sql." ; ");          
        
        if(isset($result)){
            
            foreach ($result as $key => $value){
                
                foreach ($value as $k => $val){

                    if(!is_integer($k) && !is_int($k)){
                        
                        $k = str_replace ('c_', '', $k);
                        
                        if(isset($history['data'][$k]) && $history['data'][$k] != $val){
                            if($k != 'asin1'){
                                $data[$k] = 'disable';
                                $history['disable'][$k] = true;
                            }
                        } else {
                            if(!isset($history['disable'][$k])){
                                if(is_serialized($val)){
                                    $data[$k] = unserialize($val);
                                } else {
                                    $data[$k] = $val;
                                }
                            }
                        }
                        $history['data'][$k] = $val;
                    }
                }
            }
        }
       
        return $data;
    }
    
    public function get_product_option_by_list_sku($list_sku, $id_shop, $id_lang = null)
    {    
        $data = $history = array();
        $_db = ObjectModel::$db;
        $table_name = $this->table;
        
        $list = implode("','", (array)$list_sku);
        $sql = "SELECT * FROM $table_name "
                . "WHERE sku IN ('".$list."') "
                . "AND id_shop = ".(int)$id_shop." " ; 
        
        if($this->use_site){
             $sql .= "AND id_country = " . $this->id_country . " ";
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= "AND id_lang = ".(int)$id_lang." "; 
	}
        
        $result = $_db->db_query_str($sql." ; ");          
        
        if(isset($result)){
            
            foreach ($result as $key => $value){
                
                foreach ($value as $k => $val){

                    if(!is_integer($k) && !is_int($k)){
                        
                        $k = str_replace ('c_', '', $k);
                        
                        if(isset($history['data'][$k]) && $history['data'][$k] != $val){
                            if($k != 'asin1'){
                                $data[$k] = 'disable';
                                $history['disable'][$k] = true;
                            }
                        } else {
                            if(!isset($history['disable'][$k])){
                                if(is_serialized($val)){
                                    $data[$k] = unserialize($val);
                                } else {
                                    $data[$k] = $val;
                                }
                            }
                        }
                        $history['data'][$k] = $val;
                    }
                }
            }
        }
       
        return $data;
    }
    
    public function get_product_option_by_id($id_product, $id_shop, $id_product_attribute = null, $id_lang = null)
    {         
        $_db = ObjectModel::$db;
        $table_name = $this->table;
        
        if(!$_db->db_table_exists($table_name, true)) {
            return;
        }
        
        $sql = "SELECT * FROM " . $table_name . " "
                . "WHERE id_product = ".(int)$id_product." "
                . "AND id_shop = ".(int)$id_shop." ";
        
        if($this->use_site){
             $sql .= "AND id_country = " . $this->id_country . " ";
        } else {	    
	    if(isset($id_lang) && !empty($id_lang)){
		$sql .= "AND id_lang = " . $id_lang . " ";
	    }
	}
        
        if(isset($id_product_attribute) && !empty($id_product_attribute)){
            $sql .= "AND id_product_attribute = ".(int)$id_product_attribute." ";
        } 
	
        $sql .= "; ";
        $result = $_db->db_query_str($sql);  
        $data = array();
        if(isset($result[0])){  
            $data['id_product']             = $result[0]['id_product'];
            $data['id_product_attribute']   = $result[0]['id_product_attribute'];
            $data['sku']                    = $result[0]['sku'];
            $data['id_shop']                = $result[0]['id_shop'];
            $data['id_country']             = isset($result[0]['id_country']) ? $result[0]['id_country'] : null;
            $data['id_lang']                = $result[0]['id_lang'];
            $data['force']                  = $result[0]['c_force'];
            $data['no_price_export']        = $result[0]['no_price_export'];
            $data['no_quantity_export']     = $result[0]['no_quantity_export'];           
            $data['latency']                = $result[0]['latency'];
            $data['disable']                = $result[0]['c_disable'];
            $data['price']                  = $result[0]['price'];            
            $data['text']                   = $result[0]['text'];           
            $data['shipping']               = $result[0]['shipping'];
            $data['shipping_type']          = $result[0]['shipping_type'];          
            $data['date_add']               = $result[0]['date_add'];
            $data['date_upd']               = $result[0]['date_upd'];
	    
	    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
		    if(isset($result[0][$marketplace_fields])) {
                        if(is_serialized($result[0][$marketplace_fields])){
                            $data[$marketplace_fields] = unserialize($result[0][$marketplace_fields]);
                        } else {
                            $data[$marketplace_fields] = $result[0][$marketplace_fields];
                        }
		    }
		}
	    }
            return $data;
        }
        
        return;
    }
    
    public function get_product_option_by_ids($ids_array, $id_shop, $id_lang = null)
    {			
	$table_name = $this->table; 
        
    	$return = array();
    	$_db = ObjectModel::$db;
    	
    	if(!$_db->db_table_exists($table_name, true)) return;
    
    	$sql = "SELECT * FROM $table_name "
    	. "WHERE id_product IN ('".implode("', '", $ids_array)."') "
    	. "AND id_shop = ".(int)$id_shop." ";
        
        if($this->use_site){
             $sql .= "AND id_country = " . $this->id_country . " ";
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= "AND id_lang = " . (int)$id_lang . " ";
	}       
	
    	$result = $_db->db_query_str($sql . "; ");
    	
    	if(isset($result) && is_array($result)){
            foreach ($result as $row){
                
                if(!isset($row['id_product_attribute']) || empty($row['id_product_attribute']) )
                    $row['id_product_attribute'] = 0;
  
		$return[$row['id_product']][$row['id_product_attribute']]['id_country']             = isset($row['id_country'])?$row['id_country']:null;                
                $return[$row['id_product']][$row['id_product_attribute']]['id_product']             = $row['id_product'];
                $return[$row['id_product']][$row['id_product_attribute']]['id_product_attribute']   = $row['id_product_attribute'];
                $return[$row['id_product']][$row['id_product_attribute']]['sku']                    = $row['sku'];
                $return[$row['id_product']][$row['id_product_attribute']]['id_shop']                = $row['id_shop'];
                $return[$row['id_product']][$row['id_product_attribute']]['id_lang']                = $row['id_lang'];
                $return[$row['id_product']][$row['id_product_attribute']]['force']                  = $row['c_force'];
                $return[$row['id_product']][$row['id_product_attribute']]['no_price_export']        = $row['no_price_export'];
                $return[$row['id_product']][$row['id_product_attribute']]['no_quantity_export']     = $row['no_quantity_export'];
                $return[$row['id_product']][$row['id_product_attribute']]['latency']                = $row['latency'];
                $return[$row['id_product']][$row['id_product_attribute']]['disable']                = $row['c_disable'];
                $return[$row['id_product']][$row['id_product_attribute']]['price']                  = $row['price'];
                $return[$row['id_product']][$row['id_product_attribute']]['text']                   = $row['text'];
                $return[$row['id_product']][$row['id_product_attribute']]['shipping']               = $row['shipping'];
                $return[$row['id_product']][$row['id_product_attribute']]['shipping_type']          = $row['shipping_type'];
                $return[$row['id_product']][$row['id_product_attribute']]['date_add']               = $row['date_add'];
                $return[$row['id_product']][$row['id_product_attribute']]['date_upd']               = $row['date_upd'];
		
		if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			if(isset($row[$marketplace_fields])) {
                            if(is_serialized($row[$marketplace_fields])){
                                $return[$row['id_product']][$row['id_product_attribute']][$marketplace_fields] = unserialize($row[$marketplace_fields]);
                            } else {
                                $return[$row['id_product']][$row['id_product_attribute']][$marketplace_fields] = $row[$marketplace_fields];
                            }
			}
		    }
		}
            }
    	}
    
    	return $return;
    }
    
    public function get_product_options($id_shop, $id_lang=null, $sku=null)
    {
	$table_name = $this->table; 
    	$return = array();
    	$_db = ObjectModel::$db;
    	
    	if(!$_db->db_table_exists($table_name, true)) return;
    
    	$sql = "SELECT * FROM $table_name " 
    	. "WHERE id_shop = ".(int)$id_shop." " ;
        
        if($this->use_site){
             $sql .= "AND id_country = " . $this->id_country . " ";
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= "AND id_lang = " . (int)$id_lang . " ";
	}
        
	if(isset($sku) && !empty($sku)){
	    if(is_array($sku)){
		$sql .= "AND sku IN ('" . implode("','", $sku) . "') ";
	    } else {
		$sql .= "AND sku = '$sku' ";
	    }
	}
	
    	$sql .= "; ";
	   
    	$result = $_db->db_query_str($sql);
    	
    	if(isset($result) && is_array($result)){
            foreach ($result as $row){
                
		$id_product = (int) $row['id_product'];
		$id_product_attribute = (int) $row['id_product_attribute'];
		
                //if(!isset($row['id_product_attribute']) || empty($row['id_product_attribute']) )
                //    $row['id_product_attribute'] = 0;
            
                if(isset($row['id_country']))
                    $return[$id_product][$id_product_attribute]['id_country']             = (int) $row['id_country'];
                
                $return[$id_product][$id_product_attribute]['id_product']             = (int) $row['id_product'];
                $return[$id_product][$id_product_attribute]['id_product_attribute']   = (int) $row['id_product_attribute'];
                $return[$id_product][$id_product_attribute]['sku']                    = $row['sku'];
                $return[$id_product][$id_product_attribute]['id_shop']                = (int) $row['id_shop'];
                $return[$id_product][$id_product_attribute]['id_lang']                = (int) $row['id_lang'];
                $return[$id_product][$id_product_attribute]['force']                  = (int) $row['c_force'];
                $return[$id_product][$id_product_attribute]['no_price_export']        = (int) $row['no_price_export'];
                $return[$id_product][$id_product_attribute]['no_quantity_export']     = (int) $row['no_quantity_export'];
                $return[$id_product][$id_product_attribute]['latency']                = (int) $row['latency'];
                $return[$id_product][$id_product_attribute]['disable']                = isset($row['c_disable']) ? (int) $row['c_disable'] : null;
                $return[$id_product][$id_product_attribute]['price']                  = (float) $row['price'];
                $return[$id_product][$id_product_attribute]['text']                   = $row['text'];
                $return[$id_product][$id_product_attribute]['shipping']               = $row['shipping'];
                $return[$id_product][$id_product_attribute]['shipping_type']          = $row['shipping_type'];
                $return[$id_product][$id_product_attribute]['date_add']               = $row['date_add'];
                $return[$id_product][$id_product_attribute]['date_upd']               = $row['date_upd'];
		
		if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			if(isset($row[$marketplace_fields])) {	
			    switch ($marketplace_values['type']){
				case 'tinyint' : 
				case 'int' : 
				    $return[$id_product][$id_product_attribute][$marketplace_fields] = (int) $row[$marketplace_fields];
				    break;
				case 'decimal' : 
				    $return[$id_product][$id_product_attribute][$marketplace_fields] = (float) $row[$marketplace_fields];
				    break;
				default :
                                    if(is_serialized($row[$marketplace_fields])){
                                        $return[$id_product][$id_product_attribute][$marketplace_fields] = unserialize($row[$marketplace_fields]);
                                    } else {
                                        $return[$id_product][$id_product_attribute][$marketplace_fields] = $row[$marketplace_fields];
                                    }
				    break;
			    }
			    
			}
		    }
		} 
            }
    	}
    
    	return $return;
    }
    
     public function get_product_by_id_category($id_category, $id_shop)
    {       
        $data = array();
        $_db = ObjectModel::$db;
        
        $sql = "SELECT "
                . "p.id_product as id_product, "
                . "pa.id_product_attribute as id_product_attribute, "
                . "CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS sku "
                . "FROM  {$_db->prefix_table}product p "
                . "LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop "
                . "WHERE p.id_category_default = ".$id_category." AND p.id_shop = ".(int)$id_shop."; ";                
        $result = $_db->db_query_str($sql);  
        
        if(isset($result)){
            
            foreach ($result as $key => $value){
                $data[$key]['id_product'] = $value['id_product'];
                $data[$key]['id_product_attribute'] = $value['id_product_attribute'];
                $data[$key]['sku'] = $value['sku'];
            }
        }
        
        return $data;
    }
    
    public function product_options_download($values, $id_shop, $id_lang = null){
                
        $list = array();
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['sku'] = "SKU";
        $list['header']['price'] = "Price Override";
        $list['header']['disable'] = "Disable";
        $list['header']['force'] = "Force in Stock";
        $list['header']['no_price_export'] = "Do not export price";
        $list['header']['no_quantity_export'] = "Do not export quantity";
        $list['header']['latency'] = "Latency";
        $list['header']['shipping'] = "Shipping";
	$list['header']['text'] = "Condition note";
        
	if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
	    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
		$list['header'][$marketplace_fields] = ucfirst(str_replace("_", " ", $marketplace_fields));	
	    }
	}	
       
        // Get Product Options            
        $_db = ObjectModel::$db;
        
        $sql = "SELECT 
                mpo.*,
                p.id_product as id_product, 
                pa.id_product_attribute as id_product_attribute, 
                CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference,                
                CASE WHEN pa.quantity IS NOT NULL THEN pa.quantity ELSE p.quantity END AS quantity 
                FROM {$_db->prefix_table}product p
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                LEFT JOIN ".$this->table." mpo ON p.id_product = mpo.id_product ";
        
        if($this->use_site){
             $sql .= "AND mpo.id_country = " . $this->id_country . " ";
        } else {
	    if(!isset($id_lang) || empty($id_lang)){
		$id_lang = $this->get_id_language($id_shop);		
	    }
	    $sql .= "AND mpo.id_lang = " . (int)$id_lang . " ";
	}
                    
        $sql .= "AND p.id_shop = mpo.id_shop
                WHERE p.id_shop = $id_shop AND (p.date_upd IS NULL OR p.date_upd = '') " ;
                    
        if(isset($values['category']) && !empty($values['category'])) {
            $list_category = implode(', ', $values['category']);
            if(strlen($list_category))
                $sql .= "AND ( "
                    . "p.id_category_default IN (" . $list_category . ")"
                    . ") ";
        }
        if(isset($values['only_active']) && $values['only_active'] == 1) {
            $sql .= "AND p.active = 1 ";
        }
        $sql .= " GROUP BY p.id_product, pa.id_product_attribute ORDER BY p.id_product ASC, pa.id_product_attribute ASC ; " ;
        
        $result = $_db->db_query_str($sql);  
        
        if(isset($values['only_in_stock']) && $values['only_in_stock'] == 1) {
            $product = new Product(ObjectModel::$user);
        }
        
        if(isset($result)){
            
            foreach ($result as $key => $value){
                
                if(isset($values['only_in_stock']) && $values['only_in_stock'] == 1) {
                    // Check Offer
                    if(isset($value['id_product_attribute']) && !empty($value['id_product_attribute'])){
                        $offer = $product->getProductCombinationOffer($value['id_product'], $value['id_product_attribute'], $id_shop) ;
                        if(!empty($offer) && $offer['quantity'] <= 0) continue;
                    } else {
                        $offer = $product->getProductOffer($value['id_product'], $id_shop);
                        if(!empty($offer) && $offer['quantity'] <= 0) continue;
                    }
                    // Check Product Quantity
                    if(!isset($value['quantity']) || $value['quantity'] <= 0){
                        continue;
                    }
                }  
                $list[$key]['id_product'] = $value["id_product"];
                $list[$key]['id_product_attribute'] = $value["id_product_attribute"];
                $list[$key]['sku'] = $value['reference'];
                $list[$key]['price'] = $value['price'];
                $list[$key]['disable'] = $value['c_disable'];
                $list[$key]['force'] = $value['c_force'];
                $list[$key]['no_price_export'] = $value['no_price_export'];
                $list[$key]['no_quantity_export'] = $value['no_quantity_export'];
                $list[$key]['latency'] = $value['latency'];
                $list[$key]['shipping'] = $value['shipping'];
		$list[$key]['text'] = $value['text'];
		
		if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
			$list[$key][$marketplace_fields] = $value[$marketplace_fields];	
		    }
		}               
            }
        }        
                
        ob_start();
        ob_get_clean();
        
        $filename = 'offers_options.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) { 
            fputcsv($h, $data, ';'); 
        }
    } 
    
    public function product_options_download_template(){
        
        $list = array();
        
        $list['header']['id_product'] = "id_product";
        $list['header']['id_product_attribute'] = "id_product_attribute";
        $list['header']['sku'] = "SKU";
        $list['header']['price'] = "Price Override";
        $list['header']['disable'] = "Disable";
        $list['header']['force'] = "Force in Stock";
        $list['header']['no_price_export'] = "Do not export price";
        $list['header']['no_quantity_export'] = "Do not export quantity";
        $list['header']['latency'] = "Latency";
        $list['header']['shipping'] = "Shipping";
	$list['header']['text'] = "Condition note";
        
	if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
	    foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
		$list['header'][$marketplace_fields] = ucfirst(str_replace("_", " ", $marketplace_fields));	
	    }
	}     
               
        ob_start();
        ob_get_clean();
        
        $filename = 'offers_options_template.csv';
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        $h = fopen('php://output', 'w');
        foreach ($list as $data) { 
            fputcsv($h, $data, ';'); 
        }
    }  
    
    public function product_options_upload($data, $id_shop, $id_lang = null)
    {
        $_db = ObjectModel::$db;
        $table_name = $this->table;
	
	if(!isset($id_lang) || empty($id_lang)){
	    $id_lang = $this->get_id_language($id_shop);		
	}
	
        $sql = array();
        $error_produst = array();
        $i = $count = $no_upload = $no_success = $error_error = 0;
        $update_sql = "";

        foreach ($data as $value){
	     
            $count++;
            $data_value =  array();
            $no_upload++;
            
            if(!isset($value['sku']) || empty($value['sku'])) {
                $i++; $error_error++;
                $error_produst['missing_sku'] = $i;
                continue;
            } 
            
            $sku = $value['sku'];
            
            if(!isset($value['id_product']) || empty($value['id_product'])) {
                $error_error++;
                $error_produst[$sku] = 'Missing Product ID';
                continue;
            } 
            
//            if((isset($value['price']) && !empty($value['price'])) || (isset($value['price']) && ($value['price'] === 0))) {
//                if(!is_numeric($value['price'])){
//                    $error_error++;
//                    $error_produst[$sku] = 'Price Override must have a decimal. Currently value : "' . $value['price'] . '" ';
//                    continue;
//                }
//            }
//            if((isset($value['shipping']) && !empty($value['shipping'])) || (isset($value['shipping']) && ($value['shipping'] === 0))) {
//                if(!is_numeric($value['shipping'])){
//                    $error_error++;
//                    $error_produst[$sku] = 'Shipping Override must have a decimal. Currently value : "' . $value['price'] . '" ';
//                    continue;
//                }
//            } 

            $data_value['id_product'] = (int)$value['id_product'];
            $data_value['id_product_attribute'] = isset($value['id_product_attribute']) ? (int)$value['id_product_attribute'] : 0;
            $data_value['id_shop'] = (int)$id_shop;            
            $data_value['sku'] = $sku;
            $data_value['c_force'] = (isset($value['force']) && !empty($value['force'])) ? (int)$value['force'] : '';
            $data_value['no_price_export'] = (isset($value['no_price_export']) && !empty($value['no_price_export'])) ? $value['no_price_export'] : 0;
            $data_value['no_quantity_export'] = (isset($value['no_quantity_export']) && !empty($value['no_quantity_export'])) ? $value['no_quantity_export'] : 0; 
           
            if(isset($value['latency']) && !empty($value['latency'])){
                $data_value['latency'] = (int)$value['latency']; 
            } else {
		$data_value['latency'] = null; 
	    }
            $data_value['c_disable'] = (isset($value['disable']) && !empty($value['disable'])) ? $value['disable'] : 0; 
            if((isset($value['price']) /*&& !empty($value['price'])) || (isset($value['price']) &&*/ || ($value['price'] === 0))) {
                $data_value['price'] = $value['price'];
            }else {
		$data_value['price'] = null; 
	    }
            if(isset($value['text']) && !empty($value['text'])){
                $data_value['text'] = $value['text'];
            }else {
		$data_value['text'] = null; 
	    }
              
	    if((isset($value['shipping']) /*&& !empty($value['shipping'])) || (isset($value['shipping']) &&*/ || ($value['shipping'] === 0))) {
		$data_value['shipping'] = $value['shipping'];
	    } else {
		$data_value['shipping'] = null;
	    }

	    if(isset($value['shipping_type']) && !empty($value['shipping_type'])){
		$data_value['shipping_type'] = $value['shipping_type'];
	    } else {
		$data_value['shipping_type'] = null;
	    }     
		
	    if($this->use_site){
                $data_value['id_country'] = (int)$this->id_country;
            } else {
		$data_value['id_lang'] = $id_lang ? (int)$id_lang : null;
	    }
	    
	    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
		    if(isset($value[$marketplace_fields]) && !empty($value[$marketplace_fields])) {
			$data_value[$marketplace_fields] = $value[$marketplace_fields];	
		    } else {
			$data_value[$marketplace_fields] = null;	
		    }
                    if(isset($marketplace_values['set_zero']) && $marketplace_values['set_zero']){
                        if(!isset($value[$marketplace_fields]) || empty($value[$marketplace_fields])) {
                            $data_value[$marketplace_fields] = 0;
                        }
                    }
		}
	    }
		
            $data_value['date_upd'] = date('Y-m-d H:i:s'); 
            $data_value['flag_update'] = 1;

            $sql[] = $this->replace_string($table_name, $data_value, true) ;

            if(!isset($value['id_product_attribute']) || ((int) $value['id_product_attribute'] == 0)){

                $where = array(
                    'id_product' => array('operation' => '=', 'value' => $data_value['id_product']),
                    'id_shop' => array('operation' => '=', 'value' => $data_value['id_shop']),
                );

                if($this->use_site && isset($data_value['id_country'])){
                    $where['id_country'] = array('operation' => '=', 'value' => $data_value['id_country']);
                    unset($data_value['id_country']);
                }elseif(isset($data_value['id_lang'])){
                    $where['id_lang'] = array('operation' => '=', 'value' => $data_value['id_lang']);
                    unset($data_value['id_lang']);
                }

                unset($data_value['sku']);
                unset($data_value['id_shop']);
                unset($data_value['id_product']);
                unset($data_value['id_product_attribute']);

                $update_sql .= $this->update_string($table_name, $data_value, $where)  ;
            }
            
            $no_success++;
        }        
        
        // update before replace
        if(strlen($update_sql) > 10){
            $_db->db_exec($update_sql, false, true, true, true);
        }
        
	if(!empty($sql)){
	    //$exe_sql = $this->save_offers_options($sql);
	    $fields = '';
	    if($this->use_site){
                $fields .=  'id_country, ' ;
            } else {
		$fields .=  'id_lang, ' ;
	    }
	    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
		foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
		   $fields .= $marketplace_fields . ', ' ;
		}
	    }
	    $base_sql = "REPLACE INTO {$this->table} 
            (id_product,id_product_attribute,id_shop,sku,c_force,no_price_export,no_quantity_export,latency,c_disable,price,text,shipping,shipping_type, $fields date_upd,flag_update)
            VALUES ";  
            
            if(sizeof($sql)>1000){
                $sql_list=array();
                $exe_sql='';
                foreach($sql as $i=>$sql_item){
                    $sql_list[]=$sql_item;
                    if($i!=0 && $i%1000==0){ 
                        $exe_sql .=  $base_sql. implode(',',$sql_list).";\n" ; 
                        unset($sql_list);
                        $sql_list=array();
                    }
                }
                if(!empty($sql_list)){
                    $exe_sql .=  $base_sql. implode(',',$sql_list).";\n" ; 
                }

                
                $exe = $_db->db_exec($exe_sql,false, true, true);   
            }else{  
                $exe_sql =  $base_sql. implode(',',$sql) ;
                $exe = $_db->db_exec($exe_sql,false, true, true);
            }
	}
	
        if($exe){
            $this->update_amazon_status($id_shop);
        } 

        // send update offer options
	$this->update_offer_options_to_shop($id_shop);
        
        return array(
            'pass' => ($exe) ? true : false, 
            'has_error'=> empty($error_produst) ? false : true, 
            'error_product' => $error_produst,
            'no_upload' => $no_upload,
            'no_success' => $no_success,
            'no_error' => ($error_error == 0) ? '0' : $error_error
        );        
    }
    
    public function get_offers_options_to_update_shop($id_shop, $debug = false) {

	$offers_options = array();
	
	$table_name = $this->table;
       
    	$_db = ObjectModel::$db;
    	
    	if(!$_db->db_table_exists($table_name, true)) 
	    return;
    
	// get id marketplace
	$id_marketplace = ObjectModel::get_marketplace_by_name($this->marketplace);
	
	// get all countries in this marketplace
	$UserConfiguration = new UserConfiguration();
	$countries = $UserConfiguration->getSiteByIdMargetplace($id_marketplace);
	    
    	$sql = "SELECT * FROM $table_name WHERE id_shop = ".(int)$id_shop." AND flag_update = 1 " ;
        
        if($this->use_site){
             //$sql .= "AND id_country = " . (int) $this->id_country . " ";
        } 
	
    	$sql .= "; ";
	   
    	$result = $_db->db_query_str($sql);

    	if(isset($result) && is_array($result)){	    
	    
	    if($this->use_site){
		if(!isset($countries[$id_marketplace][$this->id_country])){
		    return ;
                } 

                $country = $countries[$id_marketplace][$this->id_country];
                $ext = trim($country['iso_code']);

                foreach ($result as $row){
                    
                    $id_product = (int) $row['id_product'];
                    $id_product_attribute = (int) $row['id_product_attribute'];

                    $keys = 'Offers:'. $row['id_product'];
                    $offers_options[$this->marketplace][$ext][$keys]['Offer:ProductId'] = (int)$row['id_product'];

                    $key = 'Offer:'. $row['id_product_attribute'];
                    $offers_options[$this->marketplace][$ext][$keys][$key]['Item:ProductAttributeId'] = (int)$row['id_product_attribute'];
                    $offers_options[$this->marketplace][$ext][$keys][$key]['Item:Reference'] = $row['sku'];

                    $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Force:value'] = isset($row['c_force']) ? $row['c_force'] : null;

                    if(strtolower($this->marketplace) == 'amazon')
                    {
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Nopexport:value'] = $row['no_price_export'];
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Noqexport:value'] = $row['no_quantity_export'];
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Latency:value'] = $row['latency'];
                    }

                    $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Disable:value'] = isset($row['c_disable']) ? $row['c_disable'] : null;
                    $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Price:value'] = $row['price'];

                    if(strtolower($this->marketplace) == 'amazon')
                    {
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Text:value'] = $row['text'];
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Shipping:value'] = $row['shipping'];
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Shipping_type:value'] = $row['shipping_type'];
                    } else if(strtolower($this->marketplace) == 'main') {
                        $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Shipping:value'] = $row['shipping'];
                    }

                    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
                        foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){

                            if(isset($row[$marketplace_fields])) {

                                $marketplacefields = ucfirst($marketplace_fields);

                                switch ($marketplace_values['type']){

                                    case 'tinyint' :
                                    case 'int' :
                                        //$offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        //break;
                                    case 'decimal' :
                                        //$offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        //break;
                                    default :
                                        if(is_serialized($row[$marketplace_fields])){
                                            $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = unserialize($row[$marketplace_fields]);
                                        } else {
                                            $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        }
                                        break;
                                }

                            }
                        }
                    }
                }
	    } else {
                
                foreach ($result as $row){
                   
                    $id_lang = (int) $row['id_lang'];
                    $id_product = (int) $row['id_product'];
                    $id_product_attribute = (int) $row['id_product_attribute'];

                    $keys = 'Offers:'. $row['id_product'];
                    $offers_options['main'][$keys]['Offer:ProductId'] = $id_product;
                    $offers_options['main'][$keys]['Offer:LangId'] = $id_lang;

                    $key = 'Offer:'. $row['id_product_attribute'];
                    
                    $offers_options['main'][$keys][$key]['Item:ProductAttributeId'] = (int)$row['id_product_attribute'];
                    $offers_options['main'][$keys][$key]['Item:Reference'] = $row['sku'];
                    $offers_options['main'][$keys][$key]['Item']['Fields']['Force:value'] = isset($row['c_force']) ? $row['c_force'] : null;
                    $offers_options['main'][$keys][$key]['Item']['Fields']['Disable:value'] = isset($row['c_disable']) ? $row['c_disable'] : null;
                    $offers_options['main'][$keys][$key]['Item']['Fields']['Price:value'] = $row['price'];
                    $offers_options['main'][$keys][$key]['Item']['Fields']['Shipping:value'] = $row['shipping'];

                    if(isset(self::$fields[$this->marketplace]) && !empty(self::$fields[$this->marketplace])){
                        foreach (self::$fields[$this->marketplace] as $marketplace_fields => $marketplace_values){
                            if(isset($row[$marketplace_fields])) {
                                $marketplacefields = ucfirst($marketplace_fields);
                                switch ($marketplace_values['type']){
                                    case 'tinyint' :
                                    case 'int' :
                                        //$offers_options['main'][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        //break;
                                    case 'decimal' :
                                        //$offers_options['main'][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        //break;
                                    default :
                                        if(is_serialized($row[$marketplace_fields])){
                                            $offers_options['main'][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = unserialize($row[$marketplace_fields]);
                                        } else {
                                            $offers_options['main'][$keys][$key]['Item']['Fields'][$marketplacefields.':value'] = $row[$marketplace_fields];
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
    	}	
	
	if(isset($this->marketplace) && $this->marketplace == 'amazon') {
		
	    require_once dirname(__FILE__) . '/../../Amazon/functions/amazon.fba.stock.php';


            $userFBA = AmazonUserData::getMasterFBA(ObjectModel::$user, $this->id_country, $id_shop, false);

            if(!isset($userFBA['fba_stock_behaviour']) || empty($userFBA['fba_stock_behaviour'])){
                if ($debug){
                    echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
                    echo 'Missing fba_stock_behaviour</br>';
                }
                return $offers_options;
            }
            
	    $amazon_database = new AmazonDatabase(ObjectModel::$user); 

	    // get FBA to update from amazon_fba_flag where flag = 1
	    $flag = 1;
	    $list_fba_flag = $amazon_database->get_fba_flag($id_shop, $this->id_country, AmazonFBAStock::PROCESS_KEY, $userFBA['fba_stock_behaviour'], $flag);

	    if ($debug){
		echo '<pre><b>' . __FUNCTION__ . ' (' . __LINE__ . ')</b></pre>' ;
		echo 'list fba flag : <pre>' . print_r($list_fba_flag, true) . '</pre>';
	    }
	    
	    $list_ext = array();
	    
	    if(!isset($countries[$id_marketplace][$this->id_country]))
		return $offers_options;
	    
	    $region = $countries[$id_marketplace][$this->id_country]['id_region'];
	   
	    foreach ($countries[$id_marketplace] as $id_country => $country){
		if($country['id_region'] == $region)
		{ 
		    $list_ext[$id_country] = trim($country['iso_code']); //Amazon_Tools::iso_codeToId($country['ext']);
		}
	    } 

	    if(!empty($list_fba_flag)){
                foreach($list_fba_flag as $fba_flag){
                    foreach ($list_ext as $id_country => $ext){
                        $sql = "SELECT * FROM $table_name "
                            . "WHERE id_shop = ".(int)$id_shop." "
                            . "AND id_country = ".(int)$id_country." "
                            . "AND id_product = ".(int)$fba_flag['id_product']." "
                            . "AND id_product_attribute = ".(int)$fba_flag['id_product_attribute']." "
                            . "AND sku = '".$fba_flag['sku']."' ; " ;
                        $results = $_db->db_query_str($sql);
                        if(isset($results) && is_array($results)){
                            foreach ($results as $result){
                                $keys = 'Offers:'. $fba_flag['id_product'];
                                $offers_options[$this->marketplace][$ext][$keys]['Offer:ProductId'] = (int)$result['id_product'];
                                $key = 'Offer:'. $fba_flag['id_product_attribute'];
                                $offers_options[$this->marketplace][$ext][$keys][$key]['Item:ProductAttributeId'] = (int) $result['id_product_attribute'];
                                $offers_options[$this->marketplace][$ext][$keys][$key]['Item:Reference'] = $result['sku'];
                                $offers_options[$this->marketplace][$ext][$keys][$key]['Item']['Fields']['Fba:value'] = $result['fba'];
                            }
                        }
                    }
                }
	    }
	}
	
	return $offers_options;
	
    }   
    
    public function save_offers_options($offers, $only_value=false){
	
	$sql = '';
	$date_add = date('Y-m-d H:i:s');
	
	if(isset($offers) && !empty($offers)){
	    
	    $marketplace = strtolower(isset(self::$tables[$this->marketplace]) ? self::$tables[$this->marketplace] : $this->marketplace);  
	    
	    $table = $this->table;		    
	    $keys_stringified = $offer = array();

	    foreach (self::$fields['keys'] as $main_fields => $main_values){
		if(isset($offers[$main_fields]) /*&& !empty($offers[$main_fields])*/){
		    $keys_stringified[] = $main_fields;
		    $offer[] = $offers[$main_fields];
		}
	    }

	    foreach (self::$fields['main'] as $main_fields => $main_values){
		if(isset($offers[$main_fields]) /*&& !empty($offers[$main_fields])*/){
		    $keys_stringified[] = $main_fields;
		    $offer[] = $offers[$main_fields];
		}
	    }

	    if($marketplace != 'main'){
		if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			if(isset($offers[$marketplace_fields]) /*&& !empty($offers[$marketplace_fields])*/){
			    $keys_stringified[] = $marketplace_fields;
                            if(is_array($offers[$marketplace_fields])){
                                $offer[] = serialize($offers[$marketplace_fields]);
                            } else {
                                $offer[] = $offers[$marketplace_fields];
                            }
			}
		    }
		}
	    }

	    if(!$only_value){
		$sql .= "REPLACE INTO $table (".implode(",", $keys_stringified).",date_upd) VALUES ('" . implode("','", $offer) . "','$date_add') ";
	    } else {
		$sql .= ",('" . implode("','", $offer) . "', '$date_add')";
	    }

	    unset($keys_stringified);
	    unset($offer);
	}
	
	return $sql;
    }  
    
    public function update_offers_options($offers, $where){
	
	$sql = '';	
	if(isset($offers) && !empty($offers)){
	    $sql .= $this->update_string($this->table, $offers, $where);	 
	}
	
	return $sql;
    }  
    
    public function getProductBySKU($id_shop, $SKU=null, $is_bulk=false) {
	
        if (!isset($id_shop) || empty($id_shop) ){
	    
            if(!$is_bulk && (!isset($SKU) || empty($SKU)))
		return;
        }
	
        $products = array();
        $_db = ObjectModel::$db;
        $sql = "SELECT 
                    p.id_product AS id_product,
                    p.id_category_default AS id_category_default,
                    pa.id_product_attribute AS id_product_attribute,
                    CASE 
                        WHEN pa.quantity IS NOT NULL THEN pa.quantity                         
                        WHEN p.quantity IS NOT NULL THEN p.quantity                        
                    END AS quantity,
                    CASE 
                        WHEN pa.reference IS NOT NULL THEN pa.reference 
                        WHEN pa.sku IS NOT NULL THEN pa.sku 
                        WHEN pa.ean13 IS NOT NULL THEN pa.ean13 
                        WHEN pa.upc IS NOT NULL THEN pa.upc 
                        WHEN p.reference IS NOT NULL THEN p.reference 
                        WHEN p.sku IS NOT NULL THEN p.sku 
                        WHEN p.ean13 IS NOT NULL THEN p.ean13 
                        WHEN p.upc IS NOT NULL THEN p.upc 
                    END AS sku
                FROM {$_db->prefix_table}product p 
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                WHERE p.id_shop = " . $id_shop . "  " ;
		
	if(!$is_bulk) {
                $sql .= "AND 
                ( 
                    pa.reference = CASE WHEN pa.reference = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.sku = CASE WHEN pa.sku = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.ean13 = CASE WHEN pa.ean13 = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR pa.upc = CASE WHEN pa.upc = '" . $SKU . "' THEN '" . $SKU . "' END
                    OR p.reference = CASE WHEN p.reference = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.sku = CASE WHEN p.sku = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.ean13 = CASE WHEN p.ean13 = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.upc = CASE WHEN p.upc = '" . $SKU . "' THEN '" . $SKU . "' END 
                ) ";
		
	} else {
	    
	    if(isset($SKU) && !empty($SKU)){
		$sql .= "AND 
                ( 
                    pa.reference IN (" . $SKU . ")
                    OR p.reference IN (" . $SKU . ")
                ) ";
	    }
	    
	}
	
	$sql .= " ; ";
	
        $result = $_db->db_query_str($sql);
	
        foreach ($result as $product) {
	    
	    if(!$is_bulk) {
		$products['id_product'] = $product['id_product'];
		$products['id_product_attribute'] = $product['id_product_attribute'];
		$products['id_category_default'] = $product['id_category_default'];
		$products['quantity'] = $product['quantity'];
		$products['sku'] = $product['sku'];
	    } else {
		$key = $product['sku'];
		$products[$key]['id_product'] = $product['id_product'];
		$products[$key]['id_product_attribute'] = $product['id_product_attribute'];
		$products[$key]['id_category_default'] = $product['id_category_default'];
		$products[$key]['quantity'] = $product['quantity'];
		$products[$key]['sku'] = $product['sku'];
	    }
	    
        }

        return $products;
    }
    
    public function get_fields_options() {
	
	$fields = array();
	
	if(isset($this->marketplace)) {
	    $marketplace = $this->marketplace ;
	    if($marketplace != 'main'){
		if(isset(self::$fields[$marketplace]) && !empty(self::$fields[$marketplace])){
		    foreach (self::$fields[$marketplace] as $marketplace_fields => $marketplace_values){
			$fields[$marketplace_fields]['type'] = $marketplace_values['input_type'];
		    }
		}
	    }
	}
	
	return $fields;
    }
    
    // update amazon status
    public function update_amazon_status($id_shop){
	
	require_once dirname(__FILE__) . '/../../Amazon/classes/amazon.database.php';

        $date_add = date('Y-m-d H:i:s');
        $details = $this->table;
        
	if(!$this->use_site){
	    $countries = $this->get_country($id_shop);
	    foreach ($countries as $id_country => $country){
		$amazon_database = new AmazonDatabase(ObjectModel::$user); 
		$amazon_database->update_amazon_status($id_country, $id_shop, $date_add, $details);
	    }
	} else {
	    if(isset($this->marketplace) && $this->marketplace != 'amazon') {
		return false;
	    }
	    $amazon_database = new AmazonDatabase(ObjectModel::$user); 
	    $amazon_database->update_amazon_status($this->id_country, $id_shop, $date_add, $details);
	}
    }
    
    private function get_country($id_shop, $only_id_country=true){
        
        $mysql_db = new ci_db_connect(); 
        
        $user_name = $mysql_db->escape_str(ObjectModel::$user);
	
	if($only_id_country)
	    $select = 'id_country';
	else
	    $select = '*';
	
        $sql = "SELECT $select FROM amazon_configuration WHERE user_name = '".$user_name."' AND id_shop = '".(int)$id_shop."' AND active = 1; "; 

        $query = $mysql_db->select_query($sql);
        $data = array();
        while($row = $mysql_db->fetch($query)){ 
	    
	    if($only_id_country)
		$data[$row['id_country']] = $row['id_country'];
	    else{
		$data[$row['id_country']] = $row;
	    }
        }        
	
        return $data;
    } 
    
    public function replace_string($table, $data, $multi_value = false) {
	
	$_db = ObjectModel::$db;
	$no_prefix = false;
	if(isset($this->marketplace) && !empty($this->marketplace))
	    $no_prefix = true;
	
        if (isset($data) && !empty($data)) {
            return $_db->replace_string($table, $data, $multi_value, $no_prefix);                   
        }
        return null;
    }
    
    public function update_string($table, $data, $where = array(), $limit = 0) {
	
	$_db = ObjectModel::$db;
	$no_prefix = false;
	if(isset($this->marketplace) && !empty($this->marketplace))
	    $no_prefix = true;
	
        if (isset($data) && !empty($data)) {
            return $_db->update_string($table, $data, $where, $limit, $no_prefix);
        }
        return null;
    }
    
    public function insert_string($table, $data) {
	
	$_db = ObjectModel::$db;
	$no_prefix = false;
	if(isset($this->marketplace) && !empty($this->marketplace))
	    $no_prefix = true;	
	
        if (isset($data) && !empty($data)) {
            $filter_data = ObjectModel::_filter_data($table, $data, true, $no_prefix);
            return $_db->insert_string($table, $filter_data, $no_prefix);
        }
        return null;
    }
    
    public function get_id_language($id_shop) {
	if(!isset($id_shop) || !$id_shop) return;
	$Language = Language::getLanguageDefault($id_shop);
	$lang = array_keys($Language);
	$id_lang = $lang[0];
	return (int)$id_lang; 
    }

    /*public function getDeletedProducts($id_shop)
    {
        $product_list = array();
        $_db = ObjectModel::$db;
	
        $sql = "SELECT * FROM {$_db->prefix_table}product p WHERE p.id_shop = $id_shop AND (p.date_upd IS NULL OR p.date_upd = ''); " ;

        $result = $_db->db_query_str($sql);

        foreach ($result as $data){
            $product_list[$data['reference']] = $data;
        }

        return $product_list;
    }*/

    public function exportProductDefLanguageList($productIDList, $id_shop) {

        $productLangList = array();
        $_db = ObjectModel::$db;
        $productLangQ = "SELECT /*l.iso_code as iso_code,*/ pl.id_product as id_product,
			pl.name as name /*, pl.description as description, pl.description_short as description_short*/
			FROM {$_db->prefix_table}product_lang pl
			JOIN {$_db->prefix_table}language l ON l.id_lang = pl.id_lang and pl.id_shop=l.id_shop
			WHERE l.id_shop = '" . $id_shop . "' AND l.is_default = 1  AND  pl.id_product IN ('" . implode("', '", $productIDList) . "')
			ORDER BY pl.id_lang ; ";

        $result = $_db->db_query_str($productLangQ);

        foreach ($result as $row) {
            $id_product = $row['id_product'];
            $productLangList[$id_product]['name'] = $row['name'];
//            $productLangList[$id_product]['description'] = htmlspecialchars_decode($row['description']);
//            $productLangList[$id_product]['description_short'] = htmlspecialchars_decode($row['description_short']);
        }

        return $productLangList;

    }
}