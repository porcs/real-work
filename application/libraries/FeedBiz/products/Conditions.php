<?php

class Conditions extends ObjectModel {

    public $id_condition;
    public $name;

    public static $definition = array(
        'table' => 'conditions',
        'primary' => array('id_condition'),
        'fields' => array(
            'id_condition' => array('type' => 'int', 'required' => true, 'size' => 10),
            'name' => array('type' => 'varchar', 'required' => true, 'size' => 64),
        ),
    );

    public static function getConditionNameByID($id_condition, $id_shop) {
        $condition = '';
        if (!isset($id_condition) || empty($id_condition))
            return NULL;

        $_db = ObjectModel::$db;
        $_db->from('conditions');
        $_db->where(array('id_condition' => (int) $id_condition, 'id_shop' => (int) $id_shop));
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $cond)
            $condition = $cond['name'];

        return $condition;
    }

    public static function getConditions($id_shop) {
        $condition = array();
        $_db = ObjectModel::$db;
        $_db->from('conditions');
        $_db->where(array('id_shop' => (int) $id_shop));
        $result = $_db->db_array_query($_db->query);

        foreach ($result as  $cond)
            $condition[$cond['id_condition']] = $cond['name'];
        
        return $condition;
    }

    public static function getConditionMapping($id_shop, $id_market) {
        $_db = ObjectModel::$db;
        $condition = array();
        $add = '';
        if ($id_market != null) {
            $add = " AND mc.id_marketplace = $id_market ";
        }

        $sql = "SELECT c.id_condition as id_condition, c.name as name, mc.condition_value as condition_value 
            FROM {$_db->prefix_table}conditions c
            LEFT JOIN {$_db->prefix_table}mapping_condition mc ON c.id_shop = mc.id_shop AND c.name = mc.name $add
            WHERE c.id_shop = '" . $id_shop . "'  
            GROUP BY c.id_condition
            ORDER BY c.id_condition; ";

        $result = $_db->db_query_str($sql);        

        foreach ($result as $cond){
            $condition[$cond['id_condition']] = array('txt' => $cond['name'], 'condition_value' => $cond['condition_value']);
        }
        
        return $condition;
    }

    public static function getConditionMappingByName($id_shop, $id_market, $name) {

        $_db = ObjectModel::$db;
        $where = $condition = '';

        if ($id_market != null) {
            $where = " AND id_marketplace = " . (int) $id_market . " ";
        }

        if ($name != null) {
            $where = " AND name = '" . $name . "' ";
        }

        $sql = "SELECT condition_value FROM {$_db->prefix_table}mapping_condition   
                WHERE id_shop = " . (int) $id_shop . " $where ; ";

        $result = $_db->db_query_str($sql);

        foreach ($result as $cond)
            $condition = $cond['condition_value'];

        return $condition;
    }
    public static function setConditionMapping($id_shop,$id_marketplace,$market_condition,$shop_condition){
      
        $_db = ObjectModel::$db;
        $sql_delete = "DELETE FROM {$_db->prefix_table}mapping_condition WHERE id_shop= $id_shop and id_marketplace= $id_marketplace and name= '$shop_condition'; ";
        $_db->db_exec($sql_delete);

        $sql = "INSERT INTO {$_db->prefix_table}mapping_condition (name,id_shop,id_marketplace,condition_value) "
	. "VALUES ('$shop_condition' , '$id_shop' , '$id_marketplace' , '$market_condition')";      
        
		//$file = dirname(__FILE__).'/../../../../assets/apps/users/u00000000000035/process/sqlcondition.txt';
		//file_put_contents($file, $sql, FILE_APPEND);
          
        return $_db->db_exec($sql, false , false);
    }

    public static function removeConditionMapping($id_shop, $id_marketplace, $market_condition) {
        $_db = ObjectModel::$db;        
        $sql = "delete from {$_db->prefix_table}mapping_condition where id_shop= '$id_shop' and id_marketplace='$id_marketplace' and condition_value= '$market_condition'; ";
        return $_db->db_exec($sql);
    }

}
