<?php

class Supplier extends ObjectModel
{
    public $id_supplier;
    public $name;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'supplier',
        'primary'   => array('id_supplier'),
        'fields'    => array(
            'id_supplier'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'tmpcode'       =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    public static function getSupplierByProductId($id_product, $id_shop, $id_product_attribute = null)
    {
        if (!isset($id_product) || empty($id_product))
            return NULL;
         
        $_db = ObjectModel::$db;
        $columns = array('ps.id_product', 's.id_supplier', 's.name', 'ps.supplier_reference');
        $cols =$_db->put_array_tb_alias($columns);
        
        $_db->select($cols);
        $_db->from('product_supplier ps');
        $_db->leftJoin('supplier', 's', '(ps.id_supplier = s.id_supplier) AND (ps.id_shop = s.id_shop)');
        
        $arr_where = array('ps.id_product' => (int)$id_product, 'ps.id_shop' => (int)$id_shop) ;
        
        if(isset($id_product_attribute))
            $arr_where['ps.id_product_attribute'] = (int)$id_product_attribute;
        
        $_db->where($arr_where);
        $result = $_db->db_array_query($_db->query);
        
        $supplier = array();
        
        foreach ($result as $values)
        {
            $supplier[$values['s.id_supplier']]['name'] = $values['s.name'];
            $supplier[$values['s.id_supplier']]['supplier_reference'] = $values['ps.supplier_reference'];
        }
        
        return !empty($supplier) ? $supplier : null;
    }
    
    public static function getSuppliers($id_shop)
    {
        $supplier = array();
        
        $_db = ObjectModel::$db;
        $_db->from('supplier');
        $_db->where( array('id_shop' => (int)$id_shop) );

        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $values)
            $supplier[$values['id_supplier']]['name'] = $values['name'];
        
        return $supplier;
    }
    
}