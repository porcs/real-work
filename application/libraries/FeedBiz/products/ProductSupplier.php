<?php

class ProductSupplier extends ObjectModel
{
   
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_supplier',
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product_supplier'   =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default' => 0 ),
            'id_supplier'           =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
            'supplier_reference'    =>  array('type' => 'varchar', 'required' => false, 'size' => 10, 'default_null' => true ),
            'id_currency'           =>  array('type' => 'int', 'required' => false, 'size' => 10 ),
        ),
       
    );
    
    public static function getProductSupplierReference($id_product, $id_supplier, $id_shop, $id_product_attribute = 0)
    {
        
        if(!isset($id_product) 
                || !isset($id_product_attribute) 
                || !isset($id_supplier) 
                || empty($id_product) 
                || empty($id_product_attribute) 
                || empty($id_supplier))
            return NULL;
            
        $_db = ObjectModel::$db;
        
        $_db->select(array('ps.product_supplier_reference', 'ps.id_supplier'));
        $_db->from('product_supplier', 'ps');
        $_db->where( array('ps.id_product' => (int)$id_product, 
                    'ps.id_product_attribute' => (int)$id_product_attribute,
                    'ps.id_supplier' => (int)$id_supplier,
                    'ps.id_shop' => (int)$id_shop) 
                );

        $result = $_db->db_array_query($_db->query);
        $productSupplierReference = array();
        
        foreach ($result as $key => $values)
            $productSupplierReference[$values['id_supplier']]['supplier_reference'] = $values['product_supplier_reference'];

        return $productSupplierReference;
    }
    
    
}