<?php

class ProductCarrier extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_carrier',
        'fields'    => array(
            'id_product'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_carrier'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'price'         =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            
        ),
    );
    
}