<?php

class AttributeGroup extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'attribute_group',
        'primary'   => array('id_attribute_group'),
        'fields'    => array(
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'is_color_group'        =>  array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0' ),
            'group_type'            =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default' => 'select' ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_attribute_group'=>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
            ),
        ),
    );
    
}