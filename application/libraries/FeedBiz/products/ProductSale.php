<?php

class ProductSale extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(  
        'table'     => 'product_sale',
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
			'id_product_attribute'  =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'id_product_sale'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'date_from'         =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ),
            'date_to'           =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ),
            'reduction'         =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'reduction_type'    =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'price'             =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),

        ),
    );
    
    public static function getProductSaleByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $sales = array();
        
        $_db = ObjectModel::$db;
        
        $_db->from($_db->prefix_table.'product_sale');
        $_db->where(array('id_product' => (int)$id_product, 'id_shop' => (int)$id_shop));
	
         // check date range
        $_db->where(array('date_from' => date('Y-m-d')), '<=');
        $_db->where(array('date_to' => date('Y-m-d')), '>=');        
        
        $result = $_db->db_array_query($_db->query);
        
        $sale_no = 0;
         
        foreach ($result as $psale)
        {
            
            if(isset($psale['id_product_attribute']) && !empty($psale['id_product_attribute'])){
                $sale_no = $psale['id_product_attribute'];
                $sales[$sale_no]['id_product_attribute'] = $psale['id_product_attribute'];
            }
            
            $sales[$sale_no]['date_from'] = $psale['date_from'];
            $sales[$sale_no]['date_to'] = $psale['date_to'];
            $sales[$sale_no]['reduction'] = $psale['reduction'];
            $sales[$sale_no]['reduction_type'] = $psale['reduction_type'];
            $sales[$sale_no]['price'] = $psale['price'];
        }

        return $sales;
    }   
    
    public static function getOfferSaleByProductId($id_product, $id_shop, $offer_db)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $sales = array();
        
         if (isset($offer_db) && !empty($offer_db)) {
            $_db = $offer_db;
        } else {
            $_db = new Db(ObjectModel::$user, 'offers');
        }
        
        $_db->from('product_sale');
        $_db->where(array('id_product' => (int)$id_product, 'id_shop' => (int)$id_shop));
        $_db->where(array('date_from' => date('Y-m-d')), '<=');
        $_db->where(array('date_to' => date('Y-m-d')), '>=');  
        
        $result = $_db->db_array_query($_db->query);
        $sale_no = 0;
        foreach ($result as $psale)
        {
            if(isset($psale['id_product_attribute']) && !empty($psale['id_product_attribute'])){
                $sale_no = $psale['id_product_attribute'];
                $sales[$sale_no]['id_product_attribute'] = $psale['id_product_attribute'];
            }
            
            $sales[$sale_no]['date_from'] = $psale['date_from'];
            $sales[$sale_no]['date_to'] = $psale['date_to'];
            $sales[$sale_no]['reduction'] = $psale['reduction'];
            $sales[$sale_no]['reduction_type'] = $psale['reduction_type'];
            $sales[$sale_no]['price'] = $psale['price'];
        }

        return $sales;
    }  
}