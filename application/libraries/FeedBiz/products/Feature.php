<?php

class Feature extends ObjectModel
{
    public $id_feature;
    public $name;
    public $value;
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'feature',
        'primary'   => array('id_feature'),
        'fields'    => array(
            'id_feature'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_lang'   =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
        ),
        'table_link' => array( 
            '0' => array(
                'name' => 'feature_value', 
                'lang' => false 
            ),
        ),
    );
    
    public function __construct( $user, $id = null, $id_lang = null )
    {
        parent::__construct( $user, (int)$id, (int)$id_lang);
        
        if($id)
        {
            $feature_value = new FeatureValue($user);
            $this->value = $feature_value->getFeatureValuesById($id, $this->id_shop);
        }
    }
    
    public static function getFeaturesByProductId($id_product, $id_shop, $id_lang = null)
    {
        $iso_code = NULL;
        if(!isset($id_product) || empty($id_product))
            return NULL;

        $features = array();
        
        $_db = ObjectModel::$db;
        $columns = array('pf.id_feature', 'f.name', 'pf.id_feature_value', 'fv.id_lang', 'fv.value');
        $cols =$_db->put_array_tb_alias($columns);
        $_db->select($cols);
        $_db->from('product_feature pf');
        $_db->leftJoin('feature', 'f', '(pf.id_feature = f.id_feature) AND (pf.id_shop = f.id_shop)');
        $_db->leftJoin('feature_value', 'fv', '(pf.id_feature = fv.id_feature) AND (pf.id_feature_value = fv.id_feature_value) AND (pf.id_shop = fv.id_shop)');
        
        $arr_where['pf.id_product'] = (int)$id_product ;
        $arr_where['pf.id_shop'] = (int)$id_shop ;
        
        if(isset($id_lang)){
            $arr_where['fv.id_lang'] = (int)$id_lang;
            $arr_where['f.id_lang'] = (int)$id_lang;
        }
        
        $_db->where($arr_where);
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        
        foreach ($result as $feature)
        {
            $features[$feature['pf.id_feature']]['name'] = (string)$feature['f.name'];
            
            if(isset($lang[$feature['fv.id_lang']])) {
                $features[$feature['pf.id_feature']]['value'][ $lang[$feature['fv.id_lang']]['iso_code']] = (string)$feature['fv.value'];
            } else {
                $features[$feature['pf.id_feature']]['value'] = (string)$feature['fv.value'];
            }
            
            $features[$feature['pf.id_feature']]['id_value'] = (int)$feature['pf.id_feature_value'];
        }
        return ($features) ? $features : null;
    }
    
    public static function getFeatures($id_shop, $id_lang = null, $id_feature = null, $id_feature_value = null)
    {
        $features = array();
        $where = array();
        
        $_db = ObjectModel::$db;
        
        $_db->select(array('f.*', 'fv.*'));
        $_db->from('feature f');
        $_db->leftJoin('feature_value', 'fv', '(f.id_feature = fv.id_feature) AND (f.id_shop = f.id_shop)');

        $where['f.id_shop'] = (int)$id_shop;

        if(isset($id_lang)){
            $where['f.id_lang'] = (int)$id_lang;
            $where['fv.id_lang'] = (int)$id_lang;
        }
        if(isset($id_feature))
            $where['f.id_feature'] = (int)$id_feature;
        if(isset($id_feature_value))
            $where['fv.id_feature_value'] = (int)$id_feature_value;
        
        $_db->where($where);
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);

        foreach ($result as $feature)
        {
             
            $features[$feature['id_feature']]['name'] = (string)$feature['name'];

            foreach ($lang as $l)
                if($feature['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $features[$feature['id_feature']]['value'][$feature['id_feature_value']][$iso_code] = (string)$feature['value'];

        }
        
        return ($features) ? $features : null;
    }    
    
     //code needed for generating xml 
    public static function getFeaturesNames($id_shop, $id_lang = null, $id_feature = null, $id_feature_value = null) {
        $features = array();
        $where = array();

        $_db = ObjectModel::$db;

        $_db->select(array('f.*', 'fv.*'));
        $_db->from('feature f');
        $_db->leftJoin('feature_value', 'fv', '(f.id_feature = fv.id_feature) AND (f.id_shop = f.id_shop)');

        $where['f.id_shop'] = (int) $id_shop;

        if (isset($id_lang)) {
            $where['f.id_lang'] = (int) $id_lang;
            $where['fv.id_lang'] = (int) $id_lang;
        }
        if (isset($id_feature))
            $where['f.id_feature'] = (int) $id_feature;
        if (isset($id_feature_value))
            $where['fv.id_feature_value'] = (int) $id_feature_value;

        $_db->where($where);

        $result = $_db->db_array_query($_db->query);

        $lang = Language::getLanguages($id_shop);

        foreach ($result as $feature) {
            $features[$feature['id_feature']]['name'] = (string) $feature['name'];
        }

        return ($features) ? $features : null;
    }
    //end
    
}