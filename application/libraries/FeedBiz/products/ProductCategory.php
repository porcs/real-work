<?php

class ProductCategory extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_category',
        'fields'    => array(
            'id_product'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_category'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            
        ),
    );
    
}