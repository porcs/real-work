<?php

class Unit extends ObjectModel
{
    public $id_unit;
    public $type;
    public $name;
    static $arr_unit;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'unit',
        'primary'   => array('id_unit'),
        'fields'    => array(
            'id_unit'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'type'  =>  array('type' => 'varchar', 'required' => true, 'size' => 32 ),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
        ),
    );

    public function getUnits($id_shop)
    {
        $_db = ObjectModel::$db;
        
        $_db->from('unit');
        $_db->where(array('id_shop' => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);
        
        $units = null;
        foreach ($result as $value)
        {
            $units[$value['id_unit']]['type'] = $value['type'];
            $units[$value['id_unit']]['unit'] = $value['name'];
        }
        
        return $units;
    }

    public static function getUnitByType($id_shop, $type)
    {
        $_db = ObjectModel::$db;
	
        if(!isset(static::$arr_unit)){
	    
            $_db->from('unit');
            $_db->where(array('id_shop' => (int)$id_shop));
            $result = $_db->db_array_query($_db->query);
	    
            foreach($result as $k => $v){
                static::$arr_unit[$id_shop][$v['type']] =$v['name'];
            }
        }
	
        $units = null;
	
        if(isset(static::$arr_unit[$id_shop][$type])){	  
	    
            $units = static::$arr_unit[$id_shop][$type];
	    
        }else{
	    
            if(isset(static::$arr_unit[$id_shop])) {
		foreach(static::$arr_unit[$id_shop] as $type=>$name){
		    $units = $name;
		}
	    }
        }

        return $units;
    }
    
}