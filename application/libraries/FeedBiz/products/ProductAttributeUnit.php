<?php

class ProductAttributeUnit extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute_unit',
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_unit'               =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'name'                  =>  array('type' => 'varchar', 'required' => true, 'size' => 16 ),
            'value'                 =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            
        ),
    );
    
}