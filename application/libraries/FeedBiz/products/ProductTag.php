<?php

class ProductTag extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_tag',
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_product_tag'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
        ),
        'lang' => array(
            'fields'    => array(
                'id_product_tag'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
            ),
        ),
       
    );
  
    
}