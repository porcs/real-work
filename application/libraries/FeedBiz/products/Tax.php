<?php

class Tax extends ObjectModel
{
    public $id_tax;
    public $rate;
    public $type;
    public $name;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'tax',
        'primary'   => array('id_tax'),
        'fields'    => array(
            'id_tax'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'rate'  =>  array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000' ),
            'type'  =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
            'id_currency'  =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true ),
            
        ),
        'lang' => array(
            'fields'    => array(
                'id_tax'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'   =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            ),
        ),
        
    );
    
    public static function getTaxesRate($id_tax, $id_shop)
    {
        if(!isset($id_tax) || empty($id_tax))
            return NULL;
        
        $_db = ObjectModel::$db;
        $_db->from('tax');
        $_db->where( array("id_tax" => (int)$id_tax, "id_shop" => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        $rate = null;
        foreach ($result as $value)
            $rate = (float)$value['rate'];

        return $rate;
    }
    
    public static function getTaxesRateByProductId($id_product, $id_shop)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
        
        $_db = ObjectModel::$db;
        $_db->from('product p');
        $_db->leftJoin('tax', 't' , '(p.id_tax = t.id_tax) AND (p.id_shop = t.id_shop)');
        $_db->where( array("p.id_product" => (int)$id_product, "p.id_shop" => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        $rate = array();
        foreach ($result as $value)
            $rate[$value['id_tax']] = (float)$value['rate'];
        
        return $rate;
    }
    
    public static function getTaxes($id_shop)
    {
        $_db = ObjectModel::$db;
        
        $columns = array('t.rate', 't.type', 't.id_tax','tl.id_lang','tl.name');
        $cols =$_db->put_array_tb_alias($columns); 
        $_db->select($cols); 
        $_db->from('tax t');
        $_db->leftJoin('tax_lang', 'tl', '(t.id_tax = tl.id_tax) AND (t.id_shop = tl.id_shop)');
        $_db->where( array("t.id_shop" => (int)$id_shop) );

        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        
        $taxs = null;
        foreach ($result as $value)
        {
            $taxs[$value['t.id_tax']]['rate'] = (float)$value['t.rate'];
            $taxs[$value['t.id_tax']]['type'] = $value['t.type'];

            foreach ($lang as $l)
                if($value['tl.id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $taxs[$value['t.id_tax']]['name'][$iso_code] = $value['tl.name'];
        }
        
        return $taxs;
    }
    
}