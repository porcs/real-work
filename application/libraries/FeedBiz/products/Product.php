<?php

class Product extends ObjectModel {

    //Limit per page
    private $limit_page = 200;
    
    public $db_offer;
    public $id_product;
    public $name;
    public $description;
    public $description_short;
    public $tags;
    public $id_supplier;
    public $id_manufacturer;
    public $id_category_default;
    public $on_sale = false;
    public $reference;
    public $sku;
    public $ean13;
    public $upc;
    public $ecotax = 0;
    public $quantity = 0;
    public $price = 0;
    public $wholesale_price = 0;
    public $id_currency = 0;
    public $width = 0;
    public $height = 0;
    public $depth = 0;
    public $weight = 0;
    public $active = true;
    public $available_date = '0000-00-00';
    public $id_condition;
    public $id_tax;
    public $has_attribute;
    public $date_add;
    public $date_upd;
    public $supplier;
    public $manufacturer;
    public $category;
    public $currency;
    public $condition;
    public $tax;
    public $image;
    public $feature;
    public $carrier;
    public $combination;
    public $sale;
    public $id;    
    public $link;    
    public $date_created;    
    private $price_rules = null;
    private $garbage_enable = false;
    
    public static $definition = array(
        'table' => 'product',
        'primary' => array('id_product'),
        'fields' => array(
            'id_product' => array('type' => 'int', 'required' => true, 'size' => 10),
            'id_supplier' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_manufacturer' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_category_default' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_shop' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'on_sale' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'reference' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'old_reference' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'flag_update_ref' => array('type' => 'datetime', 'required' => false),
            'sku' => array('type' => 'varchar', 'required' => false, 'size' => 64),
            'ean13' => array('type' => 'varchar', 'required' => false, 'size' => 13),
            'upc' => array('type' => 'varchar', 'required' => false, 'size' => 12),
            'ecotax' => array('type' => 'decimal', 'required' => false, 'size' => '17,6', 'default' => '0.000000'),
            'quantity' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'price' => array('type' => 'decimal', 'required' => true, 'size' => '20,6', 'default' => '0.000000'),
            'wholesale_price' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'id_currency' => array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0'),
            'width' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'height' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'depth' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'weight' => array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000'),
            'active' => array('type' => 'tinyint', 'required' => true, 'size' => 1, 'default' => '0'),
            'available_date' => array('type' => 'date', 'required' => false),
            'id_condition' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
            'id_tax' => array('type' => 'int', 'required' => false, 'size' => 10),
            'has_attribute' => array('type' => 'tinyint', 'required' => false, 'size' => 1, 'default' => '0'),
            'date_add' => array('type' => 'datetime', 'required' => true),
            'date_upd' => array('type' => 'datetime', 'required' => false),
            'date_created' => array('type' => 'datetime', 'required' => false),
        ),
        'lang' => array(
            'fields' => array(
                'id_product' => array('type' => 'int', 'required' => true, 'size' => 10),
                'id_shop' => array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true),
                'id_lang' => array('type' => 'int', 'required' => true, 'size' => 10),
                'name' => array('type' => 'varchar', 'required' => true, 'size' => 128),
                'description' => array('type' => 'text', 'required' => false),
                'description_short' => array('type' => 'text', 'required' => false),
            ),
        ),
        'table_link' => array(
            '0' => array('name' => 'product_attribute', 'lang' => false),
            '1' => array('name' => 'product_attribute_combination', 'lang' => false),
            '2' => array('name' => 'product_attribute_image', 'lang' => false),
            '3' => array('name' => 'product_attribute_unit', 'lang' => false),
            '4' => array('name' => 'product_carrier', 'lang' => false),
            '5' => array('name' => 'product_category', 'lang' => false),
            '6' => array('name' => 'product_feature', 'lang' => false),
            '7' => array('name' => 'product_image', 'lang' => false),
            '8' => array('name' => 'product_sale', 'lang' => false),
            '9' => array('name' => 'product_supplier', 'lang' => false),
            '10' => array('name' => 'product_tag', 'lang' => true),
        ),
    );
   
    public function __construct($user, $id_product = null, $id_lang = null, $id_mode = null, $shop = null) {
	
        parent::__construct($user, $id_product, $id_lang, null, $shop);

        $this->db_offer = new Db(ObjectModel::$user, 'offers');
	$this->product_options = new MarketplaceProductOption($user);        

        if ($this->id) {
	    
            $offer = self::getProductOffer((int) $this->id, (int) $this->id_shop_default);
	    
	    $this->link = ProductUrl::getProductUrlByProductId((int) $this->id, (int) $this->id_shop_default); //2015-12-03	 
            $this->supplier = Supplier::getSupplierByProductId((int) $this->id, (int) $this->id_shop_default);
            $this->manufacturer = Manufacturer::getManufacturerByProductId((int) $this->id, (int) $this->id_shop_default);
            $this->tax = Tax::getTaxesRateByProductId((int) $this->id, (int) $this->id_shop_default);
            $this->tags = Tag::getProductTagsByProductId((int) $this->id, (int) $this->id_shop_default, (int) $id_lang);
            $this->image = Product::getImagesByProductId((int) $this->id, (int) $this->id_shop_default);
            $this->feature = Feature::getFeaturesByProductId((int) $this->id, (int) $this->id_shop_default, (int) $id_lang);
            $this->currency = Currency::getCurrencyById((int) $this->id_currency, (int) $this->id_shop_default);
            $this->carrier = Carrier::getCarriersByProductId((int) $this->id, (int) $this->id_shop_default);
            $this->condition = Conditions::getConditionNameByID((int) $this->id_condition, (int) $this->id_shop_default);
            $this->sale = ProductSale::getProductSaleByProductId((int) $this->id, (int) $this->id_shop_default);

            if (isset($offer['sale'])) {
                $this->sale = $offer['sale'];
            }

            if (isset($offer['price'])) {
                $this->price = $offer['price'];
            }

            /* wholesale_price ADD : 16/07/2015 */
            if (isset($offer['wholesale_price'])) {
                $this->wholesale_price = $offer['wholesale_price'];
            }

            $this->base_price = $this->price;

            if (isset($this->price) && $this->price != 0) {
                if (isset($price["price_overide"]) && !empty($price["price_overide"]) && $price["price_overide"] != 0) {
                    $this->price = $price["price_overide"];
                }
            }
            if (isset($offer['quantity'])) {
                $this->quantity = $offer['quantity'];
            }

            $unit_weight = Unit::getUnitByType($this->id_shop_default, 'Weight');
            $unit_dimension = Unit::getUnitByType($this->id_shop_default, 'Dimension');

            if ($this->weight > 0) {
                $weight['value'] = $this->weight;
                $weight['unit'] = $unit_weight;
                $this->weight = $weight;
            } else {
                $this->weight = array();
            }
            if ($this->width > 0) {
                $weight['value'] = $this->width;
                $weight['unit'] = $unit_dimension;
                $this->width = $weight;
            } else {
                $this->width = array();
            }
            if ($this->height > 0) {
                $weight['value'] = $this->height;
                $weight['unit'] = $unit_dimension;
                $this->height = $weight;
            } else {
                $this->height = array();
            }
            if ($this->depth > 0) {
                $weight['value'] = $this->depth;
                $weight['unit'] = $unit_dimension;
                $this->depth = $weight;
            } else {
                $this->depth = array();
            }
	    
	    // has combination
            if ($this->has_attribute) {

                $this->attribute_images[$this->id] = $this->getProductAttributeImages($this->id, $this->id_shop_default);

                $this->combination = $this->getProductCombinationByProductId($this->id, $this->id_shop_default, $this->db_offer, $this->id_category_default, $this->price, $this->id_manufacturer, $this->id_supplier, $id_lang);
                if (!isset($this->combination) || empty($this->combination)) {
                    unset($this->id_product);
                }
            } else {
                if ($this->check_rules($this->id_category_default, $this->price, $this->id_shop_default, $this->id_manufacturer, $this->id_supplier)) {
                    unset($this->id_product);
                    return false;
                }
                if (self::getProductOption($this->id_product, $this->id_shop_default, null, $id_lang) == false) {
                    unset($this->id_product);
                    return false;
                }
            }
                    
        }
    }

    public function getProductNameByID($id_product, $id_shop, $id_lang = null) {

        $products = array();
        $where = array();

        $_db = ObjectModel::$db;
        $_db->select(array('pl.name'));
        $_db->from('product p');
        $_db->leftJoin('product_lang', 'pl', 'p.id_product = pl.id_product AND p.id_shop = pl.id_shop');
        $where['p.id_shop'] = (int) $id_shop;
        $where['p.id_product'] = (int) $id_product;

        if (isset($id_lang) && $id_lang != NULL) {
            $where['pl.id_lang'] = (int) $id_lang;
        }

        $_db->where($where);
        $result = $_db->db_array_query($_db->query);
        foreach ($result as $product) {
            $products = $product['pl.name'];
        }

        return $products;
    }

    public function getProductBySKU($id_shop, $SKU) {
	
        if (!isset($id_shop) || empty($id_shop) || !isset($SKU) || empty($SKU)) {
            return;
        }

        $products = array();
        $_db = ObjectModel::$db;
        $sql = "SELECT 
                    p.id_product AS id_product,
                    p.id_category_default AS id_category_default,
                    pa.id_product_attribute AS id_product_attribute,
                    CASE 
                        WHEN pa.quantity IS NOT NULL THEN pa.quantity                         
                        WHEN p.quantity IS NOT NULL THEN p.quantity                        
                    END AS quantity,
                    CASE 
                        WHEN pa.reference IS NOT NULL THEN pa.reference 
                        WHEN pa.sku IS NOT NULL THEN pa.sku 
                        WHEN pa.ean13 IS NOT NULL THEN pa.ean13 
                        WHEN pa.upc IS NOT NULL THEN pa.upc 
                        WHEN p.reference IS NOT NULL THEN p.reference 
                        WHEN p.sku IS NOT NULL THEN p.sku 
                        WHEN p.ean13 IS NOT NULL THEN p.ean13 
                        WHEN p.upc IS NOT NULL THEN p.upc 
                    END AS sku
                FROM {$_db->prefix_table}product p 
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop 
                WHERE p.id_shop = " . $id_shop . " 
                AND 
                ( 
                    pa.reference = CASE WHEN pa.reference = '" . $SKU . "' THEN '" . $SKU . "' END 
                    OR p.reference = CASE WHEN p.reference = '" . $SKU . "' THEN '" . $SKU . "' END                     
                ) ; ";
      
        $result = $_db->db_query_str($sql);
        foreach ($result as $product) {
            $products['id_product'] = $product['id_product'];
            $products['id_product_attribute'] = $product['id_product_attribute'];
            $products['id_category_default'] = $product['id_category_default'];
            $products['quantity'] = $product['quantity'];
            $products['sku'] = $product['sku'];
        }

        return $products;
    }

    public function getProducts($id_shop, $is_active = true, $start = null, $limit = null, $order_by = null, $search = null, $num_row = null) {

        $offer = $active = null;
        $product_list = array();

        if ($is_active) {
            $active = "AND p.active = 1";
        }

        $_db = ObjectModel::$db;
        $sql = "SELECT 
                p.id_product as id_product, 
                pa.id_product_attribute as id_product_attribute,  
                p.reference as parent_sku,  
                CASE WHEN pa.reference IS NOT NULL THEN pa.reference ELSE p.reference END AS reference,
                CASE WHEN pa.quantity IS NOT NULL THEN pa.quantity ELSE p.quantity END AS quantity,
                CASE WHEN pa.price IS NOT NULL THEN pa.price ELSE p.price END AS price
                FROM {$_db->prefix_table}product p
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                WHERE p.id_shop = $id_shop $active ";

        if (isset($search) && !empty($search)) {
            $sql .= "AND ( "
                    . "p.id_product LIKE '%" . $search . "%' OR "
                    . "pa.id_product_attribute LIKE '%" . $search . "%' OR "
                    . "p.reference LIKE '%" . $search . "%' OR "
                    . "pa.reference LIKE '%" . $search . "%' "
                    . ") ";
        }

        if (isset($order_by) && !empty($order_by)) {
            $sql .= "ORDER BY $order_by ";
        } else {
            $sql .= "ORDER BY p.id_product ";
        }
        if (isset($start) && isset($limit)) {
            $sql .= "LIMIT $limit OFFSET $start ";
        }

        $sql .= " ; ";
       
        if (isset($num_row) && !empty($num_row)) {
	    
            $result = $_db->db_sqlit_query($sql);
            $row = $_db->db_num_rows($result);
            return $row;
	    
        } else {

            $result = $_db->db_query_str($sql);
            foreach ($result as $key => $data) {
                $product_list[$key]['id_product'] = $data['id_product'];
                $product_list[$key]['reference'] = $data['reference'];
                $product_list[$key]['quantity'] = $data['quantity'];
                $product_list[$key]['price'] = $data['price'];
                if (isset($data['id_product_attribute']) && !empty($data['id_product_attribute'])) {
                    $product_list[$key]['id_product_attribute'] = $data['id_product_attribute'];
                    $offer = $this->getProductCombinationOffer($data['id_product'], $data['id_product_attribute'], $id_shop);
                } else {
                    $offer = $this->getProductOffer($data['id_product'], $id_shop);
                }
                if (isset($offer) && !empty($offer)) {
                    $product_list[$key]['quantity'] = $offer['quantity'];
                    $product_list[$key]['price'] = $offer['price'];
                }
                if (!$is_active) {
                    $product_list[$key]['parent_sku'] = $data['parent_sku'];
                }
            }
            return $product_list;
        }
    }

    public function exportProducts($id_shop, $id_mode, $id_lang = null, $addtional = null) {

        $start = $limit = 1;
        $query = $category = $l = '';
        $product_list = $addtional_list = array();
        $_db = ObjectModel::$db;
        $_db->db_trans_begin();

        if (isset($addtional) && !empty($addtional)) {
	    
            foreach ($addtional as $key => $add) {
		
                switch ($key) {
                    case "category":
                        $category = isset($add) && !empty($add) ? ' AND p.id_category_default IN (' . implode(', ', $add) . ')' : '';
                        break;
                    case "manufacturer":
                        foreach ($add as $manu)
                            $manufacturer[] = $manu['id_manufacturer'];
                        break;
                    case "supplier":
                        foreach ($add as $supp)
                            $supplier[] = $supp['id_supplier'];
                        break;
                    case "carrier":
                        foreach ($add as $carr)
                            $carrier[] = $carr['id_carrier'];
                        break;
                    case "page":
                        $start_page = ($this->limit_page * ($add - $start));
                        $start = $start_page == 1 ? 0 : $start_page;
                        break;
                }
            }
        }

        $manufacturers = isset($manufacturer) && !empty($manufacturer) ? ' AND  p.id_manufacturer NOT IN (' . implode(', ', $manufacturer) . ')' : '';
        $suppliers = isset($supplier) && !empty($supplier) ? ' AND ps.id_supplier NOT IN (' . implode(', ', $supplier) . ')' : '';
        $carriers = isset($carrier) && !empty($carrier) ? ' AND pc.id_carrier IN (' . implode(', ', $carrier) . ')' : '';

        $query .= 'SELECT p.id_product as "p.id_product" '
		. $l . ', '
		. 'pc.id_carrier as "pc.id_carrier",  '
		. 'pcat.id_category as "pcat.id_category", '
		. 'p.id_manufacturer as "p.id_manufacturer",  '
		. 'ps.id_supplier as "ps.id_supplier", '
		. 'p.active as "p.active" ';
        $query .= 'FROM ' . $_db->prefix_table . 'product p ';
        $query .= 'LEFT JOIN ' . $_db->prefix_table . 'product_carrier pc ON p.id_product = pc.id_product AND p.id_shop = pc.id_shop
                LEFT JOIN ' . $_db->prefix_table . 'product_category pcat ON p.id_product = pcat.id_product 
		    AND p.id_category_default = pcat.id_category AND p.id_shop = pcat.id_shop
                LEFT JOIN ' . $_db->prefix_table . 'product_supplier ps ON p.id_product = ps.id_product AND p.id_shop = ps.id_shop ';
        $query .= ' WHERE p.id_shop = ' . $id_shop . ' ' . $carriers . $category . $manufacturers . $suppliers;
        $query .= ' GROUP BY p.id_product ORDER BY p.id_product ';
        $query .= ' LIMIT ' . $this->limit_page . ' OFFSET ' . $start . ' ;';

        $result = $_db->db_query_str($query);
        $_db->db_trans_commit();

        foreach ($result as $product) {
            $products = new Product(ObjectModel::$user, $product['p.id_product'], $id_lang, $id_mode, $id_shop);
            $product_list[] = $products;
        }

        return $product_list;
    }

    public function exportOfferArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
	
        $offerList = array();
        $offerQ = "SELECT p.id_currency, p.id_tax, p.id_product, p.quantity, p.price,p.wholesale_price , IF(pp.date_add > p.date_add, 1, 0) as product_newer "
		. " FROM {$this->db_offer->prefix_table}product p , {$this->db_product->prefix_table}product pp "
		. " WHERE p.id_product IN ('" . implode("', '", $productIDList) . "') and p.id_shop = '" . $id_shop . "' "
                . "  and pp.id_shop = p.id_shop and pp.id_product = p.id_product "
                ;
//        $e = new Exception;
//        $trace = $e->getTraceAsString();
//        file_put_contents('/var/www/backend/script/log/test_sql.txt',print_r(array(date('c'),$offerQ),true),FILE_APPEND);
        $result = $this->db_offer->db_query_str($offerQ);
	
        $skip=array();
        foreach ($result as $row) {
//            if($row['product_newer'] == 1){
////                $skip[] = array($row['id_product'] );
//                continue;
//            } 
            $offerList[$row['id_product']] = $row;
        }
//        global $argv;
//        file_put_contents('/var/www/backend/script/log/test_sql.txt',print_r(array('SKIP',$skip,$trace,$argv),true),FILE_APPEND);
        return $offerList;
    }

    public function exportCombinationOfferArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
	
        $combinationOfferList = array();
        $combinationOfferQ = "SELECT p.id_product, p.id_product_attribute, p.quantity, p.price, p.wholesale_price , IF(pp.date_add > p.date_add, 1, 0) as product_newer
			    FROM {$this->db_offer->prefix_table}product_attribute p , {$this->db_product->prefix_table}product_attribute pp
			    WHERE p.id_shop = '" . $id_shop . "' AND p.id_product IN ('" . implode("', '", $productIDList) . "') "
                                . " and pp.id_shop = p.id_shop and pp.id_product = p.id_product  and pp.id_product_attribute = p.id_product_attribute ";
        $result = $this->db_offer->db_query_str($combinationOfferQ);
//        $e = new Exception;
//        $trace = $e->getTraceAsString();
//	file_put_contents('/var/www/backend/script/log/test_sql.txt',print_r(array(date('c'),$combinationOfferQ),true),FILE_APPEND);
//        $skip=array();
        foreach ($result as $row) {
//            if($row['product_newer'] == 1){
////                $skip[] = array($row['id_product'].' '.$row['id_product_attribute']);
//                continue;
//            }
            $combinationOfferList[$row['id_product']][$row['id_product_attribute']] = array('quantity' => $row['quantity'], 'price' => $row['price']);
            if (isset($row['wholesale_price'])) {
                $combinationOfferList[$row['id_product']][$row['id_product_attribute']]['wholesale_price'] = $row['wholesale_price'];
            }
        }
//        global $argv;
//        file_put_contents('/var/www/backend/script/log/test_sql.txt',print_r(array('SKIP',$skip,$trace,$argv),true),FILE_APPEND);
        return $combinationOfferList;
    }

    public function exportManufacturerArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
        
        if(!isset($this->arr_manufacturer)){
            $this->arr_manufacturer = array();
            $this->arr_manufacturer_name = array();
        }
	
        if(!isset($this->arr_manufacturer[$id_shop])){
	    
            $this->arr_manufacturer[$id_shop] = array();
            $arr_manuQ = "select * from  {$this->db_product->prefix_table}manufacturer m where '{$id_shop}' = m.id_shop";
            $out = $this->db_product->db_query_str($arr_manuQ);
	    
            foreach($out as $v){
                $this->arr_manufacturer[$id_shop][$v['id_manufacturer']] = $v;
                $this->arr_manufacturer_name[$id_shop][$v['id_manufacturer']] = $v['name'];
            }
        } 
        
        if(!isset($this->arr_manufacturer_map)){
            $this->arr_manufacturer_map = array();
        }
	
        if(!isset($this->arr_manufacturer_map[$id_shop]) && !empty($this->arr_manufacturer_name)){
	    
            $this->arr_manufacturer_map[$id_shop] = array();
	    
            $arr_manuQ = "select * from  {$this->db_product->prefix_table}mapping_manufacturer mm "
	    . "where '{$id_shop}' = m.id_shop and mm.manufacturer in ('".implode(',',$this->arr_manufacturer_name[$id_shop])."')";
	    
            $out = $this->db_product->db_query_str($arr_manuQ);
            foreach($out as $v){
                $this->arr_manufacturer_map[$id_shop][$v['manufacturer']] = $v;
            }
        } 
        
        $manufacturerList = array();
        $manufacturerQ = "SELECT p.id_product as id_product,p.id_manufacturer 
			FROM {$this->db_product->prefix_table}product p 
			WHERE p.id_shop = '" . $id_shop . "' AND p.id_product IN ('" . implode("', '", $productIDList) . "')";
        $result = $this->db_product->db_query_str($manufacturerQ); 
	
        foreach ($result as $row) {
	    
            if (!empty($this->arr_manufacturer[$id_shop][$row['id_manufacturer']])) {
                $id_product = $row['id_product'];
                $name = isset($this->arr_manufacturer_name[$id_shop][$row['id_manufacturer']])?$this->arr_manufacturer_name[$id_shop][$row['id_manufacturer']]:'';                 
                $mapping = isset($this->arr_manufacturer_map[$id_shop][$name]['mapping'])?$this->arr_manufacturer_map[$id_shop][$name]['mapping']:$name;
                $manufacturerList[$id_product][$row['id_manufacturer']] = $mapping;
            }
	    
        } 
        if($this->garbage_enable){
            unset($this->arr_manufacturer,$this->arr_manufacturer_name,$this->arr_manufacturer_map);
        }
        return $manufacturerList;
    }

    public function exportSupplierArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
	
        if(!isset($this->arr_supplier)){
            $this->arr_supplier = array();
        }
	
        if(!isset($this->arr_supplier[$id_shop])){
            $this->arr_supplier[$id_shop] = array();
            $arr_suppQ = "select * from  {$this->db_product->prefix_table}supplier s where '{$id_shop}' = s.id_shop";
            $out = $this->db_product->db_query_str($arr_suppQ);
            foreach($out as $v){
                $this->arr_supplier[$id_shop][$v['id_supplier']] = $v;
            }
        } 
        
        $supplierList = array();
        $supplierQ = "SELECT ps.id_product as id_product, ps.id_supplier as id_supplier, ps.supplier_reference as supplier_reference
		    FROM {$this->db_product->prefix_table}product_supplier ps  
		    WHERE ps.id_shop = '" . $id_shop . "' AND ps.id_product IN ('" . implode("', '", $productIDList) . "')";
		    
        $result = $this->db_product->db_query_str($supplierQ);
        foreach ($result as $row) {
            $id_supplier = $row['id_supplier'];
            $id_product = $row['id_product'];
            $name = (isset($this->arr_supplier[$id_shop][$row['id_supplier']]['name']))?$this->arr_supplier[$id_shop][$row['id_supplier']]['name']:'';
            $supplierList[$id_product][$id_supplier]['name'] = $name;
            $supplierList[$id_product][$id_supplier]['supplier_reference'] = $row['supplier_reference'];
        }
        if($this->garbage_enable){
            unset($this->arr_supplier );
        }
        return $supplierList;
    }

    public function exportCurrencyArrayList($id_shop) {
	
        if ($id_shop == 0)
            return array();
	
        $currencyList = array();
	
        $currencyQ = "SELECT id_currency, name, iso_code
		    FROM {$this->db_product->prefix_table}currency
		    WHERE id_shop = '" . $id_shop . "'";
		    
        $result = $this->db_product->db_query_str($currencyQ);
	
        foreach ($result as $row) {
            $currencyList[$row['id_currency']]['name'] = $row['name'];
            $currencyList[$row['id_currency']]['iso_code'] = $row['iso_code'];
        }
        return $currencyList;
    }

    public function exportFeatureArrayList($productIDList, $id_shop, $id_lang) {
	
        if (empty($productIDList) || $id_shop == 0 || empty($id_lang))
            return array();
        
        $featureList = array();
        $lang = Language::getLanguages($id_shop);
	
        $featureQ = "SELECT pf.id_product as id_product, 
		    pf.id_feature as id_feature, 
		    f.name as name, 
		    pf.id_feature_value as id_feature_value, 
		    fv.id_lang as id_lang, 
		    fv.value as value
		    FROM {$this->db_product->prefix_table}product_feature pf
		    LEFT JOIN {$this->db_product->prefix_table}feature f ON pf.id_feature = f.id_feature AND pf.id_shop = f.id_shop
		    LEFT JOIN {$this->db_product->prefix_table}feature_value fv ON pf.id_feature = fv.id_feature 
			AND pf.id_feature_value = fv.id_feature_value AND pf.id_shop = fv.id_shop
		    WHERE pf.id_shop = '" . $id_shop . "' and fv.id_lang = '" . $id_lang . "' and f.id_lang = '" . $id_lang . "' 
			AND pf.id_product IN ('" . implode("', '", $productIDList) . "')";
		    
        $result = $this->db_product->db_query_str($featureQ);
	
        foreach ($result as $row) {
	    
            $id_product = $row['id_product'];
            $featureList[$id_product][$row['id_feature']]['name'] = (string) $row['name'];
	    
            if (isset($lang[$row['id_lang']])) {
                $featureList[$id_product][$row['id_feature']]['value'][$lang[$row['id_lang']]['iso_code']] = (string) $row['value'];
            } else {
                $featureList[$id_product][$row['id_feature']]['value'] = (string) $row['value'];
            }

            $featureList[$id_product][$row['id_feature']]['id_value'] = (int) $row['id_feature_value'];
        }
        return $featureList;
    }

    public function exportSaleArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
	
        $saleList = array();
	
	// SALE 
        $saleQ = "SELECT *  FROM {$this->db_product->prefix_table}product_sale
		WHERE id_shop = " . (int) $id_shop . " AND id_product IN ('" . implode("', '", $productIDList) . "') 
		and ((DATE(date_from) <= '" . date('Y-m-d') . "' and DATE(date_to) >= '" . date('Y-m-d') . "' )
                or ( DATE(date_from) = '0000-00-00 00:00:00' and DATE(date_to) = '0000-00-00 00:00:00')
                or ( DAY(date_from) = 0 and DAY(date_to) = 0 )
                ) ; ";
	
        $result = $this->db_product->db_query_str($saleQ);

        foreach ($result as $row) {
	    
            $sale_no = isset($row['id_product_attribute']) && !empty($row['id_product_attribute']) ? $row['id_product_attribute'] : 0;
            $id_product = $row['id_product'];

            $saleList[$id_product][$sale_no] = array('date_from' => $row['date_from'],
                'date_to' => $row['date_to'],
                'reduction' => $row['reduction'],
                'reduction_type' => $row['reduction_type'],
                'price' => $row['price'],'update_time'=>$row['date_add']);
        }
        unset($result);
	
        //OFFER SALE 
        $saleQ = "SELECT *  FROM {$this->db_offer->prefix_table}product_sale
		WHERE id_shop = '" . $id_shop . "' AND id_product IN ('" . implode("', '", $productIDList) . "') 
		and date_from <= '" . date('Y-m-d') . "' and date_to >= '" . date('Y-m-d') . "' ; ";
        $result = $this->db_offer->db_query_str($saleQ);

        foreach ($result as $row) {
	    
            $sale_no = isset($row['id_product_attribute']) && !empty($row['id_product_attribute']) ? $row['id_product_attribute'] : 0;
            $id_product = $row['id_product'];
            if(isset($saleList[$id_product][$sale_no]['update_time'])){
                $prod_time = $saleList[$id_product][$sale_no]['update_time'];
                $offer_time = $row['date_add'];
                if(strtotime($prod_time)>=strtotime($offer_time)){
                    continue;
                }
            }
            $saleList[$id_product][$sale_no] = array('date_from' => $row['date_from'],
                'date_to' => $row['date_to'],
                'reduction' => $row['reduction'],
                'reduction_type' => $row['reduction_type'],
                'price' => $row['price'],'update_time'=>$row['date_add']);
        }	
        unset($result);
	
        return $saleList;
    }

    public function exportTaxArrayList($id_shop) {
	
        if ($id_shop == 0)
            return array();
	
        if(isset($this->taxList))
	    return $this->taxList;
	
        $taxList = array();
        $taxQ = "SELECT * FROM {$this->db_product->prefix_table}tax WHERE id_shop = '" . $id_shop . "'";
        $result = $this->db_product->db_query_str($taxQ);
	
        foreach ($result as $row) {
            $taxList[$row['id_tax']] = (float) $row['rate'];
        }
        
        $this->taxList = $taxList;
        return $taxList;
    }
    
    public function exportProductCategoriesArrayList($productIDList, $id_shop, $show_name=false, $id_lang=null) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
        
        $productCategoriesList = array();
        $productCategoriesQ = "SELECT pc.id_product, pc.id_category " ;
        if($show_name){
            $productCategoriesQ .= ", cl.name, cl.id_lang, l.iso_code ";
        }
        $productCategoriesQ .= "FROM {$this->db_product->prefix_table}product_category pc " ;
        if($show_name){
            $productCategoriesQ .= "JOIN {$this->db_product->prefix_table}category_lang cl ON cl.id_category = pc.id_category AND cl.id_shop = pc.id_shop ";
            $productCategoriesQ .= "JOIN {$this->db_product->prefix_table}language l ON l.id_lang = cl.id_lang AND cl.id_shop = l.id_shop ";
        }
	$productCategoriesQ .="WHERE pc.id_shop = '" . $id_shop . "' AND pc.id_product IN ('" . implode("', '", $productIDList) . "') ";

        if($show_name && isset($id_lang)){
            $productCategoriesQ .= "AND cl.id_lang = ". (int)$id_lang. " ";
        }

        $result = $this->db_product->db_query_str($productCategoriesQ);
        
	if($show_name){
            foreach($result as $row){
                $productCategoriesList[$row['id_product']][$row['id_category']]['id_category'] = $row['id_category'];
                $productCategoriesList[$row['id_product']][$row['id_category']]['name'][$row['iso_code']] = $row['name'];
            }   
        } else {
            foreach($result as $row){
                $productCategoriesList[$row['id_product']][$row['id_category']] = $row['id_category'];
            }
        }
	
        return ($productCategoriesList);
    }
    
    public function exportCarrierArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
        
        if(!isset($this->arr_carrier)){
            $this->arr_carrier = array();
        }
	
        
        
        $carrierList = array();
        $carrierQ = "SELECT pc.id_product as id_product, pc.id_carrier as id_carrier, pc.price as price
		    FROM {$this->db_product->prefix_table}product_carrier pc
		    WHERE pc.id_shop = '" . $id_shop . "' AND pc.id_product IN ('" . implode("', '", $productIDList) . "')"; 

        $result = $this->db_product->db_query_str($carrierQ);
        $carrier_id_list=array();
        foreach($result as $row){
            $carrier_id_list[$row['id_carrier']] = $row['id_carrier'];
        }
        if(!empty($carrier_id_list)){
                    
                $this->arr_carrier[$id_shop] = array();
                $arr_carrierQ = "select * from  {$this->db_product->prefix_table}carrier c where '{$id_shop}' = c.id_shop and c.id_carrier in (".implode(',',$carrier_id_list).")";
                $out = $this->db_product->db_query_str($arr_carrierQ);
                foreach($out as $v){
                    $this->arr_carrier[$id_shop][$v['id_carrier']] = $v;
                } 
        }
        foreach ($result as $row) {
	    
            $name = isset($this->arr_carrier[$id_shop][$row['id_carrier']]['name']) ? $this->arr_carrier[$id_shop][$row['id_carrier']]['name']:'';
            $id_tax = isset($this->arr_carrier[$id_shop][$row['id_carrier']]['id_tax']) ? $this->arr_carrier[$id_shop][$row['id_carrier']]['id_tax']:null; 
            $id_product = $row['id_product'];
            $carrierList[$id_product][$row['id_carrier']]['name'] = $name;
            $carrierList[$id_product][$row['id_carrier']]['price'] = (float) $row['price'];
            $carrierList[$id_product][$row['id_carrier']]['tax'][$id_tax] = isset($this->taxList[$id_tax])?$this->taxList[$id_tax]:null;
	    
        }
        if($this->garbage_enable){
            unset($this->arr_carrier );
        }
	
        return $carrierList;
    }

    public function exportImageArrayList($productIDList, $id_shop) {
	
        if (empty($productIDList) || $id_shop == 0)
            return array();
	
        if(isset($this->arr_product_image))
	    return $this->arr_product_image;
	
        $imageList = array();
        $imageQ = "SELECT id_product, id_image, image_type, image_url
		    FROM {$this->db_product->prefix_table}product_image 
		    WHERE id_shop = '" . $id_shop . "' ; ";
		    
        $result = $this->db_product->db_query_str($imageQ);
	
        foreach ($result as $row) {
	    
            $id_product = $row['id_product'];
            $id_image = $row['id_image'];

            $image = array('image_type' => $row['image_type']);
            if (isset($row['image_url'])) {
                $image['image_url'] = $row['image_url'];
            }
            $imageList[$id_product][$id_image] = $image;
        }
	
        $this->arr_product_image = $imageList;
        if($this->garbage_enable){
            unset($this->arr_product_image );
        }
        return $imageList;
    }

    public function exportLanguageArrayList($productIDList, $id_shop) {
	
        $productLangList = array();
        $productLangQ = "SELECT l.iso_code as iso_code, pl.id_product as id_product, 
			pl.name as name, pl.description as description, pl.description_short as description_short 
			FROM {$this->db_product->prefix_table}product_lang pl
			JOIN {$this->db_product->prefix_table}language l ON l.id_lang = pl.id_lang and pl.id_shop=l.id_shop
			WHERE l.id_shop = '" . $id_shop . "'  AND  pl.id_product IN ('" . implode("', '", $productIDList) . "')
			ORDER BY pl.id_lang ; ";
			
        $result = $this->db_product->db_query_str($productLangQ);
	
        foreach ($result as $row) {
            $id_product = $row['id_product'];
            $productLangList[$id_product]['name'][$row['iso_code']] = $row['name'];
            $productLangList[$id_product]['description'][$row['iso_code']] = htmlspecialchars_decode($row['description']);
            $productLangList[$id_product]['description_short'][$row['iso_code']] = htmlspecialchars_decode($row['description_short']);
        }
	
        return $productLangList;
	
    }
    
    public function exportUrlArrayList($productIDList, $id_shop) {
	
        $productUrlList = array(); 
        $productUrlQ = "SELECT id_product, link 
			FROM {$this->db_product->prefix_table}product_url 			
			WHERE id_shop = " . (int)$id_shop . "  AND id_product IN ('" . implode("', '", $productIDList) . "') ; ";
			
        $result = $this->db_product->db_query_str($productUrlQ);
	
        foreach ($result as $row) {
            $id_product = $row['id_product'];
            $productUrlList[$id_product]['link'] = $row['link'];
        }
	
        return $productUrlList;
	
    }

    public function exportAttributeUrlArrayList($productIDList, $id_shop) {

        $productUrlList = array();
        $productUrlQ = "SELECT id_product,id_product_attribute , link
			FROM {$this->db_product->prefix_table}product_attribute_url
			WHERE id_shop = " . (int)$id_shop . "  AND id_product IN ('" . implode("', '", $productIDList) . "') ; ";

        $result = $this->db_product->db_query_str($productUrlQ);

        foreach ($result as $row) {
            $id_product = $row['id_product'];
            $id_product_attribute = $row['id_product_attribute'];
            $productUrlList[$id_product][$id_product_attribute]['link'] = $row['link'];
        }

        return $productUrlList;

    }
    
    public function exportCombImageArrayList($combIDList,$comb_w_product, $id_shop) {
	
	$combImageQ = "select pai.id_image as id_image ,pai.id_product_attribute "
		. "from {$this->db_product->prefix_table}product_attribute_image pai "
		. "where pai.id_product_attribute IN ('" . implode("', '", $combIDList) . "') AND pai.id_shop = '{$id_shop}' ; ";		
	$result = $this->db_product->db_query_str($combImageQ);  

	$arr_img_id = array();
	foreach($result as $v){
	    
	    if(!isset($comb_w_product[$v['id_product_attribute']])) 
		continue;
	    
	    $id_product = $comb_w_product[$v['id_product_attribute']];
	    $arr_img_id["('{$id_product}','{$v['id_image']}')"] = "('{$id_product}','{$v['id_image']}')";
	}

        $img_atr_list = $result;
                
        $combImageQ = "select  pi.image_url as url, pi.image_type as type ,pi.id_image,pi.id_product "
		. "from {$this->db_product->prefix_table}product_image pi "
		. "where (pi.id_product ,pi.id_image) IN (" . implode(", ", $arr_img_id) . ") AND pi.id_shop = '{$id_shop}'";        
        $result = $this->db_product->db_query_str($combImageQ);
	
        $img_list = array();	
        foreach($result as $v){
	    
            $img_list[$v['id_product']][$v['id_image']] = $v;
	    
        }
        
        $combImageList = array();
        
        foreach ($img_atr_list as $row) {
	    
            $id_comb_product = $row['id_product_attribute'];
            $id_product = $comb_w_product[$row['id_product_attribute']];
            $id_image = $row['id_image'];
	    
            if(!isset($img_list[$id_product][$id_image])) 
		continue;
	    
            $combImageList[$id_comb_product][$id_image]['url'] = isset($img_list[$id_product][$id_image]['url'])?$img_list[$id_product][$id_image]['url']:null;
            $combImageList[$id_comb_product][$id_image]['type'] = isset($img_list[$id_product][$id_image]['type'])?$img_list[$id_product][$id_image]['type']:null;
            
        }  
        return $combImageList;
    }
    
    public function exportCombLangArrayList($combIDList,$productIDList, $id_shop) {
	
        $pacQ = "select * from {$this->db_product->prefix_table}product_attribute_combination pac"
        . " where pac.id_product_attribute  IN ('" . implode("', '", $combIDList) . "') AND pac.id_shop = '{$id_shop}' ; ";
        $result = $this->db_product->db_query_str($pacQ);
        
        $pacList = $pacIDList = $pacMacthList = array();	
        foreach($result as $v){
            $pacIDList[$v['id_attribute_group']] = $v['id_attribute_group']; 
            $pacMacthList["('{$v['id_attribute_group']}','{$v['id_attribute']}')"]  = "('{$v['id_attribute_group']}','{$v['id_attribute']}')";
        }	
        $pacList = $result; 
        
        $langQ = "select l.id_lang, l.iso_code as iso_code  from {$this->db_product->prefix_table}language l  where l.id_shop  = '{$id_shop}' ; "; 
        $result = $this->db_product->db_query_str($langQ);
        
        $langList = array();
        $langIDList = array();
        foreach($result as $v){
            $langList[$v['id_lang']] = $v;
            $langIDList[$v['id_lang']] = $v['id_lang'];
        }
        
        $aglQ = "select agl.name as agl_name , agl.id_attribute_group ,agl.id_lang from {$this->db_product->prefix_table}attribute_group_lang agl "
        . " where  agl.id_attribute_group in ('" . implode("', '", $pacIDList) . "')  AND  agl.id_shop  = '{$id_shop}' "; 
        $result = $this->db_product->db_query_str($aglQ);
        
        $aglList = array();
        foreach($result as $v){
            $aglList[$v['id_attribute_group']][$v['id_lang']] = $v;
        }
	
        $aQ = "select  a.id_attribute_group as id_attribute_group,a.id_attribute as id_attribute,a.id_lang, a.name as a_name "
	. "from {$this->db_product->prefix_table}attribute_lang a "
        . " where   (a.id_attribute_group ,a.id_attribute) in (".implode(", ", $pacMacthList).")   AND   a.id_shop  = '{$id_shop}' "
        . " and a.id_lang in ('".implode("', '", $langIDList)."') "; 
        $result = $this->db_product->db_query_str($aQ);
                    
        $aList = array();
        foreach($result as $v){
            $aList[$v['id_attribute_group']][$v['id_attribute']][$v['id_lang']] = $v;
        }
        
        $combLangList = array();
        
        foreach ($pacList as $row) {
	    
            $id_comb_product = $row['id_product_attribute'];
            $id_attribute = $row['id_attribute'];
            $id_attribute_group= $row['id_attribute_group']; 
	    
            if(isset($aList[$id_attribute_group][$id_attribute]) && is_array($aList[$id_attribute_group][$id_attribute])){
		
                foreach($aList[$id_attribute_group][$id_attribute] as $id_lang=>$row_lang){
		    
                    $iso_code = isset($langList[$id_lang]['iso_code']) ? $langList[$id_lang]['iso_code'] : 'en';
		    
                    $combLangList[$id_comb_product][$id_attribute_group]['name'][$iso_code] = 
			    isset($aglList[$id_attribute_group][$id_lang]['agl_name'])?$aglList[$id_attribute_group][$id_lang]['agl_name']:'';
		    
                    $combLangList[$id_comb_product][$id_attribute_group]['value'][$id_attribute]['name'][$iso_code] = 
			    isset($aList[$id_attribute_group][$id_attribute][$id_lang]['a_name'])?$aList[$id_attribute_group][$id_attribute][$id_lang]['a_name']:'';
		    
                }
            }
        } 
                    
        return $combLangList;
    }
    
    public function exportProductsArray($id_shop, $id_mode, $id_lang, $addtional = null, $id_products = array()) {
	
        $start = null;
        $query = $category = '';
        $product_list = $addtional_list = array();
        $_db = ObjectModel::$db;
        $this->db_product = ObjectModel::$db;
	
	$get_detail_product_category = $get_detail_product_category_name = false; // Praew added : to get all product category detail ; 8/3/2015
		
        if (isset($addtional) && !empty($addtional)) {
	    
            foreach ($addtional as $key => $add) {
		
                switch ($key) {
		    
                    case "category":
                        $category = isset($add) && !empty($add) ? ' AND p.id_category_default  IN (' . implode(', ', $add) . ')' : '';
                        break;
                    case "manufacturer":
                        foreach ($add as $manu)
                            $manufacturer[] = $manu['id_manufacturer'];
                        break;
                    case "supplier":
                        foreach ($add as $supp)
                            $supplier[] = $supp['id_supplier'];
                        break;
//                    case "carrier":
//                        foreach ($add as $carr)
//                            $carrier[] = $carr['id_carrier'];
//                        break;
                    case "page":
                        if(!empty($add) && (int)$add!=0) {
			    $start = 1;
			    $start_page = ($this->limit_page * ($add - $start));
			    $start = $start_page == 1 ? 0 : $start_page;
			}
                        break;
		    case "product_category": // Praew added : 8/3/2015
			if(isset($add) && $add){
			    $get_detail_product_category = true;
                            if(isset($addtional['product_category_name'])){ // Praew added : 15/07/2016 // to export category name
                                $get_detail_product_category_name = true;
                            }
			}
			break;
                }
            }
        }

        $manufacturers = isset($manufacturer) && !empty($manufacturer) ? ' AND  p.id_manufacturer NOT IN (' . implode(', ', $manufacturer) . ') ' : '';
        $suppliers = isset($supplier) && !empty($supplier) ? ' AND ps.id_supplier NOT IN (' . implode(', ', $supplier) . ') ' : '';
        $carriers = isset($carrier) && !empty($carrier) ? ' AND pc.id_carrier IN (' . implode(', ', $carrier) . ') ' : '';
        $prod_add = '';
        
        if(!empty($id_products)){
            if(is_array($id_products)){
                $prod_add=" and p.id_product in (".implode(',',$id_products).") ";
            }elseif(is_numeric($id_products)){
                $prod_add=" and p.id_product = $id_products ";
            }
        }else{
            $this->garbage_enable = true;
        }
        
        $productIDList = array();
	
        $query .= 'SELECT  p.id_product,p.price ,p.id_category_default ,p.id_manufacturer,p.id_supplier FROM ' . $_db->prefix_table . 'product p ' ;
	$query .= 'left join ' . $_db->prefix_table . 'product_lang pl on pl.id_shop = p.id_shop and pl.id_product = p.id_product ';
	
        if(!empty($carriers)) {
	    $query .= 'LEFT JOIN '.$_db->prefix_table.'product_carrier pc ON pc.id_product = p.id_product AND pc.id_shop = p.id_shop ';
	}
	
        if(!empty($category)) {
	    $query .= 'LEFT JOIN '.$_db->prefix_table.'product_category pcat ON pcat.id_product = p.id_product  and pcat.id_category = p.id_category_default
		     AND pcat.id_shop = p.id_shop ';
	}
	
        if(!empty($suppliers)) {
	    $query .= 'LEFT JOIN ' . $_db->prefix_table . 'product_supplier ps ON ps.id_product = p.id_product AND ps.id_shop = p.id_shop ';
	}
	
        $query .= 'WHERE p.id_shop = '.$id_shop.' '.$prod_add . $carriers . $category . $manufacturers . $suppliers;
        $query .= 'GROUP BY p.id_product ORDER BY p.id_product ';
	
        if (isset($start)) {
            $query .= ' LIMIT ' . $this->limit_page . ' OFFSET ' . $start;
        }                    
        $result = $_db->db_query_str($query);
        $arr_product = array();
	
        foreach ($result as $product) {
	    
            $productIDList[] = $product['id_product'];
            $arr_product[$product['id_product']] = $product;
	    
        }
        if(!empty($addtional['page']) && (int)$addtional['page']!=0){
            $this->garbage_enable = false;
        }
        if(sizeof($result)>500){
            $this->garbage_enable = true; 
        }
        unset($result);
                    
        if (!empty($productIDList)) {
	    
            //OFFER LIST 
            $offerList = $this->exportOfferArrayList($productIDList, $id_shop);
            
            //COMBINATION OFFER 
            $combinationOfferList = $this->exportCombinationOfferArrayList($productIDList, $id_shop);

            //MANUFACTURER LIST 
            $manufacturerList = $this->exportManufacturerArrayList($productIDList, $id_shop);

            //SUPPLIER LIST 
            $supplierList = $this->exportSupplierArrayList($productIDList, $id_shop);

            //UNIT
            $unit_weight = Unit::getUnitByType($id_shop, 'Weight');
            $unit_dimension = Unit::getUnitByType($id_shop, 'Dimension');

            //CURRENCY 
            $currencyList = $this->exportCurrencyArrayList($id_shop);

            //FEATURE 
            $featureList = $this->exportFeatureArrayList($productIDList, $id_shop, $id_lang);

            //PRODUCT SALE 
            $saleList = $this->exportSaleArrayList($productIDList, $id_shop);

            //TAX 
            //$taxList = $this->exportTaxArrayList($id_shop);

            //CARRIER 
            $carrierList = $this->exportCarrierArrayList($productIDList, $id_shop);

            //IMAGE 
            $imageList = $this->exportImageArrayList($productIDList, $id_shop);
	    
	    //PRODUCT CATEGORY // 08/03/2016
	    if($get_detail_product_category){
		$productCategoriesList = $this->exportProductCategoriesArrayList($productIDList, $id_shop, $get_detail_product_category_name, $id_lang);
	    }
	    
            //COMBINATION 
            $combinationList = array();
            $combinationQ = "SELECT pa.id_product as id_product,
			    pa.id_product_attribute as id_product_attribute,
			    pa.reference as reference, pa.wholesale_price as wholesale_price ,
                            pa.old_reference as old_reference,pa.flag_update_ref as flag_update_ref,
			    pa.ean13 as ean13,
			    pa.sku as sku,
			    pa.upc as upc,
			    pa.price as price,
			    pa.price_type as price_type,
			    pa.quantity as quantity,
			    pa.weight as weight,
			    pa.available_date as available_date
			    FROM {$_db->prefix_table}product_attribute pa
			    WHERE pa.id_product IN ('" . implode("', '", $productIDList) . "') AND pa.id_shop = '" . $id_shop . "'  ";
            
            $result = $_db->db_query_str($combinationQ);
            if (isset($result) && !empty($result)) {
                //Product Attribute URL
		$productAttributeUrlList = $this->exportAttributeUrlArrayList($productIDList, $id_shop);
                $comb_id_list = array();
		
                foreach($result as $v){
                    $comb_id_list[$v['id_product_attribute']] = $v['id_product_attribute'];
                    $comb_w_product[$v['id_product_attribute']] = $v['id_product'];
                } 
		
                if(!empty($comb_id_list)){
		    $combImageList= $this->exportCombImageArrayList($comb_id_list,$comb_w_product, $id_shop);
		    $combLangList = $this->exportCombLangArrayList($comb_id_list,$productIDList, $id_shop);
                }
                
                foreach ($result as $row) {
                    
                    $id_product = $row['id_product'];
		    
                    if(!isset($arr_product[$id_product]))
			continue;
		    
                    $prod = $arr_product[$id_product];
                    $product_price = $prod['price']; 
                    $id_category_default = $prod['id_category_default'];  
                    $id_manufacturer = $prod['id_manufacturer']; 
                    $id_supplier = $prod['id_supplier'];                     
                    $id_product_attribute = $row['id_product_attribute'];
                    $price = $row['price'];
                    $quantity = $row['quantity'];
                    $wholesale_price = isset($row['wholesale_price']) ? $row['wholesale_price'] : null;
		    
                    $combination_offer = isset($combinationOfferList[$id_product]) && isset($combinationOfferList[$id_product][$id_product_attribute]) ? 
					    $combinationOfferList[$id_product][$id_product_attribute] : array();

                    if (isset($combination_offer['price'])) {
                        $price = $combination_offer['price'];
                    }
                    if (isset($combination_offer['wholesale_price'])) {
                        $wholesale_price = $combination_offer['wholesale_price'];
                    }

                    // Check rule
                    $products = $product_price + $price;

                    if ($this->check_rules_cache($id_category_default, $products, $id_shop, $id_manufacturer, $id_supplier)) {
                        continue;
                    }

                    if (isset($combination_offer['price'])) {
                        $combinationList[$id_product][$row['id_product_attribute']]['price_original'] = $combination_offer['price'];
                    }

                    if (isset($combination_offer['quantity'])) {
                        $quantity = $combination_offer['quantity'];
                    }

                    $combinationList[$id_product][$row['id_product_attribute']]['id_product_attribute'] = $row['id_product_attribute'];
                    $combinationList[$id_product][$row['id_product_attribute']]['reference'] = $row['reference'];
                    $combinationList[$id_product][$row['id_product_attribute']]['old_reference'] = $row['old_reference'];
                    $combinationList[$id_product][$row['id_product_attribute']]['flag_update_ref'] = $row['flag_update_ref'];
                    $combinationList[$id_product][$row['id_product_attribute']]['ean13'] = $row['ean13'];
                    $combinationList[$id_product][$row['id_product_attribute']]['sku'] = $row['sku'];
                    $combinationList[$id_product][$row['id_product_attribute']]['upc'] = $row['upc'];
                    $combinationList[$id_product][$row['id_product_attribute']]['price'] = $price;
                    $combinationList[$id_product][$row['id_product_attribute']]['price_type'] = $row['price_type'];
                    $combinationList[$id_product][$row['id_product_attribute']]['quantity'] = $quantity;
                    $combinationList[$id_product][$row['id_product_attribute']]['wholesale_price'] = $wholesale_price;
                    $combinationList[$id_product][$row['id_product_attribute']]['link'] = 
                        isset($productAttributeUrlList[$id_product][$row['id_product_attribute']]['link'])
                        ? $productAttributeUrlList[$id_product][$row['id_product_attribute']]['link'] : '';//2016-10-11

                    if ($row['weight'] > 0) {
                        $weight['value'] = $row['weight'];
                        $weight['unit'] = Unit::getUnitByType($id_shop, 'Weight');
                        $combinationList[$id_product][$row['id_product_attribute']]['weight'] = $weight;
                    } else {
                        $combinationList[$id_product][$row['id_product_attribute']]['weight'] = array();
                    }

                    $combinationList[$id_product][$row['id_product_attribute']]['available_date'] = $row['available_date'];
                    
                    if(isset($combImageList[$row['id_product_attribute']]) ){
                        $combinationList[$id_product][$row['id_product_attribute']]['images'] = $combImageList[$row['id_product_attribute']];
                    }
                    
                    if(isset($combLangList[$row['id_product_attribute']])){
                        $combinationList[$id_product][$row['id_product_attribute']]['attributes'] = $combLangList[$row['id_product_attribute']];
                    }
                    
                }
                if(!isset($combLangList)){
                    unset($combLangList,$combImageList);
                }
                if(isset($combination_offer)){
                    unset($combination_offer);
                }
                unset($combinationOfferList);
            } 
            unset($result);
           
            //PRODUCT LANG 
            $productLangList = $this->exportLanguageArrayList($productIDList, $id_shop);
            
	    //PRODUCT Url 
            $productUrlList = $this->exportUrlArrayList($productIDList, $id_shop);
	    
            //PRODUCT
            $productQ = "SELECT p.id_product as id_product, p.sku as sku, p.upc as upc, p.ean13 as ean13, p.reference as reference,
                        p.old_reference as old_reference,p.flag_update_ref as flag_update_ref,
			p.quantity as quantity, p.active as active, p.on_sale as on_sale, p.id_category_default as id_category_default, 
			p.has_attribute as has_attribute, p.id_condition as id_condition,
			p.price as price, p.id_currency as  id_currency,p.id_manufacturer as id_manufacturer,p.id_supplier as id_supplier,
                        p.width as width, p.height as height, p.depth as depth, p.weight as weight,

                        p.wholesale_price as wholesale_price,p.has_attribute as has_attribute,p.available_date as available_date,p.date_created 
			FROM {$_db->prefix_table}product p
			WHERE p.id_product IN ('" . implode("', '", $productIDList) . "') and p.id_shop = '{$id_shop}' 
			ORDER BY p.id_product";
            $result = $_db->db_query_str($productQ);

            foreach ($result as $row) {

                $id_product = $row['id_product'];
		
                settype($row['width'], "float");
                settype($row['height'], "float");
                settype($row['depth'], "float");
                settype($row['weight'], "float");
		
                $id_currency = isset($offerList[$id_product]) &&
                        isset($offerList[$id_product]['id_currency']) && !empty($offerList[$id_product]['id_currency']) &&
                        isset($currencyList[$offerList[$id_product]['id_currency']]) ?
                        $offerList[$id_product]['id_currency'] : $row['id_currency'];
		
                $wholesale_price = isset($offerList[$id_product]) &&
                        isset($offerList[$id_product]['wholesale_price']) && !empty($offerList[$id_product]['wholesale_price']) ?
                        $offerList[$id_product]['wholesale_price'] : $row['wholesale_price'];

                $product_list[$id_product] = array(
                    'name' => isset($productLangList[$id_product]['name']) ? $productLangList[$id_product]['name'] : array(),
                    'description' => isset($productLangList[$id_product]['description']) ? $productLangList[$id_product]['description'] : array(),
                    'description_short' => isset($productLangList[$id_product]['description_short']) ? $productLangList[$id_product]['description_short'] : array(),
		    'link' => isset($productUrlList[$id_product]['link']) ? $productUrlList[$id_product]['link'] : '', //2015-12-03,
                    'id_product' => $row['id_product'],
                    'sku' => $row['sku'],
                    'upc' => $row['upc'],
                    'ean13' => $row['ean13'],
                    'reference' => $row['reference'],
                    'old_reference' => $row['old_reference'],
                    'flag_update_ref' => $row['flag_update_ref'],
                    'active' => $row['active'],
                    'on_sale' => $row['on_sale'],
                    'id_condition' => $row['id_condition'],
                    'id_category_default' => $row['id_category_default'],
                    'base_price' => $row['price'],
                    'quantity' => isset($offerList[$id_product]) ? $offerList[$id_product]['quantity'] : $row['quantity'],
                    'price' => isset($offerList[$id_product]) ? $offerList[$id_product]['price'] : $row['price'],		   
                    'manufacturer' => isset($manufacturerList[$id_product]) ? $manufacturerList[$id_product] : array(),
                    'supplier' => isset($supplierList[$id_product]) ? $supplierList[$id_product] : null,
                    'width' => $row['width'] > 0 ? array('value' => $row['width'], 'unit' => $unit_dimension) : array(),
                    'height' => $row['height'] > 0 ? array('value' => $row['height'], 'unit' => $unit_dimension) : array(),
                    'depth' => $row['depth'] > 0 ? array('value' => $row['depth'], 'unit' => $unit_dimension) : array(),
                    'weight' => $row['weight'] > 0 ? array('value' => $row['weight'], 'unit' => $unit_weight) : array(),
                    'currency' => array($id_currency => $currencyList[$id_currency]),
                    'feature' => isset($featureList[$id_product]) ? $featureList[$id_product] : null,
                    'sale' => isset($saleList[$id_product]) ? $saleList[$id_product] : array(),
                    'carrier' => isset($carrierList[$id_product]) ? $carrierList[$id_product] : array(),
                    'image' => isset($imageList[$id_product]) ? $imageList[$id_product] : array(),
                    'combination' => isset($combinationList[$id_product]) ? $combinationList[$id_product] : null,
                    'has_attribute' => $row['has_attribute'],
                    'wholesale_price' => $wholesale_price,
                    'available_date' => $row['available_date'],
                    'id_manufacturer' => $row['id_manufacturer'],
                    'id_supplier' => $row['id_supplier'],
                    'date_created' => $row['date_created']
                );
		
		if($get_detail_product_category){
		    $product_list[$id_product]['category'] = isset($productCategoriesList[$id_product]) ? $productCategoriesList[$id_product] : array();
		}
		    
                $offerList[$id_product]=$manufacturerList[$id_product]
                        =$supplierList[$id_product]=$featureList[$id_product]
                        =$saleList[$id_product]=$carrierList[$id_product]
                        =$imageList[$id_product] =$productLangList[$id_product]
                        =$productUrlList[$id_product]=null;
            }
            unset($result);

            //PRODUCT OPTIONS
            $productOptions = $this->product_options->get_product_option_by_ids($productIDList, $id_shop, $id_lang);
	    
            foreach ($product_list as $product_key => $product_element) {
		
                $id_product = $product_element['id_product'];

                if (isset($productOptions[$id_product]) && isset($productOptions[$id_product][0])) {
                    if (isset($productOptions[$id_product][0]['disable']) && !empty($productOptions[$id_product][0]['disable'])) {
//                        unset($product_list[$product_key]);
//                        continue;
                        $product_list[$product_key]['disable'] = $productOptions[$id_product][0]['disable'];
                    }
                    if (isset($productOptions[$id_product][0]['force']) && !empty($productOptions[$id_product][0]['force'])) {
                        $product_list[$product_key]['force'] = $productOptions[$id_product][0]['force'];
                    }
                    if (isset($productOptions[$id_product][0]['price']) && !empty($productOptions[$id_product][0]['price']) && empty($product_element['combination'])) {
                        $product_list[$product_key]['original_price'] = isset($product_list[$product_key]['price'])?$product_list[$product_key]['price']:0;
                        $product_list[$product_key]['price'] = $productOptions[$id_product][0]['price'];
                        $product_list[$product_key]['global_override']=true;
                    }
                    if (isset($productOptions[$id_product][0]['no_price_export']) && !empty($productOptions[$id_product][0]['no_price_export'])) {
                        $product_list[$product_key]['no_price_export'] = $productOptions[$id_product][0]['no_price_export'];
                    }
                    if (isset($productOptions[$id_product][0]['no_quantity_export']) && !empty($productOptions[$id_product][0]['no_quantity_export'])) {
                        $product_list[$product_key]['no_quantity_export'] = $productOptions[$id_product][0]['no_quantity_export'];
                    }
                    if (isset($productOptions[$id_product][0]['latency']) && !empty($productOptions[$id_product][0]['latency'])) {
                        $product_list[$product_key]['latency'] = $productOptions[$id_product][0]['latency'];
                    }
                    if (isset($productOptions[$id_product][0]['shipping']) &&
                        (!empty($productOptions[$id_product][0]['shipping'])
                        || $productOptions[$id_product][0]['shipping'] === 0 
                        || $productOptions[$id_product][0]['shipping'] === '0') ) {
                        $product_list[$product_key]['shipping'] = $productOptions[$id_product][0]['shipping'];
                    }
                }

                if (isset($product_element['combination']) && is_array($product_element['combination'])) {
		    
                    foreach ($product_element['combination'] as $combination_id => $combination_element) {
			
                        if (isset($productOptions[$id_product]) && isset($productOptions[$id_product][$combination_id])) {
			    
                            $combination_options = $productOptions[$id_product][$combination_id];
			    $override_options = array();
                            if (isset($combination_options['disable']) && !empty($combination_options['disable'])) {
//                                unset($product_list[$product_key][$combination_id]);
//                                continue;
                                $override_options['disable'] = $combination_options['disable'];
                            }

                            if (is_array($combination_options)) {
                                
                                if (isset($combination_options['force']) && !empty($combination_options['force'])) {
                                    $override_options['force'] = $combination_options['force'];
                                }
                                if (isset($combination_options['price']) && !empty($combination_options['price'])) {
                                    $override_options['original_price'] = isset($combination_element['price'])?$combination_element['price']:0;
                                    $override_options['price'] = $combination_options['price'];
                                    $override_options['global_override'] = true;
                                }
                                if (isset($combination_options['no_price_export']) && !empty($combination_options['no_price_export'])) {
                                    $override_options['no_price_export'] = $combination_options['no_price_export'];
                                }
                                if (isset($combination_options['no_quantity_export']) && !empty($combination_options['no_quantity_export'])) {
                                    $override_options['no_quantity_export'] = $combination_options['no_quantity_export'];
                                }
                                if (isset($combination_options['latency']) && !empty($combination_options['latency'])) {
                                    $override_options['latency'] = $combination_options['latency'];
                                }
                                if (isset($combination_options['shipping']) && 
                                    (!empty($combination_options['shipping'])
                                    || $combination_options['shipping']===0 
                                    || $combination_options['shipping']==='0')) {
                                    $override_options['shipping'] = $combination_options['shipping'];
                                }
                                $product_list[$product_key]['combination'][$combination_id] = 
					array_merge($product_list[$product_key]['combination'][$combination_id], $override_options);
                            }
                        }
                    }
                }
                unset($productOptions[$id_product]);
            }
        }
                    
        return $product_list;
    }////////////////////////////////#################################################/////////////////////////////

    public function exportProductsForTableList($id_shop, $id_mode, $id_lang = null, $addtional = null) {
	
        $empty_txt = '-';
        $start = $limit = 1;
        $query = $category = $condition = $qsearch = $l = $attr_qsearch = '';
        $addtional_list = array();
        $this->db_product = ObjectModel::$db;
        $_db = ObjectModel::$db;
        $_db->db_trans_begin();
        $addtional['no_sub'] = false;

        if (isset($addtional) && !empty($addtional)) {
	    
            foreach ($addtional as $key => $add) {
		
                switch ($key) {
		    
                    case "category":
                        $category = isset($add) && !empty($add) ? ' AND p.id_category_default IN (' . $add . ')' : '';
                        break;
                    case "condition":
                        $condition = isset($add) && !empty($add) ? ' AND p.id_condition IN (' . $add . ')' : '';
                        break;
                    case "manufacturer":
                        foreach ($add as $manu)
                            $manufacturer[] = $manu['id_manufacturer'];
                        break;
                    case "supplier":
                        foreach ($add as $supp)
                            $supplier[] = $supp['id_supplier'];
                        break;
                    case "carrier":
                        foreach ($add as $carr)
                            $carrier[] = $carr['id_carrier'];
                        break;
                    case "page":
                        $start_page = ($this->limit_page * ($add - $start));
                        $start = $start_page == 1 ? 0 : $start_page;
                        break;
                    case "start":
                        $start = $add;
                        $limit = isset($addtional['length']) && $addtional['length'] > 0 ? $addtional['length'] : 10;
                        break;
                    case "search":

                        $qsearch = isset($add) && !empty($add) ? ' AND (BINARY p.id_product = \''. $add .'\' or p.sku like "%' . $add . '%" OR pl.name like "%' . $add . '%" OR p.ean13 like "%' . $add . '%" OR p.reference like "%' . $add . '%")' : '';
                        $attr_qsearch = isset($add) && !empty($add) ? " OR BINARY p.id_product = '{$add}' or p.id_product in (select id_product from {$_db->prefix_table}product_attribute where  sku like '%{$add}%' or ean13 like '%{$add}%' or upc like '%{$add}%' or reference like '%{$add}%') " : '';
                        break;
                }
            }
        }
        
        if(!isset($id_lang) || empty($id_lang)){
	    $id_lang = $this->get_id_language($id_shop);		
	}     
                    
        if(!isset($this->arr_cat_lang)){
            $this->arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name from {$_db->prefix_table}category_lang cl where '{$id_shop}'= cl.id_shop AND cl.id_lang = '$id_lang'";
            
            $result = $_db->db_query_str($sql_cat_lang);
            foreach($result as $v){
                $this->arr_cat_lang[$v['id_category']] = $v['name'];
            }            
        } 
        
        $cr_sql = "select cr.id_currency,cr.iso_code from {$_db->prefix_table}currency cr where  cr.id_shop = '" . $id_shop . "' ";
        $result = $_db->db_query_str($cr_sql);
        $cr_list = array();
        foreach($result as $v){
            $cr_list[$v['id_currency']] = $v['iso_code'];
        }

        
	
        $pl_list = array(); 
	
        if (isset($addtional) && !empty($addtional)) {
	    
            foreach ($addtional as $key => $add) {
		

		if ($key == 'search' && !empty($add)  ) {
		    
		    $pl_sql = "select id_product "
			    . "from {$_db->prefix_table}product_lang pl  "
			    . "where   pl.id_shop = '" . $id_shop . "' ".' and  pl.name like "%' . $add . '%" group by pl.id_product';
			    
		    $result = $_db->db_query_str($pl_sql); 
		    
		    if (isset($id_lang) && !empty($id_lang)) {
			$pl_sql .= ' and pl.id_lang =  ' . $id_lang . ' ';
		    }

		    $result = $_db->db_query_str($pl_sql); 
		    foreach($result as $v){
			$pl_list[$v['id_product']] = $v['id_product'];
		    }
		    
		    $at_sql = "select id_product "
			    . "from {$_db->prefix_table}product_attribute "
			    . "where sku like '%{$add}%' or ean13 like '%{$add}%' or upc like '%{$add}%' or reference like '%{$add}%' and id_shop = '" . $id_shop . "' ; ";
			    
		    $result = $_db->db_query_str($at_sql); 
		    
		    if (isset($id_lang) && !empty($id_lang)) {
			    $at_sql .= ' and at.id_lang =  ' . $id_lang . ' ';
		    }
		    
		    foreach($result as $v){
			$pl_list[$v['id_product']] = $v['id_product'];
		    }
		}
	    }
        }
        
	$query .= 'SELECT p.id_product,p.price,p.id_condition, p.has_attribute, p.id_manufacturer ,p.quantity, p.sku, '
                . ' p.ean13,p.upc,p.reference,p.active,p.date_add,p.date_upd,p.id_tax,p.has_attribute ,p.id_currency,p.id_category_default as id_category '
                . $l . '   FROM ' . $_db->prefix_table . 'product p ';
         
	if(!empty($qsearch)){
            $query .= ' LEFT JOIN ' . $_db->prefix_table . 'product_lang pl ON p.id_product = pl.id_product AND pl.id_shop = "' . $id_shop . '"';
        }
        
        if(!empty($pl_list)){

             $qsearch .= " or p.id_product in (".implode(',',$pl_list).") ";
	}
       
        $manufacturers = $suppliers = $carriers = '';
                    

        $query .= ' WHERE /*p.active = 1 and*/   (p.date_upd IS NULL OR p.date_upd = \'\') AND p.id_shop = "' . $id_shop . '" ' .  $category . $condition . $qsearch .$attr_qsearch  ;
        $query .= ' GROUP BY p.id_product
                ORDER BY p.id_product ';
	
        $all_filter_query = $query;
        $p_id_list = array();
        $query .= isset($addtional['start']) && isset($addtional['length']) ? ' LIMIT ' . $start . ', ' . $limit : ''; 
        $result = $_db->db_query_str($query);
        foreach($result as $res){
            $p_id_list[$res['id_product']] = $res['id_product'];
        }
        $sql_lang = "select id_product,name from {$_db->prefix_table}product_lang  where  id_shop= '{$id_shop}' and id_product in ('".implode("','",$p_id_list)."')";

        if (isset($id_lang) && !empty($id_lang))
            $sql_lang .= ' and  id_lang =  ' . $id_lang . ' ';

        $sql_lang .= " group by id_product";
        $out = $_db->db_query_str($sql_lang);
        $p_lang = array();

        foreach ($out as $o) {
            $p_lang[$o['id_product']] = $o['name'];
        }

        $sql_img = "select id_product,image_url from {$_db->prefix_table}product_image  where  id_shop= '{$id_shop}' and id_product in ('".implode("','",$p_id_list)."')";
        $sql_img .= " group by id_product";
        $out = $_db->db_query_str($sql_img);
        $p_img = array();
        foreach ($out as $o) {
            $p_img[$o['id_product']] = $o['image_url'];
        }



//        $pcat_sql = "select pcat.id_product ,pcat.id_category from {$_db->prefix_table}product_category pcat where  pcat.id_shop = '" . $id_shop . "' and id_product in ('".implode("','",$p_id_list)."') ";
//        $result = $_db->db_query_str($pcat_sql);
//        $pcat_list = array();
//        foreach($result as $v){
//            $pcat_list[$v['id_product'].'_'.$v['id_category']] = $v;
//        }


	
        $all_query = "SELECT count(p.id_product) as count from {$_db->prefix_table}product p "
                . " where (p.date_upd IS NULL OR p.date_upd = \'\') AND active = 1 and id_shop =  '{$id_shop}' " 
                . " GROUP BY p.id_shop   ";

        $tmp = $_db->db_query_str($all_query);
        $return['all_num'] = 0;
	
        if (isset($tmp[0]['count'])) {
            $return['all_num'] = $tmp[0]['count']; 
	}

        $return['all_filter_num'] = $_db->db_num_rows($_db->db_sqlit_query($all_filter_query)); 
	
        foreach ($result as $k => $res) {
	    if(!empty($res['date_upd']))return;
            $search = array(); 
            $cat = isset($res['id_category']) ? $res['id_category'] : 0;
            $pid = $res['id_product']; 
            $p_cat = array();
            $out_cat = $cat;
            $res['p_name'] = isset($p_lang[$pid]) ? $p_lang[$pid] : '';
            $res['image_url'] = isset($p_img[$pid]) ? $p_img[$pid] : '';
	    
            while ($out_cat != 0) {
                $p_cat[] = $out_cat;
                $sql = "select id_category, id_parent from {$_db->prefix_table}category where id_shop = '$id_shop' and id_category = '$out_cat' limit 1";
                $cat_res = $_db->db_query_str($sql);
                if (isset($cat_res[0])) {
                    $out_cat = $cat_res[0]['id_parent']; 
                    if ($cat_res[0]['id_category'] == $cat_res[0]['id_parent']) {
                        break;
                    }
                } else {
                    $out_cat = 0;
                }
            }
	    
            $search[] = "'" . implode("','", $p_cat) . "'";
            $offer = $this->getProductOffer($pid, $id_shop);
            $res['p.id_product'] = isset($res['p.id_product']) ? $res['p.id_product'] : $res['id_product'];
            $res['p.price'] = isset($res['p.price']) ? $res['p.price'] : $res['price'];
            $res['p.quantity'] = isset($res['p.quantity']) ? $res['p.quantity'] : $res['quantity'];
            $res['p.id_tax'] = isset($res['p.id_tax']) ? $res['p.id_tax'] : $res['id_tax'];
            $res['p.id_condition'] = isset($res['p.id_condition']) ? $res['p.id_condition'] : $res['id_condition'];
            $res['p.id_product'] = isset($res['p.id_product']) ? $res['p.id_product'] : $res['id_product'];
            $res['p.category_default_name'] = !empty($this->arr_cat_lang[$res['id_category']])?$this->arr_cat_lang[$res['id_category']]:'';
	    
            if (!empty($offer)) {
                $res['p.price'] = $offer['price'];
                $res['p.on_sale'] = $offer['on_sale'];
                $res['p.quantity'] = $offer['quantity'];
                $res['p.id_tax'] = $offer['id_tax'];
                $res['p.id_condition'] = $offer['id_condition'];
            }

            $sale = $this->exportSaleArrayList(array($pid),$id_shop);
            $return[$k]['sale_price']=$return[$k]['sale_to']=$return[$k]['sale_from']='';
            if(isset($sale[$pid][0])){
                if($sale[$pid][0]['reduction_type'] == 'amount'){
                    $return[$k]['sale_price'] = $res['p.price']- $sale[$pid][0]['reduction'] ;
                } else {
                    $return[$k]['sale_price'] = ($res['p.price'] - ( $res['p.price'] * ($sale[$pid][0]['reduction']/100) ) );
                }
                if($sale[$pid][0]['price']*1!=0){
                    $return[$k]['sale_price']=$sale[$pid][0]['price'];
                }
                $return[$k]['sale_to']=str_replace(' 00:00:00','',$sale[$pid][0]['date_to']);
                $return[$k]['sale_from']=str_replace(' 00:00:00','',$sale[$pid][0]['date_from']);
            }

            $return[$k]['cal_price'] = json_encode($this->calculate_price($cat, $res['p.price'], $id_mode));
	    
            if ((isset($addtional['no_sub']) && !$addtional['no_sub'] ) || !isset($addtional['no_sub'])) {
		
                $res['p.has_attribute'] = isset($res['p.has_attribute']) ? $res['p.has_attribute'] : $res['has_attribute'];
		
                if ($res['p.has_attribute'] != 0) {

                    $_db = ObjectModel::$db;

                    $sql = "SELECT a.id_product_attribute, a.reference, a.ean13, a.sku, a.upc, a.price, a.price_type, a.quantity,ao.price as offer_price , ao.quantity as offer_qty
                        , IF(a.date_add > ao.date_add, 1, 0) as product_newer
                            FROM {$_db->prefix_table}product_attribute  a "
                            . " left join offers_product_attribute ao on a.id_product_attribute = ao.id_product_attribute and ao.id_product = '{$pid}' and ao.id_shop = '{$id_shop}' 
			    WHERE a.id_product = " . $pid . " AND a.id_shop = " . $id_shop. " group by a.id_product_attribute";

                    $r = $_db->db_query_str($sql);

                    $attributes = array();
                    $max_price = 0;
		    
                    foreach ($r as $attribute) {
//                        if(isset($attribute['product_newer']) && $attribute['product_newer']==0){
                            if((string)$attribute['offer_price']!=''){
                                $attribute['price'] = $attribute['offer_price'];
                            }
                            if((string)$attribute['offer_qty']!=''){
                                $attribute['quantity'] = $attribute['offer_qty'];
                            }
//                        }
                        
                        if ($attribute['price'] > $max_price) {
                            $max_price = $attribute['price'];
                        }
                        $attributes[$attribute['id_product_attribute']]['price'] = $attribute['price'];
                        $attributes[$attribute['id_product_attribute']]['qty'] = $attribute['quantity'];

                        $sql = "
			SELECT agl.name as agl_name ,al.name
			FROM {$_db->prefix_table}product_attribute pa 
                        left join {$_db->prefix_table}product_attribute_combination pac on pac.id_shop = pa.id_shop and pac.id_product_attribute = pa.id_product_attribute 
                        left join {$_db->prefix_table}attribute_group_lang agl on pac.id_attribute_group = agl.id_attribute_group  and pa.id_shop = agl.id_shop  
                        left join {$_db->prefix_table}attribute_lang al on pac.id_attribute_group = al.id_attribute_group 
				and pac.id_attribute = al.id_attribute and pa.id_shop = al.id_shop ";
			
//                        if ($id_lang == null) {
                            $sql .= " inner join {$_db->prefix_table}language l on l.id_lang = al.id_lang and   agl.id_lang = al.id_lang  and l.is_default = 1 and pa.id_shop = l.id_shop ";
//                        } else {
//                            $sql .= " and al.id_lang = '$id_lang' and   agl.id_lang = al.id_lang ";
//                        }
			
                        $sql .= " WHERE pa.id_product = " . $pid . " "
				. "AND pa.id_product_attribute ='" . $attribute['id_product_attribute'] . "' "
				. "AND pa.id_shop = " . $id_shop . " order by agl.name";
			
                        $atrn = $_db->db_query_str($sql);

                        $atr_out = array();
                        $i = 0;
                        $old_gn = $old_an = '';
			
                        foreach ($atrn as $al) {
                            $i++;
                            $name = $al['agl_name'];
                            $name_al = isset($al['al.name']) ? $al['al.name'] : $al['name'];
                            if ($i > 3)
                                break;
                            if ($old_gn == $name)
                                continue;
                            $old_gn = $name;
                            $old_an = $name;
                            $atr_out[] = $name . ': ' . $name_al;
                        }
			
//                        $attributes[$attribute['id_product_attribute']]['sql'] = $sql;
                        $attributes[$attribute['id_product_attribute']]['atr'] = implode(' | ', $atr_out);
                        $attributes[$attribute['id_product_attribute']]['ref'] = array();
                        $search[] = $attributes[$attribute['id_product_attribute']]['atr'];
			
                        if (!empty($attribute['sku']))
                            $attributes[$attribute['id_product_attribute']]['ref'][] = 'SKU: ' . $attribute['sku'];
			
                        if (!empty($attribute['ean13']))
                            $attributes[$attribute['id_product_attribute']]['ref'][] = 'EAN: ' . $attribute['ean13'];
			
                        if (!empty($attribute['upc']))
                            $attributes[$attribute['id_product_attribute']]['ref'][] = 'UPC: ' . $attribute['upc'];
			
                        if (!empty($attribute['reference']))
                            $attributes[$attribute['id_product_attribute']]['ref'][] = 'Ref: ' . $attribute['reference'];

                        if (!empty($attributes[$attribute['id_product_attribute']]['ref'])) {
                            $attributes[$attribute['id_product_attribute']]['dref'] = $attributes[$attribute['id_product_attribute']]['ref'][0];
                        } else {
                            $attributes[$attribute['id_product_attribute']]['dref'] = '';
                        }
			
                        $attributes[$attribute['id_product_attribute']]['ref'] = array();
                        $attributes[$attribute['id_product_attribute']]['ref']['sku'] = $attribute['sku'] == NULL ? $empty_txt : $attribute['sku'];
                        $attributes[$attribute['id_product_attribute']]['ref']['ean13'] = $attribute['ean13'] == NULL ? $empty_txt : $attribute['ean13'];
                        $attributes[$attribute['id_product_attribute']]['ref']['upc'] = $attribute['upc'] == NULL ? $empty_txt : $attribute['upc'];
                        $attributes[$attribute['id_product_attribute']]['ref']['ref'] = $attribute['reference'] == NULL ? $empty_txt : $attribute['reference']; 
                        unset($attributes[$attribute['id_product_attribute']]['price_type']);
                        unset($attributes[$attribute['id_product_attribute']]['id_product_attribute']);
                        unset($attributes[$attribute['id_product_attribute']]['reference']);
                        unset($attributes[$attribute['id_product_attribute']]['ean13']);
                        unset($attributes[$attribute['id_product_attribute']]['upc']);
                        unset($attributes[$attribute['id_product_attribute']]['quantity']);
                        $id_attr = $attribute['id_product_attribute'];
                        $attributes[$id_attr]['sale_price']=$attributes[$id_attr]['sale_to']=$attributes[$id_attr]['sale_from']='';
                        if(isset($sale[$pid][$id_attr])){
                            if($sale[$pid][$id_attr]['reduction_type'] == 'amount'){
                                $attributes[$id_attr]['sale_price'] = $attribute['price']- $sale[$pid][$id_attr]['reduction'] ;
                            } else {
                                $attributes[$id_attr]['sale_price'] = ($attribute['price'] - ( $attribute['price'] * ($sale[$pid][$id_attr]['reduction']/100) ) );
                            }
                            if($sale[$pid][$id_attr]['price']*1!=0){
                                $attributes[$id_attr]['sale_price']=$sale[$pid][$id_attr]['price'];
                            }
                            $attributes[$id_attr]['sale_to']=$sale[$pid][$id_attr]['date_to'];
                            $attributes[$id_attr]['sale_from']=$sale[$pid][$id_attr]['date_from'];
                        }



                        $search[] = $attributes[$attribute['id_product_attribute']]['dref'];
                    }

                    $return[$k]['items'] = ($attributes);
                    $return[$k]['no_attr'] = false;
                    $res['p.price'] = $max_price;
		    
                } else {
		    
                    $item = array();

                    $res['p.sku'] = isset($res['p.sku']) ? $res['p.sku'] : $res['sku'];
                    $res['p.ean13'] = isset($res['p.ean13']) ? $res['p.ean13'] : $res['ean13'];
                    $res['p.upc'] = isset($res['p.upc']) ? $res['p.upc'] : $res['upc'];
                    $res['p.reference'] = isset($res['p.reference']) ? $res['p.sku'] : $res['reference'];

                    if (!empty($res['p.sku']))
                        $item[0]['ref'][] = 'SKU: ' . $res['p.sku'];
		    
                    if (!empty($res['p.ean13']))
                        $item[0]['ref'][] = 'EAN: ' . $res['p.ean13'];
		    
                    if (!empty($res['p.upc']))
                        $item[0]['ref'][] = 'UPC: ' . $res['p.upc'];
		    
                    if (!empty($res['p.reference']))
                        $item[0]['ref'][] = 'Ref: ' . $res['p.reference'];
		    
                    if (!empty($res['sku']))
                        $item[0]['ref'][] = 'SKU: ' . $res['sku'];
		    
                    if (!empty($res['ean13']))
                        $item[0]['ref'][] = 'EAN: ' . $res['ean13'];
		    
                    if (!empty($res['upc']))
                        $item[0]['ref'][] = 'UPC: ' . $res['upc'];
		    
                    if (!empty($res['reference']))
                        $item[0]['ref'][] = 'Ref: ' . $res['reference'];

                    if (!empty($item[0]['ref'])) {
                        $item[0]['dref'] = $item[0]['ref'][0];
                    }
                    unset($item[0]['ref']);
                    $item[0]['ref'] = array();
                    $item[0]['ref']['sku'] = $res['p.sku'] == NULL ? $empty_txt : $res['p.sku'];
                    $item[0]['ref']['ean13'] = $res['p.ean13'] == NULL ? $empty_txt : $res['p.ean13'];
                    $item[0]['ref']['upc'] = $res['p.upc'] == NULL ? $empty_txt : $res['p.upc'];
                    $item[0]['ref']['ref'] = $res['p.reference'] == NULL ? $empty_txt : $res['p.reference'];
                    $item[0]['atr'] = isset($res['p_name']) ? $res['p_name'] : $res['name'];
                    $item[0]['price'] = $res['p.price'];
                    $item[0]['qty'] = $res['p.quantity'];
                    $id_attr=0;
                    $item[$id_attr]['sale_price']=$item[$id_attr]['sale_to']=$item[$id_attr]['sale_from']='';
                    $return[$k]['items'] = ($item);
                    $return[$k]['no_attr'] = true;
                    if(isset($item[0]['dref']))
                    $search[] = $item[0]['dref'];
                }
            }
	    
            $res['p.sku'] = isset($res['p.sku']) ? $res['p.sku'] : $res['sku'];
            $res['p.ean13'] = isset($res['p.ean13']) ? $res['p.ean13'] : $res['ean13'];
            $res['p.upc'] = isset($res['p.upc']) ? $res['p.upc'] : $res['upc'];
            $res['p.reference'] = isset($res['p.sku']) ? $res['p.sku'] : (isset($res['reference'])?$res['reference']:$empty_txt);
	    
            $ref = array();
            $ref['sku'] = $res['p.sku'] == NULL ? $empty_txt : $res['p.sku'];
            $ref['ean13'] = $res['p.ean13'] == NULL ? $empty_txt : $res['p.ean13'];
            $ref['upc'] = $res['p.upc'] == NULL ? $empty_txt : $res['p.upc'];
            $ref['ref'] = $res['p.reference'] == NULL ? $empty_txt : $res['p.reference'];
	    
            if (!empty($res['p.sku']))
                $res['ref'][] = 'SKU: ' . $res['p.sku'];
	    
            if (!empty($res['p.ean13']))
                $res['ref'][] = 'EAN: ' . $res['p.ean13'];
	    
            if (!empty($res['p.upc']))
                $res['ref'][] = 'UPC: ' . $res['p.upc'];
	    
            if (!empty($res['p.reference']))
                $res['ref'][] = 'Ref: ' . $res['p.reference'];
	    
            if (!empty($res['sku']))
                $res['ref'][] = 'SKU: ' . $res['sku'];
	    
            if (!empty($res['ean13']))
                $res['ref'][] = 'EAN: ' . $res['ean13'];
	    
            if (!empty($res['upc']))
                $res['ref'][] = 'UPC: ' . $res['upc'];
	    
            if (!empty($res['reference']))
                $res['ref'][] = 'Ref: ' . $res['reference'];
	    
            $res['dref'] = '';
            if (!empty($res['ref'])) {
                $res['dref'] = ' (' . $res['ref'][0] . ')';
            }
	    
            $res['cr.iso_code'] = isset($cr_list[$res['id_currency']])?$cr_list[$res['id_currency']]:'EUR'; 
            $res['pl.name'] = isset($res['p_name']) ? $res['p_name'] : $res['name'];
	    
            if (isset($res['pimg.image_url'])) {
                $res['pimg.image_url'] = $res['pimg.image_url'];
            } else if (isset($res['image_url'])) {
                $res['pimg.image_url'] = $res['image_url'];
            } else {
                $res['pimg.image_url'] = '';
            }
	    
            $res['p.active'] = isset($res['p.active']) ? $res['p.active'] : $res['active'];
            $return[$k]['pid'] = $res['p.id_product'];
            $return[$k]['price'] = $res['p.price'];
            $return[$k]['curr'] = $res['cr.iso_code'];
            $return[$k]['cond'] = $res['p.id_condition'];
            $return[$k]['qty'] = $res['p.quantity'];
            $return[$k]['act'] = $res['p.active'];
            $return[$k]['name'] = $res['pl.name'];  
            $return[$k]['img'] = $res['pimg.image_url'];
            $return[$k]['category_default_name']=$res['p.category_default_name'];
            $return[$k]['ref'] = $ref;
            $return[$k]['search'] = ''; 
            
        } 
        return $return;
    }

    public function getImagesByProductId($id_product, $id_shop) {
	
        if (!isset($id_product) || empty($id_product))
            return NULL;

        $images = array();
        $_db = ObjectModel::$db;
        $_db->from('product_image');
        $_db->where(array('id_product' => (int) $id_product, 'id_shop' => (int) $id_shop));
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $image) {
            if (isset($image['image_url'])) {
                $images[$image['id_image']]['image_url'] = $image['image_url'];
            }
            $images[$image['id_image']]['image_type'] = (string) $image['image_type'];
        }

        return $images;
    }

    public function getProductCombinationByProductId($id_product, $id_shop, $offer_db=null, $id_category=null, $product_price=0, $id_manufacturer=null, $id_supplier=null, $id_lang=null) {
	
        if (!isset($id_product) || empty($id_product) || !isset($id_shop) || empty($id_shop))
            return;

        $_db = ObjectModel::$db;
        $sql = "SELECT pa.id_product_attribute as id_product_attribute, 
                pa.reference as reference, pa.wholesale_price ,
                pa.ean13 as ean13, 
                pa.sku as sku, 
                pa.upc as upc, 
                pa.price as price, 
                pa.price_type as price_type,
                pa.quantity as quantity, 
                pa.weight as weight, 
                pa.available_date as available_date,               
                a.id_attribute_group as id_attribute_group, agl.name as agl_name, a.id_attribute as id_attribute, a.name as a_name, l.iso_code as iso_code
                FROM {$_db->prefix_table}product_attribute pa    
                LEFT JOIN {$_db->prefix_table}product_attribute_combination pac ON pa.id_product_attribute = pac.id_product_attribute AND pa.id_shop = pac.id_shop
                LEFT JOIN {$_db->prefix_table}attribute_group_lang agl ON (pac.id_attribute_group = agl.id_attribute_group AND pac.id_shop = agl.id_shop)
                LEFT JOIN {$_db->prefix_table}attribute_lang a ON (pac.id_attribute_group = a.id_attribute_group AND pac.id_attribute = a.id_attribute AND pac.id_shop = a.id_shop)
                LEFT JOIN {$_db->prefix_table}language l ON (a.id_lang = l.id_lang AND a.id_shop = l.id_shop)
                WHERE pa.id_product = " . $id_product . " AND pa.id_shop = " . $id_shop;

        if (isset($id_lang) && !empty($id_lang)) {
            $sql .= " AND agl.id_lang = " . (int) $id_lang . " AND a.id_lang = " . (int) $id_lang . " AND l.id_lang = " . (int) $id_lang . " ";
        }

        $sql .= " GROUP BY pa.id_product_attribute, pa.id_product_attribute, a.id_attribute_group, a.id_attribute; ; ";

        $result = $_db->db_query_str($sql);
        $attributes = array();

        if (isset($result) && !empty($result)) {
	    
            foreach ($result as $attribute) {

                $options = self::getProductOption($id_product, $id_shop, $attribute['id_product_attribute'], $id_lang);

                if ($options === false) {
                    continue;
                }

                $price = $attribute['price'];

                /* wholesale_price ADD : 16/07/2015 */
                $wholesale_price = isset($attribute['wholesale_price']) ? $attribute['wholesale_price'] : null;

                $quantity = $attribute['quantity'];
                $combination_offer = self::getProductCombinationOffer($id_product, $attribute['id_product_attribute'], $id_shop, $offer_db);

                if (isset($combination_offer['price'])) {
                    $price = $combination_offer['price'];
                }
                /* wholesale_price ADD : 16/07/2015 */
                if (isset($combination_offer['wholesale_price'])) {
                    $wholesale_price = $combination_offer['wholesale_price'];
                }

                // Check rule
                $products = $product_price + $price;

                if ($this->check_rules($id_category, $products, $id_shop, $id_manufacturer, $id_supplier)) {
                    continue;
                }

                if (isset($combination_offer['price'])) {
                    $attributes[$attribute['id_product_attribute']]['price_original'] = $combination_offer['price'];
                }

                if (isset($combination_offer['quantity'])) {
                    $quantity = $combination_offer['quantity'];
                }

                $attributes[$attribute['id_product_attribute']]['id_product_attribute'] = $attribute['id_product_attribute'];
                $attributes[$attribute['id_product_attribute']]['reference'] = $attribute['reference'];
                $attributes[$attribute['id_product_attribute']]['ean13'] = $attribute['ean13'];
                $attributes[$attribute['id_product_attribute']]['sku'] = $attribute['sku'];
                $attributes[$attribute['id_product_attribute']]['upc'] = $attribute['upc'];
                $attributes[$attribute['id_product_attribute']]['price'] = $price;
                $attributes[$attribute['id_product_attribute']]['wholesale_price'] = $wholesale_price;
                $attributes[$attribute['id_product_attribute']]['price_type'] = $attribute['price_type'];
                $attributes[$attribute['id_product_attribute']]['quantity'] = $quantity;

                if ($attribute['weight'] > 0) {
                    $weight['value'] = $attribute['weight'];
                    $weight['unit'] = Unit::getUnitByType($id_shop, 'Weight');
                    $attributes[$attribute['id_product_attribute']]['weight'] = $weight;
                } else {
                    $attributes[$attribute['id_product_attribute']]['weight'] = array();
                }

                $attributes[$attribute['id_product_attribute']]['available_date'] = $attribute['available_date'];

                if (isset($this->attribute_images[$this->id][$attribute['id_product_attribute']])) {
                    $attributes[$attribute['id_product_attribute']]['images'] = $this->attribute_images[$this->id][$attribute['id_product_attribute']];
                }
                if (isset($attribute['id_attribute_group']) && isset($attribute['id_attribute']) && !empty($attribute['id_attribute_group']) && !empty($attribute['id_attribute'])) {
                    $attributes[$attribute['id_product_attribute']]['attributes'][$attribute['id_attribute_group']]['name'][$attribute['iso_code']] = $attribute['agl_name'];
                    $attributes[$attribute['id_product_attribute']]['attributes'][$attribute['id_attribute_group']]['value'][$attribute['id_attribute']]['name'][$attribute['iso_code']] = $attribute['a_name'];
                }

                if (is_array($options)) {
                    $attributes[$attribute['id_product_attribute']] = array_merge($attributes[$attribute['id_product_attribute']], $options);
                }
            }
        }
      
        return $attributes;
    }

    public static function getProductAttributeById($id_product_attribute, $id_shop, $id_mode = null, $id_lang = null) {
	
        if (!isset($id_product_attribute) || empty($id_product_attribute))
            return NULL;

        $productAttribute = array();
        $mode = '';
        $_db = ObjectModel::$db;

        $sql = "SELECT pac.id_product_attribute as 'pac.id_product_attribute', a.id_attribute_group as 'a.id_attribute_group', "
        . "agl.id_lang as 'agl.id_lang', agl.name as 'agl.name'"
        . " , a.id_attribute as 'a.id_attribute', a.id_lang as 'a.id_lang', a.name as 'a.name', l.iso_code as 'l.iso_code' "
        . "FROM {$_db->prefix_table}product_attribute_combination pac "
        . "LEFT JOIN {$_db->prefix_table}attribute_group_lang agl ON (agl.id_attribute_group = pac.id_attribute_group AND agl.id_shop = pac.id_shop) "
        . "LEFT JOIN {$_db->prefix_table}attribute_lang a ON (a.id_attribute_group = pac.id_attribute_group AND a.id_attribute = pac.id_attribute AND a.id_shop = pac.id_shop) "
        . "LEFT JOIN {$_db->prefix_table}language l ON (a.id_lang = l.id_lang AND a.id_shop = l.id_shop) "
        . "WHERE pac.id_product_attribute = " . (int) $id_product_attribute . " "
        . "AND pac.id_shop = " . (int) $id_shop . " " . $mode . " ";

        if (isset($id_lang) && !empty($id_lang)) {
            $sql .= " AND agl.id_lang = " . (int) $id_lang . " AND a.id_lang = " . (int) $id_lang . " AND l.id_lang = " . (int) $id_lang . " ";
        }

        $result = $_db->db_query_str($sql . "; ");
        if (isset($result) && !empty($result)) {
            foreach ($result as $attribute) {
                $productAttribute[$attribute['a.id_attribute_group']]['name'][$attribute['l.iso_code']] = $attribute['agl.name'];
                $productAttribute[$attribute['a.id_attribute_group']]['value'][$attribute['a.id_attribute']]['name'][$attribute['l.iso_code']] = $attribute['a.name'];
            }
        }
        return $productAttribute;
    }

    public static function getProductAttributeImages($id_product, $id_shop) {

        $images = array();
        $_db = ObjectModel::$db;
	
        $sql = "SELECT pi.id_product, pa.id_product_attribute as id_product_attribute, pai.id_image as id_image, pi.image_url as url, pi.image_type as type 
	FROM {$_db->prefix_table}product_image pi
	LEFT JOIN {$_db->prefix_table}product_attribute_image pai ON pai.id_image = pi.id_image AND pa.id_shop = pi.id_shop
	LEFT JOIN {$_db->prefix_table}product_attribute pa ON pi.id_product = pa.id_product AND pai.id_product_attribute = pa.id_product_attribute AND pa.id_shop = pi.id_shop
	WHERE pa.id_product = " . (int) $id_product . " AND pi.id_product = " . (int) $id_product . " AND pai.id_shop = " . (int) $id_shop . "; ";

        $result = $_db->db_query_str($sql);
        foreach ($result as $data) {
            $images[$data['id_product_attribute']][$data['id_image']]['id_image'] = $data['id_image'];
            $images[$data['id_product_attribute']][$data['id_image']]['url'] = $data['url'];
            $images[$data['id_product_attribute']][$data['id_image']]['type'] = $data['type'];
        }

        return $images;
    }

    public static function getProductAttributeImagesById($id_product_attribute, $id_shop) {
	
        if (!isset($id_product_attribute) || empty($id_product_attribute))
            return NULL;

        $_db = ObjectModel::$db;
        $sql = "SELECT pai.id_image as id_image, pi.image_url as url, pi.image_type as type FROM {$_db->prefix_table}product_attribute_image pai
                LEFT JOIN {$_db->prefix_table}product_attribute pa ON pai.id_product_attribute = pa.id_product_attribute AND pa.id_shop = pai.id_shop
                LEFT JOIN {$_db->prefix_table}product_image pi ON pai.id_image = pi.id_image AND pa.id_product = pi.id_product AND pa.id_shop = pi.id_shop
                WHERE pai.id_product_attribute = " . $id_product_attribute . " AND pai.id_shop = " . $id_shop . "; ";
        $result = $_db->db_query_str($sql);
        return $result;
	
    }

    public function getProductOffer($id_product, $id_shop) {
	
        $products = array();
        $where = array();
        $_db = $this->db_offer;

        $_db->from('product');
        $where['id_shop'] = (int) $id_shop;
        $where['id_product'] = (int) $id_product;
        $_db->where($where);

        $result = $_db->db_array_query($_db->query);

        $sales = ProductSale::getOfferSaleByProductId($id_product, $id_shop, $this->db_offer);

        foreach ($result as $product) {
            $this->id_category_default = $product['id_category_default'];
            $this->on_sale = $product['on_sale'];
            $this->id_currency = $product['id_currency'];
            $this->id_condition = $product['id_condition'];
            $this->id_tax = $product['id_tax'];

            if (isset($sales) && !empty($sales))
                $products['sale'] = $sales;

            $products['shipping_cost'] = $product['shipping_cost'];
            $products['quantity'] = $product['quantity'];
            $products['price'] = $product['price'];
            $products['id_tax'] = $product['id_tax'];
            $products['on_sale'] = $product['on_sale'];
            $products['id_condition'] = $product['id_condition'];

            if (isset($product['wholesale_price'])) {
                $products['wholesale_price'] = $product['wholesale_price'];
            }
        }

        return $products;
    }

    //getProductCombinationOffer
    public function getProductCombinationOffer($id_product, $id_product_attribute, $id_shop, $offer_db = null) {
        $products = array();
        $where = array();

        if (isset($offer_db) && !empty($offer_db)) {
            $_db = $offer_db;
        } else {
            $_db = $this->db_offer;
        }

        $_db->from('product_attribute');
        $where['id_shop'] = (int) $id_shop;
        $where['id_product_attribute'] = (int) $id_product_attribute;
        $where['id_product'] = (int) $id_product;
        $_db->where($where);
        $result = $_db->db_array_query($_db->query);

        foreach ($result as $product) {
            $products['quantity'] = $product['quantity'];
            $products['price'] = $product['price'];

            if (isset($product['wholesale_price'])) {
                $products['wholesale_price'] = $product['wholesale_price'];
            }
        }

        return $products;
    }

    public function check_rules($id_category, $price, $id_shop, $id_manufacturer, $id_supplier) {
        $debug = false;
        $data_list = array();
        $_db = $this->db_offer;
        
        $columns  = array('ri.id_supplier','ri.id_manufacturer','ripr.price_range_from','ripr.price_range_to'
            ,'ri.action','r.name');
        $cols =$_db->put_array_tb_alias($columns);
        
        $query = 'SELECT '.implode(',',$cols).' FROM ' . $_db->prefix_table . 'category_selected cs 
        LEFT JOIN ' . $_db->prefix_table . 'profile pr ON cs.id_profile = pr.id_profile AND cs.id_shop = pr.id_shop
        LEFT JOIN ' . $_db->prefix_table . 'profile_item pri ON pr.id_profile = pri.id_profile AND pr.id_shop = pri.id_shop
        LEFT JOIN ' . $_db->prefix_table . 'rule r ON pri.id_rule = r.id_rule AND pri.id_shop = r.id_shop
        LEFT JOIN ' . $_db->prefix_table . 'rule_item ri ON ri.id_rule = r.id_rule AND r.id_shop = ri.id_shop
        LEFT JOIN ' . $_db->prefix_table . 'rule_item_price_range ripr ON ri.id_rule_item = ripr.id_rule_item AND ri.id_shop = ripr.id_shop
        LEFT JOIN ' . $_db->prefix_table . 'profile_price pp ON (ri."action" = pp.id_profile_price AND ri.id_shop = pp.id_shop) 
            OR (pr.id_profile_price = pp.id_profile_price AND pr.id_shop = pp.id_shop)
        WHERE cs.id_category = ' . "'" . $id_category . "'" . ' 
        AND cs.id_profile != 0 
        AND cs.id_shop = ' . "'" . $id_shop . "'" . ' ;';

        $result = $_db->db_query_str($query);

        if (isset($result) && !empty($result)) {
            foreach ($result as $data) {
                //supplier
                if (!empty($data['ri.id_supplier']) || $data['ri.id_supplier'] != NULL) {
                    //manufacturer
                    if (!empty($data['ri.id_manufacturer']) || $data['ri.id_manufacturer'] != NULL) {
                        //price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //supplier & manufacturer & price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ($data['ri.id_manufacturer'] == $id_manufacturer) && 
				    ( $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to'])) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //supplier & manufacturer & !price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ($data['ri.id_manufacturer'] == $id_manufacturer)) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & manufacturer & !price range', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                    //!manufacturer            
                    else {
                        //supplier & !manufacturer & price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {

                            //supplier & !manufacturer & price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ( $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to'])) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & !manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //supplier & !manufacturer & !price range
                            if (($data['ri.id_supplier'] == $id_supplier)) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & !manufacturer & !price range', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                }

                //!supplier
                else {
                    //manufacturer
                    if (!empty($data['ri.id_manufacturer']) || $data['ri.id_manufacturer'] != NULL) {
                        //check price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //!supplier & manufacturer & price range
                            if (($data['ri.id_manufacturer'] == $id_manufacturer) && $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to']) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //!supplier & manufacturer & !price range
                            if ($data['ri.id_manufacturer'] == $id_manufacturer) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & manufacturer & !price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                    //!manufacturer            
                    else {
                        //price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //!supplier & !manufacturer & price range
                            if ($price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to']) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & !manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //!supplier & !manufacturer & !price range
                            if ($debug != null)
                                echo '<pre>' . print_r('Case - ' . '!supplier & !manufacturer & !price range ', true) . '</pre>';

                            $data_list['case'] = $data['r.name'];
                        }
                    }
                }

                //var_dump($data_list); 
                if (isset($data_list['case']) && !empty($data_list['case'])) {
                    if ($data['ri.action'] == 0)
                        return true;
                }
            } // end foreach $result.
        } // end $result.

        return false;
    }

    public function check_rules_cache($id_category, $price, $id_shop, $id_manufacturer, $id_supplier) {
	
        $debug = false;
        $data_list = array();
        $_db = $this->db_offer;

        if (!isset($this->price_rules[$id_shop])) {
	    
            $columns  = array('ri.id_supplier','ri.id_manufacturer','ripr.price_range_from','ripr.price_range_to','ri.action','r.name','cs.id_category');
            $cols =$_db->put_array_tb_alias($columns);

            $query = 'SELECT '.implode(',',$cols). " FROM {$_db->prefix_table}category_selected cs
		    LEFT JOIN {$_db->prefix_table}profile pr ON cs.id_profile = pr.id_profile AND cs.id_shop = pr.id_shop
		    LEFT JOIN {$_db->prefix_table}profile_item pri ON pr.id_profile = pri.id_profile AND pr.id_shop = pri.id_shop
		    LEFT JOIN {$_db->prefix_table}rule r ON pri.id_rule = r.id_rule AND pri.id_shop = r.id_shop
		    LEFT JOIN {$_db->prefix_table}rule_item ri ON ri.id_rule = r.id_rule AND r.id_shop = ri.id_shop
		    LEFT JOIN {$_db->prefix_table}rule_item_price_range ripr ON ri.id_rule_item = ripr.id_rule_item AND ri.id_shop = ripr.id_shop
		    LEFT JOIN {$_db->prefix_table}profile_price pp ON (ri.action = pp.id_profile_price AND ri.id_shop = pp.id_shop)
			OR (pr.id_profile_price = pp.id_profile_price AND pr.id_shop = pp.id_shop)
		    WHERE cs.id_profile != 0
		    AND cs.id_shop = '" . $id_shop . "'";

            $result = $_db->db_query_str($query);
            if (empty($result)) {
                $this->price_rules[$id_shop] = array();
            } else {
                foreach ($result as $row) {
                    $this->price_rules[$id_shop][$row['cs.id_category']][] = $row;
                }
            }
        }

        $result = isset($this->price_rules[$id_shop][$id_category]) ? $this->price_rules[$id_shop][$id_category] : array();

        if (isset($result) && !empty($result)) {
            foreach ($result as $data) {
                //supplier
                if (!empty($data['ri.id_supplier']) || $data['ri.id_supplier'] != NULL) {
                    //manufacturer
                    if (!empty($data['ri.id_manufacturer']) || $data['ri.id_manufacturer'] != NULL) {
                        //price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //supplier & manufacturer & price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ($data['ri.id_manufacturer'] == $id_manufacturer) && 
				    ( $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to'])) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //supplier & manufacturer & !price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ($data['ri.id_manufacturer'] == $id_manufacturer)) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & manufacturer & !price range', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                    //!manufacturer            
                    else {
                        //supplier & !manufacturer & price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {

                            //supplier & !manufacturer & price range
                            if (($data['ri.id_supplier'] == $id_supplier) && ( $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to'])) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & !manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //supplier & !manufacturer & !price range
                            if (($data['ri.id_supplier'] == $id_supplier)) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . 'supplier & !manufacturer & !price range', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                }

                //!supplier
                else {
                    //manufacturer
                    if (!empty($data['ri.id_manufacturer']) || $data['ri.id_manufacturer'] != NULL) {
                        //check price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //!supplier & manufacturer & price range
                            if (($data['ri.id_manufacturer'] == $id_manufacturer) && $price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to']) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //!supplier & manufacturer & !price range
                            if ($data['ri.id_manufacturer'] == $id_manufacturer) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & manufacturer & !price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                    }
                    //!manufacturer            
                    else {
                        //price range
                        if ((!empty($data['ripr.price_range_from']) || $data['ripr.price_range_from'] != NULL) && 
				(!empty($data['ripr.price_range_to']) || $data['ripr.price_range_to'] != NULL)) {
                            //!supplier & !manufacturer & price range
                            if ($price >= $data['ripr.price_range_from'] && $price <= $data['ripr.price_range_to']) {
                                if ($debug != null)
                                    echo '<pre>' . print_r('Case - ' . '!supplier & !manufacturer & price range ', true) . '</pre>';

                                $data_list['case'] = $data['r.name'];
                            }
                        }
                        else {
                            //!supplier & !manufacturer & !price range
                            if ($debug != null)
                                echo '<pre>' . print_r('Case - ' . '!supplier & !manufacturer & !price range ', true) . '</pre>';

                            $data_list['case'] = $data['r.name'];
                        }
                    }
                }
                //var_dump($data_list); 
                if (isset($data_list['case']) && !empty($data_list['case'])) {
                    if ($data['ri.action'] === 0)
                        return true;
                }
            } // end foreach $result.
        } // end $result.

        return false;
    }

    public function calculate_price($id_category, $price, $id_mode = 1, $debug = null) {
        $product_list = array();
        $product_list['price'] = $price;
        return $product_list;
    }

    public function getNumRowProducts($id_shop) {
        if (!isset($id_shop) || empty($id_shop))
            return NULL;

        $_db = ObjectModel::$db;
        $result = $_db->db_sqlit_query('SELECT p.id_product FROM ' . $_db->prefix_table . 'product p
                    LEFT JOIN ' . $_db->prefix_table . 'product_attribute pa ON p.id_product = pa.id_product AND p.id_shop = pa.id_shop
                    WHERE p.id_shop = ' . $id_shop . ' AND date_upd IS NULL OR date_upd = "";');

        return $_db->db_num_rows($result);
    }

    public function getProductOption($id_product, $id_shop, $id_product_attribute = null, $id_lang = null) {

        $product_options = $this->product_options->get_product_option_by_id($id_product, $id_shop, $id_product_attribute, $id_lang);
        $attribute_option = array();
        if (!empty($product_options)) {
            if (isset($product_options['disable']) && !empty($product_options['disable'])) {
//                return false;
                if (isset($id_product_attribute)) {
                    $attribute_option['disable'] = $product_options['disable'];
                } else {
                    $this->disabledisable = $product_options['disable'];
                }
            }
            if (isset($product_options['force']) && !empty($product_options['force'])) {
                if (isset($id_product_attribute)) {
                    $attribute_option['force'] = $product_options['force'];
                } else {
                    $this->force = $product_options['force'];
                }
            }
            if (isset($product_options['price']) && !empty($product_options['price'])) {
                if (isset($id_product_attribute)) {
                    $attribute_option['price'] = $product_options['price'];
                } else {
                    $this->price = $product_options['price'];
                }
            }
            if (isset($product_options['no_price_export']) && !empty($product_options['no_price_export'])) {
                if (isset($id_product_attribute)) {
                    $attribute_option['no_price_export'] = $product_options['no_price_export'];
                } else {
                    $this->no_price_export = $product_options['no_price_export'];
                }
            }
            if (isset($product_options['no_quantity_export']) && !empty($product_options['no_quantity_export'])) {
                if (isset($id_product_attribute)) {
                    $attribute_option['no_quantity_export'] = $product_options['no_quantity_export'];
                } else {
                    $this->no_quantity_export = $product_options['no_quantity_export'];
                }
            }
            if (isset($product_options['latency']) && !empty($product_options['latency'])) {
                if (isset($id_product_attribute)) {
                    $attribute_option['latency'] = $product_options['latency'];
                } else {
                    $this->latency = $product_options['latency'];
                }
            }
            if (isset($product_options['shipping']) && ( !empty($product_options['shipping'])
                || $product_options['shipping']===0 || $product_options['shipping'] === '0' )) {
                if (isset($id_product_attribute)) {
                    $attribute_option['shipping'] = $product_options['shipping'];
                } else {
                    $this->shipping = $product_options['shipping'];
                }
            }
            if (isset($id_product_attribute)) {
                return $attribute_option;
            }
        }
        return true;
    }
    public function get_id_language($id_shop) {
	if(!isset($id_shop) || !$id_shop) return;
	$Language = Language::getLanguageDefault($id_shop);
	$lang = array_keys($Language);
	$id_lang = isset($lang[0])?$lang[0]:0;
	return (int)$id_lang; 
    }
}