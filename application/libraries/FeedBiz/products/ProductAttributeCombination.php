<?php

class ProductAttributeCombination extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute_combination',
        'fields'    => array(
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            
        ),
       
    );
       
}