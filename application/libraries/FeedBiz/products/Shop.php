<?php

class Shop extends ObjectModel
{
    public function __construct( $user, $id = null, $connect = true)
    {
        parent::__construct( $user, $id = null, $connect = true);  
    }
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'shop',
        'primary' => array('id_shop'),
        'fields'    => array(
            'id_shop'       =>  array('type' => 'INTEGER', 'required' => true,  'primary_key' => true),
            'name'          =>  array('type' => 'varchar', 'required' => true, 'size' => 64 ),
            'is_default'    =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'active'        =>  array('type' => 'int', 'required' => false, 'size' => 1),
            'description'   =>  array('type' => 'text', 'required' => false),
            'date_add'      =>  array('type' => 'datetime', 'required' => true ),
        ),
       
    );
    
    public function getShopNameById($id_shop)
    {
        $shop = '';
        
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->where(array('id_shop' => $id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $shop)
            $shop = $shop['name'];
                    
        return $shop;
    }
    
    public function getShopIdByName($shop)
    {
        $id_shop = '';
        $_db = ObjectModel::$db;
        $_db->from('shop');
        $_db->where(array('name' => $shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $shop)
            $id_shop = $shop['id_shop'];
                    
        return $id_shop;
    }
    
    public function getOfferShopNameById($user, $id_shop)
    {
        $shop = '';
        
        $_db = new Db($user, 'offers');
        $_db->from('shop');
        $_db->where(array('id_shop' => $id_shop));
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $shop)
            $shop = $shop['name'];
                    
        return $shop;
    }
    
    public function getShops()
    {
        $shops = array();
        
        $_db = ObjectModel::$db;
        if(!$_db || empty($_db) || !$_db->db_table_exists('shop')) return $shops;
        
        $_db->from('shop');
        $_db->orderBy('name');
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $shop)
            foreach ($shop as $skey => $svalue)
                foreach (self::$definition['fields'] as $fkey => $fvalue)
                    if($skey == $fkey)
                        $shops[$key][$fkey] = $shop[$fkey];
                    
        return $shops;
    }
    
    public function getOfferShops($user)
    {
        $shops = array();
        
        $_db = new Db($user, 'offers');
        $_db->from('shop');
        $_db->orderBy('name');
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $shop)
            foreach ($shop as $skey => $svalue)
                foreach (self::$definition['fields'] as $fkey => $fvalue)
                    if($skey == $fkey)
                        $shops[$key][$fkey] = $shop[$fkey];
                    
        return $shops;
    }
    
    public static function getDefaultShop()
    {
        $shops = array();
        $_db = ObjectModel::$db;
        if(!$_db->db_table_exists(self::$definition['table'])){
            return array();
        }
        $_db->from('shop');
        $_db->where(array('is_default' => 1));
        $result = $_db->db_array_query($_db->query);
        foreach ($result as $shop)
        {
            $shops['id_shop']       = $shop['id_shop'];
            $shops['name']          = $shop['name'];
            $shops['active']        = $shop['active'];
            $shops['description']   = $shop['description'];
        }
        
        return $shops;
    }
    
    public function setDefaultShop($data)
    {
        
        $update['is_default'] = 1;
        $where['id_shop'] = array('operation' => '=', 'value' => (int)$data['id_shop']);
        
        $_db = ObjectModel::$db;
        $sql = $_db->update_string('shop', $update, $where);
        
        if($_db->db_exec($sql))
        {
            $update2['is_default'] = 0;
            $where2['id_shop'] = array('operation' => '!=', 'value' => (int)$data['id_shop']);

            $sql2 = $_db->update_string('shop', $update2, $where2);
            
            if($_db->db_exec($sql2))
            {
                $sql3 = $_db->db_query_str('SELECT name FROM '.$_db->prefix_table.'shop WHERE id_shop = "' . (int)$data['id_shop'] . '"');
                if($sql3)
                    foreach ($sql3 as $shop)
                        return $shop["name"];
            }   
        }
        
        return FALSE;
    }
    
    public function getProfilePrice($user, $id_shop,$id_mode)
    { 
        $prices = array();
        $price_name = array();
        $i = 1;
        
        $_db = new Db($user, 'offers');
        $_db->from('profile_price');
        $result = $_db->db_array_query($_db->query);
        
        if(isset($result) && !empty($result))
        {
            foreach ($result as $price)
            {
                $name = preg_replace('/[^A-Za-z0-9\-_]/', '', str_replace (" ", "_", $price['name']));
                if(isset($price_name[$price['name']]) && $price_name[$price['name']] == true)
                    $name = $name . '_' . $i++;
                
                $prices[$name]['id_profile_price'] = $price['id_profile_price'];
                $prices[$name]['price_percentage'] = $price['price_percentage'];
                $prices[$name]['price_value'] = $price['price_value'];
                $prices[$name]['rounding'] = $price['rounding'];
                $prices[$name]['name'] = $price['name'];

                $price_name[$price['name']] = true;
            }
        }
        
        return $prices;
    
    }
}