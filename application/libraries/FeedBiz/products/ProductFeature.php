<?php

class ProductFeature extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_feature',
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_feature'        =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_feature_value'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            
        ),
    );
    
}