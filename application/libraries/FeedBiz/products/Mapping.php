<?php

class Mapping extends ObjectModel
{
    
    public $attributes = array(
        'table'     => 'mapping_attribute',
        'primary'   => array('attribute'),
        'fields'    => array(
            'attribute'     =>  array('type' => 'text', 'required' => true ),
            'mapping'       =>  array('type' => 'text', 'required' => false, 'default_null' => true ),
            'session_key'   =>  array('type' => 'datetime', 'required' => false),
        ),
    );
    
    public $characteristics = array(
        'table'     => 'mapping_characteristic',
        'primary'   => array('character'),
        'fields'    => array(
            'c_character'     =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'mapping'       =>  array('type' => 'text', 'required' => false, 'default_null' => true ),
            'session_key'   =>  array('type' => 'datetime', 'required' => false ),
        ),
    );
    public $manufacturers = array(
        'table'     => 'mapping_manufacturer',
        'primary'   => array('manufacturer'),
        'fields'    => array(
            'manufacturer'  =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'mapping'       =>  array('type' => 'text', 'required' => false, 'default_null' => true ),
            'session_key'   =>  array('type' => 'datetime' ),
        ),
    );
    
    public function getAttributeValuesExcludeMappingValues($id_shop, $id_mode, $id_lang = null)
    {
        $attributes = array();
        
        $_db = ObjectModel::$db;
        
	$select ="(SELECT attribute FROM '.$_db->prefix_table.'mapping_attribute WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode . ') ";
	
        $_db->from('attribute_lang');
        $_db->where(array('id_shop' => (int)$id_shop));
        $_db->where_select(array('name' => "$select"), 'NOT IN');
        
        if(isset($id_lang) || !empty($id_lang))
            $_db->where_select(array('id_lang' => (int)$id_lang), '=');
        
        $_db->orderBy('name');
        $_db->groupBy('name');
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        
        foreach ($result as $key => $attribute)
        {
            foreach ($lang as $l)
                if($attribute['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $attributes[$key]['id_attribute_group'] = $attribute['id_attribute_group'];
            $attributes[$key]['id_attribute'] = $attribute['id_attribute'];
            $attributes[$key]['name'][$iso_code] = $attribute['name'];
        }
        
        return $attributes;
    }
    
    public function getMappingAttribute($id_shop, $id_mode)
    {   
        $mapping_attributes = array();
        
        $_db = ObjectModel::$db;
        
        $result = $_db->db_query_str(
                "SELECT 
                    al.name as attribute,
                    ma.mapping as mapping,
                    CASE 
                        WHEN ma.attribute IS NOT NULL 
                        THEN 'true' 
                        ELSE NULL 
                    END AS selected
                FROM {$_db->prefix_table}attribute_lang al
                LEFT OUTER JOIN {$_db->prefix_table}mapping_attribute ma ON al.name = ma.attribute AND al.id_shop = ma.id_shop
                WHERE al.id_shop = " . $id_shop . " 
                AND ma.id_mode = " . $id_mode . " 
                GROUP BY al.name
                ORDER BY al.name");

        foreach ($result as $key => $mapping_attribute)
        {
            $mapping_attributes[$key]['attribute'] = $mapping_attribute['attribute'];
            $mapping_attributes[$key]['value'] = $mapping_attribute['mapping'];
            $mapping_attributes[$key]['selected'] = $mapping_attribute['selected'];
        }
        
        $mapping_attributes['row'] = $this->getMappingNumRow('mapping_attribute', $id_shop, $id_mode);
        
        return $mapping_attributes;
    }
    
    
    public function getMappingCharacteristics($id_shop, $id_mode)
    {   
        $mapping_attributes = array();
        
        $_db = ObjectModel::$db;
        
        $_db->from('mapping_characteristic');
        $_db->where(array('id_shop' => (int)$id_shop, 'id_mode' => (int)$id_mode));
        $_db->orderBy('c_character');
        $result = $_db->db_array_query($_db->query);
       
        foreach ($result as $key => $mapping_attribute)
        {
            $mapping_attributes[$key]['character'] = $mapping_attribute['c_character'];
            $mapping_attributes[$key]['value'] = $mapping_attribute['mapping'];
        }
        
        $mapping_attributes['row'] = $this->getMappingNumRow('mapping_characteristic', $id_shop, $id_mode);
        
        return $mapping_attributes;
    }
    
    public function getManufacturerExcludeMapping($id_shop, $id_mode)
    {
        $attributes = array();
        
        $_db = ObjectModel::$db;
        
	$select = "(SELECT  manufacturer FROM '.$_db->prefix_table.'mapping_manufacturer WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode . ') ";
	
        $_db->from('manufacturer');
        $_db->where(array('id_shop' => (int)$id_shop));
        $_db->where_select(array('name' => "$select"), 'NOT IN');
        $_db->orderBy('name');
        $_db->groupBy('name');
        
        $result = $_db->db_array_query($_db->query );  
        
        foreach ($result as $key => $manufacturer)
            $attributes[$key]['name'] = $manufacturer['name'];
        
        return $attributes;
    }
    
    public function getMappingManufacturer($id_shop, $id_mode)
    {   
        $mapping_manufacturers = array();
        
        $_db = ObjectModel::$db;
        
        $result = $_db->db_query_str(
                "SELECT 
                    m.name as name,
                    mm.mapping as mapping,
                    CASE 
                        WHEN mm.manufacturer IS NOT NULL 
                        THEN 'true' 
                        ELSE NULL 
                    END AS selected
                FROM {$_db->prefix_table}manufacturer m
                LEFT OUTER JOIN {$_db->prefix_table}mapping_manufacturer mm ON m.name = mm.manufacturer AND m.id_shop = mm.id_shop  
                AND mm.id_mode = '" . $id_mode ."' 
                WHERE m.id_shop = '" . $id_shop ."' 
                GROUP BY m.name 
                ORDER BY m.name"); 
       
        foreach ($result as $key => $mapping_manufacturer)
        {
            $mapping_manufacturers[$key]['name'] = $mapping_manufacturer['name'];
            $mapping_manufacturers[$key]['value'] = $mapping_manufacturer['mapping'];
            $mapping_manufacturers[$key]['selected'] = $mapping_manufacturer['selected'];
        }
        
        $mapping_manufacturers['row'] = $this->getMappingNumRow('mapping_manufacturer', $id_shop, $id_mode);
         
        return $mapping_manufacturers;
    }
    
    public function saveMapping($key, $datas, $id_shop, $id_mode)
    {
        $session_key = date("Y-m-d H:i:s");
        $sql = '';
        $db = ObjectModel::$db;
        
        foreach ($datas as $data)
        {
            if(isset($data["mapping"]) && !empty($data["mapping"]) && $data["mapping"] != NULL)
            {
                $data["session_key"] = $session_key;
                $sql .= $db->insert_string('mapping_' . $key, $data);
                
                if($sql == false)
                    return false;
            }
        }
        
        if(!empty($sql))
	     $db->db_exec($sql);
         
        $delete =  "DELETE FROM {$db->prefix_table}mapping_" . $key . " "
		. "WHERE id_shop = " . (int)$id_shop ." AND id_mode = " . (int)$id_mode ." AND session_key < '" . $session_key . "'; ";
            
        if(!$db->db_exec($delete))
            return FALSE;
        
        
        return true;
    }
    
    public function getMappingNumRow($table, $id_shop, $id_mode)
    {   
        $_db = ObjectModel::$db;
        
        $_db->from($table);
        $_db->where(array('id_shop' => (int)$id_shop, 'id_mode' => (int)$id_mode));
        $result = $_db->db_query($_db->query);
        $rows = $_db->db_num_rows($result);
        
        return $rows;
    }
    
}