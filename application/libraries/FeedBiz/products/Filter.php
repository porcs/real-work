<?php

class Filter extends ObjectModel
{
    
    public $manufacturer = array(
        'table'     => 'exclude_manufacturer',
        'fields'    => array(
            'id_manufacturer'   =>  array('type' => 'int', 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'size' => 11),
            'session_key'       =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
        ),
    );
    
    public $supplier = array(
        'table'     => 'exclude_supplier',
        'fields'    => array(
            'id_supplier'       =>  array('type' => 'int', 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'size' => 11),
            'session_key'       =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
        ),
    );
    
    
    public function getFilterManufacturers($id_shop, $id_mode)
    { 
        $include_manufacturers = array();
        
        $_db = ObjectModel::$db;
        
	$select = "(SELECT id_manufacturer FROM '.$_db->prefix_table.'exclude_manufacturer WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode . ') ";
	
        $_db->from('manufacturer');
        $_db->where(array('id_shop' => (int)$id_shop));
        $_db->where_select(array('id_manufacturer' => "$select"), 'NOT IN');
	
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $manufacturer)
        {
            $include_manufacturers[$key]['id_manufacturer'] = $manufacturer['id_manufacturer'];
            $include_manufacturers[$key]['name'] = $manufacturer['name'];
        }
        
        return $include_manufacturers;
    }
    
    public function getExcludeManufacturers($id_shop, $id_mode = null)
    {   
        $exclude_manufacturers = $where = array();
        
        $_db = ObjectModel::$db;
        $_db->from('exclude_manufacturer');
        $where['id_shop'] = (int)$id_shop;
        
        if(isset($id_mode) && !empty($id_mode))
            $where['id_mode'] = (int)$id_mode;
            
        $_db->where($where);
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $manufacturer)
            $exclude_manufacturers[$key] = $manufacturer['id_manufacturer'];

        return $exclude_manufacturers;
    }
    
    public function getFilterSuppliers($id_shop, $id_mode)
    { 
        $include_suppliers = array();
        
        $_db = ObjectModel::$db;
	
	$select = "(SELECT id_supplier FROM '.$_db->prefix_table.'exclude_supplier WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode . ') ";
	
        $_db->from('supplier');
        $_db->where(array('id_shop' => (int)$id_shop));
        $_db->where_select(array('id_supplier' => "$select"), 'NOT IN');
	
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $supplier)
        {
            $include_suppliers[$key]['id_supplier'] = $supplier['id_supplier'];
            $include_suppliers[$key]['name'] = $supplier['name'];
        }
        
        return $include_suppliers;
    }
    
    public function getExcludeSuppliers($id_shop, $id_mode = null)
    {   
        $exclude_suppliers = $where = array();
        
        $_db = ObjectModel::$db;
        $_db->from('exclude_supplier');
        $where['id_shop'] = (int)$id_shop;
        
        if(isset($id_mode) && !empty($id_mode))
            $where['id_mode'] = (int)$id_mode;
        
        $_db->where($where);
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $suppliers)
            $exclude_suppliers[$key] = $suppliers['id_supplier'];
        
        return $exclude_suppliers;
    }
    
    public function saveFilters($datas)
    {
       
        $db = ObjectModel::$db;
        $sql = '';

        foreach ($datas as $key => $arr_data) {
	    
            foreach($arr_data as $data) {
		
                if(isset($data["id"]) && !empty($data["id"]) && $data["id"] != NULL)
                {
                    $session_key = $data["session_key"];
                    if($key == "manufacturers")
                    {
                        $insert_manufacturer['id_manufacturer'] = (int)$data["id"];
                        $insert_manufacturer['session_key']     = $data["session_key"];
                        $insert_manufacturer['id_shop']         = (int)$data["id_shop"];
                        $insert_manufacturer['id_mode']         = (int)$data["id_mode"];
                        
                        $sql .= $db->replace_string('exclude_manufacturer', $insert_manufacturer);
                    }
                    if($key == "suppliers")
                    {
                        $insert_supplier['id_supplier']         = (int)$data["id"];
                        $insert_supplier['session_key']         = $data["session_key"];
                        $insert_supplier['id_shop']             = (int)$data["id_shop"];
                        $insert_supplier['id_mode']             = (int)$data["id_mode"];
                        
                        $sql .= $db->replace_string('exclude_supplier', $insert_supplier);
                    }
                }
	    }
	}
		
        if(!empty($sql)){
	    if(!$db->db_exec($sql)) {
		return false;
	    }
	}
        
        if(empty($session_key)){
            $session_key = date('Y-m-d H:i:s');
        }
        
        if( $db->truncate_table( 'exclude_manufacturer' . ' WHERE session_key < "' . $session_key . '"') &&
	    $db->truncate_table('exclude_supplier' . ' WHERE session_key < "' . $session_key . '"')) 
	{
            return true;
	} else {
            return false;
	}
    }
    
}