<?php

class ProductAttribute extends ObjectModel
{
    public $id_product_attribute;
    public $reference;
    public $ean13;
    public $upc;
    public $price;
    public $price_type;
    public $quantity;
    public $weight;
    public $available_date = '0000-00-00';
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_attribute',
        'fields'    => array(
            'id_product'            =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_product_attribute'  =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'reference'             =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'old_reference'         =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'flag_update_ref'       => array('type' =>  'datetime', 'required' => false),
            'sku'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 64, 'default_null' => true ),
            'ean13'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 13, 'default_null' => true ),
            'upc'                   =>  array('type' => 'varchar', 'required' => false, 'size' => 12, 'default_null' => true ),
            'price'                 =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'price_type'            =>  array('type' => 'tinyint', 'required' => false, 'default_null' => true ),
            'quantity'              =>  array('type' => 'int', 'required' => true, 'size' => 10, 'default' => '0' ),
            'weight'                =>  array('type' => 'decimal', 'required' => false, 'size' => '20,6', 'default' => '0.000000' ),
            'available_date'        =>  array('type' => 'datetime', 'required' => false, 'default_null' => true ), 
            
        ),
       
    );
    
    public static function getProductAttributeById($id_product_attribute, $id_shop)
    {
         if(!isset($id_product_attribute) || empty($id_product_attribute))
            return (null);
         
        $productAttribute = array();
        $_db = ObjectModel::$db;
        $txt_col = "pac.id_product_attribute, a.id_attribute_group, agl.id_lang, agl.name, a.id_attribute, a.id_lang, a.name, l.iso_code";
         
        $columns = explode(',',$txt_col);
        $cols =$_db->put_array_tb_alias($columns);
            
        $sql = "SELECT ".implode(',',$cols) 
	    . " FROM {$_db->prefix_table}product_attribute_combination pac "
	    . "LEFT JOIN {$_db->prefix_table}attribute_group_lang agl ON (pac.id_attribute_group = agl.id_attribute_group AND pac.id_shop = agl.id_shop) "
	    . "LEFT JOIN {$_db->prefix_table}attribute_lang a ON (pac.id_attribute_group = a.id_attribute_group AND pac.id_attribute = a.id_attribute AND pac.id_shop = a.id_shop) "
	    . "LEFT JOIN {$_db->prefix_table}language l ON (a.id_lang = l.id_lang AND a.id_shop = l.id_shop) "
	    . "WHERE pac.id_product_attribute = " . (int)$id_product_attribute . " "
	    . "AND pac.id_shop = " . (int)$id_shop . "; ";
        
        $result = $_db->db_query_str($sql);
        
        foreach ($result as $attribute)
        {
            $iso = $attribute['l.iso_code'];
            $productAttribute[$attribute['a.id_attribute_group']]['name'][$iso] = $attribute['agl.name'];
            $attribute_name = isset($attribute['ma.mapping']) ? $attribute['ma.mapping'] : $attribute['a.name'];
            $productAttribute[$attribute['a.id_attribute_group']]['value'][$attribute['a.id_attribute']]['name'][$iso] = $attribute_name;
        }
        
        return $productAttribute;
    }
    
}