<?php

class ProductUrl extends ObjectModel
{  
    public static $definition = array(
        'table'     => 'product_url',
	'primary'       => array('id_product','id_shop'),
        'fields'    => array(
            'id_product'        =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_shop'           =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
	    'id_lang'           =>  array('type' => 'int', 'required' => false, 'size' => 10),
	    'link'              =>  array('type' => 'text', 'required' => true),
            'date_add'          =>  array('type' => 'datetime', 'required' => true ),
        ), 
    );  
    
    public static function getProductUrlByProductId($id_product, $id_shop, $id_lang = null)
    {
        if (!isset($id_product) || empty($id_product))
            return (false);
        
        $link = '';
	$where = array();        
        $_db = ObjectModel::$db;        
        
        $columns = array('link');
        $cols  =$_db->put_array_tb_alias($columns);
	
        $_db->select($cols);
        $_db->from('product_url');
	
        $where['id_product'] = (int)$id_product;
        $where['id_shop'] = (int)$id_shop;   
	
        if(isset($id_lang) && !empty($id_lang)){
            $where['id_lang'] = (int)$id_lang;
        }    
	
        if(!empty($where)) {
            $_db->where($where);
        }
        
        $result = $_db->db_array_query($_db->query);        
       
        foreach ($result as $values)
        {
            $link = $values['link'];
        }
        
        return $link;
    }
    
}