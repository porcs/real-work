<?php

require_once(dirname(__FILE__) . '/../../db.php');
require_once(dirname(__FILE__).'/../../../../libraries/ci_db_connect.php');
require_once(dirname(__FILE__) . '/../../tools.php');

abstract class ObjectModel {
    /**
     * @var array Contain object definition
     */
    public $id;
    protected static $user;
    public static $definition = array();

    /**
     * @var array Contain current object definition
     */
    protected $def;
    protected static $db = false;
    protected $_db = null;
    public static $database = 'products';
    public $id_shop = '';

    /* Build object */
    public function __construct($user, $id = null, $id_lang = null, $database = null, $shop = null) {

        if (!isset($user) && !strlen($user))
            return (false);

        ObjectModel::$user = $user;

        $class = get_class($this);
        if (!$class)
            return (false);

        $this->def = $class::$definition;

        if (isset($database) && $database != null && !empty($database))
            $database_name = $database;
        else
            $database_name = ObjectModel::$database;

        ObjectModel::$db = new Db($user, $database_name);        
        
        $this->_db = ObjectModel::$db;

        if (!$this->_db)
            return (false);

        if (isset($shop) && !empty($shop)) {
            $this->id_shop_default = $shop;
        } else {
            $shop = Shop::getDefaultShop();

            if (isset($shop) && !empty($shop))
                $this->id_shop_default = $shop['id_shop'];
        }

        if ($id) {
            $where = array();
            //echo 'ID - ' . $id;
            $this->id = (int) $id;

            if (isset($this->def) && is_array($this->def) && count($this->def)) {
                if (!(isset($this->def['table']) && strlen($this->def['table'])))
                    return (false);

                $this->_db->from($this->def['table'], 'a');

                if (!(isset($this->def['primary'][0]) && strlen($this->def['primary'][0])))
                    return (false);

                $where = array(
                    "a." . $this->def['primary'][0] => $this->id,
                    "a.id_shop" => (int) $this->id_shop_default
                );

                // Get lang informations
                if ($id_lang) {
                    $this->id_lang = (int) $id_lang;
                    if (!(isset($this->def['lang']) && is_array($this->def['lang'])))
                        return (false);

                    $where['b.id_lang'] = $this->id_lang;
                    $this->_db->leftJoin($this->def['table'] . '_lang', 'b', 'a.' . $this->def['primary'][0] . ' = b.' . $this->def['primary'][0] . ' AND a.id_shop = b. id_shop');
                }

                $this->_db->where($where);

                $object_datas = $this->_db->db_array_query($this->_db->query);

                if (is_array($object_datas) && count($object_datas)) {
                    foreach ($object_datas as $key_object => $value_object) {
                        
                        if ($id_lang && isset($this->def['lang']) && $this->def['lang'] /* && $key_object != "date_add" */) {
                            $this->_db->from($this->def['table'] . '_lang');
                            $this->_db->where(array($this->def['primary'][0] => (int) $id, "id_shop" => (int) $this->id_shop_default));

                            $object_datas_lang = $this->_db->db_array_query($this->_db->query);

                            if (is_array($object_datas_lang) && count($object_datas_lang)) {
                                $lang = Language::getLanguages((int) $this->id_shop_default);

                                foreach ($object_datas_lang as $row) {
                                	$iso_code = '';
                                	foreach ($lang as $l) {
                                		if ($row['id_lang'] == $l['id_lang']) {
                                			$iso_code = $l['iso_code'];
                                		}
                                	}
                                	
                                    foreach ($row as $key => $value) {                                        
                                        
                                        if (array_key_exists($key, $this) && $key != $this->def['primary'][0]) {
                                            if (!isset($object_datas[$key]) || !is_array($object_datas[$key]) || !count($object_datas[$key])) {
                                                
                                                if($key == "description" || $key == "description_short"){
                                                    $value = htmlspecialchars_decode($value);
                                                }
                                                
                                                if (isset($iso_code) && !empty($iso_code) && $key != "date_add") {
                                                	if(!isset($object_datas[$key_object][$key]) || !is_array($object_datas[$key_object][$key])){
                                                		$object_datas[$key_object][$key] = array();
                                                	}
                                                    $object_datas[$key_object][$key][$iso_code] = $value;
                                                } else if(!isset($object_datas[$key_object][$key])) {
                                                    $object_datas[$key_object][$key] = $value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (is_array($object_datas) && count($object_datas)) {
                    foreach ($object_datas as $values) {
                        foreach ($values as $key => $value) {
                            $key = str_replace(array("a.", "b."), "", $key);
                            if (array_key_exists($key, $this)) {                               
                                $this->{$key} = $value;
                            }
                        }
                    }
                }

                //set type
                foreach ($this->def['fields'] as $fields_key => $fields) {
                    if ($fields['type'] == 'int' || $fields['type'] == 'tinyint' || $fields['type'] == 'INTEGER')
                        settype($this->{$fields_key}, "integer");

                    if ($fields['type'] == 'decimal')
                        settype($this->{$fields_key}, "float");
                }
            }
        }
        //exit;
    }

    public function get_tmpcode($table, $column, $id) {
        $tmp = '';
        $this->_db->select(array('tmpcode'));
        $this->_db->from($table);
        $this->_db->where(array($column => $id));

        $result = $this->_db->db_array_query($this->_db->query);

        if (isset($result) && !empty($result))
            foreach ($result as $value)
                $tmp = $value['tmpcode'];

        return $tmp;
    }

    public static function get_marketplace_by_id($id) {
	
	$name = '';
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT name_offer_pkg FROM offer_packages WHERE id_offer_pkg = ".(int)$id.";" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $name = $row['name_offer_pkg'];
	}
	
        return $name;
    }
     
    public static function get_marketplace_by_name($name) {
	
	$id = null;
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT id_offer_pkg FROM offer_packages WHERE name_offer_pkg = '".$name."' ; " );
	
        while($row = $mysql_db->fetch($query)){ 
	    $id = (int) $row['id_offer_pkg'];
	}
	
        return $id;
    }
    
    public static function get_countries() {
	
	$countries = array();
	$mysql_db = new ci_db_connect();  
        $query = $mysql_db->select_query("SELECT * FROM offer_sub_packages WHERE iso_code IS NOT NULL;" );
	
        while($row = $mysql_db->fetch($query)){ 
	    $countries[$row['iso_code']]['id'] = $row['id_offer_sub_pkg'];
	    $countries[$row['iso_code']]['name'] = $row['title_offer_sub_pkg'];
	    $countries[$row['iso_code']]['iso_code'] = $row['iso_code'];
	}
	
        return $countries;
    }
    
    public static function _filter_data($table, $data, $except_primary=false, $no_prefix=false) {
        $filtered_data = array();
        $columns = self::_list_fields($table, $except_primary, $no_prefix);
	
        if (is_array($data)) {
            foreach ($columns as $column) {
                if (array_key_exists($column, $data)) {
		    if (is_array($data[$column])) {
			$filtered_data[$column] = serialize($data[$column]);
		    } else {
			$filtered_data[$column] = trim($data[$column]);
		    }
                } else {
                    $filtered_data[$column] = '';
                }
            }
        }
        return $filtered_data;
    }

    public static function _list_fields($table, $except_primary=false, $no_prefix=false) {
        $fieldnames = ObjectModel::$db->get_all_column_name($table, $except_primary, $no_prefix);
        return $fieldnames;
    }
}
