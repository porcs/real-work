<?php

class Category extends ObjectModel
{
    public $id_category;
    public $id_parent;
    public $is_root_category;
    public $name;    
    static $default_lang_id;
    static $arr_lang;
    static $arr_cat_lang;
    static $arr_cat_lang_by_id;
    
    public function __construct( $user, $database = null )
    {
        parent::__construct( $user, null, null, $database);
    }
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'category',
        'primary'   => array('id_category'),
        'fields'    => array(
            'id_category'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_parent'  =>  array('type' => 'int', 'required' => false, 'size' => 10, 'default_null' => true ),
            'is_root_category'  =>  array('type' => 'tinyint', 'required' => true, 'size' => 1, 'default' => '0' ),
            'tmpcode'           =>  array('type' => 'int', 'required' => false, 'size' => 11),
        ),
        'lang' => array(
            'fields'    => array(
                'id_category'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'   =>  array('type' => 'varchar', 'required' => false, 'size' => 128, 'default_null' => true ),
            ),
        ),
    );
    
    public static function getCategoryByProductID($id_product, $id_shop)
    {
        if (!isset($id_product) || empty($id_product))
            return NULL;
	
	$field = array(
	    'c.id_category as "c.id_category"', 
	    'cl.name as "cl.name"', 
	    'cl.id_lang as "cl.id_lang"', 
	    'c.id_parent as "c.id_parent"', 
	    'c.is_root_category as "c.is_root_category"'
	);
	
        $categories = array();
	
        $_db = ObjectModel::$db;
        $_db->select($field);
        $_db->from('product_category pc');
        $_db->leftJoin('category', 'c', '(pc.id_category = c.id_category) AND (pc.id_shop = c.id_shop)');
        $_db->leftJoin('category_lang', 'cl', '(pc.id_category = cl.id_category) AND (pc.id_shop = cl.id_shop)');
        $_db->where( array('pc.id_product' => (int)$id_product, 'c.id_shop' => (int)$id_shop) );
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        $iso_code = '';
	
        foreach ($result as $category)
        {
            foreach ($lang as $l) {
                if($category['cl.id_lang'] == $l['id_lang']) {
                    $iso_code = $l['iso_code'];
		}
	    }
		
            $categories[$category['c.id_category']]['name'][$iso_code] = $category['cl.name'];
            $parent = Category::getParentsByCatagoryID($category['c.id_parent'], (int)$id_shop);
            $categories[$category['c.id_category']]['parent']= $parent;
        }
        
        return $categories;
    }
    
    //Return array Category Name
    public static function getCategoryNameByID($id_category, $id_shop, $id_lang = null)
    {
        if (!isset($id_category) || empty($id_category))
            return NULL;
        
        $categories = array();
        $_db = ObjectModel::$db;
        
        if(!isset(static::$default_lang_id)){ 
            $sql = "select l.id_lang,l.is_default ,l.iso_code  from {$_db->prefix_table}language l where l.id_shop = '{$id_shop}'  ";
            $out = $_db->db_query_str($sql); 
            foreach($out as $v){
                if($v['is_default']==1) {
                static::$default_lang_id = $v['id_lang'];
                } 
                static::$arr_lang[$v['id_lang']] = $v;
            }
        }
        
        if(!isset(static::$arr_cat_lang)){
            static::$arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name,cl.id_lang from {$_db->prefix_table}category_lang cl where '{$id_shop}'= cl.id_shop  ";
            
            $result = $_db->db_query_str($sql_cat_lang);
            foreach($result as $v){
                static::$arr_cat_lang[$v['id_lang']][$v['id_category']] = $v['name'];
                static::$arr_cat_lang_by_id[$v['id_category']][$v['id_lang']] =$v;
            }  
        }  
        
        $result= isset(static::$arr_cat_lang_by_id[$id_category])?static::$arr_cat_lang_by_id[$id_category]:array();
        $lang = isset(static::$arr_lang)?static::$arr_lang:array();
	
        foreach ($result as $category)
        {
	    
            $lx=static::$default_lang_id;
	    
            foreach ($lang as $l) {
		
                if($category['id_lang'] == $l['id_lang']){
                    $iso_code = $l['iso_code'];
                    $lx = $l['id_lang'];
                } 
	    }
		
            if(isset($iso_code) && !empty($iso_code)){  
                $categories[$iso_code] = isset($category['name'])?$category['name']:'';
            }else{ 
                $categories = isset($result[$lx]['name'])?$result[$lx]['name']:'';
            }

        } 
        return $categories;
    }
        
    //Return array Category Parent
    public static function getParentsByCatagoryID($id_category, $id_shop)
    {
        if (!isset($id_category) || empty($id_category))
            return NULL;
                        
        $categories = array();
          
        $_db = ObjectModel::$db;
        $_db->from('category');
        $_db->where( array('id_category' => (int)$id_category, 'id_shop' => (int)$id_shop) );
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $category)
        {
            $categories[$id_category]['id'] = $category['id_category'];
            
            $name = Category::getCategoryNameByID($category['id_category'], $id_shop);
            
            if(!isset($name) || empty($name))
                $name = $category['name'];
                    
            $categories[$id_category]['name'] = $name;

            if($category['is_root_category'] == 0)
                $categories[$id_category]['parent']  = Category::getParentsByCatagoryID($category['id_parent'], $id_shop);
        }
        
        return $categories;
    }
    
    //Return array Categories
    public static function getCategories($id_shop, $id_lang = null)
    {
        $categories = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('c.*', 'cl.*'));
        $_db->from('category c');
        $_db->leftJoin('category_lang', 'cl', '(c.id_category = cl.id_category) AND (c.id_shop = cl.id_shop)');
        
        $arr_where['c.id_shop'] = (int)$id_shop;
                
        if(isset($id_lang) && !empty($id_lang))
            $arr_where['cl.id_lang'] = (int)$id_lang;
        
        $_db->where( $arr_where );
        $_db->orderBy('cl.name');
        $result = $_db->db_array_query($_db->query);

        $lang = Language::getLanguages($id_shop);
        
        foreach ($result as $category)
        {
            foreach ($lang as $l)
                if($category['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $categories[$category['id_category']]['name'][$iso_code] = $category['name'];
            $categories[$category['id_category']]['id_parent'] = $category['id_parent'];
            $categories[$category['id_category']]['is_root_category'] = $category['is_root_category'];
        }
            
        return $categories;
    }
    
    public function getSubCategories($id_parent, $id_shop)
    {
	
        $_db = ObjectModel::$db;
	
        if(!isset($this->arr_cat_by_parent)){
            $sql = "select c.id_category  ,c.id_parent from {$_db->prefix_table}category c where c.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
                if($v['id_parent']==$v['id_category']){continue;}
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] =$v;
            }
        }
	
        if(isset($this->arr_cat_by_parent[$id_parent])){
            return sizeof($this->arr_cat_by_parent[$id_parent]);
        }else{
            return 0;
        }
	
    }
    
    public function getSubCategories2($id_parent, $id_shop, $id_lang = null, $need_lang = true)
    {
        $categories = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
	
        if(!isset($this->arr_cat_lang)){
            $this->arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name,cl.id_lang from {$_db->prefix_table}category_lang cl where '{$id_shop}'= cl.id_shop  ";
            
            $result = $_db->db_query_str($sql_cat_lang);
            foreach($result as $v){
                $this->arr_cat_lang[$v['id_lang']][$v['id_category']] = $v['name']; 
            }  
        } 
        if(!isset($this->arr_cat_by_parent)){
            $sql = "select c.id_category  ,c.id_parent from {$_db->prefix_table}category c where c.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
                if($v['id_parent']==$v['id_category']){continue;}
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] =$v;
            }
        }
        
        $result = isset($this->arr_cat_by_parent[$id_parent])?$this->arr_cat_by_parent[$id_parent]:array();
        
        foreach ($result as $c_id =>$category)
        {
            $id_category = $c_id;
            $categories[$id_category]['id'] = $id_category;
            
            $name ='';
	    
            if(isset($id_lang) && !empty($id_lang) ){
                $name = isset($this->arr_cat_lang[$id_lang][$value['id_category']])?$this->arr_cat_lang[$id_lang][$value['id_category']]:'';
            }else{
                $id_lang = $this->default_lang_id;
                $name = isset($this->arr_cat_lang[$id_lang][$value['id_category']])?$this->arr_cat_lang[$id_lang][$value['id_category']]:'';
            }
	    
            $category['cl.name'] = $name;
	    
            if($need_lang) {
                $categories[$id_category]['name'] = $category['cl.name'];
	    } else {
                $categories[$id_category]['name'] = Category::getCategoryNameByID($id_category, $id_shop);
	    }
            
            $categories[$id_category]['child'] = $this->getSubCategories2($id_category, $id_shop, $id_lang, $need_lang);
        }

        return $categories;
    }
    
    public function getCatMapSumProd($cat_list,$id_shop){
	
        $prod_tmp = array('0');
//        echo '<pre>';
//	print_r($cat_list);
//        echo '</pre>';
//        return array();
        foreach($cat_list as $k => $c) {
	    
	    $cat_list[$k]['num']=0;
	    $cat_list[$k]['all_num']=0;
	    
            if(!isset($cat_list[$k]['cat_list'])) 
		continue;
	    
            $_db = ObjectModel::$db;
            if(empty($this->product_def_cat)){
            $sql = "select p.id_product as id , p.active as status ,p.id_category_default as cat_id from {$_db->prefix_table}product p where p.id_shop = '".$id_shop."' ";
            $result = $_db->db_query_str($sql);
            $this->product_def_cat=array();
                if(is_array($result)){
                    foreach($result as $r){
                        $this->product_def_cat[$r['cat_id']][$r['id']] = $r['status'];
                    }
                }
            } 
	    $cid = array($k);
            if(!empty($cat_list[$k]['cat_list'])){ 
                $cid = array_merge($cid,$cat_list[$k]['cat_list']);
            }
            $sum = 0;
            $act = 0; 
            foreach($cid as $c_id){ 
                if(!empty($this->product_def_cat[$c_id])){
                    foreach($this->product_def_cat[$c_id] as $p_id => $status){ 
                        if(isset($prod_tmp[$p_id]))continue;
                        $prod_tmp[$p_id]=$p_id;
                        if($status==1){
                            $act++;
                        }
                        $sum++;
                    }
                }
            } 
	     
            
//            if(isset($result)){
		
		$cat_list[$k]['num']=$act;
		$cat_list[$k]['all_num']=$sum;
//            }
	    
        } 
        return $cat_list;
    }
    public function getCatMapSumProd1($cat_list,$id_shop){
        
	$_db = ObjectModel::$db;
	
	$join = 'select '.$_db->prefix_table.'product_category.id_product, '.$_db->prefix_table.'product_category.id_category '
		. 'from '.$_db->prefix_table.'product_category '
		. 'join '.$_db->prefix_table.'category ON '.$_db->prefix_table.'product_category.id_category = '.$_db->prefix_table.'category.id_category ';
	
        $_db->select( array('pc.id_category','p.id_product' ,'p.active')  );
        $_db->from("product  p");
        $_db->join("left join ($join) pc on p.id_product = pc.id_product");

        $arr_where['p.id_shop'] = (int)$id_shop;       
        $_db->where( $arr_where );
	
        $_db->groupBy('p.id_product'); 
        $result = $_db->db_array_query($_db->query);
	
        foreach($cat_list as $k=>$c){
             $cat_list[$k]['num']=0;
             $cat_list[$k]['all_num']=0;
        }
	
        foreach($result as $p){
	    
            $cat_id = $p['pc.id_category'];
            $found = false;
	    
            foreach($cat_list as $k=> $c){
		
                if(in_array($cat_id,$c['cat_list'])){
		    
                    if($p['p.active']==1){			
			$cat_list[$k]['num']+=1;			
		    }
		    
                    $cat_list[$k]['all_num']+=1;
                    $found = true;
                    break;
                }
            } 
        }
	
        foreach($cat_list as $k => $c){
	    
            unset($cat_list[$k]['cat_list']);
        }
	
        return $cat_list;
    }
    
    public function getCatMapSumProd2($cat_list,$id_shop){
	
        $_db = ObjectModel::$db;
	$join = "LEFT JOIN '.$_db->prefix_table.'product_category pc ON p.id_product = pc.id_product "
		. "LEFT JOIN '.$_db->prefix_table.'category c ON pc.id_category = c.id_category";
	
        $_db->select(array('p.id_product', 'pc.id_category'));
        $_db->from('product p'); 
        $_db->join($join);
        $_db->where_select(array('c.id_category'=>'NULL'),'IS');
        $_db->groupBy('p.id_product'); 
	
        $result = $_db->db_array_query($_db->query);
	
        foreach($result as $r){
            $cid[] = $r['pc.id_category'];
        }
	
        $cid = implode(',',$cid);	
        $_db->select( array('pc.id_category','p.id_product' ,'p.active')  );
        $_db->from('product  p');         
        $_db->join('LEFT JOIN product_category pc ON pc.id_product = p.id_product');

        $arr_where['p.id_shop'] = (int)$id_shop;       
        $_db->where( $arr_where );	
        $_db->where_select(array('pc.id_category'=>'('.$cid.')'),'NOT IN');
        $_db->groupBy('p.id_product'); 
	
        $result = $_db->db_array_query($_db->query);
         
        foreach($cat_list as $k => $c){
	    
             $cat_list[$k]['num']=0;
             $cat_list[$k]['all_num']=0;
        }
	
        foreach($result as $p){
	    
            $cat_id = $p['pc.id_category'];
            $found = false;
	    
            foreach($cat_list as $k=> $c){
		
                if(in_array($cat_id,$c['cat_list'])){
		    
                    if($p['p.active']==1) {
			$cat_list[$k]['num']+=1;
		    }
		    
                    $cat_list[$k]['all_num']+=1;
                    $found = true;
                    break;
                }
            } 
        }
	
        foreach($cat_list as $k=>$c){
            unset($cat_list[$k]['cat_list']);
        }
	
        return $cat_list;
    }
    public function getSubArrCategories($id_parent, $id_shop, $root=false ){
	
        $categories = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        
        if(!isset($this->default_lang_id)){ 
	    
            $sql = "select l.id_lang,l.is_default,l.iso_code from {$_db->prefix_table}language l where l.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql); 
	    
            foreach($out as $v){
                if($v['is_default']==1) 
                $this->default_lang_id = $v['id_lang'];
                $this->arr_lang[$v['id_lang']] = $v;
            }
        }
        
        if(!isset($this->shop_name)){ 
	    
            $sql = "select name from {$_db->prefix_table}shop where id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
                $this->shop_name = $v['name'];
            }
        }
        
        if(!isset($this->arr_cat_by_parent)){
	    
            $sql = "select c.id_category  ,c.id_parent from {$_db->prefix_table}category c where c.id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
                if($v['id_parent']==$v['id_category']){continue;}
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] =$v;
            }
        }
        
        if(!isset($this->arr_cat_lang)){
	    
            $sql = "select cl.name ,cl.id_category from {$_db->prefix_table}category_lang cl where cl.id_shop = '$id_shop' and cl.id_lang = '$this->default_lang_id'";
            $out = $_db->db_query_str($sql);
             
            foreach($out as $v){
                $this->arr_cat_lang[$v['id_category']] =$v['name'];
            }
        }
        
	$result = isset($this->arr_cat_by_parent[$id_parent])?$this->arr_cat_by_parent[$id_parent]:array();
        $sum=0;
        $num=0;
        $out = array();
        $name = '';
        $out_arr = array();
	
        if(empty($result)){
            $out[$id_parent]['name']=isset($this->arr_cat_lang[$id_parent])?$this->arr_cat_lang[$id_parent]:'';
            $out[$id_parent]['parent']=0;
            $out[$id_parent]['shop_name']= $this->shop_name;
            $out[$id_parent]['cat_list'] =  array($id_parent) ;
        }else{
            foreach ($result as $category)
            { 
                if($root)
                    $out_arr = array();

                $id_category =    $category['id_category'];
                $out[$id_category]['name']=isset($this->arr_cat_lang[$id_category])?$this->arr_cat_lang[$id_category]:'';
                $out[$id_category]['parent']=$id_parent;
                $out[$id_category]['shop_name']= $this->shop_name;

                $out_arr = array_merge(array($id_category),$out_arr ,$this->getSubArrCategories($id_category, $id_shop)) ; 

                if($root){
                     $out[$id_category]['cat_list'] =  $out_arr ;
                }
            }
        }
        
        if($root){            
            return $out;
        }else{ 
            return $out_arr;
        }
	
    }
    
    public function getBigCategories($id_parent, $id_shop=null  )
    {
        $categories = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        $_db->select( array('c.id_category as "c.id_category"','cl.name as "cl.name"','p.active as "p.active"','count(p.active) as number_products')  );
        $_db->from('category c'); 
        $_db->join(' left join '.$_db->prefix_table.'product_category pc on pc.id_category = c.id_category AND (c.id_shop = pc.id_shop)');
        $_db->join('left join '.$_db->prefix_table.'product p on pc.id_product = p.id_product  AND (c.id_shop = p.id_shop) and p.active = 1 ');
        $_db->join('join '.$_db->prefix_table.'language l on cl.id_lang = l.id_lang and l.is_default = 1 ');
        $_db->leftJoin('category_lang', 'cl', '(c.id_category = cl.id_category) AND (c.id_shop = cl.id_shop)');
       
        $arr_where['c.id_parent'] = (int)$id_parent;
	
        if($id_shop!==null)
	    $arr_where['c.id_shop'] = (int)$id_shop;
        
        $_db->where( $arr_where );
        $_db->groupBy('c.id_category');
        $_db->orderBy( 'count(p.active) desc ' );
        
        $result = $_db->db_array_query($_db->query);
         
        $sum=0;
        $num=0;
        $out = array();
        $name = '';
         
        foreach ($result as $category)
        {
            $id_category =  $category['c.id_category'];
            $out[$id_category]['name'] = $category['cl.name'];
            $out[$id_category]['parent'] = $id_parent;
            $num = $category['number_products']; 
            $out[$id_category]['num'] = $num; 
            $num_ch = $this->getBigCategories($id_category, $id_shop )*1;
            $sum = $sum*1+ $num*1 + $num_ch;  
            $name = $out[$id_category]['name'];
            if($id_parent == 1){
                $out[$id_category]['num'] =  $num*1 + $num_ch; 
            }
        }
        
        if($id_parent == 1){ 
            return $out;
        }else{          
            return $sum;
        }
	
    }
    public function getRootCategories($id_shop)
    {
        $root = '';
        
        $_db = ObjectModel::$db;
        $_db->select(array('id_category'));
        $_db->from('category ');
        $arr_where['is_root_category'] = 1;
        $arr_where['id_shop'] = (int)$id_shop;
        
        $_db->where( $arr_where );
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $category)
            $root = $category['id_category'];
        if(empty($root)){
            $_db->select(array('id_category'));
            $_db->from('category ');
            $arr_where['is_root_category'] = 0;
            $arr_where['id_shop'] = (int)$id_shop;

            $_db->where( $arr_where );

            $result = $_db->db_array_query($_db->query);

            foreach ($result as $category)
                $root = $category['id_category'];

        }
        return $root;
    }
    
    public function getRootCategoriesAllShop($id_shop=null,$get_all=false)
    {
        $root = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('id_category','id_shop'));
        $_db->from('category');
        if(!$get_all){
            $arr_where['is_root_category'] = 1;
        }
        if(!empty($id_shop)){
            $arr_where['id_shop'] = $id_shop;
        }
        
        $_db->where( $arr_where );
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $category){ 
            $root[] = array('root_id'=>$category['id_category'],'shop_id'=>$category['id_shop']);
        }
        if(empty($root)){
            $_db = ObjectModel::$db;
            $_db->select(array('id_category','id_shop'));
            $_db->from('category');
            $arr_where['is_root_category'] = 0; 

            $_db->where( $arr_where );

            $result = $_db->db_array_query($_db->query);

            foreach ($result as $category){ 
                $root[] = array('root_id'=>$category['id_category'],'shop_id'=>$category['id_shop']);
            }
        }
        return $root;
    }
    public function checkSelectedCategory($id_shop=null, $id_mode=null)
    {
        $_db = ObjectModel::$db; 
	
        if($id_shop===null || $id_mode===null){	    
             $sql = "SELECT * FROM {$_db->prefix_table}category_selected";
        }else{
	    $sql = "SELECT count(id_category) FROM {$_db->prefix_table}category_selected
		    WHERE id_shop = '" . $id_shop . "'  /*AND id_mode = '" . $id_mode."'*/ group by id_shop,id_mode";
        } 
        $result = $_db->db_sqlit_query($sql); 
        return  $_db->db_num_rows($result);
    }
    
    public function getSelectedCategory($id_parent, $id_shop, $id_mode = null, $is_id = false, $id_lang = null)
    {
        $categories = array();
        
        $_db = ObjectModel::$db;
        $l = $lang = $join_lang = $mode = '';
        
        if(!isset($this->default_lang_id)){ 
	    
            $sql = "select l.id_lang,l.is_default ,l.iso_code from {$_db->prefix_table}language l where l.id_shop = ".(int)$id_shop."; ";
            $out = $_db->db_query_str($sql); 
	    
            foreach($out as $v){
                if($v['is_default'] == 1) {
		    $this->default_lang_id = $v['id_lang'];
                } 
                $this->arr_lang[$v['id_lang']] = $v;
            }
        }
        
        if(!isset($this->arr_cat_lang)){
	    
            $this->arr_cat_lang = array();
            $sql_cat_lang = "SELECT cl.id_category , cl.name,cl.id_lang from products_category_lang cl where cl.id_shop = ".(int)$id_shop."; ";
            
            $result = $_db->db_query_str($sql_cat_lang);
	    
            foreach($result as $v){
                $this->arr_cat_lang[$v['id_lang']][$v['id_category']] = $v['name']; 
            } 
	    
        } 
        if(!isset($this->arr_cat_by_parent)){
            $sql = "select c.id_category  ,c.id_parent from products_category c
                    INNER JOIN {$_db->prefix_table}category_selected cs ON cs.id_category = c.id_category AND cs.id_shop = c.id_shop "
                    . "where c.id_shop = '$id_shop' AND cs.id_shop = '$id_shop'   ";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
		
                if($v['id_parent'] == $v['id_category']) {
		    continue;
		}
		
                $this->arr_cat_by_parent[$v['id_parent']][$v['id_category']] =$v;
            }
        }
        
        $result = isset($this->arr_cat_by_parent[$id_parent])?$this->arr_cat_by_parent[$id_parent]:array();//$_db->db_query_str($sql);
        
        foreach ( $result as $keys => $value)
        {
            if(!$is_id)
            {
                $categories[$keys]['id_category'] = $value['id_category'];
                $categories[$keys]['child']  = $this->getSubCategories($value['id_category'], $id_shop);
            }
            else
            {
                $categories[$keys]['id'] = $value['id_category'];
                $categories[$keys]['child']  = $this->getSubCategories2($value['id_category'], $id_shop, $id_lang, true); 
            }
            
            $categories[$keys]['parent'] = $value['id_parent'];
            $name ='';
	    
            if(isset($id_lang) && !empty($id_lang) ){
                $name = isset($this->arr_cat_lang[$id_lang][$value['id_category']])?$this->arr_cat_lang[$id_lang][$value['id_category']]:'';
            }else{
                $id_lang = $this->default_lang_id;
                $name = isset($this->arr_cat_lang[$id_lang][$value['id_category']])?$this->arr_cat_lang[$id_lang][$value['id_category']]:'';
            }
	    
            $value['name'] = $name;
	    
            if(isset($value['name']) && !empty($value['name'])) {
                $categories[$keys]['name'] = $value['name'];
	    } else {
                $categories[$keys]['name'] = Category::getCategoryNameByID($value['id_category'], $id_shop);                      
	    }
            
            if(isset($categories[$keys]['child']) && !empty($categories[$keys]['child'])) {
                $categories[$keys]['type'] = "folder";
	    } else {
                $categories[$keys]['type'] = "item";
	    }
            
        }
        
        return $categories;
    }
    
    public function getAllRootCategories($id_shop,$is_selected=false)
    {
        $root = ''; 
        $_db = ObjectModel::$db;
        $_db->select(array('id_category','id_parent','is_root_category'));
        $_db->from('category'); 
        $arr_where['id_shop'] = (int)$id_shop;
        $_db->where( $arr_where );
        $result = $_db->db_array_query($_db->query); 
        $all_parent = array();
        $all_category = array();
        foreach ($result as $category){
            $all_parent[$category['id_parent']][$category['id_category']] = $category['id_category'];
            $all_category[$category['id_category']] = $category;
        } 
        if($is_selected){
            $this->getSelectedCategoriesOffers($id_shop);
        }
        foreach($all_parent as $par_id => $id){
//            if(!isset($all_category[$par_id])){
                foreach($all_parent[$par_id] as $c_id => $id){
                    if($is_selected&& !isset($this->arr_cat_selected[$c_id]) && isset($all_category[$c_id]['is_root_category'])&& $all_category[$c_id]['is_root_category']==0){continue;}
                    $root[] = $c_id;
                }
//            }
        } 
        return $root;
    }
    
    public function getSelectedCategoriesOffers(  $id_shop = null)
    {
        $_db = ObjectModel::$db;
        $content = array();
        $mode = '';
        $shop = '';
        
        if(!isset($this->arr_cat_selected)){
            $sql = "select * from offers_category_selected  where  id_shop = '$id_shop'";
            $out = $_db->db_query_str($sql);
            
            foreach($out as $v){
                $this->arr_cat_selected[$v['id_category']] =$v;
                $this->arr_cat_selected_mode[$v['id_mode']][$v['id_category']] =$v;
            }
        }
        if(empty($this->arr_cat_selected)){
            $this->arr_cat_selected=array();
        }
        return $this->arr_cat_selected;
    }
    
    public function getSelectedCategoryID($id_shop, $id_mode)
    {
        $categories = array();
        
        $_db = ObjectModel::$db;
        $_db->from('category_selected');
        $_db->where(array('id_shop' => $id_shop));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $category){
            $categories[$key] = $category['id_category'];
        }

        return $categories;
    }
    
    public function getSelectedCategories($id_shop, $id_mode)
    {
        $categories = array();
        
        $_db = ObjectModel::$db;
        $_db->from('category_selected');
        $_db->where(array('id_shop' => $id_shop, 'id_mode' => $id_mode));
        
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $key => $category){
            $categories[$key] = $this->getParents($category['id_category'], $id_shop);
        }

        return $categories;
    }
   
    public function getParents($id, $id_shop)
    {
        $parents = $this->getParentsByCatagoryID($id, $id_shop);
        if(isset($parents) && !empty($parents))
        {
            $ref = array();
            foreach($parents as $key => $dep)
            {
                if(isset($dep['parent']) && !empty($dep['parent']))
                {
                    $item_id = $dep['id'];

                    foreach ($dep['parent'] as $parent)
                    {
                        $id_parent = $parent['id'];
                        
                        if(isset($parent['parent']))
                            $ref[$item_id] =  $this->getParents($id_parent, $id_shop);
                        
                        if(!isset($ref[$id_parent]))  //Child
                             $ref[$id_parent] = array('id' => $item_id, 'name' => $dep['name'], 'parent' => $id_parent);
    
                        $ref[$item_id]['children'][$id_parent] = $ref[$id_parent];
                        
                    }
                }
            }
        }
        
        return $ref[$id];
        
    }
    
    public static function saveSelectedCategory($datas, $id_shop, $id_mode)
    {
        $db = ObjectModel::$db;
        
        $db->db_trans_begin();
        
        $sql_trunc = $db->truncate_table($_db->prefix_table.'category_selected WHERE id_shop = ' . $id_shop . ' AND id_mode = ' . $id_mode);
        
        if($sql_trunc)
            
            foreach ($datas as $data)

                if(isset($data["id_category"]) && !empty($data["id_category"]) && $data["id_category"] != NULL)
                {
                    $sql = $db->insert_string('category_selected', $data);

                    if($sql == false)
                        return false;

                    if(!$db->db_exec($sql))
                        return false;
                }
                
        $db->db_trans_commit();
            
        return true;
    }
    public function getCategoryMaxDate(  $id_shop = null)
    {
        $_db = ObjectModel::$db;  
        $sql = "SELECT "
		. "max( date_add ) as date  "
		. "FROM {$_db->prefix_table}category "
		. "WHERE id_shop = '".$id_shop."'";
        
        $result = $_db->db_query_str($sql);
	
        if( !isset($result[0]['date']) || (isset($result[0]['date']) && empty($result[0]['date']))) 
	    return null; 

        return $result[0]['date'];
    }
    
    public static function getCategoriesSortRoot($id_shop, $id_lang = null)
    {
        $categories = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('c.*', 'cl.*','cl.name as "cl.name"'));
        $_db->from('category c');
        $_db->leftJoin('category_lang', 'cl', '(c.id_category = cl.id_category) AND (c.id_shop = cl.id_shop)');
        
        $arr_where['c.id_shop'] = (int)$id_shop;
                
        if(isset($id_lang) && !empty($id_lang))
            $arr_where['cl.id_lang'] = (int)$id_lang;
        
        $_db->where( $arr_where );
        $_db->orderBy('c.is_root_category desc,c.id_category ');
	
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        $all_parent = array(); 
        foreach ($result as $category)
        {
            foreach ($lang as $l){
                $category['id_lang'] = isset($category['id_lang'])?$category['id_lang']:$category['id_lang'];
                if($category['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];
            }
            
            $categories[$category['id_category']]['name'][$iso_code] = $category['cl.name'];
            $categories[$category['id_category']]['id_parent'] = $category['id_parent'];
            $categories[$category['id_category']]['is_root_category'] = $category['is_root_category']; 
            if(!isset($all_parent[$category['id_parent']])){
                $all_parent[$category['id_parent']]=array();
            }
                $all_parent[$category['id_parent']][$category['id_category']]=$category['id_category'];
            
        } 
            foreach($all_parent as $par_id => $all_child){
                if(isset($categories[$par_id])){//is not root
                    
                }else{//all child is root
                    foreach($all_child as $cid=>$id){
                        $categories[$cid]['is_root_category'] = 1;
                    }
                }
            } 
            
        return $categories;
    }
    
}