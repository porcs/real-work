<?php

class Tag extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'tag',
        'primary'   => array('id_tag'),
        'fields'    => array(
            'id_tag'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_lang'      =>  array('type' => 'int', 'required' => false, 'size' => 10),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 32),
        ),
    );
    
    public static function getProductTagsByProductId($id_product, $id_shop, $id_lang = null)
    {
        if (!isset($id_product) || empty($id_product))
            return FALSE;
        
        $tags = $where = array();        
        $_db = ObjectModel::$db;        
        
        $columns = array('pt.id_product_tag', 'pt.id_lang', 'pt.name');
        $cols  =$_db->put_array_tb_alias($columns);
	
        $_db->select($cols);
        $_db->from('product_tag t');
        $_db->leftJoin('product_tag_lang', 'pt', '(pt.id_product_tag = t.id_product_tag) AND (pt.id_shop = t.id_shop)');
	
        $where['t.id_product'] = (int)$id_product;
        $where['t.id_shop'] = (int)$id_shop;   
	
        if(isset($id_lang) && !empty($id_lang)){
            $where['pt.id_lang'] = (int)$id_lang;
        }    
	
        if(!empty($where)) {
            $_db->where($where);
        }
        
        $result = $_db->db_array_query($_db->query);        
        $lang = Language::getLanguages($id_shop);
        $iso_code = -1;
	
        foreach ($result as $values)
        {
            if( isset($lang[$values['pt.id_lang']])){
                $iso_code = $lang[$values['pt.id_lang']]['iso_code'];
            }
            if($iso_code != -1){
            	$tags[$values['pt.id_product_tag']][$iso_code] = $values['pt.name'];
            }
        }
        
        return !empty($tags) ? $tags : null;
    }
    
}