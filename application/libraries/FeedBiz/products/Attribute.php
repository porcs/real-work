<?php

class Attribute extends ObjectModel
{
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'attribute',
        'primary'   => array('id_attribute'),
        'fields'    => array(
            'id_attribute_group'    =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'id_attribute'          =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'color'                 =>  array('type' => 'varchar', 'required' => false, 'size' => 32, 'default_null' => true ),
        ),
        'lang' => array(
            'fields'    => array(
                'id_attribute_group'=>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_attribute'      =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'id_lang'           =>  array('type' => 'int', 'required' => true, 'size' => 10),
                'name'              =>  array('type' => 'varchar', 'required' => true, 'size' => 128),
            ),
        ),
        'table_link' => array( 
            '0' => array(
                'name' => 'attribute_group', 
                'lang' => true 
            ),
            
        ),
    );
    
    public static function getAttributes($id_shop, $id_lang = null, $id_attribute_group = null, $id_attribute = null)
    {
        $attributes = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('ag.*', 'agl.*', 'a.*',
            'al.*','agl.id_lang as "agl.id_lang"',
            'al.id_lang as "al.id_lang"',
            'ag.id_attribute_group as "ag.id_attribute_group"',
            'al.id_attribute as "al.id_attribute"' ,
            'agl.name as "agl.name"' ,
            'ag.is_color_group as "ag.is_color_group"' ,
            'ag.group_type as "ag.group_type"', 
            'al.name as "al.name"','a.color as "a.color"'));
        $_db->from('attribute_group ag');
        $_db->leftJoin('attribute_group_lang', 'agl', '(ag.id_attribute_group = agl.id_attribute_group) AND (ag.id_shop = agl.id_shop)');
        $_db->leftJoin('attribute', 'a', '(ag.id_attribute_group = a.id_attribute_group) AND (ag.id_shop = a.id_shop)');
        $_db->leftJoin('attribute_lang', 'al', '(ag.id_attribute_group = al.id_attribute_group) AND (a.id_attribute = al.id_attribute) AND (a.id_shop = al.id_shop)');
        
        $arr_where['ag.id_shop'] = (int)$id_shop;
        
        if(isset($id_lang) || !empty($id_lang))
        {
            $arr_where['agl.id_lang'] = (int)$id_lang;
            $arr_where['al.id_lang'] = (int)$id_lang;
        }
        if(isset($id_attribute_group) || !empty($id_attribute_group))
        {
            $arr_where['ag.id_attribute_group'] = (int)$id_attribute_group;
        }
        if(isset($id_attribute) || !empty($id_attribute))
        {
            $arr_where['al.id_attribute'] = (int)$id_attribute;
        }
        
        $_db->where($arr_where);
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        foreach ($result as $attribute)
        {
            foreach ($lang as $l)
            {
                if($attribute['agl.id_lang'] == $l['id_lang'])
                    $iso_code_group = $l['iso_code'];

                if($attribute['al.id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];
            }

            $attributes[$attribute['ag.id_attribute_group']]['name'][$iso_code_group] = $attribute['agl.name'];
            $attributes[$attribute['ag.id_attribute_group']]['is_color_group'] = $attribute['ag.is_color_group'];
            $attributes[$attribute['ag.id_attribute_group']]['group_type'] = $attribute['ag.group_type'];

            $attributes[$attribute['ag.id_attribute_group']]['value'][$attribute['al.id_attribute']]['name'][$iso_code] = $attribute['al.name'];
            if($attribute['ag.is_color_group'] == 1)
                $attributes[$attribute['ag.id_attribute_group']]['value'][$attribute['al.id_attribute']]['code'] = $attribute['a.color'];
        }
        
        return $attributes;
    }
    
        //code needed for generating xml 
        public static function getAttributesNames($id_shop, $id_lang = null, $id_attribute_group = null, $id_attribute = null)
    {
        $attributes = array();
        $arr_where = array();
        
        $_db = ObjectModel::$db;
        $_db->select(array('ag.*', 'agl.*', 'a.*',
            'al.*','agl.id_lang as "agl.id_lang"',
            'al.id_lang as "al.id_lang"',
            'ag.id_attribute_group as "ag.id_attribute_group"',
            'al.id_attribute as "al.id_attribute"' ,
            'agl.name as "agl.name"' ,
            'ag.is_color_group as "ag.is_color_group"' ,
            'ag.group_type as "ag.group_type"', 
            'al.name as "al.name"','a.color as "a.color"'));
        $_db->from('attribute_group ag');
        $_db->leftJoin('attribute_group_lang', 'agl', '(ag.id_attribute_group = agl.id_attribute_group) AND (ag.id_shop = agl.id_shop)');
        $_db->leftJoin('attribute', 'a', '(ag.id_attribute_group = a.id_attribute_group) AND (ag.id_shop = a.id_shop)');
        $_db->leftJoin('attribute_lang', 'al', '(ag.id_attribute_group = al.id_attribute_group) AND (a.id_attribute = al.id_attribute) AND (a.id_shop = al.id_shop)');
        
        $arr_where['ag.id_shop'] = (int)$id_shop;
        
        if(isset($id_lang) || !empty($id_lang))
        {
            $arr_where['agl.id_lang'] = (int)$id_lang;
            $arr_where['al.id_lang'] = (int)$id_lang;
        }
        if(isset($id_attribute_group) || !empty($id_attribute_group))
        {
            $arr_where['ag.id_attribute_group'] = (int)$id_attribute_group;
        }
        if(isset($id_attribute) || !empty($id_attribute))
        {
            $arr_where['al.id_attribute'] = (int)$id_attribute;
        }
        
        $_db->where($arr_where);
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        foreach ($result as $attribute)
        {
            $attributes[$attribute['ag.id_attribute_group']]['name'] = $attribute['agl.name'];       
        }
        
        return $attributes;
    }
    //end of code
    
    public function getAttributeValues($id_shop, $id_lang = null)
    {
        $where = array();
        $attributes = array();
        
        $_db = ObjectModel::$db;
        $_db->from('attribute_lang');
        $where['id_shop'] = (int)$id_shop;
       
        if(isset($id_lang) || !empty($id_lang))
            $where['id_lang'] = (int)$id_lang;
        
        $_db->where($where);
        
        $_db->orderBy('name');
        $_db->groupBy('name');
        
        $result = $_db->db_array_query($_db->query);
        
        $lang = Language::getLanguages($id_shop);
        
        foreach ($result as $key => $attribute)
        {
            foreach ($lang as $l)
                if($attribute['id_lang'] == $l['id_lang'])
                    $iso_code = $l['iso_code'];

            $attributes[$key]['id_attribute_group'] = $attribute['id_attribute_group'];
            $attributes[$key]['id_attribute'] = $attribute['id_attribute'];
            $attributes[$key]['name'][$iso_code] = $attribute['name'];
        }
        
        return $attributes;
    }
}