<?php

class ProductImage extends ObjectModel
{
    public $id_product;
    public $id_image;
    public $image_url;
    public $image_type;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'product_image',
        'fields'    => array(
            'id_product'   =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'id_image'     =>  array('type' => 'int', 'required' => true, 'size' => 10 ),
            'image_url'    =>  array('type' => 'text', 'required' => false, ),
            'image_type'   =>  array('type' => 'varchar', 'required' => true, 'size' => 20 ),
        ),
    );
    
    public static function getImagesById($id_product, $id_image, $id_shop)
    {
        if(!isset($id_image) || empty($id_image))
            return NULL;
        
        $productImages = array();
                
        $_db = ObjectModel::$db;
        $_db->from('product_image');
        $_db->where( array('id_product' => (int)$id_product, 'id_image' => (int)$id_image, 'id_shop' => (int)$id_shop) );
        $result = $_db->db_array_query($_db->query);
        
        foreach ($result as $image)
        {
            $productImages['url'] = $image['image_url'];
            $productImages['type'] = $image['image_type'];
        }
        
        return $productImages;
    }
    
    public function getImages($id_shop)
    {
        $productImages = array();
                
        $_db = ObjectModel::$db;
        $result = $_db->db_query_str("SELECT * FROM {$_db->prefix_table}pproduct_image WHERE id_shop = " . $id_shop);
        
        foreach ($result as $keys =>$image)
        {
            foreach ($image as $key => $img)
            {
                if(!is_int($key))
                    $productImages[$keys][$key] = $img;
            }
        }
        
        return $productImages;
    }
    
    public function setImages($sql)
    {
        $return = true;
        $_db = ObjectModel::$db;
        
        if(!$_db->db_exec($sql,true))
            $return = false;
        
        return $return;
    }
    
}