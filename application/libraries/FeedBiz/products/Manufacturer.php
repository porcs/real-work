<?php

class Manufacturer extends ObjectModel
{
    public $id_manufacturer;
    public $name;
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table'     => 'manufacturer',
        'primary'   => array('id_manufacturer'),
        'fields'    => array(
            'id_manufacturer'   =>  array('type' => 'int', 'required' => true, 'size' => 10),
            'tmpcode'       =>  array('type' => 'int', 'required' => false, 'size' => 11),
            'name'  =>  array('type' => 'varchar', 'required' => true, 'size' => 64),
            'date_add'              =>  array('type' => 'datetime', 'required' => true ),
        ),
    );
    
    public static function getManufacturerByProductId($id_product, $id_shop, $id_mode = null)
    {
        if(!isset($id_product) || empty($id_product))
            return NULL;
         
        $_db = ObjectModel::$db;
        $where = array();
        
        $query_feilds = array('m.id_manufacturer',  'mm.mapping', 'm.name');
        $cols =$_db->put_array_tb_alias($query_feilds);
        $_db->select($cols);
        $_db->from('product p');
        $_db->leftJoin('manufacturer', 'm', '(p.id_manufacturer = m.id_manufacturer AND p.id_shop = m.id_shop)');
        $_db->leftJoin('mapping_manufacturer', 'mm', '(mm.manufacturer = m.name AND mm.id_shop = m.id_shop)');
        
        $where['p.id_product'] = (int)$id_product;
        $where['p.id_shop'] = (int)$id_shop;
        
        if(isset($id_mode) && $id_mode != NULL)
            $where['mm.id_mode'] = (int)$id_mode;
        
        $_db->where( $where );

        $result = $_db->db_array_query($_db->query);

        $manufacturer = array();
        
        foreach ($result as $values)
        {
            if(isset($values['m.id_manufacturer']) || !empty($values['m.id_manufacturer']))
            {
                $manufacturer_name = isset($values['mm.mapping']) ? $values['mm.mapping'] : $values['m.name'];
                $manufacturer[$values['m.id_manufacturer']] = $manufacturer_name;
            } else {
                return null;
	    }
        }

        return $manufacturer ;
    }
    
    public static function getManufacturers($id_shop)
    {
        $_db = ObjectModel::$db;
        $_db->from(self::$definition['table']);
        $_db->where(array("id_shop"  => (int)$id_shop));
        $result = $_db->db_array_query($_db->query);

        $manufacturer = array();
        
        foreach ($result as $values)
        {
            if(isset($values['id_manufacturer']) || !empty($values['id_manufacturer']))
            {
                $manufacturer[$values['id_manufacturer']] = $values['name'];
            }
            else 
                return null;
        }
        
        return $manufacturer ;
    }
    
}